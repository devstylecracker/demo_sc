
  <style>
     .datepair-wrap {
      position: relative;
      overflow: hidden;
    }

    .animation-delay-100 {
      -webkit-animation-delay: 100ms;
      animation-delay: 100ms;
    }
    
    .animation-duration-300 {
      -webkit-animation-duration: 300ms;
      animation-duration: 300ms;
    }
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
    body{
      font-size: 12px !important;
    }
    .label{
      font-size: 84% !important;
    }
    .table>tbody>tr>td{
      padding: 5px !important;
      word-wrap: break-word;
    }
     .lightbox-block {
      max-width: 600px;
      padding: 15px 20px;
      margin: 40px auto;
      overflow: auto;
      background: #fff;
      border-radius: 3px;
    }
    .highlighted {
    background-color: #E5C37E;
}
  .icon {
      font-size: 22px;

    }
    .badge{
      font-size: 10px;
      font-weight: 500;
      border-radius: 3px;
    }
    .cursor{
      cursor: pointer;
    }
    .alertify-cover {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1700;
    background-color: black;
    filter: alpha(opacity=0);
    opacity: 0.7;
} 
  .table a {
  text-decoration: underline;
  color: #57c7d4;
  font-weight: 500;
  cursor: pointer;
}
.step.col-md-4.current, .step.col-md-4.disabled{
  height: 100px;
}
.lightboxsize{
  max-width: 800px !important;
}
.radio-custom label::before {
   
    display:none;
   
}
.img-admin {
    
    width: 24px;
    padding: 4px;
    border:1px dashed #76838f;
}
.img-admin1 {
    
    width: 34px;
    padding: 4px;
    border:1px dashed #76838f;
}
img{
  width: 100%;
}
.table .cell-120 {
    width: 120px;
    display: inline-table;
    
}
@media (min-width: 768px)
{
.form-inline .radio-custom label {
     padding-left: 0px; 
}
}
.radio-custom {
     padding-left: 0px; 
}
.btn-sm{
  float: right;
}

 .new{
  top: -36px !important;
  margin: -8px 1.8em !important; 
 }
  </style>
  <!-- Inline -->
  <style>
    .animation-delay-100 {
      -webkit-animation-delay: 100ms;
      animation-delay: 100ms;
    }
    
    .animation-duration-300 {
      -webkit-animation-duration: 300ms;
      animation-duration: 300ms;
    }
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
    body{
      font-size: 12px !important;
    }
    .label{
      font-size: 84% !important;
    }
    .table>tbody>tr>td{
      padding: 5px !important;
      word-wrap: break-word;
    }
     .lightbox-block {
      max-width: 600px;
      padding: 15px 20px;
      margin: 40px auto;
      overflow: auto;
      background: #fff;
      border-radius: 3px;
    }
    .highlighted {
    background-color: #E5C37E;
}
  .icon {
      font-size: 24px;

    }
    .badge{
      font-size: 10px;
      font-weight: 500;
      border-radius: 3px;
    }
    .cursor{
      cursor: pointer;
    }
    .alertify-cover {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1700;
    background-color: black;
    filter: alpha(opacity=0);
    opacity: 0.7;
} 
  .table a {
  text-decoration: underline;
  color: #57c7d4;
  font-weight: 500;
}
.table.dataTable tbody tr.active td, .table.dataTable tbody tr.active th {
        font-weight: 400;

}
.radio-custom label::before {
   
    display:none;
   
}
.img-admin {
    
    width: 24px;
    padding: 4px;
    border:1px dashed #76838f;
}
.img-admin1 {
    
    width: 34px;
    padding: 4px;
    border:1px dashed #76838f;
}
img{
  width: 100%;
}

  </style>
</head>

<body class="site-menubar-fold" data-auto-menubar="false">
  <?php include('common/nav_view.php'); ?>

  <div class="page bg-white animsition">

    <!-- Mailbox Content -->
    <div class="page-main">

      <!-- Mailbox Header -->
      <div class="page-header">
        <h1 class="page-title">Box transaction Management</h1>
      </div>

      <!-- Mailbox Content -->
      <div class="page-content">
       <div class="panel">
       
        <!-- Mailbox -->
        <table class="table table-hover dataTable table-striped width-full cursor" id="exampleFixedHeader" align="center">
          <thead>
             <tr>
                <th>Sr.No.</th>
                <th class="cell-80">Order ID</th>
                <th class="cell-60">Invoice ID</th>
                <th class="cell-80">Customer ID</th>
                <th>Order Status</th>
                <th>Internal Status</th>
                <th>Payment Status</th>
                <th>Payment Mode</th>
                <th>Transact ID</th>
                <th>Amount</th>
                <th>Payment Complete</th>
                <th class="cell-100">Actions</th>
             </tr>
          </thead>
          <tbody class="main_table">  
           <tr align="center">
                <td>1</td>
                <td>50236_5236 SC1234567890</td>
                <td>123 456 789</td>
                <td>SCBOX1904848500</td>
                <td>Order Status (Customer Facing)</td>
                <td>Internal Status</td>
                <td><span class="badge badge-success">Paid</span><br><span class="badge badge-danger">Payment Pending</span><br><span class="badge badge-danger">Refund Pending</span></td>
                <td><span class="badge badge-primary">Online</span><br><span class="badge badge-primary">COD</span></td>
                <td>SC1234567890</td>
                <td>(+)2,500.00/<br>(-)2,500.00</td>
                <td>Yes/ <br> No</td>
                 <td align="left">
              <form>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step1" id="step1" value="0" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="Are you sure you want to do Confirm the order?"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel"><label for="step1"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="Mark as Received" title=""><i class="wb-thumb-up" aria-hidden="true"></i></div></label>

                    </div>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step1" id="step11" value="1" class="load_more"><label for="step11"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="Issue Invoice" title=""><i class="wb-order" aria-hidden="true"></i></div></label>
                    </div>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step4" id="stepcall" value="1" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="Are you sure you want to proceed with the call?"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel"><label for="stepcall"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="Issue Refund" title=""><i class="wb-reply" aria-hidden="true"></i></div></label>
                    </div>
                    <div class="radio-custom radio-primary">
                      <input type="radio" name="step1" id="target" value="3" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="Are you sure you want to do this?"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel"><label for="target"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="Update Payment Details" title=""><i class="wb-pencil" aria-hidden="true"></i></div></label>
                    </div>  
                </form>

                <div style="padding-top: 30px;"><a id="examplePopWithList" href="javascript:void(0)"><div class="img-admin"><img src="<?php echo base_url('admin_assets/images/scicon/comment.png');?>"></div><span class="badge badge-danger up new">1</span></a>
                  <div class="hidden" id="examplePopoverList">
                      <form style="padding: 20px;">
                        <div class="form-group">
                        <label class="control-label" for="textarea">Remarks</label> 
                        <br/>
                        <textarea class="form-control" id="textarea" rows="6"></textarea>
                      </div>
                      <div>
                        <button type="submit" class="btn btn-success btn-sm">Submit</button>
                      </div>
                      </form>
                  </div></div>

                </td>
                
            </tr>
          </tbody>
        </table>
      </div>
     
        
      </div>
    </div>
  </div>
   <?php include('common/footer.php'); ?>
<script type="text/javascript">

      (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
      });

       // Fixed Header Example
      // --------------------
      (function() {
        // initialize datatable
        var table = $('#exampleFixedHeader').DataTable({
          responsive: true,
          "bPaginate": false,
          "sDom": "lfirtp",
        });

        // initialize FixedHeader
        var offsetTop = 0;
        if ($('.site-navbar').length > 0) {
          offsetTop = $('.site-navbar').eq(0).innerHeight();
        }
        var fixedHeader = new FixedHeader(table, {
          offsetTop: offsetTop
        });

        // redraw fixedHeaders as necessary
        $(window).resize(function() {
          fixedHeader._fnUpdateClones(true);
          fixedHeader._fnUpdatePositions();
        });
      })();

       })(document, window, jQuery);
    </script>



