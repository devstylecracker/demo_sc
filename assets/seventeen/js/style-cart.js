var is_order_placed = false; 
$( document ).ready(function() {
	//$('#notify_me').hide();
	if(navigator.cookieEnabled == ''){
		alert("Please enabled cookies");
	}
	if($('.his_active').length > 0){
		$('.his_active').trigger('click');
	}
	if(getCookie('SCUniqueID')=='' || typeof getCookie('SCUniqueID')=='undefined'){
    	var sctimestamp = new Date().getTime();
    	document.cookie="SCUniqueID="+sctimestamp+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2500 12:00:00 UTC; path=/";
	}
	
	scUpdateCart(getCookie('SCUniqueID'),'','','','','');   

	$('#sclogout').on('click',function(e){
		var final_cookies = getCookie('SCUniqueID');
		document.cookie="SCUniqueID="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
		window.location = sc_baseurl+"schome/logout";
	});

	$('#add_new_address').on('click',function(e){	
		$('#uc_address').val('');
		$('#uc_pincode').val('');
		$('#uc_city').val('');
		$('#uc_state').val('');
		$('#uc_mobile').val('');
	});

	$('[data-toggle="popover"]').popover();
});


/* Open the cart page */
function openCartPage(){	
	scUpdateCart(getCookie('SCUniqueID'),'','','','','');
}

 $('#mycart').on('shown', function() {
      money_formatter(".price");
      money_formatter(".cart-price");
  });


/* get cookie value */
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

function scUpdateCart(uniquecookie,productid,userid,productSize,frmsource,lookid){
	$('.delivery-status').html('');
	if((uniquecookie!='' && productid!='' && productSize!='') || userid!=''){

	    $.ajax({
	      url: sc_baseurl+"sccart/update_cart",
	      type: 'POST',
	      global: false,
	      async: false,
	      data: {'uniquecookie':uniquecookie,'productid':productid,'userid':userid,'productsize':productSize,'lookid':lookid},
	      cache :true,
	      success: function(response) {
				 // Updated on 24-Dec-2015 by Arun/Sudha: User Product Click Track (Using Table: users_product_click_track) 
				if(frmsource!=''){  //Updated on 24-Dec-2015 by Saylee: User Product Click Track (Using Table: users_product_click_track) 
					// _targetProductClick(productid,frmsource);
				}
	      },
	      error: function(xhr) {

	      }
	    });

	}


	var shippincode = getCookie('stylecracker_shipping_pincode');
	var paymode = '';
	var uc_coupon = getCookie('SCCouponCode');

	/*$.ajax({
	      url: sc_baseurl+"stylecart/get_cartQty",
	      type: 'POST',
	      global: false,
	      async: false,
	      data: {'uniquecookie':uniquecookie,'uc_pincode': shippincode,'paymode':paymode,'SCCouponcookie':uc_coupon,'country':$('#country').val() },
	      cache :true,
	      success: function(response) {	      		
	      		
	      			if(response==0)
	      			{
	      				$('#total_products_cart_count').addClass('hide');
	      				$('#total_products_cart_count_mob').addClass('hide');
	      			}else
	      			{
	      				$('#total_products_cart_count').removeClass('hide');
	      				$('#total_products_cart_count_mob').removeClass('hide');
	      			}
		      		$('#total_products_cart_count').html('<span class="cart-count">'+response+'</span>');
		      		$('#total_products_cart_count_mob').html('<span class="cart-count">'+response+'</span>');	      	
		      		localStorage.setItem('newcartcount', response);
		      		      		

	      },
	      error: function(xhr) {

	      }
	    });*/
		money_formatter(".price");
      	money_formatter(".cart-price");

}

function deleteProductFromCart(cart){
	var result = confirm("Are you sure to remove the item from the cart?");
	if (result) {	
	$.ajax({
	      url: sc_baseurl+"sccart1/deleteCartProduct",
	      type: 'POST',
	      data: {'uniquecookie':getCookie('SCUniqueID'),'cart':cart},
	      cache :true,
	      async: false,
	      success: function(response) {
	      		scUpdateCart(getCookie('SCUniqueID'),'','','','','');
				//window.location.reload();
	      		//$('#cart-items-wrp').html(response);
	      },
	      error: function(xhr) {

	      }
	    });
	}
}


/*New Design Implementation*/

function add_to_cart(productId,productSize,page_form_type,product_name,product_category,product_price){
	var page_form = '';
	document.cookie="scfr="+''+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	page_form = (typeof page_form_type !== 'undefined') ?  page_form_type : page_form;
	if(page_form=='from_prodct_page'){
		$("#size span").each(function(e){
			if($(this).hasClass("active")){
				productSize = $(this).attr("attr-size");
				
			}	
		});
	}
	if(parseInt(productSize)>0){
		scUpdateCart(getCookie('SCUniqueID'),productId,'',productSize,'1','');
		if(page_form=='from_prodct_page'){
			_targetProductClick(productId,'1');
			data = 'Item added to your cart';
			$('#common-message-text').html(data);
			$("#common-modal-alert").modal('show');
			//ga('send', 'pageview', '/overlay/addtocart/?page=' + document.location.pathname + document.location.search + ' - ' +productId);
			//ga('send', 'event', 'Product', 'add_to_cart', '<brandname>-'+productId);
			/*Added Fb pixel Addtocart on 31-05-2017*/
			fbq('track', 'AddToCart', {
				content_name: product_name,
				content_category: product_category,
				content_ids: [productId],
				content_type: 'product',
				value: product_price,
				currency: 'INR'
			  }); 					
		}
	}else{
		show_alert('Please select size');
	}
	
}

 /* buy now click */
function add_to_buy_product(productId){
	//setTimeout(function(){
	$(".page-overlay").show();
	$(".fa-loader-wrp").show();
	//}, 20);
        setTimeout(function(){
	var productSize = $('#size').val();
	//ga('send', 'pageview', '/overlay/buynow/?page=' + document.location.pathname + document.location.search + ' - ' +productId);
	scUpdateCart(getCookie('SCUniqueID'),productId,'',productSize,'1','');	
	openCartPage();
	
	$(".page-overlay").hide();
	$(".fa-loader-wrp").hide();
	}, 10);
}

function show_loader(value){
  if(value){ 
    $(".page-overlay").show();
    $(".fa-loader-wrp").show();
  }else{
    $(".page-overlay").hide();
    $(".fa-loader-wrp").hide();
  }
}

function showEmailPopUp(couponData){

	$('#prompt-popup-coupemail').css('display','block');
	$('.page-dialog-overlay').css('display','block');	

	$("#sc_coupon_email").click(function(){
		  sc_check_email(couponData);
		});	
}

function sc_check_email(couponData){	
	var couponEmail = $('#couponEmail').val();	
	var uc_mobile = $('#uc_mobile').val();
	var emailres = validateEmail(couponEmail);
	
	if(couponEmail!='' && couponEmail!=null && couponData!='' && couponData!= null && emailres==true){
		
	$.ajax({
		      url: sc_baseurl+"sccart1/sc_coupon_campaign",
		      type: 'POST',
		      data: {'couponEmail': couponEmail ,'couponCode' : couponData },
		      cache :true,
		      async: false,
		      success: function(response) {
		      
		      	if(response == '1')
		      	{ 
		      		$('#coupon_email_error').html('Thank You!! Will Get Back to U. ').removeClass('hide');
		      		$('#prompt-popup-coupemail').css('display','none');
					$('.page-dialog-overlay').css('display','none');
					$('#coupon-status').html('');	
		      		/*sc_uc_order(); */
		      	}else{ 
		      		// $('#coupon_email_error').html('Thank You!! Will Get Back to U. ').removeClass('hide'); 
		      	}
		      },
		      error: function(xhr) {

		      }
			});

	}else{ $('#coupon_email_error').html('Please enter valid Email.').removeClass('hide'); }
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function get_available_offers()
{	
	var page_type = $('#page-type').val();
	
	$.ajax({
		      url: sc_baseurl+"stylecart/get_coupons",
		      type: 'POST',
		      data: {'couponEmail': '' ,'couponCode' : 'couponData', 'page_type' : page_type },
		      cache :true,
		      async: false,
		      success: function(response) { 
		      $('#modal-offers').html(response);
		      getCookie('scfr');
		      $('.scrollbar').perfectScrollbar();
		      /*	if(response == '1')
		      	{ 
		      		$('#coupon_email_error').html('Thank You!! Will Get Back to U. ').removeClass('hide');
		      		$('#prompt-popup-coupemail').css('display','none');
					$('.page-dialog-overlay').css('display','none');
					$('#coupon-status').html('');	
		      		// sc_uc_order(); 
		      	}else{ 
		      		// $('#coupon_email_error').html('Thank You!! Will Get Back to U. ').removeClass('hide'); 
		      	}*/
		      AvailPopUpBackButton();
		      },
		      error: function(xhr) {

		      }
			});
	
}

function selectOffer(elt)
{
	$('.sc-checkbox').removeClass('active');	
	$(elt).find('label').addClass('active');
	var offerid = $(elt).attr('id');	
	//var coupontype = $(elt).attr('data-coupontype-attr');
	var coupontype =1;
	if(coupontype==1)
	{
		//$("#modal-offers").modal("hide");
		var couponcode = $(elt).attr('data-couponcode-attr');
		var offerconfirm ='';
		if(getCookie('discountcoupon_stylecracker')!=''){

			if (confirm("Applying a new offer will remove the existing one") == true) {
		        offerconfirm = 1;
		    }else{
		        offerconfirm = 0;
		    }
		}else
		{
			 offerconfirm = 1;
		}

		if(offerconfirm==1)
		{
			apply_coupon_code('applyoffer');
		}
		
	}else if(coupontype==2)
	{		
		offerlistClicked(offerid);
	}
	
		/*$(".box-offers-list").hide();
	    $(".box-offers-details #details_"+offerid).show();
	    $(".modal-footer").hide();*/

}


function offerlistClicked(offerid)
{		
	    msg("offerid: "+offerid);
	    $(".box-offers-list").hide();
	    $("#details_"+offerid).show();
	    $(".modal-footer").hide();
}

function AvailPopUpBackButton(){
	$(".box-offers-details").hide();
    $(".box-offers-list").show();
    $(".modal-footer").show();
}

function place_cartscbox_order(){ 
	  show_cart_loader();
	  var billing_first_name = '';
	  var billing_last_name = '';  var error = '';
	  //var pay_mod = $("input[type='radio'][name='sc_cart_pay_mode']:checked").val();
	  //var payment_mode;
	  $(".scbox-payment-options li label").each(function(e){
	    if($(this).hasClass("active")){
	      payment_mode = $(this).attr("attr-value");
	      $('#sc_cart_pay_mode').val(payment_mode);
	       $('#cart_place_order_msg').addClass('hide');
	    }
	  });

  	
	  var pay_mod = payment_mode;
	  var codna = $('#codna').val();
	  var shina = $('#shina').val();
	  var stock_exist = $('#stock_exist').val(); 
	  var page_type = $('#page-type').val(); 

	  var regexp = /[^a-zA-Z\.]/g;
	  var regexp_mobile = /[^0-9]/g;

	 
		if(getCookie('discountcoupon_stylecracker')=='BMS15' || getCookie('discountcoupon_stylecracker')=='BMS500' || getCookie('discountcoupon_stylecracker')=='YES300' || getCookie('discountcoupon_stylecracker')=='YESBOX500' || getCookie('discountcoupon_stylecracker')=='YESBOX750' || getCookie('discountcoupon_stylecracker')=='YESBOX1000' || getCookie('scfr')=='yb'){
        		$("#cod").addClass("state-disabled");	
        	}else{
        		$("#cod").removeClass("state-disabled");	
        	}


		/*
			  var scbox_name = $('#scbox_name').val().trim();
			    scbox_name = scbox_name.replace(/[^a-z\s]/gi, ' ');
			  scbox_name = scbox_name.replace(/\s+/g, " ");
			  var res = scbox_name.split(" ");

			  billing_first_name = res[0];   
			  if(res[1]!='')
			  {
			    billing_last_name = res[1];
			  }*/
	  //var billing_first_name = $('#billing_first_name').val().trim();
	 // var billing_last_name = $('#billing_last_name').val().trim();
	  // var billing_mobile_no = $('#scbox_mobile').val().trim();
	  // var billing_pincode_no = $('#scbox_pincode').val().trim();
	  // var billing_address = $('#scbox_shipaddress').val().trim();
	  	var billing_first_name = $('#billing_first_name').val().trim();
		var billing_last_name = $('#billing_last_name').val().trim();
		var billing_mobile_no = $('#billing_mobile_no').val().trim();
		var billing_pincode_no = $('#billing_pincode_no').val().trim();
		var billing_address = $('#billing_address').val().trim();
		var billing_city = $('#billing_city').val().trim();
		var billing_state = $('#billing_state').val();
	  billing_address = billing_address.replace(/[^a-zA-Z0-9\\-\\s\\b\/\\,\(\)]/gi, ' ');
	  //billing_address = billing_address.replace(/\s+/g, " ");



	 // var billing_city = $('#scbox_city').val().trim();
	 // var billing_state = $('#scbox_state').val();
	  var cart_total = $('#cart_total').val();
	  var scbox_price = $('#scbox_price').val();

	  document.cookie = "scbox_cartotal="+cart_total+";path=/";
	  var isTrue = 0;

	  if(billing_first_name.match(regexp) || billing_first_name==''){
	    error = 'Enter Valid First Name';
	    $('#cart_place_order_msg').html('Enter Valid First Name');
	    isTrue = 1;
	  }else{
	    document.cookie = "billing_first_name="+billing_first_name+";path=/";
	    $('#cart_place_order_msg').html('');
	    //isTrue = 0;
	  }

	  if(billing_last_name.match(regexp) || billing_last_name==''){
	    error = 'Enter Valid Last Name';
	    $('#cart_place_order_msg').html('Enter Valid Last Name');
	    isTrue = 1;
	  }else{
	    document.cookie = "billing_last_name="+billing_last_name+";path=/";
	    $('#cart_place_order_msg').html('');
	    //isTrue = 0;
	  }

	  if(billing_mobile_no.match(regexp_mobile) || billing_mobile_no=='' || billing_mobile_no.length < 10){
	    error = 'Enter Valid Mobile No';
	    $('#cart_place_order_msg').html('Enter Valid Mobile No');
	    isTrue = 1;
	  }else{
	    document.cookie = "billing_mobile_no="+billing_mobile_no+";path=/";
	    $('#cart_place_order_msg').html('');
	    //isTrue = 0;
	  }

	  if(billing_pincode_no.match(regexp_mobile) || billing_pincode_no=='' || billing_pincode_no.length < 6){
	    error = 'Enter Valid Pincode';
	    $('#cart_place_order_msg').html('Enter Valid Pincode');
	    isTrue = 1;
	  }else{
	    document.cookie = "stylecracker_shipping_pincode="+billing_pincode_no+";path=/";
	    $('#cart_place_order_msg').html('');
	    //isTrue = 0;
	  }

	  /*if(billing_address==''){
	    error = 'Enter Shipping Address';
	    $('#cart_place_order_msg').html('Enter Shipping Address');
	    isTrue = 1;
	  }else{

	    document.cookie = "billing_address="+billing_address+";path=/";
	    $('#cart_place_order_msg').html('');
	    //isTrue = 0;
	  }*/

	  if(billing_city==''){
	    error = 'Enter Valid Pincode';
	    $('#cart_place_order_msg').html('Enter Shipping City');
	    isTrue = 1;
	  }else{
	    document.cookie = "billing_city="+billing_city+";path=/";
	    $('#cart_place_order_msg').html('');
	    //isTrue = 0;
	  }

	  if(billing_state==''){
	    error = 'Enter Shipping State';
	    $('#cart_place_order_msg').html('Enter Shipping State');
	    isTrue = 1;
	  }else{
	    document.cookie = "billing_state="+billing_state+";path=/";
	    $('#cart_place_order_msg').html('');
	    //isTrue = 0;
	  }

	  if(isTrue == 1){ 
	    hide_cart_loader();
	    $('#cart_place_order_msg').html('Please enter shipping address').removeClass('hide');
	    $('#cart_place_order_msg').addClass('message error');
	    $("#place_order_sc_cart").attr('disabled',true);  
	  }
	  else if(stock_exist > 0){ 
	    hide_cart_loader();
	    $('#cart_place_order_msg').html('Some of the products are out of stock').removeClass('hide');
	    $("#place_order_sc_cart").attr('disabled',true);  
	  }
	  else if(pay_mod !='cod' && pay_mod != 'card'){
	    hide_cart_loader();
	    $('#cart_place_order_msg').html('Select payment option').removeClass('hide');
	    $("#place_order_sc_cart").attr('disabled',true);  
	  }else if(pay_mod =='cod' && codna>0){ 
	    hide_cart_loader();
	    /*$("input[type='radio'][id='sc_cart_pay_mode_cod']").prop('checked',false);
	    $("input[type='radio'][id='sc_cart_pay_mode_online']").prop('checked',true);*/
	    
	    if(getCookie('discountcoupon_stylecracker') == 'NOCASH500' || getCookie('discountcoupon_stylecracker') == 'NOCASH1000'){ 
	      
	      $("#place_order_sc_cart").attr('disabled',true);      
	      
	      $('#cart_place_order_msg').html('COD is not avaliable for applied coupon code').removeClass('hide');
	      
	      }else{
	      $('#cart_place_order_msg').html('COD is not available for some products').removeClass('hide');
	    }
	  }else if(shina > 0){
	    hide_cart_loader();
	    $('#cart_place_order_msg').html('Shipping is not available for some products').removeClass('hide');
	    $("#place_order_sc_cart").attr('disabled',true);  
	  }else{  

	    var shipping_first_name = billing_first_name;
	    var shipping_last_name = billing_last_name;
	    var shipping_mobile_no = billing_mobile_no;
	    var shipping_pincode_no = billing_pincode_no;
	    var shipping_address = billing_address;
	    var shipping_city = billing_city;
	    var shipping_state = billing_state;
	    var cart_email = $('#cart_email').val().trim(); 
	    var cart_total = $('#cart_total').val();
	    var scbox_objectid = $('#scbox_objectid').val();
	    var scbox_objectid = lastparam.split('?')[0];
	    var cart_total = $('#cart_total').val();
	    var scbox_price = getCookie('Scbox_price');
	    var scbox_objectid = getCookie('scbox_objectid');
	    var scbox_quantity = $('#scbox_quantity').val();
	    	    
	   // msg(is_order_placed+'==billing_first_name=='+billing_first_name+'==billing_last_name=='+billing_last_name+'==billing_mobile_no== '+billing_mobile_no+'==billing_pincode_no=='+billing_pincode_no+'==billing_address=='+billing_address+'==billing_city=='+billing_city+'==billing_state=='+billing_state+'==shipping_first_name=='+shipping_first_name+'==shipping_last_name=='+shipping_last_name+'==shipping_mobile_no=='+shipping_mobile_no+'==shipping_pincode_no=='+shipping_pincode_no+'==shipping_address=='+shipping_address+'==shipping_city=='+shipping_city+'==shipping_state=='+shipping_state+'==cart_email=='+cart_email+'==scbox_price=='+scbox_price);

	    if(is_order_placed==false &&billing_first_name!='' && billing_last_name!='' && billing_mobile_no!='' && billing_pincode_no!='' && billing_address!='' && billing_city!='' && billing_state!='' && shipping_first_name!='' && shipping_last_name!='' && shipping_mobile_no!='' && shipping_pincode_no!='' && shipping_address!='' && shipping_city!='' && shipping_state!='' && cart_email!='' && scbox_price!=''){

 		  ga('send', 'event', 'SCBOX','clicked','Step4a-Order Place Request');
	      msg("before: is_order_placed: "+is_order_placed);
	      is_order_placed = true;
	      if(pay_mod == 'cod'){   
	      	 $("#page-loader").show();

	        $.ajax({
	                type: "post",
	                url: sc_baseurl+"scbox/place_order",
	                data: { billing_first_name:billing_first_name,billing_last_name:billing_last_name,billing_mobile_no:billing_mobile_no,billing_pincode_no:billing_pincode_no,billing_address:billing_address,billing_city:billing_city,billing_state:billing_state,shipping_first_name:shipping_first_name,shipping_last_name:shipping_last_name,shipping_mobile_no:shipping_mobile_no,shipping_pincode_no:shipping_pincode_no,shipping_address:shipping_address,shipping_city:shipping_city,shipping_state:shipping_state,cart_email:cart_email,pay_mod:pay_mod,cart_total:cart_total,scbox_price:scbox_price,scbox_objectid:scbox_objectid,scbox_quantity:scbox_quantity,page_type:page_type},
	                cache :true,
	                async: true,
	                success: function (response) {
	                	// $("#page-loader").hide();  
	                    hide_cart_loader(); remove_address_cookies('SHIP'); remove_address_cookies('BILL');
	                    if(response!='')
	                    {	
	                    	var final_cookies = '';
	                    	document.cookie="billing_address="+final_cookies+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";                    	
	                      var redirction = sc_baseurl+'book-scbox/'+scbox_objectid+'?order='+response+'&step=6';
	                     
	                      // $('#order-message').html('Your SCBOX amount of Rs.'+cart_total+' has been booked. Please fill the below details to know more about you. (COD)');                      
	                       $('#order-message').removeClass('hide');
	                    }else
	                    {
	                      var redirction = sc_baseurl+'book-scbox/'+scbox_objectid+'?step=1';
	                      // $('#order-message').html('Error');
	                      // $('#order-message').removeClass('hide');
	                    }
	                   // var redirction = sc_baseurl+'scbox/ordersuccessOrder/'+response;
	                    
	                    // var final_cookies = '';
	                    // document.cookie="SCUniqueID="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	                    // document.cookie="discountcoupon_stylecracker="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	                window.location =redirction;
	                money_formatter(".price");
	                money_formatter(".cart-price");
	                },
	                error: function (response) {
	                  hide_cart_loader();	                   
	                    msg('error=='+response);
	                }
	            });

	      }else{	          
	     
	      	 $("#page-loader").show(); 
	        $.ajax({
	                type: "post",
	                url: sc_baseurl+"scbox/place_online_order",
	                data: { billing_first_name:billing_first_name,billing_last_name:billing_last_name,billing_mobile_no:billing_mobile_no,billing_pincode_no:billing_pincode_no,billing_address:billing_address,billing_city:billing_city,billing_state:billing_state,shipping_first_name:shipping_first_name,shipping_last_name:shipping_last_name,shipping_mobile_no:shipping_mobile_no,shipping_pincode_no:shipping_pincode_no,shipping_address:shipping_address,shipping_city:shipping_city,shipping_state:shipping_state,cart_email:cart_email,pay_mod:pay_mod,cart_total:cart_total,scbox_price:scbox_price,scbox_objectid:scbox_objectid,scbox_quantity:scbox_quantity, page_type:page_type  },
	                cache :true,
	                async: true,
	                success: function (response) { 
	                	 //$("#page-loader").hide();  
	                    //hide_cart_loader();
	                   
	                    var obj = jQuery.parseJSON(response);
	                   
	                $('#txnid').val(obj.txnid);
	                $('#hash').val(obj.payu_hash);
	                $('#amount').val(obj.amount);
	                $('#firstname').val(obj.firstname);
	                $('#email').val(obj.email);
	                $('#phone').val(obj.phoneno);
	                $('#udf1').val(getCookie('SCUniqueID'));
	                $('#udf2').val(obj.address_sc);
	                $('#udf3').val(billing_pincode_no);
	                $('#udf4').val(billing_city);
	                $('#udf5').val(obj.state);
	                $('#offer_key').val(obj.offerkey);
	                var stylecracker_style = document.forms.stylecracker_style;
	                stylecracker_style.submit();
	                money_formatter(".price");
	                money_formatter(".cart-price");
	                document.cookie="scfr="+''+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	                },
	                error: function (response) {
	                  hide_cart_loader();                  
	                    msg('error=='+response);
	                }
	            });
	      }
	    }
	  }
	  hide_cart_loader();
  
}



