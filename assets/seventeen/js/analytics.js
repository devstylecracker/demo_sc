//For analytics 

//added for look click track

function _targetClick(look_id,type){
  
  //var gac = new gaCookies();
  //var user_id =  gac.getUniqueId();
  var user_id = 0;
  var getUrl = window.location;
  var baseUrl = getUrl .protocol + "//" + getUrl.host ;

    $.ajax({
      url: baseUrl+"/analytics/add_look_click",
      type: 'POST',
      data: {'look_id':look_id,'type':type,'user_id':user_id},
      cache :true,
      success: function(response) {
        console.log(response);

      },
      error: function(xhr) {

      }
    });

}

//added for product buy now click track
function _targetProductClick(product_id,frmsource,product_name){
  //var gac = new gaCookies();
  //var user_id =  gac.getUniqueId();
  var SCUniqueID = getCookie('SCUniqueID');
  var user_id = 0;
  var getUrl = window.location;
  ga('send', 'event', 'Product', 'clicked', product_name);
  var baseUrl = getUrl .protocol + "//" + getUrl.host ;
    $.ajax({
      url: baseUrl+"/analytics/add_product_click",
      type: 'POST',
      data: {'product_id':product_id,'user_id':user_id,'frmsource':frmsource,'SCUniqueID':SCUniqueID},
      cache :true,
      success: function(response) {
      },
      error: function(xhr) {

      }
    });

}

//added for brand click track
function _targetBrandClick(brand_id){
  //var gac = new gaCookies();
  //var user_id =  gac.getUniqueId();
  var user_id = 0;
  var getUrl = window.location;
   var baseUrl = getUrl .protocol + "//" + getUrl.host ;

    $.ajax({
      url: baseUrl+"/analytics/add_brand_click",
      type: 'POST',
      data: {'brand_id':brand_id,'user_id':user_id},
      cache :true,
      success: function(response) {
        
      },
      error: function(xhr) {

      }
    });

}

//added for facebook share
function _targetFacebookShare(lname,llink,lpic,look_id,ldesc){
  //var gac = new gaCookies();
  //var user_id =  gac.getUniqueId();
  var user_id = 0;
  var getUrl = window.location;
   var baseUrl = getUrl .protocol + "//" + getUrl.host ;
  //console.log(lpic);
  FB.ui(
        {
          method: 'feed',
          name: lname,
          link: llink,
          picture: lpic,
          //caption: 'This is the content of the "caption" field.',
          //description: "Get yourself a Personal Stylist on Stylecracker.com. Our experts will create custom-made fashionable looks for you, keeping your personal preferences and budget in mind. What's more? With our 'Buy Now' feature, you can instantly shop the recommendations we make!",
          description: ldesc,
          //message: ""
          },
          function(response) {
            if (response && response.post_id) {
              $.ajax({
                url: baseUrl+"/analytics/social_media_share",
                type: 'post',
                data: {'look_id':look_id,'source':'facebook'},
                success: function(response) {
                
                },
                  error: function(xhr, desc, err) {
                    console.log(err);
                  }
              });
            } else {
              //alert('Post was not published.');
            }
          }
        );
}

//added for social media share except facebook
function _targetSocialMedia(look_id,source){
  //var gac = new gaCookies();
  //var user_id =  gac.getUniqueId();
  var user_id = 0;
  var getUrl = window.location;
   var baseUrl = getUrl .protocol + "//" + getUrl.host ;
  $.ajax({
    url: baseUrl+"/analytics/social_media_share",
    type: 'post',
    data: {'look_id':look_id,'source':source},
    success: function(response) {
      
    },
      error: function(xhr, desc, err) {
        console.log(err);
      }
  });
}

  function writeAddressName(latLng) {
  //console.log(baseUrl);
    //var gac = new gaCookies();
    //var user_id =  gac.getUniqueId();
    var user_id = 0;
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({
      "location": latLng
      },
      function(results, status) {
      if (status == google.maps.GeocoderStatus.OK){
      // add ajax here to submit the location to database
        var user_id = 0;
        var getUrl = window.location;
        var baseUrl = getUrl .protocol + "//" + getUrl.host;

         var geoLoc = results[0].formatted_address;
          $.ajax({
          url: baseUrl+"/analytics/add_geo_loc",
          type: 'post',
          data: {'type' : 'add_geo_location','user_id':user_id,'geo':geoLoc},
          success: function(data, status) { 
            //msg(data);
          },
          error: function(xhr, desc, err) {
          console.log(err);
          }
             });
        }
      /*else
        document.getElementById("error").innerHTML += "Unable to retrieve your address" + "<br />";*/
      });
  } 
  function geolocationSuccess(position) {
    var userLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    // Write the formatted address
    writeAddressName(userLatLng);         
    //mapObject.fitBounds(circle.getBounds());
  } 
  function geolocationError(positionError) {
    /*document.getElementById("error").innerHTML += "Error: " + positionError.message + "<br />";*/
  } 
  function geolocateUser() {
    // If the browser supports the Geolocation API
    if (navigator.geolocation)
    {
    var positionOptions = {
    enableHighAccuracy: true,
    timeout: 30 * 1000 // 10 seconds
    };
    navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError, positionOptions);
    }
    //else
    //document.getElementById("error").innerHTML += "Your browser doesn't support the Geolocation API";
  }
  
//added for Collection/event/blog click track
function _targetClickTrack(object_id,frmsource,object_name,objet_type){
  
  var SCUniqueID = getCookie('SCUniqueID');
  var user_id = 0;
  var getUrl = window.location;
  if(objet_type == 'collection'){
	  ga('send', 'event', 'Collection', 'clicked', object_name);
  }else if(objet_type == 'blog'){
	  ga('send', 'event', 'Blog', 'clicked', object_name);
  }else if(objet_type == 'event'){
	  ga('send', 'event', 'Event', 'clicked', object_name);
  }
  
  var baseUrl = getUrl .protocol + "//" + getUrl.host ;
    $.ajax({
      url: baseUrl+"/analytics/add_collection_click",
      type: 'POST',
      data: {'object_id':object_id,'user_id':user_id,'frmsource':frmsource,'SCUniqueID':SCUniqueID},
      cache :true,
      success: function(response) {
      },
      error: function(xhr) {

      }
    });

}
  
