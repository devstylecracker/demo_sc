var isTriggered = false;
var intTriggerScroll = 500;
$(window).scroll(function(){
	
	if ($('#get_all_collections').length){
		get_more_collections();
	}
	
	if ($('#get_all_events').length){
		get_more_events();
	}
	
	if ($('#get_all_blogs').length){
		get_more_blogs();
	}
	
	if ($('#get_all_products').length){
		get_more_products();
	}
	
	if ($('#get_event_products').length){
		get_more_products_event();
	}
	
	if ($('#get_all_wishlist_products').length){
		get_more_wishlist_products();
	}
	
	if ($('#get_all_wishlist_collections').length){
		get_more_wishlist_collections();
	}
	
	if ($('#get_all_wishlist_events').length){
		get_more_wishlist_events();
	}
	
	if ($('#get_all_wishlist_blogs').length){
		get_more_wishlist_blogs();
	}
	
	if ($('#get_all_brand_products').length){
		get_more_brand_products();
	}
	
	if ($('#get_all_brands').length){
		get_more_brands();
	}
	
});

function get_more_collections(){
	
	var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());
	if(intCurrentScrollPos<intTriggerScroll && !isTriggered){
		isTriggered=true;
		var offset = $('#offset').val();
		var total_collection = $('#total_collection').val();
		if(parseInt(offset)*parseInt(12)<total_collection){
			$("#load-more").show();
			$.ajax({
				url: sc_baseurl+'Collection_web/get_more_collection',
				type: 'post',
				data: { 'offset' : offset },
				success: function(data, status) {
				  
					$('#get_all_collections').append(data);
					isTriggered=false;
					var new_offset = parseInt(offset)+parseInt(1);
					$('#offset').val(new_offset);
					$('.fa-loader-wrp-products').hide();
					$("#load-more").hide();


				}
			});
		}
	}else{
		// alert('fsdv');
	}
}

function get_more_events(){
	
	var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());
	if(intCurrentScrollPos<intTriggerScroll && !isTriggered){
		isTriggered=true;
		var offset = $('#offset').val();
		var total_events = $('#total_events').val();
		if(parseInt(offset)*parseInt(12)<total_events){
			$("#load-more").show();
			$.ajax({
				url: sc_baseurl+'event_web/get_more_events',
				type: 'post',
				data: { 'offset' : offset },
				success: function(data, status) {
				  
					$('#get_all_events').append(data);
					isTriggered=false;
					var new_offset = parseInt(offset)+parseInt(1);
					$('#offset').val(new_offset);
					$('.fa-loader-wrp-products').hide();
					$("#load-more").hide();

				}
			});
		}
	}else{
		// alert('fsdv');
	}
}

function get_more_blogs(){
	
	var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());
	if(intCurrentScrollPos<intTriggerScroll && !isTriggered){
		isTriggered=true;
		var offset = $('#offset').val();
		var total_blogs = $('#total_blogs').val();
		if(parseInt(offset)*parseInt(12)<total_blogs){
			$("#load-more").show();
			$.ajax({
				url: sc_baseurl+'blog_web/get_more_blogs',
				type: 'post',
				data: { 'offset' : offset },
				success: function(data, status) {
					
					$('#get_all_blogs').append(data);
					isTriggered=false;
					var new_offset = parseInt(offset)+parseInt(1);
					$('#offset').val(new_offset);
					$("#load-more").hide();

				}
			});
		}
	}else{
		// alert('fsdv');
	}
	
}

function get_more_products(){
	
	var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());
	if(intCurrentScrollPos<intTriggerScroll && !isTriggered){
		isTriggered=true;
		var offset = $('#offset').val();
		var object_id = $('#object_id').val();
		var total_products = $('#total_products').val();
		
		gender_cat_id = $('#gender_cat_id').val();
		arrCategorySelected = $('#arrCategorySelected').val();
		arrBrandSelected = $('#arrBrandSelected').val();
		arrSizeSelected = $('#arrSizeSelected').val();
		arrCatAttrSelected = $('#arrCatAttrSelected').val();
		intPriceSelected = $('#intPriceSelected').val();
		intSortBy = $('#intSortBy').val();
		
		if(parseInt(offset)*parseInt(20)<total_products){
			$("#load-more").show();
			$.ajax({
				url: sc_baseurl+'Collection_web/get_more_products',
				type: 'post',
				data: { 'object_id' : object_id ,'lastlevelcat' : arrCategorySelected,'brandId' : arrBrandSelected,'sizeId' : arrSizeSelected,'attributesId' : arrCatAttrSelected,'priceId' : intPriceSelected,'sort_by' : intSortBy,'offset' : offset,'gender_cat_id' : gender_cat_id},
				success: function(data, status) {
					$('#get_all_products').append(data);
					isTriggered=false;
					var new_offset = parseInt(offset)+parseInt(1);
					$('#offset').val(new_offset);
					$('.fa-loader-wrp-products').hide();
					$("#load-more").hide();
				}
			});
		}
	}else{
		// alert('fsdv');
	}
	
}

function get_more_products_event(){
	// alert('dsvfdv');
	var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());
	if(intCurrentScrollPos<intTriggerScroll && !isTriggered){
		isTriggered=true;
		var offset = $('#offset').val();
		var object_id = $('#object_id').val();
		var total_products = $('#total_products').val();
		if(parseInt(offset)*parseInt(12)<total_products){
			$("#load-more").show();
			$.ajax({
				url: sc_baseurl+'Event_web/get_more_products_event',
				type: 'post',
				data: { 'offset' : offset,'object_id' : object_id},
				success: function(data, status) {
					$('#get_event_products').append(data);
					isTriggered=false;
					var new_offset = parseInt(offset)+parseInt(1);
					$('#offset').val(new_offset);
					$('.fa-loader-wrp-products').hide();
					$("#load-more").hide();

				}
			});
		}
	}else{
		// alert('fsdv');
	}
	
}

function add_to_fav(fav_for, fav_id, user_id, fav_name){
	if(user_id >0){
		var data = {fav_for : fav_for, fav_id : fav_id, user_id : user_id};
	var url = sc_baseurl+'schome_new/fav_add';

		if(fav_for == 'collection'){
			ga('send', 'event', 'Collection', 'Bookmark', fav_name, user_id);
			if($('#wishlist_c'+fav_id).hasClass('active')){
				
				$('#wishlist_c'+fav_id).removeClass('active');
			}else{
				$('#wishlist_c'+fav_id).addClass('active');
			}
			
		}else if(fav_for == 'event'){
			ga('send', 'event', 'event', 'Bookmark', fav_name, user_id);
			if($('#wishlist_e'+fav_id).hasClass('active')){
				
				$('#wishlist_e'+fav_id).removeClass('active');
			}else{
				$('#wishlist_e'+fav_id).addClass('active');
			}
			
		}else if(fav_for == 'product'){
			ga('send', 'event', 'product', 'Bookmark', fav_name, user_id);
			if($('#wishlist_product'+fav_id).hasClass('active')){
				//alert('remove'+fav_id);
				$('#wishlist_product'+fav_id).removeClass('active');
				console.log($('#wishlist_product'+fav_id));
				console.log(fav_id);
			}else{
				//alert('add'+fav_id);
				$('#wishlist_product'+fav_id).addClass('active');
			}
			
		}else {
			ga('send', 'event', 'blog', 'Bookmark', fav_name, user_id);
			if($('#wishlist_blog'+fav_id).hasClass('active')){
				
				$('#wishlist_blog'+fav_id).removeClass('active');
			}else{
				$('#wishlist_blog'+fav_id).addClass('active');
			}
			
		} 
			
		$.ajax({
		  type: "POST",
		  url: url,
		  data: data,
		  dataType: "json",
		  success: function (response) {
			  //your success code

			if(response.msg == "Successful Added"){

				if(fav_for == 'collection'){
					$('#wishlist_c'+fav_id).addClass('active');
				}else if(fav_for == 'event'){
					$('#wishlist_e'+fav_id).addClass('active');
				}else if(fav_for == 'product'){
					$('#wishlist_product'+fav_id).addClass('active');
				}else {
					$('#wishlist_blog'+fav_id).addClass('active');
				} 
				console.log(response);
			}else if(response.msg == "Successful deleted"){
				  
				if(fav_for == 'collection'){
					$('#wishlist_c'+fav_id).removeClass('active');
				}else if(fav_for == 'event') {
					$('#wishlist_e'+fav_id).removeClass('active');
				}else if(fav_for == 'product') {
					$('#wishlist_product'+fav_id).removeClass('active');
				}else {
					$('#wishlist_blog'+fav_id).removeClass('active');
				}  
				
				console.log(response);
			}
			  console.log(response);



		  },
		  error: function (response) {
			  //your error code
			  console.log(response);
		  }
		});
	}else{
		//if user is not logged in 
		var msg = 'Please login to bookmark';
		show_alert(msg);
	}
	
}


function send_message(obj_type,obj_id,obj_name,obj_img,obj_url,user_id){
	
	if(obj_type == 'product'){
		ga('send','event','Product','ShareWithStylist', obj_name);
	}else if(obj_type == 'event'){
		ga('send','event','event','ShareWithStylist', obj_name);
	}
	
	if(user_id > 0){
		$("#page-loader").show();
		$.ajax({
			url: sc_baseurl+'message/send_message',
			type: 'post',
			data: { 'obj_type' : obj_type,'obj_id' : obj_id,'obj_name' : obj_name,'obj_img' : obj_img,'obj_url' : obj_url },
			success: function(data, status) {
				// alert(data);
				Intercom('show');
				$("#page-loader").hide();
			}
		});
	
	}else{
		message = 'Please login to share with stylist.'
		show_alert(message);
	}
	
}



function add_size_data(offset,object_id){
	$.ajax({
		url: sc_baseurl+'Collection_web/add_size_data',
		type: 'post',
		data: { 'offset' : offset,'object_id' : object_id},
		success: function(data, status) {
			$('#modal-select-size-collection').append(data);
		}
	});
}

function get_more_wishlist_products(){
	
	var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());
	if(intCurrentScrollPos<intTriggerScroll && !isTriggered){
		isTriggered=true;
		var offset = $('#offset').val();
		var total_products = $('#total_products').val();
		if(parseInt(offset)*parseInt(12)<total_products){
			$("#load-more").show();
			$.ajax({
				url: sc_baseurl+'bookmarks/get_more_bookmark',
				type: 'post',
				data: { 'offset' : offset,'fav_for' : 'product' },
				success: function(data, status) {
				  
					$('#get_all_wishlist_products').append(data);
					isTriggered=false;
					var new_offset = parseInt(offset)+parseInt(1);
					$('#offset').val(new_offset);
					$('.fa-loader-wrp-products').hide();
					$("#load-more").hide();


				}
			});
		}
	}else{
		// alert('fsdv');
	}
}

function get_more_wishlist_collections(){
	
	var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());
	if(intCurrentScrollPos<intTriggerScroll && !isTriggered){
		isTriggered=true;
		var offset = $('#offset').val();
		var total_collection = $('#total_collection').val();
		if(parseInt(offset)*parseInt(12)<total_collection){
			$("#load-more").show();
			$.ajax({
				url: sc_baseurl+'bookmarks/get_more_bookmark',
				type: 'post',
				data: { 'offset' : offset,'fav_for' : 'collection' },
				success: function(data, status) {
				  
					$('#get_all_wishlist_collections').append(data);
					isTriggered=false;
					var new_offset = parseInt(offset)+parseInt(1);
					$('#offset').val(new_offset);
					$('.fa-loader-wrp-products').hide();
					$("#load-more").hide();


				}
			});
		}
	}else{
		// alert('fsdv');
	}
}

function get_more_wishlist_events(){
	
	var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());
	if(intCurrentScrollPos<intTriggerScroll && !isTriggered){
		isTriggered=true;
		var offset = $('#offset').val();
		var total_events = $('#total_events').val();
		if(parseInt(offset)*parseInt(12)<total_events){
			$("#load-more").show();
			$.ajax({
				url: sc_baseurl+'bookmarks/get_more_bookmark',
				type: 'post',
				data: { 'offset' : offset,'fav_for' : 'event' },
				success: function(data, status) {
				  
					$('#get_all_wishlist_events').append(data);
					isTriggered=false;
					var new_offset = parseInt(offset)+parseInt(1);
					$('#offset').val(new_offset);
					$('.fa-loader-wrp-products').hide();
					$("#load-more").hide();


				}
			});
		}
	}else{
		// alert('fsdv');
	}
}

function get_more_wishlist_blogs(){
	
	var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());
	if(intCurrentScrollPos<intTriggerScroll && !isTriggered){
		isTriggered=true;
		var offset = $('#offset').val();
		var total_blogs = $('#total_blogs').val();
		if(parseInt(offset)*parseInt(12)<total_blogs){
			$("#load-more").show();
			$.ajax({
				url: sc_baseurl+'bookmarks/get_more_bookmark',
				type: 'post',
				data: { 'offset' : offset,'fav_for' : 'blog' },
				success: function(data, status) {
				  
					$('#get_all_wishlist_blogs').append(data);
					isTriggered=false;
					var new_offset = parseInt(offset)+parseInt(1);
					$('#offset').val(new_offset);
					$('.fa-loader-wrp-products').hide();
					$("#load-more").hide();


				}
			});
		}
	}else{
		// alert('fsdv');
	}
}

function get_more_brand_products(){
	var intTriggerScroll = 800;
	var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());
	if(intCurrentScrollPos<intTriggerScroll && !isTriggered){
		isTriggered=true;
		var offset = $('#offset').val();
		var brand_id = $('#brand_id').val();
		var total_products = $('#total_products').val();
		if(parseInt(offset)*parseInt(20)<total_products){
			$("#load-more").show();
			$.ajax({
				url: sc_baseurl+'brands_web/more_brand_products',
				type: 'post',
				data: { 'offset' : offset,'brand_id' : brand_id },
				success: function(data, status) {
				  
					$('#get_all_brand_products').append(data);
					isTriggered=false;
					var new_offset = parseInt(offset)+parseInt(1);
					$('#offset').val(new_offset);
					$("#load-more").hide();
				}
			});
		}
	}else{
		// alert('fsdv');
	}
}

function notification_read(notification_id){
	
	if(!$('#notification'+notification_id).hasClass('active')){
		 // alert('notification'+notification_id);
		$('#notification'+notification_id).addClass('active');
		
		$.ajax({
				url: sc_baseurl+'schome_new/update_notification',
				type: 'post',
				data: { 'notification_id' : notification_id },
				success: function(data, status) {
					
				}
		});
	}
	
}

function show_alert(message){
	$('#common-message-text').html(message);
	$("#common-modal-alert").modal('show');
}

function get_more_brands(){
	
	var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());
	if(intCurrentScrollPos<intTriggerScroll && !isTriggered){
		isTriggered=true;
		var offset = $('#offset').val();
		var total_brands = $('#total_brands').val();
		if(parseInt(offset)*parseInt(20)<total_brands){
			$("#load-more").show();
			$.ajax({
				url: sc_baseurl+'Brands_web/get_more_brands',
				type: 'post',
				data: { 'offset' : offset },
				success: function(data, status) {
				  
					$('#get_all_brands').append(data);
					isTriggered=false;
					var new_offset = parseInt(offset)+parseInt(1);
					$('#offset').val(new_offset);
					$('.fa-loader-wrp-products').hide();
					$("#load-more").hide();


				}
			});
		}
	}else{
		// alert('fsdv');
	}
}