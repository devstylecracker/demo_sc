//call zoomin
// zoominDestroy();
//  zoominState();
// /. call zoomin

//sm only slider
$('.tablet .grid-items-slider.sm-slider, .mobile .grid-items-slider.sm-slider').addClass('swiper-container');
$('.tablet .grid-items-slider.sm-slider .grid-item, .mobile .grid-items-slider.sm-slider .grid-item').addClass('swiper-slide');
$('.tablet .grid-items-slider.sm-slider .grid-row, .mobile .grid-items-slider.sm-slider .grid-row').addClass('swiper-wrapper');
$('.tablet .grid-items-slider.sm-slider .grid-col, .mobile .grid-items-slider.sm-slider .grid-col').addClass('swiper-slide');

smSwiper = new Swiper('.swiper-container.sm-slider', {
         paginationClickable: true,
         spaceBetween: 0,
         cssWidthAndHeight: true,
         slidesPerView:'auto',

         pagination: '.swiper-pagination',
         nextButton: '.swiper-button-next',
         prevButton: '.swiper-button-prev',

});

///. sm only slider

//slick
 $('.slider-top.product').slick({
 slidesToShow: 1,
 slidesToScroll: 1,
 arrows: false,
 fade: false,
  speed: 300,
 //useCSS1:true,
 //variableWidth:true,
//  centerMode: true,
 focusOnSelect: false,
 asNavFor: '.slider-thumbs.product'
});

$('.slider-thumbs.product').slick({
 slidesToShow: 'auto',
 slidesToScroll: 1,
 useCSS:true,
 variableWidth: true,
 asNavFor: '.slider-top.product',
 //dots: true,
 centerMode: false,
 focusOnSelect: true
});

$('.slider-top.product, .slider-thumbs.product').on('afterChange', function(event){
zoominDestroy();
$('.slick-slide .img-wrp').removeClass('zoom');
$('.slick-current .img-wrp').addClass('zoom');
zoomInit();
});


///slick


//product zoom in
zoomInit();
/*function zoomInit(){
    $(".zoom img").ezPlus({
        tint: true,
        tintColour: '#F90',
        tintOpacity: 0.5,
        zoomWindowOffsetX: 10,
        scrollZoom: true,
    });

};*/

//product zoom in initialize
  //function zoominState(){
  function zoomInit(){
     var $html = $('html');

    if($('.zoom').length>0){
     // if ($('html').hasClass("mobile")) {
   if ($html.hasClass('mobile') || $html.hasClass('tablet')) {
                $('.zoom a').magnifik({
                  ratio:1,
                  });
       }
       else {
             $('.zoom a').click(function( event ) {
                  event.preventDefault();
                  event.stopImmediatePropagation();
                  return false;
              });

              $(".zoom img").ezPlus({
                //  tint: false,
                //  tintColour: '#000',
                //  tintOpacity: 0.5,
                  zoomWindowOffsetX: 10,
                  scrollZoom: false,

                  zoomWindowFadeIn: 200,
                  zoomWindowFadeOut: 200,
                  lensFadeIn: 200,
                  lensFadeOut: 200,

                  zoomWindowWidth: 364,
                  zoomWindowHeight: 497

              });
        }
    }
  }///zoomin initialize

  //zoominDestroy
    function zoominDestroy(){
      $('.zoomContainer').remove();
    //  $('.zoomWindowContainer').remove();

  //    $('.zoom img').removeData('elevateZoom');
    //  $('.zoom img').removeData('zoomImage');

    }
    ///zoominDestroy
/*
  $('.product-gallery-wrp .ic-zoom-s').click(function(event) {
   $('.zoom a').trigger('click');
  });
*/
///product zoom in
