function msg(m) { console.log(m); }
 var getUrl = window.location;

$(document).ready(function(e){

var objParam = getUrl.toString().split("/");

var seclastparam = objParam[objParam.length-2];
var lastparam = objParam[objParam.length-1];

  if(seclastparam=='book-scbox')
  {
    ChangeGender();
    var boxprice = localStorage.getItem('SCbox_price');
    $('#scbox_price').val(boxprice);
  }

//##Gender Female Click -------------------------------
 $('#scboxgender-female').click(function(e){
 	$('#gender-error').addClass('hide');
      $('#scboxgender-male').removeClass('active');
      $('#scboxgender-female').addClass('active');
      $('#scbox_gender').val('1');
      $('.box-style').removeClass('box-style-male');
      $('.box-style').addClass('box-style-female');
      $('.box-body-shape').removeClass('male');
      $('.box-body-shape').addClass('female');
      $('#titlebottom').html('Top/Dress');
      intGender = $('#scbox_gender').val();
       if(intGender!=$('scbox_sessiongender').val())
      {
        $('#scbox_style').val('');
        $('#body_shape_ans').val('');
        $('#scbox_style').attr('data-style','');
      }
      GenderWiseGetData();
    });
//##Gender Male Click -------------------------------
    $('#scboxgender-male').click(function(e){
    	$('#gender-error').addClass('hide');
		$('#scboxgender-female').removeClass('active');
		$('#scboxgender-male').addClass('active');
		$('#scbox_gender').val('2');
    $('.box-style').removeClass('box-style-female');
    $('.box-style').addClass('box-style-male');
    $('.box-body-shape').removeClass('female');
    $('.box-body-shape').addClass('male');
    $('#titlebottom').html('Shirt/T-shirt');
		intGender = $('#scbox_gender').val();
     if(intGender!=$('scbox_sessiongender').val())
      {
        $('#scbox_style').val('');
        $('#body_shape_ans').val('');
        $('#scbox_style').attr('data-style','');
      }
	 	GenderWiseGetData();
    });

     $('#scbox_age').keyup(function() {  
        if ( this.value.length === 2 || this.value.length === 5 ) 
        {
            var char = $(this).val();
            $(this).val(char+'-');            
        }
    });



  //$('#scboxgender-female').trigger("click");

  /*$("#scbox-form").validate({
     focusInvalid: false,
     ignore: [],
     rules: {
      //sc_username : { required : true ,noSpace: true },
      scbox_name : { required : true ,minlength:1},
      scbox_age : { required : true },
      scbox_gender: { required: true },
      scbox_emailid:{  required : true, email :true },
      scbox_mobile: { required : true,minlength:10,maxlength:10,number:true },
      scbox_shipaddress: { required : true,maxlength:500 },
      scbox_pincode: { required : true,minlength:6,maxlength:6,number:true },
      scbox_city: { required : true ,maxlength:50 },
      scbox_state: { required : true },
      scbox_likes: { required : true,maxlength:5000 },
      scbox_dislikes: { required : true,maxlength:5000 },
      body_shape_ans: { required : true },
      scbox_style: { required : true },
      //scbox_skintone: { required : true },
      //scbox_colors: { required : true },
      //scbox_prints: { required : true },
      scbox_sizetop: { required : true},
      scbox_sizebottom: { required : true },
      scbox_sizefoot: { required : true },
      scbox_budget: { required : true },
      scbox_sizeband: {
        required: function(element) {
          //return $('#job-earnings-salary').is(':checked')
          if($('#scbox_gender').val()==1) { return true;}else return false;

        }
      },
      scbox_sizecup: {
        required: function(element) {
          //return $('#job-earnings-salary').is(':checked')
          if($('#scbox_gender').val()==1) { return true;}else return false;
        }
      }
    },
     messages: {

      scbox_name : {  required : "Please enter firstname lastname"  },
      scbox_age : {  required : "Please select date of birth"   },
      scbox_gender : {  required : "Please select gender" },
      scbox_emailid:{ required : "Please enter email-id",email :"Please enter valid email-id"    },
      scbox_mobile:{ required : "Please enter mobile no.", minlength: "Enter valid mobile no.",maxlength: "Enter valid mobile no." },
      scbox_shipaddress:{ required : "Please enter shipping address",maxlength: "Enter valid shipping address" },
      scbox_pincode:{ required : "Please enter pincode", minlength: "Enter valid pincode" },
      scbox_city:{ required : "Please enter city", maxlength: "Enter valid city"  },
      scbox_state:{ required : "Please select state"   },
      scbox_likes:{ required : "Please enter your likes"  },
      scbox_dislikes:{ required : "Please enter your dislikes" },
      body_shape_ans:{ required : "Please select bodyshape" },
      scbox_style:{ required : "Please select style"  },
      scbox_skintone:{ required : "Please select skintone" },
      scbox_colors:{ required : "Please select colour" },
      scbox_prints:{ required : "Please select prints" },
      scbox_sizetop:{ required : "Please select the top size" },
      scbox_sizebottom:{ required : "Please select bottom size" },
      scbox_sizefoot:{ required : "Please select footwear size" },
      scbox_budget:{ required : "Please select budget" },
      scbox_sizeband:{ required : "Please select band size" },
      scbox_sizecup:{ required : "Please select cup size" },

    },
    submitHandler: function(form) {
      scbox_register();
    //##GA Event add for Signup Link on 15-Jan-16 (as per Veelas email)
   // ga('send', 'pageview', '/sc-box/?page=' + document.location.pathname + document.location.search + ' - SCbox');
    }
  });
*/
  $('.box-package').click(function(e) {

    var scboxpack = $(this).attr('data-pack');
    var scboxprice = $(this).attr('data-scbox-price');
    var scboxobjectid = $(this).attr('data-scbox-objectid');
    var scboxproductid = $(this).attr('data-scbox-productid');
    if(scboxpack!='' && scboxprice!='')
    {
      $('#customboxprice-error').addClass('hide');

      $('#scbox-package').val(scboxpack);
      $('#scbox-price').val(scboxprice);
      $('#scbox-objectid').val(scboxobjectid);
      $('#scbox-productid').val(scboxproductid);

    }else if(scboxprice == '' && $('#customboxprice').val()=='')
    {
      $('#customboxprice-error').removeClass('hide');
       scboxprice = $('#customboxprice').val();
       $('#scbox-price').val(scboxprice);

       $(e).attr('data-scbox-price',scboxprice);
      $('#scbox-package').val(scboxpack);
      $('#scbox-price').val(scboxprice);
      $('#scbox-objectid').val(scboxobjectid);
      $('#scbox-productid').val(scboxproductid);
    }

  });

// $(window).keydown(function(event){
    // if(event.keyCode == 13) {
      // event.preventDefault();
      // return false;
    // }
  // });


});

//##Gender wise Data call -------------------------------
var intGender = 0;
function GenderWiseGetData() {

	if(intGender==1){
		scbox_getdata('sizeband',intGender);
		scbox_getdata('sizecup',intGender);
		$('#lingerie').removeClass('hide');
		$('#lingerie1').removeClass('hide');
		$('#lingerie2').removeClass('hide');

	}
	else if(intGender==2){
		$('#lingerie').addClass('hide');
		$('#lingerie1').addClass('hide');
		$('#lingerie2').addClass('hide');
	}

	scbox_getdata('bodyshape',intGender);
	scbox_getdata('style',intGender);
	scbox_getdata('sizetop',intGender);
	scbox_getdata('sizebottom',intGender);
	scbox_getdata('sizefoot',intGender);
}

//##Gender ajax Data call -------------------------------
function scbox_getdata(type,gender)
{
    if(type!='' && gender!='')
    {
      $.ajax({
          url:sc_baseurl+'scbox/genderwisedata',
          type:'POST',
          data:{'type':type,'gender':gender,'userid':$('#scbox_userid').val(), 'objectid': $('#scbox_objectid').val()},
          success: function(data,status){
            if(type=='bodyshape')
            {
              $('#scbox_bodyshape').html(data);
            }else if(type=='style')
            {
             
              $('#scbox-style').html(data);
              /*For mobile PA-style slider */
               if($('html').hasClass('tablet'))
              {                
                 $('#scbox-slider .grid-col').addClass('swiper-slide');
              }else if($('html').hasClass('mobile'))
              {                
                 $('#scbox-slider .grid-col').addClass('swiper-slide');
              }
              /*For mobile PA-style slider */

            }else if(type=='sizetop')
            {
              $('#top-size').html(data);
            }else if(type=='sizebottom')
            {
              $('#bottom-size').html(data);
            }else if(type=='sizefoot')
            {
              $('#foot-size').html(data);
            }else if(type=='sizeband')
            {
              $('#band-size').html(data);
            }else if(type=='sizecup')
            {
              $('#cup-size').html(data);
            }

          },
          error:function(xhr,desc,err)
          {

          }
      });
    }
}







//## Add body shape function -------------------------------
function AddBodyShape(type,id){

	if(type == 'body_shape'){
		$('#body_shape_ans').val(id);

		$(".box-body-shape > div > div").each(function(e){
			$(this).removeClass('active');
			$(this).find(".img-wrp > img").first().removeClass("hide");
			$(this).find(".img-wrp > img").last().addClass("hide");

			if(id == Number($(this).attr('data-attr')))
			{
				$(this).addClass('active');
				$(this).find(".img-wrp > img").first().addClass("hide");
				$(this).find(".img-wrp > img").last().removeClass("hide");
			}

		});

	}
}

//## Add body preference function -------------------------------
var intStyleSelectLimitA = 0;
var isStyleSelectetElementRemoved = false;
var isNextContainerAllocated=false;
var isFirstTimeBodyStyleSelected = true;
var user_body_shape= 0;
var arr_user_style_pref = new Array();
var arr_user_color =  new Array();
var arr_user_prints = new Array();
var strColor = "";
var strPrint = "";

function AddBodyPreferences(type,element){ 
  var element =  $("#style_"+element); 
  msg("----------------------------------------------");

	if(!$(element).hasClass("active")){
		if(intStyleSelectLimitA<=2){
				$(".box-style-selected .pref").each(function(e){
					if(!$(this).hasClass("filled") && !isNextContainerAllocated) {
						nextContainer = $(this)
						isNextContainerAllocated = true;
					}
				});

				isNextContainerAllocated = false;
				intStyleSelectLimitA++;
				$(element).addClass("active");
				var img = $(element).find('img').attr("src");
				
				var intStyleID = $(element).attr("id");

				if(isFirstTimeBodyStyleSelected) {
					nextContainer = $(".box-style-selected .pref").first();

					$(nextContainer).find(".text").hide();
					$(nextContainer).find(".shape-diamond").show();
					$(nextContainer).find(".close").attr("style-id",intStyleID);
					$(nextContainer).find(".close").show();
					$(nextContainer).find("img").attr("src",img);
					$(nextContainer).addClass("filled");
					isFirstTimeBodyStyleSelected = false;
				} else {
					$(nextContainer).find(".text").hide();
					$(nextContainer).find(".shape-diamond").show();
					$(nextContainer).find(".close").attr("style-id",intStyleID);
					$(nextContainer).find(".close").show();
					$(nextContainer).find("img").attr("src",img);
					$(nextContainer).addClass("filled");
				}
			} else{
				alert("You can only select 3 preferences.");
			}
	}


	$(".box-style-selected li").each(function(e){

		arr_user_style_pref.push($(this).find(".close").attr("style-id"));
	});

	$("#scbox_style").val(arr_user_style_pref);

  $("#scbox_style").attr('data-style',arr_user_style_pref);

}

//## Remove Body Preferences --------------------
function BodyPrefClose(ele){
		var intStyleID = $(ele).attr("style-id");
		$(ele).closest("li").find("img").attr("src","");
		$(ele).closest("li").find(".text").show();
		$(ele).closest("li").find(".shape-diamond").hide();
		$(ele).closest("li").find(".close").hide();
		$(ele).closest("li").removeClass("filled");
		//
		isStyleSelectetElementRemoved = true;
		$(".body-style-pref").find(".row").find("#"+intStyleID).removeClass("active");
		intStyleSelectLimitA--;
}



//## Collect Attributes Function --------------
function AddAttributes(type,e)
{
	$(e).parent().find("span").each(function(e){
		$(this).removeClass("active");
	});
	$(e).addClass('active');
	var id = $(e).attr("id");
	$("#"+type).val(id);
}

//## Collect Colors/Texture/Prints Function --------------
function AddColorAttributes(type,container){
	//##Select Skintone
	$(container).parent().find("li").each(function(e){
		if(type=="scbox_skintone"){
			$(this).removeClass("active");
			var id = $(container).attr("bg-color");
			$("#"+type).val(id);
			$(container).addClass('active');
		}
	});

	//##Select SCBox Color
	if(type=="scbox_colors"){
			if(!$(container).hasClass('active')){
				$(container).addClass('active');
			} else {
				$(container).removeClass('active');
			}

			setTimeout(function(){
				arr_user_color = [];
				$("ul.list-colors").find("li").each(function(e){
					if($(this).hasClass("active")){
						strColor = $(this).attr("bg-color");
						arr_user_color.push(strColor);
					}
				});
				$("#"+type).val(arr_user_color);
			},500);
	}

	//##Select Prints
	if(type=="scbox_prints"){
			if(!$(container).hasClass('active')){
				$(container).addClass('active');
			} else {
				$(container).removeClass('active');
			}

			setTimeout(function(){
				arr_user_prints = [];
				$("ul.list-prints").find("li").each(function(e){
					if($(this).hasClass("active")){
						strPrint = $(this).attr("bg-color");
						arr_user_prints.push(strPrint);
					}
				});
				$("#"+type).val(arr_user_prints);
			},500);
		}

	//$(e).addClass('active');
	//var id = $(e).attr("bg-color");
	//$("#"+type).val(id);
}

//## Collect Occasion value ---------------------
function AddOccasion(type,e)
{
	$(e).closest(".list-occassion").find("li").each(function(e){
		$(this).find(".sc-checkbox").removeClass("active");
	});
	$(e).find("label").addClass("active");
	var id = $(e).attr("data-attr");

	if(id=='Custom')
	{
		$("#"+type).val(id+'-'+$('#customvalue').val());
	}else
	{
		$("#"+type).val(id);
	}

}

function addcustom()
{
	var strcustom = $('#customvalue').val();
	$("#scbox_occassion").val(strcustom);
}

function scboxregister()
{
 /* var scboxpack = $('.box-package').hasClass('active');
  */
  var objectid = $('#scbox-objectid').val();
  var package = $('#scbox-package').val();
  var price = $('#scbox-price').val();
  var productid = $('#scbox-productid').val();
  if(package == 'package4')
  {
    var customprice = $('#customboxprice').val();
    price = $('#scbox-price').val(customprice);
  }
  localStorage.setItem('SCbox_price',$('#scbox-price').val());
  //document.cookie="Scbox_price="+price+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
  //msg(getCookie('Scbox_price'));

  if(objectid!='' && package!='' && price!='' && productid!='')
  {
  		$("#scboxregistererror").removeClass('error');
  		$("#scboxregistererror").addClass('hide');
     	var stylecracker_scbox = document.forms.scboxselectform;
       	scboxselectform.submit();
       	window.location.href = sc_baseurl+'book-scbox/'+objectid;
  /*$.ajax({
      url: sc_baseurl+'schome_new/bookscbox',
      type: 'post',
      data: {'type' : 'scboxregister', 'scboxprice':price },
      success: function(data, status) {
        console.log(data);
        if(data!=''){
          if(data=='success'){
            window.location.href=sc_baseurl+'book-scbox/'+objectid;
          }else if(data == 'error'){
            window.location.href=sc_baseurl+'book-scbox/'+objectid;
          }else{
            $('#scbx_error').html("Unexpected error. Please try again!");
          }
        }else{

        }
        event.preventDefault();
      },
      error: function(xhr, desc, err) {
        // console.log(err);
      }
    });*/


  }else
  {
  	$("#scboxregistererror").addClass('error');
  	$("#scboxregistererror").removeClass('hide');
  }
}

//## Select Box Package Container --------------
function SetPackage(e)
{
	$(e).closest(".row").find(".box-package").each(function(e){
		$(this).removeClass("active");
	});

	$(e).addClass('active');
	  var scboxpack = $(e).attr('data-pack');
    var scboxprice = $(e).attr('data-scbox-price');
    var scboxobjectid = $(e).attr('data-scbox-objectid');
    var scboxproductid = $(e).attr('data-scbox-productid');
    if(scboxpack!='' && scboxprice!='')
    {
      $('#scbox-package').val(scboxpack);
      $('#scbox-price').val(scboxprice);
      $('#scbox-objectid').val(scboxobjectid);
      $('#scbox-productid').val(scboxproductid);
    }
    if(lastparam=='sc-box')
    {
    	window.location.href = sc_baseurl+'book-scbox/'+scboxobjectid;
    }

	/*var id = $(e).attr("id");
	$("#"+type).val(id);*/
}

function scbox_register()
{
  var scbox_style = document.forms.scbox-form;
  scbox-form.submit();
   $("#page-loader").hide();
  var objectid = $('#scbox_objectid').val();
  window.location.href = sc_baseurl+'book-scbox/'+objectid;
}

function addexpt(e,t)
{
	$(e).closest('.title-2').find(".sc-checkbox").each(function(e){
		$(this).removeClass("active");
	});

	$(e).addClass('active');

	if(t=='yes')
	{
		$('#experiment').val(1);
	}else
	{
		$('#experiment').val(0);
	}
}

//## EDIT FUNCTIONS =================================================
var bodyshape, bodystyle;
function ChangeGender()
{
    intGender = $('#scbox_gender').val();
    var userid = $('#scbox_userid').val();
    bodyshape = $('#body_shape_ans').val();
    bodystyle = $('#scbox_style').val();
    bodystyle_arr = bodystyle.toString().split(",");


    if(intGender!='' && intGender==1)
    {
      $('#gender-error').addClass('hide');
      $('#scboxgender-male').removeClass('active');
      $('#scboxgender-female').addClass('active');
      $('#scbox_gender').val('1');
      $('.box-style').removeClass('box-style-male');
      $('.box-style').addClass('box-style-female');

      if(userid!='')
      {
        GenderWiseGetData();
        if(intGender==$('scbox_sessiongender').val())
        {
          TriggerPAFunctions();
        }

      }
    }else if(intGender==2)
    {
      $('#gender-error').addClass('hide');
      $('#scboxgender-female').removeClass('active');
      $('#scboxgender-male').addClass('active');
      $('#scbox_gender').val('2');
      $('.box-style').removeClass('box-style-female');
      $('.box-style').addClass('box-style-male');

      if(userid!='')
      {
       GenderWiseGetData();
       if(intGender==$('scbox_sessiongender').val())
        {
          TriggerPAFunctions();
        }
      }
    }


	/*
	 if(userid!='') {
	//#Set Color set----------------
		var strColors = $("#scbox_colors").val();
		var arrColor = [];
		arrColor = strColors.split(",");

		for(var i=0;i<=arrColor.length;i++){
			$("ul.list-colors").find("li")
		}
		var i = 0;

		if()

		$("ul.list-colors").find("li").each(function(e){
			if($(this).attr("bg-color")==arrColor[i]) {
				$(this).addClass("active");
			}
			i++;
		});

	//#Set Color set----------------list-prints
		var strPrints = $("#scbox_prints").val();
		var arrPrints = [];
		arrPrints = strPrints.split(",");
		var k = 0;
		msg("arrPrints: "+arrPrints);
		msg($("ul.list-prints").find("li"));
		$("ul.list-prints").find("li").each(function(e){
			msg("---> "+$(this).attr("bg-color"));
			msg("88888-> "+arrPrints[k]);
			if($(this).attr("bg-color")==arrPrints[k]) {
				$(this).addClass("active");
			}
			k++;
		});
	}
	*/

}


//##Dynamic function trigger ------------------------
function TriggerPAFunctions(){
      setTimeout(function(){
        //#Trigger Body Shape
        $("#bodyshape_"+bodyshape).trigger("click");

      //#Trigger Body Style
      for(var i=0;i<bodystyle_arr.length; i++){
        var strDiv = bodystyle_arr[i];
        $("#style_"+strDiv).trigger("click");
      }
    },2000)
}

function slidenext(id)
{
  if(id!='' && id<6 && id>0)
  {
    if(id==1)
    {
           var final_cookies = '';  
          //document.cookie="discountcoupon_stylecracker="+final_cookies+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
          document.cookie = "discountcoupon_stylecracker=BOX50;path=/";

          if($("#scbox_gender").val()=='' || $("#scbox_name").val()=='' || $("#scbox_age").val()=='' || $("#scbox_gender").val()=='' || $("#scbox_emailid").val()=='' || $("#scbox_mobile").val()=='' || $("#scbox_state").val()=='' || $("#scbox_city").val()=='' || $("#scbox_shipaddress").val()=='' || $("#scbox_pincode").val()=='' )
          {
               if($("#scbox_gender").val()=='' )
              {
                $("#scbox_gender-error").removeClass('hide');

              } if($("#scbox_name").val()=='')
              {
                //$("#scbox_name-error").removeClass('hide');
                $("#scbox_name").addClass('invalid');

              } if($("#scbox_age").val()=='')
              {
                //$("#scbox_age-error").removeClass('hide');
                $("#scbox_age").addClass('invalid');
              } if($("#scbox_gender").val()=='')
              {
                //$("#scbox_gender-error").removeClass('hide');
                $("#scbox_gender").addClass('invalid');
                 scrolltop();
              } if($("#scbox_emailid").val()=='')
              {
                //$("#scbox_emailid-error").removeClass('hide');
                $("#scbox_emailid").addClass('invalid');
              } if($("#scbox_mobile").val()=='')
              {
                //$("#scbox_mobile-error").removeClass('hide');
                $("#scbox_mobile").addClass('invalid');
              } if($("#scbox_state").val()=='')
              {
                //$("#scbox_state-error").removeClass('hide');
                $("#scbox_state").addClass('invalid');
              } if($("#scbox_city").val()=='')
              {
                //$("#scbox_city-error").removeClass('hide');
                $("#scbox_city").addClass('invalid');
              } if($("#scbox_shipaddress").val()=='')
              {
                //$("#scbox_shipaddress-error").removeClass('hide');
                $("#scbox_shipaddress").addClass('invalid');
              } if($("#scbox_pincode").val()=='')
              {
                //$("#scbox_pincode-error").removeClass('hide');
                $("#scbox_pincode").addClass('invalid');
              } if($("#scbox_likes").val()=='')
              {
                //$("#scbox_likes-error").removeClass('hide');
                $("#scbox_likes").addClass('invalid');
              } if($("#scbox_dislikes").val()=='')
              {
                //$("#scbox_dislikes-error").removeClass('hide');
                $("#scbox_dislikes").addClass('invalid');
              }/*else
              {
                $("#scbox_gender-error").removeClass('hide');
                $("#scbox_name-error").removeClass('hide');
                $("#scbox_age-error").removeClass('hide');
                $("#scbox_emailid-error").removeClass('hide');
                $("#scbox_mobile-error").removeClass('hide');
                $("#scbox_state-error").removeClass('hide');
                $("#scbox_city-error").removeClass('hide');
                $("#scbox_shipaddress-error").removeClass('hide');
                $("#scbox_pincode-error").removeClass('hide');
                $("#scbox_likes-error").removeClass('hide');
                $("#scbox_dislikes-error").removeClass('hide');
              }*/
          }else
          {
            /*$("#scbox_gender-error").addClass('hide');
            $("#scbox_name-error").addClass('hide');
            //$("#scbox_age-error").addClass('hide');
            $("#scbox_emailid-error").addClass('hide');
            $("#scbox_mobile-error").addClass('hide');
            $("#scbox_state-error").addClass('hide');
            $("#scbox_city-error").addClass('hide');
            $("#scbox_shipaddress-error").addClass('hide');
            $("#scbox_pincode-error").addClass('hide');
            $("#scbox_likes-error").addClass('hide');
            $("#scbox_dislikes-error").addClass('hide');*/
            $("#scbox_gender-error").addClass('hide');
            $("#scbox_name").removeClass('invalid');
            //$("#scbox_age-error").removeClass('invalid');
            $("#scbox_emailid").removeClass('invalid');
            $("#scbox_mobile").removeClass('invalid');
            $("#scbox_state").removeClass('invalid');
            $("#scbox_city").removeClass('invalid');
            $("#scbox_shipaddress").removeClass('invalid');
            $("#scbox_pincode").removeClass('invalid');
            $("#scbox_likes").removeClass('invalid');
            $("#scbox_dislikes").removeClass('invalid');

            var dobvalid = validatedob();
            msg(dobvalid);
            if(dobvalid==true)
            {
              ScboxFormSubmit(1);
            }


          }
    }else if(id==2)
    {
           if($("#body_shape_ans").val()=='')
          {
            $("#body_shape_ans-error").removeClass('hide');
          }else
          {
            $("#body_shape_ans-error").addClass('hide');

            ScboxFormSubmit(2);

          }


    }else if(id==3)
    {
          if($("#scbox_style").val()=='')
          {
            $("#scbox_style-error").removeClass('hide');
          }else
          {
            $("#scbox_style-error").addClass('hide');

           ScboxFormSubmit(3);

          }


    }else if(id==4)
    {
          if($("#scbox_skintone").val()=='')
          {
            $("#scbox_skintone-error").removeClass('hide');
          }else if($("#scbox_colors").val()=='')
          {
            $("#scbox_colors-error").removeClass('hide');
          }/*else if($("#scbox_prints").val()=='')
          {
            $("#scbox_prints-error").removeClass('hide');
          }*/else if($("#scbox_sizetop").val()=='')
          {
            $("#scbox_sizetop-error").removeClass('hide');
          }else if($("#scbox_sizebottom").val()=='')
          {
            $("#scbox_sizebottom-error").removeClass('hide');
          }else if($("#scbox_sizefoot").val()=='')
          {
            $("#scbox_sizefoot-error").removeClass('hide');
          }else if($("#scbox_budget").val()=='')
          {
            $("#scbox_budget-error").removeClass('hide');
          }else if($("#scbox_sizeband").val()=='' && $("#scbox_gender").val()==1)
          {
            $("#scbox_sizeband-error").removeClass('hide');
          }else if($("#scbox_sizecup").val()=='' && $("#scbox_gender").val()==1)
          {
            $("#scbox_sizecup-error").removeClass('hide');
          }

          if($("#scbox_skintone").val()=='' && $("#scbox_colors").val()=='' && $("#scbox_sizetop").val()=='' && $("#scbox_sizebottom").val()=='' && $("#scbox_sizefoot").val()=='' && ($("#scbox_sizeband").val()=='' && $("#scbox_gender").val()==1) && ($("#scbox_sizecup").val()=='' && $("#scbox_gender").val()==1))
          {
            $("#scbox_skintone-error").removeClass('hide');
            $("#scbox_colors-error").removeClass('hide');
            //$("#scbox_prints-error").removeClass('hide');
            $("#scbox_sizetop-error").removeClass('hide');
            $("#scbox_sizebottom-error").removeClass('hide');
            $("#scbox_sizefoot-error").removeClass('hide');
            $("#scbox_budget-error").removeClass('hide');
            $("#scbox_sizeband-error").removeClass('hide');
            $("#scbox_sizecup-error").removeClass('hide');
          }
          else
          {
            $("#scbox_skintone-error").addClass('hide');
            $("#scbox_colors-error").addClass('hide');
            $("#scbox_prints-error").addClass('hide');
            $("#scbox_sizetop-error").addClass('hide');
            $("#scbox_sizebottom-error").addClass('hide');
            $("#scbox_sizefoot-error").addClass('hide');
            $("#scbox_budget-error").addClass('hide');
            $("#scbox_sizeband-error").addClass('hide');
            $("#scbox_sizecup-error").addClass('hide');

            ScboxFormSubmit(4);

          }

    }else if(id==5)
    {
        /*$("#scbox-form").validate({
             rules: {
              scbox_bodyshape: { required : true },
              scbox_style: { required : true },
              scbox_skintone: { required : true },
              scbox_colors: { required : true },
              scbox_prints: { required : true },
              scbox_sizetop: { required : true},
              scbox_sizebottom: { required : true },
              scbox_sizefoot: { required : true },
              scbox_budget: { required : true },
              scbox_sizeband: {
                required: function(element) {
                  //return $('#job-earnings-salary').is(':checked')
                  if($('#scbox_gender').val()==1) { return true;}else return false;
                }
              },
              scbox_sizecup: {
                required: function(element) {
                  //return $('#job-earnings-salary').is(':checked')
                  if($('#scbox_gender').val()==1) { return true;}else return false;
                }
              }
            },
             messages: {
              scbox_bodyshape:{ required : "Please select bodyshape" },
              scbox_style:{ required : "Please select style"  },
              scbox_skintone:{ required : "Please select skintone" },
              scbox_colors:{ required : "Please select colour" },
              scbox_prints:{ required : "Please select prints" },
              scbox_sizetop:{ required : "Please select the top size" },
              scbox_sizebottom:{ required : "Please select bottom size" },
              scbox_sizefoot:{ required : "Please select footwear size" },
              scbox_budget:{ required : "Please select budget" },
              scbox_sizeband:{ required : "Please select band size" },
              scbox_sizecup:{ required : "Please select cup size" },
            },
            submitHandler: function(form) {
              ScboxFormSubmit(2);
            //##GA Event add for Signup Link on 15-Jan-16 (as per Veelas email)
           // ga('send', 'pageview', '/sc-box/?page=' + document.location.pathname + document.location.search + ' - SCbox');
            }
          });*/

          ScboxFormSubmit(5);
    }

    /*$('#slide_'+id).addClass('hide');
    var next = id+1;
    $('#slide_'+next).removeClass('hide');*/
  }
}

function slideprev(id)
{
  if(id!='' && id<7 && id>0)
  {
    if(id!=6)
    {
      $('#slide_'+id).addClass('hide');
      var prev = id-1;
      $('#slide_'+prev).removeClass('hide');
    }else
    {
      $('#slide_'+id).addClass('hide');
      var prev = id-2;
      $('#slide_'+prev).removeClass('hide');
    }
  }
}

function ScboxFormSubmit(id)
{
    var actionurl = '';
    var next = 0;
    var objectid = $('#scbox_objectid').val();
    if(id==1)
    {
      //actionurl = sc_baseurl+'book-scbox/'+objectid+'?step=1';
     if($("#scbox_gender").val()!='' && $("#scbox_name").val()!='' && $("#scbox_age").val()!='' && $("#scbox_gender").val()!='' && $("#scbox_emailid").val()!='' && $("#scbox_mobile").val()!='' && $("#scbox_state").val()!='' && $("#scbox_city").val()!='' && $("#scbox_shipaddress").val()!='' && $("#scbox_pincode").val()!='' )
        {
           $.ajax({
            url: sc_baseurl+'scbox/savescbox',
            type: 'post',
            data: {'scbox-confirm' : '1','slide':'1', 'scbox_name':$('#scbox_name').val(), 'scbox_emailid':$('#scbox_emailid').val(), 'scbox_mobile':$('#scbox_mobile').val(), 'scbox_gender':$('#scbox_gender').val(), 'scbox_age':$('#scbox_age').val(), 'scbox_objectid':$('#scbox_objectid').val(), 'scbox_shipaddress':$('#scbox_shipaddress').val(), 'scbox_pincode':$('#scbox_pincode').val(), 'scbox_city':$('#scbox_city').val(), 'scbox_state':$('#scbox_state').val(), 'scbox_likes':$('#scbox_likes').val(), 'scbox_dislikes':$('#scbox_dislikes').val(), 'scbox_city':$('#scbox_city').val(), 'scbox_city':$('#scbox_city').val(), 'scbox_userid':$('#scbox_userid').val()  },
            success: function(data, status) {
              console.log(data);
              if(data!=''){
                if(data=='success'){

                  $('#slide_'+id).addClass('hide');
                      next = id+1;
                  $('#slide_'+next).removeClass('hide');
                  $('#scbox_slide').val(next);
                  scrolltop();
                 // window.location.href=sc_baseurl+'book-scbox?slide=2';
                }else if(data == 'error'){
                  //window.location.href=sc_baseurl+'book-scbox?slide=1';
                }else{
                  $('#scbx_error').html("Unexpected error. Please try again!");
                }
              }else{

              }
              //event.preventDefault();
            },
            error: function(xhr, desc, err) {
              // console.log(err);
            }
          });
        }

      /*$('#slide_'+id).addClass('hide');
          next = id+1;
      $('#slide_'+next).removeClass('hide');
      $('#scbox_slide').val(next); */

    }else if(id==2)
    {
       $.ajax({
          url: sc_baseurl+'scbox/savescbox',
          type: 'post',
          data: {'scbox-confirm' : '1','slide':'2','scbox_gender':$('#scbox_gender').val(), 'body_shape_ans':$('#body_shape_ans').val(), 'scbox_objectid':$('#scbox_objectid').val(), 'scbox_userid':$('#scbox_userid').val() },
          success: function(data, status) {
            console.log(data);
            if(data!=''){
              if(data=='success'){
                $('#slide_'+id).addClass('hide');
                    next = id+1;
                $('#slide_'+next).removeClass('hide');
                $('#scbox_slide').val(next);
                scrolltop();
               // window.location.href=sc_baseurl+'book-scbox?slide=2';
              }else if(data == 'error'){
                //window.location.href=sc_baseurl+'book-scbox?slide=1';
              }else{
                $('#scbx_error').html("Unexpected error. Please try again!");
              }
            }else{

            }
            //event.preventDefault();
          },
          error: function(xhr, desc, err) {
            // console.log(err);
          }
        });
      /*$('#slide_'+id).addClass('hide');
        next = id+1;
      $('#slide_'+next).removeClass('hide');
      $('#scbox_slide').val(next); */
    }else if(id==3)
    {
       $.ajax({
        url: sc_baseurl+'scbox/savescbox',
        type: 'post',
        data: {'scbox-confirm' : '1','slide':'3','scbox_gender':$('#scbox_gender').val(), 'scbox_style':$('#scbox_style').val(), 'scbox_objectid':$('#scbox_objectid').val(), 'scbox_userid':$('#scbox_userid').val() },
        success: function(data, status) {
          console.log(data);
          if(data!=''){
            if(data=='success'){
                  $('#slide_'+id).addClass('hide');
                      next = id+1;
                  $('#slide_'+next).removeClass('hide');
                  $('#scbox_slide').val(next);
                  scrolltop();
                 // window.location.href=sc_baseurl+'book-scbox?slide=2';
                }else if(data == 'error'){
                  //window.location.href=sc_baseurl+'book-scbox?slide=1';
                }else{
                  $('#scbx_error').html("Unexpected error. Please try again!");
                }
              }else{

              }
              //event.preventDefault();
            },
            error: function(xhr, desc, err) {
              // console.log(err);
            }
        });
      /*$('#slide_'+id).addClass('hide');
        next = id+1;
      $('#slide_'+next).removeClass('hide');
      $('#scbox_slide').val(next); */
    }else if(id==4)
    {
        $.ajax({
          url: sc_baseurl+'scbox/savescbox',
          type: 'post',
          data: {'scbox-confirm' : '1','slide':'4','scbox_gender':$('#scbox_gender').val(), 'scbox_skintone':$('#scbox_skintone').val(), 'scbox_colors':$('#scbox_colors').val(), 'scbox_prints':$('#scbox_prints').val(), 'scbox_sizetop':$('#scbox_sizetop').val(), 'scbox_sizebottom':$('#scbox_sizebottom').val(), 'scbox_sizefoot':$('#scbox_sizefoot').val(), 'scbox_sizeband':$('#scbox_sizeband').val(), 'scbox_sizecup':$('#scbox_sizecup').val(), 'scbox_objectid':$('#scbox_objectid').val(), 'scbox_userid':$('#scbox_userid').val() },
          success: function(data, status) {
            console.log(data);
            if(data!=''){
              if(data=='success'){
                $('#slide_'+id).addClass('hide');
                    next = id+2;
                $('#slide_'+next).removeClass('hide');
                $('#scbox_slide').val(next);
                scrolltop();
               // window.location.href=sc_baseurl+'book-scbox?slide=2';
              }else if(data == 'error'){
                //window.location.href=sc_baseurl+'book-scbox?slide=1';
              }else{
                $('#scbx_error').html("Unexpected error. Please try again!");
              }
            }else{

            }
            //event.preventDefault();
          },
          error: function(xhr, desc, err) {
            // console.log(err);
          }
        });
      /*$('#slide_'+id).addClass('hide');
        next = id+1;
      $('#slide_'+next).removeClass('hide');
      $('#scbox_slide').val(next); */
    }else if(id==5)
    {
      $.ajax({
        url: sc_baseurl+'scbox/savescbox',
        type: 'post',
        data: {'scbox-confirm' : '1','slide':'5','scbox_gender':$('#scbox_gender').val(), 'scbox_package':$('#scbox_package').val(), 'scbox_price':$('#scbox_price').val(), 'scbox_productid':$('#scbox_productid').val(), 'scbox_objectid':$('#scbox_objectid').val(), 'scbox_userid':$('#scbox_userid').val() },
        success: function(data, status) {
          console.log(data);
          if(data!=''){
            if(data=='success'){
              $('#slide_'+id).addClass('hide');
                  next = id+1;
              $('#slide_'+next).removeClass('hide');
              $('#scbox_slide').val(next);
              scrolltop();
             // window.location.href=sc_baseurl+'book-scbox?slide=2';
            }else if(data == 'error'){
              //window.location.href=sc_baseurl+'book-scbox?slide=1';
            }else{
              $('#scbx_error').html("Unexpected error. Please try again!");
            }
          }else{

          }
          //event.preventDefault();
        },
        error: function(xhr, desc, err) {
          // console.log(err);
        }
      });
      /*$('#slide_'+id).addClass('hide');
        next = id+1;
      $('#slide_'+next).removeClass('hide');
      $('#scbox_slide').val(next);*/
     /* actionurl = sc_baseurl+'book-scbox/'+objectid+'?step=1';
     $('#scbox-form').attr('action', actionurl);
     var scbox_style = document.forms.scbox-form;
     scbox-form.submit();
      scbox_register();*/
    }
  //slidenext(1);
  /*if(id==1)
  {
      $.ajax({
      url: sc_baseurl+'schome_new/savescbox',
      type: 'post',
      data: {'type' : 'scboxform1', 'scboxprice':price },
      success: function(data, status) {
        console.log(data);
        if(data!=''){
          if(data=='success'){
            window.location.href=sc_baseurl+'book-scbox/'+objectid;
          }else if(data == 'error'){
            window.location.href=sc_baseurl+'book-scbox/'+objectid;
          }else{
            $('#scbx_error').html("Unexpected error. Please try again!");
          }
        }else{

        }
        event.preventDefault();
      },
      error: function(xhr, desc, err) {
        // console.log(err);
      }
    });
  }*/
}

function confirmbox(e)
{
  $(e).preventDefault();
  $("#page-loader").show();
  var objectid = $('#scbox_objectid').val();
      actionurl = sc_baseurl+'book-scbox/'+objectid+'?step=1';
     $('#scbox-form').attr('action', actionurl);
     var scbox_style = document.forms.scbox-form;
     scbox-form.submit();
      scbox_register();
}

function getcustomprice()
{//alert('onblur');
  if($('#customboxprice').val()=='')
  {
    $('#customboxprice-error').removeClass('hide');

  }else
  {
    $('#customboxprice-error').addClass('hide');
  }
   scboxprice = $('#customboxprice').val();
  // alert($('#customboxprice').val());
     $('#scbox-price').val(scboxprice);
}


function validateform(e){ 

  var regexp = /[^a-zA-Z]/g;
  var regexp_mobile = /[^0-9]/g;
  var regex_dob = /^\d{1,2}\-\d{1,2}\-\d{4}$/;
  var isTrue = 0;

  //mobileno  = $('#scbox_mobile').val();
  elementid = $(e).attr('id');
  if($('#'+elementid).val()!='')
  {
    //$('#'+elementid+'-error').addClass('hide');
    $('#'+elementid).removeClass('invalid');
  }else
  {
      if(elementid!='scbox_age')
    {
      //$('#'+elementid+'-error').removeClass('hide');
      $('#'+elementid).addClass('invalid');
    }
  }



  if(elementid=='scbox_age')
  {
    validatedob();
  }
  /*if(mobileno.match(regexp_mobile) && mobileno!='' ){
    $('#scbox_mobile-error').removeClass('hide');
    $('#scbox_mobile').val('');
    isTrue = 1;
  }else if( (mobileno.length>10 && mobileno!='' )|| (mobileno.length<10 && mobileno!=''))
  {
    $('#scbox_mobile-error').removeClass('hide');
    $('#scbox_mobile-error').html('Please enter valid mobile no.');
    isTrue = 1;
  }else{
   $('#scbox_mobile-error').addClass('hide');
    //isTrue = 0;
  }*/
}

function validatedob()
{
  var regex_dob = /^\d{1,2}\-\d{1,2}\-\d{4}$/;
  var dob = $('#scbox_age').val();
  if(dob.match(regex_dob) && dob!='')
    {
      //$('#scbox_age-error').addClass('hide');
      $('#scbox_age').removeClass('invalid');
      msg('valid');
      return true;
    }else
    {
      //$('#scbox_age-error').removeClass('hide');
        $('#scbox_age').addClass('invalid');
      msg('invalid');
      return false;
    }
}

/*
//## function to check the section --------------
function InitSectionCheck(type){
	var selectedItem = 0
	if(type == 'body_shape'){
		$("#slide4").find(".row >div").each(function( index ) {
		  if($(this).hasClass("active")){
			  selectedItem = $(this).attr("data-attr");
		  }
		});
	} else if(type == "body_style"){
		$("#slide5").find(".box-style-selected > li").each(function( index ) {
		  if($(this).hasClass("filled")){
			  selectedItem++;
		  }
		});
	}
	return selectedItem;
}
*/
