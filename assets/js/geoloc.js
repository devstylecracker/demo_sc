//added for geo location tracking after signup

function writeAddressName(latLng) {
  //var gac = new gaCookies();
  var user_id = 0;  // gac.getUniqueId();
  var getUrl = window.location;
  var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
  var geocoder = new google.maps.Geocoder();
  geocoder.geocode({
    "location": latLng
    },
    function(results, status) {
    if (status == google.maps.GeocoderStatus.OK){
    // add ajax here to submit the location to database
       var geoLoc = results[0].formatted_address;
        $.ajax({
        url: baseUrl+"/analytics/add_geo_loc",
        type: 'post',
        data: {'user_id':user_id,'geo':geoLoc},
        success: function(response) { 
          //msg(data);
        },
        error: function(xhr, desc, err) {
          //console.log(err);
        }
           });
      }
    /*else
      document.getElementById("error").innerHTML += "Unable to retrieve your address" + "<br />";*/
  });
} 
function geolocationSuccess(position) {
  var userLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
  // Write the formatted address
  writeAddressName(userLatLng);         
  //mapObject.fitBounds(circle.getBounds());
} 
function geolocationError(positionError) {
  /*document.getElementById("error").innerHTML += "Error: " + positionError.message + "<br />";*/
} 
function geolocateUser() {
// If the browser supports the Geolocation API
  if (navigator.geolocation)
  {
  var positionOptions = {
  enableHighAccuracy: true,
  timeout: 30 * 1000 // 10 seconds
  };
  navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError, positionOptions);
  }
  else
  document.getElementById("error").innerHTML += "Your browser doesn't support the Geolocation API";
}