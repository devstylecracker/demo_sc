  var getUrl = window.location;
  var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1]+ "/";
  $( document ).ready(function() {

      // navigation width
      var  winWidth =  $(window).width();
      var  contWidth =  $('.container').width();

      if(winWidth < 1366){
        menuInnerWidth = '100%';
      }
      else {
        menuInnerWidth = '1366px';
      }
      $(this).find('.sub-menu').css('width', winWidth +'px');
      $(this).find('.sub-menu-inner').css('width', menuInnerWidth);
      /// navigation width

      //menu collaspible
      $(".sc-menu-collapse").click(function(){
       $(this).toggleClass('active');
       $(this).next().slideToggle();
      });
      ///menu collaspible

      //menu-mobile
      $(".slide-left-toggle").click(function () {
          $(".sc-menu-overlay-sm").toggle();

          $(".slide-left-content").toggleClass('open');
          if(!$(".slide-left-content").hasClass('open'))
          {
          $(".slide-left-content").animate({left: '-85%'},300);
          }
          else {
          $(".slide-left-content").animate({left: '0'},300);
          }
      });
      ///menu-mobile


    // submenu
      $(".primary-navbar").hoverIntent({
                    over: addClassHover,
                    out: removeClassHover,
                    timeout:100,
                    selector: 'li'
      });

       function addClassHover()
      {
          $(this).addClass('hover');
      }
        function removeClassHover()
      {
          $(this).removeClass('hover');
      }
  // submenu

  //fav text for signup
  $(".sc-love").click(function(){
   $(".fav-txt").html("Add to Wish List");
  });

  //fav text for signup remove
  $(".menu-highlight a").click(function(){
   $(".fav-txt").html("");
  });

//custom radio
 if ($(".sc-radio input").is(':checked')) {
   $('.sc-radio input:not(:checked)').parent().removeClass("active");
   $('.sc-radio input:checked').parent().addClass("active");
 }

 if ($(".sc-radio input").is(':disabled')) {
   $('.sc-radio input:not(:disabled)').parent().removeClass("disabled");
   $('.sc-radio input:not(:checked)').parent().removeClass("active");
   $('.sc-radio input:disabled').parent().addClass("disabled");
 }


  $(".sc-radio input[type='radio']").click(function() {
    var  radioName= $(this).attr('name');
    $(".sc-radio input[name='"+ radioName + "']").closest('.sc-radio').removeClass('active');
    $(this).closest('.sc-radio').addClass('active');
 });
///custom radio
 //custom checkbox
   $(".sc-checkbox input[type='checkbox']").click(function() {
    if($(this).is(":checked")) {
     $(this).closest('.sc-checkbox').addClass('active');
    }else{
      $(this).closest('.sc-checkbox').removeClass('active');
    }
  });

  //sc popup
  $('.scmfp').magnificPopup({
    type:'image',
    image: {
    markup: '<div class="mfp-figure sc-mfp-wrp">'+
              '<div class="mfp-close"></div>'+
              '<div class="mfp-bottom-bar">'+
                '<div class="mfp-title"></div>'+
              '</div>'+
             '<div class="mfp-img-wrp"><div class="mfp-img"></div></div>'+
            '</div>',
    cursor: 'mfp-zoom-out-cur', // Class that adds zoom cursor, will be added to body. Set to null to disable zoom out cursor.
    titleSrc: 'title',
    tError: '<a href="%url%">The image</a> could not be loaded.' // Error message
  },
  });

  $('.scmfp-video').magnificPopup({
    //disableOn: 700,
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,

    fixedContentPos: false
         });

   ///sc popup

   ///sc slideover
$(".sc-slideover .close").click(function () {
  $(this).parents(".sc-slideover").slideUp('fast');
});
$(window).load(function() {
    initialSlideover();
});


function initialSlideover() {

    if(getCookie('first_time_open') != 1){
      setTimeout(openSlideover(), 100);
      document.cookie = "first_time_open=1;path=/";
    }
    setTimeout(intervalSlideover, 100);
}

function openSlideover(){
  $("#slideover").slideDown('slow');
};
var i=0;
var intervalSlideover =  setInterval(function() {
    openSlideover();
  }, getCookie("popup_timer"));

   ///sc slideover

  //category filter page
  $('.scfiltertitle').click(function(e) {
    if ($(this).find('.fa').hasClass('fa-minus')) {
        $(this).find('.fa').removeClass('fa-minus').addClass('fa-plus');
    }
    else {
        $(this).find('.fa').removeClass('fa-plus').addClass('fa-minus');
    }
  });

  $(".btn-filter-by").click(function () {
    $(".sc-sidebar-overlay-sm").show();
    $(".sidebar-filter").toggleClass('open');
    $(".sidebar-filter").fadeIn('slow');
  });


$(window).load(function() {
if( ($('#collapse-product-category.custom-scrollbar').length > 0) && ($('#collapse-product-category.custom-scrollbar .active').length > 0) ){

   var activeWrpPos =  $('#collapse-product-category.custom-scrollbar').position().top;
   var activePos = $("#collapse-product-category.custom-scrollbar .active").position().top;
   var Pos = activePos - activeWrpPos;
    $('#collapse-product-category.custom-scrollbar').scrollTop( activePos );
    $('#collapse-product-category.custom-scrollbar').perfectScrollbar('update');
}
})


/*
$(window).load(function() {
   var activeWrpPos =  $('.testwrp').position().top;
    var activePos = $(".testwrp .active").position().top;
   var Pos = activePos - activeWrpPos;
    $('.testwrp').scrollTop( activePos );
    $('.testwrp').perfectScrollbar('update');
})
*/
  ////---------

  $('.sc-sidebar-overlay-sm').click(function(event) {
      //hide Sidebar Filter Overlay
        if(!$(event.target).closest('.sidebar-filter').length &&
           !$(event.target).is('.sidebar-filter')) {
            if($('.sidebar-filter').is(":visible")) {
              $(".sidebar-filter").fadeOut('slow');
              // $(".sidebar-filter").animate({left: '-110%'},300);
                $(".sidebar-filter").removeClass('open');
               $(this).hide();

            } }
  })
  $('.sc-menu-overlay-sm').click(function(event) {
      //hideMenuOverlay
      if(!$(event.target).closest('.slide-left-content').length &&
         !$(event.target).is('.slide-left-content')) {
          if($('.slide-left-content').is(":visible")) {
              $(".slide-left-content").animate({left: '-110%'},300);
              $(".slide-left-content").removeClass('open');
              $(this).hide();
          }}
  })


  $('.filter-tag .remove').click(function(e) {
    $(this).parent().remove();
  })
  $('.collapse-gender li, .collapse-brands li, .collapse-colors li ,.collapse-product-categories li').click(function(e) {
    //$(this).toggleClass('active');
  })

  $('.collapse-offers li, .collapse-price li, .collapse-discounts li').click(function(e) {
    $(this).siblings().removeClass('active');
    $(this).addClass('active');
  })

  ///category filter page

  //top gender filter
  $('.filterbar label').click(function() {
  $(this).toggleClass('active');
  });

  /* Product Page start*/
//$('.box-added-to-cart').fadeIn('400');
//$('.box-added-to-cart').show(2000).delay(5000).slideUp(1500);
  /* Brand List Page start*/
  $("#brand-search").keyup(function(){
    $(".categories").removeClass("active");
    $("#brands_a_z").addClass("active");
    var search = $('#brand-search').val();
     $.ajax({
          url: sc_baseurl+"brands/search_brand",
          type: 'POST',
          data: {'search':search},
          cache :true,
          success: function(response) {

            $('#brandlist').html(response);

          },
          error: function(xhr) {

          }
        });
    });

    $('#brands_a_z').click(function(){
      var search = $('#brand-search').val();
      $(".categories").removeClass("active");
      $("#brands_a_z").addClass("active");
     $.ajax({
          url: sc_baseurl+"brands/search_brand",
          type: 'POST',
          data: {'search':search,'type':''},
          cache :true,
          success: function(response) {

            $('#brandlist').html(response);

          },
          error: function(xhr) {

          }
        });
    });

     $('#brands_popular').click(function(){
        var search = $('#brand-search').val();

     $.ajax({
          url: sc_baseurl+"brands/search_brand",
          type: 'POST',
          data: {'search':search,'type':'brand_popular'},
          cache :true,
          success: function(response) {

            $('#brandlist').html(response);

          },
          error: function(xhr) {

          }
        });
    });
  /* Brand List Page end*/

  /*Brand Details Page*/

     $('.do-not-del-ratings').change(function () {
        var me = $("input[name='ratings']:checked").val();
        $('#addratings').removeClass();
        $('#addratings').addClass('ratings rating-'+me);

    });
     $('.do-not-del-ratings').hover(function () {
        var me = $("input[name='ratings']:checked").val();
        $('#addratings').removeClass();
        $('#addratings').addClass('ratings rating-'+me);

    });

   $("input[name='ratings']:checked").trigger( "hover" );
     $( "#post_review" ).click(function() {
                  var post_review_text = $('#review_text').val();
                  var ratings = $('input[name=ratings]:checked').val();
                  var brand_token = $('#brand_token').val();

                  if(post_review_text.trim() == ''){
                    $('#review_error').html('Please enter the Review');
                    $('#review_error').addClass("error");
                    $('#review_error').removeClass("success");
                  }else if(ratings <= 0 || !ratings){
                    $('#review_error').html('Please give the ratings');
                    $('#review_error').addClass("error");
                    $('#review_error').removeClass("success");
                  }else{

                    $.ajax({
                      url: sc_baseurl+'brands/post_review',
                      type: 'POST',
                      data: {'post_review':post_review_text,'ratings':ratings,'brand_token':brand_token},
                      cache :true,
                      success: function(response) {
                        $('#review_error').html('Thanks for your review');
                        $('#review_error').addClass("success");
                        $('#review_error').removeClass("error");
                        get_reviews();
                        $('#review_text').val('');
                        // location.reload();

                      },
                      error: function(xhr) {
                        alert('error');
                      }
                    });

                  }
                });
  /* Brand Details Page*/

    /*Look Details Page start*/
          $('.sc-popover[data-toggle="popover"]').popover({html:true, container:'.item', placement:'bottom'});

          $('body').on('click', function (e) {
            $('.sc-popover[data-toggle="popover"]').each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
            });
          });



  $('.item-look-product .btn-quick-view').css('visibility','visible');

  $( "#post_look_review" ).click(function() {
                  var post_review_text = $('#review_text').val();
                  var ratings = $('input[name=ratings]:checked').val();
                  var brand_token = $('#brand_token').val();

                  if(post_review_text.trim() == ''){
                    $('#review_error').html('Please enter the Review');
                    $('#review_error').addClass("error");
                    $('#review_error').removeClass("success");
                  }else if(ratings <= 0 || !ratings){
                    $('#review_error').html('Please give the ratings');
                    $('#review_error').addClass("error");
                    $('#review_error').removeClass("success");
                  }else{

                    $.ajax({
                      url: sc_baseurl+'looks/post_review',
                      type: 'POST',
                      data: {'post_review':post_review_text,'ratings':ratings,'brand_token':brand_token},
                      cache :true,
                      success: function(response) {

                        $('#review_error').html('Thanks for your review');
                        $('#review_error').addClass("success");
                        $('#review_error').removeClass("error");
                        get_reviews();
                        $('#review_text').val('');
                        // location.reload();

                      },
                      error: function(xhr) {
                        alert('error');
                      }
                    });

                  }
                });
  /*Look Details Page end*/

  /* stylist page */
  $(".page-stylist .btn-more").click(function(){
       $(".more-info").animate({height: 'toggle'});
       $(this).hide();
       $(".btn-less").show();
  });

  $(".page-stylist .btn-less").click(function(){
       $(".more-info").animate({height: 'toggle'});
       $(this).hide();
      $(".btn-more").show();
  });


  $( ".page-stylist .stylist-message" ).focus(function() {
    $(".stylist-message-box .btn").fadeIn( 100 );
});


  /* //stylist page */
/* products listing page -lazyload */
if($("img.lazy").length > 0){
$("img.lazy").lazyload({
    effect : "fadeIn",
    threshold : 300,
    placeholder: 'https://www.stylecracker.com/assets/images/logo-graysclae.png'
});
}

/*  //products listing page -lazyload */


  /* order history */
  $('.page-orders .panel-heading .btn').click(function() {
  $(this).toggleClass('btn-primary');
  });
    /* order history */

initialiceMasonry();

  $(window).scroll(function(){
        //fixed header
          var sticky = $('.header.is-sticky');
          var scroll = $(window).scrollTop();
          var height = $(".header.is-sticky").height();
          var winHeight = $(window).height();
          if (scroll >= height && scroll > winHeight)
          {
             sticky.addClass('fixed');
          }
          else {
            sticky.removeClass('fixed');
          }
        ///fixed header

        // fixed filterbar-wrp for mobile
       if($(".filterbar-wrp.sm-only.is-sticky").length >0 ){
            var sticky = $(".filterbar-wrp.sm-only.is-sticky");
            var scr = $(window).scrollTop();
            var offset = sticky.position();
            var topPos = offset.top;
            var winWidth = $(window).width();
          //  $(".filterbar-wrp").text(height);
          //alert(offset.top);

            //if (scr >= 525 && winWidth > 767)
              if (scr >= topPos && winWidth > 767)
            {
               sticky.addClass('fixed');
            }
            else
            {
              sticky.removeClass('fixed');
            }
        }
    /// fixed filterbar-wrp for mobile

    // fixed filterbar-wrp for md
  /* if($(".sidebar-filter-wrp111 .sidebar-filter.is-sticky").length >0 ){

      var scrollTop = $(window).scrollTop();
      var stickyEle = $(".sidebar-filter-wrp .sidebar-filter.is-sticky");
      var offset = stickyEle.position();
      var topPos = offset.top;
      var leftPos = offset.left;
      var stickyEleHeight = stickyEle.height();
      var stickyEleWidth = stickyEle.width();
      var stickyBarHeight = stickyEleHeight + topPos;
      var winWidth = $(window).width();
      var winHeight = $(window).height();

   if (scrollTop >= stickyBarHeight && scrollTop >= winHeight && winWidth > 767)
        {
           stickyEle.addClass('fixed');
           stickyEle.css('width', stickyEleWidth);
        }
        else
        {
          stickyEle.removeClass('fixed');
        }
    }*/

    $('.sticky-sidebar').theiaStickySidebar({
      additionalMarginTop: 30
    });
/// fixed filterbar-wrp for md

 //back-to-bottom button
if($(document).height() > $(window).height() && $(window).scrollTop() < 500 ) {
  $(".back-to-top.scroll-bottom").fadeIn();
} else {
  $(".back-to-top.scroll-bottom").fadeOut();
}
 ///back-to-bottom button

var top=300;
if ($(this).scrollTop() > 300) {
// if(window.scrollY > top) {
   $(".back-to-top.scroll-top").fadeIn();
 } else {
   $(".back-to-top.scroll-top").fadeOut();
 }
 ///back-to-top button

///
  }); ///window.scroll

  //back-to-top button
  $(".back-to-top.scroll-bottom").fadeIn();

  $(".back-to-top.scroll-bottom").click(function (e) {
      $("html, body").animate({ scrollTop: $(document).height() }, 1000);
   });

  $(".back-to-top.scroll-top").click(function (e) {
        $("html, body").animate({scrollTop: 0}, 1000);
  });

//login popup
    var url="";
  $('a[href = "#myLoginModal"], a[href = "#myModal"]').click(function(){
    url = document.URL;
    var sign_up_dynamic_text = $(this).attr("title");
    //alert(sign_up_dynamic_text);
    $(".fav-txt").html(sign_up_dynamic_text);
    createCookie('current_url');
    createCookie('is_redirect');
    console.log(typeof(noCookie));
    if(typeof(noCookie) == "undefined"){
      document.cookie="current_url="+url+"; path=/";
      document.cookie="is_redirect=1"+"; path=/";
    }
  });

  function createCookie(name) {
      //document.cookie = name + '=; Max-Age=0'
      document.cookie = name + "=deleted; expires=" + new Date(0).toUTCString()+"; path=/";
  }
///login popup

  //show inline block
  $.fn.showInlineBlock = function () {
          return this.css('display', 'inline-block');
      };
  ///show inline block


    //zoomin
      zoominDestroy();
      zoominState();

    function zoominState(){
      if($('.zoom').length>0){
        if ($('body').hasClass("device-sm")) {
                  $('.zoom a').magnifik();
         }
         else {
               $('.zoom a').click(function( event ) {
                    event.preventDefault();
                    event.stopImmediatePropagation();
                    return false;
                });
              $('.zoom img').elevateZoom({
                zoomWindowWidth:'450',
                zoomWindowHeight: '450',
               //zoomWindowWidth:'446',
               //zoomWindowHeight: '490',
               cursor: "crosshair",
               scrollZoom:true,
              // zoomWindowFadeIn: 500,
              // zoomWindowFadeOut: 750,
              // zoomWindowOffetx: 20,
               responsive:true,
               zoomWindowPosition: "zoom-place-holder",
              // tint:true, tintColour:'#000000', tintOpacity:0.9,
            });
          }
      }
    }///zoomin state

    //zoominDestroy
      function zoominDestroy(){
        $('.zoomContainer').remove();
        $('.zoomWindowContainer').remove();

        $('.zoom img').removeData('elevateZoom');
        $('.zoom img').removeData('zoomImage');

      }
      ///zoominDestroy
      ///zoomin

  //slider - homepage, brands page
  var swiper = new Swiper('.swiper-container.main', {
              pagination: '.swiper-pagination.main',
              paginationClickable: true,
              slidesPerView: 1,
              loop: true,
              autoplay:4000,
              nextButton: '.swiper-button-next.main',
              prevButton: '.swiper-button-prev.main',
  });

  funCommonSwiper();
  var commonSwiper;
  function funCommonSwiper() {
   commonSwiper = new Swiper('.common.swiper-container', {
          //  pagination: '.swiper-pagination.common',
            slidesPerView: 4,
            paginationClickable: true,
            spaceBetween: 0,
          //  loop: true,
            nextButton: '.swiper-button-next.common',
            prevButton: '.swiper-button-prev.common',
            breakpoints: {
             1024: {
                 slidesPerView: 4,
             },
             768: {
                 slidesPerView: 3,
             },
             640: {
                 slidesPerView: 2,
             },
             320: {
                 slidesPerView: 2,
             }
         }

   });

   }

  function funCommonSwiperDestroy() {
    commonSwiper.destroy();
  }

   $('.common-tabs a').on('shown.bs.tab', function () {

     funCommonSwiper();

       }).on('hide.bs.tab', function () {

     //funCommonSwiperDestroy();
        //commonSwiper.destroy();
       });



  //filter page slider


     var lookGalleryTop = new Swiper('.swiper-container.look-gallery-top', {
        nextButton: '.look-gallery-top .swiper-button-next',
        prevButton: '.look-gallery-top .swiper-button-prev',
        spaceBetween: 0,
        centeredSlides: true,
        onSlideChangeStart: function (s) {
                           var activeSlide = $('.swiper-slide').eq(s.activeIndex);
                           $('.swiper-slide > div').removeClass('zoom');
                           activeSlide.find('.item-img-inner').addClass('zoom');
                           zoominDestroy();
                           zoominState();

                        },
    });
    var lookGalleryThumbs = new Swiper('.gallery-thumbs.look-gallery-thumbs', {
        spaceBetween: 15,
        slidesPerView: 4,
        touchRatio: 0.2,
        slideToClickedSlide: true,
        centeredSlides: true,
    });
    lookGalleryTop.params.control = lookGalleryThumbs;
    lookGalleryThumbs.params.control = lookGalleryTop;
    ///look page

    //product details page
    var productGalleryTop = new Swiper('.swiper-container.product-gallery-top', {
      nextButton: '.product-gallery-top .swiper-button-next',
      prevButton: '.product-gallery-top .swiper-button-prev',
      spaceBetween: 0,
      centeredSlides: true,
      onSlideChangeStart: function (s) {
                         var activeSlide = $('.swiper-slide').eq(s.activeIndex);
                         $('.swiper-slide > div').removeClass('zoom');
                         activeSlide.find('.item-img-inner').addClass('zoom');
                         zoominDestroy();
                         zoominState();

                      },

   });
    var productGalleryThumbs = new Swiper('.gallery-thumbs.product-gallery-thumbs', {
        spaceBetween: 15,
        slidesPerView: 4,
        touchRatio: 0.2,
      //  observer: true,
      //  observeParents: true,
        slideToClickedSlide: true,
        centeredSlides: true,

    });
    productGalleryTop.params.control = productGalleryThumbs;
    productGalleryThumbs.params.control = productGalleryTop;
    //product details page

    //mobile-swiper-container
  if ($('body').hasClass("device-sm")) {

      $('.grid-items-slider').addClass('mobile-swiper-container');
      $('.grid-items-slider .grid-item').addClass('swiper-slide');
      $('.grid-items-slider .mobile-swiper-wrp').addClass('swiper-wrapper');

      var mobileSlider = new Swiper('.device-sm .mobile-swiper-container', {
              //  pagination: '.swiper-pagination',
              //  slidesPerView: 1,
                paginationClickable: true,
                spaceBetween: 0,
                cssWidthAndHeight: true,
                slidesPerView:'auto',
              // nextButton: '.swiper-button-next',
              // prevButton: '.swiper-button-prev',
          });
    } ///mobile-swiper-container


  $('.swiper-pagination').css('visibility','visible');
  $('.swiper-container.common').css('visibility','visible');
  // fix for chromelink issue
if(!$(".swiper-container .swiper-slide div").hasClass('zoom')){

$(".swiper-container .swiper-slide a").bind("click", function(e) {

var target = $(this).attr('target');
  var path=$(this).attr('href');
    if(target!="_blank"){
     window.location.href= path;
    }
    else {
      e.preventDefault();
      window.open(path, '_blank');
    }
});
}
$(".swiper-container.common .swiper-slide a").bind("click", function(e) {

var target = $(this).attr('target');
  var path=$(this).attr('href');
    if(target!="_blank"){
     window.location.href= path;
    }
    else {
      e.preventDefault();
      window.open(path, '_blank');
    }
});
/// fix for chromelink issue
      //------------------

// perfectScrollbar
$('.custom-scrollbar').addClass('custom');
$('.custom-scrollbar').perfectScrollbar();

$(".collapse").on('shown.bs.collapse', function(){
      $('.custom-scrollbar').perfectScrollbar('update');
   });
         // Initialize
/// perfectScrollbar


  if($(window).width()>767){
    $('.item-hover').hover(function(){
      $(this).addClass("hover");
    }, function(){
      $(this).removeClass("hover");
    });
  }

  jQuery.validator.addMethod("accept", function(value, element, param) {
    return value.match(new RegExp("." + param + "$"));
  });

    jQuery.validator.addMethod("noSpace", function(value, element) {
    return value.indexOf(" ") < 0 && value != "";
  }, "No space please and don't leave it empty");


   $('[data-toggle="popover"]').popover();
   $('[data-toggle="tooltip"]').tooltip();
   //cart
/*  $('.popup-cart-price[data-toggle="popover"]').popover({ html:true,
           content: function() {
             var content = $(this).attr("data-popover-content");
             return $(content).html();
           }
   });
*/
   //custom sc-popover
 $('.sc-popover-btn[data-toggle="sc-popover"]').click( function () {
             var content = $(this).attr("data-content");
             //var title = $(this).attr("data-title");
             var html = '<div class="sc-popover bottom" role="tooltip"><div class="arrow"></div><div class="sc-popover-content">'+ content +'</div></div>';
             $(this).after(html);
  });
  
    $(document).on( "mouseup touchend", function (e)
  {
      var container = $(".sc-popover");
      if (!container.is(e.target)
          && container.has(e.target).length === 0)
      {
          container.remove();
      }
  });
  
  // /.custom sc-popover

  $(document).bind( "mouseup touchend", function (e)
  {
      var container = $(".popover");
      if (!container.is(e.target) // if the target of the click isn't the container...
          && container.has(e.target).length === 0) // ... nor a descendant of the container
      {
          container.remove();
          //container.prev('.sc-popover-btn').removeAttr('aria-describedby');
      }
  });

  //  prompt-popup

  	$('.prompt-popup .close').click(function(){
  	   $('.prompt-popup').hide();
       $('.page-overlay').hide();
    //  $('.page-dialog-overlay').hide(); //for dialog box
    });


   //cart page

   $('.check-billing-address').change(function() {
       if($(this).is(":checked")) {
         $('.billing-address-box').hide();
       }
       else{
       $('.billing-address-box').show();
     }
   });

   $('.show-address-box').change(function() {
       if($(this).is(":checked")) {
          $(this).parents('.show-address-box-wrp').hide();
         $('.add-address-box').show();
       }
       else{
          $(this).parents('.show-address-box-wrp').show();
       $('.billing-address-box').hide();
     }
   });

//new
$('.panel-tab').click( function(){
  $('.panel-tab-content').hide();
  $(this).parents('.panel-wrp').find('.panel-tab-content').show();
});
$( "#sc_emailid" ).keyup(function() {
  $('#sc_emailid_error').html('');
});

$( "#logPassword" ).keyup(function() {
  $('#login_error').html('');
});

   //cart page

  /*Sign up form Validation*/
        $("#scSignup").validate({
           rules: {

            //sc_username : { required : true ,noSpace: true },
            //sc_gender : { required : true ,minlength:1},
            sc_firstname : { required : true ,minlength:1},
            sc_lastname : { required : true ,minlength:1},
            sc_gender: {
                required: true
            },
            sc_emailid:{  required : true, email :true },
            sc_password: { required : true,minlength:8,maxlength:20 },
            sc_mobile: { required : true,minlength:10,maxlength:10,number:true }
          },
           messages: {
            /*sc_username : {
              required : "Please enter your username",
              noSpace :  "Only characters and numbers are allow"
            },*/
            sc_firstname : {
              required : "Please enter First Name"
            },
            sc_lastname : {
              required : "Please enter Last Name"
            },
            sc_gender : {
              required : "Please select Gender",minlength :"Please select Gender"
            },
            sc_emailid:{
              required : "Please enter Email",email :"Please enter valid Email"
            },
            sc_password:{ required : "Please enter Password", minlength: "Password should have atleast 8 character",maxlength: "Password should not greater than 20 character"
             },
            sc_mobile:{ required : "Please enter Mobile No.", minlength: "Enter Valid Mobile No.",maxlength: "Enter Valid Mobile No."
             }
          },
          submitHandler: function(form) {
            sc_signIn();


  			//##GA Event add for Signup Link on 15-Jan-16 (as per Veelas email)
  			ga('send', 'pageview', '/overlay/signup/?page=' + document.location.pathname + document.location.search + ' - Sign Up');
          }
        });

  /*Sign up form Validation for promo */

  		 $("#sc_Signuppromo1").validate({
           rules: {
            //sc_username_promo : { required : true ,noSpace: true },
            sc_firstnamesc:{required : true},
            sc_lastnamesc:{required : true},
            sc_emailid_promo:{  required : true, email :true },
            sc_password_promo: { required : true,minlength:8,maxlength:20 },
            sc_gender:{required : true},
            sc_mobile_promo: { required : true,minlength:10,maxlength:10,number:true }
          },
           messages: {
            /*sc_username_promo : {
              required : "Please enter your username",
              noSpace :  "Only characters and numbers are allow"
            },*/
            sc_firstnamesc : {
              required : "Please enter First Name"
            },
            sc_lastnamesc : {
              required : "Please enter Last Name"
            },
            sc_emailid_promo:{
              required : "Please enter Email",email :"Please enter valid Email"
            },
            sc_password_promo:{ required : "Please enter Password", minlength: "Password should have atleast 8 character",maxlength: "Password should not greater than 20 character"
             },
             sc_gender:{required : "Please select Gender" },
            sc_mobile_promo:{ required : "Please enter Mobile No.", minlength: "Enter Valid Mobile No.",maxlength: "Enter Valid Mobile No."
             }
          },
          submitHandler: function(form) {
            sc_signInp();

          }
        });


  	  /*Sign up form Validation for promo2*/

  		 $("#sc_Signuppromo2").validate({
           rules: {
            //sc_username_promo2 : { required : true ,noSpace: true },
            sc_firstname2:{required : true},
            sc_lastname2:{required : true},
            sc_emailid_promo2:{  required : true, email :true },
            sc_password_promo2:{ required : true,minlength:8,maxlength:20 },
            sc_gender2:{required : true},
            sc_mobile_promo2: { required : true,minlength:10,maxlength:10,number:true }
          },
           messages: {
            /*sc_username_promo2 : {
              required : "Please enter your username",
              noSpace :  "Only characters and numbers are allow"
            },*/
            sc_firstname2 : {
              required : "Please enter First Name"
            },
            sc_lastname2 : {
              required : "Please enter Last Name"
            },
            sc_emailid_promo2:{
              required : "Please enter Email",email :"Please enter valid Email "
            },
            sc_password_promo2:{ required : "Please enter Password", minlength: "Password should have atleast 8 character",maxlength: "Password should not greater than 20 character"
             },
            sc_gender2:{required : "Please select Gender" },
            sc_mobile_promo2:{ required : "Please enter Mobile No.", minlength: "Enter Valid Mobile No.",maxlength: "Enter Valid Mobile No."
             }
          },
          submitHandler: function(form) {
            sc_signInp2();

          }
        });
  /*Login form Validation*/
        $("#scnewLogin").validate({
           rules: {
            logEmailid:{  required : true, email :true },
            logPassword: { required : true,minlength:8,maxlength:20 }
          },
           messages: {
            logEmailid:{
              required : "Please enter Email",email :"Please enter valid Email"
            },
            logPassword:{ required : "Please enter Password", minlength: "Password should have atleast 8 character",maxlength: "Password should not greater than 20 character"
             }
          },
          submitHandler: function(form) {
            sc_login();

          }
        });

  /*Forgot-Passord form Validation*/
        $("#forgotpwdfrm").validate({
           rules: {
            Emailid:{  required : true, email :true }
          },
           messages: {
            Emailid:{
              required : "Please enter the Email",email :"Please enter the valid Email"
            }
          },
          submitHandler: function(form) {
            sc_forgotpwd();

          }
        });


        /*Reset-Passord form Validation*/
        $("#resetpwdfrm").validate({
           rules: {
            oldpwd:{  required : true },
            newpwd:{  required : true }
          },
           messages: {
            oldpwd:{
              required : "Please enter the Old-Password"
            },
            newpwd:{
              required : "Please enter the New-Password"
            }
          },
          submitHandler: function(form) {
            sc_resetpwd();

          }
        });


  /*Login form Validation*/
        $("#frmContact").validate({
           rules: {
            contact_name:{  required : true,accept: "[a-zA-Z ]" },
            email_id: { required : true,email:true },
            subject: { required : true },
            message: { required : true },

          },
           messages: {
            contact_name:{
              required : "Please enter the contact name",
              accept : "Only Characters are allow"
            },
            email_id:{
               required : "Please enter the email id",
               email : "Please enter the valid email",
            },
            subject:{
              required : "Please enter the subject"
            },
            message:{
              required : "Please enter the message"
            },
          },
          submitHandler: function(form) {

            sc_contactus();

          }
        });

  //masonry

  function initialiceMasonry(){
    if($('.grid-items-wrp.masonry').length > 0){
      var $container = $('.grid-items-wrp.masonry');
          $container.masonry({
              isInitLayout : true,
              itemSelector: '.grid-item',
              isAnimated: false,
              percentPosition: true

          });

          $container.imagesLoaded(function() {
            $( $container ).masonry( 'reloadItems' );
            $( $container ).masonry( 'layout' );
          });
        }
    }


  // /masonry


  $('#sc_change_gender').change(function(){
      var gender = $(this).val();
      if(gender == 1){
        var result = confirm("Ready to discover the lastest personalised fashion for women?");
      }else{
        var result = confirm("Ready to discover the coolest men's fashion?");
      }
      if(result){
        $.ajax({
            url: sc_baseurl+'schome/sc_change_gender',
            type: 'post',
            data: {'type' : 'sc_change_gender', 'gender':gender },
            success: function(data, status) {
              if(data == 1){
                if(gender == 2){
                  window.location.href=sc_baseurl+"schome/pa_mens";
                }else{
                  window.location.href=sc_baseurl+"schome/pa";
                }
              }else{
                window.location.href=sc_baseurl+"profile";
              }
            }
        });
      }
  });

  });

function HideModal(){
  $('body #myModal').find('.close').trigger('click');
}

  function HideModal(){
    $('body #myModal').find('.close').trigger('click');
  }

  function HideLoginModal(){
    $('body #myLoginModal').find('.close').trigger('click');
  }

  $('.wardrobe-popup-dialog').on('show.bs.modal', function () {
        $('.modal-body',this).css({width:'auto',height:'auto', 'max-height':'100%'});
  });

  function _targetTweet(loc,title,look_id){
    window.open('http://twitter.com/share?url=' + loc + '&text=' + title + '&', 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

  }


  function _targetPinterest(loc,title,look_id,img_src){
    window.open('https://www.pinterest.com/pin/create/button/?url=' + loc + '&description=' + title + '&media=' + img_src + '&', 'pinterestwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
  }

  function add_hover(){
    	if($(window).width()>767){
    	$('.item-hover').hover(function(){
    	  $(this).addClass("hover");
    	}, function(){
    	  $(this).removeClass("hover");
    	});
    }
  }

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    //return  x.toString().substring(0,x.toString().length-3).replace(/\B(?=(\d{2})+(?!\d))/g, ",") + "," + x.toString().substring(x.toString().length-3);
  }

  function money_formatter(x) {
    $(x).each( function() {
          $(this).html($(this).html().replace($(this).text().trim(), numberWithCommas($(this).text().trim())));
      });
  }

  function close_all_bar(){
     var sctimestamp = '1';
     document.cookie="sccloseappdownload="+sctimestamp+"; expires=Thu, 18 Dec 2500 12:00:00 UTC; path=/";
     $('.download-appbar').hide();
  }

  $('.modal-content').on('shown', function() {
      money_formatter(".price");
      money_formatter(".cart-price");
  });

  //document.ready
  $(document).ready(function() {
      /* brand listing */

      /*$(".price").each( function() {
          $(this).html($(this).html().replace($(this).text().trim(), numberWithCommas($(this).text().trim())));
      });*/
      money_formatter(".price");
      money_formatter(".cart-price");

      $('.download-appbar .fa-close').click( function(){
        console.log('sasasasa');
             // var sctimestamp = '1';
             // document.cookie="sccloseappdownload="+sctimestamp+"; expires=Thu, 18 Dec 2500 12:00:00 UTC; path=/";
             // $('.download-appbar').hide();
            });

        //  checkSize();
        //  $(window).resize(checkSize);


          //mobile-swiper-container
      /*  if ($('body').hasClass("device-sm")) {

            $('.grid-items-slider').addClass('mobile-swiper-container');
            $('.grid-items-slider .grid-item').addClass('swiper-slide');
            $('.grid-items-slider .mobile-swiper-wrp').addClass('swiper-wrapper');

            var mobileSlider = new Swiper('.device-sm .mobile-swiper-container', {
                      pagination: '.swiper-pagination',
                    //  slidesPerView: 1,
                      paginationClickable: true,
                      spaceBetween: 0,
                      cssWidthAndHeight: true,
                      slidesPerView:'auto',
                    // nextButton: '.swiper-button-next',
                    // prevButton: '.swiper-button-prev',
                });
          } ///mobile-swiper-container
*/

  var name = 'sccloseappdownload' + "=";
    var showcookieapp = 0;
      var ca = document.cookie.split(';');
      for(var i=0; i<ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0)==' ') c = c.substring(1);
          if (c.indexOf(name) == 0) showcookieapp = c.substring(name.length,c.length);
      }

    if(showcookieapp!=1){
      $('#download-appbar').append('<div class="download-appbar"><div class="container"><a onclick="CheckURL();" href="javascript:void(0);">Download the App and Shop Now </a><i class="fa fa-close" onclick="close_all_bar();"></i></div></div>');
    }

     $("#sc_search").keyup(function(event) {
                event.preventDefault()

                  var search = $("#sc_search").val();
                 //search = search.replace(/[^a-zA-Z0-9]/g, '');

                   if(event.keyCode == 13 && ($("#sc_search").val().trim() !='')) {
                    location.href=sc_baseurl+'all-products?search='+encodeURIComponent(search);
                 }


            });


            $("#sc_search1").keyup(function(event) {
                event.preventDefault()

                  var search = $("#sc_search1").val();
                 //search = search.replace(/[^a-zA-Z0-9]/g, '');

                   if(event.keyCode == 13 && ($("#sc_search1").val().trim() !='')) {
                    location.href=sc_baseurl+'all-products?search='+encodeURIComponent(search);
                 }


            });
            if(!$('body').hasClass('device-sm')){
            $('.sc_search').autocomplete({
              serviceUrl: sc_baseurl+"Search/autosearch",
              lookupLimit: 20,
              dataType: 'json',
              width : '198px',
              type: 'GET',
              appendTo :'.search-input-wrp-md' ,
              transformResult: function(response) {
                 return {
                  suggestions :response,

                  };
               },
                onSelect: function (suggestion) {
                  location.href=sc_baseurl+'all-products?search='+encodeURIComponent(suggestion.data);
                }
          });
          }else{
            $('.sc_search').autocomplete({
              serviceUrl: sc_baseurl+"Search/autosearch",
              lookupLimit: 20,
              dataType: 'json',
              width : '198px',
              type: 'GET',
              appendTo :'.search-input-wrp-sm' ,
              transformResult: function(response) {
                 return {
                  suggestions :response,

                  };
               },
                onSelect: function (suggestion) {
                  location.href=sc_baseurl+'all-products?search='+encodeURIComponent(suggestion.data);
                }
              });
          }


          $('#facebook_login_in').click(function(e) {
            e.preventDefault();
            facebookCommon('signup');
          });
          //facebook Initialization for  signup
          $('#facebook_login').click(function(e) {
            e.preventDefault();
            facebookCommon('login');

          });

  }); ///document.ready

  checkSize();
  $(window).resize(checkSize);

  function checkSize() {
    //var device_width = $('.device-info').width();
      var device_width = $('body').width();
      if (device_width > 768 )
        {
            $('body').addClass('device-md');
            $('body').removeClass('device-sm');
        }
        else
        {
            $('body').addClass('device-sm');
            $('body').removeClass('device-md');
        }
  }
  ///check device width

  /* Sign up start*/
  function facebook_signIn(response,type){
    ga('send', 'pageview', '/overlay/fb-signup/?page=' + document.location.pathname + document.location.search + ' - Signup');
    if(type == 'signup') type= 'sc_signup_error'; else type= 'login_error';

    $.ajax({
      url: sc_baseurl+'fb/via_facebook_signin',
      type: 'post',
      data: {'type' : 'via_facebook_signin', 'response':response },
      success: function(data, status) {
        //console.log(data);
        if(data!=''){
          if(data=='pa'){
            window.location.href=sc_baseurl+"schome/pa";
          }else if(data == 'profile'){
            window.location.href=sc_baseurl+"feed";
          }else{
            $('#sc_signup_error').html("Unexpected error. Please try again!");
          }
        }else{

        }
        event.preventDefault();

      },
      error: function(xhr, desc, err) {
        // console.log(err);
      }
    });

  }

  function google_signIn(){
    ga('send', 'pageview', '/overlay/gplus-signup/?page=' + document.location.pathname + document.location.search + ' - Signup');
    $.ajax({
      url: sc_baseurl+'schome/register_user_google_signin',
      type: 'post',
      data: {'type' : 'get_google_signin' },
      success: function(data, status) {
        if(data!=''){
          if(data=='pa'){
            window.location.href=sc_baseurl+"schome/pa";
          }else if(data == 'feed'){
            window.location.href=sc_baseurl+"feed";
          }else{
            //$('#sc_signup_error').html(data);
            $('#sc_signup_error').html(data);
          }
        }else{

        }

        return false;
        event.preventDefault();

      },
      error: function(xhr, desc, err) {
        // console.log(err);
      }
    });

  }

  function sc_signIn(){

    var sc_firstname = $('#sc_firstname').val();
    var sc_lastname = $('#sc_lastname').val();
    var sc_emailid = $('#sc_emailid').val();
    var sc_password = $('#sc_password').val();
    var sc_mobile = $('#sc_mobile').val();

    //var sc_username = $('#sc_username').val();
    var sc_gender = $('#sc_gender').val();
    $("#sc_signup").addClass( "disabled" );
    $(".fa-loader-wrp-products").css({"display":"inline"});

    if(sc_emailid!='' && sc_password!='' && sc_gender!=''){

      $.ajax({
        url: sc_baseurl+'schome/register_user',
        type: 'post',
        data: {'type' : 'get_signin','sc_email_id':sc_emailid,'sc_password':sc_password, 'sc_gender':sc_gender, 'sc_firstname':sc_firstname, 'sc_lastname':sc_lastname, 'sc_mobile':sc_mobile },
        success: function(data, status) {

          if(data!=''){
            $('#sc_emailid_error').html(data);
            $("#sc_signup").removeClass( "disabled" );
            $(".fa-loader-wrp-products").css({"display":"none"});
          }else{
            window.location.href=sc_baseurl+"schome/pa";
          }

          return false;
          event.preventDefault();

        },
        error: function(xhr, desc, err) {
          // console.log(err);
        }
      });

    }

  }

  function sc_login(){
    var logEmailid = $('#logEmailid').val();
    var logPassword = $('#logPassword').val();
    var logRemember;
    if($('#rememberMe').prop( "checked" ))
    {
      logRemember = '1';
    }else
    {
      logRemember = '0';
    }

    $("#sc_Login").addClass( "disabled" );
    $(".fa-loader-wrp-products").css({"display":"inline"});
    if(logEmailid!='' && logPassword!=''){

      $.ajax({
        url: sc_baseurl+'schome/login',
        type: 'post',
        data: {'type' : 'get_login','logEmailid':logEmailid,'logPassword':logPassword,'logRemember':logRemember },
        success: function(data, status) {

          if(data=='pa'){
            window.location.href=sc_baseurl+"schome/pa";
          }else if(data == 'feed'){
            window.location.href=sc_baseurl+"feed";
          }else{
            $("#sc_Login").removeClass( "disabled" );
            $(".fa-loader-wrp-products").css({"display":"none"});
            $('#login_error').html(data);
          }

          return false;
          event.preventDefault();

        },
        error: function(xhr, desc, err) {
          // console.log(err);
        }
      });

    }
  }
  //function to customise facebook error message for signup and login
  function facebookCommon(type){

    FB.login(function(response) {
      //console.log(response);
      if(response.authResponse) {
        uid = response.authResponse.userID;
        accessToken = response.authResponse.accessToken;
        FB.api('/me?fields=id,about,birthday,location,gender,picture,name,relationship_status,religion,email,first_name,last_name,friends.fields(id,name)', function(response) {
          //console.log(response);
          response = JSON.stringify(response);
          facebook_signIn(response,type);
          ufCheckLike(response);

        });

      }
    },{scope: 'email,user_likes,user_birthday,user_location,user_work_history,user_hometown,user_photos'}); //permissions for facebook
  }
  //facebook Initialization for  login


  function sc_forgotpwd(){

    var Emailid = $('#Emailid').val();

    if(Emailid!=''){

      $.ajax({
        url: sc_baseurl+'schome/forgotPassword',
        type: 'post',
        data: {'type' : 'forgot_pwd','Emailid':Emailid },
        success: function(data, status) {

          $('#loginfp_error').html(data);

        },
        error: function(xhr, desc, err) {
          // console.log(err);
        }
      });
    }
  }

  isLoggedIn = function() {
    if (!accessToken) {
      divLogin.style.display = "";
      return false;
    }
    return true;
  }

  ufCheckLike = function(response1) {
    response = new Array();
    response['personel'] = response1;
    //console.log(response1);
    if (isLoggedIn()) {
      FB.api(
        '/me/likes?' +
        'access_token=' + accessToken,
        'get',
        function(likes) {


          if (likes) {
            response['like']= JSON.stringify(likes);
            //console.log(response);
            ufCheckfriendlist(response);

          }
        }
      );
    }
  }

  ufCheckfriendlist = function(responseAll) {
    var result = new Array();
    result = responseAll;
    if (isLoggedIn())
    {
      FB.api(
        "/me/friends?fields=name,first_name,picture,email&",+
        'access_token=' + accessToken,
        'get',
        function (response) {
          if (response)
          {
            response = JSON.stringify(response);
            console.log(response);
            result['freinds'] = response;
            console.log(result);
          }
        }
      );
    }else
    console.log('notcoming');
  }


  /* Sign up end*/


  /* Add to faverates start */

  function add_to_fav(fav_for, fav_id, user_id,product_price,product_name,cat_id,betaout_event){
    var data = {fav_for : fav_for, fav_id : fav_id, user_id : user_id};
    var url = sc_baseurl+'schome/fav_add';
    $.ajax({
      type: "POST",
      url: url,
      data: data,
      dataType: "json",
      success: function (response) {
          //your success code

          if(response.msg == "Successful Added"){
            $('#wishlist'+fav_id).find('i').addClass('active');
            $('.label-add-to-wishlist').html('Remove from Wish List');
            //$('#wishlist'+fav_id).parents(".user-likes").text('Remove from Wish List');
            console.log(response);
          }else if(response.msg == "Successful deleted"){
            $('#wishlist'+fav_id).find('i').removeClass('active');
            $('.label-add-to-wishlist').html('Add to Wish List');
            console.log(response);
          }
          console.log(response);



      },
      error: function (response) {
          //your error code
          console.log(response);
      }
    });
  }

  /* Add to faverates end */

  // betaout code start
function _beatout_wishlist_product(productid,sc_beatout_user_id,price,product_name,cat_id,betaout_event){
		_bout.push(["activities", {
			"customer_id": sc_beatout_user_id,
			"email": sc_beatout_email_id
		}, {
			"activity_type": betaout_event,
			"products": [{
						"id": productid,
						"sku": productid,
						"name": product_name,
						"price": price,
						"quantity": 1,
						"categories":[{
							"cat_name":"String",
							"cat_id":cat_id,//required
							"parent_cat_id":"String"//optional
							},{
							"cat_name":"String",
							"cat_id":cat_id,//required
							"parent_cat_id":"String"
							}]
					}]

		}]);

}

function _beatout_view_product(productid,sc_beatout_user_id,price,product_name,cat_id,betaout_event){

		betaout_event = 'view';
		_bout.push(["activities", {
			"customer_id": sc_beatout_user_id,
			"email": sc_beatout_email_id
		}, {
			"activity_type": betaout_event,
			"products": [{
						"id": productid,
						"sku": productid,
						"name": product_name,
						"price": price,
						"quantity": 1,
						"categories":[{
							"cat_name":"String",
							"cat_id":cat_id,//required
							"parent_cat_id":"String"//optional
							},{
							"cat_name":"String",
							"cat_id":cat_id,//required
							"parent_cat_id":"String"
							}]
					}]

		}]);

}

function _beatout_share_product(productid,sc_beatout_user_id,price,product_name,cat_id,betaout_event){

		betaout_event = 'share';
		_bout.push(["activities", {
			"customer_id": sc_beatout_user_id,
			"email": sc_beatout_email_id
		}, {
			"activity_type": betaout_event,
			"products": [{
						"id": productid,
						"sku": productid,
						"name": product_name,
						"price": price,
						"quantity": 1,
						"categories":[{
							"cat_name":"String",
							"cat_id":cat_id,//required
							"parent_cat_id":"String"//optional
							},{
							"cat_name":"String",
							"cat_id":cat_id,//required
							"parent_cat_id":"String"
							}]
					}]

		}]);

}
// betaout code end


  /* Brand categories listing  */

  function get_category_brand_list(slug){
    var url = sc_baseurl+"Brands_new/get_brand_category_wise"
    var data = {slug : slug};
     $.ajax({
      type: "POST",
      url: url,
      data: data,
      dataType: "HTML",
      success: function (response) {
          //your success code
          console.log(response);
          $("#brandlist").html(response);
          $('.categories').removeClass('active');
          $('#'+slug).addClass('active');
      },
      error: function (response) {
          //your error code
         console.log(data);
      }
    });
  }
function CheckURL(){

  var OSName="Unknown OS";
  if (navigator.appVersion.indexOf("Win")!=-1) OSName="Windows";
  if (navigator.appVersion.indexOf("Mac")!=-1) OSName="MacOS";
  if (navigator.appVersion.indexOf("X11")!=-1) OSName="UNIX";
  if (navigator.appVersion.indexOf("Linux")!=-1) OSName="Linux";

  if(OSName=="Windows" || OSName=="Linux" || OSName=="Unknown OS") {
    window.open('https://play.google.com/store/apps/details?id=com.stylecracker.android&hl=en?type=individual','_blank');
  }
  else if(OSName=="MacOS") {
    window.open('https://itunes.apple.com/bj/app/stylecracker/id1061463812?mt=8?type=individual','_blank');
  }
}