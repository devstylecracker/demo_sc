var attributesId = ''; var intCtrr = 0;
$( document ).ready(function() {
	var currentState = history.state;
	$('.sc-pagination-wrp').css('display','none');
	$("#sc-filter-box").find("input:checkbox").change(function(e){
		P($(this),0);
	});
	$("#sc-filter-box").find("input:radio").click(function(e){
		P($(this),0);
	});

	$('input[type=radio][name=products-slection]').change(function() {
		$('#is_recommended').val($(this).val());
		P($(this),0);
	});

	$(document).keypress(function(e) {
    	if(e.which == 13) {
       		msg('sasasaa');
    		if (($("#min_price").is(":focus") || $("#max_price").is(":focus"))) {
				if($("#min_price").val() >= 0 && $("#max_price").val() >0){
					if($("#sc-filter-box").length > 0){
						$("#sc-filter-box").find("button").trigger('click');
					}
					if($("#sc-filter-box-brand").length > 0){
						$("#sc-filter-box-brand").find("button").trigger('click');
					}
				}
			}

    	}
	});

	$("#sc-filter-box").find("button").click(function(e){
		$("input:radio").removeAttr("checked");
		$("#sc-filter-box").find("input:radio").each(function(){
			$(this).closest('li').removeClass('active');
		});
		P($(this),1);
	});

	$('#min_price').keyup(function () { 
    	this.value = this.value.replace(/[^0-9\.]/g,'');
	});

	$('#max_price').keyup(function () { 
    	this.value = this.value.replace(/[^0-9\.]/g,'');
	});

	$("#sc-filter-box-brand").find("input:checkbox").change(function(e){
		BP($(this),0);
	});
	/*$("#sc-filter-box-brand").find("input:radio").click(function(e){
		$("input:radio").removeAttr("checked");
		$("#sc-filter-box-brand").find("input:radio").each(function(){
			$(this).closest('li').removeClass('active');
		});
		BP($(this),0);
	});*/

	$("#sc-filter-box-brand").find("input:radio").click(function(e){
		BP($(this),0);
	});

	$("#sc-filter-box-brand").find("button").click(function(e){
		BP($(this),1);
	});

	
	$("#product_sort_by_festive").change(function(e){
		P($('#sc-filter-box'),1);
	});

	$("#product_sort_by_festive1").change(function(e){
		P($('#sc-filter-box'),1);
	});

	$("#product_sort_by_brand").change(function(e){
		BP($('#sc-filter-box-brand'),1);
	});

	$("#product_sort_by_brand1").change(function(e){
		BP($('#sc-filter-box-brand'),1);
	});

	/*$("#recommended-for-you").click(function(e){
		$('#is_recommended').val(1);
		$("#recommended-for-you").addClass('hide');
		$("#remove-recommendation").removeClass('hide');
		P($(this),0);
	});
	$("#remove-recommendation").click(function(e){
		$('#is_recommended').val(0);
		$("#remove-recommendation").addClass('hide');
		$("#recommended-for-you").removeClass('hide');
		P($(this),0);
	});*/

	var track_load = 1;
	var loading  = false; 
	var total_groups = '5000000000';
	$(window).scroll(function() {
		if(Math.round($(window).scrollTop() + $(window).height()) <= Math.round($(document).height()+100)) 
		//user scrolled to bottom of the page?
        {
        	if(track_load <= total_groups && loading==false && $('#filter-selction-box-festive').length>0) //there's more data to load
            {
            	loading = true;
            	$('.fa-loader-wrp-products').show(); //show loading image
            	var sort_by = '';
                sort_by = $('.md-only').css('display') == 'block' ? $('#product_sort_by_festive').val() : $('#product_sort_by_festive1').val();
                //load data from the server using a HTTP POST request
                $.post(sc_baseurl+'all-things-new/get_more_products',{'group_no': $('#load_more_cnt').val(),'cat_id':$('#cat_id').val(),'brandId':$('#filter_brand_id').val(),'product_sort_by_sale':sort_by}, function(data){
                                    
                    $("#get_all_cat_products").append(data); //append received data into the element
                    $("img.lazy").lazyload({
				       // effect : "fadeIn",
				       		event : "sporty",
				       		threshold : 500,
						    placeholder: 'https://www.stylecracker.com/assets/images/logo-graysclae.png'
        			});
        			var timeout = setTimeout(function() { $("img.lazy").trigger("sporty") }, 1000);
                    //hide loading image
                    $('.fa-loader-wrp-products').hide(); //hide loading image once data is received
                    $('#load_more_cnt').val(parseInt($('#load_more_cnt').val())+1);
                    //track_load++; //loaded group increment
                    loading = false; 
                    
                	
                }).fail(function(xhr, ajaxOptions, thrownError) { //any errors?
                    
                    //alert(thrownError); //alert with HTTP error
                    $('.fa-loader-wrp-products').hide(); //hide loading image
                    loading = false;
                
                });
            }
        }
	});


});

function P(id,isButton){
	$('.page-overlay').show();
	$('.fa-loader-wrp').show();
	var intCtr = 0; var brandId = ''; var colorId =''; var lastCatId = ''; var priceId = ''; var sizeId = '';
	
	$('#load_more_cnt').val(1);
	/*brandId = $('#filter_brand_id').val();
	lastCatId = $('#last_cat_id').val();
	priceId = $('#priceId').val();
	sizeId = $('#sizeId').val();*/
	
	var sort_by = '';

    sort_by = $('.md-only').css('display') == 'block' ? $('#product_sort_by_festive').val() : $('#product_sort_by_festive1').val();

	//## Product Attribute Function ---------------------------------------------------------
	var intCtrr = 0; var attributesId = '';
	var objFindAttributesContainer = $(id).closest('div#sc-filter-box').find('.scattributes-filter');

		$(objFindAttributesContainer).find("input:checkbox").each(function(){
			if($(this).is(":checked")){

				var strTypeOfSelection = $(this).closest('div').attr('id');
				var intSelectedOptionIndex = $(this).parent().index();
				var intSelectedOptionLabel = $(this).next().text().trim();
				
				if(!$(this).closest('li').hasClass('checked')){
					$(this).closest('li').addClass('checked');
					AddLabel(strTypeOfSelection, intSelectedOptionIndex,intSelectedOptionLabel);
				} 

			if(intCtrr!=0){ attributesId = attributesId+','; }
			attributesId=attributesId+$(this).attr('value');
			intCtrr++;
			}else {
					$(this).closest('li').removeClass('checked');
				}
		});


		//## Product Brand Function ---------------------------------------------------------
	
		intCtr = 0;
		var objFindAttributesContainer = $(id).closest('div#sc-filter-box').find('#collapse-brands');

		$(objFindAttributesContainer).find("input:checkbox").each(function(){
			var intSelectedOptionLabel;
			if($(this).is(":checked")){

				var strTypeOfSelection = $(this).closest('div').attr('id');
				var intSelectedOptionIndex = $(this).parent().index();
				var intSelectedOptionLabel = $(this).next().text().trim();

				if(!$(this).closest('li').hasClass('checked')){
					$(this).closest('li').addClass('checked');
					AddLabel(strTypeOfSelection, intSelectedOptionIndex,intSelectedOptionLabel);
				} 

			if(intCtr!=0){ brandId = brandId+','; }
			brandId=brandId+$(this).attr('value');
			intCtr++;
			}else {
					$(this).closest('li').removeClass('checked');
				}
		});
	
	intCtr = 0;

	var objFindAttributesContainer = $(id).closest('div#sc-filter-box').find('#collapse-colors');
	
		$(objFindAttributesContainer).find("input:checkbox").each(function(){
			if($(this).is(":checked")){

			if(intCtr!=0){ colorId = colorId+','; }
			colorId=colorId+$(this).attr('value');
			intCtr++;
			}
		});
	
	
	

	//## Product Category Function ---------------------------------------------------------
	intCtr = 0;
	var objFindAttributesContainer = $(id).closest('div#sc-filter-box').find('#collapse-product-category');
		$(objFindAttributesContainer).find("input:checkbox").each(function(){
			if($(this).is(":checked")){
	
			var strTypeOfSelection = $(this).closest('div').attr('id');
			var intSelectedOptionIndex = $(this).parent().index();
			var intSelectedOptionLabel = $(this).next().text();
			var strActiveParent = $(this).parent().parent().closest("li");
				if(!$(this).closest('li').hasClass('checked')){
					$(this).closest('li').addClass('checked');
					AddLabel(strTypeOfSelection, intSelectedOptionIndex,intSelectedOptionLabel);
				} 

			if(intCtr!=0){ lastCatId = lastCatId+','; }
			lastCatId=lastCatId+$(this).attr('value');
			intCtr++;
			}else {
					$(this).closest('li').removeClass('checked');
				}
		});


	//## Product Size Function ---------------------------------------------------------
	intCtr = 0;

	var objFindAttributesContainer = $(id).closest('div#sc-filter-box').find('#collapse-size');

		$(objFindAttributesContainer).find("input:checkbox").each(function(){
			if($(this).is(":checked")){

			var strTypeOfSelection = $(this).closest('div').attr('id');
			var intSelectedOptionIndex = $(this).parent().index();
			var intSelectedOptionLabel = $(this).next().text();

			if(!$(this).closest('li').hasClass('checked')){
					$(this).closest('li').addClass('checked');
					AddLabel(strTypeOfSelection, intSelectedOptionIndex,intSelectedOptionLabel);
				} 

			if(intCtr!=0){ sizeId = sizeId+','; }
			sizeId=sizeId+$(this).attr('value');
			intCtr++;
			}else {
					$(this).closest('li').removeClass('checked');
				}
		});
	
	//## Product Price Function ---------------------------------------------------------
	var objFindAttributesContainer = $(id).closest('div#sc-filter-box').find('#collapse-price');
		
		$(objFindAttributesContainer).find("input:radio").each(function(){
			if($(this).is(":checked")){
				priceId = $(this).attr('value');
			}
		});
	

	$('#filter_brand_id').val(brandId);
	$('#filter_color_id').val(colorId);
	$('#last_cat_id').val(lastCatId);
	$('#priceId').val(priceId);
	$('#isButton').val(isButton);
	$('#attributesId').val(attributesId);
	$('#sizeId').val(sizeId);

	
	$.post(sc_baseurl+'all-things-new/filter_products',{'brandId': brandId,'cat_id':$('#cat_id').val(),'product_sort_by_sale':sort_by}, 
		function(data){
			$("#get_all_cat_products").html(data);
			$("img.lazy").lazyload({
				       // effect : "fadeIn",
				       		event : "sporty",
				       		threshold : 500,
						    placeholder: 'https://www.stylecracker.com/assets/images/logo-graysclae.png'
        			});
        			var timeout = setTimeout(function() { $("img.lazy").trigger("sporty") }, 1000);

        	$('.page-overlay').hide();
			$('.fa-loader-wrp').hide();
		}).fail(function(xhr, ajaxOptions, thrownError) { //any errors?
                    
		//alert(thrownError); //alert with HTTP error
       	$('.page-overlay').css('display','none');
		$('.fa-loader-wrp').css('display','none');
        });
	


	if(!$(id).closest('li').hasClass('checked')){
		RemoveLabelFromFilter($(id).next().text().trim());

		//$('#attributesId').val('');
		//$('#filter_brand_id').val('');
		//$('#last_cat_id').val('');

	}
	

}

function msg(m) {console.log(m);}
function AddLabel(type,id,label){
	
	$('#filter-selction-box-elas').append('<span class="filter-tag" data-attr="'+label.trim()+'"><span>'+label+'</span><span onclick="RemoveLabel('+id+',\''+type+'\',this);" class="remove"></span></span>');
	 
}



function RemoveLabel(id,type,container){
	//## Condition if the selection on product category, or else ---------------------
	if(type =="collapse-product-category"){
		$('#'+type).find('li.active').find('ul li').eq(id).find('input').trigger('click');
	} else {
		$('#'+type).find('li').eq(id).find('input').trigger('click');
		$(container).parent().remove();
	}
	$(container).parent().remove();
}

function RemoveLabelFromFilter(id) {

	$('#filter-selction-box-elas .filter-tag').each(function(e){

		if($(this).attr('data-attr')==id){
			$(this).remove();
		}
	});
	//$('#fil-'+id).remove();
}

