var cache = new ajaxCache('GET', true, 1800000);
var stylist_offset,street_offset;

$(function() {
var stylist_id = $('#stylist-id').val();
var page_name = $('#page_name').val();
      var all_offset = "stylist_offset=0&street_offset=0&filter=0&page_type="+page_name+"&page_type_id="+stylist_id;

      get_looks(all_offset);

      $( "#select_type" ).change(function() {
          $('#show_looks').html('');
          $('#show_looks').html('<div class="grid-sizer"></div>');

          all_offset = "stylist_offset=0&street_offset=0&page_type="+page_name+"&page_type_id="+stylist_id+"&filter="+$('#select_type').val();

          get_looks(all_offset);
      });

      $( window ).load( function(){
        //## UserTrack: Page ID=========================================
        //document.cookie="pid=2";
        /*Code ends*/
        $('.grid-items-wrp').masonry({
            // set itemSelector so .grid-sizer is not used in layout
            itemSelector: '.grid-item',
            // use element for option
            // columnWidth: '.grid-sizer',
            percentPosition: true
        });
      });
});

      var isTriggered = false;
          $(window).scroll(function() {
          var intTriggerScroll = 50;
          var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());
          var stylist_id = $('#stylist-id').val();
          var page_name = $('#page_name').val();
          if(intCurrentScrollPos<intTriggerScroll && !isTriggered){


          if($('#stylist_offset').length > 0){
            stylist_offset = $('#stylist_offset').val();
          }else{
            stylist_offset = '-1';
          }

          if($('#street_offset').length > 0){
            street_offset = $('#street_offset').val();
          }else{
            street_offset = '-1';
          }

           all_offset = "stylist_offset="+stylist_offset+"&street_offset="+street_offset+"&page_type="+page_name+"&page_type_id="+stylist_id+"&filter="+$('#select_type').val();
           isTriggered=true;

           if($('#stylist_offset').length >0 || $('#street_offset').length >0){
              get_looks(all_offset);
           }
          }

      });

function update_website(response){

  if($('#stylist_offset').length >0 ){ $('#stylist_offset').remove(); }
  if($('#street_offset').length >0 ){ $('#street_offset').remove(); }

  $('#show_looks').append(response);
  $('#look_loader_img').hide();
  isTriggered = false;

}

 function initialiceMasonry(){
          var $container = $('.grid-items-wrp');
            $container.masonry({
                isInitLayout : true,
                itemSelector: '.grid-item',
                isAnimated: false,
                percentPosition: true

            });

            $container.imagesLoaded(function() {
              $( $container ).masonry( 'reloadItems' );
              $( $container ).masonry( 'layout' );
            });
          }