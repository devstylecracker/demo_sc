/*=============================================*/
function msg(m) { console.log(m); }

var intCurrentTypeID = 0;
var strFirstTime = "true";
var intCtrs = 0 ;

var intC = 0;
function PAInitialize1(id, strFirstTime, currentSelecteValue){
  intCurrentTypeID = id;
  var objPASections = $('#pa-men-slider .pa-selection-wrp li');
  var objPASectionContent = $('#pa-men-slider .slide');
  $(objPASectionContent).hide();

  //##Checking the user is first time or not and PA TABS============================
  if(strFirstTime=='true'){
    $(objPASections).addClass('disabled');
    $(objPASections).eq(intCurrentTypeID).addClass('active').removeClass('disabled');
    $(objPASectionContent).eq(0).show();
    msg('FIRST TIME USER.');
  }
  else {
    if(intCurrentTypeID>=0){

      //## Selection of TAB ==========================

      for(var i=0;i<=intCurrentTypeID;i++){

        $(objPASections).eq(i).removeClass('disabled').removeClass('active');

      }

      $(objPASections).eq(intCurrentTypeID).addClass('active');



      //## Selection of TAB Content ==================

      var objTabContent = $('#slide'+(intCurrentTypeID+1));

      $(objTabContent).show();



      //## Need to Highlight the selected item =======

      $(objTabContent).find('.row .item-wrp').each(function(){

        if($(this).attr('data-attr')==currentSelecteValue){

          $(this).addClass('selected');

        }

      });



      msg('SECOND TIME USER.');

    }

  }



  //## SHOW TAB CONTENT ==============================

  var intSlide = Number(intCurrentTypeID)+1;

  $('#slide'+intSlide).show();

}



$(document).ready(function(){

//## PA TAB CLICKS =============================================================
var objPASections = $('#pa-men-slider .pa-selection-wrp li');
  $(objPASections).on('click touch', function(e){

    if(!$(this).hasClass('disabled')){

      $(objPASections).removeClass('active');

      $(this).addClass('active');

      //

      $('#slide1,#slide2,#slide3,#slide4,#slide5,#slide6,#slide7').css('display','none');

      intCurrentTypeID = $(this).index();

      $('#slide'+(intCurrentTypeID+1)).show();

      msg('intCurrentTypeID: '+intCurrentTypeID);

    }

  });



//## PA TAB CONTENT CLICKS =======================================================

  var objPASections = $('#pa-men-slider .pa-selection-wrp li');

  var intSlide = Number(intCurrentTypeID)+1;

  var objPASectionContentTabs = $('#pa-men-slider .slide .row .item-wrp');



  $('.finish').on('click touch', function(e){
    var question_0 = $("#quotient_0").val();
    var question_1 = $("#quotient_1").val();
    var question_2 = $("#quotient_2").val();
    var question_3 = $("#quotient_3").val();
    var question_4 = $("#quotient_4").val();
    msg('intCurrentTypeID: '+question_3);

    $.ajax({

        url: baseUrl+'schome/update_pa_man_finish',

        type: 'POST',

        data: { 'type' : 'update_pa','question_0':question_0,'question_1':question_1, 'question_2':question_2, 'question_3':question_3, 'question_4':question_4 },

        cache :true,

        success: function(response) {
          msg(response);
          if(response == 1){

            intCtrs = parseInt(intCurrentTypeID)+1;

            if(localStorage.getItem("edit_pa") == 'yes'){

              localStorage.setItem("edit_pa", "no");

              window.location.href=baseUrl+"profile";

            }else{

              PAInitialize1(intCtrs,'false','');

            }



          }else if(response == 'feed'){
            ga('send', 'event', 'PA-Men', 'completed');

            window.location.href=baseUrl+"feed";

          }

        },



          error: function(xhr) {

        }

    });

  });


  $(objPASectionContentTabs).on('click touch', function(e){

    $(this).parents(".slide").find('.item-wrp').removeClass("selected");
    $(this).addClass('selected');
    currentSelecteValue = $(this).attr('data-attr');
	property_name1 = $(this).attr('data-attr2');
	answer = $(this).attr('data-attr3');
	// _beatout_update_property(property_name1,currentSelecteValue,answer);
    //## PHP FUNCTION TO SAVE

    //msg('intCurrentTypeID: '+intCurrentTypeID+"  :: currentSelecteValue: "+currentSelecteValue);

     $.ajax({

        url: baseUrl+'schome/update_pa_man',

        type: 'POST',

        data: { 'type' : 'update_pa','intCurrentTypeID':intCurrentTypeID,'currentSelecteValue':currentSelecteValue  },

        cache :true,

        success: function(response) {

          if(response == 1){

            intCtrs = parseInt(intCurrentTypeID)+1;

            if(localStorage.getItem("edit_pa") == 'yes'){

              localStorage.setItem("edit_pa", "no");

              window.location.href=baseUrl+"profile";

            }else{

              PAInitialize1(intCtrs,'false','');

            }



          }else if(response == 'feed'){
            ga('send', 'event', 'PA-Men', 'completed');

            window.location.href=baseUrl+"feed";

          }

        },

          error: function(xhr) {

        }

    });



 });
 
 function _beatout_update_property(property,property_value1,answer){
	 if(property==1){
		 _bout.push(["properties", {
							"customer_id": sc_beatout_user_id,
							"email": sc_beatout_email_id
						}, {
							'update': {
								"body_type": answer,
							}
						}
					]);
	 }else if(property==2){
		 _bout.push(["properties", {
							"customer_id": sc_beatout_user_id,
							"email": sc_beatout_email_id
						}, {
							'update': {
								"body_height": answer,
							}
						}
					]);
	 }else if(property==3){
		 _bout.push(["properties", {
							"customer_id": sc_beatout_user_id,
							"email": sc_beatout_email_id
						}, {
							'update': {
								"work_style": answer,
							}
						}
					]);
	 }else if(property==4){
		 _bout.push(["properties", {
							"customer_id": sc_beatout_user_id,
							"email": sc_beatout_email_id
						}, {
							'update': {
								"personal_style": answer,
							}
						}
					]);
	 }else if(property==5){
		 _bout.push(["properties", {
							"customer_id": sc_beatout_user_id,
							"email": sc_beatout_email_id
						}, {
							'update': {
								"user_age": answer,
							}
						}
					]);
	 }else if(property==6){
		 _bout.push(["properties", {
							"customer_id": sc_beatout_user_id,
							"email": sc_beatout_email_id
						}, {
							'update': {
								"user_budget": answer,
							}
						}
					]);
	 }
	 
 }
 
//range slider
$('.range-slider-wrp .range-slider').change( function(){
  var rangeVal= Number($(this).val()) + 1;
  $(this).next().find('li').removeClass('active');
  $(this).next().find('li:nth-child(' + rangeVal + ')').addClass('active');
})

$('.range-slider-labels li').click( function(){
 var index = $(this).index();
 var index = $(this).index();

 $(this).parents('.range-slider-wrp').find('input').val(index);
 $(this).siblings().removeClass('active');
 $(this).addClass('active');
})

///range slider



});
