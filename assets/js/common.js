$('.sc-pagination-wrp').hide();
$('.fa-loader-wrp-products').hide();
$('#clear_all').hide();
var isTriggered = false;
var intTriggerScroll = 50;
/* Category filter Page*/
$(window).scroll(function(){
  
//if ($('#get_all_cat_products').length>0 && $('#filter-selction-box1').length<=0){
 // if  ($(window).scrollTop() == $(document).height() - $(window).height()){

      //get_more_products();
 // }
//}

//if ($('#get_brand_cat_products').length){
	//get_more_cat_product();
//}

if ($('#get_all_looks').length){
  //if($(window).scrollTop() == $(document).height() - $(window).height()){
    get_more_looks();
  //}
}

if ($('#get_all_brands_looks').length){
  //if  ($(window).scrollTop() == $(document).height() - $(window).height()){
      get_more_brands_looks();
  //}
}
if ($('#get_all_stylist_looks').length){
  //if  ($(window).scrollTop() == $(document).height() - $(window).height()){
      get_all_stylist_looks();
  //}
}
if ($('#get_wishlist_looks').length){
  if  ($(window).scrollTop() == $(document).height() - $(window).height()){
      get_wishlist_looks();
  }
}
if ($('#get_all_category_wise_looks').length){

  //if($(window).scrollTop() == $(document).height() - $(window).height()){
    get_all_category_wise_looks();
  //}
}
});

$( document ).ready(function() {
  $('#look_filter_type').change(function(){
    get_filtered_look();
  });
  //$('#product_sort_by').change(function(){
   // $('#product_sort_by1').val($('#product_sort_by').val());
   // show_products();
  //});

  //$('#product_sort_by1').change(function(){
  //  $('#product_sort_by').val($('#product_sort_by1').val());
  //  show_products();
 // });

  /*$("#min_price").keypress(function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
              //$( "#go_button" ).prop( "disabled", true );
              alert('Only digits are allow');
            return false;
          }else{
            $( "#go_button" ).prop( "disabled", false );
          }
          });*/
      
       /*  $("#max_price").keypress(function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
             // $( "#go_button" ).prop( "disabled", true );
              alert('Only digits are allow');
            return false;
          }else{
            $( "#go_button" ).prop( "disabled", false );
          }
          });*/
      
         $("#chkwomen").click(function() {
          if($(this).is(":checked")) {
            $(this).closest('span').addClass('active');
          }else{
            $(this).closest('span').removeClass('active');
          }
         });
      
         $("#chkmen").click(function() {
          if($(this).is(":checked")) {
           $(this).closest('span').addClass('active');
          }else{
            $(this).closest('span').removeClass('active');
          }
        });

          $('#price_flt').change(function(){
          get_all_category_wise_filterred_looks();
        });

          $('#price_flt_for_products').change(function(){
            sort_cat_products();
          });
         
});
function sort_cat_products(){
  var ascending = $('#price_flt_for_products').val();
  var sorted = $('.listing-item').sort(function(a,b){
    return (ascending ==(parseInt($(a).data("listing-price"))) < parseInt($(b).data("listing-price"))) ? 1 : -1;
  });

  ascending = ascending ? 0 : 1;

  $('#kvlist').html(sorted);

}
function deselect_menu(){
  $('.filterbar a label').removeClass('active');
}
function get_more_brands_looks(){
  var intTriggerScroll = 50;
    //var isTriggered = false;
  var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());

  if(intCurrentScrollPos<intTriggerScroll && !isTriggered){
  isTriggered=true;
  $('.fa-loader-wrp-products').css('display', 'inline-block');

  var offset = $('#offset').val();
  var brand_username = $('#brand_username').val();
  var total_looks = $('#total_looks').val();
  if(parseInt(offset)+parseInt(1)*parseInt(12) < total_looks){
     $.ajax({
              url: sc_baseurl+'brands/get_more_looks',
              type: 'post',
              data: { 'offset' : offset, 'brand_username':brand_username },
              success: function(data, status) {
                $('#get_all_brands_looks').append(data);
                

                $('#offset').val(parseInt(offset)+parseInt(1));
                $('.fa-loader-wrp-products').hide();
                load_more_hover();

              }
          });
   }
   }else{
    $('.fa-loader-wrp-products').hide();
  }
}
function get_more_looks(){ 
    //$('.fa-loader-wrp-products').showInlineBlock();
    var intTriggerScroll = 50;
    //var isTriggered = false;
    var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());

    if(intCurrentScrollPos<intTriggerScroll && !isTriggered){
    isTriggered=true;
    $('.fa-loader-wrp-products').css('display', 'inline-block');
    var offset = $('#offset').val();
    var look_type = $('#look_type').val();
    var total_looks = $('#total_looks').val();
    var look_filter_type = $('#look_filter_type').val();
    var new_offset = parseInt(offset)+parseInt(1);
          if(offset*12<total_looks){
           $.ajax({
              url: sc_baseurl+'home/get_more_looks',
              type: 'post',
              data: { 'offset' : new_offset, 'look_type':look_type,'look_filter_type':look_filter_type },
              success: function(data, status) {
                $('#get_all_looks').append(data);
                isTriggered=false;
                $('#offset').val(new_offset);
                $('.fa-loader-wrp-products').hide();
                

              }
          });
        }
  }else{
    $('.fa-loader-wrp-products').hide();
  }

}

function get_all_stylist_looks(){
   var intTriggerScroll = 50;
    //var isTriggered = false;
    var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());

    if(intCurrentScrollPos<intTriggerScroll && !isTriggered){
    isTriggered=true;
    $('.fa-loader-wrp-products').css('display', 'inline-block');
    var stylist_id = $('#stylist-id').val();
    var offset = parseInt($('#offset').val())+1;
       $.ajax({
              url: sc_baseurl+'stylist/get_all_stylist_looks',
              type: 'post',
              data: { 'offset' : offset, 'stylist_id':stylist_id },
              success: function(data, status) {
                $('#get_all_stylist_looks').append(data);
                

                $('#offset').val(offset);
                $('.fa-loader-wrp-products').hide();
                load_more_hover();

              }
          });
       }else{
    $('.fa-loader-wrp-products').hide();
  }
}

function get_all_category_wise_looks(){
      //$('.fa-loader-wrp-products').showInlineBlock();
    var intTriggerScroll = 50;
    //var isTriggered = false;
    var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());

    if(intCurrentScrollPos<intTriggerScroll && !isTriggered){
    isTriggered=true;

    $('.fa-loader-wrp-products').css('display', 'inline-block');
    var offset = $('#offset').val();
    var total_looks = $('#total_looks').val();
    var price_flt = $('#price_flt').val();
    var new_offset = parseInt(offset)+parseInt(1);
          if(offset*9<total_looks){
           $.ajax({
              url: sc_baseurl+'home/get_all_category_wise_looks',
              type: 'post',
              data: { 'offset' : new_offset,'price_flt':price_flt },
              success: function(data, status) {
                $('#get_all_category_wise_looks').append(data);
                isTriggered=false;
                $('#offset').val(new_offset);
                $('.fa-loader-wrp-products').hide();


              }
          });
        }
  }else{
    $('.fa-loader-wrp-products').hide();
  }
}

function get_all_category_wise_filterred_looks(){
    
    $('.fa-loader-wrp-products').css('display', 'inline-block');
    var offset = 0;
    var total_looks = $('#total_looks').val();
    var price_flt = $('#price_flt').val();
    var new_offset = parseInt(offset)+parseInt(1);
          if(offset*9<total_looks){
           $.ajax({
              url: sc_baseurl+'home/get_all_category_wise_looks',
              type: 'post',
              data: { 'offset' : offset,'price_flt':price_flt },
              success: function(data, status) {
                $('#get_all_category_wise_looks').html(data);
                isTriggered=false;
                $('#offset').val(new_offset);
                $('.fa-loader-wrp-products').hide();


              }
          });
        }
}

function show_products(){
   
    $('.fa-loader-wrp-products').css('display', 'inline-block');
   $('html,body').animate({
            scrollTop: 0
        }, 700);
  var brandIds = ""; var i =0 ; var j = 0; var colorIds = "";
  $('#brands_nav').html('');
    $("input[name='brand_name[]']:checked").each(function ()
      { $(this).parent('label').parent('li').addClass('active');
        if(i !=0 ){ brandIds = brandIds+','; }
          brandIds = brandIds + parseInt($(this).val());
          //$('#clear_all').show();
          $('#clear_all').showInlineBlock();
          $('#filter-selction-box').show();
          $('#brands_nav').append('<span class="filter-tag brands_nav'+parseInt($(this).val())+'">'+$(this).attr('label-for')+'<span class="remove" onclick="uncheck_brand('+parseInt($(this).val())+');"></span></span>');
          i++;
      });

    $("input[name='brand_name[]']:unchecked").each(function (){
      $(this).parent('label').parent('li').removeClass('active');
    });

    if(i > 0){ $('#brand_clear').show(); }else{ $('#brand_clear').hide(); }
    $('#color_nav').html('');
    $("input[name='color_name[]']:checked").each(function ()
      { $(this).parent('label').parent('li').addClass('active');
        if(j !=0 ){colorIds = colorIds+','; }
          colorIds = colorIds + parseInt($(this).val());
           //$('#clear_all').show();
           $('#clear_all').showInlineBlock();
           $('#filter-selction-box').show();
          $('#color_nav').append('<span class="filter-tag color_nav'+parseInt($(this).val())+'">'+$(this).attr('label-for')+'<span class="remove" onclick="uncheck_color('+parseInt($(this).val())+');"></span></span>');
          j++;
      });

    $("input[name='color_name[]']:unchecked").each(function (){
      $(this).parent('label').parent('li').removeClass('active');
    });

    if(j > 0){ $('#color_clear').show(); }else{ $('#color_clear').hide(); }

  var sc_offers = $('input[name=sc_offers]:checked').val();
  if(sc_offers > 0) {
    $('#offers_clear').show();

    $('#offers_nav').remove();
  //  $('#clear_all').show();
    $('#clear_all').showInlineBlock();
    $('#filter-selction-box').show();
    $('#filter-selction-box').append('<span class="filter-tag" id="offers_nav">'+$('input[name=sc_offers]:checked').attr('label-for')+'<span class="remove" onclick="clear_offers();"></span></span>');
  }else{ $('#offers_clear').hide(); $('#offers_nav').remove(); }

  var price_filter = $('input[name=price_filter]:checked').val();
  var min_price = $('#min_price').val();
  var max_price = $('#max_price').val();
  if(min_price>0 || max_price>0){ price_filter = 0; $('#price_nav').remove();

    $('#filter-selction-box').append('<span class="filter-tag" id="price_nav">'+min_price+' to '+max_price+'<span class="remove" onclick="clear_price();"></span></span>');
  }else{ $('#price_nav').remove();

     $('#filter-selction-box').append('<span class="filter-tag" id="price_nav">'+$('input[name=price_filter]:checked').attr('label-for')+'<span class="remove" onclick="clear_price();"></span></span>');
  }

  if(price_filter > 0 || (min_price>0 && max_price>0)) { $('#price_clear').show(); }else{ $('#price_clear').hide(); $('#price_nav').remove(); }

  var discount = $('input[name=discount]:checked').val();

  if(discount > 0) {
      $('#discount_clear').show();
      $('#discount_nav').remove();

      $('#filter-selction-box').append('<span class="filter-tag" id="discount_nav">'+$('input[name=discount]:checked').attr('label-for')+'<span class="remove" onclick="clear_discount();"></span></span>');
    }else{
      $('#discount_clear').hide();
      $('#discount_nav').remove();
    }

  var scgender = $('#scgender').val();
  var sccategory_level = $('#sccategory_level').val();
  var sccategory_slug = $('#sccategory_slug').val();
  var product_sort_by = $('#product_sort_by').length > 0 ? $('#product_sort_by').val() : $('#product_sort_by1').val();
  var universal_search = $('#universal_search').val();

  $.ajax({
    url: sc_baseurl+'all-products/filter_products',
    type: 'post',
    data: { 'brandIds':brandIds,'colorIds':colorIds,'sc_offers':sc_offers,'price_filter':price_filter,'min_price':min_price,'max_price':max_price,'discount':discount,'scgender':scgender,'sccategory_level':sccategory_level,'sccategory_slug':sccategory_slug,'product_sort_by':product_sort_by,'universal_search':universal_search },
    success: function(data, status) {
      $('#get_all_cat_products').html(data);
      if($('#products_total').val() == 1){ $('#filter-result-title').html($('#products_total').val()+' Product'); }
      else if($('#products_total').val() > 1){
      $('#filter-result-title').html($('#products_total').val()+' Products');
      }else{
      $('#filter-result-title').html('0 Product');
      }
      $('#offset').val(2);

    
      $('.fa-loader-wrp-products').hide();
      load_more_hover();
	   $("img.lazy").lazyload({
               // effect : "fadeIn",
                  event : "sporty",
                  threshold : 500,
                placeholder: 'https://www.stylecracker.com/assets/images/logo-graysclae.png'
              });
              var timeout = setTimeout(function() { $("img.lazy").trigger("sporty") }, 1000);
	  

    }
  });
  money_formatter(".price");
  money_formatter(".cart-price");

}

function get_more_cat_product(){
	
	var intTriggerScroll = 50;
    //var isTriggered = false;
    var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());
	var total_product = 100;
    if(intCurrentScrollPos<intTriggerScroll && !isTriggered){
    isTriggered=true;
    $('.fa-loader-wrp-products').css('display', 'inline-block');
    var offset = $('#offset').val();
	var category_slug = $('#category_slug').val();
	var brand_name = $('#brand_name').val();
	
    var new_offset = parseInt(offset)+parseInt(1);
          if(offset*12<total_product){
           $.ajax({
              url: sc_baseurl+'Brand_category/get_more_cat_product',
              type: 'post',
              data: { 'offset' : new_offset, category_slug : category_slug, brand_name : brand_name },
              success: function(data, status) {
                $('#get_brand_cat_products').append(data);
                isTriggered=false;
                $('#offset').val(new_offset);
					$('.fa-loader-wrp-products').hide();
					isTriggered=false;
					load_more_hover();

					$("img.lazy").lazyload({
					  //effect : "fadeIn",
					  threshold : 300,
         placeholder: 'https://www.stylecracker.com/assets/images/logo-graysclae.png'
					});

              }
          });
        }
  }
	
}

function get_more_products(){
  $('.fa-loader-wrp-products').css('display', 'inline-block');
  //$('.fa-loader-wrp-products').showInlineBlock();
    var intTriggerScroll = 50;
    //var isTriggered = false;
    var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());
    
    if(intCurrentScrollPos<intTriggerScroll && !isTriggered){
    isTriggered=true;

  var brandIds = ""; var i =0 ; var j = 0; var colorIds = "";
  $("input[name='brand_name[]']:checked").each(function ()
    {
      if(i !=0 ){brandIds = brandIds+','; }
        brandIds = brandIds + parseInt($(this).val());
        i++;
    });
  if(i > 0){ $('#brand_clear').show(); }else{ $('#brand_clear').hide(); }

   $("input[name='color_name[]']:checked").each(function ()
    {
      if(j !=0 ){colorIds = colorIds+','; }
        colorIds = colorIds + parseInt($(this).val());
        j++;
    });
   if(j > 0){ $('#color_clear').show(); }else{ $('#color_clear').hide(); }

  var offset = $('#offset').val();
  var sc_offers = $('input[name=sc_offers]:checked').val();
  var price_filter = $('input[name=price_filter]:checked').val();
  var min_price = $('#min_price').val();
  var max_price = $('#max_price').val();
  if(min_price>0 || max_price>0){ price_filter = 0; }
  var discount = $('input[name=discount]:checked').val();
  var scgender = $('#scgender').val();
  var sccategory_level = $('#sccategory_level').val();
  var sccategory_slug = $('#sccategory_slug').val();
  var product_sort_by = $('#product_sort_by').length > 0 ? $('#product_sort_by').val() : $('#product_sort_by1').val();
  var universal_search = $('#universal_search').val();

  if(parseInt(offset)+parseInt(1)*12 < $('#products_total').val()) {
	  $.ajax({
	    url: sc_baseurl+'all-products/get_more_products',
	    type: 'post',
	    data: { 'offset':offset,'brandIds':brandIds,'colorIds':colorIds,'sc_offers':sc_offers,'price_filter':price_filter,'min_price':min_price,'max_price':max_price,'discount':discount,'scgender':scgender,'sccategory_level':sccategory_level,'sccategory_slug':sccategory_slug,'product_sort_by':product_sort_by,'universal_search':universal_search },
	    success: function(data, status) {
        if(data!=''){
	      $('#get_all_cat_products').append(data);
	      if($('#products_total').val() == 1){ $('#filter-result-title').html($('#products_total').val()+' Product'); }
	      else if($('#products_total').val() > 1){
	      $('#filter-result-title').html($('#products_total').val()+' Products');
	      }else{
	      $('#filter-result-title').html('0 Product');
	      }
        console.log(parseInt(offset)+parseInt(1));
	      $('#offset').val(parseInt(offset)+parseInt(1));
	    //  initialiceMasonry('get_all_cat_products');
        $('.fa-loader-wrp-products').hide();
        isTriggered=false;
        load_more_hover();
         $("img.lazy").lazyload({
          //effect : "fadeIn",
          threshold : 300,
         placeholder: 'https://www.stylecracker.com/assets/images/logo-graysclae.png'
        });
         }else{
          $('.fa-loader-wrp-products').hide();
          isTriggered=false;
         }
	    }
	  });
	}
}
}
function initialiceMasonry(id){
    var $container = $('#'+id);
        $container.masonry({
            isInitLayout : true,
            itemSelector: '.grid-item',
          transitionDuration: 0,
            percentPosition: true

        });

        $container.imagesLoaded(function() {
          $( $container ).masonry( 'reloadItems' );
          $( $container ).masonry( 'layout' );
        });
}
function showClear(){}

function _categorytargetTweet(loc,response,title,look_id){

window.open('http://twitter.com/share?url=' + response + '&text=' + title + '&', 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');


}

function _categorytargetPinterest(loc,title,look_id,img_src){
  window.open('https://www.pinterest.com/pin/create/button/?url=' + loc + '&description=' + title + '&media=' + img_src + '&', 'pinterestwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
}

function _categorytargetFacebookShare(lname,llink,lpic,look_id){

  FB.ui(
        {
          method: 'feed',
          name: lname,
          link: llink,
          picture: lpic,
          //caption: 'This is the content of the "caption" field.',
          description: "Get yourself a Personal Stylist on Stylecracker.com. Our experts will create custom-made fashionable looks for you, keeping your personal preferences and budget in mind. What's more? With our 'Buy Now' feature, you can instantly shop the recommendations we make!",
          //message: ""
          },
          function(response) {
            if (response && response.post_id) {

            } else {
              //alert('Post was not published.');
            }
          }
        );
}


function _brandtargetTweet(loc,response,title,look_id){

window.open('http://twitter.com/share?url=' + response + '&text=' + title + '&', 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');


}

function _brandtargetPinterest(loc,title,look_id,img_src){
  window.open('https://www.pinterest.com/pin/create/button/?url=' + loc + '&description=' + title + '&media=' + img_src + '&', 'pinterestwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
}

function _brandtargetFacebookShare(lname,llink,lpic,look_id){

  FB.ui(
        {
          method: 'feed',
          name: lname,
          link: llink,
          picture: lpic,
          //caption: 'This is the content of the "caption" field.',
          description: "Get yourself a Personal Stylist on Stylecracker.com. Our experts will create custom-made fashionable looks for you, keeping your personal preferences and budget in mind. What's more? With our 'Buy Now' feature, you can instantly shop the recommendations we make!",
          //message: ""
          },
          function(response) {
            if (response && response.post_id) {

            } else {
              //alert('Post was not published.');
            }
          }
        );
}


function brand_clear(){
  $("input[name='brand_name[]']:checked").each(function (){$(this).attr('checked',false);});
  $('#brand_clear').hide();
  show_products();
}

function color_clear(){
  $("input[name='color_name[]']:checked").each(function (){$(this).attr('checked',false);});
  $('#color_clear').hide();
  show_products();
}

function clear_offers(){
  $('input[name=sc_offers]').removeAttr("checked");
  $('#offers_clear').hide();
  $('#offers_nav').remove();
  show_products();
}

function clear_price(){
  $('input[name=price_filter]').removeAttr("checked");
  $('#min_price').val('');
  $('#max_price').val('');
  $('#price_clear').hide();
  $('#price_nav').remove();
  show_products();
}

function clear_discount(){
  $('input[name=discount]').removeAttr("checked");
  $('#discount_clear').hide();
  show_products();
}

function uncheck_brand(id){
  $("input[name='brand_name[]']:checked").each(function (){
    if($(this).val() == id){
      $(this).attr('checked',false);
      $('.brands_nav'+id).remove();
      show_products();
    }
  });
}

function uncheck_color(id){
  $("input[name='color_name[]']:checked").each(function (){
    if($(this).val() == id){
      $(this).attr('checked',false);
      $('.color_nav'+id).remove();
      show_products();
    }
  });
}

function openSizeGuide(id,product_id,brand_id){

    $.ajax({
      url: sc_baseurl+'product/get_size_guide',
      type: 'post',
      data: { 'product_id' : product_id, 'brand_id':brand_id,'size_guide_id':id },
      success: function(data, status) {
        $('#product_size_guide').attr('src',data);
        $('#mysize').modal({show: true});
        }
    });
}

function add_to_wishlist(look_id){
  var onclick_str = $('#whislist'+look_id).attr("onclick");
  onclick_str = onclick_str.substr(onclick_str.indexOf(";")+1)
  msg(onclick_str);
  $('#whislist'+look_id).attr("onclick","remove_from_wishlist("+look_id+");"+onclick_str);
  $('#whislist'+look_id).attr("title","Remove from Wish List");
  $('#whislist'+look_id).find('i').addClass('active');
  //$('#wishlist'+look_id).parents(".user-likes").text('Remove from Wish List');

    $.ajax({
      url: sc_baseurl+"profile/add_to_wishlist",
      type: 'POST',
      data: {'look_id':look_id},
      cache :true,
      success: function(response) {

        $('.label-add-to-wishlist').html('Remove from Wish List');

      },
      error: function(xhr) {

      }
    });

}

function remove_from_wishlist(look_id){
  var onclick_str = $('#whislist'+look_id).attr("onclick");
  onclick_str = onclick_str.substr(onclick_str.indexOf(";")+1)
  msg(onclick_str);
  $('#whislist'+look_id).attr("onclick","add_to_wishlist("+look_id+");"+onclick_str);
  $('#whislist'+look_id).attr("title","Add to Wish List");
  $('#whislist'+look_id).find('i').removeClass('active');


    $.ajax({
      url: sc_baseurl+"profile/remove_from_wishlist",
      type: 'POST',
      data: {'look_id':look_id},
      cache :true,
      success: function(response) {

        $('.label-add-to-wishlist').html('Add to Wish List');

      },
      error: function(xhr) {

      }
    });

}

function remove_from__wishlist(look_id){
    $('#whislist'+look_id).attr("onclick","add_to_wishlist("+look_id+")");
    $('#whislist'+look_id).find('i').removeClass('active');
    $('#'+look_id).remove();

      $.ajax({
        url: sc_baseurl+"profile/remove_from_wishlist",
        type: 'POST',
        data: {'look_id':look_id},
        cache :true,
        success: function(response) {

          location.reload();

        },
        error: function(xhr) {

        }
      });

  }

  function get_wishlist_looks(){
  var offset = $('#offset').val();
  var total_looks = $('#total_looks').val();
   if(parseInt(offset)+parseInt(1)*parseInt(12) < total_looks){
     $.ajax({
              url: sc_baseurl+'wishlist/get_more_wishlist_looks',
              type: 'post',
              data: { 'offset' : offset  },
              success: function(data, status) {
                $('#get_wishlist_looks').append(data);
                //initialiceMasonry('get_wishlist_looks');

                $('#offset').val(parseInt(offset)+parseInt(1));

              }
          });
   }
}

function reload_page(){
   setTimeout(
    function() {
      location.reload();
    }, 500);
}


function scClearAll(){
  $("input[name='brand_name[]']:checked").each(function (){$(this).attr('checked',false);});
  $("input[name='color_name[]']:checked").each(function (){$(this).attr('checked',false);});
  $('input[name=sc_offers]').removeAttr("checked");
  $('input[name=price_filter]').removeAttr("checked");
  $('input[name=discount]').removeAttr("checked");
  $('#clear_all').hide();
  $('#filter-selction-box').hide();
  show_products();
}
/*
function add_to__fav(fav_for, fav_id, user_id=""){
  var data = {fav_for : fav_for, fav_id : fav_id, user_id : user_id};
  var url = sc_baseurl+'schome/fav_add';
  $.ajax({
    type: "POST",
    url: url,
    data: data,
    dataType: "json",
    success: function (response) {
        //your success code
        if(response.msg == "Successful Added"){
          $('#wishlist'+fav_id).find('i').addClass('active');
          location.reload();
        }else if(response.msg == "Successful deleted"){
          $('#wishlist'+fav_id).find('i').removeClass('active');
          location.reload();
        }
    },
    error: function (response) {
        //your error code

    }
  });
}
*/
 function get_filtered_look(){
  var intTriggerScroll = 50;
    //var isTriggered = false;
    var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());

    if(intCurrentScrollPos<intTriggerScroll && !isTriggered){
    isTriggered=true;
    $('.fa-loader-wrp-products').css('display', 'inline-block');
    var offset = 0;
    var look_type = $('#look_type').val();
    var total_looks = $('#total_looks').val();
    var look_filter_type = $('#look_filter_type').val();
    var new_offset = parseInt(offset)+parseInt(1);
          if(offset*12<total_looks && look_filter_type > 1){
           $.ajax({
              url: sc_baseurl+'home/get_more_looks',
              type: 'post',
              data: { 'offset' : new_offset, 'look_type':look_type,'look_filter_type':look_filter_type },
              success: function(data, status) {
                $('#get_all_looks').html(data);
                
                $('.fa-loader-wrp-products').hide();
                $('#offset').val(new_offset);
                load_more_hover();

              }
          });
        }
    }else{
      $('.fa-loader-wrp-products').hide();
    }
}

  function load_more_hover(){
      if($(window).width()>767){
    $('.item-hover').hover(function(){
      $(this).addClass("hover");
    }, function(){
      $(this).removeClass("hover");
    });
    }
    money_formatter(".price");
    money_formatter(".cart-price");
  }

/* ============Global GA Click==================================  */
  function AddToCartGA(brandName,productName)
  {   
    ga('send', 'event', 'Product', 'add_to_cart', brandName+' - '+productName);
	ga('send', 'event', 'AddtoCart', 'click', 'Remktg-AddtoCart');
  }

  function NotifyMeGA(brandName,productName)
  {
    ga('send', 'event', 'Product', 'notify_me',  brandName+' - '+productName);
  }

  function BuyNowGA(brandName,productName)
  {
    ga('send', 'event', 'Product', 'buy_now',  brandName+' - '+productName);
  }

  function RemoveCartPrdGA(brandPrdName)
  {   
    ga('send', 'event', 'Cart', 'removed', brandPrdName);
  }

  function LookPrdAddCartGA(lookPrds)
  {
      ga('send', 'event', 'Product', 'add_to_cart',  lookPrds);
	  ga('send', 'event', 'AddtoCart', 'click', 'Remktg-AddtoCart');
    
  }
  
  function LookPrdBuyNowGA(lookPrds)
  {
      ga('send', 'event', 'Product', 'buy_now',  lookPrds);
    
  }

  /* ============Global GA Click End============================ */