$( document ).ready(function() {

	$('.change-size').change(function(){
		allow_cart_update();
	});

	$('#cart_facebook_login').click(function() {
       facebookCommon('login');
    });
	$('#shipping_pincode_no').trigger('blur');
	$('#billing_pincode_no').trigger('blur');
    
  /*  $('#billing_first_name').blur(function(){ if($('#allow_same_shipp').prop('checked')) { $('#shipping_first_name').val($('#billing_first_name').val()); } });
    $('#billing_last_name').blur(function(){ if($('#allow_same_shipp').prop('checked')) { $('#shipping_last_name').val($('#billing_last_name').val()); } });
    $('#billing_mobile_no').blur(function(){ if($('#allow_same_shipp').prop('checked')) { $('#shipping_mobile_no').val($('#billing_mobile_no').val()); } });
    $('#billing_pincode_no').blur(function(){ 
		show_cart_loader();
		$('#shipping_pincode_no').val($('#billing_pincode_no').val());
		var billing_pincode_no = $('#billing_pincode_no').val();

		$.ajax({
			url: sc_baseurl+"cartnew/get_pincode_info",
			type: 'POST',
			data: { billing_pincode_no : billing_pincode_no },
			cache :true,
			async: true,
			success: function(response) {
				if(response == '[]'){
					$('#billing_pincode_no').val('');
					$('#shipping_pincode_no').val('');
				}
				var obj = jQuery.parseJSON(response);
				$('#billing_city').val(obj.city);
				$('#shipping_city').val(obj.city);
				$('#billing_state').val(obj.state_id);
				$('#shipping_state').val(obj.state_id);
				if($('#billing_first_name').val() !='' && $('#billing_last_name').val() !=''){
					valiate_address();
				}
				hide_cart_loader();
			},
			error: function(xhr) {
				alert('Something goes wrong')
				hide_cart_loader();
			}
		});


	});
	$('#billing_pincode_no').trigger('blur');
    $('#billing_address').blur(function(){ if($('#allow_same_shipp').prop('checked')) { $('#shipping_address').val($('#billing_address').val());} });
    $('#billing_city').blur(function(){ if($('#allow_same_shipp').prop('checked')) { $('#shipping_city').val($('#billing_city').val()); } });
    $('#billing_state').change(function(){
    	if($('#allow_same_shipp').prop('checked')) {
     	var bs = $('#billing_state').val();
     	$('#shipping_state').val(bs);
     	}
    });

*/
    if($('#codna').val() > 0){ 
    	console.log($('#codna').val());
    	$("input[type='radio'][id='sc_cart_pay_mode_cod']").parent().addClass('disabled');
    	$("input[type='radio'][id='sc_cart_pay_mode_cod']").attr('disabled',true);
    	$("input[type='radio'][name='sc_cart_pay_mode_cod']").prop('checked',false);
    }
});


function change_qty(type , id){
	if(type == 'min'){
		var proQty = $('#quantity'+id).val();
		if(proQty > 1){
			$('#quantity'+id).val(proQty-1);
			allow_cart_update();
		}
	}else{
		var proQty = $('#quantity'+id).val();
		if(proQty < 5){
			$('#quantity'+id).val(parseInt(proQty)+1);
			allow_cart_update();
		}
	}
}

function allow_cart_update(){
	$('#scupdatecart').removeClass('hide');
	$('#scupdatecart').attr('disabled',false);
	$('#proceed_to_checkout').addClass('hide');
	$('#scupdatecart1').removeClass('hide');
	$('#scupdatecart1').attr('disabled',false);
	$('#proceed_to_checkout1').addClass('hide');
}

function scremovecart(id){
	var result = confirm("Are you sure you want to remove this product from your cart?");
	if(result){ 
		show_cart_loader();
		$('#cart_'+id).fadeOut( "slow", function() { $('#cart_'+id).remove(); } ); 
		$.ajax({
			url: sc_baseurl+"cartnew/scremovecart",
			type: 'POST',
			data: { id : id },
			cache :true,
			async: true,
			success: function(response) {
				$('#sccart_product_view').html(response);
				if($('#item_count').val() == 'undefined' || $('#item_count').val()==0){
					scUpdateCart(getCookie('SCUniqueID'),'','','','','');
				}else{
					localStorage.setItem('newcartcount', $('#item_count').val());
					$('.cart-count').html($('#item_count').val());
				}
				hide_cart_loader();
				cart_popover_popup();
				
			},
			error: function(xhr) {
				alert('Something goes wrong')
				hide_cart_loader();
			}
		});

	}
}

function sc_updatecart(a){ 
	sccart_frm_submit(a);
}

function show_cart_loader(){
	$('.fa-loader-wrp').show();
	$('.page-overlay').show();
}

function hide_cart_loader(){
	$('.fa-loader-wrp').hide();
	$('.page-overlay').hide();
}

function sccart_frm_submit(a){
	show_cart_loader();
	var pincode = $('#cart_pincode').val(); 
	$("#sccart_frm").on("submit", function (e) {
      	e.preventDefault();
        $.ajax({
            type: "post",
            url: sc_baseurl+"cartnew/updatecart",
            data: $(this).serialize()+'&pincode='+pincode,
            success: function (response) {
            	$('#sccart_product_view').html(response);
				localStorage.setItem('newcartcount', $('#item_count').val());
            	$('.cart-count').html($('#item_count').val());
                hide_cart_loader();
                $('.change-size').change(function(){
					allow_cart_update();
				});

				cart_popover_popup();
				if(getCookie('discountcoupon_stylecracker')!=''){ $('#sccoupon_code').val(getCookie('discountcoupon_stylecracker')); $('#coupon_clear').removeClass('hide'); }else{ $('#coupon_clear').addClass('hide'); }

            },
            error: function (response) {
            	hide_cart_loader();
                alert(response);
            }
        });
    });

}

function cart_check_email(){
	show_cart_loader();
	var cart_email = $('#cart_email').val();
	//document.cookie = "cart_email="+cart_email;
	if(cart_email.trim()!=''){
		$.ajax({
			url: sc_baseurl+"cartnew/check_email_exist",
			type: 'POST',
			data: { cartemail : cart_email },
			cache :true,
			async: true,
			success: function(response) {
				if(response==1){ /* Exist */
					document.cookie = "cart_email="+cart_email;
					$('.box-password').removeClass('hide');
					$('.email-label').html($('#cart_email').val());
					$('#cart_email').attr('readonly','true');
					$('#sc_checkout_login_panel').addClass('hide');
					$('#cart_password').focus();
					$('#cart_email_c').html(cart_email);
					$('.cart-selected-value').removeClass('hide');
				}else if(response==0){ /* Not Exist*/
					document.cookie = "cart_email="+cart_email;
					//$('.box-password-nonexist').removeClass('hide');
					//$('#address_toggle').attr('data-toggle','collapse');
					//$('#address_toggle').trigger('click');
					$('#collapse-options1').css('display','none');
					$('#collapse-options2').css('display','block');
					$('.cart-selected-value').removeClass('hide');
					$('#cart_email_c').html(cart_email);
				}else{
					$('#email_id_error').html(response).show();
					$('#collapse-options1').css('display','block');
					$('#collapse-options2').css('display','none');

				}
				hide_cart_loader();
			},
			error: function(xhr) {
				alert('Something goes wrong')
				hide_cart_loader();
			}
		});
		
	}else{
		alert('Please enter email address');
		hide_cart_loader();
	}
}

function change_email(){
	show_cart_loader();
	$('#cart_email').focus();
	$('#cart_email').attr('readonly',false);
	$('.box-password').addClass('hide');
	$('#sc_checkout_login_panel').removeClass('hide');
	$('.cart-selected-value').addClass('hide');
	
	$('#collapse-options1').css('display','block');
	$('#collapse-options2').css('display','none');
	
	$('#cart_email').focus();
	hide_cart_loader();
}

function valiate_address(){
	show_cart_loader();
	var regexp = /[^a-zA-Z]/g;
	var regexp_mobile = /[^0-9]/g;
	var billing_first_name = $('#billing_first_name').val().trim();
	var billing_last_name = $('#billing_last_name').val().trim();
	var billing_mobile_no = $('#billing_mobile_no').val().trim();
	var billing_pincode_no = $('#billing_pincode_no').val().trim();
	var billing_address = $('#billing_address').val().trim();
	var billing_city = $('#billing_city').val().trim();
	var billing_state = $('#billing_state').val();


	var shipping_first_name = $('#shipping_first_name').val().trim();
	var shipping_last_name = $('#shipping_last_name').val().trim();
	var shipping_mobile_no = $('#shipping_mobile_no').val().trim();
	var shipping_pincode_no = $('#shipping_pincode_no').val().trim();
	var shipping_address = $('#shipping_address').val().trim();
	var shipping_city = $('#shipping_city').val().trim();
	var shipping_state = $('#shipping_state').val();


	var isTrue = 0;

	if(billing_first_name.match(regexp) || billing_first_name==''){
		$('#billing_first_name_error').html('Enter Valid First Name');
		isTrue = 1;
	}else{
		document.cookie = "billing_first_name="+billing_first_name+";path=/";
		$('#billing_first_name_error').html('');
		//isTrue = 0;
	}

	if(billing_last_name.match(regexp) || billing_last_name==''){
		$('#billing_last_name_error').html('Enter Valid Last Name');
		isTrue = 1;
	}else{
		document.cookie = "billing_last_name="+billing_last_name+";path=/";
		$('#billing_last_name_error').html('');
		//isTrue = 0;
	}

	if(billing_mobile_no.match(regexp_mobile) || billing_mobile_no=='' || billing_mobile_no.length < 10){
		$('#billing_mobile_no_error').html('Enter Valid Mobile No');
		isTrue = 1;
	}else{
		document.cookie = "billing_mobile_no="+billing_mobile_no+";path=/";
		$('#billing_mobile_no_error').html('');
		//isTrue = 0;
	}

	if(billing_pincode_no.match(regexp_mobile) || billing_pincode_no=='' || billing_pincode_no.length < 6){
		$('#billing_pincode_no_error').html('Enter Valid Pincode');
		isTrue = 1;
	}else{
		document.cookie = "stylecracker_shipping_pincode="+billing_pincode_no+";path=/";
		$('#billing_pincode_no_error').html('');
		//isTrue = 0;
	}

	if(billing_address==''){
		$('#billing_address_error').html('Enter Billing Address');
		isTrue = 1;
	}else{
		document.cookie = "billing_address="+billing_address+";path=/";
		$('#billing_address_error').html('');
		//isTrue = 0;
	}

	if(billing_city==''){
		$('#billing_city_error').html('Enter Billing City');
		isTrue = 1;
	}else{
		document.cookie = "billing_city="+billing_city+";path=/";
		$('#billing_city_error').html('');
		//isTrue = 0;
	}

	if(billing_state==''){
		$('#billing_state_error').html('Enter Billing State');
		isTrue = 1;
	}else{
		document.cookie = "billing_state="+$("#billing_state option:selected").text()+";path=/";
		$('#billing_state_error').html('');
		//isTrue = 0;
	}


	if(shipping_first_name.match(regexp) || shipping_first_name==''){
		$('#shipping_first_name_error').html('Enter Valid First Name');
		isTrue = 1;
	}else{
		document.cookie = "shipping_first_name="+shipping_first_name+";path=/";
		$('#shipping_first_name_error').html('');
		//isTrue = 0;
	}

	if(shipping_last_name.match(regexp) || shipping_last_name==''){
		$('#shipping_last_name_error').html('Enter Valid Last Name');
		isTrue = 1;
	}else{
		document.cookie = "shipping_last_name="+shipping_last_name+";path=/";
		$('#shipping_last_name_error').html('');
		//isTrue = 0;
	}

	if(shipping_mobile_no.match(regexp_mobile) || shipping_mobile_no=='' || shipping_mobile_no.length < 10){
		$('#shipping_mobile_no_error').html('Enter Valid Mobile No');
		isTrue = 1;
	}else{
		document.cookie = "shipping_mobile_no="+shipping_mobile_no+";path=/";
		$('#shipping_mobile_no_error').html('');
		//isTrue = 0;
	}

	if(shipping_pincode_no.match(regexp_mobile) || shipping_pincode_no=='' || shipping_pincode_no.length < 6){
		$('#shipping_pincode_no_error').html('Enter Valid Pincode');
		isTrue = 1;
	}else{
		document.cookie = "stylecracker_shipping_pincode="+shipping_pincode_no+";path=/";
		$('#shipping_pincode_no_error').html('');
		//isTrue = 0;
	}

	if(shipping_address==''){
		$('#shipping_address_error').html('Enter Shipping Address');
		isTrue = 1;
	}else{
		document.cookie = "shipping_address="+shipping_address+";path=/";
		$('#shipping_address_error').html('');
		//isTrue = 0;
	}

	if(shipping_city==''){
		$('#shipping_city_error').html('Enter shipping City');
		isTrue = 1;
	}else{
		document.cookie = "shipping_city="+shipping_city+";path=/";
		$('#shipping_city_error').html('');
		//isTrue = 0;
	}

	if(shipping_state==''){
		$('#shipping_state_error').html('Enter shipping State');
		isTrue = 1;
	}else{
		document.cookie = "shipping_state="+$("#shipping_state option:selected").text()+";path=/";
		$('#shipping_state_error').html('');
		//isTrue = 0;
	}

	if(isTrue == 0){
		 $.ajax({
            type: "post",
            url: sc_baseurl+"cartnew/order_summary",
            data: { pincode:$('#shipping_pincode_no').val(),sc_cart_pay_mode:$("input[type='radio'][name='sc_cart_pay_mode']:checked").val() },
            cache :true,
			async: true,
            success: function (response) {
                hide_cart_loader();
                $('#desktop_order_summary').html(response);
                payment_options();
                $('#user_address_billing_shipping').attr('style','block');
                if($('#billing_address').val()!=''){

                $('#user_address_billing_shipping_ba').html('<div class="fb">'+$('#billing_first_name').val()+' '+$('#billing_last_name').val()+'</div><div>'+$('#billing_address').val()+'</div><div>'+$('#billing_state option:selected').text()+','+$('#billing_city').val()+','+$('#billing_pincode_no').val()+'</div><a class="btn-link" onclick="open_billing_address_panel();" href="javascript:void(0);">Change</a>');
            	}

            	$('#user_address_billing_shipping').html('<div class="fb">'+$('#shipping_first_name').val()+' '+$('#shipping_last_name').val()+'</div><div>'+$('#shipping_address').val()+'</div><div>'+$('#shipping_city').val()+$('#shipping_state option:selected').text()+$('#shipping_pincode_no').val()+'</div><a class="btn-link" onclick="open_address_panel();" href="javascript:void(0);">Change</a>');
                
            },
            error: function (response) {
            	hide_cart_loader();
                //alert(response);
            }
        });

		 $.ajax({
            type: "post",
            url: sc_baseurl+"cartnew/order_summary_mobile",
            data: { pincode:$('#shipping_pincode_no').val() },
            cache :true,
			async: true,
            success: function (response) {
                hide_cart_loader();
                $('#mobile_order_summary').html(response);
                
            },
            error: function (response) {
            	hide_cart_loader();
                //alert(response);
            }
        });
		 	$('#address_error').addClass('hide');
		 	$('#collapse-options1').css('display','none');
		 	$('#collapse-options2').css('display','none');

		 	if ($('body').hasClass("device-sm")) {
		 		$('#order_summary_mobile').css('display','block');
		 		$('#collapse-options4').css('display','none');
		 	}else{
		 		$('#collapse-options4').css('display','block');
		 	}
	}else{
		$('#address_error').removeClass('hide');
		hide_cart_loader();	
	}

}

function open_address_panel(){
	$('#user_address_billing_shipping').hide();
	$('#collapse-options1').css('display','none');
	$('#collapse-options2').css('display','block');
	
	if ($('body').hasClass("device-sm")) {
		$('#order_summary_mobile').css('display','none');
	}
	$('#collapse-options4').css('display','none');
}

function mobile_hide_order_summary(){
	$('#collapse-options1').css('display','none');
	$('#collapse-options2').css('display','none');
	$('#order_summary_mobile').css('display','none');
	$('#collapse-options4').css('display','block');
}

function pay_valiation(){
	show_cart_loader();
	
	$.ajax({
            type: "post",
            url: sc_baseurl+"cartnew/order_summary",
            data: { pincode:$('#shipping_pincode_no').val(),sc_cart_pay_mode:$("input[type='radio'][name='sc_cart_pay_mode']:checked").val() },
            cache :true,
			async: true,
            success: function (response) {
                hide_cart_loader();
                $('#desktop_order_summary').html(response);
            },
            error: function (response) {
            	hide_cart_loader();
                alert(response);
            }
        });

		 $.ajax({
            type: "post",
            url: sc_baseurl+"cartnew/order_summary_mobile",
            data: { pincode:$('#shipping_pincode_no').val() },
            cache :true,
			async: true,
            success: function (response) {
                hide_cart_loader();
                $('#mobile_order_summary').html(response);
            },
            error: function (response) {
            	hide_cart_loader();
                alert(response);
            }
        });
		 
}

function payment_options(){
	if($('#codna').val() > 0 && $('#shina').val() > 0){
			$("input[type='radio'][id='sc_cart_pay_mode_cod']").parent().addClass('disabled');
		 	$("input[type='radio'][id='sc_cart_pay_mode_cod']").attr('disabled',true);
		 	
		 	$("input[type='radio'][id='sc_cart_pay_mode_online']").attr('disabled',true);
		 	$("input[type='radio'][name='sc_cart_pay_mode_cod']").prop('checked',false);
		 	$("input[type='radio'][id='sc_cart_pay_mode_online']").prop('checked',false);
		 	$('#place_order_sc_cart').addClass('hide');
		 	//$('#cart_error_msg').removeClass('hide');	
		 	var redirection = sc_baseurl+'cartnew?error=1';
		 	window.location = redirection;

		 }else if($('#codna').val() > 0){
		 	$("input[type='radio'][id='sc_cart_pay_mode_cod']").parent().addClass('disabled');
		 	$("input[type='radio'][id='sc_cart_pay_mode_cod']").attr('disabled',true);
		 	$("input[type='radio'][id='sc_cart_pay_mode_online']").attr('disabled',false);
		 	$("input[type='radio'][id='sc_cart_pay_mode_cod']").prop('checked',false);
		 	$("input[type='radio'][id='sc_cart_pay_mode_online']").parent().removeClass('disabled');
		 	$("input[type='radio'][id='sc_cart_pay_mode_online']").prop('checked',true);
		 	$("input[type='radio'][id='sc_cart_pay_mode_online']").parent().addClass('active');
		 	$('#place_order_sc_cart').removeClass('hide');
		 	
		 	$('#cart_error_msg').addClass('hide');
		 	if(getCookie('discountcoupon_stylecracker') == 'NOCASH500' || getCookie('discountcoupon_stylecracker') == 'NOCASH1000'){ $('#cart_place_order_msg').html('COD is not avaliable for applied coupon code'); }else{
		 		$('#cart_place_order_msg').html('One or Two Items in your cart is not available for COD').removeClass('hide');
		 	}
		 }else{
		 	$("input[type='radio'][id='sc_cart_pay_mode_cod']").parent().removeClass('disabled');
		 	$("input[type='radio'][id='sc_cart_pay_mode_online']").parent().removeClass('disabled');
		 	$("input[type='radio'][id='sc_cart_pay_mode_cod']").attr('disabled',false);
		 	$("input[type='radio'][id='sc_cart_pay_mode_online']").attr('disabled',false);
		 	$("input[type='radio'][id='sc_cart_pay_mode_online']").parent().addClass('active');
		 	$("input[type='radio'][id='sc_cart_pay_mode_cod']").prop('checked',false);
		 	$("input[type='radio'][id='sc_cart_pay_mode_online']").prop('checked',true);
		 	$('#place_order_sc_cart').removeClass('hide');
		 	$('#cart_error_msg').addClass('hide');
		 }

		 var pay_mod = $("input[type='radio'][name='sc_cart_pay_mode']:checked").val();
		if(pay_mod == 'card'){
			$("input[type='radio'][id='sc_cart_pay_mode_cod']").parent().removeClass('active');
			$("input[type='radio'][id='sc_cart_pay_mode_online']").parent().addClass('active');
		}else{
			$("input[type='radio'][id='sc_cart_pay_mode_cod']").parent().addClass('active');
			$("input[type='radio'][id='sc_cart_pay_mode_online']").parent().removeClass('active');
		}

		if($('.red_msg').length>0){
			if($('#product_pricess').val() < 600){
				$('.red_msg_cond').removeClass('hide');
				$('.box-mred_msg').addClass('hide');
			}else{
				$('.red_msg_cond').addClass('hide');
				$('.box-mred_msg').removeClass('hide');
			}
		}
}

function place_stylecracker_order(){
	show_cart_loader();
	var pay_mod = $("input[type='radio'][name='sc_cart_pay_mode']:checked").val();
	var codna = $('#codna').val();
	var shina = $('#shina').val();
	var stock_exist = $('#stock_exist').val();

	var regexp = /[^a-zA-Z]/g;
	var regexp_mobile = /[^0-9]/g;
	var billing_first_name = $('#billing_first_name').val().trim();
	var billing_last_name = $('#billing_last_name').val().trim();
	var billing_mobile_no = $('#billing_mobile_no').val().trim();
	var billing_pincode_no = $('#billing_pincode_no').val().trim();
	var billing_address = $('#billing_address').val().trim();
	var billing_city = $('#billing_city').val().trim();
	var billing_state = $('#billing_state').val();

	var isTrue = 0;

	if(billing_first_name.match(regexp) || billing_first_name==''){
		$('#billing_first_name_error').html('Enter Valid First Name');
		isTrue = 1;
	}else{
		document.cookie = "billing_first_name="+billing_first_name+";path=/";
		$('#billing_first_name_error').html('');
		//isTrue = 0;
	}

	if(billing_last_name.match(regexp) || billing_last_name==''){
		$('#billing_last_name_error').html('Enter Valid Last Name');
		isTrue = 1;
	}else{
		document.cookie = "billing_last_name="+billing_last_name+";path=/";
		$('#billing_last_name_error').html('');
		//isTrue = 0;
	}

	if(billing_mobile_no.match(regexp_mobile) || billing_mobile_no=='' || billing_mobile_no.length < 10){
		$('#billing_mobile_no_error').html('Enter Valid Mobile No');
		isTrue = 1;
	}else{
		document.cookie = "billing_mobile_no="+billing_mobile_no+";path=/";
		$('#billing_mobile_no_error').html('');
		//isTrue = 0;
	}

	if(billing_pincode_no.match(regexp_mobile) || billing_pincode_no=='' || billing_pincode_no.length < 6){
		$('#billing_pincode_no_error').html('Enter Valid Pincode');
		isTrue = 1;
	}else{
		document.cookie = "stylecracker_shipping_pincode="+billing_pincode_no+";path=/";
		$('#billing_pincode_no_error').html('');
		//isTrue = 0;
	}

	if(billing_address==''){
		$('#billing_address_error').html('Enter Billing Address');
		isTrue = 1;
	}else{
		document.cookie = "billing_address="+billing_address+";path=/";
		$('#billing_address_error').html('');
		//isTrue = 0;
	}

	if(billing_city==''){
		$('#billing_city_error').html('Enter Billing City');
		isTrue = 1;
	}else{
		document.cookie = "billing_city="+billing_city+";path=/";
		$('#billing_city_error').html('');
		//isTrue = 0;
	}

	if(billing_state==''){
		$('#billing_state_error').html('Enter Billing State');
		isTrue = 1;
	}else{
		document.cookie = "billing_state="+$("#billing_state option:selected").text()+";path=/";
		$('#billing_state_error').html('');
		//isTrue = 0;
	}

	if(isTrue == 1){
		hide_cart_loader();
		$('#cart_place_order_msg').html('Please enter billing address').removeClass('hide');
	}
	else if(stock_exist > 0){
		hide_cart_loader();
		$('#cart_place_order_msg').html('Some of the products are out of stock').removeClass('hide');
	}
	else if(pay_mod !='cod' && pay_mod != 'card'){
		hide_cart_loader();
		$('#cart_place_order_msg').html('Select Payment Option').removeClass('hide');
	}else if(pay_mod =='cod' && codna>0){
		hide_cart_loader();
		$("input[type='radio'][id='sc_cart_pay_mode_cod']").prop('checked',false);
		$("input[type='radio'][id='sc_cart_pay_mode_online']").prop('checked',true);
		
		if(getCookie('discountcoupon_stylecracker') == 'NOCASH500' || getCookie('discountcoupon_stylecracker') == 'NOCASH1000'){ 
			

			$("input[type='radio'][id='sc_cart_pay_mode_cod']").parent().addClass('disabled');
		 	$("input[type='radio'][id='sc_cart_pay_mode_cod']").attr('disabled',true);
		 	$("input[type='radio'][id='sc_cart_pay_mode_online']").attr('disabled',false);
		 	$("input[type='radio'][id='sc_cart_pay_mode_cod']").prop('checked',false);
		 	$("input[type='radio'][id='sc_cart_pay_mode_online']").parent().removeClass('disabled');
		 	$("input[type='radio'][id='sc_cart_pay_mode_online']").prop('checked',true);
		 	$("input[type='radio'][id='sc_cart_pay_mode_online']").parent().addClass('active');
		 	
		 	$('#cart_place_order_msg').html('COD is not avaliable for applied coupon code').removeClass('hide');
		 	
		 	}else{
		 	$('#cart_place_order_msg').html('COD is not available for some products').removeClass('hide');
		}
	}else if(shina > 0){
		hide_cart_loader();
		$('#cart_place_order_msg').html('Shipping is not available for some products').removeClass('hide');
	}else{
		var billing_first_name = $('#billing_first_name').val().trim();
		var billing_last_name = $('#billing_last_name').val().trim();
		var billing_mobile_no = $('#billing_mobile_no').val().trim();
		var billing_pincode_no = $('#billing_pincode_no').val().trim();
		var billing_address = $('#billing_address').val().trim();
		var billing_city = $('#billing_city').val().trim();
		var billing_state = $('#billing_state').val();
		var shipping_first_name = $('#shipping_first_name').val().trim();
		var shipping_last_name = $('#shipping_last_name').val().trim();
		var shipping_mobile_no = $('#shipping_mobile_no').val().trim();
		var shipping_pincode_no = $('#shipping_pincode_no').val().trim();
		var shipping_address = $('#shipping_address').val().trim();
		var shipping_city = $('#shipping_city').val().trim();
		var shipping_state = $('#shipping_state').val();
		var cart_email = $('#cart_email').val().trim(); 
		var cart_total = $('#cart_total').val();

		if(billing_first_name!='' && billing_last_name!='' && billing_mobile_no!='' && billing_pincode_no!='' && billing_address!='' && billing_city!='' && billing_state!='' && shipping_first_name!='' && shipping_last_name!='' && shipping_mobile_no!='' && shipping_pincode_no!='' && shipping_address!='' && shipping_city!='' && shipping_state!='' && cart_email!=''){

			if(pay_mod == 'cod'){

				$.ajax({
		            type: "post",
		            url: sc_baseurl+"cartnew/place_order",
		            data: { billing_first_name:billing_first_name,billing_last_name:billing_last_name,billing_mobile_no:billing_mobile_no,billing_pincode_no:billing_pincode_no,billing_address:billing_address,billing_city:billing_city,billing_state:billing_state,shipping_first_name:shipping_first_name,shipping_last_name:shipping_last_name,shipping_mobile_no:shipping_mobile_no,shipping_pincode_no:shipping_pincode_no,shipping_address:shipping_address,shipping_city:shipping_city,shipping_state:shipping_state,cart_email:cart_email,pay_mod:pay_mod,cart_total:cart_total },
		            cache :true,
					async: true,
		            success: function (response) {
		                hide_cart_loader(); remove_address_cookies('SHIP'); remove_address_cookies('BILL');
		                var redirction = sc_baseurl+'cartnew/ordersuccessOrder/'+response;
		                var final_cookies = '';
		                document.cookie="SCUniqueID="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
		                document.cookie="discountcoupon_stylecracker="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
		      			window.location =redirction;
		            },
		            error: function (response) {
		            	hide_cart_loader();
		                alert(response);
		            }
        		});

			}else{

				$.ajax({
		            type: "post",
		            url: sc_baseurl+"cartnew/place_online_order",
		            data: { billing_first_name:billing_first_name,billing_last_name:billing_last_name,billing_mobile_no:billing_mobile_no,billing_pincode_no:billing_pincode_no,billing_address:billing_address,billing_city:billing_city,billing_state:billing_state,shipping_first_name:shipping_first_name,shipping_last_name:shipping_last_name,shipping_mobile_no:shipping_mobile_no,shipping_pincode_no:shipping_pincode_no,shipping_address:shipping_address,shipping_city:shipping_city,shipping_state:shipping_state,cart_email:cart_email,pay_mod:pay_mod,cart_total:cart_total },
		            cache :true,
					async: true,
		            success: function (response) {
		                hide_cart_loader();
		                var obj = jQuery.parseJSON(response);
		               
		      			$('#txnid').val(obj.txnid);
		      			$('#hash').val(obj.payu_hash);
		      			$('#amount').val(obj.amount);
		      			$('#firstname').val(obj.firstname);
		      			$('#email').val(obj.email);
		      			$('#phone').val(obj.phoneno);
		      			$('#udf1').val(getCookie('SCUniqueID'));
		      			$('#udf2').val(obj.address_sc);
		      			$('#udf3').val(billing_pincode_no);
		      			$('#udf4').val(billing_city);
		      			$('#udf5').val(obj.state);
		      			var stylecracker_style = document.forms.stylecracker_style;
      					stylecracker_style.submit();

		            },
		            error: function (response) {
		            	hide_cart_loader();
		                alert(response);
		            }
        		});


			}
		}
	}
	hide_cart_loader();
	
}

function delete_user_address(id){
	var result = confirm("Are you sure to delete the address");
	if(result){
		show_cart_loader();

		$.ajax({
			url: sc_baseurl+"cartnew/delete_address",
			type: 'POST',
			data: { id : id },
			cache :true,
			async: true,
			success: function(response) {
				$('.user_address_'+id).remove();
				hide_cart_loader();
			},
			error: function(xhr) {
				alert('Something goes wrong')
				hide_cart_loader();
			}
		});
	}
}
function user_old_address(id,add_type){
	//$('.item-address').removeClass('active');
	$.ajax({
			url: sc_baseurl+"cartnew/get_address",
			type: 'POST',
			data: { id : id },
			cache :true,
			async: true,
			success: function(response) {
				var obj = jQuery.parseJSON(response);
				if(add_type == 'BILL'){
					$('#billing_first_name').val(obj[0].first_name);
					if(obj[0].last_name == ''){
						$('#billing_last_name').val(obj[0].first_name);
					}else{
						$('#billing_last_name').val(obj[0].last_name);
					}
					$('#billing_mobile_no').val(obj[0].mobile_no);
					$('#billing_pincode_no').val(obj[0].pincode);
					$('#billing_address').val(obj[0].shipping_address);
					$('#billing_city').val(obj[0].city_name);
					$('#billing_state').val(obj[0].state_name);

					if($('#billing_address').val()!=''){
                	$('#user_address_billing_shipping_ba').html('<div class="fb">'+$('#billing_first_name').val()+' '+$('#billing_last_name').val()+'</div><div>'+$('#billing_address').val()+'</div><div>'+$('#billing_state option:selected').text()+','+$('#billing_city').val()+','+$('#billing_pincode_no').val()+'</div><a class="btn-link" onclick="open_billing_address_panel();" href="javascript:void(0);">Change</a>');

                	//$('#buser_address_'+id).addClass('active');
            	   }

				}
				if(add_type == 'SHIP'){
					$('#shipping_first_name').val(obj[0].first_name);
					if(obj[0].last_name == ''){
						$('#shipping_last_name').val(obj[0].first_name);
					}else{
						$('#shipping_last_name').val(obj[0].last_name);
					}
					$('#shipping_mobile_no').val(obj[0].mobile_no);
					$('#shipping_pincode_no').val(obj[0].pincode);
					$('#shipping_address').val(obj[0].shipping_address);
					$('#shipping_city').val(obj[0].city_name);
					$('#shipping_state').val(obj[0].state_name);
				//	$('#suser_address_'+id).addClass('active');
				}
				hide_cart_loader();
			},
			error: function(xhr) {
				alert('Something goes wrong')
				hide_cart_loader();
			}
		});
}

function edit_user_address(id){
	$('#sc_cart_add_new_address').hide();
	$('#sc_cart_next').addClass('disabled');
	$('#sc_cart_next').removeAttr('onclick');
	$('#add_new_address').hide();
	$('#edit_new_address').show();
	//$('#user_address_billing_shipping_ba').css('display','block');
	

	$.ajax({
			url: sc_baseurl+"cartnew/get_address",
			type: 'POST',
			data: { id : id },
			cache :true,
			async: true,
			success: function(response) {
				var obj = jQuery.parseJSON(response);
				
				$('#edit_first_name').val(obj[0].first_name);
				if(obj[0].last_name == ''){
					$('#edit_last_name').val(obj[0].first_name);
				}else{
					$('#edit_last_name').val(obj[0].last_name);
				}
				$('#edit_mobile_no').val(obj[0].mobile_no);
				$('#edit_pincode_no').val(obj[0].pincode);
				$('#edit_address').val(obj[0].shipping_address);
				$('#edit_city').val(obj[0].city_name);
				$('#edit_state').val(obj[0].state_name);
				$('#edit_address_id').val(id);
				
				open_billing_address_panel();
				hide_cart_loader();
			},
			error: function(xhr) {
				alert('Something goes wrong')
				hide_cart_loader();
			}
		});
}

function edit_pincode_check(){
	var billing_pincode_no = $('#edit_pincode_no').val();
	$.ajax({
			url: sc_baseurl+"cartnew/get_pincode_info",
			type: 'POST',
			data: { billing_pincode_no : billing_pincode_no },
			cache :true,
			async: true,
			success: function(response) {
				if(response == '[]'){
					$('#edit_pincode_no').val('');
				}
				var obj = jQuery.parseJSON(response);
				$('#edit_city').val(obj.city);
				$('#edit_state').val(obj.state_id);
				
				hide_cart_loader();
			},
			error: function(xhr) {
				alert('Something goes wrong')
				hide_cart_loader();
			}
	});
}

function check_pincode_shipping(){
	var billing_pincode_no = $('#shipping_pincode_no').val();
	$.ajax({
			url: sc_baseurl+"cartnew/get_pincode_info",
			type: 'POST',
			data: { billing_pincode_no : billing_pincode_no },
			cache :true,
			async: true,
			success: function(response) {
				if(response == '[]'){
					$('#shipping_pincode_no').val('');
				}
				var obj = jQuery.parseJSON(response);
				$('#shipping_city').val(obj.city);
				$('#shipping_state').val(obj.state_id);
				
				hide_cart_loader();
			},
			error: function(xhr) {
				alert('Something goes wrong')
				hide_cart_loader();
			}
	});
}

function cancel_edit_address(){
	$('#sc_cart_next').removeClass('disabled');
	$('#sc_cart_next').attr('onclick','valiate_address();');
	$('#add_new_address').hide();
	$('#sc_cart_add_new_address').show();
	$('#edit_new_address').hide();
}

function edit_existing_address(){
	show_cart_loader();
	var regexp = /[^a-zA-Z]/g;
	var regexp_mobile = /[^0-9]/g;
	var billing_first_name = $('#edit_first_name').val().trim();
	var billing_last_name = $('#edit_last_name').val().trim();
	var billing_mobile_no = $('#edit_mobile_no').val().trim();
	var billing_pincode_no = $('#edit_pincode_no').val().trim();
	var billing_address = $('#edit_address').val().trim();
	var billing_city = $('#edit_city').val().trim();
	var billing_state = $('#edit_state').val();

	var isTrue = 0;

	if(billing_first_name.match(regexp) || billing_first_name==''){
		$('#edit_first_name_error').html('Enter Valid First Name');
		isTrue = 1;
	}else{
		$('#edit_first_name_error').html('');
		//isTrue = 0;
	}

	if(billing_last_name.match(regexp) || billing_last_name==''){
		$('#edit_last_name_error').html('Enter Valid Last Name');
		isTrue = 1;
	}else{
		
		$('#edit_last_name_error').html('');
		//isTrue = 0;
	}

	if(billing_mobile_no.match(regexp_mobile) || billing_mobile_no=='' || billing_mobile_no.length < 10){
		$('#edit_mobile_no_error').html('Enter Valid Mobile No');
		isTrue = 1;
	}else{
		
		$('#edit_mobile_no_error').html('');
		//isTrue = 0;
	}

	if(billing_pincode_no.match(regexp_mobile) || billing_pincode_no=='' || billing_pincode_no.length < 6){
		$('#edit_pincode_no_error').html('Enter Valid Pincode');
		isTrue = 1;
	}else{
		
		$('#edit_pincode_no_error').html('');
		//isTrue = 0;
	}

	if(billing_address==''){
		$('#edit_address_error').html('Enter Billing Address');
		isTrue = 1;
	}else{
		
		$('#edit_address_error').html('');
		//isTrue = 0;
	}

	if(billing_city==''){
		$('#edit_city_error').html('Enter Billing City');
		isTrue = 1;
	}else{
		
		$('#edit_city_error').html('');
		//isTrue = 0;
	}

	if(billing_state==''){
		$('#edit_state_error').html('Enter Billing State');
		isTrue = 1;
	}else{
		
		$('#edit_state_error').html('');
		//isTrue = 0;
	}
	var edit_address_id = $('#edit_address_id').val();
	if(isTrue == 0 && edit_address_id>0){

		$.ajax({
        	url: sc_baseurl+'cartnew/edit_users_address',
        	type: 'post',
        	data: {'type' : 'edit_users_address','billing_first_name':billing_first_name,'billing_last_name':billing_last_name,'billing_mobile_no':billing_mobile_no,'billing_pincode_no':billing_pincode_no,'billing_address':billing_address,'billing_city':billing_city,'billing_state':billing_state,'edit_address_id':edit_address_id },
        	success: function(data, status) {
        		window.location.href=sc_baseurl+'cartnew/checkout';
        	}
        });
		
	}else{

	}
	hide_cart_loader();

}


function cart_login(){
	var cart_email = $('#cart_email').val().trim();
	var cart_password = $('#cart_password').val().trim();

	if(cart_email==''){ alert('Please enter email address'); }
	else if(cart_password==''){ alert('Please enter password'); }
	else if(cart_email!='' && cart_password!=''){
		$.ajax({
        url: sc_baseurl+'schome/login',
        type: 'post',
        data: {'type' : 'get_login','logEmailid':cart_email,'logPassword':cart_password },
        success: function(data, status) {

          if(data=='pa'){
            window.location.href=sc_baseurl+"cartnew/checkout";
          }else if(data == 'feed'){
            window.location.href=sc_baseurl+"cartnew/checkout";
          }else{
            $("#sc_Login").removeClass( "disabled" );
            $(".fa-loader-wrp-products").css({"display":"none"});
            $('#login_error').html('Enter Valid Password');
          }

          return false;
          event.preventDefault();

        },
        error: function(xhr, desc, err) {
          // console.log(err);
        }
      });
	}

}

function checkout_as_guest(){
	$('#collapse-options1').css('display','none');
	$('#collapse-options2').css('display','block');
	$('.cart-selected-value').removeClass('hide');
}

function check_cart_availability(){
	show_cart_loader();
	var regexp_mobile = /[^0-9]/g;
	var cart_pincode = $('#cart_pincode').val();
	var stock_exist = $('#stock_exist').val();
	var	isTrue = 0;

	if(stock_exist > 0){
		$('#cart_pincode_error').html('Product Is Out of Stock');
		isTrue = 1;
		hide_cart_loader();
	}else if(cart_pincode.match(regexp_mobile) || cart_pincode=='' || cart_pincode.length < 6){
		$('#cart_pincode_error').html('Enter Valid Pincode');
		isTrue = 1;
		hide_cart_loader();
	}else{
		$('#cart_pincode_error').html('');
		isTrue = 0;
	}

	if(isTrue == 0){
		$.ajax({
            type: "post",
            url: sc_baseurl+"cartnew/check_cart_availability",
            data: { pincode:$('#cart_pincode').val() },
            cache :true,
			async: true,
            success: function (response) {
               $('#sccart_product_view').html(response);
               if($('#shina').val() == 0){
               		//$('#proceed_to_checkout').attr('disabled',false);
               		$('#proceed_to_checkout').removeClass('hide');
               		$('#proceed_to_checkout1').removeClass('hide');
               		$('#cart_pincode_error').removeClass('error').addClass('success');
               		//$('#cart_pincode_error').html('All items are available click on proceed to checkout button');
               		proceed_to_checkout();
               }else{
               		$('#cart_pincode_error').html('Change your shipping pincode otherwise remove non shipping items from your cart');
               		
               }
                hide_cart_loader();
                $('#cart_pincode').val(cart_pincode);
            },
            error: function (response) {
            	hide_cart_loader();
                alert(response);
            }
        });
	}
}

function proceed_to_checkout(){
	window.location = sc_baseurl+'cartnew/checkout';
}

function buySingleOrMultiple(productid,brandname,producname){
	show_cart_loader();
	var cart_count = $('.cart-count').html();	

	if(cart_count > 0){
		hide_cart_loader();
		$('#nonremoval').val(productid);
		$('#nonremovalproname').val(producname);
		$('#nonremovalbrandname').val(brandname);
		$('#popup-newscbuynow').modal({
	    	show: true
		});
	}else{
		add_to_buy_product(productid);
		BuyNowGA(brandname,producname);
		setTimeout('redirect_to_checkout()', 100);
	}
	hide_cart_loader();
	
}

function buySingleOrMultipleProductsLook(){
	show_cart_loader();
	var cart_count = $('.cart-count').html();	

	if(cart_count > 0){
		$('#popup-newsclookbuynow').modal({
	    	show: true
		});
	}else{

		var lookId = $(".buy-look").attr("data-attr");

		for (i = 0; i < productlook_array.length; i++) {
			productId = productlook_array[i];
			productlookNames = productlookNames_array[i];
			if(productId!=''){
    		var productSize =$('#size_'+productId).val();
    		if(productSize!='' && typeof productSize!='undefined'){
    			
    				$('#productSizeSelect'+productId+'').html('');
    				
    				scUpdateCart(getCookie('SCUniqueID'),productId,'',productSize,'1',lookId);
					//_beatout_add_to_cart(getCookie('SCUniqueID'),productId,'',productSize,'1');
					ga('send', 'pageview', '/overlay/addtocart/?page=' + document.location.pathname + document.location.search + ' - ' +productId);
    				LookPrdBuyNowGA(productlookNames);
    		}else{
    			
    			$( '#productSizeSelect'+productId+'' ).removeClass().addClass('product-sizes-error');
    		}
    	}
		}

		$(".page-overlay").hide();
		$(".fa-loader-wrp").hide();					

                proceed_to_checkout();

	}
	hide_cart_loader();
}

function remove_products_otherthan_nonremoval(a){
	show_cart_loader();
	if(a == 1){
	var nonremoval = $('#nonremoval').val();
	$.ajax({
            type: "post",
            url: sc_baseurl+"cartnew/remove_products_otherthan_nonremoval",
            data: { nonremoval:nonremoval },
            cache :true,
			async: true,
            success: function (response) {
            	add_to_buy_product($('#nonremoval').val());
				BuyNowGA($('#nonremovalbrandname').val(),$('#nonremovalproname').val());
				setTimeout('redirect_to_checkout()', 100);
            	
            }
        });
	}else{
		add_to_buy_product($('#nonremoval').val());
		BuyNowGA($('#nonremovalbrandname').val(),$('#nonremovalproname').val());
		setTimeout('redirect_to_checkout()', 100);
	}
	hide_cart_loader();
}


function remove_looks_products(){
	
	$('#popup-newsclookbuynow').modal('hide');
	show_cart_loader(); 
	var nonremoval = '';
		
		$.ajax({
            type: "post",
            url: sc_baseurl+"cartnew/remove_products_otherthan_nonremoval",
            data: { nonremoval:nonremoval },
            cache :true,
			async: true,
            success: function (response) {
            	 buylook();
            }
        });
    
	hide_cart_loader();
}

function buylook(){
		$('#popup-newsclookbuynow').modal('hide');
		show_cart_loader(); 
		var lookId = $(".buy-look").attr("data-attr");
		for (i = 0; i < productlook_array.length; i++) {
			productId = productlook_array[i];
			productlookNames = productlookNames_array[i];
			if(productId!=''){
		    	var productSize =$('#size_'+productId).val();
		    		if(productSize!='' && typeof productSize!='undefined'){
		    			$('#productSizeSelect'+productId+'').html('');
		    				
		    			scUpdateCart(getCookie('SCUniqueID'),productId,'',productSize,'1',lookId);
						//_beatout_add_to_cart(getCookie('SCUniqueID'),productId,'',productSize,'1');
						ga('send', 'pageview', '/overlay/addtocart/?page=' + document.location.pathname + document.location.search + ' - ' +productId);
		    			LookPrdBuyNowGA(productlookNames);
		    		}else{
		    			
		    			$( '#productSizeSelect'+productId+'' ).removeClass().addClass('product-sizes-error');
		    		}
		    }
		}
			
	setTimeout('redirect_to_checkout()', 100);

}

function redirect_to_checkout()
{	
    window.location=sc_baseurl+'cartnew/checkout';
}

function apply_coupon_code(){
	show_cart_loader();

	$.ajax({
            type: "post",
            url: sc_baseurl+"cartnew/apply_coupon_code",
            data: { 'sccoupon_code':$('#sccoupon_code').val() },
            cache :true,
			async: false,
            success: function (response) {
            	var obj = jQuery.parseJSON(response);
               if(obj.msg_type == 1){
                	$('#coupon_error').html(obj.msg).removeClass('error').addClass('success');
            	}else{
            		$('#coupon_error').html(obj.msg).removeClass('success').addClass('error');
            	}
            },
            error: function (response) {
            	hide_cart_loader();
                alert(response);
            }
        });
	
	$.ajax({
            type: "post",
            url: sc_baseurl+"cartnew/order_summary",
            data: { pincode:$('#shipping_pincode_no').val(),sc_cart_pay_mode:$("input[type='radio'][name='sc_cart_pay_mode']:checked").val(),'sccoupon_code':$('#sccoupon_code').val() },
            cache :true,
			async: false,
            success: function (response) {
                hide_cart_loader();
                $('#desktop_order_summary').html(response);
            },
            error: function (response) {
            	hide_cart_loader();
                alert(response);
            }
        });

		 $.ajax({
            type: "post",
            url: sc_baseurl+"cartnew/order_summary_mobile",
            data: { pincode:$('#shipping_pincode_no').val(),'sccoupon_code':$('#sccoupon_code').val() },
            cache :true,
			async: false,
            success: function (response) {
                hide_cart_loader();
                $('#mobile_order_summary').html(response);
            },
            error: function (response) {
            	hide_cart_loader();
                alert(response);
            }
        });

		 if(getCookie('discountcoupon_stylecracker')!=''){ $('#coupon_clear').removeClass('hide'); }else{ $('#coupon_clear').addClass('hide'); }
}

function sccartcheckotp(){
	 $.ajax({
            type: "post",
            url: sc_baseurl+"cartnew/sc_check_otp",
            data: { mobile_no:$('#mobile_no').val(),otp_number:$('#scopt').val(),oid:$('#oid').val() },
            cache :true,
			async: false,
            success: function (response) {
                hide_cart_loader();
                if(response == 0){
                	alert('Please enter valid OTP');
                }else{
                	location.reload();
                }
            },
            error: function (response) {
            	hide_cart_loader();
                alert(response);
            }
        });
}

function remove_address_cookies(add_type){
	
	var final_cookies = '';
	if(add_type == 'BILL'){
	document.cookie="billing_first_name="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC;  path=/";
	document.cookie="billing_last_name="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC;  path=/";
	document.cookie="billing_mobile_no="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="billing_address="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="billing_city="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="billing_state="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	}

	if(add_type == 'SHIP'){
	document.cookie="shipping_first_name="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="shipping_last_name="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="shipping_mobile_no="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="shipping_address="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="stylecracker_shipping_pincode="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="shipping_city="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="shipping_state="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="shipping_pincode_no="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";

	}
	document.cookie="cart_email="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";

}

function add_new_address(){
	$('#add_new_address').show();
	empty_all_address('SHIP');
	remove_address_cookies('SHIP');
}

function cart_popover_popup(){
	$('.sc-popover-btn[data-toggle="sc-popover"]').click( function () {
		var content = $(this).attr("data-content");
		var html = '<div class="sc-popover bottom" role="tooltip"><div class="arrow"></div><div class="sc-popover-content">'+ content +'</div></div>';
		$(this).after(html);
  	});
}

function buy_entire_look(a){
	buySingleOrMultipleProductsLook();
}

function clearCartCount(){ 
	localStorage.setItem('newcartcount', 0);
}

function empty_all_address(add_type){
	if(add_type == 'BILL'){
		$('#billing_first_name').val('');
		$('#billing_last_name').val('');
		$('#billing_mobile_no').val('');
		$('#billing_pincode_no').val('');
		$('#billing_address').val('');
		$('#billing_city').val('');
		$('#billing_state').val('');
	}
	if(add_type == 'SHIP'){
		$('#shipping_first_name').val('');
		$('#shipping_last_name').val('');
		$('#shipping_mobile_no').val('');
		$('#shipping_pincode_no').val('');
		$('#shipping_address').val('');
		$('#shipping_city').val('');
		$('#shipping_state').val('');
	}	
}

function redeem_point(is){
	if($(is).is(':checked')){
		document.cookie="is_redeem_point=1";
	}else{
		document.cookie="is_redeem_point=0";
	}
	

	$.ajax({
            type: "post",
            url: sc_baseurl+"cartnew/order_summary",
            data: { pincode:$('#shipping_pincode_no').val(),sc_cart_pay_mode:$("input[type='radio'][name='sc_cart_pay_mode']:checked").val(),'sccoupon_code':$('#sccoupon_code').val() },
            cache :true,
			async: false,
            success: function (response) {
                hide_cart_loader();
                $('#desktop_order_summary').html(response);
            },
            error: function (response) {
            	hide_cart_loader();
                alert(response);
            }
        });

		 $.ajax({
            type: "post",
            url: sc_baseurl+"cartnew/order_summary_mobile",
            data: { pincode:$('#shipping_pincode_no').val(),'sccoupon_code':$('#sccoupon_code').val() },
            cache :true,
			async: false,
            success: function (response) {
                hide_cart_loader();
                $('#mobile_order_summary').html(response);
            },
            error: function (response) {
            	hide_cart_loader();
                alert(response);
            }
        });
}

function redeem_point_cart(is){
	
	if($(is).is(':checked')){
		document.cookie="is_redeem_point=1";
	}else{
		document.cookie="is_redeem_point=0";
	}
	
	$.ajax({
		url: sc_baseurl+"cartnew/remore_redeem",
		type: 'POST',
		cache :true,
		async: true,
		success: function(response) {
			$('#sccart_product_view').html(response);
		}
	});
}

function show_refereal_msg_on_checkout(){
	if($('.red_msg').length>0){ console.log('1');
			if($('#product_pricess').val() < 600){
				$('.red_msg_cond').removeClass('hide');
				$('.box-mred_msg').addClass('hide');
			}else{
				$('.red_msg_cond').addClass('hide');
				$('.box-mred_msg').removeClass('hide');
			}
		}
}

function clear_coupon_code(){
	var final_cookies = '';
	if(getCookie('discountcoupon_stylecracker') =='NOCASH500' || getCookie('discountcoupon_stylecracker') =='NOCASH1000'){
		clear_coupon_code_with_load();
	}
	document.cookie="discountcoupon_stylecracker="+final_cookies+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	$('#sccoupon_code').val('');
	$('#coupon_error').html('');
	$('#proceed_to_checkout').addClass('hide');
	$('#proceed_to_checkout1').addClass('hide');
	$('#scupdatecart').removeClass('hide');
	$('#scupdatecart').attr('disabled',false);
	$('#scupdatecart1').removeClass('hide');
	$('#scupdatecart1').attr('disabled',false);
	apply_coupon_code();
}

function valiate_shipping_address(){

	show_cart_loader();
	var regexp = /[^a-zA-Z]/g;
	var regexp_mobile = /[^0-9]/g;

	var shipping_first_name = $('#shipping_first_name').val().trim();
	var shipping_last_name = $('#shipping_last_name').val().trim();
	var shipping_mobile_no = $('#shipping_mobile_no').val().trim();
	var shipping_pincode_no = $('#shipping_pincode_no').val().trim();
	var shipping_address = $('#shipping_address').val().trim();
	var shipping_city = $('#shipping_city').val().trim();
	var shipping_state = $('#shipping_state').val();


	var isTrue = 0;


	if(shipping_first_name.match(regexp) || shipping_first_name==''){
		$('#shipping_first_name_error').html('Enter Valid First Name');
		isTrue = 1;
	}else{
		document.cookie = "shipping_first_name="+shipping_first_name+";path=/";
		$('#shipping_first_name_error').html('');
		//isTrue = 0;
	}

	if(shipping_last_name.match(regexp) || shipping_last_name==''){
		$('#shipping_last_name_error').html('Enter Valid Last Name');
		isTrue = 1;
	}else{
		document.cookie = "shipping_last_name="+shipping_last_name+";path=/";
		$('#shipping_last_name_error').html('');
		//isTrue = 0;
	}

	if(shipping_mobile_no.match(regexp_mobile) || shipping_mobile_no=='' || shipping_mobile_no.length < 10){
		$('#shipping_mobile_no_error').html('Enter Valid Mobile No');
		isTrue = 1;
	}else{
		document.cookie = "shipping_mobile_no="+shipping_mobile_no+";path=/";
		$('#shipping_mobile_no_error').html('');
		//isTrue = 0;
	}

	if(shipping_pincode_no.match(regexp_mobile) || shipping_pincode_no=='' || shipping_pincode_no.length < 6){
		$('#shipping_pincode_no_error').html('Enter Valid Pincode');
		isTrue = 1;
	}else{
		document.cookie = "stylecracker_shipping_pincode="+shipping_pincode_no+";path=/";
		$('#shipping_pincode_no_error').html('');
		//isTrue = 0;
	}

	if(shipping_address==''){
		$('#shipping_address_error').html('Enter Shipping Address');
		isTrue = 1;
	}else{
		document.cookie = "shipping_address="+shipping_address+";path=/";
		$('#shipping_address_error').html('');
		//isTrue = 0;
	}

	if(shipping_city==''){
		$('#shipping_city_error').html('Enter shipping City');
		isTrue = 1;
	}else{
		document.cookie = "shipping_city="+shipping_city+";path=/";
		$('#shipping_city_error').html('');
		//isTrue = 0;
	}

	if(shipping_state==''){
		$('#shipping_state_error').html('Enter shipping State');
		isTrue = 1;
	}else{
		document.cookie = "shipping_state="+$("#shipping_state option:selected").text()+";path=/";
		$('#shipping_state_error').html('');
		//isTrue = 0;
	}

	if(isTrue == 0){

		if($('#allow_same_shipp').prop('checked')) {
			$('#billing_first_name').val($('#shipping_first_name').val());
			$('#billing_last_name').val($('#shipping_last_name').val());
			$('#billing_mobile_no').val($('#shipping_mobile_no').val());
			$('#billing_pincode_no').val($('#shipping_pincode_no').val());
			$('#billing_address').val($('#shipping_address').val());
			$('#billing_city').val($('#shipping_city').val());
			$('#billing_state').val($('#shipping_state').val());

			document.cookie = "billing_first_name="+shipping_first_name+";path=/";
			$('#billing_first_name_error').html('');
			document.cookie = "billing_last_name="+shipping_last_name+";path=/";
			$('#billing_last_name_error').html('');
			document.cookie = "billing_mobile_no="+shipping_mobile_no+";path=/";
			$('#billing_mobile_no_error').html('');
			document.cookie = "stylecracker_shipping_pincode="+shipping_pincode_no+";path=/";
			$('#billing_pincode_no_error').html('');
			document.cookie = "billing_address="+shipping_address+";path=/";
			$('#billing_address_error').html('');
			document.cookie = "billing_city="+shipping_city+";path=/";
			$('#billing_city_error').html('');
			document.cookie = "billing_state="+$("#billing_state option:selected").text()+";path=/";
			$('#billing_state_error').html('');

		}else{
			empty_all_address('BILL');
			remove_address_cookies('BILL');
		}
		 $.ajax({
            type: "post",
            url: sc_baseurl+"cartnew/order_summary",
            data: { pincode:$('#shipping_pincode_no').val(),sc_cart_pay_mode:$("input[type='radio'][name='sc_cart_pay_mode']:checked").val() },
            cache :true,
			async: true,
            success: function (response) {
                hide_cart_loader();
                $('#desktop_order_summary').html(response);
                payment_options();
                $('#user_address_billing_shipping').attr('style','block');
                if($('#billing_address').val()!=''){
                 $('#user_address_billing_shipping_ba').html('<div class="fb">'+$('#billing_first_name').val()+' '+$('#billing_last_name').val()+'</div><div>'+$('#billing_address').val()+'</div><div>'+$('#billing_state option:selected').text()+','+$('#billing_city').val()+','+$('#billing_pincode_no').val()+'</div><a class="btn-link" onclick="open_billing_address_panel();" href="javascript:void(0);">Change</a>');
            	}

            	$('#user_address_billing_shipping').html('<div class="fb">'+$('#shipping_first_name').val()+' '+$('#shipping_last_name').val()+'</div><div>'+$('#shipping_address').val()+'</div><div>'+$('#shipping_city').val()+$('#shipping_state option:selected').text()+$('#shipping_pincode_no').val()+'</div><a class="btn-link" onclick="open_address_panel();" href="javascript:void(0);">Change</a>');
            },
            error: function (response) {
            	hide_cart_loader();
                //alert(response);
            }
        });

		 $.ajax({
            type: "post",
            url: sc_baseurl+"cartnew/order_summary_mobile",
            data: { pincode:$('#shipping_pincode_no').val() },
            cache :true,
			async: true,
            success: function (response) {
                hide_cart_loader();
                $('#mobile_order_summary').html(response);
                
            },
            error: function (response) {
            	hide_cart_loader();
                //alert(response);
            }
        });
		 	$('#address_error').addClass('hide');
		 	$('#collapse-options1').css('display','none');
		 	$('#collapse-options2').css('display','none');
		 	//console.log($('#allow_same_shipp').prop('checked'));
		 	if($('#allow_same_shipp').prop('checked')) {
		 		//console.log('hide');
				$('#new_ba').addClass('hide');
			}else{
				//console.log('hide11')
				remove_address_cookies('BILL'); empty_all_address('BILL');
				$('#new_ba').removeClass('hide');
			}

		 	if ($('body').hasClass("device-sm")) {
		 		$('#order_summary_mobile').css('display','block');
		 		$('#collapse-options4').css('display','none');
		 	}else{
		 		$('#collapse-options4').css('display','block');
		 	}
	}else{
		$('#address_error').removeClass('hide');
		hide_cart_loader();	
	}



}

function open_billing_address_panel(){
	$('#user_address_billing_shipping_ba').css('display','none');
	$('#new_ba').css('display','block');
	$('#new_ba').removeClass('hide');
}

function add_new_billing_address(){
	$('#new_ba').show();
	$('#new_ba').removeClass('hide');
	empty_all_address('BILL');
	remove_address_cookies('BILL');
}