var swiperMain = new Swiper('.swiper-container-front.front.main', {
          pagination: '.swiper-pagination.front.main',
            paginationClickable: true,
            slidesPerView: 1,
            loop: true,
            touchRatio: 0.2,
            autoplay:4000,
            effect: 'fade',
            observer:'true',
            observeParents:'true',
});

var swiperVerticle = new Swiper('.swiper-container.front.verticle', {
       pagination: '.swiper-pagination.verticle',
      // nextButton: '.swiper-button-next.verticle',
      // prevButton: '.swiper-button-prev.verticle',

       direction: 'vertical',
       slidesPerView: 1,

       paginationClickable: true,
       spaceBetween: 30,
       mousewheelControl: true,
       mousewheelReleaseOnEdges: true,
       simulateTouch: true,
  	   iOSEdgeSwipeDetection: true,
  	   mousewheelForceToAxis: true,
	   onSliderMove: function(swiper){
          var topPos =  $('.swiper-container.front.verticle').offset().top - 60;
         $(window).scrollTop(topPos);
        },
		  onSlideChangeStart: function(swiper){
            var topPos =  $('.swiper-container.front.verticle').offset().top - 60;
           $(window).scrollTop(topPos);
          },

       breakpoints: {
        768: {
             direction: 'horizontal',
          //   nextButton: '.swiper-button-next.verticle',
          //   prevButton: '.swiper-button-prev.verticle',
        },
      },


   });

   var swiperProducts = new Swiper('.swiper-container.front.products', {
          pagination: '.swiper-pagination.products',
          nextButton: '.swiper-button-next.products',
          prevButton: '.swiper-button-prev.products',
          paginationClickable: true,
          loop: true,
          cssWidthAndHeight: true,
          slidesPerView:'auto',
		      autoplay:2000,
      });


      var swiperWork = new Swiper('.swiper-container.work', {
        pagination: '.swiper-pagination.work',
        paginationClickable: true,
        loop: true,

        cssWidthAndHeight: true,
        slidesPerView:'auto',

        nextButton: '.swiper-button-next.work',
        prevButton: '.swiper-button-prev.work',
         });

$(document).ready(function() {
   $('.swiper-container.front.products').hover(
        function (){ swiperProducts.stopAutoplay(); },
        function (){ swiperProducts.startAutoplay();}
    )

  });