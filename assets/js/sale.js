$( document ).ready(function() {
	var track_load = 1;
	var loading  = false; 
	var total_groups = '5000';
	$(window).scroll(function() {
		if($(window).scrollTop() + $(window).height() == $(document).height())  //user scrolled to bottom of the page?
        {
        	if(track_load <= total_groups && loading==false && $('#filter-selction-box1').length>0) //there's more data to load
            {
            	loading = true;

            	$('.fa-loader-wrp-products').css('display', 'inline-block'); //show loading image

                var v_arr = ''; var i = 0;
                $('.ads_Checkbox:checked').each(function () {
                       if(i != 0){ v_arr = v_arr+ ','; }
                       v_arr = v_arr+$(this).val();
                       i++;
                });
                
                var v_arr_b = ''; var k = 0;
                $('.brands_Checkbox:checked').each(function () {
                       if(k != 0){ v_arr_b = v_arr_b+ ','; }
                       v_arr_b = v_arr_b+$(this).val();
                       k++;
                });
             
                var product_sort_by_sale = $('#sort_price').val();
            	$.post(sc_baseurl+'sale/load_more_sale_products',{'group_no': track_load,'category':v_arr,'brands':v_arr_b,'product_sort_by_sale':product_sort_by_sale}, function(data){
            		if(data!=''){ 
                    $("#get_all_cat_products").append(data);
                   // console.log(data);
            		$("img.lazy").lazyload({
				        effect : "fadeIn",
                        threshold : 300,
                        placeholder: 'https://www.stylecracker.com/assets/images/logo-graysclae.png'
        			});
            		$('.fa-loader-wrp-products').hide(); //hide loading image once data is received
					track_load++; //loaded group increment
                    loading = false; 
                    }else{
                        $('.fa-loader-wrp-products').hide(); 
                    }

            	});

            }
        }
    });
    funFilterPageSwiper();
    var FilterPageSwiper;
if($('.filterpage-slider-wrp').length > 0){ 
     FilterPageSwiper = new Swiper('.filterpage.swiper-container', {
          //  pagination: '.swiper-pagination.common',
          //  slidesPerView: 10,
            paginationClickable: true,
            spaceBetween: 0,
            loop: true,
            spaceBetween: 5,
            cssWidthAndHeight: true,
            slidesPerView:'auto',
            autoplay:'1000',
           // preloadImages: false,
          // Enable lazy loading
            //lazyLoading: true,
            nextButton: '.swiper-button-next.filterpage',
            prevButton: '.swiper-button-prev.filterpage',
           breakpoints: {
             1024: {
                 slidesPerView: 4,
             },
             768: {
                 slidesPerView: 3,
             },
             640: {
                 slidesPerView: 2,
             },
             320: {
                 slidesPerView: 2,
             }
         }

   });
     
     if($('.swiper-button-next').length > 0){
    /*document.querySelector('.swiper-button-next.filterpage').addEventListener('click', function (e) {
        e.preventDefault();
        
        $.ajax({
            type: "post",
            url: sc_baseurl+"sale/getSaleBrands",
            data :{'cnt':$('#brand_slider_count').val()},
            success: function (response) {
                if(response!=''){
                    FilterPageSwiper.prependSlide(response);
                    $('#brand_slider_count').val(parseInt($('#brand_slider_count').val())+1);
                }
            },
            error: function (response) {
               
            }
        });

    });*/
    }
}
    $('.ads_Checkbox').click(function() {
        filter_products_on_sale();
    });

    $('.brands_Checkbox').click(function() {
        filter_products_on_sale();
    });

    $('.product_sort_by_sale').change(function() {
        $('#sort_price').val($(this).val())
        filter_products_on_sale();

    });

});

function filter_products_on_sale(){
    var old_height = $('#get_all_cat_products').innerHeight();
    $('.fa-loader-wrp').css('display','block');
    $('.page-overlay').css('display','block');
    var product_sort_by_sale = $('#sort_price').val();

    var v_arr = ''; var i = 0;
    $('.ads_Checkbox:checked').each(function () {
           if(i != 0){ v_arr = v_arr+ ','; }
           v_arr = v_arr+$(this).val();
           i++;
    });
    
    var v_arr_b = ''; var k = 0;
     $('.brands_Checkbox:checked').each(function () {
           if(k != 0){ v_arr_b = v_arr_b+ ','; }
           v_arr_b = v_arr_b+$(this).val();
           k++;
    });

    $.ajax({
            type: "post",
            url: sc_baseurl+"sale/filtersProducts",
            data :{ 'category':v_arr,'brands':v_arr_b,'product_sort_by_sale':product_sort_by_sale },
            async :true,
            success: function (response) {
                $("#get_all_cat_products").html(response);
                var new_height = $('#get_all_cat_products').innerHeight();
                $("img.lazy").lazyload({
                        effect : "fadeIn",
                        threshold : 300,
                        placeholder: 'https://www.stylecracker.com/assets/images/logo-graysclae.png'
                });
                console.log(old_height +'----'+ new_height);
                /*if(old_height > new_height){
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }*/
            },
            error: function (response) {
               
            }
    });

    $('.fa-loader-wrp').css('display','none');
    $('.page-overlay').css('display','none');
}

function funFilterPageSwiper(){}