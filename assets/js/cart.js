$( document ).ready(function() {
	$('#notify_me').hide();
	if(navigator.cookieEnabled == ''){
		alert("Please enabled cookies");
	}
	if($('.his_active').length > 0){
		$('.his_active').trigger('click');
	}
	if(getCookie('SCUniqueID')=='' || typeof getCookie('SCUniqueID')=='undefined'){
    	var sctimestamp = new Date().getTime();
    	document.cookie="SCUniqueID="+sctimestamp+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2500 12:00:00 UTC; path=/";
	}
	scUpdateCart(getCookie('SCUniqueID'),'','','','');
    /* buy now click */
	$(".cart-buy-now").click(function(){
    	var productId = $(this).attr("data-attr");
    	if(productId!=''){
    		var productSize =$('input[name=size_'+productId+']:checked').val();
    		if(productSize!='' && typeof productSize!='undefined'){
    				//var totalCartCount = parseInt($('#total_products_cart_count').html())+1;

    				//$('#total_products_cart_count').html(totalCartCount);
    				$('#productSizeSelect'+productId+'').html('');
    				openCartPage();
    				
    				scUpdateCart(getCookie('SCUniqueID'),productId,'',productSize,'2');
    				ga('send', 'pageview', '/overlay/buynow/?page=' + document.location.pathname + document.location.search + ' - '+ $('#ga_'+productId).html().trim());
    		}else{
    			$( '#productSizeSelect'+productId+'' ).removeClass().addClass('product-sizes-error');
    			//$( '#productSizeSelect'+productId+'' ).html("Select size");
    		}
    	}
	});



	$(".only-cart").click(function(){
    	var productId = $(this).attr("data-attr");
    	if(productId!=''){
    		var productSize =$('input[name=size_'+productId+']:checked').val();
    		if(productSize!='' && typeof productSize!='undefined'){
    				//var totalCartCount = parseInt($('#total_products_cart_count').html())+1;

    				//$('#total_products_cart_count').html(totalCartCount);
    				$('#productSizeSelect'+productId+'').html('');
    				//openCartPage();
    				scUpdateCart(getCookie('SCUniqueID'),productId,'',productSize,'1');
    				ga('send', 'pageview', '/overlay/addtocart/?page=' + document.location.pathname + document.location.search + ' - '+ $('#ga_'+productId).html().trim());
    		}else{
    			//$( '#productSizeSelect'+productId+'' ).html("Select size");
    			$( '#productSizeSelect'+productId+'' ).removeClass().addClass('product-sizes-error');
    		}
    	}
	});

	$('.product-sizes-list li label').on('click',function(e){
		$(this).closest('ul').find('li').each(function(e){
			$(this).find('label').removeClass('selected');
		});
		$(this).addClass('selected');
	})

	$(".product-avail-size").click(function(){
		productId = $(this).attr('name').replace('size_', '');
		if(productId!='' && typeof productId!='undefined'){
			$( '#productSizeSelect'+productId+'' ).html('');
		}
	});

	$("#uc_pincode").blur(function(){
    	get_city_name($("#uc_pincode").val());
	});

	$("#uc-close-modal").click(function(){
    	$('body #mycart').find('.close').trigger('click');
	});

	$(".view-order").click(function(){

    	if($(this).parent().attr('aria-expanded') == 'true'){
    		$(this).next().removeClass('fa fa-angle-double-down').addClass('fa fa-angle-double-right');
    	}else{
    		$(this).next().removeClass('fa fa-angle-double-right').addClass('fa fa-angle-double-down');
    	}
	});

	jQuery.validator.addMethod("indian_mob_val", function(value, element, param) {
		return (value.substring(0,1) == 9 || value.substring(0,1) == 8 || value.substring(0,1) == 7);
	});

	 $("#sc_uc_shopping_cart").validate({
         rules: {
          uc_first_name : { required : true ,noSpace: true },
          uc_last_name : { required : true ,noSpace: true },
          uc_email_id : { required : true ,noSpace: true,email: true },
          uc_mobile : { required : true ,noSpace: true,digits:true, minlength:10,maxlength:10 },
          uc_address : { required : true },
          uc_pincode : { required : true ,noSpace: true,digits:true, minlength:6,maxlength:6 },
          uc_city : { required : true },
          uc_state : { required : true ,noSpace: true,digits:true },
          pay_mode : { required : true }
        },
         messages: {
          uc_first_name : {
            required : "Enter first name",noSpace :  "Only characters are allow"
          },
          uc_last_name : {
            required : "Enter last name",noSpace :  "Only characters are allow"
          },
          uc_email_id : {
            required : "Enter email id",noSpace :  "Only characters are allow",email : "Enter valid email id"
          },
          uc_mobile : {
            required : "Enter mobile no",noSpace :  "Only characters are allow",digits:"Only digits are allow"
          },
          uc_address : {
            required : "Enter address",noSpace :  "Only characters are allow"
          },
          uc_pincode : {
            required : "Enter pincode",noSpace :  "Only characters are allow"
          },
          uc_city : {
            required : "Enter city",noSpace :  "Only characters are allow"
          },
          uc_state : {
            required : "Enter state",noSpace :  "Only characters are allow"
          },
          pay_mode : {
          	required : "Select Payment mode"
          }

         },
        submitHandler: function(form) {
        	var uc_first_name = $('#uc_first_name').val();
			var uc_last_name = $('#uc_last_name').val();
			var uc_email_id = $('#uc_email_id').val();
			var uc_mobile = $('#uc_mobile').val();
			var uc_address = $('#uc_address').val();
			var uc_pincode = $('#uc_pincode').val();
			var uc_city = $('#uc_city').val();
			var uc_state = $('#uc_state').val();
			var pay_mode_val = $("input[type='radio'][name='pay_mode']:checked").val();
			var SCUniqueID = getCookie('SCUniqueID');
			var orderval = $('#OrderTotal').val();
			var ucsecsue = $('#ucsecsue').val();
			var placeorder = 'no';
			var otpvalidat = 'no';
			var retVal = '';

        	if(ucsecsue == 0 && uc_first_name!='' && uc_last_name!='' && uc_email_id!='' && uc_mobile!='' && uc_address!='' && uc_pincode!='' && uc_city!='' && uc_state!='' && pay_mode_val!='' && SCUniqueID!=''){
	        	if(pay_mode_val != 'cod'){
	         		sc_uc_order();
	         	}else{
	         		showOtpPopUp();
	         	}
         	}else{
         			sc_uc_order();
         	}

        }



      });

	//$('[data-toggle="popover"]').popover({html:true});

	//cart
	/*	$('.popup-cart-price[data-toggle="popover"]').popover({ html:true,
	        content: function() {
	          var content = $(this).attr("data-popover-content");
	          return $(content).html();
	        }});*/


	$('.sc-popover-btn').on('mouseup',function(e){
		GetPinCode();
	});

	$('#sclogout').on('click',function(e){
		var final_cookies = getCookie('SCUniqueID');
		document.cookie="SCUniqueID="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
		window.location = sc_baseurl+"schome/logout";
	});

	$('#add_new_address').on('click',function(e){
	//	$('#collapse5').removeClass('in');
	//	$('#collapse4').addClass('in');
	$("#block4").trigger('click');
		//$("#collapse4").collapse();
		//$("#collapse5").collapse();
		$('#uc_address').val('');
		$('#uc_pincode').val('');
		$('#uc_city').val('');
		$('#uc_state').val('');
		$('#uc_mobile').val('');
	});
});

function showOtpPopUp(){
	$.ajax({
		url: sc_baseurl+"sccart/scgenrate_otp",
		type: 'POST',
		data: {'mobile_no': $('#uc_mobile').val() },
		cache :true,
		async: false,
		success: function(response) {
			$('#prompt-popup-otp').css('display','block');
			$('.page-dialog-overlay').css('display','block');
		},
		error: function(xhr) {

		}
	});
}

function sc_check_otp(){
	var pay_mode_val = $("input[type='radio'][name='pay_mode']:checked").val();
	var retVal = $('#singleOTPNumber').val();
	var uc_mobile = $('#uc_mobile').val();
	if(retVal!='' && retVal!=null && retVal!=undefined && pay_mode_val == 'cod'){

		$.ajax({
			      url: sc_baseurl+"sccart/sc_check_otp",
			      type: 'POST',
			      data: {'mobile_no': uc_mobile ,'otp_number' : retVal },
			      cache :true,
			      async: false,
			      success: function(response) {
			      		if(response == '1'){ sc_uc_order(); }else{ $('#otp_error').html('Incorrect OTP ').removeClass('hide'); }
			      },
			      error: function(xhr) {

			      }
				});
	} else{ $('#otp_error').html('Please enter valid OTP.').removeClass('hide'); }
}

/* Open the cart page */
function openCartPage(){
	$('#mycart').modal({
    	show: true
	});
	scUpdateCart(getCookie('SCUniqueID'),'','','','');
}

function paymentModeSel(elem){
	$('#paymod').val($(elem).attr('value'));
	scUpdateCart(getCookie('SCUniqueID'),'','','','');

	if($(elem).attr('value') == 'cod'){
		$('#payment-cod').show(); $('#payment-online').hide();
	}else{
		$('#payment-cod').hide(); $('#payment-online').show();
	}
}

function existingAddress(elem){
	$('#existing_shipping_add').val($(elem).attr('value'));
	scUpdateCart(getCookie('SCUniqueID'),'','','','');

	if($(elem).attr('value')!=''){
		$.ajax({
			      url: sc_baseurl+"sccart/existingAddress",
			      type: 'POST',
			      data: {'address':$(elem).attr('value')},
			      cache :true,
			      async: false,
			      success: function(response) {

			      	$("#block4").trigger('click');

			      	var obj =jQuery.parseJSON(response);
				      	$('#uc_first_name').val(obj[0].first_name);
				      	$('#uc_last_name').val(obj[0].last_name);
				      	$('#uc_mobile').val(obj[0].mobile_no);
			      		$('#uc_address').val(obj[0].shipping_address);
			      		$('#uc_pincode').val(obj[0].pincode);
			      		$('#uc_city').val(obj[0].city_name);
			      		$('#uc_state').val(obj[0].state_name);
			      },
			      error: function(xhr) {

			}
		});

	}
}


function updateProductQty(elem){
	var productQty = elem.value;
			var productInfo = $(elem).closest('li').attr('id');
			if(productQty!='' && productInfo!=''){

				$.ajax({
			      url: sc_baseurl+"sccart/productQtyUpdate",
			      type: 'POST',
			      data: {'uniquecookie':getCookie('SCUniqueID'),'productQty':productQty,'productInfo':productInfo},
			      cache :true,
			      async: false,
			      success: function(response) {
			      		scUpdateCart(getCookie('SCUniqueID'),'','','','');

			      },
			      error: function(xhr) {

			}
		});
	}
}
/* get cookie value */
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

function scUpdateCart(uniquecookie,productid,userid,productSize,frmsource){
	$('.delivery-status').html('');
	if((uniquecookie!='' && productid!='' && productSize!='') || userid!=''){

	    $.ajax({
	      url: sc_baseurl+"sccart/update_cart",
	      type: 'POST',
	      global: false,
	      async: false,
	      data: {'uniquecookie':uniquecookie,'productid':productid,'userid':userid,'productsize':productSize},
	      cache :true,
	      success: function(response) {
				/* Updated on 24-Dec-2015 by Arun/Sudha: User Product Click Track (Using Table: users_product_click_track) */ 
				if(frmsource!=''){ /* Updated on 24-Dec-2015 by Saylee: User Product Click Track (Using Table: users_product_click_track) */ 
					_targetProductClick(productid,frmsource);
				}
	      },
	      error: function(xhr) {

	      }
	    });

	}

	$.ajax({
	      url: sc_baseurl+"sccart/get_cart",
	      type: 'POST',
	      global: false,
	      async: false,
	      data: {'uniquecookie':uniquecookie,'uc_pincode':$('#uc_pincode').val(),'paymode':$("input[type='radio'][name='pay_mode']:checked").val() },
	      cache :true,
	      success: function(response) {

	      		$('#cart-items-wrp').html(response);
	      		if(response.indexOf('Your Shopping Cart Is Empty') != -1){

	      			$('.cart-with-items').addClass('hide');
	      			$('.cart-empty-wrp').removeClass('hide');
	      			$('.modal-footer').addClass('hide');
	      		}else{
	      			$('.cart-empty-wrp').addClass('hide');
					$('.cart-with-items').removeClass('hide');
					$('.modal-footer').removeClass('hide');
	      		}

	      		$('#total_products_cart_count').html('<span class="cart-count">'+$('#totalQty').val()+'</span>');
	      		$('#scOrderTotal').html('<span class="fa fa-inr"></span> '+$('#OrderTotal').val());
					//	$('.chosen-select').chosen();
					$('.selectpicker').selectpicker();
					//$('.sc-cart-popover[data-toggle="popover"]').popover({html:true, container:'.order-amount', placement:'right'});

					$('.popover-cart-price[data-toggle="popover"]').popover({ html:true,
						content: function() {
								var content = $(this).attr("data-popover-content");
			            return $(content).html();
			          }});
								$('body').on('click', function (e) {
				        $('.popover-cart-price[data-toggle="popover"]').each(function () {
				        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
				            $(this).popover('hide');
				        }
				    	});
					});
				/*$('.cart-items-wrp .cart-items-inner').slimScroll({
					height: 'auto',
					size : '4px',
					alwaysVisible: true
				});*/
			/*	if($('.cart-items-wrp .cart-items-inner').length > 0 && $('.cart-items-wrp .cart-items-inner').height() > 0){
		      		$('.cart-items-wrp .cart-items-inner').slimScroll({
						height: $('.cart-items-wrp .cart-items-inner').height(),
						size : '4px',
						alwaysVisible: true
					});
				}*/

	      },
	      error: function(xhr) {

	      }
	    });

}

function deleteProductFromCart(cart){
	var result = confirm("Are you sure to remove the item from the cart?");
	if (result) {
	$.ajax({
	      url: sc_baseurl+"sccart/deleteCartProduct",
	      type: 'POST',
	      data: {'uniquecookie':getCookie('SCUniqueID'),'cart':cart},
	      cache :true,
	      async: false,
	      success: function(response) {
	      		scUpdateCart(getCookie('SCUniqueID'),'','','');
	      		//$('#cart-items-wrp').html(response);
	      },
	      error: function(xhr) {

	      }
	    });
	}
}

function get_city_name(pincode){
	$.ajax({
	      url: sc_baseurl+"sccart/get_city_name",
	      type: 'POST',
	      data: {'pincode':pincode,'SCUniqueID':getCookie('SCUniqueID')},
	      cache :true,
	      async: false,
	      success: function(response) {
	      		$('.cod-status').html('');
	      		var data = JSON.parse(response);
	      		if(data.city!=''){
	      			$('#uc_city').val(data.city);
	      			$('#uc_state').prop('selected',false);
	      			$('#uc_state option[value='+data.state+']').prop('selected','selected');
	      		}else{
	      			$('#uc_city').val('');
	      			$('#uc_state').prop('selected',false);
	      			$('#uc_state option[value=0]').prop('selected','selected');
	      		}
	      		scUpdateCart(getCookie('SCUniqueID'),'','','');

	      },
	      error: function(xhr) {

	      }
	});
}

function sc_uc_order(){ 
  	var uc_first_name = $('#uc_first_name').val();
	var uc_last_name = $('#uc_last_name').val();
	var uc_email_id = $('#uc_email_id').val();
	var uc_mobile = $('#uc_mobile').val();
	var uc_address = $('#uc_address').val();
	var uc_pincode = $('#uc_pincode').val();
	var uc_city = $('#uc_city').val();
	var uc_state = $('#uc_state').val();
	var paymod = $("input[type='radio'][name='pay_mode']:checked").val();
	var existing_shipping_add = $("input[type='radio'][name='existing_shipping_add']:checked").val();
	var SCUniqueID = getCookie('SCUniqueID');
	var orderval = $('#OrderTotal').val();
	var ucsecsue = $('#ucsecsue').val();
	var placeorder = 'no';
	var otpvalidat = 'no';
	var retVal = '';

	if(ucsecsue == 0){
		if(orderval<=0){
			alert('Your cart is empty');
		}else if(uc_first_name!='' && uc_last_name!='' && uc_email_id!='' && uc_mobile!='' && uc_address!='' && uc_pincode!='' && uc_city!='' && uc_state!='' && paymod!='' && SCUniqueID!=''){
			  $('.cart-loader').show();
			  $('.page-dialog-overlay').css('z-index','1084');
			  $('.page-dialog-overlay').css('opacity','0.99');

			  $.ajax({
		      url: sc_baseurl+"sccart/sc_uc_order",
		      type: 'POST',
		      data: {'uc_first_name':uc_first_name,'uc_last_name':uc_last_name,'uc_email_id':uc_email_id,'uc_mobile':uc_mobile,'uc_address':uc_address,'uc_pincode':uc_pincode,'uc_city':uc_city,'uc_state':uc_state,'paymod':paymod,'SCUniqueID':SCUniqueID,'orderval':orderval,'existing_shipping_add':existing_shipping_add },
		      cache :true,
		      async: false,
		      success: function(response) {
		      	var obj = jQuery.parseJSON(response);
		      		if(obj.order=='online'){
		      			if(obj.chk_pay_mode=='card_pay'){
		      				$('#txnid').val(obj.txnid);
		      				$('#hash').val(obj.payu_hash);
		      				$('#amount').val(obj.amount);
		      				$('#firstname').val(obj.firstname+''+uc_last_name);
		      				$('#email').val(obj.email);
		      				$('#phone').val(obj.phoneno);
		      				$('#udf1').val(getCookie('SCUniqueID'));
		      				$('#udf2').val(obj.address_sc);
		      				$('#udf3').val(uc_pincode);
		      				$('#udf4').val(uc_city);
		      				$('#udf5').val(uc_state);
		      				var payuForm = document.forms.payuForm;
      						payuForm.submit();
		      			}
		      		}else if(obj.order=='fail'){
		      			alert(obj.msg);
		      		}else if(obj.order=='success'){

		      			var final_cookies = getCookie('SCUniqueID');
		      			$('body #mycart').find('.close').trigger('click');
		      			document.cookie="SCUniqueID="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
		      			//order_thankyou(final_cookies);
		      			var redirction = sc_baseurl+'Sccart/ordersuccessOrder/'+obj.msg;
		      			window.location =redirction;

		      		}

		      },
		      error: function(xhr) {

		      }
			});
			/*}*/
		}else{ alert('Please fill shipping address'); }
	}else{
		alert('Sorry, Delivery is not available for '+ucsecsue+' item(s) in your order. Please remove them to proceed');
	}

}

function order_thankyou(final_cookies){
	if(getCookie('SCUniqueID')=='' || typeof getCookie('SCUniqueID')=='undefined'){
    	var sctimestamp = new Date().getTime();
    	document.cookie="SCUniqueID="+sctimestamp+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2500 12:00:00 UTC; path=/";
	}
	$.ajax({
	      url: sc_baseurl+"sccart/get_order_info",
	      type: 'POST',
	      global: false,
	      async: false,
	      data: {'uniquecookie':final_cookies },
	      cache :true,
	      success: function(response) {
	      		scUpdateCart(getCookie('SCUniqueID'),'','','');
	      		$('#total_products_cart_count').html('<span class="cart-count">0</span>');
	      		$('#order-summary').html(response);
	      		$('#order-summary').modal({ show: true });

	      },
	      error: function(xhr) {

	      }
	});


}

function GetPinCode() {
	setTimeout(function(){
	$('.popup-pincode').find('button').click(function(e){
		var pincode = $(this).closest('.popup-pincode').find('input').val();
		var id = $(this).closest('.popup-pincode').find('input').attr('id');
		if(/^[0-9]{6,6}$/.test(pincode)){
			var loc = $(this).closest('.popup-pincode').find('input').next().next('div');
			$(this).closest('.popup-pincode').find('input').next().next('div').html('');
				id = id.replace('avil_pincode_','');
				$.ajax({
			      url: sc_baseurl+"sccart/getPinCode",
			      type: 'POST',
			      global: false,
			      async: false,
			      data: {'pincode':pincode,'id':id },
			      cache :true,
			      success: function(response) {

			      	$('#pin_msg'+id).html(response).show();

			      },
			      error: function(xhr) {

			      }
				});

		}else{

			//$(this).closest('.popup-pincode').find('input').next().next('div').html('<span style="color:red;">Please enter valid Pincode</span>');

			$(this).closest('.popup-pincode').find('.message').show().removeClass('success').addClass('error').html('Please enter valid pincode');

		}

	});



	},10);
}

function notify(id){
	$('#notify_error').html('');
	$('#notify_me_id').val(id);
	$('#notify_me').css('display','block');
	$('.page-overlay').css('display','block');
}

function notify_me(){
	var notify_me_id = $('#notify_me_id').val();
	var notify_emailid = $('#notify_emailid').val();

	if(notify_emailid!=''){
			$.ajax({
		      url: sc_baseurl+"sccart/notify_me",
		      type: 'POST',
		      global: false,
		      async: false,
		      data: {'notify_me_id':notify_me_id,'notify_emailid':notify_emailid },
		      cache :true,
		      success: function(response) {

		      	var obj =jQuery.parseJSON(response);

		      	$('#notify_error').html(obj.text);
				$('#notify_error').removeClass('hide');
				
				ga('send', 'pageview', '/overlay/notifyme/?page=' + document.location.pathname + document.location.search + ' - '+ $('#ga_'+obj.prod_id).html().trim());

		      },
		      error: function(xhr) {

		      }
			});
	}else{
		$('#notify_error').html("Please enter email-id");
		$('#notify_error').removeClass('hide');

	}
}
