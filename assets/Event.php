<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
class Event extends TT_REST_Controller {

	function __construct(){

       parent::__construct();
	   $this->load->model('Collection_model');
	   $this->load->model('Mobile_look_model');
	   $this->load->model('Event_model');
	   $this->load->library('curl'); 
   	}
	
	public function get_all_event_post(){
		
		$data['user_id'] = $this->post('user_id');
		$data['auth_token'] = $this->post('auth_token');
		$data['limit'] = $this->post('limit');
		$data['offset'] = $this->post('offset');
		$data['body_shape'] = $this->post('body_shape');
		$data['pa_pref1'] = $this->post('pa_pref1');
		$data['type'] = $this->post('type');
		
		$data['limit_cond'] = " LIMIT ".$data['offset'].",".$data['limit'];
		$data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
		$result = $this->Event_model->get_all_event($data);
		$this->success_response($result);
	}
	
	public function get_single_object_post(){
		$data['user_id'] = $this->post('user_id');
		$data['auth_token'] = $this->post('auth_token');
		$data['object_id'] = $this->post('object_id');
		$data['limit'] = $this->post('limit');
		$data['offset'] = $this->post('offset');
		$data['type'] = 'single';
		
		$data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
		$result = $this->Event_model->get_single_event($data['object_id'],$data);
		if(!empty($result)){
			$this->success_response($result);
		}else {
			$this->failed_response(1001, "No Record(s) Found");
		}
		
		
	}
	
}