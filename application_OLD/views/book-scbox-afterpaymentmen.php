<?php
   //echo $form;
   
   $scbox_pack = unserialize(SCBOX_PACKAGE);
   
   
   
   
   ?>
<style>

   .page-scbox-form .list-occassion li{
      text-align: center !important;
   }

   .form-control{
   text-transform: capitalize !important;
   }
  
   .page-scbox-form .list-skin-tone li .title-3 {
   padding-top: 15px !important;
   }
   .page-scbox-form .list-skin-tone li{width: 60px;} 
   @media(max-width:768px) {
  
   .page-scbox-form ul.list-skin-tone li {
   width: 60px !important;
   }
   }
   .btn-group.special {
   display: flex !important;
   }
   .page-scbox-form .list-skin-tone li .pallet {
   height: 40px !important;
   width: 40px !important;
   }
   .special .btn {
   flex: 1 !important;
   }
 
   .list-skin-tone li {
   width: 76px !important;
   }
   .page-scbox-form .list-bags li.active,
   .page-scbox-form .list-footweartype li.active,
   .page-scbox-form .list-heeltype li.active {
   outline: 2px solid #13D792 !important;
   cursor: default !important;
   }
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li,
   .page-scbox-form .list-dneed li {
   display: inline-block !important;
   width: 146px !important;
   margin: 10px 4px !important;
   cursor: pointer !important;
   }
   .page-scbox-form .list-fabrics li.active,
   .page-scbox-form .list-prints li.active,
   .page-scbox-form .list-dneed li.active {
   outline: 2px solid #ed1b2e !important;
   cursor: default !important;
   }
   .page-scbox-form .list-fabrics li {
   display: inline-block !important;
   width: 140px !important;
   margin: 10px 4px !important;
   cursor: pointer !important;
   }
   .btn {
   border: 1px solid #000 !important;
   color: #333 !important;
   border-radius: 0px !important;
   outline: none !important;
   }
   .btn-green.active,
   .btn-green.active.focus,
   .btn-green:active {
   border: 1px solid #13D792 !important;
   background: #13D792 !important;
   color: #fff !important;
   outline: none !important;
   }
   .btn-red.active,
   .btn-red.active.focus,
   .btn-red:active {
   border: 1px solid #ed1b2e !important;
   background: #ed1b2e !important;
   color: #fff !important;
   outline: none !important;
   }
   .choose_file {
   position: relative !important;
   display: inline-block !important;
   border-bottom: #13D792 solid 1px;
   width: 300px !important;
   padding: 4px 6px 4px 8px !important;
   color: #000 !important;
   margin-top: 2px !important;
   background: white !important;
   }
   .choose_file input[type="file"] {
   -webkit-appearance: none !important;
   position: absolute !important;
   top: 0 !important;
   left: 0 !important;
   opacity: 0 !important;
   }
   
   .hwidth {
   width: 15% !important;
   text-transform: uppercase !important;
   }
   @media (max-width: 768px) {
   .hwidth {
   width: 60% !important;
   text-transform: uppercase !important;
   }
   }
   
   @media (max-width: 768px) {
   .page-scbox-form .list-colors li,
   .page-scbox-form .list-prints li,
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li {
   width: 80px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   }
   }
   @media (max-width: 768px) {
   .page-scbox-form .list-bags li img,
   .page-scbox-form .list-footweartype li img,
   .page-scbox-form .list-heeltype li img {
   max-width: 100% !important;
   }
   }
   @media (max-width: 768px) {
   .page-scbox-form ..list-bags li .pallet,
   .page-scbox-form .list-footweartype li .pallet,
   .page-scbox-form .list-heeltype li .pallet {
   width: 50px !important;
   height: 50px !important;
   margin: 5px auto 5px !important;
   }
   }
   .form-control {
   display: inline !important;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   height: 280px !important;
   }
   .pallet img {
   width: 146px !important;
   }
   @media (max-width: 768px) {
   .page-scbox-form .list-colors li,
   .page-scbox-form .list-prints li,
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li {
   width: 135px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   width: 100% !important;
   }
   }
   .extrapad {
   margin-top: 60px !important;
   margin-bottom: 60px !important;
   }
   /*.extrapad {
  margin-top: 60px;
   margin-bottom: 40px;
   padding-top: 60px;
   }
   .extrapad1 {
  margin-top: 30px;
   margin-bottom: 40px;
   }*/
   @media (max-width: 768px) {
      .extrapad {
  
   padding-top: 80px !important;
   }
   .extrapad1 {
  margin-top: 30px !important;
   margin-bottom: 40px !important;
   }
   }
   .page-scbox-form .pa-que{
      padding: 0 0 !important;
   }
   .page-scbox-form .bigimg li{
   display: inline-block !important;
   width: 180px !important;
   margin: 0px 4px 20px !important;
   cursor: pointer !important;
   background: #fafafa !important;
   border:1px solid #dfdfdf !important;
   }
   .noborder{
   border:none !important;
   }
   ul.list-categories{
   text-transform: capitalize !important;
   }
   .page-scbox-form .item.quiz .title {
   text-transform: capitalize !important;
   }
   .title-3{
   font-size: 13px !important;
   }
   @media (max-width: 768px)
   {
   .page-scbox-form .bigimg li{
   display: inline-block !important;
   width: 125px !important;
   margin: 0px 8px 20px !important;
   cursor: pointer !important;
   background: #fafafa !important;
   border:1px solid #dfdfdf !important;
   }
   .noborder{
   border:none !important;
   }
   }
   .title-section-changed .title-changed{
   margin: 0;
   line-height: 1;
   color: #333;
   display: inline-block;
   font-weight:normal;
   font-size:13px;
   letter-spacing: 2px;
   font-style:italic;
   }
   @media (max-width: 768px)
   {
   .title-section-changed .title-changed {
   font-size: 13px;
   line-height: 1.3;
   }
   }
   .mobile-align {
   width: 28% !important;
   text-align: left !important;
   margin-left:38% !important; 
   }

   .desktop-align{
    width: 25% !important;
   text-align: left !important;
   margin-left:46% !important;
   }

   .mobile-shade {
   width: 68% !important;
   text-align: left !important;
   margin-left:22% !important; 
   }

   .desktop-shade{
    width: 25% !important;
   text-align: left !important;
   margin-left:38% !important;
   }
 label{
      margin-top: 5px !important;
   }
</style>
<div class="page-scbox-form">

        <!-- action="<?php //echo base_url(); ?>book-scbox/<?php //echo @$userdata['scbox_objectid']; ?>"-->

        <form name="scbox-form" id="scbox-form" method="post" action="">

           
            <div id="slide_1">

                <section class="section section-shadow ">
                   <!--  <div  class="box-message <?php  //echo @$order_msgclass==1  ? 'success':'error'; ?> text-bold" id="order-message"><?php //echo $order_message; ?></div> -->

                    <div class="title-2">You Are Almost Done! Just A Few More Questions To Go</div>

                    <div class="men_afterpayment category-apparel">

                        <div class="product-item extrapad section-height">

                            <div class="title-2">How Tall Are You?</div>

 <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick one)
                        </h1>
                     </div>
                            <div class="item-sizes-wrp">

                                <div class="form-group">

                                    <select name="height-height_feet" type="height-feet" class="form-control hwidth height_feet">

                                        <option value="">Select height</option>

                                        <option value="3 ft">3 ft</option>

                                        <option value="4 ft">4 ft</option>

                                        <option value="5 ft">5 ft</option>

                                        <option value="6 ft">6 ft</option>

                                    </select>

                                    <select name="height-height_inches" type="height-inches" class="form-control hwidth height_inches">

                                        <option value="">Select height</option>

                                        <option value="0 inch">0 inch</option>

                                        <option value="1 inch">1 inch</option>

                                        <option value="2 inch">2 inch</option>

                                        <option value="3 inch">3 inch</option>

                                        <option value="4 inch">4 inch</option>

                                        <option value="5 inch">5 inch</option>

                                        <option value="6 inch">6 inch</option>

                                        <option value="7 inch">7 inch</option>

                                        <option value="8 inch">8 inch</option>

                                        <option value="9 inch">9 inch</option>

                                        <option value="10 inch">10 inch</option>

                                        <option value="11 inch">11 inch</option>

                                    </select>

                                </div>

                            </div>

                            <label id="height-error" class="message error hide" for="height_ans">Please select height</label>

                        </div>

                    </div>

                    <div class="men_bags_afterpayment category-bags">

                              
 <div class="container bag-color-avoid">

    <div class="box-pa">

        <div class="pa-que">
             <span>You Selected A Bag! Could You Tell Us A Little More About The Kind Of Bag You Want?</span>
            
        </div>
        <div class="pa-que">
             <span>Colors I <i style="color: #ed1b2e;"> AVOID</i></span>
 <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
        </div>

    </div>

    <ul class="add-event list-skin-tone list-colordneed actdeact color-to-avoid-afterpayment bag-color-avoid" type="bag-color-avoid">
<li>
                     <div class="pallet" color="#8bf5cd" style="background-color:#8bf5cd;border:1px solid #8bf5cd;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#89aad5" style="background-color:#89aad5;border:1px solid #89aad5;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#bf94e4" style="background-color:#bf94e4;border:1px solid #bf94e4;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#aeb0ae" style="background-color:#aeb0ae;border:1px solid #aeb0ae;"></div>
                  </li>
                   <li>
                     <div class="pallet" color="#734021" style="background-color:#734021;border:1px solid #734021;"></div>
                  </li>
                   <li>
                     <div class="pallet" color="#2082ef" style="background-color:  #2082ef;border:1px solid #2082ef;"></div>
                  </li>
                   <li>
                     <div class="pallet" color="#0b0706" style="background-color:#0b0706;border:1px solid #0b0706;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#68dbd6"
                        style="background-color:  #68dbd6;border:1px solid #68dbd6;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#f5dc05" style="background-color:#f5dc05;border:1px solid #f5dc05;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#fffefe" style="background-color:#fffefe;border:1px solid #fffefe;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#0c4c8a" style="background-color:#0c4c8a;border:1px solid #0c4c8a;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#576331" style="background-color:#576331;border:1px solid #576331;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#041531" style="background-color:#041531;border:1px solid #041531;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#de443a" style="background-color:#de443a;border:1px solid #de443a;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#bde4f5"
                        style="background-color:#bde4f5;border:1px solid #bde4f5;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#ffbbc7"
                        style="background-color:#ffbbc7;border:1px solid #ffbbc7;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#0a6148"
                        style="background-color:#0a6148;border:1px solid #0a6148;"></div>
                  </li>
                 
                  <br>
                      <li class="sc-checkbox" style="width:220px !important;">
                         <div class="title-3" color="I like all colors">
                            <i class="icon icon-diamond"></i>I like all colors
                               <input type="checkbox">
                         </div>
                      </li> 

    </ul>
    
    <label id="coloravoid-error" class="message error hide" for="coloravoid">Please select a color</label>

</div>

                        <div class="container extrapad bag-print-avoid">

    <div class="box-pa">

        <div class="pa-que">

            <span>Prints and Patterns I <i style="color: #ed1b2e;">Avoid </i>

<!-- <i style="font-size:90%;">(Select 1 or more)</i> -->

</span>
 <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>

        </div>

    </div>

    <ul class="add-event list-colors other-type bag-print-avoid" type="bag-print-avoid">

       <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/prints/polka.jpg" alt="Polka dots">
                     </div>
                     <div class="title-3">polka dots</div>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/prints/1floral.jpg" alt="Floral">
                     </div>
                     <div class="title-3">floral</div>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/prints/stripes.jpg" alt="Stripes">
                     </div>
                     <div class="title-3">stripes</div>
                  </li>
            
                  <li>
                     <div class="pallet">
                        <img src=<?php echo base_url(); ?>assets/images/pa/male/prints/1quirky.jpg" alt="QUIRKY">
                     </div>
                     <div class="title-3">quirky</div>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/prints/1check.jpg" alt="Checks">
                     </div>
                     <div class="title-3">checks</div>
                  </li>
                
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/prints/1hound.jpg" alt="HOUNDSTOOTH">
                     </div>
                     <div class="title-3">houndstooth</div>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/prints/solid.jpg" alt="Solid">
                     </div>
                     <div class="title-3">solid</div>
                  </li>

    </ul>

    <label id="printavoid-error" class="message error hide" for="printavoid">Please select print to avoid</label>

</div>

                        

                        

                        

                    </div>


                    <div class="men_sunglasses_afterpayment category-accessories">

                        <div class="container accessories-sunglasses-container">

                            <div class="box-pa">

                                <div class="pa-que"><span>You Are Looking For Sunglasses ! Tell Us What Kind? <br>sunglasses</span>
                                 <div class="title-section-changed text-center">
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                   </div>

                            </div>

                            <ul class="add-event list-colors accessories-sunglasses-types" type="accessories-sunglasses-types">

                                <li>

                                    <div class="pallet">

                                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/sunglass/aviator.jpg">

                                    </div>

                                    <div class="title-3">aviators</div>

                                </li>

                                <li>

                                    <div class="pallet">

                                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/sunglass/wayfarer.jpg">

                                    </div>

                                    <div class="title-3">wayfarers</div>

                                </li>

                                <li>

                                    <div class="pallet">

                                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/sunglass/round.jpg">

                                    </div>

                                    <div class="title-3">round</div>

                                </li>

                                <li>

                                    <div class="pallet">

                                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/sunglass/square.jpg>">

                                    </div>

                                    <div class="title-3">square</div>

                                </li>


                            </ul>

                            <label id="sunglass-error" class="message error hide" for="sunglass">Please select atleast one sunglass</label>

                        </div>

                         <div class="product-item extrapad accessories-refelector-container">
                             <div class="box-pa">

                            <div class="pa-que">

                                <span> 
                                Reflectors</span>
                                 <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick yes or no)
                     </div>
                            </div>

                            </div>
                           

                            <ul class="add-event list-occassion purpose lespad accessories-refelector-types" type="accessories-refelector-types">

                                <li>

                                    <label class="sc-checkbox"><i class="icon icon-diamond"></i>

                                        <input type="checkbox"> <span>yes</span></label>

                                </li>

                                <li>

                                    <label class="sc-checkbox"><i class="icon icon-diamond"></i>

                                        <input type="checkbox"> <span>no</span></label>

                                </li>

                            </ul>

                            <label id="reflector-error" class="message error hide" for="reflector">Please select atleast one</label>

                        </div>

                    </div>


                </section>

            </div>

            <section class="section section-shadow">

                <div class="btns-wrp">

                    <!-- <a class="scbtn scbtn-primary" href="javascript:void(0);" onclick="ChangeStep(33);"><span>Previous</span></a> -->

                    <a class="scbtn scbtn-primary" href="javascript:void(0);" onclick="ChangeStep(5);"><span>Next</span></a>

                    <!-- <button class="scbtn scbtn-primary" onclick="slideprev(4);"><span>Previous</span></button>

<button class="scbtn scbtn-primary"  onclick="slidenext(4);"><span>Next</span></button> -->

                </div>

            </section>

    </div>

    </form>

    </div>
<style>

        .scbox-payment-options li {

            display: inline-block;

            margin-right: 10px;

        }

        

        .scbox-payment-options .sc-checkbox .icon {

            position: relative;

            top: 10px !important;

        }

        

        input[type=radio] {

            display: none;

        }

    </style>
