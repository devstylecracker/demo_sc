
<style>
 
   .page-scbox-form .list-colors li .pallet {
   height: 100% !important;
   }

  .pallet img{
    height: 100%;
  }
  
   @media (max-width: 768px) {
   .page-scbox-form .list-colors li {
   width: 180px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   width: 100% !important;
   }
   }
   
   .extrapad {
  margin-top: 20px;
   margin-bottom: 20px;
   }
   @media (max-width: 768px) {
      .extrapad {
  
   padding-top: 20px !important;
   }
   
   }
   .page-scbox-form .pa-que{
      padding: 0 0 !important;
   }
   
   ul.list-categories{
   text-transform: capitalize !important;
   }
   
   
   
   .title-section-changed .title-changed{
   margin: 0;
   line-height: 1;
   color: #333;
   display: inline-block;
   font-weight:normal;
  
   letter-spacing: 2px;
   font-style:italic;
   }
   @media (max-width: 768px)
   {
   .title-section-changed .title-changed {
  
   line-height: 1.3;
   }

   .page-scbox-form .list-colors li .title-3{

      font-size: 22px;
   }

   }
   
 label{
      margin-top: 5px !important;
   }
   @media screen and (min-width: 1920px) { 
.pa-que span{
            font-size: 40px;

   }

    .page-scbox-form .list-colors li .title-3{
      font-size: 22px;
   }
   .scbtn span {
    font-size: 22px;
    padding: 0px 12px;
    height: 45px;
    line-height: 45px;
}

    .scbtn:before, .scbtn:after{ 
      width:40px ;
   }
   
    }
@media screen and (min-width: 1280px) and (max-width: 1920px) { 
   .pa-que span{
            font-size: 40px;

   }
    .page-scbox-form .list-colors li .title-3{
      font-size: 22px;
   }
   .scbtn span {
    font-size: 22px;
    padding: 0px 12px;
    height: 45px;
    line-height: 45px;
}

    .scbtn:before, .scbtn:after{ 
      width:40px ;
   }
  


    }

@media screen and (max-width: 1280px) { 
  .pa-que span{
            font-size: 30px;

   }
   .scbtn span {
    font-size: 22px;
    padding: 0px 12px;
    height: 45px;
    line-height: 45px;
}
 .page-scbox-form .list-colors li .title-3{
      font-size: 22px;
   }
    .scbtn:before, .scbtn:after{ 
      width:40px ;
   }
   


    }
   
.page-scbox .section-banner {
  
    background: #F4F3F8 url(https://www.stylecracker.com/assets/images/scbox/webban.jpg) right center no-repeat !important;
}


.title-section > .title span{
  border-bottom: 1px solid #13d492 !important;
}
.page-scbox .title-section .subtitle{
  border-bottom: 1px solid #13d492 !important;
}

/* make keyframes that tell the start state and the end state of our object */
@-webkit-keyframes fadeIn { from { opacity:0; } to { opacity:1; } }
@-moz-keyframes fadeIn { from { opacity:0; } to { opacity:1; } }
@keyframes fadeIn { from { opacity:0; } to { opacity:1; } }

.fade-in {
  opacity:0;  /* make things invisible upon start */
  -webkit-animation:fadeIn ease-in 1;  /* call our keyframe named fadeIn, use animattion ease-in and repeat it only 1 time */
  -moz-animation:fadeIn ease-in 1;
  animation:fadeIn ease-in 1;

  -webkit-animation-fill-mode:forwards;  /* this makes sure that after animation is done we remain at the last keyframe value (opacity: 1)*/
  -moz-animation-fill-mode:forwards;
  animation-fill-mode:forwards;

  -webkit-animation-duration:1s;
  -moz-animation-duration:1s;
  animation-duration:1s;
}

.fade-in.one {
  -webkit-animation-delay: 1.6s;
  -moz-animation-delay: 1.6s;
  animation-delay: 1.6s;
}

.fade-in.two {
  -webkit-animation-delay: 2s;
  -moz-animation-delay:2s;
  animation-delay: 2s;
}

.fade-in.three {
  -webkit-animation-delay: 2.6s;
  -moz-animation-delay: 2.6s;
  animation-delay: 2.6s;
}



</style>

<div class="page-scbox-form base-container">
   <!-- action="<?php //echo base_url(); ?>book-scbox/<?php //echo @$userdata['scbox_objectid']; ?>"-->
   <form name="scbox-form" id="scbox-form" method="post" action="">
      
      <div id="slide_2" class="" style="padding-top: 100px;">
       <section class="section-shadow">
    <div class="container">
     
        <div class="row">
         
          <div class="section col-md-8 col-sm-6 col-xs-12">
            <div class="title-section home-patch">
              <h1 class="title" style="font-size: 21px;line-height: 1.5;text-align: left;padding-left: 10px;">
            <b>A Box Full of Fashion.<br /> That you are Guaranteed to Love.<br></b>
            <p style="font-size: 21px;line-height: 1.5;text-align: left;border-bottom: 1px solid #13d492;">Put together by our celebrity Stylists.<br /> DELIVERED STRAIGHT TO YOUR DOORSTEP.
          </p>
              <span style="font-weight:normal;font-size: 15px; margin: 10px 0 15px;padding-bottom: 15px;text-transform: left;letter-spacing: 2px;">Simple. Stylish. Stress Free.</span>
        </h1>
              
            </div>
          </div>
          <div class="col-md-4 col-sm-3 col-xs-12" style="padding-bottom: 20px;">
            <img src="https://www.stylecracker.com/assets/images/scbox/webban.jpg" style="width: 350px;">
          </div>
        </div>
      
  </div>
      
         <!-- class="<?php //echo ( @$_GET['step']==2) ? '' : 'hide'; ?>"> -->
         <div id="question_1_1" class="pa-slide-box body-shape category-apparel section-pa">
               <div class="container">
      
                  <div class="row wow bounceIn" data-wow-duration="3s">
                    <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                      <div class="box-video">
                          <div class="video-wrp">
                       <iframe width="100%" height="400" src="https://www.youtube.com/embed/VPrI2rk7FU4?rel=0&&autoplay=1" frameborder="0" allowfullscreen></iframe>
                      <!-- <iframe width="640" height="360" src="https://www.youtube.com/embed/dHzTnCwvkxQ" frameborder="0" allowfullscreen></iframe> -->
                      <!-- <iframe width="640" height="360" src="https://www.youtube.com/embed/NYQIaHWpARY?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe> -->
                      </div>
                      
                        </div>
                    </div>
                 
                </div>
              </div>  
             </div>
             <div id="question_1_9" class="container extrapad section-top-fit-like-this">
<div class="pa-que">
<span>
Gender

</span>
</div>
<ul class="add-event list-colors fit-you-like actdeact box-container single-selection" type="user-gender">

<li value="Women">
<div class="pallet fade-in one">
<img src="<?php echo base_url(); ?>assets/borough/women.jpg">
</div>
<div class="title-3 fade-in one">WOMEN</div>
</li>
<li value="Men">
<div class="pallet fade-in two">
<img src="<?php echo base_url(); ?>assets/borough/1men.jpg">
</div>
<div class="title-3 fade-in two">MEN</div>
</li>
</ul>
<label id="topfit_ans-error" class="message error hide hidden-xs hidden-sm" for="topfit_ans">Please select gender</label>
</div>
         
          
       </div>
   </form>
</div>

<!-- BUTTON -->
<style type="text/css">
    #footer {
  position: fixed;
  right: 0;
  bottom: 0;
  left: 0;
  background-color: #fff;
  visibility: hidden;
}
</style>

 <section class="section section-shadow" id="footer" style="text-align: center;">

    <div class="btns-wrp">

        <a class="scbtn scbtn-primary btn-prev"><span>Previous</span></a>

        <a class="scbtn scbtn-primary btn-next"><span>Next</span></a>

       

    </div>

 </section>
<script type="text/javascript">
    $(document).ready(function(e){
        console.log("STEP 1....");
        intCurrentScreen = 1;
        Initialize();
    });
</script>

