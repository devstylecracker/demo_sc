<style>
    
  
  .page-scbox-form .list-colors li {
   display: inline-block !important;
   width: 180px !important;
   margin: 10px 4px !important;
   cursor: pointer !important;
   background: #e4fbf3;
   }
  
  .pallet img{
  height: 100%;
 }
 
   .form-control {
   display: inline !important;
   }
   .page-scbox-form .list-colors li .pallet {
   height: 280px !important;
   }
   
   @media (max-width: 768px) {
   .page-scbox-form .list-colors li
    {
   width: 150px !important;
   text-align: center !important;
   margin: 10px 50px !important;
   background: #e4fbf3;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   width: 100% !important;
   }

    #footer {
  position: absolute;
  right: 0;
  bottom: 0;
  left: 0;
}
   }
   
   @media (max-width: 768px) {
      .extrapad {
  
   padding-top: 20px !important;
   }
   
   }
   .page-scbox-form .pa-que{
      padding: 0 0 !important;
   }
   
   
   ul.list-categories{
   text-transform: capitalize !important;
   }
   
   
   .title-section-changed .title-changed{
   margin: 0;
   line-height: 1;
   color: #333;
   display: inline-block;
   font-weight:normal;
   letter-spacing: 2px;
   font-style:italic;
   }
   @media (max-width: 768px)
   {
   .title-section-changed .title-changed {
   line-height: 1.3;
   }
   .page-scbox-form .list-colors li .title-3{

      font-size: 22px;
   }
   }
   
 label{
      margin-top: 5px !important;
   }
   @media screen and (min-width: 1920px) { 
.pa-que span{
            font-size: 22px;

   }

 
   .scbtn span {
    font-size: 22px;
    padding: 0px 12px;
    height: 45px;
    line-height: 45px;
}.page-scbox-form .list-colors li .title-3{

      font-size: 22px;
   }

    .scbtn:before, .scbtn:after{ 
      width:40px ;
   }
    }
@media screen and (min-width: 1280px) and (max-width: 1920px) { 
   .pa-que span{
            font-size: 22px;

   }
   
   .scbtn span {
    font-size: 22px;
    padding: 0px 12px;
    height: 45px;
    line-height: 45px;
}.page-scbox-form .list-colors li .title-3{

      font-size: 22px;
   }

    .scbtn:before, .scbtn:after{ 
      width:40px ;
   }


    }

@media screen and (max-width: 1280px) { 
  .pa-que span{
            font-size: 22px;

   }
   .scbtn span {
    font-size: 22px;
    padding: 0px 12px;
    height: 45px;
    line-height: 45px;
}

    .scbtn:before, .scbtn:after{ 
      width:40px ;
   }
.page-scbox-form .list-colors li .title-3{

      font-size: 22px;
   }


    }
.img-border{
  padding: 5px;width: 100%;height: 100px;
  border-bottom: 1px solid #ccc;
}

.img-border img{
 width: 50px;float: left;height: 100%;
}
/* make keyframes that tell the start state and the end state of our object */
@-webkit-keyframes fadeIn { from { opacity:0; } to { opacity:1; } }
@-moz-keyframes fadeIn { from { opacity:0; } to { opacity:1; } }
@keyframes fadeIn { from { opacity:0; } to { opacity:1; } }

.fade-in {
  opacity:0;  /* make things invisible upon start */
  -webkit-animation:fadeIn ease-in 1;  /* call our keyframe named fadeIn, use animattion ease-in and repeat it only 1 time */
  -moz-animation:fadeIn ease-in 1;
  animation:fadeIn ease-in 1;

  -webkit-animation-fill-mode:forwards;  /* this makes sure that after animation is done we remain at the last keyframe value (opacity: 1)*/
  -moz-animation-fill-mode:forwards;
  animation-fill-mode:forwards;

  -webkit-animation-duration:1s;
  -moz-animation-duration:1s;
  animation-duration:1s;
}

.fade-in.one {
  -webkit-animation-delay: 0.5s;
  -moz-animation-delay: 0.5s;
  animation-delay: 0.5s;
}

.fade-in.two {
  -webkit-animation-delay: 0.7s;
  -moz-animation-delay:0.7s;
  animation-delay: 0.7s;
}

.fade-in.three {
  -webkit-animation-delay:  0.9s;
  -moz-animation-delay:  0.9s;
  animation-delay: 0.9s;
}
.fade-in.four {
  -webkit-animation-delay:  1.1s;
  -moz-animation-delay:  1.1s;
  animation-delay: 1.1s;
}
</style>

<div class="page-scbox-form" style="padding-top: 100px;padding-bottom: 100px;">
   <!-- action="<?php //echo base_url(); ?>book-scbox/<?php //echo @$userdata['scbox_objectid']; ?>"-->
   <form name="scbox-form" id="scbox-form" method="post" action="">
     <section class="section section-shadow">  
      <div id="slide_2" class="">
          <div id="question_1_9" class="container extrapad section-top-fit-like-this">
<div class="pa-que">
<span>
Style Preference
<h3>Pick Upto <span style="color: #13D792;">2</span></h3>
</span>
</div>
<ul class="add-event list-colors box-container limited-selection" type="style-preference" selection-limit="2">
<li value='Casual' class="fade-in one">
<div class="pallet ">
<img src="<?php echo base_url(); ?>assets/borough/mlook/casual.jpg" alt="FITTED">
</div>
<div class="title-3 fade-in one">Casual</div>
</li>
<li value='Athleisure' class="fade-in two">
<div class="pallet ">
<img src="<?php echo base_url(); ?>assets/borough/mlook/2atleisure.jpg" alt="RELAXED">
</div>
<div class="title-3 fade-in two">Athleisure</div>
</li>
<li value="Bold" class="fade-in three">
<div class="pallet ">
<img src="<?php echo base_url(); ?>assets/borough/mlook/1bold.jpg" alt="FITTED">
</div>
<div class="title-3 fade-in three">Bold</div>
</li>
<li value="Well Groomed" class="fade-in four">
<div class="pallet ">
<img src="<?php echo base_url(); ?>assets/borough/mlook/well.jpg" alt="RELAXED">
</div>
<div class="title-3 fade-in four">Well Groomed</div>
</li>
</ul>
<label id="topfit_ans-error" class="message error hide hidden-xs hidden-sm" for="topfit_ans">Please select gender</label>
</div>
             


             <!-- <div id="question_1_9" class="container section-top-fit-like-this" style="padding-top: 100px;">
<div class="pa-que">
<span>
Accessories Preference

</span>
</div>
<ul class="add-event list-colors box-container multi-selection" type="jewellery-type">
<li value="Statement">
<div class="pallet fade-in one">
<img src="<?php echo base_url(); ?>assets/borough/tie.jpg" alt="FITTED">
</div>
<div class="title-3 fade-in one">Ties</div>
</li>
<li value="Minimal">
<div class="pallet fade-in two">
<img src="<?php echo base_url(); ?>assets/borough/sunglass.jpg" alt="RELAXED">
</div>
<div class="title-3 fade-in two">Sunglasses</div>
</li>
<li value="Bold">
         <div class="pallet fade-in three">
         <img src="<?php echo base_url(); ?>assets/borough/ring.jpg" alt="RELAXED">
         </div>
         <div class="title-3 fade-in three">Rings</div>
         </li>
</ul>
<label id="topfit_ans-error" class="message error hide hidden-xs hidden-sm" for="topfit_ans">Please select gender</label>
</div> -->
</div>
</section>
</form>
</div>


 <section class="section section-shadow" id="footer" style="text-align: center;">

    <div class="btns-wrp">

        <a class="scbtn scbtn-primary btn-prev"><span>Previous</span></a>

        <a class="scbtn scbtn-primary btn-next"><span>Next</span></a>

       

    </div>

 </section>
<script type="text/javascript">
    $(document).ready(function(e){
        console.log("STEP 5....");
        intCurrentScreen = 5; 
        Initialize();
    });
</script>

