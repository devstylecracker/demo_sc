<style>
    
  
  .page-scbox-form .list-colors li {
   display: inline-block !important;
   width: 200px !important;
   margin: 3px 25px !important;
   cursor: pointer !important;
   background: none;
   }
  
  
 
   .form-control {
   display: inline !important;
   }
   .page-scbox-form .list-colors li .pallet {
   height: 100% !important;
   }
   
   @media (max-width: 768px) {
   .page-scbox-form .list-colors li
    {
   width: 185px !important;
   text-align: center !important;
   margin: 3px 25px !important;
   background: none;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   width: 100% !important;
   }
   }
   
   @media (max-width: 768px) {
      .extrapad {
  
   padding-top: 20px !important;
   }
   .extrapad1 {
  margin-top: 30px !important;
   margin-bottom: 40px !important;
   }
   }
   .page-scbox-form .pa-que{
      padding: 0 0 !important;
   }
   
   
   ul.list-categories{
   text-transform: capitalize !important;
   }
   
   
   .title-section-changed .title-changed{
   margin: 0;
   line-height: 1;
   color: #333;
   display: inline-block;
   font-weight:normal;
  
   letter-spacing: 2px;
   font-style:italic;
   }
   @media (max-width: 768px)
   {
   .title-section-changed .title-changed {
   
   line-height: 1.3;
   }
   .page-scbox-form .list-colors li .title-3{
    font-size: 15px;
   }
   label.sc-checkbox span{
  font-size: 15px;
  text-transform: uppercase;
}
   }
   
 label{
      margin-top: 5px !important;
   }
   @media screen and (min-width: 1920px) { 
.pa-que span{
            font-size: 15px;

   }

   .page-scbox-form .list-colors li .title-3{
      font-size: 15px;
   }
   .scbtn span {
    font-size: 15px;
    padding: 0px 12px;
    height: 45px;
    line-height: 45px;
}

    .scbtn:before, .scbtn:after{ 
      width:40px ;
   }
   label.sc-checkbox span{
  font-size: 15px;text-transform: uppercase;
}
    }
@media screen and (min-width: 1280px) and (max-width: 1920px) { 
   .pa-que span{
            font-size: 15px;

   }
  .page-scbox-form .list-colors li .title-3{
      font-size: 15px;
   }
   .scbtn span {
    font-size: 15px;
    padding: 0px 12px;
    height: 45px;
    line-height: 45px;
}

    .scbtn:before, .scbtn:after{ 
      width:40px ;
   }

label.sc-checkbox span{
  font-size: 15px;text-transform: uppercase;
}
    }

@media screen and (max-width: 1280px) { 
  .pa-que span{
            font-size: 15px;

   }
   .scbtn span {
    font-size: 15px;
    padding: 0px 12px;
    height: 45px;
    line-height: 45px;
}
.page-scbox-form .list-colors li .title-3{
      font-size: 15px;
   }
    .scbtn:before, .scbtn:after{ 
      width:40px ;
   }

label.sc-checkbox span{
  font-size: 15px;text-transform: uppercase;
}

    }
.img-border{
  padding: 5px;width: 100%;height: 100px;
  border-bottom: 1px solid #ccc;
}

.img-border img{
 width: 50px;float: left;height: 100%;
}

/* make keyframes that tell the start state and the end state of our object */
@-webkit-keyframes fadeIn { from { opacity:0; } to { opacity:1; } }
@-moz-keyframes fadeIn { from { opacity:0; } to { opacity:1; } }
@keyframes fadeIn { from { opacity:0; } to { opacity:1; } }

.fade-in {
  opacity:0;  /* make things invisible upon start */
  -webkit-animation:fadeIn ease-in 1;  /* call our keyframe named fadeIn, use animattion ease-in and repeat it only 1 time */
  -moz-animation:fadeIn ease-in 1;
  animation:fadeIn ease-in 1;

  -webkit-animation-fill-mode:forwards;  /* this makes sure that after animation is done we remain at the last keyframe value (opacity: 1)*/
  -moz-animation-fill-mode:forwards;
  animation-fill-mode:forwards;

  -webkit-animation-duration:1s;
  -moz-animation-duration:1s;
  animation-duration:1s;
}

.fade-in.one {
  -webkit-animation-delay: 0.5s;
  -moz-animation-delay: 0.5s;
  animation-delay: 0.5s;
}

.fade-in.two {
  -webkit-animation-delay: 1.1s;
  -moz-animation-delay:1.1s;
  animation-delay: 1.1s;
}

.fade-in.three {
  -webkit-animation-delay:  1.8s;
  -moz-animation-delay:  1.8s;
  animation-delay: 1.8s;
}
</style>

<div class="page-scbox-form">
   <!-- action="<?php //echo base_url(); ?>book-scbox/<?php //echo @$userdata['scbox_objectid']; ?>"-->
   <form name="scbox-form" id="scbox-form" method="post" action="">

      <div id="slide_2" class="" style="padding-top: 100px;padding-bottom: 100px;">
        <section class="section section-shadow base-container">
         <div id="question_1_9" class="container section-top-fit-like-this">
            <div class="list-colors row" type="" style="outline: none;">
            
                     <div class="col-lg-4 col-md-4 col-sm-4">
                        <ul class="add-event list-categories box-container size-selection" type="top-size">
                           <div class="pa-que">
                              <span>
                              Top Size<br><br> Pick <span style="color: #13D792;">One</span>

                              </span>
                           </div>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="xs/uk 4/us 2/eu 32">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>xs/uk 4/us 2/eu 32</span></label>
                              <br>
                           </li>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="s/uk 6/us 4/eu 34">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>s/uk 6/us 4/eu 34</span></label>
                              <br>
                           </li>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="s/uk 8/us 6/eu 36">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>s/uk 8/us 6/eu 36</span></label>
                              <br>
                           </li>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="m/uk 10/us 8/eu 38">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>m/uk 10/us 8/eu 38</span></label>
                              <br>
                           </li>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="m/uk 12/us 10/eu 40">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>m/uk 12/us 10/eu 40</span></label>
                              <br>
                           </li>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="l/uk 14/us 12/eu 42">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>l/uk 14/us 12/eu 42</span></label>
                              <br>
                           </li>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="l/uk 16/us 14/eu 44">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>l/uk 16/us 14/eu 44</span></label>
                              <br>
                           </li>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="xl/uk 18/us 16/eu 46">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>xl/uk 18/us 16/eu 46</span></label>
                              <br>
                           </li>
                            <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="xxl/uk 20/us 18/eu 48">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>xxl/uk 20/us 18/eu 48</span></label>
                              <br>
                           </li>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="xxxl/uk 22/us 20/eu 50">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>xxxl/uk 22/us 20/eu 50</span></label>
                              <br>
                           </li>
                        </ul>
                  <label id="topfit_ans-error" class="message error hide hidden-xs hidden-sm" for="topfit_ans">Please select gender</label>
               </div>
        
    
                     <div class="col-lg-4 col-md-4 col-sm-4">
                        <ul class="add-event list-categories box-container size-selection" type="bottom-size">
                            <div class="pa-que">
                              <span>
                              Bottom Size<br><br> Pick <span style="color: #13D792;">One</span>

                              </span>
                           </div>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="xs/25">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>xs/25</span></label>
                              <br>
                           </li>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="s/26">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>s/26</span></label>
                              <br>
                           </li>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="m/28">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>m/28</span></label>
                              <br>
                           </li>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="l/30">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>l/30</span></label>
                              <br>
                           </li>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="xl/32">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>xl/32</span></label>
                              <br>
                           </li>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="xxl/34">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>xxl/34</span></label>
                              <br>
                           </li>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="xxxl/36">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>xxxl/36</span></label>
                              <br>
                           </li>
                        </ul>
                        <label id="topfit_ans-error" class="message error hide hidden-xs hidden-sm" for="topfit_ans">Please select gender</label>
                   </div>
            
                     <div class="col-lg-4 col-md-4 col-sm-4">
                        <ul class="add-event list-categories box-container size-selection" type="footwear-size" >
                            <div class="pa-que">
                              <span>
                              Footwear Size<br><br> Pick <span style="color: #13D792;">One</span>

                              </span>
                           </div>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="uk 3/us 5/eu 36">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>uk 3/us 5/eu 36</span></label>
                              <br>
                           </li>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="uk 4/us 6/eu 37">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>uk 4/us 6/eu 37</span></label>
                              <br>
                           </li>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="uk 5/us 7/eu 38">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>uk 5/us 7/eu 38</span></label>
                              <br>
                           </li>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="uk 6/us 8/eu 39">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>uk 6/us 8/eu 39</span></label>
                              <br>
                           </li>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="uk 7/us 9/eu 41">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>uk 7/us 9/eu 41</span></label>
                              <br>
                           </li>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="uk 8/us 10/eu 42">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>uk 8/us 10/eu 42</span></label>
                              <br>
                           </li>
                           <li class="wow bounceIn" data-wow-duration="1s" style="border: 1px solid #13D792;" value="uk 9/us 11/eu 43">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>uk 9/us 11/eu 43</span></label>
                              <br>
                           </li>
                        </ul>
                  <label id="topfit_ans-error" class="message error hide hidden-xs hidden-sm" for="topfit_ans">Please select gender</label>
                  </div>
            
         </div>
      </div>
      
      <div id="question_1_9" class="container section-top-fit-like-this">
         <div class="pa-que">
         <span>
        I Want It For...<br><br>
        Pick <span style="color: #13D792;">One</span>
         </span>
         </div>
         <ul class="add-event list-colors box-container single-selection" type="box-occasion">
         <li value="Work">
         <div class="pallet fade-in one">
         <img src="<?php echo base_url(); ?>assets/borough/owomen/work.jpg" alt="FITTED">
         </div>
         <div class="title-3 fade-in one">Work</div>
         </li>
         <li value="Party">
         <div class="pallet fade-in two">
         <img src="<?php echo base_url(); ?>assets/borough/owomen/party.jpg" alt="RELAXED">
         </div>
         <div class="title-3 fade-in two">Party</div>
         </li>
         <li value="Casual">
         <div class="pallet fade-in three">
         <img src="<?php echo base_url(); ?>assets/borough/owomen/casual.jpg" alt="RELAXED">
         </div>
         <div class="title-3 fade-in three">Casual</div>
         </li>
         </ul>
         <label id="topfit_ans-error" class="message error hide hidden-xs hidden-sm" for="topfit_ans">Please select gender</label>
      </div>
        </section>
     </div>
     
   </form>
</div> 
<!-- BUTTON -->
<style type="text/css">
    #footer {
  position: fixed;
  right: 0;
  bottom: 0;
  left: 0;
  background-color: #fff;
}
</style>

 <section class="section section-shadow" id="footer" style="text-align: center;">

    <div class="btns-wrp">

        <a class="scbtn scbtn-primary btn-prev"><span>Previous</span></a>

        <a class="scbtn scbtn-primary btn-next"><span>Next</span></a>

       

    </div>

 </section>  
<script type="text/javascript">
    $(document).ready(function(e){
        console.log("STEP 4....");
        intCurrentScreen = 4; 
        Initialize();
    });
</script>