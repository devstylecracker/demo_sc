<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="index, follow">
  <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/iconfonts/style.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/plugins/swiper/css/swiper.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/plugins-settings.css?v=1.004" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/common.css?v=1.009" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/btns.css?v=1.004" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/custom.css?v=1.042" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/mobile.css?v=1.004" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/keyboard.css" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900" rel="stylesheet">
  <link rel="manifest" href="/manifest.json">
  
  <script type="text/javascript">
     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-97550861-1', 'auto');//for scnest.com
          ga('require','linkid','linkid.js');
          ga('require', 'displayfeatures');
          ga('send', 'pageview');

          <?php if(isset($_GET['search']) && $_GET['search'] != ""){echo "ga('send', 'event', 'Search', 'keyword', '".$_GET['search']."');";} ?>

          var sc_user_id = "<?php echo $this->session->userdata('user_id')!='' ? $this->session->userdata('user_id') : ''; ?>";
          var sc_invite_utm_source = "<?php echo $this->session->userdata('utm_source')!='' ? $this->session->userdata('utm_source') : ''; ?>";
          var CE_SNAPSHOT_NAME = "Looks";
        var sc_baseurl = "<?php echo base_url(); ?>";
          setTimeout(function(){var a=document.createElement("script");
          var b=document.getElementsByTagName("script")[0];
          a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0040/4266.js?"+Math.floor(new Date().getTime()/3600000);
          a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);

  </script>
  </head>
  <body>

   <!-- //FB Initialization
  // FB APP Note -->
    <div id="fb-root"></div>
    <script>
         window.fbAsyncInit = function() {
            FB.init({
            appId      : '1694990140532594', // Live
            //appId      : '781978078605154', // SC Nest
            xfbml      : true,
            version    : 'v2.10' // Live
            //version    : 'v2.5' // SC nest
            });
          };

        (function(d, s, id){
           var js, fjs = d.getElementsByTagName(s)[0];
           if (d.getElementById(id)) {return;}
           js = d.createElement(s); js.id = id;
           js.src = "//connect.facebook.net/en_US/sdk.js";
           fjs.parentNode.insertBefore(js, fjs);
         }(document, 'script', 'facebook-jssdk'));
    </script>

    <!-- Facebook Pixel Code -->
  <script>
  !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
  n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
  document,'script','https://connect.facebook.net/en_US/fbevents.js');

  fbq('init', '1694990140532594');
  fbq('track', "PageView");
  </script>
  <noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1694990140532594&ev=PageView&noscript=1"
  /></noscript>
<!-- End Facebook Pixel Code -->  

<!-- GA Ecommerce Tracking Code -->
<?php 
// code for ga_ecommerce 
$order_id = @$_GET['order'];
//if(@$_GET['order']!='' && @$_GET['step']==6)
if(@$_GET['order']!='' && trim(@$this->uri->segment(2))=='step4')
{
  $res = $this->cartnew_model->su_get_order_info($order_id);

  $brand_price = $this->cartnew_model->su_get_order_price_info($order_id);
    //echo '<pre>';print_r($brand_price);exit;
    $paymod = $brand_price[0]['pay_mode'];
    if($paymod == 1) $paymentMethod = 'cod'; else $paymentMethod = 'online';
    $trans = '';
    $items = array();
    $shipping_amt = $brand_price[0]['shipping_amount']>0 ? $brand_price[0]['shipping_amount'] : 0 ;
    $tax = $brand_price[0]['order_tax_amount']>0 ? $brand_price[0]['order_tax_amount'] : 0 ;

    //-------------------------------------GA Ecommerce Tracking Code -----------------------------------------------------------------------------------------------------------
    $i=0;
    $affiliation = '';
    $brandName = '';
    foreach($res as $val)
    {
        if($affiliation!='')
        {
          if($brandName != $val['company_name'])
          {
              $affiliation = $affiliation."-".$val['company_name'];
          }
           $brandName = $val['company_name'];
          
        }else
        {
            $brandName = $val['company_name'];
           $affiliation = $val['company_name'];
        }
      // code for items array 
      if(empty($items))
        {
          $items[$i] = array('sku'=>$val['product_id'], 'name'=>$val['name'], 'category'=>'scbox', 'price'=>$val['product_price'], 'quantity'=>$val['product_qty'],'currency'=> 'INR');
        }else
        {
             $items[$i] = array('sku'=>$val['product_id'], 'name'=>$val['name'], 'category'=>'scbox', 'price'=>$val['product_price'], 'quantity'=>$val['product_qty'],'currency'=> 'INR');
        }
        $i++;
    }
    // code for trans array 
    $trans = array('id'=>$order_id, 'affiliation'=>$affiliation,'revenue'=>$brand_price[0]['order_total'], 'shipping'=>$shipping_amt, 'tax'=>$tax,'city'=>$brand_price[0]['city_name'],'state'=>$brand_price[0]['city_name'],'currency'=> 'INR');
  }
    ?>
<script type="text/javascript">
<?php  if(!empty($trans) &&  !empty($items))
      {
  ?>
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-97550861-1']);
  _gaq.push(['_trackPageview','/book-scbox/1?order=']);
  _gaq.push(['_addTrans',
    '<?=$trans['id']?>',           // transaction ID - required
    '<?=$trans['affiliation']?>',  // affiliation or store name
    '<?=$trans['revenue']?>',          // total - required
    '<?=$trans['tax']?>',           // tax
    '<?=$trans['shipping']?>',              // shipping
    '<?=$trans['city']?>',       // city
    '<?=$trans['state']?>',     // state or province
    '<?=$trans['currency']?>'             // country
  ]);

   // add item might be called for every item in the shopping cart
   // where your ecommerce engine loops through each item in the cart and
   // prints out _addItem for each
  _gaq.push(['_addItem',
    '<?=$trans['id']?>',           // transaction ID - required
    '<?=$items[0]['sku']?>',           // SKU/code - required
    '<?=$items[0]['name']?>',        // product name
    '<?=$items[0]['category']?>',   // category or variation
    '<?=$items[0]['price']?>',          // unit price - required
    '<?=$items[0]['quantity']?>'               // quantity - required
  ]);
  _gaq.push(['_trackTrans']); //submits transaction to the Analytics servers

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
<?php } ?>
</script>
<!--GA Ecommerce Tracking Code -->

<script>
var base_url ="<?php echo base_url(); ?>";
var sc_beatout_email_id = "<?php echo $this->session->userdata('email')!='' ? $this->session->userdata('email') : ''; ?>";
var sc_beatout_username = "<?php echo $this->session->userdata('user_name')!='' ? $this->session->userdata('user_name') : ''; ?>";
var sc_beatout_user_id = "<?php echo $this->session->userdata('user_id')!='' ? $this->session->userdata('user_id') : ''; ?>";
var sc_gender = "<?php echo $this->session->userdata('gender')!='' ? $this->session->userdata('gender') : ''; ?>";
if(sc_gender == 1) {sc_beatout_gender ='women'; } else sc_beatout_gender ='men'; 
</script>
<!-- popover -->
      <?php
     /* $popup = $this->home_model->get_popup();
      if(!empty($popup)){
        foreach($popup as $val){
          if(current_url() == $val['page_link'] || $val['page_link']==''){
            if($val['popup_interval_chosen'] == 1){
              setcookie('popup_timer',30*60000, time() + (86400 * 30), "/");
            }else if($val['popup_interval_chosen'] == 2){
              setcookie('popup_timer',40*60000, time() + (86400 * 30), "/");
            }else if($val['popup_interval_chosen'] == 3){
              setcookie('popup_timer',50*60000, time() + (86400 * 30), "/");
            }else if($val['popup_interval_chosen'] == 4){
              setcookie('popup_timer',60*60000, time() + (86400 * 30), "/");
            }else if($val['popup_interval_chosen'] == 5){
              setcookie('popup_timer',90*60000, time() + (86400 * 30), "/");
            }*/

      ?>
     <!--  <div id="slideover" class="sc-slideover top" style="display:none;">
        <div class="slideover-inner">
          <a href="<?php echo $val['target_link']; ?>" onclick="ga('send', 'event', 'Web Popup', 'clicked', '<?php echo $val['popup_name']; ?>');" target="_blank"><img src="<?php echo base_url(); ?>assets/images/popups/desktop/<?php echo $val['desktop_img']; ?>" /></a>
        </div>
        <div class="close">
          <i class="fa fa-close1 fa-times-circle"></i>
        </div>
      </div> -->
      <?php // } } } ?>
<!-- //popover -->
  <?php 
  if($this->uri->segment(1)=='scbox' || $this->uri->segment(1)=='')
  {
       $this->load->view('common/nav_view_scbox'); 
  }
  else
  {
        $this->load->view('common/nav_view');
  }

  ?>  
<div class="wrapper">
  <script type="text/javascript">
    var sc_baseurl = "<?php echo base_url(); ?>";
  </script>
