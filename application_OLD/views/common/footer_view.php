  <style type="text/css">
  .footer .box-social {
    border-right: none;
    height: 60px;
}
</style>
  <footer id="footer" class="footer footer-bottom section-shadow section">
  <div class="container-fluid md-only">
    <div class="row">
      <div class="col-md-4">
    
      </div>

      <div class="col-md-4">
       
      </div>
      <div class="col-md-12" style="text-align: center;">
        <div class="box-social">
          <div class="title">
            Follow us on
          </div>
          <ul class="list-inline">
            <li class="facebook">
              <a target="_blank" href="https://www.facebook.com/StyleCracker">
                <i class="fa fa-facebook-official"></i> Facebook</a>
            </li>
            <li class="instagram">
              <a target="_blank" href="https://instagram.com/stylecracker"> <i class="fa fa-instagram"></i> Instagram</a>
            </li>
            <li class="twitter">
              <a target="_blank" href="https://twitter.com/Style_Cracker"> <i class="fa fa-twitter"></i> Twitter</a>
            </li>
          <!--  <li class="pinterest">
              <a target="_blank" href="https://www.pinterest.com/stylecracker"> <i class="fa fa-pinterest"></i> Pinterest</a>
            </li>-->
            <li class="snapchat">
              <a target="_blank" href="https://www.snapchat.com/add/stylecracker"> <i class="fa fa-snapchat"></i> Snapchat</a>
            </li>

          </ul>
        </div>
      </div>
     
      <div class="row" style="text-align: center;">
        <div class="col-md-12">
          <div class="box-nav">
            <div class="title">
              &nbsp;
            </div>
            <ul class="list-inline">
              <li>
                <a href=""> Contact Us  |</a>
              </li>
            <!--  <li>
                <a href="#">Help & Feedback  |</a>
              </li> -->
              <li>
                <a href="">Terms & Conditions  | </a>
              </li>
              <li>
                <a href="">Return Policy  | </a>
              </li>
              <li>
                <a href="">Privacy Policy  | </a>
              </li>
              <li>
                <a href="">FAQ</a>
              </li>
            </ul>
          </div>
        </div>
    </div>
    </div>
  </div>
  </footer>
  <div class="page-loader" id="page-loader">
<div class="fa-loader-wrp">
  <i class="fa fa-circle-o-notch fa-spin-custom"></i> Loading...
</div>
<div class="page-overlay" id="page-overlay"></div>
</div>
<?php include('popups.php'); ?>

  <!-- js -->
  <script src="<?php echo base_url(); ?>assets/plugins/devicejs/js/device.min.js"></script>
   <script>
    var devicejs = device.noConflict();
  </script>

  <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

  <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script> 
  
  <script src="<?php echo base_url(); ?>assets/plugins/hoverintent/hoverIntent.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/plugins/swiper/js/swiper.jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/plugins/slick/slick.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/elevatezoom-plus/jquery.ez-plus.js"></script>
  <script src="<?php echo base_url(); ?>assets/plugins/magnifik/magnifik.js"></script>
  <script src="<?php echo base_url(); ?>assets/plugins/jqueryvalidate/jquery.validate.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/plugins/jquerycropit/jquery.cropit.js"></script>
  <script src="<?php echo base_url(); ?>assets/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>

  <script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>

  <script src="<?php echo base_url(); ?>assets/js/plugins.settings.js?v=1.001"></script>
  <script src="<?php echo base_url(); ?>assets/js/home.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/custom.js?v=1.008"></script>
  <!-- <script src="<?php echo base_url(); ?>assets/js/custom-new.js?v=1.011"></script> -->
  <script src="<?php echo base_url(); ?>assets/js/custom_sc.js?v=1.002"></script>
  <script src="<?php echo base_url(); ?>assets/js/common_v1.js?v=1.002"></script>
  <script src="<?php echo base_url(); ?>assets/js/analytics.js?v=1.003"></script>
  <script src="<?php echo base_url(); ?>assets/js/sc-box_v1.js?v=1.00107"></script>
  <script src="<?php echo base_url(); ?>assets/js/scbox_validate.js?v=1.000063"></script>
  <script src="<?php echo base_url(); ?>assets/js/style-cart.js?v=0.011"></script>
  <script src="<?php echo base_url(); ?>assets/js/stylecracker-cart.js?v=0.29"></script>

<!-- For homepage animation of testimonials -->
  <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/jribbble.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/drifolio.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
  <script>
        new WOW().init();
  </script>
  <?php if($this->session->userdata('user_id') > 0) { $gender = '';
  if($this->session->userdata('gender') == 1){ $gender = 'Women'; }
  if($this->session->userdata('gender') == 2){ $gender = 'Men'; }
 ?>
<script>
if($('html').hasClass("mobile") || $('html').hasClass("tablet")){
 /* window.intercomSettings = {
    app_id: "oz2nj7x5",
    //name: "<?php echo @$this->session->userdata('user_name'); ?>", // Full name
    //email: "<?php echo @$this->session->userdata('email'); ?>", // Email address
    //user_id: "<?php echo @$this->session->userdata('user_id'); ?>", // User ID
   // created_at: "<?php echo strtotime(date('Y-m-d H:i:s')) ?>", // Signup date as a Unix timestamp
   // "Bucket" : "<?php echo @$this->session->userdata('bucket'); ?>",
   // "Gender" : "<?php echo $gender; ?>",
   // user_hash: "<?php echo hash_hmac('sha256',$this->session->userdata('user_id'),'rK2JpjpInEcvXCtXb9yRzOFRLTmR4wqLfVuVPTal'); ?>",
  custom_launcher_selector: '#my_custom_link',
  hide_default_launcher: true
  };*/
}else{
  /*window.intercomSettings = {
    app_id: "oz2nj7x5",
   // name: "<?php echo @$this->session->userdata('user_name'); ?>", // Full name
  //  email: "<?php echo @$this->session->userdata('email'); ?>", // Email address
   // user_id: "<?php echo @$this->session->userdata('user_id'); ?>", // User ID
   // created_at: "<?php echo strtotime(date('Y-m-d H:i:s')) ?>", // Signup date as a Unix timestamp
  //  "Bucket" : "<?php echo @$this->session->userdata('bucket'); ?>",
  //  "Gender" : "<?php echo $gender; ?>",
  //  user_hash: "<?php echo hash_hmac('sha256',$this->session->userdata('user_id'),'rK2JpjpInEcvXCtXb9yRzOFRLTmR4wqLfVuVPTal'); ?>"
  };*/
}


</script>
<?php }else{ ?>
<script>
if($('html').hasClass("mobile") || $('html').hasClass("tablet")){
  /*window.intercomSettings = {app_id: "oz2nj7x5",
              custom_launcher_selector: '#my_custom_link',
              hide_default_launcher: true};*/
}else{
  /*window.intercomSettings = {app_id: "oz2nj7x5"};*/
}
</script>
<?php } ?>
<!-- <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/oz2nj7x5';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
 -->

<!-- If the site opens on mobile app container the header will hide Start -->
<?php if($this->session->userdata('is_app') == 1 || @$this->input->get('is_app') == 1){ $is_app = 1; }else { $is_app = 0; } ?>
<script>
  var is_app = "<?php echo $is_app; ?>";
  if(is_app ==1){ $('#header').hide(); $('#footer').hide(); }
</script>
<!-- If the site opens on mobile app container the header will hide END -->


<!-- Invite Code Start -->

<!--<a id="invite_copy" class="btn btn-refer" href="https://www.stylecracker.com/login" data-toggle="modal" title=""><i class="fa fa-gift"></i> Refer & Earn</a>-->

<div id='invtrflfloatbtn'></div>
<script>  
  var sc_user_id = "<?php echo $this->session->userdata('user_id')!='' ? $this->session->userdata('user_id') : ''; ?>";
  if(sc_user_id>0){
    $('#invite_copy').hide();
    $('#invite_message').hide();
    var invite_referrals = window.invite_referrals || {}; (function() { 
      invite_referrals.auth = { bid_e : 'AC70A50D47DD5C5FF203ED7121693FC4', bid : '11257', t : '420', email : sc_beatout_email_id, fname : sc_beatout_username }; 
    var script = document.createElement('script');script.async = true;
    script.src = (document.location.protocol == 'https:' ? "//d11yp7khhhspcr.cloudfront.net" : "//cdn.invitereferrals.com") + '/js/invite-referrals-1.0.js';
    var entry = document.getElementsByTagName('script')[0];entry.parentNode.insertBefore(script, entry); })();
  }else {
    $('#invite_copy').show();
  } 
</script>

<!-- Invite Code END -->

<!-- For homepage animation of testimonials -->
</body>

</html>


