<style type="text/css">
  .navbar-collapse {
  padding-right: 1px;
  padding-left: 1px;
  }
.sc-navbar-left-wrp .dropdown-menu{width: 400px !important;}
.sc-navbar-left-wrp .dropdown-menu li{display: inline-block !important;}
</style>
<header id="header" class="header sticky">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-1 col-sm-2 col-xs-2 sm-static">
            <div class="dropdown sc-navbar-left-wrp sm-static">
               <button type="button" class="navbar-toggle dropdown-toggle" data-toggle="dropdown" style="display: block;">
               <i class="icon-menu"></i>
               </button>
               <div class="dropdown-menu" style="z-index:1001;">
                  <ul>
                     <li onclick=""><a href="./login"><img src="<?php echo base_url(); ?>assets/images/icons/login.png" style="height:25px;" /> <span>Login</span></a></li>
                     <li onclick=""><a href="./signup"><img src="<?php echo base_url(); ?>assets/images/icons/signup.png" style="height:25px;" /> <span>Sign up</span></a></li>
                     <li><a href=""><i class="icon icon-user"></i> <span>About<br /> Us</span></a></li>
                     <li><a href=""><i class="icon icon-cart"></i> <span>My <br /> Orders</span></a></li>
                     <li><a href=""><i class="icon icon-heart"></i> <span>Bookmarks</span></a></li>
                     <li><a href=""><i class="icon icon-document"></i> <span>Referral Amount</span></a></li>
                     <li><a href=""><i class="icon icon-setting"></i> <span>Profile <br />Settings</span></a></li>
                     <li><a href=""><i class="icon icon-sclive"></i> <span>SC <br />Live</span></a></li>
                     <li><a href=""><i class="ic-yesbank"></i> <span>Yes Bank</span></a></li>
                     <li><a href=""><i class="icon icon-document"></i> <span>Terms & <br />Conditions</span></a></li>
                     <li class="sm-only"><a href=""><i class="icon icon-document"></i> <span>Return Policy</span></a></li>
                     <li class="sm-only"><a href=""><i class="icon icon-document"></i> <span>Privacy Policy</span></a></li>
                     <li class="sm-only"><a href=""><i class="icon icon-help"></i> <span>FAQ</span></a></li>
                     <li class="sm-only"><a href=""><i class="icon icon-document"></i> <span>Contact<br />Us</span></a></li>
                     <li><a href="" id="sclogout"><i class="icon icon-logout"></i>  <span>Log out</span></a></li>
                  </ul>
               </div>
            </div>
         </div>
         <div class="col-md-2 col-sm-7 col-xs-7">
            <div class="navbar-header">
               <a class="navbar-brand" href="">
               <img id="logo-desktop" class="logo logo-desktop" src="<?php echo base_url(); ?>assets/images/logo.png"/>
               </a>
            </div>
         </div>
         <div class="col-md-7 hidden-sm position-static">
            <nav class="collapse navbar-collapse sc-navbar-primary" id="sc-navbar-primary" style="text-align:left;">
               <ul class="nav navbar-nav">
                  <li><a href="#section-works" class="link-scrolling">How It Works</a></li>
                  <li><a href="#section-testimonials" class="link-scrolling">Testimonials</a></li>
                  <li><a href="#section-commitment" class="link-scrolling">OUR COMMITMENT</a></li>
                  <li><a href="#section-pricing" class="link-scrolling">Order Now</a></li>
                  <li><a href="https://www.stylecracker.com/blog/" target="_blank" >SC Live</a></li>
				  <?php if(!$this->session->userdata('user_id')){ ?>
                   <li onclick="ga('send', 'event', 'Login', 'clicked','header');" ><a href="<?php echo base_url().'login'; ?>">Login</a></li>
                  <?php }else{ ?>
				  <li class="dropdown sm-static">
                        <a class="dropdown-toggle" data-toggle="dropdown"  href="#">Hi,<?php echo substr($this->session->userdata('first_name'),0,9);/*.' '.$this->session->userdata('last_name')*/ ?><span class="caret"></span>
                        </a>
                        <div class="dropdown-menu">
                           <ul>
                              <li><a href="<?php echo base_url().'schome/logout'; ?>" id="sclogout"><i class="icon icon-logout"></i>  <span>Log out</span></a></li>
                           </ul>
                        </div>
                     </li>                  
                  <?php } ?>
               </ul>
            </nav>
         </div>
         <div class="col-md-2 col-sm-3 col-xs-3 sm-static">
            <nav class="collapse navbar-collapse sc-navbar-secondary" id="sc-navbar-secondary">
               <ul class="nav navbar-nav navbar-right">
                  <li class="md-only">
                     <a href="">
                        <i class="icon-bag"></i>
                        <!--  <span id="total_products_cart_count" class="ic-count shape-diamond solid">0</span> -->
                     </a>
                  </li>
                  <li class="dropdown sm-static" id="notification_html" onclick="">
                     <i class="icon-notification"></i>
                  </li>
                  <li class="md-only"><a href="#" data-toggle="collapse" data-target="#search-panel"><i class="icon-search"></i></a></li>
               </ul>
            </nav>
         </div>
         <!-- search form -->
         <div id="search-panel" class="collapse search-panel">
            <div class="search-panel-inner">
               <input type="text" name="search_keyword" id="search_keyword" value="" class="form-control form-control-search" placeholder="SEARCH" autofocus="">
               <span class="close"><i class="icon-close"></i></span>
            </div>
         </div>
         <!--//. -->
      </div>
   </div>
   <!-- /.container-fluid -->
</header>