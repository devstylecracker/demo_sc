<style type="text/css">
  .navbar-collapse {
  padding-right: 1px;
  padding-left: 1px;
  }
.sc-navbar-left-wrp .dropdown-menu{width: 400px !important;}
.sc-navbar-left-wrp .dropdown-menu li{display: inline-block !important;}
</style>
<header id="header" class="header sticky">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-1 col-sm-2 col-xs-2 sm-static">
            <div class="dropdown sc-navbar-left-wrp sm-static">
               <button type="button" class="navbar-toggle dropdown-toggle" data-toggle="dropdown">
               <i class="icon-menu"></i>
               </button>
               <div class="dropdown-menu" style="z-index:1001;">
                  <ul>
                     <li onclick=""><a href="<?php echo base_url('login');?>"><img src="<?php echo base_url(); ?>assets/images/icons/login.png" style="height:25px;" /> <span>Login</span></a></li>
                     <li onclick=""><a href="<?php echo base_url('signup');?>"><img src="<?php echo base_url(); ?>assets/images/icons/signup.png" style="height:25px;" /> <span>Sign up</span></a></li>
                     <li><a href=""><i class="icon icon-user"></i> <span>About<br /> Us</span></a></li>
                     <li><a href=""><i class="icon icon-cart"></i> <span>My <br /> Orders</span></a></li>
                     <li><a href=""><i class="icon icon-heart"></i> <span>Bookmarks</span></a></li>
                     <li><a href=""><i class="icon icon-document"></i> <span>Referral Amount</span></a></li>
                     <!-- <li><a href="<?php //echo base_url(); ?>brands"><i class="icon icon-star"></i> <span>Brands</span></a></li> -->
                     <!--  <li><a href="<?php //echo base_url(); ?>help-feedback"><i class="icon icon-help"></i> <span>Help & <br />Feedback</span></a></li>-->
                     <li><a href=""><i class="icon icon-setting"></i> <span>Profile <br />Settings</span></a></li>
                     <!--  <li><a href="#"><i class="icon icon-pinterest"></i> <span>Connect to <br />Pinterest</span></a></li>-->
                     <li><a href=""><i class="icon icon-sclive"></i> <span>SC <br />Live</span></a></li>
                     <li><a href=""><i class="ic-yesbank"></i> <span>Yes Bank</span></a></li>
                     <li><a href=""><i class="icon icon-document"></i> <span>Terms & <br />Conditions</span></a></li>
                     <li class="sm-only"><a href=""><i class="icon icon-document"></i> <span>Return Policy</span></a></li>
                     <li class="sm-only"><a href=""><i class="icon icon-document"></i> <span>Privacy Policy</span></a></li>
                     <li class="sm-only"><a href=""><i class="icon icon-help"></i> <span>FAQ</span></a></li>
                     <li class="sm-only"><a href=""><i class="icon icon-document"></i> <span>Contact<br />Us</span></a></li>
                     <li><a href="<?php echo base_url('schome/logout');?>" id="sclogout"><i class="icon icon-logout"></i>  <span>Log out</span></a></li>
                  </ul>
               </div>
            </div>
         </div>
         <div class="col-md-2 col-sm-7 col-xs-7">
            <div class="navbar-header">
                <a class="navbar-brand" href="">
               <img id="logo-desktop" class="logo logo-desktop" src="<?php echo base_url(); ?>assets/images/logo.png"/>
               </a>
            </div>
         </div>
         <div class="col-md-6 hidden-sm position-static">
            <nav class="collapse navbar-collapse sc-navbar-primary" id="sc-navbar-primary">
               <ul class="nav navbar-nav">
                  <!-- <li><a href="<?php //echo base_url(); ?>categories/women">Women</a>
                     <?php //$this->load->view('seventeen/common/nav/nav_menu_sm',array('type'=>'women')); ?>
                     </li>
                     <li><a href="<?php //echo base_url(); ?>categories/men">Men</a>
                     <?php// $this->load->view('seventeen/common/nav/nav_menu_sm',array('type'=>'men')); ?>
                     </li> -->
                  <!-- <li><a href="<?php //echo base_url(); ?>collections/summer-pop-up-135">Summer Store</a></li> -->
                  <!-- <li><a href="<?php //echo base_url(); ?>collections/travel-pop-up-136">Travel Shop</a></li> -->
                  <!-- <li><a href="<?php //echo base_url(); ?>sc-box">SC Box</a></li> -->
                  <li><a href="https://www.stylecracker.com/blog/" target="_blank" >SC Live</a></li>
                  <?php if(!$this->session->userdata('user_id')){ ?>
                     <li onclick="ga('send', 'event', 'Login', 'clicked','header');" ><a href="<?php echo base_url().'login'; ?>">Login</a></li>
                  <?php }else{ ?>
                     <li class="dropdown sm-static">
                        <a class="dropdown-toggle" data-toggle="dropdown"  href="#">Hi,<?php echo substr($this->session->userdata('first_name'),0,9);/*.' '.$this->session->userdata('last_name')*/ ?><span class="caret"></span>
                        </a>
                        <div class="dropdown-menu">
                           <ul>
                              <li><a href="<?php echo base_url().'schome/logout'; ?>" id="sclogout"><i class="icon icon-logout"></i>  <span>Log out</span></a></li>
                           </ul>
                        </div>
                     </li>                  
                  <?php } ?>
               </ul>
            </nav>
         </div>
         <div class="col-md-2 col-sm-3 col-xs-3 sm-static">
            <nav class="collapse navbar-collapse sc-navbar-secondary" id="sc-navbar-secondary">
               <ul class="nav navbar-nav navbar-right">
                  <li class="md-only">
                     <a href="">
                        <i class="icon-bag"></i>
                        <!--  <span id="total_products_cart_count" class="ic-count shape-diamond solid">0</span> -->
                     </a>
                  </li>
                  <li class="dropdown sm-static" id="notification_html" onclick="">
                     <i class="icon-notification"></i>
                  </li>
                  <li class="md-only"><a href="#" data-toggle="collapse" data-target="#search-panel"><i class="icon-search"></i></a></li>
               </ul>
            </nav>
         </div>
      </div>
   </div>
   <!-- search form -->
   <div id="search-panel" class="collapse search-panel">
      <div class="search-panel-inner">
         <input type="text" name="search_keyword" id="search_keyword" value="" class="form-control form-control-search" placeholder="SEARCH" autofocus="">
         <span class="close"><i class="icon-close"></i></span>
      </div>
   </div>
   <!--//. -->
   <!-- /.container-fluid -->
</header>

