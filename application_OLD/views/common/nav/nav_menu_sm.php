  <!-- men submenu -->
<?php
  $menu_id = '';
  $mm_menu = array();
  if($type=='men')
  {
    $menu_id = 'nav_men_sm';
    $mm_menu = $this->data['megamenu_mens'];
    $mm_image = base_url().'assets/images/menu/megamenu_img_men.jpeg';
  }else if($type=='women')
  {
     $menu_id = 'nav_women_sm';
     $mm_menu = $this->data['megamenu_womens'];
     $mm_image = base_url().'assets/images/menu/megamenu_img_women.jpeg';
  }
?>
<div class="sub-menu bottom">
    <div class="sub-menu-inner">
      <ul>
        <?php

        if(!empty($mm_menu))
        {
            foreach($mm_menu as $key=>$val)
            {
        ?>
                  <li class="title" ><a href="<?php echo CAT_URL; ?><?php echo @$val['slug']; ?>"><?php echo @$val['label']; ?></a></li>
                      <?php $i=0;
                          $count = sizeof($val);
                          if(!empty($val['data'])){
                          foreach($val['data'] as $k => $v)
                          {
                            if($i > 0){
                         ?>
                          <li><a href="<?php echo CAT_URL; ?><?php echo @$v[3].'-'.@$v[1]; ?>"><?php echo $v[2]; ?></a></li>
                         <?php } $i++;
                          } }
                      ?>
        <?php
            }
        }
        ?>
      </ul>
    </div>
</div>
