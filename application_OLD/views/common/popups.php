<!-- forgot password -->
<div id="modal-forgot-password" class="modal fade modal-forgot-password" role="dialog">
  <div class="modal-dialog  modal-md">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><i class="icon-close"></i></button>
        <div class="box-forgot-pw">
          <div class="title-section">
            <h1 class="title">Forgot Password?</h1>
          </div>
          <form name="forgotpwdfrm" id="forgotpwdfrm" method="post" action="">
            <div>
              <div class="form-group">
                <input type="email" name="Emailid" id="Emailid" class="form-control" placeholder="Enter your Email ID">
                <div class="message text-error hide">Email is invalid. </div>
                <div id="loginfp_error" class="form-error error"></div>
              </div>
              <div>
                <button id="sc_submit" name="sc_submit" class="scbtn scbtn-primary"><span>Reset Password</span></button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- reset password -->
<div id="modal-reset-password" class="modal fade modal-reset-password" role="dialog">
  <div class="modal-dialog  modal-md">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><i class="icon-close"></i></button>
        <div class="box-forgot-pw">
          <div class="title-section">
            <h1 class="title">Reset Password</h1>
          </div>
          <form name="resetpwdfrm" id="resetpwdfrm" method="post" action="">
            <div>
              <div class="form-group">
                <input type="password" name="oldpwd" id="oldpwd" value="" class="form-control" placeholder="Enter your Old password">
                <div id="oldpwd_error" class="message text-error hide">Enter old password </div>
                <!-- <div id="loginfp_error" class="form-error error"></div> -->
              </div>
              <div class="form-group">
                <input type="password" name="newpwd" id="newpwd" value="" class="form-control" placeholder="Enter your new password">
                <div id="newpwd_error" class="message text-error hide">Enter new password </div>
                <!-- <div id="loginfp_error" class="form-error error"></div> -->
              </div>
              <div>
                <div class="form-group">
                  <div id="resetpwd_error" class="message text-error hide"></div>
                  <!-- <button  id="sc_rpsubmit" name="sc_rpsubmit" class="scbtn scbtn-primary"><span>Reset Password</span></button> -->
                  <a id="sc_rpsubmit" name="sc_rpsubmit" class="scbtn scbtn-primary"><span>Reset Password</span></a>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end reset password -->


<!--   <div id="size_guide<?php //echo @$product_data['product_id']; ?>" class="modal fade modal-size-guide" role="dialog">
    <div class="modal-dialog  modal-lg">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="img-wrp">
          <img src="<?php //echo @$product_data['size_guide_url']; ?>">
        </div>
      </div>
    </div>
  </div> -->


    <!-- alert dialog -->
    <div id="modal-alert" class="modal fade modal-alert" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-body">
            <!--  <div class="title-section">
            <h1 class="title"></h1>
          </div>-->
            <div class="message-text">
              Product has been added to your cart.
            </div>
          </div>
          <div class="modal-footer">
            <div class="btns-wrp">
              <a class="sc-color scbtn-info" data-dismiss="modal">Cancel</a>
              <a class="scbtn-info" data-dismiss="modal">OK</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- //. alert dialog -->

    <!-- pa dialog -->
    <div id="modal-alert-pa" class="modal fade modal-alert shown" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-body">
            <!--  <div class="title-section">
            <h1 class="title"></h1>
          </div>-->
            <div class="message-text">
              Image has been Saved.
            </div>
          </div>
          <div class="modal-footer">
            <div class="btns-wrp">
              <a class="scbtn-info" data-dismiss="modal">OK</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- //. pa dialog -->

    <!-- //. filter size dialog -->
    <div id="modal-select-size-filter" class="modal fade modal-select-size" role="dialog">
      <div class="modal-dialog  modal-md">
        <div class="modal-content">
          <button type="button" class="close" data-dismiss="modal"><i class="icon-close"></i></button>
          <div class="title-2">
            Select Size
          </div>
          <div class="product-item">
            <div class="item-sizes-wrp" id="select-size-html">
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- //. filter size dialog -->

    <!-- common alert dialog -->
    <div id="common-modal-alert" class="modal fade modal-alert" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-body">
            <div class="message-text" id="common-message-text">

            </div>
          </div>
          <div class="modal-footer">
            <div class="btns-wrp">
              <a class="scbtn-info" data-dismiss="modal">OK</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- common alert dialog -->


    <!-- avail offers dialog -->

    <div id="modal-offers" class="modal fade modal-offers in" role="dialog">
      <div class="modal-dialog modal-md111">

        <div class="modal-content">
          <div class="modal-header">
            <h2 class="box-title">Avail Offers & Discounts</h2>
          </div>
          <div class="modal-body">

            <div class="offers-popup-wrp">
              <div class="box-offers-list">

                <div class="box-input">
                  <div class="input-control-wrp">
                    <div class="form-group-wrp">
                      <div class="form-group">
                        <input class="form-control" type="text" placeholder="Enter coupon code" name="" id="" value="">
                        <span id="" class="error error-msg"></span>
                      </div>
                      <button class="scbtn scbtn-primary"><span>Apply</span></button>
                    </div>
                    <div class="title-2">OR</div>
                    <div class="text-2">
                      Choose offers from below
                    </div>
                  </div>
                </div>
                <div class="offers-list-wrp scrollbar">
                  <ul class="offers-list">
                    <li>
                      <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                        <span>ICICI Bank Get 50% off</span>
                        <span class="text2">Debit Card</span>
                      </label>
                    </li>
                    <li>
                      <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                        <span>ICICI Bank Get 50% off</span>
                        <span class="text2">Debit Card</span>
                      </label>
                    </li>
                    <li class="active">
                      <label class="sc-checkbox active"><i class="icon icon-diamond"></i>
                        <span>ICICI Bank Get 50% off</span>
                        <span class="text2">Debit Card</span>
                      </label>
                    </li>
                    <li>
                      <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                        <span>ICICI Bank Get 50% off</span>
                        <span class="text2">Debit Card</span>
                      </label>
                    </li>
                  </ul>
                </div>
              </div>

              <div class="box-offers-details">
                <div class="back-wrp">
                  <span class="back"><i class="fa fa-arrow-circle-left"></i> Back</span>
                </div>
                <div class="box-input">

                  <div class="title-2">
                    ICICI Bank Get 50% off
                  </div>
                  <div class="input-control-wrp">
                    <div class="form-group-wrp">
                      <div class="form-group">
                        <input class="form-control" type="text" placeholder="Enter debit card no." name="" id="" value="">
                        <span id="" class="error error-msg"></span>
                      </div>
                      <button class="scbtn scbtn-primary"><span>Apply</span></button>
                    </div>
                  </div>
                </div>
                <div class="desc">
                  <div class="title">
                    Note:
                  </div>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nibh sem, malesuada id augue at, aliquam pharetra neque. Aenean vehicula orci et tempus ornare. Aliquam erat volutpat. Praesent commodo lorem nec leo sagittis tempus.
                </div>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <button class="scbtn scbtn-primary"><span>Proceed</span></button>
          </div>
        </div>
      </div> -->
<!--     </div> -->
    <!-- /. avail offers dialog -->


    <!-- yesbank -->
    <div id="modal-yesbank" class="modal fade modal-yesbank shown" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">

          <div class="title-section" style="margin-bottom:20px; text-align:center;">
              <div class="title">Wardrobe Revamp</div>
          </div>

            <iframe src="https://www.stylecracker.com/blog/yes-bank/" style=" width:100%; height:470px; overflow:hidden; border:none;">
            <p>Your browser does not support iframes.</p>
          </iframe>
          </div>
        </div>
      </div>
    </div>
    <!-- //. yesbank -->
