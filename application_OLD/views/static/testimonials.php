<style type="text/css">
   .extrapad-row{
   margin-top: 40px;
   }
</style>
<div class="page-about">
   <section id="section-testimonials" class="section section-shadow section-testimonials">
      <div class="container">
         <div class="title-section text-center">
            <h1 class="title">TESTIMONIALS</h1>
            <h2 style="font-weight:normal;font-size:14px;text-transform: uppercase;
               letter-spacing: 2px;">We asked a few of our customers about their first experience with the StyleCracker Box</h2>
         </div>


         <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="https://www.stylecracker.com/assets/images/home/banner/8.jpg" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>Thank you so much StyleCracker, I really appreciate you making an effort to make my experience feel special and personal. I would love to purchase more boxes from you in the future and am super happy with everything I received- STYLECRACKER ROCKS!</p>
                        <div class="name">
                           - Sana Syed
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="https://www.stylecracker.com/assets/images/home/banner/2.jpg" style="width:127px;" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>I really enjoyed my first experience with StyleCracker and will definitely be trying this out again!</p>
                        <div class="name">
                           - Garima Shandilya
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="https://www.stylecracker.com/assets/images/home/banner/priyanka1.jpg" style="width:127px;" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>I received my surprise box of goodies from StyleCracker and it just couldn't get better! In this day and age of online shopping, a box of goodies suited to my style and preference was something very outstanding. Not to mention how flattering it is to have a box personalized to my taste and style. Way to go StyleCracker!</p>
                        <div class="name">
                           - Priyanka Talreja
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="https://www.stylecracker.com/assets/images/home/banner/ramona.jpg" style="width:127px;" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>The StyleCracker stylists have been like 3 am friends. They have helped me pick products that suit me and my body type. Every purchase is tailor made and I love how personal it gets. And in rare scenario when a product doesn’t work , the team is extremely helpful and prompt in arranging easy returns.</p>
                        <div class="name">
                           - Ramona
                        </div>
                     </div>
                  </div>
               </div>
               
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="https://www.stylecracker.com/assets/images/home/banner/rajni.jpg" alt="" style="width:127px;">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>Absolutely love the stuff on stylecracker! Chic and unique. Alot of pretty cool brands to choose from. Customer service is fab! They literally take care of you and your order.</p>
                        <div class="name">
                           - Rajni Rodrigues
                        </div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12">
              <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="https://www.stylecracker.com/assets/images/home/banner/5.jpg" style="width:127px;" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>Thank you so much for this incredible experience! I loved all my products and found the service so helpful and quick. I will be posting all about the StyleCracker Box online as well.</p>
                        <div class="name">
                           - Muskan Jain
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="https://www.stylecracker.com/assets/images/home/banner/4.jpg" style="width:127px;" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>I looooooooooved my StyleCracker Box! Each product was so cool and totally my style. Can’t wait to order my next box again!!! MUAH to the StyleCracker team.</p>
                        <div class="name">
                           - Priya Mandal
                        </div>
                     </div>
                  </div>
               </div>
                <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="https://www.stylecracker.com/assets/images/home/banner/glen.jpg" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>Since I was looking for a personal stylist for myself, having one who's worked with A-listers and the biggest names in the movie business made StyleCracker my absolute choice! From being style guides to helping me with even 'what not to wear', they've been extremely helpful in solving all my fashion queries.</p>
                        <div class="name">
                           - Glenn Gonsalves
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="https://www.stylecracker.com/assets/images/home/banner/1.jpg" style="width:127px;" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>I was so happy with my StyleCracker Box. When I opened the Box, my products were exceptional. They were all branded and very well suited to my tastes. Shalini is a great listener and understood exactly what I wanted. I was very happy with everything from the quality of the products to customer service to customer satisfaction. Thumbs up!</p>
                        <div class="name">
                           - Dolly Dharmshaktu
                        </div>
                     </div>
                  </div>
               </div>
               
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="https://www.stylecracker.com/assets/images/home/banner/7.jpg" style="width:127px;" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>Thank you to Shalini for being so responsive and helpful! I loved my products and the entire experience with the StyleCracker team</p>
                        <div class="name">
                           - Krutika
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
       </div>
   </section>
</div>