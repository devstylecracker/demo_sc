<div class="page-about">
  <section class="section section-shadow">
    <div class="container">
      <div class="title-section">
        <h1 class="title">StyleCracker at a glance</h1>
      </div>
      <div class="about-intro">
<p>
StyleCracker was founded in 2013 by Dhimaan Shah (ex Investment Banker) and Archana Walavalkar (ex Fashion Editor – Vogue) with a vision to make India stylish. StyleCracker offers convenience to its users and empowers them to be the best version of themselves. StyleCracker helps each individual look great and fashionable by curating personalized boxes full of clothes and accessories, taking the stress out of the shopping process.
</p>
<p>
The StyleCracker Box is a stylist curated, customized box of fashion that gets delivered to your doorstep. All you have to do is visit our website, answer a few simple questions about yourself and our stylists will get to work. The StyleCracker Box is tailored to your tastes, budget and body type so that you receive a box of everything you love. Keep what you like and return the rest for a full refund, it's that easy!
</p>
      </div>
    </div>
  </section>

  <section class="section section-shadow">
    <div class="container">
      <div class="title-section">
        <h1 class="title">Our Stylists</h1>
      </div>

      <div class="stylist-section">
        <div class="row">
          <div class="col-md-3">
            <div class="stylist-box">
              <div class="img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/about/priyanka2.jpg">
              </div>
              <div class="stylist-info-box">
                <div class="title">
                  Priyanka
                </div>
                <div class="short-info">
                  The edgy experimental girl with an endless love for dainty jewelry. Loves her basics as much as her statement pieces.
                </div>
               
              </div>
            </div>

          </div>


          <div class="col-md-3">
            <div class="stylist-box">
              <div class="img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/about/nikita2.jpg">
              </div>
              <div class="stylist-info-box">
                <div class="title">
                  Nikita
                </div>
                <div class="short-info">
                  A comfortable dresser with lots of black and grey in her wardrobe. She’s an avid Instagram fanatic who loves clicking pictures aesthetically.
                </div>
               
              </div>
            </div>

          </div>

          <div class="col-md-3">
            <div class="stylist-box">
              <div class="img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/about/ashna2.jpg">
              </div>
              <div class="stylist-info-box">
                <div class="title">
                  Ashna
                </div>
                <div class="short-info">
                  With an incessant urge for exploring new things, she is a moody dresser who loves experimenting with silhouettes. Wears her heart on her sleeve while she loves what she does and does what she loves. </div>
               
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="stylist-box">
              <div class="img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/about/shalini2.jpg">
              </div>
              <div class="stylist-info-box">
                <div class="title">
                  Shalini
                </div>
                <div class="short-info">
                She's the unassuming indie girl with a relentless love for jhumkaas. Constantly takes inspiration from everything around the country having utmost faith in talents who support her every day. </div>
                
              </div>
            </div>
          </div>
        </div>
		
		<div class="" style="text-align:right; padding-top:20px;">
       and many more...
    </div>
	
      </div>
    </div>
  </section>
  
</div>
