<?php  
   $scbox_pack = unserialize(SCBOX_PACKAGE);
?>

<style>
   /*.page-scbox-form .list-prints li{width:180px; margin: 10px 15px;}
   .page-scbox-form .list-prints li .pallet {height:180px;}
   */
   .page-scbox-form .list-skin-tone li .title-3{padding-top: 15px;}
   @media(max-width:768px){
   /*
   .page-scbox-form .list-prints li{width:100px;}
   .page-scbox-form .list-prints li .pallet {height:100px; width:100px;}*/
   .page-scbox-form ul.list-skin-tone li { width: 60px;}
   }

   .btn-group.special {
   display: flex;
   }

   .page-scbox-form .list-skin-tone li .pallet {
   height: 30px;
   width: 30px;
   }

   .special .btn {
   flex: 1
   }

   /*select{
   width: 30% !important;
   }*/

   .list-skin-tone li {
   width: 76px;
   }

   .page-scbox-form .list-bags li.active, .page-scbox-form .list-footweartype li.active, .page-scbox-form .list-heeltype li.active {
   outline: 3px solid #13D792;
   cursor: default;
   }

   .page-scbox-form .list-bags li, .page-scbox-form .list-footweartype li,.page-scbox-form .list-heeltype li ,.page-scbox-form .list-dneed li{
   display: inline-block;
   width: 140px;
   margin: 10px 4px;
   cursor: pointer;
   }

   .page-scbox-form .list-fabrics li.active,.page-scbox-form .list-prints li.active,.page-scbox-form .list-dneed li.active{
   outline: 3px solid #ed1b2e;
   cursor: default;
   }

   .page-scbox-form .list-fabrics li{
   display: inline-block;
   width: 140px;
   margin: 10px 4px;
   cursor: pointer;
   }

   .btn {
   border: 1px solid #000;
   color: #333;
   border-radius: 0px;
   outline:none;
   }

   .btn-green.active ,.btn-green.active.focus, .btn-green:active{border: 1px solid #13D792;
   background: #13D792;
   color: #fff;
   outline:none;
   }

   .btn-red.active ,.btn-red.active.focus, .btn-red:active{border: 1px solid #ed1b2e;
   background: #ed1b2e;
   color: #fff;
   outline:none;
   }

   .choose_file{
   position:relative;
   display:inline-block;    
   border-bottom:#13D792 solid 1px;
   width:300px; 
   padding: 4px 6px 4px 8px;
   color: #000;
   margin-top: 2px;
   background:white
   }

   .choose_file input[type="file"]{
   -webkit-appearance:none; 
   position:absolute;
   top:0; left:0;
   opacity:0; 
   }

   fieldset.date { 
   margin: 0; 
   padding-top: 10px;  
   display: block; 
   border: none; 
   } 

   fieldset.date legend { 
   margin: 0; 
   padding: 0; 
   margin-top: .25em; 
   font-size: 100%; 
   } 

   fieldset.date label { 
   position: absolute; 
   top: -20em; 
   left: -200em; 
   } 

   fieldset.date select { 
   margin: 0; 
   padding: 0; 
   font-size: 100%; 
   display: inline; 
   width: 30%;
   } 

   .hwidth{width: 20%;}

   span.inst { 
   font-size: 75%; 
   color: #13D792; 
   padding-left: .25em; 
   } 

   fieldset.date select:active, 
   fieldset.date select:focus, 
   fieldset.date select:hover 
   { border-color: gray; } 

	   @media (max-width: 768px){
	   .page-scbox-form .list-colors li, .page-scbox-form .list-prints li,.page-scbox-form .list-bags li, .page-scbox-form .list-footweartype li,.page-scbox-form .list-heeltype li
	   {
		   width: 80px;
		   text-align: center;
		   margin: 5px 4px;
	   }
   }

   @media (max-width: 768px)
   {
   .page-scbox-form .list-bags li img, .page-scbox-form .list-footweartype li img, .page-scbox-form .list-heeltype li img {
   max-width: 100%;
   }
   }

   @media (max-width: 768px)
   {.page-scbox-form ..list-bags li .pallet, .page-scbox-form .list-footweartype li .pallet, .page-scbox-form .list-heeltype li .pallet {
   width: 50px;
   height: 50px;
   margin: 5px auto 5px;
   }}
   .form-control {
   display: inline !important;
   }
   .page-scbox-form .list-colors li .pallet, .page-scbox-form .list-prints li .pallet {
   height: 270px;
   }
   .pallet img{
   width: 100%;
   }

   @media (max-width: 768px){
   .page-scbox-form .list-colors li, .page-scbox-form .list-prints li, .page-scbox-form .list-bags li, .page-scbox-form .list-footweartype li, .page-scbox-form .list-heeltype li {
   width: 140px;
   text-align: center;
   margin: 5px 4px;
   }

   .page-scbox-form .list-colors li .pallet, .page-scbox-form .list-prints li .pallet{
   width: 100%;
   }
   }
   .lesspad{
    padding: 8px 0;
   }
</style>

<div class="page-scbox-form">

<!-- action="<?php //echo base_url(); ?>book-scbox/<?php //echo @$userdata['scbox_objectid']; ?>"-->
<form name="scbox-form" id="scbox-form" method="post" action="" enctype='multipart/form-data'>
   <input type='hidden' id="utm_source" name="utm_source" value="<?php echo @$_GET['utm_source']; ?>" />
   <input type='hidden' id="utm_medium" name="utm_medium" value="<?php echo @$_GET['utm_medium']; ?>" />
   <input type='hidden' id="utm_campaign" name="utm_campaign" value="<?php echo @$_GET['utm_campaign']; ?>" />
  
   <div id="slide_1">
      <section class="section section-shadow">      
     <?php 
        if($this->session->flashdata('scbox_message')): ?>
            <?php echo $this->session->flashdata('scbox_message'); ?>
        <?php endif; ?>
        <!--  <div class="title-section text-center">
            <h1 class="title">Help us know you better </h1>
            <p style="font-size: 14px; margin-top: 10px;">
               ...so we can curate the best fashion for you
            </p>
         </div> -->
         <div class="container">
            <div class="box-step-2 text-left">
               <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12"  style="padding-bottom: 30px;">
                     <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <div class="title-2">
                              <label> Comment Box</label>
                           </div>
                           <div class="form-group-md">
                              <textarea type="text" placeholder="" name="scbox_missed" id="scbox_missed" ><?php echo (@$userdata['user_missed']!='') ? @$userdata['user_missed'] : ''; ?></textarea>
                              <label>IS THERE ANYTHING WE MISSED ? </label>
                           </div>
                            <ul class="description-comment">
                              <li>Ex. I am a new mom.</li>
                              <li>Ex. I am looking for a wardrobe makeover.</li>
                              <li>Ex. I prefer everything in dark shades.</li>
                            </ul>
                          
                        </div>
                     </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12"  style="padding-bottom: 30px;">
                     <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <div class="title-2">Drop Your Social Media Handles Here 
                           </div>
                           <a class="facebook social-button" onclick="fb_login();" href="#"><i class="fa fa-facebook-official"></i> Allow access to my Facebook photos</a>  <br/><br/>
                           <!--<a  class="instagram social-button" onclick="window.open( this.href, 'windowName','left=20,top=20,width=500,height=500,toolbar=1,resizable=0' ); return false"  target="_blank" href="https://api.instagram.com/oauth/authorize/?client_id=<?php echo $this->config->item('instagram_client_id'); ?>&redirect_uri=<?php echo $this->config->item('instagram_callback_url'); ?>&response_type=code&scope=basic+public_content+follower_list+comments+relationships+likes"> <i class="fa fa-instagram"></i> Log in & Share your Photos of Instagram</a>-->
                        </div>
                     </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12"  style="padding-bottom: 30px;">
                     <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <div class="title-2">
                              <label>Pictures</label>
                           </div>
                           <p style="font-size: 14px; margin-top: 10px;">A picture speaks a thousand words, show us what you look like and help us style you better. </p>
                           <p>P.S - Send us a full solo photo(s) of you for best results. This photo is for your stylist's eyes only.
                           </p>
							             <div id="preview_img"></div>  
                           <div class="form-group-md lesspad"> 
                              <input type='file' name="image[]" id="image" multiple="multiple" onchange="readURL(this);" />
                           </div>
                           <p>Disclaimer: This photo is solely for styling purposes and will not be viewed by anyone but your stylist.</p>
                        </div>
                     </div>
                  </div>
               </div>
              </div>
            </div>
      </section>
      <section class="section section-shadow">
      <div class="btns-wrp">
      <!--<a class="scbtn scbtn-primary" href="#"><span>Confirm</span></a>-->
      <!-- <a class="scbtn scbtn-primary" href="javascript:void(0);" onclick="slideprev(6);" ><span>Previous</span></a>
         <button type="submit" id="scbox-confirm" name="scbox-confirm"  onclick="confirmbox(this);" value="<?php echo 'user-'.@$userid; ?>" class="scbtn scbtn-primary" ><span>GET YOUR BOX</span></button> -->
      <!--  <a  id="scbox-confirm" name="scbox-confirm"  onclick="confirmbox();" value="<?php echo 'user-'.@$userid; ?>" class="scbtn scbtn-primary" href="javascript:void(0);"><span>Confirm Booking</span></a>  -->
      <!--<a class="scbtn scbtn-primary" href="javascript:void(0);" onclick="ChangeStep(6);" ><span>Save</span></a>-->
       <!-- <button type="submit" id="" name="" onclick="ChangeStep(6);" class="scbtn scbtn-primary">Save</button> -->
       <a class="scbtn scbtn-primary submitform" href="javascript:void(0);" onclick="document.getElementById('scbox-form').submit(); return false;"><span>Save</span></a>
      </div>
      </section>
      </div>
</form>
</div>
<style>
   .scbox-payment-options li{display: inline-block; margin-right: 10px;}
   .scbox-payment-options  .sc-checkbox .icon{position: relative; top:10px !important;}
</style>

<script>

  ga('send', 'event', 'SCBOX','clicked','Step5-Upload Profile');

  function readURL(input) { 
       $('#preview_img').html('');
      for(var i = 0;i <= input.files.length; i++){ 
        if (input.files && input.files[i]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                //$('#preview_img').attr('src', e.target.result);
                $('#preview_img').append('<img id="preview_img" src='+e.target.result+' style="max-width: 80px; max-height: 80px;padding:0px 5px 0px 0px;"/>');
            }

            reader.readAsDataURL(input.files[i]);
        }
      }
    }



   // This is called with the results from from FB.getLoginStatus().
   function statusChangeCallback(response) {
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else {
      // The person is not logged into your app or we are unable to tell.
      document.getElementById('status').innerHTML = 'Please log ' +'into this app.';
    }
   }

   // This function is called when someone finishes with the Login
   // Button.  See the onlogin handler attached to it in the sample
   // code below.
   function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
   }
   

   window.fbAsyncInit = function() {
   FB.init({
    appId      : '1650358255198436',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.8' // use graph api version 2.8
   });

   // Now that we've initialized the JavaScript SDK, we call 
   // FB.getLoginStatus().  This function gets the state of the
   // person visiting this page and can return one of three states to
   // the callback you provide.  They can be:
   //
   // 1. Logged into your app ('connected')
   // 2. Logged into Facebook, but not your app ('not_authorized')
   // 3. Not logged into Facebook and can't tell if they are logged into
   //    your app or not.
   //
   // These three cases are handled in the callback function.
   FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
   });
   };
   // Load the SDK asynchronously

   (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));


   // Here we run a very simple test of the Graph API after login is
   // successful.  See statusChangeCallback() for when this call is made.
   function testAPI() {
   response = new Array();
   var accessToken = 0;
   FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
    accessToken = response.authResponse.accessToken;
    }
   });


   $.ajax({
      url: 'https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=1650358255198436&client_secret=403b4cbf601b9a67b6594c7be781e8b9&fb_exchange_token='+accessToken,
      type: 'get',
      data: { 'response' : response },
      success: function(data, status) {
        save_fb_token(data);
      }
    });
   }

   function save_fb_token(data){
    $.ajax({
      url: sc_baseurl+'Schome_new/save_fb_token',
      type: 'post',
      data: { 'data' : data},
      success: function(data, status) {
      }
    });   
   }

   
   function fb_login(){
    FB.login(function(response) {
      if (response.authResponse) {
        checkLoginState();
      } else {
        //user hit cancel button
        console.log('User cancelled login or did not fully authorize.');
      }
    }, {
      scope: 'public_profile,email,user_photos'
    });
   }

  

</script>