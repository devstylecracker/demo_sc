
   <style type="text/css">
  .offers-popup-wrp .offers-list li {
    width: 100%;
    display: block;
    height: 40px;
  }
.modalfooter {
    padding: 15px;
    text-align: right;
    border-top: 1px solid #e5e5e5;
}
.scbtn span{
  padding: 0 0;
  font-size: 11px;

}
</style>
<div class="page-order-history">
  <section class="section section-shadow ">
    <div class="container">
      <div class="title-section text-center">
        <h1 class="title">My Orders</h1>
      </div>
      <div class="tabs-wrp">
        <div class="text-center">
          <ul class="nav nav-tabs sc-tabs">
            <li class="active">
              <a data-toggle="tab" href="#ongoing">Ongoing</a>
            </li>
            <li>
              <a data-toggle="tab" href="#completed">Completed</a>
            </li>
          </ul>
        </div>
        <div class="tab-content sc-tab-content">
          <div id="ongoing" class="tab-pane fade in active tab-content-ongoing">
           
          <div 
style="-webkit-box-shadow: 2px 2px 17px -3px rgba(0,0,0,0.49);
-moz-box-shadow: 2px 2px 17px -3px rgba(0,0,0,0.49);
box-shadow: 2px 2px 17px -3px rgba(0,0,0,0.49);
padding: 20px;margin-bottom: 20px;">        
            <div class="order-wrp">
              <div class="grid grid-items">
                <div class="row">
                  <div class="col-md-2 col-sm-4 col-xs-4">
                    <div class="order-products-wrp">
                      <div class="title">
                        <strong>Box Details: </strong>
                      </div>
                      <ul class="order-products">
                        <li style="height: 140px;">
                          <div class="img-wrp">
                            <img src="./images/pa/female/style/prints/stripes.jpg" alt="">
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-md-5 col-sm-8 col-xs-8">
                      <div class="item">
                        <div class="title-2">Order #
                          
                        </div>
                        <p>
                          <strong>Order Placed: </strong>
                          
                        </p>
                        <div class="address">
                          <div class="title">
                            <strong>Delivery Address: </strong>
                          </div>
                          <p>
                           
                          </p>
                          <div class="desc">
                            <div class="status">
                              
                            </div>
                           
                            <div class="date">
                              <strong>Date: </strong>
                              
                            </div>
                           
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-12 col-xs-12">
                      <div class="order-products-wrp">
                        <p class="amount">
                          <strong>Total: 
                            <i class="fa fa-inr"></i>
                            
                          </strong>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              
              </div>
              <div class="order-wrp">
                <div class="grid grid-items">
                 
                  <div class="row" style="opacity: 0.4;">
                   <div class="col-md-2">
                      
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-4">
                      <div class="order-products-wrp">
                        <ul class="order-products">
                          <li style="height: 100px;">
                            <div class="img-wrp">
                              <img src="" alt="">
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-8 col-xs-8">
                        <div class="item">
                          <div class="title-2"> # </div>
                          <div class="title">
                            <strong></strong>
                          </div>
                          <div class="title-2"></div>
                         
                          <input type="hidden" name="responseType" id="responseType" > 
                                            
                          
                          
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-4">
                     <div class="btns-wrp">
                            <a class="scbtn scbtn-primary" href="#myreturn" data-toggle="modal" disabled="" onclick="" >
                              <span>Return</span>
                            </a>
                            <a class="scbtn scbtn-primary" href="#myreturn" data-toggle="modal" disabled="" onclick="" >
                              <span>Exchange</span>
                            </a>                            
                          </div>
                      </div>
                     
                </div>
                 
                  <div class="row">
                   <div class="col-md-2">
                      
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-4">
                      <div class="order-products-wrp">
                        <ul class="order-products">
                          <li style="height: 100px;">
                            <div class="img-wrp">
                              <img src="" alt="">
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-8 col-xs-8">
                        <div class="item">
                          <div class="title-2"> # </div>
                          <div class="title">
                            <strong></strong>
                          </div>
                          <div class="title-2"></div>
                         
                          <input type="hidden" name="responseType" id="responseType" > 
                                            
                          
                          
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-4">
                     <div class="btns-wrp">
                            <a class="scbtn scbtn-primary" href="#myreturn" data-toggle="modal" disabled="" onclick="" >
                              <span>Return</span>
                            </a>
                            <a class="scbtn scbtn-primary" href="#myreturn" data-toggle="modal" disabled="" onclick="" >
                              <span>Exchange</span>
                            </a>                            
                          </div>
                      </div>
                     
                </div>
               
               </div> 
              </div> 
            </div>  
    
             </div> 
            <div id="completed" class="tab-pane fade tab-content-completed">
                 
                  <div 
style="-webkit-box-shadow: 2px 2px 17px -3px rgba(0,0,0,0.49);
-moz-box-shadow: 2px 2px 17px -3px rgba(0,0,0,0.49);
box-shadow: 2px 2px 17px -3px rgba(0,0,0,0.49);
padding: 20px;margin-bottom: 20px;">        
            <div class="order-wrp">
              <div class="grid grid-items">
                <div class="row">
                  <div class="col-md-2 col-sm-4 col-xs-4">
                    <div class="order-products-wrp">
                      <div class="title">
                        <strong>Box Details: </strong>
                      </div>
                      <ul class="order-products">
                        <li style="height: 140px;">
                          <div class="img-wrp">
                            <img src="./images/pa/female/style/prints/stripes.jpg" alt="">
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-md-5 col-sm-8 col-xs-8">
                      <div class="item">
                        <div class="title-2">Order #
                          
                        </div>
                        <p>
                          <strong>Order Placed: </strong>
                          
                        </p>
                        <div class="address">
                          <div class="title">
                            <strong>Delivery Address: </strong>
                          </div>
                          <p>
                           
                          </p>
                          <div class="desc">
                            <div class="status">
                              
                            </div>
                           
                            <div class="date">
                              <strong>Date: </strong>
                              
                            </div>
                           
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-12 col-xs-12">
                      <div class="order-products-wrp">
                        <p class="amount">
                          <strong>Total: 
                            <i class="fa fa-inr"></i>
                            
                          </strong>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              
              </div>
              <div class="order-wrp">
                <div class="grid grid-items">
                 
                  <div class="row" style="opacity: 0.4;">
                   <div class="col-md-2">
                      
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-4">
                      <div class="order-products-wrp">
                        <ul class="order-products">
                          <li style="height: 100px;">
                            <div class="img-wrp">
                              <img src="" alt="">
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-8 col-xs-8">
                        <div class="item">
                          <div class="title-2"> # </div>
                          <div class="title">
                            <strong></strong>
                          </div>
                          <div class="title-2"></div>
                         
                          <input type="hidden" name="responseType" id="responseType" > 
                                            
                          
                          
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-4">
                     <div class="btns-wrp">
                            <a class="scbtn scbtn-primary" href="#myreturn" data-toggle="modal" disabled="" onclick="" >
                              <span>Return</span>
                            </a>
                            <a class="scbtn scbtn-primary" href="#myreturn" data-toggle="modal" disabled="" onclick="" >
                              <span>Exchange</span>
                            </a>                            
                          </div>
                      </div>
                     
                </div>
                 
                  <div class="row">
                   <div class="col-md-2">
                      
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-4">
                      <div class="order-products-wrp">
                        <ul class="order-products">
                          <li style="height: 100px;">
                            <div class="img-wrp">
                              <img src="" alt="">
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-8 col-xs-8">
                        <div class="item">
                          <div class="title-2"> # </div>
                          <div class="title">
                            <strong></strong>
                          </div>
                          <div class="title-2"></div>
                         
                          <input type="hidden" name="responseType" id="responseType" > 
                                            
                          
                          
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-4">
                     <div class="btns-wrp">
                            <a class="scbtn scbtn-primary" href="#myreturn" data-toggle="modal" disabled="" onclick="" >
                              <span>Return</span>
                            </a>
                            <a class="scbtn scbtn-primary" href="#myreturn" data-toggle="modal" disabled="" onclick="" >
                              <span>Exchange</span>
                            </a>                            
                          </div>
                      </div>
                     
                </div>
               
               </div> 
              </div> 
            </div> 
                    
                  </div>
                </div>
               </div>
              </div>    
                  <!--//tab-->
                </section>

                <div id="myreturn" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <div class="modal-content">
                          <button type="button" class="close" data-dismiss="modal"><i class="icon-close"></i></button>
                            <div class="modal-header">
                              <h4 class="box-title" id="modal-box-title"> Oh no! We're sorry you didn't love your product. Let us fix  <br> that for you! <br>Tell us what went wrong:</h4>
                            </div>
                            <div class="modal-body">
                              <div class="offers-popup-wrp">
                                <div class="box-return-list">
                                  <div class="offers-list-wrp scrollbar">
                                    <ul class="offers-list">
                                      <li onclick="SetActive(this);">
                                        <label class="sc-checkbox">
                                          <i class="icon icon-diamond"></i>
                                          <span> It didn't fit.</span>
                                        </label>
                                      </li>
                                      <li onclick="SetActive(this);">
                                        <label class="sc-checkbox">
                                          <i class="icon icon-diamond"></i>
                                          <span> I don't love the quality.</span>
                                        </label>
                                      </li>
                                      <li onclick="SetActive(this);" >
                                        <label class="sc-checkbox ">
                                          <i class="icon icon-diamond"></i>
                                          <span>The product is damaged.</span>
                                        </label>
                                      </li>
                                      <li onclick="SetActive(this);">
                                        <label class="sc-checkbox">
                                          <i class="icon icon-diamond"></i>
                                          <span> It doesn't fit my style.</span>
                                        </label>
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="modalfooter">
                              <button class="scbtn scbtn-primary" onclick="saveUserResponse();" >
                                <span>SUBMIT</span>
                              </button>
                            </div>
                          </div>
                        </div>
                  </div>
              </div>


<script type="text/javascript">
  
  var strOptionSelected = '';
  function SetActive(obj)
  {
    $(obj).closest("ul").find("li").each(function(e) {
        $(this).removeClass('active');
        $(this).find('.sc-checkbox').removeClass('active');
    });
    $(obj).addClass('active');
    $(obj).find('.sc-checkbox').addClass('active');
    strOptionSelected = $(obj).find('span').text().trim();
    $('#user_response').val(strOptionSelected);
    console.log('strOptionSelected: '+strOptionSelected);
  }

  function userResponse(pid,oid,type,ouid)
  {    
    if(type=='return')
    {
      $('#modal-box-title').html('Oh no! We\'re sorry you didn\'t love your product. Tell us what went wrong so we can fix it when you come back: <br/><br>Reasons for returning');
    }else
    {
      $('#modal-box-title').html('Oh no! We\'re sorry you didn\'t love your product. Let us fix <br> that for you! <br><br>Tell us what went wrong:');
    }
    $('#product_id').val(pid);
    $('#order_id').val(oid);
    $('#response_type').val(type);
    $('#order_unique_no').val(ouid);
  }

  function saveUserResponse()
  {  
    var product_id = $('#product_id').val();
    var order_id = $('#order_id').val();
    var response_type = $('#response_type').val();
    var order_unique_no = $('#order_unique_no').val();
    //var user_response = $('#user_response').val();   
    var user_response = strOptionSelected;

    if(product_id!='' && order_id!='' && response_type!='' && user_response!='')
    { 
      if (confirm('Are you sure you want to '+response_type+' the product ?')) {
          $.ajax({
              url: sc_baseurl+'Sc_orders/updateUserResponse',
              type: 'post',
              data: {'response_type' : response_type,'product_id':product_id,'order_id':order_id, 'user_response':user_response,'order_unique_no':order_unique_no},
              success: function(data, status) {
                 data = data.trim();
                  msg(data);

                if(data!=''){ 
                  if(data=='success'){ 
                      //alert('Success');
                     $("#myreturn").modal("hide"); 
                     window.location.href=sc_baseurl+'my-orders';   
                  }else if(data == 'error'){                
                  }
                }          
              },
              error: function(xhr, desc, err) {
                // console.log(err);
              }
            });
        }
    }
  }

</script>