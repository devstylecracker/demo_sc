<?php
   //echo $form;
   
   $scbox_pack = unserialize(SCBOX_PACKAGE);
   
   
   
   
   ?>
<style>

   .page-scbox-form .list-occassion li{
      text-align: center !important;
   }

   .form-control{
   text-transform: capitalize !important;
   }
  
   .page-scbox-form .list-skin-tone li .title-3 {
   padding-top: 15px !important;
   }
   .page-scbox-form .list-skin-tone li{width: 60px;} 
   @media(max-width:768px) {
  
   .page-scbox-form ul.list-skin-tone li {
   width: 60px !important;
   }
   }
   .btn-group.special {
   display: flex !important;
   }
   .page-scbox-form .list-skin-tone li .pallet {
   height: 40px !important;
   width: 40px !important;
   }
   .special .btn {
   flex: 1 !important;
   }
 
   .list-skin-tone li {
   width: 76px !important;
   }
   .page-scbox-form .list-bags li.active,
   .page-scbox-form .list-footweartype li.active,
   .page-scbox-form .list-heeltype li.active {
   outline: 2px solid #13D792 !important;
   cursor: default !important;
   }
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li,
   .page-scbox-form .list-dneed li {
   display: inline-block !important;
   width: 146px !important;
   margin: 10px 4px !important;
   cursor: pointer !important;
   }
   .page-scbox-form .list-fabrics li.active,
   .page-scbox-form .list-prints li.active,
   .page-scbox-form .list-dneed li.active {
   outline: 2px solid #ed1b2e !important;
   cursor: default !important;
   }
   .page-scbox-form .list-fabrics li {
   display: inline-block !important;
   width: 140px !important;
   margin: 10px 4px !important;
   cursor: pointer !important;
   }
   .btn {
   border: 1px solid #000 !important;
   color: #333 !important;
   border-radius: 0px !important;
   outline: none !important;
   }
   .btn-green.active,
   .btn-green.active.focus,
   .btn-green:active {
   border: 1px solid #13D792 !important;
   background: #13D792 !important;
   color: #fff !important;
   outline: none !important;
   }
   .btn-red.active,
   .btn-red.active.focus,
   .btn-red:active {
   border: 1px solid #ed1b2e !important;
   background: #ed1b2e !important;
   color: #fff !important;
   outline: none !important;
   }
   .choose_file {
   position: relative !important;
   display: inline-block !important;
   border-bottom: #13D792 solid 1px;
   width: 300px !important;
   padding: 4px 6px 4px 8px !important;
   color: #000 !important;
   margin-top: 2px !important;
   background: white !important;
   }
   .choose_file input[type="file"] {
   -webkit-appearance: none !important;
   position: absolute !important;
   top: 0 !important;
   left: 0 !important;
   opacity: 0 !important;
   }
   
   .hwidth {
   width: 15% !important;
   text-transform: uppercase !important;
   }
   @media (max-width: 768px) {
   .hwidth {
   width: 60% !important;
   text-transform: uppercase !important;
   }
   }
   
   @media (max-width: 768px) {
   .page-scbox-form .list-colors li,
   .page-scbox-form .list-prints li,
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li {
   width: 80px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   }
   }
   @media (max-width: 768px) {
   .page-scbox-form .list-bags li img,
   .page-scbox-form .list-footweartype li img,
   .page-scbox-form .list-heeltype li img {
   max-width: 100% !important;
   }
   }
   @media (max-width: 768px) {
   .page-scbox-form ..list-bags li .pallet,
   .page-scbox-form .list-footweartype li .pallet,
   .page-scbox-form .list-heeltype li .pallet {
   width: 50px !important;
   height: 50px !important;
   margin: 5px auto 5px !important;
   }
   }
   .form-control {
   display: inline !important;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   height: 280px !important;
   }
   .pallet img {
   width: 146px !important;
   }
   @media (max-width: 768px) {
   .page-scbox-form .list-colors li,
   .page-scbox-form .list-prints li,
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li {
   width: 135px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   width: 100% !important;
   }
   }
   .extrapad {
   margin-top: 60px !important;
   margin-bottom: 60px !important;
   }
   /*.extrapad {
  margin-top: 60px;
   margin-bottom: 40px;
   padding-top: 60px;
   }
   .extrapad1 {
  margin-top: 30px;
   margin-bottom: 40px;
   }*/
   @media (max-width: 768px) {
      .extrapad {
  
   padding-top: 80px !important;
   }
   .extrapad1 {
  margin-top: 30px !important;
   margin-bottom: 40px !important;
   }
   }
   .page-scbox-form .pa-que{
      padding: 0 0 !important;
   }
   .page-scbox-form .bigimg li{
   display: inline-block !important;
   width: 180px !important;
   margin: 0px 4px 20px !important;
   cursor: pointer !important;
   background: #fafafa !important;
   border:1px solid #dfdfdf !important;
   }
   .noborder{
   border:none !important;
   }
   ul.list-categories{
   text-transform: capitalize !important;
   }
   .page-scbox-form .item.quiz .title {
   text-transform: capitalize !important;
   }
   .title-3{
   font-size: 13px !important;
   }
   @media (max-width: 768px)
   {
   .page-scbox-form .bigimg li{
   display: inline-block !important;
   width: 125px !important;
   margin: 0px 8px 20px !important;
   cursor: pointer !important;
   background: #fafafa !important;
   border:1px solid #dfdfdf !important;
   }
   .noborder{
   border:none !important;
   }
   }
   .title-section-changed .title-changed{
   margin: 0;
   line-height: 1;
   color: #333;
   display: inline-block;
   font-weight:normal;
   font-size:13px;
   letter-spacing: 2px;
   font-style:italic;
   }
   @media (max-width: 768px)
   {
   .title-section-changed .title-changed {
   font-size: 13px;
   line-height: 1.3;
   }
   }
   .mobile-align {
   width: 28% !important;
   text-align: left !important;
   margin-left:38% !important; 
   }

   .desktop-align{
    width: 25% !important;
   text-align: left !important;
   margin-left:46% !important;
   }

   .mobile-shade {
   width: 68% !important;
   text-align: left !important;
   margin-left:22% !important; 
   }

   .desktop-shade{
    width: 25% !important;
   text-align: left !important;
   margin-left:38% !important;
   }
 label{
      margin-top: 5px !important;
   }
</style>

    <div class="page-scbox-form">

        <!-- action="<?php //echo base_url(); ?>book-scbox/<?php //echo @$userdata['scbox_objectid']; ?>"-->

        <form name="scbox-form" id="scbox-form" method="post" action="">

            <div id="slide_1">

                <section class="section section-shadow ">
                 <!-- <div  class="box-message <?php  //echo @$order_msgclass==1  ? 'success':'error'; ?> text-bold" id="order-message"><?php //echo $order_message; ?></div> -->

                    <div class="title-2">You Are Almost Done ! Just A Few More Questions To Go</div>

                    <div class="women_afterpayment category-apparel">

                        <div class="product-item extrapad section-height">
                        

                            <div class="pa-que">

                                <span>How Tall Are You?</span>
                                <div class="title-section-changed text-center"  >
                                <h1 class="title-changed">
                                   (Pick one)
                                </h1>
                             </div>

                            </div>

                       

                           
                            <div class="item-sizes-wrp">

                                <div class="form-group">

                                    <select name="height-height_feet" type="height-feet" class="form-control hwidth height_feet" question_id="40">

                                        <option value="" answer_id="392">Select height</option>

                                        <option value="192">3 ft</option>

                                        <option value="193">4 ft</option>

                                        <option value="194">5 ft</option>

                                        <option value="195">6 ft</option>

                                    </select>

                                    <select name="height-height_inches" type="height-inches" class="form-control hwidth height_inches" question_id="41">

                                        <option value="" answer_id="393">Select height</option>

                                        <option value="196">0 inch</option>

                                        <option value="197">1 inch</option>

                                        <option value="198">2 inch</option>

                                        <option value="199">3 inch</option>

                                        <option value="200">4 inch</option>

                                        <option value="201">5 inch</option>

                                        <option value="202">6 inch</option>

                                        <option value="203">7 inch</option>

                                        <option value="204">8 inch</option>

                                        <option value="205">9 inch</option>

                                        <option value="206">10 inch</option>

                                        <option value="207">11 inch</option>

                                    </select>

                                </div>

                            </div>

                            <label id="height-error" class="message error hide" for="height_ans">Please select height</label>

                        </div>

                    </div>

                    <div class="women_bags_afterpayment category-bags" question_id="42">

                        <div class="product-item">

                            <div class="pa-que">

                                <span>You Selected A Bag! Could You Tell Us A Little More About The Kind Of Bag You Want?</span></div>

                            <div class="pa-que">

                               <span> I Want It For</span>
                                <div class="title-section-changed text-center"  >
                                    <h1 class="title-changed">
                                       (Pick one)
                                    </h1>
                                 </div>

                            </div>

                        <ul class="add-event list-occassion purpose lespad section-bag-i-want-for" type="section-bag-i-want-for">

                            <li>

                                <label class="sc-checkbox"><i class="icon icon-diamond"></i>

                                    <input type="checkbox"> <span answer_id="208">everyday</span></label>

                            </li>

                            <li>

                                <label class="sc-checkbox"><i class="icon icon-diamond"></i>

                                    <input type="checkbox"> <span answer_id="209">work</span></label>

                            </li>

                            <li>

                                <label class="sc-checkbox"><i class="icon icon-diamond"></i>

                                    <input type="checkbox"> <span answer_id="210">party</span></label>

                            </li>

                        </ul>

                        <label id="occassion-error" class="message error hide" for="occassion">Please select atleast one</label>

                        <div class="product-item extrapad">

                            <div class="box-pa">

                            <div class="pa-que">

                                <span>I Want The Size Of My Bag To Be</span>
                                <div class="title-section-changed text-center"  >
                                    <h1 class="title-changed">
                                       (Pick one)
                                    </h1>
                                 </div>
                            </div>

                            </div>
                           
                            <div class="item-sizes-wrp">

                                <div class="form-group">

                                    <select class="form-control hwidth bag-size" type="bag-size" question_id="43">

                                        <option value="">Select bag size</option>

                                        <option value="211">small</option>

                                        <option value="212">medium</option>

                                        <option value="213">large</option>

                                    </select>

                                </div>

                            </div>

                            <label id="scbox_sizebag-error" class="message error hide" for="scbox_sizebag">Please select Bag size</label>

                        </div>

                        <div class="container bag-color-avoid">

    <div class="box-pa">

        <div class="pa-que">

            <span>Colors I <i style="color: #ed1b2e;"> Avoid</i></span>
           <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                     </div>
        </div>

    <ul class="add-event list-skin-tone list-colordneed actdeact color-to-avoid-afterpayment bag-color-avoid" type="bag-color-avoid">

        <li>
                        <div class="pallet" color="#90140c" style="background-color:#90140c;border:1px solid #90140c;"></div>
                     </li>
                      <li>
                        <div class="pallet" color="#b8bebe" style="background-color:#b8bebe;border:1px solid #b8bebe;"></div>
                     </li>
                      <li>
                        <div class="pallet" color="#be914a" style="background-color:#be914a;border:1px solid #be914a;"></div>
                     </li>
                     <li>
                        <div class="pallet" color="#ff0080" style="background-color:#FF0080;border:1px solid #FF0080;"></div>
                     </li>
                     <li>
                        <div class="pallet" color="#734021" style="background-color:  #734021;border:1px solid #734021;"></div>
                     </li>

                     <li>
                        <div class="pallet" color="#fec4c3" style="background-color:#fec4c3;border:1px solid #fec4c3;"></div>
                     </li>
                     <li>
                        <div class="pallet" color="#0b0706" style="background-color:#0b0706;border:1px solid #0b0706;"></div>
                     </li>
                     <li>
                        <div class="pallet" color="#98012e" style="background-color:  #98012e;border:1px solid #98012e;"></div>
                     </li>
                     <li>
                        <div class="pallet" color="#ffffff" style="background-color:#FFFFFF;border:1px solid #FFFFFF;"></div>
                     </li>
                     <li>
                        <div class="pallet" color="#f57d31" style="background-color:#f57d31;border:1px solid #f57d31;"></div>
                     </li>
                     <li>
                        <div class="pallet" color="#87ceeb" style="background-color:#87ceeb;border:1px solid #87ceeb;"></div>
                     </li>
                     <li>
                        <div class="pallet" color="#cdd1d1" style="background-color:#cdd1d1;border:1px solid #cdd1d1;"></div>
                     </li>
                     <li>
                        <div class="pallet" color="#d5011a" style="background-color:#d5011a;border:1px solid #d5011a;"></div>
                     </li>
                      <li>
                        <div class="pallet" color="#800080" style="background-color:#800080;border:1px solid #800080;"></div>
                     </li>
                     <li>
                        <div class="pallet" color="#bcd2ee" style="background-color:#bcd2ee;border:1px solid #bcd2ee;"></div>
                     </li>
                    
                     <li>
                        <div class="pallet" color="#576331" style="background-color:#576331;border:1px solid #576331;"></div>
                     </li>
                     <li>
                        <div class="pallet" color="#041531" style="background-color:#041531;border:1px solid #041531;"></div>
                     </li>
                     <li>
                        <div class="pallet" color="#f7b719" style="background-color:#f7b719;border:1px solid #f7b719;"></div>
                     </li><br>
                      <li class="sc-checkbox" style="width:220px !important;">
                         <div class="title-3" color="I m a rainbow I like all colors">
                            <i class="icon icon-diamond"></i>I'm a rainbow, I like all colors
                               <input type="checkbox">
                         </div>
                      </li> 

    </ul>

           

    <label id="coloravoid-error" class="message error hide" for="coloravoid">Please select a color</label>

</div>

                        <div class="container extrapad bag-print-avoid">

    <div class="box-pa">

        <div class="pa-que">

            <span>Prints and Patterns I <i style="color: #ed1b2e;">Avoid</i>

<!-- <i style="font-size:90%;">(Select 1 or more)</i> -->

</span>
    <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>

        </div>

    </div>

    <ul class="add-event list-colors list-prints other-type bag-print-avoid" type="bag-print-avoid">

         <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/prints/polka.jpg" alt="Polka dots">
                        </div>
                        <div class="title-3">polka dots</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/prints/floral.jpg" alt="Floral">
                        </div>
                        <div class="title-3">floral</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/prints/stripes.jpg" alt="Stripes">
                        </div>
                        <div class="title-3">stripes</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/prints/animal.jpg" alt="Animal">
                        </div>
                        <div class="title-3">animal</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/prints/check.jpg" alt="Checks">
                        </div>
                        <div class="title-3">checks</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/prints/solid.jpg" alt="Solid">
                        </div>
                        <div class="title-3">solid</div>
                     </li>

    </ul>

    <label id="printavoid-error" class="message error hide" for="printavoid">Please select print to avoid</label>

</div>

                        

                        

                        

                    </div>

                    <div class="women_footwear_afterpayment category-footwear" question_id="46">

                        <div class="container extrapad">

                            <div class="box-pa">

                                <div class="pa-que">

                                    <span>You’re Looking For A Pair Of Heels! Could You Tell Us What Kind Of Heels You’d Like ?<br>The Type Of Heels I Am Looking For</span>
                                     <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                                </div>

                            </div>

                            <ul class="list-colors add-event footwear-heel-type" type="footwear-heel-type">

                                <li>

                                    <div class="pallet">

                                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/footwear/1wedge.jpg">

                                    </div>

                                    <div class="title-3" answer_id="214">wedge</div>

                                </li>

                                <li>

                                    <div class="pallet">

                                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/footwear/1block.jpg">

                                    </div>

                                    <div class="title-3" answer_id="215">block</div>

                                </li>

                                <li>

                                    <div class="pallet">

                                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/footwear/1stilettoes.jpg">

                                    </div>

                                    <div class="title-3"  answer_id="216">stilletos</div>

                                </li>

                            </ul>

                            <label id="heeltype-error" class="message error hide" for="heeltype">Please select heel type</label>

                        </div>

                        <div class="product-item">
                             <div class="box-pa">

                            <div class="pa-que">

                                <span>  How Tall Would You Like Your Heels To be ?</span>
                                 <div class="title-section-changed text-center"  >
                                    <h1 class="title-changed">
                                       (Pick one)
                                    </h1>
                                 </div>
                            </div>

                            </div>
                            

                            <div class="item-sizes-wrp" id="sizeheelh">

                                <select class="form-control hwidth footwear-heel-height" type="footwear-heel-height" question_id="47">

                                    <option value="">Select heel height</option>

                                    <option value="217">low: 1.5”-2.5”</option>

                                    <option value="218">medium: 2.5”-3.5” </option>

                                    <option value="219">high: 3.5” and above</option>

                                </select>

                            </div>

                            <label id="sizeheelh-error" class="message error hide" for="sizeheelh">Please select heel height</label>

                        </div>

                    </div>

                    <div class="women_sunglasses_afterpayment category-accessories">

                        <div class="container accessories-sunglasses-container" question_id="48">

                            <div class="box-pa">

                                <div class="pa-que"><span>You Are Looking For Sunglasses ! Tell Us What Kind? <br>Sunglasses</span>
                                <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                                </div>

                            </div>

                            <ul class="add-event list-colors accessories-sunglasses-types" type="accessories-sunglasses-types">

                                <li>

                                    <div class="pallet">

                                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/sunglass/aviator.jpg">

                                    </div>

                                    <div class="title-3" answer_id="220">aviators</div>

                                </li>

                                <li>

                                    <div class="pallet">

                                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/sunglass/cat.jpg">

                                    </div>

                                    <div class="title-3" answer_id="221">cat eyes</div>

                                </li>

                                <li>

                                    <div class="pallet">

                                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/sunglass/round.jpg">

                                    </div>

                                    <div class="title-3" answer_id="222">round</div>

                                </li>

                                <li>

                                    <div class="pallet">

                                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/sunglass/square.jpg">

                                    </div>

                                    <div class="title-3" answer_id="223">square</div>

                                </li>

                                <li>

                                    <div class="pallet">

                                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/sunglass/wayfarer.jpg">

                                    </div>

                                    <div class="title-3" answer_id="224">wayfarers</div>

                                </li>

                            </ul>

                            <label id="sunglass-error" class="message error hide" for="sunglass">Please select atleast one sunglass</label>

                        </div>

                        <div class="product-item extrapad accessories-refelector-container" question_id="49">
                             <div class="box-pa">

                            <div class="pa-que">

                                <span> 
                                Reflectors</span>
                                 <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick yes or no)</h1>
                     </div>
                            </div>

                            </div>
                           

                            <ul class="add-event list-occassion purpose lespad accessories-refelector-types" type="accessories-refelector-types">

                                <li>

                                    <label class="sc-checkbox"><i class="icon icon-diamond"></i>

                                        <input type="checkbox"> <span answer_id="225">yes</span></label>

                                </li>

                                <li>

                                    <label class="sc-checkbox"><i class="icon icon-diamond"></i>

                                        <input type="checkbox"> <span answer_id="226">no</span></label>

                                </li>

                            </ul>

                            <label id="reflector-error" class="message error hide" for="reflector">Please select atleast one</label>

                        </div>

                        <div class="container accessories-belt-container" question_id="50">

                            <div class="box-pa">

                                <div class="title-2" id="">

                                    <div class="pa-que">

                                        <span>You Are Looking For A Belt! Could You Tell Us What Kind Of Belt ?<br>Belts</span>
                                    <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                 </div>

                                </div>

                            </div>

                            <ul class="add-event accessories-belt-types list-colors" type="accessories-belt-types">

                                <li >

                                    <div class="pallet">

                                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/belts/1slim.jpg">

                                    </div>

                                    <div class="title-3" answer_id="227">slim</div>

                                </li>

                                <li>

                                    <div class="pallet">

                                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/belts/1medium.jpg">

                                    </div>

                                    <div class="title-3" answer_id="228">medium</div>

                                </li>

                                <li>

                                    <div class="pallet">

                                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/belts/1chunky.jpg">

                                    </div>

                                    <div class="title-3" answer_id="229">chunky</div>

                                </li>

                            </ul>

                            <label id="belts-error" class="message error hide" for="belts">Please select a belt</label>

                        </div>

                    </div>

                    

                    

                    <div class="women_jewellery_afterpayment category-jewellery" question_id="51">

                        <div class="container">

                            <div class="box-pa">

                                <div class="title-2" id="">

                                    <div class="pa-que"><span>

You Are Looking For Jewellery! Could You Tell Us The Tone Of Jewellery You Are Looking For

<!-- <i style="font-size:90%;">(Select 1 or more)</i> -->

<br>The Tone Of Jewellery I Am looking For</span>
 <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                     </div>

                                </div>

                            </div>

                            <ul class="add-event jewellery-tone list-colors" type="jewellery-tone">

                                <li>

                                    <div class="pallet">

                                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/jewellery/1gold.jpg">

                                    </div>

                                    <div class="title-3" answer_id="230">gold</div>

                                </li>

                                <li>

                                    <div class="pallet">

                                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/jewellery/1silver.jpg">

                                    </div>

                                    <div class="title-3" answer_id="231">silver</div>

                                </li>

                                <li>

                                    <div class="pallet">

                                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/jewellery/1oxidized.jpg">

                                    </div>

                                    <div class="title-3" answer_id="232">oxidised</div>

                                </li>

                                <li>

                                    <div class="pallet">

                                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/jewellery/1multicolored.jpg">

                                    </div>

                                    <div class="title-3" answer_id="233">multicolored</div>

                                </li>

                                <li >

                                    <div class="pallet">

                                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/jewellery/1tonal.jpg">

                                    </div>

                                    <div class="title-3" answer_id="234">tonal</div>

                                </li>

                            </ul>

                            <label id="jewellerytone-error" class="message error hide" for="jewellerytone">Please select jewellery tone</label>

                        </div>

                    </div>

                    <div class="women_beauty_afterpayment category-beauty" question_id="52">

                        <div class="container extrapad">

                            <div class="title-2" id="">

                                <div class="pa-que"><span>You Are Looking For A Beauty Product! Could You Tell Us More ? <br>The Finish Of The Beauty Product I’m Looking For</span>
                               <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick one)
                        </h1>
                     </div>
                 </div>

                            </div>

                            <ul class="add-event list-occassion purpose lespad beauty-finishing-type" type="beauty-finishing-type">

                                <li >

                                    <label class="sc-checkbox"><i class="icon icon-diamond"></i>

                                        <input type="checkbox"> <span answer_id="235">matte</span></label>

                                </li>

                                <li>

                                    <label class="sc-checkbox"><i class="icon icon-diamond"></i>

                                        <input type="checkbox"> <span  answer_id="236">shiny</span></label>

                                </li>

                            </ul>

                            <label id="finish-error" class="message error hide" for="finish">Please select the type of finish</label>

                        </div>

                    </div>

                </section>

            </div>

            <section class="section section-shadow">

                <div class="btns-wrp">

                   <!--  <a class="scbtn scbtn-primary" href="javascript:void(0);" onclick="ChangeStep(33);"><span>Previous</span></a> -->

                    <a class="scbtn scbtn-primary" href="<?php echo base_url();?>book-scbox/step6" onclick=""><span>Next</span></a>

                    <!-- <button class="scbtn scbtn-primary" onclick="slideprev(4);"><span>Previous</span></button>

<button class="scbtn scbtn-primary"  onclick="slidenext(4);"><span>Next</span></button> -->

                </div>

            </section>

    </div>

    </form>

    </div>

    <style>

        .scbox-payment-options li {

            display: inline-block;

            margin-right: 10px;

        }

        

        .scbox-payment-options .sc-checkbox .icon {

            position: relative;

            top: 10px !important;

        }

        

        input[type=radio] {

            display: none;

        }

    </style>

