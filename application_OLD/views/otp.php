
<div class="page-pa">
  <div id="slide1" class="pa-slide-box">
    <section class="section section-shadow">
      <div class="container">
        <!-- pa verify mobile -->
        <div class="title-section">
          <h1 class="title">Verify Your Account</h1>
        </div>
        <div class="row">
          <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
            <div class="box-pa">
              <p>A verification code has been sent to
                <br />.</p>
              <p class="text-bold">Kindly enter it below</p>
			  <?php 
              if(!empty($this->session->flashdata('error')))
                {
                  ?>
                  <h3 class="btn btn-block btn-danger"><?php echo $this->session->flashdata('error');?></h3>
                <?php
                }
          ?>

			  <form method="post" enctype="multipart/form-data" action="<?php base_url('otp');?>" id="validate_otp" name="validate_otp">
				  <div class="form-group">
					<input type="text" id="otp" name="otp" class="form-control input-control-mobile error222 success222" placeholder="Code"  maxlength="4" minlength="4">
					<div class="message text-error hide" id="otp-error"><?php echo form_error('otp'); ?></div>
				  </div>
				  <p class="help-block"><?php echo form_error('otp'); ?></p>
				  <div class="text-center">
					<button id="checkOtp" type="submit" onclick="" class="scbtn scbtn-primary"><span>Verify</span></button>
				  </div>
			</form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section section-shadow">
      <div class="container">
        <div class="text-center">
          <div class="mobile-verify-message hide">
            <p class="text-success">
              <i class="fa fa-check"></i>
            </p>
            <p class="text-danger">
              <i class="fa fa-close"></i>
              <br /> The number you entered was incorrect.
            </p>
            <p>
              Please re-enter the correct OTP
            </p>
          </div>
          <button  onclick="" class="scbtn scbtn-tertiary"><span>Resend OTP</span></button>
        </div>
      </div>
    </section>
  </div>
  <!-- /. pa verify mobile -->


</div>
<!-- /. page -->
