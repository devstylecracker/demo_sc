<?php
   //echo $form;
   
   $scbox_pack = unserialize(SCBOX_PACKAGE);
   
   
   
   
   ?>
<style>

   .page-scbox-form .list-occassion li{
      text-align: center !important;
   }

   ul.lespad {
   display: flex !important;
   }
   .form-control{
   text-transform: capitalize !important;
   }
  
   .page-scbox-form .list-skin-tone li .title-3 {
   padding-top: 15px !important;
   }
   .page-scbox-form .list-skin-tone li{width: 60px;} 
   @media(max-width:768px) {
  
   .page-scbox-form ul.list-skin-tone li {
   width: 60px !important;
   }
   }
   .btn-group.special {
   display: flex !important;
   }
   .page-scbox-form .list-skin-tone li .pallet {
   height: 40px !important;
   width: 40px !important;
   }
   .special .btn {
   flex: 1 !important;
   }
 
   .list-skin-tone li {
   width: 76px !important;
   }
   .page-scbox-form .list-bags li.active,
   .page-scbox-form .list-footweartype li.active,
   .page-scbox-form .list-heeltype li.active {
   outline: 2px solid #13D792 !important;
   cursor: default !important;
   }
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li,
   .page-scbox-form .list-dneed li {
   display: inline-block !important;
   width: 146px !important;
   margin: 10px 4px !important;
   cursor: pointer !important;
   }
   .page-scbox-form .list-fabrics li.active,
   .page-scbox-form .list-prints li.active,
   .page-scbox-form .list-dneed li.active {
   outline: 2px solid #ed1b2e !important;
   cursor: default !important;
   }
   .page-scbox-form .list-fabrics li {
   display: inline-block !important;
   width: 140px !important;
   margin: 10px 4px !important;
   cursor: pointer !important;
   }
   .btn {
   border: 1px solid #000 !important;
   color: #333 !important;
   border-radius: 0px !important;
   outline: none !important;
   }
   .btn-green.active,
   .btn-green.active.focus,
   .btn-green:active {
   border: 1px solid #13D792 !important;
   background: #13D792 !important;
   color: #fff !important;
   outline: none !important;
   }
   .btn-red.active,
   .btn-red.active.focus,
   .btn-red:active {
   border: 1px solid #ed1b2e !important;
   background: #ed1b2e !important;
   color: #fff !important;
   outline: none !important;
   }
   .choose_file {
   position: relative !important;
   display: inline-block !important;
   border-bottom: #13D792 solid 1px;
   width: 300px !important;
   padding: 4px 6px 4px 8px !important;
   color: #000 !important;
   margin-top: 2px !important;
   background: white !important;
   }
   .choose_file input[type="file"] {
   -webkit-appearance: none !important;
   position: absolute !important;
   top: 0 !important;
   left: 0 !important;
   opacity: 0 !important;
   }
   
   .hwidth {
   width: 15% !important;
   text-transform: uppercase !important;
   }
   @media (max-width: 768px) {
   .hwidth {
   width: 60% !important;
   text-transform: uppercase !important;
   }
   }
   
   @media (max-width: 768px) {
   .page-scbox-form .list-colors li,
   .page-scbox-form .list-prints li,
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li {
   width: 80px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   }
   }
   @media (max-width: 768px) {
   .page-scbox-form .list-bags li img,
   .page-scbox-form .list-footweartype li img,
   .page-scbox-form .list-heeltype li img {
   max-width: 100% !important;
   }
   }
   @media (max-width: 768px) {
   .page-scbox-form ..list-bags li .pallet,
   .page-scbox-form .list-footweartype li .pallet,
   .page-scbox-form .list-heeltype li .pallet {
   width: 50px !important;
   height: 50px !important;
   margin: 5px auto 5px !important;
   }
   }
   .form-control {
   display: inline !important;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   height: 280px !important;
   }
   .pallet img {
   width: 146px !important;
   }
   @media (max-width: 768px) {
   .page-scbox-form .list-colors li,
   .page-scbox-form .list-prints li,
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li {
   width: 135px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   width: 100% !important;
   }
   }
   .extrapad {
   margin-top: 60px !important;
   margin-bottom: 60px !important;
   }
   /*.extrapad {
  margin-top: 60px;
   margin-bottom: 40px;
   padding-top: 60px;
   }
   .extrapad1 {
  margin-top: 30px;
   margin-bottom: 40px;
   }*/
   @media (max-width: 768px) {
      .extrapad {
  
   padding-top: 80px !important;
   }
   .extrapad1 {
  margin-top: 30px !important;
   margin-bottom: 40px !important;
   }
   }
   .page-scbox-form .pa-que{
      padding: 0 0 !important;
   }
   .page-scbox-form .bigimg li{
   display: inline-block !important;
   width: 180px !important;
   margin: 0px 4px 20px !important;
   cursor: pointer !important;
   background: #fafafa !important;
   border:1px solid #dfdfdf !important;
   }
   .noborder{
   border:none !important;
   }
   ul.list-categories{
   text-transform: capitalize !important;
   }
   .page-scbox-form .item.quiz .title {
   text-transform: capitalize !important;
   }
   .title-3{
   font-size: 13px !important;
   }
   @media (max-width: 768px)
   {
   .page-scbox-form .bigimg li{
   display: inline-block !important;
   width: 125px !important;
   margin: 0px 8px 20px !important;
   cursor: pointer !important;
   background: #fafafa !important;
   border:1px solid #dfdfdf !important;
   }
   .noborder{
   border:none !important;
   }
   }
   .title-section-changed .title-changed{
   margin: 0;
   line-height: 1;
   color: #333;
   display: inline-block;
   font-weight:normal;
   font-size:13px;
   letter-spacing: 2px;
   font-style:italic;
   }
   @media (max-width: 768px)
   {
   .title-section-changed .title-changed {
   font-size: 13px;
   line-height: 1.3;
   }
   }
   .mobile-align {
   width: 28% !important;
   text-align: left !important;
   margin-left:38% !important; 
   }

   .desktop-align{
   	width: 25% !important;
   text-align: left !important;
   margin-left:46% !important;
   }

   .mobile-shade {
   width: 68% !important;
   text-align: left !important;
   margin-left:22% !important; 
   }

   .desktop-shade{
   	width: 25% !important;
   text-align: left !important;
   margin-left:38% !important;
   }
 label{
      margin-top: 5px !important;
   }
</style>
<div class="page-scbox-form">
   <!-- action="<?php //echo base_url(); ?>book-scbox/<?php //echo @$userdata['scbox_objectid']; ?>"-->
   <form name="scbox-form" id="scbox-form" method="post" action="">
      <div id="slide_1" class="">
         <!-- class=" <?php //echo (@$_GET['step']=='1' || @$_GET['step']=='' ) ? '' : 'hide'; ?>"> -->
         <div class="women_categories category-selected">
            <section class="section section-shadow ">
               <div class="container section-category-selected scbox-container" type="checkCategories">
                  <span class="box-pa">
                     <div class="pa-que"><span>What I Want In My Box</span>
                     <div class="title-section-changed text-center"  >
                        <h1 class="title-changed" id="catsel">
                           
                        </h1>
                     </div>
                     </div>
                  </span>   
                     <div class="desktop-align hidden-xs hidden-sm">
                        <ul class="list-categories pa-women" type="pa-women">
                           <li>
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>apparel</span></label>
                              <br>
                           </li>
                           <li>
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>bags</span></label>
                              <br>
                           </li>
                           <li style="display: none;">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>footwear</span></label>
                              <br>
                           </li>
                           <li>
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>accessories</span></label>
                              <br>
                           </li>
                           <li>
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>jewellery</span></label>
                              <br>
                           </li>
                           <li>
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>beauty</span></label>
                              <br>
                           </li>
                        </ul>
                     </div>


                     <div class="mobile-align hidden-md hidden-lg">
                        <ul class="list-categories pa-women" type="pa-women">
                           <li>
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>apparel</span></label>
                              <br>
                           </li>
                           <li>
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>bags</span></label>
                              <br>
                           </li>
                           <li style="display: none;">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>footwear</span></label>
                              <br>
                           </li>
                           <li>
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>accessories</span></label>
                              <br>
                           </li>
                           <li>
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>jewellery</span></label>
                              <br>
                           </li>
                           <li>
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>beauty</span></label>
                              <br>
                           </li>
                        </ul>
                     </div>

                  <label id="women_categories-error" class="message error hide" for="women_categories">Choose categories</label>
               </div>
            </section>
            <section class="section section-shadow">
            <div class="btns-wrp">
               <!-- <a class="scbtn scbtn-primary" href="javascript:void(0);" onclick="slideprev(2);" ><span>Previous</span></a> -->
               <a class="scbtn scbtn-primary link-scrolling" href="javascript:void(0);" onclick="ChangeStep(2,'next');"><span>Next</span></a>
            </div>
         </div>
      </div>
      <div id="slide_2" class="hide"> <!-- class="hide" -->
        <section class="section section-shadow">
         <!-- class="<?php //echo ( @$_GET['step']==2) ? '' : 'hide'; ?>"> -->
         <div id="question_1_1" class="pa-slide-box body-shape category-apparel section-pa" question_id="15">
               <div class="container ">
                  <div class="pa-que">
                     <span> This Is What My Body Looks Like </span>
                     <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick one)
                        </h1>
                     </div>
                  </div>
                   <label class="message error hide hidden-lg hidden-md" for="body_shape_ans">Please select body shape</label>
                  <div class="box-body-shape female">
                     <div class="row pa-container women" name="scbox_bodyshape" id="scbox_bodyshapewomen">
                        <div class="col-md-2 col-sm-4 col-xs-6 item-col deactive" data-attr="1" data-attr2="1" data-attr3="Hourglass"  >
                           <div class="item quiz" id="bodyshape_1">
                              <div class="img-wrp">
                                 <img class="" id="oignal_img1" src="<?php echo base_url(); ?>assets/images/pa/female/shapes/1hourglass-s.png">
                                 <img class="hide" id="selected_img1" src="<?php echo base_url(); ?>assets/images/pa/female/shapes/1hourglass-select.png">
                              </div>
                              <div class="title" answer_id="60">hourglass</div>
                              <div class="desc">Medium bust
                                 <br>Slim waist
                                 <br>Medium hips
                              </div>
                           </div>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 item-col deactive" data-attr="2" data-attr2="1" data-attr3="Apple" >
                           <div class="item quiz" id="bodyshape_2">
                              <div class="img-wrp">
                                 <img class="" id="oignal_img2" src="<?php echo base_url(); ?>assets/images/pa/female/shapes/1apple-s.png">
                                 <img class="hide" id="selected_img2" src="<?php echo base_url(); ?>assets/images/pa/female/shapes/1apple-select.png">
                              </div>
                              <div class="title" answer_id="61">apple</div>
                              <div class="desc">Heavy bust
                                 <br>Wide waist
                                 <br>Narrow hips
                              </div>
                           </div>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 item-col deactive" data-attr="3" data-attr2="1" data-attr3="Column">
                           <div class="item quiz" id="bodyshape_3">
                              <div class="img-wrp">
                                 <img class="" id="oignal_img3" src="<?php echo base_url(); ?>assets/images/pa/female/shapes/1column-s.png">
                                 <img class="hide" id="selected_img3" src="<?php echo base_url(); ?>assets/images/pa/female/shapes/1column-select.png">
                              </div>
                              <div class="title"  answer_id="62">column</div>
                              <div class="desc">Small bust
                                 <br>Slender waist
                                 <br>Narrow hips
                              </div>
                           </div>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 item-col deactive" data-attr="4" data-attr2="1" data-attr3="Pear">
                           <div class="item quiz" id="bodyshape_4">
                              <div class="img-wrp">
                                 <img class="" id="oignal_img4" src="<?php echo base_url(); ?>assets/images/pa/female/shapes/1pear-s.png">
                                 <img class="hide" id="selected_img4" src="<?php echo base_url(); ?>assets/images/pa/female/shapes/1pear-select.png">
                              </div>
                              <div class="title" answer_id="63">pear</div>
                              <div class="desc">Small bust
                                 <br>Medium waist
                                 <br>Wider hips and
                                 <br>Upper thighs
                              </div>
                           </div>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 item-col deactive" data-attr="5" data-attr2="1" data-attr3="Lollipop">
                           <div class="item quiz" id="bodyshape_5">
                              <div class="img-wrp">
                                 <img class="" id="oignal_img5" src="<?php echo base_url(); ?>assets/images/pa/female/shapes/1inverted-s.png">
                                 <img class="hide" id="selected_img5" src="<?php echo base_url(); ?>assets/images/pa/female/shapes/1inverted-select.png">
                              </div>
                              <div class="title" answer_id="64">inverted triangle</div>
                              <div class="desc">Heavy bust
                                 <br>Slender waist
                                 <br>Narrow hips
                                 <br>Slim legs
                              </div>
                           </div>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 item-col deactive" data-attr="6" data-attr2="1" data-attr3="Goblet">
                           <div class="item quiz" id="bodyshape_6">
                              <div class="img-wrp">
                                 <img class="" id="oignal_img6" src="<?php echo base_url(); ?>assets/images/pa/female/shapes/1goblet-s.png">
                                 <img class="hide" id="selected_img6" src="<?php echo base_url(); ?>assets/images/pa/female/shapes/1goblet-select.png">
                              </div>
                              <div class="title" answer_id="65">goblet</div>
                              <div class="desc">Broad shoulders
                                 <br>Heavy bust
                                 <br>Medium waist
                                 <br>Narrow hips
                                 <br>Slim legs
                              </div>
                           </div>
                        </div>
                        <label class="message error hide hidden-xs hidden-sm" for="body_shape_ans">Please select body shape</label>
                     </div>
                  </div>
               </div>
             </div>
         <!--Stuffs i dont need-->
         <div class="women-apparel category-apparel hide"><!-- hide -->
            <div class="women_apparel_beforepayment">
               <div id="question_1_2" class="container extrapad section-stuff-dont-like" question_id="16">
                  <div class="pa-que">
                     <span>
                        Stuff I <i style="color: #ed1b2e;"> don't</i> need
                        <!-- <i style="font-size:90%;">(Select 1 or more)</i> -->
                     </span>
                     <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick upto 4)
                        </h1>
                     </div>
                  </div>
                   <label id="stuffs_dont_need_ans-error" class="message error hide hidden-lg hidden-md" for="stuffs_dont_need_ans">Please select atleast one</label>
                  <ul class="add-event list-colors list-dneed other-type stuff-dont-need actdeact" type="stuff-dont-need">
                     <li>
                        <div class="pallet">
                         
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/apparelavoid/tops.jpg " alt="TOPS">
                        </div>
                        <div class="title-3" answer_id="66">tops</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/apparelavoid/dress.jpg" alt="DRESSES">
                        </div>
                        <div class="title-3" answer_id="67">dresses</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/apparelavoid/shorts.jpg" alt="SHORTS AND SKIRTS">
                        </div>
                        <div class="title-3" answer_id="68">shorts and skirts</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/apparelavoid/jeans.jpg" alt="JEANS AND TROUSERS">
                        </div>
                        <div class="title-3"  answer_id="69">jeans and trousers</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/apparelavoid/outerwear.jpg" alt="OUTERWEAR">
                        </div>
                        <div class="title-3"  answer_id="70">outerwear</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/apparelavoid/jumpsuit.jpg" alt="JUMPSUITS">
                        </div>
                        <div class="title-3" answer_id="71">jumpsuits</div>
                     </li>
                  </ul>
                  <label id="stuffs_dont_need_ans-error" class="message error hide hidden-xs hidden-sm" for="stuffs_dont_need_ans">Please select atleast one</label>
               </div>
                   
               <div id="question_1_3" class="product-item section-size-top" question_id="17">
                 <!--  <div class="pa-que">
                     <span>Top
                     </span>
                        <div class="title-section-changed text-center"  >
                            <h1 class="title-changed">
                               (Pick one)
                            </h1>
                         </div>
                  </div> -->
                   <div class="pa-que">
                         <span>Size And Fit</span>
                          <div class="title-section-changed text-center"  >
                            <h1 class="title-changed">
                               (Answer All)
                            </h1>
                         </div>
                     </div>
                  <div class="item-sizes-wrp">
                     <div class="form-group">
                        <select class="form-control hwidth size-top" type="size-top" name="scbox_sizetop">
                          <option value="352">Select top size</option>
                           <option value="72">xs/uk 4/us 2/eu 32</option>
                           <option value="73">s/uk 6/us 4/eu 34</option>
                           <option value="74">s/uk 8/us 6/eu 36</option>
                           <option value="75">m/uk 10 /us 8/eu 38</option>
                           <option value="76">m/uk 12/us 10/eu 40</option>
                           <option value="77">l/uk 14/us 12/eu 42</option>
                           <option value="78">l/uk 16/us 14/eu 44</option>
                           <option value="79">xl/uk 18/us 16/eu 46</option>
                           <option value="80">xxl/uk 20/us 18/eu 48</option>
                           <option value="81">xxxl/uk 22/us 20/eu 50</option>
                        </select>
                     </div>
                  </div>
                  <label id="sizetop-error" class="message error hide" for="sizetop">Please select top size
                  </label>
               </div>
                <div id="question_1_4" class="product-item section-dress-size">
                 <!--  <div class="pa-que">
                     <span>Dress</span>
                    <div class="title-section-changed text-center"  >
                            <h1 class="title-changed">
                               (Pick one)
                            </h1>
                    </div>
                  </div> -->
                  <div class="item-sizes-wrp" id="">
                     <div class="form-group">
                        <select class="form-control hwidth dress_size" name="dress_size" type="dress_size" question_id="18">
                           <option value="373">Select dress size</option>
                           <option value="82">xs/uk 4/us 2/eu 32</option>
                           <option value="83">s/uk 6/us 4/eu 34</option>
                           <option value="84">s/uk 8/us 6/eu 36</option>
                           <option value="85">m/uk 10 /us 8/eu 38</option>
                           <option value="86">m/uk 12/us 10/eu 40</option>
                           <option value="87">l/uk 14/us 12/eu 42</option>
                           <option value="88">l/uk 16/us 14/eu 44</option>
                           <option value="89">xl/uk 18/us 16/eu 46</option>
                           <option value="90">xxl/uk 20/us 18/eu 48</option>
                           <option value="91">xxxl/uk 22/us 20/eu 50</option>
                        </select>
                     </div>
                  </div>
                  <label id="dress_size-error" class="message error hide" for="dress_size">Please select dress size
                  </label>
               </div>
               <div id="question_1_5" class="product-item section-trouser-size">
                  <!-- <div class="pa-que">
                     <span>
                        Trouser (Waist in inches)
                       
                     </span>
                     <div class="title-section-changed text-center"  >
                            <h1 class="title-changed">
                               (Pick one)
                            </h1>
                         </div>
                  </div> -->
                  <div class="item-sizes-wrp">
                     <div class="form-group">
                        <select class="form-control hwidth trouser-size" type="trouser-size" name="scbox_sizetrouser" question_id="19">
                           <option value="374">Select trouser size</option>
                           <option value="92">xs/ 25</option>
                           <option value="93">s/26</option>
                           <option value="94">m/28</option>
                           <option value="95">l/30</option>
                           <option value="96">xl/32</option>
                           <option value="97">xxl/34</option>
                           <option value="98">xxxl/36</option>
                        </select>
                     </div>
                  </div>
                  <label id="sizetrouser-error" class="message error hide" for="sizetrouser">Please select trouser size</label>
               </div>
               <div id="question_1_6" class="product-item section-jeans-size" question_id="20">
                  <!-- <div class="pa-que">
                     <span>
                        Jeans (Waist in inches)
                       
                     </span>
                     <div class="title-section-changed text-center"  >
                            <h1 class="title-changed">
                               (Pick one)
                            </h1>
                         </div>
                  </div> -->
                  <div class="item-sizes-wrp" id="">
                     <div class="form-group">
                        <select class="form-control hwidth jeans-size" type="jeans-size" name="scbox_denim_size">
                           <option value="375">select jeans size</option>
                           <option value="99">xs/ 25</option>
                           <option value="100">s/26</option>
                           <option value="101">m/28</option>
                           <option value="102">l/30</option>
                           <option value="103">xl/32</option>
                           <option value="104">xxl/34</option>
                           <option value="105">xxxl/36</option>
                        </select>
                     </div>
                  </div>
                  <label id="denim_size-error" class="message error hide" for="denim_size">Please select jeans size</label>
               </div>
               <div id="question_1_7" class="product-item section-lingerie">
                  <!-- <div class="pa-que">
                     <span>
                        Select Bra: Band Size
                       
                     </span>
                     <div class="title-section-changed text-center"  >
                            <h1 class="title-changed">
                               (Pick one)
                            </h1>
                         </div>
                  </div> -->
                  <div class="item-sizes-wrp" id="">
                     <div class="form-group">
                        <select class="form-control hwidth band-size" type="band-size" question_id="21">
                           <option value="376">Select Bra: band size</option>
                           <option value="106">30</option>
                           <option value="107">32</option>
                           <option value="108">34</option>
                           <option value="109">36</option>
                           <option value="110">38</option>
                           <option value="111">40</option>
                        </select>
                     </div>
                     <label id="sizeband-error" class="message error hide" for="sizeband">Please select band size</label>
                  </div>
                 </div>
                 <div id="question_1_8" class="product-item section-lingerie"> 
                  <!-- <div class="pa-que">
                     <span> Select Bra: Cup Size</span>
                     <div class="title-section-changed text-center"  >
                            <h1 class="title-changed">
                               (Pick one)
                            </h1>
                         </div>
                  </div> -->
                  <div class="item-sizes-wrp" id="">
                     <div class="form-group">
                        <select class="form-control hwidth cup-size" type="cup-size" question_id="22">
                           <option value="377">Select Bra: cup size</option>
                           <option value="112">a</option>
                           <option value="113">b</option>
                           <option value="114">c</option>
                           <option value="115">d</option>
                           <option value="116">dd</option>
                           <option value="117">f</option>
                        </select>
                     </div>
                     <label id="sizecup-error" class="message error hide" for="sizecup">Please select cup size</label>
                  </div>
               </div>
               <div id="question_1_9" class="container extrapad section-top-fit-like-this" question_id="23">
                  <div class="pa-que">
                     <span>
                        My Top Should Fit Like This
                        <!--  <i style="font-size:90%;">(Select 1 or more)</i> -->
                     </span>
                     <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                  </div>
                   <label id="topfit_ans-error" class="message error hide hidden-lg hidden-md" for="topfit_ans">Please select fit of top</label>
                  <ul class="add-event list-colors fit-you-like actdeact" type="fit-you-like">
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/topfit/1fitted.jpg" alt="FITTED">
                        </div>
                        <div class="title-3" answer_id="118">fitted</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/topfit/1relaxed.jpg" alt="RELAXED">
                        </div>
                        <div class="title-3" answer_id="119">relaxed</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/topfit/1flowy.jpg" alt="FLOWY">
                        </div>
                        <div class="title-3" answer_id="120">flowy</div>
                     </li>
                  </ul>
                  <label id="topfit_ans-error" class="message error hide hidden-xs hidden-sm" for="topfit_ans">Please select fit of top</label>
               </div>
               <div id="question_1_10" class="container extrapad section-dress-fit-like-this" question_id="24">
                  <div class="pa-que">
                     <span class="capcase">
                        My Dress Should Fit Like This
                        <!--  <i style="font-size:90%;">(Select 1 or more)</i> -->
                     </span>
                     <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                  </div>
                   <label id="dressfit_ans-error" class="message error hide hidden-md hidden-lg" for="dressfit_ans">Please select dress fit</label>
                  <ul class="add-event list-colors fit-like-dress actdeact" type="fit-like-dress">
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/dressfit/1bodycon.jpg" alt="BODYCON">
                        </div>
                        <div class="title-3" answer_id="121">bodycon</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/dressfit/1shift.jpg" alt="SHIFT">
                        </div>
                        <div class="title-3" answer_id="122">shift</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/dressfit/1aline.jpg" alt="A LINE">
                        </div>
                        <div class="title-3" answer_id="123">a line</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/dressfit/1skater.jpg" alt="SKATER">
                        </div>
                        <div class="title-3" answer_id="124">skater</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/dressfit/1flowy.jpg" alt="FLOWY">
                        </div>
                        <div class="title-3" answer_id="125">flowy</div>
                     </li>
                  </ul>
                  <label id="dressfit_ans-error" class="message error hide hidden-xs hidden-sm" for="dressfit_ans">Please select dress fit</label>
               </div>
               <div id="question_1_11" class="container extrapad section-bottom-fit-like-this" question_id="25">
                  <div class="pa-que">
                     <span>
                        My Bottoms Should Fit Like This
                        <!--  <i style="font-size:90%;">(Select 1 or more)</i> -->
                     </span>
                     <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                  </div>
                   <label id="bottomfit_ans-error" class="message error hide hidden-lg hidden-md" for="bottomfit_ans">Please select bottom fit</label>
                  <ul class="add-event list-colors bottom-fit-like actdeact" type="bottom-fit-like">
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/bottomfit/1skinny.jpg" alt="SKINNY">
                        </div>
                        <div class="title-3" answer_id="126">skinny</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/bottomfit/1slim.jpg" alt="SLIM">
                        </div>
                        <div class="title-3" answer_id="127">slim</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/bottomfit/1straight.jpg" alt="STRAIGHT">
                        </div>
                        <div class="title-3" answer_id="128">straight</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/bottomfit/1flared.jpg" alt="FLARED">
                        </div>
                        <div class="title-3" answer_id="129">flared</div>
                     </li>
                  </ul>
                  <label id="bottomfit_ans-error" class="message error hide hidden-sm hidden-xs" for="bottomfit_ans">Please select bottom fit</label>
               </div>
               <div id="question_1_12" class="container extrapad section-skirt-fit-like-this" question_id="26">
                  <div class="pa-que">
                     <span>
                        I Like The Length Of My Skirts And Dresses To Be  Like This
                        <!--  <i style="font-size:90%;">(Select 1 or more)</i> -->
                     </span>
                     <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                  </div>
                   <label id="dresslength_ans-error" class="message error hide hidden-lg hidden-md" for="dresslength_ans">Please select dress length</label>
                  <ul class="add-event list-colors skirt-dress-size actdeact" type="skirt-dress-size">
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/skirt/1mini.jpg">
                        </div>
                        <div class="title-3" answer_id="130">mini</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/skirt/1above.jpg">
                        </div>
                        <div class="title-3" answer_id="131">above the knee</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/skirt/1below.jpg">
                        </div>
                        <div class="title-3" answer_id="132">below the knee</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/skirt/1midi.jpg">
                        </div>
                        <div class="title-3" answer_id="133">midi</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/skirt/1maxi.jpg">
                        </div>
                        <div class="title-3" answer_id="134">maxi</div>
                     </li>
                  </ul>
                  <label id="dresslength_ans-error" class="message error hide hidden-xs hidden-sm" for="dresslength_ans">Please select dress length</label>
               </div>
               <div id="question_1_13" class="container extrapad section-dos-and-donts" question_id="27">
                  <div class="pa-que">
                     <span>My Do's And <i style="color: #ed1b2e;">Don'ts</i>
                     </span>
                     <div class="title-section-changed text-center"  >
                            <h1 class="title-changed">
                               (Pick Yes Or No For Each Style)
                            </h1>
                         </div>
                  </div>
                   <label id="dos-donts-error" class="message error hide hidden-lg hidden-md" for="dos-donts">Please select choice</label>
                  <ul class="list-colors other-type bigimg">
                     <li answer_id="135">
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/dos/1sleeveless.jpg" alt="SLEEVELESS">
                        </div>
                        <div class="title-3">sleeveless</div>
                        <ul class="add-event list-occassion lespad dos-and-donts other-type" type="dos-and-donts" style="sleeveless">
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>yes</span></label>
                           </li>
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>no</span></label>
                           </li>
                        </ul>
                     </li>
                     <li answer_id="136">
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/dos/1halter.jpg" alt="HALTER">
                        </div>
                        <div class="title-3">halter</div>
                        <ul class="add-event list-occassion lespad dos-and-donts other-type" type="dos-and-donts" style="halter">
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>yes</span></label>
                           </li>
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>no</span></label>
                           </li>
                        </ul>
                     </li>
                     <li answer_id="137">
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/dos/1strappy.jpg" alt="STRAPPY">
                        </div>
                        <div class="title-3">strappy</div>
                        <ul class="add-event list-occassion lespad dos-and-donts other-type" type="dos-and-donts" style="strappy">
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>yes</span></label>
                           </li>
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>no</span></label>
                           </li>
                        </ul>
                     </li>
                     <li answer_id="138">
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/dos/1offshoulder.jpg" alt="OFF SHOULDER">
                        </div>
                        <div class="title-3">off shoulder</div>
                        <ul class="add-event list-occassion lespad dos-and-donts other-type" type="dos-and-donts" style="off shoulder">
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>yes</span></label>
                           </li>
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>no</span></label>
                           </li>
                        </ul>
                     </li>
                     <li answer_id="354">
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/dos/1tube.jpg" alt="TUBE">
                        </div>
                        <div class="title-3">tube</div>
                        <ul class="add-event list-occassion lespad dos-and-donts other-type" type="dos-and-donts" style="tube">
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>yes</span></label>
                           </li>
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>no</span></label>
                           </li>
                        </ul>
                     </li>
                     <li answer_id="139">
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/dos/1crop.jpg" alt="CROP TOP">
                        </div>
                        <div class="title-3">crop top</div>
                        <ul class="add-event list-occassion lespad dos-and-donts other-type" type="dos-and-donts" style="crop top">
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>yes</span></label>
                           </li>
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>no</span></label>
                           </li>
                        </ul>
                     </li>
                     <li answer_id="140">
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/dos/1backless.jpg" alt="BACKLESS">
                        </div>
                        <div class="title-3">backless</div>
                        <ul class="add-event list-occassion lespad dos-and-donts other-type" type="dos-and-donts" style="backless">
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>yes</span></label>
                           </li>
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>no</span></label>
                           </li>
                        </ul>
                     </li>
                     <li answer_id="141">
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/dos/1deepneck.jpg" alt="DEEP NECK">
                        </div>
                        <div class="title-3">deep neck</div>
                        <ul class="add-event list-occassion lespad dos-and-donts other-type" type="dos-and-donts" style="deep neck">
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>yes</span></label>
                           </li>
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span>no</span></label>
                           </li>
                        </ul>
                     </li>
                  </ul>
                  <label id="dos-donts-error" class="message error hide hidden-sm hidden-xs" for="dos-donts">Please select choice</label>
               </div>
               <div id="question_1_14" class="container extrapad section-style-preference" question_id="28">
                  <div class="pa-que">
                     <span>My Style Preference</span>
                     <div class="title-section-changed text-center"  >
                            <h1 class="title-changed">
                               (Pick Love, Like or Dislike For Each Style)
                            </h1>
                         </div>
                  </div>
                   <label id="style_ans-error" class="message error hide hidden-lg hidden-md" for="style">Please select style preference</label>
                  <ul class="add-event list-colors other-type bigimg">
                     <li answer_id=142"">
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/preference/classic1.jpg" alt="CLASSICS">
                        </div>
                        <div class="title-3">classics</div> 
                        <ul class="add-event list-occassion lespad style-preference other-type" type="style-preference" style="classics">
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox" action="love"><i class="icon icon-heart"></i>
                              <input type="checkbox">
                              </label>
                           </li>
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox" action="like"><i class="icon icon-like"></i>
                              <input type="checkbox">
                              </label>
                           </li>
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox" action="dislike"><i class="icon icon-dislike"></i>
                              <input type="checkbox">
                              </label>
                           </li>
                        </ul>
                     </li>
                     <li answer_id="143">
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/preference/romantic1.jpg" alt="ROMANTIC- FEMININE">
                        </div>
                        <div class="title-3">romantic- feminine</div> 
                        <ul class="add-event list-occassion lespad style-preference other-type" type="style-preference" style="romantic-feminine">
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox" action="love"><i class="icon icon-heart"></i>
                              <input type="checkbox">
                              </label>
                           </li>
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox" action="like"><i class="icon icon-like"></i>
                              <input type="checkbox">
                              </label>
                           </li>
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox" action="dislike"><i class="icon icon-dislike"></i>
                              <input type="checkbox">
                              </label>
                           </li>
                        </ul>
                     </li>
                     <li answer_id="144">
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/preference/bohemian1.jpg" alt="FREE SPIRIT">
                        </div>
                       <div class="title-3">free spirit</div> 
                        <ul class="add-event list-occassion lespad style-preference other-type" type="style-preference" style="free sprit">
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox" action="love"><i class="icon icon-heart"></i>
                              <input type="checkbox">
                              </label>
                           </li>
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox" action="like"><i class="icon icon-like"></i>
                              <input type="checkbox">
                              </label>
                           </li>
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox" action="dislike"><i class="icon icon-dislike"></i>
                              <input type="checkbox">
                              </label>
                           </li>
                        </ul>
                     </li>
                     <li answer_id="145">
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/preference/bombshell1.jpg" alt="BOMBSHELL">
                        </div>
                       <div class="title-3">bombshell</div> 
                        <ul class="add-event list-occassion lespad style-preference other-type" type="style-preference" style="bombshell">
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox" action="love"><i class="icon icon-heart"></i>
                              <input type="checkbox">
                              </label>
                           </li>
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox" action="like"><i class="icon icon-like"></i>
                              <input type="checkbox">
                              </label>
                           </li>
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox" action="dislike"><i class="icon icon-dislike"></i>
                              <input type="checkbox">
                              </label>
                           </li>
                        </ul>
                     </li>
                     <li answer_id="146">
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/preference/bold1.jpg" alt="BOLD AND EDGY">
                        </div>
                        <div class="title-3">bold and edgy</div>
                        <ul class="add-event list-occassion lespad style-preference other-type" type="style-preference" style="bold and edgy">
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox" action="love"><i class="icon icon-heart"></i>
                              <input type="checkbox">
                              </label>
                           </li>
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox" action="like"><i class="icon icon-like"></i>
                              <input type="checkbox">
                              </label>
                           </li>
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox" action="dislike"><i class="icon icon-dislike"></i>
                              <input type="checkbox">
                              </label>
                           </li>
                        </ul>
                     </li>
                     <li answer_id="147">
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/preference/atleisure1.jpg" alt="ATHLEISURE">
                        </div>
                        <div class="title-3">athleisure</div> 
                        <ul class="add-event list-occassion lespad style-preference other-type" type="style-preference" style="athleisure">
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox" action="love"><i class="icon icon-heart"></i>
                              <input type="checkbox">
                              </label>
                           </li>
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox" action="like"><i class="icon icon-like"></i>
                              <input type="checkbox">
                              </label>
                           </li>
                           <li class="noborder" style="border:none !important;">
                              <label class="sc-checkbox" action="dislike"><i class="icon icon-dislike"></i>
                              <input type="checkbox">
                              </label>
                           </li>
                        </ul>
                     </li>
                  </ul>
                  <label id="style_ans-error" class="message error hide hidden-xs hidden-sm" for="style">Please select style preference</label>
               </div>
               <div id="question_1_15" class="container section-color-avoid" question_id="29">
                  <div class="pa-que">
                     <span>Colors I <i style="color: #ed1b2e;"> Avoid</i></span>
                         <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                  </div>
                  <label id="coloravoid-error" class="message error hide hidden-lg hidden-md" for="coloravoid">Please select a color</label>
                  <ul class="add-event list-skin-tone list-colordneed actdeact color-to-avoid-afterpayment color-avoid" type="color-avoid">
                     <li>
                        <div class="pallet" color="#90140c"  answer_id="148" style="background-color:#90140c;border:1px solid #90140c;"></div>
                     </li>
                      <li >
                        <div class="pallet" answer_id="149" color="#b8bebe" style="background-color:#b8bebe;border:1px solid #b8bebe;"></div>
                     </li>
                      <li >
                        <div class="pallet" answer_id="355" color="#be914a" style="background-color:#be914a;border:1px solid #be914a;"></div>
                     </li>
                     <li>
                        <div class="pallet"  answer_id="356" color="#ff0080" style="background-color:#FF0080;border:1px solid #FF0080;"></div>
                     </li>
                     <li>
                        <div class="pallet" answer_id="357" color="#734021" style="background-color:  #734021;border:1px solid #734021;"></div>
                     </li>

                     <li>
                        <div class="pallet"  answer_id="358" color="#fec4c3" style="background-color:#fec4c3;border:1px solid #fec4c3;"></div>
                     </li>
                     <li >
                        <div class="pallet" answer_id="359" color="#0b0706" style="background-color:#0b0706;border:1px solid #0b0706;"></div>
                     </li>
                     <li>
                        <div class="pallet" answer_id="360" color="#98012e" style="background-color:  #98012e;border:1px solid #98012e;"></div>
                     </li>
                     <li>
                        <div class="pallet" answer_id="361" color="#ffffff" style="background-color:#FFFFFF;border:1px solid #FFFFFF;"></div>
                     </li>
                     <li>
                        <div class="pallet" answer_id="362" color="#f57d31" style="background-color:#f57d31;border:1px solid #f57d31;"></div>
                     </li>
                     <li>
                        <div class="pallet"  answer_id="363" color="#87ceeb" style="background-color:#87ceeb;border:1px solid #87ceeb;"></div>
                     </li>
                     <li>
                        <div class="pallet" answer_id="364" color="#cdd1d1" style="background-color:#cdd1d1;border:1px solid #cdd1d1;"></div>
                     </li>
                     <li>
                        <div class="pallet" answer_id="365" color="#d5011a" style="background-color:#d5011a;border:1px solid #d5011a;"></div>
                     </li>
                      <li>
                        <div class="pallet" answer_id="366" color="#800080" style="background-color:#800080;border:1px solid #800080;"></div>
                     </li>
                     <li>
                        <div class="pallet" answer_id="367" color="#bcd2ee" style="background-color:#bcd2ee;border:1px solid #bcd2ee;"></div>
                     </li>
                    
                     <li>
                        <div class="pallet" answer_id="368" color="#576331" style="background-color:#576331;border:1px solid #576331;"></div>
                     </li>
                     <li>
                        <div class="pallet" answer_id="369" color="#041531" style="background-color:#041531;border:1px solid #041531;"></div>
                     </li>
                     <li>
                        <div class="pallet"  answer_id="370" color="#f7b719" style="background-color:#f7b719;border:1px solid #f7b719;"></div>
                     </li><br>
                      <li class="sc-checkbox" style="width:220px !important;" answer_id="365">
                         <div class="title-3" color="I am a rainbow I like all colors">
                            <i class="icon icon-diamond"></i>I'm a rainbow, I like all colors
                               <input type="checkbox">
                         </div>
                      </li> 
                  </ul>
                   
                  <label id="coloravoid-error" class="message error hide hidden-sm hidden-xs" for="coloravoid">Please select a color</label>
               </div>
               <div id="question_1_16" class="container extrapad section-print-avoid" question_id="30">
                  <div class="pa-que">
                     <span>
                        Prints and Patterns I <i style="color: #ed1b2e;">Avoid</i>
                        <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                        <!-- <i style="font-size:90%;">(Select 1 or more)</i> -->
                     </span>
                  </div>
                  <label id="printavoid-error" class="message error hide hidden-lg hidden-md" for="printavoid">Please select print to avoid</label>
                  <ul class="add-event list-colors list-prints actdeact other-type print-avoid" type="print-avoid">
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/prints/polka.jpg" alt="Polka dots">
                        </div>
                        <div class="title-3"  answer_id="150">polka dots</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/prints/floral.jpg" alt="Floral">
                        </div>
                        <div class="title-3" answer_id="151">floral</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/prints/stripes.jpg" alt="Stripes">
                        </div>
                        <div class="title-3" answer_id="152">stripes</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/prints/animal.jpg" alt="Animal">
                        </div>
                        <div class="title-3" answer_id="153">animal</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/prints/check.jpg" alt="Checks">
                        </div>
                        <div class="title-3" answer_id="154">checks</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/female/style/prints/solid.jpg" alt="Solid">
                        </div>
                        <div class="title-3" answer_id="155">solid</div>
                     </li>
                  </ul>
                  <label id="printavoid-error" class="message error hide hidden-sm hidden-xs" for="printavoid">Please select print to avoid</label>
               </div>
            </div>
            <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6 col-sm-12 col-xs-12" question_id="31">
                           <div class="title-2">
                              <label> Comment Box</label>
                           </div>
                           <div class="form-group-md">
                              <textarea type="text" placeholder="" id="women_commentbox_1" answer_id="378"></textarea>
                              <label>IS THERE ANYTHING WE MISSED ? </label>
                           </div>
                        </div>
                        <div class="col-md-3"></div>
                     </div>
         </div>
        </section>
         
         <!--Stuffs i dont need-->
         <section class="section section-shadow">
            <div class="btns-wrp">
               <a class="scbtn scbtn-primary" href="javascript:void(0);" onclick="ChangeStep(1,'previous');"><span>Previous</span></a>
               <a class="scbtn scbtn-primary btn-next-1 link-scrolling" href="javascript:void(0);" onclick="ChangeStep(3,'next');"><span>Next</span></a>
               <!-- <button class="scbtn scbtn-primary" onclick="slideprev(4);"><span>Previous</span></button>
                  <button class="scbtn scbtn-primary"  onclick="slidenext(4);"><span>Next</span></button> -->
            </div>
         </section>
      </div>
      <div id="slide_3" class="hide"><!-- class='hide' -->
         <section class="section section-shadow ">
            <!-- class="<?php echo ( @$_GET['step']==3) ? '' : 'hide'; ?>"> -->
            <!-- BAGS-->
            <div class="women_bags category-bags hide"><!-- hide -->
               <div class="women_bags_beforepayment">
                  <div id="question_2_1" class="container section-bag-type" question_id="32">
                    
                        <div class="pa-que">
                           <span>
                              The Kind Of Bag I Am Looking For
                              <!-- <i style="font-size:90%;">(Select 1 or more)</i> -->
                           </span>
                        </div>
                        <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                         <label id="bagtype-error" class="message error hide hidden-lg hidden-md" for="bagtype">Please select a bag</label> 
                     <ul class="add-event list-colors bag-type" type="bag-type">
                        <li>
                           <div class="pallet">
                              <img src="<?php echo base_url(); ?>assets/images/pa/female/style/bags/sling.jpg" alt="SLING">
                           </div>
                           <div class="title-3"  answer_id="156">sling</div>
                        </li>
                        <li >
                           <div class="pallet">
                              <img src="<?php echo base_url(); ?>assets/images/pa/female/style/bags/clutch.jpg" alt="CLUTCH">
                           </div>
                           <div class="title-3" answer_id="157">clutch</div>
                        </li>
                        <li>
                           <div class="pallet">
                              <img src="<?php echo base_url(); ?>assets/images/pa/female/style/bags/handbag.jpg" alt="HANDBAG">
                           </div>
                           <div class="title-3" answer_id="158">handbag</div>
                        </li>
                        <li>
                           <div class="pallet">
                              <img src="<?php echo base_url(); ?>assets/images/pa/female/style/bags/tote.jpg" alt="TOTE">
                           </div>
                           <div class="title-3" answer_id="159">tote</div>
                        </li>
                        <li>
                           <div class="pallet">
                              <img src="<?php echo base_url(); ?>assets/images/pa/female/style/bags/backpack.jpg" alt="BACKPACK">
                           </div>
                           <div class="title-3" answer_id="160">backpack</div>
                        </li>
                     </ul>
                     <label id="bagtype-error" class="message error hide hidden-xs hidden-sm" for="bagtype">Please select a bag</label>
                  </div>
               </div>
            </div>
            <!-- /BAGS-->
            <!-- FOOTWEAR -->
            <div class="women_footwear category-footwear hide"><!-- hide -->
               <div class="women_footwear_beforepayment">
                  <div id="question_2_2" class="product-item section-footwear-size" question_id="33">
                        <div class="pa-que">
                           <span>Footwear Size
                           </span>
                           <div class="title-section-changed text-center"  >
                                <h1 class="title-changed">
                                   (Pick one)
                                </h1>
                             </div>
                        </div>
                     <div class="item-sizes-wrp" id="foot-size_w">
                        <div class="form-group form-element">
                           <select class="form-control hwidth footwear-size" type="footwear-size">
                              <option value="">Select footwear size</option>
                              <option value="161">UK 3/US 5/EU 36</option>
                              <option value="161">UK 4/US 6/EU 37</option>
                              <option value="163">UK 5/US 7/EU 38</option>
                              <option value="164">UK 6/US 8/EU 39</option>
                              <option value="165">UK 7/US 9/EU 41</option>
                              <option value="166">UK 8/US 10/EU 42</option>
                              <option value="167">UK 9/US 11/EU 43</option>
                           </select>
                        </div>
                     </div>
                     <label id="scbox_foot-size_w-error" class="message error hide" for="scbox_foot-size_w">Please select footwear size</label>
                  </div>
                  <div id="question_2_3" class="product-item section-footwear-width" question_id="34">
                    
                        <div class="pa-que">
                           <span>Foot Width</span>
                            <div class="title-section-changed text-center"  >
                                <h1 class="title-changed">
                                   (Pick one)
                                </h1>
                             </div>
                        </div>
                    
                     <ul class="add-event list-occassion section-footwear-width" type="section-footwear-width">
                        <li>
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span answer_id="168">i have broad feet</span></label>
                        </li>
                        <li style="width: 180px;">
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span  answer_id="169">i have narrow feet</span></label>
                        </li>
                     </ul>
                     <label id="foottype-error" class="message error hide" for="foottype">Please select foot type</label>
                  </div>
                  <div id="question_2_4" class="container section-footwear-type-looking-for" question_id="35">
                    
                        <div class="pa-que">
                           <span>
                              The Type Of Shoes I Am Looking For <!-- <i style="font-size:90%;">(Select 1 or more)</i>  -->
                           </span>
                            <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                        </div>
                      <label id="women_footwear-error" class="message error hide hidden-lg hidden-md" for="women_footwear">Please select a footwear</label>
                     <ul class="add-event list-colors footwear-type" type="footwear-type">
                        <li>
                           <div class="pallet">
                              <img src="<?php echo base_url(); ?>assets/images/pa/female/style/footwear/1flats.jpg">
                           </div>
                           <div class="title-3"  answer_id="170">flats</div>
                        </li>
                        <li>
                           <div class="pallet">
                              <img src="<?php echo base_url(); ?>assets/images/pa/female/style/footwear/1sneakers.jpg">
                           </div>
                           <div class="title-3" answer_id="171">sneakers</div>
                        </li>
                        <li>
                           <div class="pallet">
                              <img src="<?php echo base_url(); ?>assets/images/pa/female/style/footwear/1heels.jpg">
                           </div>
                           <div class="title-3" answer_id="172">heels</div>
                        </li>
                     </ul>
                     <label id="women_footwear-error" class="message error hide hidden-sm hidden-xs" for="women_footwear">Please select a footwear</label>
                  </div>
               </div>
            </div>
            <!-- ACCESSORIES -->
            <div class="women_accessories category-accessories hide"><!-- hide -->
               <div class="women_accessories_beforepayment">
                  <div id="question_2_5" class="product-item extrapad section-accessories-types" question_id="36">
                    
                        <div class="pa-que">
                           <span>Accessories</span>
                           <div class="title-section-changed text-center"  >
                                <h1 class="title-changed">
                                   (Pick one)
                                </h1>
                             </div>
                        </div>
                    
                     <ul class="list-occassion accessories-selected leftpad" type="accessories-selected">
                        <li>
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span answer_id="173">sunglasses</span></label>
                        </li>
                        <li>
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span answer_id="174">belts</span></label>
                        </li>
                        <li>
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span answer_id="175">stationery</span></label>
                        </li>
                        <li>
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span answer_id="176">caps</span></label>
                        </li>
                        <li>
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span answer_id="177">scarves</span></label>
                        </li>
                     </ul>
                     <label id="women_accessory-error" class="message error hide" for="women_accessory">Please select an accessory</label>
                  </div>
               </div>
            </div>
            <!-- JEWELLERY -->
            <div class="women_accessories category-jewellery hide"><!-- hide -->
               <div class="women_jewellery_beforepayment">
                  <div class="women_jewellery category-jewellery"><!-- hide -->
                     <div class="women_jewellery_beforepayment">
                        <div id="question_2_6" class="container extrapad section-jewellery-type-looking-for" question_id="37">
                          
                              <div class="pa-que">
                                 <span>
                                    The Kind Of Jewellery I Am Looking For
                                    <!-- <i style="font-size:90%;">(Select 1 or more)</i> -->
                                 </span>
                                <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                              </div>
                          <label id="women_jewellery-error" class="message error hide hidden-md hidden-lg" for="women_jewellery">Please select a jewellery product</label>
                           <ul class="add-event list-colors jewellery-type" type="jewellery-type">
                              <li>
                                 <div class="pallet">
                                    <img src="<?php echo base_url(); ?>assets/images/pa/female/style/jewellery/statement.jpg">
                                 </div>
                                 <div class="title-3" answer_id="178">statement</div>
                              </li>
                              <li>
                                 <div class="pallet">
                                    <img src="<?php echo base_url(); ?>assets/images/pa/female/style/jewellery/minimal.jpg">
                                 </div>
                                 <div class="title-3" answer_id="179">minimal</div>
                              </li>
                              <li>
                                 <div class="pallet">
                                    <img src="<?php echo base_url(); ?>assets/images/pa/female/style/jewellery/chunky.jpg">
                                 </div>
                                 <div class="title-3" answer_id="180">bold/chunky</div>
                              </li>
                           </ul>
                           <label id="women_jewellery-error" class="message error hide hidden-xs hidden-sm" for="women_jewellery">Please select a jewellery product</label>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- BEAUTY -->
            <div class="women_beauty category-beauty hide"><!-- hide -->
               <div class="women_beauty_beforepayment">
                  <div id="question_2_7" class="product-item extrapad section-beauty-type-looking-for" question_id="38">
                    
                        <div class="pa-que">
                           <span>The Beauty Product I'm Looking For</span>
                           <div class="title-section-changed text-center"  >
                                <h1 class="title-changed">
                                   (Pick one)
                                </h1>
                             </div>
                        </div>
                    
                     <ul class="list-occassion section-beauty-type-looking-for leftpad" type="section-beauty-type-looking-for">
                        <li data-attr="Casuals">
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span answer_id="181">eye shadow</span></label>
                        </li>
                        <li>
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span answer_id="182">liner</span></label>
                        </li>
                        <li>
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span answer_id="183">lipstick</span></label>
                        </li>
                        <li>
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span answer_id="184">nail paint</span></label>
                        </li>
                        <li>
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span answer_id="185">mascara</span></label>
                        </li>
                        <li>
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span answer_id="186">hand creme</span></label>
                        </li>
                        <li>
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span answer_id="187">dry shampoo</span></label>
                        </li>
                     </ul>
                     <label id="women_beauty-error" class="message error hide" for="women_beauty">Please select a beauty product</label>
                  </div>
                  <div id="question_2_8" class="container container-beauty-shades" question_id="39"><!-- hide -->
                     <!-- eye shadow -->
                    
                        <div class="pa-que">
                           <span>
                              Shade
                              <!--  <i style="font-size:90%;">(Select 1 or more)</i> -->
                           </span>
                           <div class="title-section-changed text-center"  >
                                <h1 class="title-changed">
                                   (Pick one)
                                </h1>
                             </div>
                        </div>
                   	<ul class="list-skin-tone actdeact section-beauty-shade desktop-shade hidden-sm hidden-xs" type="section-beauty-shade">
                        <li style="display: inline;">
                           <label class="sc-checkbox" style="display: inline-flex;">
                              <i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span answer_id="188"  style="
                                 padding-right: 17px !important;
                                 padding-top: 10px !important;text-transform: capitalize;
                                 ">red</span>
                              <div class="pallet" color="#ff1a1a" style="background-color:#ff1a1a;border:1px solid #ff1a1a;"></div>
                              <div class="pallet" color="#ff0000" style="background-color:#ff0000;border:1px solid #ff0000;"></div>
                              <div class="pallet" color="#e60000" style="background-color:#e60000;border:1px solid #e60000;"></div>
                              <div class="pallet" color="#cc0000" style="background-color:#cc0000;border:1px solid #cc0000;"></div>
                           </label>
                        </li>
                        <br>
                        <li style="display: inline;">
                           <label class="sc-checkbox" style="display: inline-flex;">
                              <i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span answer_id="189" style="
                                 padding-right: 10px !important;
                                 padding-top: 10px !important;text-transform: capitalize;
                                 ">nude</span>
                             
                              <div class="pallet" color="#ffc0cb" style="background-color:#ffc0cb;border:1px solid #ffc0cb;"></div>
                              <div class="pallet" color="#cc99a2" style="background-color:#cc99a2;border:1px solid #cc99a2;"></div>
                               <div class="pallet" color="#d29494" style="background-color:#d29494;border:1px solid #d29494;"></div>
                              <div class="pallet" color="#ae3f3f" style="background-color:#ae3f3f;border:1px solid #ae3f3f;"></div>
                           </label>
                        </li>
                        <br>
                        <li style="display: inline;">
                           <label class="sc-checkbox" style="display: inline-flex;">
                              <i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span  answer_id="190" style="
                                 padding-right: 15px !important;
                                 padding-top: 10px !important;text-transform: capitalize;
                                 ">pink</span>
                              <div class="pallet" color="#ff77aa" style="background-color:#ff77aa;border:1px solid #ff77aa;"></div>
                              <div class="pallet" color="#ec3a8b" style="background-color:#ec3a8b;border:1px solid #ec3a8b;"></div>
                              <div class="pallet" color="#ff3377" style="background-color:#ff3377;border:1px solid #ff3377;"></div>
                              <div class="pallet" color="#ff0065" style="background-color:#ff0065;border:1px solid #ff0065;"></div>
                           </label>
                        </li>
                        <br>
                        <li style="display: inline;">
                           <label class="sc-checkbox" style="display: inline-flex;">
                              <i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span answer_id="191" style="
                                 padding-right: 10px !important;
                                 padding-top: 10px !important;text-transform: capitalize;
                                 ">deep</span>
                              <div class="pallet" color="#660000" style="background-color:#660000;border:1px solid #660000;"></div>
                              <div class="pallet" color="#330033" style="background-color:#330033;border:1px solid #330033;"></div>
                              <div class="pallet" color="#7b3f00" style="background-color:#7b3f00;border:1px solid #7b3f00;"></div>
                              <div class="pallet" color="#b30000" style="background-color:#b30000;border:1px solid #b30000;"></div>
                           </label>
                        </li>
                     </ul>



                     <ul class="list-skin-tone actdeact section-beauty-shade mobile-shade hidden-md hidden-lg" type="section-beauty-shade">
                        <li style="display: inline;">
                           <label class="sc-checkbox" style="display: inline-flex;">
                              <i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span  answer_id="188" style="
                                 padding-right: 17px !important;
                                 padding-top: 10px !important;text-transform: capitalize;
                                 ">red</span>
                              <div class="pallet" color="#cc2f2a" style="background-color:#cc2f2a;border:1px solid #cc2f2a;"></div>
                              <div class="pallet" color="#e73631" style="background-color:#e73631;border:1px solid #e73631;"></div>
                              <div class="pallet" color="#fb3d38" style="background-color:#fb3d38;border:1px solid #fb3d38;"></div>
                              <div class="pallet" color="#ff4b46" style="background-color:#ff4b46;border:1px solid #ff4b46;"></div>
                              <div class="pallet" color="#ff625e" style="background-color:#ff625e;border:1px solid #ff625e;"></div>
                           </label>
                        </li>
                        <br>
                        <li style="display: inline;">
                           <label class="sc-checkbox" style="display: inline-flex;">
                              <i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span answer_id="189" style="
                                 padding-right: 10px !important;
                                 padding-top: 10px !important;text-transform: capitalize;
                                 ">nude</span>
                              <div class="pallet" color="#f9d8d7" style="background-color:#f9d8d7;border:1px solid #f9d8d7;"></div>
                              <div class="pallet" color="#fae0e0" style="background-color:#ffe0e0;border:1px solid #ffe0e0;"></div>
                              <div class="pallet" color="#fbe7e7" style="background-color:#fbe7e7;border:1px solid #fbe7e7;"></div>
                              <div class="pallet" color="#fcefef" style="background-color:#fcefef;border:1px solid #fcefef;"></div>
                              <div class="pallet" color="#fef7f7" style="background-color:#fef7f7;border:1px solid #fef7f7;"></div>
                           </label>
                        </li>
                        <br>
                        <li style="display: inline;">
                           <label class="sc-checkbox" style="display: inline-flex;">
                              <i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span answer_id="190" style="
                                 padding-right: 15px !important;
                                 padding-top: 10px !important;text-transform: capitalize;
                                 ">pink</span>
                              <div class="pallet" color="#d14ebf" style="background-color:#d14ebf;border:1px solid #d14ebf;"></div>
                              <div class="pallet" color="#d762c6" style="background-color:#d762c6;border:1px solid #d762c6;"></div>
                              <div class="pallet" color="#dc77ce" style="background-color:#dc77ce;border:1px solid #dc77ce;"></div>
                              <div class="pallet" color="#e18bd5" style="background-color:#e18bd5;border:1px solid #e18bd5;"></div>
                              <div class="pallet" color="#e69fdc" style="background-color:#e69fdc;border:1px solid #e69fdc;"></div>
                           </label>
                        </li>
                        <br>
                        <li style="display: inline;">
                           <label class="sc-checkbox" style="display: inline-flex;">
                              <i class="icon icon-diamond"></i>
                              <input type="checkbox"> <span answer_id="191" style="
                                 padding-right: 10px !important;
                                 padding-top: 10px !important;text-transform: capitalize;
                                 ">deep</span>
                              <div class="pallet" color="#660000" style="background-color:#660000;border:1px solid #660000;"></div>
                              <div class="pallet" color="#7f0000" style="background-color:#7f0000;border:1px solid #7f0000;"></div>
                              <div class="pallet" color="#0b0706" style="background-color:#0b0706;border:1px solid #990000;"></div>
                              <div class="pallet" color="#99004c" style="background-color:#99004c;border:1px solid #99004c;"></div>
                              <div class="pallet" color="#b20059" style="background-color:#b20059;border:1px solid #b20059;"></div>
                           </label>
                        </li>
                     </ul>
                     <label id="shade-error" class="message error hide" for="shade">Please select shade</label>
                  </div>
               </div>
            </div>
            <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <div class="title-2">
                              <label> Comment Box</label>
                           </div>
                           <div class="form-group-md">
                              <textarea type="text" placeholder="" id="women_commentbox_2" ></textarea>
                              <label>IS THERE ANYTHING WE MISSED ? </label>
                           </div>
                        </div>
                        <div class="col-md-3"></div>
                     </div>
         </section>
         <!-- BUTTON -->
         <section class="section section-shadow">
            <div class="btns-wrp">
               <a class="scbtn scbtn-primary" href="javascript:void(0);" onclick="ChangeStep(2,'previous');"><span>Previous</span></a>
               <a class="scbtn scbtn-primary btn-next-2 link-scrolling" href="<?php echo base_url();?>book-scbox/cart/" onclick=""><span>Next</span></a>
               <!-- <button class="scbtn scbtn-primary" onclick="slideprev(4);"><span>Previous</span></button>
                  <button class="scbtn scbtn-primary"  onclick="slidenext(4);"><span>Next</span></button> -->
            </div>
         </section>
      </div>
   </form>
</div>
<style>
   .scbox-payment-options li {
   display: inline-block;
   margin-right: 10px;
   }
   .scbox-payment-options .sc-checkbox .icon {
   position: relative;
   top: 10px !important;
   }

</style>

<script>
   document.addEventListener("DOMContentLoaded", function(event) { 
   
       msg("LOADED STEP 2ddddd...... page");
   
       //## Get Box Package function initiated -------------
       var strBoxPackage = getCookie('scbox_objectid');
                if(strBoxPackage == 1){
                   $('.list-categories.pa-women li').eq(2).hide();
                }
                else{
                  msg('show footwear');
                  $('.list-categories.pa-women li').eq(2).show();
                }
                msg($('.list-categories.pa-women li').eq(2));
       GET_BoxPackageID();
   
   });
   
   
</script>