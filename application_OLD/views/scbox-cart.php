
<section class="section section-shadow page-cart">
  <div class="container" id="sccart_product_view">
    <div class="title-section">
      <h1 class="title">Your Cart</h1>
    </div>
   <div class="page-cart-content" id="stylecart_product_view">
    <form name="sccart_frm" id="sccart_frm" method="post">
      <div class="row">
        <div class="col-md-7 col-sm-12 col-xs-12">
          <div class="cart-items-wrp" id="">
            <div class="item">
              <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="item-img-wrp">
                     <img src="https://www.stylecracker.com/assets/images/scbox/women/package1.jpg" data-pin-nopin="true">
                  </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-8">
                  <div class="item-desc-wrp">
                    <div class="title">3 Products</div>
                    <input type="hidden" name="scbox_quantity" id="scbox_quantity" value="1" class="form-control" >
                    <div class="item-price-wrp">
                      <div class="price"><i class="fa fa-inr"></i>2,999.00</div>
                    </div>                 
                  </div>
                </div>
              </div>
            </div>
          </div>
         
            <input type="hidden" name="scitem1" id="scitem1" value="">

             <div class="scbox-payment-options" style="padding-top: 30px;">
          
                <div class="title-2">PAYMENT MODE</div>
                <ul style="text-transform: uppercase;">
                    <li>
                        <label class="sc-checkbox paymode " attr-value="card" id="card" onclick="pay_valiation(this);" style="text-transform: uppercase;"><i class="icon icon-diamond"></i><span> Online Payment</span>
                        </label>
                    </li>
                    <li>
                        <label class="sc-checkbox paymode <?php echo (@$_COOKIE['scfr']=='yb') ? 'state-disabled' : ''; ?>" attr-value="cod" id="cod" onclick="pay_valiation(this);"><i class="icon icon-diamond"></i><span> Cash On Delivery</span>
                        </label>
                    </li>
                </ul>               
            </div>
            <div  style="position:relative; height:30px;">
               <div style="position:absolute;" class="error error-msg hide message error" id="cart_place_order_msg"></div>
             </div>            
     

      </div>
      <div class="col-md-5 col-sm-12 col-xs-12 box-cart-summary-outer">
          <div class="fixed-content111">
            <div class="theiaStickySidebar111">
                <div class="box-cart-summary">
                  <div class="box-body">
                  
                    <input id="cart_pincode" class="form-control cart-pincode" type="hidden" placeholder="Enter Pincode" name="cart_pincode" maxlength="6" value="" />
                   
                    <div class="offers-section">
                      <a class="scbtn scbtn-tertiary" href="#modal-offers" data-toggle="modal" onclick="get_available_offers();"><span>Avail Offers</span></a>
                    </div>


                  <div class="title">Bill Details</div>
                  <ul class="inner">
                  <li>
                  <span class="text-label">Sub Total</span>
                  <span class="value price"> <span class="fa fa-inr"></span></span>
                  </li>
                  
                              <li>
                              <div class="discount-message">                     
                                  <span class="text-label">Coupon Discount </span>
                                  <span class="discount-name">                              
                                  </span>
                                  <span class="value cart-price"> -  <span class="fa fa-inr"></span>3,358.88</span>
                                  <div class="text-danger">This offer is applicable only if the final payment is made through
                                      
                                      &nbsp;<a  title="" data-toggle="popover" data-placement="bottom" data-html="true" data-content="<div class='box-popover'>
                                  <p>The discount will be deducted and reflected at the payment gateway page.</p>
                                 Select the Online payment option > Click on Place Order > Enter card details > Check offer </p>
                                  <p>A green notification will appear stating your debit/credit card is eligible for the discount.</p>
                                  <p>Click on Pay now, discount will reflect and payment will be processed.</p> </div>"><span class="fa fa-info-circle"></span></a>                                    
                                  </div>
                                  <span class="btn-link " id="coupon_clear" onclick="clear_coupon_code();"><i class="icon-close" ></i></span>
                                
                                   <span class="btn-link " id="coupon_clear" onclick="clear_coupon_code();"><i class="icon-close" ></i></span>
                                  </div>
                                </li>
                             
                                <li>
                                 <div class="discount-message"> 
                                  <span class="text-label">Coupon Discount </span> 
                                  <span class="discount-name">SCBOX50</span>
                                  
                                  <span class="value cart-price"> -  <span class="fa fa-inr"></span>3,358.88</span>
                                  <span class="btn-link " id="coupon_clear" onclick="clear_coupon_code();"><i class="icon-close" ></i></span>
                                   </div>

                                </li>
                         
                        <li>
                        <span class="text-label">Taxes</span>
                        <span class="value cart-price"> <span class="fa fa-inr"></span>359.88</span>

                        </li>
                 
                  
                         
                  <li>
                  <span class="text-label text-right"><span class="sc-color">Total:</span></span>
                  
                    <span class="value sc-color price"> <span class="fa fa-inr"></span>3,358.88</span>
                
                  
                  </li>
                  </ul>
                  </div>
                  <div class="box-footer">
                      <a class="scbtn scbtn-primary" onclick="" href="<?php echo base_url();?>book-scbox/step5" id="proceed_to_checkout1"><span>Place Order</span></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
  
      <div id="modal-offers" class="modal fade modal-offers show11 in" role="dialog">   
      </div>
    </form>
  </div>
 
          <script type="text/javascript">localStorage.setItem('newcartcount', 0);</script>
          <div class="box-empty-cart">
          <h4>Your shopping cart is empty. </h4>
          <a  class="scbtn scbtn-primary" href=""><span>Shop Now</span></a>
          </div>
     

  </div>

</section>
  
<style>
  .scbox-payment-options li{display: inline-block; margin-right: 10px;}
  .scbox-payment-options  .sc-checkbox .icon{position: relative; top:-3px !important; margin-right: 2px;}
</style>

<script type="text/javascript">
  ga('send', 'event', 'SCBOX','clicked','Step3a-Cart');
</script>