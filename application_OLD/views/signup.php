<style type="text/css">
  fieldset.date { 
  margin: 0; 
  padding-top: 10px;  
  display: block; 
  border: none; 
} 

fieldset.date legend { 
  margin: 0; 
  padding: 0; 
  margin-top: .25em; 
  font-size: 100%; 
} 


fieldset.date label { 
  position: absolute; 
  top: -20em; 
  left: -200em; 
} 

fieldset.date select { 
margin: 0; 
padding: 0; 
font-size: 100%; 
display: inline; 
width: 30%;
} 

.hwidth{
  width: 20%;

}
span.inst { 
  font-size: 75%; 
  color: #13D792; 
  padding-left: .25em; 
  text-transform: uppercase;
} 


fieldset.date select:active, 
fieldset.date select:focus, 
fieldset.date select:hover 
{ 
  border-color: gray; 
  
} 
</style>
<?php 
//echo '<pre>';print_r(@$this->session->userdata());
  $user_dob_year = '';
  $user_dob_month = '';
  $user_dob_day = '';
  if(!empty($inputData_post->user_dob))
  {
    $user_dob = $userdata['user_dob'];
    $user_dob_arr = explode('-', $user_dob);
    $user_dob_year = $user_dob_arr[0];
    $user_dob_month = $user_dob_arr[1];
    $user_dob_day = $user_dob_arr[2];
  }

  @$inputData_post = json_decode($inputData);
  $maleActive= '';
  $femaleActive= '';$scname = '';
  if(@$inputData_post->sc_gender==1)
  {
    $femaleActive= 'active';
  }else if(@$inputData_post->sc_gender==2)
  {
    $maleActive= 'active';
  }
  $scname = (@$inputData_post->sc_name[0]!='' && @$inputData_post->sc_name[1]!='' ) ? @$inputData_post->sc_name[0].' '.@$inputData_post->sc_name[1] : '';
?>
<div class="page-signup-wrp">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
        <div class="signup-box-wrp page-login111 page-signup">

          <div class="box-signup">
            <div class="title-section">
              <h1 class="title">Sign Up</h1>
            </div>

          <div class="box-signup-inner signup-options">
            <div class="box-title">
            SIGN UP USING ANY OF THE FOLLOWING
            </div>
            <div>
                <a href="#" title="Sign in using Facebook" class="btn-facebook-wrp" id="facebook_login_in" ><i class="icon-facebook"></i></a>
                <!-- <a href="<?php //echo $this->data['google_login_url']; ?>" title="Sign in using Google" class="btn-google-wrp" onclick=""><i class="icon-google">
                  <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i>
              </a>  -->
              <span class="btn-email-wrp">
                  <img src="<?php echo base_url(); ?>assets/images/icons/ic-email.png" />
              </span>
            </div>
          </div> 
          <?php //echo base_url().'Schome/register_user'; ?>        
          <form name="scSignup" id="scSignup" method="post" action="<?php echo base_url().'Schome/register_user'; ?>">
          <div class="signup-by-email">
          <div class="box-signup-inner">
            <div class="box-title">
              Select your Gender
            </div>
            <ul class="gender-wrp">
              <li id="gender-male" class="<?php echo $maleActive; ?>">
                <span class="ic-gender ic-male "><i class="icon-male"></i>  </span>
                <span class="text-label">M</span>
              </li>
              <li id="gender-female"  class="<?php echo $femaleActive; ?>" >
                <span class="ic-gender ic-female "><i class="icon-female"></i></span>
                <span class="text-label">F</span>
              </li>
            </ul>
            <div class="form-group">
              <input type="hidden" id="sc_gender" name="sc_gender" class="form-control" placeholder="gender"  value="" >
              <label id="sc_gender-error" class="text-error message hide" for="sc_gender">Please select gender</label>
            </div>
          </div>
          <div class="section section-shadow">
            <div class="box-signup-inner">
              <div class="box-title">
                Fill in your Details
              </div>
              <div class="form-group">
                <input type="text" name="sc_name" id="sc_name" class="form-control"  maxlength="100" placeholder="NAME"  value="<?php echo $scname; ?>" onblur="validateSignup(this);" >
                <div class="message text-error hide" id="sc_lastname-error">Please enter your full name</div>              
              </div>            
              <div class="form-group-md">
                <input type="hidden" name="sc_dob" id="sc_dob" value="" >
                <fieldset class="date"> 
                  <label for="day_start"></label> 
                  <select id="day_start" name="day_start" onchange="validateform(this);" >
                    <option value="" >Day</option>  
                  <?php for($i=1;$i<32;$i++){ ?>
                    <option value="<?php echo $i; ?>" <?php echo ($user_dob_day==$i ) ? 'selected' : ''; ?> ><?php echo $i; ?></option> 
                  <?php } ?>                     
                  </select> - <label for="month_start"></label> 
                  <select id="month_start" name="month_start" onchange="validateform(this);" >
                    <option value="" >Month</option>    
                    <option value="1" <?php echo ($user_dob_month==1 ) ? 'selected' : ''; ?> >January</option>       
                    <option value="2" <?php echo ($user_dob_month==2 ) ? 'selected' : ''; ?> >February</option>       
                    <option value="3" <?php echo ($user_dob_month==3 ) ? 'selected' : ''; ?> >March</option>       
                    <option value="4" <?php echo ($user_dob_month==4 ) ? 'selected' : ''; ?> >April</option>       
                    <option value="5" <?php echo ($user_dob_month==5 ) ? 'selected' : ''; ?> >May</option>       
                    <option value="6" <?php echo ($user_dob_month==6 ) ? 'selected' : ''; ?> >June</option>       
                    <option value="7" <?php echo ($user_dob_month==7 ) ? 'selected' : ''; ?> >July</option>       
                    <option value="8" <?php echo ($user_dob_month==8 ) ? 'selected' : ''; ?> >August</option>       
                    <option value="9" <?php echo ($user_dob_month==9 ) ? 'selected' : ''; ?> >September</option>       
                    <option value="10" <?php echo ($user_dob_month==10 ) ? 'selected' : ''; ?> >October</option>       
                    <option value="11" <?php echo ($user_dob_month==11 ) ? 'selected' : ''; ?> >November</option>       
                    <option value="12" <?php echo ($user_dob_month==12 ) ? 'selected' : ''; ?> >December</option>      
                  </select> -
                  <label for="year_start"></label> 
                  <select id="year_start" name="year_start" onchange="validateform(this);" >
                    <option value="" >Year</option>  
                  <?php for($i=date("Y");$i>=1947;$i--){ ?>
                    <option value="<?php echo $i; ?>" <?php echo ($user_dob_year==$i ) ? 'selected' : ''; ?> ><?php echo $i; ?></option> 
                  <?php } ?>
                  </select> 
                  <span class="inst">Date of Birth*</span>                 
                </fieldset>
                 <label id="sc_dob-error" class="message text-error hide" for="sc_dob">Please enter your date of birth</label>                      
            </div>
              <div class="form-group">
                <input type="email" name="sc_emailid" id="sc_emailid" class="form-control" placeholder="EMAIL ID" maxlength="100" onblur="validateform();" value="<?php echo @$inputData_post->scemail_id; ?>">
                <div class="message text-error hide"></div>
              </div>

              <div class="form-group">
                <input type="password" name="sc_password" id="sc_password" class="form-control" placeholder="PASSWORD" maxlength="100">
                <div class="message text-error hide"></div>
              </div>
              <div class="form-group">
                <input type="tel" name="sc_mobile" id="sc_mobile" class="form-control" placeholder="MOBILE NUMBER" maxlength="10"  value="<?php echo @$inputData_post->scmobile; ?>">
                <div class="message text-error hide"></div>
              </div>
                <div id="signup_error" class="message text-danger" signup-error="<?php echo @$error; ?>" ><?php echo @$error; ?></div>
              <div class="scbtns-wrp text-center">
                <!-- <button type="submit" id="sc_signup" name="sc_signup" class="scbtn scbtn-primary"><span>Sign Up</span></button> -->
                <button  id="sc_signup" name="sc_signup" class="scbtn scbtn-primary" onclick="sc_signIn();" ><span>Sign Up</span></button>
              </div>
            </div>
          </div>
        </div>
        </form>

          </div>
          <section class="section section-shadow page-signup">
            <div class="container">
              <div class="box-singn-signup-link">
                <div class="box-title">
                  Already have an account?
                </div>
                <div class="scbtns-wrp text-center" onclick="ga('send', 'event', 'Login', 'clicked','header');">
                  <a class="scbtn scbtn-tertiary" href="login"><span>Login</span></a>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
</div>
