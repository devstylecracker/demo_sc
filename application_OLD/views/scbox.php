
<style type="text/css">
  @media (min-width: 768px){
.page-scbox .section-banner {
    background: #F4F3F8 url(https://www.stylecracker.com/ URL::assets/images/scbox/box-banner.jpg) right center no-repeat;
}
}
.page-scbox .title-section .title {
    font-size: 24px;
    line-height: 1.3;
}


.navbar-collapse{
  padding-right: 0 !important;
  padding-left: 0 !important;
}

.price-wrapper{
  position: relative; 
  display: inline-block;
}

.price-slash{
    position: relative;
    width: 120%;
    height: 0;
    left: -3px;
    border-top: 1px solid red;
    transform: rotate(180deg);
    top: 7px;
}

</style>
  <link href='<?php echo base_url(); ?>assets/css/custom_scbox.css?v=1.100' rel='stylesheet' type='text/css'>
  <link href='<?php echo base_url(); ?>assets/css/mobile_scbox.css?v=1.100' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">
 <?php
  //$scbox_pack = unserialize(SCBOX_PACKAGE);
  $tab_type = 'women';
  if(!empty(@$_COOKIE['scbox-gender']) && @$_COOKIE['scbox-gender']!='')
  {
    $tab_type = @$_COOKIE['scbox-gender'];
  }else
  {
    setcookie('scbox-gender', 'women', time() + (86400 * 30), "/");
    $tab_type = @$_COOKIE['scbox-gender']; 
  }
 
?>
 
<div class="page-scbox">
  <section id="section-banner" class="section section-shadow section-banner">
    <div class="container">
      <div class="box-banner">
        <div class="row">
          <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="title-section home-patch">
              <h1 class="title">
            <span>  <b>A Box Full of Fashion.<br /> That you are Guaranteed to Love.</b><br />
            Put together by our celebrity Stylists.<br /> DELIVERED STRAIGHT TO YOUR DOORSTEP.
          </span></h1>
              <h2 class="subtitle" style="font-weight:normal;"> Simple. Stylish. Stress Free.</h2>
            </div>
            <div>
              <a class="scbtn scbtn-primary link-scrolling" href="#section-pricing" class="link-scrolling" onclick=""><span>Order Now</span></a>
            </div>
             <h2 style="font-weight:normal;font-size:14px;text-transform: uppercase;letter-spacing: 2px;">  Flat 50% off on all the boxes</h2>
          </div>
          <div class="col-md-4 col-sm-12 col-xs-12">
          </div>
        </div>
      </div>
  </section>
  <section id="section-curated" class="section section-shadow section-curated">
    <div class="container">
      <div class="title-section text-center">
        <h1 class="title"><span>Curated for You</span></h1>
      </div>
      <div class="row">
        <div class="col-md-9 col-sm-12 col-xs-12 text-center">
          <div class="box-video">
              <div class="video-wrp">
        <iframe width="640" height="360" src="https://www.youtube.com/embed/VPrI2rk7FU4" frameborder="0" allowfullscreen></iframe>
       <!-- <iframe width="640" height="360" src="https://www.youtube.com/embed/dHzTnCwvkxQ" frameborder="0" allowfullscreen></iframe> -->
          <!-- <iframe width="640" height="360" src="https://www.youtube.com/embed/NYQIaHWpARY?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe> -->
          </div>
          <div class="btns-wrp">
            <a class="scbtn scbtn-primary link-scrolling" href="#section-pricing"  onclick="" ><span>Get Started</span></a>
          </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
           <div class="box-curated">
            <h3 class="subtitle">Your Style</h3>
            <div class="desc">
              One size does not fit all and we get that. At StyleCracker we curate fashion for you keeping in mind your personality, likes, dislikes, body shape, <b>budget</b> and other important details. Yes, we've got you covered every step of the way.
            </div>
            <h3 class="subtitle">Your Budget</h3>
            <div class="desc">
              StyleCracker is for everyone! Once we've understood your <b>style</b>, our stylists will curate a box for you within your budget. While we do have pre-set amounts and quantities to get you started on your selection, you could also customize your order.
            </div>
            <h3 class="subtitle">Your Convenience</h3>
            <div class="desc">
              StyleCracker is designed to make life easier and more convenient for you. All you need to do is fill out a form, speak to our stylists and get styled. It's that simple! Leave the fashion to the experts and enjoy the stress free shopping experience!
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="section-works" class="section section-shadow section-works" style="background-color: rgba(19,215,146, .1);">
    <div class="container">
      <div class="title-section text-center">
        <h1 class="title"><span>How it Works</span></h1>
      </div>
      <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="box">
            <h3 class="subtitle">We get to know you</h3>
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/scbox/how-it-works-1.png" alt="">
            </div>
            <div class="desc">
             To get started, you'll need to fill out a simple online form. This helps us decode your style preferences. Next, a StyleCracker celebrity stylist will call you to understand your requirements. Once this is done, the stylist will get to work to curate your box.
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="box">
            <h3 class="subtitle">WE SEND YOU THE BOX</h3>
            <div class="img-wrp">
             <img src="<?php echo base_url(); ?>assets/images/scbox/how-it-works-2.png" alt="">
            </div>
            <div class="desc">
              Once our stylist has curated the box keeping in mind your likes, dislikes, body shape and budget we put it all together and ship you, your box of style. Shipping is absolutely free and it gets delivered straight to your doorstep.
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="box">
            <h3 class="subtitle">Keep only what you like</h3>
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/scbox/how-it-works-3.png" alt="">
            </div>
            <div class="desc">
              When you receive the box, check out the selection we've sent you. Try it on for size and to see how it works for you. Keep everything you like. If there's something that doesn't suit you, send it back to us. No questions asked.
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="box">
            <h3 class="subtitle">Give us your feedback</h3>
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/scbox/how-it-works-4.png" alt="">
            </div>
            <div class="desc">
              Our goal is to ensure that you, and every other <b>user</b>, loves every item curated personally for you. Your constant feedback is most valuable to make sure we get even better. So please take a couple of minutes to tell us what you liked and what you didn't.
            </div>
          </div>
        </div>
      </div>

      <div class="btns-wrp">
        <a class="scbtn scbtn-primary link-scrolling" href="#section-pricing" class="link-scrolling" onclick="" ><span>Order Now</span></a>
      </div>
    </div>
  </section>

 <section id="section-testimonials" class="section section-shadow section-testimonials">
    <div class="container">
      <div class="title-section text-center">
        <h1 class="title"><span>TESTIMONIALS</span></h1>
        <!-- <h2 style="font-weight:normal;font-size:14px;text-transform: uppercase;
    letter-spacing: 2px;">We asked a few of our customers about their first experience with the StyleCracker Box</h2> -->
      </div>
      <div class="row">
         <!-- <div class="col-md-6 col-sm-12 col-xs-12 text-center">
            <div class="box-video">
               <div class="video-wrp">
                  <iframe width="450" height="300" src="https://www.youtube.com/embed/6k5JAHuM_TY" frameborder="0" allowfullscreen></iframe>
                  <iframe width="450" height="300" src="https://www.youtube.com/embed/LErP_7JpHjA" frameborder="0" allowfullscreen></iframe> 
                  <iframe width="640" height="360" src="https://www.youtube.com/embed/dHzTnCwvkxQ" frameborder="0" allowfullscreen></iframe> 
                  <iframe width="640" height="360" src="https://www.youtube.com/embed/NYQIaHWpARY?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe> 
               </div>
               <div class="btns-wrp">
                  <a class="scbtn scbtn-primary link-scrolling" href="#section-pricing" class="link-scrolling" onclick="ga('send', 'event', 'SCBOX-OrderNow','clicked','4-Testimonials');" ><span>Order Now</span></a>
               </div>
            </div>
            </div> -->
         <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="row wow bounceIn" data-wow-duration="2s">
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="img-wrp">
                     <img src="<?php echo base_url(); ?>assets/images/home/banner/ramona.jpg" style="width:127px;" alt="">
                  </div>
               </div>
               <div class="col-md-8 col-sm-8 col-xs-12">
                  <!--    <div class="title"> </div>-->
                  <div class="desc">
                    <p>The StyleCracker stylists have been like 3 am friends. They have helped me pick products that suit me and my body type. Every purchase is tailor made and I love how personal it gets. And in rare scenario when a product doesn’t work , the team is extremely helpful and prompt in arranging easy returns.</p>
                    <div class="name">
                      - Ramona
                    </div>
                  </div>
               </div>
            </div>
            <div class="row wow bounceIn" data-wow-duration="3s">
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="img-wrp">
                     <img src="<?php echo base_url(); ?>assets/images/home/banner/rajni.jpg" style="width:127px;" alt="">
                  </div>
               </div>
               <div class="col-md-8 col-sm-8 col-xs-12">
                  <!--    <div class="title"> </div>-->
                   <div class="desc">
                      <p>Absolutely love the stuff on stylecracker! Chic and unique. Alot of pretty cool brands to choose from. Customer service is fab! They literally take care of you and your order.</p>
                      <div class="name">
                        - Rajni Rodrigues
                      </div>
                    </div>
               </div>
            </div>
         </div>
         <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="row wow bounceIn" data-wow-duration="3s">
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="img-wrp">
                     <img src="<?php echo base_url(); ?>assets/images/home/banner/priyanka1.jpg" alt="">
                  </div>
               </div>
               <div class="col-md-8 col-sm-8 col-xs-12">
                  <!--    <div class="title"> </div>-->
                  <div class="desc">
                     <p>I received my surprise box of goodies from StyleCracker and it just couldn't get better! In this day and age of online shopping, a box of goodies suited to my style and preference was something very outstanding. Not to mention how flattering
                        it is to have a box personalized to my taste and style. Way to go StyleCracker!
                     </p>
                     <div class="name">
                        - Priyanka Talreja
                     </div>
                  </div>
               </div>
            </div>
            <div class="row wow bounceIn" data-wow-duration="2s">
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="img-wrp">
                     <img src="<?php echo base_url(); ?>assets/images/home/banner/glen.jpg" alt="">
                  </div>
               </div>
               <div class="col-md-8 col-sm-8 col-xs-12">
                  <!--    <div class="title"> </div>-->
                  <div class="desc">
                     <p>Since I was looking for a personal stylist for myself, having one who's worked with A-listers and the biggest names in the movie business made StyleCracker my absolute choice! From being style guides to helping me with even 'what not to wear',
                        they've been extremely helpful in solving all my fashion queries.
                     </p>
                     <div class="name">
                        - Glenn Gonsalves
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    
    </div>
  </section>
  <section id="section-commitment" class="section section-shadow section-commitment" style="background-color: rgba(19,215,146, .1);">
    <div class="container">
      <div class="title-section text-center">
        <h1 class="title"><span>Our Commitment</span></h1>
      </div>
      <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="box">
            <h3 class="subtitle">It's all about you</h3>
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/scbox/our-comm-1.png" alt="">
            </div>
            <div class="desc">
              The StyleCracker box is about you. While our stylists put together and curate each item in the box, it's always done keeping your style in mind.
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="box">
            <h3 class="subtitle">Experienced Stylists</h3>
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/scbox/our-comm-2.png" alt="">
            </div>
            <div class="desc">
              All stylists part of the StyleCracker family come with years of experience. From styling celebrities, editorials and films to individuals for weddings and day-to-day requirements, we do it all. So worry not! You are in great hands.
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="box">
            <h3 class="subtitle">Transparent Pricing</h3>
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/scbox/our-comm-3.png" alt="">
            </div>
            <div class="desc">
             Every product you get comes with the official label and price tag, so you'll get what you paid for. More often than not, you'll get stuff worth a lot more.
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="box">
            <h3 class="subtitle">Returns, Replacements and Refunds</h3>
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/scbox/our-comm-4.png" alt="">
            </div>
            <div class="desc">
              If you don't like something we send in the StyleCracker box, just <b>return it</b>. No questions asked.
            </div>
          </div>
        </div>
      </div>

      <div class="btns-wrp">
        <a class="scbtn scbtn-primary link-scrolling" href="#section-pricing" class="link-scrolling" onclick="" ><span>Order Now</span></a>
      </div>
    </div>
  </section>

  <section id="section-pricing" class="section section-shadow section-pricing">
    <div class="container">
      <div class="title-section text-center">
        <h1 class="title"><span>Pricing</span></h1>
        <h2 style="font-weight:normal;font-size:15px;text-transform: uppercase;
          letter-spacing: 2px;"><!--  Flat 50% off on all the boxes--></h2>
          <ul class="nav nav-tabs sc-tabs">
              <li class="<?php echo (!empty($tab_type) && $tab_type=='women') ? 'active' : ''; ?>" ><a data-toggle="tab" href="#women" id="womentab" onclick="selecttab('women');" >Women</a></li>
              <li class="<?php echo (!empty($tab_type) && $tab_type=='men') ? 'active' : ''; ?>" ><a data-toggle="tab" href="#men" id="mentab" onclick="selecttab('men');" >Men</a></li>
          </ul>


      </div>

          <!-- MEN Women --> 
   <div class="tab-content">
    <div id="women"  class="tab-pane fade in <?php echo (!empty($tab_type) && $tab_type=='women') ? 'active' : ''; ?>">
      <div class="row">
		<?php 
			if(is_array($scbox_pack_female) && count($scbox_pack_female)>0)
			{
				foreach($scbox_pack_female as $key => $femalePackage)
				{
					?>
					 <div class="col-md-3 col-sm-6 col-xs-6">
						<div class="box-package " data-pack="<?php echo $femalePackage['object_meta_value']; ?>" data-scbox-price="<?php echo $femalePackage['object_meta_value']; ?>" data-scbox-objectid="<?php echo $femalePackage['id']; ?>" data-scbox-productid="<?php echo $femalePackage['id']; ?>" onclick="SetPackage(this);" style="background: #e4fbf3;">
						  <div class="img-wrp">
						  <img src="https://www.stylecracker.com/assets/images/scbox/women/<?php echo $femalePackage['object_image']; ?>" alt="">
						  </div>
						  <div class="title"><?php echo $femalePackage['object_name']; ?></div>
						<?php
						if($femalePackage['slug'] == 'unlimited_items')
							{
						?>
							<div class="form-group">
								<input type="text" class="form-control" name="customboxprice" id="customboxprice" onblur="getcustomprice();" maxlength="20" placeholder="Give us your budget" />
								<label id="customboxprice-error" class="message error hide" for="customboxprice">Please enter your budget</label>
							</div>
							<?php 
							}
							else 
								{
									echo '<i class="fa fa-inr"></i> '.$femalePackage['object_meta_value']; 
								} 
							 ?>
						   <div class="price" style="font-size: 18px;"><?php //echo $val->notice; ?></div>
						</div>
					  </div>
				<?php
					
				}
			}
		?>
		
       <!-- <?php
			
            //$pricehtml = '';            
            //<input type="tel" class="form-control" name="customboxprice" id="customboxprice" onblur="getcustomprice();" required placeholder="Give us your budget" />
          /* foreach($scbox_pack as $val){
             if($val->show==1 )
             {
                    if($val->packname=='package4')
                    {
                     $pricehtml ='<div class="form-group">
                        <input type="text" class="form-control" name="customboxprice" id="customboxprice" onblur="getcustomprice();" maxlength="20" placeholder="Give us your budget" />
                        <label id="customboxprice-error" class="message error hide" for="customboxprice">Please enter your budget</label>
                     </div>';
                   }else
                   {
                       $pricehtml = '<div class="price-wrapper"><div class="price-slash"></div><div class="price"><i class="fa fa-inr"  ></i>'.$val->price.'</div></div>';
                   }
                   if($val->type=='women' || $val->type=='a' )
                   { */
            ?>
          <div class="col-md-3 col-sm-6 col-xs-6">
            <div class="box-package " data-pack="<?php //echo $val->packname; ?>" data-scbox-price="<?php //echo $val->price; ?>" data-scbox-objectid="<?php //echo $val->id; ?>" data-scbox-productid="<?php //echo $val->productid; ?>" onclick="SetPackage(this);" style="background: #e4fbf3;">
              <div class="img-wrp">
              <img src="<?php //echo base_url(); ?>assets/images/scbox/<?php //echo $val->packname.'.jpg'; ?>" alt="">
              </div>
              <div class="title"><?php //echo $val->name; ?></div>
              <!-- <div class="desc">
               <?php //echo $val->desc; ?>
              </div> ->
                <?php //echo $pricehtml; ?>
               <div class="price" style="font-size: 18px;"><?php //echo $val->notice; ?></div>
            </div>
          </div>

        <?php /* } } } */?>-->
    </div> 
  </div>

   <div id="men"  class="tab-pane fade in  <?php echo (!empty($tab_type) && $tab_type=='men') ? 'active' : ''; ?>">
      <div class="row">
	  <?php
			if(is_array($scbox_pack_male) && count($scbox_pack_male)>0)
			{
				foreach($scbox_pack_male as $key => $malePackage)
				{
					?>
					<div class="col-md-3 col-sm-6 col-xs-6">
						<div class="box-package " data-pack="<?php echo $malePackage['object_meta_value']; ?>" data-scbox-price="<?php echo $malePackage['object_meta_value']; ?>" data-scbox-objectid="<?php echo $malePackage['id']; ?>" data-scbox-productid="<?php echo $malePackage['id']; ?>" onclick="SetPackage(this);" style="background: #e4fbf3;">
						  <div class="img-wrp">
						  <img src="https://www.stylecracker.com/assets/images/scbox/men/<?php echo $malePackage['object_image']; ?>" alt="">
						  </div>
						  <div class="title"><?php echo $malePackage['object_name']; ?></div>
						  <!-- <div class="desc">
						   <?php echo $malePackage['']; ?>
						  </div> -->
							<?php
								if($malePackage['slug'] == 'unlimited_items')
								{
								?>
								<div class="form-group">
									<input type="text" class="form-control" name="customboxprice1" id="customboxprice1" onblur="getcustomprice();" maxlength="20" placeholder="Give us your budget" />
									<label id="customboxprice1-error" class="message error hide" for="customboxprice1">Please enter your budget</label>
								 </div>
								<?php 
								}
							else 
								{
									echo '<i class="fa fa-inr"></i> '.$malePackage['object_meta_value']; 
								} 
							 ?>
						   <div class="price" style="font-size: 18px;"><?php //echo $malePackage; ?></div>
						</div>
					</div>
					<?php
				}
			}
	  ?>
	  
        <!--<?php
           // $pricehtml = '';        
            //<input type="tel" class="form-control" name="customboxprice" id="customboxprice" onblur="getcustomprice();" required placeholder="Give us your budget" />
          /*foreach($scbox_pack as $val){
             if($val->show==1 )
             {
                    if($val->packname=='package4')
                    {
                     $pricehtml ='<div class="form-group">
                        <input type="text" class="form-control" name="customboxprice1" id="customboxprice1" onblur="getcustomprice();" maxlength="20" placeholder="Give us your budget" />
                        <label id="customboxprice1-error" class="message error hide" for="customboxprice1">Please enter your budget</label>
                     </div>';
                   }else
                   {
                       $pricehtml = '<div class="price-wrapper"><div class="price-slash"></div><div class="price"><i class="fa fa-inr"  ></i>'.$val->price.'</div></div>';
                   }
                   if($val->type=='men' || $val->type=='a' )
                   {*/
            ?>
          <div class="col-md-3 col-sm-6 col-xs-6">
            <div class="box-package " data-pack="<?php //echo $val->packname; ?>" data-scbox-price="<?php //echo $val->price; ?>" data-scbox-objectid="<?php //echo $val->id; ?>" data-scbox-productid="<?php //echo $val->productid; ?>" onclick="SetPackage(this);" style="background: #e4fbf3;">
              <div class="img-wrp">
              <img src="<?php //echo base_url(); ?>assets/images/scbox/<?php //echo $val->packname.'.jpg'; ?>" alt="">
              </div>
              <div class="title"><?php //echo $val->name; ?></div>
              <!-- <div class="desc">
               <?php //echo $val->desc; ?>
              </div> ->
                <?php //echo $pricehtml; ?>
               <div class="price" style="font-size: 18px;"><?php //echo $val->notice; ?></div>
            </div>
          </div>

        <?php /*} } }*/?>-->
    </div> 
  </div>
</div>    

    <!-- MEN Women --> 

      
    <div class="text-center">
      <a class="scbtn scbtn-primary link-scrolling" href="javascript:void(0);" onclick="scboxregister();" ><span>Order Now</span></a>
      <div class="form-group" ><label class="hide" id="scboxregistererror">Select your scbox package</label></div>
    </div>
<input type="hidden"  name="utm_source" id="utm_source" value="<?php echo @$_GET['utm_source']; ?>" />
      <input type="hidden"  name="utm_medium" id="utm_medium" value="<?php echo @$_GET['utm_medium']; ?>" />
      <input type="hidden"  name="utm_campaign" id="utm_campaign" value="<?php echo @$_GET['utm_campaign']; ?>" />
      <input type="hidden"  name="tab_type" id="tab_type" value="women" />
    </div>
  </section>
  </div>
