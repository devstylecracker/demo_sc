
  
<div class="page-login-wrp">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
        <div class="login-box-wrp page-login">

          <div class="box-login">
            <div class="title-section">
              <h1 class="title">Login</h1>
            </div>
            <form name="scnewLogin" id="scnewLogin" method="post" action="">
            <div>
              <div class="form-group">
                <input type="text" id="logEmailid" name="logEmailid" class="form-control" placeholder="EMAIL ID">
                <div class="message text-error hide">Password is invalid. </div>
              </div>

              <div class="form-group has-error111">
                <input type="password" id="logPassword" name="logPassword" class="form-control" placeholder="PASSWORD">
                <div class="message text-danger hide">Password is invalid. </div>
                <a href="#modal-forgot-password" data-toggle="modal" class="link-forgot-pw">Forgot Password?</a>
              </div>
              <div id="logIn_error" class="message text-danger "><?php echo @$error; ?></div>
              <div class="btns-wrp text-center">
                 <button type="submit" id="sc_Login" name="sc_Login" class="scbtn scbtn-primary"><span>Login</span></button>
              </div>
            </div>
            </form>

            <div class="hr-or"><i></i><span>OR</span><i></i></div>

            <div class="box-title">
              Sign in using
            </div>
            <div>

              <a href="" title="Sign in using Facebook" class="btn-facebook-wrp" id="facebook_login" ><i class="icon-facebook"></i></a>

           <!--  <a href="<?php //echo $this->data['google_login_url']; ?>" title="Sign in using Google" class="btn-google-wrp"><i class="icon-google">
                <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i>
              </a>  -->
            </div>

          </div>
          <section class="section section-shadow page-login">
            <div class="box-singn-signup-link">
              <div class="box-title">
                Don't have an Account?
              </div>
              <div class="text-center" onclick="">
                <a class="scbtn scbtn-tertiary" href="./signup"><span>Sign Up</span></a>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
</div>

