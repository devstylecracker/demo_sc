<section class="section section-shadow section-get-box" style="min-height:350px; overflow:hidden;">
  <div class="container">
    <div class="row box">
      <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="img-wrp">
          <img src="<?php echo base_url(); ?>assets/images/pages/scbox.svg" alt="">
        </div>
      </div>
      <div class="col-md-6 col-sm-12 col-xs-12">
  <div  style="padding-top:80px;">
        <div class="title-section">
         <!--  <h1 class="subtitle">Thank you for registering</h1> -->
          <h1 class="subtitle">THANK YOU  </h1>
        </div>
        <div class="desc">
      <p>
        Thank you for booking your StyleCracker Box! <br>
        Your order is now complete. We're very excited to have you on board! <br>
        Now sit back, relax and leave the rest to us!
    </p>
        </div>
      </div>
    </div>
        </div>
  </div>


</section>
