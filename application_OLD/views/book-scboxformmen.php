<?php
   //echo $form;
   
   $scbox_pack = unserialize(SCBOX_PACKAGE);
   
   
   
   
   ?>
<style>

   .page-scbox-form .list-occassion li{
      text-align: center !important;
   }

   ul.lespad {
   display: flex !important;
   }
   .form-control{
   text-transform: capitalize !important;
   }
  
   .page-scbox-form .list-skin-tone li .title-3 {
   padding-top: 15px !important;
   }
   .page-scbox-form .list-skin-tone li{width: 60px;} 
   @media(max-width:768px) {
  
   .page-scbox-form ul.list-skin-tone li {
   width: 60px !important;
   }
   }
   .btn-group.special {
   display: flex !important;
   }
   .page-scbox-form .list-skin-tone li .pallet {
   height: 40px !important;
   width: 40px !important;
   }
   .special .btn {
   flex: 1 !important;
   }
 
   .list-skin-tone li {
   width: 76px !important;
   }
   .page-scbox-form .list-bags li.active,
   .page-scbox-form .list-footweartype li.active,
   .page-scbox-form .list-heeltype li.active {
   outline: 2px solid #13D792 !important;
   cursor: default !important;
   }
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li,
   .page-scbox-form .list-dneed li {
   display: inline-block !important;
   width: 146px !important;
   margin: 10px 4px !important;
   cursor: pointer !important;
   }
   .page-scbox-form .list-fabrics li.active,
   .page-scbox-form .list-prints li.active,
   .page-scbox-form .list-dneed li.active {
   outline: 2px solid #ed1b2e !important;
   cursor: default !important;
   }
   .page-scbox-form .list-fabrics li {
   display: inline-block !important;
   width: 140px !important;
   margin: 10px 4px !important;
   cursor: pointer !important;
   }
   .btn {
   border: 1px solid #000 !important;
   color: #333 !important;
   border-radius: 0px !important;
   outline: none !important;
   }
   .btn-green.active,
   .btn-green.active.focus,
   .btn-green:active {
   border: 1px solid #13D792 !important;
   background: #13D792 !important;
   color: #fff !important;
   outline: none !important;
   }
   .btn-red.active,
   .btn-red.active.focus,
   .btn-red:active {
   border: 1px solid #ed1b2e !important;
   background: #ed1b2e !important;
   color: #fff !important;
   outline: none !important;
   }
   .choose_file {
   position: relative !important;
   display: inline-block !important;
   border-bottom: #13D792 solid 1px;
   width: 300px !important;
   padding: 4px 6px 4px 8px !important;
   color: #000 !important;
   margin-top: 2px !important;
   background: white !important;
   }
   .choose_file input[type="file"] {
   -webkit-appearance: none !important;
   position: absolute !important;
   top: 0 !important;
   left: 0 !important;
   opacity: 0 !important;
   }
   
   .hwidth {
   width: 15% !important;
   text-transform: uppercase !important;
   }
   @media (max-width: 768px) {
   .hwidth {
   width: 60% !important;
   text-transform: uppercase !important;
   }
   }
   
   @media (max-width: 768px) {
   .page-scbox-form .list-colors li,
   .page-scbox-form .list-prints li,
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li {
   width: 80px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   }
   }
   @media (max-width: 768px) {
   .page-scbox-form .list-bags li img,
   .page-scbox-form .list-footweartype li img,
   .page-scbox-form .list-heeltype li img {
   max-width: 100% !important;
   }
   }
   @media (max-width: 768px) {
   .page-scbox-form ..list-bags li .pallet,
   .page-scbox-form .list-footweartype li .pallet,
   .page-scbox-form .list-heeltype li .pallet {
   width: 50px !important;
   height: 50px !important;
   margin: 5px auto 5px !important;
   }
   }
   .form-control {
   display: inline !important;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   height: 280px !important;
   }
   .pallet img {
   width: 146px !important;
   }
   @media (max-width: 768px) {
   .page-scbox-form .list-colors li,
   .page-scbox-form .list-prints li,
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li {
   width: 135px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   width: 100% !important;
   }
   }
   .extrapad {
   margin-top: 60px !important;
   margin-bottom: 60px !important;
   }
   /*.extrapad {
  margin-top: 60px;
   margin-bottom: 40px;
   padding-top: 60px;
   }
   .extrapad1 {
  margin-top: 30px;
   margin-bottom: 40px;
   }*/
   @media (max-width: 768px) {
      .extrapad {
  
   padding-top: 80px !important;
   }
   .extrapad1 {
  margin-top: 30px !important;
   margin-bottom: 40px !important;
   }
   }
   .page-scbox-form .pa-que{
      padding: 0 0 !important;
   }
   .page-scbox-form .bigimg li{
   display: inline-block !important;
   width: 180px !important;
   margin: 0px 4px 20px !important;
   cursor: pointer !important;
   background: #fafafa !important;
   border:1px solid #dfdfdf !important;
   }
   .noborder{
   border:none !important;
   }
   ul.list-categories{
   text-transform: capitalize !important;
   }
   .page-scbox-form .item.quiz .title {
   text-transform: capitalize !important;
   }
   .title-3{
   font-size: 13px !important;
   }
   @media (max-width: 768px)
   {
   .page-scbox-form .bigimg li{
   display: inline-block !important;
   width: 125px !important;
   margin: 0px 8px 20px !important;
   cursor: pointer !important;
   background: #fafafa !important;
   border:1px solid #dfdfdf !important;
   }
   .noborder{
   border:none !important;
   }
   }
   .title-section-changed .title-changed{
   margin: 0;
   line-height: 1;
   color: #333;
   display: inline-block;
   font-weight:normal;
   font-size:13px;
   letter-spacing: 2px;
   font-style:italic;
   }
   @media (max-width: 768px)
   {
   .title-section-changed .title-changed {
   font-size: 13px;
   line-height: 1.3;
   }
   }
   .mobile-align {
   width: 28% !important;
   text-align: left !important;
   margin-left:38% !important; 
   }

   .desktop-align{
      width: 25% !important;
   text-align: left !important;
   margin-left:46% !important;
   }

   .mobile-shade {
   width: 68% !important;
   text-align: left !important;
   margin-left:22% !important; 
   }

   .desktop-shade{
      width: 25% !important;
   text-align: left !important;
   margin-left:38% !important;
   }
 label{
      margin-top: 5px !important;
   }
</style>
<div class="page-scbox-form">
   <!-- action="<?php //echo base_url(); ?>book-scbox/<?php //echo @$userdata['scbox_objectid']; ?>"-->
   <form name="scbox-form" id="scbox-form" method="post" action="" >
<div id="slide_1" class="">
      <div class="men_categories category-selected">
         <section class="section section-shadow ">
            <div class="container section-category-selected scbox-container" type="checkCategories">
                  <div class="pa-que"><span>What I Want In My Box</span>
                  <div class="title-section-changed text-center"  >
                        <h1 class="title-changed" id="catsel">
                          
                        </h1>
                     </div>
                 </div>
                   <div class="desktop-align hidden-xs hidden-sm">
                     <ul class="list-categories pa-men" type="pa-men">
                        <li>
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>apparel</span></label>
                           <br>
                        </li>
                        <li>
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>bags</span></label>
                           <br>
                        </li>
                        <li style="display: none;">
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>footwear</span></label>
                           <br>
                        </li>
                        <li>
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>accessories</span></label>
                           <br>
                        </li>
                        <li>
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>grooming</span></label>
                           <br>
                        </li>
                     </ul>
                  </div>


                    <div class="mobile-align hidden-md hidden-lg">
                     <ul class="list-categories pa-men" type="pa-men">
                        <li>
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>apparel</span></label>
                           <br>
                        </li>
                        <li>
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>bags</span></label>
                           <br>
                        </li>
                        <li style="display: none;">
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>footwear</span></label>
                           <br>
                        </li>
                        <li>
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>accessories</span></label>
                           <br>
                        </li>
                        <li>
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>grooming</span></label>
                           <br>
                        </li>
                     </ul>
                  </div>

                  
            </div>
            <label id="men_categories-error" class="message error hide" for="men_categories">Choose categories</label>
         </section>
         <section class="section section-shadow">
            <div class="btns-wrp">
               <!-- <a class="scbtn scbtn-primary" href="javascript:void(0);" onclick="slideprev(2);" ><span>Previous</span></a> -->
               <a class="scbtn scbtn-primary link-scrolling" href="javascript:void(0);" onclick="ChangeStep(2,'next');"><span>Next</span></a>
            </div>
         </section>
      </div>
   </div>

 <div id="slide_2" class="hide">
    <section class="section section-shadow ">
      <div id="question_1_1" class="pa-slide-box body-shape category-apparel section-pa hide"><!-- hide -->
            <div class="container ">
               <div class="pa-que">
                  <span> This Is What My Body Looks Like</span>
                  <div class="title-section-changed text-center"  >
                     <h1 class="title-changed">
                        (Pick one)
                     </h1>
                  </div>
               </div>
               <label id="body_shape_ans-error" class="message error hide hidden-md hidden-lg" for="body_shape_ans">Please select body shape</label>
               <br />
               <br />
               <div class="box-body-shape male">
                  <div class="row pa-container men" name="scbox_bodyshape" id="scbox_bodyshapemen">
                     <div class="col-md-2 col-sm-4 col-xs-6 item-col deactive" data-attr="4695" data-attr2="1" data-attr3="Slim">
                        <div class="item quiz" id="bodyshape_4695">
                           <div class="img-wrp">
                              <img class="" id="oignal_img4695" src="https://www.stylecracker.com/assets/seventeen/images/pa/male/shapes/slim.svg">
                              <img class="hide" id="selected_img4695" src="https://www.stylecracker.com/assets/seventeen/images/pa/male/shapes/slim-selected.svg">
                           </div>
                           <div class="title">Slim</div>
                           <div class="desc">Lean Shoulders<br>Medium chest<br>Thin waist</div>
                        </div>
                     </div>
                     <div class="col-md-2 col-sm-4 col-xs-6 item-col deactive" data-attr="4696" data-attr2="1" data-attr3="Average">
                        <div class="item quiz" id="bodyshape_4696">
                           <div class="img-wrp">
                              <img class="" id="oignal_img4696" src="https://www.stylecracker.com/assets/seventeen/images/pa/male/shapes/average.svg">
                              <img class="hide" id="selected_img4696" src="https://www.stylecracker.com/assets/seventeen/images/pa/male/shapes/average-selected.svg">
                           </div>
                           <div class="title">Average</div>
                           <div class="desc">Broad shoulders<br>Wide chest<br>Narrow waist</div>
                        </div>
                     </div>
                     <div class="col-md-2 col-sm-4 col-xs-6 item-col deactive" data-attr="4697" data-attr2="1" data-attr3="Athletic">
                        <div class="item quiz" id="bodyshape_4697">
                           <div class="img-wrp">
                              <img class="" id="oignal_img4697" src="https://www.stylecracker.com/assets/seventeen/images/pa/male/shapes/athletic.svg">
                              <img class="hide" id="selected_img4697" src="https://www.stylecracker.com/assets/seventeen/images/pa/male/shapes/athletic-selected.svg">
                           </div>
                           <div class="title">Athletic</div>
                           <div class="desc">Bulky shoulders<br>Broad chest<br>Medium waist</div>
                        </div>
                     </div>
                     <div class="col-md-2 col-sm-4 col-xs-6 item-col deactive" data-attr="4698" data-attr2="1" data-attr3="Husky">
                        <div class="item quiz" id="bodyshape_4698">
                           <div class="img-wrp">
                              <img class="" id="oignal_img4698" src="https://www.stylecracker.com/assets/seventeen/images/pa/male/shapes/husky.svg">
                              <img class="hide" id="selected_img4698" src="https://www.stylecracker.com/assets/seventeen/images/pa/male/shapes/husky-selected.svg">
                           </div>
                           <div class="title">Triangular</div>
                           <div class="desc">Wide shoulders and chest<br>Bulky arms<br>Wide waist</div>
                        </div>
                     </div>
                     <div class="col-md-2 col-sm-4 col-xs-6 item-col deactive" data-attr="4699" data-attr2="1" data-attr3="Heavy">
                        <div class="item quiz" id="bodyshape_4699">
                           <div class="img-wrp">
                              <img class="" id="oignal_img4699" src="https://www.stylecracker.com/assets/seventeen/images/pa/male/shapes/heavy.svg">
                              <img class="hide" id="selected_img4699" src="https://www.stylecracker.com/assets/seventeen/images/pa/male/shapes/heavy-selected.svg">
                           </div>
                           <div class="title">Heavy</div>
                           <div class="desc">Hefty shoulders and arms<br>Large chest<br>Wide waist</div>
                        </div>
                     </div>
                  </div>
               </div>
               <label id="body_shape_ans-error" class="message error hide hidden-sm hidden-xs" for="body_shape_ans">Please select body shape</label>
            </div>
      </div>
      <div class="men-apparel category-apparel hide"> <!-- hide -->
         <div class="men_apparel_beforepayment">
            <div id="question_1_2" class="container extrapad section-stuff-dont-like">
               <div class="pa-que">
                  <span>
                     What I  <i style="color: #ed1b2e;">Don’t</i> Want 
                     <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick upto 7)
                        </h1>
                     </div>
                     <!-- <i style="font-size:90%;">(Select 1 or more)</i> -->
                  </span>
               </div>
                <label id="stuffs_i_dont_need_ans-error" class="message error hide hidden-md hidden-lg" for="stuffs_i_dont_need_ans">Please select atleast one</label>
               <ul class="add-event list-colors list-dneed other-type stuff-dont-need actdeact" type="stuff-dont-need">
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/dwant/1tshirt.jpg" alt="T-SHIRTS">
                     </div>
                     <div class="title-3">t-shirts</div>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/dwant/1casual.jpg" alt="CASUAL SHIRTS">
                     </div>
                     <div class="title-3">casual shirts</div>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/dwant/1formal.jpg" alt="FORMAL SHIRTS">
                     </div>
                     <div class="title-3">formal shirts</div>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/dwant/1short.jpg" alt="SHORTS">
                     </div>
                     <div class="title-3">shorts</div>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/dwant/1jogger.jpg" alt="JOGGERS">
                     </div>
                     <div class="title-3">joggers</div>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/dwant/1jeans.jpg" alt="JEANS">
                     </div>
                     <div class="title-3">jeans</div>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/dwant/1chinos.jpg" alt="CHINOS">
                     </div>
                     <div class="title-3">chinos</div>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/dwant/1trouser.jpg" alt="TROUSERS">
                     </div>
                     <div class="title-3">trousers</div>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/dwant/1outerwear.jpg" alt="OUTERWEAR">
                     </div>
                     <div class="title-3">outerwear</div>
                  </li>
               </ul>
               <label id="stuffs_i_dont_need_ans-error" class="message error hide hidden-xs hidden-sm" for="stuffs_i_dont_need_ans">Please select atleast one</label>
            </div>
           
            <div  id="question_1_3" class="product-item section-size-tshirt">
                <div class="pa-que">
                  <span>Size And Fit</span>
                  <div class="title-section-changed text-center"  >
                     <h1 class="title-changed">
                        (Answer All)
                     </h1>
                  </div>
               </div>
              <!--  <div class="pa-que">
                  <span> T-shirt (Chest Size In Inches)</span>
                  <div class="title-section-changed text-center"  >
                     <h1 class="title-changed">
                        (Pick one)
                     </h1>
                  </div>
               </div> -->
               <div class="item-sizes-wrp" id="">
                  <div class="form-group">
                     <select class="form-control hwidth size-tshirt" type="size-tshirt" name="scbox_tshirt_size">
                        <option value="">Select t-shirt size</option>
                        <option value="xs/34-36 ">xs/ 34-36</option>
                        <option value="s/36.5-38 ">s/ 36.5-38</option>
                        <option value="m/38-40">m/ 38-40</option>
                        <option value="l/40-42">l/ 40-42</option>
                        <option value="xl/42-44">xl/ 42-44</option>
                        <option value="xxl/44-46">xxl/ 44-46</option>
                        <option value="xxxl/46-48">xxxl/ 46-48</option>
                     </select>
                  </div>
               </div>
               <label id="tshirt_size-error" class="message error hide" for="tshirt_size">Please select 
               t-shirt size
               </label>
            </div>
            <div  id="question_1_4" class="product-item section-size-shirt">
             <!--   <div class="pa-que"><span>SHIRT</span></div> -->
               <div class="item-sizes-wrp" id="">
                  <div class="form-group">
                     <select class="form-control hwidth size-shirt" type="size-shirt" name="shirt_size">
                        <option value="">Select shirt size</option>
                        <option value="xs/chest 34-36 / neck 15">xs/chest 34-36 / neck 15”</option>
                        <option value="s/chest 36.5-38 / neck 15.5">s/chest 36.5-38 / neck 15.5”</option>
                        <option value="m/chest 38-40/ neck 16">m/chest 38-40/ neck 16”</option>
                        <option value="l/chest 40-42/ neck 17">l/chest 40-42/ neck 17”</option>
                        <option value="xl/chest 42-44/ neck 17.5">xl/chest 42-44/ neck 17.5”</option>
                        <option value="xxl/chest 44-46/ neck 18.5">xxl/chest 44-46/ neck 18.5”</option>
                        <option value="xxxl/chest 46-48/ neck 19.5">xxxl/chest 46-48/ neck 19.5”</option>
                     </select>
                  </div>
               </div>
               <label id="shirt_size-error" class="message error hide" for="shirt_size">Please select 
               shirt size
               </label>
            </div>
            <div  id="question_1_5" class="product-item section-trouser-size">
               <!-- <div class="pa-que"><span>
                  Trouser</span>
               </div> -->
               <div class="item-sizes-wrp" id="">
                  <div class="form-group">
                     <select class="form-control hwidth trouser-size" type="trouser-size" name="scbox_sizetrouser">
                        <option value="">select trouser size</option>
                        <option value="xs/ waist inches - 28">xs/ waist inches - 28</option>
                        <option value="s/ waist inches - 30">s/ waist inches - 30</option>
                        <option value="m/ waist inches - 32">m/ waist inches - 32</option>
                        <option value="l/ waist inches - 34">l/ waist inches - 34</option>
                        <option value="xl/ waist inches - 36">xl/ waist inches - 36</option>
                        <option value="xxl/ waist inches - 38">xxl/ waist inches - 38</option>
                     </select>
                  </div>
               </div>
               <label id="sizetrouser-error" class="message error hide" for="sizetrouser">Please select trouser size</label>
            </div>
            <div  id="question_1_6" class="product-item section-jeans-size">
              <!--  <div class="pa-que"><span>
                  Jeans</span>
               </div> -->
               <div class="item-sizes-wrp" id="">
                  <div class="form-group">
                     <select class="form-control hwidth jeans-size" type="jeans-size" name="scbox_denim_size">
                        <option value="">Select jeans size</option>
                        <option value="xs/ waist inches - 28">xs/ waist inches - 28</option>
                        <option value="s/ waist inches - 30">s/ waist inches - 30</option>
                        <option value="m/ waist inches - 32">m/ waist inches - 32</option>
                        <option value="l/ waist inches - 34">l/ waist inches - 34</option>
                        <option value="xl/ waist inches - 36">xl/ waist inches - 36</option>
                        <option value="xxl/ waist inches - 38">xxl/ waist inches - 38</option>
                     </select>
                  </div>
               </div>
               <label id="denim_size-error" class="message error hide" for="denim_size">Please select jeans size</label>
            </div>
            <div id="question_1_7" class="container extrapad section-top-fit-like-this-men">
               <div class="box-pa">
                  <div class="pa-que">
                     <span>
                        I Like My T-shirt To Fit Like This  
                        <!--  <i style="font-size:90%;">(Select 1 or more)</i> -->
                     </span>
                      <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                  </div>
               </div>
               <label id="tshirtfit_ans-error" class="message error hide hidden-lg hidden-md" for="tshirtfit_ans">Please select t-shirt fit</label>
               <ul class="add-event list-colors fit-you-like-men actdeact" type="fit-you-like-men">
                  <li >
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/tshirts/1slim.jpg" alt="FITTED">
                     </div>
                     <div class="title-3">slim</div>
                  </li>
                  <li >
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/tshirts/1regular.jpg" alt="RELAXED">
                     </div>
                     <div class="title-3">regular</div>
                  </li>
               </ul>
               <label id="tshirtfit_ans-error" class="message error hide hidden-sm hidden-xs" for="tshirtfit_ans">Please select t-shirt fit</label>
            </div>
            <div  id="question_1_8" class="container extrapad section-fit-you-like-men-s">
               <div class="box-pa">
                  <div class="pa-que">
                     <span>
                        I Like My Shirt To Fit Like This
                        <!--  <i style="font-size:90%;">(Select 1 or more)</i> -->
                     </span>
                      <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                  </div>
               </div>
               <label id="shirtfit_ans-error" class="message error hide hidden-md hidden-lg" for="shirtfit_ans">Please select shirt fit</label>
               <ul class="add-event list-colors fit-you-like-men-s actdeact" type="fit-you-like-men-s">
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/shirts/1slim.jpg" alt="SLIM">
                     </div>
                     <div class="title-3">slim</div>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/shirts/1regular.jpg" alt="REGULAR">
                     </div>
                     <div class="title-3">regular</div>
                  </li>
               </ul>
               <label id="shirtfit_ans-error" class="message error hide hidden-xs hidden-sm" for="shirtfit_ans">Please select shirt fit</label>
            </div>
            <div  id="question_1_9" class="container section-fit-you-like-men-t">
               <div class="box-pa">
                  <div class="pa-que">
                     <span>
                        I Like My Trouser To Fit Like This  
                        <!--  <i style="font-size:90%;">(Select 1 or more)</i> -->
                     </span>
                      <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                  </div>
               </div>
               <label id="trouserfit_ans-error" class="message error hide hidden-md hidden-lg" for="trouserfit_ans">Please select trouser fit</label>
               <ul class="add-event list-colors actdeact fit-you-like-men-t" type="fit-you-like-men-t">
                  <li >
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/trouser/1skinny.jpg" alt="FITTED">
                     </div>
                     <div class="title-3">skinny</div>
                  </li>
                  <li >
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/trouser/1slim.jpg" alt="STRAIGHT">
                     </div>
                     <div class="title-3">slim</div>
                  </li>
                  <li >
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/trouser/1regular.jpg" alt="LOOSE">
                     </div>
                     <div class="title-3">regular</div>
                  </li>
               </ul>
               <label id="trouserfit_ans-error" class="message error hide hidden-xs hidden-sm" for="trouserfit_ans">Please select trouser fit</label>
            </div>
            <div  id="question_1_10" class="container section-fit-you-like-men-j">
               <div class="box-pa">
                  <div class="pa-que">
                     <span>
                        I Like My Jeans To Fit Like This
                        <!--  <i style="font-size:90%;">(Select 1 or more)</i> -->
                     </span>
                      <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                  </div>
               </div>
               .<label id="jeansfit_ans-error" class="message error hide hidden-md hidden-lg" for="jeansfit_ans">Please select jeans fit</label>
               <ul class="add-event list-colors actdeact fit-you-like-men-j" type="fit-you-like-men-j">
                  <li >
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/jeans/1skinny.jpg" alt="SKINNY">
                     </div>
                     <div class="title-3">skinny</div>
                  </li>
                  <li >
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/jeans/1slim.jpg" alt="SLIM">
                     </div>
                     <div class="title-3">slim</div>
                  </li>
                  <li >
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/jeans/1straight.jpg" alt="REGULAR">
                     </div>
                     <div class="title-3">regular</div>
                  </li>
               </ul>
               <label id="jeansfit_ans-error" class="message error hide hidden-sm hidden-xs" for="jeansfit_ans">Please select jeans fit</label>
            </div>
            <div id="question_1_11" class="container extrapad section-dos-and-donts">
                  <div class="pa-que">
                     <span>My Do's and <i style="color: #ed1b2e;">Don'ts</i>
                     </span>
                     <div class="title-section-changed text-center"  >
                         <h1 class="title-changed">
                            (Pick Yes Or No For Each Style)
                         </h1>
                      </div>
                  </div>
                  <label id="stuffmen-error" class="message error hide hidden-lg hidden-md" for="stuffmen">Please select a choice</label>
               <ul class="list-colors other-type bigimg">
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/dos/1round.jpg" alt="ROUND NECK">
                     </div>
                     <div class="title-3">round neck</div>
                     <ul class="add-event list-occassion lespad dos-and-donts check other-type" type="dos-and-donts" style="round neck">
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>yes</span></label>
                        </li>
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>no</span></label>
                        </li>
                     </ul>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/dos/1vneck.jpg" alt="V NECK">
                     </div>
                     <div class="title-3">v neck</div>
                     <ul class="add-event list-occassion lespad dos-and-donts check other-type" type="dos-and-donts" style="v neck"  id="question_1_15">
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>yes</span></label>
                        </li>
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>no</span></label>
                        </li>
                     </ul>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/dos/1henley.jpg" alt="HENLEYS">
                     </div>
                     <div class="title-3">henleys</div>
                     <ul class="add-event list-occassion lespad dos-and-donts check other-type" type="dos-and-donts" style="henleys" id="question_1_16">
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>yes</span></label>
                        </li>
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>no</span></label>
                        </li>
                     </ul>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/dos/1collar.jpg" alt="COLLARED T-SHIRT">
                     </div>
                     <div class="title-3">collared t-shirt</div>
                     <ul class="add-event list-occassion lespad dos-and-donts check other-type" type="dos-and-donts" style="collared">
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>yes</span></label>
                        </li>
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>no</span></label>
                        </li>
                     </ul>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/dos/1half.jpg" alt="HALF SLEEVES SHIRTS">
                     </div>
                     <div class="title-3">half sleeves shirts</div>
                     <ul class="add-event list-occassion lespad dos-and-donts check other-type" type="dos-and-donts" style="half sleeves">
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>yes</span></label>
                        </li>
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>no</span></label>
                        </li>
                     </ul>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/dos/1full.jpg" alt="FULL SLEEVES SHIRTS ">
                     </div>
                     <div class="title-3">full sleeves shirts</div>
                     <ul class="add-event list-occassion lespad dos-and-donts check other-type" type="dos-and-donts" style="full sleeves">
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>yes</span></label>
                        </li>
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>no</span></label>
                        </li>
                     </ul>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/dos/1jacket.jpg" alt="JACKETS">
                     </div>
                     <div class="title-3">jackets</div>
                     <ul class="add-event list-occassion lespad dos-and-donts check other-type" type="dos-and-donts" style="jackets">
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>yes</span></label>
                        </li>
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                           <input type="checkbox"> <span>no</span></label>
                        </li>
                     </ul>
                  </li>
               </ul>
               <label id="stuffmen-error" class="message error hide hidden-sm hidden-xs" for="stuffmen">Please select a choice</label>
            </div>
            <div  id="question_1_12" class="container section-style-preference">
               <div class="box-pa">
                  <div class="pa-que">
                     <span>My Style Preference</span>
                     <div class="title-section-changed text-center"  >
                         <h1 class="title-changed">
                            (Pick Love, Like or Dislike For Each Style)
                         </h1>
                      </div>
                  </div>
               </div>
               <label id="style-men-error" class="message error hide hidden-lg hidden-md" for="style-men">Please select a choice</label>
               <ul class="add-event list-colors other-type bigimg">
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/preference/casual.jpg" alt="CASUAL">
                     </div>
                     <div class="title-3">casual</div>

                     <ul class="add-event list-occassion lespad style-preference other-type" type="style-preference" style="casual">
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox" action="love"><i class="icon icon-heart"></i>
                           <input type="checkbox">
                           </label>
                        </li>
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox" action="like"><i class="icon icon-like"></i>
                           <input type="checkbox">
                           </label>
                        </li>
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox" action="dislike"><i class="icon icon-dislike"></i>
                           <input type="checkbox">
                           </label>
                        </li>
                     </ul>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/preference/2atleisure.jpg" alt="ATHLEISURE">
                     </div>
                     <div class="title-3">athleisure</div>
                     <ul class="add-event list-occassion lespad style-preference other-type" type="style-preference" style="athleisure">
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox" action="love"><i class="icon icon-heart"></i>
                           <input type="checkbox">
                           </label>
                        </li>
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox" action="like"><i class="icon icon-like"></i>
                           <input type="checkbox">
                           </label>
                        </li>
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox" action="dislike"><i class="icon icon-dislike"></i>
                           <input type="checkbox">
                           </label>
                        </li>
                     </ul>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/preference/1bold.jpg" alt="BOLD">
                     </div>
                    <div class="title-3">bold</div>
                     <ul class="add-event list-occassion lespad style-preference other-type" type="style-preference" style="bold">
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox" action="love"><i class="icon icon-heart"></i>
                           <input type="checkbox">
                           </label>
                        </li>
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox" action="like"><i class="icon icon-like"></i>
                           <input type="checkbox">
                           </label>
                        </li>
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox" action="dislike"><i class="icon icon-dislike"></i>
                           <input type="checkbox">
                           </label>
                        </li>
                     </ul>
                  </li>
                 
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/preference/2well.jpg" alt="WELL GROOMED">
                     </div>
                     <div class="title-3">well groomed</div>
                     <ul class="add-event list-occassion lespad style-preference other-type" type="style-preference" style="well groomed">
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox" action="love"><i class="icon icon-heart"></i>
                           <input type="checkbox">
                           </label>
                        </li>
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox" action="like"><i class="icon icon-like"></i>
                           <input type="checkbox">
                           </label>
                        </li>
                        <li class="noborder" style="border:none !important;">
                           <label class="sc-checkbox" action="dislike"><i class="icon icon-dislike"></i>
                           <input type="checkbox">
                           </label>
                        </li>
                     </ul>
                  </li>
               </ul>
               <label id="style-men-error" class="message error hide hidden-sm hidden-xs" for="style-men">Please select a choice</label>
            </div>
            <div  id="question_1_13" class="container section-color-avoid">
               <div class="box-pa">
                  <div class="pa-que">
                     <span>
                        Colors I <i style="color: #ed1b2e;">Avoid </i>
                        <!--  <i style="font-size:90%;">(Select 1 or more)</i> -->
                     </span>
                      <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                  </div>
               </div>
               <label id="colormenbefore-error" class="message error hide hidden-lg hidden-md" for="colormenbefore">Please select a color</label>
               <ul class="add-event list-skin-tone list-colordneed actdeact color-to-avoid-afterpayment color-avoid" type="color-avoid">
                  <li>
                     <div class="pallet" color="#8bf5cd" style="background-color:#8bf5cd;border:1px solid #8bf5cd;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#89aad5" style="background-color:#89aad5;border:1px solid #89aad5;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#bf94e4" style="background-color:#bf94e4;border:1px solid #bf94e4;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#aeb0ae" style="background-color:#aeb0ae;border:1px solid #aeb0ae;"></div>
                  </li>
                   <li>
                     <div class="pallet" color="#734021" style="background-color:#734021;border:1px solid #734021;"></div>
                  </li>
                   <li>
                     <div class="pallet" color="#2082ef" style="background-color:  #2082ef;border:1px solid #2082ef;"></div>
                  </li>
                   <li>
                     <div class="pallet" color="#0b0706" style="background-color:#0b0706;border:1px solid #0b0706;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#68dbd6"
                        style="background-color:  #68dbd6;border:1px solid #68dbd6;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#f5dc05" style="background-color:#f5dc05;border:1px solid #f5dc05;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#fffefe" style="background-color:#fffefe;border:1px solid #fffefe;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#0c4c8a" style="background-color:#0c4c8a;border:1px solid #0c4c8a;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#576331" style="background-color:#576331;border:1px solid #576331;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#041531" style="background-color:#041531;border:1px solid #041531;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#de443a" style="background-color:#de443a;border:1px solid #de443a;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#bde4f5"
                        style="background-color:#bde4f5;border:1px solid #bde4f5;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#ffbbc7"
                        style="background-color:#ffbbc7;border:1px solid #ffbbc7;"></div>
                  </li>
                  <li>
                     <div class="pallet" color="#0a6148"
                        style="background-color:#0a6148;border:1px solid #0a6148;"></div>
                  </li>
                 
                  <br>
                      <li class="sc-checkbox" style="width:220px !important;">
                         <div class="title-3" color="I like all colors">
                            <i class="icon icon-diamond"></i>I like all colors
                               <input type="checkbox">
                         </div>
                      </li> 
                  
               </ul>
              
               <label id="colormenbefore-error" class="message error hide hidden-xs hidden-sm" for="colormenbefore">Please select a color</label>
            </div>
            <div  id="question_1_14" class="container extrapad section-print-avoid-men">
               <div class="box-pa">
                  <div class="pa-que">
                     <span>
                        Prints and Patterns I <i style="color: #ed1b2e;">Avoid </i>
                        <!--  <i style="font-size:90%;">(Select 1 or more)</i> -->
                     </span>
                      <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                  </div>
               </div>
               <label id="printbeforemen-error" class="message error hide hidden-lg hidden-md" for="printbeforemen">Please select a print</label>
               <ul class="add-event list-colors list-prints actdeact other-type print-avoid-men" type="print-avoid-men">
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/prints/polka.jpg" alt="Polka dots">
                     </div>
                     <div class="title-3">polka dots</div>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="./images/pa/male/prints/1floral.jpg" alt="Floral">
                     </div>
                     <div class="title-3">floral</div>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/prints/stripes.jpg" alt="Stripes">
                     </div>
                     <div class="title-3">stripes</div>
                  </li>
                
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/prints/1quirky.jpg" alt="QUIRKY">
                     </div>
                     <div class="title-3">quirky</div>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="./images/pa/male/prints/1check.jpg" alt="Checks">
                     </div>
                     <div class="title-3">checks</div>
                  </li>
                 
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/prints/1hound.jpg" alt="HOUNDSTOOTH">
                     </div>
                     <div class="title-3">houndstooth</div>
                  </li>
                  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url(); ?>assets/images/pa/female/style/prints/solid.jpg" alt="Solid">
                     </div>
                     <div class="title-3">solid</div>
                  </li>
               </ul>
               <label id="printbeforemen-error" class="message error hide hidden-sm hidden-xs" for="printbeforemen">Please select a print</label>
            </div>
            <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <div class="title-3">
                              <label> Comment Box</label>
                           </div>
                           <div class="form-group-md">
                              <textarea type="text" placeholder="" id="men_commentbox_2" ></textarea>
                              <label>IS THERE ANYTHING WE MISSED ? </label>
                           </div>
                        </div>
                        <div class="col-md-3"></div>
                     </div>
         </div>
      </div>   
</section>
<section class="section section-shadow">
         <div class="btns-wrp">
            <a class="scbtn scbtn-primary" href="javascript:void(0);" onclick="ChangeStep(1,'previous');"><span>Previous</span></a>
            <a class="scbtn scbtn-primary btn-next-1 link-scrolling" href="javascript:void(0);" onclick="ChangeStep(3,'next');"><span>Next</span></a>
            <!-- <button class="scbtn scbtn-primary" onclick="slideprev(4);"><span>Previous</span></button>
               <button class="scbtn scbtn-primary"  onclick="slidenext(4);"><span>Next</span></button> -->
         </div>
      </section>
</div>    

    <div id="slide_3" class="hide">
      <section class="section section-shadow ">
         <!--menbags-->
         <div class="men_bags category-bags">
            <div class="men_bags_beforepayment">
               <div id="question_2_1" class="container section-bag-type">
                  <div class="box-pa">
                     <div class="pa-que">
                        <span>
                           The Kind Of Bag I Am Looking For 
                           <!-- <i style="font-size:90%;">(Select 1 or more)</i> -->
                        </span>
                     </div>
                      <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                  </div>
                   <label id="bag_ans-error" class="message error hide hidden-lg hidden-md" for="bag_ans">Please select bag</label>
                  <ul class="add-event list-colors bag-type" type="bag-type">
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/male/bags/1wallet.jpg" alt="WALLETS">
                        </div>
                        <div class="title-3">wallets</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/male/bags/1messenger.jpg" alt="MESSENGER BAGS ">
                        </div>
                        <div class="title-3">messenger bags</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/male/bags/1laptop.jpg" alt="LAPTOP  BAGS">
                        </div>
                        <div class="title-3">laptop bags</div>
                     </li>
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/male/bags/1backpack.jpg" alt="BACKPACKS">
                        </div>
                        <div class="title-3">backpacks</div>
                     </li>
                  </ul>
               </div>
               <label id="bag_ans-error" class="message error hide hidden-sm hidden-xs" for="bag_ans">Please select bag</label>
            </div>
         </div>
         <!--menbags-->       
         <!-- FOOTWEAR -->
         <!--menfootwear-->
         <div class="men_footwear category-footwear hide"><!-- hide -->
            <div class="men_footwear_beforepayment">
               <div id="question_3_1" class="product-item section-footwear-size">
                  <div class="pa-que">
                     <span>Footwear</span>
                     <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick one)
                        </h1>
                     </div>
                  </div>
                  <div class="item-sizes-wrp" id="foot-size_m">
                     <div class="form-group form-element">
                        <select class="form-control hwidth footwear-size" type="footwear-size">
                           <option value="">Select footwear size</option>
                           <option value="eu 38/uk 5/us 6">eu 38/uk 5/us 6</option>
                           <option value="eu 40/uk 6/us 7">eu 40/uk 6/us 7</option>
                           <option value="eu 41/uk 7/us 8">eu 41/uk 7/us 8</option>
                           <option value="42.5/uk 8/us 9">42.5/uk 8/us 9</option>
                           <option value="44/uk 9/us 10">44/uk 9/us 10</option>
                           <option value="45/uk 10/ us 11">45/uk 10/ us 11</option>
                           <option value="46/uk 11/ us 12">46/uk 11/ us 12</option>
                           <option value="47.5/uk 12/ us 13">47.5/uk 12/ us 13</option>
                        </select>
                     </div>
                  </div>
                  <label id="foot-size_m-error" class="message error hide" for="foot-size_m">Please select footwear size</label>
               </div>
               <div id="question_3_2" class="container section-footwear-type-looking-for">
                  <div class="pa-que">
                     <span>
                        The Type Of Shoes I Am Looking For <!--  <i style="font-size:90%;">(Select 1 or more)</i> --> 
                     </span>
                      <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                  </div>

                   <label id="shoe_ans-error" class="message error hide hidden-lg hidden-md" for="shoe_ans">Please select footwear</label>
                  <ul class="add-event list-colors footwear-type" type="footwear-type">
                     <li>
                        <div class="pallet">
                           <img src="<?php echo base_url(); ?>assets/images/pa/male/shoe/1flip.jpg">
                        </div>
                        <div class="title-3" style="font-size: 12px;">flip flops  & sandals</div>
                     </li>
                     <li>
                        <div class="pallet" >
                           <img src="<?php echo base_url(); ?>assets/images/pa/male/shoe/1sneaker.jpg">
                        </div>
                        <div class="title-3">sneakers</div>
                     </li>
                     <li>
                        <div class="pallet" >
                           <img src="<?php echo base_url(); ?>assets/images/pa/male/shoe/1casual.jpg">
                        </div>
                        <div class="title-3">casual shoes</div>
                     </li>
                     <li>
                        <div class="pallet" >
                           <img src="<?php echo base_url(); ?>assets/images/pa/male/shoe/1loafer.jpg">
                        </div>
                        <div class="title-3">loafers</div>
                     </li>
                     <li>
                        <div class="pallet" >
                           <img src="<?php echo base_url(); ?>assets/images/pa/male/shoe/1formal.jpg">
                        </div>
                        <div class="title-3">formal shoes</div>
                     </li>
                  </ul>
                  <label id="shoe_ans-error" class="message error hide hidden-xs hidden-sm" for="shoe_ans">Please select footwear</label>
               </div>
            </div>
         </div>
         <!--menfootwear-->
         <!--men accessories-->
         <div class="men_accessories category-accessories hide"><!-- hide -->
            <div class="men_accessories_beforepayment">
               <div id="question_4_1" class="product-item extrapad section-accessories-types">
                  <div class="pa-que">
                     <span>
                     Accessories</span>
                     <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick one)
                        </h1>
                     </div>
                  </div>
                  <ul class="list-occassion accessories-selected" type="accessories-selected">
                     <li data-attr="Casuals">
                        <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                        <input type="checkbox"> <span>sunglasses</span></label>
                     </li>
                     <li data-attr="Formal">
                        <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                        <input type="checkbox"> <span>belts</span></label>
                     </li>
                     <li data-attr="Party">
                        <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                        <input type="checkbox"> <span>caps</span></label>
                     </li>
                     <li data-attr="Bridal">
                        <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                        <input type="checkbox"> <span>pocket square</span></label>
                     </li>
                     <li data-attr="Bridal">
                        <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                        <input type="checkbox"> <span>ties and bow ties</span></label>
                     </li>
                     <li data-attr="Bridal">
                        <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                        <input type="checkbox"> <span>lapel and collar pins</span></label>
                     </li>
                     <li data-attr="Bridal">
                        <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                        <input type="checkbox"> <span>cuff links</span></label>
                     </li>
                     <li data-attr="Bridal">
                        <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                        <input type="checkbox"> <span>jewellery</span></label>
                     </li>
                  </ul>
                  <label id="accesories_ans-error" class="message error hide" for="accessories_ans">Please select an accessory</label>
               </div>
            </div>
         </div>
        <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <div class="title-3">
                              <label> Comment Box</label>
                           </div>
                           <div class="form-group-md">
                              <textarea type="text" placeholder="" id="men_commentbox_1" ></textarea>
                              <label>IS THERE ANYTHING WE MISSED ? </label>
                           </div>
                        </div>
                        <div class="col-md-3"></div>
                     </div>
         <!--menaccessories-->
      </section>
      <section class="section section-shadow">
         <div class="btns-wrp">
            <a class="scbtn scbtn-primary" href="javascript:void(0);" onclick="ChangeStep(2,'previous');"><span>Previous</span></a>
            <a class="scbtn scbtn-primary btn-next-2 link-scrolling" href="javascript:void(0);" onclick="ChangeStep(4,'next');"><span>Next</span></a>
            <!-- <button class="scbtn scbtn-primary" onclick="slideprev(4);"><span>Previous</span></button>
               <button class="scbtn scbtn-primary"  onclick="slidenext(4);"><span>Next</span></button> -->
         </div>
      </section>
   </div>


   </div>
<style>
   .scbox-payment-options li {
   display: inline-block;
   margin-right: 10px;
   }
   .scbox-payment-options .sc-checkbox .icon {
   position: relative;
   top: 10px !important;
   }

</style>

<script>
   document.addEventListener("DOMContentLoaded", function(event) { 
   
       msg("LOADED STEP 2ddddd...... page");
   
       //## Get Box Package function initiated -------------
       var strBoxPackage = getCookie('scbox_objectid');
                if(strBoxPackage == 1){
                   $('.list-categories.pa-men li').eq(2).hide();
                }
                else{
                  msg('show footwear');
                  $('.list-categories.pa-men li').eq(2).show();
                }
                msg($('.list-categories.pa-men li').eq(2));
       GET_BoxPackageID();
   
   });
   
   
</script>