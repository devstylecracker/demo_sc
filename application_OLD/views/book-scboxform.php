<?php  
//ARRAY_FILTER_USE_BOTH = 1;
$username_ = ''; 
$email_ = ''; 
$gender_ = ''; 
$mobileno_ = ''; 

if(!empty(@$_GET['b']) && !empty(@$_GET['g']))
{
  setcookie('scbox_objectid',trim(@$_GET['b']),time() + 86400, "/"); 
  $scboxprice = $this->Pa_model->get_scboxdata(trim(@$_GET['b']),'id')->price;
  if($scboxprice!='')
  {
    setcookie('scbox_price',$scboxprice,time() + 86400, "/"); 
  }

  if($_GET['g']==1)
  {
    setcookie('scbox-gender','women',time() + 86400, "/"); 
  }else
  {
    setcookie('scbox-gender','men',time() + 86400, "/"); 
  }  
}

  if((@$this->session->userdata('first_name')!='') || (@$this->session->userdata('last_name')!=''))
  {
    $username_ = $this->session->userdata('first_name').' '.$this->session->userdata('last_name');   
  }    
  $email_ = $this->session->userdata('email');
  $gender_ = $this->session->userdata('gender');
  $mobileno_ = $this->session->userdata('contact_no');

$objectid = @$_COOKIE['scbox_objectid'];
$user_dob_year = '';
$user_dob_month = '';
$user_dob_day = '';
if(!empty($userdata['user_dob']))
{
  $user_dob = $userdata['user_dob'];
  $user_dob_arr = explode('-', $user_dob);
  $user_dob_year = $user_dob_arr[0];
  $user_dob_month = $user_dob_arr[1];
  $user_dob_day = $user_dob_arr[2];
}
?>
<style>

/*select{
    
    width: 30% !important;
}*/

fieldset.date { 
  margin: 0; 
  padding-top: 10px;  
  display: block; 
  border: none; 
} 

fieldset.date legend { 
  margin: 0; 
  padding: 0; 
  margin-top: .25em; 
  font-size: 100%; 
} 


fieldset.date label { 
  position: absolute; 
  top: -20em; 
  left: -200em; 
} 

fieldset.date select { 
margin: 0; 
padding: 0; 
font-size: 100%; 
display: inline; 
width: 30%;
} 

span.inst { 
  font-size: 75%; 
  color: #13D792; 
  padding-left: .25em; 
  text-transform: uppercase;
} 


fieldset.date select:active, 
fieldset.date select:focus, 
fieldset.date select:hover 
{ 
  border-color: gray; 
  
} 

.form-control {
    display: inline !important;
    }
 
.form-group-md select.invalid ~ label.error{
      color: #d50000;
    font-size: 12px;
    font-weight: 500;
    text-transform: none;
    position: absolute;
    pointer-events: none;
    width: 100%;
    margin: 0;
    left: 0px;
    bottom: 2px;
    -moz-transition: 0.2s ease all;
    -webkit-transition: 0.2s ease all;
    -o-transition: 0.2s ease all;
    -ms-transition: 0.2s ease all;
    transition: 0.2s ease all;
    
}
}
 
input[type=radio] {

    display: none !important;
} 

.title-section-changed .title-changed{
      font-size: 30px;
    font-weight: 200;
    margin: 0;
    line-height: 1;
    color: #333;
    display: inline-block;
}

@media (max-width: 768px)
{
.title-section-changed .title-changed {
    font-size: 26px;
    line-height: 1.3;
}
}

</style>
<div class="page-scbox-form">
   <!-- action="<?php //echo base_url(); ?>book-scbox/<?php //echo base_url('scbox/step1'); ?>"-->
   <form name="scbox-form" id="scbox-form" method="post" name="user-frm" action="" >
      <input type='hidden' id="utm_source" name="utm_source" value="<?php echo @$_GET['utm_source']; ?>" />
      <input type='hidden' id="utm_medium" name="utm_medium" value="<?php echo @$_GET['utm_medium']; ?>" />
      <input type='hidden' id="utm_campaign" name="utm_campaign" value="<?php echo @$_GET['utm_campaign']; ?>" />
      <input type='hidden' id="scbox_package" name="scbox_package" value="<?php echo @$userdata['scbox_pack'];?>" />
      <input type='hidden' id="scbox_price" name="scbox_price" value="<?php echo @$_COOKIE['scbox_price'];?>" />
      <input type='hidden' id="scbox_userid" name="scbox_userid" value="<?php echo @$userid;?>" />
      <input type='hidden' id="scbox_objectid" name="scbox_objectid" value="<?php echo @$objectid!='' ? $objectid : @$userdata['scbox_objectid'];?>" />
      <input type='hidden' id="scbox_productid" name="scbox_productid" value="<?php echo @$userdata['scbox_productid'];?>" />
      <input type='hidden' id="scbox_sessiongender" name="scbox_sessiongender" value="<?php echo @$this->session->userdata('gender');?>" />
      <input type='hidden' id="scbox_slide" name="scbox_slide" value="<?php echo @$_GET['step'];?>" />
      <!-- <form name="scbox-form1" id="scbox-form1" method="post" > -->
      <div id="slide_1" class="">
         <section class="section section-shadow">
            <div class="container">
               <div class="box-step-1">
                  <div class="title-section-changed text-center">
                     <h1 class="title-changed">
                        Help us know you better<br>
                        <p style="font-size: 18px;margin-top: 12px;">So we can curate the best fashion for you</p>
                     </h1>
                  </div>
                  <div class="box-title">
                     <ul class="add-event forme " type="forme" style="display: inline-flex;" >
                        <li style="width: 150px; cursor: pointer;text-transform: capitalize;">
                           <label for="chkNo" class="sc-checkbox recipient_add active">
                           <input type="radio" id="chkNo" name="chkrecipient_add" onclick="ShowHideDiv()" style="display: none;" checked/>
                           <i class="icon icon-diamond"></i><span> For me</span>
                           </label>   
                        </li>
                        <li style="width: 150px; cursor: pointer;text-transform: capitalize;"> <label for="chkYes" class="sc-checkbox recipient_add">
                           <input type="radio" id="chkYes" name="chkrecipient_add" onclick="ShowHideDiv()" style="display: none;"/>
                           <i class="icon icon-diamond"></i><span> This is a Gift</span>
                           </label>
                        </li>
                     </ul>
                  </div>
                  <div class="row">
                     <div class="title-section-changed text-center" >
                        <h1 class="title-changed" style="font-weight:normal;font-size:18px;letter-spacing: 2px;font-style:italic;"> About me: </span></h1>
                     </div>
                     <h2 style="font-weight:normal;font-size:15px;text-transform: uppercase;
                        letter-spacing: 2px;color: #9c9c9c;">
                     </h2>
                     <div class="gender-wrp-outer">
                        <ul class="gender-wrp " id="logged-gender" >
                          
                        </ul>
                       
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group-md">
                           <input type="text" name="scbox_name" id="scbox_name" required value="" onblur="validateform(this);" maxlength="250"/>
                           <label>Full Name <span class="star">*</span></label>
                           <label id="scbox_name-error" class="error" for="scbox_name">Please enter your first and last name</label>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group-md">
                           <input type="hidden" name="scbox_age" id="scbox_age" value="" >
                           <fieldset class="date">
                              <label for="day_start"></label> 
                              <select id="day_start" name="day_start" onchange="validateform(this);" >
								<option value="" >Day</option>  
								  <?php for($i=1;$i<32;$i++){ ?>
									<option value="<?php echo $i; ?>" <?php echo ($user_dob_day==$i ) ? 'selected' : ''; ?> ><?php echo $i; ?></option> 
								  <?php } ?>                     
							  </select>
                              - <label for="month_start"></label> 
                              <select id="month_start" name="month_start" onchange="validateform(this);" >
								<option value="" >Month</option>    
								<option value="1" <?php echo ($user_dob_month==1 ) ? 'selected' : ''; ?> >January</option>       
								<option value="2" <?php echo ($user_dob_month==2 ) ? 'selected' : ''; ?> >February</option>       
								<option value="3" <?php echo ($user_dob_month==3 ) ? 'selected' : ''; ?> >March</option>       
								<option value="4" <?php echo ($user_dob_month==4 ) ? 'selected' : ''; ?> >April</option>       
								<option value="5" <?php echo ($user_dob_month==5 ) ? 'selected' : ''; ?> >May</option>       
								<option value="6" <?php echo ($user_dob_month==6 ) ? 'selected' : ''; ?> >June</option>       
								<option value="7" <?php echo ($user_dob_month==7 ) ? 'selected' : ''; ?> >July</option>       
								<option value="8" <?php echo ($user_dob_month==8 ) ? 'selected' : ''; ?> >August</option>       
								<option value="9" <?php echo ($user_dob_month==9 ) ? 'selected' : ''; ?> >September</option>       
								<option value="10" <?php echo ($user_dob_month==10 ) ? 'selected' : ''; ?> >October</option>       
								<option value="11" <?php echo ($user_dob_month==11 ) ? 'selected' : ''; ?> >November</option>       
								<option value="12" <?php echo ($user_dob_month==12 ) ? 'selected' : ''; ?> >December</option>      
							  </select>
                              -
                              <label for="year_start"></label> 
                               <select id="year_start" name="year_start" onchange="validateform(this);" >
								<option value="" >Year</option>  
							  <?php for($i=date("Y");$i>=1947;$i--){ ?>
								<option value="<?php echo $i; ?>" <?php echo ($user_dob_year==$i ) ? 'selected' : ''; ?> ><?php echo $i; ?></option> 
							  <?php } ?>
							  </select> 
                              <span class="inst">Date of Birth*</span>  
                           </fieldset>
                           <label id="scbox_billage-error" class="error hide111" for="scbox_billage">Please enter your date of birth</label>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group-md">
                           <input type="email" placeholder="" name="scbox_emailid" id="scbox_emailid" required value="" onblur="validateform(this);" />
                           <label>Email  <span class="star">*</span></label>
                           <label id="scbox_emailid-error" class="error" for="scbox_emailid">Please enter your e-mail ID</label>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group-md">
                           <input type="tel" placeholder="" name="scbox_mobile" id="scbox_mobile"   required value="" onblur="validateform(this);" maxlength="10" onkeypress="return isNumber(event);" />
                           <label>Mobile <span class="star">*</span></label>
                           <label id="scbox_mobile-error" class="error" for="scbox_mobile">Please enter your mobile number</label>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group-md">
                           <input type="tel" placeholder="" name="scbox_pincode" id="scbox_pincode"  required value=""  minlength="6" maxlength="6" onblur="validateform(this);" onkeypress="return isNumber(event);" />
                           <label>Pincode <span class="star">*</span></label>
                           <label id="scbox_pincode-error" class="error" for="scbox_pincode">Please enter pincode</label>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group-md">
                           <input type="text" placeholder="" name="scbox_city" id="scbox_city" value="" onblur="validateform(this);" style="color: black;" disabled />
                           <label>City <span class="star">*</span></label>
                           <label id="scbox_city-error" class="error" for="scbox_city">Please enter your city</label>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group-md">
                           <!--<select name="scbox_state" id="scbox_state" onblur="validateform(this);" disabled >
                              <option value="">Select State</option>
                           </select>-->
						   <input type="text" placeholder="" name="scbox_state" id="scbox_state" value="" onblur="validateform(this);" style="color: black;" disabled />
                           <label>State <span class="star">*</span></label>
                           <label id="scbox_state-error" class="error" for="scbox_state">Please select state</label>
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="form-group-md">
                           <select name="scbox_profession" id="scbox_profession" onblur="validateform(this);" onchange="ShowHideprofession();">
                              <option value="">Select</option>
                              <option value="corporate">Corporate</option>
                              <option value="entertainment">Entertainment</option>
                              <option value="entrepreneur">Entrepreneur</option>
                              <option value="fashion">Fashion</option>
                              <option value="homemaker">Homemaker</option>
                              <option value="professional">Professional</option>
                              <option value="student">Student</option>
                              <option value="other">Other</option>
                           </select>
                           <label>Profession <span class="star">*</span></label>
                           <label id="scbox_profession-error" class="error" for="scbox_profession">Please select profession</label>
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-12 col-xs-12">
                        <div  id="dvProfession" class="form-group-md">
                           <input type="text" placeholder="" id="txtprofession" name="txtprofession" required onblur="validateform(this);" style="padding: 9px 0 9px 0;" />
                           <label>Enter Profession<span class="star">*</span></label>
                           <label id="txtprofession-error" class="error" for="txtprofession">Please enter profession</label>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group-md">
                           <textarea placeholder=""  name="scbox_shipaddress" id="scbox_shipaddress" onblur="validateform(this);" ></textarea>
                           <label>Shipping Address <span class="star">*</span></label>
                           <label id="scbox_shipaddress-error" class="error" for="scbox_shipaddress">Please enter shipping address</label>
                        </div>
                     </div>
                  </div>
                  <div class="row" id="recipient_address" style="display: none;">
                     <!-- id="recipient_address" style="display: none;" -->
                     <div class="title-section-changed text-center" id="subtitle-section" >
                        <h1 class="title-changed" style="font-weight:normal;font-size:18px;letter-spacing: 2px;font-style:italic;">About receiver:</h1>
                     </div>
                     <h2 style="font-weight:normal;font-size:15px;text-transform: uppercase;
                        letter-spacing: 2px;color: #9c9c9c;">
                        Gender<br>
                        Select Gender
                     </h2>
                     <div class="gender-wrp-outer">
                        <ul class="gender-wrp">
                           <li id="scboxgender-billmale" class="">
                              <span class="ic-gender ic-male"><i class="icon-male"></i>  </span>
                              <span class="text-label">M</span>
                           </li>
                           <li id="scboxgender-billfemale" class="" >
                              <span class="ic-gender ic-female"><i class="icon-female"></i></span>
                              <span class="text-label">W</span>
                           </li>
                        </ul>
                        <div class="form-group" id="billgender-error">
                           <input type="hidden" id="scbox_billgender" name="scbox_billgender" value="" onblur="validateform(this);" >
                           <label id="scbox_billgender-error" class="message error hide" for="scbox_billgender" style="text-align: center;">Please select gender</label>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group-md">
                           <input type="text" name="scbox_billname" id="scbox_billname" required value="" onblur="validateform(this);" maxlength="250"/>
                           <label>Full Name <span class="star">*</span></label>
                           <label id="scbox_billname-error" class="error" for="scbox_billname">Please enter your first and last name</label>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group-md">
                           <input type="hidden" name="scbox_billage" id="scbox_billage" value="" >
                           <fieldset class="date">
                              <label for="day_billstart"></label> 
                              <select id="day_billstart" name="day_billstart" />
                                 <option value="" >Day</option>
								  <?php for($i=1;$i<32;$i++){ ?>
									<option value="<?php echo $i; ?>" <?php echo ($user_dob_day==$i ) ? 'selected' : ''; ?> ><?php echo $i; ?></option> 
								  <?php } ?>  
                              </select>
                              - <label for="month_billstart"></label> 
                              <select id="month_billstart" name="month_billstart" > 
							    <option value="" >Month</option>
							  <option value="1" <?php echo ($user_dob_month==1 ) ? 'selected' : ''; ?> >January</option>       
								<option value="2" <?php echo ($user_dob_month==2 ) ? 'selected' : ''; ?> >February</option>       
								<option value="3" <?php echo ($user_dob_month==3 ) ? 'selected' : ''; ?> >March</option>       
								<option value="4" <?php echo ($user_dob_month==4 ) ? 'selected' : ''; ?> >April</option>       
								<option value="5" <?php echo ($user_dob_month==5 ) ? 'selected' : ''; ?> >May</option>       
								<option value="6" <?php echo ($user_dob_month==6 ) ? 'selected' : ''; ?> >June</option>       
								<option value="7" <?php echo ($user_dob_month==7 ) ? 'selected' : ''; ?> >July</option>       
								<option value="8" <?php echo ($user_dob_month==8 ) ? 'selected' : ''; ?> >August</option>       
								<option value="9" <?php echo ($user_dob_month==9 ) ? 'selected' : ''; ?> >September</option>       
								<option value="10" <?php echo ($user_dob_month==10 ) ? 'selected' : ''; ?> >October</option>       
								<option value="11" <?php echo ($user_dob_month==11 ) ? 'selected' : ''; ?> >November</option>       
								<option value="12" <?php echo ($user_dob_month==12 ) ? 'selected' : ''; ?> >December</option>
                              </select> -
                              <label for="year_billstart"></label> 
                              <select id="year_billstart" name="year_billstart" />
                                 <option value="" >Year</option>
								 <?php for($i=date("Y");$i>=1947;$i--){ ?>
								<option value="<?php echo $i; ?>" <?php echo ($user_dob_year==$i ) ? 'selected' : ''; ?> ><?php echo $i; ?></option> 
							  <?php } ?>
                              </select>
                              <span class="inst">Date of Birth*</span> 
                           </fieldset>
                           <label id="scbox_billage-error" class="error hide111" for="scbox_billage">Please enter your date of birth</label>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group-md">
                           <input type="email" placeholder="" name="scbox_billemailid" id="scbox_billemailid" required value="" onblur="validateform(this);"  />
                           <label>Email  <span class="star">*</span></label>
                           <label id="scbox_billemailid-error" class="error" for="scbox_billemailid">Please enter your e-mail ID</label>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group-md">
                           <input type="tel" placeholder="" name="scbox_billmobile" id="scbox_billmobile"   required value="" onblur="validateform(this);" maxlength="10" onkeypress="return isNumber(event);" />
                           <label>Mobile <span class="star">*</span></label>
                           <label id="scbox_billmobile-error" class="error" for="scbox_billmobile">Please enter your mobile number</label>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group-md">
                           <input type="tel" placeholder="" name="scbox_billpincode" id="scbox_billpincode"  required value=""  minlength="6" maxlength="6" onblur="validateform(this);" onkeypress="return isNumber(event);" />
                           <label>Pincode <span class="star">*</span></label>
                           <label id="scbox_billpincode-error" class="error" for="scbox_billpincode">Please enter pincode</label>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group-md">
                           <input type="text" placeholder="" name="scbox_billcity" id="scbox_billcity" value="" onblur="validateform(this);" style="color: black;" disabled />
                           <label>City <span class="star">*</span></label>
                           <label id="scbox_billcity-error" class="error" for="scbox_billcity">Please enter your city</label>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group-md">
                           <!--<select name="scbox_billstate" id="scbox_billstate" onblur="validateform(this);" disabled >
                              <option value="">Select State</option>
                           </select>-->
						   <input type="text" placeholder="" name="scbox_billstate" id="scbox_billstate" value="" onblur="validateform(this);" style="color: black;" disabled />
                           <label>State <span class="star">*</span></label>
                           <label id="scbox_billstate-error" class="error" for="scbox_billstate">Please select state</label>
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="form-group-md">
                           <select name="scbox_billprofession" id="scbox_billprofession" onblur="validateform(this);" onchange="ShowHideprofession();">
                              <option value="">Select</option>
                              <option value="corporate">Corporate</option>
                              <option value="entertainment">Entertainment</option>
                              <option value="entrepreneur">Entrepreneur</option>
                              <option value="fashion">Fashion</option>
                              <option value="homemaker">Homemaker</option>
                              <option value="professional">Professional</option>
                              <option value="student">Student</option>
                              <option value="other">Other</option>
                           </select>
                           <label>Profession <span class="star">*</span></label>
                           <label id="scbox_billprofession-error" class="error" for="scbox_billprofession">Please select profession</label>
                        </div>
                     </div>
                      <div class="col-md-3 col-sm-12 col-xs-12">
                        <div  id="dvProfession" class="form-group-md">
                           <input type="text" placeholder="" id="txtprofession" name="txtprofession" required onblur="validateform(this);" style="padding: 9px 0 9px 0;" />
                           <label>Enter Profession<span class="star">*</span></label>
                           <label id="txtprofession-error" class="error" for="txtprofession">Please enter profession</label>
                        </div>
                     </div>
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                           <div class="col-md-6 col-sm-12 col-xs-12">
                              <div class="form-group-md">
                                 <textarea placeholder=""  name="scbox_billaddress" id="scbox_billaddress" onblur="validateform(this);" ></textarea>
                                 <label>Billing Address <span class="star">*</span></label>
                                 <label id="scbox_billaddress-error" class="error" for="scbox_billaddress">Please enter billing address</label>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 hide" id="div-stylist-call">
                           <ul>
                              <li>
                                 <label class="sc-checkbox" id="stylist_call_li" ><i class="icon icon-diamond"></i>Don't know their sizes ? Don't worry, have our stylist call the receiver.
                                 <input type="checkbox" name="stylist_call" id="stylist_call" onchange="stylistCall(this);">
                                 </label>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="section section-shadow">
            <div class="btns-wrp">
               <a class="scbtn scbtn-primary" href="javascript:void(0);" onclick="ChangeStep(33);" ><span>Next</span></a>      
            </div>
         </section>
      </div>
</div>
</section>
<!--  </form> -->
</form>
</div>
<!--  </form> -->

<script>


   function ShowHideprofession() {
        var ddlProfession = document.getElementById("scbox_profession");
        var dvProfession = document.getElementById("dvProfession");        
        dvProfession.style.display = scbox_profession.value == "other" ? "block" : "none";
        var scbox_profession_ = $('#scbox_profession').val();
        if(scbox_profession_!='other')
        {
          $('#txtprofession').val(scbox_profession_);
        }
        else{
          $('#txtprofession').val('');
          scbox_profession_ = $('#txtprofession').val();
          $('#txtprofession').val(scbox_profession_);
        }
    }

   function ShowHideDiv() {
        var chkYes = document.getElementById("chkYes");
        var recipient_address = document.getElementById("recipient_address");
        recipient_address.style.display = chkYes.checked ? "block" : "none";
        var scbox_gender = '';
        var scbox_billgender = '';
        //if(recipient_address.style.display = 'block')
        //alert('chkYes.checked--'+chkYes.checked);
        if(chkYes.checked==true ){ 
           document.cookie = "scbox_isgift=true;path=/";
           $('#subtitle-section').html('<h1 class="title-changed" style="font-weight:normal;font-size:18px;letter-spacing: 2px;font-style:italic;">About receiver:</h1>');
           $('#scbox_emailid').removeAttr('disabled');
           $('#div-stylist-call').removeClass('hide');
      
           if(document.getElementById("chkYes").checked){
                if($('#stylist_call').is(":checked")){
                  document.cookie = "stylist_call=yes;path=/";         
                }else{
                  document.cookie = ";stylist_call=no;path=/";
                  $('#stylist_call').removeAttr('checked');
                } 
            }
           scbox_gender = $('#scbox_gender').val();
           scbox_billgender = $('#scbox_billgender').val();
           // $('#scbox_name').val(''); 
           // $('#day_start').val('');  
           // $('#month_start').val('');  
           // $('#year_start').val('');  
           // $('#scbox_emailid').val(''); 
           // $('#scbox_mobile').val(''); 
           // $('#scbox_pincode').val(''); 
           // $('#scbox_city').val(''); 
           // $('#scbox_state').val(''); 
           // $('#scbox_profession').val(''); 
           // $('#txtprofession').val(''); 
           // $('#scbox_shipaddress').val('');             
        }else
        {
          document.cookie = "scbox_isgift=false;path=/";
           $('#subtitle-section').html('<h1 class="title-changed" style="font-weight:normal;font-size:18px;letter-spacing: 2px;font-style:italic;">About me: </span></h1>');
            $('#div-stylist-call').addClass('hide');      
           // $('#logged-gender').removeClass('hide');
           // $('#gift-gender').addClass('hide');
           
        }
        
    }

function stylistCall(e)
{  
  //alert($('#stylist_call').is(":checked"));
    if($('#stylist_call').is(":checked")){
      document.cookie = "stylist_call=yes;path=/";
      $('#stylist_call_li').addClass('active');
    }else{
      document.cookie = "stylist_call=no;path=/"; 
      $('#stylist_call_li').removeClass('active'); }
}
</script>