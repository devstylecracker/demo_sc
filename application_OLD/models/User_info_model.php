<?php
class User_info_model extends MY_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function registerUser($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$sc_gender,$json_array){
     
        $insert = 0;
        $picture = '';
        $facebook_id = '';
        $google_id = '';
        $event_code = '';
        $birthday = '';
        $scmobile = @$json_array['scmobile'];
        $fbbirthday = @$json_array['birthday'];
		$json_array['firstname'] = ucfirst(strtolower($json_array['firstname']));
		$json_array['last_name'] = ucfirst(strtolower(@$json_array['last_name']));
		$medium_array = array("facebook", "google", "event", "mobile_ios_facebook", "mobile_android_facebook", "mobile_android_google", "mobile_ios_google");	
        if($fbbirthday!='')
        {
           $date = str_replace('/', '-', $fbbirthday);
           $birthday = date('Y-m-d', strtotime($date)); 
        }       
        

          if ($this->agent->is_browser())
          {
              $agent = 'Computer '.$this->agent->browser().' '.$this->agent->version();
          }
          elseif ($this->agent->is_robot())
          {
              $agent = 'Robot '.$this->agent->robot();
          }
          elseif ($this->agent->is_mobile())
          {
              $agent = 'Mobile '.$this->agent->mobile();
          }
          else
          {
              $agent = 'Unidentified';
          }

            $platform_info = $this->agent->platform();

        if(isset($medium) && (in_array($medium, $medium_array))){
            if($medium === 'google'){
                $picture = $json_array['picture'];
                $google_id = $json_array['id'];
            }
            elseif($medium === 'facebook') {
                //$picture = $json_array['picture']['data']['url'];
                $facebook_id = $json_array['id'];
                

            }elseif($medium === 'event')/* Added by Sudha For Event*/
            {
                $event_code = $json_array['event_code'];
                $medium ='website';
            }            
        }

        if($this->emailUnique($scemail_id) != NULL){
                 $query = $this->db->query('select id from user_login where email="'.$scemail_id.'"');
                 $result = $query->result_array();
                 $insert = 1;
            }
            
        if($insert===0){
        /***Code Ends***/
            $salt = substr(md5(uniqid(rand(), true)), 0, 10);
            $password   =$salt . substr(sha1($salt . $scpassword), 0, -10);
            $data = array(
			   'user_name' => $scusername,
			   'email' => $scemail_id,
			   'password' => $password,
			   'auth_token'=> substr(md5(uniqid(rand(1,6))), 0, 15),
			   'ip_address' => $ip,
			   'status' => 1,
			   'created_datetime' => $this->config->item('sc_date'),
			   'modified_datetime' => $this->config->item('sc_date'),
			   'event_code' => $event_code,
               'created_by' =>0,
               'modified_by' => 0
            );
            $this->db->insert('user_login', $data);
            $Lst_ins_id = $this->db->insert_id();
            if($Lst_ins_id){
                $data = array('facebook_id'=>$facebook_id,'google_id'=>$google_id,'profile_pic'=>$picture, 'user_id' => $Lst_ins_id, 'first_name' => $json_array['firstname'],'last_name' => $json_array['lastname'],  'created_datetime' => $this->config->item('sc_date'),
               'modified_datetime' => $this->config->item('sc_date'), 'registered_from'=>$medium,'created_by' => $Lst_ins_id, 'modified_by' => $Lst_ins_id, 'gender' => $sc_gender,'gender_c'=>'1','platform_name'=>$platform_info,'contact_no'=>$scmobile,'birth_date'=>$birthday);

                $this->db->insert('user_info', $data);

                $data = array('user_id'=>$Lst_ins_id,'role_id'=>$role,'created_by'=>$Lst_ins_id,'modified_by'=>$Lst_ins_id);

                $this->db->insert('user_role_mapping', $data);

				$data = array('user_id'=>$Lst_ins_id,'question_id'=>$json_array['question_id'],'answer_id'=>$json_array['age_range'],'created_by'=>$Lst_ins_id,'modified_by'=>$Lst_ins_id, 'created_datetime' => $this->config->item('sc_date'),'modified_datetime' => $this->config->item('sc_date'),'is_active' =>1);

                $this->db->insert('users_answer', $data);
				
                $res = $this->login_user($scemail_id,$scpassword,$medium,'');

                if(strlen($facebook_id)>1 || strlen($google_id)>1)
                    return "pa";
				
				if($medium ==='facebook' || $medium === 'google' || $medium === 'website'){
					return true;
				}else{
					return $res;
				}
            }
        }else{
            
            $query = $this->db->query("select profile_pic from user_info where user_id = ".$result[0]['id']);
            $res = $query->result();
            if(empty($res[0]->profile_pic) ){
                $data = array('facebook_id'=>$facebook_id,'google_id'=>$google_id,'profile_pic'=>$picture, 'registered_from'=>$medium,'modified_by' => $result[0]['id'],'modified_datetime' => $this->config->item('sc_date'),'gender_c'=>'1','birth_date'=>$birthday);
            }else{
                $data = array('facebook_id'=>$facebook_id,'google_id'=>$google_id,'registered_from'=>$medium,'modified_by' => $result[0]['id'],'modified_datetime' => $this->config->item('sc_date'),'gender_c'=>'1','birth_date'=>$birthday);
            }
            $this->db->update('user_info', $data, array('user_id' => $result[0]['id'] ));
            $query = $this->db->query("select user_role_mapping.role_id,user_info.user_id,user_login.user_name,user_login.email,user_info.created_datetime,user_info.first_name,user_info.last_name,user_login.bucket,user_login.question_comp,user_login.question_resume_id,user_info.gender,user_info.gender_c,user_info.contact_no from user_info,user_role_mapping,user_login where user_info.user_id = ".$result[0]['id']." and user_info.user_id = user_role_mapping.user_id and user_info.user_id = user_login.id");
            $res = $query->result();

            if(!empty($res)){
				if($medium != 'website'){
					$data = array('auth_token' => substr(md5(uniqid(rand(1,6))), 0, 15));
					$this->db->update('user_login', $data, array('id' =>$res[0]->user_id));//update token
				}
				$data_attempts = array('user_id'=>$res[0]->user_id,'login_time'=>$this->config->item('sc_date'));
				$this->db->insert('user_login_attempts', $data_attempts);
				$login_attempts_id = $this->db->insert_id();

				$array_field=array('user_name' => $res[0]->user_name,'user_id' => $res[0]->user_id,'role_id'=>$res[0]->role_id,'first_name'=>$res[0]->first_name,'last_name'=>$res[0]->last_name,'bucket'=>$res[0]->bucket,'question_comp'=>$res[0]->question_comp,'question_resume_id'=>$res[0]->question_resume_id,'login_attempts_id'=>$login_attempts_id,'email' => $res[0]->email,'gender' => $res[0]->gender,'gender_c' => $res[0]->gender_c,'contact_no' => $res[0]->contact_no,'utm_source1'=>$this->session->userdata('utm_source1'));

				$query_res = $this->db->get_where('user_address', array('user_id' => $res[0]->user_id,'is_default'=>1));
				$res_res = $query_res->result_array();
				if(!empty($res_res)){ $array_field['pincode'] = $res_res[0]['pincode']; 
				setcookie('stylecracker_shipping_pincode', $res_res[0]['pincode'], time() + ((86400 * 2) * 30), "/", "",  0);
				}

				$this->session->set_userdata($array_field);

				if($medium ==='facebook' || $medium === 'google' || $medium === 'website'){
					if($res[0]->bucket == 0)
					return 'pa';
					else
					return 'profile';
				}else{
					return $array_field;
				}
				
            }else{
                return 2;
            }

        }
    }
	
	function emailUnique($emaild){
        $query = $this->db->query('select email from user_login where email ="'.$emaild.'"');
        $username_unique = $query->result_array();
        return $username_unique;
    }
	
	function login_user($email,$password,$medium,$type){
        
        $query = $this->db->get_where('user_login', array('email' => $email,'status'=>1));
        $result = $query->result();
        if(!empty($result)){
        $salt = substr($result[0]->password, 0, 10);

        $db_password =  $salt . substr(sha1($salt . $password), 0, -10);

        if($db_password == $result[0]->password || $type == 'facebook' || $type == 'google'){

            $query = $this->db->query("select user_role_mapping.role_id,user_info.user_id,user_info.created_datetime,user_info.first_name,user_info.last_name,user_login.email,user_login.bucket,user_login.question_comp,user_login.question_resume_id,user_login.user_name,user_info.gender,user_info.gender_c,user_info.contact_no from user_info,user_role_mapping,user_login where user_info.user_id = ".$result[0]->id." and user_info.user_id = user_role_mapping.user_id and user_info.user_id = user_login.id");
            $res = $query->result();
            if(!empty($res)){
				if($medium != 'website'){
					$data = array('auth_token' => substr(md5(uniqid(rand(1,6))), 0, 15));
					$this->db->update('user_login', $data, array('id' =>$res[0]->user_id));//update token
				}
                $data_attempts = array('user_id'=>$res[0]->user_id,'login_time'=>$this->config->item('sc_date'));
                $this->db->insert('user_login_attempts', $data_attempts);
                $login_attempts_id = $this->db->insert_id();

                $array_field=array('user_name' => $res[0]->user_name,'user_id' => $res[0]->user_id,'role_id'=>$res[0]->role_id,'first_name'=>$res[0]->first_name,'last_name'=>$res[0]->last_name,'bucket'=>$res[0]->bucket,'question_comp'=>$res[0]->question_comp,'question_resume_id'=>$res[0]->question_resume_id,'login_attempts_id'=>$login_attempts_id,'email' => $res[0]->email,'gender' => $res[0]->gender,'gender_c' => $res[0]->gender_c,'contact_no' => $res[0]->contact_no,'utm_source1'=>$this->session->userdata('utm_source1'));

                $query_res = $this->db->get_where('user_address', array('user_id' => $res[0]->user_id,'is_default'=>1));
                $res_res = $query_res->result_array();
                if(!empty($res_res)){ $array_field['pincode'] = $res_res[0]['pincode']; 
                setcookie('stylecracker_shipping_pincode', $res_res[0]['pincode'], time() + ((86400 * 2) * 30), "/", "",  0);
                }

                $this->session->set_userdata($array_field);
				if($medium ==='facebook' || $medium === 'google' || $medium === 'website'){
					return 1;
				}else return $array_field;
            }else{
                return 2;
            }
        }else{
                return 2;
            }
        }else{ return 2; }
    }
	
	/* extra function for mobile app */
	function get_gender_by_email($email){
		$query = $this->db->query(" SELECT b.gender FROM user_login as a,user_info as b WHERE a.email='$email' AND a.id = b.user_id ");
		$result = $query->result_array();
		// echo $result[0]['gender'];exit;
		if(!empty($result)){
			return $result[0]['gender'];
		}else {
			return 1;
		}
		
	}
	
	function get_users_info_mobile($user_id){
		
		$query = $this->db->query("select a.user_id,a.created_datetime,a.first_name,b.email,a.last_name,b.bucket,b.auth_token,b.question_comp,b.question_resume_id,a.contact_no,a.registered_from,a.facebook_id,a.google_id,IF(a.profile_pic='', IF(a.gender = 1,'".WOMEN_DEFAULT_IMG."','".MEN_DEFAULT_IMG."'), CONCAT('".PROFILE_IMG_URL."',a.profile_pic)) as profile_pic , IF(a.profile_cover_pic IS NULL, 'http://www.stylecracker.com/assets/images/profile/cover.jpg', CONCAT('".COVER_IMG_URL."',a.profile_cover_pic)) as profile_cover_pic,IF(a.gender=1 ,'women', 'men') as gender,(select c.answer_id from users_answer as c WHERE b.id = c.user_id AND (c.question_id = 8 OR c.question_id = 15) limit 0,1 ) as age_answer_id from user_info as a,user_login as b where a.user_id = ".$user_id." and a.user_id = b.id ");
		$res = $query->result();
		return $res[0];
    }
	
	//get the gender of user 
	function get_user_gender($user_id){
			$query = $this->db->query("select a.gender from user_info a where a.user_id = $user_id ");
			$res = $query->result_array();
			if(!empty($res)){ return $res[0]['gender']; } else return 1;
			
    }
	
	function scgenrate_otp($user_data){

		$time = new DateTime(date('Y-m-d H:i:s'));
		$dateM = $time->format('Y-m-d H:i:s');
		$otp_no = rand(4,100000);
		$digits = 4;
		$otp_no =rand(pow(10, $digits-1), pow(10, $digits)-1);
		// $otp_no = 9999;
		
		$query = $this->db->query('select otp_no from user_mobile_otp where mobile_no = '.$user_data['mobile_no'].' AND type = "'.$user_data['type'].'" AND created_datetime between now() -INTERVAL 30 MINUTE AND now() + INTERVAL 30 MINUTE ');
		$res = $query->result_array();
		if(empty($res)){ 
			$data = array('mobile_no'=>$user_data['mobile_no'],'otp_no'=>$otp_no,'type'=>$user_data['type'],'created_datetime'=>$dateM);
			$this->db->insert('user_mobile_otp', $data);
			$Lst_ins_id = $this->db->insert_id();
			$query = $this->db->query("select otp_no from user_mobile_otp where id = $Lst_ins_id");
			$res = $query->result_array();
			return $res[0]['otp_no'];
		}else{
			return $res[0]['otp_no'];
		}
    }

    function updateOtpStatus($mobileno,$otp,$userid)
    {
        $res = $this->db->update('user_mobile_otp', array('status'=>1), array('mobile_no'=>$mobileno, 'otp_no'=>$otp ,'type'=>'register' ));
        $this->db->update('user_login',array('confirm_mobile'=>1), array('id'=>$userid));
        if($res)
        {
          return true;
        }else
        {
          return false;
        }
    }

    function getOtpStatus($userid)
    {
      $query = $this->db->query("select confirm_mobile from user_login where id = $userid");
      $res = $query->result_array();      
      return $res[0]['confirm_mobile'];
    }
	
	function get_wishlisted_ids($data,$fav_for){
		$query = $this->db->query("select group_concat(fav_id) as fav_ids from favorites where user_id = ".$data['user_id']." AND fav_for = '$fav_for' ");
		$res = $query->result_array();
		if(!empty($res)){
			return $res[0]['fav_ids'];
		}else{
			$data = array();
			return $data;
		}
	}
	
	function recommended_products_via_attributes($bucket){
        $product_id = ''; 
        if($bucket>0){

            $query = $this->db->query("select a.attribute_value_ids from (select object_id,attribute_value_ids from pa_object_relationship where object_type='category' and pa_type='bucket' and object_value =".$bucket." and attribute_value_ids!='') as a ");
            $res = $query->result_array($query);
           
            if(!empty($res)){ $i=0;$cat_id = '';
            foreach($res as $val){
                if($val['attribute_value_ids']!=''){
                    if($i!=0){ $cat_id = $cat_id.','; }

                    $dt = unserialize($val['attribute_value_ids']);
                    $cat_ids = implode(',', $dt);
                    $cat_id = $cat_id.$cat_ids;
                    $i++;
                }
            $w_c3='';
            if($cat_id==''){ $cat_id = '0'; }
             $get_pro_id1 = $this->db->query("select GROUP_CONCAT(id) as att from attribute_values_relationship where id IN (".$cat_id.") group by attribute_id");
             $resul1 = $get_pro_id1->result_array();
             $att = array();

             
             if(!empty($resul1)){ $f = 0;
                $w_c3 = ''; $w_c3 = $w_c3.' Having (';
                foreach($resul1 as $vall){ 
                    $att = explode(',', $vall['att']);
                    if($f!=0){ $w_c3 = $w_c3.') and ('; }
                    $k=0;
                    foreach($att as $val){
                        if($k!=0){ $w_c3 = $w_c3.' or '; }
                        $w_c3 = $w_c3.' Find_In_Set('.$val.',attr)';
                        $k++;
                    }
                    $f++;

                }

                $w_c3 = $w_c3.')'; 

            if($w_c3!=''){
               $query11 = $this->db->query("select object_id ,GROUP_CONCAT(attribute_id) as attr from attribute_object_relationship group by object_id".$w_c3);
                $res11 = $query11->result_array();
				$i = 1;
                if(!empty($res11)){
                    foreach($res11 as $val1){
						if($i == 1){
							$product_id = $val1['object_id'];
						}else{
							$product_id = $product_id.','.$val1['object_id'];
						}
                       $i++;
                    }
                }
            }
        }
            }
        }
        }
        $product_id = $this->product_availability($product_id);
        if($product_id==''){
            $product_id1 = $this->recommended_products_via_category($bucket);
            $product_id = $this->product_availability($product_id1);
        }
		
        return $product_id;
    }
	
	function recommended_products_via_category($bucket){

        if($bucket>0){
            $pro_ids = '';
            if($bucket!=''){
    
            $query = $this->db->query("select object_id from pa_object_relationship where object_type='category' and pa_type='bucket' and object_value =".$bucket." ");

            $res = $query->result_array();

            $cat_id =''; $product_id = '';
            if(!empty($res)){ 
                foreach($res as $val){
                    $cat_id = $cat_id.','.$val['object_id'];
                }
            

                $query1 = $this->db->query("select distinct object_id from category_object_relationship where category_id IN(-1".$cat_id.") and object_type='product'");
                $res1 = $query1->result_array();
				$i = 1;
                if(!empty($res1)){
                    foreach($res1 as $val){
						if($i == 1){
							$product_id = $val['object_id'];
						}else{
							$product_id = $product_id.','.$val['object_id'];
						}
                       $i++;
                    }
                }
                
                return $product_id; 

            }else{
                return '';    
            }
            }
        }else{
            return '';
        }

    }
	
	function product_availability($product_ids){
        $pro_id = '';
        if($product_ids!=''){
			$query11 = $this->db->query("select id from product_desc where status=1 and approve_reject IN('A','D') and id in(0".$product_ids.")");
			$res11 = $query11->result_array();
			$i = 1;
			if(!empty($res11)){
				foreach($res11 as $val){
					if($i == 1){
						$pro_id = $val['id'];
					}else{
						$pro_id = $pro_id.','.$val['id'];
					}
				   $i++;
				}
			}
            return $pro_id;
        }else{
            return $pro_id;
        }
    }
	
	/*function update_account_details($user_data){
		$data_info = array('first_name' => $user_data['first_name'],'last_name' => $user_data['last_name'],'gender' => $user_data['gender'],'contact_no' => $user_data['mobile_no']);
		$this->db->update('user_info', $data_info, array('user_id' =>$user_data['user_id']));
		
		$data_answer = array('user_id'=>$user_data['user_id'],'question_id'=>$user_data['question_id'],'answer_id'=>$user_data['age_range'],'created_by'=>$user_data['user_id'],'modified_by'=>$user_data['user_id'], 'created_datetime' => $this->config->item('sc_date'),'modified_datetime' => $this->config->item('sc_date'),'is_active' =>1);
		$this->db->update('users_answer', $data_answer, array('user_id' =>$user_data['user_id']));
		if($user_data['is_gender_change'] == '1'){
			
			$data = array('bucket'=>0,'question_comp'=>0,'question_resume_id'=>'');
			$this->db->where('id',$user_data['user_id']);
			$this->db->update('user_login',$data);
			$user_id = $user_data['user_id'];
			$this->db->query("UPDATE object_meta SET object_meta_value = null  where object_meta_key IN('user_body_shape','user_style_p1','user_style_p2','user_style_p3','user_brand_list') AND object_id = $user_id ");
			
		}
		return 1;
	}
*/
    function update_account_details($user_data){
        //echo '<pre>';print_r($user_data);
        $returnvalue = 1;
        if(!empty($user_data))
        {
            //echo '<pre>';print_r($this->session->userdata());
            if(@$user_data['first_name']!='' && @$user_data['last_name']!='' && @$user_data['gender']!='' && @$user_data['mobile_no']!='' )
            {

        		$data_info = array('first_name' => $user_data['first_name'],'last_name' => $user_data['last_name'],'gender' => $user_data['gender'],'contact_no' => $user_data['mobile_no']);
        		$this->db->update('user_info', $data_info, array('user_id' =>$user_data['user_id']));

                if(trim($user_data['mobile_no']) != trim($this->session->userdata('contact_no')))
                {
                    $returnvalue = 2;                 
                }
                $array_field=array('first_name'=>$user_data['first_name'],'last_name'=>$user_data['last_name'],'question_comp'=>0,'gender' => $user_data['gender'],'gender_c' => 1,'contact_no' => $user_data['mobile_no']);

                $this->session->set_userdata($array_field);

            }
            
            if(@$user_data['user_id']!='' && @$user_data['question_id']!='' && @$user_data['age_range']!='')
            {
                    $query11 = $this->db->query("select user_id from users_answer where user_id=".$user_data['user_id']." ");
                    $res11 = $query11->result_array();
                    if(!empty($res11))
                    {
                         $data_answer = array('user_id'=>$user_data['user_id'],'question_id'=>$user_data['question_id'],'answer_id'=>$user_data['age_range'],'created_by'=>$user_data['user_id'],'modified_by'=>$user_data['user_id'], 'created_datetime' => $this->config->item('sc_date'),'modified_datetime' => $this->config->item('sc_date'),'is_active' =>1);
                        $res = $this->db->update('users_answer', $data_answer, array('user_id' =>$user_data['user_id'],'question_id' =>$user_data['question_id']));                       
                    }else
                    {    
                        $data_answer = array('user_id'=>$user_data['user_id'],'question_id'=>$user_data['question_id'],'answer_id'=>$user_data['age_range'],'created_by'=>$user_data['user_id'],'modified_by'=>$user_data['user_id'], 'created_datetime' => $this->config->item('sc_date'),'modified_datetime' => $this->config->item('sc_date'),'is_active' =>1);
                        $this->db->insert('users_answer', $data_answer);
                       
                    }
                    
                //echo '<pre>';print_r($res);exit;
            }
            //echo '<pre>';print_r($this->session->userdata());exit;

    		if($user_data['is_gender_change'] == '1'){
    			$data = array('bucket'=>0,'question_comp'=>0,'question_resume_id'=>'');
    			$this->db->where('id',$user_data['user_id']);
    			$this->db->update('user_login',$data);
    			$user_id = $user_data['user_id'];
    			$this->db->query("UPDATE object_meta SET object_meta_value = null  where object_meta_key IN('user_body_shape','user_style_p1','user_style_p2','user_style_p3','user_brand_list') AND object_id = $user_id ");
    		}
    		return $returnvalue;

        }else
        {
            return 0;
        }

    }

  function get_userage_range($dob,$gender)
  {
      $agerangeid = 0;
      //$dob = "2007-02-14";
      $curdate = date('Y-m-d');
      $diff = abs(strtotime($curdate) - strtotime($dob));

      $years = floor($diff / (365*60*60*24));
     
      if(!empty($years) && $years>=16  && $gender==1)
      {
        if($years<=20)
        {
          $agerangeid = 39;
        }else if($years<=25)
        {
          $agerangeid = 40;
        }else if($years<=30)
        {
          $agerangeid = 41;
        }else if($years<=35)
        {
          $agerangeid = 42;
        }else if($years<=40)
        {
          $agerangeid = 43;
        }else if($years>40)
        {
          $agerangeid = 44;
        }

      }else
      {
        if($years<=21)
        {
          $agerangeid = 4683;
        }else if($years<=27)
        {
          $agerangeid = 4684;
        }else if($years<=35)
        {
          $agerangeid = 4685;
        }else if($years<=45)
        {
          $agerangeid = 4686;
        }else if($years>45)
        {
          $agerangeid = 4687;
        }
      }
      return $agerangeid;
  }

	
	function get_all_notification($user_data){
		$notification_array = array();$i=0;$object_id=0;
		
		$query = $this->db->query('SELECT GROUP_CONCAT(b.object_id) as notification_ids from object a,object_meta b WHERE a.object_id = b.object_id AND b.object_meta_key = "user_id" AND b.object_meta_value = '.$user_data['user_id'].' AND  a.object_type = "notification" ');
		$result = $query->result_array();
		
		if(!empty($result) && @$result[0]['notification_ids'] != ''){
			$query = $this->db->query('SELECT a.object_id,b.* from object a,object_meta b WHERE a.object_id = b.object_id AND a.object_type = "notification" AND a.object_id IN('.$result[0]['notification_ids'].') ORDER BY a.object_id desc');
			$res = $query->result_array();
			
			foreach($res as $val){
				if($val['object_id'] == $object_id || $object_id == 0){
					$notification_array[$i][$val['object_meta_key']] = $val['object_meta_value'];
				}else{
					$notification_array[$i][$val['object_meta_key']] = $val['object_meta_value'];
					$i++;
				}
				$object_id = $val['object_id'];
				$notification_array[$i]['notification_id'] = $object_id;
			}
			
		}
		
		return $notification_array;
	}

	function update_notification($user_data){
		$this->db->query('UPDATE object_meta SET object_meta_value = '.$user_data['order_notification_read'].'  where object_meta_key = "order_notification_read" AND object_id = '.$user_data['notification_id'].' ');
		return true;
	}
	
	function saveupdate_fb_token($fb_token_data,$user_id){
		
		$this->db->query('UPDATE user_info SET fb_data = \''.$fb_token_data.'\' where user_id = '.$user_id.' ');
		echo $this->db->last_query();
		return true;
		
	}

    function saveInstaData($userid,$instadata)
    {

        if($userid!='' && $instadata!='')
        {
            $data = array('instagram_data'=> $instadata);
            $this->db->update('user_info', $data, array('user_id' => $userid ));
            return 1;
        }else
        {
            return 0;
        }        
    }

    function getInstaData($userid)
    {
       $query = $this->db->query("select instagram_data from user_info where user_id = ".$userid);
       $res = $query->result_array();

       if(!empty($res))
       {
         //echo '<pre>';print_r($res);
         return $res[0]['instagram_data'];
       }
    }
	
	
	public function addUserInfo($arrData = array())
	{
		$this->db->insert('object_meta', $arrData);
		$omid = $this->db->insert_id();
		return  $omid;
	}
	
	public function addUserWardrobe($arrData = array())
	{		
		$this->db->insert('users_wardrobe', $arrData);
		$id = $this->db->insert_id();
		return  $id;
	}
	
}

?>