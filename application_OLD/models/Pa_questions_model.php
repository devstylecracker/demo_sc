<?php
class Pa_questions_model extends CI_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	public function get_pa_questions()
	{
		$this->db->select('tt.taxonomy_content,tt.scx_term_id,t.term_slug,t.term_name,tm.termmeta_key,tm.termmeta_value');
		$this->db->from('term_taxonomy as tt');
		$this->db->join('terms as t', 't.id = tt.scx_term_id', 'INNER');
		$this->db->join('term_meta as tm', 'tm.scx_term_id = t.id', 'INNER');
		$this->db->where('tt.taxonomy_type', 'pa');
		$this->db->where('tt.parent', 1);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}
	
	public function questionby_term_id($tem_id='')
	{
		$this->db->select('tt.taxonomy_content,tt.scx_term_id,t.term_slug,t.term_name');
		$this->db->from('term_taxonomy as tt');
		$this->db->join('terms as t', 't.id = tt.scx_term_id', 'INNER');
		$this->db->where('tt.taxonomy_type', 'pa');
		$this->db->where('tt.parent', $tem_id);
		$query = $this->db->get();
	/* 	echo $this->db->last_query();
		exit();   */
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}
	
	public function questionby_term_meta_keyvalue($term_meta_key,$term_meta_value,$parent)
	{
		
		//$this->db->select('tt.scx_term_id,tt.taxonomy_content,t.term_slug,t.term_name');
		$this->db->select('DISTINCT (tt.scx_term_id),tt.taxonomy_content,t.term_slug,t.term_name,tt.display_order,t.id');
		$this->db->from('term_taxonomy as tt');
		$this->db->join('terms as t', 't.id = tt.scx_term_id', 'INNER');
		$this->db->join('term_meta as tm', 't.id = tm.scx_term_id', 'INNER');
		$this->db->where('tt.taxonomy_type', 'pa');
		$this->db->where_in('tt.parent', $parent);
		$this->db->where('tm.termmeta_key', $term_meta_key);
		$this->db->where('tm.termmeta_value', $term_meta_value);
		$this->db->where('tm.scx_term_id!=', 14);
		$this->db->order_by('tt.display_order', 'ASC');
		$query = $this->db->get();
		/*echo $this->db->last_query();
		exit();*/ 
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}
	
	public function get_question_answers($term_id)
	{
		$this->db->select('tt.taxonomy_content,tt.scx_term_id,t.term_slug,t.term_name,tm.termmeta_key,tm.termmeta_value as image_default');
		$this->db->from('term_taxonomy as tt');
		$this->db->join('terms as t', 't.id = tt.scx_term_id', 'INNER');
		$this->db->join('term_meta as tm', 't.id = tm.scx_term_id', 'INNER');
		$this->db->where('tt.taxonomy_type', 'pa');
		$this->db->where('tt.parent', $term_id);
		$this->db->where('tm.termmeta_key', 'image_default');
		$this->db->order_by('tt.display_order', 'ASC');
		$query = $this->db->get();
		/*  echo $this->db->last_query();
		exit(); */ 
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}
	
	public function get_term_meta($termid,$term_meta_key)
	{
		$this->db->select('tm.termmeta_value');
		$this->db->from('term_meta as tm');	
		$this->db->where('tm.scx_term_id', $termid);
		$this->db->where('tm.termmeta_key', $term_meta_key);		
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if($query->num_rows()>0)
		{
			$res = $query->result_array();			
			return $res[0]['termmeta_value'];
		}
		return false;
	}
	
	public function get_termtaxanomy_data($tem_id='')
	{
		$this->db->select('tt.taxonomy_content,tt.scx_term_id,t.term_slug,t.term_name');
		$this->db->from('term_taxonomy as tt');
		$this->db->join('terms as t', 't.id = tt.scx_term_id', 'INNER');
		$this->db->where('tt.taxonomy_type', 'pa');
		$this->db->where('tt.scx_term_id', $tem_id);
		$query = $this->db->get();
	/* 	echo $this->db->last_query();
		exit();   */
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}
	
	
} 
