<?php
class Cartnew_model extends MY_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_cart(){
    	$user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = ''; }
        //echo $_COOKIE['SCUniqueID'];exit;
    	if(isset($_COOKIE['SCUniqueID']) && $_COOKIE['SCUniqueID'] !=""){
      		$uniquecookie = $_COOKIE['SCUniqueID'];
      	if($user_id == '' || $user_id ==0){
    	$query = $this->db->query('select d.cod_available,d.shipping_exc_location,d.cod_exc_location,b.brand_id,b.slug,a.`id`,a.`product_id`,d.`return_policy`,a.`product_qty`,b.`name`,(a.`product_qty`*b.price) as price,CASE WHEN DATE_FORMAT(b.discount_start_date,"%Y-%m-%d  %h:%i") <= DATE_FORMAT(now(),"%Y-%m-%d %h:%i") and DATE_FORMAT(b.discount_end_date,"%Y-%m-%d  %h:%i") >= DATE_FORMAT(now(),"%Y-%m-%d %h:%i") then (a.`product_qty`*b.compare_price) ELSE 0 END AS compare_price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,b.`id` as pid,b.`price` as product_price,c.id as size_id,a.`product_price` as cart_product_price from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and b.status = 1 and (a.`SCUniqueID` = "'.$uniquecookie.'") and a.`product_id` NOT IN ('.SCBOX_PRODUCTID_STR.') order by b.`brand_id`');
        }else{
            $query = $this->db->query('select d.cod_available,d.shipping_exc_location,d.cod_exc_location,b.brand_id,b.slug,a.`id`,a.`product_id`,d.`return_policy`,a.`product_qty`,b.`name`,(a.`product_qty`*b.price) as price,CASE WHEN DATE_FORMAT(b.discount_start_date,"%Y-%m-%d  %h:%i") <= DATE_FORMAT(now(),"%Y-%m-%d %h:%i") and DATE_FORMAT(b.discount_end_date,"%Y-%m-%d  %h:%i") >= DATE_FORMAT(now(),"%Y-%m-%d %h:%i") then (a.`product_qty`*b.compare_price) ELSE 0 END AS compare_price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,b.`id` as pid,b.`price` as product_price,c.id as size_id,a.`product_price` as cart_product_price from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and b.status = 1 and (a.`SCUniqueID` = "'.$uniquecookie.'" || a.user_id = "'.$user_id.'") and a.`product_id` NOT IN ('.SCBOX_PRODUCTID_STR.') order by b.`brand_id`');
        }
    	$res = $query->result_array();
    	$product_data = array();
            if(!empty($res)){ $i = 0;
                foreach ($res as $key => $value) {

                    if($value['product_id']==SCBOX_CUSTOM_ID)
                    {
                        // $value['compare_price'] = @$_COOKIE['Scbox_price'];
                        // $value['product_price'] = @$_COOKIE['Scbox_price'];
                        $value['compare_price'] = $value['cart_product_price'];
                        $value['product_price'] = $value['cart_product_price'];
                    }
                    
                    $product_data[$i]['id'] = $value['id'];
                    $product_data[$i]['product_id'] = $value['product_id'];
                    $product_data[$i]['cod_available'] = $value['cod_available'];
                    $product_data[$i]['shipping_exc_location'] = $value['shipping_exc_location'];
                    $product_data[$i]['cod_exc_location'] = $value['cod_exc_location'];
                    $product_data[$i]['slug'] = $value['slug'];
                    $product_data[$i]['product_qty'] = $value['product_qty'];
                    $product_data[$i]['return_policy'] = $value['return_policy'];
                    $product_data[$i]['name'] =$value['name'];
                    $product_data[$i]['price'] = $value['compare_price'] > $value['product_price'] ?  $value['compare_price'] : $value['product_price'];
                    $product_data[$i]['image'] =$value['image'];
                    $product_data[$i]['size_text'] =$value['size_text'];
                    $product_data[$i]['company_name'] =$value['company_name'];
                    $product_data[$i]['user_id'] =$value['user_id'];
                    $product_data[$i]['min_del_days'] =$value['min_del_days'];
                    $product_data[$i]['max_del_days'] =$value['max_del_days'];
                    $product_data[$i]['store_tax'] =$value['store_tax'];
                    $product_data[$i]['is_shipping'] =$value['is_shipping'];
                    $product_data[$i]['shipping_charges'] =$value['shipping_charges'];
                    $product_data[$i]['shipping_min_values'] =$value['shipping_min_values'];
                    $product_data[$i]['shipping_max_values'] =$value['shipping_max_values'];
                    $product_data[$i]['is_cod'] =$value['is_cod'];
                    $product_data[$i]['cod_charges'] =$value['cod_charges'];
                    $product_data[$i]['cod_min_value'] =$value['cod_min_value'];
                    $product_data[$i]['code_max_value'] =$value['code_max_value'];
                    $product_data[$i]['product_size'] =$value['product_size'];
                    $product_data[$i]['compare_price']= $value['compare_price'];

                    $discount_percent = $this->get_discount_percentage($value['user_id'],$value['pid']);
                    $product_data[$i]['discount_price'] =round($value['product_qty']*($value['product_price']-($value['product_price']*$discount_percent)/100));
                    $product_data[$i]['discount_percent'] = $this->product_desc_model->calculate_discount_percent($product_data[$i]['price'],$product_data[$i]['discount_price'],$value['price']);

                    $product_data[$i]['discount_type'] =  $this->get_discount_id($value['user_id'],$value['pid'])['discount_type_id'];

                    if($product_data[$i]['discount_type'] == 1){
                     $product_data[$i]['discount_id'] =  $this->get_discount_id($value['user_id'],0)['id'];
                    }else if($product_data[$i]['discount_type'] == 2){
                        $product_data[$i]['discount_id'] =  $this->get_discount_id(0,$value['pid'])['id'];
                    }else{
                         $product_data[$i]['discount_id'] =0;
                         $product_data[$i]['discount_type'] =0;
                    }
                    $product_data[$i]['discount_max_amount'] = $this->get_discount_maxvalue($value['user_id'],$value['pid']);
                    $product_data[$i]['product_all_size'] = $this->get_size($value['product_id']);
                    $product_data[$i]['brand_unique_name'] =$this->get_user_info($value['user_id'])[0]['user_name'];
                    $product_data[$i]['in_stock'] = $this->get_product_availablility($value['product_id'],$value['size_id']);
                    $product_data[$i]['brand_id'] =$value['brand_id'];
                    /*$product_data[$i]['coupon_amount'] = $this->getCouponData()*/

                    $i++;
                }
               /* $product_data['coupon_amount'] = $this->getCouponData($coupon_code)['coupon_amount'];*/
               /*echo "<pre>";print_r($product_data);*/
            }
        return $product_data;
        }
    }

     function get_discount_percentage($brand_id,$product_id){
        if($brand_id > 0){
            $query = $this->db->query("select discount_percent from discount_availed as da where (da.brand_id = '".$brand_id."' || da.product_id = '".$product_id."') AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1");
           /* echo $this->db->last_query();*/
            $result = $query->result_array();
            if(!empty($result)){
                return $result[0]['discount_percent'];
            }else{
                return 0;
            }
        }else{ return 0; }
    }

    function get_discount_id($brand_id,$product_id)
    {
        if($brand_id > 0 && $product_id > 0){
           $query = $this->db->query("select id, discount_type_id from discount_availed as da where (da.brand_id = '".$brand_id."' OR da.product_id = '".$product_id."' ) AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1 ");
       
            $result = $query->result_array();

            if(!empty($result)){
                return $result[0];
            }else{
                return 0;
            }
        }else if($brand_id > 0){
            $query = $this->db->query("select id, discount_type_id from discount_availed as da where (da.brand_id = '".$brand_id."' ) AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1 ");
       
            $result = $query->result_array();
            if(!empty($result)){
                return $result[0];
            }else{
                return 0;
            }

        }else{

            if($product_id > 0){
                $query = $this->db->query("select da.id, da.discount_type_id from discount_availed as da where (da.brand_id = 0 AND da.product_id = '".$product_id."' ) AND da.status=1");
             
                $result = $query->result_array();
                if(!empty($result)){
                    return $result[0];
                }else{
                    return 0;
                }
            }else
            {
                $query = $this->db->query("select da.id, da.discount_type_id from discount_availed as da where (da.brand_id = '".$brand_id."' AND da.product_id = 0 ) AND da.status=1");
              
                $result = $query->result_array();
                if(!empty($result)){
                    return $result[0];
                }else{
                    return 0;
                }
            }
         }
    }

    function get_discount_maxvalue($brand_id,$product_id){
        if($brand_id > 0){
            
            $query = $this->db->query("select max_amount from discount_availed as da where (da.brand_id = '".$brand_id."') AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1");
            /*echo $this->db->last_query();exit;*/
            $result = $query->result_array();
            if(!empty($result)){
                return $result[0]['max_amount'];
            }else{
                return 0;
            }
        }else{ return 0; }
    }

     function get_size($product_id){
        
        $query = $this->db->query("select distinct b.`product_id`,c.`size_text`,b.stock_count,c.`id` from `product_inventory` as b,`product_size` as c where b.`size_id` = c.`id` and b.stock_count > 0 and b.`product_id`='".$product_id."'");
        $res = $query->result_array();
        return $res;

    }


    function get_user_info($user_id){
        $query = $this->db->get_where('user_login', array('id' => $user_id));
        return $query->result_array();
    }

     function get_product_availablility($product_id,$size_id){
        $query = $this->db->query("select sum(stock_count) as stock_count from `product_inventory` where product_id = '".$product_id."' and size_id = '".$size_id."'");
        $res = $query->result_array();
        if(!empty($res)){
            if($res[0]['stock_count'] > 0){
                return 0;
            }else{
                return 1;
            }
        }
    }

    function getBrandExtraInfo($brand_id){
        $query = $this->db->get_where('brand_info', array('user_id' => $brand_id));
        $res = $query->result_array();
        if(!empty($res)){
            return $res;
        }else{
            return '';
        }
    }

    function scremovecart($id){
        if($id > 0){
            $this->db->delete('cart_info', array('id' => $id)); 
        }
    }

    function updatecart($scitem,$quantity,$scsize){
        $data = array(
               'product_size' => $scsize,
               'product_qty' => $quantity
            );

        $this->db->where('id', $scitem);
        $this->db->update('cart_info', $data); 
        return true;
    }

    /*
        Discount Type 
        1 : Cart Discount
        2 : Cart % Discount
        3 : Product Discount
        4 : Product % Discount
    */

    function check_coupon_exist($coupon=null,$bin_no=null,$page_type=null){

        $user_email_id = $this->session->userdata('email');
        $cart_product_cost = 0; $discount_amount = 0; $response = array(); $cart_brand = array();
        if($page_type=='scboxcart')
        {
            if($coupon!='')
            { 
                 $query = $this->db->query("SELECT `id`, `coupon_code`, `coupon_desc`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `individual_use_only`, `exclude_sale_items`, `brand_id`, `products`, `exclude_products`, `prd_category`, `exclude_prd_category`, `email_restriction`, `usage_limit_per_coupon`, `usage_limit_to_items`, `usage_limit_to_user`, `coupon_used_count`, `coupon_email_applied`, `stylecrackerDiscount`, `status`, `is_delete`, `created_datetime`, `created_by`, `modified_datetime`, `modified_by`,`bin_no` FROM `coupon_settings` WHERE 1 and LOWER(coupon_code) = '".$coupon."' and coupon_start_datetime <= now() and coupon_end_datetime >= now() and status = 1 and is_delete = 0 and (is_special_code=1 OR is_special_code=3)  and usage_limit_per_coupon!=coupon_used_count limit 0,1");
             }else
             { 
                 $query = $this->db->query("SELECT `id`, `coupon_code`, `coupon_desc`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `individual_use_only`, `exclude_sale_items`, `brand_id`, `products`, `exclude_products`, `prd_category`, `exclude_prd_category`, `email_restriction`, `usage_limit_per_coupon`, `usage_limit_to_items`, `usage_limit_to_user`, `coupon_used_count`, `coupon_email_applied`, `stylecrackerDiscount`, `status`, `is_delete`, `created_datetime`, `created_by`, `modified_datetime`, `modified_by`,`bin_no` FROM `coupon_settings` WHERE 1  and coupon_start_datetime <= now() and coupon_end_datetime >= now() and status = 1 and is_delete = 0 and (is_special_code=1 OR is_special_code=3)  and usage_limit_per_coupon!=coupon_used_count limit 0,1");
             }
            $res = $query->result_array();

        }else
        {
            $query = $this->db->query("SELECT `id`, `coupon_code`, `coupon_desc`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `individual_use_only`, `exclude_sale_items`, `brand_id`, `products`, `exclude_products`, `prd_category`, `exclude_prd_category`, `email_restriction`, `usage_limit_per_coupon`, `usage_limit_to_items`, `usage_limit_to_user`, `coupon_used_count`, `coupon_email_applied`, `stylecrackerDiscount`, `status`, `is_delete`, `created_datetime`, `created_by`, `modified_datetime`, `modified_by`,`bin_no` FROM `coupon_settings` WHERE 1 and LOWER(coupon_code) = '".$coupon."' and coupon_start_datetime <= now() and coupon_end_datetime >= now() and status = 1 and is_delete = 0 and is_special_code!=1 and usage_limit_per_coupon!=coupon_used_count limit 0,1");
            $res = $query->result_array();
        }
        //echo $this->db->last_query();

        if(!empty($res)){     
            //echo $this->uri->segment(2);exit;
            if($page_type!='' && @$page_type=='scboxcart')
            {
                $products_cost = $this->get_scboxcart();    
            }else
            {
                $products_cost = $this->get_cart();    
            }
           
            $coupon_amt = 0; $coupon_per = 0; $cart_product_cost=0;

            $individual_use_only = $res[0]['individual_use_only'];
            $coupon_email_applied = $res[0]['coupon_email_applied'];
            $brand_id = $res[0]['brand_id'];
            $products = $res[0]['products'];
            $discount_type = $res[0]['discount_type'];
            $coupon_amount = $res[0]['coupon_amount'];
            $min_spend = $res[0]['min_spend'];
            $max_spend = $res[0]['max_spend'];
            $stylecrackerDiscount  = $res[0]['stylecrackerDiscount'];
            $db_bin_no  = $res[0]['bin_no'];


            if($discount_type == '1' || $discount_type == '3'){
                $coupon_amt = $coupon_amount;
            }else{
                $coupon_per = $coupon_amount;
            }


            $cart_users = array();
            $cart_users = explode(',',$coupon_email_applied);
            $coupon = strtoupper($coupon);
            // added for bms and yes bank
            /*if(isset($_COOKIE['discountcoupon_binno']) && $_COOKIE['discountcoupon_binno']!=''){ 
                $bin_no = $_COOKIE['discountcoupon_binno'];
            }

            if(($coupon == 'BMS15' || $coupon == 'YESBANK1000') && $bin_no == ''){
                $response['msg'] = 'This offer is for special users, Please enter valid numbers';
                $response['msg_type'] = '2';
                $response['coupon_code'] = '';
                $response['coupon_discount_amount'] = '';
                return $response;
            }else if(($coupon == BMSCODE || $coupon == YESBANKCODE) && $bin_no != ''){
                $bin_nos = explode(',',$db_bin_no);
                if(!in_array($bin_no,$bin_nos)){
                    $response['msg'] = 'Please enter valid numbers';
                    $response['msg_type'] = '2';
                    $response['coupon_code'] = '';
                    $response['coupon_discount_amount'] = '';
                    return $response;
                }else
                {
                     setcookie('discountcoupon_binno', $bin_no, time() + (86400 * 30), "/"); 
                }
            }*/
            // added for bms and yes bank

            if($individual_use_only == 1 && $coupon_email_applied!='' && $user_email_id=='' && $stylecrackerDiscount!=1){
                setcookie('discountcoupon_stylecracker', '', -1, "/"); 
                $response['msg'] = 'Please login to apply coupon code';
                $response['msg_type'] = '0';
                $response['coupon_code'] = '';
                return $response;
            }else if($individual_use_only == 1 && $coupon_email_applied!='' && $user_email_id!='' && in_array($user_email_id,$cart_users) == 1){
                /* Abendant Cart Discount Userwise*/
                  
                if(!empty($products_cost) && !empty($cart_users)){
                    foreach ($products_cost as $value) {
                        $cart_product_cost = $cart_product_cost + $value['discount_price'];
                    }
                    if($min_spend == 0 && $max_spend==0){
                        $cart_product_cost = $cart_product_cost;   
                    }else if($min_spend <= $cart_product_cost && $max_spend >= $cart_product_cost){
                        $cart_product_cost = $cart_product_cost;  
                    }else{

                        $cart_product_cost = 0;
                    }
                }

                if($cart_product_cost > 0){
                    if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                    }
                }

                if($discount_amount > 0){ 
                    setcookie('discountcoupon_stylecracker', strtoupper($coupon), time() + (86400 * 30), "/"); 
                    $response['msg'] = 'Coupon Applied'; $response['brand_id'] = 0; $response['msg_type'] = '1'; 
                    $response['coupon_code'] = strtoupper($coupon);
                     } else{
                    setcookie('discountcoupon_stylecracker', '', -1, "/"); 
                    $response['msg'] = 'Userwise Discount'; $response['msg_type'] = '1';
                    $response['coupon_code'] = strtoupper($coupon);
                }
                $response['coupon_discount_amount'] = $discount_amount;
                return $response;

            }else if($brand_id == 0 && $products == 0 && $stylecrackerDiscount==1){
                /* StyleCracker Discount */
              
                if(!empty($products_cost)){ 
                    foreach ($products_cost as $value) { 
                        $cart_product_cost = (float)$cart_product_cost+(float)$value['discount_price'];                        
                    } 


                   
                    if($min_spend == 0 && $max_spend==0){ 
                        $cart_product_cost = $cart_product_cost;   
                    }else if($min_spend <= $cart_product_cost && $max_spend >= $cart_product_cost){ 
                        $cart_product_cost = (float)$cart_product_cost;  
                    }else{
                        $cart_product_cost = 0; 
                        // added for bms and yes bank
                       /* if($coupon == YESBANKCODE  && $bin_no != '' && $cart_product_cost>$max_spend && $discount_type=2){
                            $cart_product_cost = $cart_product_cost;
                            $coupon_amt = onlimit_bms_discount;
                            $coupon_amount = onlimit_bms_discount;                            
                        }else
                        {
                            $cart_product_cost = 0; 
                        }       */
                        // added for bms and yes bank                 
                    }
                }
                
                if($cart_product_cost > 0){ 
                    if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $coupon_amount = $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                        $coupon_amount = $coupon_amount/100; 
                    }
                }

                if($discount_amount > 0){ 
                    setcookie('discountcoupon_stylecracker', strtoupper($coupon), time() + (86400 * 30), "/");                    
                    $response['msg'] = 'Coupon Applied';$response['brand_id'] = 0;$response['msg_type'] = '1'; 
                    $response['coupon_code'] = strtoupper($coupon);
                     } else{
                        if($page_type!='scboxcart')
                        {
                             setcookie('discountcoupon_stylecracker', '', -1, "/");
                        } 
                    $response['msg'] = 'Coupon will apply if you spend minimum '.$min_spend.' on StyleCracker';
                    $response['msg_type'] = '2'; 
                    $response['coupon_code'] = '';
                }
                $response['coupon_discount_amount'] = $discount_amount;
                
                return $response;
            }else if($brand_id > 0 && $products == 0){
                /* Brands Discount */
               
                if(!empty($products_cost)){
                    foreach ($products_cost as $value) {
                        if($value['brand_id'] == $brand_id){
                            $cart_product_cost = $cart_product_cost + $value['discount_price'];
                        }
                    }
                    if($min_spend == 0 && $max_spend==0){
                        $cart_product_cost = $cart_product_cost;   
                    }else if($min_spend <= $cart_product_cost && $max_spend >= $cart_product_cost){
                        $cart_product_cost = $cart_product_cost;  
                    }else{
                        $cart_product_cost = 0;
                    }
                }

                if($cart_product_cost > 0 ){
                    if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                    }
                }
                if($discount_amount > 0){
					setcookie('discountcoupon_stylecracker', strtoupper($coupon), time() + (86400 * 30), "/"); 
					$response['msg'] = 'Coupon Applied'; $response['brand_id'] = $brand_id; $response['msg_type'] = '1'; 
					$response['coupon_code'] = strtoupper($coupon);
				} else{
					setcookie('discountcoupon_stylecracker', '', -1, "/"); 
					$response['msg'] = 'Coupon will apply if you spend minimum '.$min_spend.' on '.$this->getBrandExtraInfo($brand_id)[0]['company_name'].'s products';
					$response['msg_type'] = '2';
					$response['coupon_code'] = ''; 
                }
                $response['coupon_discount_amount'] = $discount_amount;
                return $response;
            }else if($brand_id > 0 && $products > 0){
                /* Productwise Discount*/
               
                $cart_products = array();
                $cart_products = explode(',',$products);
                
                  if(!empty($products_cost) && !empty($cart_products)){
                    foreach ($products_cost as $value) {
                        if(in_array($value['product_id'],$cart_products) == true){
                            $cart_product_cost = $cart_product_cost + $value['discount_price'];
                        }
                    }
                    if($min_spend == 0 && $max_spend==0){
                        $cart_product_cost = $cart_product_cost;   
                    }else if($min_spend <= $cart_product_cost && $max_spend >= $cart_product_cost){
                        $cart_product_cost = $cart_product_cost;  
                    }else{
                        $cart_product_cost = 0;
                    }
                }

                if($cart_product_cost > 0 ){
                    if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                    }
                } 
                if($discount_amount > 0){ 
                    setcookie('discountcoupon_stylecracker', strtoupper($coupon), time() + (86400 * 30), "/"); 
                    $response['msg'] = 'Coupon Applied'; $response['brand_id'] = $brand_id; $response['msg_type'] = '1'; 
                    $response['coupon_code'] = strtoupper($coupon);
                } else{

                    setcookie('discountcoupon_stylecracker', '', -1, "/"); 
                    $response['msg'] = 'Coupon code will be applied for specific products'; $response['msg_type'] = '0';
                    $response['coupon_code'] = '';
                }
                $response['coupon_discount_amount'] = $discount_amount;
                
                return $response;
            }

        }else{
            setcookie('discountcoupon_stylecracker', '', -1, "/");  
            $response['msg'] = 'Enter valid coupon code';
            $response['msg_type'] = '2';
            $response['coupon_code'] = '';
            $response['coupon_discount_amount'] = 0;
            return $response;
        }
    }



    function check_coupon_after_order_place($coupon=null,$email=null,$order_unique_no=null){
        $user_email_id = $email;
        $cart_product_cost = 0; $discount_amount = 0; $response = array(); $cart_brand = array();
        $query = $this->db->query("SELECT `id`, `coupon_code`, `coupon_desc`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `individual_use_only`, `exclude_sale_items`, `brand_id`, `products`, `exclude_products`, `prd_category`, `exclude_prd_category`, `email_restriction`, `usage_limit_per_coupon`, `usage_limit_to_items`, `usage_limit_to_user`, `coupon_used_count`, `coupon_email_applied`, `stylecrackerDiscount`, `status`, `is_delete`, `created_datetime`, `created_by`, `modified_datetime`, `modified_by` FROM `coupon_settings` WHERE 1 and LOWER(coupon_code) = '".$coupon."' and coupon_start_datetime <= now() and coupon_end_datetime >= now() and status = 1 and is_delete = 0 and usage_limit_per_coupon!=coupon_used_count limit 0,1");
        $res = $query->result_array();
        //echo $this->db->last_query();
        if(!empty($res)){
            $products_cost = $this->cart_simple_total($order_unique_no);

            $coupon_amt = 0; $coupon_per = 0; 

            $individual_use_only = $res[0]['individual_use_only'];
            $coupon_email_applied = $res[0]['coupon_email_applied'];
            $brand_id = $res[0]['brand_id'];
            $products = $res[0]['products'];
            $discount_type = $res[0]['discount_type'];
            $coupon_amount = $res[0]['coupon_amount'];
            $min_spend = $res[0]['min_spend'];
            $max_spend = $res[0]['max_spend'];
            $stylecrackerDiscount  = $res[0]['stylecrackerDiscount'];


            if($discount_type == '1' || $discount_type == '3'){
                $coupon_amt = $coupon_amount;
            }else{
                $coupon_per = $coupon_amount;
            }

            $cart_users = array();
            $cart_users = explode(',',$coupon_email_applied);

            if($individual_use_only == 1 && $coupon_email_applied!='' && $user_email_id=='' && $stylecrackerDiscount!=1){
                setcookie('discountcoupon_stylecracker', '', -1, "/"); 
                $response['msg'] = 'Please login to apply coupon code';
                $response['msg_type'] = '0';
                $response['coupon_code'] = '';
                return $response;
            }else if($individual_use_only == 1 && $coupon_email_applied!='' && $user_email_id!='' && in_array($user_email_id,$cart_users) == 1){
                /* Abendant Cart Discount Userwise*/
               
                if(!empty($products_cost) && !empty($cart_users)){ 
                    foreach ($products_cost as $value) {
                        $cart_product_cost = $cart_product_cost + $value['discount_price'];
                    }
                    if($min_spend == 0 && $max_spend==0){
                        $cart_product_cost = $cart_product_cost;   
                    }else if($min_spend <= $cart_product_cost && $max_spend >= $cart_product_cost){
                        $cart_product_cost = $cart_product_cost;  
                    }else{
                        $cart_product_cost = 0;
                    }
                }

                if($cart_product_cost > 0){
                    if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                    }
                }
                if($discount_amount > 0){ 
                    setcookie('discountcoupon_stylecracker', strtoupper($coupon), time() + (86400 * 30), "/"); 
                    $response['msg'] = 'Coupon Applied'; $response['brand_id'] = 0; $response['msg_type'] = '1'; 
                    $response['coupon_code'] = strtoupper($coupon);
                     } else{
                    setcookie('discountcoupon_stylecracker', '', -1, "/"); 
                    $response['msg'] = 'Userwise Discount'; $response['msg_type'] = '1';
                    $response['coupon_code'] = strtoupper($coupon);
                }
                $response['coupon_discount_amount'] = $discount_amount;
                return $response;

            }else if($brand_id == 0 && $products == 0 && $stylecrackerDiscount==1){
                /* StyleCracker Discount */
                
                if(!empty($products_cost)){
                    foreach ($products_cost as $value) {
                        $cart_product_cost = $cart_product_cost + $value['discount_price'];
                    }
                    if($min_spend == 0 && $max_spend==0){
                        $cart_product_cost = $cart_product_cost;   
                    }else if($min_spend <= $cart_product_cost && $max_spend >= $cart_product_cost){
                        $cart_product_cost = $cart_product_cost;  
                    }else{
                        $cart_product_cost = 0;
                    }
                }

                if($cart_product_cost > 0){ 
                    if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $coupon_amount = $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                        $coupon_amount = $coupon_amount/100; 
                    }
                }
                if($discount_amount > 0){ 
                    setcookie('discountcoupon_stylecracker', strtoupper($coupon), time() + (86400 * 30), "/"); 
                    $response['msg'] = 'Coupon Applied';$response['brand_id'] = 0;$response['msg_type'] = '1'; 
                    $response['coupon_code'] = strtoupper($coupon);
                     } else{
                    setcookie('discountcoupon_stylecracker', '', -1, "/"); 
                    $response['msg'] = 'Coupon will apply if you spend minimum <span class="fa fa-inr"></span>'.$min_spend.' on StyleCracker';
                    $response['msg_type'] = '0'; 
                    $response['coupon_code'] = '';
                }
                $response['coupon_discount_amount'] = $discount_amount;
                return $response;
            }else if($brand_id > 0 && $products == 0){
                /* Brands Discount */
               
                if(!empty($products_cost)){
                    foreach ($products_cost as $value) {
                        if($value['brand_id'] == $brand_id){
                            $cart_product_cost = $cart_product_cost + $value['discount_price'];
                        }
                    }
                    if($min_spend == 0 && $max_spend==0){
                        $cart_product_cost = $cart_product_cost;   
                    }else if($min_spend <= $cart_product_cost && $max_spend >= $cart_product_cost){
                        $cart_product_cost = $cart_product_cost;  
                    }else{
                        $cart_product_cost = 0;
                    }
                }

                if($cart_product_cost > 0 ){
                    if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                    }
                }
                if($discount_amount > 0){ 
                    setcookie('discountcoupon_stylecracker', strtoupper($coupon), time() + (86400 * 30), "/"); 
                    $response['msg'] = 'Coupon Applied'; $response['brand_id'] = $brand_id; $response['msg_type'] = '1'; 
                    $response['coupon_code'] = strtoupper($coupon);
                    } else{
                    setcookie('discountcoupon_stylecracker', '', -1, "/"); 
                    $response['msg'] = 'Coupon will apply if you spend minimum <span class="fa fa-inr"></span>'.$min_spend.' on '.$this->getBrandExtraInfo($brand_id)[0]['company_name'].'s products';
                     $response['msg_type'] = '0';
                     $response['coupon_code'] = ''; 
                }
                $response['coupon_discount_amount'] = $discount_amount;
                return $response;
            }else if($brand_id > 0 && $products > 0){
                /* Productwise Discount*/
               
                $cart_products = array();
                $cart_products = explode(',',$products);
                
                  if(!empty($products_cost) && !empty($cart_products)){
                    foreach ($products_cost as $value) {
                        if(in_array($value['product_id'],$cart_products) == true){
                            $cart_product_cost = $cart_product_cost + $value['discount_price'];
                        }
                    }
                    if($min_spend == 0 && $max_spend==0){
                        $cart_product_cost = $cart_product_cost;   
                    }else if($min_spend <= $cart_product_cost && $max_spend >= $cart_product_cost){
                        $cart_product_cost = $cart_product_cost;  
                    }else{
                        $cart_product_cost = 0;
                    }
                }

                if($cart_product_cost > 0 ){
                    if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                    }
                } 
                if($discount_amount > 0){ 
                    setcookie('discountcoupon_stylecracker', strtoupper($coupon), time() + (86400 * 30), "/"); 
                    $response['msg'] = 'Coupon Applied'; $response['brand_id'] = $brand_id; $response['msg_type'] = '1'; 
                    $response['coupon_code'] = strtoupper($coupon);
                } else{
                    setcookie('discountcoupon_stylecracker', '', -1, "/"); 
                    $response['msg'] = 'Coupon code will be applied for specific products'; $response['msg_type'] = '0';
                    $response['coupon_code'] = '';
                }
                $response['coupon_discount_amount'] = $discount_amount;

                
                return $response;
            }

        }else{
            setcookie('discountcoupon_stylecracker', '', -1, "/");  
            $response['msg'] = 'No Coupon Found or Expired';
            $response['msg_type'] = '0';
            $response['coupon_code'] = '';
            $response['coupon_discount_amount'] =0;
            return $response;
        }
    }

    function check_email_exist($cartemail){
        $query = $this->db->get_where('user_login', array('email' => $cartemail,'status'=>1));
        $res = $query->result_array();
        if(!empty($res)){
            return 1;
        }else{
            return 0;
        }
    }

    function get_pincode_info($pincode){
        $query = $this->db->query("select b.state_id,b.city from pincode as a,city as b where a.city_id = b.id and a.pincode = '".trim($pincode)."'");
        $res = $query->result_array();
        $data = array();
        if(!empty($res)){
            $data['state_id'] = $res[0]['state_id'];
            $data['city'] = $res[0]['city'];
        }
        return json_encode($data);
    }

    function stylecracker_place_order($data){
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }
        $brandwise_cost = array(); $storetax = 0; $brand_tax_cal = 0; $total_tax = 0; $coupon_discount=0; $total_cod = 0; $cod_charges = 0; $shipping_charges = 0; $prod_info = array(); $order_ids = array(); $order_unique_no = '';$tax_payble_amount = 0;$coupon_code_amt = 0;
        $order_display_no = 'SC'.time();
        if(!empty($data)){
            $cart_info = $data['cart_info'];

            if($user_id == 0){
                $user_id = $this->check_user_exist_or_not($data['user_info']['cart_email']);
            }
            
            $product_info = array(); $brandwise_sub_total = array(); $brandwise_tax = array();
            $brandwise_cod = array(); $brandwise_shipping = array();

            if($user_id == 0 ){
                $scusername = $this->Schome_model->randomPassword();
                $scpassword = $this->Schome_model->randomPassword();
                $ip = $this->input->ip_address();
                $pnfo['firstname'] = $data['user_info']['billing_first_name'];
                $pnfo['lastname'] = $data['user_info']['billing_first_name'];
                $pnfo['scmobile'] = $data['user_info']['billing_mobile_no'];
                $pnfo['birthday'] = '';
                
                $this->Schome_model->registerUser('website',$scusername,$data['user_info']['cart_email'],$scpassword,$ip,2,'',$pnfo);
                $user_id = $this->check_user_exist_or_not($data['user_info']['cart_email']);
            }
            $this->update_user_address($data['user_info'],$user_id);
            if(!empty($cart_info)){
                foreach($cart_info as $val){
                    $brandwise_cost[$val['user_id']][] =  $val['discount_price'];
                    $prod_info[$val['user_id']][] = array(
                                'product_id' => $val['product_id'],
                                'created_datetime' => date('Y-m-d H:i:s'),
                                'user_id' => $user_id,
                                'product_size' => $val['product_size'],
                                'product_qty' => $val['product_qty'],
                                'cart_id' => $val['id'],
                                'product_price' => $val['discount_price'],
                                'discount_percent' => $val['discount_percent'],
                                'compare_price' => $val['compare_price'] > 0 ? $val['compare_price'] : $val['price'],
                                'discount_availed_id' => $val['discount_id'],
                                'sc_discount' => 0,
                        );
                }
                $order_unique_no = $user_id;
                if(!empty($brandwise_cost)) { 
                          foreach($brandwise_cost as $key=>$v){ 
                            $brand_tax_cal =  array_sum($brandwise_cost[$key]);
                           
                            $brandwise_sub_total[$key] =  $brand_tax_cal;

                            $storetax = $this->getBrandExtraInfo($key)[0]['store_tax'];
                            
                            $brandwise_tax[$key] =  $brand_tax_cal*($storetax/100);
                            /* COD charges start*/
                            if($this->getBrandExtraInfo($key)[0]['is_cod'] == 1 && $data['user_info']['pay_mod']=='cod'){    
                                if($this->getBrandExtraInfo($key)[0]['cod_min_value'] == 0 && $this->getBrandExtraInfo($key)[0]['code_max_value'] == 0){

                                    $brandwise_cod[$key] = $this->getBrandExtraInfo($key)[0]['cod_charges'];
                                }else if($this->getBrandExtraInfo($key)[0]['cod_min_value'] >= 0 && $this->getBrandExtraInfo($key)[0]['code_max_value'] > 0 && $this->getBrandExtraInfo($key)[0]['cod_min_value']<=$brand_tax_cal && $this->getBrandExtraInfo($key)[0]['code_max_value']>=$brand_tax_cal){
                                    
                                   
                                    $brandwise_cod[$key] = $this->getBrandExtraInfo($key)[0]['cod_charges'];
                                }
                            }

                            /* Shipping charges start*/
                            if($this->getBrandExtraInfo($key)[0]['is_shipping'] == 1){    
                                if($this->getBrandExtraInfo($key)[0]['shipping_min_values'] == 0 && $this->getBrandExtraInfo($key)[0]['shipping_max_values'] == 0){

                                    $brandwise_shipping[$key] = $this->getBrandExtraInfo($key)[0]['shipping_charges'];

                                }else if($this->getBrandExtraInfo($key)[0]['shipping_min_values'] >= 0 && $this->getBrandExtraInfo($key)[0]['shipping_max_values'] > 0 && $this->getBrandExtraInfo($key)[0]['shipping_min_values']<=$brand_tax_cal && $this->getBrandExtraInfo($key)[0]['shipping_max_values']>=$brand_tax_cal){
                                    

                                    $brandwise_shipping[$key] = $this->getBrandExtraInfo($key)[0]['shipping_charges'];

                                }
                            }
                          }
                      }
                      
                  if($val['user_id'] == SCBOX_BRAND_ID){
                        if(@$data['coupon_code_msg']['brand_id'] == 0){
                            $coupon_code = $data['coupon_code'];
                            $coupon_code_amt = @$data['coupon_code_msg']['coupon_discount_amount'];
                        }else if($data['coupon_code_msg']['brand_id'] > 0 && $val['user_id'] ==$data['coupon_code_msg']['brand_id'] ){
                            $coupon_code = $data['coupon_code'];
                            $coupon_code_amt = $data['coupon_code_msg']['coupon_discount_amount'];
                        }
                        $tax_payble_amount = $val['discount_price'] - $coupon_code_amt;
                        $brandwise_tax[$val['user_id']] = ($storetax / 100) * $tax_payble_amount;
                    } 

                     $order_info = array(); $order_product_info = array(); $order_status = 1;
                     $data['user_info']['pay_mod'] == 'cod' ? $pay_mode = 1 :  $pay_mode = 2;
                     $data['user_info']['pay_mod'] == 'cod' ? $order_status = 1 :  $order_status = 9;
                     if(!empty($brandwise_sub_total)){
                        foreach($brandwise_sub_total as $o=>$u){
                            $order_amt = $u+@$brandwise_tax[$o]+@$brandwise_cod[$o]+@$brandwise_shipping[$o];
                            $coupon_code = '';  $coupon_code_amt = '';
                            
                            if($data['coupon_code']!=''){
                                //echo $data['coupon_code_msg']['brand_id'];
                                if(@$data['coupon_code_msg']['brand_id'] == 0){
                                    $coupon_code = $data['coupon_code'];
                                    $coupon_code_amt = $data['coupon_code_msg']['coupon_discount_amount'];
                                }else if($data['coupon_code_msg']['brand_id'] > 0 && $o ==$data['coupon_code_msg']['brand_id'] ){
                                    $coupon_code = $data['coupon_code'];
                                    $coupon_code_amt = $data['coupon_code_msg']['coupon_discount_amount'];
                                }
                            }
                            $order_info = array(
                                'user_id' => $user_id,
                                'brand_id' => $o,
                                //'order_unique_no' => $order_unique_no,
                                'order_tax_amount' =>@$brandwise_tax[$o] > 0 ? @$brandwise_tax[$o] : 0,
                                'cod_amount' => @$brandwise_cod[$o] >0 ? @$brandwise_cod[$o] : 0,
                                'shipping_amount' => @$brandwise_shipping[$o] > 0 ? @$brandwise_shipping[$o] : 0,
                                'order_sub_total' => $order_amt,
                                'order_total' => $order_amt,
                                'pay_mode'=>$pay_mode,
                                'created_datetime' => date('Y-m-d H:i:s'),
                                'order_status' => $order_status,
                                'first_name' =>$data['user_info']['shipping_first_name'], 
                                'last_name' => $data['user_info']['shipping_last_name'],
                                'email_id' => $data['user_info']['cart_email'],
                                'mobile_no' => $data['user_info']['shipping_mobile_no'],
                                'shipping_address' => $data['user_info']['shipping_address'],
                                'pincode' => $data['user_info']['shipping_pincode_no'],
                                'city_name' => $data['user_info']['shipping_city'],
                                'state_name'=>$data['user_info']['shipping_state'],
                                'coupon_code' => $coupon_code,
                                'coupon_amount' => $coupon_code_amt,
                                'order_display_no' =>$order_display_no,
                                'billing_address' => json_encode($data['user_info'])
                                );
                            $order_id = $this->sc_place_order($order_info);
                            $order_ids[] = $order_id;
                            $order_unique_no = $order_unique_no.'_'.$order_id;
                           if(!empty($prod_info[$o])){
                                foreach ($prod_info[$o] as $cd) {
                                   $cd['order_id'] = $order_id;
                                   $cd['coupon_code'] = $coupon_code;
                                   $this->sc_place_order_info($cd,$pay_mode);
                                }
                            }
                        }
                        
                     }

                     if(!empty($order_ids)){

                        foreach($order_ids as $oval){
                            $oundata = array(
                               'order_unique_no' => $order_unique_no,
                                        );

                            $this->db->where('id', $oval);
                            $this->db->update('order_info', $oundata); 
                        }
                     }

                     
                
            }
        }

        return $order_unique_no;
    }

    function sc_place_order($data){
        if(!empty($data)){
            $this->db->insert('order_info', $data);
            return $this->db->insert_id();
        }
    }

    function sc_place_order_info($data,$pay_mode){
        if(!empty($data)){
            $this->db->insert('order_product_info', $data);
            $last_in_id = $this->db->insert_id();

            if($pay_mode == '1'){
                $product_id = $data['product_id'];
                $product_size = $data['product_size'];

                if($product_id > 0){
                    $query = $this->db->query('update product_inventory set stock_count = (stock_count-1) where product_id = "'.$product_id.'" and size_id = "'.$product_size.'"');
                }
            }

            return $last_in_id;
        }
    }

    function check_user_exist_or_not($email_id){
        if($email_id!=''){
            $check_email = $this->emailUnique($email_id);
                if(!empty($check_email)){
                   return $check_email[0]['id'];
                }else{
                    return 0;
                }

        }
    }

    function emailUnique($emaild){
        $query = $this->db->query('select id,email from scx_user where email ="'.$emaild.'"');
        $username_unique = $query->result_array();
        return $username_unique;
    }

    
    function update_user_address($data,$user_id){
        //$user_id = $this->session->userdata('user_id');
        $query_res = $this->db->get_where('user_address', array('user_id' => $user_id));
        $res_res = $query_res->result_array();


        $query = $this->db->get_where('user_address', array('user_id' => $user_id,'shipping_address'=>$data['shipping_address'],'pincode'=>$data['shipping_pincode_no'],'city_name'=>$data['shipping_city'],'state_name'=>$data['shipping_state'],'first_name'=>$data['shipping_first_name'],'last_name'=>$data['shipping_last_name'],'mobile_no'=>$data['shipping_mobile_no']));

        $res = $query->result_array();
        if(empty($res)){
            $add_data = array(
                    'user_id'=>$user_id,
                    'shipping_address'=>$data['shipping_address'],
                    'pincode'=>$data['shipping_pincode_no'],
                    'city_name'=>$data['shipping_city'],
                    'state_name'=>$data['shipping_state'],
                    'first_name'=>$data['shipping_first_name'],
                    'last_name'=>$data['shipping_last_name'],
                    'mobile_no'=>$data['shipping_mobile_no'],
                    'address_type' =>'S',
                );
            if(empty($res_res)){ $add_data['is_default'] = 1; }
            $this->db->insert('user_address', $add_data);

        }else{
            $add_data = array(
                    'user_id'=>$user_id,
                    'shipping_address'=>$data['shipping_address'],
                    'pincode'=>$data['shipping_pincode_no'],
                    'city_name'=>$data['shipping_city'],
                    'state_name'=>$data['shipping_state'],
                    'first_name'=>$data['shipping_first_name'],
                    'last_name'=>$data['shipping_last_name'],
                    'mobile_no'=>$data['shipping_mobile_no'],
                    'address_type' =>'S',
                );
            $this->db->where(array('id'=>$data['existing_shipping_add'],'user_id'=>$user_id));
            $this->db->update('user_address', $add_data);
        }


        $query = $this->db->get_where('user_address', array('user_id' => $user_id,'shipping_address'=>$data['billing_address'],'pincode'=>$data['billing_pincode_no'],'city_name'=>$data['billing_city'],'state_name'=>$data['billing_state'],'first_name'=>$data['billing_first_name'],'last_name'=>$data['billing_last_name'],'mobile_no'=>$data['billing_mobile_no']));
        $res = $query->result_array();

     

        if(empty($res)){
            $add_data = array(
                    'user_id'=>$user_id,
                    'shipping_address'=>$data['billing_address'],
                    'pincode'=>$data['billing_pincode_no'],
                    'city_name'=>$data['billing_city'],
                    'state_name'=>$data['billing_state'],
                    'first_name'=>$data['billing_first_name'],
                    'last_name'=>$data['billing_last_name'],
                    'mobile_no'=>$data['billing_mobile_no'],
                    'address_type' =>'B',
                );
            $this->db->insert('user_address', $add_data);

        }else{
            $add_data = array(
                    'user_id'=>$user_id,
                    'shipping_address'=>$data['billing_address'],
                    'pincode'=>$data['billing_pincode_no'],
                    'city_name'=>$data['billing_city'],
                    'state_name'=>$data['billing_state'],
                    'first_name'=>$data['billing_first_name'],
                    'last_name'=>$data['billing_last_name'],
                    'mobile_no'=>$data['billing_mobile_no'],
                    'address_type' =>'B',
                );
            $this->db->where(array('id'=>$data['existing_shipping_add'],'user_id'=>$user_id));
            $this->db->update('user_address', $add_data);
        }

    }

    function change_order_status($data){
       // echo '<pre>';print_r($data);        
        if(!empty($data)){

            if(@$data['stylecracker']!='' && $data['stylecracker']==1)
            {
                $mihpayid = $data['mihpayid'];
                $txnid = $data['txnid'];
                $order_unique_no = $data['order_unique_no']; 
                $payudiscount = $data['payudiscount']; 
                $discountsd = $data['discountcoupon_stylecracker'];           
                $offer_codes = unserialize(OFFER_CODES);
            }else
            {
                $mihpayid = $this->input->post('mihpayid');
                $txnid = $this->input->post('txnid');
                $order_unique_no = $this->input->post('udf5'); 
                $payudiscount = $this->input->post('discount'); 
                $discountsd = @$_COOKIE['discountcoupon_stylecracker'];           
                $offer_codes = unserialize(OFFER_CODES);
            }
          
            if($order_unique_no > 0){
                if($payudiscount==0 && in_array($discountsd, $offer_codes))
                {
                    $udata = array(
                   'order_status' => 1,
                   'mihpayid' => $mihpayid,
                   'txnid' => $txnid,
                   'coupon_code' => '',
                   'coupon_amount' => 0.00
                    );
                }else
                {
                    $udata = array(
                   'order_status' => 1,
                   'mihpayid' => $mihpayid,
                   'txnid' => $txnid
                     );
                }
                

                $this->db->where('order_unique_no', $order_unique_no);
                $this->db->update('order_info', $udata); 

                $this->update_order_id_in_cart($order_unique_no);

                $query = $this->db->get_where('order_info', array('order_unique_no'=> $order_unique_no));
                $res = $query->result_array();

                if(!empty($res)){

                    foreach ($res as $value) {
                        
                        if($payudiscount==0 && in_array($discountsd, $offer_codes))
                        {  
                            $this->db->where('order_id', $value['id']); 
                            $this->db->update('order_product_info', array('coupon_code' => '')); 
                        }

                        $query = $this->db->get_where('order_product_info', array('order_id' => $value['id']));
                        $prores = $query->result_array();
                                               

                        if(!empty($prores)){

                            foreach ($prores as $pvalue) {
                                
                                $product_id = $pvalue['product_id'];
                                $product_size = $pvalue['product_size'];

                                if($product_id > 0){
                                   $query = $this->db->query('update product_inventory set stock_count = (stock_count-1) where product_id = "'.$product_id.'" and size_id = "'.$product_size.'"');
                                }

                            }

                        }

                    }

                }

            }
        }
    }


    function su_get_order_info($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where order_unique_no = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

            $order_ids = implode(",",$order_info);

            /*$query = $this->db->query('select a.`order_id`,b.price, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`, a.`product_price`,b.`name`,b.`image`,b.`brand_id`,c.size_text,d.company_name,d.min_del_days,d.max_del_days from order_product_info as a,product_desc as b,product_size as c,`brand_info` as d where a.`product_id` = b.`id` and a.`order_id` in ('.$order_ids.') and a.`user_id`='.$user_id.' and a.product_size = c.`id` and d.`user_id` = b.`brand_id`' );*/
           $query = $this->db->query('select a.`order_id`, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`,  a.`product_price`,b.`name`,b.`image`,b.`brand_id`,c.size_text,d.company_name,d.min_del_days,d.max_del_days,e.order_tax_amount, e.cod_amount, e.shipping_amount,e.order_total,(select a.`product_qty`*(b.price-((b.price*discount_percent)/100)) from discount_availed as da where da.brand_id = b.brand_id AND NOW() >= da.start_date AND NOW() <= da.end_date AND (da.discount_type_id=1 ) AND da.status = 1 AND da.is_delete=0 )as discount_price,a.coupon_code,a.created_datetime,a.modified_datetime  from order_product_info as a,product_desc as b,product_size as c,`brand_info` as d,order_info as e where a.`product_id` = b.`id` and a.`order_id` in ('.$order_ids.') and a.`user_id`='.$user_id.' and a.product_size = c.`id` and d.`user_id` = b.`brand_id` and a.`order_id`=e.`id` ' );
            $query_res = $query->result_array();
            return $query_res;

        }
    }

        function br_get_order_info($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where id = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

            $order_ids = implode(",",$order_info);

           /* $query = $this->db->query('select a.`order_id`, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`, a.`product_price`,b.`name`,b.`image`,b.`brand_id`,c.size_text,d.company_name from order_product_info as a,product_desc as b,product_size as c,`brand_info` as d where a.`product_id` = b.`id` and a.`order_id` in ('.$order_ids.') and a.`user_id`='.$user_id.' and a.product_size = c.`id` and d.`user_id` = b.`brand_id`' );*/
           $query = $this->db->query('select a.`order_id`, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`,  a.`product_price`,b.`name`,b.`image`,b.`brand_id`,c.size_text,d.company_name,d.min_del_days,d.max_del_days,e.order_tax_amount, e.cod_amount, e.shipping_amount,e.order_total,(select a.`product_qty`*(b.price-((b.price*discount_percent)/100)) from discount_availed as da where da.brand_id = b.brand_id AND NOW() >= da.start_date AND NOW() <= da.end_date AND (da.discount_type_id=1 ) AND da.status = 1 AND da.is_delete=0 )as discount_price,a.coupon_code  from order_product_info as a,product_desc as b,product_size as c,`brand_info` as d,order_info as e where a.`product_id` = b.`id` and a.`order_id` in ('.$order_ids.') and a.`user_id`='.$user_id.' and a.product_size = c.`id` and d.`user_id` = b.`brand_id` and a.`order_id`=e.`id` ' );

            $query_res = $query->result_array();

            return $query_res;

        }
    }

    function su_get_order_price_info($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where order_unique_no = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info);

        /*$query = $this->db->query('select sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no, a.shipping_address,a.pincode,a.city_name,s.state_name,a.pay_mode,a.order_status,a.user_id,a.state_name as state_id,a.it_state,a.country_name,a.confirm_mobile_no,a.order_display_no from order_info as a,states as s where a.`state_name` = s.`id` and a.id in ('.$order_ids.')' );*/
        $query = $this->db->query('select sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no, a.shipping_address,a.pincode,a.city_name,s.state_name,a.pay_mode,a.order_status,a.user_id,a.state_name as state_id,a.it_state,a.country_name,a.confirm_mobile_no,a.billing_address,a.order_display_no from order_info as a,states as s where a.`state_name` = s.`id` and a.id in ('.$order_ids.')' );

            $query_res = $query->result_array();
            return $query_res;
        }
    }

       function br_get_order_price_info($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where id = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info);

        $query = $this->db->query('select a.id,a.created_datetime,sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no, a.shipping_address,a.pincode,a.city_name,a.state_name as state_id,a.it_state,a.country_name,s.state_name,a.brand_id,a.pay_mode,a.order_display_no from order_info as a,states as s where a.`state_name` = s.`id` and a.id in ('.$order_ids.')' );

            $query_res = $query->result_array();
            return $query_res;
        }
    }

    function getUserCoupon($couponCode, $CbrandId = "")
    {

      if($CbrandId!='')
      {
        //$brand_cond =' AND `brand_id`='.$CbrandId;
        $brand_cond =' AND ( `brand_id` = "'.$CbrandId.'" || brand_id=0) ';
      }else
      {
        $brand_cond = '';
      }
        $query = $this->db->query("SELECT `coupon_code`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `brand_id`, `products`, `exclude_products`,`coupon_used_count`,`usage_limit_per_coupon`,`stylecrackerDiscount`,`individual_use_only` FROM `coupon_settings` WHERE NOW() BETWEEN `coupon_start_datetime` AND `coupon_end_datetime` AND`status`=1 AND `is_delete`=0  AND `coupon_code`='".$couponCode."' ".$brand_cond."; "); 
            //return $this->db->last_query();
            $result = $query->result_array();
           
            if(!empty($result))
            {
              /* $coupon_info['coupon_products'] =  $coupon_products.$result[0]['products'];*/
               $coupon_info['coupon_amount'] = $result[0]['coupon_amount']; 
               $coupon_info['brand_id'] = $result[0]['brand_id']; 
               $coupon_info['coupon_products'] = $result[0]['products'];
               $coupon_info['coupon_min_spend'] = $result[0]['min_spend'];
               $coupon_info['coupon_max_spend'] = $result[0]['max_spend'];
               $coupon_info['coupon_discount_type'] = $result[0]['discount_type'];
               $coupon_info['coupon_used_count'] = $result[0]['coupon_used_count'];
               $coupon_info['usage_limit_per_coupon'] = $result[0]['usage_limit_per_coupon'];
                     $coupon_info['stylecrackerDiscount'] = $result[0]['stylecrackerDiscount'];
               $coupon_info['individual_use_only'] = $result[0]['individual_use_only'];
            }  

            if(!empty($coupon_info))
            {
              return $coupon_info;
            }else
            {
                return 0;
            }               
    }

     function getProductSku($productId,$productSize)
    {
        if($productId!="" && $productSize!="")
        {
          $query = $this->db->query("select product_sku from product_inventory where product_id='".$productId."' AND size_id='".$productSize."'; ");
           /* echo $this->db->last_query();*/
            $result = $query->result_array();           
        }   

        if($result!="")
        {
          return @$result[0]['product_sku'];
        }else
        {
          return '';
        }     
          
    }

    function delete_address($id){
        if($id > 0){
            $this->db->delete('user_address', array('id' => $id));
        }
    }

    function get_address($id){
        if($id > 0){
            $query = $this->db->get_where('user_address', array('id' => $id));
            $res = $query->result_array();
            return json_encode($res);
        }
    }

    function remove_nonremoval_cart($productid){
        $user_id = $this->session->userdata('user_id');
        if(isset($_COOKIE['SCUniqueID'])){
            $uniquecookie = $_COOKIE['SCUniqueID'];
            if($uniquecookie!='' && $productid!=''){
                if($user_id == '' || $user_id ==0){
                $query = $this->db->query("delete from cart_info where (SCUniqueID='".$uniquecookie."') and product_id != '".$productid."'");
                }else{
                   $query = $this->db->query("delete from cart_info where (SCUniqueID='".$uniquecookie."' || user_id = '".$user_id."') and product_id != '".$productid."'"); 
                }
                
            }
        }
    }

    function delete_unsuccessfully_order($uniquecookie){
           if($uniquecookie >0 ){
                $query = $this->db->get_where('order_info', array('order_unique_no' => $uniquecookie));
                $res = $query->result_array();

                if(!empty($res)){
                    foreach ($res as $key => $value) {
                        if($value['id'] >0 ){
                            $this->db->delete('order_product_info', array('order_id' => $value['id'])); 
                        }
                    }
                }
                $this->db->delete('order_info', array('order_unique_no' => $uniquecookie)); 

           }
           return true; 
    }

    function scgenrate_otp($mobile,$otp_no){

        $time = new DateTime(date('Y-m-d H:i:s'));
        $time->add(new DateInterval('PT330M'));
        $dateM = $time->format('Y-m-d H:i:s');

        $data = array('mobile_no'=>$mobile,'otp_no'=>$otp_no,'created_datetime'=>$dateM);
        $this->db->insert('user_mobile_otp', $data);
        return true;
    }

    function sc_check_otp($mobile,$otp_no,$order_id){
        $query = $this->db->query('select count(*) as cnt from `user_mobile_otp` where `mobile_no`="'.$mobile.'" and `otp_no`="'.$otp_no.'" and status = 0 order by id desc');
        $res = $query->result_array();
        if(!empty($res)){
            if($res['0']['cnt'] == 0){
                return 0;
            }else{

                if($mobile > 0){
                    $query = $this->db->query('update `user_mobile_otp` set status = 1 where `mobile_no`="'.$mobile.'"');
                }
               if($order_id > 0){
                    $query = $this->db->query('update `order_info` set confirm_mobile_no = 1 where `order_unique_no`="'.$order_id.'"');                
               }
                return 1;
            }
        }
        
    }

   function cart_total($order_unique_no=null,$page_type=null){
    $offer_codes = unserialize(OFFER_CODES);
        if($order_unique_no > 0){
            $cinfo = array();
            $query = $this->db->query('select sum(order_total) as order_total,coupon_code from order_info where order_unique_no ="'.$order_unique_no.'"');
            $res = $query->result_array();
            if(!empty($res)){

                $query1 = $this->db->query('select coupon_code from order_info where order_unique_no ="'.$order_unique_no.'" and coupon_code!="" limit 0,1');
                $res1 = $query1->result_array();

                if(@$res1[0]['coupon_code']!='' && @$res1[0]['coupon_code']!=null){
                $cinfo = $this->check_coupon_exist($res1[0]['coupon_code'],'',$page_type); }
                if(!empty($cinfo) && isset($cinfo['coupon_discount_amount']) == TRUE){
                    if(in_array($res1[0]['coupon_code'], $offer_codes))
                    {
                         return $res[0]['order_total'];
                    }else
                    {
                         return $res[0]['order_total']-$cinfo['coupon_discount_amount'];
                    }
                   
                }else{
                    return $res[0]['order_total'];
                }
            }else{
                return 0;
            }
        }
    }

    function cart_simple_total($order_unique_no=null){
        
            $uniquecookie = $order_unique_no; 
            $uniquecookie_ = '';
            $order_ids = explode('_', $uniquecookie);
            array_shift($order_ids); 
            if(!empty($order_ids)){ $i = 0;
                foreach ($order_ids as $key => $value) {
                    if($i!=0){ $uniquecookie_ = $uniquecookie_.','; }
                    $uniquecookie_ = $uniquecookie_.$value;
                    $i++;
                }
            }
        
        $query = $this->db->query('select d.cod_available,d.shipping_exc_location,d.cod_exc_location,b.brand_id,b.slug,a.`id`,a.`product_id`,d.`return_policy`,a.`product_qty`,b.`name`,(a.`product_qty`*b.price) as price,CASE WHEN  DATE_FORMAT(b.discount_start_date,"%Y-%m-%d  %h:%i") <= DATE_FORMAT(now(),"%Y-%m-%d %h:%i") and DATE_FORMAT(b.discount_end_date,"%Y-%m-%d  %h:%i") >= DATE_FORMAT(now(),"%Y-%m-%d %h:%i") then (a.`product_qty`*b.compare_price) ELSE 0 END AS compare_price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,b.`id` as pid,b.`price` as product_price,c.id as size_id from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and b.status = 1 and a.`order_id` IN ('.$uniquecookie_.') order by b.`brand_id`');
        $res = $query->result_array();
        
        $product_data = array();
            if(!empty($res)){ $i = 0;
                foreach ($res as $key => $value) {
                    $product_data[$i]['id'] = $value['id'];
                    $product_data[$i]['product_id'] = $value['product_id'];
                    $product_data[$i]['cod_available'] = $value['cod_available'];
                    $product_data[$i]['shipping_exc_location'] = $value['shipping_exc_location'];
                    $product_data[$i]['cod_exc_location'] = $value['cod_exc_location'];
                    $product_data[$i]['slug'] = $value['slug'];
                    $product_data[$i]['product_qty'] = $value['product_qty'];
                    $product_data[$i]['return_policy'] = $value['return_policy'];
                    $product_data[$i]['name'] =$value['name'];
                    $product_data[$i]['price'] = $value['compare_price'] > $value['product_price'] ?  $value['compare_price'] : $value['product_price'];
                    $product_data[$i]['image'] =$value['image'];
                    $product_data[$i]['size_text'] =$value['size_text'];
                    $product_data[$i]['company_name'] =$value['company_name'];
                    $product_data[$i]['user_id'] =$value['user_id'];
                    $product_data[$i]['min_del_days'] =$value['min_del_days'];
                    $product_data[$i]['max_del_days'] =$value['max_del_days'];
                    $product_data[$i]['store_tax'] =$value['store_tax'];
                    $product_data[$i]['is_shipping'] =$value['is_shipping'];
                    $product_data[$i]['shipping_charges'] =$value['shipping_charges'];
                    $product_data[$i]['shipping_min_values'] =$value['shipping_min_values'];
                    $product_data[$i]['shipping_max_values'] =$value['shipping_max_values'];
                    $product_data[$i]['is_cod'] =$value['is_cod'];
                    $product_data[$i]['cod_charges'] =$value['cod_charges'];
                    $product_data[$i]['cod_min_value'] =$value['cod_min_value'];
                    $product_data[$i]['code_max_value'] =$value['code_max_value'];
                    $product_data[$i]['product_size'] =$value['product_size'];
                    $discount_percent = $this->get_discount_percentage($value['user_id'],$value['pid']);
                    $product_data[$i]['discount_price'] =round($value['product_qty']*($value['product_price']-($value['product_price']*$discount_percent)/100));
                    $product_data[$i]['discount_percent'] = $this->product_desc_model->calculate_discount_percent($product_data[$i]['price'],$product_data[$i]['discount_price'],$value['price']);

                    $product_data[$i]['discount_type'] =  $this->get_discount_id($value['user_id'],$value['pid'])['discount_type_id'];

                    if($product_data[$i]['discount_type'] == 1){
                     $product_data[$i]['discount_id'] =  $this->get_discount_id($value['user_id'],0)['id'];
                    }else if($product_data[$i]['discount_type'] == 2){
                        $product_data[$i]['discount_id'] =  $this->get_discount_id(0,$value['pid'])['id'];
                    }else{
                         $product_data[$i]['discount_id'] =0;
                         $product_data[$i]['discount_type'] =0;
                    }
                    $product_data[$i]['discount_max_amount'] = $this->get_discount_maxvalue($value['user_id'],$value['pid']);
                    $product_data[$i]['product_all_size'] = $this->get_size($value['product_id']);
                    $product_data[$i]['brand_unique_name'] =$this->get_user_info($value['user_id'])[0]['user_name'];
                    $product_data[$i]['in_stock'] = $this->get_product_availablility($value['product_id'],$value['size_id']);
                    $product_data[$i]['brand_id'] =$value['brand_id'];
                    /*$product_data[$i]['coupon_amount'] = $this->getCouponData()*/

                    $i++;
                }
               /* $product_data['coupon_amount'] = $this->getCouponData($coupon_code)['coupon_amount'];*/
               /*echo "<pre>";print_r($product_data);*/
            }
        return $product_data;
    }

       function cart_coupon($order_unique_no=null){
        if($order_unique_no > 0){
            $cinfo = array();
            
                $query1 = $this->db->query('select coupon_code,email_id from order_info where order_unique_no ="'.$order_unique_no.'" and coupon_code!="" limit 0,1');
                $res1 = $query1->result_array();
                if($res1[0]['coupon_code']!='' && $res1[0]['coupon_code']!=null){
                $cinfo = $this->check_coupon_after_order_place(@$res1[0]['coupon_code'],$res1[0]['email_id'],$order_unique_no); 
                
                }
                if(!empty($cinfo) && isset($cinfo['coupon_discount_amount']) == TRUE){
                    return $cinfo['coupon_discount_amount'];
                }else{
                    return 0;
                }
            
        }
    }


     function cart_coupon_code($order_unique_no=null){
        if($order_unique_no > 0){
            $cinfo = array();
            
                $query1 = $this->db->query('select coupon_code,email_id from order_info where order_unique_no ="'.$order_unique_no.'" and coupon_code!="" limit 0,1');
                $res1 = $query1->result_array();
                if($res1[0]['coupon_code']!='' && $res1[0]['coupon_code']!=null){
                $cinfo = $this->check_coupon_after_order_place($res1[0]['coupon_code'],$res1[0]['email_id'],$order_unique_no); 
                
                }
                if(!empty($cinfo) && isset($cinfo['coupon_discount_amount']) == TRUE){
                    return $res1[0]['coupon_code'];
                }else{
                    return 0;
                }
            
        }
    }

    function calculate_product_discountAmt($product_price,$totalproductprice,$discountType,$discountAmt)
    { //echo $product_price.'--'.$totalproductprice.'--'.$discountType.'--'.$discountAmt.'<br>';
      if($product_price!='' && $totalproductprice!=0 && $discountAmt!='')
      {  

        if($discountType==1)
        {
            $percent = ($product_price*100)/$totalproductprice;
            $DiscountAmt = ($discountAmt*$percent)/100;
            $productDiscountAmt = $product_price-$DiscountAmt;
            return $productDiscountAmt;

        }else if($discountType==2)   
        {     
            $percent = $discountAmt;  
            $DiscountAmt = ($product_price*$percent)/100;
            $productDiscountAmt = $product_price-$DiscountAmt;  
            return $productDiscountAmt;
        }   
       
      }else
      {
        return 0;
      }
    }

     function calculate_product_discountAmt_percent($product_price,$count,$totalproductprice,$discountType,$discountAmt)
    { //echo $product_price.'--'.$totalproductprice.'--'.$discountType.'--'.$discountAmt.'<br>';
      if($product_price!='' && $totalproductprice!=0 && $discountAmt!='')
      {  

        if($discountType==2)   
        {     
            $percent = $discountAmt;  
            //$DiscountAmt = ($totalproductprice*$percent)/100;
            //$productDiscountAmt = $totalproductprice-$DiscountAmt;
            //$productDiscountAmt = $product_price - ($productDiscountAmt/$count);
            $DiscountAmt = ($product_price*$percent)/100;
            $productDiscountAmt = $product_price-$DiscountAmt;
            return $productDiscountAmt;
        }   
       
      }else
      {
        return 0;
      }
    }    

    function deleteEntireCart(){
        $user_id = $this->session->userdata('user_id') > 0 ? $this->session->userdata('user_id') : 0;
        $uniquecookie = $_COOKIE['SCUniqueID'];
         if($uniquecookie!=''){
            //$this->db->delete('cart_info', array('SCUniqueID' => $uniquecookie)); 
            if($user_id == '' || $user_id ==0){
                $query = $this->db->query('delete from cart_info where (SCUniqueID = "'.$uniquecookie.'")');
            }else{
                $query = $this->db->query('delete from cart_info where (SCUniqueID = "'.$uniquecookie.'" || user_id = "'.$user_id.'")');
            }
        }
    }

    function update_order_id_in_cart($order_unique_no){
        if($order_unique_no > 0){
            $query = $this->db->query('select id from `order_info` where order_unique_no="'.$order_unique_no.'"');
            $res = $query->result_array();

            if(!empty($res)){
                foreach ($res as $value) {
                    $query1 = $this->db->query('select order_id,cart_id from `order_product_info` where order_id="'.$value['id'].'"');
                    $res1 = $query1->result_array();

                    if(!empty($res1)){
                        foreach($res1 as $val){
                            $this->db->query('update cart_info set order_id ="'.$val['order_id'].'" where id = "'.$val['cart_id'].'"');
                            $this->db->query('update fail_order set status =1 where cart_id = "'.$val['cart_id'].'"');

                        }
                    }
                }
            }

        }
    }

    function failed_order($first_name,$last_name,$email_id,$address,$SCUniqueID,$mobile_no,$paymod,$uc_pincode,$uc_city,$country=null,$uc_state2=null){
        $user_id = $this->session->userdata('user_id') > 0 ? $this->session->userdata('user_id') : 0;
        if($user_id == '' || $user_id ==0){
            $query = $this->db->query("select id from cart_info where (`SCUniqueID` = '".$SCUniqueID."') AND `order_id` is null");
        }else{
            $query = $this->db->query("select id from cart_info where (`SCUniqueID` = '".$SCUniqueID."' || `user_id`='".$user_id."') AND `order_id` is null");
        }
        $result = $query->result_array();
        $var = $address.','.$uc_pincode.','.$uc_city;
        if(!empty($result))
        {
        $i= 0;
         foreach($result as $row)
         {  $date = date('Y-m-d H:i:s');
             $add_data = array(
                    'SCUniqueID'=>$SCUniqueID,
                    'cart_id'=>$result[$i]['id'],
                    'first_name'=>$first_name,
                    'last_name'=>$last_name,
                    'email_id'=>$email_id,
                    'address'=>$var,
                    'it_state'=>$uc_state2,
                    'country'=>$country,
                    'mobile_no'=>$mobile_no,
                    'created_datetime'=>$date,
                    'paymod'=>$paymod,
                );
            $this->db->insert('fail_order', $add_data);
            $i++;
         }
        }
        return true;
    }

    function edit_users_address($data,$id){
        if(!empty($data) && $id >0){
            $this->db->where('id', $id);
            $this->db->update('user_address',$data); 
        }
    }

    function updateCouponInc($order_unique_no){
        if($order_unique_no > 0){
        $query = $this->db->query("select distinct coupon_code,brand_id from order_info where order_unique_no='".$order_unique_no."'");
        $res = $query->result_array();
        if(!empty($res)){
            foreach ($res as $value) {
                $coupon_amount = strtolower($value['coupon_code']);
                $brand_id = $value['brand_id'];

                if($coupon_amount!='' && $coupon_amount!='NULL'){
                     $query = $this->db->query("update coupon_settings set coupon_used_count= coupon_used_count + 1 where LOWER(coupon_code) ='".$coupon_amount."'");

                }

            }
        }
        }
    }

    function su_get_order_price_info_res($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where order_unique_no = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info);

        $query = $this->db->query('select sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no,a.brand_id, a.coupon_code,a.coupon_amount,a.shipping_address,a.pincode,a.city_name,s.state_name,a.pay_mode,a.order_status,a.user_id,a.state_name as state_id,a.it_state,a.country_name,a.confirm_mobile_no,a.order_display_no from order_info as a,states as s where a.`state_name` = s.`id` and a.id in ('.$order_ids.') group by a.brand_id' );

            $query_res = $query->result_array();
            return $query_res;
        }
    }

     function calculateCouponAmount($cartproduct=array(),$totalProductPrice=null)
    { 

      if(!empty($cartproduct))
      {
        $total_product_price = 0; $coupon_product_price = 0;
        foreach($cartproduct as $val)
        {
            if(@$val['discount_percent']!='' || @$val['discount_percent']!=0)
            {
               $price = $val['discount_price'];            
            }else
            {
               $price = $val['product_price'];            
            }
            $total_product_price = $total_product_price+$price;
        }
        foreach($cartproduct as $val)
        {

          if(@$val['discount_percent']!='' || @$val['discount_percent']!=0)
          {
             $price = $val['discount_price'];            
          }else
          {
             $price = $val['product_price'];            
          }

           $coupon_info = $this->getUserCoupon($val['coupon_code'],$val['brand_id']);
            $coupon_brand = $coupon_info['brand_id'];
            $coupon_products = $coupon_info['coupon_products'];
            if($coupon_products!='' || $coupon_products!=0)
            {
              $coupon_products_arr =explode(',',$coupon_products);
            }
            $coupon_min_spend = $coupon_info['coupon_min_spend'];
            $coupon_max_spend = $coupon_info['coupon_max_spend'];
            $coupon_discount_type = $coupon_info['coupon_discount_type']; 
            $coupon_stylecracker =  $coupon_info['stylecrackerDiscount'];
            $individual_use_only =  $coupon_info['individual_use_only']; 

          if(isset($val['coupon_code']) && $val['coupon_code']!='')
          {
           
            /*$data['coupon_discount'] = $coupon_info['coupon_amount'];
            $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];*/
          
            $data['coupon_code'] = $val['coupon_code'];           

              if($coupon_brand==$val['brand_id'] && $coupon_brand!=0)
              {        
                if($coupon_products!=''&& $coupon_products!=0)
                {
                   //coupon_discount_type =3 (Product discount)
                  if($coupon_discount_type==3)
                  {
                    if(in_array($val['product_id'], $coupon_products_arr))
                    {              
                      if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                      {
                        $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                        $data['coupon_amount'] =  $coupon_info['coupon_amount'];                      
                        $data['coupon_discount'] = $coupon_info['coupon_amount'];  
                        $data['coupon_code'] = $val['coupon_code'];
                        $data['coupon_brand'] = $coupon_brand;                  
                      } 
                    }
                  } 
                   //coupon_discount_type =4 (Product % discount)
                  if($coupon_discount_type==4)
                  {
                    if(in_array($val['product_id'], $coupon_products_arr))
                    {              
                      if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                      {
                        $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                        $data['coupon_amount'] =  $coupon_info['coupon_amount'];  
                        $coupon_percent = $coupon_info['coupon_amount'];
                        $data['coupon_discount'] = $price*($coupon_percent/100); 
                        $data['coupon_code'] = $val['coupon_code']; 
                        $data['coupon_brand'] = $coupon_brand;                     
                      } 
                    }
                  }                 

                }else
                { 
                   //coupon_discount_type =1 (cart discount)
                  if($coupon_discount_type==1)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_info['coupon_amount']; 
                      $data['coupon_code'] = $val['coupon_code'];
                      $data['coupon_brand'] = $coupon_brand;                   
                    }
                  }
                   //coupon_discount_type =2 (cart % discount)
                  if($coupon_discount_type==2)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];  
                      $coupon_percent = $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];
                      $data['coupon_brand'] = $coupon_brand;                    
                    }
                  }
                }          
              }

             /*  if($coupon_brand==0 && strtolower($val['coupon_code'])=='sccart20')
              { 
                   $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $coupon_percent = $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];                      
                    }
              }*/
        
       if(($coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only!=1  )|| ( $coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only==1 ))        
              { 
                $coupon_product_price = $totalProductPrice;

                     //coupon_discount_type =1 (cart discount)
                  if($coupon_discount_type==1)
                  {
                    
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    { 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];            
                      $data['coupon_discount'] = $coupon_info['coupon_amount']; 
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                      $data['coupon_code'] = $val['coupon_code']; 
                      $data['coupon_stylecracker'] = $coupon_stylecracker;
                      $data['coupon_brand'] = $coupon_brand;

                    }
                  }
                   //coupon_discount_type =2 (cart % discount)
                  if($coupon_discount_type==2)
                  {                   
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    { 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];                       
                      $coupon_percent = $coupon_info['coupon_amount']; 
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];  
                      $data['coupon_discount'] = $totalProductPrice*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];
                      $data['coupon_stylecracker'] = $coupon_stylecracker;
                      $data['coupon_brand'] = $coupon_brand;                      
                    }
                  }
              }
              

          }else
          {
            if(!isset($data['coupon_code']))
            {
                $data['coupon_discount']=0;
                $data['coupon_code'] = '';
            }
          }         
        }      
      } 
      /*echo '<pre>';print_r($data);exit;*/
      return $data;      
  }

      /*Referal Start*/
    function get_refer_point(){
        $user_id = $this->session->userdata('user_id');
        $email = $this->session->userdata('email');

        $query = $this->db->query("SELECT confirm_email from user_login where email = '".$email."' limit 0,1");
        $res = $query->result_array();

        if($user_id && !empty($res) && @$res[0]['confirm_email'] == 1){
            $query = $this->db->query("SELECT GROUP_CONCAT(source_email) as source_email FROM referral_info where user_id=".$user_id." and type!='debit' and status!='done' group by user_id");
            $res = $query->result_array();
            if(!empty($res)){
                //$referral_email =  $res[0]['source_email'];
                //$referral_email = explode(',', $referral_email);
                //$referral_email = implode("','", $referral_email);
                /*$query1 = $this->db->query("select b.points from user_login as a,referral_info as b where a.confirm_email = 1 and a.email IN('".$referral_email."') and b.status!='done' and a.id = b.user_id and b.type='credit'");*/

                 $query1 = $this->db->query("select a.points,(select c.confirm_email from user_login as c where c.email = source_email ) as source_email_confirm from referral_info as a,user_login as b where a.user_id = ".$user_id." AND b.id = a.user_id AND a.status != 'done' and a.type='credit'");

                $res1 = $query1->result_array();
                if(!empty($res1)){
                    return 1;    
                }
            }
        }

        return 0;
    }
   

    /*function redeem_points($unique_id){
        $user_id = $this->session->userdata('user_id');
        $query = $this->db->query("SELECT GROUP_CONCAT(source_email) as source_email FROM referral_info where user_id=".$user_id." and type!='debit' and status!='done' group by user_id");
        $res = $query->result_array();
        if(!empty($res)){
            $referral_email =  $res[0]['source_email'];
            $referral_email = explode(',', $referral_email);
            $referral_email = implode("','", $referral_email);
            $query1 = $this->db->query("select b.points,a.email from user_login as a,referral_info as b where a.confirm_email = 1 and a.email IN('".$referral_email."') and b.status!='done' and a.id = b.user_id and b.type='credit' limit 0,1");
            $res1 = $query1->result_array();
            if(!empty($res1)){
                foreach($res1 as $val){

                    $query2 = $this->db->query("select `id`, `user_id`, `user_email`, `type`, `description`, `points`, `date`, `source`, `source_id`, `source_email`, `status`, `invite_order_id` from referral_info where user_id=".$user_id." and source_email = '".trim($val['email'])."' and type='credit'");
                   $res2 = $query2->result_array();
                   if(!empty($res2)){
                   $data = array();
                   $data = array(
                        'user_id' => $res2[0]['user_id'],
                        'user_email'=>$res2[0]['user_email'],
                        'type'=>'debit',
                        'description'=>$res2[0]['description'],
                        'points'=>POINT_VAlUE,
                        'source'=>'order',
                        'source_id'=>$res2[0]['source_id'],
                        'source_email'=>$res2[0]['source_email'],
                        'status'=>'done',
                        'invite_order_id'=>$unique_id
                    );
                    $this->db->insert('referral_info', $data);

                    $data1=array('status'=>'done');
                    $this->db->where('id',$res2[0]['id']);
                    $this->db->update('referral_info',$data1);
                    
                    }
                } 
            }
        }
       
    }*/

    function redeem_points($unique_id){
         $user_id = $this->session->userdata('user_id');
          $query2 = $this->db->query("select `id`, `user_id`, `user_email`, `type`, `description`, `points`, `date`, `source`, `source_id`, `source_email`, `status`, `invite_order_id` from referral_info where user_id=".$user_id." and type='credit' and `status`!='done' limit 0,1");
                   $res2 = $query2->result_array();
                   if(!empty($res2)){
                   $data = array();
                   $data = array(
                        'user_id' => $res2[0]['user_id'],
                        'user_email'=>$res2[0]['user_email'],
                        'type'=>'debit',
                        'description'=>$res2[0]['description'],
                        'points'=>POINT_VAlUE,
                        'source'=>'order',
                        'source_id'=>$res2[0]['source_id'],
                        'source_email'=>$res2[0]['source_email'],
                        'status'=>'done',
                        'invite_order_id'=>$unique_id
                    );
                    $this->db->insert('referral_info', $data);

                    $data1=array('status'=>'done');
                    $this->db->where('id',$res2[0]['id']);
                    $this->db->update('referral_info',$data1);
                    
                    }
    }

    function get_refre_amt($order_id){
        $query = $this->db->query('select points from referral_info where invite_order_id ="'.$order_id.'"');
        $res = $query->result_array();
        return @$res[0]['points'];
    }

    /*Referal End*/

    function get_order_price_ref($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where order_unique_no = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info);

        $query = $this->db->query('SELECT SUM(product_price) as product_price FROM `order_product_info` WHERE order_id IN ('.$order_ids.')' );

            $query_res = $query->result_array();
            if(!empty($query_res)){
                return $query_res[0]['product_price'];
            }else{
                return 0;
            }
        }
    }

    function get_cartid($uniquecookie,$productid,$userid,$size,$agent,$platform_info,$look_id)
    {    
          $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }

          if($productid!='' && $size!='' && $user_id!=0 ){
          $query = $this->db->query("select * from cart_info where ( user_id = '".$user_id."' and `product_id` = '".$productid."' and `product_size` = '".$size."' and `order_id` is null )");
          $res = $query->result_array();
          //echo $this->db->last_query();
          //echo '<pre>';print_r($res);exit;
          return $res[0]['id'];

        }
    }

    public function getUserEmail($userid){

        $query_order = $this->db->query('select count(*) as ordcnt,order_unique_no,user_id from order_info group by order_unique_no having user_id = "'.$userid.'" ');
        $order_result = $query_order->result_array();        
        $order_count = count($order_result);
        if($order_count>1)
        {
            return false;
        }else
        {
            $query = $this->db->query('select email from user_login where id ="'.$userid.'"');
            $username_email = $query->result_array();
            return $username_email[0]['email'];
        }
    }

    public function update_couponcode($email,$couponcode)
    {
        $query = $this->db->query('select * from `coupon_settings` where coupon_code = "'.$couponcode.'" ');
        $res = $query->result_array();
        $email_applied = '';        
        
        if($res[0]['coupon_email_applied']!='')
        {
            $email_applied = $res[0]['coupon_email_applied'].','.$email;
            $result = $this->db->query('update `coupon_settings` SET coupon_email_applied = "'.$email_applied.'" WHERE coupon_code = "'.$couponcode.'" ');
            return 1;
        }else
        {
            return 2;
        }
    }

     function deleteCart(){
        $user_id = $this->session->userdata('user_id') > 0 ? $this->session->userdata('user_id') : 0;
        $uniquecookie = $_COOKIE['SCUniqueID'];
         if($uniquecookie!=''){
            //$this->db->delete('cart_info', array('SCUniqueID' => $uniquecookie)); 
            if($user_id == '' || $user_id ==0){
                $query = $this->db->query('delete from cart_info where (SCUniqueID = "'.$uniquecookie.'")');
            }else{
                $query = $this->db->query('delete from cart_info where (SCUniqueID = "'.$uniquecookie.'" || user_id = "'.$user_id.'") ');
            }
        }
        return true;
    }

     function get_scboxcart($userid=null){
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = $userid; }       
        //echo $_COOKIE['SCUniqueID'];exit;
        $uniquecookie = null;
         $query = '';
        if((isset($_COOKIE['SCUniqueID']) && $_COOKIE['SCUniqueID'] !="") || $userid!=''){ 
            $uniquecookie = @$_COOKIE['SCUniqueID'];
         
        if($user_id == '' || $user_id ==0){
        $query = $this->db->query('select d.cod_available,d.shipping_exc_location,d.cod_exc_location,b.brand_id,b.slug,a.`id`,a.`product_id`,d.`return_policy`,a.`product_qty`,b.`name`,(a.`product_qty`*a.product_price) as price,CASE WHEN DATE_FORMAT(b.discount_start_date,"%Y-%m-%d  %h:%i") <= DATE_FORMAT(now(),"%Y-%m-%d %h:%i") and DATE_FORMAT(b.discount_end_date,"%Y-%m-%d  %h:%i") >= DATE_FORMAT(now(),"%Y-%m-%d %h:%i") then (a.`product_qty`*b.compare_price) ELSE 0 END AS compare_price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,b.`id` as pid,b.`price` as product_price,c.id as size_id,a.`product_price` as cart_product_price from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id`  and (a.`SCUniqueID` = "'.$uniquecookie.'") and a.`product_id` IN ('.SCBOX_PRODUCTID_STR.') order by b.`brand_id` limit 0,1');
        }else{
        $query = $this->db->query('select d.cod_available,d.shipping_exc_location,d.cod_exc_location,b.brand_id,b.slug,a.`id`,a.`product_id`,d.`return_policy`,a.`product_qty`,b.`name`,(a.`product_qty`*a.product_price) as price,CASE WHEN DATE_FORMAT(b.discount_start_date,"%Y-%m-%d  %h:%i") <= DATE_FORMAT(now(),"%Y-%m-%d %h:%i") and DATE_FORMAT(b.discount_end_date,"%Y-%m-%d  %h:%i") >= DATE_FORMAT(now(),"%Y-%m-%d %h:%i") then (a.`product_qty`*b.compare_price) ELSE 0 END AS compare_price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,b.`id` as pid,b.`price` as product_price,c.id as size_id,a.`product_price` as cart_product_price from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id`  and (a.`SCUniqueID` = "'.$uniquecookie.'" and a.user_id = "'.$user_id.'") and a.`product_id` IN ('.SCBOX_PRODUCTID_STR.') order by b.`brand_id` limit 0,1 ');
        }
        //echo $this->db->last_query();exit;
        $res = $query->result_array();        
        $product_data = array();
            if(!empty($res)){ $i = 0;
                foreach ($res as $key => $value) {

                    if($value['product_id']==SCBOX_CUSTOM_ID)
                    {
                        // $value['compare_price'] = @$_COOKIE['Scbox_price'];
                        // $value['product_price'] = @$_COOKIE['Scbox_price'];
                        $value['compare_price'] = $value['cart_product_price'];
                        $value['product_price'] = $value['cart_product_price'];
                    }
                    $product_data[$i]['id'] = $value['id'];
                    $product_data[$i]['product_id'] = $value['product_id'];
                    $product_data[$i]['cod_available'] = $value['cod_available'];
                    $product_data[$i]['shipping_exc_location'] = $value['shipping_exc_location'];
                    $product_data[$i]['cod_exc_location'] = $value['cod_exc_location'];
                    $product_data[$i]['slug'] = $value['slug'];
                    $product_data[$i]['product_qty'] = $value['product_qty'];
                    $product_data[$i]['return_policy'] = $value['return_policy'];
                    $product_data[$i]['name'] =$value['name'];
                    $product_data[$i]['price'] = $value['compare_price'] > $value['product_price'] ?  $value['compare_price'] : $value['product_price'];
                    $product_data[$i]['image'] =$value['image'];
                    $product_data[$i]['size_text'] =$value['size_text'];
                    $product_data[$i]['company_name'] =$value['company_name'];
                    $product_data[$i]['user_id'] =$value['user_id'];
                    $product_data[$i]['min_del_days'] =$value['min_del_days'];
                    $product_data[$i]['max_del_days'] =$value['max_del_days'];
                    $product_data[$i]['store_tax'] =$value['store_tax'];
                    $product_data[$i]['is_shipping'] =$value['is_shipping'];
                    $product_data[$i]['shipping_charges'] =$value['shipping_charges'];
                    $product_data[$i]['shipping_min_values'] =$value['shipping_min_values'];
                    $product_data[$i]['shipping_max_values'] =$value['shipping_max_values'];
                    $product_data[$i]['is_cod'] =$value['is_cod'];
                    $product_data[$i]['cod_charges'] =$value['cod_charges'];
                    $product_data[$i]['cod_min_value'] =$value['cod_min_value'];
                    $product_data[$i]['code_max_value'] =$value['code_max_value'];
                    $product_data[$i]['product_size'] =$value['product_size'];
                    $product_data[$i]['compare_price']= $value['compare_price'];

                    $discount_percent = $this->get_discount_percentage($value['user_id'],$value['pid']);
                    $product_data[$i]['discount_price'] =round($value['product_qty']*($value['product_price']-($value['product_price']*$discount_percent)/100));
                    $product_data[$i]['discount_percent'] = $this->product_desc_model->calculate_discount_percent($product_data[$i]['price'],$product_data[$i]['discount_price'],$value['price']);

                    $product_data[$i]['discount_type'] =  $this->get_discount_id($value['user_id'],$value['pid'])['discount_type_id'];

                    if($product_data[$i]['discount_type'] == 1){
                     $product_data[$i]['discount_id'] =  $this->get_discount_id($value['user_id'],0)['id'];
                    }else if($product_data[$i]['discount_type'] == 2){
                        $product_data[$i]['discount_id'] =  $this->get_discount_id(0,$value['pid'])['id'];
                    }else{
                         $product_data[$i]['discount_id'] =0;
                         $product_data[$i]['discount_type'] =0;
                    }
                    $product_data[$i]['discount_max_amount'] = $this->get_discount_maxvalue($value['user_id'],$value['pid']);
                    $product_data[$i]['product_all_size'] = $this->get_size($value['product_id']);
                    $product_data[$i]['brand_unique_name'] =$this->get_user_info($value['user_id'])[0]['user_name'];
                    $product_data[$i]['in_stock'] = $this->get_product_availablility($value['product_id'],$value['size_id']);
                    $product_data[$i]['brand_id'] =$value['brand_id'];
                    /*$product_data[$i]['coupon_amount'] = $this->getCouponData()*/

                    $i++;
                }
               /* $product_data['coupon_amount'] = $this->getCouponData($coupon_code)['coupon_amount'];*/
              /* echo "<pre>";print_r($product_data);*/
            }
        return $product_data;
        }
    }

     function updateUserAddress($data,$user_id){
        //$user_id = $this->session->userdata('user_id');
        $query_res = $this->db->get_where('user_address', array('user_id' => $user_id));
        $res_res = $query_res->result_array();


        $query = $this->db->get_where('user_address', array('user_id' => $user_id,'shipping_address'=>$data['user_address'],'pincode'=>$data['user_pincode_no'],'city_name'=>$data['user_city'],'state_name'=>$data['user_state'],'first_name'=>$data['user_first_name'],'last_name'=>$data['user_last_name'],'mobile_no'=>$data['user_mobile_no']));

        $res = $query->result_array();
        if(empty($res) && @$data['existing_shipping_add']==''){
            $add_data = array(
                    'user_id'=>$user_id,
                    'shipping_address'=>$data['user_address'],
                    'pincode'=>$data['user_pincode_no'],
                    'city_name'=>$data['user_city'],
                    'state_name'=>$data['user_state'],
                    'first_name'=>$data['user_first_name'],
                    'last_name'=>$data['user_last_name'],
                    'mobile_no'=>$data['user_mobile_no'],
                    'address_type' =>'S',
                );
            if(empty($res_res)){ $add_data['is_default'] = 1; }
            $this->db->insert('user_address', $add_data);
            $insert_id = $this->db->insert_id();
            return  $insert_id;
            
        }else{
            $add_data = array(
                    'user_id'=>$user_id,
                    'shipping_address'=>$data['user_address'],
                    'pincode'=>$data['user_pincode_no'],
                    'city_name'=>$data['user_city'],
                    'state_name'=>$data['user_state'],
                    'first_name'=>$data['user_first_name'],
                    'last_name'=>$data['user_last_name'],
                    'mobile_no'=>$data['user_mobile_no'],
                    'address_type' =>'S',
                );
            $this->db->where(array('id'=>$data['existing_shipping_add'],'user_id'=>$user_id));
            $this->db->update('user_address', $add_data);
            return $data['existing_shipping_add'];
        }      

    }

    function getCartQty($uniquecookie){
       $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = ''; }
       /*$query = $this->db->query('select a.`id`,a.`product_id`,d.`return_policy`,a.`product_qty`,b.`name`,(a.`product_qty`*b.price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size` from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`SCUniqueID` = "'.$uniquecookie.'") order by b.`brand_id`');*/
       if($user_id == '' || $user_id ==0){
        $query = $this->db->query('select a.`id`,a.`product_id`,d.`return_policy`,a.`product_qty`,b.`name`,(a.`product_qty`*b.price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,b.`id` as pid,b.`price` as product_price from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and b.status = 1 and (a.`SCUniqueID` = "'.$uniquecookie.'") and a.`product_id` NOT IN ('.SCBOX_PRODUCTID_STR.') order by b.`brand_id`');
      }else{
        $query = $this->db->query('select a.`id`,a.`product_id`,d.`return_policy`,a.`product_qty`,b.`name`,(a.`product_qty`*b.price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,b.`id` as pid,b.`price` as product_price from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id`  and b.status = 1 and (a.`SCUniqueID` = "'.$uniquecookie.'" || a.`user_id`="'.$user_id.'") and a.`product_id` NOT IN ('.SCBOX_PRODUCTID_STR.') order by b.`brand_id`');
      }
      //echo $this->db->last_query();
        //$res = $query->result_array();
        //return $res;
        $res = $query->result_array();
        $product_data = array();
            if(!empty($res)){ $i = 0;
                foreach ($res as $key => $value) {
                    $product_data[$i]['id'] = $value['id'];
                    $product_data[$i]['product_id'] = $value['product_id'];
                    $product_data[$i]['product_qty'] = $value['product_qty'];
                    $product_data[$i]['return_policy'] = $value['return_policy'];
                    $product_data[$i]['name'] =$value['name'];
                    $product_data[$i]['price'] = $value['price'];
                    $product_data[$i]['image'] =$value['image'];
                    $product_data[$i]['size_text'] =$value['size_text'];
                    $product_data[$i]['company_name'] =$value['company_name'];
                    $product_data[$i]['user_id'] =$value['user_id'];
                    $product_data[$i]['min_del_days'] =$value['min_del_days'];
                    $product_data[$i]['max_del_days'] =$value['max_del_days'];
                    $product_data[$i]['store_tax'] =$value['store_tax'];
                    $product_data[$i]['is_shipping'] =$value['is_shipping'];
                    $product_data[$i]['shipping_charges'] =$value['shipping_charges'];
                    $product_data[$i]['shipping_min_values'] =$value['shipping_min_values'];
                    $product_data[$i]['shipping_max_values'] =$value['shipping_max_values'];
                    $product_data[$i]['is_cod'] =$value['is_cod'];
                    $product_data[$i]['cod_charges'] =$value['cod_charges'];
                    $product_data[$i]['cod_min_value'] =$value['cod_min_value'];
                    $product_data[$i]['code_max_value'] =$value['code_max_value'];
                    $product_data[$i]['product_size'] =$value['product_size'];
                    $product_data[$i]['discount_percent'] = $this->get_discount_percentage($value['user_id'],$value['pid']);
                    $product_data[$i]['discount_price'] =round($value['product_qty']*($value['product_price']-($value['product_price']*$product_data[$i]['discount_percent'])/100));

                    $product_data[$i]['discount_type'] =  $this->get_discount_id($value['user_id'],$value['pid'])['discount_type_id'];

                    if($product_data[$i]['discount_type'] == 1){
                     $product_data[$i]['discount_id'] =  $this->get_discount_id($value['user_id'],0)['id'];
                    }else if($product_data[$i]['discount_type'] == 2){
                        $product_data[$i]['discount_id'] =  $this->get_discount_id(0,$value['pid'])['id'];
                    }else{
                         $product_data[$i]['discount_id'] =0;
                         $product_data[$i]['discount_type'] =0;
                    }
                    $product_data[$i]['discount_max_amount'] = $this->get_discount_maxvalue($value['user_id'],$value['pid']);
                    /*$product_data[$i]['coupon_amount'] = $this->getCouponData()*/

                    $i++;
                }
               /* $product_data['coupon_amount'] = $this->getCouponData($coupon_code)['coupon_amount'];*/
               /*echo "<pre>";print_r($product_data);*/
            }
        return $product_data;
    }

    function check_mobile($user_data){

        $time = new DateTime(date('Y-m-d H:i:s'));
        $dateM = $time->format('Y-m-d H:i:s');
        $otp_no = rand(4,100000);
        $digits = 4;
        $otp_no =rand(pow(10, $digits-1), pow(10, $digits)-1);
        //$otp_no = 9999;

        if( @$user_data['mobile_no']!='' && @$user_data['type']!='')
        {
            $query1 = $this->db->query('select otp_no from user_mobile_otp where mobile_no = '.$user_data['mobile_no'].' AND type = "register"; ');
            $res1 = $query1->result_array();

            if(empty($res1))
            {
               $query = $this->db->query('select id,otp_no from user_mobile_otp where mobile_no = '.$user_data['mobile_no'].' AND type = "'.$user_data['type'].'" AND created_datetime between now() -INTERVAL 30 MINUTE AND now() + INTERVAL 30 MINUTE ');
                $res = $query->result_array();
                
                if(empty($res)){ 
                    $data = array('mobile_no'=>$user_data['mobile_no'],'otp_no'=>$otp_no,'type'=>$user_data['type'],'created_datetime'=>$dateM);
                    $this->db->insert('user_mobile_otp', $data);
                    $Lst_ins_id = $this->db->insert_id();
                    $query = $this->db->query("select otp_no from user_mobile_otp where id = $Lst_ins_id");

                    $res = $query->result_array();
                    return $res[0]['otp_no'];
                }else{

                    if(@$res[0]['otp_no'] == @$user_data['otp_number'])
                    {
                        $this->db->where('id', $res[0]['id']);
                        $this->db->update('user_mobile_otp', array('type' => 'register','status' => '1'));                     
                    }
                    return $res[0]['otp_no'];                    
                } 
            }else
            {
                return 'registered';
            }
        }       
    }

    function calculatetax($price,$tax)
    {
        return $price*$tax/100;
    }

    function get_ordercount($user_id)
    {
        $result_count = array();
        if($user_id!='')
        {
           $query = $this->db->query('Select count(id) as ordercount from order_info where user_id="'.$user_id.'" and brand_id = "'.SCBOX_BRAND_ID.'" and order_status!=9 and order_status!=7');
           $result_count = $query->result_array();
        }
       
       return @$result_count[0]['ordercount'];
    }

    function get_defaultcoupons($userid,$user_email,$page_type=null,$price=null)
    {
         
        if($page_type=='scboxcart' && $price!='')
        {
            $query = $this->db->query('select a.id,a.coupon_code,a.coupon_desc,a.show_on_web,a.coupon_amount,a.min_spend,a.max_spend,a.bin_no,a.is_special_code,
            a.coupon_email_applied,a.coupon_end_datetime,a.usage_limit_to_user from coupon_settings as a where a.coupon_start_datetime < now() AND now() < a.coupon_end_datetime AND a.is_delete = 0 AND a.status = 1 AND a.brand_id = 0 AND a.usage_limit_per_coupon > a.coupon_used_count  AND a.is_special_code=1 AND a.usage_limit_to_user>0 AND a.min_spend <= '.$price.' AND a.max_spend >= '.$price.' order by a.id desc limit 0,1');

            $result = $query->result_array();
             //echo $this->db->last_query();exit;
            //echo '<pre>';print_r($result);exit;
            return @$result[0]['coupon_code'];           
        }
    }

    function get_deliveredOrders($order_display_no=null)
    {
        if($order_display_no!='')
        {
          $cond = " and a.order_display_no = '".$order_display_no."'";  
        }else
        {
          $cond = '';
        }
        
       /* $query = $this->db->query("Select a.user_id,a.order_unique_no,a.order_display_no,a.order_total,a.created_datetime,a.order_status,a.coupon_code,a.coupon_amount,a.first_name,a.last_name,a.email_id,a.mobile_no,a.shipping_address,a.pincode,a.city_name,b.product_price,b.modified_datetime,DATEDIFF(NOW(),b.modified_datetime) as datediff from order_info as a, order_product_info as b where a.id = b.order_id and b.order_status=4  and DATEDIFF(NOW(),b.modified_datetime) >14 and brand_id IN (".SCBOX_BRAND_ID.") ".$cond." ");*/
        $query = $this->db->query("Select a.user_id,a.order_unique_no,a.order_display_no,a.order_total,a.created_datetime,a.order_status,a.coupon_code,a.coupon_amount,a.first_name,a.last_name,a.email_id,a.mobile_no,a.shipping_address,a.pincode,a.city_name,b.product_price,b.modified_datetime,DATEDIFF(NOW(),b.modified_datetime) as datediff from order_info as a, order_product_info as b where a.id = b.order_id and (b.order_status=4 OR b.order_status=6 ) and DATEDIFF(NOW(),b.modified_datetime) =2 and b.modified_datetime >= '2017-07-01 00:00:00' and brand_id IN (".SCBOX_BRAND_ID.") ".$cond." ");
        //echo $this->db->last_query();exit;
        $result = $query->result_array();
        return $result;
    }

    function get_deliveredOrders_recall($order_display_no=null)
    {
        if($order_display_no!='')
        {
          $cond = " and a.order_display_no = '".$order_display_no."'";  
        }else
        {
          $cond = '';
        }

        $data = array();
        $date = new DateTime(date('Y-m-d'));
        $date->sub(new DateInterval('P7D'));
        $newdate = $date->format('Y-m-d');

        $query_obj = $this->db->get_where('object', array('object_slug' => 'order_feedback' ));
        $result_obj = $query_obj->result_array();
        $object_id = $result_obj[0]['object_id'];

        // $query_s = $this->db->query("Select a.object_meta_value from object_meta as a where a.object_meta_key LIKE 'order_feedback_sent_".$newdate."' and a.object_id = ".$object_id." and DATEDIFF(NOW(),a.created_datetime) =7 ".$cond." ");
         $query_s = $this->db->query("Select a.object_meta_value from object_meta as a where a.object_meta_key LIKE 'order_feedback_sent_".$newdate."' and a.object_id = ".$object_id." ".$cond." ");
        $result_s = $query_s->result_array();        
       // echo $result_s[0]['object_meta_value'];exit;
         //echo $this->db->last_query();exit;
        if(!empty($result_s))
        {
            $orderids = explode(',', $result_s[0]['object_meta_value']);                        
            foreach($orderids as $val)
            {
                $res = $this->get_objectmeta_bycond($object_id,$val,'0');   
                //|| @$res[0]['object_meta_value']==''             
                if(empty($res))
                {
                   $query = $this->db->query("Select a.user_id,a.order_unique_no,a.order_display_no,a.order_total,a.created_datetime,a.order_status,a.coupon_code,a.coupon_amount,a.first_name,a.last_name,a.email_id,a.mobile_no,a.shipping_address,a.pincode,a.city_name,b.product_price,b.modified_datetime,DATEDIFF(NOW(),b.modified_datetime) as datediff from order_info as a, order_product_info as b where a.id = b.order_id and  (b.order_status=4 OR b.order_status=6 ) and a.order_display_no IN ('".$val."') and brand_id IN (".SCBOX_BRAND_ID.") ".$cond." ");
                    $result = $query->result_array(); 
                    $data[] = $result[0]; 
                }                
            }           
            return $data;
        }         
    }

    function get_objectmeta_bycond($objectid,$objectmetakey,$userid)
    {
        //$query = $this->db->get_where('object_meta', array('object_id' => $objectid,'object_meta_key' => $objectmetakey ,'created_by' => $userid ));
        $query = $this->db->get_where('object_meta', array('object_meta_key' => $objectmetakey ,'object_id' =>$objectid));
        $result = $query->result_array();
        return $result;
    }

    function get_customOrdersbyrange($order_display_no=null)
    {
        if($order_display_no!='')
        {
          $cond = " and a.order_display_no = '".$order_display_no."'";  
        }else
        {
          $cond = '';
        }        
      
        $query = $this->db->query("Select a.user_id,a.order_unique_no,a.order_display_no,a.order_total,a.created_datetime,a.order_status,a.coupon_code,a.coupon_amount,a.first_name,a.last_name,a.email_id,a.mobile_no,a.shipping_address,a.pincode,a.city_name,b.product_price from order_info as a, order_product_info as b where a.id = b.order_id and a.created_datetime >= '2017-07-01 00:00:00' and a.created_datetime <= '2017-08-31 00:00:00' and a.order_status!=9 and brand_id IN (".SCBOX_BRAND_ID.") ".$cond." ");
        //echo $this->db->last_query();exit;
        $result = $query->result_array();
        return $result;
    }




}

?>