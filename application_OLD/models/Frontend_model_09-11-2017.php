<?php
class Frontend_model extends CI_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	public function scxObject($taxonomy_id = '')
	{
		$this->db->select('o.id,o.object_name,o.slug,o.object_image,or.scx_term_taxonomy_id,om.object_meta_value');
		$this->db->from('object as o');
		$this->db->join('term_object_relationship as or','or.object_id = o.id','INNER');
		$this->db->join('object_meta as om','om.object_id = o.id','INNER');
		$this->db->where('scx_term_taxonomy_id',$taxonomy_id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}
		
	public function userMeta($arrData = array())
	{
		$this->db->insert('user_meta', $arrData);
		$metaId = $this->db->insert_id();
		return $metaId;
	}
		
	public function orderMeta($arrData = array())
	{
		$this->db->insert('order_meta', $arrData);
		$metaId = $this->db->insert_id();
		return $metaId;
	}
		
	public function scxOrder($arrData = array())
	{
		$this->db->insert('scx_order', $arrData);
		$orderId = $this->db->insert_id();
		return $orderId;
	}
		
	public function getCategory($gender)
	{
		if($gender!='')
		{
			$this->db->select('term_taxonomy_id as id,taxonomy_content as name');
			$this->db->from('term_taxonomy');
			$this->db->where('parent', $gender);
			$this->db->order_by('display_order', 'ASC');
			$query = $this->db->get();
			if($query->num_rows()>0)
			{
				return $query->result_array();
			}		
		}else
		{
			return false;
		}
		
	}
	
	

	
	
	
} 
