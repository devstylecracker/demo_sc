<?php
class Lookcreator_model extends CI_Model {

    
    public $ouptut = array();
    public $depth = 0; public $children = 0;
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
    }
    
    function get_product_list($variation_id,$color,$brand,$tag,$variation_type,$variations_value,$lookoffset){ 
    	$lookoffset = $lookoffset*LOOK_CREATOR_PAGE_OFFSET;
		if($this->session->userdata('user_id')){
			
			$brand_cond = ''; $color_cond = '';
			
			if($brand >0 ){
				$brand_cond = ' and b.brand_id ='.$brand;
			}else{
				$brand_list = $this->get_paid_brands();
				$brand_cond = ' and b.brand_id IN('.$brand_list.')';
			}

			if($color >0 ){
				$color_cond = ' and d.tag_id ='.$color;
			}

			if($tag){
				$tag_id = $this->tagId_by_name($tag);

				if($tag_id){ 
					if($color_cond!=''){ $color_cond = ' and (d.tag_id ='.$color.' or d.tag_id ='.$tag_id.')';  }
					else{ $color_cond = ' and d.tag_id ='.$tag_id; }
				}
			}
		

			if($variation_type == ''){
		

			$query = $this->db->query("SELECT distinct b.`id` as product_id , b.`name` , b.`image` , b.`product_used_count`, b.`url`, b.`price` FROM `product_desc` AS b,`product_tag` as d,`product_variations` as c WHERE b.`status` =1 and d.`product_id` = b.`id` and b.`is_delete`=0 and b.`id`=c.`product_id` and b.`id`>='3598'  and b.`approve_reject`='A' and b.`product_sub_cat_id`=".$variation_id." ".$brand_cond." ".$color_cond." order by b.`id` desc limit ".$lookoffset.",".LOOK_CREATOR_PAGE_OFFSET);
			}else{

				if($variations_value!=''){
					$variations_value_cond = " and c.product_var_val_id in (".$variations_value.")";
				}else{ 
					$variations_value_cond = "";
				}
				$query = $this->db->query("SELECT distinct b.`id` as product_id , b.`name` , b.`image` , b.`product_used_count`, b.`url`, b.`price` FROM `product_desc` AS b,`product_variations` as c,`product_tag` as d WHERE b.`status` =1 and b.`is_delete`=0 and b.`id`=c.`product_id` and b.`id`>='3598'  and b.`approve_reject`='A' and d.`product_id` = b.`id` and b.`product_sub_cat_id`=".$variation_id." ".$brand_cond." ".$color_cond." and c.product_var_type_id='".$variation_type."' ".$variations_value_cond." order by b.`id` desc limit ".$lookoffset.",".LOOK_CREATOR_PAGE_OFFSET);
			}


			//echo $this->db->last_query();
			return $result = $query->result_array();
			
		}else{
			return false;
		}
	}

	 function get_product_list_v2($brand,$cat,$attributes_chosen,$category_specific_attr,$lookoffset){
    	$lookoffset = $lookoffset*LOOK_CREATOR_PAGE_OFFSET; $where_cond = '';
    	if($this->session->userdata('user_id')){
    		$brand_cond = '';
    		$brand_cond = $brand_cond." and a.brand_id IN(".$this->get_paid_brands().")";
    		if($brand!='' && $cat!='' && $attributes_chosen =='' && $category_specific_attr == ''){
    			if($brand >0){
    				$brand_cond = "and a.brand_id = ".$brand;
    			}

    			
    		$query = $this->db->query("select distinct a.id as product_id,a.name,a.image,a.product_used_count,a.url,a.price,a.brand_id from product_desc as a,category_object_relationship as b where a.id = b.object_id and b.object_type='product' and a.`id`>='3598' ".$brand_cond." and b.category_id = ".$cat." and a.approve_reject = 'A' order by a.id desc limit ".$lookoffset.",".LOOK_CREATOR_PAGE_OFFSET);
    		}else{
    			$brand_cond = '';
    			if($attributes_chosen!=''){
    				$where_cond = $where_cond . "and c.attribute_id IN (".$attributes_chosen.")";
    			}

    			if($category_specific_attr!=''){
    				$where_cond = $where_cond . "and c.attribute_id =".$category_specific_attr."";
    			}
    			if($brand >0){
    				$brand_cond = "and a.brand_id = ".$brand;
    			}
    		$query = $this->db->query("select distinct a.id as product_id,a.name,a.image,a.product_used_count,a.url,a.price,a.brand_id from product_desc as a,category_object_relationship as b,attribute_object_relationship as c where a.id = b.object_id and a.id = c.object_id and b.object_type='product' and a.`id`>='3598' ".$brand_cond." and b.category_id = ".$cat." ".$where_cond ." and a.approve_reject = 'A' order by a.id desc limit ".$lookoffset.",".LOOK_CREATOR_PAGE_OFFSET);	
    		}
    		$result = $query->result_array();
			
			$product_array = array();$i=0;$imagify_brandid = unserialize(IMAGIFY_BRANDID);
			foreach($result as $val){
				$product_array[$i]['product_id'] = $val['product_id'];
				$product_array[$i]['name'] = $val['name'];
				$product_array[$i]['product_used_count'] = $val['product_used_count'];
				$product_array[$i]['url'] = $val['url'];
				$product_array[$i]['price'] = $val['price'];
				// added for new product url  
				$productUrl = '';
				if(in_array($val['brand_id'],$imagify_brandid)){
					$productUrl = $this->config->item('products_tiny').$val['image'];
				}else{
					$productUrl =  base_url().'assets/products/thumb_124/'.$val['image'];                
				}
				// added for new product url
				$product_array[$i]['image'] = $productUrl;
				$i++;
			}
		
			return $product_array;
    	}
	}
	
	public function product_stock($product_id){
		$query = $this->db->query("select sum(stock_count) as stock_count from product_inventory where product_id ='".$product_id."'");
		$result = $query->result_array();
		if(!empty($result)){
			return $result[0]['stock_count'];
		}else{
			return 0;
		}
	}

	 public function save_look($look_name,$look_desc,$street_canvas_img_id,$prod_look,$look_type,$look_tag)
        {
                //$look_slug = str_replace(' ','-',strtolower($look_name)).time().mt_rand(0,10000);

                $string = str_replace(' ', '-',$look_name); // Replaces all spaces with hyphens.

   			  	$look_name_new = preg_replace('/[^A-Za-z0-9\-]/', '', $string);

                $look_slug = str_replace(' ','-',strtolower($look_name_new)).'-'.time().mt_rand(0,10000);

                $data = array('name'=>$look_name,'description'=>$look_desc,'look_type'=>$look_type,'image'=>$street_canvas_img_id,'created_datetime'=>date('Y-m-d H:i:s'),'slug'=>$look_slug,'created_by'=> $this->session->userdata('user_id'));
                $this->db->insert('look', $data);
                $ins_id = $this->db->insert_id();
                $data = array();

                $this->db->insert('look_tags', array('look_id'=>$ins_id,'look_tags'=>$look_tag,'created_datetime'=>date('Y-m-d H:i:s'),'created_by'=> $this->session->userdata('user_id'))); 
                 foreach($prod_look as $val){
                        $data[] = array('look_id'=>$ins_id,'product_id'=>$val,'deflag'=>0);
                        }
                $this->db->insert_batch('look_products', $data);

                return true;
        }
	
	function get_all_look($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL){

		if($search_by == '1'){ $search_cond = " AND a.id = '".strip_tags(trim($table_search))."'"; }
		else if($search_by == '2'){ $search_cond = " AND a.name LIKE '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '3'){ $search_cond = " AND a.look_type = 1"; }
		else if($search_by == '4'){ $search_cond = " AND a.look_type = 3"; }
		else if($search_by == '5'){ $search_cond = " AND a.look_type = 2"; }
		else if($search_by == '6'){ $search_cond = " AND a.look_type = 4"; }
		else if($search_by == '7'){ $search_cond = " AND a.look_type = 6"; }
		else if($search_by == '8'){ $table_search = strtolower($table_search);if($table_search == 'pending'){ $status = '0';}else if($table_search == 'broadcast'){ $status = '1';}else if($table_search == 'rejected'){ $status = '2';}else $status = '5'; $search_cond = ' AND a.status = '.$status.' ';  }
                else{ $search_cond =''; }


		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
                                $limit_cond = " LIMIT ".$page.",".$per_page;
                        }else{
                                $limit_cond = '';
                        }

		$query = $this->db->query('select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime from 
(SELECT a.id,a.slug,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 '.$search_cond.') as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 OR z.look_type = 6) order by z.id desc '.$limit_cond);
		return $query->result_array();
	}
	
	function get_look_info($id){
		if($id){
		$query = $this->db->query('select z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.description,z.status,z.deflag,z.looks_for from 
(SELECT a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.description,a.status,a.deflag,a.looks_for FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 and a.id = '.$id.') as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 OR z.look_type = 6) ');
			$res = $query->result_array();
			return $res;
		}else{
		return false;
		}

	}


	function update_look($id,$look_name,$look_desc){

		$string = str_replace(' ', '-',$look_name); // Replaces all spaces with hyphens.

   		$look_name_new = preg_replace('/[^A-Za-z0-9\-]/', '', $string);

        $look_slug = str_replace(' ','-',strtolower($look_name_new)).time().mt_rand(0,10000);
		
		$data = array('name'=>$look_name,'description'=>$look_desc,'slug'=>$look_slug,"modified_by"=>$this->session->userdata('user_id'),"modified_datetime"=>date('Y-m-d H:i:s'));
		$this->db->where('id', $id);
		$this->db->update('look', $data);
		return true;
	}
	
	function reject_look($id){
			$data = array('status'=>2,"modified_by"=>$this->session->userdata('user_id'),"modified_datetime"=>date('Y-m-d H:i:s'));
			$this->db->where('id', $id);
			$this->db->update('look', $data);
			return true;
		}
	
	function delete_look($id){
			$data = array('deflag'=>1,"modified_by"=>$this->session->userdata('user_id'),"modified_datetime"=>date('Y-m-d H:i:s'));
			$this->db->where('id', $id);
			$this->db->update('look', $data);
			
			return true;
		}
		
	function broadcastlook($look_id,$body_shape,$body_style,$look_cat_id,$stylist_id,$budget,$size,$age,$gender=1,$mens_work_style=0,$mens_personal_style=0){
			$data = array('status'=>1,'looks_for' => $gender ,"modified_by"=>$this->session->userdata('user_id'),"broadcast_datetime"=>date('Y-m-d H:i:s'),"stylist_id"=>$stylist_id);
			$this->db->where('id', $look_id);
			$this->db->update('look', $data);

			$bucket_id = $this->get_bucket_id($body_shape,$body_style,$mens_work_style,$mens_personal_style);
			
			$query = $this->db->get_where('look_bucket_mapping', array('bucket_id' => $bucket_id,'look_id' => $look_id));
			$res = $query->result_array();
			if(!$res){
				$data = array(
				   'look_id' => $look_id ,
				   'bucket_id' => $bucket_id,
				   'budget' => $budget,
				   'size' => $size,
				   'age' => $age,
				   'created_datetime' => date('Y-m-d H:i:s'),
				   'created_by'=>$this->session->userdata('user_id')
				);

				$this->db->insert('look_bucket_mapping', $data);
			}

			$query = $this->db->get_where('look_category_mapping', array('look_id' => $look_id,'cat_id' => $look_cat_id));
			$res = $query->result_array();
			if(empty($res)){
				$data1 = array(
					'look_id' => $look_id ,
					'cat_id'  => $look_cat_id,
					'created_by' =>$this->session->userdata('user_id')
				);
				
				$this->db->insert('look_category_mapping', $data1);
			}
			return true;
		
		}
	
	function get_looks_product($look_id){
			$query = $this->db->query('select a.image, a.product_used_count,a.price, a.id as product_id from product_desc as a,look_products as b  where a.id = b.product_id  and b.look_id = '.$look_id);
			$data = $query->result_array(); 
			return $data;
		}
		
		
	function get_look_categories(){
			$query = $this->db->query('SELECT `id`, `category_name`, `status`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `look_category` WHERE 1 and `status`=1');
			$res = $query->result_array();
		}
		
	function get_look_sub_categories(){
			$query = $this->db->query('SELECT `id`, `category_id`, `category_name`, `status`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `look_sub_category` WHERE 1 and `status`=1');
			$res = $query->result_array();
			return $res;
		}

	function get_stylist_data(){

		$query = $this->db->get('stylist');
		$res = $query->result_array();
		return $res;
	}

	function get_tags_by_tag_type($tag_type_id){

		if($tag_type_id!='' && $tag_type_id>0){
			$query = $this->db->get_where('tag',array('tag_type_id'=>$tag_type_id));
			$res = $query->result_array();
			return $res;
		}
		return false;
	}

	function get_brands_list(){
		$query = $this->db->query("select a.id,a.user_name, a.bucket, a.created_datetime, a.email,c.cpa_percentage from user_login as a, user_role_mapping as b,brand_info as c WHERE a.`status` =1 and a.`id` = b.`user_id` and b.`role_id`=6 and a.`id` = c.`user_id` and c.is_paid=1 order by a.user_name asc ");
		$res = $query->result_array();
		return $res;
	}

	function tagId_by_name($tag_name){
		if($tag_name){
			$query = $this->db->query("select id from tag where lower(name) =lower('".$tag_name."')");
            $res = $query->result_array();
            if(!empty($res)){
            	return $res[0]['id'];
            }else{
            	return false;
            }
		}
	}


	function get_looks_productdetail($look_id){
			$query = $this->db->query('select a.id, a.product_used_count, a.id as product_id from product_desc as a, look_products as b where a.id = b.product_id and b.look_id = '.$look_id);

			$data = $query->result_array(); 
			return $data;
	}

	function setproduct_usedcount($product)
	{
		
		$product_data = array();
		$flag = 0;

		if($product)
		{
			foreach($product as $val)
			{
				$product_data[] =  array('id' => $val['id'],
								      'product_used_count' => $val['product_used_count']+1
								      );
			}

			foreach($product_data as $val)
			{
				if($val['product_used_count'] > PRODUCT_USE_MAX)
				{
					$flag = 1;
				}
			}
			
		}

		if($flag != 1)
		{
			if($this->db->update_batch('product_desc', $product_data, 'id'))
			{
				return true;
			}else
			{
				return false;
			} 

		}else
		{
			return false;
		}
		
		
	}

	function broadcastlook_reject($look_id,$stylist_id,$product){
			$data = array('status'=>2,"modified_by"=>$this->session->userdata('user_id'),"modified_datetime"=>date('Y-m-d H:i:s'),"stylist_id"=>$stylist_id);
			$this->db->where('id', $look_id);
			$this->db->update('look', $data);
			
				if($product)
				{
					foreach($product as $val)
					{
						$product_data[] =  array('id' => $val['id'],
										      'status' => 0
										      );
					}			
					
				}
			
				if($this->db->update_batch('product_desc', $product_data, 'id'))
				{
					return true;
				}else
				{
					return false;
				} 
	
 	} 

 	/*function catgory_list(){
 		$query = $this->db->query("SELECT `id`, `name`, `slug`, `status`, `is_delete`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `product_category` WHERE `status` = 1");
        $res = $query->result_array();
        return $res;
 	}*/

 	function get_all_variationtype($sub_cat_id){

 		$query = $this->db->query("SELECT `id`, `prod_sub_cat_id`, `name`, `slug`, `status`, `is_delete`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `product_variation_type` WHERE 1 and `is_delete` = 0 and `status` = 1 and `prod_sub_cat_id` = '".$sub_cat_id."'");		
		$res = $query->result_array();		
		return $res;

 	}

 	function get_variationvalue($sub_cat_id,$variation_type){
 		$query = $this->db->query("SELECT `id`, `product_variation_type_id`, `name`, `slug`, `status`, `is_delete`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `product_variation_value` where  `product_variation_type_id` = '".$variation_type."'");
 		$res = $query->result_array();		
		return $res;
 	}

 	function get_sticker_list(){
 		$query = $this->db->get_where('sticker', array('status' => 1));
 		$data = $query->result_array(); 
 		return $data;
 	}

 	function broadcastlookEdit($look_id,$budget,$look_cost,$product_ids)
 	{ 		
			$data = array(	
			   'budget' => $budget,
			   'look_cost' => $look_cost,
			   'modified_by'=>$this->session->userdata('user_id')
			);
			$this->db->where('look_id', $look_id);			
			$this->db->update('look_bucket_mapping', $data);	

			if($product_ids!='')
			{
				$this->db->where(array('look_id'=>$look_id));
				$this->db->update('look_products', array('in_look_cost' => 0));					

				$productArr = explode(',',$product_ids);
				$data1 = array(	
				   'in_look_cost' => 1  
				);
				foreach($productArr as $val)
				{
					$this->db->where(array('look_id'=>$look_id,'product_id'=>$val));			
			        $this->db->update('look_products', $data1);	
				}
			}		
			
		return true;
 	}

 	function get_paid_brands(){
 		$brands = $this->get_brands_list(); $brand_html = '';
 		if(!empty($brands)){ $i = 0; 
 			foreach ($brands as $data) {
 				if($i!=0) { $brand_html = $brand_html.','; }
 				$brand_html = $brand_html.$data['id'];
 				$i++;
 			}
 		}
 		return $brand_html;
 	}
 	function all_attributes(){
		$query = $this->db->get_where('attributes', array('attribute_parent' => '-1'));
		$res = $query->result_array();
		return $res;
	}

	function attributes_values($id){
		$query = $this->db->get_where('attribute_values_relationship', array('attribute_id' => $id));
		$res = $query->result_array();
		return $res;
	}

	function all_attributes_depends_on_cat($id){
		$query = $this->db->get_where('attributes', array('attribute_parent' => $id));
		$res = $query->result_array();
		return $res;
	}
	function parent_catgory_list(){
		$query = $this->db->get_where('category', array('category_parent' => '-1'));
		$res = $query->result_array();
		return $res;
	}

	function get_all_child($id){
		$query = $this->db->query("select id,category_name,category_parent from (select * from category order by category_parent, id) products_sorted,(select @pv := '".$id."') initialisation where find_in_set(category_parent, @pv) > 0 and @pv := concat(@pv, ',', id)");
		$res = $query->result_array();
		return $res;
	}

	function catgory_list(){
 		$query = $this->db->query("SELECT `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `category` WHERE 1");
        $res = $query->result_array();
        $tree = $this->buildTree($res);

        $res1 = $this->printAllValues($tree,$this->ouptut);
        return $res1;
 	}
	
	
	function get_men_categories(){
 		$query = $this->db->query("select  `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` from (select * from category order by category_parent, id) products_sorted,(select @pv := '3') initialisation where (find_in_set(category_parent, @pv) > 0 and @pv := concat(@pv, ',', id)) or (id = 3)");
        $res = $query->result_array();
        $tree = $this->buildTree($res);

        $res1 = $this->printAllValues($tree,$this->ouptut);
        return $res1;
 	}
	
	function get_women_categories(){
 		$query = $this->db->query("select  `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` from (select * from category order by category_parent, id) products_sorted,(select @pv := '1') initialisation where (find_in_set(category_parent, @pv) > 0 and @pv := concat(@pv, ',', id)) or (id = 1)");

        $res = $query->result_array();
        $tree = $this->buildTree($res);
        $res1 = $this->printAllValues($tree,$this->ouptut);
        return $res1;
 	}


 	function buildTree(array $elements, $parentId = '-1') {
    $branch = array();

    foreach ($elements as $element) {
        if ($element['category_parent'] == $parentId) {
            $children = $this->buildTree($elements, $element['id']);
            if ($children) {
                $element['children'] = $children;
            }
            $branch[] = $element;
        }
    }

    return $branch;
	}

	function printAllValues(array $array,$ouptut) {

		foreach($array as $value){
			if($value['category_parent'] == -1){ $this->depth = 0; }
			$nbsp = '';
			$i = $this->depth;
			/*while($i>=0){
				$nbsp .='&nbsp;&nbsp;';
				$i--;
			}*/
			$this->ouptut[$value['id']] = '<option slug="'.$value['category_slug'].'" label="'.$value['category_name'].'" value="'.$value['id'].'" class="level-'.$i.'">'.$nbsp.$value['category_name'].'</option>';
			
			if(isset($value['children'])){
				$this->depth = $this->depth+1;
				$this->printAllValues($value['children'],$ouptut);
			}else{
				$this->children = 0;
			}
		
		}
		return $this->ouptut;
	}
}
?>