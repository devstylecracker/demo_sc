<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories_model extends MY_Model {
	public $ouptut = array();
    public $depth = 0; public $children = 0;
	 function __construct()
    {
        // Call the Model constructor
        $this->slug = '';
        parent::__construct();
    }	

    function save_category($data){
    	if(!empty($data)){
            /* echo "<pre>";
                print_r($_POST);
                print_r($_FILES);
                echo "</pre>";
                exit();*/
            $term_data = array();
            $termtax_data = array();
    		$term_name = $this->input->post('term_name');
    		$term_slug = $this->input->post('term_slug');
    		$term_parent = $this->input->post('term_parent');
    		$term_desc = $this->input->post('term_description');
            //$size_guide = $this->input->post('size_guide');
            $sort_order = $this->input->post('sort_order')!='' ? $this->input->post('sort_order') : 0;
            $termtax_type = $this->input->post('termtax_type');

    		if($term_name!=''){  
                $term_data['term_name'] = $term_name;               
                if($this->get_term_slug($term_parent)!=''){
                    $term_data['term_slug'] = $this->get_term_slug($term_parent).'-'.strtolower($term_slug);
                }else{
                    $term_data['term_slug'] = strtolower($term_slug);
                }                           
                $term_data['created_datetime'] = date('Y-m-d H:i:s');
                $term_data['term_group'] = $termtax_type;
              
    			$term_id = $this->add_term($term_data);
               /* echo $this->db->last_query();
                exit();*/
                $termtax_data['scx_term_id'] = $term_id;
                $termtax_data['taxonomy_type'] = $termtax_type;
                $termtax_data['taxonomy_content'] = $term_desc;
                $termtax_data['show_on_web'] = '1';
                $termtax_data['display_order'] = $sort_order;
                $termtax_data['parent'] = $term_parent;

                $term_id = $this->add_term_taxonomy($termtax_data);
               /* echo $this->db->last_query();
                echo $term_id;
                exit();*/
                if($_FILES["term_image"]["name"] !=''){
                    $target_dir = "assets/images/products_category/".$term_id.'.jpg';
                    if(move_uploaded_file($_FILES["term_image"]["tmp_name"], $target_dir)){
                        $term_id = $this->update_cat_image($term_id);
                    }
                }
    		}
    	}
    }

    /*function add_term($data){
        if(!empty($data)){
            $this->db->insert('scx_terms', $data); 
            return $this->db->insert_id(); 
        }else{
            return 0;
        }
    }*/

    /*function add_term_taxonomy($data){
        if(!empty($data)){
            $this->db->insert('scx_term_taxonomy', $data); 
            return $this->db->insert_id(); 
        }else{
            return 0;
        }
    }*/

    function get_all_categories($cat_id = '-1'){
        $this->db->from('category');
        $this->db->order_by("category_parent", "asc");
        $this->db->where('category_parent',$cat_id);
        $query = $this->db->get(); 
        return $query->result_array();
    }

    function get_all(){
        $this->db->select('id, category_name,  category_slug,category_parent');
        $this->db->from('category');
        $this->db->order_by("category_parent", "asc");
        $query = $this->db->get(); 
        return $query->result_array();
    }

    function get_all_parent(){/*
        $this->db->select('category_parent');
        $this->db->from('category');
        $this->db->order_by("category_parent", "asc");
        $query = $this->db->get();*/ //select id, category_name,  category_slug,category_parent from category where id in
        $query = $this->db->query("(select distinct category_parent as id from category) order by id asc"); 

        return $query->result_array();
    }

    function get_categories($cat_id){
        $this->db->from('category');
        $this->db->order_by("category_parent", "asc");
        $this->db->where('id',$cat_id);
        $query = $this->db->get(); 
        return $query->result_array();
    }

    function get_filtered_category($search){
        if($search!=''){
            $query = $this->db->query("select distinct `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`,`status` from category where category_name like '%".$search."%' order by category_parent asc");  
            $res = $query->result_array();

            return $res;
        }
    }

    function get_term_slug($cat_id=null){
        if($cat_id!='-1'){
            $this->db->from('scx_terms');
            $this->db->where('id',$cat_id);
            $query = $this->db->get(); 
            $res = $query->result_array(); 
            if(!empty($res)){
                return $res[0]['term_slug'];
            }
        }else{ return ''; }
    }

    function update_cat_image($cat_id=null){

        $this->db->select('scx_term_id');
        $this->db->from('term_meta');
        $this->db->where('scx_term_id', $cat_id);
        $query = $this->db->get();
         if($query->num_rows() == $cat_id) 
         {
            $this->db->where('id', $cat_id);
            $this->db->update('term_meta', $data);          
        }
         else 
        {
            $img['termmeta_value']      = $cat_id.'.jpg';
            $img['scx_term_id']         = $cat_id;
            $img['termmeta_key']        = 'default_image';
            $img['created_by']          = '1';
            $img['created_datetime']    = date('Y-m-d H:i:s');
            $this->db->insert('term_meta', $img) ;
            $imageId = $this->db->insert_id();
            return $imageId;
        }
    }

    function edit_category($data,$cat_id=null){
        if($cat_id!=null && $cat_id >0){
            $category_slug = '';
            if($this->get_term_slug($data['category_parent'])!=''){
                    $category_slug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $data['category_name'])); 
                    $category_slug = $this->get_term_slug($data['category_parent']).'-'.strtolower($category_slug);
            }else{
                    $category_slug = strtolower($data['category_slug']);
            }


            $data1 = array(
               'category_name' => $data['category_name'],
               'category_content' => $data['category_description'],
               'category_parent' => $data['category_parent'],
              // 'category_slug' => $category_slug,
               'size_guide' => $data['size_guide'],
               'sort_order' => $data['sort_order'],
               'comp_cat' => $data['comp_cat'],
            );

            $this->db->where('id', $cat_id);
            $this->db->update('category', $data1); 

            if($_FILES["category_image"]["name"] !=''){
                $target_dir = "../assets/images/products_category/".$cat_id.'.jpg';
                if(move_uploaded_file($_FILES["category_image"]["tmp_name"], $target_dir)){
                    $cat_id = $this->update_cat_image($cat_id);
                }
            }
            $this->db->delete('pa_object_relationship', array('object_id' => $cat_id,'pa_type'=>'bucket','object_type'=>'category'));
            if(!empty($data['bucket'])){ 
                

                foreach ($data['bucket'] as $value) {
                    $k = array(
                            'object_id' => $cat_id,
                            'pa_type' => 'bucket',
                            'object_type' => 'category',
                            'object_value' => $value,
                            'created_by' => $this->session->userdata('user_id'),
                            'modified_by' => $this->session->userdata('user_id'),
                            'created_datetime' => date('Y-m-d H:i:s')
                        );
                    $this->db->insert('pa_object_relationship', $k); 
                    
                 } 
            }
            $this->db->delete('pa_object_relationship', array('object_id' => $cat_id,'pa_type'=>'age','object_type'=>'category'));
            if(!empty($data['age'])){ 
                

                foreach ($data['age'] as $value) {
                    $k = array(
                            'object_id' => $cat_id,
                            'pa_type' => 'age',
                            'object_type' => 'category',
                            'object_value' => $value,
                            'created_by' => $this->session->userdata('user_id'),
                            'modified_by' => $this->session->userdata('user_id'),
                            'created_datetime' => date('Y-m-d H:i:s')
                        );
                    $this->db->insert('pa_object_relationship', $k); 
                    
                 } 
            }
             $this->db->delete('pa_object_relationship', array('object_id' => $cat_id,'pa_type'=>'budget','object_type'=>'category'));
            if(!empty($data['budget'])){  
               

                foreach ($data['budget'] as $value) {
                    $k = array(
                            'object_id' => $cat_id,
                            'pa_type' => 'budget',
                            'object_type' => 'category',
                            'object_value' => $value,
                            'created_by' => $this->session->userdata('user_id'),
                            'modified_by' => $this->session->userdata('user_id'),
                            'created_datetime' => date('Y-m-d H:i:s')
                        );
                    $this->db->insert('pa_object_relationship', $k); 
                    
                 } 
            }
        }
    }

    function delete_category($id){
        if($id > 0){
            $this->db->delete('category', array('id' => $id)); 
            return 1;
        }else{
            return 2;
        }
    }

   /* function get_cat($id = null){
       $query = $this->db->query("SELECT b.`term_taxonomy_id`,b.`scx_term_id`,b.`taxonomy_type`,b.`taxonomy_content`,b.`show_on_web`,b.`parent`,a.`count`,b.`created_by`,b.`modified_by`,b.`created_datetime`,b.`modified_datetime`,a.`id`,a.`term_name`,a.`term_slug` FROM `scx_terms` as a ,`scx_term_taxonomy` as b WHERE b.`term_taxonomy_id` = a.`id` ");
        $res = $query->result_array();
        return $res;


    }*/

    public function get_total_cat($data = array())
    {
        $this->db->select('t.id');
        $this->db->from('terms as t');
        $total_records = $this->db->count_all_results();
        return  $total_records;
    }

    public function get_cat($limit='0', $page_num='0',$data = array())
    {
        $start = ($page_num  == NULL) ? 0 :($page_num * $limit) - $limit;
        if($start <0)
        {
            $start = 0;
        }
        $this->db->select('DISTINCT (tt.scx_term_id),tt.term_taxonomy_id,tt.taxonomy_type,tt.taxonomy_content,tt.show_on_web,tt.parent,t.count,t.created_by,t.modified_by,t.created_datetime,t.modified_datetime,t.id,t.term_name,t.term_slug');
        $this->db->from('terms as t');
        $this->db->join('term_taxonomy` as tt', 'tt.scx_term_id = t.id', 'INNER');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
            
            if ($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            return false;
    }



    

    function buildTree(array $elements, $parentId = '-1') {
    $branch = array();

    foreach ($elements as $element) {
        if ($element['parent'] == $parentId) {
            $children = $this->buildTree($elements, $element['id']);
            if ($children) {
                $element['children'] = $children;
            }
            $branch[] = $element;
        }
    }

    return $branch;
    }

    function printAllValues(array $array,$ouptut) {

        foreach($array as $value){
            if($value['parent'] == -1){ $this->depth = 0; }
            $nbsp = '';
            $i = $this->depth;
           
            while($i>=0){
                //$nbsp .='-';
                $nbsp='';
                $i--;
            }

            /*$this->ouptut[$value['id']] = '<option value="'.$value['id'].'" class="level-'.$i.'">'.$nbsp.$value['category_name'].'</option>';*/

            if($value['status'] == 0){ $status = '<span class="text-orange">(Hide)</span>'; }else{ $status = ''; }
            $this->ouptut[$value['id']] = "<tr><td>".$value['id']."</td><td><input type='checkbox' name='cat_show[]'
             value='".$value['id']."' class='checkbox'/></td><td>".$nbsp.$value['term_name'].$status."</td><td>".$value['term_slug']."</td><td>".$value['count']."</td><td><a href='".base_url()."categories/categories_edit/".$value['id']."'><button class='btn btn-primary btn-xs editProd' type='button' ><i class='fa fa-edit'></i> Edit</button></a> <button class='btn btn-xs btn-primary btn-delete' type='button' onclick='delete_category(".$value['id'].");'><i class='fa fa-trash-o'></i> Delete</button></td></tr>";
            
            if(isset($value['children'])){
                $this->depth = $this->depth+1;
                $this->printAllValues($value['children'],$ouptut);
            }else{
                $this->children = 0;
            }
        
        }
        return $this->ouptut;
    }


    function printAllValues1(array $array,$ouptut,$id=null) {

        foreach($array as $value){
            if($value['parent'] == -1){ $this->depth = 0; }
            $nbsp = '';
            $i = $this->depth;
            /*while($i>=0){
                $nbsp .='&nbsp;&nbsp;';
                $i--;
            }*/
            if($id!=null && $value['id'] == $id){
            $this->ouptut[$value['id']] = '<option value="'.$value['id'].'" class="level-'.$i.'" selected>'.$nbsp.$value['term_name'].'->'.$this->get_slug($value['parent']).'</option>'; $this->slug ='';
            }else{
            $this->ouptut[$value['id']] = '<option value="'.$value['id'].'" class="level-'.$i.'">'.$nbsp.$value['term_name'].'->'.$this->get_slug($value['parent']).'</option>'; $this->slug ='';
            }
            
            if(isset($value['children'])){
                $this->depth = $this->depth+1;
                $this->printAllValues1($value['children'],$ouptut,$id);
            }else{
                $this->children = 0;
            }
        
        }
        return $this->ouptut;
    }

    function get_all_buckets(){
        $query = $this->db->query("select a.id,(select answer from answers where id =a.body_shape) as body_shape,(select answer from answers where id = a.style) as style,(select answer from answers where id = a.work_style) as work_style,(select answer from answers where id = a.personal_style) as personal_style from bucket as a");
        $res = $query->result_array();
        return $res;
    }

    function get_question_answers($id){
        if($id){
            $query = $this->db->query('SELECT a.id,a.answer,a.description FROM `answers` as a, `question_answer_assoc`  as b where a.`id` = b.`answer_id` AND a.`status` =1 AND b.`question_id` IN ('.$id.')');

            $result = $query->result_array();
            return $result;
        }

    }

    function get_pa_data($id,$type,$pa){
        if($id !=''){
            $query = $this->db->get_where('pa_object_relationship', array('object_id' => $id,'object_type'=>$type,'pa_type'=>$pa));
            $res = $query->result_array(); 
            $objects = array();
            if(!empty($res)){
                foreach($res as $val){

                    $objects[] = $val['object_value'];
                }
            }

            return $objects;
        }
    }

    function get_slug($id){
        
        if($id!='0'){
        //$query = $this->db->query('select category_name,category_parent from category where id = "'.$id.'"');
        $query = $this->db->query('select a.term_name,b.parent from scx_terms as a,scx_term_taxonomy as b where a.id = b.scx_term_id and a.id = "'.$id.'"');
        $res = $query->result_array();
        if(!empty($res)){
            foreach($res as $val){

                $this->get_slug($val['parent']);

                $this->slug = '->'.$val['term_name'].$this->slug; 

            }
            return  $this->slug ;
        }
        }
        
    }

    function hide_show_cat($cat_hide_show,$cat){
        if(!empty($cat)){
            foreach ($cat as $value) {
                $data=array('status'=>$cat_hide_show);
                $this->db->where('id',$value);
                $this->db->update('category',$data);
            }
        }
    }



    function get_cat_data($id = null){
     // $query = $this->db->query("SELECT `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`,`status`,sort_order FROM `category` WHERE 1");
        $query = $this->db->query("SELECT a.`term_taxonomy_id`,a.`scx_term_id`,a.`taxonomy_type`,a.`taxonomy_content`,a.`show_on_web`,a.`parent`,a.`count`,a.`created_by`,a.`modified_by`,a.`created_datetime`,a.`modified_datetime`,b.`id`,b.`term_name`,b.`term_slug` FROM `scx_term_taxonomy` as a, `scx_terms` as b WHERE a.`term_taxonomy_id` = b.`id` ");
        $res = $query->result_array();
        //echo '<pre>';print_r($res);exit;
      // $tree = $this->buildTree($res);
      //$res1 = $this->printAllValues1($tree,$this->ouptut,$id);
        $res1 = $this->printAllValues1($res,$this->ouptut,$id);       
        //return $res1;
        return $res1;

    }


    /*public function get_cat_data($id = '')
    {
        $this->db->select('tt.*,t.*');
        $this->db->from('term_taxonomy as tt');
        $this->db->join('terms as t', 'tt.term_taxonomy_id = t.id');
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        return false;
    }*/


    public function add_term($arrData = array())
    {
        $this->db->insert('terms', $arrData);
        $insertId = $this->db->insert_id();
        return $insertId;
    }

     public function add_term_taxonomy($arrData = array())
    {
        $this->db->insert('term_taxonomy', $arrData);
        $insertId = $this->db->insert_id();
        return $insertId;
    }

    public function get_categories_by_id($id)
    {
        $this->db->select('t.term_name,t.term_slug,tt.scx_term_id,tt.taxonomy_type,tt.taxonomy_content,tt.parent,tt.display_order');
        $this->db->from('terms as t');
        $this->db->join('term_taxonomy as tt', 't.id = tt.scx_term_id', 'INNER');
        $this->db->where('t.id', $id);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $res =  $query->row_array();
            return $res;
        }
        return false;
    }

    public function update_term($arrData = array(), $id='')
    {
        $this->db->where('id', $id);
        $this->db->update('terms', $arrData);
        return true;
    }

     public function update_term_taxonomy($arrData = array(), $id='')
    {
        $this->db->where('scx_term_id', $id);
        $this->db->update('term_taxonomy', $arrData);
        return true;
    }

    public function delete_term($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('terms'); 
        return true;
    }

    public function delete_term_taxonomy($id)
    {
        $this->db->where('scx_term_id', $id);
        $this->db->delete('term_taxonomy'); 
        return true;
    }

}
?>