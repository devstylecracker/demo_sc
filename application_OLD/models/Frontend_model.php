<?php
class Frontend_model extends CI_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	public function scxObject($taxonomy_id = '')
	{
		$this->db->select('o.id,o.object_name,o.slug,o.object_image,or.scx_term_taxonomy_id,om.object_meta_value');
		$this->db->from('object as o');
		$this->db->join('term_object_relationship as or','or.object_id = o.id','INNER');
		$this->db->join('object_meta as om','om.object_id = o.id','INNER');
		$this->db->where('scx_term_taxonomy_id',$taxonomy_id);
		$this->db->where('om.object_meta_key','price');
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}
		
	/*public function userMeta($arrData = array())
	{
		$this->db->insert('user_meta', $arrData);
		$metaId = $this->db->insert_id();
		return $metaId;
	}*/

	public function userMeta($arrData = array(),$user_id=null)
	{		
		foreach($arrData as $key=>$value)
		{
			$this->db->select('id,scx_user_id');
			$this->db->from('user_meta');
			$this->db->where('scx_user_id',$user_id);
			$this->db->where_in('meta_key',$value);
			$query = $this->db->get();
			
			if($query->num_rows()>0)
			{
				$this->db->where('scx_user_id',$user_id);
				$this->db->where('meta_key',$arrData['meta_key']);
				$this->db->update('user_meta', array('meta_value'=>$arrData['meta_value']));	
				return 'true';
			}else
			{
				$this->db->insert('user_meta', $arrData);
				$metaId = $this->db->insert_id();
				return $metaId;
			}
		}
		
	}
		
	public function orderMeta($arrData = array())
	{
		$this->db->insert('order_meta', $arrData);
		$metaId = $this->db->insert_id();
		return $metaId;
	}
		
	public function scxOrder($arrData = array())
	{
		$this->db->insert('scx_order', $arrData);
		$orderId = $this->db->insert_id();
		return $orderId;
	}

	public function getOrderDisplayId($orderid)
	{
		$this->db->select('id,order_id');
		$this->db->from('scx_order');
		$this->db->where('id',$orderid);		
		$query = $this->db->get();
		$result = $query->result_array();		
		return $result[0]['order_id'];
	}		
		
	public function getCategory($gender)
	{
		//$this->db->select('term_taxonomy_id,scx_term_id,taxonomy_content,parent');
		/* $this->db->select('tt.term_taxonomy_id as id,tt.taxonomy_content as name, t.term_slug,,tm.termmeta_key,tm.termmeta_value,tt.taxonomy_type,tt.taxonomy_content');
		$this->db->from('term_taxonomy as tt');
		$this->db->join('terms as t', 't.id = tt.scx_term_id', 'inner');
		$this->db->join('term_meta as tm', 't.id = tm.scx_term_id', 'inner');
		$this->db->where('tt.parent', $gender);
		$this->db->where('t.term_slug', 'category_selection');
		$this->db->order_by('tt.display_order', 'ASC');
		$query = $this->db->get();
		echo $this->db->last_query();exit;
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false; */
		$this->db->select('t.id as id,t.term_name as name');	
		$this->db->from('terms as t');
		$this->db->join('term_taxonomy as tt', 'tt.scx_term_id = t.id', 'INNER');
		$this->db->where('t.term_group', 'category');
		$this->db->where_in('tt.parent', $gender);		
		$this->db->order_by('tt.display_order', 'ASC');		
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}
	
	
	public function get_package_category($gender,$objectid)
	{
		/* $this->db->select('o.id as oid,tor.object_id as torid,tt.scx_term_id as ttid, o.object_name,o.slug,o.object_image,t.term_name,t.term_group,om.object_meta_value');
		$this->db->from('object as o');
		$this->db->join('term_object_relationship as tor', 'o.id = tor.object_id', 'INNER');
		$this->db->join('terms as t', 't.id = tor.scx_term_taxonomy_id', 'INNER');
		$this->db->join('object_meta as om', 'om.object_id = o.id', 'INNER');
		$this->db->join('term_taxonomy as tt', 'tt.scx_term_id = t.id', 'INNER');
		$this->db->where_in('tt.parent', $gender);
		$this->db->where('om.object_meta_key', 'instruction_key');		
		$query = $this->db->get(); */
		
		$this->db->select('t.id as id,t.term_name as name');
		$this->db->from('object as o');
		$this->db->join('term_object_relationship as tor', 'o.id = tor.object_id', 'INNER');
		$this->db->join('terms as t', 't.id = tor.scx_term_taxonomy_id', 'INNER');
		$this->db->join('object_meta as om', 'om.object_id = o.id', 'INNER');
		$this->db->join('term_taxonomy as tt', 'tt.scx_term_id = t.id', 'INNER');
		$this->db->where_in('tt.parent', $gender);
		$this->db->where('om.object_meta_key', 'instruction_key');	
		$this->db->where('o.id', $objectid);			
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if($query->num_rows()>0)
		{			 
			$res = $query->result_array();
			//echo '<pre>';print_r($res);exit;
			return $res;
		}
		return false;
	}
	
	public function get_object_property($objectid,$object_meta_key)
	{
		$this->db->select('om.object_meta_value');
		$this->db->from('object_meta as om');	
		$this->db->where('om.object_id', $objectid);
		$this->db->where('om.object_meta_key', $object_meta_key);		
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if($query->num_rows()>0)
		{
			$res = $query->result_array();			
			return $res[0]['object_meta_value'];
		}
		return false;
	}
	
	function register_emial_web($email=''){

        $this->db->select('id,email');
        $this->db->from('user');
        $this->db->where('email', $email);
        $query = $this->db->get();
         if($query->num_rows()>0) 
         {
            $id = $query->row_array(); 
			return $id['id'];
        }
	}
	function register_user_web($arrData = array()){
		$this->db->insert('user', $arrData) ;
		$userId = $this->db->insert_id();
		return $userId;
        
    }
	
	

	
	
	
} 
