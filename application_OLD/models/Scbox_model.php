<?php
class Scbox_model extends CI_Model {

    function __construct(){
		
		parent::__construct();
        
    }
	
	function registerUser($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$sc_gender,$json_array){
     
        $insert = 0;
        $picture = '';
        $facebook_id = '';
        $google_id = '';
        $event_code = '';
        $birthday = '';
        $scmobile = @$json_array['scmobile'];
        $fbbirthday = @$json_array['birthday'];
		$json_array['firstname'] = ucfirst(strtolower($json_array['firstname']));
		$json_array['last_name'] = ucfirst(strtolower($json_array['last_name']));
		    $medium_array = array("facebook", "google", "event", "mobile_ios_facebook", "mobile_android_facebook", "mobile_android_google", "mobile_ios_google");	
        if($fbbirthday!='')
        {
           $date = str_replace('/', '-', $fbbirthday);
           $birthday = date('Y-m-d', strtotime($date)); 
        }       
        

          if ($this->agent->is_browser())
          {
              $agent = 'Computer '.$this->agent->browser().' '.$this->agent->version();
          }
          elseif ($this->agent->is_robot())
          {
              $agent = 'Robot '.$this->agent->robot();
          }
          elseif ($this->agent->is_mobile())
          {
              $agent = 'Mobile '.$this->agent->mobile();
          }
          else
          {
              $agent = 'Unidentified';
          }

            $platform_info = $this->agent->platform();

        if(isset($medium) && (in_array($medium, $medium_array))){
            if($medium === 'google'){
                $picture = $json_array['picture'];
                $google_id = $json_array['id'];
            }
            elseif($medium === 'facebook') {
                //$picture = $json_array['picture']['data']['url'];
                $facebook_id = $json_array['id'];
                

            }elseif($medium === 'event')/* Added by Sudha For Event*/
            {
                $event_code = $json_array['event_code'];
                $medium ='website';
            }            
        }

        if($this->emailUnique($scemail_id) != NULL){
                 $query = $this->db->query('select id from user_login where email="'.$scemail_id.'"');
                 $result = $query->result_array();
                 $insert = 1;
            }
            
        if($insert===0){
        /***Code Ends***/
            $salt = substr(md5(uniqid(rand(), true)), 0, 10);
            $password   =$salt . substr(sha1($salt . $scpassword), 0, -10);
            $data = array(
			   'user_name' => $scusername,
			   'email' => $scemail_id,
			   'password' => $password,
			   'auth_token'=> substr(md5(uniqid(rand(1,6))), 0, 15),
			   'ip_address' => $ip,
			   'status' => 1,
			   'created_datetime' => $this->config->item('sc_date'),
			   'modified_datetime' => $this->config->item('sc_date'),
			   'event_code' => $event_code

            );
            $this->db->insert('user_login', $data);
            $Lst_ins_id = $this->db->insert_id();
            if($Lst_ins_id){
                $data = array('facebook_id'=>$facebook_id,'google_id'=>$google_id,'profile_pic'=>$picture, 'user_id' => $Lst_ins_id, 'first_name' => $json_array['firstname'],'last_name' => $json_array['lastname'],  'created_datetime' => $this->config->item('sc_date'),
               'modified_datetime' => $this->config->item('sc_date'), 'registered_from'=>$medium,'created_by' => $Lst_ins_id, 'modified_by' => $Lst_ins_id, 'gender' => $sc_gender,'gender_c'=>'1','platform_name'=>$platform_info,'contact_no'=>$scmobile,'birth_date'=>$birthday);

                $this->db->insert('user_info', $data);

                $data = array('user_id'=>$Lst_ins_id,'role_id'=>$role,'created_by'=>$Lst_ins_id,'modified_by'=>$Lst_ins_id);

                $this->db->insert('user_role_mapping', $data);

				$data = array('user_id'=>$Lst_ins_id,'question_id'=>$json_array['question_id'],'answer_id'=>$json_array['age_range'],'created_by'=>$Lst_ins_id,'modified_by'=>$Lst_ins_id, 'created_datetime' => $this->config->item('sc_date'),'modified_datetime' => $this->config->item('sc_date'),'is_active' =>1);

                $this->db->insert('users_answer', $data);
				
                $res = $this->login_user($scemail_id,$scpassword,$medium,'');

                if(strlen($facebook_id)>1 || strlen($google_id)>1)
                    return "pa";
				
				if($medium ==='facebook' || $medium === 'google' || $medium === 'website'){
					return $Lst_ins_id;
				}else{
					return $res;
				}
            }
        }else{
            
            $query = $this->db->query("select profile_pic from user_info where user_id = ".$result[0]['id']);
            $res = $query->result();
            if(empty($res[0]->profile_pic) ){
                $data = array('facebook_id'=>$facebook_id,'google_id'=>$google_id,'profile_pic'=>$picture, 'registered_from'=>$medium,'modified_by' => $result[0]['id'],'modified_datetime' => $this->config->item('sc_date'),'gender_c'=>'1','birth_date'=>$birthday);
            }else{
                $data = array('facebook_id'=>$facebook_id,'google_id'=>$google_id,'registered_from'=>$medium,'modified_by' => $result[0]['id'],'modified_datetime' => $this->config->item('sc_date'),'gender_c'=>'1','birth_date'=>$birthday);
            }
            $this->db->update('user_info', $data, array('user_id' => $result[0]['id'] ));
            $query = $this->db->query("select user_role_mapping.role_id,user_info.user_id,user_login.user_name,user_login.email,user_info.created_datetime,user_info.first_name,user_info.last_name,user_login.bucket,user_login.question_comp,user_login.question_resume_id,user_info.gender,user_info.gender_c,user_info.contact_no from user_info,user_role_mapping,user_login where user_info.user_id = ".$result[0]['id']." and user_info.user_id = user_role_mapping.user_id and user_info.user_id = user_login.id");
            $res = $query->result();

            if(!empty($res)){
				if($medium != 'website'){
					$data = array('auth_token' => substr(md5(uniqid(rand(1,6))), 0, 15));
					$this->db->update('user_login', $data, array('id' =>$res[0]->user_id));//update token
				}
				$data_attempts = array('user_id'=>$res[0]->user_id,'login_time'=>$this->config->item('sc_date'));
				$this->db->insert('user_login_attempts', $data_attempts);
				$login_attempts_id = $this->db->insert_id();

				$array_field=array('user_name' => $res[0]->user_name,'user_id' => $res[0]->user_id,'role_id'=>$res[0]->role_id,'first_name'=>$res[0]->first_name,'last_name'=>$res[0]->last_name,'bucket'=>$res[0]->bucket,'question_comp'=>$res[0]->question_comp,'question_resume_id'=>$res[0]->question_resume_id,'login_attempts_id'=>$login_attempts_id,'email' => $res[0]->email,'gender' => $res[0]->gender,'gender_c' => $res[0]->gender_c,'contact_no' => $res[0]->contact_no);

				$query_res = $this->db->get_where('user_address', array('user_id' => $res[0]->user_id,'is_default'=>1));
				$res_res = $query_res->result_array();

				if($medium ==='facebook' || $medium === 'google' || $medium === 'website'){
					if($res[0]->bucket == 0)
					return 'pa';
					else
					return 'profile';
				}else{
					return $array_field;
				}
				
            }else{
                return 2;
            }

        }
    }
	
	function emailUnique($emaild){
        $query = $this->db->query('select email from user_login where email ="'.$emaild.'"');
        $username_unique = $query->result_array();
        return $username_unique;
    }
	
	function login_user($email,$password,$medium,$type){
        
        $query = $this->db->get_where('user_login', array('email' => $email,'status'=>1));
        $result = $query->result();
        if(!empty($result)){
        $salt = substr($result[0]->password, 0, 10);

        $db_password =  $salt . substr(sha1($salt . $password), 0, -10);

        if($db_password == $result[0]->password || $type == 'facebook' || $type == 'google'){

            $query = $this->db->query("select user_role_mapping.role_id,user_info.user_id,user_info.created_datetime,user_info.first_name,user_info.last_name,user_login.email,user_login.bucket,user_login.question_comp,user_login.question_resume_id,user_login.user_name,user_info.gender,user_info.gender_c,user_info.contact_no from user_info,user_role_mapping,user_login where user_info.user_id = ".$result[0]->id." and user_info.user_id = user_role_mapping.user_id and user_info.user_id = user_login.id");
            $res = $query->result();
            if(!empty($res)){
                $data_attempts = array('user_id'=>$res[0]->user_id,'login_time'=>$this->config->item('sc_date'));
                $this->db->insert('user_login_attempts', $data_attempts);
                $login_attempts_id = $this->db->insert_id();

                $array_field=array('user_name' => $res[0]->user_name,'user_id' => $res[0]->user_id,'role_id'=>$res[0]->role_id,'first_name'=>$res[0]->first_name,'last_name'=>$res[0]->last_name,'bucket'=>$res[0]->bucket,'question_comp'=>$res[0]->question_comp,'question_resume_id'=>$res[0]->question_resume_id,'login_attempts_id'=>$login_attempts_id,'email' => $res[0]->email,'gender' => $res[0]->gender,'gender_c' => $res[0]->gender_c,'contact_no' => $res[0]->contact_no);

                $query_res = $this->db->get_where('user_address', array('user_id' => $res[0]->user_id,'is_default'=>1));
                $res_res = $query_res->result_array();
				if($medium ==='facebook' || $medium === 'google' || $medium === 'website'){
					return 1;
				}else return $array_field;
            }else{
                return 2;
            }
        }else{
                return 2;
            }
        }else{ return 2; }
    }
	
	public function update_couponcode($email,$couponcode){
		
        $query = $this->db->query('select * from `coupon_settings` where coupon_code = "'.$couponcode.'" ');
        $res = $query->result_array();
        $email_applied = '';        
        
        if(@$res[0]['coupon_email_applied']!=''){
			
            $email_applied = $res[0]['coupon_email_applied'].','.$email;
            $result = $this->db->query('update `coupon_settings` SET coupon_email_applied = "'.$email_applied.'" WHERE coupon_code = "'.$couponcode.'" ');
            return 1;
        }else
        {
            return 2;
        }
    }
	
	function get_userage_range($dob,$gender)
  {
      $agerangeid = 0;
      //$dob = "2007-02-14";
      $curdate = date('Y-m-d');
      $diff = abs(strtotime($curdate) - strtotime($dob));

      $years = floor($diff / (365*60*60*24));
     
      if(!empty($years) && $years>=16  && $gender==1)
      {
        if($years<=20)
        {
          $agerangeid = 39;
        }else if($years<=25)
        {
          $agerangeid = 40;
        }else if($years<=30)
        {
          $agerangeid = 41;
        }else if($years<=35)
        {
          $agerangeid = 42;
        }else if($years<=40)
        {
          $agerangeid = 43;
        }else if($years>40)
        {
          $agerangeid = 44;
        }

      }else
      {
        if($years<=21)
        {
          $agerangeid = 4683;
        }else if($years<=27)
        {
          $agerangeid = 4684;
        }else if($years<=35)
        {
          $agerangeid = 4685;
        }else if($years<=45)
        {
          $agerangeid = 4686;
        }else if($years>45)
        {
          $agerangeid = 4687;
        }
      }
      return $agerangeid;
	}
	
	function getpincodedata($pincode){
		
        if($pincode!='')
        {
            $query = $this->db->query(' select a.id,a.state_name,b.city from states as a,city as b,pincode as c where a.id = b.state_id AND b.id = c.city_id AND c.pincode = "'.$pincode.'" ');
            $result = $query->result_array();
            return @$result[0];
        }
        
    }
	
	function save_scboxuser_answer($answer_id,$user_id=0,$objectid=0,$question=0){


        $scbox_pack = unserialize(SCBOX_PACKAGE);
        // $query = $this->db->get_where('object_meta', array('object_id' => $objectid,'created_by' => $user_id,'object_meta_key' => $question ));
        $query = $this->db->get_where('object_meta', array('created_by' => $user_id,'object_meta_key' => $question ));

        $count = $query->num_rows(); //counting result from query

        
        if($count>0){
                /*Update the PA*/

                $data = array('object_meta_value'=>$answer_id,'created_by'=>$user_id,'object_status'=>1);
                //$this->db->update('object_meta', $data, array('object_id' => $objectid,'created_by' => $user_id,'object_meta_key' => $question ));    
                $this->db->update('object_meta', $data, array('created_by' => $user_id,'object_meta_key' => $question ));  
               // echo $this->db->last_query();exit;          

        }else{
            /*Insert the PA*/
            
            $data = array('object_id'=>$objectid,'object_meta_key'=>$question,'object_meta_value'=>$answer_id,'created_datetime'=>date('Y-m-d H:i:s'),'created_by'=>$user_id,'modified_by'=>$user_id);
            $this->db->insert('object_meta', $data);
			
			$query = $this->db->query('update user_login set question_comp = 1,question_resume_id = 1 where id = '.$user_id.' ');
        }
    }
	
	function get_scbox_userdata($user_id){
		$query = $this->db->get_where('object_meta', array('created_by' => $user_id));
		$result = $query->result_array();
		$scbox_array = array();$i=0;
		
		if(!empty($result)){
			foreach($result as $val){
				$scbox_array[$val['object_meta_key']]['object_id'] = $val['object_id'];
				$scbox_array[$val['object_meta_key']]['object_meta_key'] = $val['object_meta_key'];
				$scbox_array[$val['object_meta_key']]['object_meta_value'] = $val['object_meta_value'];
				$scbox_array[$val['object_meta_key']]['object_id'] = $val['object_id'];
				$i++;
			}
		}
		// echo "<pre>";print_r($scbox_array);exit;
		return $scbox_array;
	}
	
	function get_user_info($user_id){
		$result = $this->db->query("SELECT *FROM user_info where user_id = ".$user_id);
        $result = $result->result_array();
		return $result;
	}
	
	function update_user_info($first_name,$last_name,$user_id){
		$result = $this->db->query('UPDATE user_info SET first_name = "'.$first_name.'",last_name = "'.$last_name.'" where user_id = '.$user_id.' ');
		return true;
	}

	function get_scbox_data($object_id,$object_meta_key){
		$query = $this->db->query('select a.object_meta_value from object_meta as a where a.object_id = "'.$object_id.'" AND a.object_meta_key = "'.$object_meta_key.'" ');
		$result = $query->result_array();
		if(!empty($result)){
			return unserialize($result[0]['object_meta_value']);
		}else{
			return array();
		}
	}
}
?>