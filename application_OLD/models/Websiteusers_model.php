<?php
class Websiteusers_model extends CI_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_allusers($search_by,$table_search,$page,$per_page){ 
		if($search_by == '1'){ $search_cond = " AND a.user_name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '2'){ $search_cond = " AND a.email like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '3'){ $search_cond = " AND a.created_datetime like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '4'){ $search_cond = " AND a.id =".$table_search; }
		else if($search_by == '5'){ $search_cond = " AND a.user_name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '6'){ $search_cond = " AND c.first_name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '7'){ $search_cond = " AND c.last_name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '9'){ $search_cond = " AND c.registered_from like '%".strip_tags(trim($table_search))."%'"; }
		//else if($search_by == '11'){ $search_cond = " AND a.email like '%".strip_tags(trim($table_search))."%'"; }
			//else if($search_by == '12'){ $search_cond = " AND a.email like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '9')
		{
			{ $search_cond = " AND c.registered_from like '%".strip_tags(trim($table_search))."%'"; }


		}	
		else{ $search_cond =''; }
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}
		
		$query = $this->db->query("select a.id,a.user_name, a.bucket, a.created_datetime, a.email ,c.`first_name`,c.`last_name`,c.`registered_from`,c.`gender`,c.`contact_no`, d.`role_name` ,z.last_visit_time,c.registered_from from user_login as a,user_info as c, user_role_mapping as b ,role_master as d ,
		user_visit_history as z WHERE a.`status` =1 and a.`test_user`!=1 and  
		 a.`id` = b.`user_id` and b.`role_id`=2 and c.`user_id` = b.`user_id` and z.`id`=a.`id` and b.`role_id` = d.`id`   
		 ".$search_cond." order by a.id desc ".$limit_cond);
		$res = $query->result();
	//echo $this->db->last_query();
		return $res;
		echo $res;


		
	}

	function get_scboxusers($search_by,$table_search,$page='',$per_page='',$is_download='',$is_stylist_download=''){ 
		$order_by = ' order by a.id desc';$search_cond = '';$subquery='';
		if($search_by == '1'){ $search_cond = " AND a.user_name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '2'){ $search_cond = " AND a.email like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '3'){ $search_cond = " AND a.created_datetime like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '4'){ $search_cond = " AND a.id =".$table_search; }
		else if($search_by == '5'){ $search_cond = " AND a.user_name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '6'){ $search_cond = " AND c.first_name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '7'){ $search_cond = " AND c.last_name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '9'){ $search_cond = " AND c.registered_from like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '10'){ $order_by = " order by order_count asc "; }
		else if($search_by == '11'){ $order_by = " order by order_count desc "; }
		else if($search_by == '12'){ 
			$table_search = explode(',',base64_decode($table_search));
			$date_from = $table_search[0];
			$date_to = $table_search[1];
			$search_cond = " AND a.`created_datetime` >= '".$date_from." 00:00:00'  and a.`created_datetime` <= '".$date_to." 23:59:59' "; 
		}else{ $search_cond =''; }
		 
		if($page!='' || $per_page!=''){ 
			if($page != 1 && $page !=0){
			   $page = $page*$per_page-$per_page;
			}else{ $page = 0;}
			$limit_cond = "LIMIT ".$page.",".$per_page;
		}else{
			$limit_cond = '';	
		}
		if($is_download == '1'){
			$subquery = ' ,(select f.object_meta_value from object_meta as f where f.object_meta_key = "stylist_remark" AND a.id = f.created_by) as remarks,(select h.answer from users_answer as g,answers as h where a.id = g.user_id AND (g.question_id = 8 OR g.question_id  = 17 ) AND g.answer_id = h.id limit 0,1) as user_age ';
		}
		
		$query = $this->db->query("Select a.id,a.user_name, a.bucket, a.created_datetime, a.email ,c.`first_name`,c.`last_name`,c.`registered_from`,c.`gender`,c.`contact_no` as login_contact_no, d.`role_name` ,c.registered_from,(select count(oi.id) as order_count from order_info as oi where oi.user_id = a.id AND oi.brand_id = ".SCBOX_BRAND_ID." AND oi.order_status != 9 ) as order_count, c.`modified_datetime`, e.object_meta_value as contact_no,e.object_id ".$subquery." from user_login as a,user_info as c, user_role_mapping as b ,role_master as d,object_meta as e  WHERE a.`status` =1 and a.`test_user`!=1 and  a.`id` = b.`user_id` and b.`role_id`=2 and c.`user_id` = b.`user_id`  and b.`role_id` = d.`id` and a.`id` = e.`created_by` and e.`object_meta_key` LIKE 'user_mobile' ".$search_cond." ".$order_by." ".$limit_cond);	
		$res = $query->result();
		if($is_stylist_download == '1'){
			$user_data = array();$i=0;
			if(!empty($res)){
				foreach($res as $val){
					$user_data[$i] = $this->Scbox_model->get_scbox_userdata($val->id);
					$i++;
				}
			}
			return $user_data;
		}
		return $res;		
	}

	function get_scbox_userdata($user_id)
    {  
    	$data = array();
        $query = $this->db->query('select * from object_meta as a where object_id in ('.SCBOX_ID.') and a.created_by = '.$user_id.'  order by a.created_by desc ');
        $result = $query->result_array();
        //echo '<pre>';print_r($result);exit; 
        foreach($result as $val)
        {
        	$data[$val['object_meta_key']] = $val['object_meta_value'];
        }   
        //echo '<pre>';print_r($data);exit;    
        return $data;
    }

    function get_size_data($id)
    {
        $filter='';       
        if(!empty($id))
        {           
            $query = $this->db->query('select a.id,a.size_text as answer from product_size as a where  a.id IN ('.$id.') order by a.order ');            
            $result = $query->result_array();
            return $result;
        }
    } 

    function get_state($id){
        $query = $this->db->query("select * from `states` as a where a.`id` = '".$id."' ");
        $res = $query->result_array();       
        return $res[0]['state_name'];
    }

	

 } 
?>
