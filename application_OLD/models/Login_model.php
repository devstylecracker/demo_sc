<?php
class Login_model extends CI_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function login_user($email,$password){
	
		$query = $this->db->get_where('scx_user', array('email' => $email,'status'=>1));
		$result = $query->result();
		if(!empty($result)){
		$salt = substr($result[0]->password, 0, 10);
			
		$db_password =  $salt . substr(sha1($salt . $password), 0, -10); 
	
		if($db_password == $result[0]->password){

			$query = $this->db->query("select user_role_mapping.id,user_info.id,user_info.created_datetime,user_info.first_name,user_info.last_name from scx_user as user_info,scx_role_mappings as user_role_mapping where user_info.id = ".$result[0]->id." and user_info.id = user_role_mapping.user_id");
			$res = $query->result();
			
			//$res = $this->db->get()->result_array();
			if(!empty($res)){
				if($res[0]->id != 2)
				{
					$array_field=array('user_id' => $res[0]->id,'role_id'=>$res[0]->id,'first_name'=>$res[0]->first_name,'last_name'=>$res[0]->last_name,'created_datetime'=>$res[0]->created_datetime);
					
					$this->session->set_userdata($array_field);
				
					return 1;
				}else
				{
					return 2;
				}
			}else{
				return 2;	
			}
		}else{ 
				return 2;
			}
		}else{ return 2; } 
	}
	
	
	
	public function verify_login($email='', $password='')
		{
			$salt = substr($password, 0, 10);
			$db_password =  $salt . substr(sha1($salt . $password), 0, -10); 
			$this->db->where('email',$email);
			$this->db->where('password',$db_password);
			$this->db->where('status',1);
			$query = $this->db->get("user");
			if ($query->num_rows() > 0)
			{
				$result = $query->result();
				return $result[0]->id;
			}

			return false;

		}

	
 } 
?>
