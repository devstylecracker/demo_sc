<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends MY_Model {
	
	 function __construct()
    {
        // Call the Model constructor
       
        parent::__construct();
    }	

     public function get_total_users($data = array())
    {
        $this->db->select('u.id');
        $this->db->from('scx_user as u');
        $total_records = $this->db->count_all_results();
        return  $total_records;
    }

    public function get_users($limit='0', $page_num='0',$data = array())
    {
        $start = ($page_num  == NULL) ? 0 :($page_num * $limit) - $limit;
        if($start <0)
        {
            $start = 0;
        }
        $this->db->select('u.id,u.username,u.email,u.first_name,u.last_name,u.user_type,u.created_datetime');
        $this->db->from('scx_user as u');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
            
            if ($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            return false;
    }

	 /**************************  START INSERT QUERY ***************/
    public function insert_data($data){
        $this->db->insert('scx_user', $data); 
        return TRUE;
    }
    /**************************  END INSERT QUERY ****************/


    /*************  START EDIT PARTICULER DATA QUERY *************/
    public function edit_user_by_id($id){
		$this->db->from('user');
		$this->db->where('id', $id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		return false;
    }
    /*************  END EDIT PARTICULER DATA QUERY ***************/

	public function update_user($arrData = array(), $id='')
	{
		$this->db->where('id', $id);
		$this->db->update('user',$arrData);
		return true;
	}

	/*public function delete_role($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scx_role_master'); 
        return true;
    }*/

    public function delete_user_meta($id)
    {
        $this->db->where('scx_user_id', $id);
        $this->db->delete('user_meta'); 
        return true;
    }

	public function delete_user($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('user'); 
        return true;
    }

}
?>