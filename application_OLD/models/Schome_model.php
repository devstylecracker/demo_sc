<?php
class Schome_model extends MY_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_homepage_looks($stylist_offset,$street_offset,$blog_offset,$filter_value){
        /*
            STYLIST LOOK WITHOUT FILTER
        */

        if($stylist_offset >=0 && $filter_value == ''){
       $stylist_off = $this->config->item('stylist_plus_promo_look')*$stylist_offset;

        $query = $this->db->query('select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc limit '.$stylist_off.',12');

 		$result['stylist_look'] = $query->result_array();
        $result['offset']['stylist'] = ($stylist_offset+1);
        }

        /*
            STYLIST LOOK WITH FILTER
        */

        if($stylist_offset >=0 && $filter_value != ''){

        $stylist_off = $this->config->item('stylist_plus_promo_look')*$stylist_offset;

        $query = $this->db->query('select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 and a.look_type = "'.$filter_value.'") as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc limit '.$stylist_off.',12');

        $result['stylist_look'] = $query->result_array();
        $result['offset']['stylist'] = ($stylist_offset+1);

        }


        /*
            BLOG WITHOUT FILTER
        */

        if($blog_offset >=0 && $filter_value == ''){

        $blog_off = $this->config->item('blog_look')*$blog_offset;

        $query = $this->db->query('SELECT a.ID,a.post_date,a.post_title,a.post_status,a.post_name FROM '.BLOG_DATABASE_NAME.'.wp_posts as a WHERE a.post_status = "publish" and a.post_type = "post" order by a.post_date desc limit '.$blog_off.',2');

        $result_blog = $query->result_array();
        $result['offset']['blog'] = ($blog_offset+1);

        }

        /*
            BLOG WITH FILTER
        */

        if($blog_offset >=0 && $filter_value == 5){

        $blog_off = $this->config->item('blog_look')*$blog_offset;

        $query = $this->db->query('SELECT a.ID,a.post_date,a.post_title,a.post_status,a.post_name FROM '.BLOG_DATABASE_NAME.'.wp_posts as a WHERE a.post_status = "publish" and a.post_type = "post" order by a.post_date desc limit '.$blog_off.',2');

        $result_blog = $query->result_array();
        $result['offset']['blog'] = ($blog_offset+1);

        }

        if(!empty($result_blog)) { $o = 0;
            foreach($result_blog as $val){
                $result['blog']['name'][$o] = $val['post_title'];
                $result['blog']['post_name'][$o] = $val['post_name'];
                $result['blog']['post_date'][$o] = $val['post_date'];

               // $result['blog']['second_img'] = $this->get_post_second_thumb($val['ID']);

                $blog_img_ext = strrchr($this->get_post_thumb($val['ID']),'.');
                $add_blog_thumb_dim = '-374x212'.$blog_img_ext;


                $new_blog_img = str_replace($blog_img_ext,$add_blog_thumb_dim,$this->get_post_thumb($val['ID']));

                //$new_blog_img = $this->get_post_thumb($val['ID']);

                $url = $new_blog_img;
                $headers = @get_headers($url);
                if(strpos($headers[0],'404') === false)
                {
                     $result['blog']['guid'][$o] =$url;
                }
                else
                {
                    $result['blog']['guid'][$o] =$this->get_post_thumb($val['ID']);
                }

                //$result['blog']['guid'][$o] =$new_blog_img;
            $o++; }
        }

         if($stylist_offset >=0 && $filter_value == '6'){
            $stylist_off = $this->config->item('stylist_plus_promo_look')*$stylist_offset;

            $query = $this->db->query('select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime from
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN ((select distinct look_id from look_category_mapping where cat_id = 5)) ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4) order by z.modified_datetime desc limit '.$stylist_off.',12');

            $result['stylist_look'] = $query->result_array();
            $result['offset']['stylist'] = ($stylist_offset+1);
        }

         if($stylist_offset >=0 && $filter_value == '7'){
            $stylist_off = $this->config->item('stylist_plus_promo_look')*$stylist_offset;

            $query = $this->db->query('select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime from
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN ((select distinct look_id from look_category_mapping where cat_id = 40)) ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4) order by z.modified_datetime desc limit '.$stylist_off.',12');

            $result['stylist_look'] = $query->result_array();
            $result['offset']['stylist'] = ($stylist_offset+1);
        }

          if($stylist_offset >=0 && $filter_value == '8'){
            $stylist_off = $this->config->item('stylist_plus_promo_look')*$stylist_offset;

            $query = $this->db->query('select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime from
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN ((select distinct lp.look_id from discount_availed as da,look_products as lp,product_desc as pd where NOW() >= da.start_date AND NOW() <= da.end_date AND lp.product_id = pd.id  AND da.status=1 and (da.product_id = lp.product_id))) ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4) order by z.modified_datetime desc limit '.$stylist_off.',12');

            $result['stylist_look'] = $query->result_array();
            $result['offset']['stylist'] = ($stylist_offset+1);
        }

 		return $result;
	}

     function get_post_thumb($id){

        $query = $this->db->query('select b.guid from '.BLOG_DATABASE_NAME.'.`wp_postmeta` as a,'.BLOG_DATABASE_NAME.'.wp_posts as b where a.`meta_key` = "_thumbnail_id" and a.`meta_value` = b.`ID` and a.`post_id`='.$id);

        $result = $query->result_array();
        if(!empty($result)){
            return $result[0]['guid'];
        }else{
            return '';
        }
    }

    function get_stylist_look_count(){
        $query = $this->db->query('select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc');

        $stylist_look = $query->result_array();
        return $stylist_look;
    }

    function get_promotional_look_count(){
        $query = $this->db->query('select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 and a.look_type != 3) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc');

        $stylist_look = $query->result_array();
        return $stylist_look;
    }

    function get_street_look_count(){
        $query = $this->db->query('select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 and a.look_type != 2) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc');

        $stylist_look = $query->result_array();
        return $stylist_look;
    }

    function get_blog_count(){
        /*$query = $this->db->query('SELECT a.ID,a.post_date,a.post_title,a.post_status FROM '.BLOG_DATABASE_NAME.'.wp_posts as a WHERE a.post_status = "publish" and a.post_type = "post" order by a.post_date desc');

        $stylist_look = $query->result_array();
        return $stylist_look;*/

    }
    function usernameUnique($username){
        $query = $this->db->query('select user_name from user_login where user_name ="'.$username.'"');

        $username_unique = $query->result_array();
        return $username_unique;

    }

    function emailUnique($emaild){
        $query = $this->db->query('select email from user_login where email ="'.$emaild.'"');
        $username_unique = $query->result_array();
        return $username_unique;
    }

    function registerUser($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$sc_gender=1,$json_array){
        /*added by Hari for facebook and google login*/
        $insert = 0;
        $picture = '';
        $facebook_id = '';
        $google_id = '';
        $event_code = '';
        $birthday = '';     
        $scmobile = @$json_array['scmobile'];       
        $fbbirthday = @$json_array['birthday'];         
        if($fbbirthday!='')     
        {       
           $date = str_replace('/', '-', $fbbirthday);      
           $birthday = date('Y-m-d', strtotime($date));         
        }     

        if ($this->agent->is_browser())
          {
              $agent = 'Computer '.$this->agent->browser().' '.$this->agent->version();
          }
          elseif ($this->agent->is_robot())
          {
              $agent = 'Robot '.$this->agent->robot();
          }
          elseif ($this->agent->is_mobile())
          {
              $agent = 'Mobile '.$this->agent->mobile();
          }
          else
          {
              $agent = 'Unidentified';
          }

        $platform_info = $this->agent->platform();

        if(isset($medium) && ($medium ==='facebook' || $medium === 'google' ||  $medium === 'event')){
            if($medium === 'google'){
                $picture = $json_array['picture'];
                $google_id = $json_array['id'];
            }
            elseif($medium === 'facebook') {
                //$picture = $json_array['picture']['data']['url'];
                $facebook_id = $json_array['id'];

            }elseif($medium === 'event')/* Added by Sudha For Event*/
            {
                $event_code = $json_array['event_code'];
                $medium ='website';
            }
            /*if($this->usernameUnique($scusername) != NULL){
                 $scusername = $scusername.rand(1,1000);
            }*/
            if($this->emailUnique($scemail_id) != NULL){
                 $query = $this->db->query('select id from user_login where email="'.$scemail_id.'"');
                 $result = $query->result_array();
                 $insert = 1;
            }
        }


        if($insert===0){
        /***Code Ends***/
            $salt = substr(md5(uniqid(rand(), true)), 0, 10);
            $password   =$salt . substr(sha1($salt . $scpassword), 0, -10);
            $data = array(
               'user_name' => $scusername,
               'email' => $scemail_id,
               'password' => $password,
               'ip_address' => $ip,
               'status' => 1,
               'created_datetime' => $this->config->item('sc_date'),
               'modified_datetime' => $this->config->item('sc_date'),
               'event_code' => $event_code

            );
            $this->db->insert('user_login', $data);
            $Lst_ins_id = $this->db->insert_id();
            if($Lst_ins_id){
                $data = array('facebook_id'=>$facebook_id,'google_id'=>$google_id,'profile_pic'=>$picture, 'user_id' => $Lst_ins_id, 'first_name' => $json_array['firstname'],'last_name' => $json_array['lastname'],  'created_datetime' => $this->config->item('sc_date'),
               'modified_datetime' => $this->config->item('sc_date'), 'registered_from'=>$medium,'created_by' => $Lst_ins_id, 'modified_by' => $Lst_ins_id, 'gender' => $sc_gender,'platform_name'=>$platform_info,'contact_no'=>$scmobile,'birth_date'=>$birthday);

                $this->db->insert('user_info', $data);

                $data = array('user_id'=>$Lst_ins_id,'role_id'=>$role,'created_by'=>$Lst_ins_id,'modified_by'=>$Lst_ins_id);

                $this->db->insert('user_role_mapping', $data);

                $this->login_user($scemail_id,$scpassword);

                if(strlen($facebook_id)>1 || strlen($google_id)>1)
                    return "pa";

                return true;
            }
        }else{
            
            /*edit section written by Hari on 08 Sep 2015 */
            $query = $this->db->query("select profile_pic from user_info where user_id = ".$result[0]['id']);
            $res = $query->result();
            if(empty($res[0]->profile_pic) ){
                $data = array('facebook_id'=>$facebook_id,'google_id'=>$google_id,'profile_pic'=>$picture, 'registered_from'=>$medium,'modified_by' => $result[0]['id'],'modified_datetime' => $this->config->item('sc_date'),'birth_date'=>$birthday);
            }else{
                $data = array('facebook_id'=>$facebook_id,'google_id'=>$google_id,'registered_from'=>$medium,'modified_by' => $result[0]['id'],'modified_datetime' => $this->config->item('sc_date'),'birth_date'=>$birthday);
            }
            $this->db->update('user_info', $data, array('user_id' => $result[0]['id'] ));
            $query = $this->db->query("select user_role_mapping.role_id,user_info.user_id,user_login.user_name,user_login.email,user_info.created_datetime,user_info.first_name,user_info.last_name,user_login.bucket,user_login.question_comp,user_login.question_resume_id,user_info.gender from user_info,user_role_mapping,user_login where user_info.user_id = ".$result[0]['id']." and user_info.user_id = user_role_mapping.user_id and user_info.user_id = user_login.id");
            $res = $query->result();

            if(!empty($res)){
                $data_attempts = array('user_id'=>$res[0]->user_id,'login_time'=>$this->config->item('sc_date'));
                $this->db->insert('user_login_attempts', $data_attempts);
                $login_attempts_id = $this->db->insert_id();

                  $array_field=array('user_name' => $res[0]->user_name,'user_id' => $res[0]->user_id,'role_id'=>$res[0]->role_id,'first_name'=>$res[0]->first_name,'last_name'=>$res[0]->last_name,'bucket'=>$res[0]->bucket,'question_comp'=>$res[0]->question_comp,'question_resume_id'=>$res[0]->question_resume_id,'login_attempts_id'=>$login_attempts_id,'email' => $res[0]->email,'gender' => $res[0]->gender,'gender_c' => $res[0]->gender_c,'utm_source1'=>$this->session->userdata('utm_source1'));

                $query_res = $this->db->get_where('user_address', array('user_id' => $res[0]->user_id,'is_default'=>1));
                $res_res = $query_res->result_array();
                if(!empty($res_res)){ $array_field['pincode'] = $res_res[0]['pincode']; 
                    setcookie('stylecracker_shipping_pincode', $res_res[0]['pincode'], time() + ((86400 * 2) * 30), "/", "",  0);
                }
                
                $this->session->set_userdata($array_field);
                if($res[0]->bucket == 0)
                    return 'pa';
                else
                    return 'profile';
            }else{
                return 2;
            }

        }
    }


     function login_user($email,$password){
        
        $query = $this->db->get_where('user_login', array('email' => $email,'status'=>1));
        $result = $query->result();
        if(!empty($result)){
        $salt = substr($result[0]->password, 0, 10);

        $db_password =  $salt . substr(sha1($salt . $password), 0, -10);

        if($db_password == $result[0]->password){

            $query = $this->db->query("select user_role_mapping.role_id,user_info.user_id,user_info.created_datetime,user_info.first_name,user_info.last_name,user_login.email,user_login.bucket,user_login.question_comp,user_login.question_resume_id,user_login.user_name,user_info.gender,user_info.gender_c from user_info,user_role_mapping,user_login where user_info.user_id = ".$result[0]->id." and user_info.user_id = user_role_mapping.user_id and user_info.user_id = user_login.id");
            $res = $query->result();
            if(!empty($res)){

                $data_attempts = array('user_id'=>$res[0]->user_id,'login_time'=>$this->config->item('sc_date'));
                $this->db->insert('user_login_attempts', $data_attempts);
                $login_attempts_id = $this->db->insert_id();

                $array_field=array('user_name' => $res[0]->user_name,'user_id' => $res[0]->user_id,'role_id'=>$res[0]->role_id,'first_name'=>$res[0]->first_name,'last_name'=>$res[0]->last_name,'bucket'=>$res[0]->bucket,'question_comp'=>$res[0]->question_comp,'question_resume_id'=>$res[0]->question_resume_id,'login_attempts_id'=>$login_attempts_id,'email' => $res[0]->email,'gender' => $res[0]->gender,'gender_c' => $res[0]->gender_c,'utm_source1'=>$this->session->userdata('utm_source1'));

                $query_res = $this->db->get_where('user_address', array('user_id' => $res[0]->user_id,'is_default'=>1));
                $res_res = $query_res->result_array();
                if(!empty($res_res)){ $array_field['pincode'] = $res_res[0]['pincode']; 
                setcookie('stylecracker_shipping_pincode', $res_res[0]['pincode'], time() + ((86400 * 2) * 30), "/", "",  0);
                }

                $this->session->set_userdata($array_field);



                return 1;
            }else{
                return 2;
            }
        }else{
                return 2;
            }
        }else{ return 2; }
    }

    function get_users_info(){

         $user_id = $this->session->userdata('user_id');

         $query = $this->db->query("select user_role_mapping.role_id,user_info.user_id,user_info.created_datetime,user_info.first_name,user_info.last_name,user_login.bucket,user_login.question_comp,user_login.question_resume_id,user_info.registered_from,user_info.facebook_id,user_info.google_id,user_info.profile_pic,user_info.profile_cover_pic from user_info,user_role_mapping,user_login where user_info.user_id = ".$user_id." and user_info.user_id = user_role_mapping.user_id and user_info.user_id = user_login.id");
            $res = $query->result();
            return $res;
    }

	function get_answer_id($question_id){
		$user_id = $this->session->userdata('user_id');
		$query = $this->db->query("select answer_id from users_answer where user_id = $user_id AND question_id = $question_id ");
		$res = $query->result_array();
		if(!empty($res)){
			return $res[0];
		}else return null;
	}

    function get_otherthan_homepage_looks($stylist_offset,$street_offset,$filter_value,$page_type,$page_type_id){

        /*
            Page
        */
        $user_id = $this->session->userdata('user_id');
        $where_condition = '';
        if($page_type == 'profile'){
            // $bucket_id = $this->get_users_info()[0]->bucket;
            // $where_condition = $where_condition.' and m.bucket_id='.$bucket_id;
			$question_id = '7';
            $CI =& get_instance();
            $CI->load->model('Pa_model');
            if($this->Pa_model->get_gender($this->session->userdata('user_id')) == 2){
                 $question_id = '18';
            }
			$budget_id = $this->get_answer_id($question_id);
            $bucket_id = $this->get_users_info()[0]->bucket;
			if(!empty($bucket_id)){
				
				if($question_id == '18'){
					if($budget_id['answer_id'] == '4688')
					{
						$where_condition = $where_condition.' and m.bucket_id='.$bucket_id.' AND m.budget = '.$budget_id['answer_id'];
					}else if($budget_id['answer_id'] == '4689'){
						$where_condition = $where_condition.' and m.bucket_id='.$bucket_id.' AND m.budget in (4688,4689)';
					}else if($budget_id['answer_id'] == '4690'){
						$where_condition = $where_condition.' and m.bucket_id='.$bucket_id.' AND m.budget in (4688,4689,4690)';
					}else if($budget_id['answer_id'] == '4691'){
						$where_condition = $where_condition.' and m.bucket_id='.$bucket_id.' AND m.budget in (4688,4689,4690,4691)';
					}
				}else if($question_id == '7'){
					if($budget_id['answer_id'] == '36')
					{
						$where_condition = $where_condition.' and m.bucket_id='.$bucket_id.' AND m.budget = '.$budget_id['answer_id'];
					}else if($budget_id['answer_id'] == '37'){
						$where_condition = $where_condition.' and m.bucket_id='.$bucket_id.' AND m.budget in (36,37)';
					}else if($budget_id['answer_id'] == '38'){
						$where_condition = $where_condition.' and m.bucket_id='.$bucket_id.' AND m.budget in (36,37,38)';
					}else if($budget_id['answer_id'] == '4670'){
						$where_condition = $where_condition.' and m.bucket_id='.$bucket_id.' AND m.budget in (36,37,38,4670)';
					}
				}
			
			}else
            $where_condition = $where_condition.' and m.bucket_id='.$bucket_id;

        }else if($page_type == 'stylist'){
            $stylist_id =  $page_type_id;
            $where_condition = $where_condition.' and k.stylist_id='.$stylist_id;
        }else if($page_type == 'brands'){
            $stylist_id =  $page_type_id;
            $where_condition = $where_condition.' and i.`brand_id` = '.$stylist_id;
        }else if($page_type == 'look'){
             $stylist_id =  $page_type_id;
            $where_condition = $where_condition.' and look_id = '.$stylist_id;
        }else if($page_type == 'wishlist'){
            $where_condition = $where_condition.' and h.`user_id` = '.$user_id;
        }else if($page_type == 'search'){
            /*$where_condition = $where_condition.' and LOWER(a.`name`) LIKE LOWER("%'.$page_type_id.'%")';*/
              $where_condition = $where_condition.'where `name` LIKE LOWER("%'.urldecode($page_type_id).'%") or `look_tags` LIKE LOWER
        ("%'.urldecode($page_type_id).'%")';
        }

if($page_type != 'brands' && $page_type != 'look' && $page_type != 'wishlist' && $page_type != 'search'){
         /*
            STYLIST LOOK WITHOUT FILTER
        */

        if($stylist_offset >=0 && $filter_value == ''){
        $stylist_off = $this->config->item('stylist_plus_promo_look')*$stylist_offset;

        $query = $this->db->query('select distinct k.id,k.slug,product_img,k.look_type,k.image,look_image,name,k.created_datetime,k.modified_datetime,k.broadcast_datetime,k.status,k.deflag,k.stylist_id from
(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.broadcast_datetime,z.status,z.deflag,z.`stylist_id` from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.`broadcast_datetime`,a.status,a.deflag,a.`stylist_id` FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc) as k,look_bucket_mapping as m where k.id = m.look_id '.$where_condition.' order by k.id desc,k.broadcast_datetime desc limit '.$stylist_off.',12');

        $result['stylist_look'] = $query->result_array();
        $result['offset']['stylist'] = ($stylist_offset+1);
        }

        /*
            STYLIST LOOK WITH FILTER
        */

        if($stylist_offset >=0 && $filter_value != '' && $filter_value != 6){

        $stylist_off = $this->config->item('stylist_plus_promo_look')*$stylist_offset;

        $query = $this->db->query('select distinct k.id,k.slug,product_img,k.look_type,k.image,look_image,name,k.created_datetime,k.modified_datetime,k.status,k.deflag,k.stylist_id from
(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag,z.`stylist_id` from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag,a.`stylist_id` FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 and a.look_type = '.$filter_value.') as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc) as k,look_bucket_mapping as m where k.id = m.look_id '.$where_condition.' order by k.modified_datetime desc limit '.$stylist_off.',12');

        $result['stylist_look'] = $query->result_array();
        $result['offset']['stylist'] = ($stylist_offset+1);

        }

        if($stylist_offset >=0 && $filter_value == 6){

        $stylist_off = $this->config->item('stylist_plus_promo_look')*$stylist_offset;

        $query = $this->db->query('select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime from
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN ((select distinct j.look_id from look_category_mapping as j,look_bucket_mapping as u where j.look_id = u.look_id and j.cat_id = 5 and u.bucket_id="'.$this->get_users_info()[0]->bucket.'") )) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc limit '.$stylist_off.',12');

        $result['stylist_look'] = $query->result_array();
        $result['offset']['stylist'] = ($stylist_offset+1);

        }

        if($stylist_offset >=0 && $filter_value == 7){

        $stylist_off = $this->config->item('stylist_plus_promo_look')*$stylist_offset;
        /*echo 'select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime from
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN ((select distinct j.look_id from look_category_mapping as j,look_bucket_mapping as u where j.look_id = u.look_id and j.cat_id = 40 and u.bucket_id="'.$this->get_users_info()[0]->bucket.'") )) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc limit '.$stylist_off.',12';*/
        $query = $this->db->query('select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime from
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN ((select distinct j.look_id from look_category_mapping as j,look_bucket_mapping as u where j.look_id = u.look_id and j.cat_id = 40 and u.bucket_id="'.$this->get_users_info()[0]->bucket.'") )) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc limit '.$stylist_off.',12');

        $result['stylist_look'] = $query->result_array();
        $result['offset']['stylist'] = ($stylist_offset+1);

        }


}else if($page_type == 'brands'){


      /*
            STYLIST LOOK WITHOUT FILTER
        */

        if($stylist_offset >=0 && $filter_value == ''){
       $stylist_off = $this->config->item('stylist_plus_promo_look')*$stylist_offset;

        $query = $this->db->query('select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime from
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN (select distinct u.`look_id` from `look_products` as u,`product_desc` as i where u.`product_id` = i.`id` '.$where_condition.' )) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4|| z.look_type = 6) order by z.modified_datetime desc limit '.$stylist_off.',12');

        $result['stylist_look'] = $query->result_array();
        $result['offset']['stylist'] = ($stylist_offset+1);
        }


         /*
            STYLIST LOOK WITH FILTER
        */

        if($stylist_offset >=0 && $filter_value != ''){

        $stylist_off = $this->config->item('stylist_plus_promo_look')*$stylist_offset;

       $query = $this->db->query('select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime from
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.look_type ='.$filter_value.' and a.id IN (select distinct u.`look_id` from `look_products` as u,`product_desc` as i where u.`product_id` = i.`id` '.$where_condition.' ) ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4|| z.look_type = 6)  order by z.modified_datetime desc limit '.$stylist_off.',12');

        $result['stylist_look'] = $query->result_array();
        $result['offset']['stylist'] = ($stylist_offset+1);

        }


}else if($page_type == 'wishlist'){


      /*
            STYLIST LOOK WITHOUT FILTER
        */


        if($stylist_offset >=0 && $filter_value == ''){
       $stylist_off = $this->config->item('stylist_plus_promo_look')*$stylist_offset;

        $query = $this->db->query('select o.slug,o.id,o.product_img,o.look_type,o.image,o.look_image,name,o.created_datetime,o.modified_datetime,o.status,o.deflag from
(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) )as o,look_fav as h where o.`id` = h.`look_id` '.$where_condition.' order by o.modified_datetime limit '.$stylist_off.',12');

        $result['stylist_look'] = $query->result_array();
        $result['offset']['stylist'] = ($stylist_offset+1);
        }


         /*
            STYLIST LOOK WITH FILTER
        */

        if($stylist_offset >=0 && $filter_value != ''){

        $stylist_off = $this->config->item('stylist_plus_promo_look')*$stylist_offset;

       $query = $this->db->query('select o.slug,o.id,o.product_img,o.look_type,o.image,o.look_image,name,o.created_datetime,o.modified_datetime,o.status,o.deflag from
(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 and a.look_type = '. $filter_value.') as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) )as o,look_fav as h where o.`id` = h.`look_id` '.$where_condition.' order by o.modified_datetime limit '.$stylist_off.',12');

        $result['stylist_look'] = $query->result_array();
        $result['offset']['stylist'] = ($stylist_offset+1);

        }


} else if($page_type == 'search'){

        /*
            STYLIST LOOK WITHOUT FILTER
        */

        if($stylist_offset >=0 && $filter_value == ''){
       $stylist_off = $this->config->item('stylist_plus_promo_look')*$stylist_offset;

        /*$query = $this->db->query('select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 and a.status=1 '.$where_condition.') as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 || z.look_type = 6) order by z.id desc limit '.$stylist_off.',12');*/
        $query = $this->db->query('select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,LOWER(e.look_tags) as look_tags from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 and a.status=1 ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 || z.look_type = 6) LEFT JOIN look_tags as e ON z.id = e.look_id
 '.$where_condition.' order by z.id desc limit '.$stylist_off.',12');
        $result['stylist_look'] = $query->result_array();
        $result['offset']['stylist'] = ($stylist_offset+1);
        }


        /*
            STYLIST LOOK WITH FILTER
        */

        if($stylist_offset >=0 && $filter_value != ''){

        $stylist_off = $this->config->item('stylist_plus_promo_look')*$stylist_offset;

       /*$query = $this->db->query('select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 and a.status=1 '.$where_condition.' and a.look_type = '. $filter_value.') as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 || z.look_type = 6) order by z.id desc limit '.$stylist_off.',12');*/

          $query = $this->db->query('select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,LOWER(e.look_tags) as look_tags  from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 and a.status=1  and a.look_type = '. $filter_value.') as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 || z.look_type = 6)  LEFT JOIN look_tags as e ON z.id = e.look_id  '.$where_condition.'  order by z.id desc limit '.$stylist_off.',12');

        $result['stylist_look'] = $query->result_array();
        $result['offset']['stylist'] = ($stylist_offset+1);

        }


}else{


 /*
            STYLIST LOOK WITHOUT FILTER
        */

        if($stylist_offset >=0 && $filter_value == ''){
       $stylist_off = $this->config->item('stylist_plus_promo_look')*$stylist_offset;

        $query = $this->db->query('select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime from
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id != "'.$page_type_id.'" and a.id IN ((select distinct look_id from look_category_mapping where cat_id = (select cat_id from look_category_mapping where 1 '.$where_condition.')) )) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc limit '.$stylist_off.',12');

        $result['stylist_look'] = $query->result_array();
        $result['offset']['stylist'] = ($stylist_offset+1);
        }


         /*
            STYLIST LOOK WITH FILTER
        */

        if($stylist_offset >=0 && $filter_value != ''){

        $stylist_off = $this->config->item('stylist_plus_promo_look')*$stylist_offset;

       $query = $this->db->query('select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime from
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.look_type ='.$filter_value.' and a.id != "'.$page_type_id.'" and a.id IN ((select distinct look_id from look_category_mapping where cat_id = (select cat_id from look_category_mapping where 1 '.$where_condition.'))) ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc limit '.$stylist_off.',12');

        $result['stylist_look'] = $query->result_array();
        $result['offset']['stylist'] = ($stylist_offset+1);

        }

}
        return $result;
    }

    function get_all_stylist(){
        $query = $this->db->get_where('stylist', array('status' => 1));
        $res = $query->result_array();
        return $res;
    }

    function get_stylist_details($stylist_name){
         $query = $this->db->get_where('stylist', array('status' => 1,'slug'=>$stylist_name));
        $res = $query->result_array();
        return $res;
    }

    function add_to_wishlist($look_id){
        $user_id = $this->session->userdata('user_id');
        if($look_id!='' && $user_id!=''){
        $data = array(
           'user_id' => $user_id ,
           'look_id' => $look_id ,
           'created_by' => $user_id,
           'created_datetime' => $this->config->item('sc_date'),
           'modified_datetime' => $this->config->item('sc_date')
        );

        $this->db->insert('look_fav', $data);
        return true;
        }
    }

    function remove_from_wishlist($look_id){
        $user_id = $this->session->userdata('user_id');
        if($look_id!='' && $user_id!=''){
            $this->db->delete('look_fav', array('look_id' => $look_id,'user_id'=>$user_id));
        return true;
        }
    }

    function get_look_fav_or_not($look_id){
        $user_id = $this->session->userdata('user_id');
        if($look_id!='' && $user_id!=''){
           $res =  $this->db->get_where('look_fav', array('look_id' => $look_id,'user_id' => $user_id));
            return count($res->result_array());
        }
    }

    function add_contact_us($data){
        if(!empty($data)){
            if($this->db->insert('user_enquiry', $data)){
                return true;
            } else{
                return false;
            }
        }
    }

    function track_logout(){
        $user_id = $this->session->userdata('user_id');
        $login_attempts_id = $this->session->userdata('login_attempts_id');

        if($user_id!='' && $login_attempts_id!=''){
            $data = array('logout_time'=>$this->config->item('sc_date'));
            $this->db->where('id', $login_attempts_id);
            $this->db->update('user_login_attempts', $data);
        }
    }

    function get_search($search){
        $where_condition = '';
        /* $where_condition = $where_condition.' and LOWER(a.`name`) LIKE LOWER("%'.urldecode($search).'%")';*/
           /* $query = $this->db->query('select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag from
            (SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0  and a.status=1 '.$where_condition.') as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 || z.look_type = 6) order by z.id desc');*/
                   $where_condition = $where_condition.'where `name` LIKE LOWER("%'.urldecode($search).'%") or `look_tags` LIKE LOWER("%'.urldecode($search).'%")';
           $query = $this->db->query('select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,LOWER(name),z.created_datetime,z.status,z.deflag ,LOWER(e.look_tags) as look_tags from
            (SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0  and a.status=1 ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 || z.look_type = 6) LEFT JOIN look_tags as e ON z.id = e.look_id   '.$where_condition.' order by z.id desc');

             $result = $query->result_array();
             return sizeof($result);
    }

    function forgot_password($Emailid)
    {
        //$query = $this->db->query("Select email,password from user_login where email='".$Emailid."'");
        //$result = $query->result_array();

		$this->db->select('email,password');
		$this->db->from('user');
		$this->db->where('email',$Emailid);
		$query = $this->db->get();
		$result = $query->result_array();
		
        if(sizeof($result)>0)
        {
            $scpassword =$this->randomPassword();
            $salt = substr(md5(uniqid(rand(), true)), 0, 10);
            $password   =$salt . substr(sha1($salt . $scpassword), 0, -10);
            $data=array('password'=>$password);
            $this->db->update('user', $data, array('email' => $result[0]['email'] ));

            return  $scpassword;
        }else
        {
            return false;
        }
    }

     function reset_password($oldpwd,$newpwd)
    {
        $userid = $this->session->userdata('user_id');
        $query = $this->db->get_where('user_login', array('id' => $userid,'status'=>1));
        $result = $query->result();
        if(!empty($result))
        {
            $salt1 = substr($result[0]->password, 0, 10);
            $db_password =  $salt1 . substr(sha1($salt1 . $oldpwd), 0, -10);


            if($db_password == $result[0]->password)
            {
                $salt = substr(md5(uniqid(rand(), true)), 0, 10);
                $getnewpassword   =$salt . substr(sha1($salt . $newpwd), 0, -10);
                $data=array('password'=>$getnewpassword);
                $this->db->update('user_login', $data, array('id' => $userid ));
                return true;
            }else
            {
                return false;
            }

        }else
        {
            return "Error";
        }

    }


   public function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
  }

    public function autosearch($search){
        $query = $this->db->query("select distinct name from tag where status = 1 and name like '".$search."%' UNION select distinct name from look where status = 1 and name like '%".$search."%'");
        $result = $query->result_array();
        $look_suggestion = '[';
        if(!empty($result)){ $i = 0;

            foreach($result as $val){
                if($i!=0){ $look_suggestion = $look_suggestion.','; }
                $look_suggestion = $look_suggestion.'{"value":"'.stripslashes($val['name']).'","data":"'.stripslashes($val['name']).'"}';

                $i++;
            }
        }
        $look_suggestion = $look_suggestion.']';
        return $look_suggestion;
    }

      public function look_hv_discount($look_id){
        if($look_id > 0){
            $query = $this->db->query('select da.id, da.discount_type_id,lp.look_id,lp.product_id from discount_availed as da,look_products as lp,product_desc as pd where NOW() >= da.start_date AND NOW() <= da.end_date AND lp.product_id = pd.id  AND da.status=1 and (da.product_id = lp.product_id) and lp.look_id = "'.$look_id.'"');
            $res = $query->result_array();

            if(!empty($res)){
                return 1;
            }else{

            $query = $this->db->query('select da.id, da.discount_type_id,lp.look_id,lp.product_id from discount_availed as da,look_products as lp,product_desc as pd where NOW() >= da.start_date AND NOW() <= da.end_date AND lp.product_id = pd.id  AND da.status=1 and (da.brand_id =pd.brand_id) and lp.look_id = "'.$look_id.'"');
            $res = $query->result_array();
                if(!empty($res)){
                    return 1;
                }else{
                    return 0;
                }

            }


        }
    }
	  
	public function get_all_sliders(){
		$time = new DateTime(date('Y-m-d H:i:s'));
        // $time->add(new DateInterval('PT330M'));
        $dateM = $time->format('Y-m-d H:i:s');
		// working query
		// $query = $this->db->query("select a.* from sc_home_slider as a where a.start_date <= '".date('Y-m-d h:i:s')."' AND a.end_date >= '".date('Y-m-d h:i:s')."' order by a.banner_position ASC");
		// new query
		$query = $this->db->query("select a.* from sc_home_slider as a where a.start_date <= '".$dateM."' AND a.end_date >= '".$dateM."' AND is_active = 1 OR a.banner_type = 0 order by a.banner_position,a.start_date ASC");
		// if($_GET['test'] == 1){ echo $this->db->last_query();exit;}
		$res = $query->result_array();
		// echo "<pre>";print_r($res);exit;
		$slider_array = array();
		$last_banner_position = 0;
		$new_banner_position = 1;
		$i = 0;
		if(!empty($res)){
			foreach($res as $row){
				if($last_banner_position != $row['banner_position']){
				$slider_array[$i]['id'] = $row['id'];
				$slider_array[$i]['caption_line_1'] = $row['caption_line_1'];
				$slider_array[$i]['caption_line_2'] = $row['caption_line_2'];
				$slider_array[$i]['lable_link'] = $row['lable_link'];
				$slider_array[$i]['slide_url'] = $row['slide_url'];
				$slider_array[$i]['is_active'] = $row['is_active'];
				$slider_array[$i]['start_date'] = $row['start_date'];
				$slider_array[$i]['end_date'] = $row['end_date'];
				$slider_array[$i]['banner_url'] = $row['banner_url'];
				$slider_array[$i]['banner_position'] = $row['banner_position'];
				$slider_array[$i]['banner_type'] = $row['banner_type'];
				$slider_array[$i]['is_deleted'] = $row['is_deleted'];
				$last_banner_position = $row['banner_position']; 
				$i++;
				// echo "<pre>";print_r($slider_array);exit;
				}else{
				$i = $i-1; 
				$slider_array[$i]['id'] = $row['id'];
				$slider_array[$i]['caption_line_1'] = $row['caption_line_1'];
				$slider_array[$i]['caption_line_2'] = $row['caption_line_2'];
				$slider_array[$i]['lable_link'] = $row['lable_link'];
				$slider_array[$i]['slide_url'] = $row['slide_url'];
				$slider_array[$i]['is_active'] = $row['is_active'];
				$slider_array[$i]['start_date'] = $row['start_date'];
				$slider_array[$i]['end_date'] = $row['end_date'];
				$slider_array[$i]['banner_url'] = $row['banner_url'];
				$slider_array[$i]['banner_position'] = $row['banner_position'];
				$slider_array[$i]['banner_type'] = $row['banner_type'];
				$slider_array[$i]['is_deleted'] = $row['is_deleted'];
				$last_banner_position = $row['banner_position']; 
				// echo "<pre>";print_r($slider_array);exit;
				$i++;
				} 
				
			}
		}
		// if($_GET['test'] == 1){ echo "<pre>";print_r($slider_array);exit;}
		return $slider_array;
	}

    public function update_couponcode($email,$couponcode)
    {
        $query = $this->db->query('select * from `coupon_settings` where coupon_code = "'.$couponcode.'" ');
        $res = $query->result_array();
        $email_applied = '';        
        
        if(@$res[0]['coupon_email_applied']!='')
        {
            $email_applied = $res[0]['coupon_email_applied'].','.$email;
            $result = $this->db->query('update `coupon_settings` SET coupon_email_applied = "'.$email_applied.'" WHERE coupon_code = "'.$couponcode.'" ');
            return 1;
        }else
        {
            return 2;
        }
    }

    public function get_nonscdomain($email)
    {
        $allowed = array('stylecracker.com');

        $explodedEmail = explode('@', $email);
        $domain = array_pop($explodedEmail);
        $domain1 = strtolower($domain);
        if (! in_array($domain1, $allowed))
        {
            return true;
        }else
        {
            return false;
        }       
    }
	
	function get_brand_info(){
		
		$query =$this->db->query("select a.company_name,b.email,a.contract_end_date from brand_info as a,user_login as b where a.contract_end_date = (CURRENT_DATE + INTERVAL 10 DAY) AND a.user_id = b.id ");
		$res = $query->result_array();
		return $res;
	}
	
	function get_home_page_products($key,$data){
		$query = $this->db->query('select object_meta_value from object_meta where object_meta_key = "'.$key.'" ');
		$result = $query->result_array();
		
		$data['user_id'] = $this->session->userdata('user_id');
		if($key == 'home_page_products'){
			$product_ids = explode(',',@$result[0]['object_meta_value']);
			$home_page_products = $this->Mobile_look_model->get_products_list($product_ids,$data);
			return $home_page_products;
		}else if($key == 'home_page_collections'){
			$data['table_search'] = @$result[0]['object_meta_value'];
			$data['search_by'] = '1';
			$home_page_collections = $this->Collection_model->get_all_collection($data);
			return $home_page_collections;
		}else{
			return array();
		}
		
		
	}

    function getpincodedata($pincode)
    {
        if($pincode!='')
        {
            $query = $this->db->query(' select a.id,a.state_name,b.city from states as a,city as b,pincode as c where a.id = b.state_id AND b.id = c.city_id AND c.pincode = "'.$pincode.'" ');
            $result = $query->result_array();
            return $result;
        }
        
    }

    function get_array_keyvalue($array,$userkey,$uservalue)
    {
        foreach($array as $key => $value) {
            if($value->$userkey==$uservalue )
            {
                return $key;
            }
        }
    }

      function scbox_get_orderproduct_info($order_id){
        $result = array();  
        $order_array = explode('_',$order_id);         
        if(@$order_array[0]==$this->session->userdata('user_id'))
        {
           $query = $this->db->query('select id,user_id,product_id,order_id from `order_product_info` where order_id = "'.$order_array[1].'"');
           
           $result = $query->result_array();
            return $result;
        }else
        { 
          return $result;
        }
    }

 }
?>
