<?php
class Brandusers_model extends CI_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->model('category_model');
    }
    
    
    
    function get_allusers($search_by,$table_search,$page,$per_page){ 
		if($search_by == '1'){ $search_cond = " AND a.user_name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '2'){ $search_cond = " AND a.email like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '3'){ $search_cond = " AND a.created_datetime like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '4'){ $search_cond = " AND a.id =".$table_search; }
		else{ $search_cond =''; }
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}
		if($this->session->userdata('role_id') == 6 ){ 
		 $query = $this->db->query("select a.id,a.user_name, a.bucket, a.created_datetime, a.email from user_login as a, user_role_mapping as b,brand_info as c WHERE a.`status` =1 and a.`id` = b.`user_id` and b.`role_id`=6 and a.id=".$this->session->userdata('user_id')." and a.`id`= c.`user_id` AND c.is_form_completed = 1 ".$search_cond." order by a.id desc ".$limit_cond);
		}else{	
		$query = $this->db->query("select a.id,a.user_name,c.`company_name`, a.bucket, a.created_datetime, a.email from user_login as a, user_role_mapping as b, brand_info as c WHERE a.`status` =1 and a.`id` = b.`user_id` and a.`id`= c.`user_id` and b.`role_id`=6 AND c.is_form_completed = 1 ".$search_cond." order by a.id desc ".$limit_cond);
		}
		$res = $query->result();
		//echo $this->db->last_query();
		return $res;
	}
	
	function get_user_data($brand_id){
		$res = $this->db->get_where('brand_info', array('user_id' => $brand_id));
		return $res->result();
		}
	
	function target_market(){
		$res = $this->db->get_where('target_market', array('status' => 1));
		return $res->result();
		}	
		
	function target_market_by_brand($brand_id){
		$res = $this->db->query('select target_market_id from brand_target_market_assoc where brand_id='.$brand_id." and status = 1");
		return $res->result_array();
		}	
		
	function update_brand_data($data){ 
		$res= $this->db->get_where('brand_info',array('user_id'=>$data['brand_info']['user_id']));
		$res = $res->result(); 
			if(count($res)>0){
				$this->db->where('user_id', $data['brand_info']['user_id']);
				$result = $this->db->update('brand_info', $data['brand_info']);

				if($this->db->affected_rows())
				{
					$this->db->where('user_id', $data['brand_info']['user_id']);
					$result = $this->db->update('brand_info', array('approve_reject' => 'P'));
				}

			}else{
					$this->db->insert('brand_info', $data['brand_info']);
			} 
			
			$this->db->delete('brand_target_market_assoc', array('brand_id' => $data['brand_info']['user_id'])); 
			if(!empty($data['target_market'])){
			foreach($data['target_market'] as $val){
					$target_market[] = array('brand_id'=>$data['brand_info']['user_id'],'target_market_id'=>$val,'status'=>1, 'created_by'=>$this->session->userdata('user_id'),'modified_by'=>$this->session->userdata('user_id'),'created_datetime'=>date('Y-m-d H:i:s'));
				}
			}

		  
		 	$this->db->insert_batch('brand_target_market_assoc', $target_market); 

		 	$res1= $this->db->get_where('brand_prod_sub_cat_assoc',array('brand_id'=>$data['brand_cat_info']['brand_id']));
			$res1 = $res1->result(); 
			if(count($res1)>0){
				$this->db->delete('brand_prod_sub_cat_assoc', array('brand_id'=>$data['brand_cat_info']['brand_id']));
		 	 	$this->db->insert('brand_prod_sub_cat_assoc', $data['brand_cat_info']);
			}else
			{
				$this->db->insert('brand_prod_sub_cat_assoc', $data['brand_cat_info']);
			}
		  return true;
			
		}
		
		
		
	function update_brand_contact_data($data){
		$res= $this->db->get_where('brand_contact_info',array('brand_id'=>$data['contact_info']['brand_id']));
		//echo $this->db->last_query();
		$res = $res->result(); 
			if(count($res)>0){
				$this->db->where('brand_id', $data['contact_info']['brand_id']);
				$resultBrand = $this->db->update('brand_contact_info', $data['contact_info']);

				if($resultBrand)
				{
					$this->db->where('user_id',$data['contact_info']['brand_id']);
			    	$this->db->update('brand_info', array('approve_reject' => 'P'));
				}
				
				
			}else{
				$this->db->insert('brand_contact_info', $data['contact_info']);
			}
		
		}

	function get_user_contact_data($brand_id){
		$res = $this->db->get_where('brand_contact_info', array('brand_id' => $brand_id));
			return $res->result();
		}

	function approveReject_brand_m($apr_status,$brandID,$reason)
	{
		
		if($apr_status == 'approve')
		{
			$this->db->where('id', $brandID);
			return $this->db->update('brand_info', array('approve_reject' => 'A'));
			

		}else
		{
			if($apr_status == 'reject')
			{
				$this->db->where('id', $brandID);
				return $this->db->update('brand_info', array('approve_reject' => 'R','reject_reason'=>$reason));
				
			}else
			{
				return false;
			}
			
		}
	}

	function get_var_type($subcat_type){

		if($subcat_type!=''){
		$query = $this->db->query('SELECT id, name FROM `product_variation_type`  where  `prod_sub_cat_id`='.$subcat_type." order by id desc");
		$result = $query->result_array();
		return $result;
		}else
		{
			return "";
		}
	}
	

	function get_brand_cat()
	{
		$brand_id =  $this->uri->segment(3);
		$res = $this->db->query("select brand_id,prod_cat_id, prod_sub_cat_id,	var_type_id from brand_prod_sub_cat_assoc where brand_id='".$brand_id."' and 	is_active = 1;");
		return $res->result_array();

	}

	/* Added by pratik for store settings*/

	function user_menu($user_id)
	{		 
	  
		/*$res=$this->db->query("select `is_paid`,`show_in brand_list`,`onboarded_date`,`cpc_percentage`,`cpa_percentage`,`is_shipping`,`shipping_charges`,`shipping_min_values`,`shipping_max_values`,`is_cod`,`cod_min_value`,`code_max_value`,`shipping_exc_location`,`cod_exc_location`,`registered_address`,`registration_number`,`contact_email_address`,`vat_tin_number`,`store_tax`,`cst_number`,`query_address`,`query_contact_no`,`query_email_id`,`sign_img`,`cod_charges`,`brand_code`,`min_del_days`,`max_del_days` from brand_info where user_id='".$user_id."';");*/

		//$res=$this->db->query("select `is_paid`,`show_in brand_list`,`onboarded_date`,`cpc_percentage`,`cpa_percentage`,`is_shipping`,`shipping_charges`,`shipping_min_values`,`shipping_max_values`,`is_cod`,`cod_min_value`,`code_max_value`,`shipping_exc_location`,`cod_exc_location`,`registered_address`,`registration_number`,`contact_email_address`,`vat_tin_number`,`store_tax`,`cst_number`,`query_address`,`query_contact_no`,`query_email_id`,`sign_img`,`cod_charges`,`brand_code`,`min_del_days`,`max_del_days`,`cod_available`,`return_policy`, `payment_gateway_charges`, `service_tax`, `integration_charges`,`commission_cat`,contract_end_date from brand_info where user_id='".$user_id."';");
		$res=$this->db->query("select `contract_name`,`user_id`,`is_paid`,`logo`,`show_in brand_list`,`onboarded_date`,`cpc_percentage`,`cpa_percentage`,`is_shipping`,`shipping_charges`,`shipping_min_values`,`shipping_max_values`,`is_cod`,`cod_min_value`,`code_max_value`,`shipping_exc_location`,`cod_exc_location`,`registered_address`,`registration_number`,`contact_email_address`,`vat_tin_number`,`store_tax`,`cst_number`,`query_address`,`query_contact_no`,`query_email_id`,`sign_img`,`cod_charges`,`brand_code`,`min_del_days`,`max_del_days`,`cod_available`,`return_policy`, `payment_gateway_charges`, `service_tax`, `integration_charges`,`commission_cat`,`is_verify`,`gst_type` from brand_info where user_id='".$user_id."';");
		
  		return $res->result_array();


	}
		function user_menu_update($ispaid,$showin,$date_from,$cpc,$cpa,$is_shipping,$Shipping_Charges,$min_sc,$max_sc,$is_cod,$min_cod,$max_cod,$shipping_location,$cod_location,$Store_register_address,$Store_Registrations_ID,$contact_email_address,$vat_tin_no,$tax,$cst_no,$Store_address_returning,$store_contact_no,$Store_Email,$sign_image,$cod_charge,$brand_code,$min_del,$max_del,$company_logo,$user_id,$cod_available,$return_policy,$payment_gateway_charges,$service_tax,$integration_charges,$commission=null,$contract_end_date=null,$gst_type=null){ 

		$query= $this->db->get_where('brand_info',array('user_id'=>$user_id));
       	
		$res = $query->result_array();

		$count_array=count($res);
		
	
      



		if($count_array > 0){
			
			$query1= $this->db->get_where('brand_info',array('brand_code'=>$brand_code,'user_id!='=>$user_id));
			$res1 = $query->result_array();
       		$count_code=count($query1);

            

       		if($count_array > 0){
       			$query =$this->db->query("UPDATE brand_info SET is_paid='".$ispaid."',`show_in brand_list`='".$showin."',onboarded_date='".$date_from."',cpc_percentage='".$cpc."',cpa_percentage='".$cpa."',is_shipping='".$is_shipping."',shipping_charges='".$Shipping_Charges."',shipping_min_values='".$min_sc."',shipping_max_values='".$max_sc."',is_cod='".$is_cod."',cod_min_value='".$min_cod."',code_max_value='".$max_cod."',shipping_exc_location='".$shipping_location."',cod_exc_location='".$cod_location."',registered_address='".$Store_register_address."',registration_number='".$Store_Registrations_ID."',contact_email_address='".$contact_email_address."',vat_tin_number='".$vat_tin_no."',cod_charges='".$cod_charge ."',store_tax='".$tax."',cst_number='".$cst_no."',query_address='".$Store_address_returning."',brand_code='".$brand_code."',min_del_days='".$min_del."',max_del_days='".$max_del."',query_contact_no='".$store_contact_no."',query_email_id='".$Store_Email."',sign_img='".$company_logo."',`cod_available` = '".$cod_available."' , `return_policy` = '".$return_policy."',
					`payment_gateway_charges` = '$payment_gateway_charges',
					`service_tax` = '$service_tax',
					`integration_charges` = '$integration_charges',`commission_cat` = '".$commission."',`contract_end_date` = '".$contract_end_date."',
					`gst_type` = '$gst_type'
					WHERE 
					`user_id`='".$user_id."' ");       			
			}else {
				return false;
			}

				
           
				
				
			}else{

			$query1= $this->db->get_where('brand_info',array('brand_code'=>$brand_code,'user_id!='=>$user_id));
			$res1 = $query->result_array();
       		$count_code=count($query1);

       		if($count_array == 0){

				$query =$this->db->insert("INSERT INTO  brand_info (is_paid,`show_in brand_list`,onboarded_date,cpc_percentage,cpa_percentage,is_shipping,shipping_charges,shipping_min_values,shipping_max_values,is_cod_charges,cod_min_value,code_max_value,shipping_exc_location,cod_exc_location,registered_address,registration_number,contact_email_address,vat_tin_number,store_tax,cst_number,query_address,query_contact_no,sign_img,brand_code,min_del_days,max_del_days,cod_available,return_policy,payment_gateway_charges,service_tax,integration_charges,commission_cat,contract_end_date)VALUES ('".$ispaid."','".$showin."','".$date_from."','".$cpc."','".$cpa."','".$is_shipping."','".$Shipping_Charges."','".$min_sc."','".$max_sc."','".$is_cod."','".$min_cod."','".$max_cod."','".$shipping_location."','".$cod_location."','".$Store_register_address."','".$Store_Registrations_ID."','".$contact_email_address."','".$vat_tin_no."','".$cod_charge."','".$cst_no."','".$Store_address_returning."','".$store_contact_no."','".$Store_Email."','".$company_logo."','".$brand_code."','".$min_del."','".$max_del."','".$cod_available."','".$return_policy."' ,'$payment_gateway_charges','$service_tax','$integration_charges,'".$commission."','".$contract_end_date."')");
				$res = $query->result_array();
				return $res;

				}else {

                          return false;
				}
			}
			return true;

}
		function get_brands_image($brand_id){
			$query =$this->db->query("SELECT user_id,logo,cover_image,home_page_promotion_img,listing_page_img from brand_info WHERE user_id = '".$brand_id."'");
			$res = $query->result_array();
			return $res;
		}

		function update_brand_img($brand_id,$field_name,$field_value){
			if($brand_id > 0 && $field_name !='' && $field_value!=''){
				$data = array($field_name =>$field_value);

			$this->db->where('user_id', $brand_id);
			$this->db->update('brand_info', $data);

			}
		}

		function get_paid_brands(){
			$query =$this->db->query("select user_id, company_name from `brand_info` where is_paid = 1");
			$res = $query->result_array();
			return $res;
		}
		
		public function contact_info($post = null,$user_id){
			$data = array();

			if(!empty($post)){
				
				$data = array(
						'company_address' => $post['company_address'],
						'city' => $post['city'],
						'state' => $post['state'],
						'pincode' => $post['pincode'],
						'country' => $post['country'],
						'contact_person' => $post['contact_person'],
						'phone_one' => $post['phone_one'],
						'emailid' => $post['emailid'],
						'contact_person_two' => $post['contact_person_two'],
						'phone_two' => $post['phone_two'],
						'alt_emailid' => $post['alt_emailid'],
						'contact_person_three' => $post['contact_person_three'],
						'phone_three' => $post['phone_three'],
						'alt_emailid_three' => $post['alt_emailid_three'],
						'warehouse_address' => $post['warehouse_address'],
						'warehouse_city' => $post['warehouse_city'],
						'warehouse_state' => $post['warehouse_state'],
						'warehouse_pincode' => $post['warehouse_pincode'],
						'warehouse_country' => $post['warehouse_country'],
					);

    		$this->db->where('brand_id', $user_id);
			$this->db->update('brand_contact_info', $data);
			
    	}


    	$this->db->from('brand_contact_info');
        $this->db->where('brand_id',$user_id);
        $query = $this->db->get();
        $data['contact_info'] = $query->result_array();
        return $data;
    }
	
	function get_child_brand($user_id){
		$query = $this->db->get_where('brand_store',array('brand_id'=>$user_id));
		$res = $query->result_array();
		// echo "<pre>";print_r($res);exit;
		return $res;
	}
	
	function save_brand_cpa_percentage($cpa_percentage,$brand_Store_id,$commission,$show_on_web){
		if($brand_Store_id > 0){
			$data = array('cpa_percentage' =>$cpa_percentage,'commission_cat' =>$commission,'show_on_web' =>$show_on_web);

			$this->db->where('id', $brand_Store_id);
			$this->db->update('brand_store', $data);
		}
	}
	
	/*function update_brand_data_new($ispaid,$date_from,$brand_code,$cpa,$showin,$commission,$payment_gateway_charges,$service_tax,$integration_charges,$user_id){		
		$query =$this->db->query("UPDATE brand_info SET is_paid='".$ispaid."',`show_in brand_list`='".$showin."',onboarded_date='".$date_from."',brand_code='".$brand_code."',`payment_gateway_charges` = '".$payment_gateway_charges."',`service_tax` = '".$service_tax."',`integration_charges` = '".$integration_charges."' WHERE `user_id`='".$user_id."' ");		
		return true;
	}*/


	function update_brand_data_new($ispaid,$date_from,$brand_code,$cpa,$showin,$commission,$payment_gateway_charges,$service_tax,$integration_charges,$user_id){		
		$query =$this->db->query("UPDATE brand_info SET is_paid='".$ispaid."',`show_in brand_list`='".$showin."',onboarded_date='".$date_from."',cpa_percentage='".$cpa."',brand_code='".$brand_code."',
					`payment_gateway_charges` = '".$payment_gateway_charges."',
					`service_tax` = '".$service_tax."',
					`integration_charges` = '".$integration_charges."',`commission_cat` = '".$commission."' 
					WHERE 
					`user_id`='".$user_id."' ");
		// echo $this->db->last_query();exit;
		return true;
	}
	function get_user_email($user_id){
		$query =$this->db->query("select a.email from user_login as a where a.id = $user_id ");
		$res = $query->result_array();
		return $res[0]['email'];
	}

	function brand_logo_contract($company_logo,$contract_name,$user_id,$is_verify){
		if($company_logo != '' && $contract_name != '' ){
			$query =$this->db->query("UPDATE brand_info SET logo='".$company_logo."', contract_name='".$contract_name."', is_verify='".$is_verify."' WHERE user_id='".$user_id."'");		
			//echo $this->db->last_query(); exit;
			return true;
		}elseif($company_logo != ''){
			$query =$this->db->query("UPDATE brand_info SET logo='".$company_logo."', is_verify='".$is_verify."' WHERE user_id='".$user_id."'");		
			return true;
		}else{
			$query =$this->db->query("UPDATE brand_info SET contract_name='".$contract_name."', is_verify='".$is_verify."' WHERE user_id='".$user_id."'");		
			return true;
		}
	}
} 
?>
