<?php
class Mobile_cart_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function update_order_data($order_id,$pa_data,$user_id)
	{
		$arrData = array();
		$this->db->select('id');
		$this->db->from('order_meta');
		$this->db->where('scx_order_id',$order_id);
		$this->db->where('meta_key','pa_data');
		$query = $this->db->get();
		
		if($query->num_rows()>0)
		{ 
			$this->db->where('scx_order_id',$order_id);
			$this->db->where('meta_key','pa_data');
			$this->db->where('created_by',$user_id);
			$this->db->update('order_meta', array('meta_value'=>$pa_data));				
			//echo '<pre>';print_r($query);exit;			
			return 'true';
		}else
		{
			$arrData = array('meta_key'=>'pa_data','meta_value'=>$pa_data,'created_by'=>$user_id,'created_datetime'=>date('Y-m-d H:i:s'),'scx_order_id'=>$order_id);
			$this->db->insert('order_meta', $arrData);
			$metaId = $this->db->insert_id();
			//echo $this->db->last_query();exit;
			return $metaId;
		}
	}
  
}


?>