<?php
class Userregister_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
   public function register($arrData = array())
   {
	   $this->db->insert('user', $arrData);
	   $userId = $this->db->insert_id();
	   return $userId;
   }
   
   
   
   function registerUser($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$sc_gender,$json_array){    
        $insert 		= 0;
        $picture 		= '';
        $facebook_id 	= '';
        $google_id 		= '';
        $event_code 	= '';
        $birthday 		= @$json_array['birthday'];	
        $scmobile 		= @$json_array['scmobile'];
        $fbbirthday 	= @$json_array['birthday'];		

       
		$json_array['firstname'] 	= ucfirst(strtolower($json_array['firstname']));
		$json_array['lastname'] 	= ucfirst(strtolower(@$json_array['lastname']));
		
		$medium_array = array("facebook", "google", "event", "mobile_ios_facebook", "mobile_android_facebook", "mobile_android_google", "mobile_ios_google");
		
        
          if ($this->agent->is_browser())
          {
              $agent = 'Computer '.$this->agent->browser().' '.$this->agent->version();
          }
          elseif ($this->agent->is_robot())
          {
              $agent = 'Robot '.$this->agent->robot();
          }
          elseif ($this->agent->is_mobile())
          {
              $agent = 'Mobile '.$this->agent->mobile();
          }
          else
          {
              $agent = 'Unidentified';
          }

          $platform_info = $this->agent->platform();

        if(isset($medium) && (in_array($medium, $medium_array))){
            if($medium === 'google'){
                $picture = $json_array['picture'];
                $google_id = $json_array['id'];
            }
            elseif($medium === 'facebook') 
			{
                $facebook_id = $json_array['id'];
            }
			elseif($medium === 'event')/* Added by Sudha For Event*/
            {
                $event_code = $json_array['event_code'];
                $medium 	='website';
            }            
        }

        if($this->emailUnique($scemail_id) != NULL){ 
				 $insert = 1;
         }
            
        if($insert===0){
        /***Code Ends***/
            $salt = substr(md5(uniqid(rand(), true)), 0, 10);
            $password   =$salt . substr(sha1($salt . $scpassword), 0, -10);
            $data = array(			   
			   'first_name' 		=> $json_array['firstname'] ,
			   'last_name' 			=> $json_array['lastname'] ,
			   'username' 			=> $scusername,
			   'email' 				=> $scemail_id,
			   'password' 			=> $password,
			   'auth_token'			=> substr(md5(uniqid(rand(1,6))), 0, 15),			  
			   'status' 			=> 1,
			   'created_datetime' 	=> date('Y-m-d H:i:s'),
			   //'modified_datetime' 	=> $this->config->item('sc_date'),
			   //'event_code' 		=> $event_code,
			   'created_by' 		=>0,
			   'modified_by' 		=> 0
            );           
            $this->db->insert('user', $data);
            $Lst_ins_id = $this->db->insert_id();
			
            if($Lst_ins_id){
                $userMeta = array(
					'facebook_id'		=> $facebook_id,
					'google_id'			=> $google_id,
					'profile_pic'		=> $picture,  
					'registered_from'	=> $medium,
					'gender' 			=> $sc_gender,
					'platform_name'		=> $platform_info,
					'contact_no'		=> $scmobile,
					'birth_date'		=> $birthday,
					'auth_token'		=> substr(md5(uniqid(rand(1,6))), 0, 15),
					'ip_address' 		=> $ip,
			    );
				
				foreach($userMeta as $keyValue => $keyData)
				{
					$keyMeta = array();
					$keyMeta['meta_key']			=	$keyValue;	
					$keyMeta['meta_value']			=	$keyData;	
					$keyMeta['created_datetime']	=	$this->config->item('sc_date');
					$keyMeta['scx_user_id']			= 	$Lst_ins_id;					
					$this->db->insert('user_meta', $keyMeta);
				}

                $data = array(
					'user_id'		=> $Lst_ins_id,
					'module_id'		=> $role,
					'type'			=> 'user_role',
					'created_by'	=> $Lst_ins_id,
					//'modified_by'	=> $Lst_ins_id
				);
                
                $this->db->insert('role_mappings', $data);
				/* echo $this->db->last_query();
				exit(); */
				/*$data = array(
					'user_id'			=> $Lst_ins_id,
					'question_id'		=> $json_array['question_id'],
					'answer_id'			=> $json_array['age_range'],
					'created_by'		=> $Lst_ins_id,
					'modified_by'		=> $Lst_ins_id,
					'created_datetime' 	=> $this->config->item('sc_date'),
					'modified_datetime' => $this->config->item('sc_date'),
					'is_active' 		=> 1
					);

                $this->db->insert('users_answer', $data);*/
                $res = $this->login_user($scemail_id,$scpassword,$medium,'');

                if(strlen($facebook_id)>1 || strlen($google_id)>1)
                    return "pa";
				
				if($medium ==='facebook' || $medium === 'google' || $medium === 'website'){
					return true;
				}else{
					return $res;
				}
            }
        }else
		{ 
            $query = $this->db->query("select profile_pic from user_meta where user_id = ".$result[0]['id']." and ");
            $res = $query->result();
			
            if(empty($res[0]->profile_pic))
			{
                $data = array(
					'facebook_id' 		=> $facebook_id,
					'google_id' 		=> $google_id,
					'profile_pic'		=> $picture,
					'registered_from'	=> $medium,
					'modified_by' 		=> $result[0]['id'],
					'modified_datetime' => $this->config->item('sc_date'),
					'gender_c'			=> '1',
					'birth_date'		=> $birthday
					//'auth_token'		=> substr(md5(uniqid(rand(1,6))), 0, 15);
				);
            }else{
				$data = array(
					'facebook_id'		=> $facebook_id,
					'google_id'			=> $google_id,
					'registered_from'	=> $medium,
					'modified_by' 		=> $result[0]['id'],
					'modified_datetime' => $this->config->item('sc_date'),
					'gender_c'			=> '1',
					'birth_date'		=> $birthday
				);
            }
            $this->db->update('user_meta', $data, array('user_id' => $result[0]['id'] ));
            $query = $this->db->query("select user_role_mapping.role_id,user_info.user_id,user_login.username,user_login.email,user_info.created_datetime,user_info.first_name,user_info.last_name,user_login.bucket,user_login.question_comp,user_login.question_resume_id,user_info.gender,user_info.gender_c,user_info.contact_no from user_info,user_role_mapping,user_login where user_info.user_id = ".$result[0]['id']." and user_info.user_id = user_role_mapping.user_id and user_info.user_id = user_login.id");
            $res = $query->result();

            if(!empty($res)){
				if($medium != 'website'){
					$data = array('auth_token' => substr(md5(uniqid(rand(1,6))), 0, 15));
					$this->db->update('user', $data, array('id' =>$res[0]->user_id));//update token
				}
				$data_attempts = array('user_id'=>$res[0]->user_id,'login_time'=>$this->config->item('sc_date'));
				$this->db->insert('user_login_attempts', $data_attempts);
				$login_attempts_id = $this->db->insert_id();

				$array_field=array(
					'username' 				=> $res[0]->username,
					'user_id' 				=> $res[0]->user_id,
					'role_id'				=> $res[0]->role_id,
					'first_name'			=> $res[0]->first_name,
					'last_name'				=> $res[0]->last_name,
					'bucket'				=> $res[0]->bucket,
					'question_comp'			=> $res[0]->question_comp,
					'question_resume_id'	=> $res[0]->question_resume_id,
					'login_attempts_id'		=> $login_attempts_id,
					'email' 				=> $res[0]->email,
					'gender' 				=> $res[0]->gender,					
					'contact_no' 			=> $res[0]->contact_no,
					'utm_source1'			=> $this->session->userdata('utm_source1'));

				$query_res = $this->db->get_where('user_address', array('user_id' => $res[0]->user_id,'is_default'=>1));
				$res_res = $query_res->result_array();
				if(!empty($res_res)){ $array_field['pincode'] = $res_res[0]['pincode']; 
				setcookie('stylecracker_shipping_pincode', $res_res[0]['pincode'], time() + ((86400 * 2) * 30), "/", "",  0);
				}

				$this->session->set_userdata($array_field);

				if($medium ==='facebook' || $medium === 'google' || $medium === 'website'){
					if($res[0]->bucket == 0)
					return 'pa';
					else
					return 'profile';
				}else{
					return $array_field;
				}
				
            }else{
                return 2;
            }

        }
    }
	
	function emailUnique($emaild){
		$username_unique = 0;
        if($emaild!='')
        {
	        $query = $this->db->query('select email from scx_user where email ="'.$emaild.'"');
		    $username_unique = $query->result_array();		      
        }       
        return $username_unique;	   	
         /*$this->db->select('id');
		 $this->db->from('user');
		 $this->db->where('email',$emaild);
		 $query = $this->db->get();
		
		//echo '<pre>';print_r($query->num_rows());exit;
		 if($query->num_rows()>0)
		 {
		 	 
			 return true;
		 }else
		 {
		 	return false;	   
		 }*/
		 
    }   
	
	
	function login_user($email,$password,$medium,$type){
        
        $query = $this->db->get_where('user', array('email' => $email,'status'=>1));
        $result = $query->result();
        if(!empty($result)){
        $salt = substr($result[0]->password, 0, 10);

        $db_password =  $salt . substr(sha1($salt . $password), 0, -10);

        if($db_password == $result[0]->password || $type == 'facebook' || $type == 'google'){
           //$query = $this->db->query("select role_mappings.module_id,user_meta.scx_user_id,user_meta.created_datetime,user_meta.first_name,user_meta.last_name,user.email,user.bucket,user.question_comp,user.question_resume_id,user.user_name,user_meta.gender,user_meta.gender_c,user_meta.contact_no from scx_user_meta as user_meta,scx_role_mappings as role_mappings,scx_user as user  where user_meta.scx_user_id = ".$result[0]->id." and user_meta.scx_user_id = role_mappings.created_by and user_meta.scx_user_id = user.id");
		   
		   $query = $this->db->query("select role_mappings.module_id,user_meta.scx_user_id,user_meta.created_datetime,user.email,user.first_name,user.last_name,user.username from scx_user_meta as user_meta,scx_role_mappings as role_mappings,scx_user as user 
		   where user_meta.scx_user_id = ".$result[0]->id." and user_meta.scx_user_id = role_mappings.created_by and user_meta.scx_user_id = user.id");
		   
            $res = $query->result();
			
            if(!empty($res)){
				/*if($medium != 'website'){
					$data = array(
								'meta_key' 			=> 'auth_token',
								'meta_value' 		=> substr(md5(uniqid(rand(1,6))), 0, 15),
								'scx_user_id' 		=> $res[0]->scx_user_id,
								'modified_datetime' => date('Y-m-d H:i:s'),
							);
					$this->db->update('user_meta', $data, array('id' =>$res[0]->scx_user_id));//update token
					//$this->db->update('user_meta', array('id' =>$res[0]->scx_user_id));//update token
				}*/
                /*$data_attempts = array(
						'scx_user_id'		=>$res[0]->scx_user_id,
						'login_time'		=>$this->config->item('sc_date')
						);
                $this->db->insert('user_login_attempts', $data_attempts);
                $login_attempts_id = $this->db->insert_id();*/

                $array_field=array(
					'username' 			=> $res[0]->username,
					'user_id' 		=> $res[0]->scx_user_id,
					//'role_id'			=> $res[0]->role_id,
					'first_name'		=> $res[0]->first_name,
					'last_name'			=> $res[0]->last_name,					
					//'login_attempts_id'	=> $login_attempts_id,
					'email' 			=> $res[0]->email,
					//'gender' 			=> $res[0]->gender,					
					//'contact_no' 		=> $res[0]->contact_no,
					//'utm_source1'		=> $this->session->userdata('utm_source1')
					);
					/* echo "<pre>";
					print_r($array_field);
					echo "<pre>";
					exit(); */

                $query_res = $this->db->get_where('scx_user_address', array('scx_user_id' => $res[0]->scx_user_id,'is_default'=>1));
                $res_res = $query_res->result_array();
                if(!empty($res_res)){ $array_field['pincode'] = $res_res[0]['pincode']; 
                setcookie('stylecracker_shipping_pincode', $res_res[0]['pincode'], time() + ((86400 * 2) * 30), "/", "",  0);
                }

                $this->session->set_userdata($array_field);
				if($medium ==='facebook' || $medium === 'google' || $medium === 'website'){
					return 1;
				}else return $array_field;
            }else{
                return 2;
            }
        }else{
                return 2;
            }
        }else{ return 2; }
    }
	

	
	function get_users_info_mobile($user_id){
		
		//$query = $this->db->query("select a.user_id,a.created_datetime,a.first_name,b.email,a.last_name,b.bucket,b.auth_token,b.question_comp,b.question_resume_id,a.contact_no,a.registered_from,a.facebook_id,a.google_id,IF(a.profile_pic='', IF(a.gender = 1,'".WOMEN_DEFAULT_IMG."','".MEN_DEFAULT_IMG."'), CONCAT('".PROFILE_IMG_URL."',a.profile_pic)) as profile_pic , IF(a.profile_cover_pic IS NULL, 'http://www.stylecracker.com/assets/images/profile/cover.jpg', CONCAT('".COVER_IMG_URL."',a.profile_cover_pic)) as profile_cover_pic,IF(a.gender=1 ,'women', 'men') as gender,(select c.answer_id from users_answer as c WHERE b.id = c.user_id AND (c.question_id = 8 OR c.question_id = 15) limit 0,1 ) as age_answer_id from scx_user as a,scx_user_meta as b where a.user_id = ".$user_id." and a.user_id = b.id ");
		
		//$query = $this->db->query("select a.id,a.created_datetime,a.auth_token,a.first_name,a.email,a.last_name,a.registered_from,a.facebook_id,a.google_id,IF(a.profile_pic='', IF(a.gender = 1,'".WOMEN_DEFAULT_IMG."','".MEN_DEFAULT_IMG."'), CONCAT('".PROFILE_IMG_URL."',a.profile_pic)) as profile_pic , IF(a.profile_cover_pic IS NULL, 'http://www.stylecracker.com/assets/images/profile/cover.jpg', CONCAT('".COVER_IMG_URL."',a.profile_cover_pic)) as profile_cover_pic,IF(a.gender=1 ,'women', 'men') as gender from scx_user as a,scx_user_meta as b where a.id = ".$user_id." and a.user_id = b.id ");
		
		
		
		$this->db->select('u.id,u.username,u.first_name,u.last_name,u.email,u.auth_token,u.created_datetime');
		$this->db->from('user as u');
		//$this->db->join('user_meta as um', 'u.id=um.scx_user_id', 'inner');
		$this->db->where('u.id',$user_id);
		$query = $this->db->get();
		/* echo $this->db->last_query();
		exit(); */
		if($query->num_rows()>0)
		{
			$res = $query->row_array();
			return $res;
		}
		return false;
		
		/* $res = $query->result();
		return $res[0]; */
    }
	
	public function usermeta($user_id)
	{
		$this->db->select('um.meta_key,um.meta_value');
		$this->db->from('user_meta as um');
		$this->db->where('um.scx_user_id', $user_id);
		$query = $this->db->get();
		/* echo $this->db->last_query();
		exit(); */
		if($query->num_rows()>0)
		{
			$resall = $query->result_array();
			return $resall;
		}
		return false;
	}

	public function usermetabyKey($user_id,$key)
	{
		$this->db->select('um.meta_key,um.meta_value');
		$this->db->from('user_meta as um');
		$this->db->where('um.scx_user_id', $user_id);
		$this->db->where('um.meta_key', $key);
		$query = $this->db->get();
		/* echo $this->db->last_query();
		exit(); */
		if($query->num_rows()>0)
		{
			$resall = $query->result_array();			
			return $resall[0]['meta_value'];
		}
		return false;
	}
	
	function get_user_gender($user_id){		
		$this->db->select('a.meta_value');
		$this->db->from('user_meta as a');
		$this->db->where('a.scx_user_id',$user_id);
		$this->db->where('a.meta_key','gender');
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$res = $query->result_array();
			return $res[0]['meta_value'];
		}
		return false;
    }

    function scgenrate_otp($user_data){

		$time = new DateTime(date('Y-m-d H:i:s'));
		$dateM = $time->format('Y-m-d H:i:s');
		$otp_no = rand(4,100000);
		$digits = 4;
		$otp_no =rand(pow(10, $digits-1), pow(10, $digits)-1);
		// $otp_no = 9999;
		
		$query = $this->db->query('select otp_no from scx_user_mobile_otp where mobile_no = '.$user_data['mobile_no'].' AND type = "'.$user_data['type'].'" AND created_datetime between now() -INTERVAL 30 MINUTE AND now() + INTERVAL 30 MINUTE ');
		$res = $query->result_array();
		if(empty($res)){ 
			$data = array('mobile_no'=>$user_data['mobile_no'],'otp_no'=>$otp_no,'type'=>$user_data['type'],'created_datetime'=>$dateM);
			$this->db->insert('user_mobile_otp', $data);
			$Lst_ins_id = $this->db->insert_id();
			$query = $this->db->query("select otp_no from scx_user_mobile_otp where id = $Lst_ins_id");
			$res = $query->result_array();
			return $res[0]['otp_no'];
		}else{
			return $res[0]['otp_no'];
		}
    }
	public function get_user_mobile($id)
	{
		$this->db->select('meta_value');
		$this->db->from('user_meta');
		$this->db->where('meta_key','contact_no');
		$this->db->where('scx_user_id',$id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		return false;
		
	}
	
	public function val_otp($data = array())
	{
		$update = array(
			'status'=>'1',
		);
		$this->db->where('mobile_no', $data['mobile']);
		$this->db->where('otp_no', $data['otp']);
		$update = $this->db->update('user_mobile_otp', $update);
		if($this->db->affected_rows() > 0)
		{
		  return true;
		}
		else
		{
		  return false; 
		}
		
	}
	public function confirm_mobile_meta($arrData = array())
	{
		$this->db->insert('user_meta', $arrData);
		$mobileMeta = $this->db->insert_id();
		return $mobileMeta;
	}
 } 
