<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 */
class MY_Controller extends CI_Controller {

	public $data = array();
	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	var $seo_title = '';
	var $seo_desc = '';
	var $seo_keyword = '';
	var $megamenu = '';
	var $seo_canonical = '';
	var $seo_next = '';
	var $seo_prev = '';
	var $footer_cat = '';
	public function __construct()
	{
		parent::__construct();	
		$this->load->driver('cache');
		//$this->register_user_facebook();
		$this->seo_title = '';
		$this->seo_desc = '';
		$this->seo_keyword = '';
		$this->megamenu = '';
		$this->seo_canonical = '';
		$this->seo_next = '';
		$this->seo_prev = '';
		$this->footer_cat = '';
    	$this->register_user_google();    
		$this->get_uc_states();
		$this->get_user_address();		
	}

	public function register_user_google(){	    
	   	//$this->data['google_login_url'] = $this->googleplus->loginURL();
	    //$this->load->view('welcome_message',$contents);
	}

	public function get_uc_states(){
		$all_uc_states = array();
		//$all_uc_states=$this->cart_model->get_all_states();		
		$this->data['all_uc_states'] = $all_uc_states;
	}

	public function get_user_address(){
		$user_existing_address = array();
		//$user_existing_address= $this->cart_model->get_user_existing_address();		
		$this->data['user_existing_address'] = $user_existing_address;
	}

	function encryptOrDecrypt($mprhase, $crypt) {
	     $MASTERKEY = "STYLECRACKERAPI";
	     $td = mcrypt_module_open('tripledes', '', 'ecb', '');
	     $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
	     mcrypt_generic_init($td, $MASTERKEY, $iv);
	     if ($crypt == 'encrypt')
	     {
	         $return_value = base64_encode(mcrypt_generic($td, $mprhase));
	     }
	     else
	     {
	         $return_value = mdecrypt_generic($td, base64_decode($mprhase));
	     }
	     mcrypt_generic_deinit($td);
	     mcrypt_module_close($td);
	     return $return_value;
	}


}
