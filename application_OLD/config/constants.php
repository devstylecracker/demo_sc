<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('SMS_USERNAME','stylecracker_com');
define('SMS_PASSWORD','SC$123*');


/*Added for SCBOX */

define('SCBOX_PACKAGE',serialize(array(
'0'=>(object) ['id' => '1','name' => '3 items','desc' =>  'Apparel<br/>
				Bag <br/>
				Jewellery','price'=>'2999','productid' => '190481','packname' => 'package1','notice'=>'<p style="padding-bottom:10px;">&#8377; 1500</p>','type' =>'women','show'=>'1'],
'1'=>(object) ['id' => '2','name' => '4 items','desc' =>  'Apparel<br/>
				Bag OR Footwear<br/>
				Beauty Product<br/>
				Jewellery','price'=>'4999','productid' => '190482','packname' => 'package2','notice'=>'<p style="padding-bottom:10px;">&#8377; 2500</p>','type' =>'women','show'=>'1'],
'2'=>(object) ['id' => '3','name' => '5 items','desc' =>  'Apparel<br/>
				Bag <br/>
				Beauty Product <br/>
				Footwear <br/>
				Jewellery <br/>','price'=>'6999','productid' => '190483','packname' => 'package3','notice'=>'<p style="padding-bottom:10px;">&#8377; 3500</p>','type' =>'women','show'=>'1'],
'4'=>(object) ['id' => '1','name' => '3 items','desc' =>  'Apparel<br/>
				Bag <br/>
				Jewellery','price'=>'2999','productid' => '190481','packname' => 'package1','notice'=>'<p style="padding-bottom:10px;">&#8377; 1500</p>','type' =>'men','show'=>'1'],
'5'=>(object) ['id' => '2','name' => '4 items','desc' =>  'Apparel<br/>
				Bag OR Footwear<br/>
				Beauty Product<br/>
				Jewellery','price'=>'4999','productid' => '190482','packname' => 'package2','notice'=>'<p style="padding-bottom:10px;">&#8377; 2500</p>','type' =>'men','show'=>'1'],
'6'=>(object) ['id' => '3','name' => '5 items','desc' =>  'Apparel<br/>
				Bag <br/>
				Beauty Product <br/>
				Footwear <br/>
				Jewellery <br/>','price'=>'6999','productid' => '190483','packname' => 'package3','notice'=>'<p style="padding-bottom:10px;">&#8377; 3500</p>','type' =>'men','show'=>'1'],
'3'=>(object) ['id' => '4','name' => 'Unlimited items','desc' => 'Customized based on the type and number of products that you are looking for.<br><br><br>','price'=>'','productid' => '190484','packname' => 'package4','notice'=>'<p style="padding-bottom:1px;"></p>','type' =>'a','show'=>'1'],
'7'=>(object) ['id' => '7','name' => '4 items rakhi','desc' => 'Sling Bag<br/> Scarf<br/> Tassel<br/> Earrings <br/>Snackible Snack.','price'=>'999','productid' => '192378','packname' => 'package5','notice'=>'','show'=>'0'],
'8'=>(object) ['id' => '8','name' => '6 items rakhi','desc' => 'Necklace<br/>Pouch<br/>Tote Bag<br/>Dupatta<br/>Beauty Product<br/> Snackible Snack.','price'=>'1999','productid' => '192379','packname' => 'package6','notice'=>'','show'=>'0'],

)));

define('SCBOX_ID',"'1','2','3','4','5','6'");
