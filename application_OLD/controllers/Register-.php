<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
//require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/TT_REST_Controller.php';

class Register extends TT_REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
		
		
		$this->load->library('bitly');
		$this->load->library('email');
		$this->load->library('user_agent');
		$this->load->library('curl');
		$this->load->model('userregister_model');
		$this->load->model('schome_model');		
		$this->open_methods = array('userregister_post','login_post','scgenrate_otp_post');    
	}

   public function userregister_post(){
		
		$scusername 	= $this->post('sc_username_name');
		$scemail_id 	= $this->post('sc_email_id');
		$scpassword 	= $this->post('sc_password');
		$medium 		= $this->post('medium');
		$medium_id 		= $this->post('medium_id');
		$gender 		= $this->post('gender');
		$contact_no 	= $this->post('contact_no');
		$birth_date 	= $this->post('birth_date');
		$version 		= $this->post('version');
		$age_range 		= $this->post('age_range'); 
		
		$json_array = array();
		
		if($gender == 'male' || $gender == 'men'){ 
			$gender = 2; 
			$json_array['question_id'] = 15;
		}else if($gender == 'female' || $gender == 'women'){ 
			$gender = 1;
			$json_array['question_id'] = 8;
		}else $gender = 1;
		
		$skip = 0;
		if($medium == 'mobile_facebook' || $medium == 'mobile_ios_facebook' || $medium == 'mobile_android_facebook' || $medium == 'mobile_ios_google' || $medium == 'mobile_android_google'){ $skip  = 1;}
		
		if(trim($scemail_id) == ''){
			$this->failed_response(1006, "Please enter the Email-id");

		}else if(trim($scpassword) == ''){
			$this->failed_response(1007, "Please enter the Password");

		}else if(filter_var($scemail_id, FILTER_VALIDATE_EMAIL) === false){
			$this->failed_response(1008, "Please enter valid Email-id");

		}else if($this->emailUnique($scemail_id) > 0 && $skip == 0){
			$this->failed_response(1010, "Email ID already exists.");
		}
		else if(strlen($scpassword) < 8 && $skip == 0){
			$this->failed_response(1013, "Password should have atleast 8 character");
		}
		else if(strlen($scpassword) > 20 && $skip == 0){
			$this->failed_response(1014, "Password should not greater than 20 character");
		}
		else{

			$name 				= str_replace(' ', '', $scusername);
			$random 			= md5(uniqid(rand(0,2), true));
			$random 			= substr($random, 0, 5);
			$firstname_split 	= explode(' ',$scusername);
			$firstname 			= $firstname_split[0];
			$last_name 			= @$firstname_split[1];
			$scusername 		= $name.$random;
			
			if($this->emailUnique($scemail_id) > 0){
				$gender = $this->User_info_model->get_gender_by_email($scemail_id); 
			}else{
				$email_sent = $this->send_welcome_email($scemail_id);
			}
		
			$ip = $this->input->ip_address();
			$json_array['firstname'] 	= $firstname;
			$json_array['lastname'] 	= $last_name;
			$json_array['scmobile'] 	= $contact_no;
			$json_array['event_code'] 	= '';
			$json_array['id'] 			= $medium_id;
			$json_array['birthday'] 	= $birth_date;
			$json_array['age_range'] 	= $age_range;

			$res = $this->userregister_model->registerUser($medium,$scusername,$scemail_id,$scpassword,$ip,2,$gender,$json_array);
			if(!empty($res)) { 
				$data 				= new stdClass();
				$data->user_info	= $this->getuserinfo_get($res['scx_user_id']);
				$user_meta			= $this->userregister_model->usermeta($res['scx_user_id']);
				
				if(is_array($user_meta) && count($user_meta)>0)
				{
					foreach($user_meta as $key => $value)
					{
						$data->user_info->$value['meta_key'] = $value['meta_value'];
					} 
				}
				//$data->user_info->meta = $userMeta;
				//$gender 			= $this->Pa_model->get_gender($res['scx_user_id']);
				//$data->pa			= $this->pa_get($res['scx_user_id'],$gender);
				
				$this->success_response($data);
			}
			else{ 
				$this->failed_response(1011, "Registration failed!"); 
			}
		}					
	}
	
	public function emailUnique($email){
		$res = $this->userregister_model->emailUnique($email);
		return count($res);
	}
	public function send_welcome_email($email){

      $config['protocol'] = 'smtp';
      $config['charset'] = 'iso-8859-1';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');
	 
	  // added for confiem email
	  $email_en = base64_encode($email);
	  $user_id_en = base64_encode($this->session->userdata('user_id'));
	  // $link = base_url()."schome/confirm_email/".$email_en."";
	  // $email = 'rajesh@stylecracker.com';
	  
      $this->email->initialize($config);

      $this->email->from($this->config->item('from_email'), 'Stylecracker');
      $this->email->to($email);
      $this->email->subject('Thanks for joining StyleCracker!');
      $this->email->message('Hi there,<br><br>Welcome to StyleCracker! <br><br> StyleCracker is India\'s first online styling platform and we are here to make you the best version of youself! <br><br> When we started StyleCracker our aim was to make personalized styling available to everyone. We have a dedicated team of stylists who are online 24/7 waiting to answer ANY style query you may have. Never again will you have to worry about \'what do I wear\'- be it day-to-day or for special occasions, we will be there! <br><br> So, please explore the website and let us know how we can help. It would be great if you could also share your feedback with us so that we can help choose the best products for you. <br><br> Love, <br>Archana Walavalkar & Dhimaan Shah <br>Founders<br> <a href="'.base_url().'">www.stylecracker.com</a>');

      //$this->email->send();

  }
  
  
  public function getuserinfo_get($user_id)
	{
		if($user_id){
			$user_info = (object)$this->userregister_model->get_users_info_mobile($user_id);
			return $user_info;
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
	}
	

	public function login_post(){
		$gcm_id 		= '';
		$logEmailid 	= $this->post('sc_email_id');
		$logPassword 	= $this->post('sc_password');
		$type 			= $this->post('type');
		print_r($_POST);
		exit();
		if(trim($logEmailid) == ''){
			$this->failed_response(1006, "Please enter the Email-id");

		}else if($this->emailUnique($logEmailid) > 0){
			$result = $this->userregister_model->login_user($logEmailid,$logPassword,'mobile',$type);

			if($result != NULL && $result != 2){
					
					if(isset($result['question_comp'])) $question_comp = $result['question_comp']; else $question_comp = '';

					//$question_resume_id = $this->session->userdata('question_resume_id');
					$data = new stdClass();
					if($result['bucket'] == '' || $question_comp!=1 ){
						$data->redirect_url = 'pa';
					}else{
						$data->redirect_url = 'profile';
					}
					$gender = $this->userregister_model->get_user_gender($result['user_id']);
					$data->user_info = $this->getuserinfo_get($result['user_id']);//return value of get_users_info_mobile function
					// $update_gcm_id = $this->Mobile_model_android->update_gcm_id($gcm_id,$result[0]['user_id']);
					$data->pa = $this->pa_get($result['user_id'],$gender);
					$this->success_response($data);
			}else{
				$this->failed_response(1012, "Email id or password is incorrect");
			}
		}else {
			if($type == 'facebook' || $type == 'google'){
				$data = new stdClass();
				$data->success = 'Email id or password is incorrect';
				$this->success_response($data);
			}else{
				$this->failed_response(1006, "Email id or password is incorrect");
			}
		}
	}
	
	
	
	public function forget_user_email_get(){
	echo $sc_email_id = $this->get('sc_email_id');
	exit();
	$res = $this->schome_model->forgot_password($sc_email_id);
	if($res != false){
		if($this->send_forgotpwd_email($sc_email_id,$res)) 
		{
			   $data = new stdClass();
		       $data->message = 'New Password has been Send to your email-id '.$sc_email_id;
			   $this->success_response($data);
		}
		else{
		  $this->failed_response(1017, "Invalid Email-id ");
		}	
	}else{
		$this->failed_response(1012, "Authorization failed!");
	}
	
  }
  
  public function hello_post(){
	echo '\sdasd';
	exit();	
	
  }
	
	
	
	
	}
