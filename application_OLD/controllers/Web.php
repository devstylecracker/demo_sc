<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
    {
        // Call the Model constructor
        
        parent::__construct();
       $this->load->library('bcrypt');
       $this->load->library('form_validation');
      
        
        
    }
    
	public function order_history()
	{
	
			$this->load->view('common/header_view');
			
			$this->load->view('order-history');

			$this->load->view('common/footer_view');
	
	}

	public function cart()
	{
	
			$this->load->view('common/header_view');
			
			$this->load->view('scbox-cart');

			$this->load->view('common/footer_view');
	
	}

	public function thankyou()
	{
	
			$this->load->view('common/header_view');
			
			$this->load->view('scbox_thankyou');

			$this->load->view('common/footer_view');
	
	}

	public function about_us()
	{
	
			$this->load->view('common/header_view');
			
			$this->load->view('static/about_us');

			$this->load->view('common/footer_view');
	
	}


	public function contact_us()
	{
	
			$this->load->view('common/header_view');
			
			$this->load->view('static/contact_us');

			$this->load->view('common/footer_view');
	
	}

	public function faq()
	{
	
			$this->load->view('common/header_view');
			
			$this->load->view('static/faq');

			$this->load->view('common/footer_view');
	
	}
	public function help_feedback()
	{
	
			$this->load->view('common/header_view');
			
			$this->load->view('static/help_feedback');

			$this->load->view('common/footer_view');
	
	}
	public function privacy_policy()
	{
	
			$this->load->view('common/header_view');
			
			$this->load->view('static/privacy_policy');

			$this->load->view('common/footer_view');
	
	}
	public function return_policy()
	{
	
			$this->load->view('common/header_view');
			
			$this->load->view('static/return_policy');

			$this->load->view('common/footer_view');
	
	}
	public function return_policy_scbox()
	{
	
			$this->load->view('common/header_view');
			
			$this->load->view('static/return_policy_scbox');

			$this->load->view('common/footer_view');
	
	}
	public function terms_of_use()
	{
	
			$this->load->view('common/header_view');
			
			$this->load->view('static/terms_of_use');

			$this->load->view('common/footer_view');
	
	}
	public function testimonials()
	{
	
			$this->load->view('common/header_view');
			
			$this->load->view('static/testimonials');

			$this->load->view('common/footer_view');
	
	}
}
