<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scbox extends CI_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
    {
        // Call the Model constructor
        
        parent::__construct();
       $this->load->library('bcrypt');
       $this->load->library('form_validation');
       $this->load->model('Cart_model');
       $this->load->model('cartnew_model');
       $this->load->model('User_info_model');
       $this->load->model('frontend_model');
       $this->load->model('schome_model');
    }
    


	public function step1()
	{
		 $data = array();
		$objectid = @$_COOKIE['scbox_objectid'];
		$user_id = $this->session->userdata('user_id');
		if($objectid!='' && $user_id!='' )
		{
		  $scbox_data = $this->Pa_model->get_scbox_userdata($objectid,$user_id);
			  if(!empty($scbox_data))
			  {
				foreach($scbox_data as $val)
				{
				  $data['userdata'][$val['object_meta_key']] = $val['object_meta_value'];
				}
			  }
			   echo '<pre>';print_r($data);exit;
		}
			
			$this->load->view('common/header_view');
			$this->load->view('book-scboxform');
			$this->load->view('common/footer_view');
		
	}

	 public function getpincodedata(){
      $pincode = $this->input->post('scbox_pincode');
	  
			$curl = curl_init();
			curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => 'http://postalpincode.in/api/pincode/'.$pincode,
			CURLOPT_USERAGENT => 'Codular Sample cURL Request'
			));
			$resp = curl_exec($curl);
			curl_close($curl);
			$json = json_decode($resp, true);
			 $data = $json['PostOffice'];
			 //print_r($data);
			//exit();
      //$data = $this->Schome_model->getpincodedata($pincode);
      if(!empty($data)){
         echo '{"statename" : "'.trim(@$data[0]['State']).'", "city" : "'.trim(@$data[0]['District']).'"}';
       }else{
          echo 0;
       }     
  }
	
	
	
	public function step2()
	{
		
			$this->load->view('common/header_view');
			
			$this->load->view('book-scboxformwomen');

			$this->load->view('common/footer_view');
		
	}

	public function step3()
	{
		
			$this->load->view('common/header_view');
			
			$this->load->view('book-scboxformmen');

			$this->load->view('common/footer_view');
		
	}

	public function step4()
	{
		
			$this->load->view('common/header_view');
			
			$this->load->view('book-scbox-afterpaymentmen');

			$this->load->view('common/footer_view');
		
	}

	public function step5()
	{
		
			$this->load->view('common/header_view');
			
			$this->load->view('book-scbox-afterpaymentwomen');

			$this->load->view('common/footer_view');
		
	}

	public function my_orders()
	{
		
			$this->load->view('common/header_view');
			
			$this->load->view('order-history');

			$this->load->view('common/footer_view');
		
	}

	public function cart()
	{
		
			$this->load->view('common/header_view');
			
			$this->load->view('scbox-cart');

			$this->load->view('common/footer_view');
		
	}

	public function step6()
	{
		
			$this->load->view('common/header_view');
			
			$this->load->view('book-scbox_optional');

			$this->load->view('common/footer_view');
		
	}
	
	public function savescbox()
	{
		//$user_id			=	$this->input->post('user_id');
		//$auth_token			=	$this->input->post('auth_token');
		//$for				=	$this->input->post('for');
		$dob = explode('-',$this->input->post('scbox_age'));
		
		$gender				=	$this->input->post('gender');
		$full_name			=	$this->input->post('scbox_name');
		$day				=	$dob['2'];
		$month				=	$dob['1'];
		$year				=	$dob['0'];
		$email				=	$this->input->post('scbox_emailid');
		$mobile				=	$this->input->post('scbox_mobile');
		$pincode			=	$this->input->post('scbox_pincode');
		$city				=	$this->input->post('scbox_city');
		$state				=	$this->input->post('scbox_state');
		$profession			=	''; //$this->input->post('profession');
		$shipping_address   =	$this->input->post('scbox_shipaddress');
		$product_id   		=	$this->input->post('scbox_productid');
		$price   			=	$this->input->post('scbox_price');
		
		$scusername 		= preg_replace('/\s+/', ' ', trim($full_name));
		$username = explode(' ',trim($scusername));       
        $json_array['firstname'] = ucwords(strtolower($username[0]));
        $json_array['lastname'] = ucwords(strtolower($username[1]));
		
		
		$scpassword = $this->schome_model->randomPassword();
		$salt = substr(md5(uniqid(rand(), true)), 0, 10);
		$password   = $salt . substr(sha1($salt . $scpassword), 0, -10);
		
		$userEmail = $this->frontend_model->register_emial_web($email);
		/*echo "<pre>";
		print_r($user_id);
		echo "</pre>";
		exit();*/
		if(!empty($userEmail))
		{
			echo $user_id = $userEmail;
		}
		else{
			$arrAddUser 					= array();
			$arrAddUser['username']			= $scusername;			
			$arrAddUser['first_name']		= $username[0];			
			$arrAddUser['last_name']		= $username[1];			
			$arrAddUser['email']			= $email;			
			$arrAddUser['password']			= $password;			
			$arrAddUser['status']			= '1';			
			$arrAddUser['auth_token']		= substr(md5(uniqid(rand(1,6))), 0, 15);			
			$arrAddUser['created_datetime']	= date('Y-m-d H:i:s');			
			$user_id = $this->frontend_model->register_user_web($arrAddUser);
		}
		
		$arrPaData			= array();
		$arrPaData[0]['meta_key']		= 'pincode';
		$arrPaData[0]['meta_value']		= $pincode;
		$arrPaData[1]['meta_key']		= 'city';
		$arrPaData[1]['meta_value']		= $city;
		$arrPaData[2]['meta_key']		= 'state';
		$arrPaData[2]['meta_value']		= $state;
		$arrPaData[3]['meta_key']		= 'profession';
		$arrPaData[3]['meta_value']		= $profession;
		$arrPaData[4]['meta_key']		= 'shipping_address';
		$arrPaData[4]['meta_value']		= $shipping_address;
		
		if(is_array($arrPaData) && count($arrPaData)>0)
		{	
			foreach($arrPaData as $key => $addData)
			{
				$addData['scx_user_id']			= $user_id;
				$addData['created_datetime']	= date('Y-m-d H:i:s');
				$this->frontend_model->userMeta($addData);
				
				//echo $this->db->last_query();
				//echo "<br>=====<br>";
			}
			foreach($arrPaData as $orderKey => $orderMeta)
			{
				$forMeMeta[$orderMeta['meta_key']] = $orderMeta['meta_value'];
			} 
			
		}
		// for gift
		if($this->input->post('scbox_isgift') == 'true')
		{	
			$giftdob = explode('-',$this->input->post('scbox_billage'));
			
			$friend_gender			=	$this->input->post('scbox_billgender');
			$friend_full_name		=	$this->input->post('scbox_billname');
			$friend_day				=	$giftdob['2'];
			$friend_month			=	$giftdob['1'];
			$friend_year			=	$giftdob['0'];
			$friend_email			=	$this->input->post('scbox_billemailid');
			$friend_mobile			=	$this->input->post('scbox_billmobile');
			$friend_pincode			=	$this->input->post('scbox_billpincode');
			$friend_city			=	$this->input->post('scbox_billcity');
			$friend_state			=	$this->input->post('scbox_billstate');
			$friend_profession		=	$this->input->post('scbox_profession');
			$friend_billing_address =	$this->input->post('scbox_billaddress');	
			
			$gifter_details				= array();
			$gifter_details['gender'] 	= $friend_gender;
            $gifter_details['name'] 	= $friend_full_name;
            $gifter_details['email'] 	= $friend_email;
            $gifter_details['mobileno'] = $friend_mobile;
            $gifter_details['pincode'] 	= $friend_pincode;
            $gifter_details['city'] 	= $friend_city;
            $gifter_details['state'] 	= $friend_state;
            $gifter_details['address'] 	= $friend_billing_address;
            $gifter_details['profession'] = $friend_profession; 
            $gifter_details['dob'] 		= $friend_year.'-'.$friend_month.'-'.$friend_day; 
            $gifter_details_json 		= serialize($gifter_details);
			
			
			
			/*if(is_array($arrAddFriendData) && count($arrAddFriendData)>0)
			{	
				foreach($arrAddFriendData as $key => $addData)
				{
					 $this->Pa_model->save_scboxuser_answer('',$addData['object_meta_value'],$user_id,$object_id,$addData['object_meta_key']);
					 //echo $this->db->last_query();
					 
				}
			}*/	
				
		}
		
		$scxOrder = array();
		$scxOrder['order_id']			= 'SC'.time();
		$scxOrder['brand_id']			= '0';
		$scxOrder['product_id']			= $product_id;
		$scxOrder['product_price']		= $price;
		$scxOrder['order_status']		= '0';
		$scxOrder['parent_id']			= '0';
		$scxOrder['user_id']			= $user_id;
		$scxOrder['created_by']			= $user_id;
		$scxOrder['created_datetime']	= date('Y-m-d H:i:s');
		$orderId = $this->frontend_model->scxOrder($scxOrder);
		//echo $this->db->last_query();
		//echo "<br>=====<br>";
		
		$orderPaData = array();
		$orderPaData['meta_key']		= 'user_billing';
		$orderPaData['meta_value']		= serialize($forMeMeta);
		$orderPaData['created_by']		= $user_id;
		$orderPaData['scx_order_id']	= $orderId; 
		$orderPaData['created_datetime']= date('Y-m-d H:i:s');
		$this->frontend_model->orderMeta($orderPaData);
		//echo $this->db->last_query();
		//echo "<br>=====<br>";
		
		if($this->input->post('scbox_isgift') == 'true')
		{
			$arrAddFriendData 	= array();
			$arrAddFriendData['meta_key'] 		= 'user_shipping';
			$arrAddFriendData['meta_value'] 	= $gifter_details_json;
			$arrAddFriendData['created_by']		= $user_id;
			$arrAddFriendData['scx_order_id']	= $orderId; 
			$arrAddFriendData['created_datetime']= date('Y-m-d H:i:s');
			$this->frontend_model->orderMeta($arrAddFriendData);
			//echo $this->db->last_query();
			//echo "<br>=====<br>";
			
		}
		
		/* $data = new stdClass();
		$data->message = 'Data Send successfully.';
		$this->success_response($data); */
	}
	
	function checkuserdata(){
    $gender = '';
    $useremail = $this->input->post('useremail');
    $user_data =  $this->Cart_model->getUserDataByEmail($useremail);

    if(!empty($user_data))
    {
      $gender = $user_data[0]['gender']!='' ? $user_data[0]['gender'] : 0;
    }else
    {
      $gender = 0;
    }
    echo $gender;
  }
	
}
