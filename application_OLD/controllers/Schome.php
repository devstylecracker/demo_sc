<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// require APPPATH.'libraries/IntercomClient.php';
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
class Schome extends MY_Controller {

	
	function __construct(){
		parent::__construct();
		$this->load->model('Schome_model');
    $this->load->model('Userregister_model');
    $this->load->library('bitly');
    $this->load->library('email');
    $this->load->driver('cache');
		/*$this->load->model('home_model');
		$this->load->model('Pa_model');
		$this->load->model('Product_desc_model');	
		$this->load->library('user_agent');
		$this->load->library('curl');
		$this->load->model('Collection_model');
		$this->load->model('Event_model');
		$this->load->model('Mobile_look_model');
		$this->load->model('Favorites_model');
		$this->load->model('User_info_model');
		$this->load->model('Blog_model');
		$this->load->model('Brand_model');*/
		$this->load->model('frontend_model');

  }


 public function index(){
    
      if(!$this->session->userdata('user_id')){

        if($this->input->cookie('34b07cba21adf4cd95d35c1c0bd47ff0') && $this->input->cookie('1c0253b55e82e53890985a837274811b')){
            $logEmailid = $this->encryptOrDecrypt($this->input->cookie('34b07cba21adf4cd95d35c1c0bd47ff0'),'');
            $logPassword = $this->encryptOrDecrypt($this->input->cookie('1c0253b55e82e53890985a837274811b'),'');
            //$result = $this->User_info_model->login_user(trim($logEmailid),trim($logPassword),'website','');
          }
        if(!$this->session->userdata('user_id')){
              $data = array();
              $user_data = array();
              $this->seo_title = $this->lang->line('seo_title');
              $this->seo_desc = $this->lang->line('seo_desc');
              $this->seo_keyword = $this->lang->line('seo_keyword');

              if(isset($_REQUEST['utm_source'])){
                $utm_source = substr($_REQUEST['utm_source'],strpos($_REQUEST['utm_source'],"utmcsr="));
                $array_field = array('utm_source' => $utm_source,'utm_source1' => trim($utm_source));
                $this->session->set_userdata($array_field);
              }             

			    $taxonomy_id_female	= 1;
				$data['scbox_pack_female'] 	= $this->frontend_model->scxObject($taxonomy_id_female);
				//echo $this->db->last_query();
				$taxonomy_id_male	= 2;
				$data['scbox_pack_male'] 	= $this->frontend_model->scxObject($taxonomy_id_male);
              // if( $this->cache->file->get('filter_type_scbox') == FALSE)
              // {
                $this->load->view('common/header_view');
                $this->load->view('scbox',$data);
                $this->load->view('common/footer_view');
              //   $this->cache->file->save('filter_type_scbox', $data,500000000000000000);
              // }else
              // {                
              //    $data = $this->cache->file->get('filter_type_scbox');
              // }
        }else{
          header("Location: ".base_url());
          exit;
        }

      }else{

          $data = array();
		  
		    $taxonomy_id_female	= 1;
			$data['scbox_pack_female'] 	= $this->frontend_model->scxObject($taxonomy_id_female);
			$taxonomy_id_male	= 2;
			$data['scbox_pack_male'] 	= $this->frontend_model->scxObject($taxonomy_id_male);
		  
          $this->seo_title = $this->lang->line('seo_title');
          $this->seo_desc = $this->lang->line('seo_desc');
          $this->seo_keyword = $this->lang->line('seo_keyword');
          $this->load->helper('cookie');
          if(isset($_REQUEST['utm_source'])){
             $utm_source = substr($_REQUEST['utm_source'],strpos($_REQUEST['utm_source'],"utmcsr="));
             $array_field = array('utm_source' => $utm_source,'utm_source1' => trim($utm_source));
             $this->session->set_userdata($array_field);
           }

           if(($this->uri->segment(1)) != "" && ($this->uri->segment(1)) != "all"){
            setcookie('SCGender', $this->uri->segment(1), time() + ((86400 * 2) * 30), "/", "",  0);
            $filter_selection = $this->uri->segment(1);
            $filter_type = strtolower($this->uri->segment(1));
            $this->seo_canonical = base_url().$this->uri->segment(1);
          }else if($this->uri->segment(1) == "all"){
            $this->seo_canonical = base_url();
            setcookie('SCGender', $this->uri->segment(1), time()-3600, "/", "",  0);
          }else if(isset($_COOKIE['SCGender'])){

            $filter_selection = $_COOKIE['SCGender'];
            $filter_type = strtolower($_COOKIE['SCGender']);
          }

          if(isset($_COOKIE['SCGender'])){
            $uri = $this->uri->segment(1);
            if($uri != $filter_type && $uri==''){
              redirect($filter_type);
            }
          }


          $user_data['user_id'] = $this->session->userdata('user_id');
          $user_data['limit'] = 4;
          $user_data['offset'] = 0;
          //$user_data['body_shape'] = @$this->Pa_model->get_users_answer_obj($user_data['user_id'],'user_body_shape')[0]['id'];
         // $user_data['pa_pref1'] = @$this->Pa_model->get_users_answer_obj($user_data['user_id'],'user_style_p1')[0]['id'];
          $user_data['type'] = '';
          $user_data['gender'] = $this->session->userdata('gender'); 
          $data['user_id'] = $this->session->userdata('user_id');
          $user_data['bucket_id'] = $this->session->userdata('bucket');       
        
        // if( $this->cache->file->get('filter_type_scbox') == FALSE)
        // {
          $this->load->view('common/header_view');
          $this->load->view('scbox',$data);
          $this->load->view('common/footer_view');
        //   $this->cache->file->save('filter_type_scbox', $data,500000000000000000);
        // }else
        // {                
        //    $data = $this->cache->file->get('filter_type_scbox');
        // }
    }
}

 public function register_user(){
    $data = array();
    $data['agerange'] = '';

    if(empty($this->session->userdata('user_id')))
    {
        if(!empty($this->input->post())){ 
          $sc_name = preg_replace('/\s+/', ' ',trim($this->input->post('sc_name')));
         
          $sc_name = explode(' ',$sc_name);
          @$sc_firstname = trim($sc_name[0]);
          @$sc_lastname = trim($sc_name[1]);
          $dob = $this->input->post('sc_dob');
          $user_day = $this->input->post('day_start');
          $user_month = $this->input->post('month_start');
          $user_year = $this->input->post('year_start');          
          $scmobile = $this->input->post('sc_mobile');
          @$json_array['firstname'] = $sc_firstname;
          @$json_array['lastname'] = $sc_lastname;
          @$json_array['birthday'] = $dob;         
          
          @$scusername = @$json_array['firstname'].'-'. @$json_array['lastname'];
          @$sc_gender = $this->input->post('sc_gender');
          @$json_array['scmobile'] = $scmobile;
          @$json_array['question_id'] = $sc_gender==1 ? 8:17;
          @$json_array['sc_dob'] = $this->input->post('sc_dob');
		 /*  $this->load->library("session");
		  $this->session->set_userdata('userDetail', $json_array); */
          $scemail_id = $this->input->post('sc_emailid');
          $scpassword = $this->input->post('sc_password');
          $promo = $this->input->post('registered_from') !='' ? $this->input->post('registered_from') : '';
		  
          /* Added By Sudha For Event*/
          $event = $this->input->post('sc_event') !='' ? $this->input->post('sc_event') : '';
          $data['inputData'] = '';  

         if(trim($sc_firstname) == '' || trim($sc_lastname) == ''){
            $data['error'] = "Please enter the Firstname Lastname";

          }else if(trim($json_array['sc_dob']) == ''){
             $data['error'] = "Please select Date of birth";

          }else  if(trim($scemail_id) == ''){
           $data['error'] = "Please enter the Email-id";

          }else if(trim($sc_gender) == ''){
            $data['error'] = "Please select Gender";

          }else if(trim($scpassword) == ''){
            $data['error'] = "Please enter the Password";

          }else if(trim($scmobile) == '')
          {
            $data['error'] = "Please enter the Mobile No.";
          }else if(filter_var($scemail_id, FILTER_VALIDATE_EMAIL) === false){ 
            $data['error'] = "Please enter valid Email-id";

          }else if(!empty($this->Userregister_model->emailUnique($scemail_id))){
             $data['error'] = "Email id already exists";
          }else if($event!=''){ 

             //  echo 'website=='.$scusername.'==,=='.$scemail_id.'==,=='.$scpassword.'==,=='.$ip.'==,=='.$sc_gender.'==,=='.$json_array;exit;
            $ip = $this->input->ip_address();
            $scevent = strip_tags(trim($event));
            $eventcode = $scevent;
            $json_array['event_code'] = $eventcode;
            $this->signup('event',$scusername,$scemail_id,$scpassword,$ip,2,$sc_gender,$json_array);

          }else{   
            
            $ip = $this->input->ip_address();
            //echo 'website=='.$scusername.'==,=='.$scemail_id.'==,=='.$scpassword.'==,=='.$ip.'==,=='.$sc_gender.'==,=='.$json_array;exit;
            if($this->input->post('sc_signup')=='edit'){ 
                $this->signupedit('website',$scusername,$scemail_id,$scpassword,$ip,2,$sc_gender,$json_array);
            }else{ 
                $webres = $this->signup('website',$scusername,$scemail_id,$scpassword,$ip,2,$sc_gender,$json_array);
            }

            if($promo=='comp'){
                //$this->session->set_userdata('campaign','yes');
            }
            if($webres)
            {
              /*  // Mailchimp call For New User
                 if(!empty($this->session->userdata('user_id')))
                {
                  if($this->Schome_model->get_nonscdomain($scemail_id))
                  {
                   // $result = $this->curl->simple_post(BACKENDURL.'/Mailchimp/newUserImport/'.$this->session->userdata('user_id'), false, array(CURLOPT_USERAGENT => true));
                  }
                }
                // End Mailchimp call For New User
                if($scemail_id!='')
                {
                  //$this->Schome_model->update_couponcode($scemail_id,'Welcome10');
                }*/
                $otp = $this->scgenrate_otp($scmobile);
               $this->send_welcome_email($scemail_id);
               redirect('otp');
               //redirect(base_url().'scbox');

            }else
            {
              //redirect(base_url().'signup');
            }
           
          }
          $inputData = array('sc_name' => $sc_name, 'sc_gender' => $sc_gender, 'scmobile' => $scmobile, 'scemail_id' => $scemail_id, 'user_dob' => $dob);
		  
		  
          $data['inputData'] = json_encode($inputData);
          $this->load->view('common/header_view');
		  $this->load->view('signup',$data);
          $this->load->view('common/footer_view');

        }else{

          $this->load->view('common/header_view');
          $this->load->view('signup',$data);
          $this->load->view('common/footer_view');

        }
    }else
    { echo 'redirect';exit;
       redirect('/');
    }

  }

  
  public function signup($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$sc_gender=1, $json_array = array()){
     $res = array();
      $res = $this->Userregister_model->registerUser($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$sc_gender,$json_array);
      if($scemail_id!='')
      {
        //$this->Schome_model->update_couponcode($scemail_id,'Welcome10');
      }
      if($res){ return $res; }else{ return '0'; }

  }

  
  public function logout(){ 
      $result = $this->Schome_model->track_logout();
      unset($_COOKIE['billing_first_name']);
      unset($_COOKIE['billing_last_name']);
      unset($_COOKIE['billing_mobile_no']);
      unset($_COOKIE['billing_address']);
      unset($_COOKIE['billing_city']);
      unset($_COOKIE['billing_state']);

      unset($_COOKIE['shipping_first_name']);
      unset($_COOKIE['shipping_last_name']);
      unset($_COOKIE['shipping_mobile_no']);
      unset($_COOKIE['shipping_address']);
      unset($_COOKIE['shipping_city']);
      unset($_COOKIE['shipping_state']);
      unset($_COOKIE['SCUniqueID']);

      setcookie('billing_first_name', '', time() - 3600, '/');
      setcookie('billing_last_name', '', time() - 3600, '/');
      setcookie('billing_mobile_no', '', time() - 3600, '/');
      setcookie('billing_address', '', time() - 3600, '/');
      setcookie('billing_city', '', time() - 3600, '/');
      setcookie('billing_state', '', time() - 3600, '/');

      setcookie('shipping_first_name', '', time() - 3600, '/');
      setcookie('shipping_last_name', '', time() - 3600, '/');
      setcookie('shipping_mobile_no', '', time() - 3600, '/');
      setcookie('shipping_address', '', time() - 3600, '/');
      setcookie('shipping_city', '', time() - 3600, '/');
      setcookie('shipping_state', '', time() - 3600, '/');
      setcookie('SCUniqueID', '', time() - 3600, '/');

      session_destroy();
      //md5(STYLELOGINID)   34b07cba21adf4cd95d35c1c0bd47ff0
      //md5(STYLELOGINPWD) 1c0253b55e82e53890985a837274811b
      delete_cookie("34b07cba21adf4cd95d35c1c0bd47ff0");
      delete_cookie("1c0253b55e82e53890985a837274811b");
      redirect('/');
  }

  public function login(){
    $data = array();
	if(empty($this->session->userdata('user_id')))
    {
			  if(!empty($this->input->post())){

  				$logEmailid = $this->input->post('logEmailid');
  				$logPassword = $this->input->post('logPassword');
  				$logRemember = $this->input->post('logRemember');

				 if(trim($logEmailid) == ''){

					  $data['error'] = 'Please enter your email id';
					  $this->load->view('seventeen/common/header_view');
					  $this->load->view('seventeen/login',$data);
					  $this->load->view('seventeen/common/footer_view');

				  }else if(trim($logPassword) == ''){

					  $data['error'] = 'Please enter the Password';
					  $this->load->view('common/header_view');
					  $this->load->view('login',$data);
					  $this->load->view('common/footer_view');

				  }else{
				   $result = $this->Userregister_model->login_user($logEmailid,$logPassword,'website',' ');
					if($result == 1){

					 if($logRemember == 1){
					  //md5(STYLELOGINID)   34b07cba21adf4cd95d35c1c0bd47ff0
					  //md5(STYLELOGINPWD) 1c0253b55e82e53890985a837274811b
					  setcookie('34b07cba21adf4cd95d35c1c0bd47ff0',$this->encryptOrDecrypt($logEmailid,'encrypt'), time() + (86400 * 30), "/");
					  setcookie('1c0253b55e82e53890985a837274811b',$this->encryptOrDecrypt($logPassword,'encrypt'), time() + (86400 * 30), "/");
					 }

  					  $question_comp = $this->session->userdata('question_comp');
  					  $question_resume_id = $this->session->userdata('question_resume_id');

  					  if($this->session->userdata('bucket') == '' || $question_comp!=1 ){					
  						  // redirect(base_url().'schome');
  						if(@$this->session->userdata('url') != ''){
  							// redirect($this->session->userdata('url'));
  							header("Location: ".$this->session->userdata('url'));
  						}else{
  							redirect(base_url().'schome');
  						}						

					  }else{
  						setcookie('is_redirect', 1, time() + (86400 * 30), "/");					
  						//redirect(base_url().'Userfeed', 'refresh');
  						// redirect(base_url().'schome');
  						if($this->session->userdata('url') != ''){
  							// redirect($this->session->userdata('url'));
  							header("Location: ".$this->session->userdata('url'));
  						}else{
  							redirect(base_url().'schome');
  						}
					  }

					  /*Mailchimp call For UserSignUp*/
						 if(!empty($this->session->userdata('user_id')))
						{
						  if($this->Schome_model->get_nonscdomain($logEmailid))
						  {
							//$result = $this->curl->simple_post(BACKENDURL.'/Mailchimp/newUserImport/'.$this->session->userdata('user_id'), false, array(CURLOPT_USERAGENT => true));
						  }
						}
					  /*End Mailchimp call For UserSignUp*/

					  /*OneSignal Notification*/
					  //$os_data = $this->senddata_onesignal();
					  //$this->session->set_userdata(array('onesignaldata'=>$os_data));
					  /*OneSignal Notification End*/

					}else{

					  $data['error'] = 'Invalid Email and Password';
					  $this->load->view('common/header_view');
					  $this->load->view('login',$data);
					  $this->load->view('common/footer_view');
					}

				  }
			 }else
			 {
				$this->load->view('common/header_view');
				$this->load->view('login');
				$this->load->view('common/footer_view');
			 }
	}else
    {
       redirect('/');
    }
  }




  public function register_user_google_signin(){

    if (isset($_GET['code'])) {

      $this->googleplus->getAuthenticate();
      $user_profile = $this->googleplus->getUserInfo();
      //$this->session->set_userdata('login',true);
      //$user_profile = $this->session->set_userdata('user_profile',$this->googleplus->getUserInfo());

      $scpassword = $this->randomPassword();
      $ip = $this->input->ip_address();
      if(isset($user_profile['given_name'])) $user_profile['firstname'] = $firstName = $user_profile['given_name']; else $firstName = '';
      if(isset($user_profile['family_name'])) $user_profile['lastname'] = $lastName = $user_profile['family_name']; else $lastName = '';
      $scusername = $firstName.$lastName;
      $scemail_id = $user_profile['email'];
      $gender = 1;
      if($user_profile['gender'] == 'male')
        $gender = 2;

      $result = $this->signup('google',$scusername,$scemail_id,$scpassword,$ip,2,$gender,$user_profile);
      /*Mailchimp call For UserSignIn*/
         if(!empty($this->session->userdata('user_id')))
        {
            //$result = $this->curl->simple_post(BACKENDURL.'/Mailchimp/newUserImport/'.$this->session->userdata('user_id'), false, array(CURLOPT_USERAGENT => true));
        }
      /*End Mailchimp call For UserSignIn*/
      if($result == 'pa')
      {
         redirect('schome/pa?ga=ga');
      }else redirect('schome');

    }

    $this->data['google_login_url'] = $this->googleplus->loginURL();
  }

  public function send_welcome_email($email){

      $config['protocol'] = 'smtp';
//      $config['mailpath'] = '/usr/sbin/sendmail';
      $config['charset'] = 'iso-8859-1';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');
      $data = array();
      $this->email->initialize($config);

      $username = $this->session->userdata('first_name');
      $this->email->from($this->config->item('from_email'), 'Stylecracker');
      $this->email->to($email);
      $this->email->subject('Welcome to StyleCracker');
      $this->email->message('Hi '.$username.',<br>Welcome to StyleCracker! <br><br> StyleCracker is India\'s first online styling platform and we are here to make you the best version of youself! <br><br> When we started StyleCracker our aim was to make personalized styling available to all. We have a dedicated team of stylists who are online 24/7 waiting to answer ANY style query you may have. Never again will you have to worry about \'what do I wear\'- be it day-to-day or for special occasions, we will be there! <br><br> So, please explore the website and let us know how we can help. It would be great if you could also share your feedback with us, it will help us present the best product for you. <br><br> Love, <br>Archana Walavalkar & Dhimaan Shah <br>Founders<br> <a href="'.base_url().'">www.stylecracker.com</a>');
      $this->email->send();
      /*$this->load->view('emailer/welcome-to-sc',$data);
      $this->email->subject('StyleCracker has a new friend');
      $message = $this->load->view('emailer/welcome-to-sc',$data,true);
      $this->email->message($message);
     $this->email->send();*/
  }

  /*Forgot Password Function*/
  public function forgotPassword()
  {
    $Emailid = $this->input->post('Emailid');
    if(trim($Emailid) == ''){
            echo "Please enter your email id";

          }else{
            $result = $this->Schome_model->forgot_password($Emailid);
            if($result != false){

               if($this->send_forgotpwd_email($Emailid,$result))
               {
                  echo 'New Password has been Send to your email-id '.$Emailid;
               }else
               {
                  echo "Email Error";
               }

            }else{
              echo 'Invalid Email-id ';

            }

          }
  }

/*Reset Password Function*/
  public function resetPassword()
  {
    $oldpwd = $this->input->post('oldpwd');
    $newpwd = $this->input->post('newpwd');

    if(trim($oldpwd) == ''){
        echo "Please enter your Old Password";

          }if(trim($newpwd) == ''){
            echo "Please enter your New Password";

          }else{
            $result = $this->Schome_model->reset_password($oldpwd,$newpwd);
            if($result){

             echo "Your Password has been reset";

            }else{

              if($result == false){
                echo "Old Password is not correct";
              }else
              {
                echo $result;
              }
            }
          }
  }


  public function send_forgotpwd_email($email_id,$new_password){

      $config['protocol'] = 'smtp';
//      $config['mailpath'] = '/usr/sbin/sendmail';
 //     $config['charset'] = 'iso-8859-1';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');

      $this->email->initialize($config);

      $this->email->from($this->config->item('from_email'), 'Stylecracker');
      $this->email->to($email_id);
      $this->email->subject('StyleCracker: New Password');
      $this->email->message("Your login details are <br> Email : ".$email_id."<br><br>Your new password is:".$new_password."<br><br>You can change your password at any time by logging into your account. <br><br>Warm Regards,<br>StyleCracker");

      if($this->email->send())
      {
        return "Email Sent";
      }else
      {
        return "Email Error";
      }

  }

 
	function confirm_email(){
		$email = base64_decode($this->uri->segment(3));
		$confirm_email = $this->Schome_model->confirm_email($email);
		$referee_id = $this->Schome_model->getreferee_id($email);
		if($confirm_email == '1'){

			// if($referee_id > 0){
				// $bid = '11257';
				// $secretKey = '72BB9BBBC9FDCA3165355D2F0985A983';
				// $campaignID = '10607';
				// $orderID = $referee_id; //set order id
				// $status = '1'; //status of the order
				// $event = 'register';
				// $http_val = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://';
				// $result = file_get_contents($http_val.'www.ref-r.com/campaign/t1/confirmConversion?secretKey='.$secretKey.'&bid='.$bid.'&campaignID='.$campaignID.'&orderID='.$orderID.'&status='.$status.'&event='.$event);
			// }

			$this->load->view('common/header_view');
			$this->load->view('login/login');
			$this->load->view('common/footer_view');
		}else {
			 header("Location: https://www.stylecracker.com/");
		}

	}

  public function home_new(){
    if($this->session->userdata('user_id') > 0){
      header("Location: ".base_url());
      exit;
    }

    if(isset($_REQUEST['utm_source'])){
      $utm_source = substr($_REQUEST['utm_source'],strpos($_REQUEST['utm_source'],"utmcsr="));
      $array_field = array('utm_source' => trim($utm_source),'utm_source1' => trim($utm_source));
      $this->session->set_userdata($array_field);

   }

   $this->seo_title = $this->lang->line('seo_title');
   $this->seo_desc = $this->lang->line('seo_desc');
   $this->seo_keyword = $this->lang->line('seo_keyword');

    if($this->cache->file->get('home_new') == FALSE){
      //$data['products'] = $this->home_model->get_latest_home_page_products();
      $data['products'] = '';
      $this->cache->file->save('home_new', $data['products'],500000000000000000);
    }else{
       $data['products'] = $this->cache->file->get('home_new');
    }
    $this->load->view('common/header_view');
    $this->load->view('home/home_new',$data);
    $this->load->view('common/footer_view');
  }

	public function senddata_onesignal(){
		$osdata = array();
		$os_fname = (@$this->session->userdata('first_name')!='') ? $this->session->userdata('first_name') : ' ';
		$os_lname = (@$this->session->userdata('last_name')!='') ? $this->session->userdata('last_name') : ' ';
		$osdata['name'] =  $os_fname.' '.$os_lname;
		$osdata['email'] =   (@$this->session->userdata('email')!='') ? $this->session->userdata('email') : ' ';
		$osdata['gender'] =   (@$this->session->userdata('gender')!='' && @$this->session->userdata('gender')==1 ) ? 'female' : 'men ';
		$osdata['user_id'] =   (@$this->session->userdata('user_id')!='') ? $this->session->userdata('user_id') : ' ';

		//$pa_data = $this->home_model->get_user_pa($this->session->userdata('user_id'));
    $pa_data = '';
		 $question_ans = array();
			 if(!empty($pa_data)){
				foreach($pa_data as $val){
					 $question_ans[$val['question_id']] = $val['answer'];
				}
			 }
		//$cart = $this->home_model->get_user_cart(@$this->session->userdata('user_id'));
		if(@$this->session->userdata('gender')!='' && @$this->session->userdata('gender')==1)
		{ /*For Female*/


		  $osdata['user_age'] =   (@$question_ans[8]!='') ? $question_ans[8] : ' ';
		  $osdata['bucket_id'] =   (@$this->session->userdata('bucket')!='') ? $this->session->userdata('bucket') : ' ';
		  $osdata['user_size'] =   (@$question_ans[9]!='') ? $question_ans[9] : ' ';
		  $osdata['user_style'] =   (@$question_ans[2]!='') ? $question_ans[2] : ' ';
		  $osdata['user_budget'] =   (@$question_ans[7]!='') ? $question_ans[7] : ' ';
		  $osdata['user_body_shape'] = (@$question_ans[1]!='') ? $question_ans[1] : ' ';
		  $osdata['abandoned_cart'] =  (@$cart==1) ? 'yes' : 'no';

		}else
		{ /*For Male*/
		  $bt = strip_tags($question_ans[13]);
		  $bodyType=substr($bt, 0, strrpos($bt, ' '));
		  $osdata['user_age'] =   (@$question_ans[15]!='') ? $question_ans[15] : ' ';
		  $osdata['body_type'] =   (@$bodyType!='') ? $bodyType : ' ';
		  $osdata['bucket_id'] =   (@$this->session->userdata('bucket')!='') ? $this->session->userdata('bucket') : ' ';
		  $osdata['work_style'] =   (@$question_ans[14]!='') ? strip_tags($question_ans[14]) : ' ';
		  $osdata['body_height'] =   (@$question_ans[17]!='') ? $question_ans[17] : ' ';
		  $osdata['user_budget'] =   (@$question_ans[18]!='') ? $question_ans[18] : ' ';
		  $osdata['personal_style'] =   (@$question_ans[16]!='') ? $question_ans[16] : ' ';
		  $osdata['abandoned_cart'] =   (@$cart==1) ? 'yes' : 'no';
		}

		$encode_os = json_encode($osdata);
		 //$this->session->set_userdata(array('onesignaldata'=>$os_data));
		return $encode_os;
	}


	
	/*function get_all_notification(){
		$user_data['user_id'] = $this->session->userdata('user_id');
		$data = $this->User_info_model->get_all_notification($user_data);
		$li_html = '';$count_html = '';$notification_count = 0;
		if(!empty($data)){
			foreach($data as $val){
				$active = '';if($val['order_notification_read'] == 1){ $active = 'active'; }else{ $notification_count++; }
				$li_html = $li_html.'<li onclick="ga("send", "event", "Notification", "clicked", "'.$val['notification_id'].'");" class="'.$active.'" id="notification'.$val['notification_id'].'" onclick="notification_read('.$val['notification_id'].');"><i class="icon-bag ic"></i> '.$val['message'].' </li>';
			}
		}else {
			$li_html = 'No notification found';
		}
		if($notification_count>0){$count_html = '<span class="ic-count shape-diamond solid">'.$notification_count.'</span>';}
		$notification_html = '<a class="dropdown-toggle" data-toggle="dropdown"  href="#"><i class="icon-notification"></i>
             '.$count_html.'
              </a>
              <div class="dropdown-menu dropdown-menu-notifications">
                  <div class="scrollbar">
                <ul>
                  '.$li_html.'
                </ul>
                  </div>
              </div>';
		
		// echo "<pre>";print_r($data);exit;
		echo $notification_html;
	}*/
	
	/*function update_notification(){
		$user_data['notification_id'] = $this->input->post('notification_id');
		$user_data['order_notification_read'] = '1';
		$data = $this->User_info_model->update_notification($user_data);
		return true;
	}*/

   /*public function cart_login(){
    $data = array();
      if(!empty($this->input->post())){

        $logEmailid = $this->input->post('logEmailid');
        $logPassword = $this->input->post('logPassword');       

         if(trim($logEmailid) == ''){

             $data['error'] = 'Please enter your email id';
              // $this->load->view('seventeen/common/header_view');
              // $this->load->view('seventeen/login',$data);
              // $this->load->view('seventeen/common/footer_view');

          }else if(trim($logPassword) == ''){

            $data['error'] = 'Please enter the Password';
              // $this->load->view('seventeen/common/header_view');
              // $this->load->view('seventeen/login',$data);
              // $this->load->view('seventeen/common/footer_view');

          }else{

           $result = $this->User_info_model->login_user($logEmailid,$logPassword,'website',' ');

            if($result == 1){

                echo 'Loggedin';

             }
           }
         }
       } */
	
	function fb_test(){
		$user_data = $this->input->post('response');
		$test = $this->input->post('test');
		if($test == 'test'){ echo "<pre>";print_r($user_data);exit; }
		if(!empty($user_data)){ echo "<pre>";print_r($user_data);exit; }
		$this->load->view('seventeen/common/header_view');
		$this->load->view('seventeen/test',$object_data);
		$this->load->view('seventeen/common/footer_view');
	}

	function save_fb_token(){
		$user_id = $this->session->userdata('user_id');
		$data = serialize($this->input->post('data'));
		//$result = $this->User_info_model->saveupdate_fb_token($data,$user_id);
		return true;
	}

  public function scgenrate_otp($uc_mobile_no){
    if($uc_mobile_no!='')
    {
      $data['mobile_no'] = '91'.$uc_mobile_no;
      $data['type'] = 'register';
      $otp_no = $this->Userregister_model->scgenrate_otp($data);
      if($otp_no > 0){
        /* Call SMS send API */
        $message = 'Welcome+to+StyleCracker.+Your+OTP+for+mobile+verification+is+'.$otp_no.'.+Stay+Stylish!';
        file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile='.$data['mobile_no'].'&message='.$message);       
        /* end of code Call SMS send API */
        return $otp_no;
      }else{
        return false;
      }
    }
  }
  
  public function otp()
  {
	$data  = array();
	$validation_config = array(
		array(
			'field'=>'otp',
			'label'=>'OTP',
			'rules'=>'trim|required'
		)
	);
	$this->form_validation->set_rules($validation_config);
	if($this->form_validation->run() == FALSE)
	{
		$this->load->view('common/header_view');
		$this->load->view('otp', $data);
		$this->load->view('common/footer_view');
	}
	else
	{
		$userdata = $this->session->userdata("userDetail");
		$user_id = $_SESSION['user_id'];
		$getMobile = $this->Userregister_model->get_user_mobile($user_id);
		$val['otp'] 	= $this->input->post('otp');
		$val['mobile'] 	= '91'.$getMobile['meta_value'];
		$update = $this->Userregister_model->val_otp($val);
		if($update == 1)
		{
			$userMeta = array();
			$userMeta['meta_key'] 			= 'confirm_mobile';
			$userMeta['meta_value']			= '1';
			$userMeta['scx_user_id']		= $_SESSION['user_id'];
			$userMeta['created_datetime']	= date('Y-m-d H:i:s');
			$this->Userregister_model->confirm_mobile_meta($userMeta);
		}
		else{
			$this->session->set_flashdata('error','Invalid OTP! Please Enter Correct OTP.'); 
			redirect('otp');
			exit();
		}
		redirect(base_url().'scbox');
	}
  }

	
}
