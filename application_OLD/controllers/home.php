<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
		 
	function __construct()
    {
        // Call the Model constructor        
        parent::__construct();        
    }
    
	public function index()
	{		
		if(!$this->session->userdata('user_id')){		
			$this->load->view('login');
		}else{
			$this->load->view('home');
		}	
	}

	public function categories()
	{		
		$this->load->view('categories');	
	}

}