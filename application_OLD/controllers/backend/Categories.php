<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Categories_model');
    }

    function index(){
    	
    	if($this->has_rights(1) == 1 && $this->session->userdata('user_id')){
    		$data = array();
    		 $is_true = 0;
    		if($this->input->post()){
                $this->form_validation->set_rules('category_name','Category Name','required|trim|min_length[2]|max_length[50]');
                
                if($this->form_validation->run() == FALSE){   
                    
                }else{ 
                   
                    if(@$_FILES['category_image']['name']!=''){
                       $ext = pathinfo($_FILES['category_image']['name'], PATHINFO_EXTENSION);
                        $image_info = getimagesize($_FILES["category_image"]["tmp_name"]);
                        $image_width = $image_info[0]; 
                        $image_height = $image_info[1];
                        if($ext!='jpg' || $image_width!=530 || $image_height!=530){
                            $is_true = 1;
                       }
                    }

                }
                if($is_true == 0){
    			     $this->Categories_model->save_category($_POST);
                     $_POST = array();
                }else{
                    $data['error'] = "Check image format or dimension";
                }
    		}
            $data['cat_data'] = $this->Categories_model->get_cat_data();
            $data['get_cat'] = $this->Categories_model->get_cat();
            
			$this->load->view('backend/common/header');
            $this->load->view('categories',$data);
            $this->load->view('backend/common/footer');

        }else{

            $this->load->view('backend/common/header');
            $this->load->view('backend/not_permission');
            $this->load->view('backend/common/footer');
        }
        
    }

    function categories_edit($id=null){
        if($this->has_rights(1) == 1 && $this->session->userdata('user_id')){
            $data = array(); $is_true = 0;
            
            if($this->input->post()){

                if(@$_FILES['category_image']['name']!=''){
                       $ext = pathinfo($_FILES['category_image']['name'], PATHINFO_EXTENSION);
                        $image_info = getimagesize($_FILES["category_image"]["tmp_name"]);
                        $image_width = $image_info[0]; 
                        $image_height = $image_info[1];
                        if($ext!='jpg' || $image_width!=530 || $image_height!=530){
                            $is_true = 1;
                       }
                }

                if($is_true == 0){
                     $this->Categories_model->edit_category($_POST,$id);
                     $_POST = array();
                }else{
                    $data['error'] = "Check image format or dimension";
                }

            }

            $data['cat_data_data'] = $this->Categories_model->get_categories($id);
            
            $data['get_cat'] = $this->Categories_model->get_cat();
            $data['cat_data'] = $this->Categories_model->get_cat_data($data['cat_data_data'][0]['category_parent']);

            $data['bucket'] = $this->Categories_model->get_all_buckets();

            $data['bucket_data'] = $this->Categories_model->get_pa_data($id,'category','bucket');
            $data['age_data'] = $this->Categories_model->get_pa_data($id,'category','age');
            $data['budget_data'] = $this->Categories_model->get_pa_data($id,'category','budget');

            $data['comp_cat'] = $this->Categories_model->get_cat_data($data['cat_data_data'][0]['comp_cat']);

            $age_id = '8,17';
            $data['age'] = $this->Categories_model->get_question_answers($age_id);

            $budget_id = '7,18';
            $data['budget'] = $this->Categories_model->get_question_answers($budget_id);

            $this->load->view('common/header');
            $this->load->view('categories',$data);
            $this->load->view('common/footer');
         }else{

            $this->load->view('common/header');
            $this->load->view('not_permission');
            $this->load->view('common/footer');
        }
    }

    function check_product_count(){
        if (!$this->input->is_ajax_request()) {
           exit('No direct script access allowed');
        }else{
           $id = $this->input->post('id');
           echo $this->Categories_model->get_categories($id)[0]['count'];
        }
    }

    function delete_category(){
        if (!$this->input->is_ajax_request()) {
           exit('No direct script access allowed');
        }else{
           $id = $this->input->post('id');
           echo $this->Categories_model->delete_category($id);
        }
    }

    function search_category(){
        if (!$this->input->is_ajax_request()) {
           exit('No direct script access allowed');
        }else{
            $data = array();
            $search_category = @$this->input->post('search_text');
            $data['cat_data'] = $this->Categories_model->get_filtered_category($search_category);
            
            $this->load->view('categories_list',$data);
        }
    }

    function hide_show_cat(){
        if (!$this->input->is_ajax_request()) {
           exit('No direct script access allowed');
        }else{
            $cat_hide_show = $this->input->post('cat_hide_show');
            $cat = $this->input->post('cat');
            $this->Categories_model->hide_show_cat($cat_hide_show,$cat);
        }
    }
}
?>