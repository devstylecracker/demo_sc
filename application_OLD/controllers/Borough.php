<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Borough extends CI_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
    {
        // Call the Model constructor
        
        parent::__construct();
       $this->load->library('bcrypt');
       $this->load->library('form_validation');
      
        
        
    }
    
	public function home()
	{
	
			$this->load->view('common/header_borough');
			
			$this->load->view('borough/home');

			$this->load->view('common/footer_borough');
	
	}

	public function index()
	{
	
			$this->load->view('common/header_borough');
			
			$this->load->view('borough/home');

			$this->load->view('common/footer_borough');
	
	}
	public function step1()
	{
			$this->load->view('common/header_borough');
			
			$this->load->view('borough/step1');

			$this->load->view('common/footer_borough');
	
	}

	public function step1_men()
	{
			$this->load->view('common/header_borough');
			
			$this->load->view('borough/step1_men');

			$this->load->view('common/footer_borough');
	
	}

	public function step2()
	{
			
			
			$this->load->view('borough/step2');

			
	
	}

	public function step3()
	{
			$this->load->view('common/header_borough');
			
			$this->load->view('borough/step3');

			$this->load->view('common/footer_borough');
	
	}

	public function step3_men()
	{
			$this->load->view('common/header_borough');
			
			$this->load->view('borough/step3_men');

			$this->load->view('common/footer_borough');
	
	}

	public function step4()
	{
			$this->load->view('common/header_borough');
			
			$this->load->view('borough/step4');

			$this->load->view('common/footer_borough');
	
	}

	public function step4_men()
	{
			$this->load->view('common/header_borough');
			
			$this->load->view('borough/step4_men');

			$this->load->view('common/footer_borough');
	
	}

	public function step5()
	{	
		
	 if(is_array($_COOKIE) && count($_COOKIE))
	{
      $msg = '<table style="width:600px;border:1px dashed #ccc;border-radius:3px;border-collapse: collapse; " class="table">
				<tr> <th style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">SCBox Package</th> <td style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">'.$_COOKIE['str_package_type'].'</td> <tr>
				<tr> <th style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">SCBox Combo</th> <td style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">'.$_COOKIE['str_package_combotype'].'</td> <tr>
				<tr> <th style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">Name</th> <td style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">'.$_COOKIE['str_name'].'</td> <tr>
				<tr> <th style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">Email</th> <td style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">'.$_COOKIE['str_email'].'</td> <tr>
				<tr> <th style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">Mobile</th> <td style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">'.$_COOKIE['str_mobile'].'</td> <tr>
				<tr> <th style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">Gender</th> <td style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">'.$_COOKIE['str_gender'].'</td> <tr>
				<tr> <th style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">Top Size</th> <td style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">'.$_COOKIE['str_top_size'].'</td> <tr>
				<tr> <th style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">Bottom Size</th> <td style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">'.$_COOKIE['str_bottom_size'].'</td> <tr>
				<tr> <th style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">Footwear Size</th> <td style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">'.$_COOKIE['str_footwear_size'].'</td> <tr>
				<tr> <th style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">Occasion</th> <td style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">'.$_COOKIE['str_occasion'].'</td> <tr><tr> <th style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">Style Preferences</th> <td style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">'.$_COOKIE['arr_style_preferences'].'</td> <tr>';
				
				if($_COOKIE['str_gender'] == 'Women')
				{
					$msg = $msg.'<tr> <th style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">Jewellery Types</th> <td style="width:20%;border: 1px solid #dddddd;text-align: left;padding: 8px;">'.$_COOKIE['arr_jewellery_types'].'</td> <tr>';
				}
			$msg = $msg.'</table>';
	  
	  
	  $config['protocol'] = 'smtp';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');

      $this->email->initialize($config);
	  $name = explode(" ", $_COOKIE['str_name']);
      $this->email->from($_COOKIE['str_email'], $_COOKIE['str_name']);
      $this->email->to('styling@stylecracker.com');
      $this->email->subject('StyleCracker: Borough PA Leads');
      $this->email->message($msg);
		$mobile_text = 'Hi+'.$name[0].',+Sit+back+and+relax,+your+Personal+Stylist+will+get+in+touch+with+you+soon.';
	  file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+91'.$_COOKIE["str_mobile"].'&message='.$mobile_text);
	  
      if($this->email->send())
      {
        $this->load->view('common/header_borough');
		$this->load->view('borough/step5');
		$this->load->view('common/footer_borough');
      }else
      {
        return "Email Error";
      }
	} 
	
			
	
	}
}
