

  <!-- Inline -->
  <style>
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
  </style>

</head>
<body class="site-menubar-fold" data-auto-menubar="false">
  <?php include('common/nav_view.php'); ?>

  <!-- Page -->
  <div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">Store Users</h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li class="active">Store Users</li>
      </ol>
    </div>
    <div class="page-content">
     
      <!-- Panel FixedHeader -->
      <div class="panel">
        <header class="panel-heading">
          <h3 class="panel-title">
            List of Store Users
          </h3>
        </header>
        <div class="panel-body">
          <table class="table table-hover dataTable table-striped width-full" id="exampleFixedHeaderusers">
            <thead>
              <tr>
                 <th>ID</th>
                 <th>Username</th>
                 <th>Company Name</th>
                  <th>Email</th>
                  <th>Registered Date</th>
                  <th>Action</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
      <!-- End Panel FixedHeader -->
    </div>
  </div>
  <!-- End Page -->

<?php include('common/footer.php'); ?>

 
  
  <script>
 

    (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
      });


      // Fixed Header Example
      // --------------------
      (function() {
        // initialize datatable
        var table = $('#exampleFixedHeaderusers').DataTable({
          responsive: true,
          "bPaginate": false,
          "sDom": "lfirtp",
        });

        // initialize FixedHeader
        var offsetTop = 0;
        if ($('.site-navbar').length > 0) {
          offsetTop = $('.site-navbar').eq(0).innerHeight();
        }
        var fixedHeader = new FixedHeader(table, {
          offsetTop: offsetTop
        });

        // redraw fixedHeaders as necessary
        $(window).resize(function() {
          fixedHeader._fnUpdateClones(true);
          fixedHeader._fnUpdatePositions();
        });
      })();

    })(document, window, jQuery);
  </script>

</body>

</html>