

  <!-- Inline -->
  <style>
     .datepair-wrap {
      position: relative;
      overflow: hidden;
    }

    .animation-delay-100 {
      -webkit-animation-delay: 100ms;
      animation-delay: 100ms;
    }
    
    .animation-duration-300 {
      -webkit-animation-duration: 300ms;
      animation-duration: 300ms;
    }
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
    body{
      font-size: 12px !important;
    }
    .label{
      font-size: 84% !important;
    }
    .table>tbody>tr>td{
      padding: 5px !important;
      word-wrap: break-word;
    }
     .lightbox-block {
      max-width: 600px;
      padding: 15px 20px;
      margin: 40px auto;
      overflow: auto;
      background: #fff;
      border-radius: 3px;
    }
    .highlighted {
    background-color: #E5C37E;
}
  .icon {
      font-size: 22px;

    }
    .badge{
      font-size: 10px;
      font-weight: 500;
      border-radius: 3px;
    }
    .cursor{
      cursor: pointer;
    }
    .alertify-cover {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1700;
    background-color: black;
    filter: alpha(opacity=0);
    opacity: 0.7;
} 
  .table a {
  text-decoration: underline;
  color: #57c7d4;
  font-weight: 500;
  cursor: pointer;
}
.step.col-md-4.current, .step.col-md-4.disabled{
  height: 100px;
}
.lightboxsize{
  max-width: 800px !important;
}
.radio-custom label::before {
   
    display:none;
   
}
.img-admin {
    
    width: 24px;
    padding: 4px;
    border:1px dashed #76838f;
}
.img-admin1 {
    
    width: 34px;
    padding: 4px;
    border:1px dashed #76838f;
}
img{
  width: 100%;
}
.table .cell-120 {
    width: 120px;
    display: inline-table;
    
}
@media (min-width: 768px)
{
.form-inline .radio-custom label {
     padding-left: 0px; 
}
}
.radio-custom {
     padding-left: 0px; 
}
.btn-sm{
  float: right;
}

 .new{
  top: -36px !important;
  margin: -8px 1.8em !important; 
 }
  </style>

</head>
<body class="site-menubar-fold" data-auto-menubar="false">
 

<?php include('common/nav_view.php'); ?>

  <div class="page bg-white animsition">

    <!-- Mailbox Content -->
    <div class="page-main">

      <!-- Mailbox Header -->
      <div class="page-header">
        <h1 class="page-title">Manage Box Order</h1>
      </div>

      <!-- Mailbox Content -->
      <div class="page-content">
       <div class="panel">
       
        <!-- Mailbox -->
        <table class="table table-hover dataTable table-striped width-full cursor" id="exampleFixedHeader" align="center">
          <thead>
             <tr align="center">
                <th>Date</th>
                <th>Order ID</th>
                <th>Owner</th>
                <th>Total</th>
                <th>Dis. Price  </th>
                <th>Order Total </th>
                <th>Pmt. Type  </th>
                <th>Name </th>
                <th>Mobile No. </th>
                <th class="cell-100">Status</th>
                <th class="cell-100">Actions</th>
                <th class="cell-100">Pmt. Status</th>
              </tr>
          </thead>
          <tbody class="main_table">
            <!-- <tr data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">
              <td class="cell-60">
                <div class="checkbox-custom checkbox-primary checkbox-lg multi-select">
                  <input type="checkbox" class="mailbox-checkbox" id="mail_1" />
                  <label for="mail_1"></label>
                </div>
              </td>
              <td class="cell-30 responsive-hide">
                <div class="checkbox-important checkbox-default">
                  <input type="checkbox" class="mailbox-checkbox mailbox-important" id="mail_1_important"
                  />
                  <label for="mail_1_important"></label>
                </div>
              </td>
              <td class="cell-60 responsive-hide">
                <a class="avatar" href="javascript:void(0)">
                  <img class="img-responsive" src="<?php echo base_url(); ?>admin_assets/portraits/11.jpg"
                  alt="...">
                </a>
              </td>
              <td>
                <div class="content">
                  <div class="title">Gwendolyn Wheeler</div>
                  <div class="abstract">Genus alteram linguam ut isdem</div>
                </div>
              </td>
              <td class="cell-30 responsive-hide">
                <i class="icon wb-paperclip" aria-hidden="true"></i>
              </td>
              <td class="cell-130">
                <div class="time">1 day ago</div>
                <div class="identity"><i class="wb-medium-point red-600" aria-hidden="true"></i>Work</div>
              </td>
            </tr> -->
            <tr id="cancel" class="step1">
              <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel"  class="cell-60">2017-07-27 09:37:23</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">50236_5236 SC1234567890</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel"><p style="border-bottom: 1px dashed #000;">Stylistname</p></td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel"  class="cell-30">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel"  class="cell-30">3,333.00</td>
                <td  data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel" lass="cell-30">3,333.00</td>
                <td  data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel"  class="cell-30">Online</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/userpanel" data-toggle="slidePanel"><p style="border-bottom: 1px dashed #000;">Customer Name</p>

               
                  </td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel" >9167698562</td>
                <td><span class="badge badge-primary">Call pending</span>
                  
                </td>
                <td align="left"> 
                 
                   <div class="radio-custom radio-primary">
                      <div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="CONFIRM" title=""><img src="<?php echo base_url('admin_assets/images/scicon/edit.png');?>"></div>

                    </div>
                   <div class="radio-custom radio-primary">
                      <div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="CALL" title=""><img src="<?php echo base_url('admin_assets/images/scicon/call.png');?>"></div>
                    </div>
                   <div class="radio-custom radio-primary">
                      <div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="SCHEDULE CALL" title=""><img src="<?php echo base_url('admin_assets/images/scicon/pend.png');?>"></div>
                    </div>
                    <div class="radio-custom radio-primary">
                     <div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="CANCEL" title=""><img src="<?php echo base_url('admin_assets/images/scicon/cancel.png');?>"></div>
                    </div>  
               
                <div style="padding-top: 18px; display: inline-flex;">
                 <!-- Pop with List -->
                  <div><a id="examplePopWithList" href="javascript:void(0)"><div class="img-admin"><img src="<?php echo base_url('admin_assets/images/scicon/comment.png');?>"></div><span class="badge badge-danger up new">1</span></a>
                  <div class="hidden" id="examplePopoverList">
                      <form style="padding: 20px;">
                        <div class="form-group">
                        <label class="control-label" for="textarea">Remarks</label> 
                        <br/>
                        <textarea class="form-control" id="textarea" rows="6"></textarea>
                      </div>
                      <div>
                        <button type="submit" class="btn btn-success btn-sm">Submit</button>
                      </div>
                      </form>
                  </div></div>
                  <div>
                     <a id="examplePopupForm_pa" href="#exampleForm16"><div class="img-admin"><img src="<?php echo base_url('admin_assets/images/scicon/add.png');?>"></div></a>
                <!-- Form Itself -->
                  <div class="mfp-hide lightbox-block lightboxsize" id="exampleForm16">
                     <!-- Panel Wizard Form -->
          <div class="panel" id="exampleWizardForm">
            <div class="panel-heading">
              <h3 class="panel-title">User SCBOX Data</h3>
            </div>
            <div class="panel-body">
              <!-- Steps -->
              <div class="steps steps-sm row"data-plugin="matchHeight" data-by-row="true" role="tablist">
                <div class="step col-md-4 current" data-target="#exampleAccount" role="tab">
                  <span class="step-number">1</span>
                  <div class="step-desc">
                    <span class="step-title">Step1</span>
                    <p>HELP US KNOW YOU BETTER</p>
                  </div>
                </div>

                <div class="step col-md-4" data-target="#exampleBilling" role="tab">
                  <span class="step-number">2</span>
                  <div class="step-desc">
                    <span class="step-title">Step2</span>
                    <p>PA</p>
                  </div>
                </div>

                <div class="step col-md-4" data-target="#exampleGetting" role="tab">
                  <span class="step-number">3</span>
                  <div class="step-desc">
                    <span class="step-title">Step3</span>
                    <p>Optional</p>
                  </div>
                </div>
              </div>
              <!-- End Steps -->

              <!-- Wizard Content -->
              <div class="wizard-content">
                <div class="wizard-pane active" id="exampleAccount" role="tabpanel">
                  <form id="exampleAccountForm">
                    <div class="row">
                      <div class="col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                      <label class="control-label" for="inputFirstName">First Name*</label>
                      <input type="text" class="form-control" id="inputFirstName" name="firstname" required="required">
                    </div>
                      <div class="form-group">
                          <label class="control-label" for="inputMobile">Mobile Number*</label>
                          <input type="text" class="form-control" id="inputMobile" name="mobile" required="required">
                        </div>
                     
                     <div class="form-group">
                      <label class="control-label" for="inputAddress">Address*</label>
                      <textarea class="form-control" id="inputAddress" name="address" required></textarea>
                    </div>
                    <div class="form-group">
                          <div class="radio-custom radio-primary">
                              <input type="radio" id="inputP2999" name="inputpackage" checked />
                              <label for="inputP2999">Rs.2999</label>
                          </div>
                           <div class="radio-custom radio-primary">
                              <input type="radio" id="inputP4999" name="inputpackage" />
                              <label for="inputP4999">Rs.4999</label>
                          </div>
                           <div class="radio-custom radio-primary">
                              <input type="radio" id="inputP6999" name="inputpackage" />
                              <label for="inputP6999">Rs.6999</label>
                          </div>
                           <div class="input-group">
                              <span class="input-group-addon">
                                <span class="radio-custom radio-primary">
                                  <input type="radio" id="inputPCustom" name="inputpackage">
                                  <label for="inputPCustom"></label>
                                </span>
                              </span>
                              <input type="text" class="form-control">
                            </div>
                         </div> 
                      </div>
                      <div class="col-sm-12 col-md-4 col-lg-4">   
                        <div class="form-group">
                          <label class="control-label" for="inputLastName">Last Name*</label>
                          <input type="text" class="form-control" id="inputLastName" name="lastname" required="required">
                        </div>  
                      
                        <div class="form-group">
                          <label class="control-label" for="inputEmail">Email*</label>
                          <input type="email" class="form-control" id="inputEmail" name="email"
                          required="required">
                        </div> 
                       <div class="form-group">
                      <label class="control-label" for="inputLikes">Tell us what you like to wear</label>
                      <textarea class="form-control" id="inputLikes" name="likes"></textarea>
                    </div>
                         
                      </div>
                      <div class="col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                           <label class="control-label" for="inputGender">Gender</label>
                            <select class="form-control" id="inputGender">
                              <option>Female</option>
                              <option>Male</option>
                            </select>
                        </div>
                         <div class="form-group">
                      <label class="control-label" for="inputPinCode">PinCode*</label>
                      <input type="number" class="form-control" id="inputPinCode" name="pincode"
                      required="required">
                    </div>
                     <div class="form-group">
                          <label class="control-label" for="inputDisLikes">Tell us what you wouldn't want us to buy for you</label>
                          <textarea class="form-control" id="inputDisLikes" name="dislikes"></textarea>
                        </div>
                      </div>
                    </div>

                  </form>
                </div>
                <div class="wizard-pane" id="exampleBilling" role="tabpanel">
                  <form id="exampleBillingForm">
                    <div class="row">
                      <div class="col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                     <label class="control-label" for="inputShape">Body Shape*</label>
                      <select class="form-control" id="inputShape" required>
                        <option>Athletic</option>
                        <option>Average</option>
                        <option>Heavy</option>
                        <option>Husky</option>
                        <option>Slim</option>
                      </select>
                    </div>
                    <div class="form-group">
                     <label class="control-label" for="inputSP1">Style Preferences 1*</label>
                      <select class="form-control" id="inputSP1" required>
                        <option>9-5</option>
                        <option>Bold</option>
                        <option>Casual</option>
                        <option>Experimental Casual</option>
                        <option>Suited Up</option>
                        <option>Well Groomed</option>
                      </select>
                    </div>
                    <div class="form-group">
                     <label class="control-label" for="inputSP2">Style Preferences 2*</label>
                      <select class="form-control" id="inputSP2" required>
                        <option>9-5</option>
                        <option>Bold</option>
                        <option>Casual</option>
                        <option>Experimental Casual</option>
                        <option>Suited Up</option>
                        <option>Well Groomed</option>
                      </select>
                    </div>
                    <div class="form-group">
                     <label class="control-label" for="inputSP3">Style Preferences 3*</label>
                      <select class="form-control" id="inputSP3" required>
                        <option>9-5</option>
                        <option>Bold</option>
                        <option>Casual</option>
                        <option>Experimental Casual</option>
                        <option>Suited Up</option>
                        <option>Well Groomed</option>
                      </select>
                    </div>
                    <div class="form-group">
                     <label class="control-label" for="inputST">Skin Tone*</label>
                      <select class="form-control" id="inputST" required>
                        <option>Pale</option>
                        <option>Rosy Pale</option>
                        <option>Light</option>
                        <option>Wheatish</option>
                        <option>tan</option>
                        <option>Medium</option>
                        <option>Dark</option>
                      </select>
                    </div>
                      </div>
                      <div class="col-sm-12 col-md-4 col-lg-4">
                     <div class="form-group">
                     <label class="control-label" for="inputCp">Colour Palette*</label>
                      <select class="form-control" id="inputCp" multiple="" required>
                        <option>Pastel</option>
                        <option>Metallic</option>
                        <option>Neutral</option>
                        <option>Earthy</option>
                        <option>Bright</option>
                        <option>BnW</option>
                      </select>
                    </div>
                     <div class="form-group">
                     <label class="control-label" for="inputPrints">Prints*</label>
                      <select class="form-control" id="inputPrints" multiple="" required>
                        <option>Floral</option>
                        <option>Geometric</option>
                        <option>Abstract</option>
                        <option>Animal</option>
                        <option>No Prints</option>
                      </select>
                    </div>
                     <div class="form-group">
                     <label class="control-label" for="inputSS">Shirt/T-shirt Size*</label>
                      <select class="form-control" id="inputSS" multiple="" required>
                        <option>S</option>
                        <option>M</option>
                        <option>L</option>
                        <option>XL</option>
                        <option>XS</option>
                      </select>
                    </div>
                      </div>
                      <div class="col-sm-12 col-md-4 col-lg-4">
                     <div class="form-group">
                     <label class="control-label" for="inputSS">Waist Size(inch)*</label>
                      <select class="form-control" id="inputSS" multiple="" required>
                        <option>32</option>
                        <option>34</option>
                        <option>36</option>
                        <option>38</option>
                      </select>
                    </div>
                     <div class="form-group">
                     <label class="control-label" for="inputSS">Footwear Size(Ind)*</label>
                      <select class="form-control" id="inputSS" multiple="" required>
                        <option>6</option>
                        <option>6.5</option>
                        <option>7</option>
                        <option>7.5</option>
                      </select>
                    </div>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="wizard-pane" id="exampleGetting" role="tabpanel">
                  <form id="finishform">
                    <div class="row">
                      <div class="col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                          <label class="control-label" for="inputProfession">Profession</label>
                          <input type="text" class="form-control" id="inputProfession" name="profession">
                        </div>
                        <div class="form-group">
                          <label class="control-label" for="inputwardrobe">What does your wardrobe mainly consist of?</label>
                          <textarea class="form-control" id="inputwardrobe" name="wardrobe"></textarea>
                        </div>
                         <div class="form-group">
                          <label class="control-label" for="inputexp">Willingness to experiment?</label>
                          <div class="radio-custom radio-primary">
                              <input type="radio" id="inputyes" name="inputexp" checked />
                              <label for="inputP2999">Yes</label>
                          </div>
                           <div class="radio-custom radio-primary">
                              <input type="radio" id="inputno" name="inputexp" />
                              <label for="inputP4999">No</label>
                          </div>
                         </div> 
                      </div>
                      <div class="col-sm-12 col-md-6 col-lg-6">
                         <div class="form-group">
                          <label class="control-label" for="inputbrandP">Any brand preferences?</label>
                          <textarea class="form-control" id="inputbrandP" name="brandp"></textarea>
                        </div>
                     
                       <div class="form-group">
                          <label class="control-label" for="inputoccasion">Occasion</label>
                          <div class="radio-custom radio-primary">
                              <input type="radio" id="inputOC" name="inputoccasion" checked />
                              <label for="inputOC">Casual</label>
                          </div>
                           <div class="radio-custom radio-primary">
                              <input type="radio" id="inputOW" name="inputoccasion" />
                              <label for="inputOW">Work</label>
                          </div>
                           <div class="radio-custom radio-primary">
                              <input type="radio" id="inputOP" name="inputoccasion" />
                              <label for="inputOP">Party</label>
                          </div>
                           <div class="radio-custom radio-primary">
                              <input type="radio" id="inputOB" name="inputoccasion" />
                              <label for="inputOB">Bridal</label>
                          </div>
                           <div class="input-group">
                              <span class="input-group-addon">
                                <span class="radio-custom radio-primary">
                                  <input type="radio" id="inputOPS" name="inputoccasion">
                                  <label for="inputOPS"></label>
                                </span>
                              </span>
                              <input type="text" class="form-control">
                            </div>
                         </div> 
                          </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- End Wizard Content -->

            </div>
          </div>
          <!-- End Panel Wizard One Form -->
                  </div>
                  <!-- End Form -->
                  </div>
                </div>  
                  <!-- End Pop with List --> 
                </td>
                <td align="right"><!-- Example Popup With Form -->
                  <a class="btn btn-danger btn-outline btn-xs" id="examplePopupForm" href="#exampleForm">Pending&nbsp;&nbsp;<span class="badge" style="border-left: 1px solid #f96868;border-radius: 0px;padding: 2px;background-color:#f96868; "><img src="<?php echo base_url('admin_assets/images/scicon/pay.png');?>" style="width: 16px;"></span></a>
                  <!-- Form Itself -->
                  <form class="mfp-hide lightbox-block" id="exampleForm">
                    <h1>Payment Details</h1>
                    <div class="row">
                     <div class="col-sm-4 col-md-4">
                     <div class="form-group">
                    <label class="control-label">Payment Date</label>
                      <input type="text" class="form-control" id="inputDate2" data-plugin="formatter"
                      data-pattern="[[99]]/[[99]]/[[9999]]" />
                      <p class="help-block">24/03/1994</p>
                </div></div>
                <div class="col-sm-4 col-md-4">
                    <div class="form-group">
                      <label class="control-label" for="inputName">Transaction ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="Transaction ID"
                      required>
                    </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                   <div class="form-group">
                      <label class="control-label" for="inputName">PAYU ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="PAYU ID"
                      required>
                    </div>
                    </div>
                   </div>
                    <div class="row">
                    
                     <div class="col-sm-4 col-md-4">
                    <div class="form-group">
                      <label class="control-label" for="inputName">Payment Price</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="Paid Price"
                      required>
                    </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                    <div class="form-group">
                      <label class="control-label">Payment Status</label>
                      <select class="form-control">
                        <option>Select</option>
                        <option>Done</option>
                        <option>Not Done</option>
                      </select>
                     </div></div>
                        <div class="col-sm-4 col-md-4">
                     <div class="form-group">
                      <label class="control-label">Payment Mode</label>
                      <select class="form-control">
                        <option>Select</option>
                        <option>Online</option>
                        <option>Cash</option>
                      </select>
                     </div></div>
                     </div>
                    
                     <div class="row">
                        <div class="col-sm-4 col-md-4">
                     <div class="form-group">
                      <label class="control-label">Payment Notes</label>
                      <textarea></textarea>
                     </div></div></div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-success btn-sm">Submit</button>
                    </div>



                  </form>
                  <!-- End Form --></td>
            </tr>
             <!-- data-url="<?php //echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel" -->
            <tr id="cancel">
              <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel" class="cell-60">2017-07-27 09:37:23</td>
                <td>50236_5236 SC1234567890</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">Stylistname</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">Online</td>
                <td class="cell-60">Nikita Kale</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">9167698562</td>
                <td><span class="badge badge-warning">Call Scheduled</span><br><br><br>
                   
                </td>
                <td align="left">
              <form>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step1" id="step1" value="0" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="Are you sure you want to do Confirm the order?"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel"><label for="step1"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="CONFIRM" title=""><img src="<?php echo base_url('admin_assets/images/scicon/edit.png');?>"></div></label>

                    </div>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step1" id="step11" value="1" class="load_more"><label for="step11"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="CALL" title=""><img src="<?php echo base_url('admin_assets/images/scicon/call.png');?>"></div></label>
                    </div>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step4" id="stepcall" value="1" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="Are you sure you want to proceed with the call?"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel"><label for="stepcall"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="SCHEDULE CALL" title=""><img src="<?php echo base_url('admin_assets/images/scicon/pend.png');?>"></div></label>
                    </div>
                    <div class="radio-custom radio-primary">
                      <input type="radio" name="step1" id="target" value="3" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="Are you sure you want to do this?"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel"><label for="target"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="CANCEL" title=""><img src="<?php echo base_url('admin_assets/images/scicon/cancel.png');?>"></div></label>
                    </div>  
                </form>
                </td>
                <td class="cell-30"  align="right"><!-- Example Popup With Form -->
                  <a class="btn btn-success btn-outline btn-xs" id="examplePopupForm1" href="#exampleForm1">Paid</a>
                  <!-- Form Itself -->
                  <form class="mfp-hide lightbox-block" id="exampleForm1">
                    <h1>Payment Details</h1>
                    <div class="form-group">
                      <label class="control-label" for="inputName">Transaction ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" value="TRNID123456" placeholder="Transaction ID"
                      readonly>
                    </div>
                   <div class="form-group">
                      <label class="control-label" for="inputName">PAYU ID</label>
                      <input type="text" class="form-control" id="inputName" value="PAYU123456" name="name" placeholder="PAYU ID"
                      readonly>
                    </div>
                  </form>
                  <!-- End Form --></td>
            </tr>
            <tr id="cancel">
              <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel" class="cell-60">2017-07-27 09:37:23</td>
                <td>50236_5236 SC1234567890</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">Stylistname</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">Online</td>
                <td>Nikita Kale</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">9167698562</td>
                <td><span class="badge badge-warning">Call <br> Rescheduled</span><br><br><br>
                  
                </td>
                <td align="left">
                     <form>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step1" id="step1" value="0" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="Are you sure you want to do Confirm the order?"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel"><label for="step1"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="CONFIRM" title=""><img src="<?php echo base_url('admin_assets/images/scicon/edit.png');?>"></div></label>

                    </div>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step1" id="step11" value="1" class="load_more"><label for="step11"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="CALL" title=""><img src="<?php echo base_url('admin_assets/images/scicon/call.png');?>"></div></label>
                    </div>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step4" id="stepcall" value="1" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="Are you sure you want to proceed with the call?"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel"><label for="stepcall"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="SCHEDULE CALL" title=""><img src="<?php echo base_url('admin_assets/images/scicon/pend.png');?>"></div></label>
                    </div>
                    <div class="radio-custom radio-primary">
                      <input type="radio" name="step1" id="target" value="3" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="Are you sure you want to do this?"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel"><label for="target"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="CANCEL" title=""><img src="<?php echo base_url('admin_assets/images/scicon/cancel.png');?>"></div></label>
                    </div>  
                </form>
                </td>
                <td align="right"><!-- Example Popup With Form -->
                  <a class="btn btn-success btn-outline btn-xs" id="examplePopupForm2" href="#exampleForm">Paid</a>
                  <!-- Form Itself -->
                  <form class="mfp-hide lightbox-block" id="exampleForm">
                    <h1>Payment Details</h1>
                    <div class="form-group">
                      <label class="control-label" for="inputName">Transaction ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="Transaction ID"
                      required>
                    </div>
                   <div class="form-group">
                      <label class="control-label" for="inputName">PAYU ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="PAYU ID"
                      required>
                    </div>
                  </form>
                  <!-- End Form --></td>
            </tr>

            <tr>
              <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">2017-07-27 09:37:23</td>
                <td>50236_5236 SC1234567890</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">Stylistname</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">Online</td>
                <td>Nikita Kale</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">9167698562</td>
                <td><span class="badge badge-success">Product<br>Selection</span><br><br><br>
                  
                </td>
                <td align="left">
                   <form>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step4" id="step4" value="1" class="load_more"><label for="step4"><p data-url="<?php echo base_url(); ?>admin/manage_stylistorder/userpanel" data-toggle="slidePanel" class="cell-60"><div class="img-admin1 tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="PLACE ORDER" title=""><img src="<?php echo base_url('admin_assets/images/scicon/gift.png');?>"></div></p></label>
                    </div>
                   </div>
                  </form> 
                </td>
                <td class="cell-30"  align="right"><!-- Example Popup With Form -->
                  <a class="btn btn-success btn-outline btn-xs" id="examplePopupForm3" href="#exampleForm">Paid</a>
                  <!-- Form Itself -->
                  <form class="mfp-hide lightbox-block" id="exampleForm">
                    <h1>Payment Details</h1>
                    <div class="form-group">
                      <label class="control-label" for="inputName">Transaction ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="Transaction ID"
                      required>
                    </div>
                   <div class="form-group">
                      <label class="control-label" for="inputName">PAYU ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="PAYU ID"
                      required>
                    </div>
                  </form>
                  <!-- End Form --></td>
            </tr>
             <tr >
              <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">2017-07-27 09:37:23</td>
                <td>50236_5236 SC1234567890</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">Stylistname</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">Online</td>
                <td>Nikita Kale</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">9167698562</td>
                <td><span class="badge badge-warning">Pending from <br>Brands</span><br><br><br>
                  
                </td>
                <td class="load_more"  align="left">
                  <!--  <form>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step5" id="step5" value="1" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="Are you sure you want to do this?"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel" class="load_more"><label for="1">Ready for Packing</label>
                    </div>
                   </div>
                  </form>  -->
                </td>
                <td align="right"><!-- Example Popup With Form -->
                  <a class="btn btn-success btn-outline btn-xs" id="examplePopupForm12" href="#exampleForm">Paid</a>
                  <!-- Form Itself -->
                  <form class="mfp-hide lightbox-block" id="exampleForm">
                    <h1>Payment Details</h1>
                    <div class="form-group">
                      <label class="control-label" for="inputName">Transaction ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="Transaction ID"
                      required>
                    </div>
                   <div class="form-group">
                      <label class="control-label" for="inputName">PAYU ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="PAYU ID"
                      required>
                    </div>
                  </form>
                  <!-- End Form --></td>
            </tr>
             <tr>
              <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel" class="cell-60">2017-07-27 09:37:23</td>
                <td>50236_5236 SC1234567890</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">Stylistname</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">Online</td>
                <td>Nikita Kale</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">9167698562</td>
                <td><span class="badge badge-primary">Ready to Ship</span><br><br><br>
                  
                </td>
                <td align="left">
                   <form>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step6" id="step6" value="1" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="Are you sure you want to do this?"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel" class="load_more"><label for="step6"><div class="img-admin1 tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="SHIPPED" title=""><img src="<?php echo base_url('admin_assets/images/scicon/ship.png');?>"></div></label>
                    </div>
                   </div>
                  </form> 
                </td>
                <td align="right"><!-- Example Popup With Form -->
                  <a class="btn btn-success btn-outline btn-xs" id="examplePopupForm13" href="#exampleForm">Paid</a>
                  <!-- Form Itself -->
                  <form class="mfp-hide lightbox-block" id="exampleForm">
                    <h1>Payment Details</h1>
                    <div class="form-group">
                      <label class="control-label" for="inputName">Transaction ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="Transaction ID"
                      required>
                    </div>
                   <div class="form-group">
                      <label class="control-label" for="inputName">PAYU ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="PAYU ID"
                      required>
                    </div>
                  </form>
                  <!-- End Form --></td>
            </tr>
           <tr>
              <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">2017-07-27 09:37:23</td>
                <td>50236_5236 SC1234567890</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">Stylistname</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">Online</td>
                <td>Nikita Kale</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">9167698562</td>
                <td><span class="badge badge-primary">In Transit</span><br><br><br>
                   
                </td>
                <td align="left">
                   <form>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step7" id="step7" value="1" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="Are you sure you want to do this?"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel" class="load_more"><label for="step7"><div class="img-admin1 tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="DELIVERED" title=""><img src="<?php echo base_url('admin_assets/images/scicon/delivery.png');?>"></div></label>
                    </div>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step7" id="step72" value="1" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="Are you sure you want to do this?"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel"><label for="step72"><div class="img-admin1 tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="DELIVERY REJECTED" title=""><img src="<?php echo base_url('admin_assets/images/scicon/return.png');?>"></div></label>
                    </div> 
                   </div>
                  </form> 
                </td>
                <td align="right"><!-- Example Popup With Form -->
                  <a class="btn btn-success btn-outline btn-xs" id="examplePopupForm4" href="#exampleForm">Paid</a>
                  <!-- Form Itself -->
                  <form class="mfp-hide lightbox-block" id="exampleForm">
                    <h1>Payment Details</h1>
                    <div class="form-group">
                      <label class="control-label" for="inputName">Transaction ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="Transaction ID"
                      required>
                    </div>
                   <div class="form-group">
                      <label class="control-label" for="inputName">PAYU ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="PAYU ID"
                      required>
                    </div>
                  </form>
                  <!-- End Form --></td>
            </tr>
            <tr>
              <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">2017-07-27 09:37:23</td>
                <td>50236_5236 SC1234567890</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">Stylistname</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">Online</td>
                <td>Nikita Kale</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">9167698562</td>
                <td><span class="badge badge-success">Delivered</span><br><br>
                <span class="badge badge-danger">Exchange <br> period active</span><br><br><br>
                  
                </td>
                <td align="left">
                   
                      <a class="btn btn-primary btn-outline btn-xs" id="examplePopWithList1" href="javascript:void(0)">Add Feedback</a><br><br>
                      <div class="hidden" id="examplePopoverList13">
                      <form style="padding: 20px;">
                        <div class="form-group">
                        <label class="control-label" for="textarea">Remarks</label> 
                        <br/>
                        <textarea class="form-control" id="textarea" rows="6"></textarea>
                      </div>
                      <div>
                        <button type="submit" class="btn btn-success btn-sm">Submit</button>
                      </div>
                      </form>
                    </div>
                    
                   <form>  
                    <div class="radio-custom radio-primary">
                      <input type="radio" name="step9" id="step10" value="1" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="Are you sure you want to do this?"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel" class="load_more"><label for="step10"><div class="img-admin1 tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="RETURN/EXCHANGE ACTIVE" title=""><img src="<?php echo base_url('admin_assets/images/scicon/shuffle.png');?>"></div></label>
                    </div>
                  </form> 
                </td>
                <td align="right"><!-- Example Popup With Form -->
                  <a class="btn btn-success btn-outline btn-xs" id="examplePopupForm5" href="#exampleForm">Paid</a>
                  <!-- Form Itself -->
                  <form class="mfp-hide lightbox-block" id="exampleForm">
                    <h1>Payment Details</h1>
                    <div class="form-group">
                      <label class="control-label" for="inputName">Transaction ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="Transaction ID"
                      required>
                    </div>
                   <div class="form-group">
                      <label class="control-label" for="inputName">PAYU ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="PAYU ID"
                      required>
                    </div>
                  </form>
                  <!-- End Form --></td>
            </tr>
            <tr>
              <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">2017-07-27 09:37:23</td>
                <td>50236_5236 SC1234567890</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">Stylistname</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">Online</td>
                <td>Nikita Kale</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">9167698562</td>
                <td><span class="badge badge-danger">Return /<br>Exchange</span><br><br><br>
                  
                </td>
                <td align="left">
                   <form>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step112" id="step112" value="1" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="Are you sure you want to do this?"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel" class="load_more"><label for="step112"><div class="img-admin1 tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="RETURN IN TRANSIT" title=""><img src="<?php echo base_url('admin_assets/images/scicon/pickup.png');?>"></div></label>
                    </div>
                   </div>
                  </form> 
                </td>
                <td align="right"><!-- Example Popup With Form -->
                  <a class="btn btn-success btn-outline btn-xs" id="examplePopupForm6" href="#exampleForm1">Paid</a>
                  <!-- Form Itself -->
                  <form class="mfp-hide lightbox-block" id="exampleForm1">
                    <h1>Payment Details</h1>
                    <div class="form-group">
                      <label class="control-label" for="inputName">Transaction ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="Transaction ID"
                      required>
                    </div>
                   <div class="form-group">
                      <label class="control-label" for="inputName">PAYU ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="PAYU ID"
                      required>
                    </div>
                  </form>
                  <!-- End Form --></td>
            </tr>
            <tr>
              <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">2017-07-27 09:37:23</td>
                <td>50236_5236 SC1234567890</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">Stylistname</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">Online</td>
                <td class="cell-60">Nikita Kale</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">9167698562</td>
                <td><span class="badge badge-danger">Return in <br>Transit</span><br><br><br>
                  
                </td>
                <td align="left">
                   <form>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step12" id="step12" value="1" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="Are you sure you want to do this?"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel" class="load_more"><label for="step12"><div class="img-admin1 tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="PACKAGE RETURNED" title=""><img src="<?php echo base_url('admin_assets/images/scicon/returned.png');?>"></div></label>
                    </div>
                   </div>
                  </form> 
                </td>
                <td align="right"><!-- Example Popup With Form -->
                  <a class="btn btn-success btn-outline btn-xs" id="examplePopupForm7" href="#exampleForm1">Paid</a>
                  <!-- Form Itself -->
                  <form class="mfp-hide lightbox-block" id="exampleForm1">
                    <h1>Payment Details</h1>
                    <div class="form-group">
                      <label class="control-label" for="inputName">Transaction ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="Transaction ID"
                      required>
                    </div>
                   <div class="form-group">
                      <label class="control-label" for="inputName">PAYU ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="PAYU ID"
                      required>
                    </div>
                  </form>
                  <!-- End Form --></td>
            </tr>
            <tr>
              <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">2017-07-27 09:37:23</td>
                <td>50236_5236 SC1234567890</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">Stylistname</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">3,333.00</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">Online</td>
                <td>Nikita Kale</td>
                <td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">9167698562</td>
                <td><span class="badge badge-danger">Closed</span><br><br><br>
                  
                </td>
                <td align="left">
                  <!--  <form>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step10" id="step10" value="1" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="Are you sure you want to do this?"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel" class="load_more"><label for="1">Return to Transit</label>
                    </div>
                   </div>
                  </form>  -->
                </td>
                <td align="right"><!-- Example Popup With Form -->
                  <a class="btn btn-success btn-outline btn-xs" id="examplePopupForm8" href="#exampleForm1">Paid</a>
                  <!-- Form Itself -->
                  <form class="mfp-hide lightbox-block" id="exampleForm1">
                    <h1>Payment Details</h1>
                    <div class="form-group">
                      <label class="control-label" for="inputName">Transaction ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="Transaction ID"
                      required>
                    </div>
                   <div class="form-group">
                      <label class="control-label" for="inputName">PAYU ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="PAYU ID"
                      required>
                    </div>
                  </form>
                  <!-- End Form --></td>
            </tr>

          </tbody>
        </table>
      </div>
     
        
      </div>
    </div>
  </div>

  <!-- App Action -->
  <!-- <div class="site-action">
    <button type="button" class="btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-pencil animation-scale-up" aria-hidden="true"></i>
      <i class="back-icon wb-close animation-scale-up" aria-hidden="true"></i>
    </button>
    <div class="site-action-buttons">
      <button type="button" class="btn-raised btn btn-success btn-floating animation-slide-bottom animation-delay-100">
        <i class="icon wb-trash" aria-hidden="true"></i>
      </button>
      <button type="button" class="btn-raised btn btn-success btn-floating animation-slide-bottom">
        <i class="icon wb-inbox" aria-hidden="true"></i>
      </button>
    </div>
  </div> -->

  <!-- Modal -->



  <?php include('common/footer.php'); ?>

   <script>

   $(function(){
    var currP = 0;
var rowNP = $('.main_table_panel tr').length;

$('.main_table_panel tr').eq(currP).siblings().hide(); // hide all but current one

$('.load_more_panel').on('click',function(e){
    e.preventDefault(); // prevent default anchor behavior
    
    ++currP;
    if(currP===rowNP){currP=0;} // reset to 0 at some point
        
    $('.main_table_panel tr').eq(currP).show().siblings().hide();
});
   });

   /*$(function(){
    var curr = 0;
var rowN = $('.main_table tr').length;

$('.main_table tr').eq(curr).siblings().hide(); // hide all but current one

$('.load_more').on('click',function(e){
    e.preventDefault(); // prevent default anchor behavior
    
    ++curr;
    if(curr===rowN){curr=0;} // reset to 0 at some point
        
    $('.main_table tr').eq(curr).show().siblings().hide();
});
   });*/
        
     $("#step11,#step22,#step33").click(function (event) {
        alertify.prompt('Input (text):').set('type', 'datetime-local')
     });

   $("#target").click(function (event) {
    $("#cancel").slideUp("slow", function () {
        $(this).replaceWith('<tr align="center">' +
              '<td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel" class="cell-60">2017-07-27 09:37:23</td>' +
                '<td class="cell-60">50236_5236 SC1234567890</td>' +
                '<td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel" class="cell-80">Stylistname</td>'+'<td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel" class="cell-30">3,333.00</td>'+
                '<td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel" class="cell-30">3,333.00</td>'+
                '<td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel" class="cell-30">3,333.00</td>'+
                '<td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel" class="cell-30">Online</td>'+
                '<td class="cell-60">Nikita Kale</td>'+
                '<td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">9167698562</td>'+
               '<td class="cell-60"><span class="badge badge-danger">Cancelled</span><br><br><br>'+
                   
                   '<a id="examplePopWithList1" href="javascript:void(0)"><div class="img-admin"><img src="<?php echo base_url('admin_assets/images/scicon/comment.png');?>"></div><span class="badge badge-danger up new">1</span></a>'+
                  '<div class="hidden" id="examplePopoverList11">'+
                      '<form style="padding: 20px;">'+
                        '<div class="form-group">'+
                        '<label class="control-label" for="textarea">Remarks</label>'+ 
                        '<br/>'+
                        '<textarea class="form-control" id="textarea" rows="6"></textarea>'+
                      '</div>'+
                      '<div>'+' <button type="submit" class="btn btn-success btn-sm">Submit</button>'+
                      '</div>'+
                      '</form>'+
                 '</div>'+
                '</td>'+
                '<td class="cell-120"  align="left">'+
                 
               ' </td>'+
                '<td class="cell-30"  align="right">'+
                  '<a class="btn btn-success btn-outline btn-xs" id="examplePopupForm9" href="#exampleForm">Paid</a>'+
                '</td>'+
            '</tr>');
             })
    event.preventDefault();
});



    $("#step1").click(function (event) {
    $(".step1").slideUp("slow", function () {
        $(this).replaceWith('<tr align="center">' +
              '<td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel" class="cell-60">2017-07-27 09:37:23</td>' +
                '<td class="cell-60">50236_5236 SC1234567890</td>' +
                '<td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel" class="cell-80">Stylistname</td>'+'<td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel" class="cell-30">3,333.00</td>'+
                '<td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel" class="cell-30">3,333.00</td>'+
                '<td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel" class="cell-30">3,333.00</td>'+
                '<td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel" class="cell-30">Online</td>'+
                '<td class="cell-60">Nikita Kale</td>'+
                '<td data-url="<?php echo base_url(); ?>admin/manage_stylistorder/panel" data-toggle="slidePanel">9167698562</td>'+
               '<td class="cell-60"><span class="badge badge-success">Product<br>Selection</span><br><br><br>'+
                   
                '</td>'+
                '<td class="cell-120"  align="left">'+
                  '<form>'+
                   '<div class="radio-custom radio-primary">'+
                      '<input type="radio" name="step4" id="step4" value="1" data-plugin="alertify" data-type="confirm" data-confirm-title="Are you sure you want to do this?" data-success-message="You ve clicked OK" data-error-message="You ve clicked Cancel" class="load_more"><label for="1">Place Order</label></div></form>'+ 
                '</td><td class="cell-30"  align="right"><a class="btn btn-success btn-outline btn-xs" id="examplePopupForm10" href="#exampleForm1">Paid</a><form class="mfp-hide lightbox-block" id="exampleForm1"><h1>Payment Details</h1><div class="form-group"><label class="control-label" for="inputName">Transaction ID</label><input type="text" class="form-control" id="inputName" name="name" placeholder="Transaction ID" required></div> <div class="form-group"><label class="control-label" for="inputName">PAYU ID</label><input type="text" class="form-control" id="inputName" name="name" placeholder="PAYU ID" required></div></form>'+
               ' </td>'+
            '</tr>');
             })
    event.preventDefault();
});






    $(function() {
    $('td').click(function() {
        $('tr').removeClass('active');
        $(this).parent().addClass('active'); 
    });
});

    (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
      });





     (function(){
  $("tr").click(function() {
    $(this).closest("tr").siblings().removeClass("highlighted");
    $(this).toggleClass("highlighted");
  })
});  
       var defaults = $.components.getDefaults("webuiPopover");
      // Example Webui Popover Pop with List
      // -----------------------------------
      (function() {
        var listContent = $('#examplePopoverList,#examplePopoverList1,#examplePopoverList2,#examplePopoverList3,#examplePopoverList4,#examplePopoverList5,#examplePopoverList6,#examplePopoverList7,#examplePopoverList8,#examplePopoverList8,#examplePopoverList9,#examplePopoverList10,#examplePopoverList11,#examplePopoverList12,#examplePopoverList13').html(),
          listSettings = {
            content: listContent,
            title: '',
            padding: false
          };

        $('#examplePopWithList,#examplePopWithList1').webuiPopover($.extend({}, defaults,
          listSettings));

       /* $('input[name=step1]').change(function(){
        $('form').submit();
        });*/

      })();
     
       // Example Popup With Video Rr Map
      // -------------------------------
     
      $('#examplePopupForm,#examplePopupForm_pa,#examplePopupForm_paside,#examplePopupForm_datepair').magnificPopup({
        type: 'inline',
        preloader: false,
       // focus: '#inputName',

        // When elemened is focused, some mobile browsers in some cases zoom in
        // It looks not nice, so we disable it:
        callbacks: {
          beforeOpen: function() {
            if ($(window).width() < 700) {
              this.st.focus = false;
            } else {
             // this.st.focus = '#inputName';
            }
          }
        }
      });
   
      // Example Popup With Video Rr Map
      // -------------------------------
      $('#examplePopupForm1,#examplePopupForm2,#examplePopupForm3,#examplePopupForm4,#examplePopupForm5,#examplePopupForm6,#examplePopupForm7,#examplePopupForm8,#examplePopupForm9,#examplePopupForm10,#examplePopupForm13,#examplePopupForm12,#examplePopupForm11,#examplePopupForm17').magnificPopup({
        type: 'inline',
        preloader: false,
       // focus: '#inputName',

        // When elemened is focused, some mobile browsers in some cases zoom in
        // It looks not nice, so we disable it:
        callbacks: {
          beforeOpen: function() {
            if ($(window).width() < 700) {
              this.st.focus = false;
            } else {
             // this.st.focus = '#inputName';
            }
          }
        }
      });

      // Fixed Header Example
      // --------------------
      (function() {
        // initialize datatable
        var table = $('#exampleFixedHeader').DataTable({
          responsive: true,
          "bPaginate": false,
          "sDom": "lfirtp",
        });

        // initialize FixedHeader
        var offsetTop = 0;
        if ($('.site-navbar').length > 0) {
          offsetTop = $('.site-navbar').eq(0).innerHeight();
        }
        var fixedHeader = new FixedHeader(table, {
          offsetTop: offsetTop
        });

        // redraw fixedHeaders as necessary
        $(window).resize(function() {
          fixedHeader._fnUpdateClones(true);
          fixedHeader._fnUpdatePositions();
        });
      })();

      // Individual column searching
      // ---------------------------
      (function() {
        $(document).ready(function() {
          var defaults = $.components.getDefaults("dataTable");

          var options = $.extend(true, {}, defaults, {
            initComplete: function() {
              this.api().columns().every(function() {
                var column = this;
                var select = $(
                    '<select class="form-control width-full"><option value=""></option></select>'
                  )
                  .appendTo($(column.footer()).empty())
                  .on('change', function() {
                    var val = $.fn.dataTable.util.escapeRegex(
                      $(this).val()
                    );

                    column
                      .search(val ? '^' + val + '$' : '',
                        true, false)
                      .draw();
                  });

                column.data().unique().sort().each(function(
                  d, j) {
                  select.append('<option value="' + d +
                    '">' + d + '</option>')
                });
              });
            }
          });

          $('#exampleTableSearch').DataTable(options);
        });
      })();

      // Table Tools
      // -----------
      (function() {
        $(document).ready(function() {
          var defaults = $.components.getDefaults("dataTable");

          var options = $.extend(true, {}, defaults, {
            "aoColumnDefs": [{
              'bSortable': false,
              'aTargets': [-1]
            }],
            "iDisplayLength": 5,
            "aLengthMenu": [
              [5, 10, 25, 50, -1],
              [5, 10, 25, 50, "All"]
            ],
            "sDom": '<"dt-panelmenu clearfix"Tfr>t<"dt-panelfooter clearfix"ip>',
            "oTableTools": {
              "sSwfPath": "<?php echo base_url(); ?>admin_assets/vendor/datatables-tabletools/swf/copy_csv_xls_pdf.swf"
            }
          });

          $('#exampleTableTools').dataTable(options);
        });
      })();


      // Table Add Row
      // -------------

      (function() {
        $(document).ready(function() {
          var defaults = $.components.getDefaults("dataTable");

          var t = $('#exampleTableAdd').DataTable(defaults);

          $('#exampleTableAddBtn').on('click', function() {
            t.row.add([
              'Adam Doe',
              'New Row',
              'New Row',
              '30',
              '2015/10/15',
              '$20000'
            ]).draw();
          });
        });
      })();


      // Example Wizard Form
      // -------------------
      (function() {
        // set up formvalidation
        $('#exampleAccountForm').formValidation({
          framework: 'bootstrap',
          fields: {
            username: {
              validators: {
                notEmpty: {
                  message: 'The username is required'
                },
                stringLength: {
                  min: 6,
                  max: 30,
                  message: 'The username must be more than 6 and less than 30 characters long'
                },
                regexp: {
                  regexp: /^[a-zA-Z0-9_\.]+$/,
                  message: 'The username can only consist of alphabetical, number, dot and underscore'
                }
              }
            },
            password: {
              validators: {
                notEmpty: {
                  message: 'The password is required'
                },
                different: {
                  field: 'username',
                  message: 'The password cannot be the same as username'
                }
              }
            }
          }
        });

        $("#exampleBillingForm").formValidation({
          framework: 'bootstrap',
          fields: {
            number: {
              validators: {
                notEmpty: {
                  message: 'The credit card number is required'
                }
                // creditCard: {
                //   message: 'The credit card number is not valid'
                // }
              }
            },
            cvv: {
              validators: {
                notEmpty: {
                  message: 'The CVV number is required'
                }
                // cvv: {
                //   creditCardField: 'number',
                //   message: 'The CVV number is not valid'
                // }
              }
            }
          }
        });
        $("#finishform").formValidation({
          framework: 'bootstrap',
          fields: {
            number: {
              validators: {
                notEmpty: {
                  message: 'The credit card number is required'
                }
                // creditCard: {
                //   message: 'The credit card number is not valid'
                // }
              }
            },
            cvv: {
              validators: {
                notEmpty: {
                  message: 'The CVV number is required'
                }
                // cvv: {
                //   creditCardField: 'number',
                //   message: 'The CVV number is not valid'
                // }
              }
            }
          }
        });

        // init the wizard
        var defaults = $.components.getDefaults("wizard");
        var options = $.extend(true, {}, defaults, {
          buttonsAppendTo: '.panel-body'
        });

        var wizard = $("#exampleWizardForm").wizard(options).data(
          'wizard');

        // setup validator
        // http://formvalidation.io/api/#is-valid
        wizard.get("#exampleAccount").setValidator(function() {
          var fv = $("#exampleAccountForm").data('formValidation');
          fv.validate();

          if (!fv.isValid()) {
            return false;
          }

          return true;
        });

        wizard.get("#exampleBilling").setValidator(function() {
          var fv = $("#exampleBillingForm").data('formValidation');
          fv.validate();

          if (!fv.isValid()) {
            return false;
          }

          return true;
        });

        wizard.get("#exampleGetting").setValidator(function() {
          var fv = $("#finishform").data('formValidation');
          fv.validate();

          if (!fv.isValid()) {
            return false;
          }

          return true;
        });
      })();

    })(document, window, jQuery);
  </script>
