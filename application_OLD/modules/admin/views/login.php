
  <!-- Page -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/css/pages/login.css">
  <style type="text/css">
    .site-footer{
      display: none;
    }
  </style>
</head>
<body class="page-login layout-full">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->


  <!-- Page -->
  <div class="page animsition vertical-align text-center" data-animsition-in="fade-in"
  data-animsition-out="fade-out">
    <div class="page-content vertical-align-middle">
      <div class="brand">
        <img class="brand-img" src="<?php echo base_url(); ?>admin_assets/images/logo-small.jpg">
        <h2 class="brand-text"> StyleCracker <strong>Admin</strong></h2>
      </div>
      <p>Sign in to start your session</p>
       <!-- <?php echo $this->session->flashdata('feedback'); ?> -->
      <?php if(isset($error)) { echo $error; } ?>
	  <?php 
              if(!empty($this->session->flashdata('success')))
                {
                  ?>
                  <h3 class="btn btn-block btn-success"><?php echo $this->session->flashdata('success');?></h3>
                <?php
                }
          ?>
      <form action="<?php echo base_url(); ?>admin/login/check_login" method="post">
        <!-- <div class="form-group">
          <label class="sr-only" for="inputName">Name</label>
          <input type="text" class="form-control" id="inputName" placeholder="Name">
        </div> -->
        <div class="form-group">
          <label class="sr-only" for="inputEmail">Email</label>
           <input type="email" name="email" id="email" class="form-control" placeholder="Email"/>
           <div class="error"><?php form_error('email'); ?></div>
        </div>
        <div class="form-group">
          <label class="sr-only" for="inputPassword">Password</label>
         <input type="password" name="password" id="password" class="form-control" placeholder="Password"/>
          <div class="error"><?php echo form_error('password'); ?></div>
        </div>
        <div class="form-group clearfix">
          <div class="checkbox-custom checkbox-inline pull-left">
            <input type="checkbox" id="inputCheckbox" name="checkbox">
            <label for="inputCheckbox">Remember me</label>
          </div>
          <a class="pull-right" href="<?php echo base_url('admin/login/forgotPassword');?>">Forgot password?</a>
        </div>
        <button type="submit" class="btn btn-primary btn-block">Sign in</button>
      </form>
      <p>Still no account? Please go to <a href="register.html">Register</a></p>

      <footer class="page-copyright">
        <p>WEBSITE BY Tech Team</p>
        <p>© 2017. All RIGHT RESERVED.</p>
       <!--  <div class="social">
          <a href="javascript:void(0)">
            <i class="icon bd-twitter" aria-hidden="true"></i>
          </a>
          <a href="javascript:void(0)">
            <i class="icon bd-facebook" aria-hidden="true"></i>
          </a>
          <a href="javascript:void(0)">
            <i class="icon bd-dribbble" aria-hidden="true"></i>
          </a>
        </div> -->
      </footer>
    </div>

  <!-- End Page -->

      <?php include('common/footer.php'); ?>
<script type="text/javascript">

      (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
      });

    

       })(document, window, jQuery);
    </script>