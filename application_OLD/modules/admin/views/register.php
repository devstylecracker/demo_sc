
  <!-- Page -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/css/pages/register.css">
</head>
<body class="page-register layout-full">


  <!-- Page -->
  <div class="page animsition vertical-align text-center" data-animsition-in="fade-in"
  data-animsition-out="fade-out">
    <div class="page-content vertical-align-middle">
      <div class="brand">
        <img class="brand-img" src="<?php echo base_url(); ?>admin_assets/images/logo.png" alt="...">
        <h2 class="brand-text">Remark</h2>
      </div>
      <p>Sign up to find interesting thing</p>
      <form method="post" role="form">
        <div class="form-group">
          <label class="sr-only" for="inputName">Name</label>
          <input type="text" class="form-control" id="inputName" placeholder="Name">
        </div>
        <div class="form-group">
          <label class="sr-only" for="inputEmail">Email</label>
          <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Email">
        </div>
        <div class="form-group">
          <label class="sr-only" for="inputPassword">Password</label>
          <input type="password" class="form-control" id="inputPassword" name="password"
          placeholder="Password">
        </div>
        <div class="form-group">
          <label class="sr-only" for="inputPasswordCheck">Retype Password</label>
          <input type="password" class="form-control" id="inputPasswordCheck" name="passwordCheck"
          placeholder="Confirm Password">
        </div>
        <button type="submit" class="btn btn-primary btn-block">Register</button>
      </form>
      <p>Have account already? Please go to <a href="login.html">Sign In</a></p>

      <footer class="page-copyright">
        <p>WEBSITE BY amazingSurge</p>
        <p>© 2015. All RIGHT RESERVED.</p>
        <div class="social">
          <a href="javascript:void(0)">
            <i class="icon bd-twitter" aria-hidden="true"></i>
          </a>
          <a href="javascript:void(0)">
            <i class="icon bd-facebook" aria-hidden="true"></i>
          </a>
          <a href="javascript:void(0)">
            <i class="icon bd-dribbble" aria-hidden="true"></i>
          </a>
        </div>
      </footer>
    </div>
  </div>
  <!-- End Page -->


      <?php include('common/footer.php'); ?>
<script type="text/javascript">

      (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
      });

    

       })(document, window, jQuery);
    </script>