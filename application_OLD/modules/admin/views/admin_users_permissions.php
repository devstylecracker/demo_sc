

  <!-- Inline -->
  <style>
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }

    .control-label{
      font-weight: 500;
    }
  </style>
</head>  
<body>
  <?php include('common/nav_view.php'); ?>

  <!-- Page -->
  <div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">User Permissions</h1>
     <!--  <ol class="breadcrumb">
        <li><a href="">Home</a></li>
        <li><a href="javascript:void(0)">Forms</a></li>
        <li class="active">Material</li>
      </ol> -->
    </div>
    <div class="page-content container-fluid">
      <!-- Panel Jquery Multi Select -->
      <div class="panel">
       <!--  <div class="panel-heading">
          <h3 class="panel-title">User Permissions
            <small><a class="example-plugin-link" href="http://loudev.com/" target="_blank">official website</a></small>
          </h3>
        </div> -->
        <div class="panel-body container-fluid">
          <div class="row row-lg">
            <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Category
                    <span class="required">*</span>
                  </label>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="view" required="">
                          <label for="inputForProject">View</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForWebsite" name="for[]" value="add">
                          <label for="inputForWebsite">Add</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForAll" name="for[]" value="update">
                          <label for="inputForAll">Update</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForAll" name="for[]" value="delete">
                          <label for="inputForAll">Delete</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>

           <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Sub Category
                    <span class="required">*</span>
                  </label>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="view" required="">
                          <label for="inputForProject">View</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForWebsite" name="for[]" value="add">
                          <label for="inputForWebsite">Add</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForAll" name="for[]" value="update">
                          <label for="inputForAll">Update</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForAll" name="for[]" value="delete">
                          <label for="inputForAll">Delete</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Admin Users
                    <span class="required">*</span>
                  </label>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="view" required="">
                          <label for="inputForProject">View</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForWebsite" name="for[]" value="add">
                          <label for="inputForWebsite">Add</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForAll" name="for[]" value="update">
                          <label for="inputForAll">Update</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForAll" name="for[]" value="delete">
                          <label for="inputForAll">Delete</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>

             <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Variation Type
                    <span class="required">*</span>
                  </label>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="view" required="">
                          <label for="inputForProject">View</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForWebsite" name="for[]" value="add">
                          <label for="inputForWebsite">Add</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForAll" name="for[]" value="update">
                          <label for="inputForAll">Update</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForAll" name="for[]" value="delete">
                          <label for="inputForAll">Delete</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Variation
                    <span class="required">*</span>
                  </label>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="view" required="">
                          <label for="inputForProject">View</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForWebsite" name="for[]" value="add">
                          <label for="inputForWebsite">Add</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForAll" name="for[]" value="update">
                          <label for="inputForAll">Update</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForAll" name="for[]" value="delete">
                          <label for="inputForAll">Delete</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Tag
                    <span class="required">*</span>
                  </label>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="view" required="">
                          <label for="inputForProject">View</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForWebsite" name="for[]" value="add">
                          <label for="inputForWebsite">Add</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForAll" name="for[]" value="update">
                          <label for="inputForAll">Update</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForAll" name="for[]" value="delete">
                          <label for="inputForAll">Delete</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Product
                    <span class="required">*</span>
                  </label>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="view" required="">
                          <label for="inputForProject">View</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForWebsite" name="for[]" value="add">
                          <label for="inputForWebsite">Add</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForAll" name="for[]" value="update">
                          <label for="inputForAll">Update</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForAll" name="for[]" value="delete">
                          <label for="inputForAll">Delete</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForAll" name="for[]" value="approval">
                          <label for="inputForAll">Approval</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
           
             <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Brand Users
                    <span class="required">*</span>
                  </label>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="view" required="">
                          <label for="inputForProject">View</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForWebsite" name="for[]" value="update">
                          <label for="inputForWebsite">Update Business Profile</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForAll" name="for[]" value="update">
                          <label for="inputForAll">Update Contact Profile</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForAll" name="for[]" value="delete">
                          <label for="inputForAll">Delete</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForAll" name="for[]" value="view">
                          <label for="inputForAll">View Store</label>
                        </div>
                      </div>
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForAll" name="for[]" value="add">
                          <label for="inputForAll">Add Store</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
           
            <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Others
                    <span class="required">*</span>
                  </label>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="send" required="">
                          <label for="inputForProject">Send Sms</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="create" required="">
                          <label for="inputForProject">Create Event</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="manage" required="">
                          <label for="inputForProject">Manage Event</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="image" required="">
                          <label for="inputForProject">Collection/event image upload</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="create" required="">
                          <label for="inputForProject">Create Collection</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="manage" required="">
                          <label for="inputForProject">Manage Collection</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="event" required="">
                          <label for="inputForProject">Event</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="collection" required="">
                          <label for="inputForProject">Collection</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="discount" required="">
                          <label for="inputForProject">Discount Setting</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">SCbox
                    <span class="required">*</span>
                  </label>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="manage" required="">
                          <label for="inputForProject">Manage</label>
                        </div>
                      </div>
                    </div>
                  </div>
                   <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="add" required="">
                          <label for="inputForProject">Add</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
             <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Manage
                    <span class="required">*</span>
                  </label>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="page_settings" required="">
                          <label for="inputForProject">Homepage UI Settings</label>
                        </div>
                      </div>
                    </div>
                  </div>
                   <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="ui_settings" required="">
                          <label for="inputForProject">Brand Page Settings </label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Manage
                    <span class="required">*</span>
                  </label>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="orders" required="">
                          <label for="inputForProject">Orders</label>
                        </div>
                      </div>
                    </div>
                  </div>
                   <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="order_payments" required="">
                          <label for="inputForProject">Order Payments</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Look
                    <span class="required">*</span>
                  </label>
                   <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="creator" required="">
                          <label for="inputForProject">Creator</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Delivery Process
                    <span class="required">*</span>
                  </label>
                   <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="management" required="">
                          <label for="inputForProject">Management</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
             <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Referral
                    <span class="required">*</span>
                  </label>
                   <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="setting" required="">
                          <label for="inputForProject">Setting</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
             <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Product
                    <span class="required">*</span>
                  </label>
                   <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="logic" required="">
                          <label for="inputForProject">Personalisation Logic</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Cache
                    <span class="required">*</span>
                  </label>
                   <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="management" required="">
                          <label for="inputForProject">Management</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">User Enquiry
                    <span class="required">*</span>
                  </label>
                   <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="view" required="">
                          <label for="inputForProject">View</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Feedback
                    <span class="required">*</span>
                  </label>
                   <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="view" required="">
                          <label for="inputForProject">View</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Databases
                    <span class="required">*</span>
                  </label>
                   <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="all" required="">
                          <label for="inputForProject">All Access</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Upload
                    <span class="required">*</span>
                  </label>
                   <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="stickers" required="">
                          <label for="inputForProject">Stickers</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Store
                    <span class="required">*</span>
                  </label>
                   <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="settings" required="">
                          <label for="inputForProject">Settings</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Product Inventory
                    <span class="required">*</span>
                  </label>
                   <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="view" required="">
                          <label for="inputForProject">Creator</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
             <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Role
                    <span class="required">*</span>
                  </label>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="management" required="">
                          <label for="inputForProject">Management</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
             <div class="col-md-3">
              <div class="form-group">
                  <label class="col-lg-12 col-sm-3 control-label">Affiliate Users
                    <span class="required">*</span>
                  </label>
                  <div class="col-lg-12 col-sm-9">
                    <div class="input-group">
                      <div>
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="inputForProject" name="for[]" value="manage" required="">
                          <label for="inputForProject">Manage Orders</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>

          </div>
        </div>
      </div>
      <!-- End Panel Jquery Multi Select -->
    </div>
  </div>
  <!-- End Page -->

 <?php include('common/footer.php'); ?>
<script type="text/javascript">

      (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
      });

       })(document, window, jQuery);
    </script>



     
