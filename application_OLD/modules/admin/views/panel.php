<style type="text/css">
   .slidePanel-right, .slidePanel-left {
    width: 900px;
    height: 100%;
     height: -webkit-calc(100% - 66px); 
    height: calc(100% - 66px); 
     margin-top: 66px; 
}
.modal-backdrop {
    
    z-index: 999;
   
}
.table>tbody>tr>td{
  word-wrap: break-word;
}

.btn-pure.btn-inverse{
  color: #000;
}
</style>
</head>
<header class="slidePanel-header">
  <div class="slidePanel-actions" aria-label="actions" role="group">
    <button type="button" class="btn btn-icon btn-pure btn-inverse slidePanel-close actions-top icon wb-close"
    aria-hidden="true"></button>
   <!--  <div class="btn-group actions-bottom" role="group">
      <div class="pull-left" style="position:relative;">
        <button type="button" class="btn btn-icon btn-pure btn-inverse dropdown-toggle icon wb-more-horizontal"
        data-toggle="dropdown" aria-expanded="false" aria-hidden="true"></button>
        <ul class="dropdown-menu dropdown-menu-right" role="menu" style="color: #76838f">
          <li>Platform: Android</li>
          <li>UTM-source: Fb</li>
          <li>UTM-medium:Fb-Web-Ads</li>
          <li>UTM-campaign: 2353WpunBaby</li>
        </ul>
      </div>
       <button type="button" class="btn btn-icon btn-pure btn-inverse icon wb-chevron-left"
      aria-hidden="true"></button>
      <button type="button" class="btn btn-icon btn-pure btn-inverse icon wb-chevron-right"
      aria-hidden="true"></button> 
    </div> -->
  </div>
  <h1>SCBOX Product Details</h1>
  <h1>SC123456789147</h1>
  <div class="row">
    <div class="col-sm-12 col-md-6 col-lg-6">
       <div>
        <span class="name">Nikita Kale</span>
        </div>
        <div>xyz,lorem epsum lorem epsum lorem epsum,loreum-400025.
        </div>
         <div>6985214569
        </div>
    </div>
     <div class="col-sm-12 col-md-6 col-lg-6">
        <ul style="list-style: none;">
          <li>Platform: Android</li>
          <li>UTM-source: Fb</li>
          <li>UTM-medium: Fb-Web-Ads</li>
          <li>UTM-campaign: 2353WpunBaby</li>
        </ul>
    </div>
  </div>
 
</header>
<div class="slidePanel-inner">
  <section class="slidePanel-inner-section">
    <!-- <div class="mail-header">
      <div class="mail-header-main">
        <div>
          <span class="name">Nikita Kale</span>
        </div>
        <div>xyz,lorem epsum lorem epsum lorem epsum,loreum-400025.
        </div>
         <div>6985214569
        </div>
      </div>
      <div class="mail-header-right">
        <div>
          Platform: Android
        </div>
        <div>
           UTM-source: Fb
        </div>
         <div>
           UTM-medium:Fb-Web-Ads
        </div>
         <div>
          UTM-campaign: 2353WpunBaby
            
        </div>

      </div>
    </div> -->
    <div class="mail-content">
      <table class="table table-hover dataTable table-striped width-full" id="FixedHeader">
          <thead>
             <tr align="center" style="background-color: rgba(19,215,146, .1);">
                <th>Order Date</th>
                <th>AWP</th>
                <th class="cell-100">Order ID</th>
                <th class="cell-80">SKU</th>
                <th>Image</th>
                <th>Brand</th>
                <th>Name</th>
                <th>Size</th>
                <th>Prdt. Price</th>
                <th>Disc. Price </th>
                <th>Dvy. Date</th>
                <th>Status</th>
                <th align="right" class="cell-80">Actions</th>
             </tr>
          </thead>
          <tbody class="main_table_panel">
            <tr align="center" class="stepp1">
                <td>2017-07-27</td>
                <td>123456789</td>
                <td>50236_5236 SC1234567890</td>
                <td>SCBOX1904848500</td>
                <td><img src="https://www.stylecracker.com/sc_admin/admin_assets/product_images/normal/14755855544341.jpg" style="width:40px;"></td>
                <td>Only Two Feet</td>
                <td><a data-target="#exampleGrid" data-toggle="modal">High heels in black</a>
                  <!-- Example Grid In Modal -->            
                 
                  <!-- Modal -->
                  <div class="modal fade" id="exampleGrid" aria-hidden="true" aria-labelledby="exampleGrid"
                  role="dialog" tabindex="-1">
                    <div class="modal-dialog" style="width: 850px;">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title">Product Details</h4>
                        </div>
                        <div class="modal-body">
                        <div class="row">
                          <div class="col-sm-12 col-md-6 col-lg-6">
                          <div class="example-grid">
                            <table class="table table-bordered">
                              <tr>
                                <td>Name</td>
                                <td>5 products 6-men</td>
                              </tr>
                              <tr>
                                <td>Description</td>
                                <td>  1 apparel, shoes, Bag, Accessories or Cosmetics</td>
                              </tr>
                              <tr>
                                <td>Store</td>
                                <td>StyleCrackerStore</td>
                              </tr>
                              <tr>
                                <td>Category</td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>Sub Category</td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>Product URL</td>
                                <td>http://localhost/projectbitb/sc-box</td>
                              </tr>
                               <tr>
                                <td>Tags</td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>Price Range</td>
                                <td>under 15000</td>
                              </tr>
                              <tr>
                                <td>PriceL</td>
                                <td>6,333</td>
                              </tr>
                               <tr>
                                <td>Target Market</td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>Consumer Type</td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>Brand</td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>Rating</td>
                                <td>0</td>
                              </tr>
                            </table>  
                          </div>
                          </div> 

                          <div class="col-sm-12 col-md-6 col-lg-6">
                          <div class="example-grid">
                             <table class="table table-bordered">
                              <tr>
                                <td>Image</td>
                                <td><img src="http://localhost/projectbitb/sc_admin/admin_assets/products/thumb_124/1496826572641.png" width="120"></td>
                              </tr>
                              <tr>
                                <td>Body Part</td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>Body Shape</td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>Body Type</td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>Personality</td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>Age Group</td>
                                <td></td>
                               <tr>
                                <td>Promotional</td>
                                <td>No</td>
                              </tr>
                              <tr>
                                <td>Active</td>
                                <td>Yes</td>
                              </tr>
                            </table>  
                          </div>
                          </div> 

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- End Modal -->
              
              
              <!-- End Example Grid In Modal -->  </td>
                <td>39</td>
                <td>2,500.00</td>
                <td>2,000.00</td>
             
                <td></td>
                <td><span class="label label-primary">Pending</span><br><br><br>
                  
                </td>
                 <td>
                   <form>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="stepp1" id="stepp1" value="1" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="This is a confirm dialog"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel" class="load_more_panel"><label for="stepp1"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="CONFIRM" title=""><img src="<?php echo base_url('admin_assets/images/scicon/edit.png');?>"></div></label>
                    </div>
                    <div class="radio-custom radio-primary">
                      <input type="radio" name="stepp2" id="stepp2" value="2" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="This is a confirm dialog"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel" class="load_more_panel"><label for="stepp2"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="CANCEL" title=""><img src="<?php echo base_url('admin_assets/images/scicon/cancel.png');?>"></div></label>
                    </div>
                  </form> 
                </td>
            </tr>
             <tr align="center" class="stepp2">
                <td>2017-07-27</td>
                <td>123456789</td>
                <td>50236_5236 SC1234567890</td>
                <td>SCBOX1904848500</td>
                <td><img src="https://www.stylecracker.com/sc_admin/admin_assets/product_images/normal/14755855544341.jpg" style="width:40px;"></td>
                <td>Only Two Feet</td>
                <td>High heels in black</td>
                <td>39</td>
                <td>2,500.00</td>
                <td>2,000.00</td>
                 
                <td></td>
                <td><span class="label label-success">Confirmed</span><br><br><br>
                  
                </td>
                 <td>
                   <form>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="stepp13" id="stepp13" value="1" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="This is a confirm dialog"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel" class="load_more_panel"><label for="stepp13"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="PRODUCT RECEIVED" title=""><img src="<?php echo base_url('admin_assets/images/scicon/delivery.png');?>"></div></label>
                    </div>
                     <div class="radio-custom radio-primary">
                      <input type="radio" name="stepp14" id="stepp14" value="1" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="This is a confirm dialog"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel" class="load_more_panel"><label for="stepp14"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="RETURN PRODUCT" title=""><img src="<?php echo base_url('admin_assets/images/scicon/return.png');?>"></div></label>
                    </div>
                  </form> 
                </td>
            </tr>
            
             <tr align="center" class="stepp4">
                <td>2017-07-27</td>
                <td>123456789</td>
                <td>50236_5236 SC1234567890</td>
                <td>SCBOX1904848500</td>
                <td><img src="https://www.stylecracker.com/sc_admin/admin_assets/product_images/normal/14755855544341.jpg" style="width:40px;"></td>
                <td>Only Two Feet</td>
                <td>High heels in black</td>
                <td>39</td>
                <td>2,500.00</td>
                <td>2,000.00</td>
                
                <td></td>
                <td><span class="label label-danger">Returned</span><br><br><br>
                  
                </td>
                 <td>
                    <form>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step41" id="step41" value="1" class="load_more_panel" data-plugin="alertify"
                  data-alert-message="You are about to place an order for the box products"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel"><label for="step41"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="PLACE ORDER" title=""><img src="<?php echo base_url('admin_assets/images/scicon/gift.png');?>"></div></label>
                    </div>
                   </div>
                  </form> 
                </td>
            </tr>

            <tr align="center" class="stepp5">
               <td>2017-07-27</td>
                <td>123456789</td>
                <td>50236_5236 SC1234567890</td>
                <td>SCBOX1904848500</td>
                <td><img src="https://www.stylecracker.com/sc_admin/admin_assets/product_images/normal/14755855544341.jpg" style="width:40px;"></td>
                <td>Only Two Feet</td>
                <td>High heels in black</td>
                <td>39</td>
                <td>2,500.00</td>
                <td>2,000.00</td>
               
                <td>2017-07-30</td>
                <td><span class="label label-success">Received</span><br><br><br>
                  
                </td>
                 <td>
                  <!--  <form>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step1" id="" value="1" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="This is a confirm dialog"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel"><label for="1">Received</label>
                    </div>
                  </form>  -->
                </td>
            </tr>
            
          </tbody>
        </table>
    </div>
    </div>
  </section>
  </div>
</div>
<script>
  

    (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
      });

      (function(){
    var currP = 0;
var rowNP = $('.main_table_panel tr').length;

$('.main_table_panel tr').eq(currP).siblings().hide(); // hide all but current one

$('.load_more_panel').on('click',function(e){
    e.preventDefault(); // prevent default anchor behavior
    
    ++currP;
    if(currP===rowNP){currP=0;} // reset to 0 at some point
        
    $('.main_table_panel tr').eq(currP).show().siblings().hide();
});
   });

      // Fixed Header Example
      // --------------------
      (function() {
        // initialize datatable
        var table = $('#FixedHeader').DataTable({
          responsive: true,
          "bPaginate": false,
          "sDom": "lfirtp",
        });

      })();
      var defaults = $.components.getDefaults("webuiPopover");
      (function() {
        var listContent = $('#examplePopoverList2').html(),
          listSettings = {
            content: listContent,
            title: '',
            padding: false
          };

        $('#examplePopWithList2').webuiPopover($.extend({}, defaults,
          listSettings));

       /* $('input[name=step1]').change(function(){
        $('form').submit();
        });*/

      })();
    })(document, window, jQuery);
  </script>
