
   <script type="text/javascript">
     function genarate_slug(){
        var category_name = $('#category_name').val();
        var slug = category_name.replace(/[^a-zA-Z0-9]/g, "-");
        $('#category_slug').val(slug.toLowerCase());
      }
   </script>
   <script>

    (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
      });

      // Fixed Header Example
      // --------------------
      (function() {
        // initialize datatable
        var table = $('#table_admin,#exampleFixedHeaderusers,#exampleFixedHeader,#FixedHeader').DataTable({
          responsive: true,
          "bPaginate": false,
          "sDom": "lfirtp",
        });

        // initialize FixedHeader
        var offsetTop = 0;
        if ($('.site-navbar').length > 0) {
          offsetTop = $('.site-navbar').eq(0).innerHeight();
        }
        var fixedHeader = new FixedHeader(table, {
          offsetTop: offsetTop
        });

        // redraw fixedHeaders as necessary
        $(window).resize(function() {
          fixedHeader._fnUpdateClones(true);
          fixedHeader._fnUpdatePositions();
        });
      })();


      

      var defaults = $.components.getDefaults("webuiPopover");
      // Example Webui Popover Pop with List
      // -----------------------------------
      (function() {
        var listContent = $('#examplePopoverList').html(),
          listSettings = {
            content: listContent,
            title: '',
            padding: false
          };

        $('#examplePopWithList').webuiPopover($.extend({}, defaults,
          listSettings));

       /* $('input[name=step1]').change(function(){
        $('form').submit();
        });*/

      })();

      (function() {
        var listContent = $('#examplePopoverList,#examplePopoverList1,#examplePopoverList2,#examplePopoverList3,#examplePopoverList4,#examplePopoverList5,#examplePopoverList6,#examplePopoverList7,#examplePopoverList8,#examplePopoverList8,#examplePopoverList9,#examplePopoverList10,#examplePopoverList11,#examplePopoverList12,#examplePopoverList13').html(),
          listSettings = {
            content: listContent,
            title: '',
            padding: false
          };

        $('#examplePopWithList,#examplePopWithList1').webuiPopover($.extend({}, defaults,
          listSettings));

       /* $('input[name=step1]').change(function(){
        $('form').submit();
        });*/

      })();
     
       // Example Popup With Video Rr Map
      // -------------------------------
     
      $('#examplePopupForm,#examplePopupForm_pa,#examplePopupForm_paside,#examplePopupForm_datepair').magnificPopup({
        type: 'inline',
        preloader: false,
       // focus: '#inputName',

        // When elemened is focused, some mobile browsers in some cases zoom in
        // It looks not nice, so we disable it:
        callbacks: {
          beforeOpen: function() {
            if ($(window).width() < 700) {
              this.st.focus = false;
            } else {
             // this.st.focus = '#inputName';
            }
          }
        }
      });
    
     // Example Wizard Form
      // -------------------
      (function() {
        // set up formvalidation
        $('#exampleAccountForm').formValidation({
          framework: 'bootstrap',
          fields: {
            username: {
              validators: {
                notEmpty: {
                  message: 'The username is required'
                },
                stringLength: {
                  min: 6,
                  max: 30,
                  message: 'The username must be more than 6 and less than 30 characters long'
                },
                regexp: {
                  regexp: /^[a-zA-Z0-9_\.]+$/,
                  message: 'The username can only consist of alphabetical, number, dot and underscore'
                }
              }
            },
            password: {
              validators: {
                notEmpty: {
                  message: 'The password is required'
                },
                different: {
                  field: 'username',
                  message: 'The password cannot be the same as username'
                }
              }
            }
          }
        });

        $("#exampleBillingForm").formValidation({
          framework: 'bootstrap',
          fields: {
            number: {
              validators: {
                notEmpty: {
                  message: 'The credit card number is required'
                }
                // creditCard: {
                //   message: 'The credit card number is not valid'
                // }
              }
            },
            cvv: {
              validators: {
                notEmpty: {
                  message: 'The CVV number is required'
                }
                // cvv: {
                //   creditCardField: 'number',
                //   message: 'The CVV number is not valid'
                // }
              }
            }
          }
        });
        $("#finishform").formValidation({
          framework: 'bootstrap',
          fields: {
            number: {
              validators: {
                notEmpty: {
                  message: 'The credit card number is required'
                }
                // creditCard: {
                //   message: 'The credit card number is not valid'
                // }
              }
            },
            cvv: {
              validators: {
                notEmpty: {
                  message: 'The CVV number is required'
                }
                // cvv: {
                //   creditCardField: 'number',
                //   message: 'The CVV number is not valid'
                // }
              }
            }
          }
        });

        // init the wizard
        var defaults = $.components.getDefaults("wizard");
        var options = $.extend(true, {}, defaults, {
          buttonsAppendTo: '.panel-body'
        });

        var wizard = $("#exampleWizardForm").wizard(options).data(
          'wizard');

        // setup validator
        // http://formvalidation.io/api/#is-valid
        wizard.get("#exampleAccount").setValidator(function() {
          var fv = $("#exampleAccountForm").data('formValidation');
          fv.validate();

          if (!fv.isValid()) {
            return false;
          }

          return true;
        });

        wizard.get("#exampleBilling").setValidator(function() {
          var fv = $("#exampleBillingForm").data('formValidation');
          fv.validate();

          if (!fv.isValid()) {
            return false;
          }

          return true;
        });

        wizard.get("#exampleGetting").setValidator(function() {
          var fv = $("#finishform").data('formValidation');
          fv.validate();

          if (!fv.isValid()) {
            return false;
          }

          return true;
        });
      })();

      // Example Popup With Video Rr Map
      // -------------------------------
      $('#examplePopupForm1,#examplePopupForm2,#examplePopupForm3,#examplePopupForm4,#examplePopupForm5,#examplePopupForm6,#examplePopupForm7,#examplePopupForm8,#examplePopupForm9,#examplePopupForm10,#examplePopupForm13,#examplePopupForm12,#examplePopupForm11,#examplePopupForm17').magnificPopup({
        type: 'inline',
        preloader: false,
       // focus: '#inputName',

        // When elemened is focused, some mobile browsers in some cases zoom in
        // It looks not nice, so we disable it:
        callbacks: {
          beforeOpen: function() {
            if ($(window).width() < 700) {
              this.st.focus = false;
            } else {
             // this.st.focus = '#inputName';
            }
          }
        }
      });
     
      (function() {
        var listContent = $('#examplePopoverList1').html(),
          listSettings = {
            content: listContent,
            title: '',
            padding: false
          };

        $('#examplePopWithList1').webuiPopover($.extend({}, defaults,
          listSettings));

       /* $('input[name=step1]').change(function(){
        $('form').submit();
        });*/

      })();
      // Example Popup With Video Rr Map
      // -------------------------------
      $('#examplePopupForm').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#inputName',

        // When elemened is focused, some mobile browsers in some cases zoom in
        // It looks not nice, so we disable it:
        callbacks: {
          beforeOpen: function() {
            if ($(window).width() < 700) {
              this.st.focus = false;
            } else {
              this.st.focus = '#inputName';
            }
          }
        }
      });
      // Example Popup With Video Rr Map
      // -------------------------------
      $('#examplePopupForm1').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#inputName',

        // When elemened is focused, some mobile browsers in some cases zoom in
        // It looks not nice, so we disable it:
        callbacks: {
          beforeOpen: function() {
            if ($(window).width() < 700) {
              this.st.focus = false;
            } else {
              this.st.focus = '#inputName';
            }
          }
        }
      });

      // Example Multi-Select
      // --------------------
      (function() {
        // for multi-select public methods example
        $('.multi-select-methods').multiSelect();
        $('#buttonSelectAll').click(function() {
          $('.multi-select-methods').multiSelect('select_all');
          return false;
        });
        $('#buttonDeselectAll').click(function() {
          $('.multi-select-methods').multiSelect('deselect_all');
          return false;
        });
        $('#buttonSelectSome').click(function() {
          $('.multi-select-methods').multiSelect('select', ['Idaho',
            'Montana', 'Arkansas'
          ]);
          return false;
        });
        $('#buttonDeselectSome').click(function() {
          $('.multi-select-methods').multiSelect('select', ['Idaho',
            'Montana', 'Arkansas'
          ]);
          return false;
        });
        $('#buttonRefresh').on('click', function() {
          $('.multi-select-methods').multiSelect('refresh');
          return false;
        });
        $('#buttonAdd').on('click', function() {
          $('.multi-select-methods').multiSelect('addOption', {
            value: 42,
            text: 'test 42',
            index: 0
          });
          return false;
        });
      })();
	
 





    })(document, window, jQuery);
  </script>

</body>

</html>
