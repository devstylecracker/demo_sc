  
  </div>
  <!-- End Page -->

   <!-- Footer -->
  <footer class="site-footer">
    <span class="site-footer-legal">© 2017 Stylecracker</span>
    <div class="site-footer-right">
      Crafted with <i class="red-600 wb wb-heart"></i> by <a href="http://themeforest.net/user/amazingSurge">Techteam</a>
    </div>
  </footer>

  <!-- Core  -->
  <script src="<?php echo base_url(); ?>admin_assets/vendor/jquery/jquery.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/bootstrap/bootstrap.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/animsition/jquery.animsition.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/asscroll/jquery-asScroll.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/asscrollable/jquery.asScrollable.all.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>

  <!-- Plugin -->
  <script src="<?php echo base_url(); ?>admin_assets/vendor/switchery/switchery.min.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/intro-js/intro.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/screenfull/screenfull.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/slidepanel/jquery-slidePanel.js"></script>

  <script src="<?php echo base_url(); ?>admin_assets/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/datatables-fixedheader/dataTables.fixedHeader.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/datatables-responsive/dataTables.responsive.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/datatables-tabletools/dataTables.tableTools.js"></script>

   <script src="<?php echo base_url(); ?>admin_assets/vendor/x-editable/bootstrap-editable.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/typeahead-js/bloodhound.min.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/typeahead-js/typeahead.jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/x-editable/address.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/moment/moment.min.js"></script>
  
  <script src="<?php echo base_url(); ?>admin_assets/vendor/jquery-ui/jquery-ui.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/blueimp-tmpl/tmpl.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/blueimp-canvas-to-blob/canvas-to-blob.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/blueimp-load-image/load-image.all.min.js"></script>

  <script src="<?php echo base_url(); ?>admin_assets/vendor/blueimp-file-upload/jquery.fileupload.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/blueimp-file-upload/jquery.fileupload-process.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/blueimp-file-upload/jquery.fileupload-image.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/blueimp-file-upload/jquery.fileupload-audio.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/blueimp-file-upload/jquery.fileupload-video.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/blueimp-file-upload/jquery.fileupload-validate.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/blueimp-file-upload/jquery.fileupload-ui.js"></script>
  
  <script src="<?php echo base_url(); ?>admin_assets/vendor/alertify-js/alertify.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/formatter-js/jquery.formatter.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/select2/select2.min.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/bootstrap-markdown/bootstrap-markdown.js"></script>
  
  <script src="<?php echo base_url(); ?>admin_assets/vendor/marked/marked.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/to-markdown/to-markdown.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/aspaginator/jquery.asPaginator.min.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/magnific-popup/jquery.magnific-popup.js"></script>
   <script src="<?php echo base_url(); ?>admin_assets/vendor/datepair-js/datepair.min.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/datepair-js/jquery.datepair.min.js"></script>


  <script src="<?php echo base_url(); ?>admin_assets/vendor/formvalidation/formValidation.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/formvalidation/framework/bootstrap.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/chartist-js/chartist.min.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/aspieprogress/jquery-asPieProgress.min.js"></script>
 
  <script src="<?php echo base_url(); ?>admin_assets/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/jquery-wizard/jquery-wizard.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/webui-popover/jquery.webui-popover.min.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/datepair-js/datepair.min.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/datepair-js/jquery.datepair.min.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/multi-select/jquery.multi-select.js"></script>

   <!-- Scripts -->
  <script src="<?php echo base_url(); ?>admin_assets/js/core.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/js/site.js"></script>

  <script src="<?php echo base_url(); ?>admin_assets/js/sections/menu.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/js/sections/menubar.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/js/sections/sidebar.js"></script>


  <script src="<?php echo base_url(); ?>admin_assets/js/configs/config-colors.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/js/configs/config-tour.js"></script>

  <script src="<?php echo base_url(); ?>admin_assets/js/components/asscrollable.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/js/components/animsition.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/js/components/slidepanel.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/js/components/switchery.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/js/components/datatables.js"></script>

  <script src="<?php echo base_url(); ?>admin_assets/js/apps/app.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/js/apps/mailbox.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/js/components/select2.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/js/components/webui-popover.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/js/components/alertify-js.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/js/components/magnific-popup.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/js/components/formatter-js.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/js/custom.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/js/components/datepair-js.js"></script>
   <script src="<?php echo base_url(); ?>admin_assets/js/components/jquery-wizard.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/js/components/matchheight.js"></script> 
  <script src="<?php echo base_url(); ?>admin_assets/js/components/multi-select.js"></script>
  
</body>

</html>
