<!DOCTYPE html>
<html class="no-js before-run" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">

  <title></title>

  <link rel="apple-touch-icon" href="<?php echo base_url(); ?>admin_assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="<?php echo base_url(); ?>admin_assets/images/favicon.ico">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/css/bootstrap-extend.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/css/site.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/flag-icon-css/flag-icon.css">

   <!-- Plugin -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/datatables-bootstrap/dataTables.bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/datatables-responsive/dataTables.responsive.css">
   <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/x-editable/x-editable.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/typeahead-js/typeahead.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/select2/select2.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/bootstrap-markdown/bootstrap-markdown.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/select2/select2.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/webui-popover/webui-popover.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/alertify-js/alertify.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/magnific-popup/magnific-popup.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/jquery-wizard/jquery-wizard.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/formvalidation/formValidation.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/jquery-strength/jquery-strength.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/multi-select/multi-select.css">

  <!-- Fonts -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/fonts/web-icons/web-icons.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/fonts/font-awesome/font-awesome.css">

  <!-- Scripts -->
  <script src="<?php echo base_url(); ?>admin_assets/vendor/modernizr/modernizr.js"></script>
  <script src="<?php echo base_url(); ?>admin_assets/vendor/breakpoints/breakpoints.js"></script>
  <script>
    Breakpoints();
  </script>




