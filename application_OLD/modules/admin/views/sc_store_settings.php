

  <!-- Inline -->
  <style>
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
  </style>

  
</head>
<body class="site-menubar-fold" data-auto-menubar="false">
  <?php include('common/nav_view.php'); ?>

  <!-- Page -->
  <div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">SC Store Settings</h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li class="active">SC Store Settings</li>
      </ol>
    </div>
   <div class="page-content">
      <!-- Panel X-Editable -->
      <div class="panel">
      	<header class="panel-heading">
          <h3 class="panel-title">
            SC Store Settings
            <span class="panel-desc">Click to edit.</span>
          </h3>
        </header>
        <div class="panel-body">
          <button class="btn btn-primary btn-outline margin-bottom-10" id="editableEnable"
          type="button">enable / disable</button>
          <div class="table-responsive row">
	          <div class="col-lg-6 col-md-6 col-sm-12">	
	            <table class="table table-bordered table-striped" id="editableUser">
	              <tbody>
	              	 <tr>
	                  <td>Settings</td>
	                  <td>
	                    <a id="settings" href="javascript:void(0)" data-type="checklist" data-value="2,3"
	                    data-title="Select Settings"></a>
	                  </td>
	                </tr>
	                <tr>
	                  <td>On-board Date</td>
	                  <td>
	                    <a id="on_date" href="javascript:void(0)" data-type="combodate" data-value="1984-05-15"
	                    data-format="YYYY-MM-DD" data-viewformat="DD/MM/YYYY" data-template="D / MMM / YYYY"
	                    data-pk="1" data-title="Select On-board Date"></a>
	                  </td>
	                </tr>
	                
	                <tr>
	                  <td>Brand CPA (%)</td>
	                  <td>
	                    <a id="brand_cpa" href="javascript:void(0)" data-type="text" data-value="1"
	                    data-title="Enter Brand CPA"></a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td>Commission model</td>
	                  <td>
	                    <a id="commision_model" href="javascript:void(0)" data-type="select" data-pk="1" data-value=""
	                    data-title="Select Commission model"></a>
	                  </td>
	                </tr>
	                <tr>
	                  <td>Verify brand</td>
	                  <td>
	                    <a id="verify_brand" href="javascript:void(0)" data-type="select" data-pk="1" data-value=""
	                    data-title="Select Verify brand"></a>
	                  </td>
	                </tr>

	               
	                <tr>
	                  <td style="width:35%">Brand Initials</td>
	                  <td style="width:65%">
	                    <a id="brand_initials" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter Brand Initials">brand Initials</a>
	                  </td>
	                </tr>

	              </tbody>
	            </table>
	        </div>
	        <div class="col-lg-6 col-md-6 col-sm-12">
	        	<table class="table table-bordered table-striped" id="editableUser">
	              <tbody>
	               <tr>
	                  <td>Payment Gateway charges (%)</td>
	                  <td>
	                    <a id="payment_gateway" href="javascript:void(0)" data-type="text" data-value="1"
	                    data-title="Enter Payment Gateway charges"></a>
	                  </td>
	                </tr>
	                <tr>
	                  <td>Service Tax (%)</td>
	                  <td>
	                    <a id="service_tax" href="javascript:void(0)" data-type="text" data-value="1"
	                    data-title="Enter Service Tax"></a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td>Integration Charges (%)</td>
	                  <td>
	                    <a id="integration" href="javascript:void(0)" data-type="text" data-value="1"
	                    data-title="Enter Integration Charges (%)"></a>
	                  </td>
	                </tr>
	                <tr>
	                	<td>
	                		Company Logo
	                	</td>
	                	<td>
	                		<div class="form-group">
			                  <div class="input-group input-group-file">
			                    <input type="text" class="form-control" readonly="">
			                    <span class="input-group-btn">
			                      <span class="btn btn-success btn-file">
			                        <i class="icon wb-upload" aria-hidden="true"></i>
			                        <input type="file" name="" multiple="">
			                      </span>
			                    </span>
			                  </div>
			                </div>
	                	</td>
	                </tr>
	                 <tr>
	                	<td>
	                		Upload Contract
	                	</td>
	                	<td>
	                		<div class="form-group">
			                  <div class="input-group input-group-file">
			                    <input type="text" class="form-control" readonly="">
			                    <span class="input-group-btn">
			                      <span class="btn btn-success btn-file">
			                        <i class="icon wb-upload" aria-hidden="true"></i>
			                        <input type="file" name="" multiple="">
			                      </span>
			                    </span>
			                  </div>
			                </div>
	                	</td>
	                </tr>
	              </tbody>
	            </table>
	        </div>
          </div>
        </div>
      </div>
      <!-- End Panel X-Editable -->

    </div>
  </div>
  <!-- End Page -->
<?php include('common/footer.php'); ?>
 <script>
    (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
        //enable / disable
        $('#editableEnable').click(function() {
          $('#editableUser .editable').editable('toggleDisabled');
        });

        var init_x_editable = function() {

          $.fn.editableform.buttons =
            '<button type="submit" class="btn btn-primary btn-sm editable-submit">' +
            '<i class="icon wb-check" aria-hidden="true"></i>' +
            '</button>' +
            '<button type="button" class="btn btn-default btn-sm editable-cancel">' +
            '<i class="icon wb-close" aria-hidden="true"></i>' +
            '</button>';

          $.fn.editabletypes.datefield.defaults.inputclass =
            "form-control input-sm";

          //defaults
          $.fn.editable.defaults.url = '/post';

          //editables
          $('#brand_cpa').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'brand_cpa',
            title: 'Enter Brand CPA'
          }); 

            

           $('#payment_gateway').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'payment_gateway',
            title: 'Enter Payment Gateway charges'
          });  

$('#service_tax').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'service_tax',
            title: 'Enter Service Tax'
          });  

$('#integration').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'integration',
            title: 'Enter Integration charges'
          });  

          $('#brand_initials').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'brand_initials',
            title: 'Enter Brand Initials'
          });


      


          $('#on_date').editable();

         

         

           $('#commision_model').editable({
            prepend: "not selected",
            source: [{
              value: 1,
              text: 'All exclusive'
            }, {
              value: 2,
              text: 'Flat Commission model'
            },
              {
              value: 3,
              text: ' Flat Commission excluding taxes'
            }

            ]
          });


           $('#verify_brand').editable({
            prepend: "not selected",
            source: [{
              value: 1,
              text: 'yes'
            }, {
              value: 2,
              text: 'No'
            }

            ]
          });

           
          

          $('#settings').editable({
            pk: 1,
            limit: 3,
            source: [{
              value: 1,
              text: 'Paid Brand'
            }, {
              value: 2,
              text: 'Show on Web'
            }]
          });


         
          // $("#editableUser").find(".form-control").addClass(".input-sm");
        };

        var destory_x_editable = function() {
          $('#brand_cpa').editable('destroy');
         
          $('#payment_gateway').editable('destroy');
          $('#service_tax').editable('destroy');
          $('#integration').editable('destroy');
          $('#brand_initials').editable('destroy');
         

          
          $('#commision_model').editable('destroy');
          $('#verify_brand').editable('destroy');
          
          $('#on_date').editable('destroy');
         
         
          $('#settings').editable('destroy');
         
        };

        $.fn.editable.defaults.mode = 'inline';
        init_x_editable();

        // $('#editableControls').on("click", "label", function() {
        //   xMode = $(this).find("input").val();
        //   $.fn.editable.defaults.mode = xMode;
        //   destory_x_editable();
        //   init_x_editable();
        // });
      });
    })(document, window, jQuery);
  </script>

</body>

</html>