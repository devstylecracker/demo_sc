

  <!-- Inline -->
  <style>
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
  </style>

 
  
</head>
<body class="site-menubar-fold" data-auto-menubar="false">
  <?php include('common/nav_view.php'); ?>

  <!-- Page -->
  <div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">Store Details</h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li class="active">Store Details</li>
      </ol>
    </div>
   <div class="page-content">
      <!-- Panel X-Editable -->
      <div class="panel">
      	<header class="panel-heading">
          <h3 class="panel-title">
            Business Profile
            <span class="panel-desc">Click to edit.</span>
          </h3>
        </header>
        <div class="panel-body">
          <button class="btn btn-primary btn-outline margin-bottom-10" id="editableEnable"
          type="button">enable / disable</button>
          <div class="table-responsive row">
	          <div class="col-lg-6 col-md-6 col-sm-12">	
	            <table class="table table-bordered table-striped" id="editableUser">
	              <tbody>
	                <tr>
	                  <td style="width:35%">Brand Name</td>
	                  <td style="width:65%">
	                    <a id="brand_name" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter Brand Name">brand name</a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td style="width:35%">Year of Establishment</td>
	                  <td style="width:65%">
	                    <a id="establishment" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter Year of Establishment">1926</a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td style="width:35%">Registration No.</td>
	                  <td style="width:65%">
	                    <a id="registration" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter Registration">Registration</a>
	                  </td>
	                </tr>
	                <tr>
	                  <td style="width:35%">Website</td>
	                  <td style="width:65%">
	                    <a id="website" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter website">website</a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td style="width:35%">Shop URL</td>
	                  <td style="width:65%">
	                    <a id="shop" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter shop url">shop url</a>
	                  </td>
	                </tr>
	                <tr>
	                  <td style="width:35%">Designer Name</td>
	                  <td style="width:65%">
	                    <a id="designer_name" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter Designer Name">Designer Name</a>
	                  </td>
	                </tr>
	                <tr>
	                  <td>Category</td>
	                  <td>
	                    <a id="category" href="javascript:void(0)" data-type="select" data-pk="1" data-value=""
	                    data-title="Select category"></a>
	                  </td>
	                </tr>
	                <tr>
	                  <td>Sub Category</td>
	                  <td>
	                    <a id="sub_category" href="javascript:void(0)" data-type="select" data-pk="1" data-value=""
	                    data-title="Select sub category"></a>
	                  </td>
	                </tr>
	                <tr>
	                  <td>Variation Type</td>
	                  <td>
	                    <a id="variation_type" href="javascript:void(0)" data-type="select" data-pk="1" data-value=""
	                    data-title="Select Variation Type"></a>
	                  </td>
	                </tr>
	              </tbody>
	            </table>
	        </div>
	        <div class="col-lg-6 col-md-6 col-sm-12">
	        	<table class="table table-bordered table-striped" id="editableUser">
	              <tbody>
	                <tr>
	                  <td style="width:35%">Age Group</td>
	                  <td style="width:65%">
	                    <a id="age_1" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter age">10</a> to 
	                    <a id="age_2" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter age">30</a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td style="width:35%">Price Range</td>
	                  <td style="width:65%">
	                    <a id="price_1" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter Price">1500</a> to 
	                    <a id="price_2" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter Price">6000</a>
	                  </td>
	                </tr>
	                <tr>
	                  <td>Short Description</td>
	                  <td>
	                    <a id="short_desc" href="javascript:void(0)" data-type="textarea" data-pk="1"
	                    data-placeholder="Your comments here..." data-title="Enter Description">Description</a>
	                  </td>
	                </tr>
	                <tr>
	                  <td>Detailed Description</td>
	                  <td>
	                    <a id="detail_desc" href="javascript:void(0)" data-type="textarea" data-pk="1"
	                    data-placeholder="Your comments here..." data-title="Enter Description">Description</a>
	                  </td>
	                </tr>
	                <tr>
	                  <td>Target Market</td>
	                  <td>
	                    <a id="market" href="javascript:void(0)" data-type="checklist" data-value="2,3"
	                    data-title="Select market"></a>
	                  </td>
	                </tr>
	                <tr>
	                  <td>Demographic</td>
	                  <td>
	                    <a id="demographic" href="javascript:void(0)" data-type="checklist" data-value="2,3"
	                    data-title="Select demographic"></a>
	                  </td>
	                </tr>
	                <tr>
	                  <td>Store Type</td>
	                  <td>
	                    <a id="store_type" href="javascript:void(0)" data-type="checklist" data-value="2,3"
	                    data-title="Select Store Type"></a>
	                  </td>
	                </tr>
	              </tbody>
	            </table>
	        </div>
          </div>
        </div>
      </div>
      <!-- End Panel X-Editable -->

      <!-- Panel X-Editable -->
      <div class="panel">
      	<header class="panel-heading">
          <h3 class="panel-title">
            Contact Details
            <span class="panel-desc">Click to edit.</span>
          </h3>
        </header>
        <div class="panel-body">
          <button class="btn btn-primary btn-outline margin-bottom-10" id="editableEnable"
          type="button">enable / disable</button>
          <div class="table-responsive row">
	          <div class="col-lg-6 col-md-6 col-sm-12">
	          		 <span class="panel-desc">Address</span>	
	            <table class="table table-bordered table-striped" id="editableUser">
	              <tbody>
	              	<tr>
	                  <td>Office Address</td>
	                  <td>
	                    <a id="address_1" href="javascript:void(0)" data-type="address" data-pk="1"
	                    data-title="Please, fill address"></a>
	                  </td>
	                </tr>
	              </tbody>
	            </table>
	        </div>
	        <div class="col-lg-6 col-md-6 col-sm-12">
	        		<span class="panel-desc">Warehouse Address</span>
	        	<table class="table table-bordered table-striped" id="editableUser">
	              <tbody>
	               <tr>
	                  <td>Office Address</td>
	                  <td>
	                    <a id="address_2" href="javascript:void(0)" data-type="address" data-pk="1"
	                    data-title="Please, fill address"></a>
	                  </td>
	                </tr>
	              </tbody>
	            </table>
	        </div>
	        <div class="col-lg-4 col-md-6 col-sm-12">
	        		<span class="panel-desc">Business Contact Details</span>
	        	<table class="table table-bordered table-striped" id="editableUser">
	              <tbody>
	              	 <tr>
	                  <td style="width:35%">Name</td>
	                  <td style="width:65%">
	                    <a id="contact_1" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter Name">Name</a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td style="width:35%">Mobile</td>
	                  <td style="width:65%">
	                    <a id="mobile_1" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter mobile">mobile</a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td style="width:35%">Email</td>
	                  <td style="width:65%">
	                    <a id="email_1" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter email">email</a>
	                  </td>
	                </tr>
	              </tbody>
	            </table>
	        </div>
	        <div class="col-lg-4 col-md-6 col-sm-12">
	        		<span class="panel-desc">Tech Contact Details</span>
	        	<table class="table table-bordered table-striped" id="editableUser">
	              <tbody>
	              	 <tr>
	                  <td style="width:35%">Name</td>
	                  <td style="width:65%">
	                    <a id="contact_2" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter Name">Name</a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td style="width:35%">Mobile</td>
	                  <td style="width:65%">
	                    <a id="mobile_2" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter mobile">mobile</a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td style="width:35%">Email</td>
	                  <td style="width:65%">
	                    <a id="email_2" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter email">email</a>
	                  </td>
	                </tr>
	              </tbody>
	            </table>
	        </div>
	        <div class="col-lg-4 col-md-6 col-sm-12">
	        		<span class="panel-desc">Other Contact Details</span>
	        	<table class="table table-bordered table-striped" id="editableUser">
	              <tbody>
	              	 <tr>
	                  <td style="width:35%">Name</td>
	                  <td style="width:65%">
	                    <a id="contact_3" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter Name">Name</a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td style="width:35%">Mobile</td>
	                  <td style="width:65%">
	                    <a id="mobile_3" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter mobile">mobile</a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td style="width:35%">Email</td>
	                  <td style="width:65%">
	                    <a id="email_3" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter email">email</a>
	                  </td>
	                </tr>
	              </tbody>
	            </table>
	        </div>
          </div>
        </div>
      </div>
      <!-- End Panel X-Editable -->

    </div>
  </div>
  <!-- End Page -->

<?php include('common/footer.php'); ?>

 <script>
 

    (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
         $('#editableEnable').click(function() {
         $('#editableUser .editable').editable('toggleDisabled');
      });

       var init_x_editable = function() {

          $.fn.editableform.buttons =
            '<button type="submit" class="btn btn-primary btn-sm editable-submit">' +
            '<i class="icon wb-check" aria-hidden="true"></i>' +
            '</button>' +
            '<button type="button" class="btn btn-default btn-sm editable-cancel">' +
            '<i class="icon wb-close" aria-hidden="true"></i>' +
            '</button>';

          $.fn.editabletypes.datefield.defaults.inputclass =
            "form-control input-sm";

          //defaults
          $.fn.editable.defaults.url = '/post';

         
           //editables
          $('#brand_name').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'brand_name',
            title: 'Enter Brand Name'
          });

           $('#contact_1').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'contact_1',
            title: 'Enter Contact Name'
          });
          $('#contact_2').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'contact_2',
            title: 'Enter Contact Name'
          });
          $('#contact_3').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'contact_3',
            title: 'Enter Contact Name'
          });


          $('#mobile_1').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'mobile_1',
            title: 'Enter Contact Name'
          });
          $('#mobile_2').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'mobile_2',
            title: 'Enter Contact Name'
          });
          $('#mobile_3').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'mobile_3',
            title: 'Enter Contact Name'
          });

           $('#email_1').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'email_1',
            title: 'Enter Contact Name'
          });

           $('#email_2').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'email_2',
            title: 'Enter Contact Name'
          });

           $('#email_3').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'email_3',
            title: 'Enter Contact Name'
          });

          $('#age_1').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'age_1',
            title: 'Enter Age'
          });

          $('#age_2').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'age_2',
            title: 'Enter Age'
          });

           $('#price_1').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'price_1',
            title: 'Enter Price'
          });

           $('#price_2').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'price_2',
            title: 'Enter Price'
          });

          $('#establishment').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'establishment',
            title: 'Enter Year of Establishment'
          });

          $('#registration').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'registration',
            title: 'Enter registration no.'
          });

          $('#website').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'website',
            title: 'Enter website'
          });

          $('#shop').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'shop url',
            title: 'Enter shop url'
          });

          $('#designer_name').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'designer_name',
            title: 'Enter Designer Name'
          });

         

          $('#category').editable({
            prepend: "not selected",
            source: [{
              value: 1,
              text: 'Men'
            }, {
              value: 2,
              text: 'Kids Wear'
            },
              {
              value: 3,
              text: 'Lifestyle'
            },
              {
              value: 4,
              text: 'Rainboot'
            },
              {
              value: 5,
              text: 'Womens wear'
            },

            ]
          });

           $('#sub_category').editable({
            prepend: "not selected",
            source: [{
              value: 1,
              text: 'Men'
            }, {
              value: 2,
              text: 'Kids Wear'
            },
              {
              value: 3,
              text: 'Lifestyle'
            },
              {
              value: 4,
              text: 'Rainboot'
            },
              {
              value: 5,
              text: 'Womens wear'
            },

            ]
          });

          $('#variation_type').editable({
            prepend: "not selected",
            source: [{
              value: 1,
              text: 'Men'
            }, {
              value: 2,
              text: 'Kids Wear'
            },
              {
              value: 3,
              text: 'Lifestyle'
            },
              {
              value: 4,
              text: 'Rainboot'
            },
              {
              value: 5,
              text: 'Womens wear'
            },

            ]
          });

         

          $('#short_desc, #detail_desc').editable({
            showbuttons: 'bottom'
          });

          

          $('#market').editable({
            pk: 1,
            limit: 3,
            source: [{
              value: 1,
              text: 'Export Surplus(INR 0-499)'
            }, {
              value: 2,
              text: 'High Street(INR 2000-9999)'
            }, {
              value: 3,
              text: 'Premium(INR 10000-25000)'
            }, {
              value: 4,
              text: 'Luxury(INR 50000+)'
            }, {
              value: 5,
              text: 'Street(INR 500-1999)'
            }]
          });

          $('#demographic').editable({
            pk: 1,
            limit: 3,
            source: [{
              value: 1,
              text: 'Male'
            }, {
              value: 2,
              text: 'Female'
            }, {
              value: 3,
              text: 'Children'
            }]
          });

           $('#store_type').editable({
            pk: 1,
            limit: 3,
            source: [{
              value: 1,
              text: 'Online'
            }, {
              value: 2,
              text: 'Offline'
            }]
          });


          $('#address_1,#address_2').editable({
            url: '/post',
            value: {
              city: "Moscow",
              street: "Lenina",
              state: "Assam",
              pincode: "302022",
              country: "India"
            },
            validate: function(value) {
              if (value.city === '') return 'city is required!';
            },
            display: function(value) {
              if (!value) {
                $(this).empty();
                return;
              }
              var html = '<b>' + $('<div>').text(value.city).html() +
                '</b>, ' + $('<div>').text(value.street).html() +
                '</b>, ' + $('<div>').text(value.state).html() +
                '</b>, ' + $('<div>').text(value.pincode).html() +  
                '</b>, ' + $('<div>').text(value.country)
                .html();   
              $(this).html(html);
            }
          });

          // $("#editableUser").find(".form-control").addClass(".input-sm");
        };
 var destory_x_editable = function() {
          $('#brand_name').editable('destroy');
          $('#contact_1').editable('destroy');
          $('#contact_2').editable('destroy');
          $('#contact_3').editable('destroy');
          $('#mobile_1').editable('destroy');
          $('#mobile_2').editable('destroy');
          $('#mobile_3').editable('destroy');
          $('#email_1').editable('destroy');
          $('#email_2').editable('destroy');
          $('#email_3').editable('destroy');
          $('#age_1').editable('destroy');
          $('#age_2').editable('destroy');
          $('#price_1').editable('destroy');
          $('#price_2').editable('destroy');
          $('#establishment').editable('destroy');

          $('#registration').editable('destroy');
          $('#website').editable('destroy');
          $('#shop').editable('destroy');
          $('#designer_name').editable('destroy');
          $('#sub_category').editable('destroy');
          $('#variation_type').editable('destroy');
          $('#short_desc').editable('destroy');
          $('#detail_desc').editable('destroy');

          $('#market').editable('destroy');
          $('#demographic').editable('destroy');
          $('#store_type').editable('destroy');
          $('#address_1').editable('destroy');
          $('#address_2').editable('destroy');
          $('#variation_type').editable('destroy');
          $('#short_desc').editable('destroy');
          $('#detail_desc').editable('destroy');
        };
       

        $.fn.editable.defaults.mode = 'inline';
        init_x_editable();

        // $('#editableControls').on("click", "label", function() {
        //   xMode = $(this).find("input").val();
        //   $.fn.editable.defaults.mode = xMode;
        //   destory_x_editable();
        //   init_x_editable();
        // });
      });
     
    })(document, window, jQuery);
  </script>

</body>

</html>