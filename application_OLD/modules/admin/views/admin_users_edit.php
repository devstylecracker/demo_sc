 <?php include('common/header.php'); ?>
   <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/jquery-strength/jquery-strength.css">

  <!-- Inline -->
  <style>
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }

    .form-material .form-control:disabled, .form-material .form-control[disabled], fieldset[disabled] .form-material .form-control{
      background-color: #FFFFE0;
    }
  </style>
</head>
  
<body>
  <?php include('common/nav_view.php'); ?>

  <!-- Page -->
  <div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">Edit User
      <a class="btn btn-info btn-xs" href="<?php echo base_url(); ?>admin/users/">Users List</a></h1>
     <!--  <ol class="breadcrumb">
        <li><a href="">Home</a></li>
        <li><a href="javascript:void(0)">Forms</a></li>
        <li class="active">Material</li>
      </ol> -->
    </div>
    <div class="page-content container-fluid">
      <div class="row">
        <div class="col-sm-12">
          <!-- Panel Static Lables -->
          <div class="panel">
            <div class="panel-body container-fluid">
            
               <form method="post" enctype="multipart/form-data" action="<?php echo base_url('admin/users/edit_data/'.$user_id);?>" name="data_register">
               <div class="form-group form-material row">
                 <div class="col-sm-6">
                    <label class="control-label" for="user_type">User Type</label>
                    <select class="form-control" id="user_type" name="user_type">
            <option value='Admin' <?php echo ($userDetail['user_type'] == 'Admin')?"selected":"";?>>Admin</option>
            <option value='Members/Website Users' <?php echo ($userDetail['user_type'] == 'Members/Website Users')?"selected":"";?>>Members/Website Users</option>
            <option value='Stylist' <?php echo ($userDetail['user_type'] == 'Stylist')?"selected":"";?>>Stylist</option>
            <option value='Intern' <?php echo ($userDetail['user_type'] == 'Intern')?"selected":"";?>>Intern</option>
            <option value='Data' <?php echo ($userDetail['user_type'] == 'Data')?"selected":"";?>>Data</option>
            <option value='Store' <?php echo ($userDetail['user_type'] == 'Store')?"selected":"";?>>Store</option>
            <option value='Delivery Partner' <?php echo ($userDetail['user_type'] == 'Delivery Partner')?"selected":"";?>>Delivery Partner</option>
            <option value='Affiliate User' <?php echo ($userDetail['user_type'] == 'Affiliate User')?"selected":"";?>>Affiliate User</option>
                    </select>
          <p class="help-block"><?php echo form_error('user_type'); ?></p>
                  </div>
                  <div class="col-sm-6">
                    <label class="control-label" for="user_name">UserName</label>
                    <input type="text" class="form-control" id="user_name" name="user_name" value="<?php echo $userDetail['username']; ?>" placeholder="User Name" disabled
                    />
          <p class="help-block"><?php echo form_error('user_name'); ?></p>
                  </div>
                 
                  <div class="col-sm-12">
                    <label class="control-label" for="user_email">Email</label>
                    <input type="email" class="form-control" id="user_email" name="user_email" value="<?php echo $userDetail['email']; ?>" placeholder="Email" disabled
                    />
          <p class="help-block"><?php echo form_error('user_email'); ?></p>
                  </div>
               
                  <div class="col-sm-6">
                    <label class="control-label" for="user_fname">First Name</label>
                    <input type="text" class="form-control" id="user_fname" value="<?php echo $userDetail['first_name']; ?>" name="user_fname" placeholder="First Name"
                    />
          <p class="help-block"><?php echo form_error('user_fname'); ?></p>
                  </div>
                 <div class="col-sm-6">
                    <label class="control-label" for="user_lname">Last Name</label>
                    <input type="text" class="form-control" id="user_lname" name="user_lname" value="<?php echo $userDetail['last_name']; ?>" placeholder="Last Name"
                    />
          <p class="help-block"><?php echo form_error('user_lname'); ?></p>
                  </div>
                
               
                    <div class="col-sm-6">
                      <label class="control-label margin-bottom-15" for="inputPassword">Password</label>
                      <input type="password" class="password-strength-example2 form-control" id="password"
                      name="password" data-plugin="strength" data-show-toggle="true" value="<?php echo $userDetail['password']; ?>" />
            <p class="help-block"><?php echo form_error('password'); ?></p>
                    </div>
                    <div class="col-sm-6">
                      <label class="control-label margin-bottom-15" for="inputCPassword">Confirm Password</label>
                      <input type="password" class="password-strength-example2 form-control" id="cpassword"
                      name="cpassword"value="<?php echo $userDetail['password']; ?>" />
                    </div>
                </div>
                <div class="text-right">
                  <button type="submit" name="submit" class="btn btn-primary" id="" onclick="return Validate()">Submit</button>
                </div>
              </form>
              <?php
                 /* }
                endif*/               ?>
            </div>
          </div>
          <!-- End Panel Static Lables -->
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->
 <?php include('common/footer.php'); ?>
 
 <script type="text/javascript">
    function Validate() {
        var password = document.getElementById("password").value;
        var confirmPassword = document.getElementById("cpassword").value;
        if (password != confirmPassword) {
            alert("Passwords do not match.");
            return false;
        }
        return true;
    }
</script>

 
<script type="text/javascript">

      (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
      });

       })(document, window, jQuery);
    </script>
