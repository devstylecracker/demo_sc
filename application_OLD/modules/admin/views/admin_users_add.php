
   <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/vendor/jquery-strength/jquery-strength.css">

  <!-- Inline -->
  <style>
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
  </style>
</head>
 
<body>
  <?php include('common/nav_view.php'); ?>

  <!-- Page -->
  <div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">Add User
      <a class="btn btn-info btn-xs" href="<?php echo base_url(); ?>admin/users/">Users List</a>
      </h1>
     <!--  <ol class="breadcrumb">
        <li><a href="">Home</a></li>
        <li><a href="javascript:void(0)">Forms</a></li>
        <li class="active">Material</li>
      </ol> -->
    </div>
    <div class="page-content container-fluid">
      <div class="row">
        <div class="col-sm-12">
          <!-- Panel Static Lables -->
          <div class="panel">
            <div class="panel-body container-fluid">
              
                 
              <form method="post" action="<?php echo base_url(); ?>admin/users/submit_data" name="data_register">
                <?php 
                    if(!empty($this->session->flashdata('success')))
                      {
                        ?>
                        <h3 class="label label-success center-block font-size-16 padding-vertical-5 width-350 margin-bottom-20" id="successMessage"><?php echo $this->session->flashdata('success');?></h3>
                      <?php
                      }
                ?>
               <div class="form-group form-material row">
                  <div class="col-sm-6">
                   <label class="control-label" for="user_type">User Type</label>
                    <select class="form-control" id="user_type" name="user_type">
                      <option value=""></option>	
                      <option value='Admin'>Admin</option>
                      <option value='Members/Website Users'>Members/Website Users</option>
                      <option value='Stylist'>Stylist</option>
                      <option value='Intern'>Intern</option>
                      <option value='Data'>Data</option>
                      <option value='Store'>Store</option>
                      <option value='Delivery Partner'>Delivery Partner</option>
                      <option value='Affiliate User'>Affiliate User</option>
                    </select>
                    <p class="help-block"><?php echo form_error('user_type'); ?></p>
                  </div>
                  <div class="col-sm-6">
                    <label class="control-label" for="user_name">UserName</label>
                    <input type="text" class="form-control" id="user_name" name="user_name" placeholder=""
                    />
          <p class="help-block"><?php echo form_error('user_name'); ?></p>

                  </div>  
                  <div class="col-sm-12">
                    <label class="control-label" for="user_email">Email</label>
                    <input type="email" class="form-control" id="user_email" name="user_email" placeholder=""
                    />
          <p class="help-block"><?php echo form_error('user_email'); ?></p>

                  </div>
                  <div class="col-sm-6">
                    <label class="control-label" for="user_fname">First Name</label>
                    <input type="text" class="form-control" id="user_fname" name="user_fname" placeholder=""
                    />
          <p class="help-block"><?php echo form_error('user_fname'); ?></p>

                  </div>
                 <div class="col-sm-6">
                    <label class="control-label" for="user_lname">Last Name</label>
                    <input type="text" class="form-control" id="user_lname" name="user_lname" placeholder=""
                    />
          <p class="help-block"><?php echo form_error('user_lname'); ?></p>

                  </div>
                
                    <div class="col-sm-6">
                      <label class="control-label margin-bottom-15" for="password">Password</label>
                      <input type="password" class="password-strength-example2 form-control" id="password"
                      name="password" data-plugin="strength" data-show-toggle="true"
                      />
            <p class="help-block"><?php echo form_error('password'); ?></p>

                    </div>
                    <div class="col-sm-6">
                      <label class="control-label margin-bottom-15" for="cpassword">Confirm Password</label>
                      <input type="password" class="password-strength-example2 form-control" id="cpassword"
                      name="cpassword"  />
                    </div>
                </div>
                <div class="text-right">
                  <button type="submit" class="btn btn-success" onclick="return Validate()">Submit</button>
                  <button class="btn btn-danger" type="reset">Reset</button>
                </div>
              </form>
            </div>
          </div>
          <!-- End Panel Static Lables -->
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->
  <?php include('common/footer.php'); ?>

  <script type="text/javascript">
    function Validate() {
        var password = document.getElementById("password").value;
        var confirmPassword = document.getElementById("cpassword").value;
        if (password != confirmPassword) {
            alert("Passwords do not match.");
            return false;
        }
        return true;
    }

     $(document).ready(function(){
        setTimeout(function() {
          $('#successMessage').fadeOut('fast');
        }, 5000); // <-- time in milliseconds
    });
</script>

<script type="text/javascript">

      (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
      });

       })(document, window, jQuery);
    </script>


  
