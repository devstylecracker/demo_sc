
  <!-- Inline -->
  <style>
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
    .img-admin {
    
    width: 24px;
    padding: 4px;
    border:1px dashed #76838f;
  }
    .img-admin1 {
        
        width: 34px;
        padding: 4px;
        border:1px dashed #76838f;
    }
  </style>
  
</head>
<body class="site-menubar-fold" data-auto-menubar="false">

  <?php include('common/nav_view.php'); ?>

  <!-- Page -->
  <div class="page animsition">
    
    <div class="page-header">
      <h1 class="page-title">Manage Brand</h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li class="active">Brands</li>
      </ol>
    </div>
    <div class="page-content">
     
      <!-- Panel FixedHeader -->
      <div class="panel">
        <header class="panel-heading">
          <h3 class="panel-title">
            List of Brands
          </h3>
        </header>
        <div class="panel-body">
          <table class="table table-hover dataTable table-striped width-full" id="exampleFixedHeaderBrands">
            <thead>
              <tr>
                  <th>Brand Name</th>
                  <th>Address</th>
                  <th>Contact</th>
                  <th>Online</th>
                  <th>Offline</th>
                  <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Gucci</td>
                <td>abc,something something,400026</td>
                <td>78569845</td>
                <td></td>
                <td></td>
                <td>
                   <div class="radio-custom radio-primary">
                     <a href=""><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="wb-pencil" aria-hidden="true"></i></div></a>

                    </div>
                   <div class="radio-custom radio-primary">
                      <a href=""><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="return confirm('Are you sure?');"><i class="wb-close" aria-hidden="true"></i></div></a>
                    </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <!-- End Panel FixedHeader -->
   </div>
      
<?php include('common/footer.php'); ?>

    <script type="text/javascript">

      (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
      });

       // Fixed Header Example
      // --------------------
      (function() {
        // initialize datatable
        var table = $('#exampleFixedHeaderBrands').DataTable({
          responsive: true,
          "bPaginate": false,
          "sDom": "lfirtp",
        });

        // initialize FixedHeader
        var offsetTop = 0;
        if ($('.site-navbar').length > 0) {
          offsetTop = $('.site-navbar').eq(0).innerHeight();
        }
        var fixedHeader = new FixedHeader(table, {
          offsetTop: offsetTop
        });

        // redraw fixedHeaders as necessary
        $(window).resize(function() {
          fixedHeader._fnUpdateClones(true);
          fixedHeader._fnUpdatePositions();
        });
      })();

       })(document, window, jQuery);
    </script>


  
