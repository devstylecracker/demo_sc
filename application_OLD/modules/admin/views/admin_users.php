
  <!-- Inline -->
  <style>
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
    .img-admin {
    
    width: 24px;
    padding: 4px;
    border:1px dashed #76838f;
  }
    .img-admin1 {
        
        width: 34px;
        padding: 4px;
        border:1px dashed #76838f;
    }
  </style>
  
</head>
<body class="site-menubar-fold" data-auto-menubar="false">

  <?php include('common/nav_view.php'); ?>

  <!-- Page -->
  <div class="page animsition">
    
    <div class="page-header">
      <h1 class="page-title">Admin Users</h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li class="active">Users</li>
      </ol>
    </div>
    <div class="page-content">
     
      <!-- Panel FixedHeader -->
      <div class="panel">
        <header class="panel-heading">
          <h3 class="panel-title">
            List of Admin Users
             <a class="btn btn-success btn-xs" href="<?php echo base_url(); ?>admin/users/add_data" onclick="" style="color: #fff;">Add User</a>
          </h3>
          
        </header>
        <div class="panel-body">
          <?php 
                    if(!empty($this->session->flashdata('success')))
                      {
                        ?>
                        <h3 class="label label-success center-block font-size-16 padding-vertical-5 width-350 margin-bottom-20" id="successMessage"><?php echo $this->session->flashdata('success');?></h3>
                      <?php
                      }
                ?>
          <table class="table table-hover dataTable table-striped width-full" id="exampleFixedHeaderusers">
            <thead>
              <tr>
                 <th>ID</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Role</th>
                  <th>Created Date</th>
                  <th>Action</th>
              </tr>
            </thead>
            <tbody>
           <?php
              if(!empty($get_users))
              {
                foreach($get_users as $val)
                {
                  ?>
              <tr>
                <td><?php echo $val['id']; ?></td>
                    <td><?php echo $val['username']; ?></td>
                    <td><?php echo $val['email']; ?></td>
                    <td><?php echo $val['first_name']; ?></td>
                    <td><?php echo $val['last_name']; ?></td>
                    <td><?php echo $val['user_type']; ?></td>
                  <td><?php echo $val['created_datetime']; ?></td>
                <td>
                   <div class="radio-custom radio-primary">
                     <a href="<?php echo base_url('admin/users/edit_data/').$val['id'];?>" data-toggle="tooltip" data-placement="top" title="Edit"><div class="img-admin"><i class="wb-pencil" aria-hidden="true"></i></div></a>

                    </div>
                   <div class="radio-custom radio-primary">
                      <a href="<?php echo base_url('admin/users/user_delete/').$val['id'];?>" data-toggle="tooltip" data-placement="top" title="Delete"><div class="img-admin" onclick="return confirm('Are you sure?');"><i class="wb-close" aria-hidden="true"></i></div></a>
                    </div>
                </td>
              </tr>
               <?php

                }
              }

              ?>
            </tbody>
          </table>
          <div class="example">
            <nav>
           <?php
              $pagermessage = isset($pagermessage)?$pagermessage:'';
              echo @$paginglinks;
           ?>
           </nav>
           </div>
        </div>
      </div>
      <!-- End Panel FixedHeader -->
   </div>
      
<?php include('common/footer.php'); ?>

    <script type="text/javascript">
       $(document).ready(function(){
        setTimeout(function() {
          $('#successMessage').fadeOut('fast');
        }, 5000); // <-- time in milliseconds
      });

      (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
        $('[data-toggle="tooltip"]').tooltip();
      });

       // Fixed Header Example
      // --------------------
      (function() {
        // initialize datatable
        var table = $('#exampleFixedHeaderusers').DataTable({
          responsive: true,
          "bPaginate": false,
          "sDom": "lfrtp",
        });

        // initialize FixedHeader
        var offsetTop = 0;
        if ($('.site-navbar').length > 0) {
          offsetTop = $('.site-navbar').eq(0).innerHeight();
        }
        var fixedHeader = new FixedHeader(table, {
          offsetTop: offsetTop
        });

        // redraw fixedHeaders as necessary
        $(window).resize(function() {
          fixedHeader._fnUpdateClones(true);
          fixedHeader._fnUpdatePositions();
        });
      })();

       })(document, window, jQuery);
    </script>


  
