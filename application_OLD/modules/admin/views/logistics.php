

    <style>
     .datepair-wrap {
      position: relative;
      overflow: hidden;
    }

    .animation-delay-100 {
      -webkit-animation-delay: 100ms;
      animation-delay: 100ms;
    }
    
    .animation-duration-300 {
      -webkit-animation-duration: 300ms;
      animation-duration: 300ms;
    }
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
    body{
      font-size: 12px !important;
    }
    .label{
      font-size: 84% !important;
    }
    .table>tbody>tr>td{
      padding: 5px !important;
      word-wrap: break-word;
    }
     .lightbox-block {
      max-width: 600px;
      padding: 15px 20px;
      margin: 40px auto;
      overflow: auto;
      background: #fff;
      border-radius: 3px;
    }
    .highlighted {
    background-color: #E5C37E;
}
  .icon {
      font-size: 22px;

    }
    .badge{
      font-size: 10px;
      font-weight: 500;
      border-radius: 3px;
    }
    .cursor{
      cursor: pointer;
    }
    .alertify-cover {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1700;
    background-color: black;
    filter: alpha(opacity=0);
    opacity: 0.7;
} 
  .table a {
  text-decoration: underline;
  color: #57c7d4;
  font-weight: 500;
  cursor: pointer;
}
.step.col-md-4.current, .step.col-md-4.disabled{
  height: 100px;
}
.lightboxsize{
  max-width: 800px !important;
}
.radio-custom label::before {
   
    display:none;
   
}
.img-admin {
    
    width: 24px;
    padding: 4px;
    border:1px dashed #76838f;
}
.img-admin1 {
    
    width: 34px;
    padding: 4px;
    border:1px dashed #76838f;
}
img{
  width: 100%;
}
.table .cell-120 {
    width: 120px;
    display: inline-table;
    
}
@media (min-width: 768px)
{
.form-inline .radio-custom label {
     padding-left: 0px; 
}
}
.radio-custom {
     padding-left: 0px; 
}
.btn-sm{
  float: right;
}

 .new{
  top: -36px !important;
  margin: -8px 1.8em !important; 
 }
  </style>

</head>

<body class="site-menubar-fold" data-auto-menubar="false">

    <?php include('common/nav_view.php'); ?>

  <div class="page bg-white animsition">

    <!-- Mailbox Content -->
    <div class="page-main">

      <!-- Mailbox Header -->
      <div class="page-header">
        <h1 class="page-title">Logistics Panel</h1>
      </div>

      <!-- Mailbox Content -->
      <div class="page-content">
       <div class="panel">
       
        <!-- Mailbox -->
        <table class="table table-hover dataTable table-striped width-full cursor" id="exampleFixedHeader" align="center">
          <thead>
             <tr align="center">
                <th>Date</th>
                <th>Order Unique ID</th>
                <th>Owner</th>
                <th>Total</th>
                <th>Discounted Price  </th>
                <th>Order Total </th>
                <th>Payment Type  </th>
                <th>Customer Name </th>
                <th>Mobile Number </th>
                <th>Status</th>
                <th>Actions</th>
                <th>Payment Status</th>
              </tr>
          </thead>
          <tbody class="main_table">
             <tr align="center">
              <td  class="cell-60">2017-07-27 09:37:23</td>
                <td class="cell-60">50236_5236 SC1234567890</td>
                <td  class="cell-80">Stylistname</td>
                <td  class="cell-30">3,333.00</td>
                <td  class="cell-30">3,333.00</td>
                <td  class="cell-30">3,333.00</td>
                <td  class="cell-30">Online</td>
                <td class="cell-60">Nikita Kale</td>
                <td >9167698562</td>
                <td class="cell-60"><span class="badge badge-primary">Ready to Ship</span>
                </td>
                <td class="cell-120"  align="left">
                  
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step6" id="step6" value="1" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="Are you sure you want to do this?"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel" class="load_more"><label for="step6"><div class="img-admin1 tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="SHIPPED" title=""><img src="<?php echo base_url('admin_assets/images/scicon/ship.png');?>"></div></label>
                    </div>
                  
                  <div style="padding-top: 10px;"><a id="examplePopWithList" href="javascript:void(0)"><div class="img-admin"><img src="<?php echo base_url('admin_assets/images/scicon/comment.png');?>"></div><span class="badge badge-danger up new">1</span></a>
                  <div class="hidden" id="examplePopoverList">
                      <form style="padding: 20px;">
                        <div class="form-group">
                        <label class="control-label" for="textarea">Remarks</label> 
                        <br/>
                        <textarea class="form-control" id="textarea" rows="6"></textarea>
                      </div>
                      <div>
                        <button type="submit" class="btn btn-success btn-sm">Submit</button>
                      </div>
                      </form>
                  </div></div>
                </td>
                <td class="cell-30"  align="right"><!-- Example Popup With Form -->
                  <a class="btn btn-success btn-outline btn-xs" id="examplePopupForm1" href="#exampleForm">Paid</a>
                  <!-- Form Itself -->
                  <form class="mfp-hide lightbox-block" id="exampleForm">
                    <h1>Payment Details</h1>
                    <div class="form-group">
                      <label class="control-label" for="inputName">Transaction ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="Transaction ID"
                      required>
                    </div>
                   <div class="form-group">
                      <label class="control-label" for="inputName">PAYU ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="PAYU ID"
                      required>
                    </div>
                  </form>
                  <!-- End Form --></td>
            </tr>
           <tr align="center">
              <td  class="cell-60">2017-07-27 09:37:23</td>
                <td class="cell-60">50236_5236 SC1234567890</td>
                <td  class="cell-80">Stylistname</td>
                <td  class="cell-30">3,333.00</td>
                <td  class="cell-30">3,333.00</td>
                <td  class="cell-30">3,333.00</td>
                <td  class="cell-30">Online</td>
                <td class="cell-60">Nikita Kale</td>
                <td >9167698562</td>
                <td class="cell-60"><span class="badge badge-primary">In Transit</span><br><br><br>
                   <!-- Pop with List -->
                   <a id="examplePopWithList1" href="javascript:void(0)"><i class="icon fa-comment" aria-hidden="true"></i><span class="badge badge-danger up">1</span></a>
                  <div class="hidden" id="examplePopoverList1">
                      <form style="padding: 20px;">
                        <div class="form-group">
                        <label class="control-label" for="textarea">Remarks</label> 
                        <br/>
                        <textarea class="form-control" id="textarea" rows="6"></textarea>
                      </div>
                      <div>
                        <button type="submit" class="btn btn-success btn-sm">Submit</button>
                      </div>
                      </form>
                  </div>
                  <!-- End Pop with List -->
                </td>
                <td class="cell-120"  align="left">
                    <form>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step7" id="step7" value="1" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="Are you sure you want to do this?"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel" class="load_more"><label for="step7"><div class="img-admin1 tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="DELIVERED" title=""><img src="<?php echo base_url('admin_assets/images/scicon/delivery.png');?>"></div></label>
                    </div>
                   </div>
                  </form> 
                </td>
                <td class="cell-30"  align="right"><!-- Example Popup With Form -->
                  <a class="btn btn-success btn-outline btn-xs" id="examplePopupForm1" href="#exampleForm">Paid</a>
                  <!-- Form Itself -->
                  <form class="mfp-hide lightbox-block" id="exampleForm">
                    <h1>Payment Details</h1>
                    <div class="form-group">
                      <label class="control-label" for="inputName">Transaction ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="Transaction ID"
                      required>
                    </div>
                   <div class="form-group">
                      <label class="control-label" for="inputName">PAYU ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="PAYU ID"
                      required>
                    </div>
                  </form>
                  <!-- End Form --></td>
            </tr>
            <tr align="center">
              <td  class="cell-60">2017-07-27 09:37:23</td>
                <td class="cell-60">50236_5236 SC1234567890</td>
                <td  class="cell-80">Stylistname</td>
                <td  class="cell-30">3,333.00</td>
                <td  class="cell-30">3,333.00</td>
                <td  class="cell-30">3,333.00</td>
                <td  class="cell-30">Online</td>
                <td class="cell-60">Nikita Kale</td>
                <td >9167698562</td>
                <td class="cell-60"><span class="badge badge-success">Delivered</span><br><br><br>
                   <!-- Pop with List -->
                   <a id="examplePopWithList1" href="javascript:void(0)"><i class="icon fa-comment" aria-hidden="true"></i><span class="badge badge-danger up">1</span></a>
                  <div class="hidden" id="examplePopoverList1">
                      <form style="padding: 20px;">
                        <div class="form-group">
                        <label class="control-label" for="textarea">Remarks</label> 
                        <br/>
                        <textarea class="form-control" id="textarea" rows="6"></textarea>
                      </div>
                      <div>
                        <button type="submit" class="btn btn-success btn-sm">Submit</button>
                      </div>
                      </form>
                  </div>
                  <!-- End Pop with List -->
                </td>
                <td class="cell-120"  align="left">
                   <!-- <form>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="step8" id="step8" value="1" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="Are you sure you want to do this?"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel" class="load_more"><label for="1">Delivered</label>
                    </div>
                   </div>
                  </form>  -->
                </td>
                <td class="cell-30"  align="right"><!-- Example Popup With Form -->
                  <a class="btn btn-success btn-outline btn-xs" id="examplePopupForm1" href="#exampleForm">Paid</a>
                  <!-- Form Itself -->
                  <form class="mfp-hide lightbox-block" id="exampleForm">
                    <h1>Payment Details</h1>
                    <div class="form-group">
                      <label class="control-label" for="inputName">Transaction ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="Transaction ID"
                      required>
                    </div>
                   <div class="form-group">
                      <label class="control-label" for="inputName">PAYU ID</label>
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="PAYU ID"
                      required>
                    </div>
                  </form>
                  <!-- End Form --></td>
            </tr>
           
          </tbody>
        </table>
      </div>
     
        
      </div>
    </div>
  </div>

  <!-- App Action -->
  <!-- <div class="site-action">
    <button type="button" class="btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-pencil animation-scale-up" aria-hidden="true"></i>
      <i class="back-icon wb-close animation-scale-up" aria-hidden="true"></i>
    </button>
    <div class="site-action-buttons">
      <button type="button" class="btn-raised btn btn-success btn-floating animation-slide-bottom animation-delay-100">
        <i class="icon wb-trash" aria-hidden="true"></i>
      </button>
      <button type="button" class="btn-raised btn btn-success btn-floating animation-slide-bottom">
        <i class="icon wb-inbox" aria-hidden="true"></i>
      </button>
    </div>
  </div> -->

  <!-- Modal -->
  <div class="modal fade" id="addMailForm" aria-hidden="true" aria-labelledby="addMailForm"
  role="dialog" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
          <h4 class="modal-title">Create New Messages</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <select id="topicTo" class="form-control" data-plugin="select2" multiple="multiple"
              data-placeholder="To:">
                <optgroup label="">
                  <option value="AK">Alaska</option>
                  <option value="HI">Hawaii</option>
                </optgroup>
              </select>
            </div>
            <div class="form-group">
              <select id="topicSubject" class="form-control" data-plugin="select2" multiple="multiple"
              data-placeholder="Subject:">
                <optgroup label="">
                  <option value="AK">Alaska</option>
                  <option value="HI">Hawaii</option>
                </optgroup>
              </select>
            </div>
            <div class="form-group">
              <textarea name="content" data-provide="markdown" data-iconlibrary="fa" rows="10"></textarea>
            </div>
          </form>
        </div>
        <div class="modal-footer text-left">
          <button class="btn btn-primary" data-dismiss="modal" type="submit">Send</button>
          <a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:void(0)">Cancel</a>
        </div>
      </div>
    </div>
  </div>
     <?php include('common/footer.php'); ?>
<script type="text/javascript">

      (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
      });

       // Fixed Header Example
      // --------------------
      (function() {
        // initialize datatable
        var table = $('#exampleFixedHeader').DataTable({
          responsive: true,
          "bPaginate": false,
          "sDom": "lfirtp",
        });

        // initialize FixedHeader
        var offsetTop = 0;
        if ($('.site-navbar').length > 0) {
          offsetTop = $('.site-navbar').eq(0).innerHeight();
        }
        var fixedHeader = new FixedHeader(table, {
          offsetTop: offsetTop
        });

        // redraw fixedHeaders as necessary
        $(window).resize(function() {
          fixedHeader._fnUpdateClones(true);
          fixedHeader._fnUpdatePositions();
        });
      })();

       })(document, window, jQuery);
    </script>

