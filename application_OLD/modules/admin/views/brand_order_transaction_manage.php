
  <style>
     .datepair-wrap {
      position: relative;
      overflow: hidden;
    }

    .animation-delay-100 {
      -webkit-animation-delay: 100ms;
      animation-delay: 100ms;
    }
    
    .animation-duration-300 {
      -webkit-animation-duration: 300ms;
      animation-duration: 300ms;
    }
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
    body{
      font-size: 12px !important;
    }
    .label{
      font-size: 84% !important;
    }
    .table>tbody>tr>td{
      padding: 5px !important;
      word-wrap: break-word;
    }
     .lightbox-block {
      max-width: 600px;
      padding: 15px 20px;
      margin: 40px auto;
      overflow: auto;
      background: #fff;
      border-radius: 3px;
    }
    .highlighted {
    background-color: #E5C37E;
}
  .icon {
      font-size: 22px;

    }
    .badge{
      font-size: 10px;
      font-weight: 500;
      border-radius: 3px;
    }
    .cursor{
      cursor: pointer;
    }
    .alertify-cover {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1700;
    background-color: black;
    filter: alpha(opacity=0);
    opacity: 0.7;
} 
  .table a {
  text-decoration: underline;
  color: #57c7d4;
  font-weight: 500;
  cursor: pointer;
}
.step.col-md-4.current, .step.col-md-4.disabled{
  height: 100px;
}
.lightboxsize{
  max-width: 800px !important;
}
.radio-custom label::before {
   
    display:none;
   
}
.img-admin {
    
    width: 24px;
    padding: 4px;
    border:1px dashed #76838f;
}
.img-admin1 {
    
    width: 34px;
    padding: 4px;
    border:1px dashed #76838f;
}
img{
  width: 100%;
}
.table .cell-120 {
    width: 120px;
    display: inline-table;
    
}
@media (min-width: 768px)
{
.form-inline .radio-custom label {
     padding-left: 0px; 
}
}
.radio-custom {
     padding-left: 0px; 
}
.btn-sm{
  float: right;
}

 .new{
  top: -36px !important;
  margin: -8px 1.8em !important; 
 }
  </style>
  <!-- Inline -->
  <style>
    .animation-delay-100 {
      -webkit-animation-delay: 100ms;
      animation-delay: 100ms;
    }
    
    .animation-duration-300 {
      -webkit-animation-duration: 300ms;
      animation-duration: 300ms;
    }
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
    body{
      font-size: 12px !important;
    }
    .label{
      font-size: 84% !important;
    }
    .table>tbody>tr>td{
      padding: 5px !important;
      word-wrap: break-word;
    }
     .lightbox-block {
      max-width: 600px;
      padding: 15px 20px;
      margin: 40px auto;
      overflow: auto;
      background: #fff;
      border-radius: 3px;
    }
    .highlighted {
    background-color: #E5C37E;
}
  .icon {
      font-size: 24px;

    }
    .badge{
      font-size: 10px;
      font-weight: 500;
      border-radius: 3px;
    }
    .cursor{
      cursor: pointer;
    }
    .alertify-cover {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1700;
    background-color: black;
    filter: alpha(opacity=0);
    opacity: 0.7;
} 
  .table a {
  text-decoration: underline;
  color: #57c7d4;
  font-weight: 500;
}
.table.dataTable tbody tr.active td, .table.dataTable tbody tr.active th {
        font-weight: 400;

}
.radio-custom label::before {
   
    display:none;
   
}
.img-admin {
    
    width: 24px;
    padding: 4px;
    border:1px dashed #76838f;
}
.img-admin1 {
    
    width: 34px;
    padding: 4px;
    border:1px dashed #76838f;
}
.img-admin1 img, .img-admin img{
  width: 100%;
}

  </style>

<body class="site-menubar-fold" data-auto-menubar="false">
  <?php include('common/nav_view.php'); ?>

  <div class="page bg-white animsition">

    <!-- Mailbox Content -->
    <div class="page-main">

      <!-- Mailbox Header -->
      <div class="page-header">
        <h1 class="page-title">Box transaction Management</h1>
      </div>

      <!-- Mailbox Content -->
      <div class="page-content">
       <div class="panel">
       
        <!-- Mailbox -->
        <table class="table table-hover dataTable table-striped width-full cursor" id="exampleFixedHeader" align="center">
          <thead>
             <tr>
                <th>Br.OrderID</th>
                <th class="cell-80">Box OrderID</th>
                <th class="cell-60">Prod. ID</th>
                <th class="cell-80">Prod. Name</th>
                <th>Br. ID</th>
                <th>Br. Name</th>
                <th>Order Date</th>
                <th>Dlvy Date</th>
                <th>Amount</th>
                <th>Br.Order Status</th>
                <th>Box Order Status</th>
                <th>Br.Order Pay Status</th>
                <th>Box Order pay Status</th>
                <th class="cell-60">Actions</th>
             </tr>
          </thead>
          <tbody class="main_table">  
           <tr align="center">
                <td>13561 614561465</td>
                <td>50236_5236 SC1234567890</td>
                <td>123 456 789</td>
                <td>Product Naem</td>
                <td>165 651561651</td>
                <td>Brand Naem</td>
                <td>12/3/1994</td>
                <td>24/3/1194</td>
                <td>2,500.00</td>
                <td><span class="badge badge-primary">Pending</span>
                    <span class="badge badge-success">Received</span>
                    <span class="badge badge-danger">Rejected</span>
                    <span class="badge badge-danger">Cancelled</span>
                    <span class="badge badge-danger">Returned</span>
                </td>
                 <td><span class="badge badge-primary">Processing</span>
                    <span class="badge badge-warning">Curating</span>
                    <span class="badge badge-success">Shipped</span>
                    <span class="badge badge-success">Delivered</span>
                    <span class="badge badge-danger">Closed</span>
                </td>
               <td><span class="badge badge-success">Paid</span>
                    <span class="badge badge-danger">Payment<br> Pending</span>
                    <span class="badge badge-primary">Refund <br>Pending</span>
                   
                </td>
                <td><span class="badge badge-success">Paid</span>
                    <span class="badge badge-danger">Payment <br>Pending</span>
                    <span class="badge badge-primary">Refund <br>Pending</span>
                   
                </td>
                 <td align="left">
              <form>
                  
                    <div class="radio-custom radio-primary">
                     <div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="Update Payment Details" title=""><i class="wb-pencil" aria-hidden="true"></i></div>
                    </div>  
                </form>

                <div style="padding-top: 30px;"><a id="examplePopWithList" href="javascript:void(0)"><div class="img-admin"><img src="<?php echo base_url('admin_assets/images/scicon/comment.png');?>"></div><span class="badge badge-danger up new">1</span></a>
                  <div class="hidden" id="examplePopoverList">
                      <form style="padding: 20px;">
                        <div class="form-group">
                        <label class="control-label" for="textarea">Remarks</label> 
                        <br/>
                        <textarea class="form-control" id="textarea" rows="6"></textarea>
                      </div>
                      <div>
                        <button type="submit" class="btn btn-success btn-sm">Submit</button>
                      </div>
                      </form>
                  </div></div>

                </td>
                
            </tr>
          </tbody>
        </table>
      </div>
     
        
      </div>
    </div>
  </div>

     <?php include('common/footer.php'); ?>
<script type="text/javascript">

      (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
      });

       // Fixed Header Example
      // --------------------
      (function() {
        // initialize datatable
        var table = $('#exampleFixedHeader').DataTable({
          responsive: true,
          "bPaginate": false,
          "sDom": "lfirtp",
        });

        // initialize FixedHeader
        var offsetTop = 0;
        if ($('.site-navbar').length > 0) {
          offsetTop = $('.site-navbar').eq(0).innerHeight();
        }
        var fixedHeader = new FixedHeader(table, {
          offsetTop: offsetTop
        });

        // redraw fixedHeaders as necessary
        $(window).resize(function() {
          fixedHeader._fnUpdateClones(true);
          fixedHeader._fnUpdatePositions();
        });
      })();

       })(document, window, jQuery);
    </script>

 