

  <!-- Inline -->
  <style>
     .datepair-wrap {
      position: relative;
      overflow: hidden;
    }

    .animation-delay-100 {
      -webkit-animation-delay: 100ms;
      animation-delay: 100ms;
    }
    
    .animation-duration-300 {
      -webkit-animation-duration: 300ms;
      animation-duration: 300ms;
    }
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
    body{
      font-size: 12px !important;
    }
    .label{
      font-size: 84% !important;
    }
    .table>tbody>tr>td{
      padding: 5px !important;
      word-wrap: break-word;
    }
     .lightbox-block {
      max-width: 600px;
      padding: 15px 20px;
      margin: 40px auto;
      overflow: auto;
      background: #fff;
      border-radius: 3px;
    }
    .highlighted {
    background-color: #E5C37E;
}
  
    .badge{
      font-size: 10px;
      font-weight: 500;
      border-radius: 3px;
    }
    .cursor{
      cursor: pointer;
    }
    .alertify-cover {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1700;
    background-color: black;
    filter: alpha(opacity=0);
    opacity: 0.7;
} 
  .table a {
  text-decoration: underline;
  color: #57c7d4;
  font-weight: 500;
  cursor: pointer;
}
.step.col-md-4.current, .step.col-md-4.disabled{
  height: 100px;
}
.lightboxsize{
  max-width: 800px !important;
}
.radio-custom label::before {
   
    display:none;
   
}
.img-admin {
    
    width: 24px;
    padding: 4px;
    border:1px dashed #76838f;
}
.img-admin1 {
    
    width: 34px;
    padding: 4px;
    border:1px dashed #76838f;
}
img{
  width: 100%;
}
.table .cell-120 {
    width: 120px;
    display: inline-table;
    
}
@media (min-width: 768px)
{
.form-inline .radio-custom label {
     padding-left: 0px; 
}
}
.radio-custom {
     padding-left: 0px; 
}
.btn-sm{
  float: right;
}

 .new{
  top: -36px !important;
  margin: -8px 1.8em !important; 
 }

 .page-aside+.page-main {
    margin-left: 500px;
}
  </style>

</head>
 
<body class="site-menubar-fold" data-auto-menubar="false">
 

<?php include('common/nav_view.php'); ?>
<!-- Page -->
   <div class="page animsition">
    <div class="page-aside" style="width: 500px;">
      <div class="page-aside-switch">
        <i class="icon wb-chevron-left" aria-hidden="true"></i>
        <i class="icon wb-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner">
        <section class="page-aside-section">
          <h5 class="page-aside-title">Add New Category</h5>
          <div class="list-group">
           <?php if(!empty($user_id)){ ?>
            <form name="frmcategory" id="frmcategory" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/categories/categories_edit/<?php echo $page_no.'/'.$user_id; ?>">
          <?php }else{ ?>
            <form name="frmcategory" id="frmcategory" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/categories">
          <?php } ?>

           <?php 
              if(!empty($this->session->flashdata('success')))
                {
                  ?>
                  <div class="form-group list-group-item"><h3 class="btn btn-block btn-success"><?php echo $this->session->flashdata('success');?></h3></div>
                <?php
                }
          ?>
                <div class="form-group list-group-item">
                <label>Name</label>
                <input type="text" placeholder="" name="term_name" id="term_name" value="<?php if(!empty($user_id)){echo $getCategories['term_name'];} ?>" class="form-control" onblur="genarate_slug()">
                <p class="help-block"><?php echo form_error('term_name'); ?></p>
                </div>
                <div class="form-group list-group-item">
                <label>Slug </label>
                <input type="text" placeholder="" name="term_slug" id="term_slug" value="<?php if(!empty($user_id)){ echo $getCategories['term_slug'];} ?>" class="form-control" >
                <p class="help-block" style="word-wrap: break-word;"><?php echo form_error('term_slug'); ?></p>
                </div>
                <div class="form-group list-group-item">
                <label>Parent</label>
                <select name="term_parent" id="term_parent" class="form-control chosen-select" tabindex="7">
                  <option value="0">None</option>
                  <?php if(!empty($cat_data)){
                    foreach($cat_data as $val){
                      echo $val;
                    }

                    } ?>
                </select>
                <p class="help-block">Optional.</p>
                </div>
                <div class="form-group list-group-item">
                <label>Description</label>
                <textarea name="term_description" id="term_description" class="form-control" rows="3"><?php if(!empty($user_id)){ echo $getCategories['taxonomy_content']; } ?></textarea>
                </div>
                  <!-- Example Multi-Select Simple -->
                <!-- <div class="form-group list-group-item">              
                <div class="example-wrap margin-md-0">
                 <label>Personality</label>
                  <div class="example">
                    <select class="form-control" data-plugin="multiSelect">
                        <option value="AK">Alaska</option>
                        <option value="HI">Hawaii</option>
                        <option value="CA">California</option>
                        <option value="NV">Nevada</option>
                        <option value="OR">Oregon</option>
                        <option value="WA">Washington</option>
                        <option value="AZ">Arizona</option>
                        <option value="CO">Colorado</option>
                        <option value="ID">Idaho</option>
                        <option value="MT">Montana</option>
                        <option value="NE">Nebraska</option>
                        <option value="NM">New Mexico</option>
                        <option value="ND">North Dakota</option>
                        <option value="UT">Utah</option>
                        <option value="WY">Wyoming</option>
                    </select>
                </div>                
                </div>
                </div>--> 
                <!-- End Example Multi-Select Simple -->
                 <!-- Example Multi-Select Simple -->
                <!-- <div class="form-group list-group-item">               
                <div class="example-wrap margin-md-0">
                  <label>Age</label>
                  <div class="example">
                    <select class="form-control" data-plugin="multiSelect">
                        <option value="AK">Alaska</option>
                        <option value="HI">Hawaii</option>
                        <option value="CA">California</option>
                        <option value="NV">Nevada</option>
                        <option value="OR">Oregon</option>
                        <option value="WA">Washington</option>
                        <option value="AZ">Arizona</option>
                        <option value="CO">Colorado</option>
                        <option value="ID">Idaho</option>
                        <option value="MT">Montana</option>
                        <option value="NE">Nebraska</option>
                        <option value="NM">New Mexico</option>
                        <option value="ND">North Dakota</option>
                        <option value="UT">Utah</option>
                        <option value="WY">Wyoming</option>
                    </select>
                </div>               
                </div>
                </div> -->
                 <!-- End Example Multi-Select Simple -->
                 <!-- Example Multi-Select Simple -->
               <!--  <div class="form-group list-group-item">                
                <div class="example-wrap margin-md-0">
                  <label>Budget</label>
                  <div class="example">
                      <select class="form-control" data-plugin="multiSelect">
                          <option value="AK">Alaska</option>
                          <option value="HI">Hawaii</option>
                          <option value="CA">California</option>
                          <option value="NV">Nevada</option>
                          <option value="OR">Oregon</option>
                          <option value="WA">Washington</option>
                          <option value="AZ">Arizona</option>
                          <option value="CO">Colorado</option>
                          <option value="ID">Idaho</option>
                          <option value="MT">Montana</option>
                          <option value="NE">Nebraska</option>
                          <option value="NM">New Mexico</option>
                          <option value="ND">North Dakota</option>
                          <option value="UT">Utah</option>
                          <option value="WY">Wyoming</option>
                      </select>
                  </div>                
                </div> 
                </div>-->
                <!-- End Example Multi-Select Simple -->
                <!-- <div class="form-group list-group-item">
                <label>Size Guide</label>
                <select class="form-control" name="size_guide" id="size_guide">
                  <option>Default</option>
                  <option>No Size Chart</option>
                  <option>Top</option>
                  <option>Bottom</option>
                  <option>Footwear</option>
                   <option>Top & Bottom</option>
                  <option>Bottom Men</option>
                  <option>Footwear Men</option>
                  <option>Top & Bottom Men</option>
                </select>
                </div> -->
                <div class="form-group list-group-item">
                <label>Taxanomy Type</label>
                <select class="form-control" name="termtax_type" id="termtax_type">
                  <option value="category" <?php if(!empty($user_id)){ echo ($getCategories['taxonomy_type'] == 'category')?"selected='selected'":""; }?> >Category</option>
                  <option value="pa"  <?php if(!empty($user_id)){ echo ($getCategories['taxonomy_type'] == 'pa')?"selected='selected'":""; }?> >PA</option>               
                </select>
                </div> 
                <div class="form-group list-group-item">
                <label>Order</label>
                <input type="text" placeholder="" name="sort_order" id="sort_order" value="<?php if(!empty($user_id)){ echo $getCategories['display_order']; }?>" class="form-control">
                <p class="help-block">(Only numbers allowed) Display on the website on filter page by ascending order</p>
                <p class="help-block"><?php echo form_error('sort_order'); ?></p>
                </div>
                <div class="form-group list-group-item">
                  <label>Upload Image</label>
                   <input type="file" name="term_image" id="term_image">
                  <p class="help-block"> Personalisation Category Mapping </p>
                </div> 
                <!-- <div class="form-group list-group-item">
                <label>Image Upload</label>
                    <div class="input-group input-group-file">
                      <input type="text" class="form-control" readonly="">
                      <span class="input-group-btn">
                        <span class="btn btn-outline btn-file">
                          <i class="icon wb-upload" aria-hidden="true"></i>
                          <input type="file" name="term_image" id="term_image">
                        </span>
                      </span>
                    </div>
                </div> -->
                <div class="form-group list-group-item">
                <button type="submit" class="btn btn-success btn-sm">Submit</button>
                </div>
            </form>
          </div>
        </section>
      </div>
    </div>
    <div class="page-main">
      <div class="page-header">
        <h1 class="page-title">View Categories</h1>
      </div>
      <div class="page-content">
        <div class="panel">
          <!-- <div class="panel-heading">
            <h3 class="panel-title">DEMO CONTENT</h3>
          </div> -->
          <div class="panel-body">
            <table class="table table-hover dataTable table-striped width-full" id="table_admin">
              <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Slug</th>
                    <th>Type</th>
                    <th>Action</th>
                </tr>
              </thead>
              <tbody>
              <?php
              if(!empty($get_cat))
              {
                foreach($get_cat as $val)
                {
                  ?>

                  <tr>
                    <td><?php echo $val['id']; ?></td>
                    <td><?php echo $val['term_name']; ?></td>
                    <td><?php echo $val['term_slug']; ?></td>
                  <td><?php echo $val['taxonomy_type']; ?></td>
                    <td>
                     
                   <div class="radio-custom">
                      <div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="EDIT" title=""><a href="<?php echo base_url('admin/categories/categories_edit/').$page_no.'/'.$val['id'];?>"><img src="<?php echo base_url('admin_assets/images/scicon/pay-b.png');?>"></a></div>
                    </div>
                  
                    <div class="radio-custom">
                     <div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="DELETE" title="" onclick="return confirm('Are you sure?');"><a href="<?php echo base_url('admin/categories/categories_delete/').$val['id'];?>"><img src="<?php echo base_url('admin_assets/images/scicon/cancel.png');?>"></a></div>
                    </div>  
                
                    </td>
                  </tr>
                  <?php

                }
              }

              ?>
                
              </tbody>
           </table>
           <div class="example">
            <nav>
           <?php
              $pagermessage = isset($pagermessage)?$pagermessage:'';
              echo @$paginglinks;
           ?>
           </nav>
           </div>
          </div>
        </div>
      </div>
    </div>
  </div>


     <?php include('common/footer.php'); ?>

<script type="text/javascript">

     function genarate_slug() {
    var term_name = $('#term_name').val();
        var slug = term_name.replace(/[^a-zA-Z0-9]/g, "-");
        $('#term_slug').val(slug.toLowerCase());
    };
    

      (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
      });
   
       // Fixed Header Example
      // --------------------
      (function() {
        // initialize datatable
        var table = $('#table_admin').DataTable({
          responsive: true,
          "bPaginate": false,
          "sDom": "lfirtp",
        });

        // initialize FixedHeader
        var offsetTop = 0;
        if ($('.site-navbar').length > 0) {
          offsetTop = $('.site-navbar').eq(0).innerHeight();
        }
        var fixedHeader = new FixedHeader(table, {
          offsetTop: offsetTop
        });

        // redraw fixedHeaders as necessary
        $(window).resize(function() {
          fixedHeader._fnUpdateClones(true);
          fixedHeader._fnUpdatePositions();
        });
      })();

      

       })(document, window, jQuery);
    </script>
   

