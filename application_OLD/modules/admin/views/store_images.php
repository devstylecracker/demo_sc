

  <!-- Inline -->
  <style>
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
  </style>

</head>
<body class="site-menubar-fold" data-auto-menubar="false">
  <?php include('common/nav_view.php'); ?>

  <!-- Page -->
  <div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">Store Images</h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li class="active">Store Images</li>
      </ol>
    </div>
   <div class="page-content">
      <!-- Panel X-Editable -->
      <div class="panel">
      	<header class="panel-heading">
          <h3 class="panel-title">
            Store Images
            <span class="panel-desc">Click to edit.</span>
          </h3>
        </header>
        <div class="panel-body">
          <button class="btn btn-primary btn-outline margin-bottom-10" id="editableEnable"
          type="button">enable / disable</button>
          <div class="table-responsive row">
	          <div class="col-lg-4 col-md-4 col-sm-12">	
	            <table class="table table-bordered table-striped" id="editableUser">
	              <tbody>
                   <tr>
                      <td>Brand</td>
                      <td>
                        <a id="brand" href="javascript:void(0)" data-type="select" data-pk="1" data-value=""
                        data-title="Select Brand"></a>
                      </td>
                    </tr>
	              	  <tr>
                    <td>
                     Brands Logo
                    </td>
                    <td>
                      <div class="form-group">
                        <div class="input-group input-group-file">
                          <input type="text" class="form-control" readonly="">
                          <span class="input-group-btn">
                            <span class="btn btn-success btn-file">
                              <i class="icon wb-upload" aria-hidden="true"></i>
                              <input type="file" name="">
                            </span>
                          </span>
                        </div>
                      </div>
                    </td>
                  </tr>
                   <tr>
                    <td>
                     Brands Cover Photo
                    </td>
                    <td>
                      <div class="form-group">
                        <div class="input-group input-group-file">
                          <input type="text" class="form-control" readonly="">
                          <span class="input-group-btn">
                            <span class="btn btn-success btn-file">
                              <i class="icon wb-upload" aria-hidden="true"></i>
                              <input type="file" name="">
                            </span>
                          </span>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Brands Mobile Cover Photo
                    </td>
                    <td>
                      <div class="form-group">
                        <div class="input-group input-group-file">
                          <input type="text" class="form-control" readonly="">
                          <span class="input-group-btn">
                            <span class="btn btn-success btn-file">
                              <i class="icon wb-upload" aria-hidden="true"></i>
                              <input type="file" name="">
                            </span>
                          </span>
                        </div>
                      </div>
                    </td>
                  </tr>
                   <tr>
                    <td>
                     Brands Home Page Promotions
                    </td>
                    <td>
                      <div class="form-group">
                        <div class="input-group input-group-file">
                          <input type="text" class="form-control" readonly="">
                          <span class="input-group-btn">
                            <span class="btn btn-success btn-file">
                              <i class="icon wb-upload" aria-hidden="true"></i>
                              <input type="file" name="">
                            </span>
                          </span>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Brands Listing Page
                    </td>
                    <td>
                      <div class="form-group">
                        <div class="input-group input-group-file">
                          <input type="text" class="form-control" readonly="">
                          <span class="input-group-btn">
                            <span class="btn btn-success btn-file">
                              <i class="icon wb-upload" aria-hidden="true"></i>
                              <input type="file" name="">
                            </span>
                          </span>
                        </div>
                      </div>
                    </td>
                  </tr>
	              </tbody>
	            </table>
	        </div>
          <div class="col-lg-4 col-md-4 col-sm-12">
            <table class="table table-bordered table-striped" id="editableUser">
                <tbody>
                  <tr><td>Women</td></tr>
                   <tr>
                    <td>
                      Top
                    </td>
                    <td>
                      <div class="form-group">
                        <div class="input-group input-group-file">
                          <input type="text" class="form-control" readonly="">
                          <span class="input-group-btn">
                            <span class="btn btn-success btn-file">
                              <i class="icon wb-upload" aria-hidden="true"></i>
                              <input type="file" name="">
                            </span>
                          </span>
                        </div>
                      </div>
                    </td>
                  </tr>
                   <tr>
                    <td>
                      Bottom
                    </td>
                    <td>
                      <div class="form-group">
                        <div class="input-group input-group-file">
                          <input type="text" class="form-control" readonly="">
                          <span class="input-group-btn">
                            <span class="btn btn-success btn-file">
                              <i class="icon wb-upload" aria-hidden="true"></i>
                              <input type="file" name="">
                            </span>
                          </span>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Footwear
                    </td>
                    <td>
                      <div class="form-group">
                        <div class="input-group input-group-file">
                          <input type="text" class="form-control" readonly="">
                          <span class="input-group-btn">
                            <span class="btn btn-success btn-file">
                              <i class="icon wb-upload" aria-hidden="true"></i>
                              <input type="file" name="">
                            </span>
                          </span>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Top & Bottom
                    </td>
                    <td>
                      <div class="form-group">
                        <div class="input-group input-group-file">
                          <input type="text" class="form-control" readonly="">
                          <span class="input-group-btn">
                            <span class="btn btn-success btn-file">
                              <i class="icon wb-upload" aria-hidden="true"></i>
                              <input type="file" name="">
                            </span>
                          </span>
                        </div>
                      </div>
                    </td>
                  </tr>
                  
                </tbody>
              </table>
          </div>
	        <div class="col-lg-4 col-md-4 col-sm-12">
	        	<table class="table table-bordered table-striped" id="editableUser">
	              <tbody>
                  
                  <tr><td>Men</td></tr>
                   <tr>
                    <td>
                      Top
                    </td>
                    <td>
                      <div class="form-group">
                        <div class="input-group input-group-file">
                          <input type="text" class="form-control" readonly="">
                          <span class="input-group-btn">
                            <span class="btn btn-success btn-file">
                              <i class="icon wb-upload" aria-hidden="true"></i>
                              <input type="file" name="">
                            </span>
                          </span>
                        </div>
                      </div>
                    </td>
                  </tr>
                   <tr>
                    <td>
                      Bottom
                    </td>
                    <td>
                      <div class="form-group">
                        <div class="input-group input-group-file">
                          <input type="text" class="form-control" readonly="">
                          <span class="input-group-btn">
                            <span class="btn btn-success btn-file">
                              <i class="icon wb-upload" aria-hidden="true"></i>
                              <input type="file" name="">
                            </span>
                          </span>
                        </div>
                      </div>
                    </td>
                  </tr>
                   <tr>
                    <td>
                      Footwear
                    </td>
                    <td>
                      <div class="form-group">
                        <div class="input-group input-group-file">
                          <input type="text" class="form-control" readonly="">
                          <span class="input-group-btn">
                            <span class="btn btn-success btn-file">
                              <i class="icon wb-upload" aria-hidden="true"></i>
                              <input type="file" name="">
                            </span>
                          </span>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Top & Bottom
                    </td>
                    <td>
                      <div class="form-group">
                        <div class="input-group input-group-file">
                          <input type="text" class="form-control" readonly="">
                          <span class="input-group-btn">
                            <span class="btn btn-success btn-file">
                              <i class="icon wb-upload" aria-hidden="true"></i>
                              <input type="file" name="">
                            </span>
                          </span>
                        </div>
                      </div>
                    </td>
                  </tr>
	              </tbody>
	            </table>
	        </div>
          </div>
        </div>
      </div>
      <!-- End Panel X-Editable -->

    </div>
  </div>
  <!-- End Page -->
<?php include('common/footer.php'); ?>

 <script>
    (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
        //enable / disable
        $('#editableEnable').click(function() {
          $('#editableUser .editable').editable('toggleDisabled');
        });

        var init_x_editable = function() {

          $.fn.editableform.buttons =
            '<button type="submit" class="btn btn-primary btn-sm editable-submit">' +
            '<i class="icon wb-check" aria-hidden="true"></i>' +
            '</button>' +
            '<button type="button" class="btn btn-default btn-sm editable-cancel">' +
            '<i class="icon wb-close" aria-hidden="true"></i>' +
            '</button>';

          $.fn.editabletypes.datefield.defaults.inputclass =
            "form-control input-sm";

          //defaults
          $.fn.editable.defaults.url = '/post';

          //editables
          


           $('#brand').editable({
            prepend: "not selected",
            source: [{
              value: 1,
              text: 'yes'
            }, {
              value: 2,
              text: 'No'
            }

            ]
          });

           
        

         
          // $("#editableUser").find(".form-control").addClass(".input-sm");
        };

        var destory_x_editable = function() {
          
          $('#brand').editable('destroy');
         
        };

        $.fn.editable.defaults.mode = 'inline';
        init_x_editable();

        // $('#editableControls').on("click", "label", function() {
        //   xMode = $(this).find("input").val();
        //   $.fn.editable.defaults.mode = xMode;
        //   destory_x_editable();
        //   init_x_editable();
        // });
      });
    })(document, window, jQuery);
  </script>

</body>

</html>