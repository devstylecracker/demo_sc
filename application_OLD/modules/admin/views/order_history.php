
  <!-- Inline -->
  <style>
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
    .img-admin {
    
    width: 24px;
    padding: 4px;
    border:1px dashed #76838f;
  }
    .img-admin1 {
        
        width: 34px;
        padding: 4px;
        border:1px dashed #76838f;
    }
  </style>
  
</head>
<body class="site-menubar-fold" data-auto-menubar="false">

  <?php include('common/nav_view.php'); ?>

  <!-- Page -->
  <div class="page animsition">
    <div class="page-content">
     
      <!-- Panel FixedHeader -->
      <div class="panel">
        <header class="panel-heading">
          <h3 class="panel-title">
            Order History
          </h3>
        </header>
        <div class="panel-body">
          <table class="table table-hover dataTable table-striped width-full" id="exampleFixedHeaderusers">
            <thead>
              <tr>
                <th>Date</th>
                <th class="cell-80">Order ID</th>
                <th>Image</th>
                <th class="cell-80">Products</th>
                <th>Size</th>
                <th>Qty</th>
                <th>Price</th>
                <th>Brand</th>
                <th>Total</th>
                <th>Shipping Add</th>
                <th>Status</th>
                <th>Actions</th>
             </tr>
            </thead>
            <tbody>
               <tr>
               <td>2017-07-27</td>
                <td>50236_5236 SC1234567890</td>
                <td><img src="https://www.stylecracker.com/sc_admin/assets/product_images/normal/14755855544341.jpg" width="40px;"></td>
                <td>3 Products
                 <div class="radio-custom radio-primary">
                     <a href=""><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="Product Info" title="Product info"><i class="wb-info" aria-hidden="true"></i></div></a>
                    </div>
                </td>
                 <td>39</td>
                 <td>1</td>
                 <td>2,500.00</td>
                <td>BRID1235</td>
                <td>2,000.00</td>
                <td>Pg west delhi BE 372 HARI NAGAR OPPOSITE VATIKA APARTMENT GATE NO 1 CENTRAL GOVERNMENT EMPLOYEES GATE MAHAKAVI GOSWAMI TULSIDAS MARG</td>
                <td><span class="label label-success">Confirmed</span><br><br><br>
                   
                </td>
                <td>
                   <div class="radio-custom radio-primary">
                     <a href=""><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="Add Products" title=""><i class="wb-plus" aria-hidden="true"></i></div></a>

                    </div>
                </td>
              </tr>
             
            </tbody>
          </table>
        </div>
      </div>
      <!-- End Panel FixedHeader -->
   </div>
      
<?php include('common/footer.php'); ?>

    <script type="text/javascript">

      (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
      });

       // Fixed Header Example
      // --------------------
      (function() {
        // initialize datatable
        var table = $('#exampleFixedHeaderusers').DataTable({
          responsive: true,
          "bPaginate": true,
          "sDom": "lfirtp",
        });

        // initialize FixedHeader
        var offsetTop = 0;
        if ($('.site-navbar').length > 0) {
          offsetTop = $('.site-navbar').eq(0).innerHeight();
        }
        var fixedHeader = new FixedHeader(table, {
          offsetTop: offsetTop
        });

        // redraw fixedHeaders as necessary
        $(window).resize(function() {
          fixedHeader._fnUpdateClones(true);
          fixedHeader._fnUpdatePositions();
        });
      })();

       })(document, window, jQuery);
    </script>


  
