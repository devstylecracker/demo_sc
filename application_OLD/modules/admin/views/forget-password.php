


  <!-- Page -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/css/pages/forgot-password.css">

</head>
<body class="page-forgot-password layout-full">
 

  <!-- Page -->
  
  <div class="page animsition vertical-align text-center" data-animsition-in="fade-in"
  data-animsition-out="fade-out">
    <div class="page-content vertical-align-middle">
      <h2 class="page-title">Forgot Your Password ?</h2>
      <p>Input your registered email to reset your password</p>
	  <?php 
              if(!empty($this->session->flashdata('error')))
                {
                  ?>
                  <h3 class="btn btn-block btn-danger"><?php echo $this->session->flashdata('error');?></h3>
                <?php
                }
          ?>

      <form class="width-300 margin-top-30 center-block" method="post" action="<?php echo base_url('admin/login/forgotPassword');?>">
        <div class="form-group">
          <input type="email" class="form-control" id="inputEmail" name="emailid" placeholder="Your Email">
		   <div class="error"><?php echo form_error('emailid'); ?></div>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary btn-block">Reset Your Password</button>
        </div>
      </form>

      <footer class="page-copyright">
        <p>WEBSITE BY amazingSurge</p>
        <p>© 2015. All RIGHT RESERVED.</p>
        <div class="social">
          <a href="javascript:void(0)">
            <i class="icon bd-twitter" aria-hidden="true"></i>
          </a>
          <a href="javascript:void(0)">
            <i class="icon bd-facebook" aria-hidden="true"></i>
          </a>
          <a href="javascript:void(0)">
            <i class="icon bd-dribbble" aria-hidden="true"></i>
          </a>
        </div>
      </footer>
    </div>
  </div>
  <!-- End Page -->

<?php include('common/footer.php'); ?>
  <script>
    (function(document, window, $) {
      'use strict';

      var Site = window.Site;
      $(document).ready(function() {
        Site.run();
      });
    })(document, window, jQuery);
  </script>

</body>

</html>