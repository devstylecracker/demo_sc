
  <style>
     .datepair-wrap {
      position: relative;
      overflow: hidden;
    }

    .animation-delay-100 {
      -webkit-animation-delay: 100ms;
      animation-delay: 100ms;
    }
    
    .animation-duration-300 {
      -webkit-animation-duration: 300ms;
      animation-duration: 300ms;
    }
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
    body{
      font-size: 12px !important;
    }
    .label{
      font-size: 84% !important;
    }
    .table>tbody>tr>td{
      padding: 5px !important;
      word-wrap: break-word;
    }
     .lightbox-block {
      max-width: 600px;
      padding: 15px 20px;
      margin: 40px auto;
      overflow: auto;
      background: #fff;
      border-radius: 3px;
    }
    .highlighted {
    background-color: #E5C37E;
}
  .icon {
      font-size: 22px;

    }
    .badge{
      font-size: 10px;
      font-weight: 500;
      border-radius: 3px;
    }
    .cursor{
      cursor: pointer;
    }
    .alertify-cover {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1700;
    background-color: black;
    filter: alpha(opacity=0);
    opacity: 0.7;
} 
  .table a {
  text-decoration: underline;
  color: #57c7d4;
  font-weight: 500;
  cursor: pointer;
}
.step.col-md-4.current, .step.col-md-4.disabled{
  height: 100px;
}
.lightboxsize{
  max-width: 800px !important;
}
.radio-custom label::before {
   
    display:none;
   
}
.img-admin {
    
    width: 24px;
    padding: 4px;
    border:1px dashed #76838f;
}
.img-admin1 {
    
    width: 34px;
    padding: 4px;
    border:1px dashed #76838f;
}
img{
  width: 100%;
}
.table .cell-120 {
    width: 120px;
    display: inline-table;
    
}
@media (min-width: 768px)
{
.form-inline .radio-custom label {
     padding-left: 0px; 
}
}
.radio-custom {
     padding-left: 0px; 
}
.btn-sm{
  float: right;
}

 .new{
  top: -36px !important
;  margin: -8px 1.8em !important; 
 }
  </style>
  <!-- Inline -->
  <style>
    .animation-delay-100 {
      -webkit-animation-delay: 100ms;
      animation-delay: 100ms;
    }
    
    .animation-duration-300 {
      -webkit-animation-duration: 300ms;
      animation-duration: 300ms;
    }
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
    body{
      font-size: 12px !important;
    }
    .label{
      font-size: 84% !important;
    }
    .table>tbody>tr>td{
      padding: 5px !important;
      word-wrap: break-word;
    }
     .lightbox-block {
      max-width: 600px;
      padding: 15px 20px;
      margin: 40px auto;
      overflow: auto;
      background: #fff;
      border-radius: 3px;
    }
    .highlighted {
    background-color: #E5C37E;
}
  .icon {
      font-size: 24px;

    }
    .badge{
      font-size: 10px;
      font-weight: 500;
      border-radius: 3px;
    }
    .cursor{
      cursor: pointer;
    }
    .alertify-cover {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1700;
    background-color: black;
    filter: alpha(opacity=0);
    opacity: 0.7;
} 
  .table a {
  text-decoration: underline;
  color: #57c7d4;
  font-weight: 500;
}
.table.dataTable tbody tr.active td, .table.dataTable tbody tr.active th {
        font-weight: 400;

}
.radio-custom label::before {
   
    display:none;
   
}
.img-admin {
    
    width: 24px;
    padding: 4px;
    border:1px dashed #76838f;
}
.img-admin1 {
    
    width: 34px;
    padding: 4px;
    border:1px dashed #76838f;
}
img{
  width: 100%;
}

  </style>

</head>
<body class="site-menubar-fold" data-auto-menubar="false">
  <?php include('common/nav_view.php'); ?>

  <div class="page bg-white animsition">

    <!-- Mailbox Content -->
    <div class="page-main">

      <!-- Mailbox Header -->
      <div class="page-header">
        <h1 class="page-title">Product Management</h1>
      </div>

      <!-- Mailbox Content -->
      <div class="page-content">
       <div class="panel">
       
        <!-- Mailbox -->
        <table class="table table-hover dataTable table-striped width-full cursor" id="exampleFixedHeader">
          <thead>
             <tr>
                <th>PID</th>
                <th>Image</th>
                <th class="cell-100">Name</th>
                <th>Price</th>
                <th>Status</th>
                <th>Store</th>
                <th>Created Date</th>
                <th>Actions</th>
             </tr>
          </thead>
          <tbody class="main_table">  
           <tr>
                <td>193306</td>
                <td><img src="https://www.stylecracker.com/sc_admin/admin_assets/products/thumb_124/15096950522281.jpg" style="width: 40px;"></td>
                <td>Code By Lifestyle Textured Top With Bell Sleeves</td>
                <td>2563</td>
                <td><span class="badge badge-warning">Pending</span></td>
                <td>WareHouse <br> WareHouse</td>
                <td>2017-11-03 13:14:12</td>
                 <td align="left">
              <form>
                   <div class="radio-custom radio-primary">
                     <div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="wb-order" aria-hidden="true"></i></div>

                    </div>
                   <div class="radio-custom radio-primary">
                     <div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="wb-pencil" aria-hidden="true"></i></div>

                    </div>
                   <div class="radio-custom radio-primary">
                      <div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title=""><i class="wb-close" aria-hidden="true"></i></div>
                    </div>
                </form>

                <div style="padding-top: 30px;"><a id="examplePopWithList" href="javascript:void(0)"><div class="img-admin"><img src="<?php echo base_url('admin_assets/images/scicon/comment.png');?>"></div><span class="badge badge-danger up new">1</span></a>
                  <div class="hidden" id="examplePopoverList">
                      <form style="padding: 20px;">
                        <div class="form-group">
                        <label class="control-label" for="textarea">Remarks</label> 
                        <br/>
                        <textarea class="form-control" id="textarea" rows="6"></textarea>
                      </div>
                      <div>
                        <button type="submit" class="btn btn-success btn-sm">Submit</button>
                      </div>
                      </form>
                  </div></div>

                </td>
                
            </tr>
          </tbody>
        </table>
      </div>
     
        
      </div>
    </div>
  </div>

 
  <?php include('common/footer.php'); ?>
   <script>
 

    $(function() {
    $('td').click(function() {
        $('tr').removeClass('active');
        $(this).parent().addClass('active'); 
    });
});

    (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
      });

     (function(){
  $("tr").click(function() {
    $(this).closest("tr").siblings().removeClass("highlighted");
    $(this).toggleClass("highlighted");
  })
});  
       var defaults = $.components.getDefaults("webuiPopover");
      // Example Webui Popover Pop with List
      // -----------------------------------
      (function() {
        var listContent = $('#examplePopoverList').html(),
          listSettings = {
            content: listContent,
            title: '',
            padding: false
          };

        $('#examplePopWithList').webuiPopover($.extend({}, defaults,
          listSettings));

       /* $('input[name=step1]').change(function(){
        $('form').submit();
        });*/

      })();
     
      (function() {
        var listContent = $('#examplePopoverList1').html(),
          listSettings = {
            content: listContent,
            title: '',
            padding: false
          };

        $('#examplePopWithList1').webuiPopover($.extend({}, defaults,
          listSettings));

       /* $('input[name=step1]').change(function(){
        $('form').submit();
        });*/

      })();
     


      // Fixed Header Example
      // --------------------
      (function() {
        // initialize datatable
        var table = $('#exampleFixedHeader').DataTable({
          responsive: true,
          "bPaginate": false,
          "sDom": "lfirtp",
        });

        // initialize FixedHeader
        var offsetTop = 0;
        if ($('.site-navbar').length > 0) {
          offsetTop = $('.site-navbar').eq(0).innerHeight();
        }
        var fixedHeader = new FixedHeader(table, {
          offsetTop: offsetTop
        });

        // redraw fixedHeaders as necessary
        $(window).resize(function() {
          fixedHeader._fnUpdateClones(true);
          fixedHeader._fnUpdatePositions();
        });
      })();

     
    })(document, window, jQuery);
  </script>

</body>

</html>