

  <!-- Inline -->
  <style>
    .animation-delay-100 {
      -webkit-animation-delay: 100ms;
      animation-delay: 100ms;
    }
    
    .animation-duration-300 {
      -webkit-animation-duration: 300ms;
      animation-duration: 300ms;
    }
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
    body{
      font-size: 12px !important;
    }
    .label{
      font-size: 84% !important;
    }
    .table>tbody>tr>td{
      padding: 5px !important;
      word-wrap: break-word;
    }
     .lightbox-block {
      max-width: 600px;
      padding: 15px 20px;
      margin: 40px auto;
      overflow: auto;
      background: #fff;
      border-radius: 3px;
    }
    .highlighted {
    background-color: #E5C37E;
}
  .icon {
      font-size: 24px;

    }
    .badge{
      font-size: 10px;
      font-weight: 500;
      border-radius: 3px;
    }
    .cursor{
      cursor: pointer;
    }
    .alertify-cover {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1700;
    background-color: black;
    filter: alpha(opacity=0);
    opacity: 0.7;
} 
  .table a {
  text-decoration: underline;
  color: #57c7d4;
  font-weight: 500;
}
.table.dataTable tbody tr.active td, .table.dataTable tbody tr.active th {
        font-weight: 400;

}
.radio-custom label::before {
   
    display:none;
   
}
.img-admin {
    
    width: 24px;
    padding: 4px;
    border:1px dashed #76838f;
}
.img-admin1 {
    
    width: 34px;
    padding: 4px;
    border:1px dashed #76838f;
}

.img-admin img{
    
   width: 100%;
}
.img-admin1 img{
    
    width: 100%;
}


  </style>
</head>
 
<body class="site-menubar-fold" data-auto-menubar="false">
  <?php include('common/nav_view.php'); ?>

  <div class="page bg-white animsition">

    <!-- Mailbox Content -->
    <div class="page-main">

      <!-- Mailbox Header -->
      <div class="page-header">
        <h1 class="page-title">Manage Product Orders</h1>
      </div>

      <!-- Mailbox Content -->
      <div class="page-content">
       <div class="panel">
       
        <!-- Mailbox -->
        <table class="table table-hover dataTable table-striped width-full cursor" id="exampleFixedHeader" align="center">
          <thead>
             <tr>
                <th>Date</th>
                <th class="cell-80">Order ID</th>
                <th class="cell-60">AWP</th>
                <th class="cell-80">SKU</th>
                <th>Image</th>
                <th>Name</th>
                <th>Brand ID</th>
                <th>Brand Name</th>
                <th>Size</th>
                <th>Prdt Price</th>
                <th>Disc Price </th>
                <th>Dvy Date</th>
                <th>Status</th>
                <th class="cell-100">Actions</th>
             </tr>
          </thead>
          <tbody class="main_table">
             <tr align="center">
                  <td>2017-07-27</td>
                <td>50236_5236 SC1234567890</td>
                <td>123456789</td>
                <td>SCBOX1904848500</td>
                <td><img src="https://www.stylecracker.com/sc_admin/assets/product_images/normal/14755855544341.jpg" width="40px;"></td>
                
                <td><a data-target="#exampleGrid" data-toggle="modal">High heels in black</a>
                  <!-- Example Grid In Modal -->            
                 
                  <!-- Modal -->
                  <div class="modal fade" id="exampleGrid" aria-hidden="true" aria-labelledby="exampleGrid"
                  role="dialog" tabindex="-1">
                    <div class="modal-dialog" style="width: 1000px;">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title">Product Details</h4>
                        </div>
                        <div class="modal-body">
                        <div class="row">
                          <div class="col-sm-12 col-md-6 col-lg-6">
                          <div class="example-grid">
                            <table class="table table-bordered">
                              <tr>
                                <th>Name</th>
                                <td>5 products 6-men</td>
                              </tr>
                              <tr>
                                <th>Description</th>
                                <td>  1 apparel, shoes, Bag, Accessories or Cosmetics</td>
                              </tr>
                              <tr>
                                <th>Store</th>
                                <td>StyleCrackerStore</td>
                              </tr>
                              <tr>
                                <th>Category</th>
                                <td></td>
                              </tr>
                              <tr>
                                <th>Sub Category</th>
                                <td></td>
                              </tr>
                              <tr>
                                <th>Product URL</th>
                                <td>http://localhost/projectbitb/sc-box</td>
                              </tr>
                               <tr>
                                <th>Tags</th>
                                <td></td>
                              </tr>
                              <tr>
                                <th>Price Range</th>
                                <td>under 15000</td>
                              </tr>
                              <tr>
                                <th>PriceL</th>
                                <td>6,333</td>
                              </tr>
                               <tr>
                                <th>Target Market</th>
                                <td></td>
                              </tr>
                              <tr>
                                <th>Consumer Type</th>
                                <td></td>
                              </tr>
                              <tr>
                                <th>Brand</th>
                                <td></td>
                              </tr>
                              <tr>
                                <th>Rating</th>
                                <td>0</td>
                              </tr>
                            </table>  
                          </div>
                          </div> 

                          <div class="col-sm-12 col-md-6 col-lg-6">
                          <div class="example-grid">
                             <table class="table table-bordered">
                              <tr>
                                <td>Image</td>
                                <td><img src="http://localhost/projectbitb/admin/assets/products/thumb_124/1496826572641.png" width="200"></td>
                              </tr>
                              <tr>
                                <th>Body Part</th>
                                <td></td>
                              </tr>
                              <tr>
                                <th>Body Shape</th>
                                <td></td>
                              </tr>
                              <tr>
                                <th>Body Type</th>
                                <td></td>
                              </tr>
                              <tr>
                                <th>Personality</th>
                                <td></td>
                              </tr>
                              <tr>
                                <th>Age Group</th>
                                <td></td>
                               <tr>
                                <th>Promotional</th>
                                <td>No</td>
                              </tr>
                              <tr>
                                <th>Active</th>
                                <td>Yes</td>
                              </tr>
                            </table>  
                          </div>
                          </div> 

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- End Modal -->
              
              
              <!-- End Example Grid In Modal -->  
                  </td>
                <td>BRID1235</td>
                <td>Only Two Feet</td>
                <td>39</td>
                <td>2,500.00</td>
                <td>2,000.00</td>
                <td>2017-07-27</td>
               
                <td><span class="label label-primary">Pending</span><br><br><br>
                  
                </td>
                 <td align="left"  style="display: inline;">
                   <form>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="stepp1" id="stepp11" value="1" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="This is a confirm dialog"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel" class="load_more_panel"><label for="stepp11"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="CONFIRM" title=""><img src="<?php echo base_url('admin_assets/images/scicon/edit.png');?>"></div></label>
                    </div>
                    <div class="radio-custom radio-primary">
                      <input type="radio" name="stepp1" id="stepp12" value="2" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="This is a confirm dialog"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel" class="load_more_panel"><label for="stepp12"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="CANCEL" title=""><img src="<?php echo base_url('admin_assets/images/scicon/cancel.png');?>"></div></label>
                    </div>
                  </form> 
                </td>
            </tr>
            <tr align="center">
              <td>2017-07-27</td>
                <td>50236_5236 SC1234567890</td>
                <td>123456789</td>
                <td>SCBOX1904848500</td>
                <td><img src="https://www.stylecracker.com/sc_admin/assets/product_images/normal/14755855544341.jpg" width="40px;"></td>
                
                <td>High heels in black</td>
                <td>BRID1235</td>
                <td>Only Two Feet</td>
                <td>39</td>
                <td>2,500.00</td>
                <td>2,000.00</td>
                <td>2017-07-27</td>
               
                <td><span class="label label-success">Confirmed</span><br><br><br>
                   
                </td>
                 <td align="left" style="display: inline;">
                   <form>
                   <div class="radio-custom radio-primary">
                      <input type="radio" name="stepp1" id="stepp11" value="1" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="This is a confirm dialog"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel" class="load_more_panel"><label for="stepp11"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="DELIVERED" title=""><img src="<?php echo base_url('admin_assets/images/scicon/delivery.png');?>"></div></label>
                    </div>
                    <div class="radio-custom radio-primary">
                      <input type="radio" name="stepp1" id="stepp12" value="2" data-plugin="alertify"
                  data-type="confirm" data-confirm-title="This is a confirm dialog"
                  data-success-message="You've clicked OK" data-error-message="You've clicked Cancel" class="load_more_panel"><label for="stepp12"><div class="img-admin tooltip-default tooltip-scale"  data-toggle="tooltip" data-placement="top" data-original-title="DELIVERY REJECTED" title=""><img src="<?php echo base_url('admin_assets/images/scicon/return.png');?>"></div></label>
                    </div>
                  </form> 
                </td>
            </tr>
           <tr align="center">
              <td>2017-07-27</td>
                <td>50236_5236 SC1234567890</td>
                <td>123456789</td>
                <td>SCBOX1904848500</td>
                <td><img src="https://www.stylecracker.com/sc_admin/assets/product_images/normal/14755855544341.jpg" width="40px;"></td>
                 <td>High heels in black</td>
                <td>BRID1235</td>
                <td>Only Two Feet</td>
                <td>39</td>
                <td>2,500.00</td>
                <td>2,000.00</td>
                <td>2017-07-27</td>
             
                <td><span class="label label-success">Received</span><br><br><br>
                  
                </td>
               <td></td>
            </tr>
            <tr align="center">
              <td>2017-07-27</td>
                <td>50236_5236 SC1234567890</td>
                <td>123456789</td>
                <td>SCBOX1904848500</td>
                <td><img src="https://www.stylecracker.com/sc_admin/assets/product_images/normal/14755855544341.jpg" width="40px;"></td>
                 <td>High heels in black</td>
                <td>BRID1235</td>
                <td>Only Two Feet</td>
                <td>39</td>
                <td>2,500.00</td>
                <td>2,000.00</td>
                <td>2017-07-27</td>
                
                <td><span class="label label-danger">Returned</span><br><br><br>
                  
                </td>
                <td></td>
            </tr>
            <tr align="center">
              <td>2017-07-27</td>
                <td>50236_5236 SC1234567890</td>
                <td>123456789</td>
                <td>SCBOX1904848500</td>
                <td><img src="https://www.stylecracker.com/sc_admin/assets/product_images/normal/14755855544341.jpg" width="40px;"></td>
                 <td>High heels in black</td>
                <td>BRID1235</td>
                <td>Only Two Feet</td>
                <td>39</td>
                <td>2,500.00</td>
                <td>2,000.00</td>
                <td>2017-07-27</td>
                
                <td><span class="label label-danger">Cancelled</span><br><br><br>
                  
                </td>
                 <td></td>
            </tr>
          </tbody>
        </table>
      </div>
     
        
      </div>
    </div>
  </div>
  <?php include('common/footer.php'); ?>
<script type="text/javascript">

      (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
      });

       // Fixed Header Example
      // --------------------
      (function() {
        // initialize datatable
        var table = $('#exampleFixedHeader').DataTable({
          responsive: true,
          "bPaginate": false,
          "sDom": "lfirtp",
        });

        // initialize FixedHeader
        var offsetTop = 0;
        if ($('.site-navbar').length > 0) {
          offsetTop = $('.site-navbar').eq(0).innerHeight();
        }
        var fixedHeader = new FixedHeader(table, {
          offsetTop: offsetTop
        });

        // redraw fixedHeaders as necessary
        $(window).resize(function() {
          fixedHeader._fnUpdateClones(true);
          fixedHeader._fnUpdatePositions();
        });
      })();

       })(document, window, jQuery);
    </script>

