 <style>
     .datepair-wrap {
      position: relative;
      overflow: hidden;
    }

    .animation-delay-100 {
      -webkit-animation-delay: 100ms;
      animation-delay: 100ms;
    }
    
    .animation-duration-300 {
      -webkit-animation-duration: 300ms;
      animation-duration: 300ms;
    }
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
    body{
      font-size: 12px !important;
    }
    .label{
      font-size: 84% !important;
    }
    .table>tbody>tr>td{
      padding: 5px !important;
      word-wrap: break-word;
    }
     .lightbox-block {
      max-width: 600px;
      padding: 15px 20px;
      margin: 40px auto;
      overflow: auto;
      background: #fff;
      border-radius: 3px;
    }
    .highlighted {
    background-color: #E5C37E;
}
  
    .badge{
      font-size: 10px;
      font-weight: 500;
      border-radius: 3px;
    }
    .cursor{
      cursor: pointer;
    }
    .alertify-cover {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1700;
    background-color: black;
    filter: alpha(opacity=0);
    opacity: 0.7;
} 
  .table a {
  text-decoration: underline;
  color: #57c7d4;
  font-weight: 500;
  cursor: pointer;
}
.step.col-md-4.current, .step.col-md-4.disabled{
  height: 100px;
}
.lightboxsize{
  max-width: 800px !important;
}
.radio-custom label::before {
   
    display:none;
   
}
.img-admin {
    
    width: 24px;
    padding: 4px;
    border:1px dashed #76838f;
}
.img-admin1 {
    
    width: 34px;
    padding: 4px;
    border:1px dashed #76838f;
}
img{
  width: 100%;
}
.table .cell-120 {
    width: 120px;
    display: inline-table;
    
}
@media (min-width: 768px)
{
.form-inline .radio-custom label {
     padding-left: 0px; 
}
}
.radio-custom {
     padding-left: 0px; 
}
.btn-sm{
  float: right;
}

 .new{
  top: -36px !important;
  margin: -8px 1.8em !important; 
 }
  </style>
  <!-- Inline -->
  <style>
    .animation-delay-100 {
      -webkit-animation-delay: 100ms;
      animation-delay: 100ms;
    }
    
    .animation-duration-300 {
      -webkit-animation-duration: 300ms;
      animation-duration: 300ms;
    }
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
    body{
      font-size: 12px !important;
    }
    .label{
      font-size: 84% !important;
    }
    .table>tbody>tr>td{
      padding: 5px !important;
      word-wrap: break-word;
    }
     .lightbox-block {
      max-width: 600px;
      padding: 15px 20px;
      margin: 40px auto;
      overflow: auto;
      background: #fff;
      border-radius: 3px;
    }
    .highlighted {
    background-color: #E5C37E;
}
  
    .badge{
      font-size: 10px;
      font-weight: 500;
      border-radius: 3px;
    }
    .cursor{
      cursor: pointer;
    }
    .alertify-cover {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1700;
    background-color: black;
    filter: alpha(opacity=0);
    opacity: 0.7;
} 
  .table a {
  text-decoration: underline;
  color: #57c7d4;
  font-weight: 500;
}
.table.dataTable tbody tr.active td, .table.dataTable tbody tr.active th {
        font-weight: 400;

}
.radio-custom label::before {
   
    display:none;
   
}
.img-admin {
    
    width: 24px;
    padding: 4px;
    border:1px dashed #76838f;
}
.img-admin1 {
    
    width: 34px;
    padding: 4px;
    border:1px dashed #76838f;
}
img{
  width: 100%;
}

  </style>
</head>
<body class="site-menubar-fold" data-auto-menubar="false">

  <?php include('common/nav_view.php'); ?>

 <!-- Page -->
  <div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">View/Edit</h1>
     <!--  <ol class="breadcrumb">
        <li><a href="../index.html">Home</a></li>
        <li><a href="javascript:void(0)">Forms</a></li>
        <li class="active">Editable</li>
      </ol> -->
    </div>
    <div class="page-content">
      <!-- Panel X-Editable -->
      <div class="panel">
        <div class="panel-body">
          <div>
            <button class="btn btn-primary btn-outline margin-bottom-10" id="editableEnable"
            type="button">enable / disable</button>
          </div>
        <div class="row">  
          <div class="table-responsive col-lg-6 col-md-6 col-sm-6">
            <table class="table table-bordered table-striped" id="editableUser">
              <header class="panel-heading">
                <h3 class="panel-title">
                  Product Basic
                </h3>
              </header>
              <tbody>
                <tr>
                  <td style="width:35%">Product Name</td>
                  <td style="width:65%">
                    <a id="editableName" href="javascript:void(0)" data-type="text" data-pk="1"
                    data-title="Enter Name">Name</a>
                  </td>
                </tr>
                <tr>
                  <td style="width:35%">Product Slug</td>
                  <td style="width:65%">
                    <a id="editableSlug" href="javascript:void(0)" data-type="text" data-pk="1"
                    data-title="Enter Slug">Slug</a>
                  </td>
                </tr>
                <tr>
                  <td style="width:35%">Product URL</td>
                  <td style="width:65%">
                    <a id="editableURL" href="javascript:void(0)" data-type="text" data-pk="1"
                    data-title="Enter URL">URL</a>
                  </td>
                </tr>
                <tr>
                  <td style="width:35%">Price</td>
                  <td style="width:65%">
                    <a id="editablePrice" href="javascript:void(0)" data-type="text" data-pk="1"
                    data-title="Enter Price">Price</a>
                  </td>
                </tr>
                 <tr>
                  <td style="width:35%">MRP Price</td>
                  <td style="width:65%">
                    <a id="editableMRP" href="javascript:void(0)" data-type="text" data-pk="1"
                    data-title="Enter MRP Price">MRP Price</a>
                  </td>
                </tr>
                <tr>
                  <td>Product Description</td>
                  <td>
                    <a id="editableDescription" href="javascript:void(0)" data-type="textarea" data-pk="1"
                    data-placeholder="Your comments here..." data-title="Enter Description">Description</a>
                  </td>
                </tr>
                <tr>
                <tr>
                  <td>Active</td>
                  <td>
                    <a id="editableActive" href="javascript:void(0)" data-type="select" data-pk="1" data-value=""
                    data-title="Select Active"></a>
                  </td>
                </tr>

                <tr>
                  <td>Discount Start Date</td>
                  <td>
                    <a id="editableStart" href="javascript:void(0)" data-type="combodate" data-value="1984-05-15"
                    data-format="YYYY-MM-DD" data-viewformat="DD/MM/YYYY" data-template="D / MMM / YYYY"
                    data-pk="1" data-title="Select Discount Start Date"></a>
                  </td>
                </tr>
                <tr>
                  <td>Discount End Date</td>
                  <td>
                    <a id="editableEnd" href="javascript:void(0)" data-type="combodate" data-value="1984-05-15"
                    data-format="YYYY-MM-DD" data-viewformat="DD/MM/YYYY" data-template="D / MMM / YYYY"
                    data-pk="1" data-title="Select Discount End Date"></a>
                  </td>
                </tr>
                <tr>
                  <td>Store</td>
                  <td>
                    <a id="editableStore" href="javascript:void(0)" data-type="select" data-pk="1" data-value=""
                    data-title="Select Store"></a>
                  </td>
                </tr>
                <tr>
                  <td>Brand</td>
                  <td>
                    <a id="editableBrand" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Brand"></a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="table-responsive col-lg-6 col-md-6 col-sm-6">
            <table class="table table-bordered table-striped" id="editableUser">
              <header class="panel-heading">
                <h3 class="panel-title">
                  Product Grouping
                </h3>
              </header>
              <tbody>
                <tr>
                  <td style="width:35%">Category</td>
                  <td style="width:65%">
                    <a id="editableCategory" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Category"></a>
                  </td>
                </tr>
                 <tr>
                  <td style="width:35%">Tags</td>
                  <td style="width:65%">
                    <a id="editableTags" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Tags"></a>
                  </td>
                </tr>
                <tr>
                  <td style="width:35%">Personality</td>
                  <td style="width:65%">
                    <a id="editablePersonality" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Personality"></a>
                  </td>
                </tr>
                 <tr>
                  <td style="width:35%">Age</td>
                  <td style="width:65%">
                    <a id="editableAge" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Age"></a>
                  </td>
                </tr>
                <tr>
                  <td style="width:35%">Budget</td>
                  <td style="width:65%">
                    <a id="editableBudget" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Budget"></a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="table-responsive col-lg-6 col-md-6 col-sm-6">
            <table class="table table-bordered table-striped" id="editableUser">
              <header class="panel-heading">
                <h3 class="panel-title">
                  Product Attribute Mapping
                </h3>
              </header>
              <tbody>
                <tr>
                  <td style="width:35%">Material</td>
                  <td style="width:65%">
                    <a id="editableMaterial" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Material"></a>
                  </td>
                </tr>
                 <tr>
                  <td style="width:35%">Colour</td>
                  <td style="width:65%">
                    <a id="editableColour" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Colour"></a>
                  </td>
                </tr>
                <tr>
                  <td style="width:35%">Details</td>
                  <td style="width:65%">
                    <a id="editableDetails" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Details"></a>
                  </td>
                </tr>
                 <tr>
                  <td style="width:35%">Prints </td>
                  <td style="width:65%">
                    <a id="editablePrints " href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Prints "></a>
                  </td>
                </tr>
                <tr>
                  <td style="width:35%">Neck</td>
                  <td style="width:65%">
                    <a id="editableNeck" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Neck"></a>
                  </td>
                </tr>
                 <tr>
                  <td style="width:35%">Silhoutte</td>
                  <td style="width:65%">
                    <a id="editableSilhoutte" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Silhoutte"></a>
                  </td>
                </tr>
                 <tr>
                  <td style="width:35%">Sleeve Length</td>
                  <td style="width:65%">
                    <a id="editableSleeveLength" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Sleeve Length"></a>
                  </td>
                </tr>
                <tr>
                  <td style="width:35%">Length</td>
                  <td style="width:65%">
                    <a id="editableLength" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Length"></a>
                  </td>
                </tr>
                <tr>
                  <td style="width:35%">Collar</td>
                  <td style="width:65%">
                    <a id="editableCollar" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Collar"></a>
                  </td>
                </tr>
                 <tr>
                  <td style="width:35%">Waist</td>
                  <td style="width:65%">
                    <a id="editableWaist" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Waist"></a>
                  </td>
                </tr>
                <tr>
                  <td style="width:35%">Dress Length</td>
                  <td style="width:65%">
                    <a id="editableDressLength" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Dress Length"></a>
                  </td>
                </tr>
                 <tr>
                  <td style="width:35%">Dress Prints</td>
                  <td style="width:65%">
                    <a id="editableDressPrints" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Dress Prints"></a>
                  </td>
                </tr>
                 <tr>
                  <td style="width:35%">Skirts Length</td>
                  <td style="width:65%">
                    <a id="editableSkirtsLength" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Skirts Length"></a>
                  </td>
                </tr>
                <tr>
                  <td style="width:35%">Neck</td>
                  <td style="width:65%">
                    <a id="editableNecks" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Neck"></a>
                  </td>
                </tr>
                <tr>
                  <td style="width:35%">Fit</td>
                  <td style="width:65%">
                    <a id="editableFit" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Fit"></a>
                  </td>
                </tr>
                <tr>
                  <td style="width:35%">Occasions</td>
                  <td style="width:65%">
                    <a id="editableOccasions" href="javascript:void(0)" data-type="checklist" data-value="2,3"
                    data-title="Select Occasions"></a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
           <div class="table-responsive col-lg-6 col-md-6 col-sm-6">
            <div class="row">
            <div class="table-responsive col-lg-12 col-md-12 col-sm-12">
              <table class="table table-bordered table-striped" id="editableUser">
                <header class="panel-heading">
                  <h3 class="panel-title">
                    Product
                  </h3>
                </header>
                <tbody>
                  <tr>
                    <td style="width:35%">Target Market</td>
                    <td style="width:65%">
                      <a id="editableMarket" href="javascript:void(0)" data-type="select" data-pk="1" data-value=""
                      data-title="Select Market"></a>
                    </td>
                  </tr>
                   <tr>
                    <td style="width:35%">Consumer Type</td>
                    <td style="width:65%">
                      <a id="editableConsumer" href="javascript:void(0)" data-type="select" data-pk="1" data-value=""
                      data-title="Select Consumer"></a>
                    </td>
                  </tr>
                 <tr>
                    <td style="width:35%">Rating</td>
                    <td style="width:65%">
                      <a id="editableRating" href="javascript:void(0)" data-type="text" data-pk="1"
                      data-title="Enter Rating">Rating</a>
                    </td>
                  </tr>
                </tbody>
                </table>
              </div>
              <div class="table-responsive col-lg-12 col-md-12 col-sm-12">
              <table class="table table-bordered table-striped" id="editableUser">
                <header class="panel-heading">
                  <h3 class="panel-title">
                    Product Inventory
                  </h3>
                </header>
                <tbody>
                  <tr>
                    <td><input id="btnAdd" type="button" value="Add Products" class="btn btn-success btn-xs" /><br>
                      <div id="TextBoxContainer">
                         <!--Textboxes will be added here -->
                      </div>
                    </td>
                  </tr>
                </tbody>
                </table>
              </div>
            </div>
            </div>
        </div>

        </div>
      </div>
      <!-- End Panel X-Editable -->

    </div>
  </div>
  <!-- End Page -->
<style type="text/css">
  .inside-container input{
    margin-right: 5px;
  }
</style>
<?php include('common/footer.php'); ?>
  

   <script>
    $(function () {
      $("#btnAdd").bind("click", function () {
          var div = $("<div />");
          div.html(GetDynamicTextBox(""));
          $("#TextBoxContainer").append(div);
      });
      $("#btnGet").bind("click", function () {
          var values = "";
          $("input[name=Size]").each(function () {
              values += $(this).val() + "\n";
          });
          $("input[name=Qty]").each(function () {
              values += $(this).val() + "\n";
          });
          $("input[name=Sku]").each(function () {
              values += $(this).val() + "\n";
          });
         
      });
      $("body").on("click", ".remove", function () {
          $(this).closest("div").remove();
      });
  });
  function GetDynamicTextBox(value) {
      return  '<div class="inside-container" style="margin:10px;">'+
              '<input name = "Size" type="text" value = "' + value + '" />' +
              '<input name = "Qty" type="text" value = "' + value + '" />' +
              '<input name = "Sku" type="text" value = "' + value + '" />' +
              '<input type="button" value="Remove" class="remove btn btn-danger btn-xs" />'+
              '</div>'
  }

    (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
        //enable / disable
        $('#editableEnable').click(function() {
          $('#editableUser .editable').editable('toggleDisabled');
      });

      var init_x_editable = function() {

          $.fn.editableform.buttons =
            '<button type="submit" class="btn btn-primary btn-sm editable-submit">' +
            '<i class="icon wb-check" aria-hidden="true"></i>' +
            '</button>' +
            '<button type="button" class="btn btn-default btn-sm editable-cancel">' +
            '<i class="icon wb-close" aria-hidden="true"></i>' +
            '</button>';

          $.fn.editabletypes.datefield.defaults.inputclass =
            "form-control input-sm";

          //defaults
          $.fn.editable.defaults.url = '/post';

          //editables
          $('#editableName').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'product_name',
            title: 'Enter Name'
          });

          $('#editableRating').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'rating',
            title: 'Enter Rating'
          });

          $('#editableSlug').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'product_slug',
            title: 'Enter Slug'
          });

          $('#editableURL').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'product_url',
            title: 'Enter URL'
          });

          $('#editablePrice').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'product_price',
            title: 'Enter Price'
          });

          $('#editableMRP').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'product_mrp',
            title: 'Enter MRP'
          });


          $('#editableActive').editable({
            prepend: "not selected",
            source: [{
              value: 1,
              text: 'Yes'
            }, {
              value: 2,
              text: 'No'
            }],
           
          });

           $('#editableStore').editable({
            prepend: "not selected",
            source: [{
              value: 1,
              text: 'Yes'
            }, {
              value: 2,
              text: 'No'
            }],
           
          });



          $('#editableDescription').editable({
            showbuttons: 'bottom'
          });

          

          $('#editableStart').editable({
            showbuttons: 'bottom'
          });

          $('#editableEnd').editable({
            showbuttons: 'bottom'
          });


          $('#editableBrand,#editableBudget,#editablePersonality,#editableAge,#editableCategory,#editableTags,#editableMarket,#editableConsumer,#editableMaterial,#editableColour,#editableDetails,#editablePrints,#editableNecks,#editableNeck,#editableSilhoutte,#editableSleeveLength,#editableSkirtsLength,#editableLength,#editableCollar,#editableWaist,editableDressPrints,#editableDressLength,#editableFit,#editableOccasions').editable({
            pk: 1,
            limit: 10,
            source: [{
              value: 1,
              text: 'banana'
            }, {
              value: 2,
              text: 'peach'
            }, {
              value: 3,
              text: 'apple'
            }, {
              value: 4,
              text: 'watermelon'
            }, {
              value: 5,
              text: 'orange'
            }]
          });

          

          // $("#editableUser").find(".form-control").addClass(".input-sm");
        };

        var destory_x_editable = function() {
          $('#editableSuperuser').editable('destroy');
          $('#editableFirstname').editable('destroy');
          $('#editableSex').editable('destroy');
          // $('#editableStatus').editable('destroy');
          $('#editableVacation').editable('destroy');
          $('#editableDob').editable('destroy');
          $('#editableEvent').editable('destroy');
          $('#editableMeetingStart').editable('destroy');
          $('#editableComments').editable('destroy');
          $('#editableNote').editable('destroy');
          $('#editablePencil').editable('destroy');
          $('#editableState').editable('destroy');
          $('#editableState2').editable('destroy');
          $('#editableFruits').editable('destroy');
          $('#editableAddress').editable('destroy');
        };

        $.fn.editable.defaults.mode = 'inline';
        init_x_editable();

        // $('#editableControls').on("click", "label", function() {
        //   xMode = $(this).find("input").val();
        //   $.fn.editable.defaults.mode = xMode;
        //   destory_x_editable();
        //   init_x_editable();
        // });
      });

// Example File Upload
      // -------------------
      $('#exampleUploadForm').fileupload({
        url: '../../server/fileupload/',
        dropzone: $('#exampleUploadForm'),
        filesContainer: $('.file-list'),
        uploadTemplateId: false,
        downloadTemplateId: false,
        uploadTemplate: tmpl(
          '{% for (var i=0, file; file=o.files[i]; i++) { %}' +
          '<div class="file template-upload fade col-lg-2 col-md-4 col-sm-6 {%=file.type.search("image") !== -1? "image" : "other-file"%}">' +
          '<div class="file-item">' +
          '<div class="preview vertical-align">' +
          '<div class="file-action-wrap">' +
          '<div class="file-action">' +
          '{% if (!i && !o.options.autoUpload) { %}' +
          '<i class="icon wb-upload start" data-toggle="tooltip" data-original-title="Upload file" aria-hidden="true"></i>' +
          '{% } %}' +
          '{% if (!i) { %}' +
          '<i class="icon wb-close cancel" data-toggle="tooltip" data-original-title="Stop upload file" aria-hidden="true"></i>' +
          '{% } %}' +
          '</div>' +
          '</div>' +
          '</div>' +
          '<div class="info-wrap">' +
          '<div class="title">{%=file.name%}</div>' +
          '</div>' +
          '<div class="progress progress-striped active" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" role="progressbar">' +
          '<div class="progress-bar progress-bar-success" style="width:0%;"></div>' +
          '</div>' +
          '</div>' +
          '</div>' +
          '{% } %}'
        ),
        downloadTemplate: tmpl(
          '{% for (var i=0, file; file=o.files[i]; i++) { %}' +
          '<div class="file template-download fade col-lg-2 col-md-4 col-sm-6 {%=file.type.search("image") !== -1? "image" : "other-file"%}">' +
          '<div class="file-item">' +
          '<div class="preview vertical-align">' +
          '<div class="file-action-wrap">' +
          '<div class="file-action">' +
          '<i class="icon wb-trash delete" data-toggle="tooltip" data-original-title="Delete files" aria-hidden="true"></i>' +
          '</div>' +
          '</div>' +
          '<img src="{%=file.url%}"/>' +
          '</div>' +
          '<div class="info-wrap">' +
          '<div class="title">{%=file.name%}</div>' +
          '</div>' +
          '</div>' +
          '</div>' +
          '{% } %}'
        ),
        forceResize: true,
        previewCanvas: false,
        previewMaxWidth: false,
        previewMaxHeight: false,
        previewThumbnail: false
      }).on('fileuploadprocessalways', function(e, data) {
        var length = data.files.length;

        for (var i = 0; i < length; i++) {
          if (!data.files[i].type.match(
              /^image\/(gif|jpeg|png|svg\+xml)$/)) {
            data.files[i].filetype = 'other-file';
          } else {
            data.files[i].filetype = 'image';
          }
        }
      }).on('fileuploadadded', function(e) {
        var $this = $(e.target);

        if ($this.find('.file').length > 0) {
          $this.addClass('has-file');
        } else {
          $this.removeClass('has-file');
        }
      }).on('fileuploadfinished', function(e) {
        var $this = $(e.target);

        if ($this.find('.file').length > 0) {
          $this.addClass('has-file');
        } else {
          $this.removeClass('has-file');
        }
      }).on('fileuploaddestroyed', function(e) {
        var $this = $(e.target);

        if ($this.find('.file').length > 0) {
          $this.addClass('has-file');
        } else {
          $this.removeClass('has-file');
        }
      }).on('click', function(e) {
        if ($(e.target).parents('.file').length === 0) $('#inputUpload')
          .trigger('click');
      });

      $(document).bind('dragover', function(e) {
        var dropZone = $('#exampleUploadForm'),
          timeout = window.dropZoneTimeout;
        if (!timeout) {
          dropZone.addClass('in');
        } else {
          clearTimeout(timeout);
        }
        var found = false,
          node = e.target;
        do {
          if (node === dropZone[0]) {
            found = true;
            break;
          }
          node = node.parentNode;
        } while (node !== null);
        if (found) {
          dropZone.addClass('hover');
        } else {
          dropZone.removeClass('hover');
        }
        window.dropZoneTimeout = setTimeout(function() {
          window.dropZoneTimeout = null;
          dropZone.removeClass('in hover');
        }, 100);
      });

      $('#inputUpload').on('click', function(e) {
        e.stopPropagation();
      });

      $('#uploadlink').on('click', function(e) {
        e.stopPropagation();
      });
    })(document, window, jQuery);
  </script>

</body>

</html>