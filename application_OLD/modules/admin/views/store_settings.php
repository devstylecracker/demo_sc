

  <!-- Inline -->
  <style>
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
  </style>

</head>
<body class="site-menubar-fold" data-auto-menubar="false">
  <?php include('common/nav_view.php'); ?>

  <!-- Page -->
  <div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">Store Settings</h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li class="active">Store Settings</li>
      </ol>
    </div>
   <div class="page-content">
      <!-- Panel X-Editable -->
      <div class="panel">
      	<header class="panel-heading">
          <h3 class="panel-title">
            Store Settings
            <span class="panel-desc">Click to edit.</span>
          </h3>
        </header>
        <div class="panel-body">
          <button class="btn btn-primary btn-outline margin-bottom-10" id="editableEnable"
          type="button">enable / disable</button>
          <div class="table-responsive row">
	          <div class="col-lg-6 col-md-6 col-sm-12">	
	            <table class="table table-bordered table-striped" id="editableUser">
	              <tbody>
	              	 <tr>
	                  <td>Settings</td>
	                  <td>
	                    <a id="settings" href="javascript:void(0)" data-type="checklist" data-value="2,3"
	                    data-title="Select Settings"></a>
	                  </td>
	                </tr>
	                <tr>
	                  <td>On-board Date</td>
	                  <td>
	                    <a id="on_date" href="javascript:void(0)" data-type="combodate" data-value="1984-05-15"
	                    data-format="YYYY-MM-DD" data-viewformat="DD/MM/YYYY" data-template="D / MMM / YYYY"
	                    data-pk="1" data-title="Select On-board Date"></a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td>Contract end Date</td>
	                  <td>
	                    <a id="end_date" href="javascript:void(0)" data-type="combodate" data-value="1984-05-15"
	                    data-format="YYYY-MM-DD" data-viewformat="DD/MM/YYYY" data-template="D / MMM / YYYY"
	                    data-pk="1" data-title="Select Contract end Date"></a>
	                  </td>
	                </tr>
	                <tr>
	                  <td>Brand CPA (%)</td>
	                  <td>
	                    <a id="brand_cpa" href="javascript:void(0)" data-type="text" data-value="1"
	                    data-title="Enter Brand CPA"></a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td>Brand Tax (%)</td>
	                  <td>
	                    <a id="brand_tax" href="javascript:void(0)" data-type="text" data-value="1"
	                    data-title="Enter Brand Tax (%)"></a>
	                  </td>
	                </tr>
	                <tr>
	                  <td>Payment Gateway charges (%)</td>
	                  <td>
	                    <a id="payment_gateway" href="javascript:void(0)" data-type="text" data-value="1"
	                    data-title="Enter Payment Gateway charges"></a>
	                  </td>
	                </tr>
	                <tr>
	                  <td>Service Tax (%)</td>
	                  <td>
	                    <a id="service_tax" href="javascript:void(0)" data-type="text" data-value="1"
	                    data-title="Enter Service Tax"></a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td>Integration Charges (%)</td>
	                  <td>
	                    <a id="integration" href="javascript:void(0)" data-type="text" data-value="1"
	                    data-title="Enter Integration Charges (%)"></a>
	                  </td>
	                </tr>

	                <tr>
	                  <td style="width:35%">Brand Initials</td>
	                  <td style="width:65%">
	                    <a id="brand_initials" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter Brand Initials">brand Initials</a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td style="width:35%">VAT/TIN Number</td>
	                  <td style="width:65%">
	                    <a id="vat_tin" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter VAT/TIN Number">102146516544165</a>
	                  </td>
	                </tr>
	                <tr>
	                  <td style="width:35%">Brand Registration ID</td>
	                  <td style="width:65%">
	                    <a id="registration" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter Brand Registration ID">102146516544165</a>
	                  </td>
	                </tr>
	                <tr>
	                  <td style="width:35%">CST Number</td>
	                  <td style="width:65%">
	                    <a id="cst_num" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter CST Number">102146516544165</a>
	                  </td>
	                </tr>
	                
	              </tbody>
	            </table>
	        </div>
	        <div class="col-lg-6 col-md-6 col-sm-12">
	        	<table class="table table-bordered table-striped" id="editableUser">
	              <tbody>
	               <tr>
	                  <td>COD Available</td>
	                  <td>
	                    <a id="cod" href="javascript:void(0)" data-type="select" data-pk="1" data-value=""
	                    data-title="Select COD Available"></a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td style="width:35%">COD Charges</td>
	                  <td style="width:65%">
	                    Amount:<a id="cod_1" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter Cod Charges">100</a>
	                    Min:<a id="cod_2" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter Cod Charges">50</a>
	                    Max:<a id="cod_3" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter Cod Charges">130</a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td>Non-Area Shipping Location</td>
	                  <td>
	                    <a id="non_area_ship" href="javascript:void(0)" data-type="textarea" data-pk="1"
	                    data-placeholder=" e.g (123456,678907,124568)" data-title="Enter Non-Area Shipping Location">Enter Non-Area Shipping Location</a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td>Non-Area COD Location</td>
	                  <td>
	                    <a id="non_area_cod" href="javascript:void(0)" data-type="textarea" data-pk="1"
	                    data-placeholder=" e.g (123456,678907,124568)" data-title="Enter Non-Area COD Location">Non-Area COD Location</a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td style="width:35%">Product Delivery Days</td>
	                  <td style="width:65%">
	                   Min: <a id="delivery_1" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter Min Product Delivery">10</a> to 
	                   Max: <a id="delivery_2" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter Max Product Delivery">30</a>
	                  </td>
	                </tr>
	                <tr>
	                  <td>Office Address</td>
	                  <td>
	                    <a id="address_1" href="javascript:void(0)" data-type="address" data-pk="1"
	                    data-title="Please, fill address"></a>
	                  </td>
	                </tr>
	                 <tr>
	                  <td>Store Address</td>
	                  <td>
	                    <a id="address_2" href="javascript:void(0)" data-type="address" data-pk="1"
	                    data-title="Please, fill address"></a>
	                  </td>
	                </tr>
	                <tr>
	                  <td style="width:35%">Email</td>
	                  <td style="width:65%">
	                    <a id="email" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter Email">Email</a>
	                  </td>
	                </tr>
	                  <td style="width:35%">Enquiry Email ID</td>
	                  <td style="width:65%">
	                    <a id="en_email" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter Enquiry Email ID">Enquiry Email ID</a>
	                  </td>
	                </tr>
	                </tr>
	                  <td style="width:35%">Enquiry Phone No.</td>
	                  <td style="width:65%">
	                    <a id="en_phone" href="javascript:void(0)" data-type="text" data-pk="1"
	                    data-title="Enter Enquiry Phone No.">Enquiry Phone No.</a>
	                  </td>
	                </tr>
	               <tr>
	                  <td>Return Policy</td>
	                  <td>
	                    <a id="return_policy" href="javascript:void(0)" data-type="textarea" data-pk="1"
	                    data-placeholder="Your comments here..." data-title="Return Policy">Return Policy</a>
	                  </td>
	                </tr>
	                <tr>
	                  <td>Commission model</td>
	                  <td>
	                    <a id="commision_model" href="javascript:void(0)" data-type="select" data-pk="1" data-value=""
	                    data-title="Select Commission model"></a>
	                  </td>
	                </tr>
	                 <tr>
	                	<td>
	                		Upload Signature
	                	</td>
	                	<td>
	                		<div class="form-group">
			                  <div class="input-group input-group-file">
			                    <input type="text" class="form-control" readonly="">
			                    <span class="input-group-btn">
			                      <span class="btn btn-success btn-file">
			                        <i class="icon wb-upload" aria-hidden="true"></i>
			                        <input type="file" name="" multiple="">
			                      </span>
			                    </span>
			                  </div>
			                </div>
	                	</td>
	                </tr>
	              </tbody>
	            </table>
	        </div>
          </div>
        </div>
      </div>
      <!-- End Panel X-Editable -->

    </div>
  </div>
  <!-- End Page -->

<?php include('common/footer.php'); ?>


 <script>
    (function(document, window, $) {
      'use strict';

      var Site = window.Site;

      $(document).ready(function($) {
        Site.run();
        //enable / disable
        $('#editableEnable').click(function() {
          $('#editableUser .editable').editable('toggleDisabled');
        });

        var init_x_editable = function() {

          $.fn.editableform.buttons =
            '<button type="submit" class="btn btn-primary btn-sm editable-submit">' +
            '<i class="icon wb-check" aria-hidden="true"></i>' +
            '</button>' +
            '<button type="button" class="btn btn-default btn-sm editable-cancel">' +
            '<i class="icon wb-close" aria-hidden="true"></i>' +
            '</button>';

          $.fn.editabletypes.datefield.defaults.inputclass =
            "form-control input-sm";

          //defaults
          $.fn.editable.defaults.url = '/post';

          //editables
          $('#brand_cpa').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'brand_cpa',
            title: 'Enter Brand CPA'
          }); 

           $('#brand_tax').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'brand_tax',
            title: 'Enter Brand Tax (%)'
          }); 

           $('#payment_gateway').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'payment_gateway',
            title: 'Enter Payment Gateway charges'
          });  

$('#service_tax').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'service_tax',
            title: 'Enter Service Tax'
          });  

$('#integration').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'integration',
            title: 'Enter Integration charges'
          });  

          $('#brand_initials').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'brand_initials',
            title: 'Enter Brand Initials'
          });

          $('#vat_tin').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'vat_tin',
            title: 'Enter VAT/TIN Number'
          }); 

          $('#cst_num').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'cst_num',
            title: 'Enter CST Number'
          });  

           $('#email').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'email',
            title: 'Enter Contact Email'
          });

      

          $('#en_phone').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'en_phone',
            title: 'Enter Enquiry Phone'
          });

           $('#cod_1').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'cod_1',
            title: 'Enter Price'
          });

           $('#cod_2').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'cod_2',
            title: 'Enter Min Price'
          });

           $('#cod_3').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'cod_3',
            title: 'Enter Max Price'
          });

          $('#en_email').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'en_email',
            title: 'Enter Enquiry email'
          });

          $('#on_date, #end_date').editable();

          $('#delivery_2').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'delivery_2',
            title: 'Enter Min Product Delivery'
          });

          $('#delivery_1').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'delivery_1',
            title: 'Enter Min Product Delivery'
          });

         

           $('#commision_model').editable({
            prepend: "not selected",
            source: [{
              value: 1,
              text: 'All exclusive'
            }, {
              value: 2,
              text: 'Flat Commission model'
            },
              {
              value: 3,
              text: ' Flat Commission excluding taxes'
            }

            ]
          });

           $('#registration').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'registration',
            title: 'Enter registration no.'
          });

          $('#email').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'email',
            title: 'Enter email'
          });

           $('#cod').editable({
            prepend: "not selected",
            source: [{
              value: 1,
              text: 'Yes'
            }, {
              value: 2,
              text: 'No'
            }

            ]
          });

          $('#return_policy, #non_area_ship, #non_area_cod').editable({
            showbuttons: 'bottom'
          });

          

          $('#settings').editable({
            pk: 1,
            limit: 3,
            source: [{
              value: 1,
              text: 'Paid Brand'
            }, {
              value: 2,
              text: 'Show on Web'
            }]
          });


          $('#address_1,#address_2').editable({
          	showbuttons: 'bottom',
            url: '/post',
            value: {
              city: "Moscow",
              street: "Lenina",
              state: "Assam",
              pincode: "302022",
              country: "India"
            },
            validate: function(value) {
              if (value.city === '') return 'city is required!';
            },
            display: function(value) {
              if (!value) {
                $(this).empty();
                return;
              }
              var html = '<b>' + $('<div>').text(value.city).html() +
                '</b>, ' + $('<div>').text(value.street).html() +
                '</b>, ' + $('<div>').text(value.state).html() +
                '</b>, ' + $('<div>').text(value.pincode).html() +  
                '</b>, ' + $('<div>').text(value.country)
                .html();   
              $(this).html(html);
            }
          });

          // $("#editableUser").find(".form-control").addClass(".input-sm");
        };

        var destory_x_editable = function() {
          $('#brand_cpa').editable('destroy');
          $('#brand_tax').editable('destroy');
          $('#payment_gateway').editable('destroy');
          $('#service_tax').editable('destroy');
          $('#integration').editable('destroy');
          $('#brand_initials').editable('destroy');
          $('#vat_tin').editable('destroy');
          $('#cst_num').editable('destroy');
          $('#email').editable('destroy');
          $('#en_phone').editable('destroy');
          $('#cod_1').editable('destroy');
          $('#cod_2').editable('destroy');
          $('#cod_3').editable('destroy');
          $('#en_email').editable('destroy');
          $('#registration').editable('destroy');

          $('#email').editable('destroy');
          $('#delivery_1').editable('destroy');
          $('#delivery_2').editable('destroy');
          $('#commision_model').editable('destroy');
          $('#cod').editable('destroy');
          $('#on_date').editable('destroy');
          $('#end_date').editable('destroy');
          $('#return_policy').editable('destroy');

          $('#non_area_ship').editable('destroy');
          $('#non_area_cod').editable('destroy');
          $('#settings').editable('destroy');
          $('#address_1').editable('destroy');
          $('#address_2').editable('destroy');
        };

        $.fn.editable.defaults.mode = 'inline';
        init_x_editable();

        // $('#editableControls').on("click", "label", function() {
        //   xMode = $(this).find("input").val();
        //   $.fn.editable.defaults.mode = xMode;
        //   destory_x_editable();
        //   init_x_editable();
        // });
      });
    })(document, window, jQuery);
  </script>

</body>

</html>