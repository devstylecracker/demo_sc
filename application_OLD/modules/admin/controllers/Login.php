<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
    {
        // Call the Model constructor        
        parent::__construct();
        $this->load->library('bcrypt');
        $this->load->library('form_validation');
        $this->load->model('login_model');
        $this->load->model('Userregister_model');
        $this->load->model('Schome_model');
    }
    
	public function index()
	{ 
		if(!$this->session->userdata('user_id')){
			$this->load->view('common/header');
			$this->load->view('login');
		}else{ 
			$this->load->view('common/header');
			$this->load->view('home');
			//$this->load->view('common/footer');
		}
	}

	
	
	public function check_login(){ 
		
			$this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
		
			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('common/header');
				$this->load->view('login');
			}
			else
			{
				 $email 		= $this->input->post('email');
				 $password 		= $this->input->post('password');
				 $remember_me 	= $this->input->post('remember_me');
				
				 if($remember_me == 1){
				 	setcookie('7572ce8a49ff8982ba4d17f9e56b6d1f',$this->encryptOrDecrypt($email,'encrypt'), time() + (86400 * 30), "/");
				 	setcookie('a6c16340e402e946c64ce53bb41ac24a',$this->encryptOrDecrypt($password,'encrypt'), time() + (86400 * 30), "/");				 	
				 }

				$res = $this->login_model->login_user($email,$password); 
				 //$res = $this->login_model->verify_login($email,$password);
				 if($res == 1){ 
						redirect('admin/categories');
				 } else{
						$data['error'] = "Invalid Username and Password";
						$this->load->view('common/header');
						$this->load->view('login',$data); 
				 }
				 
			}

		
	}
	

  
  public function forgotPassword()
	{
		$data	= array();
		
		$validation_config = array(
               array(
                     'field'   => 'emailid', 
                     'label'   => 'Email Id', 
                     'rules'   => 'trim|required'
                  )
            );

		$this->form_validation->set_rules($validation_config);
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('common/header');
			$this->load->view('forget-password',$data); 
		}
		else
		{
			$emailid 		= $this->input->post('emailid');
			$result = $this->Schome_model->forgot_password($emailid);	
			//echo $this->db->last_query();
			if($result != false){
               if($this->send_forgotpwd_email($emailid,$result))
               {
               // echo 'New Password has been Send to your email-id '.$emailid;
				$this->session->set_flashdata('success','New password has been sent to <br />'.$emailid); 
				redirect('admin/login');
               }
			   else
               {
				$this->session->set_flashdata('error','Email Error'); 
				redirect('admin/login/forgotPassword');
               }

            }else{
			  $this->session->set_flashdata('error','Invalid Email-id'); 
			  redirect('admin/login/forgotPassword');
            }			
		}
	}
	
	public function send_forgotpwd_email($email_id,$new_password){

      $config['protocol'] = 'smtp';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');

      $this->email->initialize($config);

      $this->email->from($this->config->item('from_email'), 'Stylecracker');
      $this->email->to($email_id);
      $this->email->subject('StyleCracker: New Password');
      $this->email->message("Your login details are <br> Email : ".$email_id."<br><br>Your new password is:".$new_password."<br><br>You can change your password at any time by logging into your account. <br><br>Warm Regards,<br>StyleCracker");

      if($this->email->send())
      {
        return "Email Sent";
      }else
      {
        return "Email Error";
      }

  }

 
	
	
	public function logout(){
		session_destroy();
		delete_cookie("7572ce8a49ff8982ba4d17f9e56b6d1f");
		delete_cookie("a6c16340e402e946c64ce53bb41ac24a");
		redirect('/admin');
		
		}
}
