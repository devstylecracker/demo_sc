<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Categories_model');
    }

  
    public function index()
    {
        $this->categories_add();
    }

    public function categories_add($page_no='0')
    {
        $data = array();
        $validation_config = array
            (
                array(
                        'field'=>'term_name',
                        'label'=>'Term Name',
                        'rules'=>'trim|required',
                    ),
                array(
                        'field'=>'term_slug',
                        'label'=>'Term Slug',
                        'rules'=>'trim|required',
                    ),
                array(
                        'field'=>'term_parent',
                        'label'=>'Term Parent',
                        'rules'=>'trim|required',
                    ),
                  array(
                        'field'=>'term_description',
                        'label'=>'Term Description',
                        'rules'=>'trim|required',
                    ),
                  array(
                        'field'=>'termtax_type',
                        'label'=>'Term Taxanomy Type',
                        'rules'=>'trim|required',
                    ),
                   array(
                        'field'=>'sort_order',
                        'label'=>'Sort Order',
                        'rules'=>'trim|required',
                    ),
            );
            $data['scontent']           = array();
            $offset                     = ($this->uri->segment(3) != '' ? $this->uri->segment(3): 0);
            $per_page                   = 20;
            $numRows                    = $this->Categories_model->get_total_cat($data['scontent']);
            //echo $this->db->last_query();
            $arrResult                  = array();
            if($numRows > 0)
            {
                $arrResult              = $this->Categories_model->get_cat($per_page, $page_no,$data['scontent']);
            }
           // echo $this->db->last_query();
            $this->load->library("pagination");
            $config['total_rows']       = $numRows;
            $config['per_page']         = $per_page;
            $config['next_link']        = '<span aria-hidden="true">»</span><span class="sr-only">Next</span>';
            $config['prev_link']        = '<span aria-hidden="true">«</span><span class="sr-only">Prev</span>';
            $config['uri_segment']      = 4;
            $config['base_url']         = base_url().'/admin/categories/categories_add';
            
            if(isset($_GET))
            {
                $config['first_url']    = $config['base_url'].'/1'.'?'.http_build_query($_GET, '', "&");    
            }
             
            $config['full_tag_open']    = "<ul class='pagination pagination-sm no-margin pull-right'>";
            $config['full_tag_close']   ="</ul>";
            $config['num_tag_open']     = '<li>';
            $config['num_tag_close']    = '</li>';
            $config['cur_tag_open']     = "<li class='active'><a href='javascript:void(0);'>";
            $config['cur_tag_close']    = "<span class='sr-only'></span></a></li>";
            $config['next_tag_open']    = "<li>";
            $config['next_tag_close']  = "</li>";
            $config['prev_tag_open']    = "<li>";
            $config['prev_tag_close']  = "</li>";
            $config['first_tag_open']   = "<li>";
            $config['first_tag_close'] = "</li>";
            $config['last_tag_open']    = "<li>";
            $config['last_tag_close']  = "</li>";
            if(isset($_GET))
            {
                $config['suffix']           = '?'.http_build_query($_GET, '', "&");
            }
            
            $config['use_page_numbers'] = TRUE; 
            $this->pagination->initialize($config);
        
        
        $data['paginglinks']        = $this->pagination->create_links();
        // Showing total rows count 
        if($data['paginglinks']!= '') {
          $data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$this->pagination->per_page)+1).' to '.(( $numRows > $this->pagination->cur_page*$this->pagination->per_page)?$this->pagination->cur_page*$this->pagination->per_page:$numRows).' of '.$numRows;
        } 

            $data['cat_data']           = $this->Categories_model->get_cat_data();            
            $data['get_cat']            = $arrResult;
            $data['page_no']            = $page_no;
            $this->form_validation->set_rules($validation_config);
            if($this->form_validation->run() == FALSE)
            {
                $this->load->view('common/header');
                $this->load->view('categories',$data);
                
            }
            else
            {
                $term_name      = $this->input->post('term_name');
                $term_slug      = $this->input->post('term_slug');
                $term_parent    = $this->input->post('term_parent');
               if($term_name!='')
                    {  
                        $term_data['term_name'] = $term_name;               
                        if($this->Categories_model->get_term_slug($term_parent)!='')
                        {
                            $term_slug = $this->Categories_model->get_term_slug($term_parent).'-'.strtolower($term_slug);
                        }
                        else
                        {
                            $term_slug = strtolower($term_slug);
                        }     

                    }
                

                $arrAddTermData = array();
                $arrAddTermData['term_name']           = $term_name;
                $arrAddTermData['term_slug']           = $term_slug;
                $arrAddTermData['term_group']          = $this->input->post('termtax_type');
                $arrAddTermData['created_datetime']    = date('Y-m-d H:i:s');
                $termId = $this->Categories_model->add_term($arrAddTermData);

                $arrAddTaxonomyData = array();
                $arrAddTaxonomyData['scx_term_id']         = $termId;
                $arrAddTaxonomyData['taxonomy_type']       = $this->input->post('termtax_type');
                $arrAddTaxonomyData['taxonomy_content']    = $this->input->post('term_description');
                $arrAddTaxonomyData['parent']              = $term_parent;
                $arrAddTaxonomyData['display_order']       = $this->input->post('sort_order');
                $arrAddTermData['created_datetime']    = date('Y-m-d H:i:s');
                $taxonomyId = $this->Categories_model->add_term_taxonomy($arrAddTaxonomyData);
               
                if(@$_FILES['term_image']['name']!=''){
                       $ext = pathinfo($_FILES['term_image']['name'], PATHINFO_EXTENSION);
                        $image_info = getimagesize($_FILES["term_image"]["tmp_name"]);
                        $image_width = $image_info[0]; 
                        $image_height = $image_info[1];
                        if($ext!='jpg' || $image_width!=530 || $image_height!=530){
                            $is_true = 1;
                       }
                    $target_dir = "assets/images/products_category/".$termId.'.jpg';
                    if(move_uploaded_file($_FILES["term_image"]["tmp_name"], $target_dir)){
                        $termImageId = $this->Categories_model->update_cat_image($termId);
                    }
                }
                $this->session->set_flashdata('success','Category Added successfully'); 
                redirect('admin/categories'); 
            }
    }

    

    public function categories_edit($page_no="", $id='')
    {
        $data = array();
        $validation_config = array
            (
                array(
                        'field'=>'term_name',
                        'label'=>'Term Name',
                        'rules'=>'trim|required',
                    ),
                array(
                        'field'=>'term_slug',
                        'label'=>'Term Slug',
                        'rules'=>'trim|required',
                    ),
                array(
                        'field'=>'term_parent',
                        'label'=>'Term Parent',
                        'rules'=>'trim|required',
                    ),
                  array(
                        'field'=>'term_description',
                        'label'=>'Term Description',
                        'rules'=>'trim|required',
                    ),
                  array(
                        'field'=>'termtax_type',
                        'label'=>'Term Taxanomy Type',
                        'rules'=>'trim|required',
                    ),
                   array(
                        'field'=>'sort_order',
                        'label'=>'Sort Order',
                        'rules'=>'trim|required',
                    ),
            );

            $data['scontent']           = array();
            $offset                     = ($this->uri->segment(3) != '' ? $this->uri->segment(3): 0);
            $per_page                   = 20;
            $numRows                    = $this->Categories_model->get_total_cat($data['scontent']);
           // echo $this->db->last_query();
            $arrResult                  = array();
            if($numRows > 0)
            {
                $arrResult              = $this->Categories_model->get_cat($per_page, $page_no,$data['scontent']);
            }
            //echo $this->db->last_query();
            $this->load->library("pagination");
            $config['total_rows']       = $numRows;
            $config['per_page']         = $per_page;
            $config['next_link']        = '<span aria-hidden="true">»</span><span class="sr-only">Next</span>';
            $config['prev_link']        = '<span aria-hidden="true">«</span><span class="sr-only">Prev</span>';
            $config['uri_segment']      = 3;
            $config['base_url']         = base_url().'/admin/categories/categories_add';
            
            if(isset($_GET))
            {
                $config['first_url']    = $config['base_url'].'/1'.'?'.http_build_query($_GET, '', "&");    
            }
             
            $config['full_tag_open']    = "<ul class='pagination pagination-sm no-margin pull-right'>";
            $config['full_tag_close']   ="</ul>";
            $config['num_tag_open']     = '<li>';
            $config['num_tag_close']    = '</li>';
            $config['cur_tag_open']     = "<li class='active'><a href='javascript:void(0);'>";
            $config['cur_tag_close']    = "<span class='sr-only'></span></a></li>";
            $config['next_tag_open']    = "<li>";
            $config['next_tag_close']  = "</li>";
            $config['prev_tag_open']    = "<li>";
            $config['prev_tag_close']  = "</li>";
            $config['first_tag_open']   = "<li>";
            $config['first_tag_close'] = "</li>";
            $config['last_tag_open']    = "<li>";
            $config['last_tag_close']  = "</li>";
            if(isset($_GET))
            {
                $config['suffix']           = '?'.http_build_query($_GET, '', "&");
            }
            
            $config['use_page_numbers'] = TRUE; 
            $this->pagination->initialize($config);
        
        
        $data['paginglinks']        = $this->pagination->create_links();
        // Showing total rows count 
        if($data['paginglinks']!= '') {
          $data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$this->pagination->per_page)+1).' to '.(( $numRows > $this->pagination->cur_page*$this->pagination->per_page)?$this->pagination->cur_page*$this->pagination->per_page:$numRows).' of '.$numRows;
        } 




            $data['get_cat']            = $arrResult;
            $data['user_id']            = $id;
            $data['page_no']            = $page_no;
            $getCategories              = $this->Categories_model->get_categories_by_id($id);
            //echo $this->db->last_query();
            $data['getCategories']      = $getCategories;

            $this->form_validation->set_rules($validation_config);
            if($this->form_validation->run() == FALSE)
            {
                $this->load->view('admin/common/header');
                $this->load->view('admin/categories',$data);
                
            }
            else
            {
                
                $arrEditTermData = array();
                $arrEditTermData['term_name']           = $this->input->post('term_name');
                $arrEditTermData['term_slug']           = $this->input->post('term_slug');
                $this->Categories_model->update_term($arrEditTermData, $id);

                $arrEditTaxonomyData = array();
                $arrEditTaxonomyData['taxonomy_type']       = $this->input->post('termtax_type');
                $arrEditTaxonomyData['taxonomy_content']    = $this->input->post('term_description');
                $arrEditTaxonomyData['parent']              = $this->input->post('term_parent');
                $arrEditTaxonomyData['display_order']       = $this->input->post('sort_order');
                $this->Categories_model->update_term_taxonomy($arrEditTaxonomyData, $id);

                $this->session->set_flashdata('success','Category Edit successfully'); 
                redirect('admin/categories'); 
            }

    }

    function check_product_count(){
        if (!$this->input->is_ajax_request()) {
           exit('No direct script access allowed');
        }else{
           $id = $this->input->post('id');
           echo $this->Categories_model->get_categories($id)[0]['count'];
        }
    }

    function delete_category(){
        if (!$this->input->is_ajax_request()) {
           exit('No direct script access allowed');
        }else{
           $id = $this->input->post('id');
           echo $this->Categories_model->delete_category($id);
        }
    }

    function search_category(){
        if (!$this->input->is_ajax_request()) {
           exit('No direct script access allowed');
        }else{
            $data = array();
            $search_category = @$this->input->post('search_text');
            $data['cat_data'] = $this->Categories_model->get_filtered_category($search_category);
            
            $this->load->view('categories_list',$data);
        }
    }

    function hide_show_cat(){
        if (!$this->input->is_ajax_request()) {
           exit('No direct script access allowed');
        }else{
            $cat_hide_show = $this->input->post('cat_hide_show');
            $cat = $this->input->post('cat');
            $this->Categories_model->hide_show_cat($cat_hide_show,$cat);
        }
    }

    public function categories_delete($id)
    {
        $this->Categories_model->delete_term_taxonomy($id);
        $this->Categories_model->delete_term($id);
        $this->session->set_flashdata('success','Category Deleted successfully'); 
        redirect('admin/categories'); 
    }
}
?>