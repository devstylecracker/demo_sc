<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_stylistorder extends CI_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
    {
        // Call the Model constructor
        
        parent::__construct();
       $this->load->library('bcrypt');
       $this->load->library('form_validation');
      
        
        
    }
    
	public function index()
	{
		
			$this->load->view('common/header');
			
			$this->load->view('manage_stylistorder');
		
	}
	public function user_history()
	{
		
			$this->load->view('common/header');
			
			$this->load->view('order_history');
		
	}
	public function user_cart()
	{
		
			$this->load->view('common/header');
			
			$this->load->view('user_cart');
		
	}
	public function brands()
	{
		
			$this->load->view('common/header');
			
			$this->load->view('brands');
		
	}
	public function brand_settings()
	{
		
			$this->load->view('common/header');
			
			$this->load->view('brand_settings');
		
	}

	public function user_dashboard()
	{
		
			$this->load->view('common/header');
			
			$this->load->view('user_dashboard');
		
	}
	
	public function panel()
	{
		
			$this->load->view('common/header');
			$this->load->view('panel');
		
	}
	public function userpanel()
	{
		 
			$this->load->view('common/header');
			$this->load->view('userpanel');

		
	}
	public function logistics()
	{
		
			$this->load->view('common/header');
			$this->load->view('logistics');

		
	}
	public function accounts()
	{
		
			$this->load->view('common/header');
			$this->load->view('accounts');
			
		
	}
	public function all_products()
	{
		
			$this->load->view('common/header');
			$this->load->view('all_products');
		
	}

	public function box_transaction_manage()
	{
		 
			$this->load->view('common/header');
			$this->load->view('box_transaction_manage');

				
	}
	public function brand_order_transaction_manage()
	{
		 
			$this->load->view('common/header');
			$this->load->view('brand_order_transaction_manage');
		
	}

	public function product_manage()
	{
		
			$this->load->view('common/header');
			$this->load->view('product_manage');
		
	}

	public function product_manage_add()
	{
		
			$this->load->view('common/header');
			$this->load->view('product_manage_add');

		
	}

	public function coupon_setting()
	{
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{ 
			//$this->load->view('common/header');
			$this->load->view('coupon_setting1');

			//$this->load->view('common/footer');
		}
	}


	// public function users()
	// {
		
	// 		$this->load->view('common/header');
	// 		$this->load->view('admin_users');
			
		
	// }

	public function users_edit()
	{
		 
			$this->load->view('common/header');
			$this->load->view('admin_users_edit');
		
	}

	public function users_add()
	{
		
			$this->load->view('common/header');
			$this->load->view('admin_users_add');
			
	}

	public function users_permissions()
	{
		 
			$this->load->view('common/header');
			$this->load->view('admin_users_permissions');
		
	}
	
	public function customers()
	{
		
			$this->load->view('common/header');
			$this->load->view('scbox_users');

		
	}


	public function store()
	{
		 
			$this->load->view('common/header');
			$this->load->view('store_users');

			
		
	}

	public function store_view()
	{
		 
		$this->load->view('common/header');
			$this->load->view('store_view');

			
		
	}

	public function store_settings()
	{
		 
			$this->load->view('common/header');
			$this->load->view('store_settings');

			
		
	}

	public function store_images()
	{
		
			$this->load->view('common/header');
			$this->load->view('store_images');

		
	}

	public function store_sc_settings()
	{
		
			$this->load->view('common/header');
			$this->load->view('sc_store_settings');

		
	}

	public function home()
	{
		
			$this->load->view('common/header');
			$this->load->view('home');

		
	}

	
}
