<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->database(); // load database
        $this->load->model('Users_model');
        $this->load->library('form_validation');

    }

  
    public function index($page_no='0') {
            $data['scontent']           = array();
            $offset                     = ($this->uri->segment(3) != '' ? $this->uri->segment(3): 0);
            $per_page                   = 20;
            $numRows                    = $this->Users_model->get_total_users($data['scontent']);
            //echo $this->db->last_query();
            $arrResult                  = array();
            if($numRows > 0)
            {
                $arrResult              = $this->Users_model->get_users($per_page, $page_no,$data['scontent']);
            }
           // echo $this->db->last_query();
            $this->load->library("pagination");
            $config['total_rows']       = $numRows;
            $config['per_page']         = $per_page;
            $config['next_link']        = '<span aria-hidden="true">»</span><span class="sr-only">Next</span>';
            $config['prev_link']        = '<span aria-hidden="true">«</span><span class="sr-only">Prev</span>';
            $config['uri_segment']      = 4;
            $config['base_url']         = base_url().'/admin/Users/index/';
            
            if(isset($_GET))
            {
                $config['first_url']    = $config['base_url'].'/1'.'?'.http_build_query($_GET, '', "&");    
            }
             
            $config['full_tag_open']    = "<ul class='pagination pagination-sm no-margin pull-right'>";
            $config['full_tag_close']   ="</ul>";
            $config['num_tag_open']     = '<li>';
            $config['num_tag_close']    = '</li>';
            $config['cur_tag_open']     = "<li class='active'><a href='javascript:void(0);'>";
            $config['cur_tag_close']    = "<span class='sr-only'></span></a></li>";
            $config['next_tag_open']    = "<li>";
            $config['next_tag_close']  = "</li>";
            $config['prev_tag_open']    = "<li>";
            $config['prev_tag_close']  = "</li>";
            $config['first_tag_open']   = "<li>";
            $config['first_tag_close'] = "</li>";
            $config['last_tag_open']    = "<li>";
            $config['last_tag_close']  = "</li>";
            if(isset($_GET))
            {
                $config['suffix']           = '?'.http_build_query($_GET, '', "&");
            }
            
            $config['use_page_numbers'] = TRUE; 
            $this->pagination->initialize($config);
        
        
        $data['paginglinks']        = $this->pagination->create_links();
        // Showing total rows count 
        if($data['paginglinks']!= '') {
          $data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$this->pagination->per_page)+1).' to '.(( $numRows > $this->pagination->cur_page*$this->pagination->per_page)?$this->pagination->cur_page*$this->pagination->per_page:$numRows).' of '.$numRows;
        } 

            $data['users_data']           = $this->Users_model->get_total_users();            
            $data['get_users']            = $arrResult;
            $data['page_no']            = $page_no;


           $this->load->view('common/header');
           $this->load->view('admin_users', $data); // load the view file , we are passing $data array to view file
         }
      

    public function add_data()
    {
         $this->load->view('common/header');
         $this->load->view('admin_users_add');
    }

    /****************************  START INSERT FORM DATA ********************/
    public function submit_data()
    {
        // Displaying Errors In Div
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        // Validation For Name Field
        $this->form_validation->set_rules('user_name', 'Username', 'required|is_unique[scx_user.username]');

        $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email|is_unique[scx_user.email]');
        // Validation 
        $this->form_validation->set_rules('user_fname', 'First Name', 'required');
        // Validation 
        $this->form_validation->set_rules('user_lname', 'Last Name', 'required');

         $this->form_validation->set_rules('user_type', 'User Type', 'required');

         $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]|max_length[20]');

        if ($this->form_validation->run() == FALSE)

          {  
            $this->load->view('common/header');
            $this->load->view('admin_users_add');  

         }
         else
         {
             $data = array('username'                   => $this->input->post('user_name'),
                  'email'                      => $this->input->post('user_email'),
                  'first_name'                   => $this->input->post('user_fname'),
                  'last_name'                    => $this->input->post('user_lname'),
                  'user_type '        =>          $this->input->post('user_type'),
                  'password'      => $this->input->post('password'),
                   'created_datetime'  => date('Y-m-d H:i:s')
                    );


    $insert = $this->Users_model->insert_data($data);
    $this->session->set_flashdata('success', 'User Added Successfully');
    redirect('admin/users/add_data');
                  
         }       
    
    }

    public function edit_data($id)
    {
        $data = array();
        $validation_config = array
            (
                /*array(
                        'field'=>'user_email',
                        'label'=>'User Email',
                        'rules'=>'trim|required',
                    ),*/
                array(
                        'field'=>'user_fname',
                        'label'=>'User First Name',
                        'rules'=>'trim|required',
                    ),
                array(
                        'field'=>'user_lname',
                        'label'=>'User Last Name',
                        'rules'=>'trim|required',
                    ),
                  /*array(
                        'field'=>'user_name',
                        'label'=>'User Name',
                        'rules'=>'trim|required',
                    ),*/
        array(
                        'field'=>'user_type',
                        'label'=>'User Type',
                        'rules'=>'trim|required',
                    ),
        array(
                        'field'=>'password',
                        'label'=>'Password',
                        'rules'=>'trim|required',
                    ),
            );
            $data['user_id']            = $id;
            $data['userDetail']         = $this->Users_model->edit_user_by_id($id);

            $this->form_validation->set_rules($validation_config);
            if($this->form_validation->run() == FALSE)
            {
          $this->load->view('admin/common/header');
                $this->load->view('admin/admin_users_edit',$data);
        //if(isset($_POST['submit'])){echo "Arun Prajapati";print_r($this->form_validation->run()); exit();}               

            }
            else
            {
        $arrEditData = array();
                //$arrEditData['email']             = $this->input->post('user_email');
                $arrEditData['first_name']          = $this->input->post('user_fname');
                $arrEditData['last_name']       = $this->input->post('user_lname');
                //$arrEditData['username  ']      = $this->input->post('user_name');
                $arrEditData['user_type ']        = $this->input->post('user_type');
                $arrEditData['password']      = $this->input->post('password');
                $arrEditData['modified_datetime']   = date('Y-m-d H:i:s');
                $this->Users_model->update_user($arrEditData, $id);
        //echo $this->db->last_query();
        //exit();
                $this->session->set_flashdata('success','User Edit successful'); 
                redirect('admin/users'); 
            }
    }

     public function user_delete($id)
    {
        $this->Users_model->delete_user_meta($id);
        $this->Users_model->delete_user($id);
        $this->session->set_flashdata('success','User deleted successfully'); 
        redirect('admin/users'); 
    }

    function check_email(){
        $email = $this->input->post('email');
        $res = $this->users_model->emailUnique($email);
        $count = count($res);
        // echo $count;exit;
        $html = '';
        if($count > 0)
        {   $html = 'Email id already exists';
            echo $html; 
        }
        else echo $html;
  }


}
?>