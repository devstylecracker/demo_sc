<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
class Frontend extends TT_REST_Controller 
{
  function __construct()
	{
		parent::__construct();
		$this->load->library('bitly');
		$this->load->library('email');
		$this->load->library('user_agent');
		$this->load->library('curl');
		$this->load->model('frontend_model');	
		$this->open_methods = array('register_user_post','login_post','scgenrate_otp_post');    
	}
   
	public function sc_box_home_get()
	{
		$data = new stdClass();
		
		$data->banner['title'] 			= '<h1><span>  <b>A Box Full of Fashion.<br /> That you are Guaranteed to Love.</b><br />Put together by our celebrity Stylists.<br /> DELIVERED STRAIGHT TO YOUR DOORSTEP.</span></h1>';
		$data->banner['subtitle'] 		= 'Simple. Stylish. Stress Free.';
		$data->banner['button_text'] 		= 'Order Now';
		$data->banner['discount'] 		= 'Upto 30% off on your first box.';
		$data->banner['image'] 			= 'https://www.stylecracker.com/assets/images/scbox/box-banner.jpg';
		
		$data->curated_for_you['youtube_video'] 			= 'https://www.youtube.com/embed/VPrI2rk7FU4';
		$data->curated_for_you['button_text'] 				= 'Get Started';
		$data->curated_for_you['steps'][0]['title'] 		= 'Your Style';
		$data->curated_for_you['steps'][0]['desc'] 		= ' One size does not fit all and we get that. At StyleCracker we curate fashion for you keeping in mind your personality, likes, dislikes, body shape, <b>budget</b> and other important details. Yes, we'."'".'ve got you covered every step of the way.';
		$data->curated_for_you['steps'][1]['title']		= 'Your Budget';
		$data->curated_for_you['steps'][1]['desc']		= ' StyleCracker is for everyone! Once we'."'".'ve understood your <b>style</b>, our stylists will curate a box for you within your budget. While we do have pre-set amounts and quantities to get you started on your selection, you could also customize your order.';
		$data->curated_for_you['steps'][2]['title']	= 'Your Convenience';
		$data->curated_for_you['steps'][2]['desc']	= 'StyleCracker is designed to make life easier and more convenient for you. All you need to do is fill out a form, speak to our stylists and get styled. It'."'".'s that simple! Leave the fashion to the experts and enjoy the stress free shopping experience!';
		

		//	HOW IT WORKS	
		$data->how_it_works['title'] 		= 'How it Works';
		
		$data->how_it_works['steps'][0]['sub_title'] 	= 'We get to know you';
		$data->how_it_works['steps'][0]['image'] 		= 'https://www.stylecracker.com/assets/images/scbox/how-it-works-1.png';
		$data->how_it_works['steps'][0]['desc'] 		= 'To get started, you'."'".'ll need to fill out a simple online form. This helps us decode your style preferences. Next, a StyleCracker celebrity stylist will call you to understand your requirements. Once this is done, the stylist will get to work to curate your box.';
		
		$data->how_it_works['steps'][1]['sub_title'] 	= 'WE SEND YOU THE BOX';
		$data->how_it_works['steps'][1]['image'] 		= 'https://www.stylecracker.com/assets/images/scbox/how-it-works-2.png';
		$data->how_it_works['steps'][1]['desc'] 		= 'Once our stylist has curated the box keeping in mind your likes, dislikes, body shape and budget we put it all together and ship you, your box of style. Shipping is absolutely free and it gets delivered straight to your doorstep.';
		
		$data->how_it_works['steps'][2]['sub_title'] 	= 'Keep only what you like';
		$data->how_it_works['steps'][2]['image'] 		= 'https://www.stylecracker.com/assets/images/scbox/how-it-works-3.png';
		$data->how_it_works['steps'][2]['desc']  		= 'When you receive the box, check out the selection we'."'".'ve sent you. Try it on for size and to see how it works for you. Keep everything you like. If there'."'".'s something that doesn'."'".'t suit you, send it back to us. No questions asked.';
		
		$data->how_it_works['steps'][3]['sub_title'] 	= 'Give us your feedback';
		$data->how_it_works['steps'][3]['image'] 		= 'https://www.stylecracker.com/assets/images/scbox/how-it-works-4.png';
		$data->how_it_works['steps'][3]['desc'] 		= 'Our goal is to ensure that you, and every other <b>user</b>, loves every item curated personally for you. Your constant feedback is most valuable to make sure we get even better. So please take a couple of minutes to tell us what you liked and what you didn'."'".'t.';
		$data->how_it_works['button_text'] 			= 'Order Now';
		
		//TESTIMONIALS
		$data->testimonial['title'] 			= 'TESTIMONIALS';
		$data->testimonial['desc'] 				= 'We asked a few of our customers about their first experience with the StyleCracker Box';
		$data->testimonial['video_link'] 		= 'https://www.youtube.com/embed/6k5JAHuM_TY';
		$data->testimonial['button_text'] 		= 'Order Now';
		
		$data->testimonial['customers'][0]['name'] 				= 'Priyanka Talreja';
		$data->testimonial['customers'][0]['image'] 			= 'https://www.stylecracker.com/assets/images/home/banner/priyanka1.jpg';
		$data->testimonial['customers'][0]['desc'] 				= 'I received my surprise box of goodies from StyleCracker and it just couldn'."'".'t get better! In this day and age of online shopping, a box of goodies suited to my style and preference was something very outstanding. Not to mention how flattering it is to have a box personalized to my taste and style. Way to go StyleCracker!';
		$data->testimonial['customers'][1]['name'] 				= 'Glenn Gonsalves';
		$data->testimonial['customers'][1]['image'] 			= 'https://www.stylecracker.com/assets/images/home/banner/glen.jpg';
		$data->testimonial['customers'][1]['desc'] 				= 'Since I was looking for a personal stylist for myself, having one who'."'".'s worked with A-listers and the biggest names in the movie business made StyleCracker my absolute choice! From being style guides to helping me with even '."'".'what not to wear'."'".',
              they'."'".'ve been extremely helpful in solving all my fashion queries.';
		
		//OUR COMMITMENT
		
		$data->our_commitment['title'] 			= 'OUR COMMITMENT';
		
		$data->our_commitment['steps'][0]['sub_title'] 		= 'It'."'".'s all about you';
		$data->our_commitment['steps'][0]['image'] 			= 'https://www.stylecracker.com/assets/images/scbox/our-comm-1.png';
		$data->our_commitment['steps'][0]['desc'] 			= 'The StyleCracker box is about you. While our stylists put together and curate each item in the box, it'."'".'s always done keeping your style in mind.';
		
		$data->our_commitment['steps'][1]['sub_title'] 		= 'Experienced Stylists';
		$data->our_commitment['steps'][1]['image'] 			= 'https://www.stylecracker.com/assets/images/scbox/our-comm-2.png';
		$data->our_commitment['steps'][1]['desc'] 			= ' All stylists part of the StyleCracker family come with years of experience. From styling celebrities, editorials and films to individuals for weddings and day-to-day requirements, we do it all. So worry not! You are in great hands.';
		
		$data->our_commitment['steps'][2]['sub_title'] 		= 'Transparent Pricing';
		$data->our_commitment['steps'][2]['image'] 			= 'https://www.stylecracker.com/assets/images/scbox/our-comm-3.png';
		$data->our_commitment['steps'][2]['desc'] 			= 'Every product you get comes with the official label and price tag, so you'."'".'ll get what you paid for. More often than not, you'."'".'ll get stuff worth a lot more.';
		
		$data->our_commitment['steps'][3]['sub_title'] 		= 'Returns, Replacements and Refunds';
		$data->our_commitment['steps'][3]['image'] 			= 'https://www.stylecracker.com/assets/images/scbox/our-comm-4.png';
		$data->our_commitment['steps'][3]['desc'] 			= ' If you don'."'".'t like something we send in the StyleCracker box, just <b>return it</b>. No questions asked.';
		$data->our_commitment['button_text'] 			= 'Order Now';
		
		//Pricing
		$data->package['title']				= 'Pricing';
		$data->package['pricing_title']		= 'Upto 30% off on your first order.';
		//$scbox_pack = unserialize(SCBOX_PACKAGE);
		$taxonomy_id_female	= 1;
		$scbox_pack_female 	= $this->frontend_model->scxObject($taxonomy_id_female);
		 $i=0;
		 foreach($scbox_pack_female as $val){
			$data->package['women'][$i]['object_id']	= $val['id'];
			$data->package['women'][$i]['name']			= $val['object_name'];
			$data->package['women'][$i]['price']		= $val['object_meta_value'];
			$data->package['women'][$i]['packname']		= $val['object_name'];
			//$data->package['women'][$i]['desc']		= $val->desc;
			$data->package['women'][$i]['image']		= 'https://www.stylecracker.com/assets/images/scbox/women/'.$val['object_image'];
			$i++;
		 }
		$taxonomy_id_male	= 2;
		$scbox_pack_male 	= $this->frontend_model->scxObject($taxonomy_id_male);
		$j=0;
		 foreach($scbox_pack_male as $val){
				$data->package['men'][$j]['object_id']		= $val['id'];
				$data->package['men'][$j]['name']			= $val['object_name'];
				$data->package['men'][$j]['price']			= $val['object_meta_value'];
				$data->package['men'][$j]['packname']		= $val['object_name'];
				//$data->package['men'][$j]['desc']			= $val->desc;
				$data->package['men'][$j]['image']			= 'https://www.stylecracker.com/assets/images/scbox/men/'.$val['object_image'];
				$j++;
		 }
		$data->package['button_text'] 			= 'Order Now';
		$this->success_response($data);
	}
	
	public function sc_box_step1_post()
	{
		$user_id			=	$this->input->post('user_id');
		$auth_token			=	$this->input->post('auth_token');
		$for				=	$this->input->post('for');
		$gender				=	$this->input->post('gender');
		$full_name			=	$this->input->post('full_name');
		$day				=	$this->input->post('day');
		$month				=	$this->input->post('month');
		$year				=	$this->input->post('year');
		$email				=	$this->input->post('email');
		$mobile				=	$this->input->post('mobile');
		$pincode			=	$this->input->post('pincode');
		$city				=	$this->input->post('city');
		$state				=	$this->input->post('state');
		$profession			=	$this->input->post('profession');
		$shipping_address   =	$this->input->post('address');
		$product_id   		=	$this->input->post('product_id');
		$price   			=	$this->input->post('price');
		
		$scusername 		= preg_replace('/\s+/', ' ', trim($full_name));
		$username = explode(' ',trim($scusername));       
        $json_array['firstname'] = ucwords(strtolower($username[0]));
        $json_array['lastname'] = ucwords(strtolower($username[1]));
		
		
		if(trim($for) 				== ''){ $this->failed_response(1006, "Please check for me / this is gift"); }
		else if(trim($gender) 		== ''){$this->failed_response(1006, "Please select gender");}
		else if(trim($full_name)	== ''){$this->failed_response(1006, "Please enter full name");}
		else if(trim($day) 			== ''){$this->failed_response(1006, "Please select day");}
		else if(trim($month) 		== ''){$this->failed_response(1006, "Please select month");}
		else if(trim($year) 		== ''){$this->failed_response(1006, "Please select year");}
		else if(trim($email) 		== ''){$this->failed_response(1006, "Please select email");}
		else if(trim($mobile) 		== ''){$this->failed_response(1006, "Please select mobile");}
		else if(trim($pincode) 		== ''){$this->failed_response(1006, "Please select pincode");}
		else if(trim($city) 		== ''){$this->failed_response(1006, "Please select city");}
		else if(trim($state) 		== ''){$this->failed_response(1006, "Please select state");}
		else if(trim($profession) 	== ''){$this->failed_response(1006, "Please select profession");}
		else if(trim($shipping_address) == ''){$this->failed_response(1006, "Please Enter shipping address");}
		else if(trim($product_id) 	== ''){$this->failed_response(1006, "Please Enter Product");}
		else if(trim($price) 		== ''){$this->failed_response(1006, "Please Enter price");}
		else if($username[0] 	== ''){$this->failed_response(1006, "Please Enter first name");}
		else if($username[1] 	== ''){$this->failed_response(1006, "Please Enter last name");}

		
		
		
		$arrPaData			= array();
		$arrPaData[0]['meta_key']		= 'pincode';
		$arrPaData[0]['meta_value']		= $pincode;
		$arrPaData[1]['meta_key']		= 'city';
		$arrPaData[1]['meta_value']		= $city;
		$arrPaData[2]['meta_key']		= 'state';
		$arrPaData[2]['meta_value']		= $state;
		$arrPaData[3]['meta_key']		= 'profession';
		$arrPaData[3]['meta_value']		= $profession;
		$arrPaData[4]['meta_key']		= 'shipping_address';
		$arrPaData[4]['meta_value']		= $shipping_address;
		
		if(is_array($arrPaData) && count($arrPaData)>0)
		{	
			foreach($arrPaData as $key => $addData)
			{
				$addData['scx_user_id']			= $user_id;
				$addData['created_datetime']	= date('Y-m-d H:i:s');
				$this->frontend_model->userMeta($addData);
			}
			foreach($arrPaData as $orderKey => $orderMeta)
			{
				$forMeMeta[$orderMeta['meta_key']] = $orderMeta['meta_value'];
			} 
			
		}
		// for gift
		if($for == '2')
		{	
			$friend_gender			=	$this->input->post('friend_gender');
			$friend_full_name		=	$this->input->post('friend_full_name');
			$friend_day				=	$this->input->post('friend_day');
			$friend_month			=	$this->input->post('friend_month');
			$friend_year			=	$this->input->post('friend_year');
			$friend_email			=	$this->input->post('friend_email');
			$friend_mobile			=	$this->input->post('friend_mobile');
			$friend_pincode			=	$this->input->post('friend_pincode');
			$friend_city			=	$this->input->post('friend_city');
			$friend_state			=	$this->input->post('friend_state');
			$friend_profession		=	$this->input->post('friend_profession');
			$friend_billing_address =	$this->input->post('friend_address');	
			
			if(trim($friend_gender) == ''){$this->failed_response(1006, "Please select your friend gender");}
			else if(trim($friend_full_name)	== ''){$this->failed_response(1006, "Please enter your friend full name");}
			else if(trim($friend_day) 		== ''){$this->failed_response(1006, "Please enter  day");}
			else if(trim($friend_month) 	== ''){$this->failed_response(1006, "Please enter month");}
			else if(trim($friend_year) 		== ''){$this->failed_response(1006, "Please enter year");}
			else if(trim($friend_email) 	== ''){$this->failed_response(1006, "Please enter your friend email");}
			else if(trim($friend_mobile) 	== ''){$this->failed_response(1006, "Please enter your friend mobile");}
			else if(trim($friend_pincode) 	== ''){$this->failed_response(1006, "Please enter your friend pincode");}
			else if(trim($friend_city) 		== ''){$this->failed_response(1006, "Please select your friend city");}
			else if(trim($friend_state) 	== ''){$this->failed_response(1006, "Please select your friend state");}
			else if(trim($friend_profession) == ''){$this->failed_response(1006, "Please enter your friend profession");}
			else if(trim($friend_billing_address) == ''){$this->failed_response(1006, "Please enter billing address");}
			
			$gifter_details				= array();
			$gifter_details['gender'] 	= $friend_gender;
            $gifter_details['name'] 	= $friend_full_name;
            $gifter_details['email'] 	= $friend_email;
            $gifter_details['mobileno'] = $friend_mobile;
            $gifter_details['pincode'] 	= $friend_pincode;
            $gifter_details['city'] 	= $friend_city;
            $gifter_details['state'] 	= $friend_state;
            $gifter_details['address'] 	= $friend_billing_address;
            $gifter_details['profession'] = $friend_profession; 
            $gifter_details['dob'] 		= $friend_year.'-'.$friend_month.'-'.$friend_day; 
            $gifter_details_json 		= serialize($gifter_details);
			
			
			
			/*if(is_array($arrAddFriendData) && count($arrAddFriendData)>0)
			{	
				foreach($arrAddFriendData as $key => $addData)
				{
					 $this->Pa_model->save_scboxuser_answer('',$addData['object_meta_value'],$user_id,$object_id,$addData['object_meta_key']);
					 //echo $this->db->last_query();
					 
				}
			}*/	
				
		}
		
		$scxOrder = array();
		$scxOrder['order_id']			= 'SC'.time();
		$scxOrder['brand_id']			= '0';
		$scxOrder['product_id']			= $product_id;
		$scxOrder['product_price']		= $price;
		$scxOrder['order_status']		= '0';
		$scxOrder['parent_id']			= '0';
		$scxOrder['user_id']			= $user_id;
		$scxOrder['created_by']			= $user_id;
		$scxOrder['created_datetime']	= date('Y-m-d H:i:s');
		$orderId = $this->frontend_model->scxOrder($scxOrder);
		
		$orderPaData = array();
		$orderPaData['meta_key']		= 'user_billing';
		$orderPaData['meta_value']		= serialize($forMeMeta);
		$orderPaData['created_by']		= $user_id;
		$orderPaData['scx_order_id']	= $orderId; 
		$orderPaData['created_datetime']= date('Y-m-d H:i:s');
		$this->frontend_model->orderMeta($orderPaData);
		
		if($for == 2)
		{
			$arrAddFriendData 	= array();
			$arrAddFriendData['meta_key'] 		= 'user_shipping';
			$arrAddFriendData['meta_value'] 	= $gifter_details_json;
			$arrAddFriendData['created_by']		= $user_id;
			$arrAddFriendData['scx_order_id']	= $orderId; 
			$arrAddFriendData['created_datetime']= date('Y-m-d H:i:s');
			$this->frontend_model->orderMeta($arrAddFriendData);
		}
		
		$data = new stdClass();
		$data->message = 'Data Send successfully.';
		$this->success_response($data);		
	}
	
	/*public function sc_box_step2_get()
	{
		$data = array();
		//$data = new stdClass();
		$category = new stdClass();
		$gender = $this->input->get('gender');
		if(trim($gender) == ''){
		   $this->failed_response(1006, "Please select gender");
		 }
		
		$getCategory = $this->frontend_model->getCategory($gender);

		if($gender == 1 && $gender != '')
		{			
			$category->women->categories[0] = $getCategory;
			$category->women->category_combos[0]['pkg_price'] = "2999";
			$category->women->category_combos[0]['pkg_combo'] = array(
			array("apparel", "bags", "accessories"),
			array("apparel", "bags", "jewellery"),
			array("apparel", "bags", "beauty"),

			array("apparel", "footwear", "accessories"),
			array("apparel", "footwear", "jewellery"),
			array("apparel", "footwear", "beauty"),

			array("bags", "accessories", "jewellery"),
			array("bags", "accessories", "beauty"),

			array("bags", "jewellery", "beauty"),

			array("footwear", "beauty", "accessories"),
			array("footwear", "beauty", "jewellery"),

			array("footwear", "jewellery", "accessories"),
			);

			$category->women->category_combos[1]['pkg_price'] = "4999";
			$category->women->category_combos[1]['pkg_combo'] = array(
			array("apparel", "bags", "accessories", "jewellery"),
			array("apparel", "bags", "accessories", "beauty"),
			array("apparel", "bags", "jewellery", "beauty"),

			array("apparel", "footwear", "accessories", "jewellery"),
			array("apparel", "footwear", "accessories", "beauty"),
			array("apparel", "footwear", "jewellery", "beauty"),

			array("bags", "footwear", "accessories", "jewellery"),
			array("bags", "footwear", "accessories", "beauty"),
			array("bags", "footwear", "jewellery", "beauty"),
			);

			$category->women->category_combos[2]['pkg_price'] = "6999";
			$category->women->category_combos[2]['pkg_combo'] = array(
			array("apparel", "bags", "footwear", "accessories", "jewellery"),
			array("apparel", "bags", "footwear", "jewellery", "beauty"),
			array("apparel", "bags", "footwear", "accessories", "beauty"),
			array("apparel", "bags", "accessories", "jewellery", "beauty"),

			array("apparel", "footwear", "accessories", "jewellery", "beauty"),
			array("bags", "footwear", "accessories", "jewellery", "beauty"),
			);

			
		}
		else if($gender == 2 && $gender != '')
		{
			
			$category->men->categories[0] = $getCategory;
			$category->men->category_combos[0]['pkg_price'] = "2999";
			$category->men->category_combos[0]['pkg_combo'] = array(
			array("apparel", "bags", "accessories"),
			array("apparel", "bags", "grooming"),
			array("apparel", "accessories", "grooming"),

			array("apparel", "footwear", "accessories"),
			array("apparel", "footwear", "grooming"),

			array("bags", "footwear", "accessories"),
			array("bags", "footwear", "grooming"),

			array("bags", "accessories", "grooming"),

			array("footwear", "accessories", "grooming"),
			);

			$category->men->category_combos[1]['pkg_price'] = "4999";
			$category->men->category_combos[1]['pkg_combo'] = array(
			array("apparel", "bags", "accessories", "grooming"),
			array("apparel", "footwear", "accessories", "grooming"),
			array("bags", "footwear", "accessories", "grooming"),
			);

			$category->men->category_combos[2]['pkg_price'] = "6999";
			$category->men->category_combos[2]['pkg_combo'] =
			array("apparel", "bags", "footwear", "accessories", "grooming");

		}
		//echo '<pre>';print_r($category);exit;
		$data = $category;
		$this->success_response($data);
	}
	*/
	
	
	public function sc_box_step2_get()
	{
		$data = array();
		$gender 		= $this->input->get('gender');
		if(trim($gender) == ''){
			$this->failed_response(1006, "Please select gender");
		}
		$getCategory	= $this->frontend_model->getCategory($gender);
		if($gender == 1 && $gender != '')
		{
			$category['categories']	= $getCategory;
			$category['category_combos'][0]['pkg_price'] = "2999";
			$category['category_combos'][0]['pkg_combo'] = array(		
					array("Apparel", "Bags", "Accessories"),
					array("Apparel", "Bags", "Jewellery"),
					array("Apparel", "Bags", "Beauty"),
					
					array("Apparel", "Footwear", "Accessories"),
					array("Apparel", "Footwear", "Jewellery"),
					array("Apparel", "Footwear", "Beauty"),
					
					array("Bags", "Accessories", "Jewellery"),
					array("Bags", "Accessories", "Beauty"),
					
					array("Bags", "Jewellery", "Beauty"),
					
					array("Footwear", "Beauty", "Accessories"),
					array("Footwear", "Beauty", "Jewellery"),
					
					array("Footwear", "Jewellery", "Accessories"),				
			);
			
			$category['category_combos'][1]['pkg_price'] = "4999";
			$category['category_combos'][1]['pkg_combo'] = array(		
					array("Apparel", "Bags", "Accessories", "Jewellery"),
					array("Apparel", "Bags", "Accessories", "Beauty"),
					array("Apparel", "Bags", "Jewellery", "Beauty"),
					
					array("Apparel", "Footwear", "Accessories", "Jewellery"),
					array("Apparel", "Footwear", "Accessories", "Beauty"),
					array("Apparel", "Footwear", "Jewellery", "Beauty"),
					
					array("Bags", "Footwear", "Accessories", "Jewellery"),
					array("Bags", "Footwear", "Accessories", "Beauty"),
					array("Bags", "Footwear", "Jewellery", "Beauty"),				
			);
			
			$category['category_combos'][2]['pkg_price'] = "6999";
			$category['category_combos'][2]['pkg_combo'] = array(		
					array("Apparel", "Bags", "Footwear", "Accessories", "Jewellery"),
					array("Apparel", "Bags", "Footwear", "Jewellery", "Beauty"),
					array("Apparel", "Bags", "Footwear", "Accessories", "Beauty"),
					array("Apparel", "Bags", "Accessories", "Jewellery", "Beauty"),
					
					array("Apparel", "Footwear", "Accessories", "Jewellery", "Beauty"),
					array("Bags", "Footwear", "Accessories", "Jewellery", "Beauty"),				
			);
		}
		else if($gender == 2 && $gender != '')
		{
			$category['categories']	= $getCategory;
			$category['category_combos'][0]['pkg_price'] = "2999";
			$category['category_combos'][0]['pkg_combo'] = array(		
					array("Apparel", "Bags", "Accessories"),
					array("Apparel", "Bags", "Grooming"),
					array("Apparel", "Accessories", "Grooming"),
					
					array("Apparel", "Footwear", "Accessories"),
					array("Apparel", "Footwear", "Grooming"),
					
					array("Bags", "Footwear", "Accessories"),
					array("Bags", "Footwear", "Grooming"),	

					array("Bags", "Accessories", "Grooming"),
					
					array("Footwear", "Accessories", "Grooming"),	
			);
			
			$category['category_combos'][1]['pkg_price'] = "4999";
			$category['category_combos'][1]['pkg_combo'] = array(		
					array("Apparel", "Bags", "Accessories", "Grooming"),
					array("Apparel", "Footwear", "Accessories", "Grooming"),
					array("Bags", "Footwear", "Accessories", "Grooming"),	
			);
			
			$category['category_combos'][2]['pkg_price'] = "6999";
			$category['category_combos'][2]['pkg_combo'] =		
					array("Apparel", "Bags", "Footwear", "Accessories", "Grooming");	
			 
		}
		$data = $category;
		$this->success_response($data);
	}
	
	
}