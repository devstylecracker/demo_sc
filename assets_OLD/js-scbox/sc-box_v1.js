﻿/**
 * SCBox JS
 * User On-boarding
 * @author StyleCracker Tech
 */

(function($){
    
    function msg(m) {
        console.log(m);
    }
    msg("JS LOADED....");


    //== Generic function for Check box =======================
    $("input:checkbox").on("click touch",function () {      
        var objSelector = $(this).closest(".sc-checkbox");      
        if(!objSelector.hasClass("active") && !objSelector.hasClass("state-disabled")){
            $(this).closest("label").addClass("active");
            
            //== Common function triggers globally
            CheckWhichEvent($(this).closest(".scbox-container").attr("type"),$(this));
        } 
        else if(!objSelector.hasClass("state-disabled")){
            $(this).closest("label").removeClass("active");
            
            //== Common function triggers globally
            CheckWhichEvent($(this).closest(".scbox-container").attr("type"),$(this));
        }
    });
    
	$("input:radio").click(function () {      
        CheckWhichEvent($(this).closest(".scbox-container").attr("type"),$(this));
    });
    
	$("select").on("change",function () {
        //CheckWhichEvent($(this).closest(".scbox-container").attr("type"),$(this));
        CheckWhichEvent($(this).attr("type"),$(this));
    });
    
    $("ul.actdeact li").on("click touch", function(){
       var thisObj = $(this);
        if(!thisObj.closest('ul').hasClass('other-type')){
          if(!$(this).hasClass('active')){
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }  
        }
        //CheckWhichEvent($(this).closest("ul").attr("type"),$(this));
        CheckWhichEvent($(this).closest("ul").attr("type"),$(this));
    });
    
    //== Trigger Functions =======================   
    function CheckWhichEvent(eventType,obj){
        if(eventType=="boxType"){
            SCBoxType(obj);
        } 
        else if(eventType=="checkCategories"){
            SCBoxCategorySelection(obj);
        } 
        else if(eventType=="userInfo"){
            SCBoxUserInfo(obj);
        }
        else if(eventType=="topSize"){
            SCBoxSizeInfo(obj);
        }
        else if(eventType=="stuff-dont-need"){
            GetStuffDontNeed(obj);
        }
        else if(eventType=="height-feet"){
            GET_MyHeight(obj);
        }
        else if(eventType=="height-inches"){
            GET_MyHeightInches(obj);
        }
         else if(eventType=="cup-size"){
            GET_MyCupSize(obj);
        }
        else if(eventType=="band-size"){
            GET_MyBandSize(obj);
        }
        else if(eventType=="size-top"){
            GET_MySizeTop(obj);
        }
        else if(eventType=="size-tshirt"){
            GET_MySizeTshirt(obj);
        }
        else if(eventType=="size-trouser"){
            GET_MySizeTrouser(obj);
        } 
        else if(eventType=="size-jeans"){
            GET_MySizeJeans(obj);
        }  
        else if(eventType=="fit-you-like"){
            GET_FitYouLike(obj);
        }else if(eventType=="dress_size"){
            GET_DressSize(obj);
        } 
        else if(eventType=="fit-like-dress"){
            GET_FitLikeDress(obj);
        } 
        else if(eventType=="trouser-size"){
            GET_TrouserSize(obj);
        } 
        else if(eventType=="jeans-size"){
            GET_JeansSize(obj);
        }
        else if(eventType=="bottom-fit-like"){
            GET_BottomFitLike(obj);
        }
        else if(eventType=="length-skirt-dress"){
            GET_LengthSkirtDress(obj);
        }
        else if(eventType=="print-to-avoid"){
            GET_PrintToAvoid(obj);
        }
         else if(eventType=="print-to-avoid-afterpayment"){
            GET_PrintToAvoidAfterPayment(obj);
        }
        else if(eventType=="bel-ts"){
            GET_BeltsAfterPayment(obj);
        }
        else if(eventType=="color-to-avoid-afterpayment"){
            GET_ColorToAvoidAfterPayment(obj);
        }
        else if(eventType=="sun-glasses"){
            GET_Sunglasses(obj);
        } 
        else if(eventType=="jewel-lery"){
            GET_JewelleryAfterPayment(obj);
        }
         else if(eventType=="jewellery_type"){
            GET_JewelleryTypeAfterPayment(obj);
        }
        else if(eventType=="heel-height-size"){
            GET_HeelheightSize(obj);
        }  
        else if(eventType=="heel-type"){
            GET_HeelTypeAfterPayment(obj);
        }
        else if(eventType=="size-bag"){
            GET_SizeBagAfterPayment(obj);
        }
         else if(eventType=="reflectors"){
            GET_ReflectorsAfterPayment(obj);
        }
         else if(eventType=="beautyshade-afterpayment"){
            GET_BeautyShadeAfterPayment(obj);
        }
        else if(eventType=="bag_men"){
            GET_Bagmen(obj);
        }
        else if(eventType=="bag_women"){
            GET_Bagwomen(obj);
        }
         else if(eventType=="footwear_men"){
            GET_FootwearMen(obj);
        }
        else if(eventType=="footwear_women"){
            GET_FootwearWomen(obj);
        }
         else if(eventType=="footwear_men_type"){
            GET_FootwearMenType(obj);
        }
        else if(eventType=="footwear_women_type"){
            GET_FootwearWomenType(obj);
        }
        else if(eventType=="accesory_men"){
            GET_AccessoryMen(obj);
        }
        else if(eventType=="accesory_women"){
            GET_AccessoryWomen(obj);
        }
        else if(eventType=="beautyproduct_women"){
            GET_BeautyProductWomen(obj);
        }

        //TriggerAllTime();
    }
    
    //== 0. VARIABLE declaration =========================
   var arr2999 =[
        ["Apparel","Bags","Accessories"],
        ["Apparel","Bags","Jewellery"],
        ["Apparel","Bags","Beauty"],
        
        ["Apparel","Footwear","Accessories"],
        ["Apparel","Footwear","Jewellery"],
        ["Apparel","Footwear","Beauty"],
        
        ["Bags","Accessories","Jewellery"],
        ["Bags","Accessories","Beauty"],
        
        ["Bags","Jewellery","Beauty"],
        
        ["Footwear","Beauty","Accessories"],
        ["Footwear","Beauty","Jewellery"],
          
        ["Footwear","Jewellery","Accessories"],
    ];
    
    var arr4999 = [
        ["Apparel","Bags","Accessories","Jewellery"],
        ["Apparel","Bags","Accessories","Beauty"],
        ["Apparel","Bags","Jewellery","Beauty"],
                
        ["Apparel","Footwear","Accessories","Jewellery"],
        ["Apparel","Footwear","Accessories","Beauty"],
        ["Apparel","Footwear","Jewellery","Beauty"],
        
        ["Bags","Footwear","Accessories","Jewellery"],
        ["Bags","Footwear","Accessories","Beauty"],
        ["Bags","Footwear","Jewellery","Beauty"]
    ];
    
    var arr6999 = [
        ["Apparel","Bags","Footwear","Accessories","Jewellery"],
        ["Apparel","Bags","Footwear","Jewellery","Beauty"],
        ["Apparel","Bags","Footwear","Accessories","Beauty"],      
        ["Apparel","Bags","Accessories","Jewellery","Beauty"],
              
        ["Apparel","Footwear","Accessories","Jewellery","Beauty"],
        
        ["Bags","Footwear","Accessories","Jewellery","Beauty"],
    ];
    
    
    var strCurrBoxType, strCurrSelectedCategory;
    var arrCurrBoxType = [];
    var isCategorySelect = false;
    
    var arrRemainingCategories = [];
    var arrShuffledCategories = [];
    
    //== 0 RESET ALL function ============================
    function ResetAll(){
        strCurrBoxType = "";
        arrCurrBoxType = [];
    }
    
    //== 1. BOX TYPE function ============================ 
    //@@HARDCODE
    arrCurrBoxType = arr2999;
    
    
    function SCBoxType(obj) {
        var currentObj = $(obj);
        strCurrBoxType = currentObj.val();
        if(strCurrBoxType=="2999"){
            arrCurrBoxType = arr2999;
        }
        else if(strCurrBoxType=="4999"){
            arrCurrBoxType = arr4999;
        }
        else if (strCurrBoxType=="6999"){
            arrCurrBoxType = arr6999; 
        }
    }
  
    //== Category Selection function =====================
    var arrFilteredCategorySet = [];
    var currentObj, currentObjContain, currentObjValue;
    function SCBoxCategorySelection(obj){
        currentObj = $(obj);       
        currentObjContain =  currentObj.closest(".scbox-container");
        arrShuffledCategories = [];      
        intCtr = 0;
        
        msg("INITddd ================================================================");
        GetAllCategorySets();
        //msg("$$STORE ================================================================");
        //msg(arrUserCategorySelected);
    }
    
    
 //== 1.Get all Array set as per category selection
    var intCtr = 0;
    var strSelectedValue;
    function GetAllCategorySets(currentObjValue) { 
        isCategorySelect = false;   
        currentObjContain.find(".sc-checkbox").each(function(e){
            if($(this).hasClass("active")){
                currentObjValue = $(this).find("span").text();
                if(!isCategorySelect) {
                    GetCategorySet(arrCurrBoxType,currentObjValue);
                    isCategorySelect = true;
                } 
                else {
                    GetCategorySet(arrFiltered,currentObjValue);
                } 
            }
        });
    }
    
    //== 2.fetching the combination set of arrays as per user selection
    var arrFiltered = [];
    function GetCategorySet(arrCategory,currentObjValue){
        arrFiltered = [];
        //pushing all the category set as per user selection
        $.each(arrCategory, function(i, filterName) {
            $.each(arrCategory[i],function(j,filterName){
                if(arrCategory[i][j]==currentObjValue){
                    arrShuffledCategories.push(arrCategory[i]);
                    arrFiltered.push(arrCategory[i]);
                }
            });         
        });        
        //msg(arrFiltered);
        //Convert multi-deminsion array to single array
        ChangeToSingleArray(arrFiltered);
    }
    

    //== 3.Change the multi set of arrays to single array
    function ChangeToSingleArray(arr){    
        var arrCombinationCategories = [];
        $.each(arr,function(i,value){
            arrCombinationCategories = arrCombinationCategories.concat(arr[i]);
        });
        //msg("Filter-1 for array =================");
        //msg(arrCombinationCategories);
        
        //== remove duplicated values from an array
        arrCombinationCategories = arrCombinationCategories.filter( function( item, index, inputArray ) {
            return inputArray.indexOf(item) == index;
        });
        
        //msg("Filter-1 for array =================");
        //msg(arrCombinationCategories);
        
        var i = 0;
        var arrSelectedCategories = [];
        arrUserCategorySelected = [];      
        currentObjContain.find(".sc-checkbox").each(function(e){
            if($(this).hasClass("active")){
                var removeItem = $(this).find("span").text();
              
                //store the user selected values from filtered category array
                arrUserCategorySelected.push(removeItem);
                
                //remove the user selected values from filtered category array
                arrCombinationCategories = $.grep(arrCombinationCategories, function(value) {
                      return value != removeItem;
                });
                
            }
        });
        
        //msg("Filter-3 for array =================");
        //msg(arrCombinationCategories);
        EnableDisableCheckbox(arrCombinationCategories);
    }
    
    //== Enable/Disable category check boxes as per matix
    function EnableDisableCheckbox(arr){
        currentObjContain.find(".sc-checkbox").each(function(e){
            if(!$(this).hasClass("active")){
              $(this).addClass("state-disabled"); 
            }
            
        });
        
        var containCategory = currentObjContain.find(".sc-checkbox");
       $.each(arr,function(i,arrValue){ 
            $.each(containCategory,function(j,eleValue){
                if(arrValue == $(eleValue).find("span").text()){
                    $(eleValue).removeClass("state-disabled");
                }

            });
        });     
    }
    
    //== CURRENTLY NOT IN USE ====================== (DONT DELETE)
    function RemoveSelectedCategories (arr){
        $.each(arr,function(i,value){
            if(value==strCurrSelectedCategory){
                arr.splice(i, 1);
            }
        });
        return arr;
    } 
    
    //== UserInfo Save function ======================
    function SCBoxUserInfo(obj) {
        var currentObj = $(obj);
        msg(currentObj.val());
    }
    
 
    
    function TriggerAllTime(){
        //msg("strCurrBoxType: "+strCurrBoxType);
    }
    
    //== DATA SAVE STRUCTURE ==========================
    var arrUserCategorySelected = [];
    
    
    
    $(document).ready(function(){
    msg("LOADDDDDDDDING..."); 
});

})(jQuery);


//== To change the steps ==============================
function ChangeStep(intScreen){
    msg("intScreen: "+intScreen);
    $("#scbox-form > div").each(function(e){
        $(this).addClass("hide");
    });
    $("#slide_"+intScreen).removeClass("hide");
}


//== APPREAL CATEGORY SELECTION =======================

//## GLOBAL PARAM -------------------------------
var currentGender;
currentGender = "women";

//## Apparel: PA function ******************************************************

//## Apparel: Add body shape function -----------
var strBodyShape;
var objBodyShapeContainer;
$(".pa-container.women > div, .pa-container.men > div").on("click touch", function(){
    ResetBodyShape();  
    GetBodyShape($(this));
});

//## GetBodyShape function
function GetBodyShape(obj){
    ResetBodyShape();
    obj.removeClass('deactive').addClass('active');
    obj.find(".img-wrp > img").first().addClass("hide");
    obj.find(".img-wrp > img").last().removeClass("hide");
    
    strBodyShape = obj.find(".title").text();
    msg("strBodyShape: "+strBodyShape);
}

//## SetBodyShape function
SetBodyShape('Apple');
function SetBodyShape(str){
    var obj;
    ResetBodyShape();
    objBodyShapeContainer.each(function() {     
        if($(this).find(".title").text() == str){
            obj = $(this);        
            obj.removeClass('deactive').addClass('active');
            obj.find(".img-wrp > img").first().addClass("hide");
            obj.find(".img-wrp > img").last().removeClass("hide");
        }
    });  
}




//## ResetBodyShape function (women and men)
function ResetBodyShape() {
    $(".pa-container.women > div, .pa-container.men > div").each(function(e){
        $(this).removeClass('active');
        $(this).addClass('deactive');
        $(this).find(".img-wrp > img").first().removeClass("hide");
        $(this).find(".img-wrp > img").last().addClass("hide");
    });
    if(currentGender=="women"){
        objBodyShapeContainer = $(".pa-container.women > div");
    } else{
        objBodyShapeContainer = $(".pa-container.men > div");
    }
    
}

//## Apparel: STUFF-DONT-NEED function ******************************************************
var intStuff = 0;
var intMaxLimit_StuffDoneNeed = 5;
var arrStuffDontNeed = [];

//## GetStuffDontNeed CLICK/TOUCH EVENT function ----------------
function GetStuffDontNeed(obj) {
    arrStuffDontNeed = [];
    var obj = $(obj);
    if(intStuff<intMaxLimit_StuffDoneNeed){
        if(!obj.hasClass("active")){
            obj.addClass("active");
            intStuff++;
        } else {
            obj.removeClass("active");
            intStuff--;
        } 
    } 
    else if(intStuff==intMaxLimit_StuffDoneNeed) {
        if(obj.hasClass("active")){
            obj.removeClass("active");
            intStuff--;
        }
    } 
    GET_StuffDontNeed('get');
    msg(arrStuffDontNeed);
}

//## SetStuffDontNeed GETTER ------------------------------------
function GET_StuffDontNeed(type,arr){
    $.each($(".stuff-dont-need li"), function(i, value){
        var thisObj = $(this);
        var thisObjValue = thisObj.find(".title-3").text();
        if(type=="get"){
            if(thisObj.hasClass('active')) {
                arrStuffDontNeed.push(thisObjValue);
            }
        }
        else if(type=="reset"){
            thisObj.removeClass("active");
        }
    });
}

//## SetStuffDontNeed SETTER ------------------------------------
arrStuffDoneNeed =  ["DRESSES", "SHORTS AND SKIRTS", "OUTERWEAR ", "JUMPSUITS "];
SET_StuffDontNeed(arrStuffDoneNeed)
function SET_StuffDontNeed(arr){
     $.each($("ul.stuff-dont-need li"), function(j, value){             
        $.each($("ul.stuff-dont-need li"), function(i, value){
            if($(this).find(".title-3").text() == arrStuffDoneNeed[j]){
                $(this).trigger('click');
            }
        });
    });   
}






//## Apparel: MY-HEIGHT function ******************************************************

//## GET_MyHeight function ------------------------------------
var strMyHT_feet;
function GET_MyHeight(obj){
    var thisObj = $(obj);
    strMyHT_feet = thisObj.val(); 
    msg(strMyHT_feet);
}
//## SET_MyHeight function ------------------------------------
strMyHT_feet = '4 ft';
SET_MyHeight(strMyHT_feet)
function SET_MyHeight(str){
    $('select.height_feet').val(str);
}

//## GET_MyHeightInches function ------------------------------------
var strMyHT_inches;
function GET_MyHeightInches(obj){
    var thisObj = $(obj);
    strMyHT_inches = thisObj.val(); 
    msg(strMyHT_inches);
}
//## SET_MyHeightInches function ------------------------------------
strMyHT_inches = '10 inch';
SET_MyHeightInches(strMyHT_inches)
function SET_MyHeightInches(str){
    $('select.height_inches').val(str);
}



//## GET_Beautyproduct function ------------------------------------
var strBeautyProduct;
function GET_BeautyProductWomen(obj){
    var thisObj = $(obj);
    strBeautyProduct = thisObj.val(); 
    msg(strBeautyProduct);
}
//## SET_MyHeight function ------------------------------------
strBeautyProduct = 'LIPSTICK';
SET_BeautyProductWomen(strBeautyProduct)
function SET_BeautyProductWomen(str){
    $('select.beautyproduct_women').val(str);
}



//## GET_MyCupSize function ------------------------------------
var strMyCSize;
function GET_MyCupSize(obj){
    var thisObj = $(obj);
    strMyCSize = thisObj.val(); 
    msg(strMyCSize);
}
//## SET_MyCupSize function ------------------------------------
strMyCSize = 'DD';
SET_MyCupSize(strMyCSize)
function SET_MyCupSize(str){
    $('select.cup-size').val(str);
}



//## GET_MyBandSize function ------------------------------------
var strMyBSize;
function GET_MyBandSize(obj){
    var thisObj = $(obj);
    strMyBSize = thisObj.val(); 
    msg(strMyBSize);
}
//## SET_MyCupSize function ------------------------------------
strMyBSize = '36';
SET_MyBandSize(strMyBSize)
function SET_MyBandSize(str){
    $('select.band-size').val(str);
}

//## GET_MybagSize function ------------------------------------
var strMybagsize;
function GET_SizeBagAfterPayment(obj){
    var thisObj = $(obj);
    strMybagsize = thisObj.val(); 
    msg(strMybagsize);
}
//## SET_MybagSize function ------------------------------------
strMybagsize = 'S';
SET_SizeBagAfterPayment(strMybagsize)
function SET_SizeBagAfterPayment(str){
    $('select.bag-size').val(str);
}

//## GET_MySizeTop function ------------------------------------
var strMySizeTop;
function GET_MySizeTop(obj){
    var thisObj = $(obj);
    if(thisObj.hasClass('size-top')){
        strMySizeTop = thisObj.val();
    }
    msg(strMySizeTop);
}

//## SET_MyTop function ------------------------------------
strMySizeTop = 'XXL/UK 20/US 18/EU 48';
SET_MySizeTop(strMySizeTop);
function SET_MySizeTop(str){
    $('select.size-top').val(str);
}

//## GET_AccessoryMen function ------------------------------------
var strAccessorymen;
function GET_AccessoryMen(obj){
    var thisObj = $(obj);
    if(thisObj.hasClass('accesory_men')){
        strAccessorymen = thisObj.val();
    }
    msg(strAccessorymen);
}



//## SET_AccessoryMen function ------------------------------------
strAccessorymen = 'XXL/UK 20/US 18/EU 48';
SET_AccessoryMen(strAccessorymen);
function SET_AccessoryMen(str){
    $('select.accesory_men').val(str);
}

//## GET_AccessoryWomen function ------------------------------------
var strAccessorywomen;
function GET_AccessoryWomen(obj){
    var thisObj = $(obj);
    if(thisObj.hasClass('accesory_women')){
        strAccessorywomen = thisObj.val();
    }
    msg(strAccessorywomen);
}

//## SET_AccessoryWomen function ------------------------------------
strAccessorywomen = 'XXL/UK 20/US 18/EU 48';
SET_AccessoryWomen(strAccessorywomen);
function SET_AccessoryWomen(str){
    $('select.accesory_women').val(str);
}



//## GET_FootwearMen function ------------------------------------
var strFootwearmen;
function GET_FootwearMen(obj){
    var thisObj = $(obj);
    if(thisObj.hasClass('footwear_men')){
        strFootwearmen = thisObj.val();
    }
    msg(strFootwearmen);
}

//## SET_FootwearMen function ------------------------------------
strFootwearmen = '44/UK 9/US 10';
SET_FootwearMen(strFootwearmen);
function SET_FootwearMen(str){
    $('select.footwear_men').val(str);
}

//## GET_FootwearWomen function ------------------------------------
var strFootwearwomen;
function GET_FootwearWomen(obj){
    var thisObj = $(obj);
    if(thisObj.hasClass('footwear_women')){
        strFootwearwomen = thisObj.val();
    }
    msg(strFootwearwomen);
}

//## SET_FootwearWomen function ------------------------------------
strFootwearwomen = '44/UK 9/US 10';
SET_FootwearWomen(strFootwearwomen);
function SET_FootwearWomen(str){
    $('select.footwear_women').val(str);
}


//## GET_MySizeTshirt function ------------------------------------
var strMySizeTshirt;
function GET_MySizeTshirt(obj){
    var thisObj = $(obj);
    if(thisObj.hasClass('size-tshirt')){
        strMySizeTshirt = thisObj.val();
    }
    msg(strMySizeTshirt);
}

//## SET_MySizeTshirt function ------------------------------------
strMySizeTshirt = '';
SET_MySizeTshirt(strMySizeTshirt);
function SET_MySizeTshirt(str){
    $('select.size-tshirt').val(str);
}


//## GET_MySizeTrouser function ------------------------------------
var strMySizeTrouser;
function GET_MySizeTrouser(obj){
    var thisObj = $(obj);
    if(thisObj.hasClass('size-trouser')){
        strMySizeTrouser = thisObj.val();
    }
    msg(strMySizeTrouser);
}

//## SET_MySizeTshirt function ------------------------------------
strMySizeTrouser = '';
SET_MySizeTrouser(strMySizeTrouser);
function SET_MySizeTrouser(str){
    $('select.size-trouser').val(str);
}

//## GET_MySizeJeans function ------------------------------------
var strMySizeJeans;
function GET_MySizeJeans(obj){
    var thisObj = $(obj);
    if(thisObj.hasClass('size-jeans')){
        strMySizeJeans = thisObj.val();
    }
    msg(strMySizeJeans);
}

//## SET_MySizeJeans function ------------------------------------
strMySizeJeans = '';
SET_MySizeJeans(strMySizeJeans);
function SET_MySizeJeans(str){
    $('select.size-jeans').val(str);
}


//## GetFitYouLike CLICK/TOUCH EVENT function ----------------
var arrFitYouLike = [];
//GETTER
function GET_FitYouLike(type){
    arrFitYouLike = [];
    $.each($("ul.fit-you-like li"), function(i, value){
            if($(this).hasClass('active')) {
                arrFitYouLike.push($(this).find(".title-3").text());
            }
    });
    msg(arrFitYouLike)
}

//SETTER  
arrFitYouLike =  ["RELAXED", "FLOWY"];
SET_FitYouLike(arrFitYouLike)
function SET_FitYouLike(arr){
     $.each($("ul.fit-you-like li"), function(j, value){             
        $.each($(".fit-you-like li"), function(i, value){
            if($(this).find(".title-3").text() == arr[j]){
                $(this).trigger('click');
            }
        });
    });   
}

//## GetFootwearWomenType CLICK/TOUCH EVENT function ----------------
var arrFootwearwomentype = [];
//GETTER
function GET_FootwearWomenType(type){
    arrFootwearwomentype = [];
    $.each($("ul.footwear_women_type li"), function(i, value){
            if($(this).hasClass('active')) {
                arrFootwearwomentype.push($(this).find(".title-3").text());
            }
    });
    msg(arrFootwearwomentype)
}

//SETTER  
arrFootwearwomentype =  ["FLATS"];
SET_FootwearWomenType(arrFootwearwomentype)
function SET_FootwearWomenType(arr){
     $.each($("ul.footwear_women_type li"), function(j, value){             
        $.each($(".footwear_women_type li"), function(i, value){
            if($(this).find(".title-3").text() == arr[j]){
                $(this).trigger('click');
            }
        });
    });   
}

//## GetFootwearMenType CLICK/TOUCH EVENT function ----------------
var arrFootwearmentype = [];
//GETTER
function GET_FootwearMenType(type){
    arrFootwearmentype = [];
    $.each($("ul.footwear_men_type li"), function(i, value){
            if($(this).hasClass('active')) {
                arrFootwearmentype.push($(this).find(".title-3").text());
            }
    });
    msg(arrFootwearmentype)
}

//SETTER  
arrFootwearmentype =  ["SNEAKERS"];
SET_FootwearMenType(arrFootwearmentype)
function SET_FootwearMenType(arr){
     $.each($("ul.footwear_men_type li"), function(j, value){             
        $.each($(".footwear_men_type li"), function(i, value){
            if($(this).find(".title-3").text() == arr[j]){
                $(this).trigger('click');
            }
        });
    });   
}

//## GetBagmen CLICK/TOUCH EVENT function ----------------
var arrBagMen = [];
//GETTER
function GET_Bagmen(type){
    arrBagMen = [];
    $.each($("ul.bag_men li"), function(i, value){
            if($(this).hasClass('active')) {
                arrBagMen.push($(this).find(".title-3").text());
            }
    });
    msg(arrBagMen)
}

//SETTER  
arrBagMen =  ["WALLETS"];
SET_Bagmen(arrBagMen)
function SET_Bagmen(arr){
     $.each($("ul.bag_men li"), function(j, value){             
        $.each($(".bag_men li"), function(i, value){
            if($(this).find(".title-3").text() == arr[j]){
                $(this).trigger('click');
            }
        });
    });   
}

//## GetBagwomen CLICK/TOUCH EVENT function ----------------
var arrBagWomen = [];
//GETTER
function GET_Bagwomen(type){
    arrBagWomen = [];
    $.each($("ul.bag_women li"), function(i, value){
            if($(this).hasClass('active')) {
                arrBagWomen.push($(this).find(".title-3").text());
            }
    });
    msg(arrBagWomen)
}

//SETTER  
arrBagWomen =  ["SLING"];
SET_Bagwomen(arrBagWomen)
function SET_Bagwomen(arr){
     $.each($("ul.bag_women li"), function(j, value){             
        $.each($(".bag_women li"), function(i, value){
            if($(this).find(".title-3").text() == arr[j]){
                $(this).trigger('click');
            }
        });
    });   
}


//## GetSunglasses CLICK/TOUCH EVENT function ----------------
var arrSunGlasses = [];
//GETTER
function GET_Sunglasses(type){
    arrSunGlasses = [];
    $.each($("ul.sun-glasses li"), function(i, value){
            if($(this).hasClass('active')) {
                arrSunGlasses.push($(this).find(".title-3").text());
            }
    });
    msg(arrSunGlasses)
}

//SETTER  
arrSunGlasses =  ["AVIATORS", "ROUND"];
SET_Sunglasses(arrSunGlasses)
function SET_Sunglasses(arr){
     $.each($("ul.sun-glasses li"), function(j, value){             
        $.each($(".sun-glasses li"), function(i, value){
            if($(this).find(".title-3").text() == arr[j]){
                $(this).trigger('click');
            }
        });
    });   
}


//## GET_Dress function ------------------------------------
var strDressSize;
function GET_DressSize(obj){
    if($(obj).hasClass('dress_size')){
        strDressSize = $(obj)   .val();
    } 
    msg(strDressSize);
}

//## SET_Dress function ------------------------------------
strDressSize = 'XL/UK 18/US 16/EU 46';
SET_DressSize(strDressSize)
function SET_DressSize(str){
    $('select.dress_size').val(str);
}

//## FIT-LIKE-DRESS function -----------------------------

//GETTER
var arrFitLikeDress = [];
function GET_FitLikeDress(obj){
    arrFitLikeDress = [];
    $.each($("ul.fit-like-dress li"), function(i, value){
            if($(this).hasClass('active')) {
                arrFitLikeDress.push($(this).find(".title-3").text());
            }
    });
    msg(arrFitLikeDress)
}
//SETTER
arrFitLikeDress =  ["BODYCON", "SKATER", "A LINE"];
SET_FitLikeDress(arrFitLikeDress)
function SET_FitLikeDress(arr){
   $.each($("ul.fit-like-dress li"), function(j, value){             
        $.each($(".fit-like-dress li"), function(i, value){
            if($(this).find(".title-3").text() == arr[j]){
                $(this).trigger('click');
            }
        });
    });  
}


//## LengthSkirtDress CLICK/TOUCH EVENT function ----------------
var arrLengthSkirtDress = [];
//GETTER
function GET_LengthSkirtDress(type){
    arrLengthSkirtDress = [];
    $.each($("ul.length-skirt-dress li"), function(i, value){
            if($(this).hasClass('active')) {
                arrLengthSkirtDress.push($(this).find(".title-3").text());
            }
    });
    msg(arrLengthSkirtDress)
}

//SETTER  
arrLengthSkirtDress =  ["MINI", "MAXI"];
SET_LengthSkirtDress(arrLengthSkirtDress)
function SET_LengthSkirtDress(arr){
     $.each($("ul.length-skirt-dress li"), function(j, value){             
        $.each($(".length-skirt-dress li"), function(i, value){
            if($(this).find(".title-3").text() == arr[j]){
                $(this).trigger('click');
            }
        });
    });   
}

//## PrintToAvoid CLICK/TOUCH EVENT function ----------------
var arrPrintToAvoid = [];
//GETTER
function GET_PrintToAvoid(type){
    arrPrintToAvoid = [];
    $.each($("ul.print-to-avoid li"), function(i, value){
            if($(this).hasClass('active')) {
                arrPrintToAvoid.push($(this).find(".title-3").text());
            }
    });
    msg(arrPrintToAvoid)
}

//SETTER  
arrPrintToAvoid =  ["STRIPES", "ANIMAL"];
SET_PrintToAvoid(arrPrintToAvoid)
function SET_PrintToAvoid(arr){
     $.each($("ul.print-to-avoid li"), function(j, value){             
        $.each($(".print-to-avoid li"), function(i, value){
            if($(this).find(".title-3").text() == arr[j]){
                $(this).trigger('click');
            }
        });
    });   
}


//## PrintToAvoidAfterPayment CLICK/TOUCH EVENT function ----------------
var arrPrinttoavoidafterpayment = [];
//GETTER
function GET_PrintToAvoidAfterPayment(type){
    arrPrinttoavoidafterpayment = [];
    $.each($("ul.print-to-avoid-afterpayment li"), function(i, value){
            if($(this).hasClass('active')) {
                arrPrinttoavoidafterpayment.push($(this).find(".title-3").text());
            }
    });
    msg(arrPrinttoavoidafterpayment)
}

//SETTER  
arrPrinttoavoidafterpayment =  ["Stripes", "Animal"];
SET_PrintToAvoidAfterPayment(arrPrinttoavoidafterpayment)
function SET_PrintToAvoidAfterPayment(arr){
     $.each($("ul.print-to-avoid-afterpayment li"), function(j, value){             
        $.each($(".print-to-avoid-afterpayment li"), function(i, value){
            if($(this).find(".title-3").text() == arr[j]){
                $(this).trigger('click');
            }
        });
    });   
}


//## ColorToAvoidAfterPayment CLICK/TOUCH EVENT function ----------------
var arrColortoavoidafterpayment = [];
//GETTER
function GET_ColorToAvoidAfterPayment(type){
    arrColortoavoidafterpayment = [];
    $.each($("ul.color-to-avoid-afterpayment li"), function(i, value){
            if($(this).hasClass('active')) {
                arrColortoavoidafterpayment.push($(this).find(".title-3").text());
            }
    });
    msg(arrColortoavoidafterpayment)
}

//SETTER  
arrColortoavoidafterpayment =  ["", ""];
SET_ColorToAvoidAfterPayment(arrColortoavoidafterpayment)
function SET_ColorToAvoidAfterPayment(arr){
     $.each($("ul.color-to-avoid-afterpayment li"), function(j, value){             
        $.each($(".color-to-avoid-afterpayment li"), function(i, value){
            if($(this).find(".title-3").text() == arr[j]){
                $(this).trigger('click');
            }
        });
    });   
}


//## BeautyShadeAfterPayment CLICK/TOUCH EVENT function ----------------
var arrBeautyshadeafterpayment = [];
//GETTER
function GET_BeautyShadeAfterPayment(type){
    arrBeautyshadeafterpayment = [];
    $.each($("ul.beautyshade-afterpayment li"), function(i, value){
            if($(this).hasClass('active')) {
                arrBeautyshadeafterpayment.push($(this).find(".title-3").text());
            }
    });
    msg(arrBeautyshadeafterpayment)
}

//SETTER  
arrBeautyshadeafterpayment =  ["", ""];
SET_BeautyShadeAfterPayment(arrBeautyshadeafterpayment)
function SET_BeautyShadeAfterPayment(arr){
     $.each($("ul.beautyshade-afterpayment li"), function(j, value){             
        $.each($(".beautyshade-afterpayment li"), function(i, value){
            if($(this).find(".title-3").text() == arr[j]){
                $(this).trigger('click');
            }
        });
    });   
}



//## BeltsAfterPayment CLICK/TOUCH EVENT function ----------------
var arrbeltsafterpayment = [];
//GETTER
function GET_BeltsAfterPayment(type){
    arrbeltsafterpayment = [];
    $.each($("ul.bel-ts li"), function(i, value){
            if($(this).hasClass('active')) {
                arrbeltsafterpayment.push($(this).find(".title-3").text());
            }
    });
    msg(arrbeltsafterpayment)
}

//SETTER  
arrbeltsafterpayment =  ["Stripes", "Animal"];
SET_BeltsAfterPayment(arrbeltsafterpayment)
function SET_BeltsAfterPayment(arr){
     $.each($("ul.bel-ts li"), function(j, value){             
        $.each($(".bel-ts li"), function(i, value){
            if($(this).find(".title-3").text() == arr[j]){
                $(this).trigger('click');
            }
        });
    });   
}


//## JewelleryAfterPayment CLICK/TOUCH EVENT function ----------------
var arrjewelleryafterpayment = [];
//GETTER
function GET_JewelleryAfterPayment(type){
    arrjewelleryafterpayment = [];
    $.each($("ul.jewel-lery li"), function(i, value){
            if($(this).hasClass('active')) {
                arrjewelleryafterpayment.push($(this).find(".title-3").text());
            }
    });
    msg(arrjewelleryafterpayment)
}

//SETTER  
arrjewelleryafterpayment =  ["", ""];
SET_JewelleryAfterPayment(arrjewelleryafterpayment)
function SET_JewelleryAfterPayment(arr){
     $.each($("ul.jewel-lery li"), function(j, value){             
        $.each($(".jewellery li"), function(i, value){
            if($(this).find(".title-3").text() == arr[j]){
                $(this).trigger('click');
            }
        });
    });   
}



//## JewelleryTypeAfterPayment CLICK/TOUCH EVENT function ----------------
var arrjewellerytypeafterpayment = [];
//GETTER
function GET_JewelleryTypeAfterPayment(type){
    arrjewellerytypeafterpayment = [];
    $.each($("ul.jewellery_type li"), function(i, value){
            if($(this).hasClass('active')) {
                arrjewellerytypeafterpayment.push($(this).find(".title-3").text());
            }
    });
    msg(arrjewellerytypeafterpayment)
}

//SETTER  
arrjewelleryafterpayment =  ["", ""];
SET_JewelleryTypeAfterPayment(arrjewellerytypeafterpayment)
function SET_JewelleryTypeAfterPayment(arr){
     $.each($("ul.jewellery_type li"), function(j, value){             
        $.each($(".jewellery_type li"), function(i, value){
            if($(this).find(".title-3").text() == arr[j]){
                $(this).trigger('click');
            }
        });
    });   
}





//## HeeltypeAfterPayment CLICK/TOUCH EVENT function ----------------
var arrHeelTypeafterpayment = [];
//GETTER
function GET_HeelTypeAfterPayment(type){
    arrHeelTypeafterpayment = [];
    $.each($("ul.heel-type li"), function(i, value){
            if($(this).hasClass('active')) {
                arrHeelTypeafterpayment.push($(this).find(".title-3").text());
            }
    });
    msg(arrHeelTypeafterpayment)
}

//SETTER  
arrHeelTypeafterpayment =  ["", ""];
SET_HeelTypeAfterPayment(arrHeelTypeafterpayment)
function SET_HeelTypeAfterPayment(arr){
     $.each($("ul.heel-type li"), function(j, value){             
        $.each($(".heel-type li"), function(i, value){
            if($(this).find(".title-3").text() == arr[j]){
                $(this).trigger('click');
            }
        });
    });   
}




//## TrouserSize function ------------------------------------
//GETTER
var strTrouserSize;
function GET_TrouserSize(obj){
    if($(obj).hasClass('trouser-size')){
        strTrouserSize = $(obj).val();
    } 
    msg(strTrouserSize);
}
//SETTER
strTrouserSize = 'M/28';
SET_TrouserSize(strTrouserSize)
function SET_TrouserSize(str){
    $('select.trouser-size').val(str);
}


//## HeelheightSize function ------------------------------------
//GETTER
var strHhSize;
function GET_HeelheightSize(obj){
    if($(obj).hasClass('heel-height-size')){
        strHhSize = $(obj).val();
    } 
    msg(strHhSize);
}
//SETTER
strHhSize = 'MEDIUM:2.5”-3.5”';
SET_HeelheightSize(strHhSize)
function SET_HeelheightSize(str){
    $('select.heel-height-size').val(str);
}



//## JeansSize function ------------------------------------
//GETTER
var strJeansSize;
function GET_JeansSize(obj){
    if($(obj).hasClass('jeans-size')){
        strJeansSize = $(obj).val();
    } 
    msg(strJeansSize);
}
//SETTER
strJeansSize = 'XXL/34';
SET_JeansSize(strJeansSize)
function SET_JeansSize(str){
    $('select.jeans-size').val(str);
}


//## BOTTOM-FIT-LIKE function ------------------------------------

//

//GETTER
var arrBottomFitLike = [];
function GET_BottomFitLike(obj){
    arrBottomFitLike = [];
    $.each($("ul.bottom-fit-like li"), function(i, value){
            if($(this).hasClass('active')) {
                arrBottomFitLike.push($(this).find(".title-3").text());
            }
    });
    msg('2.arrBottomFitLike: '+arrBottomFitLike);
}

//SETTER
arrBottomFitLike =  ["FLARED", "SLIM"];
SET_BottomFitLike (arrBottomFitLike)
function SET_BottomFitLike(arr){
   $.each($("ul.bottom-fit-like li"), function(j, value){             
        $.each($("ul.bottom-fit-like li"), function(i, value){
            if($(this).find(".title-3").text() == arr[j]){
                $(this).trigger('click');
            }
        });
    });  
}


    