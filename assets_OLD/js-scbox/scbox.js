function msg(m) { console.log(m); 
}
var getUrl = window.location;
var clickcnt = 0;
var scbox_taxes = $('#scbox_servicetax').val();
var scbox_quantity = 1;
var is_order_placed = false; 

$(document).ready(function(e){

var objParam = getUrl.toString().split("/");
var seclastparam = objParam[objParam.length-2];
var lastparam = objParam[objParam.length-1];
/*msg('getUrl--'+getUrl);
msg('seclastparam--'+seclastparam);
msg('lastparam--'+lastparam);*/
  if(seclastparam=='book-scbox')
  { 
    ChangeGender();
    //var boxprice = localStorage.getItem('SCbox_price');
    var boxprice = getCookie('Scbox_price');
    $('#scbox_price').val(boxprice);    
    $('#checkout_price').html('<i class="fa fa-inr"></i> '+parseFloat(boxprice).toFixed(2));   
    var scbox_subtotal = parseFloat(scbox_quantity)*parseFloat(boxprice);
    var checkout_taxes  = (parseFloat(scbox_taxes)*parseFloat(scbox_subtotal))/100;
    checkout_taxes = Math.round(checkout_taxes * 100) / 100;
    $('#checkout_price').html('<i class="fa fa-inr"></i> '+parseFloat(scbox_subtotal).toFixed(2));
    $('#checkout_subtotal').html('<i class="fa fa-inr"></i> '+parseFloat(scbox_subtotal).toFixed(2));
    $('#checkout_taxes').html('<i class="fa fa-inr"></i> '+parseFloat(checkout_taxes).toFixed(2));    
    scbox_total =  scbox_subtotal + checkout_taxes;
    scbox_total = Math.round(scbox_total * 100) / 100;
    $('#checkout_total').html('<i class="fa fa-inr"></i> '+parseFloat(scbox_total).toFixed(2));
    $('#cart_total').val(scbox_total);   
  }
  //console.log('lastparam--'+lastparam);
  if(lastparam=='sc-box?f=1')
  {
   $('html, body').animate({
          scrollTop: $("#scbox-packages").offset().top
      }, 2000);
  }


//##Gender Female Click -------------------------------
 $('#scboxgender-female').click(function(e){
      if(!$(this).hasClass("state-disabled")) {
     	    $('#gender-error').addClass('hide');
          $('#scboxgender-male').removeClass('active');
          $('#scboxgender-female').addClass('active');
          $('#scbox_gender').val('1');
          $('.box-style').removeClass('box-style-male');
          $('.box-style').addClass('box-style-female');
          $('.box-body-shape').removeClass('male');
          $('.box-body-shape').addClass('female');
          $('#titlebottom').html('Top/Dress');
          intGender = $('#scbox_gender').val();
           document.cookie = "scbox-gender=women;path=/";
           if(intGender!=$('scbox_sessiongender').val())
          {
            $('#scbox_style').val('');
            $('#body_shape_ans').val('');
            $('#scbox_style').attr('data-style','');
          }/*else
          {
            $('#scboxgender-female').trigger("click");
          }*/
          GenderWiseGetData();
      }
      else {

         $('#gender-error').addClass('hide');
          $('#scboxgender-male').removeClass('active');
          $('#scboxgender-female').addClass('active');
          $('#scbox_gender').val('1');
          $('.box-style').removeClass('box-style-male');
          $('.box-style').addClass('box-style-female');
          $('.box-body-shape').removeClass('male');
          $('.box-body-shape').addClass('female');
          $('#titlebottom').html('Top/Dress');
          intGender = $('#scbox_gender').val();
          document.cookie = "scbox-gender=women;path=/";
          GenderWiseGetData();

      }
  });
//##Gender Male Click -------------------------------
    $('#scboxgender-male').click(function(e){
       if(!$(this).hasClass("state-disabled")) {
        	$('#gender-error').addClass('hide');
      		$('#scboxgender-female').removeClass('active');
      		$('#scboxgender-male').addClass('active');
      		$('#scbox_gender').val('2');
          $('.box-style').removeClass('box-style-female');
          $('.box-style').addClass('box-style-male');
          $('.box-body-shape').removeClass('female');
          $('.box-body-shape').addClass('male');
          $('#titlebottom').html('Shirt/T-shirt');
      		intGender = $('#scbox_gender').val();
          document.cookie = "scbox-gender=men;path=/";
           if(intGender!=$('scbox_sessiongender').val())
            {
              $('#scbox_style').val('');
              $('#body_shape_ans').val('');
              $('#scbox_style').attr('data-style','');
            }/*else
            {
              $('#scboxgender-male').trigger("click");
            }*/
      	 	GenderWiseGetData();
         } else{

           $('#gender-error').addClass('hide');
            $('#scboxgender-female').removeClass('active');
            $('#scboxgender-male').addClass('active');
            $('#scbox_gender').val('2');
            $('.box-style').removeClass('box-style-female');
            $('.box-style').addClass('box-style-male');
            $('.box-body-shape').removeClass('female');
            $('.box-body-shape').addClass('male');
            $('#titlebottom').html('Shirt/T-shirt');
            intGender = $('#scbox_gender').val();
            document.cookie = "scbox-gender=men;path=/";
              GenderWiseGetData();

         }

    });

    //##Bill Gender Female1 Click -------------------------------
 $('#scboxgender-billfemale').click(function(e){
      if(!$(this).hasClass("state-disabled")) {
          $('#billgender-error').addClass('hide');
          $('#scboxgender-billmale').removeClass('active');
          $('#scboxgender-billfemale').addClass('active');
          $('#scbox_billgender').val('1');
          $('.box-style').removeClass('box-style-male');
          $('.box-style').addClass('box-style-female');
          $('.box-body-shape').removeClass('male');
          $('.box-body-shape').addClass('female');
          $('#titlebottom').html('Top/Dress');         
          intGenderbill = $('#scbox_billgender').val();
          document.cookie = "scbox-billgender=women;path=/";
        
             
      }else{
         $('#gender-billerror').addClass('hide');
          $('#scboxgender-billmale').removeClass('active');
          $('#scboxgender-billfemale').addClass('active');
          $('#scbox_billgender').val('1');  
          $('.box-style').removeClass('box-style-male');
          $('.box-style').addClass('box-style-female');
          $('.box-body-shape').removeClass('male');
          $('.box-body-shape').addClass('female');
          $('#titlebottom').html('Top/Dress');       
          intGenderbill = $('#scbox_billgender').val();
          document.cookie = "scbox-billgender=women;path=/"; }
  });
//##Bill Gender Male Click -------------------------------
    $('#scboxgender-billmale').click(function(e){
       if(!$(this).hasClass("state-disabled")) {
          $('#billgender-error').addClass('hide');
          $('#scboxgender-billfemale').removeClass('active');
          $('#scboxgender-billmale').addClass('active');
          $('#scbox_billgender').val('2');
          /*$('.box-style').removeClass('box-style-female');
          $('.box-style').addClass('box-style-male');
          $('.box-body-shape').removeClass('female');
          $('.box-body-shape').addClass('male');
          $('#titlebottom').html('Shirt/T-shirt');  */       
          intGenderbill = $('#scbox_billgender').val();
          document.cookie = "scbox-billgender=men;path=/";
         }else{
            $('#billgender-error').addClass('hide');
            $('#scboxgender-billfemale').removeClass('active');
            $('#scboxgender-billmale').addClass('active');
            $('#scbox_billgender').val('2'); 
           /* $('.box-style').removeClass('box-style-female');
            $('.box-style').addClass('box-style-male');
            $('.box-body-shape').removeClass('female');
            $('.box-body-shape').addClass('male');
            $('#titlebottom').html('Shirt/T-shirt');          */
            intGenderbill = $('#scbox_billgender').val();
            document.cookie = "scbox-billgender=men;path=/"; }
    });

    $('#scbox_age').keyup(function() {  
    if ( this.value.length === 2 || this.value.length === 5 ) 
    {
        var char = $(this).val();
        $(this).val(char+'-');            
    }
    });

     $('#customboxprice').keypress(function(event){

       if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
           event.preventDefault(); //stop character from entering input
       }

      });
  //$('#scboxgender-female').trigger("click");
  
  //============================================
 // stylistCall();
  
//===============================================
  $('.box-package').click(function(e) {

    var scboxpack = $(this).attr('data-pack');
    var scboxprice = $(this).attr('data-scbox-price');
    var scboxobjectid = $(this).attr('data-scbox-objectid');
    var scboxproductid = $(this).attr('data-scbox-productid');
    if(scboxpack!='' && scboxprice!='')
    {
      $('#customboxprice-error').addClass('hide');

      $('#scbox-package').val(scboxpack);
      $('#scbox-price').val(scboxprice);
      $('#scbox-objectid').val(scboxobjectid);
      $('#scbox-productid').val(scboxproductid);
      if(scboxpack!='package4')
      {
        $('#customboxprice').val('');
      }

    }else if(scboxprice == '' && $('#customboxprice').val()=='')
    {
      $('#customboxprice-error').removeClass('hide');
       scboxprice = $('#customboxprice').val();
       $('#scbox-price').val(scboxprice);

       $(e).attr('data-scbox-price',scboxprice);
      $('#scbox-package').val(scboxpack);
      $('#scbox-price').val(scboxprice);
      $('#scbox-objectid').val(scboxobjectid);
      $('#scbox-productid').val(scboxproductid);
    }

  });




});
 msg("str 1");
  $(window).on("keypress",function(event){
    msg("str 2");
    if( (event.keyCode == 13)) {
      event.preventDefault();
      return false;
    }
  });

//##Gender wise Data call -------------------------------
var intGender = 0;
var intGenderbill = 0;
function GenderWiseGetData() {
  var gendervar = 0;
  var chkYes = document.getElementById("chkYes");
 /* if(chkYes.checked){
    gendervar = intGenderbill;
  }else{
    gendervar = intGender;
  }
  alert('chkYes.checked--'+chkYes.checked);
  alert('intGenderbill--'+intGenderbill);*/
  //alert('intGender--'+intGender);
  
  gendervar = intGender;
	if(gendervar==1){
		scbox_getdata('sizeband',gendervar);
		scbox_getdata('sizecup',gendervar);
		$('#lingerie').removeClass('hide');
		$('#lingerie1').removeClass('hide');
		$('#lingerie2').removeClass('hide');

	}else if(gendervar==2){
		$('#lingerie').addClass('hide');
		$('#lingerie1').addClass('hide');
		$('#lingerie2').addClass('hide');
	}

	scbox_getdata('bodyshape',gendervar);
	scbox_getdata('style',gendervar);
	scbox_getdata('sizetop',gendervar);
	scbox_getdata('sizebottom',gendervar);
	scbox_getdata('sizefoot',gendervar);
}

//##Gender ajax Data call -------------------------------
function scbox_getdata(type,gender)
{
    if(type!='' && gender!='')
    {
      $.ajax({
          url:sc_baseurl+'scbox/genderwisedata',
          type:'POST',
          data:{'type':type,'gender':gender,'userid':$('#scbox_userid').val(), 'objectid': $('#scbox_objectid').val()},
          success: function(data,status){
            if(type=='bodyshape')
            {
              $('#scbox_bodyshape').html(data);
            }else if(type=='style')
            {
             
              $('#scbox-style').html(data);
              msg($('#scbox-style').html());
              /*For mobile PA-style slider */
               if($('body').hasClass('device-sm'))
              {                
                 $('#scbox-slider .grid-col').addClass('swiper-slide');
              }
            /*  else if($('html').hasClass('mobile'))
              {                
                 $('#scbox-slider .grid-col').addClass('swiper-slide');
              }*/
              /*For mobile PA-style slider */

            }else if(type=='sizetop')
            {
              $('#top-size').html(data);
            }else if(type=='sizebottom')
            {
              $('#bottom-size').html(data);
            }else if(type=='sizefoot')
            {
              $('#foot-size').html(data);
            }else if(type=='sizeband')
            {
              $('#band-size').html(data);
            }else if(type=='sizecup')
            {
              $('#cup-size').html(data);
            }

          },
          error:function(xhr,desc,err)
          {

          }
      });
    }
}

//## Add body shape function -------------------------------
function AddBodyShape(type,id){

	if(type == 'body_shape'){
		$('#body_shape_ans').val(id);

		$(".box-body-shape > div > div").each(function(e){
  			$(this).removeClass('active');
        $(this).addClass('deactive');
  			$(this).find(".img-wrp > img").first().removeClass("hide");
  			$(this).find(".img-wrp > img").last().addClass("hide");

			if(id == Number($(this).attr('data-attr')))
			{
        $(this).removeClass('deactive');
				$(this).addClass('active');
				$(this).find(".img-wrp > img").first().addClass("hide");
				$(this).find(".img-wrp > img").last().removeClass("hide");
			}

		});

	}
}

//## Add body preference function -------------------------------
var intStyleSelectLimitA = 0;
var isStyleSelectetElementRemoved = false;
var isNextContainerAllocated=false;
var isFirstTimeBodyStyleSelected = true;
var user_body_shape= 0;
var arr_user_style_pref = new Array();
var arr_user_color =  new Array();
var arr_user_prints = new Array();
var strColor = "";
var strPrint = "";

function AddBodyPreferences(type,element){
  msg("element: "+element);
  var element =  $("#style_"+element);
  msg(element);
  msg("----------------------------------------------");

	if(!$(element).hasClass("active")){
		if(intStyleSelectLimitA<=2){
				$(".box-style-selected .pref").each(function(e){
					if(!$(this).hasClass("filled") && !isNextContainerAllocated) {
						nextContainer = $(this)
						isNextContainerAllocated = true;
					}
				});

				isNextContainerAllocated = false;
				intStyleSelectLimitA++;
				$(element).addClass("active");
				var img = $(element).find('img').attr("src");
				msg(img);
				msg("img");
				var intStyleID = $(element).attr("id");

				if(isFirstTimeBodyStyleSelected) {
					nextContainer = $(".box-style-selected .pref").first();

					$(nextContainer).find(".text").hide();
					$(nextContainer).find(".shape-diamond").show();
					$(nextContainer).find(".close").attr("style-id",intStyleID);
					$(nextContainer).find(".close").show();
					$(nextContainer).find("img").attr("src",img);
					$(nextContainer).addClass("filled");
					isFirstTimeBodyStyleSelected = false;
				} else {
					$(nextContainer).find(".text").hide();
					$(nextContainer).find(".shape-diamond").show();
					$(nextContainer).find(".close").attr("style-id",intStyleID);
					$(nextContainer).find(".close").show();
					$(nextContainer).find("img").attr("src",img);
					$(nextContainer).addClass("filled");
				}
			} else{
				alert("You can only select 3 preferences.");
			}
	}


	$(".box-style-selected li").each(function(e){

		arr_user_style_pref.push($(this).find(".close").attr("style-id"));
	});

	$("#scbox_style").val(arr_user_style_pref);

  $("#scbox_style").attr('data-style',arr_user_style_pref);

}

//## Remove Body Preferences --------------------
function BodyPrefClose(ele){
		var intStyleID = $(ele).attr("style-id");
		$(ele).closest("li").find("img").attr("src","");
		$(ele).closest("li").find(".text").show();
		$(ele).closest("li").find(".shape-diamond").hide();
		$(ele).closest("li").find(".close").hide();
		$(ele).closest("li").removeClass("filled");
		//
		isStyleSelectetElementRemoved = true;
		$(".body-style-pref").find(".row").find("#"+intStyleID).removeClass("active");
		intStyleSelectLimitA--;
}



//## Collect Attributes Function --------------
function AddAttributes(type,e)
{
	$(e).parent().find("span").each(function(e){
		$(this).removeClass("active");
	});
	$(e).addClass('active');
	var id = $(e).attr("id");
	$("#"+type).val(id);
}

//## Collect Colors/Texture/Prints Function --------------
/*function AddColorAttributes(type,container){
	//##Select Skintone
	$(container).parent().find("li").each(function(e){
		if(type=="scbox_skintone"){
			$(this).removeClass("active");
			var id = $(container).attr("bg-color");
			$("#"+type).val(id);
			$(container).addClass('active');
		}
	});

	//##Select SCBox Color
	if(type=="scbox_colors"){
			if(!$(container).hasClass('active')){
				$(container).addClass('active');
			} else {
				$(container).removeClass('active');
			}

			setTimeout(function(){
				arr_user_color = [];
				$("ul.list-colors").find("li").each(function(e){
					if($(this).hasClass("active")){
						strColor = $(this).attr("bg-color");
						arr_user_color.push(strColor);
					}
				});
				$("#"+type).val(arr_user_color);
			},500);
	}

	//##Select Prints
	if(type=="scbox_prints"){
			if(!$(container).hasClass('active')){
				$(container).addClass('active');
			} else {
				$(container).removeClass('active');
			}

			setTimeout(function(){
				arr_user_prints = [];
				$("ul.list-prints").find("li").each(function(e){
					if($(this).hasClass("active")){
						strPrint = $(this).attr("bg-color");
						arr_user_prints.push(strPrint);
					}
				});
				$("#"+type).val(arr_user_prints);
			},500);
		}

	//$(e).addClass('active');
	//var id = $(e).attr("bg-color");
	//$("#"+type).val(id);
}*/

//## Collect Occasion value ---------------------
function AddOccasion(type,e)
{
	$(e).closest(".list-occassion").find("li").each(function(e){
		$(this).find(".sc-checkbox").removeClass("active");
	});
	$(e).find("label").addClass("active");
	var id = $(e).attr("data-attr");

	if(id=='Custom')
	{
		$("#"+type).val(id+'-'+$('#customvalue').val());
	}else
	{
		$("#"+type).val(id);
	}

}

function addcustom()
{
	var strcustom = $('#customvalue').val();
	$("#scbox_occassion").val(strcustom);
}

function scboxregister()
{
 /* var scboxpack = $('.box-package').hasClass('active');
  */
  var objectid = $('#scbox-objectid').val();
  var package = $('#scbox-package').val();
  var price = $('#scbox-price').val();
  var productid = $('#scbox-productid').val();
  var utm_source = $('#utm_source').val();
  var utm_medium = $('#utm_medium').val();
  var utm_campaign = $('#utm_campaign').val();
  var page_type = $('#scbox-page').val();
  var utm_str = '';

   if(utm_source!='' || utm_medium!='' || utm_campaign!='')
   {
      utm_str = '&utm_source='+utm_source+'&utm_medium='+utm_medium+'&utm_campaign='+utm_campaign;
   }
  //console.log('objectid='+objectid+'=package='+package+'=price='+price+'=productid='+productid);
  if(package == 'package4')
  {
    var customprice = $('#customboxprice').val();
    if(customprice >= 2000)
    {
      $('#scbox-price').val(customprice);
      price = $('#scbox-price').val();
    }else
    {
      alert('The minimum amount for Customized SC Box is ₹2,000');
    }
    
  }else
  {
    $('#customboxprice').val('');
  }
  localStorage.setItem('SCbox_price',$('#scbox-price').val());
  document.cookie="Scbox_price="+price+";path=/";  
  document.cookie = "scbox_objectid="+objectid+";path=/";
  document.cookie="scfr="+'scbox'+";path=/";

  //msg(getCookie('Scbox_price'));
  ga('send', 'event', 'SCBOX','clicked',price);msg("ga('send', 'event', 'SCBOX','clicked',price) "+price);
  if(objectid!='' && package!='' && price!='' && productid!='' && price >=2000 )
  {
  		$("#scboxregistererror").removeClass('error');
  		$("#scboxregistererror").addClass('hide');
     //	var stylecracker_scbox = document.forms.scboxselectform;
      // 	scboxselectform.submit();
      if(page_type=='rakhi')
      {
       	window.location.href = sc_baseurl+'book-scbox/'+objectid+'?step=1a'+utm_str;
      }else
      {
        window.location.href = sc_baseurl+'book-scbox/'+objectid+'?step=1'+utm_str;
      }

  }else
  {
  	$("#scboxregistererror").addClass('error');
  	$("#scboxregistererror").removeClass('hide');
  }
}

//## Select Box Package Container --------------
function SetPackage(e)
{
	$(e).closest(".row").find(".box-package").each(function(e){
		$(this).removeClass("active");
    $(this).addClass('deactive');
	});
  $(e).removeClass('deactive');
	$(e).addClass('active');

	  var scboxpack = $(e).attr('data-pack');
    var scboxprice = $(e).attr('data-scbox-price');
    var scboxobjectid = $(e).attr('data-scbox-objectid');
    var scboxproductid = $(e).attr('data-scbox-productid');
    if(scboxpack!='' && scboxprice!='')
    {
      $('#scbox-package').val(scboxpack);
      $('#scbox-price').val(scboxprice);
      $('#scbox-objectid').val(scboxobjectid);
      $('#scbox-productid').val(scboxproductid);
      if(scboxpack!='package4')
      {
         scboxregister();
      }
     
    }
    if(lastparam=='sc-box')
    {
    	//window.location.href = sc_baseurl+'book-scbox/'+scboxobjectid;
    }

	/*var id = $(e).attr("id");
	$("#"+type).val(id);*/
}

function scbox_register()
{
  console('removecookie');
  
  var scbox_style = document.forms.scbox-form;
  scbox-form.submit();
   $("#page-loader").hide();

  var objectid = $('#scbox_objectid').val();
  window.location.href = sc_baseurl+'book-scbox/'+objectid+'?step=1';
}

function addexpt(e,t)
{
	$(e).closest('.title-2').find(".sc-checkbox").each(function(e){
		$(this).removeClass("active");
	});

	$(e).addClass('active');

	if(t=='yes')
	{
		$('#experiment').val(1);
	}else
	{
		$('#experiment').val(0);
	}
}

//## EDIT FUNCTIONS =================================================
var bodyshape, bodystyle;
function ChangeGender()
{
    intGender = $('#scbox_gender').val();
    var userid = $('#scbox_userid').val();
    bodyshape = $('#body_shape_ans').val();
    bodystyle = $('#scbox_style').val();
    bodystyle_arr = bodystyle.toString().split(",");


    if(intGender!='' && intGender==1)
    {
      $('#gender-error').addClass('hide');
      $('#scboxgender-male').removeClass('active');
      $('#scboxgender-female').addClass('active');
      $('#scbox_gender').val('1');
      $('.box-style').removeClass('box-style-male');
      $('.box-style').addClass('box-style-female');

      if(userid!='')
      {
        GenderWiseGetData();
        // if(intGender==$('scbox_sessiongender').val())
        // {
          TriggerPAFunctions();
        //}       
      }
    }else if(intGender==2)
    {
      $('#gender-error').addClass('hide');
      $('#scboxgender-female').removeClass('active');
      $('#scboxgender-male').addClass('active');
      $('#scbox_gender').val('2');
      $('.box-style').removeClass('box-style-female');
      $('.box-style').addClass('box-style-male');

      if(userid!='')
      {
       GenderWiseGetData();
       // if(intGender==$('scbox_sessiongender').val())
       //  {
          TriggerPAFunctions();
        //}
      }
    }	
}


//##Dynamic function trigger ------------------------
function TriggerPAFunctions(){
      setTimeout(function(){
        //#Trigger Body Shape
        $("#bodyshape_"+bodyshape).trigger("click");

      //#Trigger Body Style
      for(var i=0;i<bodystyle_arr.length; i++){
        var strDiv = bodystyle_arr[i];
        $("#style_"+strDiv).trigger("click");
      }
    },2000)
}

function slidenext(id)
{
  if(id!='' && id<7 && id>0)
  {

    if(id==1)
    {
		
      var final_cookies = '';
      //document.cookie = "discountcoupon_stylecracker=BOX50;path=/";
			var is_valid = 0;
      var chkYes = document.getElementById("chkYes");
      if($("#scbox_gender").val()=='' || $("#scbox_name").val()=='' || $("#scbox_age").val()=='' || $("#scbox_gender").val()=='' || $("#scbox_emailid").val()=='' || $("#scbox_mobile").val()=='' || $("#scbox_state").val()=='' || $("#scbox_city").val()=='' || $("#scbox_shipaddress").val()=='' || $("#scbox_pincode").val()=='' ){
              if($("#scbox_gender").val()=='' ){
                $("#scbox_gender-error").removeClass('hide');
                $("#scbox_gender").addClass('invalid');}
              if($("#scbox_name").val()==''){
                $("#scbox_name").addClass('invalid');}
              if($("#scbox_age").val()==''){
                $("#scbox_age").addClass('invalid');}
              if($("#scbox_emailid").val()==''){
                $("#scbox_emailid").addClass('invalid');}
              if($("#scbox_mobile").val()==''){
                $("#scbox_mobile").addClass('invalid');}
              if($("#scbox_state").val()==''){
                $("#scbox_state").addClass('invalid');}
              if($("#scbox_city").val()==''){
                $("#scbox_city").addClass('invalid');}
              if($("#scbox_shipaddress").val()==''){
                $("#scbox_shipaddress").addClass('invalid');}
              if($("#scbox_pincode").val()==''){
                $("#scbox_pincode").addClass('invalid');
				        is_valid =1;}
              
              if(chkYes.checked)
              {
                  if($("#scbox_billgender").val()=='' ){
                    $("#scbox_billgender-error").removeClass('hide');
                    $("#scbox_billgender").addClass('invalid');} 
                  if($("#scbox_billname").val()==''){
                    $("#scbox_billname").addClass('invalid');}  
                  if($("#scbox_billemailid").val()==''){
                    $("#scbox_billemailid").addClass('invalid');} 
                  if($("#scbox_billmobile").val()==''){
                    $("#scbox_billmobile").addClass('invalid');}
                  if($("#scbox_billstate").val()==''){
                    $("#scbox_billstate").addClass('invalid');} 
                  if($("#scbox_billcity").val()==''){
                    $("#scbox_billcity").addClass('invalid');}
                  if($("#scbox_billaddress").val()==''){
                    $("#scbox_billaddress").addClass('invalid');}
                  if($("#scbox_profession").val()==''){
                    $("#scbox_profession").addClass('invalid');}
                  if($("#scbox_billpincode").val()==''){
                    $("#scbox_billpincode").addClass('invalid');                   
                    is_valid =1; }
              }
              scrolltop();
          }else
          {
            $("#scbox_gender-error").addClass('hide');           
            $("#scbox_emailid").removeClass('invalid');
            $("#scbox_mobile").removeClass('invalid');
            $("#scbox_state").removeClass('invalid');
            $("#scbox_city").removeClass('invalid');
            $("#scbox_shipaddress").removeClass('invalid');
            $("#scbox_pincode").removeClass('invalid');
            $("#scbox_profession").removeClass('invalid');
            
            if(chkYes.checked){
                $("#scbox_billgender-error").addClass('hide');           
                $("#scbox_billemailid").removeClass('invalid');
                $("#scbox_billmobile").removeClass('invalid');
                $("#scbox_billstate").removeClass('invalid');
                $("#scbox_billcity").removeClass('invalid');
                $("#scbox_billaddress").removeClass('invalid');
                $("#scbox_billpincode").removeClass('invalid');
                validatedob('bill');
               }
              var mobile = $("#scbox_mobile").val();
        			var pincode = $("#scbox_pincode").val();
              var dobvalid = validatedob('ship');
        			var validname = true;
        			var namestr = $('#scbox_name').val();	
              namestr = namestr.trim();
              namestr = namestr.replace(/[^a-z\s]/gi, ' ');
              namestr = namestr.replace(/\s+/g, " ");
              $('#scbox_name').val(namestr);
        			var namearr = namestr.split(" ");			
        			if(namearr[1]==undefined || namearr[1]=='')
        			{
        				$('#scbox_name').addClass('invalid');
                scrolltop();
        				validname = false;
        			}else
        			{
        				$('#scbox_name').removeClass('invalid');
        				validname = true;
        			}
              msg(dobvalid);
        			if(pincode.length != 6){ 
        				is_valid =1;  
        				$("#scbox_pincode").addClass('invalid');
                 scrolltop();
        			}else{
                is_valid =0; 
        				$("#scbox_pincode").removeClass('invalid');
        			}

              if(mobile.length != 10){ 
                is_valid =1;  
                $("#scbox_mobile").addClass('invalid');
                 scrolltop();
              }else{
                is_valid =0;  
                $("#scbox_mobile").removeClass('invalid');
              }

              var emailId = $("#scbox_emailid").val();
              var validemail = validateEmail(emailId); 

            if(dobvalid==true && validname==true && is_valid == 0 && validemail==true)
            {
      				ga('send', 'event', 'SCBOX','clicked','Step1-User Details');
      				msg("ga('send', 'event', 'SCBOX','clicked','Step1-User Details')");
      				ScboxFormSubmit(1);
            }


          }
    }else if(id==2)
    {
           if($("#body_shape_ans").val()=='')
          {
            $("#body_shape_ans-error").removeClass('hide');
          }else
          {
            $("#body_shape_ans-error").addClass('hide');
			      ga('send', 'event', 'SCBOX','clicked','Step2-Body Shape');
			      msg("ga('send', 'event', 'SCBOX','clicked','Step2-Body Shape')");
            ScboxFormSubmit(2);

          }


    }else if(id==3)
    {
          if($("#scbox_style").val()=='')
          {
            $("#scbox_style-error").removeClass('hide');
          }else
          {
            $("#scbox_style-error").addClass('hide');
      			ga('send', 'event', 'SCBOX','clicked','Step3-Style Preference');
      			msg("ga('send', 'event', 'SCBOX','clicked','Step3-Style Preference')");
            ScboxFormSubmit(3);

          }


    }else if(id==4)
    {
         
		  if($("#scbox_skintone").val()=='' && $("#scbox_colors").val()=='' && $("#scbox_sizetop").val()=='' && $("#scbox_sizebottom").val()=='' && $("#scbox_sizefoot").val()=='' && ($("#scbox_sizeband").val()=='' && $("#scbox_gender").val()==1) && ($("#scbox_sizecup").val()=='' && $("#scbox_gender").val()==1))
          {
            $("#scbox_skintone-error").removeClass('hide');
            $("#scbox_colors-error").removeClass('hide');
            //$("#scbox_prints-error").removeClass('hide');
            $("#scbox_sizetop-error").removeClass('hide');
            $("#scbox_sizebottom-error").removeClass('hide');
            $("#scbox_sizefoot-error").removeClass('hide');
            $("#scbox_budget-error").removeClass('hide');
            $("#scbox_sizeband-error").removeClass('hide');
            $("#scbox_sizecup-error").removeClass('hide');

          }else
          {
           
             if($("#scbox_skintone").val()=='')
            {
              $("#scbox_skintone-error").removeClass('hide');
              scrolltop();
            }else if($("#scbox_colors").val()=='')
            {
              $("#scbox_colors-error").addClass('hide');
              $("#scbox_colors-error").removeClass('hide');
              scrolltop();
            }/*else if($("#scbox_prints").val()=='')
            {
              $("#scbox_prints-error").removeClass('hide');
            }*/else if($("#scbox_sizetop").val()=='')
            {
              $("#scbox_skintone-error").addClass('hide');
              $("#scbox_sizetop-error").removeClass('hide');
              scrolltop();
            }else if($("#scbox_sizebottom").val()=='')
            {              
               $("#scbox_sizetop-error").addClass('hide');
              $("#scbox_sizebottom-error").removeClass('hide');
            }else if($("#scbox_sizefoot").val()=='')
            {
               $("#scbox_sizetop-error").addClass('hide');
              $("#scbox_sizefoot-error").removeClass('hide');
            }/*else if($("#scbox_budget").val()=='')
            {
              $("#scbox_budget-error").removeClass('hide');
            }*/else if($("#scbox_sizeband").val()=='' && $("#scbox_gender").val()==1)
            {
              $("#scbox_sizebottom-error").addClass('hide');
              $("#scbox_sizeband-error").removeClass('hide');
            }else if($("#scbox_sizecup").val()=='' && $("#scbox_gender").val()==1)
            {
              $("#scbox_sizeband-error").addClass('hide');
              $("#scbox_sizecup-error").removeClass('hide');
            }else
            {
                $("#scbox_skintone-error").addClass('hide');
                $("#scbox_colors-error").addClass('hide');
                $("#scbox_prints-error").addClass('hide');
                $("#scbox_sizetop-error").addClass('hide');
                $("#scbox_sizebottom-error").addClass('hide');
                $("#scbox_sizefoot-error").addClass('hide');
                $("#scbox_budget-error").addClass('hide');
                $("#scbox_sizeband-error").addClass('hide');
                $("#scbox_sizecup-error").addClass('hide');
                ga('send', 'event', 'SCBOX','clicked','Step4-Select Prints');
                msg("ga('send', 'event', 'SCBOX','clicked','Step4-Select Prints')");
                ScboxFormSubmit(4);
            }
			      

          }

    }else if(id==5)
    {
       ScboxFormSubmit(5);
    }else if(id==6)
    {
       ScboxFormSubmit(6);
    }

  }
}

function slideprev(id)
{
  if(id!='' && id<7 && id>0)
  {
    if(id!=6)
    {
      $('#slide_'+id).addClass('hide');
      var prev = id-1;
      $('#slide_'+prev).removeClass('hide');
      var newurl = getUrl.toString().split("=");  
      if(newurl[0]!=undefined)
      {              
        history.pushState(null, null, newurl[0]+'='+prev);
      }
    }else
    {
      $('#slide_'+id).addClass('hide');
      var prev = id-2;
      $('#slide_'+prev).removeClass('hide');
      var newurl = getUrl.toString().split("=");  
      if(newurl[0]!=undefined)
      {              
        history.pushState(null, null, newurl[0]+'='+prev);
      }
    }
  }
}

function ScboxFormSubmit(id)
{
    var actionurl = '';var isGift = '';
    var next = 0;
    var objectid = $('#scbox_objectid').val(); 
    

          
    if(id==1)
    {
		 
      //actionurl = sc_baseurl+'book-scbox/'+objectid+'?step=1';
     if($("#scbox_gender").val()!='' && $("#scbox_name").val()!='' && $("#scbox_age").val()!='' && $("#scbox_gender").val()!='' && $("#scbox_emailid").val()!='' && $("#scbox_mobile").val()!='' && $("#scbox_state").val()!='' && $("#scbox_city").val()!='' && $("#scbox_shipaddress").val()!='' && $("#scbox_pincode").val()!='' ){

          var scbox_name = $('#scbox_name').val().trim();
          scbox_name = scbox_name.replace(/[^a-z\s]/gi, ' ');
          scbox_name = scbox_name.replace(/\s+/g, " ");
          var res = scbox_name.split(" ");
          billing_first_name = res[0];   
          if(res[1]!=''){
            billing_last_name = res[1]; }          
          var billing_mobile_no = $('#scbox_mobile').val().trim();
          var billing_pincode_no = $('#scbox_pincode').val().trim();
          var billing_address = $('#scbox_shipaddress').val().trim();
          billing_address = billing_address.replace(/[^a-zA-Z0-9\\-\\s\\b\/\,\(\)]/gi, ' ');
          billing_address = billing_address.replace(/\s+/g, ' ');
          var billing_city = $('#scbox_city').val().trim();
          var billing_state = $('#scbox_state').val();
          var cart_total = $('#cart_total').val();
          var scbox_price = $('#scbox_price').val();
          var billing_statename = '';
          var scbox_emailid = $("#scbox_emailid").val().trim();
          var utm_source = $('#utm_source').val();
          var utm_medium = $('#utm_medium').val();
          var utm_campaign = $('#utm_campaign').val();
          document.cookie = "cart_email="+scbox_emailid+";path=/";
          document.cookie = "billing_first_name="+billing_first_name+";path=/";    
          document.cookie = "billing_last_name="+billing_last_name+";path=/";     
          document.cookie = "billing_mobile_no="+billing_mobile_no+";path=/";     
          document.cookie = "stylecracker_billing_pincode="+billing_pincode_no+";path=/";    
          document.cookie = "billing_address="+billing_address+";path=/";     
          document.cookie = "billing_city="+billing_city+";path=/";     
          document.cookie = "billing_state="+billing_state+";path=/";
          document.cookie = "billing_stateid="+billing_state+";path=/";
          document.cookie = "Scbox_price="+scbox_price+";path=/";
          document.cookie = "sc_source="+utm_source+";path=/";
          document.cookie = "sc_medium="+utm_medium+";path=/";
          document.cookie = "sc_campaign="+utm_campaign+";path=/";
          document.cookie = "scbox_objectid="+objectid+";path=/";

          if(document.getElementById("chkYes").checked){
              isGift = 'true';
              var gifted_email = $("#scbox_emailid").val();
              document.cookie = "scbox_isgift="+isGift+";path=/";
              document.cookie = "scbox_gifted_email="+gifted_email+";path=/"; 

              if(document.getElementById("chkYes").checked){
                document.cookie = "stylist_call=yes;path=/";
              }else{
                document.cookie = "stylist_call=no;path=/";
              }      
           }else{
               isGift = 'false';
               document.cookie = "scbox_isgift="+isGift+";path=/"; }

           $.ajax({
            url: sc_baseurl+'scbox/savescbox',
            type: 'post',
            data: {'scbox-confirm' : '1','slide':'1', 'scbox_name':$('#scbox_name').val(), 'scbox_emailid':$('#scbox_emailid').val(), 'scbox_mobile':$('#scbox_mobile').val(), 'scbox_gender':$('#scbox_gender').val(), 'scbox_age':$('#scbox_age').val(), 'scbox_objectid':$('#scbox_objectid').val(), 'scbox_shipaddress':$('#scbox_shipaddress').val(), 'scbox_pincode':$('#scbox_pincode').val(), 'scbox_city':$('#scbox_city').val(), 'scbox_state':$('#scbox_state').val(), 'scbox_likes':$('#scbox_likes').val(), 'scbox_dislikes':$('#scbox_dislikes').val(), 'scbox_city':$('#scbox_city').val(), 'scbox_city':$('#scbox_city').val(), 'scbox_userid':$('#scbox_userid').val(), 'scbox_price' : $('#scbox_price').val(),'scbox_package':$('#scbox_package').val(),  'scbox_productid':$('#scbox_productid').val(),'lastparam':lastparam,'utm_source':utm_source,'utm_medium':utm_medium,'utm_campaign':utm_campaign, 'scbox_billname':$('#scbox_billname').val(), 'scbox_billemailid':$('#scbox_billemailid').val(), 'scbox_billmobile':$('#scbox_billmobile').val(), 'scbox_billgender':$('#scbox_billgender').val(),'scbox_billage':$('#scbox_billage').val(),'scbox_billaddress':$('#scbox_billaddress').val(), 'scbox_billpincode':$('#scbox_billpincode').val(), 'scbox_billcity':$('#scbox_billcity').val(), 'scbox_billstate':$('#scbox_billstate').val(), 'scbox_giftmsg':$('#scbox_giftmsg').val(),'scbox_isgift':isGift  },
            success: function(data,status){
             msg(data);
              data = data.trim();
              if(data!=''){
                if(data=='success1'){
                   $('#slide_'+id).addClass('hide');
                   next = 4;
                   $('#slide_'+next).removeClass('hide');
                   $('#scbox_slide').val(next);
                   window.location.href=sc_baseurl+'book-scbox/'+objectid+'?step='+next;
                  /* location.reload();*/
                }else if(data=='success'){
                 /* location.reload();*/
				 //alert('stylist_call--'+getCookie('stylist_call')+'--scbox_isgift--'+getCookie('scbox_isgift'));
                 if(getCookie('stylist_call')=='yes' && getCookie('scbox_isgift')=='true')
                 {
                    window.location.href=sc_baseurl+'cart/'+objectid+'?type=scbox&slide=5';
                 }else
                 {
					 window.location.href=sc_baseurl+'book-scbox/'+objectid+'?step=2';                    
                 }
               
                 /* $('#slide_'+id).addClass('hide');
                      next = id+1;
                  $('#slide_'+next).removeClass('hide');
                  $('#scbox_slide').val(next);*/
                /*  var newurl = getUrl.toString().split("=");  
                if(newurl[0]!=undefined)
                {              
                  history.pushState(null, null, newurl[0]+'='+next);
                }*/
                  scrolltop();
                }else if(data=='utm'){
                  window.location.href=sc_baseurl+'cart/'+objectid+'?type=scbox&slide=5'; 
                }else if(data == 'error'){
                  //window.location.href=sc_baseurl+'book-scbox?slide=1';
                }else{
                  $('#scbx_error').html("Unexpected error. Please try again!");
                }
              }else{
              }
              //event.preventDefault();
            },
            error: function(xhr, desc, err) {
              // console.log(err);
            }
          });
        }

      /*$('#slide_'+id).addClass('hide');
          next = id+1;
      $('#slide_'+next).removeClass('hide');
      $('#scbox_slide').val(next); */

    }else if(id==2)
    {
       $.ajax({
          url: sc_baseurl+'scbox/savescbox',
          type: 'post',
          data: {'scbox-confirm' : '1','slide':'2','scbox_gender':$('#scbox_gender').val(), 'body_shape_ans':$('#body_shape_ans').val(), 'scbox_objectid':$('#scbox_objectid').val(), 'scbox_userid':$('#scbox_userid').val(),'scbox_isgift':isGift },
          success: function(data, status) {
             data = data.trim();
              msg(data);

            if(data!=''){
              if(data=='success'){
                $('#slide_'+id).addClass('hide');
                    next = id+1;
                $('#slide_'+next).removeClass('hide');
                $('#scbox_slide').val(next);
                scrolltop();
                
                var newurl = getUrl.toString().split("=");  
                if(newurl[0]!=undefined)
                {              
                  history.pushState(null, null, newurl[0]+'=3');
                }
               // window.location.href=sc_baseurl+'book-scbox?slide=2';
              }else if(data == 'error'){
                //window.location.href=sc_baseurl+'book-scbox?slide=1';
              }else{
                $('#scbx_error').html("Unexpected error. Please try again!");
              }
            }else{

            }
            //event.preventDefault();
          },
          error: function(xhr, desc, err) {
            // console.log(err);
          }
        });
     
    }else if(id==3)
    {
      if($('#scbox_style').val()!='')
      {
         $.ajax({
          url: sc_baseurl+'scbox/savescbox',
          type: 'post',
          data: {'scbox-confirm' : '1','slide':'3','scbox_gender':$('#scbox_gender').val(), 'scbox_style':$('#scbox_style').val(), 'scbox_objectid':$('#scbox_objectid').val(), 'scbox_userid':$('#scbox_userid').val(),'body_shape_ans':$('#body_shape_ans').val(),'scbox_isgift':isGift },
          success: function(data, status) {
             data = data.trim();
            msg(data);
            if(data!=''){
              if(data=='success'){
                    $('#slide_'+id).addClass('hide');
                        next = id+1;
                    $('#slide_'+next).removeClass('hide');
                    $('#scbox_slide').val(next);
                    scrolltop();                  
                    var newurl = getUrl.toString().split("=");                 
                    if(newurl[0]!=undefined)
                    {
                      history.pushState(null, null, newurl[0]+'=4');
                    }
                    
                   // window.location.href=sc_baseurl+'book-scbox?slide=2';
                  }else if(data == 'error'){
                    //window.location.href=sc_baseurl+'book-scbox?slide=1';
                  }else{
                    $('#scbx_error').html("Unexpected error. Please try again!");
                  }
                }else{

                }
                //event.preventDefault();
              },
              error: function(xhr, desc, err) {
                // console.log(err);
              }
          });
       }else
       {
         $('#scbox_style-error').removeClass('hide');
       }
    
    }else if(id==4)
    {
        $.ajax({
          url: sc_baseurl+'scbox/savescbox',
          type: 'post',
          data: {'scbox-confirm' : '1','slide':'4','scbox_gender':$('#scbox_gender').val(), 'scbox_skintone':$('#scbox_skintone').val(), 'scbox_colors':$('#scbox_colors').val(), 'scbox_prints':$('#scbox_prints').val(), 'scbox_sizetop':$('#scbox_sizetop').val(), 'scbox_sizebottom':$('#scbox_sizebottom').val(), 'scbox_sizefoot':$('#scbox_sizefoot').val(), 'scbox_sizeband':$('#scbox_sizeband').val(), 'scbox_sizecup':$('#scbox_sizecup').val(), 'scbox_objectid':$('#scbox_objectid').val(), 'scbox_userid':$('#scbox_userid').val(), 
          'scbox_price' : $('#scbox_price').val(),'scbox_isgift':isGift },
          success: function(data, status) {
             data = data.trim();
            msg(data);
            if(data!=''){
              if(data=='success'){
                $('#slide_'+id).addClass('hide');
                    next = id+2;
                //$('#slide_'+next).removeClass('hide');
                //$('#scbox_slide').val(next);
                next = 7;
                $('#slide_'+next).removeClass('hide');
                $('#scbox_slide').val(next);
                  scrolltop(); 
                 
                  if(getCookie('scfr')=='yb')  
                  {
                    $("#cod").addClass("state-disabled");
                  }            
                  var newurl = getUrl.toString().split("=");                 
                  // if(newurl[0]!=undefined)
                  // {
                  //   history.pushState(null, null, newurl[0]+'=5');
                  // }

                  window.location.href=sc_baseurl+'cart/'+objectid+'?type=scbox&slide=5';
               // window.location.href=sc_baseurl+'book-scbox?slide=2';
              }else if(data == 'error'){
                //window.location.href=sc_baseurl+'book-scbox?slide=1';
              }else{
                $('#scbx_error').html("Unexpected error. Please try again!");
              }
            }else{

            }
            //event.preventDefault();
          },
          error: function(xhr, desc, err) {
            // console.log(err);
          }
        });
     
    }else if(id==5)
    {
      $.ajax({
        url: sc_baseurl+'scbox/savescbox',
        type: 'post',
        data: {'scbox-confirm' : '1','slide':'5','scbox_gender':$('#scbox_gender').val(), 'scbox_package':$('#scbox_package').val(), 'scbox_price':$('#scbox_price').val(), 'scbox_productid':$('#scbox_productid').val(), 'scbox_objectid':$('#scbox_objectid').val(), 'scbox_userid':$('#scbox_userid').val(),'scbox_isgift':isGift },
        success: function(data, status) {
           data = data.trim();
            msg(data);
            if(data!=''){
              if(data=='success'){
                $('#slide_'+id).addClass('hide');
                    next = id+1;
                $('#slide_'+next).removeClass('hide');
                $('#scbox_slide').val(next);
                scrolltop();
               // window.location.href=sc_baseurl+'book-scbox?slide=2';
              }else if(data == 'error'){
                //window.location.href=sc_baseurl+'book-scbox?slide=1';
              }else{
                $('#scbx_error').html("Unexpected error. Please try again!");
              }
            }else{

            }
          //event.preventDefault();
        },
        error: function(xhr, desc, err) {
          // console.log(err);
        }
      });    

    }else if(id==6)
    {
      //alert($('textarea#brand_pref').val()); 
      ga('send', 'event', 'SCBOX','clicked','Step5-Optional Info');
      $.ajax({
        url: sc_baseurl+'scbox/savescbox',
        type: 'post',
        data: {'scbox-confirm' : '1','slide':'6','scbox_profession':$('#scbox_profession').val(), 'scbox_occassion':$('#scbox_occassion').val(), 'scbox_wardrobe':$('textarea#scbox_wardrobe').val(), 'experiment':$('#experiment').val(), 'brand_pref':$('textarea#brand_pref').val(), 'scbox_userid':$('#scbox_userid').val(), 'scbox_objectid':$('#scbox_objectid').val(),'scbox_isgift':isGift },
        success: function(data, status) {
           data = data.trim();
            msg(data);
            if(data!=''){
              if(data=='success'){
               /* $('#slide_'+id).addClass('hide');
                    next = id+1;
                $('#slide_'+next).removeClass('hide');
                $('#scbox_slide').val(next);*/
                var final_cookies = '';
                document.cookie="sc_source="+final_cookies+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
                document.cookie="sc_medium="+final_cookies+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
                document.cookie="sc_campaign="+final_cookies+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
                scrolltop();
                window.location.href=sc_baseurl+'sc-box?success=1';
              }else if(data == 'error'){
                //window.location.href=sc_baseurl+'book-scbox?slide=1';
              }else{
                $('#scbx_error').html("Unexpected error. Please try again!");
              }
            }else{

            }
          //event.preventDefault();
        },
        error: function(xhr, desc, err) {
          // console.log(err);
        }
      });    
    }
  
}

function confirmbox()
{
  
  if(clickcnt==0)
  { clickcnt = clickcnt+1;
  	ga('send', 'event', 'SCBOX','clicked','Step5-Optional Info');
  	msg("ga('send', 'event', 'SCBOX','clicked','Step5-Optional Info')");
    $("#page-loader").show();
    var objectid = $('#scbox_objectid').val();
        actionurl = sc_baseurl+'book-scbox/'+objectid+'?step=1';
       $('#scbox-form').attr('action', actionurl);
       var scbox_style = document.forms.scbox-form;
       scbox-form.submit();
        scbox_register();
      $('#scbox-confirm').prop("disabled",true);
  }else
  {
    $('#scbox-confirm').prop("disabled",true);
  }
}

function getcustomprice()
{
  if($('#customboxprice').val()=='')
  {
    $('#customboxprice-error').removeClass('hide');

  }else
  {
    $('#customboxprice-error').addClass('hide');
  }
   scboxprice = $('#customboxprice').val();
  // alert($('#customboxprice').val());
     $('#scbox-price').val(scboxprice);
}


function validateform(e){

  msg(e);

  var regexp = /[^a-zA-Z]/g;
  var regexp_mobile = /[^0-9]/g;
  var regex_dob = /^\d{1,2}\-\d{1,2}\-\d{4}$/;
  var isTrue = 0;

  elementid = $(e).attr('id');
  if($('#'+elementid).val()!='')
  {  
      if(elementid=='scbox_name'){
        var namestr = $('#scbox_name').val(); 
        namestr = namestr.trim();   
        namestr = namestr.replace(/\s+/g, " ");   
        var namearr = namestr.split(" ");     
        if(namearr[1]==undefined || namearr[1]=='' ){
          $('#scbox_name').addClass('invalid');
        }else{
           $('#scbox_name').removeClass('invalid'); }         
      }  

    if(elementid=='scbox_pincode')
    { 
      if(e.value.length != '' && e.value.length === 6){
        var pincode = $('#'+elementid).val();
        setpincodedata_scbox(pincode,'ship');
		    $('#scbox_city').focus();
        $('#scbox_pincode').removeClass('invalid');
        $('#scbox_city').addClass('not-empty'); 
      }else{
        $('#scbox_pincode').addClass('invalid');
        $('#scbox_pincode1').addClass('invalid'); }
    }else{           
       $('#'+elementid).removeClass('invalid'); }

    if(elementid=='scbox_billpincode'){ 
      if(e.value.length != '' && e.value.length === 6){
         var pincode = $('#'+elementid).val();
         setpincodedata_scbox(pincode,'bill');
        $('#scbox_billcity').focus();
        $('#scbox_billpincode').removeClass('invalid');
        $('#scbox_billcity').addClass('not-empty'); 
      }else{
        $('#scbox_billpincode').addClass('invalid'); }
    }else{           
       $('#'+elementid).removeClass('invalid'); }

    if(elementid=='scbox_mobile')
    {
      if(e.value.length != '' && e.value.length === 10)
      {
        $('#scbox_mobile').removeClass('invalid');
      }else
      {
        $('#scbox_mobile').addClass('invalid');
      }

    }


    if(elementid=='scbox_emailid')
    {
      var emailId = $('#scbox_emailid').val();
      var validemail = validateEmail(emailId);
      if(validemail==true)
      {
       $("#scbox_emailid").removeClass('invalid');
        checkUserEmail(emailId);
      }else
      {
         $("#scbox_emailid").addClass('invalid');
      }      
    }
   
    
  }else
  {
      if(elementid!='scbox_age' || elementid!='scbox_likes' || elementid!='scbox_dislikes')
    {
      //$('#'+elementid+'-error').removeClass('hide');        
        $('#'+elementid).addClass('invalid');      
    }
    if(elementid=='scbox_likes' || elementid=='scbox_dislikes')
    {
      $('#'+elementid).removeClass('empty');
    }
  }

  if(elementid=='scbox_age' || elementid=='scbox_billage')
  {
    validatedob('ship');
  }
  if(elementid=='scbox_billage')
  {
    validatedob('bill');
  }   
}

function validatedob(type)
{
  var regex_dob = /^\d{1,2}\-\d{1,2}\-\d{4}$/;
  var dob = '';
  if(type=='ship')
  {
    dob = $('#scbox_age').val();
    if(dob.match(regex_dob) && dob!=''){       
        $('#scbox_age').removeClass('invalid');       
        return true;
      }else{
        $('#scbox_age').addClass('invalid');
        return false;
      }
  }else if(type=='bill')
  {
      var dob = $('#scbox_billage').val();
      if(dob.match(regex_dob) && dob!=''){
          $('#scbox_billage').removeClass('invalid');
          return true;
        }else{
          $('#scbox_billage').addClass('invalid');         
          return false;
        }
  }    
}

function setpincodedata_scbox(pincode,type)
{ 
  $.ajax({
        url: sc_baseurl+'scbox/getpincodedata',
        type: 'post',
        data: {'scbox_pincode' : pincode },
        success: function(data, status) {
          var obj = jQuery.parseJSON(data);
          //msg(obj);             
          if(obj.id!='' && obj.city!=''){
              if(type=='ship'){
                $('#scbox_state').val(obj.id);
                $('#scbox_city').val(obj.city);
                $('#scbox_state').removeClass('invalid');
                $('#scbox_city').removeClass('invalid');
              }else if(type='bill'){
                $('#scbox_billstate').val(obj.id);
                $('#scbox_billcity').val(obj.city);
                $('#scbox_billstate').removeClass('invalid');
                $('#scbox_billcity').removeClass('invalid'); }
          }else if(data===0){    
            if(type=='ship'){      
              $('#scbox_pincode-error').html('Please enter valid pincode');
              $('#scbox_pincode').addClass('invalid');
              $('#scbox_state').val('');
              $('#scbox_city').val('');
            }else if(type=='bill'){
              $('#scbox_billpincode-error').html('Please enter valid pincode');
              $('#scbox_billpincode').addClass('invalid');
              $('#scbox_billstate').val('');
              $('#scbox_billcity').val(''); } }  
        },
        error: function(xhr, desc, err) {
          // console.log(err);
        }
      });
}


function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function validateEmail(sEmail) {
  var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
  if (filter.test(sEmail)) {   
  return true;
  }else {   
    return false;
  }
}

/*
//## function to check the section --------------
function InitSectionCheck(type){
	var selectedItem = 0
	if(type == 'body_shape'){
		$("#slide4").find(".row >div").each(function( index ) {
		  if($(this).hasClass("active")){
			  selectedItem = $(this).attr("data-attr");
		  }
		});
	} else if(type == "body_style"){
		$("#slide5").find(".box-style-selected > li").each(function( index ) {
		  if($(this).hasClass("filled")){
			  selectedItem++;
		  }
		});
	}
	return selectedItem;
}
*/
 $(".scbox-payment-options li label").click(function(e){
    if($(this).hasClass("active")){
      payment_mode = $(this).attr("attr-value");
      $('#sc_cart_pay_mode').val(payment_mode);
       $('#cart_place_order_msg').addClass('hide');
    }
  });

function scboxchange_qty(type,id){
   //var scbox_quantity = 1;  

   var scbox_prdprice =  $('#scbox_price').val();
   var scbox_subtotal =  $('#scbox_price').val();
   var checkout_taxes = 0;

  if(type == 'min'){
    var proQty = $('#quantity'+id).val();
    if(proQty > 1){
      $('#quantity'+id).val(proQty-1);
    }
  }else{
    var proQty = $('#quantity'+id).val();
    if(proQty < 5){
      $('#quantity'+id).val(parseInt(proQty)+1);      
    }
  }

  scbox_quantity = parseFloat($('#quantity'+id).val());
  scbox_subtotal = parseFloat(scbox_quantity)*parseFloat(scbox_prdprice);
  checkout_taxes = (parseFloat(scbox_taxes)*parseInt(scbox_subtotal))/100; 
  scbox_total =  scbox_subtotal + checkout_taxes;

  
  $('#checkout_price').html('<i class="fa fa-inr"></i> '+parseFloat(scbox_subtotal).toFixed(2));
  $('#checkout_subtotal').html('<i class="fa fa-inr"></i> '+parseFloat(scbox_subtotal).toFixed(2));
  checkout_taxes = Math.round(checkout_taxes * 100) / 100;
  $('#checkout_taxes').html('<i class="fa fa-inr"></i> '+checkout_taxes);
  scbox_total = Math.round(scbox_total * 100) / 100;
  $('#checkout_total').html('<i class="fa fa-inr"></i> '+scbox_total);
  $('#cart_total').val(scbox_total);

  $('#scbox_price').val(boxprice);    
  $('#checkout_price').html('<i class="fa fa-inr"></i> '+boxprice);
 
 // allow_cart_update();
}


function place_scbox_order(){
  show_cart_loader();
  var billing_first_name = '';
  var billing_last_name = '';
  //var pay_mod = $("input[type='radio'][name='sc_cart_pay_mode']:checked").val();
  //var payment_mode;
  $(".scbox-payment-options li label").each(function(e){
    if($(this).hasClass("active")){
      payment_mode = $(this).attr("attr-value");
      $('#sc_cart_pay_mode').val(payment_mode);
       $('#cart_place_order_msg').addClass('hide');
    }
  });

  
  var pay_mod = payment_mode;
  var codna = $('#codna').val();
  var shina = $('#shina').val();
  var stock_exist = $('#stock_exist').val();  

  var regexp = /[^a-zA-Z\.]/g;
  var regexp_mobile = /[^0-9]/g;

  var scbox_name = $('#scbox_name').val().trim();
    scbox_name = scbox_name.replace(/[^a-z\s]/gi, ' ');
  scbox_name = scbox_name.replace(/\s+/g, " ");
  var res = scbox_name.split(" ");

  billing_first_name = res[0];   
  if(res[1]!='')
  {
    billing_last_name = res[1];
  }
  //var billing_first_name = $('#billing_first_name').val().trim();
 // var billing_last_name = $('#billing_last_name').val().trim();
  var billing_mobile_no = $('#scbox_mobile').val().trim();
  var billing_pincode_no = $('#scbox_pincode').val().trim();
  var billing_address = $('#scbox_shipaddress').val().trim();
  billing_address = billing_address.replace(/[^a-zA-Z0-9\\-\\s\\b\/\\,\(\)]/gi, ' ');
  billing_address = billing_address.replace(/\s+/g, " ");

  var billing_city = $('#scbox_city').val().trim();
  var billing_state = $('#scbox_state').val();
  var cart_total = $('#cart_total').val();
  var scbox_price = $('#scbox_price').val();

  var isTrue = 0;
  
  if(billing_first_name.match(regexp) || billing_first_name==''){
    var error = 'Enter Valid First Name';
    $('#cart_place_order_msg').html('Enter Valid First Name');
    isTrue = 1;
  }else{
    document.cookie = "billing_first_name="+billing_first_name+";path=/";
    $('#cart_place_order_msg').html('');
    //isTrue = 0;
  }

  if(billing_last_name.match(regexp) || billing_last_name==''){
    var error = 'Enter Valid Last Name';
    $('#cart_place_order_msg').html('Enter Valid Last Name');
    isTrue = 1;
  }else{
    document.cookie = "billing_last_name="+billing_last_name+";path=/";
    $('#cart_place_order_msg').html('');
    //isTrue = 0;
  }

  if(billing_mobile_no.match(regexp_mobile) || billing_mobile_no=='' || billing_mobile_no.length < 10){
    var error = 'Enter Valid Mobile No';
    $('#cart_place_order_msg').html('Enter Valid Mobile No');
    isTrue = 1;
  }else{
    document.cookie = "mobile_no="+billing_mobile_no+";path=/";
    $('#cart_place_order_msg').html('');
    //isTrue = 0;
  }

  if(billing_pincode_no.match(regexp_mobile) || billing_pincode_no=='' || billing_pincode_no.length < 6){
    var error = 'Enter Valid Pincode';
    $('#cart_place_order_msg').html('Enter Valid Pincode');
    isTrue = 1;
  }else{
    document.cookie = "stylecracker_shipping_pincode="+billing_pincode_no+";path=/";
    $('#cart_place_order_msg').html('');
    //isTrue = 0;
  }

  if(billing_address==''){
    var error = 'Enter Shipping Address';
    $('#cart_place_order_msg').html('Enter Shipping Address');
    isTrue = 1;
  }else{

    document.cookie = "billing_address="+billing_address+";path=/";
    $('#cart_place_order_msg').html('');
    //isTrue = 0;
  }

  if(billing_city==''){
    var error = 'Enter valid pincode';
    $('#cart_place_order_msg').html('Enter Shipping City');
    isTrue = 1;
  }else{
    document.cookie = "billing_city="+billing_city+";path=/";
    $('#cart_place_order_msg').html('');
    //isTrue = 0;
  }

  if(billing_state==''){
    var error = 'Enter valid pincode';
    $('#cart_place_order_msg').html('Enter Shipping State');
    isTrue = 1;
  }else{
    document.cookie = "billing_state="+billing_state+";path=/";
    $('#cart_place_order_msg').html('');
    //isTrue = 0;
  }

  if(isTrue == 1){ 
    hide_cart_loader();
    $('#cart_place_order_msg').html(error).removeClass('hide');
    $('#cart_place_order_msg').addClass('message error');
    $("#place_order_sc_cart").attr('disabled',true);  
  }
  else if(stock_exist > 0){ 
    hide_cart_loader();
    $('#cart_place_order_msg').html('Some of the products are out of stock').removeClass('hide');
    $("#place_order_sc_cart").attr('disabled',true);  
  }
  else if(pay_mod !='cod' && pay_mod != 'card'){
    hide_cart_loader();
    $('#cart_place_order_msg').html('Select payment option').removeClass('hide');
    $("#place_order_sc_cart").attr('disabled',true);  
  }else if(pay_mod =='cod' && codna>0){ 
    hide_cart_loader();
    /*$("input[type='radio'][id='sc_cart_pay_mode_cod']").prop('checked',false);
    $("input[type='radio'][id='sc_cart_pay_mode_online']").prop('checked',true);*/
    
    if(getCookie('discountcoupon_stylecracker') == 'NOCASH500' || getCookie('discountcoupon_stylecracker') == 'NOCASH1000'){ 
      
      $("#place_order_sc_cart").attr('disabled',true);      
      
      $('#cart_place_order_msg').html('COD is not avaliable for applied coupon code').removeClass('hide');
      
      }else{
      $('#cart_place_order_msg').html('COD is not available for some products').removeClass('hide');
    }
  }else if(shina > 0){
    hide_cart_loader();
    $('#cart_place_order_msg').html('Shipping is not available for some products').removeClass('hide');
    $("#place_order_sc_cart").attr('disabled',true);  
  }else{   

    var shipping_first_name = billing_first_name;
    var shipping_last_name = billing_last_name;
    var shipping_mobile_no = billing_mobile_no;
    var shipping_pincode_no = billing_pincode_no;
    var shipping_address = billing_address;
    var shipping_city = billing_city;
    var shipping_state = billing_state;
    var cart_email = $('#scbox_emailid').val().trim(); 
    var cart_total = $('#cart_total').val();
    var scbox_objectid = $('#scbox_objectid').val();
    var scbox_objectid = lastparam.split('?')[0];
   

    if(is_order_placed==false &&billing_first_name!='' && billing_last_name!='' && billing_mobile_no!='' && billing_pincode_no!='' && billing_address!='' && billing_city!='' && billing_state!='' && shipping_first_name!='' && shipping_last_name!='' && shipping_mobile_no!='' && shipping_pincode_no!='' && shipping_address!='' && shipping_city!='' && shipping_state!='' && cart_email!='' && scbox_price!=''){

      msg("before: is_order_placed: "+is_order_placed);
      is_order_placed = true;
      if(pay_mod == 'cod'){

        $.ajax({
                type: "post",
                url: sc_baseurl+"scbox/place_order",
                data: { billing_first_name:billing_first_name,billing_last_name:billing_last_name,billing_mobile_no:billing_mobile_no,billing_pincode_no:billing_pincode_no,billing_address:billing_address,billing_city:billing_city,billing_state:billing_state,shipping_first_name:shipping_first_name,shipping_last_name:shipping_last_name,shipping_mobile_no:shipping_mobile_no,shipping_pincode_no:shipping_pincode_no,shipping_address:shipping_address,shipping_city:shipping_city,shipping_state:shipping_state,cart_email:cart_email,pay_mod:pay_mod,cart_total:cart_total,scbox_price:scbox_price,scbox_objectid:scbox_objectid,scbox_quantity:scbox_quantity},
                cache :true,
                async: true,
                success: function (response) {
                    hide_cart_loader(); remove_address_cookies('SHIP'); remove_address_cookies('BILL');
                    if(response!='')
                    {
                      var redirction = sc_baseurl+'book-scbox/'+scbox_objectid+'?order='+response+'&step=6';
                      document.cookie="Scbox_price="+''+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
                     /* $('#order-message').html('Your SCBOX amount of Rs.'+cart_total+' has been booked. Please fill the below details to know more about you. (COD)');                      
                      $('#order-message').removeClass('hide');*/
                    }else
                    {
                      var redirction = sc_baseurl+'book-scbox/'+scbox_objectid+'?step=1';
                      /*$('#order-message').html('Error');
                      $('#order-message').removeClass('hide');*/
                    }
                   // var redirction = sc_baseurl+'scbox/ordersuccessOrder/'+response;
                    
                    var final_cookies = '';
                    document.cookie="SCUniqueID="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
                    document.cookie="discountcoupon_stylecracker="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
                window.location =redirction;
                money_formatter(".price");
                money_formatter(".cart-price");
                },
                error: function (response) {
                  hide_cart_loader();
                    //alert(response);
                    msg(response);
                }
            });

      }else{
          
        $.ajax({
                type: "post",
                url: sc_baseurl+"scbox/place_online_order",
                data: { billing_first_name:billing_first_name,billing_last_name:billing_last_name,billing_mobile_no:billing_mobile_no,billing_pincode_no:billing_pincode_no,billing_address:billing_address,billing_city:billing_city,billing_state:billing_state,shipping_first_name:shipping_first_name,shipping_last_name:shipping_last_name,shipping_mobile_no:shipping_mobile_no,shipping_pincode_no:shipping_pincode_no,shipping_address:shipping_address,shipping_city:shipping_city,shipping_state:shipping_state,cart_email:cart_email,pay_mod:pay_mod,cart_total:cart_total,scbox_price:scbox_price,scbox_objectid:scbox_objectid,scbox_quantity:scbox_quantity  },
                cache :true,
                async: true,
                success: function (response) {
                    hide_cart_loader();
                    var obj = jQuery.parseJSON(response);
                   
                $('#txnid').val(obj.txnid);
                $('#hash').val(obj.payu_hash);
                $('#amount').val(obj.amount);
                $('#firstname').val(obj.firstname);
                $('#email').val(obj.email);
                $('#phone').val(obj.phoneno);
                $('#udf1').val(getCookie('SCUniqueID'));
                $('#udf2').val(obj.address_sc);
                $('#udf3').val(billing_pincode_no);
                $('#udf4').val(billing_city);
                $('#udf5').val(obj.state);
                var stylecracker_style = document.forms.stylecracker_style;
                stylecracker_style.submit();
                document.cookie="Scbox_price="+''+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
                money_formatter(".price");
                money_formatter(".cart-price");


                },
                error: function (response) {
                  hide_cart_loader();                  
                    msg(response);
                }
            });
      }
    }
  }
  hide_cart_loader();
  
}

function checkUserEmail(useremail)
{
    if(useremail!='')
    {
     $.ajax({
                type: "post",
                url: sc_baseurl+"scbox/checkuserdata",
                data: { useremail:useremail },
                cache :true,
                async: true,
                success: function (response) {
                     response = response.trim();
                    if(response==1)
                    {                      
                      $('#scboxgender-female').trigger("click");                 
                    }else if(response==2)
                    {
                     
                       $('#scboxgender-male').trigger("click");                     
                    }               
                },
                error: function (response) {
                  hide_cart_loader();                  
                    msg(response);
                }
            });
    }
}


$('#scbox_shipaddress').keypress(function (e) {      
      var regex = new RegExp("^[a-zA-Z0-9\\-\\s\\b\/\\,\(\)]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
          return true;
      }

      e.preventDefault();
      return false;
  });

  $('#scbox_name').keypress(function (e) {      
      var regex = new RegExp("^[a-zA-Z\\s\\b]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
          return true;
      }

      e.preventDefault();
      return false;
  });

  function validateName(scboxname)
  {
    var regex = /[^a-zA-Z\s]/g;
    if(scboxname.match(regex))
    {      
      return true;
    }else
    {     
      return false;
    }
  }


  function getScboxSizeChart(sizetype)
  {
    var scbox_gender = $('#scbox_gender').val();
    if(scbox_gender!='' && sizetype!='')
    {
        $.ajax({
                type: "post",
                url: sc_baseurl+"scbox/getgenderSizeGuide",
                data: { 'scbox_gender':scbox_gender,'size_type':sizetype},
                cache :true,
                async: true,
                success: function (response) {
                    
                    if(response!="")
                    { 
                      $('#size_guide_img').attr("src",response);                 
                    }else 
                    {

                    }               
                },
                error: function (response) {
                  hide_cart_loader();                  
                    msg(response);
                }
            });
    }
    
  }


function slidetocart(p)
{
    var is_valid = 0;
    if($("#scbox_name1").val()=='' || $("#scbox_emailid1").val()=='' || $("#scbox_mobile1").val()=='' || $("#scbox_state1").val()=='' || $("#scbox_city1").val()=='' || $("#scbox_shipaddress1").val()=='' || $("#scbox_pincode1").val()=='' )
          {
              if($("#scbox_name1").val()==''){
                $("#scbox_name1").addClass('invalid');}  
              if($("#scbox_emailid1").val()==''){
                $("#scbox_emailid1").addClass('invalid');} 
              if($("#scbox_mobile1").val()==''){
                $("#scbox_mobile1").addClass('invalid');} 
              if($("#scbox_state1").val()==''){
                $("#scbox_state1").addClass('invalid');} 
              if($("#scbox_city1").val()==''){
                $("#scbox_city1").addClass('invalid');} 
              if($("#scbox_shipaddress1").val()==''){
                $("#scbox_shipaddress1").addClass('invalid');} 
              if($("#scbox_pincode1").val()==''){
                $("#scbox_pincode1").addClass('invalid');is_valid =1;}
              scrolltop();
          }else
          {  
            $("#scbox_name1").addClass('invalid');   
            $("#scbox_emailid1").removeClass('invalid');
            $("#scbox_mobile1").removeClass('invalid');
            $("#scbox_state1").removeClass('invalid');
            $("#scbox_city1").removeClass('invalid');
            $("#scbox_shipaddress1").removeClass('invalid');
            $("#scbox_pincode1").removeClass('invalid');
         
              var mobile = $("#scbox_mobile1").val();
              var pincode = $("#scbox_pincode1").val();             
              var validname = true;
              var namestr = $('#scbox_name1').val(); 
               namestr = namestr.trim();
               namestr = namestr.replace(/[^a-z\s]/gi, ' ');
              namestr = namestr.replace(/\s+/g, " ");
              $('#scbox_name1').val(namestr);
              var namearr = namestr.split(" ");     
              if(namearr[1]==undefined || namearr[1]==''){
                $('#scbox_name1').addClass('invalid');scrolltop();
                validname = false;
              }else{
                $('#scbox_name1').removeClass('invalid');
                validname = true;
               }
               
              if(pincode.length != 6){ 
                is_valid =1;  
                $("#scbox_pincode1").addClass('invalid');
                 scrolltop();
              }else{
                is_valid =0; 
                $("#scbox_pincode1").removeClass('invalid');
              }

              if(mobile.length != 10){ 
                is_valid =1;  
                $("#scbox_mobile1").addClass('invalid');
                 scrolltop();
              }else{
                is_valid =0;  
                $("#scbox_mobile1").removeClass('invalid');
              }
              var emailId = $("#scbox_emailid1").val();
              var validemail = validateEmail(emailId); 

            if(validname==true && is_valid == 0 && validemail==true)
            {
              ga('send', 'event', 'SCBOX','clicked','Step1a-User Details');
              msg("ga('send', 'event', 'SCBOX','clicked','Step1a-User Details')");
              ScboxCampaignOrder('1a');
            }
        }
}

function stylistCall(e)
{  
  //alert($('#stylist_call').is(":checked"));
    if($('#stylist_call').is(":checked")){
      document.cookie = "stylist_call=yes;path=/";
    }else{
      document.cookie = "stylist_call=no;path=/"; }
}

/*function ScboxCampaignOrder(id)
{ 
  if(id=='1a')
  {
      //actionurl = sc_baseurl+'book-scbox/'+objectid+'?step=1';
     if($("#scbox_name1").val()!='' && $("#scbox_emailid1").val()!='' && $("#scbox_mobile1").val()!='' && $("#scbox_state1").val()!='' && $("#scbox_city1").val()!='' && $("#scbox_shipaddress1").val()!='' && $("#scbox_pincode1").val()!='' )
        {

          var scbox_name = $('#scbox_name1').val().trim();
            scbox_name = scbox_name.replace(/[^a-z\s]/gi, ' ');
          scbox_name = scbox_name.replace(/\s+/g, " ");
          var res = scbox_name.split(" ");

          billing_first_name = res[0];   
          if(res[1]!='')
          {
            billing_last_name = res[1];
          }
          
          var billing_mobile_no = $('#scbox_mobile1').val().trim();
          var billing_pincode_no = $('#scbox_pincode1').val().trim();
          var billing_address = $('#scbox_shipaddress1').val().trim();
          billing_address = billing_address.replace(/[^a-zA-Z0-9\\-\\s\\b\/\,\(\)]/gi, ' ');
          billing_address = billing_address.replace(/\s+/g, ' ');

          var billing_city = $('#scbox_city1').val().trim();
          var billing_state = $('#scbox_state1').val();
          var cart_total = $('#cart_total').val();
          var scbox_price = $('#scbox_price').val();
          var billing_statename = '';
          var scbox_emailid = $("#scbox_emailid1").val().trim();
          var scbox_message = $("#scbox_message").val().trim();
          var objectid = $('#scbox_objectid').val().trim();
          var scbox_gender = 1;

          var utm_source = $('#utm_source').val();
          var utm_medium = $('#utm_medium').val();
          var utm_campaign = $('#utm_campaign').val();

          document.cookie = "cart_email="+scbox_emailid+";path=/";
          document.cookie = "billing_first_name="+billing_first_name+";path=/";    
          document.cookie = "billing_last_name="+billing_last_name+";path=/";     
          document.cookie = "billing_mobile_no="+billing_mobile_no+";path=/";     
          document.cookie = "stylecracker_billing_pincode="+billing_pincode_no+";path=/";    
          document.cookie = "billing_address="+billing_address+";path=/";     
          document.cookie = "billing_city="+billing_city+";path=/";     
          document.cookie = "billing_state="+billing_state+";path=/";
          document.cookie = "billing_stateid="+billing_state+";path=/";
          document.cookie = "Scbox_price="+scbox_price+";path=/";
          document.cookie = "sc_source="+utm_source+";path=/";
          document.cookie = "sc_medium="+utm_medium+";path=/";
          document.cookie = "sc_campaign="+utm_campaign+";path=/";
          document.cookie = "scbox_objectid="+objectid+";path=/";

           $.ajax({
            url: sc_baseurl+'scbox/savescbox',
            type: 'post',
            data: {'scbox-confirm' : '1','slide':'1', 'scbox_name':$('#scbox_name1').val(), 'scbox_emailid':$('#scbox_emailid1').val(), 'scbox_mobile':$('#scbox_mobile1').val(), 'scbox_gender':scbox_gender, 'scbox_age':$('#scbox_age').val(), 'scbox_objectid':$('#scbox_objectid').val(), 'scbox_shipaddress':$('#scbox_shipaddress1').val(), 'scbox_pincode':$('#scbox_pincode1').val(), 'scbox_city':$('#scbox_city1').val(), 'scbox_state':$('#scbox_state1').val(), 'scbox_message':scbox_message, 'scbox_userid':$('#scbox_userid').val(), 'scbox_price' : $('#scbox_price').val(), 
            'scbox_package':$('#scbox_package').val(),  'scbox_productid':$('#scbox_productid').val(),'lastparam':lastparam,'utm_source':utm_source,'utm_medium':utm_medium,'utm_campaign':utm_campaign   },
            success: function(data, status) {
              msg(data);
              data = data.trim();
              if(data!=''){

                if(data=='success1')
                {
                    window.location.href=sc_baseurl+'cart/'+objectid+'?type=scbox&slide=5';  
                }else if(data=='success'){
                  window.location.href=sc_baseurl+'cart/'+objectid+'?type=scbox&slide=5';  
                  scrolltop();
                }else if(data=='utm'){ 
                  window.location.href=sc_baseurl+'cart/'+objectid+'?type=scbox&slide=5';                
                  scrolltop();
                }else if(data == 'error'){
                  //window.location.href=sc_baseurl+'book-scbox?slide=1';
                }else{
                  $('#scbx_error').html("Unexpected error. Please try again!");
                }
              }else{
              }
              //event.preventDefault();
            },
            error: function(xhr, desc, err) {msg(err);}
          });
        }

    }
}*/