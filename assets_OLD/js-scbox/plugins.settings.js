//call zoomin
//  zoominDestroy();
//  zoominState();
// /. call zoomin

//sm only slider
$('.device-sm .grid-items-slider.sm-slider').addClass('swiper-container');
$('.device-sm .grid-items-slider.sm-slider .grid-item').addClass('swiper-slide');
$('.device-sm .grid-items-slider.sm-slider .grid-row').addClass('swiper-wrapper');
$('.device-sm .grid-items-slider.sm-slider .grid-col').addClass('swiper-slide');

smSwiper = new Swiper('.swiper-container.sm-slider', {
         paginationClickable: true,
         spaceBetween: 0,
         cssWidthAndHeight: true,
         slidesPerView:'auto',

         pagination: '.swiper-pagination',
         nextButton: '.swiper-button-next',
         prevButton: '.swiper-button-prev',

});

///. sm only slider
/*
 var swiperMain = new Swiper('.swiper-container-front.front.main', {
           pagination: '.swiper-pagination.front.main',
             paginationClickable: true,
             slidesPerView: 1,
             loop: true,
             touchRatio: 0.2,
             autoplay:4000,
             effect: 'fade',
             observer:'true',
             observeParents:'true',
 });
*/

 //product details page
/* var productGalleryTop = new Swiper('.swiper-container.product-gallery-top',
  {
    nextButton: '.product-gallery-top .swiper-button-next',
    prevButton: '.product-gallery-top .swiper-button-prev',
    spaceBetween: 0,
    centeredSlides: true,
    slideToClickedSlide: true,
   onSlideChangeStart: function (s) {
                       var activeSlide = $('.swiper-slide').eq(s.activeIndex);
                       $('.swiper-slide > div').removeClass('zoom');
                       activeSlide.find('.item-img-inner').addClass('zoom');
                       zoominDestroy();
                       zoominState();

                    },
});
 var productGalleryThumbs = new Swiper('.gallery-thumbs.product-gallery-thumbs', {
     spaceBetween: 15,
     slidesPerView: 4,
     touchRatio: 0.2,

     slideToClickedSlide: true,

 });


 productGalleryTop.params.control = productGalleryThumbs;
 productGalleryThumbs.params.control = productGalleryTop;
 //product details page
*/
//slick
/*
 $('.slider-top.product').slick({
 slidesToShow: 1,
 slidesToScroll: 1,
 arrows: false,
 fade: true,
 focusOnSelect: false,
 asNavFor: '.slider-thumbs.product'
});

$('.slider-thumbs.product').slick({
 slidesToShow: 3,
 slidesToScroll: 1,
 asNavFor: '.slider-top.product',
 centerMode: true,
 focusOnSelect: true
});

$('.slider-top.product, .slider-thumbs.product').on('afterChange', function(event){
zoominDestroy();
$('.slick-slide .img-wrp').removeClass('zoom');
$('.slick-current .img-wrp').addClass('zoom');
zoomInit();
});
*/
///slick


//product zoom in
//zoomInit();
/*function zoomInit(){
    $(".zoom img").ezPlus({
        tint: true,
        tintColour: '#F90',
        tintOpacity: 0.5,
        zoomWindowOffsetX: 10,
        scrollZoom: true,
    });

};*/

//product zoom in initialize
  //function zoominState(){
    /*
  function zoomInit(){
     var $html = $('html');

    if($('.zoom').length>0){
     // if ($('html').hasClass("mobile")) {
   if ($html.hasClass('mobile') || $html.hasClass('tablet')) {
                $('.zoom a').magnifik();
       }
       else {
             $('.zoom a').click(function( event ) {
                  event.preventDefault();
                  event.stopImmediatePropagation();
                  return false;
              });

              $(".zoom img").ezPlus({
                //  tint: false,
                //  tintColour: '#000',
                //  tintOpacity: 0.5,
                  zoomWindowOffsetX: 10,
                  scrollZoom: true,

                  zoomWindowFadeIn: 200,
                  zoomWindowFadeOut: 200,
                  lensFadeIn: 200,
                  lensFadeOut: 200,

                  zoomWindowWidth: 360,
                  zoomWindowHeight: 491

              });
        }
    }
  }
*/
  ///zoomin initialize

  //zoominDestroy
   /* function zoominDestroy(){
      $('.zoomContainer').remove();  
    }*/
    ///zoominDestroy
/*
  $('.product-gallery-wrp .ic-zoom-s').click(function(event) {
   $('.zoom a').trigger('click');
  });
*/
///product zoom in
