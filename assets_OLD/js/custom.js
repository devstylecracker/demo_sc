$(document).ready(function() {

// navigation width
var  winWidth =  $(window).width();
//var  contWidth =  $('.container').width();
var  winWidth =  $(window).width();
var  contWidth =  $('.container-fluid').width();

if(winWidth < 910){
  menuInnerWidth = '100%';
}
else {
  menuInnerWidth = '910px';
}
$(this).find('.sub-menu').css('width', '100%');
//$(this).find('.sub-menu').css('width', winWidth +'px');
$(this).find('.sub-menu-inner').css('width', menuInnerWidth);
/// navigation width
// submenu

  $(".sc-navbar-primary").hoverIntent({
                over: addClassHover,
                out: removeClassHover,
                timeout:100,
                selector: 'li'
  });

   function addClassHover()
  {
    if (!$(".sc-navbar-left-wrp").hasClass("open")) {
        $(this).addClass('hover');
      };
  }
    function removeClassHover()
  {
      $(this).removeClass('hover');
  }
// submenu

$(document).on('click', '.dropdown .dropdown-menu', function (e) {
  e.stopPropagation();
});

// search bar
$('.form-control-search').keyup(function (e){
$(".search-panel-inner .close").fadeIn(600);
});
$(".search-panel-inner .close").click(function(e) {
  $('.form-control-search').val("");
  $(this).fadeOut(600);
    $('.form-control-search').focus();
});

$(window).click(function() {
  $("#search-panel").collapse("hide");
});

$('#search-panel').click(function(event){
    event.stopPropagation();
});

///. search bar

//signup
$('.btn-email-wrp').click(function(event) {

    $(".signup-by-email").show();
    $(".signup-options").hide();

});
///.signup

// checkout


$('.offers-list .sc-checkbox').click(function(event) {
    var height = $(this).parents('.modal-content').height();
    $(this).parents('.modal-content').css('height', height+'px');

    $(".box-offers-list").hide();
    $(".box-offers-details").show();
    $(".modal-footer").hide();

});

$('.box-offers-details .back').click(function(event) {
    $(".box-offers-details").hide();
    $(".box-offers-list").show();
    $(".modal-footer").show();
});

$('.box-checkout.step-done .box-title').click(function(event) {
    $(".box-checkout").removeClass('open');
    $(this).parents(".box-checkout").addClass('open');
});

///.checkout

//material fields

  $('.form-group-md input, .form-group-md textarea').each(
      function(){
          var val = $(this).val().trim();
          if (val == ''){
            $(this).addClass('empty');
            $(this).removeClass('not-empty');
          }
          if (val.length){
            $(this).addClass('not-empty');
            $(this).removeClass('empty');
          }
      });

  $('.form-group-md input + label, .form-group-md textarea + label').css({'visibility': 'visible'})

// /.material fields

// Added for Product-detail  size selection
$('.productsize').click(function(event){
  $( ".productsize" ).each( function( index, element ){
    $(this).removeClass('active');
  });
  $(this).addClass('active');
});

//pa -cropit
$('#image-cropper').cropit({smallImage:'allow'  });
//pa -cropit
/*
$(window).scroll(function(){
      //fixed header
        var sticky = $('.header.sticky');
        var scroll = $(window).scrollTop();
        var height = $(".header.sticky").height();
        var winHeight = $(window).height() + 80;

      //  if (scroll >= height && scroll > winHeight)
        if ( scroll > winHeight)
        {
           sticky.addClass('fixed');
        }
        else {
          sticky.removeClass('fixed');
        }
      ///fixed header

});
*/

/* sticky header */
/*
$(function(){
    var headerHeight = $(".header.sticky").height();
    var winHeight = $(window).height();
  $(window).scroll(function() {
    var scroll = getCurrentScroll();
      if (scroll > headerHeight  ) {
           $('.header.sticky').addClass('fixed');
        }
        else {
            $('.header.sticky').removeClass('fixed');
        }
  });
function getCurrentScroll() {
    return window.pageYOffset || document.documentElement.scrollTop;
    }
});
*/
/* //sticky header */

/* sticky header updated */
// Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var headerHeight = $('.header.sticky').outerHeight();
var normalHeight = $(window).height() + headerHeight;

$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();

    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
        return;

    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    //if (st > lastScrollTop && st > normalHeight){
    if (st > normalHeight ){
        // Scroll Down
        $('.header.sticky').addClass('fixed');

      /*  if (st > lastScrollTop ){
                $('.header.sticky').removeClass('fixed');
              }*/

    } else {
        // Scroll Up
            $('.header.sticky').removeClass('fixed');
    }

    lastScrollTop = st;
}

/* //sticky header updated*/

/* smooth scrolling */
$('a.link-scrolling').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
        || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
           if (target.length) {
             $('html,body').animate({
                 scrollTop: target.offset().top
            }, 1000);
            return false;
        }
    }
});
/* ///.smooth scrolling */

$('.scrollbar').perfectScrollbar();

//checkout page
checkoutAccordian();

//tootip
$('[data-toggle="tooltip"]').tooltip();

//datepicker
$(".datepicker").datepicker({ format: "dd-mm-yyyy",
 endDate: '-15y',
 autoclose: true});

$('.datepicker').change(function(e) {
	$('.datepicker').addClass('not-empty');
});

//sc slideover
$(".sc-slideover .close").click(function () {
$(this).parents(".sc-slideover").slideUp('fast');
});
$(window).load(function() {
 initialSlideover();
});

///sc slideover

 // $('.sc-popover[data-toggle="popover"]').popover({html:true, container:'.item', placement:'bottom'});

          $('body').on('click', function (e) {
            $('[data-toggle="popover"]').each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
            });
          });


// /. documet.ready
});

/*==================================================================================================================== */
/*==================================================================================================================== */
/* all functions */

$('.form-group-md input, .form-group-md textarea').on('blur', function(event) {
  if(!$(this).val()){
      $(this).addClass('empty');
      $(this).removeClass('not-empty');
    }
    if($(this).val()){
      $(this).addClass('not-empty');
      $(this).removeClass('empty');
    }
});
// /.material fields
function checkoutAccordian(){
$('.box-checkout.step-done .box-title').click(function(event) {
    $(".box-checkout").removeClass('open');
    $(this).parents(".box-checkout").addClass('open');
	$('html, body').animate({ scrollTop: ($(this).offset().top) - 100 }, 600);


});
}

function _targetTweet(loc,title,look_id){
    window.open('http://twitter.com/share?url=' + loc + '&text=' + title + '&', 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

}


function _targetPinterest(loc,title,look_id,img_src){
    window.open('https://www.pinterest.com/pin/create/button/?url=' + loc + '&description=' + title + '&media=' + img_src + '&', 'pinterestwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    //return  x.toString().substring(0,x.toString().length-3).replace(/\B(?=(\d{2})+(?!\d))/g, ",") + "," + x.toString().substring(x.toString().length-3);
}

function money_formatter(x) {
  $(x).each( function() {
        $(this).html($(this).html().replace($(this).text().trim(), numberWithCommas($(this).text().trim())));
    });
}

function scrolltop(){
  $("html, body").animate({ scrollTop: 0 }, 600);
}

function initialSlideover() {
 if(getCookiee('first_time_open') != 1){
   setTimeout(openSlideover(), 100);
   document.cookie = "first_time_open=1;path=/";
 }
 setTimeout(intervalSlideover, 100);
}

//Slideover
function openSlideover(){
$("#slideover").slideDown('slow');
};
var i=0;
var intervalSlideover =  setInterval(function() {
 openSlideover();
}, getCookiee("popup_timer"));

////Slideover

/* get cookie value */
function getCookiee(cname) {
     var name = cname + "=";
     var ca = document.cookie.split(';');


}
