function msg(m){

    console.log(m);

}
        $(document).ready(function() {
            $(".page-scbox-form").on("contextmenu",function(e){
               return false;
            }); 
        });

function Initialize(){

    msg('JS Initialize function....');

    //== Generic function for UL LIST =======================

	$("ul.add-event li").on("click touch", function(event) {

        event.preventDefault();

		var thisObj = $(this);

		if (thisObj.closest('ul').hasClass('single-selection')) {

			msg('single-selection event...');

            thisObj.closest('ul').find('li').removeClass('active');

            thisObj.addClass('active');

		} else if (thisObj.closest('ul').hasClass('package-selection')) {

            msg('package-selection event...');

            $('.base-container').find('ul').each(function(i,element){

                $(element).find('li').each(function(i,e){

                    $(e).removeClass('active');

                });

            });

            thisObj.addClass('active');

        } else if (thisObj.closest('ul').hasClass('size-selection')) {

            msg('size-selection event...');

            thisObj.closest('ul').find('label.sc-checkbox').each(function(i,container){

                $(this).removeClass('active');

            });

            var obj = thisObj.find('label.sc-checkbox');

            if (!obj.hasClass('active')) {

				obj.addClass('active');

			} else {

				obj.removeClass('active');

			}

        } else if (thisObj.closest('ul').hasClass('multi-selection'))  {

            msg('Multi selection event...');

            if (!thisObj.hasClass('active')) {

				thisObj.addClass('active');

			} else {

				thisObj.removeClass('active');

			}

        } else if (thisObj.closest('ul').hasClass('limited-selection'))  {

            msg('limited selection events...');

            

            var intSelectionLimit = thisObj.closest('ul').attr('selection-limit');

            var intUserSelected = 0;

            thisObj.closest('ul').find('li').each(function(i,element){

                if($(this).hasClass('active')){

                    intUserSelected++;

                }

            });

            if (!thisObj.hasClass('active') && intUserSelected<intSelectionLimit) {

				thisObj.addClass('active');

			} else {

				thisObj.removeClass('active');

			}

        } 

		CheckWhichEvent($(this).closest(".box-container").attr("type"),$(this).closest(".box-container"));

	});

    //== KEYBOARD function for BUTTON ======================= 

    if(intCurrentScreen==3) {

       $('#scbox_billname').keyboard({

             validate : function(keyboard, value, isClosing) {

                strName = value;

                setCookie('str_name', strName);

                CurrentPageQuestionCompleted();

                return true;

              }

         });

        $('#scbox_billemailid').keyboard({

             validate : function(keyboard, value, isClosing) {

                strEmail = value;

                setCookie('str_email', strEmail);

                CurrentPageQuestionCompleted();

                return true;

              }

        });

        $('#scbox_billmobile').keyboard({

            validate : function(keyboard, value, isClosing) {

                intMobile = value;

                setCookie('str_mobile', intMobile);

                CurrentPageQuestionCompleted();

                return true;

              }

        }); 

    }

    



    //== Generic function for BUTTON =======================   

    $('.btns-wrp .scbtn').on('click touch', function(e){

        ChangeScreen($(this));

    });

    //##STEP SELECTION FUNCTION ==================

    var intStep = 0;

    $('#header').find('.step').each(function(i,element){

        intStep++;

        if(intStep<intCurrentScreen){

            $(this).addClass('complete');

        }

    });

    if(intCurrentScreen==6){

        setTimeout(function(){

            ResetAll();

            window.location.href = baseURL+arrPageLinks[1];

        },3000);

    }

    ShowHideButtons();

    msg('intStep: '+intStep+'  ::  intCurrentScreen: '+intCurrentScreen);

}



//## CHECKWHICHEVENT FUNCTION -----------------------

function CheckWhichEvent(p_container,p_basecontainer){

    if(p_container == 'user-gender') {

        strGender = GetSet_UserSelection(p_basecontainer);

        setCookie('str_gender', strGender);
        //# Edited as per feedback =========================
        setTimeout(function(){ 
            $('.btn-next').trigger('click');
        },800);

    } else if(p_container == 'box-package') {

        strPackageType= $(p_basecontainer).attr('value');

        strPackageComboType = GetSet_UserSelection(p_basecontainer);

        setCookie('str_package_type', strPackageType);

        setCookie('str_package_combotype',strPackageComboType);

    } else if(p_container == 'top-size') {

        strTopSize = GetSet_UserSelection(p_basecontainer,true);

        setCookie('str_top_size', strTopSize);

    } else if(p_container == 'bottom-size') {

        strBottomSize = GetSet_UserSelection(p_basecontainer,true);

        setCookie('str_bottom_size', strBottomSize);

    } else if(p_container == 'footwear-size') {

        strFootwearSize = GetSet_UserSelection(p_basecontainer,true);

        setCookie('str_footwear_size', strFootwearSize);

    }else if(p_container == 'box-occasion') {

        strOccasion = GetSet_UserSelection(p_basecontainer);

        setCookie('str_occasion', strOccasion);

    }else if(p_container == 'jewellery-type') {

        arrJewelleryTypes = GetSet_UserSelection(p_basecontainer);

        setCookie('arr_jewellery_types', arrJewelleryTypes);

    }else if(p_container == 'style-preference') {

        arrStylePreferences = GetSet_UserSelection(p_basecontainer);

        setCookie('arr_style_preferences', arrStylePreferences);

    }

    CurrentPageQuestionCompleted();

}



//## GET USER SELECTION FUNCTION ---------------------

function GetSet_UserSelection(objContainer,isInnerElement){

    //var objContainer = FindContainer(obj);

    var arrUserSelected = [];

    objContainer.find('li').each(function(i,obj){ 

        if(isInnerElement==true){

            if($(this).find('label').hasClass('active')){  

                arrUserSelected.push($(this).attr('value'));

            }

        } else {

            if($(this).hasClass('active')){  

                arrUserSelected.push($(this).attr('value'));

            }

        }

    });

    return arrUserSelected;

} 



//## PREV-NEXT FUNCTION ---------------------

var intTotalScreen = 6; 

var intCurrentScreen = 0;

var arrPageLinks = ['','home','step1','step2','step3','step4','step5'];

var arrPageLinks_men = ['','home','step1_men','step2','step3_men','step4_men','step5'];

var baseURL = 'http://scnest.in/borough/';

   var strGenderSelected = getCookie('str_gender');

function ChangeScreen(obj){

   var strGenderSelected = getCookie('str_gender');

    if(strGenderSelected=='Men') {

        arrPageLinks = arrPageLinks_men;


    }
     //alert('strGenderSelected: ' +strGenderSelected + ' arrPageLinks: ' +arrPageLinks);

    if(intCurrentScreen==3){

        UpdateUserInfo();

    }   

    if(obj.hasClass('btn-next') && !obj.hasClass('state-disabled')){

        msg("Next Button");
         $(".se-pre-con").show();
        intCurrentScreen<arrPageLinks.length-1?intCurrentScreen++:intCurrentScreen;

        window.location.href = baseURL+arrPageLinks[intCurrentScreen];

    } else if(obj.hasClass('btn-prev')){

        msg("Prev Button");
         $(".se-pre-con").show();
        intCurrentScreen>0?intCurrentScreen--:intCurrentScreen;

        window.location.href = baseURL+arrPageLinks[intCurrentScreen];

    } else {

        alert('Please select all question(s).');

    }
     

} 



function ShowHideButtons(){

    if(intCurrentScreen==1){

        $('.btn-prev').addClass('hide');

        $('.btn-next').addClass('state-disabled')

    }

    else if(intCurrentScreen==6){

        $('.btn-prev').addClass('hide');

        $('.btn-next').addClass('hide');

    }

    else if(intCurrentScreen>1 && intCurrentScreen<=6){

        $('.btn-prev').removeClass('hide');

        $('.btn-next').removeClass('hide').addClass('state-disabled');

    }

    msg('intCurrentScreen---: '+intCurrentScreen);

}

//## VARIBALE DECLARATIONS --------------------------

    var strGender = '';

    var strPackageType = '';

    var strPackageComboType = '';

    var strName = '';

    var strEmail = '';

    var intMobile = '';

    var strTopSize = '';

    var strBottomSize = '';

    var strFootwearSize = '';

    var strOccasion = '';

    var arrStylePreferences = [];

    var arrJewelleryTypes = [];

    var arrGrooming = [];



function UpdateUserInfo(){

     

}



//setCookie('scbox_objectid', 281, 365);

//strGenderSelected = getCookie('scbox-gender');

function ResetAll(){

    strGender = '';

    strPackageType = '';

    strPackageComboType = '';

    strName = '';

    strEmail = '';

    intMobile = '';

    strTopSize = '';

    strBottomSize = '';

    strFootwearSize = '';

    strOccasion = '';

    arrStylePreferences = [];

    arrJewelleryTypes = [];

    arrGrooming = [];

}

//## CHECK QUESTION COMPLETED FUNCTION ------------

var arrQuestion = [1,1,3,4,2];

function CurrentPageQuestionCompleted(){

    var isPageQuestionCompleted = false;

    if(intCurrentScreen==1){

        if(strGender.length>0){

            isPageQuestionCompleted = true;

        }

    } else if(intCurrentScreen==2){

        if(strPackageType.length>0 && strPackageComboType.length>0){

            isPageQuestionCompleted = true;

        }

    } else if(intCurrentScreen==3){

        if(strName.length>0 && strEmail.length>0 && intMobile.length>0){

            isPageQuestionCompleted = true;

        }

    }else if(intCurrentScreen==4){

        if(strTopSize.length>0 && strBottomSize.length>0 && strFootwearSize.length>0 && strOccasion.length>0){

            isPageQuestionCompleted = true;

        }

    }else if(intCurrentScreen==5){

            if(strGenderSelected=='Men') {

               if(arrStylePreferences.length>0){

                                isPageQuestionCompleted = true;

                            }


            }else if(strGenderSelected=='Women') {

               if(arrStylePreferences.length>0 && arrJewelleryTypes.length>0){

                                isPageQuestionCompleted = true;

                            }


            }
                       

    }

    //## if questions completed then enable next button

    if(isPageQuestionCompleted ==true){

        $('.btn-next').removeClass('state-disabled');

    } else {

        $('.btn-next').addClass('state-disabled');

    }

    msg('isPageQuestionCompleted: '+isPageQuestionCompleted);

}



//## SET COOKIE FUNCTION --------------------------

function setCookie(key, value) {

    var expires = new Date();

    expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));

    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();

}



//## GET COOKIE FUNCTION --------------------------

function getCookie(key) {

    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');

    return keyValue ? keyValue[2] : null;

}

/*(function($){
    $.fn.disableSelection = function() {
        return this
                 .attr('unselectable', 'on')
                 .css('user-select', 'none')
                 .on('selectstart', false);
    };
})(jQuery);*/