var getUrl = window.location;
var clickcnt = 0;
var scbox_taxes = $('#scbox_servicetax').val();
var scbox_quantity = 1;
var is_order_placed = false; 
var intGender = 0;
var intGenderbill = 0;

$(document).ready(function(e){
var objParam = getUrl.toString().split("/");
var seclastparam = objParam[objParam.length-2];
var lastparam = objParam[objParam.length-1];

  if(seclastparam=='book-scbox')
  { 
    //ChangeGender();   
    var boxprice = getCookie('scbox_price');
    $('#scbox_price').val(boxprice);    
    $('#checkout_price').html('<i class="fa fa-inr"></i> '+parseFloat(boxprice).toFixed(2));   
    var scbox_subtotal = parseFloat(scbox_quantity)*parseFloat(boxprice);
    var checkout_taxes  = (parseFloat(scbox_taxes)*parseFloat(scbox_subtotal))/100;
    checkout_taxes = Math.round(checkout_taxes * 100) / 100;
    $('#checkout_price').html('<i class="fa fa-inr"></i> '+parseFloat(scbox_subtotal).toFixed(2));
    $('#checkout_subtotal').html('<i class="fa fa-inr"></i> '+parseFloat(scbox_subtotal).toFixed(2));
    $('#checkout_taxes').html('<i class="fa fa-inr"></i> '+parseFloat(checkout_taxes).toFixed(2));    
    scbox_total =  scbox_subtotal + checkout_taxes;
    scbox_total = Math.round(scbox_total * 100) / 100;
    $('#checkout_total').html('<i class="fa fa-inr"></i> '+parseFloat(scbox_total).toFixed(2));
    $('#cart_total').val(scbox_total);   
  }else
  {
    $( "#womentab" ).trigger( "click" );
  }
  //console.log('lastparam--'+lastparam);
  if(lastparam=='sc-box?f=1')
  {
   $('html, body').animate({
          scrollTop: $("#scbox-packages").offset().top
      }, 2000);
  }


//##Gender Female Click -------------------------------
 $('#scboxgender-female').click(function(e){
      if(!$(this).hasClass("state-disabled")) {
          $('#gender-error').addClass('hide');
          $('#scboxgender-male').removeClass('active');
          $('#scboxgender-female').addClass('active');
          $('#scbox_gender').val('1');      
          intGender = $('#scbox_gender').val();
          document.cookie = "scbox-gender=women;path=/";         
      }else{
          $('#gender-error').addClass('hide');
          $('#scboxgender-male').removeClass('active');
          $('#scboxgender-female').addClass('active');
          $('#scbox_gender').val('1');         
          intGender = $('#scbox_gender').val();
          document.cookie = "scbox-gender=women;path=/";        
      }
  });
//##Gender Male Click -------------------------------
  $('#scboxgender-male').click(function(e){
      if(!$(this).hasClass("state-disabled")) {
        $('#gender-error').addClass('hide');
        $('#scboxgender-female').removeClass('active');
        $('#scboxgender-male').addClass('active');
        $('#scbox_gender').val('2');        
        intGender = $('#scbox_gender').val();
        document.cookie = "scbox-gender=men;path=/";          
      }else{
        $('#gender-error').addClass('hide');
        $('#scboxgender-female').removeClass('active');
        $('#scboxgender-male').addClass('active');
        $('#scbox_gender').val('2');            
        intGender = $('#scbox_gender').val();
        document.cookie = "scbox-gender=men;path=/"; 
      }
  });

    //##Bill Gender Female1 Click -------------------------------
  $('#scboxgender-billfemale').click(function(e){
      if(!$(this).hasClass("state-disabled")) {
        $('#billgender-error').addClass('hide');
        $('#scboxgender-billmale').removeClass('active');
        $('#scboxgender-billfemale').addClass('active');
        $('#scbox_billgender').val('1');         
        intGenderbill = $('#scbox_billgender').val();
        document.cookie = "scbox-billgender=women;path=/"; 
      }else{
        $('#gender-billerror').addClass('hide');
        $('#scboxgender-billmale').removeClass('active');
        $('#scboxgender-billfemale').addClass('active');
        $('#scbox_billgender').val('1');
        intGenderbill = $('#scbox_billgender').val();
        document.cookie = "scbox-billgender=women;path=/"; 
      }
  });
//##Bill Gender Male Click -------------------------------
  $('#scboxgender-billmale').click(function(e){
      if(!$(this).hasClass("state-disabled")) {
        $('#billgender-error').addClass('hide');
        $('#scboxgender-billfemale').removeClass('active');
        $('#scboxgender-billmale').addClass('active');
        $('#scbox_billgender').val('2');               
        intGenderbill = $('#scbox_billgender').val();
        document.cookie = "scbox-billgender=men;path=/";
      }else{
        $('#billgender-error').addClass('hide');
        $('#scboxgender-billfemale').removeClass('active');
        $('#scboxgender-billmale').addClass('active');
        $('#scbox_billgender').val('2');
        intGenderbill = $('#scbox_billgender').val();
        document.cookie = "scbox-billgender=men;path=/"; }
  });   

  $('#customboxprice').keypress(function(event){
     if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
         event.preventDefault(); //stop character from entering input
     }
  });
 
  //============================================
 // stylistCall();
  
//===============================================
  $('.box-package').click(function(e) {
    var scboxpack = $(this).attr('data-pack');
    var scboxprice = $(this).attr('data-scbox-price');
    var scboxobjectid = $(this).attr('data-scbox-objectid');
    var scboxproductid = $(this).attr('data-scbox-productid');
    if(scboxpack!='' && scboxprice!='')
    {
      $('#customboxprice-error').addClass('hide');
      $('#scbox-package').val(scboxpack);
      $('#scbox-price').val(scboxprice);
      $('#scbox-objectid').val(scboxobjectid);
      $('#scbox-productid').val(scboxproductid);
      if(scboxpack!='package4'){
        $('#customboxprice').val('');
      }
    }else if(scboxprice == '' && $('#customboxprice').val()=='')
    {
      $('#customboxprice-error').removeClass('hide');
      scboxprice = $('#customboxprice').val();
      $('#scbox-price').val(scboxprice);
      $(e).attr('data-scbox-price',scboxprice);
      $('#scbox-package').val(scboxpack);
      $('#scbox-price').val(scboxprice);
      $('#scbox-objectid').val(scboxobjectid);
      $('#scbox-productid').val(scboxproductid);
    }

  });
});

//##Gender wise Data call -------------------------------
function scboxregister()
{ 
 /* var scboxpack = $('.box-package').hasClass('active');
  */
  var objectid = $('#scbox-objectid').val();
  var package = $('#scbox-package').val();
  var price = $('#scbox-price').val();
  var productid = $('#scbox-productid').val();
  var utm_source = $('#utm_source').val();
  var utm_medium = $('#utm_medium').val();
  var utm_campaign = $('#utm_campaign').val();
  var page_type = $('#scbox-page').val();
  var utm_str = '';
  if(utm_source!='' || utm_medium!='' || utm_campaign!=''){
    utm_str = '?&utm_source='+utm_source+'&utm_medium='+utm_medium+'&utm_campaign='+utm_campaign;
  }
  //console.log('objectid='+objectid+'=package='+package+'=price='+price+'=productid='+productid);
  if(package == 'package4'){
    var customprice = $('#customboxprice').val();
    var customprice1 = $('#customboxprice1').val();
    if(customprice >= 8000 || customprice1 >= 8000){
      if(customprice >= 8000)
      {
         $('#scbox-price').val(customprice);
      }else if( customprice1 >= 8000)
      {
         $('#scbox-price').val(customprice1);
      }
     
      price = $('#scbox-price').val();
    }else{
      alert('The minimum amount for Customized SC Box is ₹ 8,000');
    }    
  }else
  {
    $('#customboxprice').val('');
  }  
  document.cookie="scbox_price="+price+";path=/";  
  document.cookie = "scbox_objectid="+objectid+";path=/";
  document.cookie="scfr="+'scbox'+";path=/";
  //msg(getCookie('Scbox_price'));
  ga('send', 'event', 'SCBOX','clicked',price);msg("ga('send', 'event', 'SCBOX','clicked',price) "+price);
  fbq('track', 'ViewContent', price);
  //&& price >=8000
  if(objectid!='' && package!='' && price!='' && productid!='' && package!='package4' )
  {
      $("#scboxregistererror").removeClass('error');
      $("#scboxregistererror").addClass('hide');    
      //window.location.href = sc_baseurl+'/book-scbox/'+objectid+'?step=1'+utm_str;
      window.location.href = sc_baseurl+'book-scbox/step1/'+utm_str;
  }if(objectid!='' && package!='' && price!='' && productid!='' && package=='package4' && price>=8000)
  {
     window.location.href = sc_baseurl+'book-scbox/step1/'+utm_str;
  }else
  {
    $("#scboxregistererror").addClass('error');
    $("#scboxregistererror").removeClass('hide');
  }
}

//## Select Box Package Container --------------
function SetPackage(e)
{
  $(e).closest(".row").find(".box-package").each(function(e){
    $(this).removeClass("active");
    $(this).addClass('deactive');
  });
  $(e).removeClass('deactive');
  $(e).addClass('active');

  var defaultgender = getCookie('scbox-gender'); 
  if(defaultgender=='')
  {
     document.cookie = "scbox-gender=women;path=/"; 
  }

    var scboxpack = $(e).attr('data-pack');
    var scboxprice = $(e).attr('data-scbox-price');
    var scboxobjectid = $(e).attr('data-scbox-objectid');
    var scboxproductid = $(e).attr('data-scbox-productid');
    if(scboxpack!='' && scboxprice!='')
    {
      $('#scbox-package').val(scboxpack);
      $('#scbox-price').val(scboxprice);
      $('#scbox-objectid').val(scboxobjectid);
      $('#scbox-productid').val(scboxproductid);
      if(scboxpack!='package4')
      {
         scboxregister();
      }
     
    }
    if(lastparam=='sc-box')
    {
      //window.location.href = sc_baseurl+'/book-scbox/'+scboxobjectid;
    }

  /*var id = $(e).attr("id");
  $("#"+type).val(id);*/
}

//## EDIT FUNCTIONS =================================================
var bodyshape, bodystyle;
function ChangeGender()
{
    intGender = $('#scbox_gender').val();
    var userid = $('#scbox_userid').val();
    bodyshape = $('#body_shape_ans').val();
    bodystyle = $('#scbox_style').val();
    bodystyle_arr = bodystyle.toString().split(",");

    if(intGender!='' && intGender==1)
    {
      $('#gender-error').addClass('hide');
      $('#scboxgender-male').removeClass('active');
      $('#scboxgender-female').addClass('active');
      $('#scbox_gender').val('1');
      $('.box-style').removeClass('box-style-male');
      $('.box-style').addClass('box-style-female');
      if(userid!=''){
        //GenderWiseGetData();        
      }
    }else if(intGender==2)
    {
      $('#gender-error').addClass('hide');
      $('#scboxgender-female').removeClass('active');
      $('#scboxgender-male').addClass('active');
      $('#scbox_gender').val('2');
      $('.box-style').removeClass('box-style-female');
      $('.box-style').addClass('box-style-male');
      if(userid!=''){
       //GenderWiseGetData();       
      }
    } 
}


function validateStep(id)
{
  if(id!='' && id<7 && id>0)
  { 
    if(id==1)
    {
      var final_cookies = '';
      var is_valid = 0;
      var chkYes = document.getElementById("chkYes");
      if($("#scbox_gender").val()=='' || $("#scbox_name").val()=='' || $("#scbox_gender").val()=='' || $("#scbox_emailid").val()=='' || $("#scbox_mobile").val()=='' || $("#scbox_state").val()=='' || $("#scbox_city").val()=='' || $("#scbox_shipaddress").val()=='' || $("#scbox_pincode").val()=='' || $("#day_start").val()==''|| $("#month_start").val()==''|| $("#year_start").val()=='' || $("#scbox_profession").val()=='' ){ //|| $("#scbox_profession").val()==''
              if($("#scbox_gender").val()=='' ){
                $("#scbox_gender-error").removeClass('hide');
                $("#scbox_gender").addClass('invalid');}
              if($("#scbox_name").val()==''){
                $("#scbox_name").addClass('invalid');}             
              if($("#scbox_emailid").val()==''){
                $("#scbox_emailid").addClass('invalid');}
              if($("#scbox_mobile").val()==''){
                $("#scbox_mobile").addClass('invalid');}
              if($("#scbox_state").val()==''){
                $("#scbox_state").addClass('invalid');}
              if($("#scbox_city").val()==''){
                $("#scbox_city").addClass('invalid');}
              if($("#scbox_shipaddress").val()==''){
                $("#scbox_shipaddress").addClass('invalid');}
              if($("#scbox_pincode").val()==''){
                $("#scbox_pincode").addClass('invalid');}
              if($("#day_start").val()==''){
                $("#scbox_age").addClass('invalid');}
              if($("#month_start").val()==''){
                $("#scbox_age").addClass('invalid');}
              if($("#year_start").val()==''){
                $("#scbox_age").addClass('invalid');} 
              if($("#scbox_profession").val()==''){
                $("#scbox_profession").addClass('invalid');}  

               //is_valid =1;
              if(chkYes.checked)
              {
                  if($("#scbox_billgender").val()=='' ){
                    $("#scbox_billgender-error").removeClass('hide');
                    $("#scbox_billgender").addClass('invalid');} 
                  if($("#scbox_billname").val()==''){
                    $("#scbox_billname").addClass('invalid');}  
                  if($("#scbox_billemailid").val()==''){
                    $("#scbox_billemailid").addClass('invalid');} 
                  if($("#scbox_billmobile").val()==''){
                    $("#scbox_billmobile").addClass('invalid');}
                  if($("#scbox_billstate").val()==''){
                    $("#scbox_billstate").addClass('invalid');} 
                  if($("#scbox_billcity").val()==''){
                    $("#scbox_billcity").addClass('invalid');}
                  if($("#scbox_billaddress").val()==''){
                    $("#scbox_billaddress").addClass('invalid');}
                  if($("#scbox_profession").val()==''){
                    $("#scbox_profession").addClass('invalid');}
                  if($("#scbox_billpincode").val()==''){
                    $("#scbox_billpincode").addClass('invalid');}
                  if($("#day_billstart").val()==''){
                    $("#scbox_billage").addClass('invalid');}
                  if($("#month_billstart").val()==''){
                    $("#scbox_billage").addClass('invalid');}
                  if($("#year_billstart").val()==''){
                    $("#scbox_billage").addClass('invalid');} 
                  //is_valid =1;
              }
              scrolltop();
          }else if( (chkYes.checked) && ($("#scbox_billgender").val()=='' || $("#scbox_billname").val()=='' || $("#scbox_billemailid").val()=='' || $("#scbox_emailid").val()=='' || $("#scbox_billmobile").val()=='' || $("#scbox_billstate").val()=='' || $("#scbox_billcity").val()=='' || $("#scbox_billaddress").val()=='' || $("#scbox_billpincode").val()=='' || $("#day_billstart").val()==''|| $("#month_billstart").val()==''|| $("#year_billstart").val()=='' )){
              
              if(chkYes.checked)
              {
                  if($("#scbox_billgender").val()=='' ){
                    $("#scbox_billgender-error").removeClass('hide');
                    $("#scbox_billgender").addClass('invalid');} 
                  if($("#scbox_billname").val()==''){
                    $("#scbox_billname").addClass('invalid');}  
                  if($("#scbox_billemailid").val()==''){
                    $("#scbox_billemailid").addClass('invalid');} 
                  if($("#scbox_billmobile").val()==''){
                    $("#scbox_billmobile").addClass('invalid');}
                  if($("#scbox_billstate").val()==''){
                    $("#scbox_billstate").addClass('invalid');} 
                  if($("#scbox_billcity").val()==''){
                    $("#scbox_billcity").addClass('invalid');}
                  if($("#scbox_billaddress").val()==''){
                    $("#scbox_billaddress").addClass('invalid');}
                  if($("#scbox_profession").val()==''){
                    $("#scbox_profession").addClass('invalid');}
                  if($("#scbox_billpincode").val()==''){
                    $("#scbox_billpincode").addClass('invalid');}
                  if($("#day_billstart").val()==''){
                    $("#scbox_billage").addClass('invalid');}
                  if($("#month_billstart").val()==''){
                    $("#scbox_billage").addClass('invalid');}
                  if($("#year_billstart").val()==''){
                    $("#scbox_billage").addClass('invalid');} 
                  //is_valid =1;
              }
              scrolltop();
          }else
          {     
            $("#scbox_gender-error").addClass('hide');           
            $("#scbox_emailid").removeClass('invalid');
            $("#scbox_mobile").removeClass('invalid');
            $("#scbox_state").removeClass('invalid');
            $("#scbox_city").removeClass('invalid');
            $("#scbox_shipaddress").removeClass('invalid');
            $("#scbox_pincode").removeClass('invalid');
            $("#scbox_profession").removeClass('invalid');
            
            if(chkYes.checked){
                $("#scbox_billgender-error").addClass('hide');           
                $("#scbox_billemailid").removeClass('invalid');
                $("#scbox_billmobile").removeClass('invalid');
                $("#scbox_billstate").removeClass('invalid');
                $("#scbox_billcity").removeClass('invalid');
                $("#scbox_billaddress").removeClass('invalid');
                $("#scbox_billpincode").removeClass('invalid');
                validatedob('bill');
               }
              var mobile = $("#scbox_mobile").val();
              var pincode = $("#scbox_pincode").val();
              var dobvalid = validatedob('ship');
              var validname = true;
              var namestr = $('#scbox_name').val(); 
              namestr = namestr.trim();
              namestr = namestr.replace(/[^a-z\s]/gi, ' ');
              namestr = namestr.replace(/\s+/g, " ");
              $('#scbox_name').val(namestr);
              var namearr = namestr.split(" ");     
              if(namearr[1]==undefined || namearr[1]=='')
              {
                $('#scbox_name').addClass('invalid');
                scrolltop();
                validname = false;
              }else
              {
                $('#scbox_name').removeClass('invalid');
                validname = true;
              }
              msg(dobvalid);
              if(pincode.length != 6){ 
                is_valid =1;  
                $("#scbox_pincode").addClass('invalid');
                 scrolltop();
              }else{
                is_valid =0; 
                $("#scbox_pincode").removeClass('invalid');
              }

              if(mobile.length != 10){ 
                is_valid =1;  
                $("#scbox_mobile").addClass('invalid');
                 scrolltop();
              }else{
                is_valid =0;  
                $("#scbox_mobile").removeClass('invalid');
              }

              var emailId = $("#scbox_emailid").val();
              var validemail = validateEmail(emailId); 

            if(dobvalid==true && validname==true && is_valid == 0 && validemail==true)
            {
              ga('send', 'event', 'SCBOX','clicked','Step1-User Details');
              msg("ga('send', 'event', 'SCBOX','clicked','Step1-User Details')");
              return true;
            }

          }
    }else if(id==2)
    {

    }

  }
}

function ScboxFormSubmit(id)
{
    var actionurl = '';var isGift = '';
    var next = 0;
    //var objectid = $('#scbox_objectid').val(); 
     var objectid = getCookie('scbox_objectid');     
    if(id==1)
    {
      //actionurl = sc_baseurl+'/book-scbox/'+objectid+'?step=1';
     if($("#scbox_gender").val()!='' && $("#scbox_name").val()!='' && $("#day_start").val()!='' && $("#month_start").val()!='' && $("#year_start").val()!='' && $("#scbox_gender").val()!='' && $("#scbox_emailid").val()!='' && $("#scbox_mobile").val()!='' && $("#scbox_state").val()!='' && $("#scbox_city").val()!='' && $("#scbox_shipaddress").val()!='' && $("#scbox_pincode").val()!='' ){

          var scbox_name = $('#scbox_name').val().trim();
          scbox_name = scbox_name.replace(/[^a-z\s]/gi, ' ');
          scbox_name = scbox_name.replace(/\s+/g, " ");
          var res = scbox_name.split(" ");
          billing_first_name = res[0];   
          if(res[1]!=''){
            billing_last_name = res[1]; }          
          var billing_mobile_no = $('#scbox_mobile').val().trim();
          var billing_pincode_no = $('#scbox_pincode').val().trim();
          var billing_address = $('#scbox_shipaddress').val().trim();
          billing_address = billing_address.replace(/[^a-zA-Z0-9\\-\\s\\b\/\,\(\)]/gi, ' ');
          billing_address = billing_address.replace(/\s+/g, ' ');
          var billing_city = $('#scbox_city').val().trim();
          var billing_state = $('#scbox_state').val();
          var cart_total = $('#cart_total').val();
          var scbox_price = $('#scbox_price').val();
          var billing_statename = '';
          var scbox_emailid = $("#scbox_emailid").val().trim();
          var utm_source = $('#utm_source').val();
          var utm_medium = $('#utm_medium').val();
          var utm_campaign = $('#utm_campaign').val();
          var scbox_profession = $('#txtprofession').val();
          var scbox_billprofession = $('#txtbillprofession').val();
          
          document.cookie = "cart_email="+scbox_emailid+";path=/";
          document.cookie = "billing_first_name="+billing_first_name+";path=/";    
          document.cookie = "billing_last_name="+billing_last_name+";path=/";     
          document.cookie = "billing_mobile_no="+billing_mobile_no+";path=/";     
          document.cookie = "stylecracker_billing_pincode="+billing_pincode_no+";path=/";    
          document.cookie = "billing_address="+billing_address+";path=/";     
          document.cookie = "billing_city="+billing_city+";path=/";     
          document.cookie = "billing_state="+billing_state+";path=/";
          document.cookie = "billing_stateid="+billing_state+";path=/";
          document.cookie = "scbox_price="+scbox_price+";path=/";
          document.cookie = "sc_source="+utm_source+";path=/";
          document.cookie = "sc_medium="+utm_medium+";path=/";
          document.cookie = "sc_campaign="+utm_campaign+";path=/";
          document.cookie = "scbox_objectid="+objectid+";path=/";

          if(document.getElementById("chkYes").checked){
              isGift = 'true';
              var gifted_email = $("#scbox_billemailid").val();
              document.cookie = "scbox_isgift="+isGift+";path=/";
              document.cookie = "scbox_gifted_email="+gifted_email+";path=/"; 

              if($('#stylist_call').is(":checked")){
                document.cookie = "stylist_call=yes;path=/";
              }else{
                document.cookie = "stylist_call=no;path=/";
              }      
           }else{
               isGift = 'false';
               document.cookie = "scbox_isgift="+isGift+";path=/"; }

           $.ajax({
                url: sc_baseurl+'scbox/savescbox',
                type: 'post',
                data: {'scbox-confirm' : '1','slide':'1', 'scbox_name':$('#scbox_name').val(), 'scbox_emailid':$('#scbox_emailid').val(), 'scbox_mobile':$('#scbox_mobile').val(), 'scbox_gender':$('#scbox_gender').val(), 'scbox_age':$('#scbox_age').val(), 'scbox_objectid':$('#scbox_objectid').val(), 'scbox_shipaddress':$('#scbox_shipaddress').val(), 'scbox_pincode':$('#scbox_pincode').val(), 'scbox_city':$('#scbox_city').val(), 'scbox_state':$('#scbox_state').val(), 'scbox_likes':$('#scbox_likes').val(), 'scbox_dislikes':$('#scbox_dislikes').val(), 'scbox_city':$('#scbox_city').val(), 'scbox_city':$('#scbox_city').val(), 'scbox_userid':$('#scbox_userid').val(), 'scbox_price' : $('#scbox_price').val(),'scbox_package':$('#scbox_package').val(),  'scbox_productid':$('#scbox_productid').val(),'lastparam':lastparam,'utm_source':utm_source,'utm_medium':utm_medium,'utm_campaign':utm_campaign, 'scbox_billname':$('#scbox_billname').val(), 'scbox_billemailid':$('#scbox_billemailid').val(), 'scbox_billmobile':$('#scbox_billmobile').val(), 'scbox_billgender':$('#scbox_billgender').val(),'scbox_billage':$('#scbox_billage').val(),'scbox_billaddress':$('#scbox_billaddress').val(), 'scbox_billpincode':$('#scbox_billpincode').val(), 'scbox_billcity':$('#scbox_billcity').val(), 'scbox_billstate':$('#scbox_billstate').val(), 'scbox_giftmsg':$('#scbox_giftmsg').val(),'scbox_profession':scbox_profession,'scbox_billprofession':scbox_billprofession,'scbox_isgift':isGift  },
                success: function(data,status){
                         msg(data);                        
                          data = data.trim();
                          var objectid = getCookie('scbox_objectid');    
                          if(data!=''){
                            if(data=='success1'){
                               $('#slide_'+id).addClass('hide');
                               next = 4;
                               $('#slide_'+next).removeClass('hide');
                               $('#scbox_slide').val(next);
                               // window.location.href=sc_baseurl+'/book-scbox/step2';       
                                if(getCookie('stylist_call')=='yes' && getCookie('scbox_isgift')=='true'){
                                  //window.location.href=sc_baseurl+'/cart/'+objectid+'?type=scbox&slide=5';
                                  window.location.href=sc_baseurl+'/cart/'+objectid+'?type=scbox&step=3';
                               }else{
                                  //window.location.href=sc_baseurl+'/book-scbox/'+objectid+'?step=2'; 
                                  window.location.href=sc_baseurl+'/book-scbox/step2';                    
                               }                    
                            }else if(data=='success'){          
                               if(getCookie('stylist_call')=='yes' && getCookie('scbox_isgift')=='true'){
                                  //window.location.href=sc_baseurl+'/cart/'+objectid+'?type=scbox&slide=5';
                                  window.location.href=sc_baseurl+'/cart/'+objectid+'?type=scbox&step=3';
                               }else{
                                  //window.location.href=sc_baseurl+'/book-scbox/'+objectid+'?step=2'; 
                                  window.location.href=sc_baseurl+'/book-scbox/step2';                    
                               }               
                              scrolltop();
                            }else if(data=='utm'){
                              window.location.href=sc_baseurl+'/cart/'+objectid+'?type=scbox&step=3'; 
                            }else if(data == 'error'){
                              //window.location.href=sc_baseurl+'/book-scbox?slide=1';
                            }else{
                              $('#scbx_error').html("Unexpected error. Please try again!");
                            }
                          }else{
                          }
                          //event.preventDefault();
                        },
                error: function(xhr, desc, err){
                          // console.log(err);
                        }
                      });
        }
    }else if(id=='2a')
    {      
      var category_selection = getCookie('category_selection');
      var objectid = getCookie('scbox_objectid');
      $.ajax({
          url: sc_baseurl+'scbox/savescbox',
          type: 'post',
          data: {'scbox-confirm' : '1','slide':'2a','scbox_gender':$('#scbox_gender').val(), 'category_selection':category_selection, 'scbox_objectid':$('#scbox_objectid').val(), 'scbox_userid':$('#scbox_userid').val() },
          success: function(data, status) {
             data = data.trim();
              msg(data);

            if(data!=''){
              if(data=='success'){               
               
               //SwitchStep(2);
              }else if(data == 'error'){
                //window.location.href=sc_baseurl+'/book-scbox?slide=1';
              }else{
                $('#scbx_error').html("Unexpected error. Please try again!");
              }
            }else{

            }
            //event.preventDefault();
          },
          error: function(xhr, desc, err) {
            // console.log(err);
          }
        });
     
    }else if(id==2)
    {      
      var pa_selection = getCookie('pa_selection');
      var objectid = getCookie('scbox_objectid');
      $.ajax({
          url: sc_baseurl+'scbox/savescbox',
          type: 'post',
          data: {'scbox-confirm' : '1','slide':'2','scbox_gender':$('#scbox_gender').val(), 'body_shape_ans':$('#body_shape_ans').val(), 'scbox_objectid':$('#scbox_objectid').val(), 'scbox_userid':$('#scbox_userid').val(),'scbox_isgift':isGift,'pa_data': pa_selection },
          success: function(data, status) {
             data = data.trim();
              msg(data);

            if(data!=''){
              if(data=='success'){               
                scrolltop();
                window.location.href=sc_baseurl+'/cart/'+objectid+'?type=scbox&step=3';
               
              }else if(data == 'error'){
                //window.location.href=sc_baseurl+'/book-scbox?slide=1';
              }else{
                $('#scbx_error').html("Unexpected error. Please try again!");
              }
            }else{

            }
            //event.preventDefault();
          },
          error: function(xhr, desc, err) {
            // console.log(err);
          }
        });
     
    }else if(id=='3')
    {      
      var pa_selection = getCookie('pa_selection');
      var objectid = getCookie('scbox_objectid');
      $.ajax({
          url: sc_baseurl+'scbox/savescbox',
          type: 'post',
          data: {'scbox-confirm' : '1','slide':'2','scbox_gender':$('#scbox_gender').val(), 'body_shape_ans':$('#body_shape_ans').val(), 'scbox_objectid':$('#scbox_objectid').val(), 'scbox_userid':$('#scbox_userid').val(),'scbox_isgift':isGift,'pa_data': pa_selection },
          success: function(data, status) {
             data = data.trim();
              msg(data);

            if(data!=''){
              if(data=='success'){               
              
              }else if(data == 'error'){
                //window.location.href=sc_baseurl+'/book-scbox?slide=1';
              }else{
                $('#scbx_error').html("Unexpected error. Please try again!");
              }
            }else{

            }
            //event.preventDefault();
          },
          error: function(xhr, desc, err) {
            // console.log(err);
          }
        });
     
    }else if(id==5)/*After Payment Optonal part*/
    {      
      var pa_selection_optional = getCookie('pa_selection_optional');
      var objectid = getCookie('scbox_objectid');
      var order_no = $('#order_no').val();
      $.ajax({
          url: sc_baseurl+'scbox/savescbox',
          type: 'post',
          data: {'scbox-confirm' : '1','slide':'5','scbox_gender':$('#scbox_gender').val(), 'body_shape_ans':$('#body_shape_ans').val(), 'scbox_objectid':$('#scbox_objectid').val(), 'scbox_userid':$('#scbox_userid').val(),'scbox_isgift':isGift,'pa_data_optional': pa_selection_optional,'order_no':order_no },
          success: function(data, status) {
             data = data.trim();
              msg(data);

            if(data!=''){
              if(data=='success'){               
                scrolltop();
                window.location.href=sc_baseurl+'/book-scbox/step5';
               
              }else if(data == 'error'){
                //window.location.href=sc_baseurl+'/book-scbox?slide=1';
              }else{
                $('#scbx_error').html("Unexpected error. Please try again!");
              }
            }else{

            }
            //event.preventDefault();
          },
          error: function(xhr, desc, err) {
            // console.log(err);
          }
        });
     
    }  
}

function getcustomprice()
{

  if($('#customboxprice').val()==''){
    $('#customboxprice-error').removeClass('hide');
  }else{
    $('#customboxprice-error').addClass('hide');
    scboxprice = $('#customboxprice').val();
  }

  if($('#customboxprice1').val()==''){
    $('#customboxprice1-error').removeClass('hide');
  }else{
    $('#customboxprice1-error').addClass('hide');
    scboxprice = $('#customboxprice1').val();
  }
   
  $('#scbox-price').val(scboxprice);
}

function validateform(e){
  msg(e); 
  var regexp = /[^a-zA-Z]/g;
  var regexp_mobile = /[^0-9]/g;
  var regex_dob = /^\d{1,2}\-\d{1,2}\-\d{4}$/;
  var isTrue = 0;
  elementid = $(e).attr('id');
  if($('#'+elementid).val()!='')
  {  
      if(elementid=='scbox_name'){
        var namestr = $('#scbox_name').val(); 
        namestr = namestr.trim();   
        namestr = namestr.replace(/\s+/g, " ");   
        var namearr = namestr.split(" ");     
        if(namearr[1]==undefined || namearr[1]=='' ){
          $('#scbox_name').addClass('invalid');
        }else{
           $('#scbox_name').removeClass('invalid'); }         
      }  

      if(elementid=='scbox_pincode'){ 
        if(e.value.length != '' && e.value.length === 6){
          var pincode = $('#'+elementid).val();
          setpincodedata_scbox(pincode,'ship');
          $('#scbox_city').focus();
		    $('#scbox_state').focus();
          $('#scbox_pincode').removeClass('invalid');
          $('#scbox_city').addClass('not-empty'); 
        
        }else{
          $('#scbox_pincode').addClass('invalid');
          $('#scbox_pincode1').addClass('invalid'); }
      }else{           
         $('#'+elementid).removeClass('invalid'); }

      if(elementid=='scbox_billpincode'){ 
        if(e.value.length != '' && e.value.length === 6){
           var pincode = $('#'+elementid).val();
           setpincodedata_scbox(pincode,'bill');
          $('#scbox_billcity').focus();
          $('#scbox_billstate').focus();
          $('#scbox_billpincode').removeClass('invalid');
          $('#scbox_billcity').addClass('not-empty'); 
        }else{
          $('#scbox_billpincode').addClass('invalid'); }
      }else{           
         $('#'+elementid).removeClass('invalid'); }

      if(elementid=='scbox_mobile'){
        if(e.value.length != '' && e.value.length === 10)
        {
          $('#scbox_mobile').removeClass('invalid');
        }else
        {
          $('#scbox_mobile').addClass('invalid');
        }
      }  

      if(elementid=='scbox_emailid'){
        var emailId = $('#scbox_emailid').val();
        var validemail = validateEmail(emailId);
        if(validemail==true)
        {
         $("#scbox_emailid").removeClass('invalid');
          checkUserEmail(emailId);
        }else
        {
           $("#scbox_emailid").addClass('invalid');
        }      
      }
      if(elementid=='day_start' || elementid=='month_start' || elementid=='year_start'){ 
        $('#scbox_age').removeClass('invalid');
      }  
    
  }else{    
     $('#'+elementid).addClass('invalid');
     if(elementid=='day_start' || elementid=='month_start' || elementid=='year_start'){ 
        $('#scbox_age').addClass('invalid');
      } 
  }

  if(elementid=='day_start' || elementid=='month_start' || elementid=='year_start' )
  {
    validatedob('ship');
  }
  if(elementid=='day_billstart' || elementid=='month_billstart' || elementid=='year_billstart' )
  {
    validatedob('bill');
  }   
}

function validatedob(type)
{  
  var dob = '';  
  if(type=='ship')
  {
    var day = $('#day_start').val();
    var month = $('#month_start').val();
    var year = $('#year_start').val();
    dob = year+'-'+month+'-'+day;
    //dob = day+'-'+month+'-'+year;    
    $('#day_start').removeClass('invalid');  
    $('#month_start').removeClass('invalid');   
    $('#year_start').removeClass('invalid');   
    $('#scbox_age').val(dob);     
    return true;
    
  }else if(type=='bill')
  {
    var day = $('#day_billstart').val();
    var month = $('#month_billstart').val();
    var year = $('#year_billstart').val();
    dob = year+'-'+month+'-'+day;
    //dob = day+'-'+month+'-'+year;     
    $('#day_billstart').removeClass('invalid');  
    $('#month_billstart').removeClass('invalid');   
    $('#year_billstart').removeClass('invalid');
    $('#scbox_billage').val(dob);     
    return true;    
  }    
}

function setpincodedata_scbox(pincode,type)
{ 
  $.ajax({
        url: sc_baseurl+'/scbox/getpincodedata',
        type: 'post',
        data: {'scbox_pincode' : pincode },
        success: function(data, status) {
          var obj = jQuery.parseJSON(data);
          //msg(obj);   
				
          if(obj.statename!='' && obj.city!=''){
              if(type=='ship'){
                $('#scbox_state').val(obj.statename);
                $('#scbox_city').val(obj.city);
                $('#scbox_state').removeClass('invalid');
                $('#scbox_city').removeClass('invalid');
              }else if(type='bill'){
                $('#scbox_billstate').val(obj.statename);
                $('#scbox_billcity').val(obj.city);
                $('#scbox_billstate').removeClass('invalid');
                $('#scbox_billcity').removeClass('invalid'); }
          }else if(data===0){    
            if(type=='ship'){      
              $('#scbox_pincode-error').html('Please enter valid pincode');
              $('#scbox_pincode').addClass('invalid');
              $('#scbox_state').val('');
              $('#scbox_city').val('');
            }else if(type=='bill'){
              $('#scbox_billpincode-error').html('Please enter valid pincode');
              $('#scbox_billpincode').addClass('invalid');
              $('#scbox_billstate').val('');
              $('#scbox_billcity').val(''); } }  
        },
        error: function(xhr, desc, err) {
          // console.log(err);
        }
      });
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function validateEmail(sEmail) {
  var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
  if (filter.test(sEmail)) {   
  return true;
  }else {   
    return false;
  }
}

 $(".scbox-payment-options li label").click(function(e){
    if($(this).hasClass("active")){
      payment_mode = $(this).attr("attr-value");
      $('#sc_cart_pay_mode').val(payment_mode);
       $('#cart_place_order_msg').addClass('hide');
    }
  });

function scboxchange_qty(type,id){
   //var scbox_quantity = 1;
   var scbox_prdprice =  $('#scbox_price').val();
   var scbox_subtotal =  $('#scbox_price').val();
   var checkout_taxes = 0;

  if(type == 'min'){
    var proQty = $('#quantity'+id).val();
    if(proQty > 1){
      $('#quantity'+id).val(proQty-1);
    }
  }else{
    var proQty = $('#quantity'+id).val();
    if(proQty < 5){
      $('#quantity'+id).val(parseInt(proQty)+1);      
    }
  }
  scbox_quantity = parseFloat($('#quantity'+id).val());
  scbox_subtotal = parseFloat(scbox_quantity)*parseFloat(scbox_prdprice);
  checkout_taxes = (parseFloat(scbox_taxes)*parseInt(scbox_subtotal))/100; 
  scbox_total =  scbox_subtotal + checkout_taxes;  
  $('#checkout_price').html('<i class="fa fa-inr"></i> '+parseFloat(scbox_subtotal).toFixed(2));
  $('#checkout_subtotal').html('<i class="fa fa-inr"></i> '+parseFloat(scbox_subtotal).toFixed(2));
  checkout_taxes = Math.round(checkout_taxes * 100) / 100;
  $('#checkout_taxes').html('<i class="fa fa-inr"></i> '+checkout_taxes);
  scbox_total = Math.round(scbox_total * 100) / 100;
  $('#checkout_total').html('<i class="fa fa-inr"></i> '+scbox_total);
  $('#cart_total').val(scbox_total);
  $('#scbox_price').val(boxprice);    
  $('#checkout_price').html('<i class="fa fa-inr"></i> '+boxprice); 
 // allow_cart_update();
}

function checkUserEmail(useremail)
{
    if(useremail!='')
    {
     $.ajax({
                type: "post",
                url: sc_baseurl+"scbox/checkuserdata",
                data: { useremail:useremail },
                cache :true,
                async: true,
                success: function (response) {
                     response = response.trim();
                    if(response==1)
                    {                      
                      $('#scboxgender-female').trigger("click");                 
                    }else if(response==2)
                    {                     
                       $('#scboxgender-male').trigger("click");                     
                    }               
                },
                error: function (response) {
                  hide_cart_loader();                  
                    msg(response);
                }
            });
    }
}


$('#scbox_shipaddress').keypress(function (e) {      
      var regex = new RegExp("^[a-zA-Z0-9\\-\\s\\b\/\\,\(\)]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
          return true;
      }

      e.preventDefault();
      return false;
  });

  $('#scbox_name').keypress(function (e) {      
      var regex = new RegExp("^[a-zA-Z\\s\\b]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
          return true;
      }

      e.preventDefault();
      return false;
  });

  function validateName(scboxname)
  {
    var regex = /[^a-zA-Z\s]/g;
    if(scboxname.match(regex))
    {      
      return true;
    }else
    {     
      return false;
    }
  }


  function getScboxSizeChart(sizetype)
  {
    var scbox_gender = $('#scbox_gender').val();
    if(scbox_gender!='' && sizetype!='')
    {
        $.ajax({
                type: "post",
                url: sc_baseurl+"/scbox_new/getgenderSizeGuide",
                data: { 'scbox_gender':scbox_gender,'size_type':sizetype},
                cache :true,
                async: true,
                success: function (response) {
                    
                    if(response!="")
                    { 
                      $('#size_guide_img').attr("src",response);                 
                    }else 
                    {

                    }               
                },
                error: function (response) {
                  hide_cart_loader();                  
                    msg(response);
                }
            });
    }    
  }

function selecttab(type)
  { 
    //$('.tab-find').attr('id',type); 
    if(type=='women'){
      $('#'+type).addClass('active');
      $('#men').removeClass('active');
    }else{
       $('#'+type).addClass('active');
       $('#women').removeClass('active');
    }     
    document.cookie="scbox-gender="+type+";path=/";
  }

  function place_cartscboxnew_order(){  
    show_cart_loader();
    var billing_first_name = '';
    var billing_last_name = '';  var error = '';    
    $(".scbox-payment-options li label").each(function(e){
      if($(this).hasClass("active")){
        payment_mode = $(this).attr("attr-value");
        $('#sc_cart_pay_mode').val(payment_mode);
         $('#cart_place_order_msg').addClass('hide');
      }
    });
    
    var pay_mod = payment_mode;
    var codna = $('#codna').val();
    var shina = $('#shina').val();
    var stock_exist = $('#stock_exist').val(); 
    var page_type = $('#page-type').val(); 
    var regexp = /[^a-zA-Z\.]/g;
    var regexp_mobile = /[^0-9]/g;
   
    if(getCookie('discountcoupon_stylecracker')=='BMS15' || getCookie('discountcoupon_stylecracker')=='BMS500' || getCookie('discountcoupon_stylecracker')=='YES300' || getCookie('discountcoupon_stylecracker')=='YESBOX500' || getCookie('discountcoupon_stylecracker')=='YESBOX750' || getCookie('discountcoupon_stylecracker')=='YESBOX1000' || getCookie('scfr')=='yb'){
            $("#cod").addClass("state-disabled"); 
          }else{
            $("#cod").removeClass("state-disabled");  
          }
 
    var billing_first_name = $('#billing_first_name').val().trim();
    var billing_last_name = $('#billing_last_name').val().trim();
    var billing_mobile_no = $('#billing_mobile_no').val().trim();
    var billing_pincode_no = $('#billing_pincode_no').val().trim();
    var billing_address = $('#billing_address').val().trim();
    var billing_city = $('#billing_city').val().trim();
    var billing_state = $('#billing_state').val();
    billing_address = billing_address.replace(/[^a-zA-Z0-9\\-\\s\\b\/\\,\(\)]/gi, ' ');
    //billing_address = billing_address.replace(/\s+/g, " ");
    var cart_total = $('#cart_total').val();
    var scbox_price = $('#scbox_price').val();
    document.cookie = "scbox_cartotal="+cart_total+";path=/";
    var isTrue = 0;

    if(billing_first_name.match(regexp) || billing_first_name==''){
      error = 'Enter Valid First Name';
      $('#cart_place_order_msg').html('Enter Valid First Name');
      isTrue = 1;
    }else{
      document.cookie = "billing_first_name="+billing_first_name+";path=/";
      $('#cart_place_order_msg').html('');
      //isTrue = 0;
    }

    if(billing_last_name.match(regexp) || billing_last_name==''){
      error = 'Enter Valid Last Name';
      $('#cart_place_order_msg').html('Enter Valid Last Name');
      isTrue = 1;
    }else{
      document.cookie = "billing_last_name="+billing_last_name+";path=/";
      $('#cart_place_order_msg').html('');
      //isTrue = 0;
    }

    if(billing_mobile_no.match(regexp_mobile) || billing_mobile_no=='' || billing_mobile_no.length < 10){
      error = 'Enter Valid Mobile No';
      $('#cart_place_order_msg').html('Enter Valid Mobile No');
      isTrue = 1;
    }else{
      document.cookie = "billing_mobile_no="+billing_mobile_no+";path=/";
      $('#cart_place_order_msg').html('');
      //isTrue = 0;
    }

    if(billing_pincode_no.match(regexp_mobile) || billing_pincode_no=='' || billing_pincode_no.length < 6){
      error = 'Enter Valid Pincode';
      $('#cart_place_order_msg').html('Enter Valid Pincode');
      isTrue = 1;
    }else{
      document.cookie = "stylecracker_shipping_pincode="+billing_pincode_no+";path=/";
      $('#cart_place_order_msg').html('');
      //isTrue = 0;
    }

    /*if(billing_address==''){
      error = 'Enter Shipping Address';
      $('#cart_place_order_msg').html('Enter Shipping Address');
      isTrue = 1;
    }else{

      document.cookie = "billing_address="+billing_address+";path=/";
      $('#cart_place_order_msg').html('');
      //isTrue = 0;
    }*/

    if(billing_city==''){
      error = 'Enter Valid Pincode';
      $('#cart_place_order_msg').html('Enter Shipping City');
      isTrue = 1;
    }else{
      document.cookie = "billing_city="+billing_city+";path=/";
      $('#cart_place_order_msg').html('');
      //isTrue = 0;
    }

    if(billing_state==''){
      error = 'Enter Shipping State';
      $('#cart_place_order_msg').html('Enter Shipping State');
      isTrue = 1;
    }else{
      document.cookie = "billing_state="+billing_state+";path=/";
      $('#cart_place_order_msg').html('');
      //isTrue = 0;
    }

    if(isTrue == 1){ 
      hide_cart_loader();
      $('#cart_place_order_msg').html('Please enter shipping address').removeClass('hide');
      $('#cart_place_order_msg').addClass('message error');
      $("#place_order_sc_cart").attr('disabled',true);  
    }else if(stock_exist > 0){ 
      hide_cart_loader();
      $('#cart_place_order_msg').html('Some of the products are out of stock').removeClass('hide');
      $("#place_order_sc_cart").attr('disabled',true);

    }else if(pay_mod !='cod' && pay_mod != 'card'){
      hide_cart_loader();
      $('#cart_place_order_msg').html('Select payment option').removeClass('hide');
      $("#place_order_sc_cart").attr('disabled',true);

    }else if(pay_mod =='cod' && codna>0){ 
      hide_cart_loader();
      /*$("input[type='radio'][id='sc_cart_pay_mode_cod']").prop('checked',false);
      $("input[type='radio'][id='sc_cart_pay_mode_online']").prop('checked',true);*/
      
      if(getCookie('discountcoupon_stylecracker') == 'NOCASH500' || getCookie('discountcoupon_stylecracker') == 'NOCASH1000'){ 
        
        $("#place_order_sc_cart").attr('disabled',true);      
        
        $('#cart_place_order_msg').html('COD is not avaliable for applied coupon code').removeClass('hide');
        
        }else{
        $('#cart_place_order_msg').html('COD is not available for some products').removeClass('hide');
      }
    }else if(shina > 0){
      hide_cart_loader();
      $('#cart_place_order_msg').html('Shipping is not available for some products').removeClass('hide');
      $("#place_order_sc_cart").attr('disabled',true);  
    }else{  

      var shipping_first_name = billing_first_name;
      var shipping_last_name = billing_last_name;
      var shipping_mobile_no = billing_mobile_no;
      var shipping_pincode_no = billing_pincode_no;
      var shipping_address = billing_address;
      var shipping_city = billing_city;
      var shipping_state = billing_state;
      var cart_email = $('#cart_email').val().trim(); 
      var cart_total = $('#cart_total').val();
      var scbox_objectid = $('#scbox_objectid').val();
      var scbox_objectid = lastparam.split('?')[0];
      var cart_total = $('#cart_total').val();
      var scbox_price = getCookie('scbox_price');
      var scbox_objectid = getCookie('scbox_objectid');
      var scbox_quantity = $('#scbox_quantity').val();
            
      /*msg(is_order_placed+'==billing_first_name=='+billing_first_name+'==billing_last_name=='+billing_last_name+'==billing_mobile_no== '+billing_mobile_no+'==billing_pincode_no=='+billing_pincode_no+'==billing_address=='+billing_address+'==billing_city=='+billing_city+'==billing_state=='+billing_state+'==shipping_first_name=='+shipping_first_name+'==shipping_last_name=='+shipping_last_name+'==shipping_mobile_no=='+shipping_mobile_no+'==shipping_pincode_no=='+shipping_pincode_no+'==shipping_address=='+shipping_address+'==shipping_city=='+shipping_city+'==shipping_state=='+shipping_state+'==cart_email=='+cart_email+'==scbox_price=='+scbox_price);*/
      /*alert(is_order_placed+'==billing_first_name=='+billing_first_name+'==billing_last_name=='+billing_last_name+'==billing_mobile_no== '+billing_mobile_no+'==billing_pincode_no=='+billing_pincode_no+'==billing_address=='+billing_address+'==billing_city=='+billing_city+'==billing_state=='+billing_state+'==shipping_first_name=='+shipping_first_name+'==shipping_last_name=='+shipping_last_name+'==shipping_mobile_no=='+shipping_mobile_no+'==shipping_pincode_no=='+shipping_pincode_no+'==shipping_address=='+shipping_address+'==shipping_city=='+shipping_city+'==shipping_state=='+shipping_state+'==cart_email=='+cart_email+'==scbox_price=='+scbox_price)*/

      if(is_order_placed==false &&billing_first_name!='' && billing_last_name!='' && billing_mobile_no!='' && billing_pincode_no!='' && billing_address!='' && billing_city!='' && billing_state!='' && shipping_first_name!='' && shipping_last_name!='' && shipping_mobile_no!='' && shipping_pincode_no!='' && shipping_address!='' && shipping_city!='' && shipping_state!='' && cart_email!='' && scbox_price!=''){

      ga('send', 'event', 'SCBOX','clicked','Step3b-Order Place Request');
       fbq('track', 'AddPaymentInfo', '');
        msg("before: is_order_placed: "+is_order_placed);
        is_order_placed = true;
        if(pay_mod == 'cod'){   
          $("#page-loader").show();

          $.ajax({
                  type: "post",
                  url: sc_baseurl+"/scbox_new/place_order",
                  data: { billing_first_name:billing_first_name,billing_last_name:billing_last_name,billing_mobile_no:billing_mobile_no,billing_pincode_no:billing_pincode_no,billing_address:billing_address,billing_city:billing_city,billing_state:billing_state,shipping_first_name:shipping_first_name,shipping_last_name:shipping_last_name,shipping_mobile_no:shipping_mobile_no,shipping_pincode_no:shipping_pincode_no,shipping_address:shipping_address,shipping_city:shipping_city,shipping_state:shipping_state,cart_email:cart_email,pay_mod:pay_mod,cart_total:cart_total,scbox_price:scbox_price,scbox_objectid:scbox_objectid,scbox_quantity:scbox_quantity,page_type:page_type},
                  cache :true,
                  async: true,
                  success: function (response) {
                    // $("#page-loader").hide();  
                      hide_cart_loader(); remove_address_cookies('SHIP'); remove_address_cookies('BILL');
                      if(response!='')
                      { 
                        var final_cookies = '';
                                         
                       // var redirction = sc_baseurl+'book-scbox/'+scbox_objectid+'?order='+response+'&step=6';
                        
                            if(getCookie('stylist_call')=='yes' && getCookie('scbox_isgift')=='true'){

                                var redirction = sc_baseurl+'/scbox-thankyou?success=1';
								deleteAllCookies();
                                document.cookie="billing_address="+final_cookies+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";      
                            }else{
                               var redirction = sc_baseurl+'/book-scbox/step4?order='+response;
                                document.cookie="billing_address="+final_cookies+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";     
                            }   
                         $('#order-message').removeClass('hide');
                      }else
                      {
                         //var redirction = sc_baseurl+'/book-scbox/step1';  
                        //var redirction = sc_baseurl+'/book-scbox/'+scbox_objectid+'?order='+response+'&step=6';  
                        // $('#order-message').html('Error');
                        // $('#order-message').removeClass('hide');
                      }
                     // var redirction = sc_baseurl+'/scbox/ordersuccessOrder/'+response;
                      
                      // var final_cookies = '';
                      // document.cookie="SCUniqueID="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
                      // document.cookie="discountcoupon_stylecracker="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
                 
                  window.location =redirction;
                  money_formatter(".price");
                  money_formatter(".cart-price");
                  },
                  error: function (response) {
                    hide_cart_loader();                    
                      msg('error=='+response);
                  }
              });

        }else{            
       
           $("#page-loader").show(); 
          $.ajax({
                  type: "post",
                  url: sc_baseurl+"/scbox_new/place_online_order",
                  data: { billing_first_name:billing_first_name,billing_last_name:billing_last_name,billing_mobile_no:billing_mobile_no,billing_pincode_no:billing_pincode_no,billing_address:billing_address,billing_city:billing_city,billing_state:billing_state,shipping_first_name:shipping_first_name,shipping_last_name:shipping_last_name,shipping_mobile_no:shipping_mobile_no,shipping_pincode_no:shipping_pincode_no,shipping_address:shipping_address,shipping_city:shipping_city,shipping_state:shipping_state,cart_email:cart_email,pay_mod:pay_mod,cart_total:cart_total,scbox_price:scbox_price,scbox_objectid:scbox_objectid,scbox_quantity:scbox_quantity, page_type:page_type  },
                  cache :true,
                  async: true,
                  success: function (response) { 
                     //$("#page-loader").hide();  
                      //hide_cart_loader();                     
                      var obj = jQuery.parseJSON(response);                     
                      $('#txnid').val(obj.txnid);
                      $('#hash').val(obj.payu_hash);
                      $('#amount').val(obj.amount);
                      $('#firstname').val(obj.firstname);
                      $('#email').val(obj.email);
                      $('#phone').val(obj.phoneno);
                      $('#udf1').val(getCookie('SCUniqueID'));
                      $('#udf2').val(obj.address_sc);
                      $('#udf3').val(billing_pincode_no);
                      $('#udf4').val(billing_city);
                      $('#udf5').val(obj.state);
                      $('#offer_key').val(obj.offerkey);
                      var stylecracker_style = document.forms.stylecracker_style;
                      stylecracker_style.submit();
                      money_formatter(".price");
                      money_formatter(".cart-price");
                      document.cookie="scfr="+''+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
                      },
                      error: function (response) {
                        hide_cart_loader();                  
                          msg('error=='+response);
                  }
              });
        }
      }
    }
    hide_cart_loader();  
}

function deleteAllCookies() {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
    document.cookie="pa_selection="+''+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
    document.cookie="pa_selection_optional="+''+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
    document.cookie="scbox_cartotal="+''+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
    document.cookie="category_selection="+''+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
}



