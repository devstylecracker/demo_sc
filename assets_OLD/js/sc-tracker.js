/*
 * SC - User Track Functions
 * Author - Arun Arya
 * Date : 21-July-2015
 * */
//## Variables  ========================================================
var timeStartTrack = 2000;
var timeThowTrackRecord = 10000; //or window.close
var intCurrentPID = 0 ;
var intPageVisited  = 1;

var objUserTrackRecord;
var intPageScroll = 0;
var intTimeSpentMillSec = 0;
var intStoreCtr=0;
var arrUserTrackRecord = [];
//##  Init function ====================================================
function msg(m) { console.log(m); }
function InitializeTrack () {
	//## Reset Timer value ---------------------------------------------
	intTimeSpentMillSec = 0;	
	setTimeout(function(e){
		//## User Scroll Track -----------------------------------------
		TrackUserScroll();
		
		//## For getting PID -------------------------------------------
		intCurrentPID = Number(getCookie('pid'));
		
		//## Time Spend on each page -----------------------------------
		setInterval(function(){ intTimeSpentMillSec++; },1);
		
	},timeStartTrack);
	
	//## Window Scroll func --------------------------------------------
	$(window).scroll(TrackUserScroll);
	
	//## Storing Data on local storage after certain time --------------
		/*setInterval(function(e){
		
	},timeThowTrackRecord);*/
	
	$('.navbar-nav li a, .logo-img.sc-logo, .team-single a').on('click touch', function(e){
		ThrowUserTrackRecord();
		GetInfo();
	});
	//msg("sc-tracker init func....");
}

//##  Get Page ID from cookies =========================================
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

//##  Track User Scroll (px) function ==================================
function TrackUserScroll() {
	intPageScroll = $(window).scrollTop();
}

function ThrowUserTrackRecord() {
	//msg("Storing Data on local storage... "+ intCurrentPID);
	//##
	StorePageCount();
}

//## Local Storage Page ID - 1 =========================================
function StorePageCount(){
	//
	var objPageVisited = JSON.parse(sessionStorage.getItem('page_visited_'+intCurrentPID));
	if(objPageVisited==null){
		intPageVisited = 1
		sessionStorage.setItem('page_visited_'+intCurrentPID,JSON.stringify({
			p_visited: intPageVisited,
			p_id:intCurrentPID
		}));
	} else {
		intPageVisited = parseInt(objPageVisited.p_visited);
		intPageVisited = intPageVisited+1;
		sessionStorage.setItem('page_visited_'+intCurrentPID,JSON.stringify({
			p_visited: intPageVisited,
			p_id:intCurrentPID
		}));
	}

	//## Getting the page visited counter to create new object
	StorePageInfo();

}

//## Storing the page information on session storage ===================
function StorePageInfo(){
	sessionStorage.setItem('page_info_'+intCurrentPID, JSON.stringify({
		p_visited: intPageVisited,
		p_id:intCurrentPID,
		page_scroll:intPageScroll,
		time_spent:intTimeSpentMillSec
	}));
}

//## To show a data as per relevent page id ============================
function GetInfo() {
        var getUrl = window.location;
        var baseUrl = getUrl .protocol + "//" + getUrl.host ;
	var objStored = JSON.parse(sessionStorage.getItem('page_visited_'+intCurrentPID));
	var objPageInfo = JSON.parse(sessionStorage.getItem('page_info_'+intCurrentPID));
	//var getUrl = window.location;
        //var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
	//objPageInfo = JSON.stringify(objPageInfo);
	var p_visited = objPageInfo.p_visited;
	var p_id = objPageInfo.p_id;
	var page_scroll = objPageInfo.page_scroll;
	var time_spent = objPageInfo.time_spent;
	//var gac = new gaCookies();
	//user_id =  gac.getUniqueId();
        user_id =  0;
	$.ajax({
	  url:  baseUrl+"/analytics/track_user_activity",
	  type: 'POST',
	  data: {'type' : 'track_user_activity','p_visited':p_visited,'user_id':user_id, 'p_id':p_id, 'page_scroll':page_scroll, 'time_spent':time_spent},
	  cache :true,
	success: function(response) {
	
	},
	error: function(xhr) {
	
	}
	});
}

/*window.onbeforeunload = function(e){
    var msg = 'Are you sure?';
    e = e || window.event;

    if(e)
        e.returnValue = msg;

    return msg;
}*/