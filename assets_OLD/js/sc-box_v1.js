/*********************************************************

 * SCBox JS

 * User On-boarding

 * @author StyleCracker Tech

 *********************************************************/

function msg(m) {

    console.log(m);

}


//## Currently added for testing, this should set dynamically as per user selection (need to comment later) @HARDCODE

//setCookie('scbox-gender', 'men', 365);

 //setCookie('scbox-gender', 'women', 365);

 setCookie('scbox_objectid', 2 , 365);

//## WOMEN PACKAGE MATIX =================================

var arrCurrBoxType = [];

var w_arr2999 = [

    ["apparel", "bags", "accessories"],

    ["apparel", "bags", "jewellery"],

    ["apparel", "bags", "beauty"],

    // ["apparel", "footwear", "accessories"],

    // ["apparel", "footwear", "jewellery"],

    // ["apparel", "footwear", "beauty"],

    ["bags", "accessories", "jewellery"],

    ["bags", "accessories", "beauty"],

    ["bags", "jewellery", "beauty"],



    // ["footwear", "beauty", "accessories"],

    // ["footwear", "beauty", "jewellery"],



    // ["footwear", "jewellery", "accessories"],

];

var w_arr4999 = [

    ["apparel", "bags", "accessories", "jewellery"],

    ["apparel", "bags", "accessories", "beauty"],

    ["apparel", "bags", "jewellery", "beauty"],



    ["apparel", "footwear", "accessories", "jewellery"],

    ["apparel", "footwear", "accessories", "beauty"],

    ["apparel", "footwear", "jewellery", "beauty"],



    ["bags", "footwear", "accessories", "jewellery"],

    ["bags", "footwear", "accessories", "beauty"],

    ["bags", "footwear", "jewellery", "beauty"]

];

var w_arr6999 = [

    ["apparel", "bags", "footwear", "accessories", "jewellery"],

    ["apparel", "bags", "footwear", "jewellery", "beauty"],

    ["apparel", "bags", "footwear", "accessories", "beauty"],

    ["apparel", "bags", "accessories", "jewellery", "beauty"],



    ["apparel", "footwear", "accessories", "jewellery", "beauty"],



    ["bags", "footwear", "accessories", "jewellery", "beauty"],

];



var m_arr2999 = [

    ["apparel", "bags", "accessories"],

    ["apparel", "bags", "grooming"],

    ["apparel", "accessories", "grooming"],



    // ["apparel", "footwear", "accessories"],

    // ["apparel", "footwear", "grooming"],



    // ["bags", "footwear", "accessories"],

    // ["bags", "footwear", "grooming"],



    ["bags", "accessories", "grooming"],



   // ["footwear", "accessories", "grooming"],

];



var m_arr4999 = [

    ["apparel", "bags", "accessories", "grooming"],



    ["apparel", "footwear", "accessories", "grooming"],



    ["bags", "footwear", "accessories", "grooming"],

];



var m_arr6999 = [

    ["apparel", "bags", "footwear", "accessories", "grooming"],

];





//## GET BOX PACKAGE ID (MEN & WOMEN)===================

var strGenderSelected;

var strBoxPackage;

var isGift = false;

function GET_BoxPackageID() {
//scbox_isgift
    isGift = getCookie('scbox_isgift');
    if(isGift=='true'){
        strGenderSelected = getCookie('scbox-billgender');
    } else {
        strGenderSelected = getCookie('scbox-gender');
    }

    strBoxPackage = getCookie('scbox_objectid');
msg('isGift: '+isGift);
msg("scbox-billgender: " +getCookie('scbox-billgender'));
msg("scbox-gender: " +getCookie('scbox-gender'));
msg('strGenderSelected661: '+strGenderSelected);
    
    SET_GenderAndBoxPackage(strGenderSelected, strBoxPackage);

}



//## CONSTRAINTS FOR BOX TYPE ----------------------------

//var arrLocalPackageType = [276, 275, 274, 277, 281, 282, 283];

var arrLocalPackageType = [1,2,3,4,5,6,7]

var arrBoxType = arrLocalPackageType;



//## SET THE GENDER AND BOX PACKAGE 

function SET_GenderAndBoxPackage(strGenderSelected, strBoxPackage) {

    msg('strGenderSelected: ' + strGenderSelected + '  :strBoxPackage :' + strBoxPackage);

    if (strGenderSelected == 'women') {

        if (strBoxPackage == arrBoxType[0]) {

            msg('Women 2999');
            $('#catsel').html('(Choose 3)');
            arrCurrBoxType = w_arr2999;


        } else if (strBoxPackage == arrBoxType[1]) {

            msg('Women 4999');
            $('#catsel').html('(Choose 4)');
            arrCurrBoxType = w_arr4999;

        } else if (strBoxPackage == arrBoxType[2]) {

            msg('Women 6999');
            $('#catsel').html('(Choose 5)');
            arrCurrBoxType = w_arr6999;

        } else if (strBoxPackage == arrBoxType[3]) {

            msg('Women Customer');
            $('#catsel').html('(Choose any)');
            arrCurrBoxType = 'custom';

        }



    } else if (strGenderSelected == 'men') {

        if (strBoxPackage == arrBoxType[0]) {

            msg('Men 2999');
            $('#catsel').html('(Choose 3)');
            arrCurrBoxType = m_arr2999;

        } else if (strBoxPackage == arrBoxType[1]) {

            msg('Men 4999');
            $('#catsel').html('(Choose 4)');
            arrCurrBoxType = m_arr4999;

        } else if (strBoxPackage == arrBoxType[2]) {

            msg('Men 6999');
            $('#catsel').html('(Choose 5)');
            arrCurrBoxType = m_arr6999;

        } else if (strBoxPackage == arrBoxType[3]) {
            $('#catsel').html('(Choose any)');
            msg('Men Customer');

        }

    }

}









/**********************************************************

//## ALL EVENT REGISTERED FOR CHECKBOX, RADIO, COMBO, DIMAOND BUTTONS & UL-LIs

/**********************************************************/

//== Generic function for Check box =======================

$("input:checkbox").on("click touch", function() {

    var objSelector = $(this).closest(".sc-checkbox");

    if (!objSelector.hasClass("active") && !objSelector.hasClass("state-disabled")) {

        $(this).closest("label").addClass("active");

        //== Common function triggers globally

        CheckWhichEvent($(this).closest("ul").attr("type"), $(this));

    } else if (!objSelector.hasClass("state-disabled")) {

        $(this).closest("label").removeClass("active");

        //== Common function triggers globally

        CheckWhichEvent($(this).closest("ul").attr("type"), $(this));

    }

});

//== Generic function for Radio box =======================

$("input:radio").click(function() {

    CheckWhichEvent($(this).closest(".scbox-container").attr("type"), $(this));

});



//== Generic function for Combo box =======================

$("select").on("change", function() {

    //CheckWhichEvent($(this).closest(".scbox-container").attr("type"),$(this));

    CheckWhichEvent($(this).attr("type"), $(this));

});



//== Generic function for UL LIST =======================

$("ul.add-event li").on("click touch", function() {

    var thisObj = $(this);

    if (!thisObj.closest('ul').hasClass('other-type')) {

        if (!$(this).hasClass('active')) {

            $(this).addClass('active');

        } else {

            $(this).removeClass('active');

        }

    }

    CheckWhichEvent($(this).closest("ul").attr("type"), $(this));

});

//== Generic function for Dimaond Button Event =======================

$(".btn-diamond.btn-size").on("click touch", function() {

    var thisObj = $(this);

    var thisObjContainer = $(this).closest(".box-container");

    thisObjContainer.find("span").each(function() {

        $(this).removeClass('active');

    });

    thisObj.addClass('active');

    CheckWhichEvent($(this).closest("div").attr("type"), $(this), thisObj);

});



//== General Trigger Functions =======================   

function CheckWhichEvent(eventType, obj) {

    if (eventType == "boxType") {

        SCBoxType(obj);

    } else if (eventType == "pa-women") {

        SCBoxCategorySelection(obj);

    } else if (eventType == "userInfo") {

        SCBoxUserInfo(obj);

    } else if (eventType == "topSize") {

        SCBoxSizeInfo(obj);

    } else if (eventType == "stuff-dont-need") {

        GetStuffDontNeed(obj);

    } else if (eventType == "height-feet") {

        GET_MyHeight(obj);

    } else if (eventType == "height-inches") {

        GET_MyHeightInches(obj);

    } else if (eventType == "size-top") {

        GET_MySizeTop(obj);

    } else if (eventType == "fit-you-like") {

        GET_FitYouLike(obj);

    } else if (eventType == "dress_size") {

        GET_DressSize(obj);

    } else if (eventType == "fit-like-dress") {

        GET_FitLikeDress(obj);

    } else if (eventType == "trouser-size") {

        GET_TrouserSize(obj);

    } else if (eventType == "jeans-size") {

        GET_JeansSize(obj);

    } else if (eventType == "bottom-fit-like") {

        GET_BottomFitLike(obj);

    } else if (eventType == "band-size") {

        GET_BandSize(obj);

    } else if (eventType == "cup-size") {

        GET_CupSize(obj);

    } else if (eventType == "skirt-dress-size") {

        GET_SkirtDressSize(obj);

    } else if (eventType == "color-avoid") {

        GET_ColorAvoid(obj);

    } else if (eventType == "print-avoid") {

        GetPrintAvoid(obj);

    } else if (eventType == "bag-type") {

        GET_BagType(obj);

    } else if (eventType == "footwear-type") {

        GET_FootwearType(obj);

    } else if (eventType == "section-footwear-width") {

        GET_SectionFootwearWidth(obj);

    } else if (eventType == "jewellery-type") {

        GET_JewelleryTypes(obj);

    } else if (eventType == "footwear-size") {

        GET_FootwearSize(obj);

    } else if (eventType == "accessories-selected") {

        GET_AccessoriesSelected(obj);

    } else if (eventType == "section-beauty-type-looking-for") {

        GET_BeautySelected(obj);

    } else if (eventType == "dos-and-donts") {

        GET_DoAndDonts(obj);

    } else if (eventType == "style-preference") {

        GET_StylePreference(obj);

    } else if (eventType == "section-beauty-shade") {

        GET_SectionBeautyShade(obj);

    } else if (eventType == "forme") {

        //Added by Sudha

        GET_forme(obj);

    } else if (eventType == "section-bag-i-want-for") {

        GET_SectionBagIWantFor(obj);

    } else if (eventType == "accessories-refelector-types") {

        GET_AcessoriesRefelectorType(obj);

    } else if (eventType == "beauty-finishing-type") {

        GET_BeautyFinishingType(obj);

    } else if (eventType == "bag-size") {

        GET_BagSize(obj);

    } else if (eventType == "footwear-heel-height") {

        GET_FootwearHeelHeight(obj);

    } else if (eventType == "footwear-heel-type") {

        GET_FootwearHeelType(obj);

    } else if (eventType == "accessories-sunglasses-types") {

        GET_AccessoriesSunglassesTypes(obj);

    } else if (eventType == "accessories-belt-types") {

        GET_AccessoriesBeltTypes(obj);

    } else if (eventType == "jewellery-tone") {

        GET_JewelleryTones(obj);

    } else if (eventType == "bag-color-avoid") {

        GET_BagColorAvoid(obj);

    } else if (eventType == "bag-print-avoid") {

        GetBagPrintAvoid(obj);

    }



    //MEN

    else if (eventType == "size-tshirt") {

        GET_MySizeTshirt(obj);

    } else if (eventType == "fit-you-like-men") {

        GET_FitYouLikeMen(obj);

    } else if (eventType == "size-shirt") {

        GET_MySizeShirt(obj);

    } else if (eventType == "fit-you-like-men-s") {

        GET_FitYouLikeMenS(obj);

    } else if (eventType == "fit-you-like-men-t") {

        GET_FitYouLikeMenT(obj);

    } else if (eventType == "fit-you-like-men-j") {

        GET_FitYouLikeMenJ(obj);

    } else if (eventType == "pa-men") {

        SCBoxCategorySelection(obj);

    } else if (eventType == "stuff-dont-need-men") {

        GetStuffDontNeedMen(obj);

    } else if (eventType == "print-avoid-men") {

        GetPrintAvoidMen(obj);

    }



    //TriggerAllTime();

}





/**********************************************************

//## ALL FUNCTIONS AFTER TRIGGERS ON CHECKBOX, RADIO, COMBO, DIMAOND BUTTONS & UL-LIs

/**********************************************************/



//## Varibale Declariation ==========================

var strCurrGender, strCurrBoxType, strCurrSelectedCategory;

//var arrCurrBoxType = [];

var isCategorySelect = false;



var arrRemainingCategories = [];

var arrShuffledCategories = [];



//== 0 RESET ALL function ============================

function ResetAll() {

    strCurrBoxType = "";

    arrCurrBoxType = [];

}



//== 1. BOX TYPE function ============================ 

//@@HARDCODE

//strCurrGender = 'women';

//arrCurrBoxType = w_arr2999;

/*

function SCBoxType(obj) {

    var currentObj = $(obj);

    strCurrBoxType = currentObj.val();

    if (strCurrBoxType == "2999") {

        arrCurrBoxType = w_arr2999;

    } else if (strCurrBoxType == "4999") {

        arrCurrBoxType = w_arr4999;

    } else if (strCurrBoxType == "6999") {

        arrCurrBoxType = w_arr6999;

    }

}

*/



/**********************************************************

//## STEP 1: CATEGORY SELECTION ENABLE/DISABLE FUNCTIONS

/**********************************************************/



//== 2. Category Selection function =====================

//var arrFilteredCategorySet = [];

var arrCategorySelectedByUser = [];

var currentObj, currentObjContain, currentObjValue;



function SCBoxCategorySelection(obj) {

    currentObj = $(obj);

    currentObjContain = currentObj.closest(".scbox-container");

    arrShuffledCategories = [];

    intCtr = 0;

    msg('strBoxPackage: ' + strBoxPackage);

    if (strBoxPackage == arrBoxType[3]) {

        isPAStep1 = true;

        ShowHideCategoryContainers();

    } else {

        GetAllCategorySets();

    }

}



//== 2.1 Get all Array set as per category selection

var intCtr = 0;

var strSelectedValue;



function GetAllCategorySets(currentObjValue) {

    isCategorySelect = false;

    currentObjContain.find(".sc-checkbox").each(function(e) {

        if ($(this).hasClass("active")) {

            currentObjValue = $(this).find("span").text();

            if (!isCategorySelect) {

                GetCategorySet(arrCurrBoxType, currentObjValue);

                isCategorySelect = true;

            } else {

                GetCategorySet(arrFiltered, currentObjValue);

            }

        }

    });

}



//== 2.2 fetching the combination set of arrays as per user selection

var arrFiltered = [];



function GetCategorySet(arrCategory, currentObjValue) {

    arrFiltered = [];

    //pushing all the category set as per user selection

    $.each(arrCategory, function(i, filterName) {

        $.each(arrCategory[i], function(j, filterName) {

            if (arrCategory[i][j] == currentObjValue) {

                arrShuffledCategories.push(arrCategory[i]);

                arrFiltered.push(arrCategory[i]);

            }

        });

    });

    //Convert multi-deminsion array to single array

    ChangeToSingleArray(arrFiltered);

}



//== 2.3 Change the multi set of arrays to single array

function ChangeToSingleArray(arr) {

    var arrCombinationCategories = [];

    $.each(arr, function(i, value) {

        arrCombinationCategories = arrCombinationCategories.concat(arr[i]);

    });

    //msg("Filter-1 for array =================");

    //msg(arrCombinationCategories); 

    //== remove duplicated values from an array

    arrCombinationCategories = arrCombinationCategories.filter(function(item, index, inputArray) {

        return inputArray.indexOf(item) == index;

    });

    //msg("Filter-1 for array =================");

    //msg(arrCombinationCategories);

    var i = 0;

    var arrSelectedCategories = [];

    arrUserCategorySelected = [];

    currentObjContain.find(".sc-checkbox").each(function(e) {

        if ($(this).hasClass("active")) {

            var removeItem = $(this).find("span").text();

            //store the user selected values from filtered category array

            arrUserCategorySelected.push(removeItem);

            //remove the user selected values from filtered category array

            arrCombinationCategories = $.grep(arrCombinationCategories, function(value) {

                return value != removeItem;

            });

        }

    });

    //msg("Filter-3 for array =================");

    //msg(arrCombinationCategories);

    EnableDisableCheckbox(arrCombinationCategories);

}

//== 2.4 Enable/Disable category check boxes as per matix

function EnableDisableCheckbox(arr) {

    currentObjContain.find(".sc-checkbox").each(function(e) {

        if (!$(this).hasClass("active")) {

            $(this).addClass("state-disabled");

        }

    });



    var containCategory = currentObjContain.find(".sc-checkbox");

    $.each(arr, function(i, arrValue) {

        $.each(containCategory, function(j, eleValue) {

            if (arrValue == $(eleValue).find("span").text()) {

                //msg($(eleValue));

                $(eleValue).removeClass("state-disabled");

            }

        });

    });

    //## announcing that step 1 is completed

    if (arr.length == 0) {

        isPAStep1 = true;

        $('.category-selected').find('.message.error').addClass('hide');

        ShowHideCategoryContainers();

    } else {

        isPAStep1 = false;

    }

}









//## SHOW/HIDE CATEGORY CONTAINERS AS PER SELECTION ===================

var isApparelSelected = false;



function ShowHideCategoryContainers() {

    arrCategorySelectedByUser = [];

    isApparelSelected = false;

    $.each($('.section-category-selected ul li'), function(i, e) {

        if ($(e).find('label').hasClass('active')) {

            var strCategory = $(e).find('span').text().toLowerCase();
            msg('strCategory: '+strCategory);
            arrCategorySelectedByUser.push(strCategory);

        }

    });

    $('.category-apparel,.category-bags,.category-footwear,.category-accessories,.category-jewellery,.category-beauty,.category-grooming').addClass('hide');

    $.each(arrCategorySelectedByUser, function(i, e) {

        if (e == 'apparel') {

            $('.category-apparel').removeClass('hide');

            isApparelSelected = true;

        } else if (e == 'bags') {

            $('.category-bags').removeClass('hide');

            if (strBoxPackage == arrBoxType[0]) {

                $('ul.bag-type li').eq(2).hide();

                $('ul.bag-type li').eq(3).hide();

                $('ul.bag-type li').eq(4).hide();

            } else {

                $('ul.bag-type li').eq(2).show();

                $('ul.bag-type li').eq(3).show();

                $('ul.bag-type li').eq(4).show();

            }

        } else if (e == 'footwear') {

            $('.category-footwear').removeClass('hide');

            if (strBoxPackage == arrBoxType[0]) {

                $('ul.footwear-type li').eq(2).hide();

                $('ul.footwear-type li').eq(3).hide();

                $('ul.footwear-type li').eq(4).hide();

            } else {

                $('ul.footwear-type li').eq(2).show();

                $('ul.footwear-type li').eq(3).show();

                $('ul.footwear-type li').eq(4).show();

            }

        } else if (e == 'accessories') {

            $('.category-accessories').removeClass('hide');

            if (strBoxPackage == arrBoxType[0]) {

                $('ul.accessories-selected li').eq(4).hide();

                $('ul.accessories-selected li').eq(6).hide();



            } else {

                $('ul.accessories-selected li').eq(4).show();

                $('ul.accessories-selected li').eq(6).show();



            }

        } else if (e == 'jewellery') {

            $('.category-jewellery').removeClass('hide');

        } else if (e == 'beauty') {

            $('.category-beauty').removeClass('hide');

        } else if (e == 'grooming') {

            $('.category-grooming').removeClass('hide');

        }

    });

    //msg('arr: ' + arrCategorySelectedByUser);

}

/*

//== 2.7 CURRENTLY NOT IN USE ====================== (DONT DELETE)

function RemoveSelectedCategories(arr) {

    $.each(arr, function(i, value) {

        if (value == strCurrSelectedCategory) {

            arr.splice(i, 1);

        }

    });

    return arr;

}





//== 3.0 UserInfo Save function ======================

function SCBoxUserInfo(obj) {

    var currentObj = $(obj);

    msg(currentObj.val());

}







function TriggerAllTime() {

    //msg("strCurrBoxType: "+strCurrBoxType);

}



//== DATA SAVE STRUCTURE ==========================

var arrUserCategorySelected = [];







$(document).ready(function() {

    msg("LOADDDDDDDDING...");

});

*/



//## CHANGE STEP FUNCTION  ==============================

var isStep1, isStep2, isStep3, isStep4 = false;



function ChangeStep(intScreen, strButton) {

    msg("intScreen: " + intScreen + "  :: strButton: " + strButton);

    //## STEP 1: WOMEN: Category Selection Page -----------------------------

    if (intScreen == 2 && strButton == 'next') {

        if (isPAStep1) {

            //##Save category selection on cookies

            setCookie('category_selection', arrCategorySelectedByUser, 365);
            msg('arrCategorySelectedByUser: '+arrCategorySelectedByUser);

            $('.category-selected').find('.message.error').addClass('hide');

            if (isApparelSelected == false) {                
                SwitchStep(intScreen + 1);

            } else {         
                SwitchStep(intScreen);
            }
            ScboxFormSubmit('2a');
            ga('send', 'event', 'SCBOX','clicked','Step2a-Category Selected');
            fbq('track', 'InitiateCheckout', arrCategorySelectedByUser);
        } else {

            $('.category-selected').find('.message.error').removeClass('hide');

        }

    }



    //## STEP 2: WOMEN: Appreal Selection Page -----------------------------

    if (intScreen == 1 && strButton == 'previous') {

        SwitchStep(intScreen);

    }

    if (intScreen == 3 && strButton == 'next') {

        msg('isPAStep2: ' + isPAStep2);

        if (!isPAStep2) {

            CheckQuestionCompleted();

            $.each(arrQuestionNotCompleted, function(i, obj) {

                $('#' + obj).find('.message.error').removeClass('hide');

            });

        } else {

            $('.message.error').addClass('hide');
            var objectid = getCookie('scbox_objectid');
            var myJSONBP = JSON.stringify(jsonObjBeforePayment);
            setCookie('pa_selection', myJSONBP, 365);
            SwitchStep(intScreen);
            ScboxFormSubmit('3');
             ga('send', 'event', 'SCBOX','clicked','Step2b-PA Info Apparel Before Payment');

        }

    }



    //## STEP 3: WOMEN: BAG,FOOTWEAR,ACCESSORIES,BEAUTY,JEWELLERY Selection Page ----------------------------- 

    if (intScreen == 2 && strButton == 'previous') {

        !isApparelSelected ? SwitchStep(1) : SwitchStep(intScreen);

        msg("SCREEN 2, Previous");

    }

    if (intScreen == 4 && strButton == 'next') {

        msg('isPAStep3' + isPAStep3);

        if (!isPAStep3) {

            CheckQuestionCompleted();

            $.each(arrQuestionNotCompleted, function(i, obj) {

                $('#' + obj).find('.message.error').removeClass('hide');

            })

        } else {

            //SwitchStep(intScreen);

            msg('DONE..3333333');

            var objectid = getCookie('scbox_objectid');

            var myJSONBP = JSON.stringify(jsonObjBeforePayment);

            setCookie('pa_selection', myJSONBP, 365);

            ScboxFormSubmit(2);

            ga('send', 'event', 'SCBOX','clicked','Step2b-PA Info Before Payment');

        }



    }

    /* if (intScreen == 4) {

         

          var objectid = getCookie('scbox_objectid');

          var myJSONBP = JSON.stringify(jsonObjBeforePayment);

          setCookie('pa_selection', myJSONBP, 365); 

          ScboxFormSubmit(2);

        

     }   */

    if (intScreen == 5) {

        //window.location.href = "http://www.scnest.com/book-scbox/step5";

        //Added By Sudha

        var objectid = getCookie('scbox_objectid');

        var myJSONAP = JSON.stringify(jsonObjAfterPayment);

        setCookie('pa_selection_optional', myJSONAP, 365);

        ScboxFormSubmit(5);

        ga('send', 'event', 'SCBOX','clicked','Step4-PA Info After Payment');

        //window.location.href = sc_baseurl+"book-scbox/step5";

        //End Added By Sudha

    }



    if (intScreen == 33) {

        var is_valid = validateStep(1);

        if (is_valid) {

            ScboxFormSubmit(1);

            ga('send', 'event', 'SCBOX','clicked','Step1-User Details');

            // window.location.href = sc_baseurl+"book-scbox/step2";

        }

    }

    if (intScreen == 6) {

        //complete();

         ga("send", "event", "SCBOX","clicked","Step5-Upload Profile");

        window.location.href = sc_baseurl + "scbox-thankyou?success=1";

    }

}



function SwitchStep(int) {

    $("#scbox-form > div").each(function(e) {

        $(this).addClass("hide");

    });

    $("#slide_" + int).removeClass("hide");

    scrolltop();

    //

    isCurrentStep = int;

}



/**********************************************************

//## STEP 2: APPREAL CATEGORY FUNCTIONALITY

/**********************************************************/



//## GLOBAL PARAM -------------------------------

var currentGender;

//currentGender = "women";



/********************************

// PA - BODYSHAPE FUNCTION

/********************************/



//## Appreal: Add body shape function -----------

var strBodyShape;

var objBodyShapeContainer;

$(".pa-container.women > div, .pa-container.men > div").on("click touch", function() {

    ResetBodyShape();

    GetBodyShape($(this));

});



//## GetBodyShape function ----------------------

function GetBodyShape(obj) {

    ResetBodyShape();

    obj.removeClass('deactive').addClass('active');

    obj.find(".img-wrp > img").first().addClass("hide");

    obj.find(".img-wrp > img").last().removeClass("hide");

    strBodyShape = obj.find(".title").attr("answer_id");

    //@@

    //UpdateJSON(strBodyShape);

    //@@

    UpdateJSONBeforePayment(strBodyShape);

}



//## SetBodyShape function ----------------------

function SetBodyShape(str) {

    var obj;

    ResetBodyShape();

    objBodyShapeContainer.each(function() {

        if ($(this).find(".title").attr("answer_id") == str) {

            obj = $(this);

            obj.removeClass('deactive').addClass('active');

            obj.find(".img-wrp > img").first().addClass("hide");

            obj.find(".img-wrp > img").last().removeClass("hide");

        }

    });



}

//## ResetBodyShape function (women and men)-----

function ResetBodyShape() {

    $(".pa-container.women > div, .pa-container.men > div").each(function(e) {

        $(this).removeClass('active');

        $(this).addClass('deactive');

        $(this).find(".img-wrp > img").first().removeClass("hide");

        $(this).find(".img-wrp > img").last().addClass("hide");

    });

    if (currentGender == "women") {

        objBodyShapeContainer = $(".pa-container.women > div");

    } else {

        objBodyShapeContainer = $(".pa-container.men > div");

    }

}



/********************************

// APPAREL: STUFF DONT NEED

/********************************/

var intStuff = 0;

var intMaxLimit_StuffDoneNeed;

/*if (strGenderSelected == 'women') {

    intMaxLimit_StuffDoneNeed = 4;

} else {

    intMaxLimit_StuffDoneNeed = 7;

}*/

var arrStuffDontNeed = [];

var arrGetStuffDontNeed = [];

//## GetStuffDontNeed CLICK/TOUCH EVENT function ---

function GetStuffDontNeed(obj) {

    if (strGenderSelected == 'women') {

        intMaxLimit_StuffDoneNeed = 4;

    } else {

        intMaxLimit_StuffDoneNeed = 7;

    }

    //arrStuffDontNeed = [];

    var obj = $(obj);

    if (intStuff < intMaxLimit_StuffDoneNeed) {

        if (!obj.hasClass("active")) {

            obj.addClass("active");

            intStuff++;

        } else {

            obj.removeClass("active");

            intStuff--;

        }

    } else if (intStuff == intMaxLimit_StuffDoneNeed) {

        if (obj.hasClass("active")) {

            obj.removeClass("active");

            intStuff--;

        }

    }

    GET_StuffDontNeed('get');

 

}



//## SetStuffDontNeed GETTER ------------------------------------

var arrStuffDoneNeed = [];

var arrGetStuffDontNeed = [];



function GET_StuffDontNeed(type, arr) {

    arrGetStuffDontNeed = []

    $.each($(".stuff-dont-need li"), function(i, value) {

        var thisObj = $(this);

        var thisObjValue = thisObj.find(".title-3").attr("answer_id");

        if (type == "get") {

            if (thisObj.hasClass('active')) {

                //msg('--->'+thisObjValue);
                //arrGetStuffDontNeed.push(thisObjValue);
                 arrGetStuffDontNeed.push({
                    'collection_id':thisObjValue,
                    'answer_value':thisObjValue
                 });
                // msg('===============================');
                 //msg(arrGetStuffDontNeed);

            }

        } else if (type == "reset") {

            thisObj.removeClass("active");

        }

    });

  //##SHOW HIDE QUESTION AS PER SELECTION
    $('.section-top-fit-like-this').addClass('hide');
    $('.section-dress-fit-like-this').addClass('hide');
    $('.section-skirt-fit-like-this').addClass('hide');
    $('.section-bottom-fit-like-this').addClass('hide');
    $.each($(".stuff-dont-need li"), function(i, value) {
        if(!$(this).hasClass('active')){
            var val = $(this).find(".title-3").attr("answer_id");
            if(val=='66' || val=='70' || val=='71'){
                $('.section-top-fit-like-this').removeClass('hide');
            }
            if(val=='67' || val=='71'){
                $('.section-dress-fit-like-this').removeClass('hide');
            }
            if(val=='68' || val=='67'){
                $('.section-skirt-fit-like-this').removeClass('hide');
            }
            if(val=='69' || val=='71'){
                $('.section-bottom-fit-like-this').removeClass('hide');
            }
        }
    });

    //@@

    UpdateJSONBeforePayment(arrGetStuffDontNeed);

}



//## SetStuffDontNeed SETTER ------------------------------------

function SET_StuffDontNeed(arr) {

    $.each($("ul.stuff-dont-need li"), function(j, value) {

        $.each($("ul.stuff-dont-need li"), function(i, value) {

            if ($(this).find(".title-3").attr("answer_id") == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}



//## appreal: MY-HEIGHT function ******************************************************



//## GET_MyHeight function ------------------------------------

var strMyHT_feet;



function GET_MyHeight(obj) {

    var thisObj = $(obj);

    strMyHT_feet = thisObj.val();

    UpdateJSONAfterPayment(strMyHT_feet);

}

//## SET_MyHeight function ------------------------------------

function SET_MyHeight(str) {

    $('select.height_feet').val(str);

}



//## GET_MyHeightInches function ------------------------------------

var strMyHT_inches;



function GET_MyHeightInches(obj) {

    var thisObj = $(obj);

    strMyHT_inches = thisObj.val();

    UpdateJSONAfterPayment(strMyHT_inches);

}

//## SET_MyHeightInches function ------------------------------------

function SET_MyHeightInches(str) {

    $('select.height_inches').val(str);

}



//## GET_MySizeTop function ------------------------------------

var strMySizeTop;
var arrMySizeTop = [];



function GET_MySizeTop(obj) {

    var thisObj = $(obj);

    if (thisObj.hasClass('size-top')) {

        //strMySizeTop = thisObj.val();

        arrMySizeTop.push({
                    'collection_id':thisObj.val(),
                    'answer_value':thisObj.val()
        });




    }

    UpdateJSONBeforePayment(arrMySizeTop);

}



//## SET_MyHeight function ------------------------------------

function SET_MySizeTop(str) {

    $('select.size-top').val(str);

}





//##Men MEn MEn MEn===============================



//## GET_MySizeTshirt function ------------------------------------

var strMySizeTshirt;



function GET_MySizeTshirt(obj) {

    var thisObj = $(obj);

    if (thisObj.hasClass('size-tshirt')) {

        strMySizeTshirt = thisObj.val();

    }

    //msg(strMySizeTshirt);

    //@@ Update jSON -------

    UpdateJSONBeforePayment(strMySizeTshirt);

}



//## SET_MySizeTshirt function ------------------------------------

//strMySizeTshirt = 'l/40-42';

//SET_MySizeTshirt(strMySizeTshirt);



function SET_MySizeTshirt(str) {

    $('select.size-tshirt').val(str);

}



//## GetFitYouLike CLICK/TOUCH EVENT function ----------------

var arrFitYouLikeMen = [];

//GETTER

function GET_FitYouLikeMen(type) {

    arrFitYouLikeMen = [];

    $.each($("ul.fit-you-like-men li"), function(i, value) {

        if ($(this).hasClass('active')) {

            arrFitYouLikeMen.push($(this).find(".title-3").attr('answer_id'));

            arrFitYouLikeMen.push({
                    'collection_id':thisObj.val(),
                    'answer_value':thisObj.val()
                });

        }

    });

    //msg(arrFitYouLikeMen)

    //@@ Update jSON -------

    UpdateJSONBeforePayment(arrFitYouLikeMen);

}



//SETTER  

//arrFitYouLikeMen = ["SLIM"];

//SET_FitYouLikeMen(arrFitYouLikeMen)



function SET_FitYouLikeMen(arr) {

    $.each($("ul.fit-you-like-men li"), function(j, value) {

        $.each($(".fit-you-like-men li"), function(i, value) {

            if ($(this).find(".title-3").attr('answer_id') == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}



//## GET_MySizeshirt function ------------------------------------

var strMySizeShirt;



function GET_MySizeShirt(obj) {

    var thisObj = $(obj);

    if (thisObj.hasClass('size-shirt')) {

        strMySizeShirt = thisObj.val();

    }

    //msg(strMySizeShirt);

    //@@ Update jSON -------

    UpdateJSONBeforePayment(strMySizeShirt);

}



//## SET_MySizeShirt function ------------------------------------

//strMySizeShirt = 'XS/CHEST 34-36 / NECK 15”';

//SET_MySizeShirt(strMySizeShirt);



function SET_MySizeShirt(str) {

    $('select.size-shirt').val(str);

}



//## GetFitYouLike CLICK/TOUCH EVENT function ----------------

var arrFitYouLikeMenS = [];

//GETTER

function GET_FitYouLikeMenS(type) {

    arrFitYouLikeMenS = [];

    $.each($("ul.fit-you-like-men-s li"), function(i, value) {

        if ($(this).hasClass('active')) {

            arrFitYouLikeMenS.push($(this).find(".title-3").attr('answer_id'));

        }

    });

    //msg(arrFitYouLikeMenS)

    //@@ Update jSON -------

    UpdateJSONBeforePayment(arrFitYouLikeMenS);

}



//SETTER  

//arrFitYouLikeMenS = ["REGULAR"];

//SET_FitYouLikeMenS(arrFitYouLikeMenS)



function SET_FitYouLikeMenS(arr) {

    $.each($("ul.fit-you-like-men-s li"), function(j, value) {

        $.each($(".fit-you-like-men-s li"), function(i, value) {

            if ($(this).find(".title-3").attr('answer_id') == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}





//## GetFitYouLike CLICK/TOUCH EVENT function ----------------

var arrFitYouLikeMenT = [];

//GETTER

function GET_FitYouLikeMenT(type) {

    arrFitYouLikeMenT = [];

    $.each($("ul.fit-you-like-men-t li"), function(i, value) {

        if ($(this).hasClass('active')) {

            arrFitYouLikeMenT.push($(this).find(".title-3").attr('answer_id'));

        }

    });

    // msg(arrFitYouLikeMenT)

    //@@ Update jSON -------

    UpdateJSONBeforePayment(arrFitYouLikeMenT);

}



//SETTER  

//arrFitYouLikeMenT = ["SKINNY"];

//SET_FitYouLikeMenT(arrFitYouLikeMenT)



function SET_FitYouLikeMenT(arr) {

    $.each($("ul.fit-you-like-men-t li"), function(j, value) {

        $.each($(".fit-you-like-men-t li"), function(i, value) {

            if ($(this).find(".title-3").attr('answer_id') == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}



//## GetFitYouLike CLICK/TOUCH EVENT function ----------------

var arrFitYouLikeMenJ = [];

//GETTER

function GET_FitYouLikeMenJ(type) {

    arrFitYouLikeMenJ = [];

    $.each($("ul.fit-you-like-men-j li"), function(i, value) {

        if ($(this).hasClass('active')) {

            arrFitYouLikeMenJ.push($(this).find(".title-3").attr('answer_id'));

        }

    });

    //msg(arrFitYouLikeMenJ)

    //@@ Update jSON -------

    UpdateJSONBeforePayment(arrFitYouLikeMenJ);

}



//SETTER  

//arrFitYouLikeMenJ = ["SLIM"];

//SET_FitYouLikeMenJ(arrFitYouLikeMenJ)



function SET_FitYouLikeMenJ(arr) {

    $.each($("ul.fit-you-like-men-j li"), function(j, value) {

        $.each($(".fit-you-like-men-j li"), function(i, value) {

            if ($(this).find(".title-3").attr('answer_id') == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}



//# MEn====================================

//## GETTER GetFitYouLike CLICK/TOUCH EVENT function ----------------

var arrFitYouLike = [];



function GET_FitYouLike(type) {

    arrFitYouLike = [];

    $.each($("ul.fit-you-like li"), function(i, value) {

        var thisObj = $(this);

        if ($(this).hasClass('active')) {



        var thisObjValue = thisObj.find(".title-3").attr("answer_id");


            //arrFitYouLike.push($(this).find(".title-3").attr('answer_id'));

             arrFitYouLike.push({
                    'collection_id':thisObjValue,
                    'answer_value':thisObjValue
                 });

        }

    });

    UpdateJSONBeforePayment(arrFitYouLike);

}



//SETTER  

function SET_FitYouLike(arr) {

    $.each($("ul.fit-you-like li"), function(j, value) {

        $.each($(".fit-you-like li"), function(i, value) {

            if ($(this).find(".title-3").attr('answer_id') == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}



//## GET_Dress function ------------------------------------

var strDressSize;

var arrDressSize = [];



function GET_DressSize(obj) {

     var thisObj = $(obj);

     if (thisObj.hasClass('dress_size')) {

        //strDressSize = $(obj).val();

         arrDressSize.push({
                    'collection_id':thisObj.val(),
                    'answer_value':thisObj.val()
        });


    }

    UpdateJSONBeforePayment(arrDressSize);

}


//## SET_Dress function ------------------------------------

function SET_DressSize(str) {

    $('select.dress_size').val(str);

}



//## FIT-LIKE-DRESS function -----------------------------



//GETTER

var arrFitLikeDress = [];



function GET_FitLikeDress(obj) {

    arrFitLikeDress = [];

    $.each($("ul.fit-like-dress li"), function(i, value) {

         var thisObj = $(this);
        
        if ($(this).hasClass('active')) {


        var thisObjValue = thisObj.find(".title-3").attr("answer_id");

            //arrFitLikeDress.push($(this).find(".title-3").attr("answer_id"));

             arrFitLikeDress.push({
                    'collection_id':thisObjValue,
                    'answer_value':thisObjValue
                 });

        }

    });

    UpdateJSONBeforePayment(arrFitLikeDress);

}

//SETTER

function SET_FitLikeDress(arr) {

    $.each($("ul.fit-like-dress li"), function(j, value) {

        $.each($(".fit-like-dress li"), function(i, value) {

            if ($(this).find(".title-3").attr("answer_id") == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}



//## TrouserSize function ------------------------------------

//GETTER
var strTrouserSize;

var arrTrouserSize = [];



function GET_TrouserSize(obj) {

    var thisObj = $(obj);

     if (thisObj.hasClass('trouser-size')){

        //strTrouserSize = $(obj).val();
        arrTrouserSize.push({
                    'collection_id':thisObj.val(),
                    'answer_value':thisObj.val()
        });

    }

    //@@ Update jSON -------

    UpdateJSONBeforePayment(arrTrouserSize);

}

//SETTER

function SET_TrouserSize(str) {

    $('select.trouser-size').val(str);

}

//## TrouserSize function ------------------------------------

//GETTER

var strJeansSize;

var arrJeansSize = [];


function GET_JeansSize(obj) {

    var thisObj = $(obj);

     if (thisObj.hasClass('jeans-size')){

        //strJeansSize = $(obj).val();

        arrJeansSize.push({
                    'collection_id':thisObj.val(),
                    'answer_value':thisObj.val()
        });

    }

    UpdateJSONBeforePayment(arrJeansSize);

}

//SETTER

function SET_JeansSize(str) {

    $('select.jeans-size').val(str);

}

//## BOTTOM-FIT-LIKE function ------------------------------------

//GETTER

var arrBottomFitLike = [];



function GET_BottomFitLike(obj) {

    arrBottomFitLike = [];

    $.each($("ul.bottom-fit-like li"), function(i, value) {

        var thisObj = $(this);
        
        if ($(this).hasClass('active')) {


        var thisObjValue = thisObj.find(".title-3").attr("answer_id");

           // arrBottomFitLike.push($(this).find(".title-3").attr("answer_id"));
           arrBottomFitLike.push({
                    'collection_id':thisObjValue,
                    'answer_value':thisObjValue
        });

        }

    });

    UpdateJSONBeforePayment(arrBottomFitLike);

}



//SETTER

function SET_BottomFitLike(arr) {

    $.each($("ul.bottom-fit-like li"), function(j, value) {

        $.each($("ul.bottom-fit-like li"), function(i, value) {

            if ($(this).find(".title-3").attr("answer_id") == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}

//## BAND-SIZE function ------------------------------------

//GETTER

var strBandSize;

var arrBandSize = [];


function GET_BandSize(obj) {

     var thisObj = $(obj);

     if (thisObj.hasClass('band-size')){

        //strBandSize = $(obj).val();
        arrBandSize.push({
                    'collection_id':thisObj.val(),
                    'answer_value':thisObj.val()
        });

    }

    UpdateJSONBeforePayment(arrBandSize);

}

//SETTER

function SET_BandSize(str) {

    $('select.band-size').val(str);

}

//## CUP-SIZE function ------------------------------------

//GETTER

var strCupSize;

var arrCupSize = [];



function GET_CupSize(obj) {

    var thisObj = $(obj);

     if (thisObj.hasClass('cup-size')){

        //strCupSize = $(obj).val();
         arrCupSize.push({
                    'collection_id':thisObj.val(),
                    'answer_value':thisObj.val()
        });

    }

    UpdateJSONBeforePayment(arrCupSize);

}

//SETTER

function SET_CupSize(str) {

    $('select.cup-size').val(str);

}



//## SKIRT-DRESS-FIT function ------------------------------------

//GETTER

var arrSkirtDressSize = [];



function GET_SkirtDressSize(obj) {

    arrSkirtDressSize = [];

    $.each($("ul.skirt-dress-size li"), function(i, value) {

         var thisObj = $(this);
        
        if ($(this).hasClass('active')) {


        var thisObjValue = thisObj.find(".title-3").attr("answer_id");

            //arrSkirtDressSize.push($(this).find(".title-3").attr("answer_id"));
            arrSkirtDressSize.push({
                    'collection_id':thisObjValue,
                    'answer_value':thisObjValue
        });

        }

    });

    UpdateJSONBeforePayment(arrSkirtDressSize);

}



//SETTER

function SET_SkirtDressSize(arr) {

    $.each($("ul.skirt-dress-size li"), function(j, value) {

        $.each($("ul.skirt-dress-size li"), function(i, value) {

            if ($(this).find(".title-3").attr("answer_id") == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}



//## COLOR-AVOID function ------------------------------------

//GETTER

var arrColorAvoid = [];



function GET_ColorAvoid(obj) {

    arrColorAvoid = [];

    $.each($("ul.color-avoid li"), function(i, value) {

        var thisObj = $(this);
        
        if ($(this).hasClass('active')) {

            //arrColorAvoid.push($(this).find('div').attr('answer_id'));
             var thisObjValue = thisObj.find('div').attr("answer_id");

              arrColorAvoid.push({
                    'collection_id':thisObjValue,
                    'answer_value':thisObjValue
        });

        }

    });

    UpdateJSONBeforePayment(arrColorAvoid);

}



//SETTER

function SET_ColorAvoid(arr) {

    $.each($("ul.color-avoid li"), function(j, value) {

        $.each($("ul.color-avoid li"), function(i, value) {

            if ($(this).find('div').attr('answer_id') == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}



//## GetStuffDontNeed CLICK/TOUCH EVENT function ----------------

var intPrintAvoid = 0;

var intMaxLimit_PrintAvoid = 4;

var arrPrintAvoid = [];



function GetPrintAvoid(obj) {

    arrPrintAvoid = [];

    var obj = $(obj);

    if (intPrintAvoid < intMaxLimit_PrintAvoid) {

        if (!obj.hasClass("active")) {

            obj.addClass("active");

            intPrintAvoid++;

        } else {

            obj.removeClass("active");

            intPrintAvoid--;

        }

    } else if (intPrintAvoid == intMaxLimit_PrintAvoid) {

        if (obj.hasClass("active")) {

            obj.removeClass("active");

            intPrintAvoid--;

        }

    }

    GET_GetPrintAvoid('get');

}



//## GetPrintAvoid GETTER ------------------------------------

function GET_GetPrintAvoid(type, arr) {

    $.each($("ul.print-avoid li"), function(i, value) {


        var thisObj = $(this);

        var thisObjValue = thisObj.find(".title-3").attr('answer_id');

        if (type == "get") {

            if (thisObj.hasClass('active')) {

                arrPrintAvoid.push({
                    'collection_id':thisObjValue,
                    'answer_value':thisObjValue
                 });

                //arrPrintAvoid.push(thisObjValue);

            }

        } else if (type == "reset") {

            thisObj.removeClass("active");

        }

    });

    UpdateJSONBeforePayment(arrPrintAvoid);

}

//## GetPrintAvoid SETTER ------------------------------------

function SET_GetPrintAvoid(arr) {

    $.each($("ul.print-avoid li"), function(j, value) {

        $.each($("ul.print-avoid li"), function(i, value) {

            if ($(this).find(".title-3").attr("answer_id") == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}





//##MEN=====================================================



//## GetStuffDontNeed CLICK/TOUCH EVENT function ----------------

var intPrintAvoid = 0;

var intMaxLimit_PrintAvoid = 6;

var arrPrintAvoidMen = [];



function GetPrintAvoidMen(obj) {

    arrPrintAvoidMen = [];

    var obj = $(obj);

    if (intPrintAvoid < intMaxLimit_PrintAvoid) {

        if (!obj.hasClass("active")) {

            obj.addClass("active");

            intPrintAvoid++;

        } else {

            obj.removeClass("active");

            intPrintAvoid--;

        }

    } else if (intPrintAvoid == intMaxLimit_PrintAvoid) {

        if (obj.hasClass("active")) {

            obj.removeClass("active");

            intPrintAvoid--;

        }

    }

    GET_GetPrintAvoidMen('get');

}



//## GetPrintAvoid GETTER ------------------------------------

function GET_GetPrintAvoidMen(type, arr) {

    $.each($("ul.print-avoid-men li"), function(i, value) {

        var thisObj = $(this);

        var thisObjValue = thisObj.find(".title-3").attr('answer_id');

        if (type == "get") {

            if (thisObj.hasClass('active')) {

                arrPrintAvoidMen.push(thisObjValue);

            }

        } else if (type == "reset") {

            thisObj.removeClass("active");

        }

    });

    UpdateJSONBeforePayment(arrPrintAvoidMen);

}

//## GetPrintAvoid SETTER ------------------------------------

function SET_GetPrintAvoidMen(arr) {

    $.each($("ul.print-avoid-men li"), function(j, value) {

        $.each($("ul.print-avoid-men li"), function(i, value) {

            if ($(this).find(".title-3").attr('answer_id') == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}



//Men=============================================================

//## BAG-TYPE function ------------------------------------

//GETTER

var arrBagType = [];



function GET_BagType(obj) {

    arrBagType = [];

    $.each($("ul.bag-type li"), function(i, value) {

        var thisObj = $(this);


        if ($(this).hasClass('active')) {

            //arrBagType.push($(this).find(".title-3").attr('answer_id'));

            var thisObjValue = thisObj.find(".title-3").attr('answer_id');

            arrBagType.push({
                    'collection_id':thisObjValue,
                    'answer_value':thisObjValue
                 });

        }

    });

    UpdateJSONBeforePayment(arrBagType);

}



//SETTER

function SET_BagType(arr) {

    $.each($("ul.bag-type li"), function(j, value) {

        $.each($("ul.bag-type li"), function(i, value) {

            if ($(this).find(".title-3").attr('answer_id') == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}



//## FOOTWEAR-TYPE function ------------------------------------

//GETTER

var arrFootwearType = [];



function GET_FootwearType(obj) {

    arrFootwearType = [];

    $.each($("ul.footwear-type li"), function(i, value) {

            var thisObj = $(this);

        if ($(this).hasClass('active')) {

            var thisObjValue = thisObj.find(".title-3").attr('answer_id');
            //arrFootwearType.push($(this).find(".title-3").attr('answer_id'));
             arrFootwearType.push({
                    'collection_id':thisObjValue,
                    'answer_value':thisObjValue
                 });

        }

    });

    UpdateJSONBeforePayment(arrFootwearType);

}



//SETTER

function SET_FootwearType(arr) {

    $.each($("ul.footwear-type li"), function(j, value) {

        $.each($("ul.footwear-type li"), function(i, value) {

            if ($(this).find(".title-3").attr('answer_id') == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}



//## GETTER - FOOTWEAR WIDTH function -----------------------------

var strSectionFootwearWidth;



function GET_SectionFootwearWidth(obj) {

    $.each($("ul.section-footwear-width li label"), function(i, value) {

        $(this).removeClass('active');

    });

    $(obj).find('label').addClass('active')

    strSectionFootwearWidth = $(obj).find('span').attr('answer_id');

    UpdateJSONBeforePayment(strSectionFootwearWidth);

}

//SETTER

function SET_SectionFootwearWidth(obj) {

    $.each($("ul.section-footwear-width li label"), function(i, value) {

        $(this).removeClass('active');

        if ($(this).find('span').attr('answer_id') == strSectionFootwearWidth) {

            $(this).trigger('click');

        }

    });

}

//## JEWELLERY LOOKING function ------------------------------------

//GETTER

var arrJewelleryTypes = [];



function GET_JewelleryTypes(obj) {

    arrJewelleryTypes = [];

    $.each($("ul.jewellery-type li"), function(i, value) {

        if ($(this).hasClass('active')) {

            arrJewelleryTypes.push($(this).find(".title-3").attr('answer_id'));

        }

    });

    UpdateJSONBeforePayment(arrJewelleryTypes);

}



//SETTER

function SET_JewelleryTypes(arr) {

    $.each($("ul.jewellery-type li"), function(j, value) {

        $.each($("ul.jewellery-type li"), function(i, value) {

            if ($(this).find(".title-3").attr('answer_id') == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}



//## FOOTWEAR-SIZE function ------------------------------------

//GETTER


var strFootwearSize;
var arrFootwearSize = [];


function GET_FootwearSize(obj) {

    var thisObj = $(obj);

    if ($(obj).hasClass('footwear-size')) {

        //strFootwearSize = $(obj).val();

        arrFootwearSize.push({
                    'collection_id':thisObj.val(),
                    'answer_value':thisObj.val()
        });


    }

    UpdateJSONBeforePayment(arrFootwearSize);

}

//SETTER

function SET_FootwearSize(str) {

    $('select.footwear-size').val(str);

}

//## GETTER - ACCESSORIES function -----------------------------

var strAccessorySelected;
var arrAccessorySelected = [];


function GET_AccessoriesSelected(obj) {

    $.each($("ul.accessories-selected li label"), function(i, value) {

        $(this).removeClass('active');

    });

    $(obj).closest('label').addClass('active')

    var strAccessorySelect = $(obj).closest('label').find('span').attr('answer_id');

    arrAccessorySelected.push({
                    'collection_id':strAccessorySelect,
                    'answer_value':strAccessorySelect
        });


    UpdateJSONBeforePayment(arrAccessorySelected);

}

//SETTER

function SET_AccessoriesSelected(obj) {

    $.each($("ul.accessories-selected li label"), function(i, value) {

        $(this).removeClass('active');

        if ($(this).find('span').attr('answer_id') == strAccessorySelected) {

            $(this).addClass('active');

        }

    });

}





var strGroomingProduct;

//## BEAUTY TYPE function -----------------------------

var strBeautySelected;

//GETTER

function GET_BeautySelected(obj) {

    msg(obj);

    $.each($("ul.section-beauty-type-looking-for li label"), function(i, value) {

        $(this).removeClass('active');

    });

    $(obj).closest('label').addClass('active');

    strBeautySelected = $(obj).closest('label').find('span').attr("answer_id");

    ShowHideBeautyShade(strBeautySelected);

    UpdateJSONBeforePayment(strBeautySelected);

}



//SETTER

function SET_BeautySelected(obj) {

    $.each($("ul.section-beauty-type-looking-for li label"), function(i, value) {

        $(this).removeClass('active');

        if ($(this).find('span').attr('answer_id') == strBeautySelected) {

            //$(this).trigger('click');

            $(this).addClass('active');

            ShowHideBeautyShade(strBeautySelected);

        }

    });

}



function ShowHideBeautyShade(str) {

    /*if (str == 'lipstick' || str == 'nail paint') {

        $('.container-beauty-shades').removeClass('hide');

    } else {

        $('.container-beauty-shades').addClass('hide');

    }*/

}



//## BEAUTY TYPE function -----------------------------

var strSectionBeautyShade;

//GETTER

function GET_SectionBeautyShade(obj) {

    $.each($("ul.section-beauty-shade li label"), function(i, value) {

        $(this).removeClass('active');

    });

    $(obj).closest('label').addClass('active');

    strSectionBeautyShade = $(obj).closest('label').find('span').attr('answer_id');

    UpdateJSONBeforePayment(strSectionBeautyShade);

}

//SETTER

function SET_SectionBeautyShade(obj) {

    $.each($("ul.section-beauty-shade li label"), function(i, value) {

        $(this).removeClass('active');

        if ($(this).find('span').attr('answer_id') == strSectionBeautyShade) {

            //$(this).trigger('click');

            $(this).addClass('active');

        }

    });

}

//Added by Sudha

function GET_forme(obj) {

    var thisContainer = $(obj).closest('ul').find('li');

    var strCurrentValue = '';

    var strCurrentStyle = '';

    $.each(thisContainer, function(i, value) {

        $(this).find('label').removeClass('active');

    });

    $(obj).find('label').addClass('active');

    strCurrentValue = $(obj).find('span').text();

    strCurrentStyle = $(obj).closest('ul').attr('style');

    //ADD_Forme(strCurrentStyle,strCurrentValue);

}

//End Added by Sudha

//## GETTER - DOs-AND-DONTs function -----------------------------

//var arrDosAndDonts = [];

var arrDosAndDonts = [

    ['sleeveless', '135' ,''],

    ['halter','136' ,''],

    ['strappy', '137',''],

    ['off shoulder', '138',''],

    ['tube', '354',''],

    ['crop top', '139',''],

    ['backless', '140',''],

    ['deep neck', '141',''],

    ['round neck', ''],

    ['v neck', ''],

    ['henleys', ''],

    ['collared', ''],

    ['half sleeves', ''],

    ['full sleeves', ''],

    ['jackets', '']

];



function GET_DoAndDonts(obj) {

    var thisContainer = $(obj).closest('ul').find('li');

    var strCurrentValue = '';

    var strCurrentStyle = '';

    $.each(thisContainer, function(i, value) {

        $(this).find('label').removeClass('active');

    });

    $(obj).find('label').addClass('active');

    strCurrentValue = $(obj).find('span').text();

    strCurrentStyle = $(obj).closest('ul').attr('style');

    ADD_DosAndDonts(strCurrentStyle, strCurrentValue);

}



function ADD_DosAndDonts(strStyle, strValue) {

    $.each(arrDosAndDonts, function(i, value) {

        if (arrDosAndDonts[i][0] == strStyle) {

            arrDosAndDonts[i][2] = strValue;

            //## checking func CheckQuestionCompleted ---
/*
            if (strGenderSelected == 'women') {

                question_1_13 = true;

            } else if (strGenderSelected == 'men') {

                question_1_11 = true;

            }*/

        }

    });

    CheckAllDosAndDontsDone();

    UpdateJSONBeforePayment(arrDosAndDonts);

}

function CheckAllDosAndDontsDone(){
    var intCtr = 0;
    var intTotalElementCtr =0;
    $('.section-dos-and-donts ul.dos-and-donts li').each(function(i,obj){
        if($(obj).find('label.sc-checkbox').hasClass('active')){
            intCtr++;  
        }
        intTotalElementCtr++;
    }); 
    intTotalElementCtr = intTotalElementCtr/2;
    if(intCtr==intTotalElementCtr){
        msg('DONE');
        if (strGenderSelected == 'women') {
            question_1_13 = true;
        } else if (strGenderSelected == 'men') {
            question_1_11 = true;
        }
    } else {
        msg('NOT DONE');
    }
}

//SETTER

function SET_DoAndDonts(arr) {

    $.each($("ul.dos-and-donts"), function(i, value) {

        $.each($(this).find('li'), function(j, value) {

            if ($(this).find('span').text() == arrDosAndDonts[i][1]) {

                $(this).find('label').addClass('active');

            }

        });

    });

}



//## STYLE-PREFERENCE function -----------------------------

//var arrStylePreference = [[]];

var arrStylePreference = [

    ['classics', '142',''],

    ['romantic-feminine', '143',''],

    ['free sprit', '144',''],

    ['bombshell', '145',''],

    ['bold and edgy', '146',''],

    ['athleisure', '147',''],

    ['casual', ''],

    ['bold', ''],

    ['9-5', ''],

    ['well groomed', '']

];



function GET_StylePreference(obj) {

    var thisContainer = $(obj).closest('ul').find('li');

    var strCurrentValue = '';

    var strCurrentStyle = '';

    $.each(thisContainer, function(i, value) {

        $(this).find('label').removeClass('active');

    });

    $(obj).find('label').addClass('active');

    strCurrentValue = $(obj).find('label').attr('action');

    strCurrentStyle = $(obj).closest('ul').attr('style');

    ADD_StylePreference(strCurrentStyle, strCurrentValue);

}



function ADD_StylePreference(strStyle, strValue) {

    $.each(arrStylePreference, function(i, value) {

        if (arrStylePreference[i][0] == strStyle) {

            arrStylePreference[i][2] = strValue;

            //## checking func CheckQuestionCompleted ---

           /* if (strGenderSelected == 'women') {

                question_1_14 = true;

            } else if (strGenderSelected == 'men') {

                question_1_12 = true;

            }*/

        }

    });

     CheckAlllovelikedislike();


    UpdateJSONBeforePayment(arrStylePreference);

}

function CheckAlllovelikedislike(){
    var intCtr = 0;
    var intTotalElementCtr =0;
    $('.section-style-preference ul.style-preference li').each(function(i,obj){
        if($(obj).find('label.sc-checkbox').hasClass('active')){
            intCtr++;  
        }
        intTotalElementCtr++;
    }); 
    intTotalElementCtr = intTotalElementCtr/3;
    if(intCtr==intTotalElementCtr){
        msg('DONE');
        if (strGenderSelected == 'women') {
            question_1_14 = true;
        } else if (strGenderSelected == 'men') {
            question_1_12 = true;
        }
    } else {
        msg('NOT DONE');
    }
}

//SETTER

function SET_StylePreference(arr) {

    $.each($("ul.style-preference"), function(i, value) {

        $.each($(this).find('li'), function(j, value) {

            if ($(this).find('label').attr('action') == arrStylePreference[i][1]) {

                $(this).trigger('click');

            }

        });

    });

}

//## GETTER - BAG I WANT FOR function -----------------------------

var strSectionBagIWantFor;



function GET_SectionBagIWantFor(obj) {

    $.each($("ul.section-bag-i-want-for li label"), function(i, value) {

        $(this).removeClass('active');

    });

    $(obj).find('label').addClass('active')

    strSectionBagIWantFor = $(obj).find('span').attr('answer_id');

    UpdateJSONAfterPayment(strSectionBagIWantFor);

}

//SETTER

function SET_SectionBagIWantFor(str) {

    $.each($("ul.section-bag-i-want-for li label"), function(i, value) {

        $(this).removeClass('active');

        if ($(this).find('span').attr('answer_id') == str) {

            $(this).trigger('click');

        }

    });

}



//## GETTER - ACESSORIES REFELECTOR TYPES function -----------------------------

var strAcessoriesRefelectorType;



function GET_AcessoriesRefelectorType(obj) {

    $.each($("ul.accessories-refelector-types li label"), function(i, value) {

        $(this).removeClass('active');

    });

    $(obj).find('label').addClass('active')

    strAcessoriesRefelectorType = $(obj).find('span').attr('answer_id');

    UpdateJSONAfterPayment(strAcessoriesRefelectorType);

}

//SETTER

function SET_AcessoriesRefelectorType(str) {

    $.each($("ul.accessories-refelector-types li label"), function(i, value) {

        $(this).removeClass('active');

        if ($(this).find('span').attr('answer_id') == str) {

            $(this).trigger('click');

        }

    });

}



//## GETTER - BEAUTY FINISHING TYPES function -----------------------------

var strBeautyFinishingType;



function GET_BeautyFinishingType(obj) {

    $.each($("ul.beauty-finishing-type li label"), function(i, value) {

        $(this).removeClass('active');

    });

    $(obj).find('label').addClass('active')

    strBeautyFinishingType = $(obj).find('span').attr('answer_id');

    UpdateJSONAfterPayment(strBeautyFinishingType);

}

//SETTER

function SET_BeautyFinishingType(str) {

    $.each($("ul.beauty-finishing-type li label"), function(i, value) {

        $(this).removeClass('active');

        if ($(this).find('span').attr('answer_id') == str) {

            $(this).trigger('click');

        }

    });

}

//## BAG SIZE function ------------------------------------

var strBagSize;



function GET_BagSize(obj) {

    var thisObj = $(obj);

    strBagSize = thisObj.val();

    UpdateJSONAfterPayment(strBagSize);

}

//## SET_MyHeight function ------------------------------------

function SET_BagSize(str) {

    $('select.bag-size').val(str);

}



//## FOOTWEAR HEEL HEIGHT function ------------------------------------

var strFootwearHeelHeight;



function GET_FootwearHeelHeight(obj) {

    var thisObj = $(obj);

    strFootwearHeelHeight = thisObj.val();

    UpdateJSONAfterPayment(strFootwearHeelHeight);

}

//## SET_FootwearHeelHeight function ------------------------------------

function SET_FootwearHeelHeight(str) {

    $('select.footwear-heel-height').val(str);

}

//## GETTER - HEEL TYPES function ------------------------------------

var arrFootwearHeelType = [];



function GET_FootwearHeelType(obj) {

    arrFootwearHeelType = [];

    $.each($("ul.footwear-heel-type li"), function(i, value) {

        if ($(this).hasClass('active')) {

        	var thisObj = $(this);
        	
        	var thisObjValue = thisObj.find(".title-3").attr('answer_id');
            //arrFootwearType.push($(this).find(".title-3").attr('answer_id'));
             arrFootwearHeelType.push({
                    'collection_id':thisObjValue,
                    'answer_value':thisObjValue
                 });

        }

    });

    UpdateJSONBeforePayment(arrFootwearHeelType);

}

//SETTER

function SET_FootwearHeelType(arr) {

    $.each($("ul.footwear-heel-type li"), function(j, value) {

        $.each($("ul.footwear-heel-type li"), function(i, value) {

            if ($(this).find(".title-3").attr('answer_id') == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}



//## SUNGLASSES TYPES function ------------------------------------

//GETTER

var arrAccessoriesSunglassesTypes = [];



function GET_AccessoriesSunglassesTypes(obj) {

    arrAccessoriesSunglassesTypes = [];

    $.each($("ul.accessories-sunglasses-types li"), function(i, value) {

        if ($(this).hasClass('active')) {

            arrAccessoriesSunglassesTypes.push($(this).find(".title-3").attr('answer_id'));

        }

    });

    UpdateJSONAfterPayment(arrAccessoriesSunglassesTypes);

}

//SETTER

function SET_AccessoriesSunglassesTypes(arr) {

    $.each($("ul.accessories-sunglasses-types li"), function(j, value) {

        $.each($("ul.accessories-sunglasses-types li"), function(i, value) {

            if ($(this).find(".title-3").attr('answer_id') == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}



//## SUNGLASSES TYPES function ------------------------------------

//GETTER

var arrAccessoriesBeltTypes = [];



function GET_AccessoriesBeltTypes(obj) {

    arrAccessoriesBeltTypes = [];

    $.each($("ul.accessories-belt-types li"), function(i, value) {

        if ($(this).hasClass('active')) {

            arrAccessoriesBeltTypes.push($(this).find(".title-3").attr('answer_id'));

        }

    });

    UpdateJSONAfterPayment(arrAccessoriesBeltTypes);

}

//SETTER

function SET_AccessoriesBeltTypes(arr) {

    $.each($("ul.accessories-belt-types li"), function(j, value) {

        $.each($("ul.accessories-belt-types li"), function(i, value) {

            if ($(this).find(".title-3").attr('answer_id') == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}



//## JEWELLERY TYPES function ------------------------------------

//GETTER

var arrJewelleryTones = [];



function GET_JewelleryTones(obj) {

    arrJewelleryTones = [];

    $.each($("ul.jewellery-tone li"), function(i, value) {

        if ($(this).hasClass('active')) {

            arrJewelleryTones.push($(this).find(".title-3").attr('answer_id'));

        }

    });

    UpdateJSONAfterPayment(arrJewelleryTones);

}

//SETTER

function SET_JewelleryTones(arr) {

    $.each($("ul.jewellery-tone li"), function(j, value) {

        $.each($("ul.jewellery-tone li"), function(i, value) {

            if ($(this).find(".title-3").attr('answer_id') == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}



//## BAG-COLOR-AVOID function ------------------------------------

//GETTER

var arrBagColorAvoid = [];



function GET_BagColorAvoid(obj) {

    arrBagColorAvoid = [];

    $.each($("ul.bag-color-avoid li"), function(i, value) {

        if ($(this).hasClass('active')) {

            arrBagColorAvoid.push($(this).find('div').attr('color'));

        }

    });

    UpdateJSONAfterPayment(arrBagColorAvoid);

}

//SETTER

function SET_BagColorAvoid(arr) {

    $.each($("ul.bag-color-avoid li"), function(j, value) {

        $.each($("ul.bag-color-avoid li"), function(i, value) {

            if ($(this).find('div').attr('color') == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}

//## BAG-COLOR-AVOID function ------------------------------------

var intBagPrintAvoid = 0;

var intMaxLimit_BagPrintAvoid = 4;

var arrBagPrintAvoid = [];



function GetBagPrintAvoid(obj) {

    arrBagPrintAvoid = [];

    var obj = $(obj);

    if (intBagPrintAvoid < intMaxLimit_BagPrintAvoid) {

        if (!obj.hasClass("active")) {

            obj.addClass("active");

            intBagPrintAvoid++;

        } else {

            obj.removeClass("active");

            intBagPrintAvoid--;

        }

    } else if (intBagPrintAvoid == intMaxLimit_BagPrintAvoid) {

        if (obj.hasClass("active")) {

            obj.removeClass("active");

            intBagPrintAvoid--;

        }

    }

    GET_BagPrintAvoid('get');

    UpdateJSONAfterPayment(arrBagPrintAvoid);

}



//## GET_GetBagPrintAvoid GETTER ------------------------------------

function GET_BagPrintAvoid(type, arr) {

    $.each($("ul.bag-print-avoid li"), function(i, value) {

        var thisObj = $(this);

        var thisObjValue = thisObj.find(".title-3").text();

        if (type == "get") {

            if (thisObj.hasClass('active')) {

                arrBagPrintAvoid.push(thisObjValue);

            }

        } else if (type == "reset") {

            thisObj.removeClass("active");

        }

    });

}



//## GetPrintAvoid SETTER ------------------------------------

function SET_GetBagPrintAvoid(arr) {

    $.each($("ul.bag-print-avoid li"), function(j, value) {

        $.each($("ul.bag-print-avoid li"), function(i, value) {

            if ($(this).find(".title-3").text() == arr[j]) {

                $(this).trigger('click');

            }

        });

    });

}

var strCommentWomen ='';
var strCommentMen = ''; 
$('#women_commentbox_1,#women_commentbox_2').on('change',function(){
    strCommentWomen = $('#women_commentbox_1').val() +' '+ $('#women_commentbox_2').val();
    strCommentWomen = strCommentWomen.replace(/[^a-zA-Z0-9\\-\\s\\b\/\\,\(\)]/gi, ' ');
    UpdateJSONBeforePayment();
});

$('#men_commentbox_1,#men_commentbox_2').on('change',function(){
    strCommentMen = $('#men_commentbox_1').val() +' '+ $('#men_commentbox_2').val();
    strCommentMen = strCommentMen.replace(/[^a-zA-Z0-9\\-\\s\\b\/\\,\(\)]/gi, ' ');
    UpdateJSONBeforePayment();
});


//## JSON CREATE============================================================================================

var isPAStep1 = false;

var isPAStep2 = false;

var isPAStep3 = false;

var isPAStep3_bag = false;

var isPAStep3_footwear = false;

var isPAStep3_accessory = false;

var isPAStep3_jewellery = false;

var isPAStep3_beauty = false;





var isPAStep4 = false;

/*var jsonObj = [];



function createJSON() {

    $('.list-categories.pa-women li').each(function() {

        if ($(this).find('label').hasClass('active')) {

            categories = {};

            var str = $(this).find('span').text();

            categories[str] = str;

            jsonObj.push(categories);

        }



    });

    msg(jsonObj);

}

*/

//createJSON();



/**********************************************************

//## JSON OBJECT SAVING (BEFORE PAYMENT)

/**********************************************************/

var jsonObjBeforePayment = [];

var jsonObject = {};

function UpdateJSONBeforePayment(data) {

    msg('strAccessorySelected' +strAccessorySelected);
    msg('arrCategorySelected' +arrCategorySelected);
    msg("Before Payment Data:" + data);

    //## Check question completed

    CheckQuestionCompleted(data);

    //New jSON start =======================
    jsonObject = {
  "status": "success",
  "response": [
    {
      "gender": "women",
      "pre_payment": {
        "categories": [
          {
            "category_id": "3",
            "name": "Apparel",
            "questions": [
              {
                "question_id": "15",
                "collection": []
              },
              {
                "question_id": "16",
                "collection": arrGetStuffDontNeed
              },
              {
                "question_id": "17",
                "collection": arrMySizeTop
              },
              {
                "question_id": "18",
                "collection": arrDressSize
              },
              {
                "question_id": "19",
                "collection": arrTrouserSize
              },
              {
                "question_id": "20",
                "collection": arrJeansSize
              },
              {
                "question_id": "21",
                "collection": arrBandSize
              },
              {
                "question_id": "22",
                "collection": arrCupSize
              },
              {
                "question_id": "23",
                "collection": arrFitYouLike
              },
              {
                "question_id": "24",
                "collection": arrFitLikeDress
              },
              {
                "question_id": "25",
                "collection": arrBottomFitLike
              },
              {
                "question_id": "26",
                "collection": arrSkirtDressSize
              },
              {
                "question_id": "27",
                "collection": arrDosAndDonts
              },
              {
                "question_id": "27",
                "collection": arrDosAndDonts
              },
              {
                "question_id": "28",
                "collection": []
              },
              {
                "question_id": "29",
                "collection": arrColorAvoid
              },
              {
                "question_id": "30",
                "collection": []
              },
              {
                "question_id": "31",
                "collection": []
              }
            ]
          },
          {
            "category_id": "4",
            "name": "Bags",
            "questions": [
              {
                "question_id": "32",
                "collection": arrBagType
              },
              {
                "question_id": "32",
                "collection": arrBagType
              }
            ]
          },
          {
            "category_id": "5",
            "name": "Footwear",
            "questions": [
              {
                "question_id": "15",
                "collection": []
              },
              {
                "question_id": "34",
                "collection": []
              },
              {
                "question_id": "35",
                "collection": arrFootwearType
              },
              {
                "question_id": "33",
                "collection": arrFootwearSize
              }
            ]
          },
          {
            "category_id": "6",
            "name": "Accessories",
            "questions": [
              {
                "question_id": "36",
                "collection": []
              },
              {
                "question_id": "36",
                "label": "ACCESSORIES",
                "collection": arrAccessorySelected,
                "conditional_logic": {
                  "operator": "",
                  "rules": {
                    "field": "",
                    "operator": "",
                    "value": ""
                  }
                }
              }
            ]
          },
          {
            "category_id": "7",
            "name": "Jewellery",
            "questions": []
          },
          {
            "category_id": "8",
            "name": "Beauty",
            "questions": [
              {
                "question_id": "38",
                "label": "THE BEAUTY PRODUCT I'M LOOKING FOR",
                "collection": []
              },
              {
                "question_id": "39",
                "label": "SHADE",
                "collection": []
              }
            ]
          }
        ]
      },
      "post_payment": {
        "categories": [
          {
            "category_id": "3",
            "name": "Apparel",
            "questions": [
              {
                "question_id": "389",
                "label": "",
                "instruction_text": "",
                "answer_key": "",
                "template_type": "3",
                "template_sub_type": "1",
                "min_selection": "1",
                "max_selection": "1",
                "selection_color": ""
              },
              {
                "question_id": "40",
                "collection": []
              },
              {
                "question_id": "41",
                "collection": []
              }
            ]
          },
          {
            "category_id": "4",
            "name": "Bags",
            "questions": [
              {
                "question_id": "42",
                "collection": []
              },
              {
                "question_id": "43",
                "label": "I WANT THE SIZE OF MY BAG TO BE",
                "collection": []
              }
            ]
          },
          {
            "category_id": "5",
            "name": "Footwear",
            "questions": [
              {
                "question_id": "46",
                "label": "YOU'RE LOOKING FOR A PAIR OF SHOES OF HEELS! COULD YOU TELL US WHAT KIND OF HEELS YOU'D THIS? THE TYPE OF HEELS I AM LOOKING FOR",
                "collection": arrFootwearHeelType
              },
              {
                "question_id": "47",
                "label": "HOW TALL WOULD YOU LIKE YOUR HEELS TO BE?",
                "collection": []
              }
            ]
          },
          {
            "category_id": "6",
            "name": "Accessories",
            "questions": [
              {
                "question_id": "48",
                "collection": []
              },
              {
                "question_id": "49",
                "label": "REFELECTORS",
                "collection": []
              },
              {
                "question_id": "50",
                "label": "YOU'RE LOOKING FOR A BELT! COULD YOU TELL US WHAT KIND OF BELT? BELTS",
                "collection": []
              }
            ]
          },
          {
            "category_id": "7",
            "name": "Jewellery",
            "questions": [
              {
                "question_id": "51",
                "collection": []
              }
            ]
          },
          {
            "category_id": "8",
            "name": "Beauty",
            "questions": [
              {
                "question_id": "52",
                "label": "YOU'RE LOOKING FOR A BEAUTY PRODUCT! COULD YOU TELL US MORE? THE FINISH OF THE BEAUTY PRODUCT I'M LOOKING FOR",
                "collection": []
              }
            ]
          }
        ]
      }
    },
    {
      "gender": "men"
    }
  ],
  "type": "array"
}
    
    msg(jsonObject);
    //New jSON end =======================


    jsonObjBeforePayment = [];

    objApparelMain = {

        objApparel: {

            pa_bodshape: strBodyShape,

            apparel_stuff_dont_like: arrGetStuffDontNeed,

            apparel_top_size: arrMySizeTop,

            apparel_top_fit_like_this: arrFitYouLike,

            apparel_dress_size: strDressSize,

            apparel_dress_fit_like_this: arrFitLikeDress,

            apparel_trouser_size: strTrouserSize,

            apparel_jeans_size: strJeansSize,

            apparel_bottom_fit_like_this: arrBottomFitLike,

            apparel_band_size: strBandSize,

            apparel_cup_size: strCupSize,

            apparel_skirt_fit_like_this: arrSkirtDressSize,

            apparel_color_avoid: arrColorAvoid,

            apparel_print_avoid: arrPrintAvoid,

            apparel_dos_and_donts: arrDosAndDonts,

            apparel_style_preference: arrStylePreference,

            women_commentbox: strCommentWomen,

            
            //#####MEN========================================================

            apparel_tshirt_size: strMySizeTshirt,

            apparel_tshirt_fit_like_this: arrFitYouLikeMen,

            apparel_shirt_size: strMySizeShirt,

            apparel_shirt_fit_like_this: arrFitYouLikeMenS,

            apparel_trouser_fit_like_this: arrFitYouLikeMenT,

            apparel_jeans_fit_like_this: arrFitYouLikeMenJ,

            apparel_print_avoid_men: arrPrintAvoidMen,

            men_commentbox: strCommentMen,

        



        }

    };



    objBagMain = {

        objBag: {

            bag_type: arrBagType

        }

    };

    objFootwearMain = {

        objFootwear: {

            footwear_size: strFootwearSize,

            footwear_type: arrFootwearType,

            footwear_width: strSectionFootwearWidth

        }

    };

    objAccessoriesMain = {

        objAccessories: {

            accessories_types: strAccessorySelected

        }

    };

    objJewelleryMain = {

        objJewellery: {

            jewellery_type: arrJewelleryTypes

        }

    };

    objBeautyMain = {

        objBeauty: {

            beauty_type: strBeautySelected,

            beauty_shade: strSectionBeautyShade

        }

    };



    objGroomingMain = {

        objGrooming: {

            grooming_product: strGroomingProduct

        }

    };

    //msg('accessories_selected == '+strAccessorySelected);

    setCookie('accessories_selected', strAccessorySelected, 365)

    jsonObjBeforePayment.push(objApparelMain);

    jsonObjBeforePayment.push(objFootwearMain);

    jsonObjBeforePayment.push(objAccessoriesMain);

    jsonObjBeforePayment.push(objJewelleryMain);

    jsonObjBeforePayment.push(objBeautyMain);

    jsonObjAfterPayment.push(objGroomingMain);

    jsonObjBeforePayment.push(objBagMain)

    setCookie('jsonObjBeforePayment', jsonObjBeforePayment, 365);

    msg(jsonObjBeforePayment);

}



/**********************************************************

//## JSON OBJECT SAVING (AFTER PAYMENT)

/**********************************************************/

var jsonObjAfterPayment = [];



function UpdateJSONAfterPayment(data) {

    msg("After Payment Data:" + data);

    jsonObjAfterPayment = [];

    objApparelMain = {

        objApparel: {

            apparel_height_feet: strMyHT_feet,

            apparel_height_inches: strMyHT_inches

        }

    };

    objBagMain = {

        objBag: {

            bag_i_want: strSectionBagIWantFor,

            bag_size: strBagSize,

            bag_color_avoid: arrBagColorAvoid,

            bag_print_avoid: arrBagPrintAvoid

        }

    };

    objFootwearMain = {

        objFootwear: {

            footwear_heel_type: arrFootwearHeelType,

            footwear_heel_height: strFootwearHeelHeight,

        }

    };

    objAccessoriesMain = {

        objAccessories: {

            accessories_sunglasses_type: arrAccessoriesSunglassesTypes,

            accessories_refelector_type: strAcessoriesRefelectorType,

            accessories_belt_type: arrAccessoriesBeltTypes,

        }

    };

    objJewelleryMain = {

        objJewellery: {

            jewellery_tone: arrJewelleryTones,

        }

    };

    objBeautyMain = {

        objBeauty: {

            beauty_finishing_type: strBeautyFinishingType

        }

    };





    jsonObjAfterPayment.push(objApparelMain);

    jsonObjAfterPayment.push(objFootwearMain);

    jsonObjAfterPayment.push(objAccessoriesMain);

    jsonObjAfterPayment.push(objJewelleryMain);

    jsonObjAfterPayment.push(objBeautyMain);



    jsonObjAfterPayment.push(objBagMain);

    setCookie('jsonObjAfterPayment', jsonObjAfterPayment, 365);

    msg(jsonObjAfterPayment);

}



//## SET COOKIES --------------------------------

function setCookie(cname, cvalue, exdays) {

    var d = new Date();

    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));

    var expires = "expires=" + d.toUTCString();

    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";

}



//## GET COOKIES --------------------------------

function getCookie(cname) {

    var name = cname + "=";

    var ca = document.cookie.split(';');

    for (var i = 0; i < ca.length; i++) {

        var c = ca[i];

        while (c.charAt(0) == ' ') {

            c = c.substring(1);

        }

        if (c.indexOf(name) == 0) {

            return c.substring(name.length, c.length);

        }

    }

    return "";

}

//## SHOW HIDE CATEGORY SECTIONS -----------------

function ShowHideCategorySections() {

    var arr = getCookie('category_selection').split(",");

    var isApparelSelected = false;

    $('.category-apparel,.category-bags,.category-footwear,.category-accessories,.category-jewellery,.category-beauty,.category-grooming').addClass('hide');

    $.each(arr, function(i, e) {

        if (e == 'apparel') {

            $('.category-apparel').removeClass('hide');

            isApparelSelected = true;

        } else if (e == 'bags') {

            $('.category-bags').removeClass('hide');

            if (isApparelSelected) {

                $('.container.bag-print-avoid, .bag-color-avoid').addClass('hide');

            } else {

                $('.container.bag-print-avoid, .bag-color-avoid').removeClass('hide');

            }

        } else if (e == 'footwear') {

            $('.category-footwear').removeClass('hide');

        } else if (e == 'accessories') {

            $('.category-accessories').removeClass('hide');

            //Get Category selection



            var strCaregorySelected = getCookie('accessories_selected');



            if (strCaregorySelected == 'sunglasses') {

                $('.accessories-sunglasses-container').show();

                $('.accessories-refelector-container').show();

                $('.accessories-belt-container').hide();

            } else if (strCaregorySelected == 'belts') {

                $('.accessories-sunglasses-container').hide();

                $('.accessories-refelector-container').hide();

                $('.accessories-belt-container').show();

            } else if (strCaregorySelected == 'caps' || strCaregorySelected == 'scarves' || strCaregorySelected == 'stationary' ||

                strCaregorySelected == 'pocket square' || strCaregorySelected == 'ties and bow ties' || strCaregorySelected == 'lapel and collar pins' ||

                strCaregorySelected == 'cuff links' || strCaregorySelected == 'jewellery') {

                $('.category-accessories').hide();

            }

        } else if (e == 'jewellery') {

            $('.category-jewellery').removeClass('hide');

        } else if (e == 'beauty') {

            $('.category-beauty').removeClass('hide');

        }

    });

}





/**********************************************************

//## CHECK IF ANY QUESTION BLANK AND SHOW ERROR MESSAGE

/**********************************************************/

var isCurrentStep = 0;

var intBagActive, intAccessoriesActive, intFootwearActive, intJewelleryActive, intBeautyActive,intGroomingActive,intBeautyActive1 = 0;

//var question_2_1, question_3_1, question_3_2, question_3_3 , question_4_1, question_5_1, question_6_1, question_6_2;

var question_2_1, question_2_2, question_2_3, question_2_4, question_2_5, question_2_6, question_2_7, question_2_8;

var arrQuestionNotCompleted = [];

var objQuestionToScroll;

//var arrQuestionLimitInPage;

//## Adding scroll to top uncomplete question an Step Complete - relate to ChangeStep

var isCheckFirstErrorQuestion = false;

//??arrQuestionNotCompleted = [];

var question_1_13 = false; 
var question_1_14 = false; 
var question_1_15 = false; 
var question_1_16 = false;
var arrCategorySelected = [];

function CheckQuestionCompleted(data) {

    if (strGenderSelected == 'women') {



        var cat_selection = getCookie('category_selection');

         arrCategorySelected = cat_selection.split(",");

        arrQuestionNotCompleted = [];

        if ($.inArray("apparel", arrCategorySelected) !== -1) {

            //msg('WOMEN STEP 1 APPAREL ============================***');

            intStep = 1;

           /* strBodyShape != undefined && strBodyShape.length > 0 ? question_1_1 = true : question_1_1 = false;

            arrGetStuffDontNeed.length > 0 ? question_1_2 = true : question_1_2 = false;

            strMySizeTop != undefined && strMySizeTop.length > 0 ? question_1_3 = true : question_1_3 = false;

            arrFitYouLike.length > 0 ? question_1_4 = true : question_1_4 = false;

            strDressSize != undefined && strDressSize.length > 0 ? question_1_5 = true : question_1_5 = false;

            arrFitLikeDress.length > 0 ? question_1_6 = true : question_1_6 = false;

            strTrouserSize != undefined && strTrouserSize.length > 0 ? question_1_7 = true : question_1_7 = false;

            strJeansSize != undefined && strJeansSize.length > 0 ? question_1_8 = true : question_1_8 = false;

            arrBottomFitLike.length > 0 ? question_1_9 = true : question_1_9 = false;

            strBandSize != undefined && strBandSize.length > 0 ? question_1_10 = true : question_1_10 = false;

            strCupSize != undefined && strCupSize.length > 0 ? question_1_11 = true : question_1_11 = false;

            arrSkirtDressSize.length > 0 ? question_1_12 = true : question_1_12 = false;

            //arrDosAndDonts>0?question_1_13 = true:question_1_13 = false; (DIRECTLY DEFINED TRUE ON THEIR FUNCTION)

            //arrStylePreference>0?question_1_14 = true:question_1_14 = false; (DIRECTLY DEFINED TRUE ON THEIR FUNCTION)

            arrColorAvoid.length > 0 ? question_1_15 = true : question_1_15 = false;

            arrPrintAvoid.length > 0 ? question_1_16 = true : question_1_16 = false;
*/
            strBodyShape != undefined && strBodyShape.length > 0 ? question_1_1 = true : question_1_1 = false;
            arrGetStuffDontNeed.length > 0 ? question_1_2 = true : question_1_2 = false;
            strMySizeTop != undefined && strMySizeTop.length > 0 ? question_1_3 = true : question_1_3 = false;
            strDressSize != undefined && strDressSize.length > 0 ? question_1_4 = true : question_1_4 = false
            strTrouserSize != undefined && strTrouserSize.length > 0 ? question_1_5 = true : question_1_5 = false;
            strJeansSize != undefined && strJeansSize.length > 0 ? question_1_6 = true : question_1_6 = false;
            strBandSize != undefined && strBandSize.length > 0 ? question_1_7 = true : question_1_7 = false;
            strCupSize != undefined && strCupSize.length > 0 ? question_1_8 = true : question_1_8 = false;       
            arrFitYouLike.length > 0 ? question_1_9 = true : question_1_9 = false;
            arrFitLikeDress.length > 0 ? question_1_10 = true : question_1_10 = false;
            arrBottomFitLike.length > 0 ? question_1_11 = true : question_1_11 = false;
            arrSkirtDressSize.length > 0 ? question_1_12 = true : question_1_12 = false;
            //arrDosAndDonts>0?question_1_13 = true:question_1_13 = false; (DIRECTLY DEFINED TRUE ON THEIR FUNCTION)
            //arrStylePreference>0?question_1_14 = true:question_1_14 = false; (DIRECTLY DEFINED TRUE ON THEIR FUNCTION)
            arrColorAvoid.length > 0 ? question_1_15 = true : question_1_15 = false;
            arrPrintAvoid.length > 0 ? question_1_16 = true : question_1_16 = false;


            //## Adding scroll to top uncomplete question an Step Complete - relate to ChangeStep
            var isCheckFirstErrorQuestion = false;
            //arrQuestionNotCompleted = [];
            for (var i = 1; i < 17; i++) {
                if (eval('question_1_' + i) == true) {
                    $('#question_1_' + i).find('.message.error').addClass('hide');
                } else {
                    arrQuestionNotCompleted.push(('question_1_' + i));
                }
            }
/*NEW */             
            arrQuestionNotCompleted = [];
            if(question_1_1 == false){
                arrQuestionNotCompleted.push('question_1_1');
            }
            $('.women_apparel_beforepayment > div').each(function(i,obj){
                if(!$(obj).hasClass('hide') && eval('question_1_' + (i+2)) == false){
                    msg($(obj).attr('id'));
                    arrQuestionNotCompleted.push($(obj).attr('id'))
                }
            });
            msg('updated js');
            msg(arrQuestionNotCompleted);
/*NEW */            

            var objQuestionToScroll = '#' + arrQuestionNotCompleted[0];

            var objNextButton = '.btn-next-' + intStep + '.link-scrolling';

            if (arrQuestionNotCompleted.length > 0) {

                $(objNextButton).attr('href', objQuestionToScroll)

                isPAStep2 = false;

            } else {

                $(objNextButton).removeAttr('href')

                isPAStep2 = true;

            }

        }

        if (isCurrentStep == 3) {

            msg("IN STEP 3.......................");

            if ($.inArray("bags", arrCategorySelected) !== -1) {

                intBagActive = 1;

                intStep = 2;

                arrBagType.length > 0 ? question_2_1 = true : question_2_1 = false;

                for (var i = 1; i < 2; i++) {

                    if (eval('question_' + intStep + '_' + i) == true) {

                        msg('COMPLETED :' + ('question_' + intStep + '_' + i));

                        $('#question_' + intStep + '_' + i).find('.message.error').addClass('hide');

                    } else {

                        arrQuestionNotCompleted.push(('question_' + intStep + '_' + i));

                    }

                }

            }

            if ($.inArray("footwear", arrCategorySelected) !== -1) {

                intFootwearActive = 1;

                intStep = 2;

                strFootwearSize != undefined && strFootwearSize.length > 0 ? question_2_2 = true : question_2_2 = false;

                strSectionFootwearWidth != undefined && strSectionFootwearWidth.length > 0 ? question_2_3 = true : question_2_3 = false;

                arrFootwearType.length > 0 ? question_2_4 = true : question_2_4 = false;

                for (var i = 2; i < 5; i++) {

                    if (eval('question_' + intStep + '_' + i) == true) {

                        msg('footwear COMPLETED :' + ('question_' + intStep + '_' + i));

                        $('#question_' + intStep + '_' + i).find('.message.error').addClass('hide');

                    } else {

                        arrQuestionNotCompleted.push(('question_' + intStep + '_' + i));

                    }

                }

            }

            if ($.inArray("accessories", arrCategorySelected) !== -1) {

                intAccessoriesActive = 1;

                intStep = 2;

                strAccessorySelected != undefined && strAccessorySelected.length > 0 ? question_2_5 = true : question_2_5 = false;

                for (var i = 5; i < 6; i++) {

                    if (eval('question_' + intStep + '_' + i) == true) {

                        msg('accessories COMPLETED :' + ('question_' + intStep + '_' + i));

                        $('#question_' + intStep + '_' + i).find('.message.error').addClass('hide');

                    } else {

                        msg(' accessories NOT COMPLETED :' + ('question_' + intStep + '_' + i));

                        arrQuestionNotCompleted.push(('question_' + intStep + '_' + i));

                    }

                }

            }

            if ($.inArray("jewellery", arrCategorySelected) !== -1) {

                intJewelleryActive = 1;

                intStep = 2;

                arrJewelleryTypes.length > 0 ? question_2_6 = true : question_2_6 = false;

                for (var i = 6; i < 7; i++) {

                    if (eval('question_' + intStep + '_' + i) == true) {

                        msg('COMPLETED :' + ('question_' + intStep + '_' + i));

                        $('#question_' + intStep + '_' + i).find('.message.error').addClass('hide');

                    } else {

                        msg('NOT COMPLETED :' + ('question_' + intStep + '_' + i));

                        arrQuestionNotCompleted.push(('question_' + intStep + '_' + i));

                    }

                }

            }

            if ($.inArray("beauty", arrCategorySelected) !== -1) {

                intBeautyActive = 1;

                intStep = 2;

                strBeautySelected != undefined && strBeautySelected.length > 0 ? question_2_7 = true : question_2_7 = false;

               //strSectionBeautyShade != undefined && strSectionBeautyShade.length > 0 ? question_2_8 = true : question_2_8 = false;

                for (var i = 7; i < 8; i++) {

                    if (eval('question_' + intStep + '_' + i) == true) {

                        msg('COMPLETED :' + ('question_' + intStep + '_' + i));

                        $('#question_' + intStep + '_' + i).find('.message.error').addClass('hide');

                    } else {

                        msg('NOT COMPLETED :' + ('question_' + intStep + '_' + i));

                        arrQuestionNotCompleted.push(('question_' + intStep + '_' + i));

                    }



                }

            }

            

            //## To scroll to first error question

            var objQuestionToScroll = '#' + arrQuestionNotCompleted[0];

            var objNextButton = '.btn-next-' + intStep + '.link-scrolling';

            if (arrQuestionNotCompleted.length > 0) {

                $(objNextButton).attr('href', objQuestionToScroll)

                isPAStep3 = false;

            } else {

                $(objNextButton).removeAttr('href')

                isPAStep3 = true;

            }

        }

        //msg(arrQuestionNotCompleted);

        //msg(intBagActive+' :: '+intAccessoriesActive+' :: '+intFootwearActive+' :: '+intJewelleryActive+' :: '+intBeautyActive);

    }

     else if (strGenderSelected == 'men') {


        var cat_selection = getCookie('category_selection');

        arrCategorySelected = cat_selection.split(",");

        arrQuestionNotCompleted = [];

        if ($.inArray("apparel", arrCategorySelected) !== -1) {

            msg('MEN STEP 1 APPAREL ============================***');

            intStep = 1;

            strBodyShape != undefined && strBodyShape.length > 0 ? question_1_1 = true : question_1_1 = false;

            arrGetStuffDontNeed.length > 0 ? question_1_2 = true : question_1_2 = false;

            strMySizeTshirt != undefined && strMySizeTshirt.length > 0 ? question_1_3 = true : question_1_3 = false;

            arrFitYouLikeMen.length > 0 ? question_1_7 = true : question_1_7 = false;

            strMySizeShirt != undefined && strMySizeShirt.length > 0 ? question_1_4 = true : question_1_4 = false;

            arrFitYouLikeMenS.length > 0 ? question_1_8 = true : question_1_8 = false;

            strTrouserSize != undefined && strTrouserSize.length > 0 ? question_1_5 = true : question_1_5 = false;

            arrFitYouLikeMenT.length > 0 ? question_1_9 = true : question_1_9 = false;

            strJeansSize != undefined && strJeansSize.length > 0 ? question_1_6 = true : question_1_6 = false;

            arrFitYouLikeMenJ.length > 0 ? question_1_10 = true : question_1_10 = false;

            //arrDosAndDonts>0?question_1_11 = true:question_1_11 = false; (DIRECTLY DEFINED TRUE ON THEIR FUNCTION)

            //arrStylePreference>0?question_1_12 = true:question_1_12 = false; (DIRECTLY DEFINED TRUE ON THEIR FUNCTION)

            arrColorAvoid.length > 0 ? question_1_13 = true : question_1_13 = false;

            arrPrintAvoidMen.length > 0 ? question_1_14 = true : question_1_14 = false;



            //## Adding scroll to top uncomplete question an Step Complete - relate to ChangeStep

            var isCheckFirstErrorQuestion = false;

            //arrQuestionNotCompleted = [];

            for (var i = 1; i < 15; i++) {

                if (eval('question_' + intStep + '_' + i) == true) {

                    msg('COMPLETED :' + ('question_' + intStep + '_' + i));

                    $('#question_' + intStep + '_' + i).find('.message.error').addClass('hide');

                } else {

                    msg('NOT COMPLETED :' + ('question_' + intStep + '_' + i));

                    arrQuestionNotCompleted.push(('question_' + intStep + '_' + i));

                }



            }

            var objQuestionToScroll = '#' + arrQuestionNotCompleted[0];

            var objNextButton = '.btn-next-' + intStep + '.link-scrolling';

            if (arrQuestionNotCompleted.length > 0) {

                $(objNextButton).attr('href', objQuestionToScroll)

                isPAStep2 = false;

            } else {

                $(objNextButton).removeAttr('href')

                isPAStep2 = true;

            }

        }

        if (isCurrentStep == 3) {

            if ($.inArray("bags", arrCategorySelected) !== -1) {

                msg('bags---');

                intBagActive = 1;

                intStep = 2;

                arrBagType.length > 0 ? question_2_1 = true : question_2_1 = false;

                for (var i = 1; i < 2; i++) {

                    if (eval('question_' + intStep + '_' + i) == true) {

                        msg('COMPLETED :' + ('question_' + intStep + '_' + i));

                        $('#question_' + intStep + '_' + i).find('.message.error').addClass('hide');

                    } else {

                        msg('NOT COMPLETED :' + ('question_' + intStep + '_' + i));

                        arrQuestionNotCompleted.push(('question_' + intStep + '_' + i));

                    }



                }



            }

            if ($.inArray("footwear", arrCategorySelected) !== -1) {

                msg('footwear---');

                intFootwearActive = 1;

                intStep = 3;

                strFootwearSize != undefined && strFootwearSize.length > 0 ? question_3_1 = true : question_3_1 = false;

                arrFootwearType.length > 0 ? question_3_2 = true : question_3_2 = false;

                for (var i = 1; i < 3; i++) {

                    if (eval('question_' + intStep + '_' + i) == true) {

                        msg('COMPLETED :' + ('question_' + intStep + '_' + i));

                        $('#question_' + intStep + '_' + i).find('.message.error').addClass('hide');

                    } else {

                        msg('NOT COMPLETED :' + ('question_' + intStep + '_' + i));

                        arrQuestionNotCompleted.push(('question_' + intStep + '_' + i));

                    }



                }

            }

            if ($.inArray("accessories", arrCategorySelected) !== -1) {

                msg('accessories---');

                intAccessoriesActive = 1;

                intStep = 4;

                strAccessorySelected != undefined && strAccessorySelected.length > 0 ? question_4_1 = true : question_4_1 = false;

                for (var i = 1; i < 2; i++) {

                    if (eval('question_' + intStep + '_' + i) == true) {

                        msg('COMPLETED :' + ('question_' + intStep + '_' + i));

                        $('#question_' + intStep + '_' + i).find('.message.error').addClass('hide');

                    } else {

                        msg('NOT COMPLETED :' + ('question_' + intStep + '_' + i));

                        arrQuestionNotCompleted.push(('question_' + intStep + '_' + i));

                    }

                }

            }

            var objQuestionToScroll = '#' + arrQuestionNotCompleted[0];

            var objNextButton = '.btn-next-' + intStep + '.link-scrolling';

            if (arrQuestionNotCompleted.length > 0) {

                $(objNextButton).attr('href', objQuestionToScroll)

                isPAStep3 = false;

            } else {

                $(objNextButton).removeAttr('href')

                isPAStep3 = true;

            }

        }



        msg(arrQuestionNotCompleted);

    }





}



/**********************************************************

//## SET FUNCTION 

/**********************************************************/

function SET_AllSaveSelections() {

    SetBodyShape('apple');



    arrStuffDoneNeed = ["DRESSES", "shorts and skirts", "OUTERWEAR", "JUMPSUITS"];

    SET_StuffDontNeed(arrStuffDoneNeed);



    strMyHT_feet = '4 ft';

    SET_MyHeight(strMyHT_feet)



    strMyHT_inches = '10 inch';

    SET_MyHeightInches(strMyHT_inches)



    strMySizeTop = 'XXL/UK 20/US 18/EU 48';

    SET_MySizeTop(strMySizeTop);



    arrFitYouLike = ["RELAXED", "FLOWY"];

    SET_FitYouLike(arrFitYouLike)



    strDressSize = 'XL/UK 18/US 16/EU 46';

    SET_DressSize(strDressSize)



    arrFitLikeDress = ["BODYCON", "SKATER", "A LINE"];

    SET_FitLikeDress(arrFitLikeDress)



    strTrouserSize = 'M/28';

    SET_TrouserSize(strTrouserSize)



    strJeansSize = 'XXL/34';

    SET_JeansSize(strJeansSize)



    arrBottomFitLike = ["FLARED", "SLIM"];

    SET_BottomFitLike(arrBottomFitLike)





    strBandSize = '38';

    SET_BandSize(strBandSize)





    strCupSize = 'DD';

    SET_CupSize(strCupSize)



    arrSkirtDressSize = ["MIDI", "MINI"];

    SET_SkirtDressSize(arrSkirtDressSize)



    arrColorAvoid = ['#000000', '#ffffff', '#3aa5dc', '#ff0080', '#660099']

    SET_ColorAvoid(arrColorAvoid)



    arrPrintAvoid = ["Polka dots", "Animal", "Checks"];

    SET_GetPrintAvoid(arrPrintAvoid);



    arrBagType = ["tote", "sling"];

    SET_BagType(arrBagType)



    arrFootwearType = ["sneakers", "heels"];

    SET_FootwearType(arrFootwearType);



    strSectionFootwearWidth = 'I have Broad feet';

    SET_SectionFootwearWidth(strSectionFootwearWidth);



    arrJewelleryTypes = ["statement", "bold/chunky"];

    SET_JewelleryTypes(arrJewelleryTypes)





    strFootwearSize = 'UK 6/US 8/EU 39';

    SET_FootwearSize(strFootwearSize)



    strAccessorySelected = 'caps';

    SET_AccessoriesSelected(strAccessorySelected);



    strBeautySelected = 'LINER';

    SET_BeautySelected(strBeautySelected);



    strSectionBeautyShade = 'RED';

    SET_SectionBeautyShade(strSectionBeautyShade);





    var arrDosAndDonts = [

        ['sleeveless', 'yes'],

        ['halter', 'no'],

        ['strappy', 'no'],

        ['off shoulder', 'no'],

        ['tube', 'yes'],

        ['crop top', 'yes'],

        ['backless', 'yes'],

        ['deep neck', 'yes']

    ];

    SET_DoAndDonts(arrDosAndDonts);





    var arrStylePreference = [

        ['classics', 'love'],

        ['romantic-feminine', 'like'],

        ['free sprit', 'like'],

        ['bombshell', 'dislike'],

        ['bold and edgy', 'like'],

        ['athleisure', 'dislike']

    ];

    SET_StylePreference(arrStylePreference);



    strSectionBagIWantFor = 'Work';

    SET_SectionBagIWantFor(strSectionBagIWantFor);



    strAcessoriesRefelectorType = 'Yes';

    SET_AcessoriesRefelectorType(strAcessoriesRefelectorType);





    strBeautyFinishingType = 'Matte';

    SET_BeautyFinishingType(strBeautyFinishingType);





    strBagSize = 'small';

    SET_BagSize(strBagSize)



    strFootwearHeelHeight = 'MEDIUM:2.5-3.5"';

    SET_FootwearHeelHeight(strFootwearHeelHeight)



    arrFootwearHeelType = ["WEDGE", "STILLETOS"];

    SET_FootwearHeelType(arrFootwearHeelType)



    arrAccessoriesSunglassesTypes = ["CAT EYES", "SQUARE"];

    SET_AccessoriesSunglassesTypes(arrAccessoriesSunglassesTypes)



    arrAccessoriesBeltTypes = ["SLIM", "CHUNKY"];

    SET_AccessoriesBeltTypes(arrAccessoriesBeltTypes)



    arrJewelleryTones = ["GOLD", "OXIDISED"];

    SET_JewelleryTones(arrJewelleryTones)



    arrBagColorAvoid = ['#000000', '#ffffff', '#3aa5dc', '#ff0080', '#660099']

    SET_BagColorAvoid(arrBagColorAvoid)





    arrBagPrintAvoid = ["Polka dots", "Animal", "Checks"];

    SET_GetBagPrintAvoid(arrBagPrintAvoid);

}

 /*after payment comment*/
 $(function(){
        //console.log('strGender: ' +strGender);
        //var strGender = getCookie('scbox-gender');
        if (strGenderSelected == 'men') {
          $('ul.description-comment li').eq(0).hide();
        }

}); 


function complete(e) {

    //var base_url = window.location.origin;

    /* e.preventDefault();

     $.ajax({

         type:'POST',

         url: sc_baseurl+"scbox_new/step5",

         data :  $(form).serialize(),

         cache:false,

         contentType: false,

         processData: false,

         success:function(data){

             var html  = ('<div class="alert alert-success"><strong>Success!</strong> Product Added Successfully.</div>');

             $('#productData').append(html);

             window.location.href = sc_baseurl+"scbox-thankyou?success=1";

             //setTimeout(function() {$('#addProductModal').modal('hide');}, 1500);

         },

         error: function(data){

             console.log("error");

             console.log(data);

         }

     });*/

}