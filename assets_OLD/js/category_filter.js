var isTriggered = false;
var intTriggerScroll = 500;
var arr_cat_breadcrump = new Array();
var arrCategorySelected = new Array();
var arrBrandSelected = new Array();
var arrSizeSelected = new Array();
var arrCatAttrSelected = new Array();
var intPriceSelected,intSortBy,gender_cat_id;

$(window).scroll(function(){
	
	if ($('#get_all_cat_products').length){
		get_more_filter_products();
	}
	
});

$( document ).ready(function(e){
	//##
	var gender = parseInt($("#gender_cat").val());

	if(gender>0)arr_cat_breadcrump.push($("#gender_cat").val());
	
	//gender list for collection
	$('#content-gender li').on("mousedown",function(e){

		clear_filter('collection_filter_gender');
		$('#content-gender li label').removeClass('active');
		$(this).find("label").addClass('active');
		filter_collection_products();

	});
	
	//price list filter--------------
	$('#sortby li').click(function(e) {
		$('#sortby li label').removeClass('active');
		$(this).find("label").addClass('active');
	});
	
	//price list filter--------------
	$('#content-price li').click(function(e) {
		$('#content-price li label').removeClass('active');
		$(this).find("label").addClass('active');
	});
	
	//price list filter--------------
	$('#content-price li').click(function(e) {
		$('#content-price li label').removeClass('active');
		$(this).find("label").addClass('active');
	});
	
	//categories Content filter--------------
	$('#content-categories li label').on("click",function(e) {
		if(!$(this).hasClass("active")) {
			$(this).addClass('active');
		} else {
			$(this).removeClass('active');
		}
	});
	
	//brands Content filter--------------
	$('#content-brands li label').on("click",function(e) {
		if(!$(this).hasClass("active")) {
			$(this).addClass('active');
		} else {
			$(this).removeClass('active');
		}
	});
	
	//Size Content filter--------------
	$('#content-size li label').on("click",function(e) {
		if(!$(this).hasClass("active")) {
			$(this).addClass('active');
		} else {
			$(this).removeClass('active');
		}
	});
	
	//color and categories-attr Content filter--------------
	$('.content-category-attr li label').on("click",function(e) {
		if(!$(this).hasClass("active")) {
			$(this).addClass('active');
		} else {
			$(this).removeClass('active');
		}
	});
	
	//global search
	$("#search_keyword").keypress(function(event) {
		if (event.which == 13 && ($("#search_keyword").val().trim() !='')) {
			var search = $('#search_keyword').val();
			location.href=sc_baseurl+'category/product-filter?search='+encodeURIComponent(search);
		}
	});
	
});

function msg(m){ 
    //console.log(m); 
}
function get_child_cat(cat_id,category_name){
	var cat_name = '';
	cat_name = (typeof category_name !== 'undefined') ?  category_name : cat_name;
	if(cat_name!=''){
		if(cat_id == '1' || cat_id == '2' || cat_id == '3'){
			arr_cat_breadcrump = [];
			if(arr_cat_breadcrump.indexOf(cat_name)==-1)arr_cat_breadcrump.push(cat_name);
		}else {
			// arr_cat_breadcrump.pop();
			if(arr_cat_breadcrump.indexOf(cat_name)==-1)arr_cat_breadcrump.push(cat_name);
		}
	}else{
		if(cat_id == '1' || cat_id == '2' || cat_id == '3'){
			arr_cat_breadcrump = [];
			arr_cat_breadcrump.push($("#gender_cat").val());
		}else{
			arr_cat_breadcrump.pop();
		}
	}
	
	msg(arr_cat_breadcrump);
	
	$("#load-more").show();
	$.ajax({
		url: sc_baseurl+'Category/get_child_cat',
		type: 'post',
		data: { 'cat_id' : cat_id ,'arr_cat_breadcrump' : arr_cat_breadcrump},
		success: function(data, status) {
			$("#test1212").val('dvdv');
			$('#category_list').html(data);
			$("#load-more").hide();
		}
	});
}

function add_active_category(id,cat_name){
	
	$("#dropdown").removeClass('open');
	$("#all_category_type li").each(function(e){
		$(this).find("label").removeClass('active');
	});
	$("#checkbox"+id).addClass('active');
	get_child_cat(id,cat_name);
	
}

function filter_products(search_key){
	var search = '';
	search = (typeof search_key !== 'undefined') ?  search_key : search;
	arrCategorySelected = [];
	arrBrandSelected = [];
	arrSizeSelected = [];
	arrCatAttrSelected = [];
	intPriceSelected = 0;
	intSortBy = 0;
	$("#content-categories li").each(function(e){
		if($(this).find("label").hasClass("active")){
			arrCategorySelected.push($(this).find("label").attr("attr-value"));
		}
	});
	
	$("#content-brands li").each(function(e){
		if($(this).find("label").hasClass("active")){
			arrBrandSelected.push($(this).find("label").attr("attr-value"));
		}
	});
	
	$("#content-size li").each(function(e){
		if($(this).find("label").hasClass("active")){
			arrSizeSelected.push($(this).find("label").attr("attr-value"));
		}
	});
	
	$("#content-price li").each(function(e){
		if($(this).find("label").hasClass("active")){
			intPriceSelected = $(this).find("label").attr("attr-value");
		}
	});
	
	$(".content-category-attr li").each(function(e){
		if($(this).find("label").hasClass("active")){
			arrCatAttrSelected.push($(this).find("label").attr("attr-value"));
		}
	});
	$('#sortby li').each(function(e){
		if($(this).find("label").hasClass("active")){
			intSortBy = $(this).find("label").attr("attr-value");
		}
	});
	var cat_id = $('#cat_id').val();
	var offset = $('#offset').val();
	var offset = parseInt(offset)+parseInt(1);
	if(offset<1){offset =1;}
	$('#arrCategorySelected').val(arrCategorySelected);
	$('#arrBrandSelected').val(arrBrandSelected);
	$('#arrSizeSelected').val(arrSizeSelected);
	$('#arrCatAttrSelected').val(arrCatAttrSelected);
	$('#intPriceSelected').val(intPriceSelected);
	$('#intSortBy').val(intSortBy);
	search = $('#search').val();
	
	$("#page-loader").show();
	$.ajax({
		url: sc_baseurl+'Category/filter_products',
		type: 'post',
		data: { 'cat_id' : cat_id ,'lastlevelcat' : arrCategorySelected,'brandId' : arrBrandSelected,'sizeId' : arrSizeSelected,'attributesId' : arrCatAttrSelected,'priceId' : intPriceSelected,'sort_by' : intSortBy,'offset' : offset,'search' : search},
		success: function(data, status) {
			$('#filter_product').html(data);
			$("#page-loader").hide();
			$('.scrollbar').perfectScrollbar();
		}
	});
	
}


function get_more_filter_products(){
	
	var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());
	if(intCurrentScrollPos<intTriggerScroll && !isTriggered){
		isTriggered=true;
		var cat_id = $('#cat_id').val();
		var offset = $('#offset').val();
		var offset = parseInt(offset)+parseInt(1);
		arrCategorySelected = $('#arrCategorySelected').val();
		arrBrandSelected = $('#arrBrandSelected').val();
		arrSizeSelected = $('#arrSizeSelected').val();
		arrCatAttrSelected = $('#arrCatAttrSelected').val();
		intPriceSelected = $('#intPriceSelected').val();
		intSortBy = $('#intSortBy').val();
		search = $('#search').val();
		
		var total_filter_products = $('#total_filter_products').val();
		
		if(parseInt(offset)*parseInt(20)<total_filter_products){
			$("#load-more").show();
			$.ajax({
				url: sc_baseurl+'Category/get_more_filter_products',
				type: 'post',
				data: { 'cat_id' : cat_id ,'lastlevelcat' : arrCategorySelected,'brandId' : arrBrandSelected,'sizeId' : arrSizeSelected,'attributesId' : arrCatAttrSelected,'priceId' : intPriceSelected,'sort_by' : intSortBy,'offset' : offset,'search' : search},
				success: function(data, status) {
					$('#get_all_cat_products').append(data);
					isTriggered=false;
					var new_offset = parseInt(offset)+parseInt(1);
					$('#offset').val(new_offset);
					$("#load-more").hide();
				}
			});
		}
		
	}else{
		// alert('something went wrong please try again');
	}
}

function GetProductSize(container){
	var intProdID = $(container).attr("id");
	var strProdSize = $(container).attr("data-sizes");
	var strProdLabel = $(container).attr("data-label");
	var GAproductname = $(container).attr('data-productname');
	var GAbrandname = $(container).attr('data-brandname');
	var GAproductprice = $(container).attr('data-productprice');
	//alert(intProdID+" :: "+intProdSize+" :: "+intProdLabel);
	$.ajax({
		url: sc_baseurl+'Category/get_size_html',
		type: 'post',
		data: { 'intProdID' : intProdID ,'strProdSize' : strProdSize,'strProdLabel' : strProdLabel},
		success: function(data, status) {
			$('#select-size-html').html(data);
			$("#modal-select-size-filter").modal('show');
			ga('send', 'event', 'Product', 'add_to_cart',GAbrandname+'-'+GAproductname,GAproductprice);
			/*Added Fb pixel Addtocart on 31-05-2017*/
			fbq('track', 'AddToCart');
		}
	});
	
}

function add_active_class_size(product_id,size_id){
	$( ".productsize"+product_id ).each( function( index, element ){
		$( ".productsize"+product_id ).removeClass('active');
	});
	$("#product_size"+size_id).addClass('active');
	add_to_cart(product_id,size_id);	
	data = 'Item added to your cart';
	$('#common-message-text').html(data);
	$("#modal-select-size-filter").modal('hide');
	$("#common-modal-alert").modal('show');
	_targetProductClick(product_id,'1');
	
	//added for facebook add_to_cart
	
	var productname = $("#"+product_id).attr("data-productname");
	var category_name = $("#"+product_id).attr("data-category");
	var content_ids = product_id;
	var value = $("#"+product_id).attr("data-productprice");

	fbq('track', 'AddToCart', {
        content_name: productname ,
        content_category: category_name,
        content_ids: [content_ids],
        content_type: 'product',
        value: value,
        currency: 'INR'
      }); 

	// added for facebook add_to_cart
	
	var GAbrandname = $("#"+product_id).attr('data-brandname');
	//added for ga add_to_cart
		ga('send', 'event', 'Product', 'add_to_cart',GAbrandname+'-'+productname,value);
	//added for ga add_to_cart

}

function clear_filter(page_form){
	
	//gender from collection page
	$('#content-gender li').click(function(e){
		$('#content-gender li label').removeClass('active');
	});
	
	$("#content-categories li").each(function(e){
		$(this).find("label").removeClass("active");
	});
	
	$("#content-brands li").each(function(e){
		$(this).find("label").removeClass("active");
	});
	
	$("#content-price li").each(function(e){
		$(this).find("label").removeClass("active");
	});
	
	$("#content-size li").each(function(e){
		$(this).find("label").removeClass("active");
	});
	
	$(".content-category-attr li").each(function(e){
		$(this).find("label").removeClass("active");
	});
	$('#sortby li').each(function(e){
		$(this).find("label").removeClass("active");
	});
	if(page_form == 'collection_filter'){
		filter_collection_products();
	}else if(page_form == 'collection_filter_gender'){
		return true;
	}else{
		filter_products();
	}
	
}

function filter_collection_products(){
	arrCategorySelected = [];
	arrBrandSelected = [];
	arrSizeSelected = [];
	arrCatAttrSelected = [];
	intPriceSelected = 0;
	intSortBy = 0;
	gender_cat_id = 0;
	
	$("#content-gender li").each(function(e){
		if($(this).find("label").hasClass("active")){
			gender_cat_id = $(this).find("label").attr("attr-value");
		}
	});
	
	$("#content-categories li").each(function(e){
		if($(this).find("label").hasClass("active")){
			arrCategorySelected.push($(this).find("label").attr("attr-value"));
		}
	});
	
	$("#content-brands li").each(function(e){
		if($(this).find("label").hasClass("active")){
			arrBrandSelected.push($(this).find("label").attr("attr-value"));
		}
	});
	
	$("#content-size li").each(function(e){
		if($(this).find("label").hasClass("active")){
			arrSizeSelected.push($(this).find("label").attr("attr-value"));
		}
	});
	
	$("#content-price li").each(function(e){
		if($(this).find("label").hasClass("active")){
			intPriceSelected = $(this).find("label").attr("attr-value");
		}
	});
	
	$(".content-category-attr li").each(function(e){
		if($(this).find("label").hasClass("active")){
			arrCatAttrSelected.push($(this).find("label").attr("attr-value"));
		}
	});
	$('#sortby li').each(function(e){
		if($(this).find("label").hasClass("active")){
			intSortBy = $(this).find("label").attr("attr-value");
		}
	});
	var object_id = $('#object_id').val();
	var offset = 0;
	$('#arrCategorySelected').val(arrCategorySelected);
	$('#arrBrandSelected').val(arrBrandSelected);
	$('#arrSizeSelected').val(arrSizeSelected);
	$('#arrCatAttrSelected').val(arrCatAttrSelected);
	$('#intPriceSelected').val(intPriceSelected);
	$('#intSortBy').val(intSortBy);
	$('#gender_cat_id').val(gender_cat_id);
	
	$("#page-loader").show();
	$.ajax({
		url: sc_baseurl+'Collection_web/filter_collection_products',
		type: 'post',
		data: { 'object_id' : object_id ,'lastlevelcat' : arrCategorySelected,'brandId' : arrBrandSelected,'sizeId' : arrSizeSelected,'attributesId' : arrCatAttrSelected,'priceId' : intPriceSelected,'sort_by' : intSortBy,'offset' : offset,'gender_cat_id' : gender_cat_id },
		success: function(data, status) {
			$('#filter_collection_product').html(data);
			$("#page-loader").hide();
			offset = 1;
			$('#offset').val(offset);
			$('.scrollbar').perfectScrollbar();
		}
	});
	
}