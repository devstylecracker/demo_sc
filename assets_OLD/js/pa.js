var getUrl = window.location;
// var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1]+ "/";
var baseUrl = 'http://localhost/projectbitb/';
///document.ready
var user_body_shape= 0;
var arr_user_style_pref = new Array();
var arr_user_brand_list = new Array();


$( document ).ready(function(){
	
	//##STEP 1-------------------------------------------------------------------
	$('#save_img').click(function(e){
		e.preventDefault();
		$('#slide2').addClass('hide');
		$('#slide3').removeClass('hide');
		scrolltop();
		//1.## When Body Shape Screen Shown -------------------------------------
		InitSectionCheck("body_shape");
	});
	
	//##STEP 1-------------------------------------------------------------------
	$('#takeQuiz').click(function(e){
		e.preventDefault();
		$('#slide3').addClass('hide');
		$('#slide4').removeClass('hide');
		scrolltop();
		//1.## When Body Shape Screen Shown -------------------------------------
		InitSectionCheck("body_shape");
	});
	
	//##STEP 2-------------------------------------------------------------------
	$('#body_shape_next').click(function(e){
		e.preventDefault();		
		if(InitSectionCheck("body_shape")>=1){
			user_body_shape = InitSectionCheck("body_shape");
			$('#slide4').addClass('hide');
			$('#slide5').removeClass('hide');
			scrolltop();
			//2.## When Body Style Preferences Screen Shown --------------------
			InitSectionCheck("body_style");
			InitSectionBodyStyle();
			//##End ------------------------------------------------------------
			msg("user_body_shape: "+user_body_shape);
		} else {
			var message = 'Please select the body shape.';
			$('#common-message-text').html(message);
			$("#common-modal-alert").modal('show');
			
		}
	});
	
	//##STEP 3-------------------------------------------------------------------
	$('#body_style_next').click(function(e){
		e.preventDefault();
		if(InitSectionCheck("body_style")==3){
			
			$(".box-style-selected li").each(function(e){
				msg($(this).find(".close").attr("style-id"));
				arr_user_style_pref.push($(this).find(".close").attr("style-id"));
			});
			msg("arr_user_style_pref: "+arr_user_style_pref);
			$('#slide5').addClass('hide');
			$('#slide6').removeClass('hide');
			scrolltop();
		} else {
			var message = 'Please select the body style preference.';
			$('#common-message-text').html(message);
			$("#common-modal-alert").modal('show');
		}
	});
	//##STEP 4 ------------------------------------------------------------------
	$('#get_started').click(function(e){
		arr_user_brand_list = [];
		FetchBrandList();
		save_pa();
	});
	
	$(function() {
	$('.image-editor').cropit({
	  imageState: {
		src: 'http://lorempixel.com/500/400/',
	  },
	});

	$('.rotate-cw').click(function() {
	  $('.image-editor').cropit('rotateCW');
	});
	$('.rotate-ccw').click(function() {
	  $('.image-editor').cropit('rotateCCW');
	});

	$('.export').click(function() {
	  var imageData = $('.image-editor').cropit('export');
	  window.open(imageData);
	});
	});
	
	//##Skip img back button-------------------------------------------------------------------
	$('#skip_img').click(function(e){
		e.preventDefault();
		$('#slide2').addClass('hide');
		$('#slide3').removeClass('hide');
		
	});
	
	//##body_shape back button-------------------------------------------------------------------
	$('#body_shape_back').click(function(e){
		e.preventDefault();
		$('#slide4').addClass('hide');
		$('#slide3').removeClass('hide');
		
	});
	
	//##body_style back button-------------------------------------------------------------------
	$('#body_style_back').click(function(e){
		e.preventDefault();
		$('#slide5').addClass('hide');
		$('#slide4').removeClass('hide');
		
	});
	
	//##brand back button-------------------------------------------------------------------
	$('#brand_back_btn').click(function(e){
		e.preventDefault();
		$('#slide6').addClass('hide');
		$('#slide5').removeClass('hide');
		
	});
	
//EDIT INIT---------------------------------------------------\
msg("getUrl: "+getUrl);


var intEditParam = getUrl.toString().substr(-1);

	if(intEditParam!=""){
		if(intEditParam==1 || intEditParam==2 || intEditParam==3 || intEditParam==4 || intEditParam==5)
		{
			EditPA(intEditParam);
		}else
		{
			msg("New PA");
		}
		
	}

});



var arrValues = [];

function EditPA(id){

	var body = Number($("#slide4").attr("user-body-attr"));
	var style = $("#slide5").attr("user-style-attr");
	var brand = $("#slide6").attr("user-brand-attr");
	var brandname = $("#slide6").attr("user-brand-name");	

	if(id ==1){
		msg("Otp Check.")
		$('#slide1').removeClass('hide');
	}
	else if(id ==2){
		msg("Profile Edit Section.")
		$('#slide2').removeClass('hide');
		$('#slide4').addClass('hide');	
	}
	else if(id ==3){
		msg("Body Shape Edit Section.");
		msg(body);
		$('#slide2').addClass('hide');
		$('#slide5').addClass('hide');		
		$('#slide4').removeClass('hide');
		InitSectionCheck("body_shape");			

		$(".box-body-shape > div > div").each(function(e){
			$(this).removeClass('active');
			if(body == Number($(this).attr('data-attr')))
			{
				//msg('sdas=='+Number($(this).attr('data-attr')));
				//add_active_class('body_shape',body);
				$(this).addClass('active');
				$(this).find('#oignal_img'+body).addClass('hide');
				$(this).find('#selected_img'+body).removeClass('hide');
			}
				
		});
	}
	else if(id ==4){
		msg("Style Edit Section.");
		$('#slide2').addClass('hide');		
		$('#slide4').addClass('hide');
		$('#slide5').removeClass('hide');			
		InitSectionCheck("body_style");
		InitSectionBodyStyle();

		var style = style.split(",");
		var i =0;

		$("#slide5 .row > div").each(function(e){	
			var intID = Number($(this).attr("id"));
			if(intID == style[0]) {
				$(this).trigger("click");
			}
			if(intID == style[1]) {
				$(this).trigger("click");
			}
			if(intID == style[2]) {
				$(this).trigger("click");
			}
		});

	}	
	else if(id ==5){
		msg("Wardrobe Edit Section.");
		$('#slide2').addClass('hide');
		$('#slide4').addClass('hide');
		$('#slide5').addClass('hide');
		$('#slide6').removeClass('hide');

		var brand = brand.split(",");
		var brandname = brandname.split(",");
		
		for (var i = 0; i < brand.length; i++) { 		   
		    arr_user_brand_list.push(brand[i]);
	    	add_in_list(brand[i],brandname[i]);		  
		}	
		msg(arr_user_brand_list);
	}
}




///document.ready

//Add BRAND list --------------------------
function add_in_list(brand_id,brand_name){
	
	if(brand_id!='' && brand_name!=''){
		$('#selected_brands').append('<span id="'+brand_id+'" class="tag active">'+brand_name+'<i onclick = "remove_brand('+brand_id+');" class="fa fa-close"></i></span>');
		$('#brand_list_'+brand_id).addClass('hide');
	}
	
}

function remove_brand(brand_id){
	$("#"+brand_id).remove();
	$('#brand_list_'+brand_id).removeClass('hide');
	
}

function add_active_class(type,id){
	
	if(type == 'body_shape'){
		old_id = parseInt($('#body_shape_ans').val());
		$('#body_shape_ans').val(id);
		$(".box-body-shape > div > div").each(function(e){
			$(this).removeClass('active');
			if(old_id != id){
				$("#oignal_img"+old_id).removeClass('hide');
				$("#selected_img"+old_id).addClass('hide');
			}
			if(id == Number($(this).attr('data-attr')))
			{
				$(this).addClass('active');
				$("#oignal_img"+id).addClass('hide');
				$("#selected_img"+id).removeClass('hide');
			}
				
		});

	}
}

$(function() {
	   //$( "#sortable").sortable();
    });




//## Select Body Preferences --------------------
var intStyleSelectLimit = 0;
var isStyleSelectetElementRemoved = false;
var isNextContainerAllocated=false;
var isFirstTimeBodyStyleSelected = true;
var nextContainer;
function InitSectionBodyStyle(){
	$("#slide5").find(".row > div").on("click touch", function(){

	if(!$(this).hasClass("active")) {
			if(intStyleSelectLimit<=2){
				$(".box-style-selected .pref").each(function(e){
					if(!$(this).hasClass("filled") && !isNextContainerAllocated) {
						nextContainer = $(this)
						isNextContainerAllocated = true;
					}
					
				});
				isNextContainerAllocated = false;
				intStyleSelectLimit++;
				$(this).addClass("active");
				var img = $(this).find('img').attr("src");
				var intStyleID = $(this).attr("id");
		
				if(isFirstTimeBodyStyleSelected) {
					nextContainer = $(".box-style-selected .pref").first();
					$(nextContainer).find(".text").hide();
					$(nextContainer).find(".shape-diamond").show();
					$(nextContainer).find(".close").attr("style-id",intStyleID);
					$(nextContainer).find(".close").show();
					$(nextContainer).find("img").attr("src",img);
					$(nextContainer).addClass("filled");
					isFirstTimeBodyStyleSelected = false;
				} else {
					$(nextContainer).find(".text").hide();
					$(nextContainer).find(".shape-diamond").show();
					$(nextContainer).find(".close").attr("style-id",intStyleID);
					$(nextContainer).find(".close").show();
					$(nextContainer).find("img").attr("src",img);
					$(nextContainer).addClass("filled");	
				}
			} else{
				var message = 'You can only select 3 preferences.';
				$('#common-message-text').html(message);
				$("#common-modal-alert").modal('show');
			}
			msg("intStyleSelectLimit: "+intStyleSelectLimit);
			//user_style_p1
		}
	});
}

//## Remove Body Preferences --------------------
$(".box-style-selected .pref .close").on("click touch", function(e){
	intStyleSelectLimit--;
	var intStyleID = $(this).attr("style-id");
	$(this).closest("li").find("img").attr("src","");
	$(this).closest("li").find(".text").show();
	$(this).closest("li").find(".shape-diamond").hide();
	$(this).closest("li").find(".close").hide();
	$(this).closest("li").removeClass("filled");
	//
	isStyleSelectetElementRemoved = true;
	$("#slide5").find(".row").find("#"+intStyleID).removeClass("active");
});

//## function to check the section --------------
function InitSectionCheck(type){
	var selectedItem = 0;
	if(type == 'body_shape'){
		$("#slide4").find(".row >div").each(function( index ) {
		  if($(this).hasClass("active")){
			  selectedItem = $(this).attr("data-attr");
		  }
		});
	} else if(type == "body_style"){
		$("#slide5").find(".box-style-selected > li").each(function( index ) {
		  if($(this).hasClass("filled")){
			  selectedItem++;
		  }
		});
	}
	return selectedItem;
}

function FetchBrandList(){
	$("#selected_brands span").each(function(e){
		arr_user_brand_list.push($(this).attr("id"));
	});
	msg("arr_user_brand_list: "+arr_user_brand_list);
}


function msg(m) { console.log(m);}

function save_pa(){
	
		$.ajax({
			url: sc_baseurl+'Schome_new/save_pa',
			type: 'post',
			data: { 'user_body_shape' : user_body_shape,'arr_user_style_pref' : arr_user_style_pref,'arr_user_brand_list' : arr_user_brand_list },
			success: function(data, status) {
				// alert(baseUrl+"schome_new");
				window.location = baseUrl+"schome_new";
			}
		});
	
}

function save_img(){
	var imageData = $('#image-cropper').cropit('export');
	$("#page-loader").show();
	$.ajax({
			url: sc_baseurl+'Schome_new/save_profile_img',
			type: 'post',
			data: { 'imageData' : imageData },
			success: function(data, status) {
					$("#page-loader").hide();
					$('#common-message-text').html(data);
					$("#common-modal-alert").modal('show');
			}
		});
}

function updatepa(type)
{

	if(type!='')
	{
		if(type=="body_shape")
		{	
			InitSectionCheck("body_shape");		
			user_body_shape = 0;
			$(".box-body-shape .item-col").each(function(){
				if($(this).hasClass("active")){
					user_body_shape = $(this).attr("data-attr");
				}

			});


			$.ajax({
				url: sc_baseurl+'Schome_new/update_pa',
				type: 'post',
				data: { 'type' : type ,'answer' : user_body_shape },
				success: function(data, status) {
					 window.location = baseUrl+"profile";
					
				}
			});
		}else if(type=="body_style")
		{
			InitSectionCheck("body_style");		
			
				arr_user_style_pref = [];

				$("#sortable li").each(function(e){
					arr_user_style_pref.push($(this).find(".close").attr("style-id"));
				});

				msg('arr_user_style_pref --'+arr_user_style_pref);

			$.ajax({
				url: sc_baseurl+'Schome_new/update_pa',
				type: 'post',
				data: { 'type' : type ,'answer' : arr_user_style_pref },
				success: function(data, status) {
					window.location = baseUrl+"profile";
				
				}
			});
		}else if(type=="brandlist")
		{
			arr_user_brand_list = [];
			FetchBrandList();			
			$.ajax({
				url: sc_baseurl+'Schome_new/update_pa',
				type: 'post',
				data: { 'type' : type ,'answer' : arr_user_brand_list },
				success: function(data, status) {
					
					 window.location = baseUrl+"profile";			
				}
			});
		}else if(type=="profilepic")
		{
			/*var imageData = $('#image-cropper').cropit('export');
			$("#page-loader").show();
			$.ajax({
					url: sc_baseurl+'Schome_new/save_profile_img',
					type: 'post',
					data: { 'imageData' : imageData },
					success: function(data, status) {
							$("#page-loader").hide();
							$('#common-message-text').html(data);
							$("#common-modal-alert").modal('show');
							window.location = baseUrl+"profile";	
					}
				});*/
			window.location = baseUrl+"profile";
			// location.reload();
		}
	}
}
