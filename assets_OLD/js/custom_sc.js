  var getUrl = window.location;
  var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1]+ "/";
  ///document.ready
$( document ).ready(function(){
  var objParam = getUrl.toString().split("/");
  var lastparam = objParam[objParam.length-2]; 

  if($('#signup_error').attr('signup-error')!='')
  {
    $(".signup-by-email").show();
    $(".signup-options").hide();
  }

 /*Sign up form Validation*/
  $("#scSignup").validate({
     focusInvalid: false,
     ignore: [],
     rules: {     
      sc_name : { required : true, minlength:1 },      
      sc_gender: { required: true },
      sc_emailid:{  required : true, email :true },
      sc_password: { required : true,minlength:8,maxlength:20 },
      sc_mobile: { required : true,minlength:10,maxlength:10,number:true },
      sc_dob: {
        required: function(element) {               
          if(dobValidate()) { return true;}else return false;          
        }
      }
    },
     messages: {      
      sc_name : {
        required : "Please enter your full name"
      },     
      sc_dob : {
        required : "Please select date of birth"
      },
      sc_emailid:{
        required : "Please enter your email ID",email :"Please enter a valid email ID"
      },
      sc_password:{ required : "Please enter password", minlength: "Password should have atleast 8 characters",maxlength: "Password should not greater than 20 characters"
       },
      sc_mobile:{ required : "Please enter mobile no.", minlength: "Enter valid mobile no.",maxlength: "Enter valid mobile no."
       },
      sc_gender : {
        required : "Please select gender"
      }
    },
    submitHandler: function(form) { 
      ga('send', 'event', 'Sign Up', 'source', 'email');
      var sc_name = $('#sc_name').val().trim();
      var  sc_first_name = '';
      var sc_last_name = '';
      sc_name = sc_name.replace(/\s+/g, " ");
      var res = sc_name.split(" ");
       sc_first_name = res[0];
        if(res[1]!='')
        {
          sc_last_name = res[1];
          $('#sc_name-error').addClass('hide');
        }else
        {
          $('#sc_name-error').removeClass('hide');
        }
      if(sc_first_name!='' && sc_last_name!='')
      {
        sc_signIn();
      }      
      //##GA Event add for Signup Link on 15-Jan-16 (as per Veelas email)
      ga('send', 'pageview', '/overlay/signup/?page=' + document.location.pathname + document.location.search + ' - Sign Up');

    }
  });

/*Sign up form Validation for promo */
   $("#scSignupedit").validate({
       rules: {     
      sc_name1 : { required : true ,minlength:1},
      sc_dob : { required : true },
      sc_gender1: { required: true },
      sc_emailid1:{  required : true, email :true },
      sc_password1: { required : true,minlength:8 },
      sc_mobile1: { required : true,minlength:10,maxlength:10,number:true }
    },
     messages: {     
      sc_name1 : {
        required : " Please enter your full name"
      },
      sc_agerange1 : {
        required : "Please select age range"
      },
      sc_gender1 : {
        required : "Please select gender"
      },
      sc_emailid1 :{
        required : "Please enter your email ID",email :"Please enter a valid email ID"
      },
      sc_password1 :{ required : "Please enter password", minlength: "Password should have atleast 8 characters",maxlength: "Password should not greater than 20 characters"
       },
      sc_mobile1 :{ required : "Please enter mobile no.", minlength: "Enter valid mobile no.",maxlength: "Enter valid mobile no."
       }
    },
    submitHandler: function(form) {
      sc_signInedit();    
    }
    });

    /*Login form Validation*/
    $("#scnewLogin").validate({
       rules: {
        logEmailid:{  required : true, email :true },
        logPassword: { required : true,minlength:8,maxlength:20 }
      },
       messages: {
        logEmailid:{
          required : "Please enter your email ID",email :"Please enter a valid email ID"
        },
        logPassword:{ required : "Please enter password", minlength: "Password should have atleast 8 characters",maxlength: "Password should not greater than 20 characters"
         }
      },
      submitHandler: function(form) {
        sc_login();
      }
    });
 
  
    $("#scnewLogin:input").focus(function() {     
      $('#logIn_error').addClass('hide');    
    });

    //facebook Initialization for Signup
    $('#facebook_login_in').click(function(e) {
      e.preventDefault();
      facebookCommon('signup');
    });
    //facebook Initialization for login
    $('#facebook_login').click(function(e) {
      e.preventDefault();
      facebookCommon('login');
    });

    $('#gender-female').click(function(e){
      $('#gender-male').removeClass('active');
      $('#gender-female').addClass('active');
      $('#sc_gender').val('1');   
      $('#sc_gender1').val('1');   
      $('#sc_gender-error').addClass('hide');
      $('#sc_gender-error1').addClass('hide');     
    });    

    $('#gender-male').click(function(e){
      $('#gender-female').removeClass('active');
      $('#gender-male').addClass('active');
      $('#sc_gender').val('2');
      $('#sc_gender1').val('2');
      $('#sc_gender-error').addClass('hide');
      $('#sc_gender-error1').addClass('hide');          
    });

    $('#sc_rpsubmit').click(function(e){
      var oldpwd = $('#oldpwd').val();
      var newpwd = $('#newpwd').val();
      if(oldpwd!='' && newpwd!='')
      {
          $.ajax({
            url: sc_baseurl+'/Profiledit/resetPassword',
            type: 'post',
            data: { 'type' : 'reset_pwd' ,'oldpwd' : oldpwd , 'newpwd' : newpwd  },
            success: function(data, status) {
                
                  $('#resetpwd_error').html(data);                  
                  
                  if(data=='Your Password has been reset')
                  {
                    $('#resetpwd_error').removeClass('text-error');
                    $('#resetpwd_error').addClass('text-success');
                  }
                 
                  $('#resetpwd_error').removeClass('hide');
                }              
          });
      }else
      {
          $('#resetpwd_error').html('');
          $('#oldpwd_error').removeClass('hide');
          $('#newpwd_error').removeClass('hide');
      }

    });
    
  // get notification
  if(parseInt(sc_user_id) >0){    
    $.ajax({
      url: sc_baseurl+'schome/get_all_notification',
      type: 'post',
      data: {'type' : 'web'},
      success: function(data, status) {
      $('#notification_html').html(data);
      $('.scrollbar').perfectScrollbar();
      },
      error: function(xhr, desc, err) {
      // console.log(err);
      }
    });
  }

});
///document.ready

//function to customise facebook error message for signup and login
  function facebookCommon(type){
    if(type=='signup')
    {
      ga('send', 'event', 'Sign Up', 'source', 'facebook');
    }else if(type=='login')
    {
      ga('send', 'event', 'Login', 'source', 'facebook');
    }
    
    FB.login(function(response) {
      //console.log(response);
      if(response.authResponse) {
        uid = response.authResponse.userID;
        accessToken = response.authResponse.accessToken;
        FB.api('/me?fields=id,about,birthday,location,gender,picture,name,relationship_status,religion,email,first_name,last_name,friends.fields(id,name)', function(response) {
          //console.log(response);
          response = JSON.stringify(response);
          facebook_signIn(response,type);
          ufCheckLike(response);

        });
      }
    },{scope: 'email,user_likes,user_birthday,user_location,user_work_history,user_hometown,user_photos'}); //permissions for facebook
  }

/* Sign up start*/
  function facebook_signIn(response,type){
     ga('send', 'event', 'Sign Up', 'source', 'facebook');
    ga('send', 'pageview', '/overlay/fb-signup/?page=' + document.location.pathname + document.location.search + ' - Signup');
    if(type == 'signup') type= 'sc_signup_error'; else type= 'login_error';

    $.ajax({
      url: sc_baseurl+'/fb/via_facebook_signin',
      type: 'post',
      data: {'type' : 'via_facebook_signin', 'response':response },
      success: function(data, status) {       
        if(data!=''){
          if(data=='pa'){
            window.location.href=sc_baseurl+"schome/pa";
          }else if(data == 'profile'){
            window.location.href=sc_baseurl+"schome";
          }else{
            $('#sc_signup_error').html("Unexpected error. Please try again!");
          }
        }else{

        }
        event.preventDefault();
      },
      error: function(xhr, desc, err) {
        // console.log(err);
      }
    });
  }

  isLoggedIn = function() {
    if (!accessToken) {
      divLogin.style.display = "";
      return false;
    }
    return true;
  }

  ufCheckLike = function(response1) {
    response = new Array();
    response['personel'] = response1;   
    if (isLoggedIn()) {
      FB.api(
        '/me/likes?' +
        'access_token=' + accessToken,
        'get',
        function(likes) {
          if (likes) {
            response['like']= JSON.stringify(likes);          
            ufCheckfriendlist(response);
          }
        }
      );
    }
  }

  ufCheckfriendlist = function(responseAll) {
    var result = new Array();
    result = responseAll;
    if (isLoggedIn())
    {
      FB.api(
        "/me/friends?fields=name,first_name,picture,email&",+
        'access_token=' + accessToken,
        'get',
        function (response) {
          if (response)
          {
            response = JSON.stringify(response);        
            result['freinds'] = response;         
          }
        }
      );
    }
  }
/* Sign up end*/

 //function for google signup and login 
  function google_signIn(){
    ga('send', 'event', 'Sign Up', 'source', 'gplus');
    ga('send', 'pageview', '/overlay/gplus-signup/?page=' + document.location.pathname + document.location.search + ' - Signup');
    $.ajax({
      url: sc_baseurl+'schome/register_user_google_signin',
      type: 'post',
      data: {'type' : 'get_google_signin' },
      success: function(data, status) {
        if(data!=''){
          if(data=='pa'){
            window.location.href=sc_baseurl+"schome/pa";
          }else if(data == 'feed'){
            window.location.href=sc_baseurl+"schome";
          }else{
            //$('#sc_signup_error').html(data);
            $('#sc_signup_error').html(data);
          }
        }else{
        }
        return false;
        event.preventDefault();
      },
      error: function(xhr, desc, err) {      
      }
    });
  }
/// function for google signup and login 

// function for log-in
  function sc_login(){
    var logEmailid = $('#logEmailid').val();
    var logPassword = $('#logPassword').val();
    var logRemember;
    $('#logIn_error').addClass('hide');
    if($('#rememberMe').prop( "checked" ))
    {
      logRemember = '1';
    }else
    {
      logRemember = '0';
    }

    ga('send', 'event', 'Login', 'source', 'email', sc_user_id);

    $("#sc_Login").addClass( "disabled" );
    $(".fa-loader-wrp-products").css({"display":"inline"});
    if(logEmailid!='' && logPassword!=''){

      var stylecracker_style = document.forms.scnewLogin;
       scnewLogin.submit();

      /*$.ajax({
        url: sc_baseurl+'schome/login',
        type: 'post',
        data: {'type' : 'get_login'},
        success: function(data, status) {
          if(data=='pa'){
            window.location.href=sc_baseurl+"schome/pa";
          }else if(data == 'feed'){
            window.location.href=sc_baseurl+"/feed";
          }else{
            $("#sc_Login").removeClass( "disabled" );
            $(".fa-loader-wrp-products").css({"display":"none"});
            $('#login_error').html(data);
          }

          return false;
          event.preventDefault();

        },
        error: function(xhr, desc, err) {
          // console.log(err);
        }
      });*/

    }
  }
/// function for log-in
// function for Signup
 function sc_signIn(){
    
    var sc_name = $('#sc_name').val().trim();
    var sc_dob = $('#sc_dob').val().trim();
    var sc_emailid = $('#sc_emailid').val().trim();
    var sc_password = $('#sc_password').val().trim();
    var sc_mobile = $('#sc_mobile').val().trim();  
    var sc_gender = $('#sc_gender').val();  

    $("#sc_signup").addClass( "disabled" );
    $(".fa-loader-wrp-products").css({"display":"inline"});

    sc_name = sc_name.replace(/\s+/g, " ");
    var res = sc_name.split(" ");
    sc_first_name = res[0];
    if(res[1]!='')
    {
      sc_last_name = res[1];
      $('#sc_name-error').addClass('hide');
    }else
    {
      $('#sc_name-error').removeClass('hide');
    }

    if(sc_first_name!='' && sc_last_name!='' && sc_emailid!='' && sc_password!='' && sc_gender!=''){ 
       var stylecracker_style = document.forms.scSignup;
       scSignup.submit();
        $(".fa-loader-wrp-products").css({"display":"none"});   
        //ga('send', 'event', 'Sign Up', 'source', 'email');   
    }
  }
/// function for Signup

// function for Signup edit
 function sc_signInedit(){
  
    var sc_name = $('#sc_name1').val();
    var sc_agerange = $('#sc_agerange1').val();
    var sc_emailid = $('#sc_emailid1').val();
    var sc_password = $('#sc_password1').val();
    var sc_mobile = $('#sc_mobile1').val();

    //var sc_username = $('#sc_username').val();
    var sc_gender = $('#sc_gender').val();   
    $("#sc_signup").addClass( "disabled" );
    $(".fa-loader-wrp-products").css({"display":"inline"});

    if(sc_emailid!='' && sc_name!='' && sc_agerange!=''){

       var stylecracker_style = document.forms.scSignupedit;
       scSignupedit.submit();
        $(".fa-loader-wrp-products").css({"display":"none"});
        //ga('send', 'event', 'Sign Up', 'source', 'email');
     
    }else if(sc_mobile!='' && sc_mobile!=saved_mobile)
    {
      window.location.href=sc_baseurl+"schome/pa/1";
    }else if(sc_gender!='' && sc_agerange!='')
    {
      window.location.href=sc_baseurl+"schome";
    }
  }
/// function for Signup edit

//Forgot-Passord form Validation
  $("#forgotpwdfrm").validate({
     rules: {
      Emailid:{  required : true, email :true }
    },
     messages: {
      Emailid:{
        required : "Please enter your email ID",email :"Please enter a valid email ID"
      }
    },
    submitHandler: function(form) {
      sc_forgotpwd();
    }
  });

  function sc_forgotpwd(){ 
    var Emailid = $('#Emailid').val();   
    if(Emailid!=''){
      $.ajax({
        url: sc_baseurl+'schome/forgotPassword',
        type: 'post',
        data: {'type' : 'forgot_pwd','Emailid':Emailid },
        success: function(data, status) {
          $('#loginfp_error').html(data);
        },
        error: function(xhr, desc, err) {  
        }
      });
    }
  }
//Forgot-Passord form Validation

// Validate dob
function dobValidate()
{   
    var dob = '';    
    var day = $('#day_start').val();
    var month = $('#month_start').val();
    var year = $('#year_start').val();
    dob = year+'-'+month+'-'+day;    
    $('#day_start').removeClass('invalid');  
    $('#month_start').removeClass('invalid');   
    $('#year_start').removeClass('invalid');   
    $('#sc_dob').val(dob);     
    return true;     
}

function generateOtp(mobile_no,type,userid)
{    
    ga('send', 'event', 'OTP', 'sent');
    var otp_number = $('#otp').val();
    if( (type=='check_otp' && otp_number!='' && mobile_no!='') || ( type=='resend_otp' && mobile_no!=''))
    {  
      $.ajax({
            url:sc_baseurl+'schome/sc_check_otp',
            type:'POST',
            data:{'type':type,'mobile_no':mobile_no,'otp_number':otp_number,'userid':userid},
            success: function(data,status){
              console.log(data);
              if(type=='check_otp')
              {               
                if(data==true)
                {
                  $('#otp-error').html('OTP verified successfully');
                  $('#otp-error').removeClass('hide');
                  $('#otp-error').removeClass('text-error');
                  $('#otp-error').addClass('text-success');

                  if(lastparam==10)
                  {
                    window.location.href=sc_baseurl+"/profile";
                  }else
                  {
                    $('#slide1').addClass('hide');
                    $('#slide2').removeClass('hide');
                  }                  
                  ga('send', 'event', 'OTP', 'confirmed');
                  
                }else
                {
                  $('#otp-error').html('Invalid OTP');                  
                  $('#otp-error').removeClass('text-success');
                  $('#otp-error').addClass('text-error');
                  $('#otp-error').removeClass('hide');
                  
                }
              }else if(type=='resend_otp')
              {
                if(data==true)
                {
                  $('#otp-error').html('OTP resend successfully');
                  $('#otp-error').removeClass('hide');
                  $('#otp-error').removeClass('text-error');
                  $('#otp-error').addClass('text-success');
                }else
                {
                  $('#otp-error').html('Error');
                  $('#otp-error').removeClass('text-success');
                  $('#otp-error').addClass('text-error');
                  $('#otp-error').removeClass('hide');
                }
              }

            },
            error:function(xhr,desc,err)
            {

            }
      });
    }else
    {
      $('#otp-error').html('Enter OTP');                  
      $('#otp-error').removeClass('text-success');
      $('#otp-error').addClass('text-error');
      $('#otp-error').removeClass('hide');

    }  
}

function changegender()
{
  var gender = $('#scbox_gender').val();
    
    if(gender!='' && gender==1)
    {
      scbox_getdata('bodyshape',gender);
      scbox_getdata('style',gender);
      scbox_getdata('sizetop',gender);
      scbox_getdata('sizebottom',gender);
      scbox_getdata('sizefoot',gender);
      scbox_getdata('sizeband',gender);
      scbox_getdata('sizecup',gender);
      $('#lingerie1').removeClass('hide');
      $('#lingerie2').removeClass('hide');
    }else
    {
      scbox_getdata('bodyshape',gender);
      scbox_getdata('style',gender);
      scbox_getdata('sizetop',gender);
      scbox_getdata('sizebottom',gender);
      scbox_getdata('sizefoot',gender);
      $('#lingerie1').addClass('hide');
      $('#lingerie2').addClass('hide');
    }
}

function GenderValidate()
{  
  if($('#sc_gender').val()=="") {  $("#sc_gender-error1").removeClass("hide"); return false; }else{
   $("#sc_gender-error1").addClass("hide"); return true;}             
}

function resetPassword()
{ 
  $("#modal-reset-password").modal('show');
}

function validateform()
{
  //$('#signup_error').addClass('hide');
}

function validateSignup(element)
{
  if($(element).attr('id') == 'sc_name')
  {
     var sc_name = $('#sc_name').val().trim();
     var  sc_first_name = '';
     var sc_last_name = '';
      sc_name = sc_name.replace(/\s+/g, " ");
      var res = sc_name.split(" ");
       sc_first_name = res[0];


      
        if( res[1]!='' &&  res[1]!=undefined)
        {
          sc_last_name = res[1];
         
          $('#sc_lastname-error').addClass('hide');
          $('#sc_signup').removeAttr('disabled');
        }else
        {  
          if(res[0]=='' && res[1]=='' &&  res[0]==undefined &&  res[1]==undefined)
          {
             $('#sc_lastname-error').addClass('hide');
             $('#sc_signup').attr('disabled','true'); 
          }else
          { 
            $('#sc_lastname-error').removeClass('hide');
            $('#sc_signup').attr('disabled','true');          
          }
       }
  }
  
}

$('#sc_name').keypress(function (e) {      
      var regex = new RegExp("^[a-zA-Z\\s\\b]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
          return true;
      }

      e.preventDefault();
      return false;
  });


