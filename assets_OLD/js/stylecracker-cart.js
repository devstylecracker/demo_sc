var intSelectedShippingID, intSelectedBillingID = 0;
var cartAmt = 0.0;
var couponClick = 0;

var objParam = getUrl.toString().split("/");
var lastparam = objParam[objParam.length-1];
var seclastparam = objParam[objParam.length-2];
var noloAddress = new Array();
var addcnt = 0;
var sess_userid = $('#sess_userid').val();
var page_type = '';

cartAmt = $('#cart_total').val();
cartAmt = parseFloat(Math.round(cartAmt * 100) / 100).toFixed(2);
	$('#payAmt').html(cartAmt);

$( document ).ready(function() {

	checkoutAccordian();
	 money_formatter(".price");
     money_formatter(".cart-price");
	

	$('#cart_facebook_login').click(function() {
       facebookCommon('login');
    });
	$('#shipping_pincode_no').trigger('blur');
	$('#billing_pincode_no').trigger('blur');

	cartAmt = $('#cart_total').val();
	cartAmt = parseFloat(Math.round(cartAmt * 100) / 100).toFixed(2);
	$('#payAmt').html(cartAmt);

	//## Ship and Bill Click event -----------------------------

	$("#user-exist-address li .btns-wrp label").on("click", function(e){ 
		if($(this).hasClass("ship-checkbox")){
			$("#user-exist-address li .btns-wrp").find(".ship-checkbox").removeClass("active");
			$(this).addClass("active");
			intSelectedShippingID = $(this).attr("attr-value");
			document.cookie = "shipping_id="+intSelectedShippingID+";path=/";
			changeAddress('ship');

		}
		else if($(this).hasClass("bill-checkbox")){
			$("#user-exist-address li .btns-wrp").find(".bill-checkbox").removeClass("active");
			$(this).addClass("active");
			intSelectedBillingID = $(this).attr("attr-value");
			document.cookie = "billing_id="+intSelectedBillingID+";path=/";
			changeAddress('bill');
		}
		msg("intSelectedShippingID: "+intSelectedShippingID);
		msg("intSelectedBillingID: "+intSelectedBillingID);

	});

	$(".default-address").click(function(e){
		if($(this).hasClass('active'))
		{
			$(this).removeClass('active');
		}else{
			$(this).addClass('active');			
		}

	});

	$('#user_address').keypress(function (e) {    	
    	var regex = new RegExp("^[a-zA-Z0-9\\-\\s\\b\/\\,\(\)]+$");
	    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
	    if (regex.test(str)) {
	        return true;
	    }

	    e.preventDefault();
	    return false;
	});
	

	/*$('sc-checkbox redeem').click(function(e){
		if($('#redeem').hasClass('active'))
		{
			$('#redeem').removeClass('active');	
		}else
		{
			$('#redeem').addClass('active');	
		}
		
	});*/

  
 if($('#codna').val() > 0){ 
    	console.log($('#codna').val());
    	$("input[type='radio'][id='sc_cart_pay_mode_cod']").parent().addClass('disabled');
    	$("input[type='radio'][id='sc_cart_pay_mode_cod']").attr('disabled',true);
    	$("input[type='radio'][name='sc_cart_pay_mode_cod']").prop('checked',false);
    }

    $('[data-toggle="popover"]').popover();
});

//##Payment mode event -----------------------
var payment_mode = "";
var err = 0;


function change_qty(type,id){
	if(type == 'min'){
		var proQty = $('#quantity'+id).val();
		if(proQty > 1){
			$('#quantity'+id).val(proQty-1);
		}
	}else{
		var proQty = $('#quantity'+id).val();
		if(proQty < 5){
			$('#quantity'+id).val(parseInt(proQty)+1);			
		}
	}
	allow_cart_update();
}

function allow_cart_update(){
	/*$('#scupdatecart').removeClass('hide');
	$('#scupdatecart').attr('disabled',false);
	$('#proceed_to_checkout').addClass('hide');
	$('#scupdatecart1').removeClass('hide');
	$('#scupdatecart1').attr('disabled',false);
	$('#proceed_to_checkout1').addClass('hide');*/
	sc_updatecart(0);
}

function scremovecart(id,container){

	var GAproductname = $(container).attr('data-productname');	
	var GAbrandname = $(container).attr('data-brandname');
	var GAproductprice = $(container).attr('data-productprice');

	var result = confirm("Are you sure you want to remove this item from your cart?");
	if(result){ 
		show_cart_loader();
		$('#cart_'+id).fadeOut( "slow", function() { $('#cart_'+id).remove(); } ); 
		$.ajax({
			url: sc_baseurl+"stylecart/scremovecart",
			type: 'POST',
			data: { id : id, 'page-type' : $('#page-type').val() },
			cache :true,
			async: true,
			success: function(response) {
				var cartcount = scUpdateCart(getCookie('SCUniqueID'),'','','','','');
				$('#item_count').val(cartcount);	
				if($('#item_count').val() == undefined || $('#item_count').val()==0){				
					
					if(cartcount==0)
					{
						$('#total_products_cart_count').addClass('hide');
						localStorage.setItem('newcartcount', cartcount);
						$('#item_count').val(cartcount);
						$('.cart-count').html(cartcount);
					}
					
				}else{					
					
					if(cartcount>0)
					{
						$('#total_products_cart_count').removeClass('hide');
					}else
					{
						$('#total_products_cart_count').addClass('hide');
					}
					localStorage.setItem('newcartcount', cartcount);
					//localStorage.setItem('newcartcount', $('#item_count').val());
					//$('.cart-count').html($('#item_count').val());
					$('#item_count').val(cartcount);
					$('.cart-count').html(cartcount);
				}

				if(lastparam=='cart' || seclastparam=='cart')
				{			
					$('#stylecart_product_view').html(response);
					//location.reload();
				}else
				{	
					$('#sccart_product_view').html(response);
				}
				money_formatter(".price");
    			money_formatter(".cart-price");
				hide_cart_loader();
				//cart_popover_popup();
				if($('#redeem').hasClass('active')){			
					$('#referral-price').removeClass('deactive');		
				}else{			
					$('#referral-price').addClass('deactive');	
				}
				
			},
			error: function(xhr) {				
				msg('Something goes wrong')
				hide_cart_loader();
			}
		});

		ga('send', 'event', 'Cart', 'removed', GAbrandname+'-'+GAproductname,GAproductprice);

	}
}

function sc_updatecart(a){	
	sccart_frm_submit(a);			
}

function show_cart_loader(){
	$('.fa-loader-wrp').show();
	$('.page-overlay').show();
}

function hide_cart_loader(){
	$('.fa-loader-wrp').hide();
	$('.page-overlay').hide();
}

function sccart_frm_submit(a){	
	show_cart_loader();	
	var pincode = $('#cart_pincode').val();
	
		$("#sccart_frm").on("submit", function (e) {		
	      	e.preventDefault();
	        $.ajax({
	            type: "post",
	            url: sc_baseurl+"stylecart/updatecart",
	            data: $(this).serialize()+'&pincode='+pincode,
	            success: function (response) {
	            	if(lastparam=='cart' || seclastparam=='cart')
					{	
						$('#stylecart_product_view').html(response);
						//location.reload();
					}else
					{
						$('#sccart_product_view').html(response);
					}
	            	
					localStorage.setItem('newcartcount', $('#item_count').val());
	            	$('.cart-count').html($('#item_count').val());
	                hide_cart_loader();
	                
					if(getCookie('discountcoupon_stylecracker')!=''){ $('#sccoupon_code').val(getCookie('discountcoupon_stylecracker')); $('#coupon_clear').removeClass('hide'); }else{ $('#coupon_clear').addClass('hide'); }
					money_formatter(".price");
     				money_formatter(".cart-price");
     				if($('#redeem').hasClass('active')){			
						$('#referral-price').removeClass('deactive');		
					}else{			
						$('#referral-price').addClass('deactive');	
					}
					
	            },
	            error: function (response) {
	            	hide_cart_loader();
	                //alert(response);
	                msg(response);
	            }
	        });
	    });
	
}

function cart_check_email(){
	show_cart_loader();
	var final_cookies = '';
	var cart_email = $('#cart_email').val();
	document.cookie="cart_email="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	//document.cookie = "cart_email="+cart_email;
	if(cart_email.trim()!=''){
		$.ajax({
			url: sc_baseurl+"stylecart/check_email_exist",
			type: 'POST',
			data: { cartemail : cart_email },
			cache :true,
			async: true,
			success: function(response) { 
				if(response==1){ /* Exist */
					document.cookie = "cart_email="+cart_email;
					$('#box-password').removeClass('hide');
					$('.email-label').html($('#cart_email').val());
					$('#cart_email').attr('readonly','true');
					$('#sc_checkout_login_panel').addClass('hide');
					$('#cart_password').focus();
					$('#cart_email_c').html(cart_email);
					$('#email_id_error').html();
					$('#div-checkout-address').addClass('open');
					$('#div-checkout-login').addClass('step-done');
					$('#email-label').html(getCookie('cart_email'));
					checkoutAccordian();

				}else if(response==0){ /* Not Exist*/
					document.cookie = "cart_email="+cart_email;
					$('.box-password-nonexist').removeClass('hide');					
					$('.cart-selected-value').removeClass('hide');
					$('#cart_email_c').html(cart_email);
					$('#email_id_error').html();
					$('#div-checkout-login').removeClass('open');
					$('#div-checkout-address').addClass('open');
					$('#div-checkout-login').addClass('step-done');
					$('#email-label').html(getCookie('cart_email'));
					checkoutAccordian();
				}else{

					$('#email_id_error').html(response).show();	
					$('#email_id_error').removeClass('hide');				
					$('#email-label').html(getCookie('cart_email'));

				}
				hide_cart_loader();
				
			},
			error: function(xhr) {
				//alert('Something goes wrong');
				msg('Something goes wrong');
				hide_cart_loader();
			}
		});
		
	}else{
		alert('Please enter email address');
		hide_cart_loader();
	}
}

function change_email(){ 
	show_cart_loader();
	$('#cart_email').focus();
	$('#cart_email').attr('readonly',false);
	$('#box-password').addClass('hide');
	$('#sc_checkout_login_panel').removeClass('hide');
	$('.cart-selected-value').addClass('hide');
	
	$('#collapse-options1').css('display','block');
	$('#collapse-options2').css('display','none');
	
	$('#cart_email').focus();
	hide_cart_loader();
}

function valiate_address(){
	show_cart_loader();
	var regexp = /[^a-zA-Z]/g;
	var regexp_mobile = /[^0-9]/g;
	var billing_first_name = $('#billing_first_name').val().trim();
	var billing_last_name = $('#billing_last_name').val().trim();
	var billing_mobile_no = $('#billing_mobile_no').val().trim();
	var billing_pincode_no = $('#billing_pincode_no').val().trim();
	var billing_address = $('#billing_address').val().trim();
	var billing_city = $('#billing_city').val().trim();
	var billing_state = $('#billing_state').val().trim();


	var shipping_first_name = $('#shipping_first_name').val().trim();
	var shipping_last_name = $('#shipping_last_name').val().trim();
	var shipping_mobile_no = $('#shipping_mobile_no').val().trim();
	var shipping_pincode_no = $('#shipping_pincode_no').val().trim();
	var shipping_address = $('#shipping_address').val().trim();
	var shipping_city = $('#shipping_city').val().trim();
	var shipping_state = $('#shipping_state').val().trim();


	var isTrue = 0;

	if(billing_first_name.match(regexp) || billing_first_name==''){
		$('#billing_first_name_error').html('Enter Valid First Name');
		isTrue = 1;
	}else{
		document.cookie = "billing_first_name="+billing_first_name+";path=/";
		$('#billing_first_name_error').html('');
		//isTrue = 0;
	}

	if(billing_last_name.match(regexp) || billing_last_name==''){
		$('#billing_last_name_error').html('Enter Valid Last Name');
		isTrue = 1;
	}else{
		document.cookie = "billing_last_name="+billing_last_name+";path=/";
		$('#billing_last_name_error').html('');
		//isTrue = 0;
	}


	if(billing_mobile_no.match(regexp_mobile) || billing_mobile_no=='' || billing_mobile_no.length < 10){
		$('#billing_mobile_no_error').html('Enter Valid Mobile No');
		isTrue = 1;
	}else{
		document.cookie = "billing_mobile_no="+billing_mobile_no+";path=/";
		$('#billing_mobile_no_error').html('');
		//isTrue = 0;
	}

	if(billing_pincode_no.match(regexp_mobile) || billing_pincode_no=='' || billing_pincode_no.length < 6){
		$('#billing_pincode_no_error').html('Enter Valid Pincode');
		isTrue = 1;
	}else{
		document.cookie = "stylecracker_billing_pincode="+billing_pincode_no+";path=/";
		$('#billing_pincode_no_error').html('');
		//isTrue = 0;
	}

	if(billing_address==''){
		$('#billing_address_error').html('Enter Billing Address');
		isTrue = 1;
	}else{
		document.cookie = "billing_address="+billing_address+";path=/";
		$('#billing_address_error').html('');
		//isTrue = 0;
	}

	if(billing_city==''){
		$('#billing_city_error').html('Enter Billing City');
		isTrue = 1;
	}else{
		document.cookie = "billing_city="+billing_city+";path=/";
		$('#billing_city_error').html('');
		//isTrue = 0;
	}

	if(billing_state==''){
		$('#billing_state_error').html('Enter Billing State');
		isTrue = 1;
	}else{
		document.cookie = "billing_state="+billing_state+";path=/";
		$('#billing_state_error').html('');
		//isTrue = 0;
	}


	if(shipping_first_name.match(regexp) || shipping_first_name==''){
		$('#shipping_first_name_error').html('Enter Valid First Name');
		isTrue = 1;
	}else{
		document.cookie = "shipping_first_name="+shipping_first_name+";path=/";
		$('#shipping_first_name_error').html('');
		//isTrue = 0;
	}

	if(shipping_last_name.match(regexp) || shipping_last_name==''){
		$('#shipping_last_name_error').html('Enter Valid Last Name');
		isTrue = 1;
	}else{
		document.cookie = "shipping_last_name="+shipping_last_name+";path=/";
		$('#shipping_last_name_error').html('');
		//isTrue = 0;
	}

	if(shipping_mobile_no.match(regexp_mobile) || shipping_mobile_no=='' || shipping_mobile_no.length < 10){
		$('#shipping_mobile_no_error').html('Enter Valid Mobile No');
		isTrue = 1;
	}else{
		document.cookie = "shipping_mobile_no="+shipping_mobile_no+";path=/";
		$('#shipping_mobile_no_error').html('');
		//isTrue = 0;
	}

	if(shipping_pincode_no.match(regexp_mobile) || shipping_pincode_no=='' || shipping_pincode_no.length < 6){
		$('#shipping_pincode_no_error').html('Enter Valid Pincode');
		isTrue = 1;
	}else{
		document.cookie = "stylecracker_shipping_pincode="+shipping_pincode_no+";path=/";
		$('#shipping_pincode_no_error').html('');
		//isTrue = 0;
	}

	if(shipping_address==''){
		$('#shipping_address_error').html('Enter Shipping Address');
		isTrue = 1;
	}else{
		document.cookie = "shipping_address="+shipping_address+";path=/";
		$('#shipping_address_error').html('');
		//isTrue = 0;
	}

	if(shipping_city==''){
		$('#shipping_city_error').html('Enter shipping City');
		isTrue = 1;
	}else{
		document.cookie = "shipping_city="+shipping_city+";path=/";
		$('#shipping_city_error').html('');
		//isTrue = 0;
	}

	if(shipping_state==''){
		$('#shipping_state_error').html('Enter shipping State');
		isTrue = 1;
	}else{
		document.cookie = "shipping_state="+$("#shipping_state option:selected").text()+";path=/";
		$('#shipping_state_error').html('');
		//isTrue = 0;
	}

	if(isTrue == 0){
		 $.ajax({
            type: "post",
            url: sc_baseurl+"stylecart/order_summary",
            data: { pincode:$('#shipping_pincode_no').val(),sc_cart_pay_mode : payment_mode },
            cache :true,
			async: true,
            success: function (response) {
                hide_cart_loader();
                
              
                payment_options();
                //$('#user_address_billing_shipping').attr('style','block');
                if($('#billing_address').val()!=''){

               /* $('#user_address_billing_shipping_ba').html('<div class="fb">'+$('#billing_first_name').val()+' '+$('#billing_last_name').val()+'</div><div>'+$('#billing_address').val()+'</div><div>'+$('#billing_state option:selected').text()+','+$('#billing_city').val()+','+$('#billing_pincode_no').val()+'</div><a class="btn-link" onclick="open_billing_address_panel();" href="javascript:void(0);">Change</a>');*/
            	}

            	/*$('#user_address_billing_shipping').html('<div class="fb">'+$('#shipping_first_name').val()+' '+$('#shipping_last_name').val()+'</div><div>'+$('#shipping_address').val()+'</div><div>'+$('#shipping_city').val()+$('#shipping_state option:selected').text()+$('#shipping_pincode_no').val()+'</div><a class="btn-link" onclick="open_address_panel();" href="javascript:void(0);">Change</a>');*/
            	$('#desktop_order_summary').html(response);
            	money_formatter(".price");
     			money_formatter(".cart-price");
     			 $('[data-toggle="popover"]').popover();
                
            },
            error: function (response) {
            	hide_cart_loader();
                //alert(response);
            }
        });

		
	}else{
		$('#address_error').removeClass('hide');
		hide_cart_loader();	
	}

}


function pay_valiation(element){

	var codna = $('#codna').val();
	var shina = $('#shina').val();
	var stock_exist = $('#stock_exist').val();	
	var page_type = $('#page-type').val();
	
	if($(element).attr("attr-value")=="card"){

			$("#cod").removeClass("active");
			$("#card").addClass("active");
			payment_mode = "card";
			
			//console.log('codna--'+codna);
			if(codna > 0)
			{
				$('#cart_place_order_msg').addClass('hide');
				$('#place_order_sc_cart').attr('disabled',false);
			}
			//console.log('shina--'+shina);
			 if(shina > 0)
			{
				$('#cart_place_order_msg').html('Shipping is not available for some products').removeClass('hide');
				$('#place_order_sc_cart').attr('disabled',true);
			} 
			//console.log('stock_exist--'+stock_exist);
			if(stock_exist > 0)
			{
				$('#cart_place_order_msg').html('Some of the products are out of stock').removeClass('hide');
				$('#place_order_sc_cart').attr('disabled',true);
			}/*else
			{
				$('#cart_place_order_msg').addClass('hide');
			}*/	
			if(codna==0 && shina==0 && stock_exist==0)
			{
				$('#place_order_sc_cart').attr('disabled',false);
			}	
		
	}else{

			/*if(getCookie('discountcoupon_stylecracker')=='BMS15' || getCookie('discountcoupon_stylecracker')=='YESBANK1000' )
			{ 
				$("#cod").addClass("state-disabled");				
			}else
			{
				$("#card").removeClass("active");
				$("#cod").addClass("active");
				payment_mode = "cod";
			}*/
			
			 if(page_type=='scboxcart')
			 {
				if(getCookie('discountcoupon_stylecracker')=='BMS15' || getCookie('discountcoupon_stylecracker')=='BMS500' || getCookie('discountcoupon_stylecracker')=='YES300' || getCookie('discountcoupon_stylecracker')=='YESBOX500' || getCookie('discountcoupon_stylecracker')=='YESBOX750' || getCookie('discountcoupon_stylecracker')=='YESBOX1000' || getCookie('scfr')=='yb' )
	            {
        			$("#cod").addClass("state-disabled");	
        			$("#cod").removeClass("active");
	        	}else{
	        		$("#cod").removeClass("state-disabled");	
	        		$("#card").removeClass("active");
					$("#cod").addClass("active");
	        	}
			}else
			{
				if(getCookie('discountcoupon_stylecracker')=='BMS15' || getCookie('discountcoupon_stylecracker')=='BMS500' || getCookie('discountcoupon_stylecracker')=='YES300' || getCookie('discountcoupon_stylecracker')=='YESBOX500' || getCookie('discountcoupon_stylecracker')=='YESBOX750' || getCookie('discountcoupon_stylecracker')=='YESBOX1000' )
	            {
        			$("#cod").addClass("state-disabled");	
        			$("#cod").removeClass("active");
	        	}else{ 
	        		$("#cod").removeClass("state-disabled");	
	        		$("#card").removeClass("active");
					$("#cod").addClass("active");
	        	}

			}
			// $("#card").removeClass("active");
			// $("#cod").addClass("active");
			

			if(codna > 0)
			{
				$('#cart_place_order_msg').html('COD is not available for some products').removeClass('hide');
				$('#place_order_sc_cart').attr('disabled',true);
				//$('#cart_place_order_msg').removeClass('hide');
			}
			 if(shina > 0)
			{
				$('#cart_place_order_msg').html('Shipping is not available for some products').removeClass('hide');
				$('#place_order_sc_cart').attr('disabled',true);
			}
			 if(stock_exist > 0)
			{
				$('#cart_place_order_msg').html('Some of the products are out of stock').removeClass('hide');
				$('#place_order_sc_cart').attr('disabled',true);
			}/*else
			{
				$('#cart_place_order_msg').addClass('hide');
			}	*/		
	}

	$('#sc_cart_pay_mode').val(payment_mode);

	show_cart_loader();

	if(page_type!='scboxcart')
	{
		$.ajax({
	            type: "post",
	            url: sc_baseurl+"stylecart/order_summary",
	            data: { pincode:$('#shipping_pincode_no').val(),sc_cart_pay_mode : payment_mode },
	            cache :true,
				async: true,
	            success: function (response) {
	                hide_cart_loader();
	                
	                $('#desktop_order_summary').html(response);
	                //$('#desktop_order_summary').addClass('open');
	                cartAmt = $('#cart_total').val();   
	                cartAmt = parseFloat(Math.round(cartAmt * 100) / 100).toFixed(2);             
					$('#payAmt').html(cartAmt);
					money_formatter(".price");
	     			money_formatter(".cart-price");
					checkoutAccordian();
					 $('[data-toggle="popover"]').popover();
	            },
	            error: function (response) {
	            	hide_cart_loader();
	                //alert(response);
	                msg(response);
	            }
	        });		
			 
			//proceed_to_checkout();
	}
	
}

function payment_options(){
	if($('#codna').val() > 0 && $('#shina').val() > 0){
			
		 	$('#place_order_sc_cart').addClass('hide');
		 	//$('#cart_error_msg').removeClass('hide');	

		 	//var redirection = sc_baseurl+'stylecart?error=1';
		 	//window.location = redirection;

		 }else if($('#codna').val() > 0){
		 	
		 	$('#place_order_sc_cart').removeClass('hide');
		 	
		 	$('#cart_error_msg').addClass('hide');
		 	if(getCookie('discountcoupon_stylecracker') == 'NOCASH500' || getCookie('discountcoupon_stylecracker') == 'NOCASH1000'){ $('#cart_place_order_msg').html('COD is not avaliable for applied coupon code'); }else{
		 		$('#cart_place_order_msg').html('One or Two Items in your cart is not available for COD').removeClass('hide');
		 	}
		 }else{
		 	
		 	$('#place_order_sc_cart').removeClass('hide');
		 	$('#cart_error_msg').addClass('hide');
		 }

		

		 var pay_mod = $('#sc_cart_pay_mode').val();
		if(pay_mod == 'card'){
			/*$("input[type='radio'][id='sc_cart_pay_mode_cod']").parent().removeClass('active');
			$("input[type='radio'][id='sc_cart_pay_mode_online']").parent().addClass('active');*/
		}else{
			/*$("input[type='radio'][id='sc_cart_pay_mode_cod']").parent().addClass('active');
			$("input[type='radio'][id='sc_cart_pay_mode_online']").parent().removeClass('active');*/
		}

		

		if($('.red_msg').length>0){
			if($('#product_pricess').val() < 600){
				$('.red_msg_cond').removeClass('hide');
				$('.box-mred_msg').addClass('hide');
			}else{
				$('.red_msg_cond').addClass('hide');
				$('.box-mred_msg').removeClass('hide');
			}
		}
}

function place_stylecracker_order(){
	show_cart_loader();
	//var pay_mod = $("input[type='radio'][name='sc_cart_pay_mode']:checked").val();
	//var payment_mode;
	$(".box-payment-options li label").each(function(e){
		if($(this).hasClass("active")){
			payment_mode = $(this).attr("attr-value");
			$('#sc_cart_pay_mode').val(payment_mode);
		}
	});

	
	var pay_mod = payment_mode;
	var codna = $('#codna').val();
	var shina = $('#shina').val();
	var stock_exist = $('#stock_exist').val();	

	var regexp = /[^a-zA-Z]/g;
	var regexp_mobile = /[^0-9]/g;
	var billing_first_name = $('#billing_first_name').val().trim();
	var billing_last_name = $('#billing_last_name').val().trim();
	var billing_mobile_no = $('#billing_mobile_no').val().trim();
	var billing_pincode_no = $('#billing_pincode_no').val().trim();
	var billing_address = $('#billing_address').val().trim();
	var billing_city = $('#billing_city').val().trim();
	var billing_state = $('#billing_state').val();

	var isTrue = 0;
	
	if(billing_first_name.match(regexp) || billing_first_name==''){
		var error = 'billing_first_name_error';
		$('#billing_first_name_error').html('Enter Valid First Name');
		isTrue = 1;
	}else{
		document.cookie = "billing_first_name="+billing_first_name+";path=/";
		$('#billing_first_name_error').html('');
		//isTrue = 0;
	}

	if(billing_last_name.match(regexp) || billing_last_name==''){
		var error = 'billing_last_name_error';
		$('#billing_last_name_error').html('Enter Valid Last Name');
		isTrue = 1;
	}else{
		document.cookie = "billing_last_name="+billing_last_name+";path=/";
		$('#billing_last_name_error').html('');
		//isTrue = 0;
	}

	if(billing_mobile_no.match(regexp_mobile) || billing_mobile_no=='' || billing_mobile_no.length < 10){
		var error = 'billing_mobile_no_error';
		$('#billing_mobile_no_error').html('Enter Valid Mobile No');
		isTrue = 1;
	}else{
		document.cookie = "billing_mobile_no="+billing_mobile_no+";path=/";
		$('#billing_mobile_no_error').html('');
		//isTrue = 0;
	}

	if(billing_pincode_no.match(regexp_mobile) || billing_pincode_no=='' || billing_pincode_no.length < 6){
		var error = 'billing_pincode_no_error';
		$('#billing_pincode_no_error').html('Enter Valid Pincode');
		isTrue = 1;
	}else{
		document.cookie = "stylecracker_shipping_pincode="+billing_pincode_no+";path=/";
		$('#billing_pincode_no_error').html('');
		//isTrue = 0;
	}

	if(billing_address==''){
		var error = 'billing_address_error';
		$('#billing_address_error').html('Enter Billing Address');
		isTrue = 1;
	}else{
		document.cookie = "billing_address="+billing_address+";path=/";
		$('#billing_address_error').html('');
		//isTrue = 0;
	}

	if(billing_city==''){
		var error = 'billing_city_error';
		$('#billing_city_error').html('Enter Billing City');
		isTrue = 1;
	}else{
		document.cookie = "billing_city="+billing_city+";path=/";
		$('#billing_city_error').html('');
		//isTrue = 0;
	}

	if(billing_state==''){
		var error = 'billing_state_error';
		$('#billing_state_error').html('Enter Billing State');
		isTrue = 1;
	}else{
		document.cookie = "billing_state="+billing_state+";path=/";
		$('#billing_state_error').html('');
		//isTrue = 0;
	}

	if(isTrue == 1){ 
		hide_cart_loader();
		$('#cart_place_order_msg').html('Please enter billing address').removeClass('hide');
		$('#cart_place_order_msg').addClass('message error');
		$("#place_order_sc_cart").attr('disabled',true);	
	}
	else if(stock_exist > 0){ 
		hide_cart_loader();
		$('#cart_place_order_msg').html('Some of the products are out of stock').removeClass('hide');
		$("#place_order_sc_cart").attr('disabled',true);	
	}
	else if(pay_mod !='cod' && pay_mod != 'card'){
		hide_cart_loader();
		$('#cart_place_order_msg').html('Select Payment Option').removeClass('hide');
		$("#place_order_sc_cart").attr('disabled',true);	
	}else if(pay_mod =='cod' && codna>0){ 
		hide_cart_loader();
		/*$("input[type='radio'][id='sc_cart_pay_mode_cod']").prop('checked',false);
		$("input[type='radio'][id='sc_cart_pay_mode_online']").prop('checked',true);*/
		
		if(getCookie('discountcoupon_stylecracker') == 'NOCASH500' || getCookie('discountcoupon_stylecracker') == 'NOCASH1000'){ 
			
		 	$("#place_order_sc_cart").attr('disabled',true);		 	
		 	
		 	$('#cart_place_order_msg').html('COD is not avaliable for applied coupon code').removeClass('hide');
		 	
		 	}else{
		 	$('#cart_place_order_msg').html('COD is not available for some products').removeClass('hide');
		}
	}else if(shina > 0){
		hide_cart_loader();
		$('#cart_place_order_msg').html('Shipping is not available for some products').removeClass('hide');
		$("#place_order_sc_cart").attr('disabled',true);	
	}else{
		var billing_first_name = $('#billing_first_name').val().trim();
		var billing_last_name = $('#billing_last_name').val().trim();
		var billing_mobile_no = $('#billing_mobile_no').val().trim();
		var billing_pincode_no = $('#billing_pincode_no').val().trim();
		var billing_address = $('#billing_address').val().trim();
		var billing_city = $('#billing_city').val().trim();
		var billing_state = $('#billing_state').val();
		var shipping_first_name = $('#shipping_first_name').val().trim();
		var shipping_last_name = $('#shipping_last_name').val().trim();
		var shipping_mobile_no = $('#shipping_mobile_no').val().trim();
		var shipping_pincode_no = $('#shipping_pincode_no').val().trim();
		var shipping_address = $('#shipping_address').val().trim();
		var shipping_city = $('#shipping_city').val().trim();
		var shipping_state = $('#shipping_state').val();
		var cart_email = $('#cart_email').val().trim(); 
		var cart_total = $('#cart_total').val();
		//$("#place_order_sc_cart").attr('disabled',false);

		if(billing_first_name!='' && billing_last_name!='' && billing_mobile_no!='' && billing_pincode_no!='' && billing_address!='' && billing_city!='' && billing_state!='' && shipping_first_name!='' && shipping_last_name!='' && shipping_mobile_no!='' && shipping_pincode_no!='' && shipping_address!='' && shipping_city!='' && shipping_state!='' && cart_email!=''){

			if(pay_mod == 'cod'){

				$.ajax({
		            type: "post",
		            url: sc_baseurl+"stylecart/place_order",
		            data: { billing_first_name:billing_first_name,billing_last_name:billing_last_name,billing_mobile_no:billing_mobile_no,billing_pincode_no:billing_pincode_no,billing_address:billing_address,billing_city:billing_city,billing_state:billing_state,shipping_first_name:shipping_first_name,shipping_last_name:shipping_last_name,shipping_mobile_no:shipping_mobile_no,shipping_pincode_no:shipping_pincode_no,shipping_address:shipping_address,shipping_city:shipping_city,shipping_state:shipping_state,cart_email:cart_email,pay_mod:pay_mod,cart_total:cart_total },
		            cache :true,
					async: true,
		            success: function (response) {
		                hide_cart_loader(); remove_address_cookies('SHIP'); remove_address_cookies('BILL');
		                var redirction = sc_baseurl+'stylecart/ordersuccessOrder/'+response;
		                var final_cookies = '';
		                document.cookie="SCUniqueID="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
		               // document.cookie="discountcoupon_stylecracker="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
		                //document.cookie="discountcoupon_binno="+final_cookies+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
		      			window.location =redirction;
		      			money_formatter(".price");
     					money_formatter(".cart-price");
		            },
		            error: function (response) {
		            	hide_cart_loader();
		                //alert(response);
		                msg(response);
		            }
        		});

			}else{
					
				$.ajax({
		            type: "post",
		            url: sc_baseurl+"stylecart/place_online_order",
		            data: { billing_first_name:billing_first_name,billing_last_name:billing_last_name,billing_mobile_no:billing_mobile_no,billing_pincode_no:billing_pincode_no,billing_address:billing_address,billing_city:billing_city,billing_state:billing_state,shipping_first_name:shipping_first_name,shipping_last_name:shipping_last_name,shipping_mobile_no:shipping_mobile_no,shipping_pincode_no:shipping_pincode_no,shipping_address:shipping_address,shipping_city:shipping_city,shipping_state:shipping_state,cart_email:cart_email,pay_mod:pay_mod,cart_total:cart_total },
		            cache :true,
					async: true,
		            success: function (response) {
		                hide_cart_loader();
		                var obj = jQuery.parseJSON(response);
		               
		      			$('#txnid').val(obj.txnid);
		      			$('#hash').val(obj.payu_hash);
		      			$('#amount').val(obj.amount);
		      			$('#firstname').val(obj.firstname);
		      			$('#email').val(obj.email);
		      			$('#phone').val(obj.phoneno);
		      			$('#udf1').val(getCookie('SCUniqueID'));
		      			$('#udf2').val(obj.address_sc);
		      			$('#udf3').val(billing_pincode_no);
		      			$('#udf4').val(billing_city);
		      			$('#udf5').val(obj.state);
		      			$('#offer_key').val(obj.offerkey);
		      			var stylecracker_style = document.forms.stylecracker_style;
      					stylecracker_style.submit();
      					money_formatter(".price");
     					money_formatter(".cart-price");

		            },
		            error: function (response) {
		            	hide_cart_loader();		               
		                msg(response);
		            }
        		});
			}
		}
	}
	hide_cart_loader();
	
}

function delete_user_address(id){
	var result = confirm("Are you sure to delete the address");
	if(result){
		show_cart_loader();

		$.ajax({
			url: sc_baseurl+"stylecart/delete_address",
			type: 'POST',
			data: { id : id },
			cache :true,
			async: true,
			success: function(response) {
				msg('Remove Address.');				
				$('#user_address_'+id).remove();				
				hide_cart_loader();
				money_formatter(".price");
    			money_formatter(".cart-price");
			},
			error: function(xhr) {				
				msg('Something goes wrong')
				hide_cart_loader();
			}
		});
	}
}

function edit_user_address(id){
	$('#sc_cart_add_new_address').hide();
	$('#sc_cart_next').addClass('disabled');
	$('#sc_cart_next').removeAttr('onclick');
	$('#add_new_address').hide();
	$('#edit_new_address').show();
	//$('#user_address_billing_shipping_ba').css('display','block');	

	$.ajax({
			url: sc_baseurl+"stylecart/get_address",
			type: 'POST',
			data: { id : id },
			cache :true,
			async: true,
			success: function(response) {
				var obj = jQuery.parseJSON(response);
				
				/*$('#edit_first_name').val(obj[0].first_name);
				if(obj[0].last_name == ''){
					$('#edit_last_name').val(obj[0].first_name);
				}else{
					$('#edit_last_name').val(obj[0].last_name);
				}*/
				var name = obj[0].first_name+' '+obj[0].last_name;
				$('#edit_name').val(name);
				$('#edit_mobile_no').val(obj[0].mobile_no);
				$('#edit_pincode_no').val(obj[0].pincode);
				$('#edit_address').val(obj[0].shipping_address);
				$('#edit_city').val(obj[0].city_name);
				$('#edit_state').val(obj[0].state_name);
				$('#edit_address_id').val(id);
				money_formatter(".price");
     			money_formatter(".cart-price");
				
				open_billing_address_panel();
				hide_cart_loader();
			},
			error: function(xhr) {
				//alert('Something goes wrong')
				msg('Something goes wrong');
				hide_cart_loader();
			}
		});
}

function edit_pincode_check(){
	var billing_pincode_no = $('#edit_pincode_no').val();
	$.ajax({
			url: sc_baseurl+"stylecart/get_pincode_info",
			type: 'POST',
			data: { billing_pincode_no : billing_pincode_no },
			cache :true,
			async: true,
			success: function(response) {
				if(response == '[]'){
					$('#edit_pincode_no').val('');
				}
				var obj = jQuery.parseJSON(response);
				$('#edit_city').val(obj.city);
				$('#edit_state').val(obj.state_id);
				
				hide_cart_loader();
			},
			error: function(xhr) {
				//alert('Something goes wrong')
				msg('Something goes wrong')
				hide_cart_loader();
			}
	});
}

function check_pincode_shipping(){
	var billing_pincode_no = $('#shipping_pincode_no').val();
	$.ajax({
			url: sc_baseurl+"stylecart/get_pincode_info",
			type: 'POST',
			data: { billing_pincode_no : billing_pincode_no },
			cache :true,
			async: true,
			success: function(response) {
				if(response == '[]'){
					$('#shipping_pincode_no').val('');
				}
				var obj = jQuery.parseJSON(response);
				$('#shipping_city').val(obj.city);
				$('#shipping_state').val(obj.state_id);
				
				hide_cart_loader();
			},
			error: function(xhr) {
				//alert('Something goes wrong')
				msg('Something goes wrong');
				hide_cart_loader();
			}
	});
}

function cancel_edit_address(){
	$('#sc_cart_next').removeClass('disabled');
	$('#sc_cart_next').attr('onclick','valiate_address();');
	$('#add_new_address').hide();
	$('#sc_cart_add_new_address').show();
	$('#edit_new_address').hide();
}

function cart_login(){
	var cart_email = $('#cart_email').val().trim();
	var cart_password = $('#cart_password').val().trim();

	if(cart_email==''){ alert('Please enter email address'); }
	else if(cart_password==''){ alert('Please enter password'); }
	else if(cart_email!='' && cart_password!=''){
		$.ajax({
        url: sc_baseurl+'schome_new/cart_login',
        type: 'post',
        data: {'type' : 'get_login','logEmailid':cart_email,'logPassword':cart_password },
        success: function(data, status) {

          if(data=='Loggedin'){
            //window.location.href=sc_baseurl+"stylecart/checkout";
            window.location.href=sc_baseurl+"checkout";
            $('#div-checkout-login').removeClass('open');
            $('#div-checkout-login').addClass('step-done');
			$('#div-checkout-address').addClass('open');			
          }else if(data == 'feed'){
           // window.location.href=sc_baseurl+"stylecart/checkout";
           window.location.href=sc_baseurl+"checkout";
            $('#div-checkout-login').removeClass('open');
            $('#div-checkout-login').addClass('step-done');
			$('#div-checkout-address').addClass('open');			
          }else{ 
            $("#sc_Login").removeClass( "disabled" );
            //$(".fa-loader-wrp-products").css({"display":"none"});
            $('#login_error').html('Enter Valid Password');
            $('#login_error').removeClass('hide');
            $('#div-checkout-login').addClass('step-done');
          }

          return false;
          event.preventDefault();

        },
        error: function(xhr, desc, err) {
          // console.log(err);
        }
      });
	}

}

function checkout_as_guest(){
	$('#div-checkout-login').removeClass('open');
	$('#div-checkout-address').addClass('open');	
}

function check_cart_availability(){
	//show_cart_loader();
	var regexp_mobile = /[^0-9]/g;
	var cart_pincode = $('#cart_pincode').val();
	var stock_exist = $('#stock_exist').val();
	var	isTrue = 0;

	if(stock_exist > 0){
		$('#cart_pincode_error').html('Product Is Out of Stock');
		isTrue = 1;		
		hide_cart_loader();
	}/*else if(cart_pincode.match(regexp_mobile) || cart_pincode=='' || cart_pincode.length < 6){
		$('#cart_pincode_error').html('Enter Valid Pincode');
		isTrue = 1;
		hide_cart_loader();
	}*/else{
		$('#cart_pincode_error').html('');
		isTrue = 0;	
	}
	
	if(isTrue == 0 ){
		$.ajax({
            type: "post",
            url: sc_baseurl+"stylecart/check_cart_availability",
            data: { pincode:$('#cart_pincode').val() },
            cache :true,
			async: true,
            success: function (response) {
               $('#sccart_product_view').html(response);               
               if($('#shina').val() == 0){
               		//$('#proceed_to_checkout').attr('disabled',false);
               		$('#proceed_to_checkout1').removeClass('hide');               		
               		$('#cart_pincode_error').removeClass('error').addClass('success');
               		//$('#cart_pincode_error').html('All items are available click on proceed to checkout button');
               		proceed_to_checkout();
               }else{
               		$('#cart_pincode_error').html('Change your shipping pincode otherwise remove non shipping items from your cart');               		
               }
                hide_cart_loader();
                $('#cart_pincode').val(cart_pincode);
            },
            error: function (response) {
            	hide_cart_loader();
                //alert(response);
                msg(response);
            }
        });
        proceed_to_checkout();
	}
	
	//hide_cart_loader();
}

function proceed_to_checkout(){
	//window.location = sc_baseurl+'stylecart/checkout';
	window.location = sc_baseurl+'checkout';
}


function remove_products_otherthan_nonremoval(a){
	show_cart_loader();
	if(a == 1){
	var nonremoval = $('#nonremoval').val();
	$.ajax({
            type: "post",
            url: sc_baseurl+"stylecart/remove_products_otherthan_nonremoval",
            data: { nonremoval:nonremoval },
            cache :true,
			async: true,
            success: function (response) {
            	add_to_buy_product($('#nonremoval').val());
				BuyNowGA($('#nonremovalbrandname').val(),$('#nonremovalproname').val());
				setTimeout('redirect_to_checkout()', 100);
            	
            }
        });
	}else{
		add_to_buy_product($('#nonremoval').val());
		BuyNowGA($('#nonremovalbrandname').val(),$('#nonremovalproname').val());
		setTimeout('redirect_to_checkout()', 100);
	}
	hide_cart_loader();
}

function redirect_to_checkout()
{	
    //window.location=sc_baseurl+'stylecart/checkout';
     window.location=sc_baseurl+'checkout';
}

function apply_coupon_code(type){
	//show_cart_loader(); 	
	var sccoupon_code = '';
	var sc_binno = '';
	var intCouponType;
	var strCouponCode;
	var offerid;
	var final_cookies = '';
	console.log("type=="+type);
	if(type=='apply' || type=='clear')
	{
		sccoupon_code = $('#sccoupon_code').val();	
		document.cookie="discountcoupon_binno="+final_cookies+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	}else if(type=='applyoffer')
	{
		$("#modal-offers ul.offers-list li").each(function(e){
			if($(this).find(".sc-checkbox").hasClass("active")){
				intCouponType = $(this).attr("data-coupontype-attr");
				strCouponCode = $(this).attr("data-couponcode-attr");
				offerid = $(this).attr("id");
				msg("intCouponType: "+intCouponType);
				msg("strCouponCode: "+strCouponCode);
			}
		});
		
		sccoupon_code = strCouponCode;
		sc_binno = $('#binno_'+offerid).val();		
		document.cookie="discountcoupon_binno="+sc_binno+";path=/";
	} 

	if(sccoupon_code!='')
	{
		var page_type = $('#page-type').val();
		$.ajax({
	            type: "post",
	            url: sc_baseurl+"stylecart/apply_coupon_code",
	            data: { 'sccoupon_code':sccoupon_code , 'sc_binno': sc_binno, 'page_type' : page_type, 'type':type },
	            cache :true,
				async: false,
	            success: function (response) { 
	            	var obj = jQuery.parseJSON(response); 
	               if(obj.msg_type == 1){ 
	                	//$('#coupon_error').html(obj.msg).removeClass('error').addClass('success');
	                	//$('#couponbinno_error').html(obj.msg).removeClass('error').addClass('success'); 
	                	//$('#coupon_modal_message').html(obj.msg).removeClass('error').addClass('success');                	
	                	$("#modal-offers").modal("hide");   
	                	offerOrderSummary(type);              	                  	
	                	alert(obj.msg);  
	                	if(getCookie('discountcoupon_stylecracker')=='BMS15' || getCookie('discountcoupon_stylecracker')=='BMS500' || getCookie('discountcoupon_stylecracker')=='YES300' || getCookie('discountcoupon_stylecracker')=='YESBOX500' || getCookie('discountcoupon_stylecracker')=='YESBOX750' || getCookie('discountcoupon_stylecracker')=='YESBOX1000' )
	                	{
	                		$("#cod").addClass("state-disabled");	
	                	}else{
	                		$("#cod").removeClass("state-disabled");	
	                	}
	                	//$('.modal-backdrop').removeClass('in');
	                	//$('.modal-backdrop').addClass('hide');
	                	$('.modal-backdrop').remove();
	                	      	
	            	}else if(obj.msg_type == 2){ 
	            		//$('#coupon_error').html(obj.msg).removeClass('success').addClass('error');
	            		//$('#couponbinno_error').removeClass('hide');
	            		//$('#coupon_error').html(obj.msg).removeClass('success').addClass('error');
	            		//$('#couponbinno_error').html(obj.msg).removeClass('success').addClass('error');
	            		//$('#couponbinno_error').html(obj.msg);
	            		alert(obj.msg); 
	            		$('.sc-checkbox').removeClass('active');	

	            	}else{
	            		 
	            		$('#coupon_error').html(obj.msg).removeClass('success').addClass('error');
	            		$('#couponbinno_error').html(obj.msg).removeClass('success').addClass('error');
	            		$("#modal-offers").modal("hide"); 
	            		offerOrderSummary(type);
	            		//$('#coupon_modal_message').html(obj.msg).removeClass('success').addClass('error');            		
	            		//$('.modal-backdrop').removeClass('in');
	            		//$('.modal-backdrop').addClass('hide');
	            		   $('.modal-backdrop').remove();     		
	            	}
	            },
	            error: function (response) {
	            	hide_cart_loader();
	                //alert(response);
	                msg(response);
	            }
	        });
	}else
	{
		$('#coupon_error').html('Enter valid coupon code').removeClass('success').addClass('error');
	}
	
	/*$.ajax({
            type: "post",
            url: sc_baseurl+"stylecart/order_summary",
            data: { pincode:$('#shipping_pincode_no').val(),sc_cart_pay_mode : payment_mode,'sccoupon_code':$('#sccoupon_code').val(),'page-type':$('#page-type').val() },
            cache :true,
			async: false,
            success: function (response) {
                //hide_cart_loader();
                if(lastparam=='cart')
				{	
					$('#stylecart_product_view').html(response);
					//location.reload();
				}else
				{
                	$('#desktop_order_summary').html(response);   
                }
                money_formatter(".price");
     			money_formatter(".cart-price");
            },
            error: function (response) {
            	//hide_cart_loader();
                //alert(response);
                 msg(response);
            }
        });

		if(type=='clear')
		{
			$('#coupon_error').html('');
			$('#coupon_error').addClass('hide');
		}

		if(type=='apply')
		{			
			$('#coupon_error').removeClass('hide');
		}
		
		 if(getCookie('discountcoupon_stylecracker')!=''){ $('#coupon_clear').removeClass('hide'); }else{ $('#coupon_clear').addClass('hide'); }*/

}

function sccartcheckotp(){
	 $.ajax({
            type: "post",
            url: sc_baseurl+"stylecart/sc_check_otp",
            data: { mobile_no:$('#mobile_no').val(),otp_number:$('#scopt').val(),oid:$('#oid').val() },
            cache :true,
			async: false,
            success: function (response) {
                hide_cart_loader();
                if(response == 'success'){                   
                	var oid = $('#oid').val();                 	        	 
                	window.location.href=sc_baseurl+'stylecart/ordersuccessOrder/'+oid+'?success=1';   
                	//location.reload();               	
                }else{
                	msg(response);
                	$('#otp-error').html('Please enter valid OTP');
                	$('#otp-error').removeClass('hide');
                }
            },
            error: function (response) {
            	hide_cart_loader();
                //alert(response);
                 msg(response);
            }
        });
}

function remove_address_cookies(add_type){
	
	var final_cookies = '';
	if(add_type == 'BILL'){
	document.cookie="billing_first_name="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC;  path=/";
	document.cookie="billing_last_name="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC;  path=/";
	document.cookie="billing_mobile_no="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="billing_address="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="billing_city="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="billing_state="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="stylecracker_billing_pincode="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="billing_id="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	}

	if(add_type == 'SHIP'){
	document.cookie="shipping_first_name="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="shipping_last_name="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="shipping_mobile_no="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="shipping_address="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="stylecracker_shipping_pincode="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="shipping_city="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="shipping_state="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="shipping_pincode_no="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="shipping_id="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";

	}
	document.cookie="cart_email="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="Scbox_price="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";

}

function add_new_address(e){
	//$('#add_new_address').show();

	//empty_all_address('SHIP');
	//remove_address_cookies('SHIP');	
	valiate_shipping_address(e);
	var addressid = $('#user_address_id').val();
	if(addressid!='')
	{
		saveAddress('edit');
	}else
	{
		saveAddress('add');	
	}
	
}


function clearCartCount(){ 
	localStorage.setItem('newcartcount', 0);
}

function empty_all_address(add_type){
	if(add_type == 'BILL'){
		$('#billing_first_name').val('');
		$('#billing_last_name').val('');
		$('#billing_mobile_no').val('');
		$('#billing_pincode_no').val('');
		$('#billing_address').val('');
		$('#billing_city').val('');
		$('#billing_state').val('');
	}
	if(add_type == 'SHIP'){
		$('#shipping_first_name').val('');
		$('#shipping_last_name').val('');
		$('#shipping_mobile_no').val('');
		$('#shipping_pincode_no').val('');
		$('#shipping_address').val('');
		$('#shipping_city').val('');
		$('#shipping_state').val('');
	}	
}

function redeem_point(is){
	/*if($(is).is(':checked')){
		document.cookie="is_redeem_point=1";
	}else{
		document.cookie="is_redeem_point=0";
	}*/
	/*
	if($('#redeem').hasClass('active')){
		document.cookie="is_redeem_point=0";		
	}else{		
		document.cookie="is_redeem_point=1";	
	}*/

	$.ajax({
            type: "post",
            url: sc_baseurl+"stylecart/order_summary",
            data: { pincode:$('#shipping_pincode_no').val(),sc_cart_pay_mode : payment_mode,'sccoupon_code':$('#sccoupon_code').val() },
            cache :true,
			async: false,
            success: function (response) {
                hide_cart_loader();
               
                $('#desktop_order_summary').html(response);
                money_formatter(".price");
     			money_formatter(".cart-price");
     			  $('[data-toggle="popover"]').popover();
            },
            error: function (response) {
            	hide_cart_loader();
                //alert(response);
                 msg(response);
            }
        });
}

function redeem_point_cart(element){

	var page_type = $('#page-type').val(); 
	
	if($(element).hasClass('active')){
		$(element).removeClass('active');		
		$('#referral-price').addClass('deactive');
		document.cookie="is_redeem_point=0";	
		
	}else{	
		$(element).addClass('active');	
		$('#referral-price').removeClass('deactive');
		document.cookie="is_redeem_point=1";		
	}
	
	$.ajax({
		url: sc_baseurl+"stylecart/remore_redeem",
		type: 'POST',
		data : {'page_type':page_type },
		cache :true,
		async: true,
		success: function(response) {
			//alert('lastparam--'+lastparam);
			//alert('seclastparam--'+seclastparam);

			if(lastparam=='cart')
			{	
				$('#stylecart_product_view').html(response);
				//location.reload();
			}else if(seclastparam=='cart')
			{	
				$('#stylecart_product_view').html(response);
				//location.reload();
			}else
			{	
				$('#stylecart_product_view').html(response);	
			}
			money_formatter(".price");
     		money_formatter(".cart-price");
     		if($('#redeem').hasClass('active')){			
				$('#referral-price').removeClass('deactive');		
			}else{			
				$('#referral-price').addClass('deactive');	
			}
			//$('#sccart_product_view').html(response);
		}
	});	

}

function show_refereal_msg_on_checkout(){
	if($('.red_msg').length>0){ console.log('1');
			if($('#product_pricess').val() < 600){
				$('.red_msg_cond').removeClass('hide');
				$('.box-mred_msg').addClass('hide');
			}else{
				$('.red_msg_cond').addClass('hide');
				$('.box-mred_msg').removeClass('hide');
			}
		}
}

function clear_coupon_code(){
	var final_cookies = '';
	if(getCookie('discountcoupon_stylecracker') =='NOCASH500' || getCookie('discountcoupon_stylecracker') =='NOCASH1000'){
		clear_coupon_code_with_load();
	}
	document.cookie="discountcoupon_stylecracker="+final_cookies+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	document.cookie="discountcoupon_binno="+final_cookies+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
	$('#sccoupon_code').val('');
	$('#coupon_error').html('');
	$('#coupon_error').addClass('hide');
	$('#proceed_to_checkout').addClass('hide');
	//$('#proceed_to_checkout1').addClass('hide');
	// $('#scupdatecart').removeClass('hide');
	// $('#scupdatecart').attr('disabled',false);
	// $('#scupdatecart1').removeClass('hide');
	// $('#scupdatecart1').attr('disabled',false);
	//apply_coupon_code('clear');
	offerOrderSummary('clear');
}

function valiate_shipping_address(e){	

	show_cart_loader();
	var regexp = /[^a-zA-Z]/g;
	var regexp_mobile = /[^0-9]/g;

	var shipping_first_name = '';
	var shipping_last_name = '';
	var shipping_name = $('#user_name').val().trim();
	shipping_name = shipping_name.replace(/\s+/g, " ");
	var res = shipping_name.split(" ");
	shipping_first_name = res[0];
	if(res[1]!='')
	{
	  shipping_last_name = res[1];
	}
	
	var selectedid = $(e).attr('id');

	// var shipping_first_name = $('#shipping_first_name').val().trim();
	// var shipping_last_name = $('#shipping_last_name').val().trim();
	var shipping_mobile_no = $('#user_mobile_no').val().trim();
	var shipping_pincode_no = $('#user_pincode_no').val().trim();
	var shipping_address = $('#user_address').val().trim();
	var shipping_city = $('#user_city').val().trim();
	var shipping_state = $('#user_state').val();
	var shipping_statename = $("#user_state option:selected").text();
	
	var isTrue = 0;
	//if($('#DShip_address').hasClass('active')) {
		if(shipping_name==''){
			if(selectedid == 'user_name')
			{
				$('#user_name_error').html('Enter Valid First Name and Last Name');
				$('#user_name_error').removeClass('hide');	
			}			
			isTrue = 1;
		}else{		
			document.cookie = "shipping_first_name="+shipping_first_name+";path=/";
			$('#user_name_error').html('');
			$('#user_name_error').addClass('hide');

			
			if( shipping_last_name=='' || shipping_last_name==undefined){
				$('#user_name_error').html('Enter Valid Last Name');
				$('#user_name_error').removeClass('hide');
				isTrue = 1;
			}else
			{ 		
				if(shipping_last_name.match(regexp))
				{
					$('#user_name_error').html('Enter Valid Last Name');
					$('#user_name_error').removeClass('hide');
					isTrue = 1;
				}else
				{
					document.cookie = "shipping_last_name="+shipping_last_name+";path=/";
					$('#user_name_error').html('');
					$('#user_name_error').addClass('hide');
					//isTrue = 0;
				}
			}
			//isTrue = 0;
		}
		
		if(shipping_mobile_no.match(regexp_mobile) || shipping_mobile_no=='' || shipping_mobile_no.length < 10){
			if(selectedid == 'user_mobile_no')
			{
				$('#user_mobile_no_error').html('Enter Valid Mobile No');
				$('#user_mobile_no_error').removeClass('hide');
			}
			$('#user_mobile_no').val('');
			isTrue = 1;
		}else{
			document.cookie = "shipping_mobile_no="+shipping_mobile_no+";path=/";
			$('#user_mobile_no_error').html('');
			$('#user_mobile_no_error').addClass('hide');
			//isTrue = 0;
		}

		if(shipping_pincode_no.match(regexp_mobile) || shipping_pincode_no=='' || shipping_pincode_no.length < 6){
			if(selectedid == 'user_pincode_no')
			{
				$('#user_pincode_no_error').html('Enter Valid Pincode');
				$('#user_pincode_no_error').removeClass('hide');
			}
			 $('#user_pincode_no').val('');		
			isTrue = 1;
		}else{
			document.cookie = "stylecracker_shipping_pincode="+shipping_pincode_no+";path=/";
			$('#user_pincode_no_error').html('');
			$('#user_pincode_no_error').addClass('hide');
			setpincodedata();
			//isTrue = 0;
		}

		if(shipping_address==''){
			if(selectedid == 'user_address')
			{
				$('#user_address_error').html('Enter Shipping Address');
				$('#user_address_error').removeClass('hide');
			}
			isTrue = 1;
		}else{
			document.cookie = "shipping_address="+shipping_address+";path=/";
			$('#user_address_error').html('');
			$('#user_address_error').addClass('hide');
			//isTrue = 0;
		}

		if(shipping_city==''){
			if(selectedid == 'user_city')
			{
				$('#user_city_error').html('Enter shipping City');
				$('#user_city_error').removeClass('hide');
			}
			isTrue = 1;
		}else{
			document.cookie = "shipping_city="+shipping_city+";path=/";
			$('#user_city_error').html('');
			$('#user_city_error').addClass('hide');
			//isTrue = 0;
		}

		if(shipping_state==''){
			if(selectedid == 'user_state')
			{
				$('#user_state_error').html('Enter shipping State');
				$('#user_state_error').removeClass('hide');
			}
			isTrue = 1;
		}else{
			document.cookie = "shipping_state="+$("#user_state option:selected").text()+";path=/";
			$('#user_state_error').html('');
			$('#user_state_error').addClass('hide');
			//isTrue = 0;
		}

		$("#user-exist-address li").each(function(e){
			if($(this).find(".btns-wrp label.ship-checkbox").hasClass("active")){
				$('#ship-bill-address').addClass('hide');
			}else
			{
				isTrue = 1;
				$('#ship-bill-address').html('Select Shipping Address');
				$('#ship-bill-address').removeClass('hide');
			}

			if($(this).find(".btns-wrp label.bill-checkbox").hasClass("active")){
				$('#ship-bill-address').addClass('hide');
			}else
			{ 
				isTrue = 1;
				$('#ship-bill-address').html('Select Billing Address');
				$('#ship-bill-address').removeClass('hide');
			}

		});
	//}

	if(isTrue == 0){

		if($('#DBill_address').hasClass('active')) {
			$('#billing_first_name').val($('#shipping_first_name').val());
			$('#billing_last_name').val($('#shipping_last_name').val());
			$('#billing_mobile_no').val($('#shipping_mobile_no').val());
			$('#billing_pincode_no').val($('#shipping_pincode_no').val());
			$('#billing_address').val($('#shipping_address').val());
			$('#billing_city').val($('#shipping_city').val());
			$('#billing_state').val($('#shipping_state').val());

			document.cookie = "billing_first_name="+shipping_first_name+";path=/";
			$('#billing_first_name_error').html('');
			$('#billing_first_name_error').addClass('hide');
			document.cookie = "billing_last_name="+shipping_last_name+";path=/";
			$('#billing_last_name_error').html('');
			$('#billing_last_name_error').addClass('hide');
			document.cookie = "billing_mobile_no="+shipping_mobile_no+";path=/";
			$('#billing_mobile_no_error').html('');
			$('#billing_mobile_no_error').addClass('hide');
			document.cookie = "stylecracker_billing_pincode="+shipping_pincode_no+";path=/";
			$('#billing_pincode_no_error').html('');
			$('#billing_pincode_no_error').addClass('hide');
			document.cookie = "billing_address="+shipping_address+";path=/";
			$('#billing_address_error').html('');
			$('#billing_address_error').addClass('hide');
			document.cookie = "billing_city="+shipping_city+";path=/";
			$('#billing_city_error').html('');
			$('#billing_city_error').addClass('hide')
			document.cookie = "billing_state="+shipping_statename+";path=/";
			$('#billing_state_error').html('');
			$('#billing_state_error').addClass('hide');

		}else{
			empty_all_address('BILL');
			remove_address_cookies('BILL');
		}
		 $.ajax({
            type: "post",
            url: sc_baseurl+"stylecart/order_summary",
            data: { pincode:$('#shipping_pincode_no').val(),sc_cart_pay_mode:payment_mode },
            cache :true,
			async: true,
            success: function (response) {
                hide_cart_loader();
               
                $('#desktop_order_summary').html(response);
                payment_options();
                money_formatter(".price");
     			money_formatter(".cart-price");
     			if($('#redeem').hasClass('active')){			
					$('#referral-price').removeClass('deactive');		
				}else{			
					$('#referral-price').addClass('deactive');	
				}
				  $('[data-toggle="popover"]').popover();
                /*$('#user_address_billing_shipping').attr('style','block');
                if($('#billing_address').val()!=''){
                 $('#user_address_billing_shipping_ba').html('<div class="fb">'+$('#billing_first_name').val()+' '+$('#billing_last_name').val()+'</div><div>'+$('#billing_address').val()+'</div><div>'+$('#billing_state').val()+','+$('#billing_city').val()+','+$('#billing_pincode_no').val()+'</div><a class="btn-link" onclick="open_billing_address_panel();" href="javascript:void(0);">Change</a>');
            	}

            	$('#user_address_billing_shipping').html('<div class="fb">'+$('#shipping_first_name').val()+' '+$('#shipping_last_name').val()+'</div><div>'+$('#shipping_address').val()+'</div><div>'+$('#shipping_city').val()+$('#shipping_state option:selected').text()+$('#shipping_pincode_no').val()+'</div><a class="btn-link" onclick="open_address_panel();" href="javascript:void(0);">Change</a>');*/
            },
            error: function (response) {
            	hide_cart_loader();
                //alert(response);
            }
        });

		
	}else{
		$('#address_error').removeClass('hide');
		hide_cart_loader();	
	}
}



function editAddress(element,id)
{	
	var type = '';
	$('#userdata-modal-title').html('Edit Address');	
	$('#user_address_id').val(id);
	// $('#DShip_address').removeClass('active');
	// $('#DBill_address').removeClass('active');	
	if(sess_userid>0){
			if(id!='')
			{
				$.ajax({
						url: sc_baseurl+"stylecart/getUserAddress",
						type: 'POST',
						data: { id : id },
						cache :true,
						async: true,
						success: function(response) {
							var obj = jQuery.parseJSON(response);				
							/*$('#edit_first_name').val(obj[0].first_name);
							if(obj[0].last_name == ''){
								$('#edit_last_name').val(obj[0].first_name);
							}else{
								$('#edit_last_name').val(obj[0].last_name);
							}*/
							var name = obj[0].first_name+' '+obj[0].last_name;

							$('#user_name').val(name);
							$('#user_mobile_no').val(obj[0].mobile_no);
							$('#user_pincode_no').val(obj[0].pincode);
							$('#user_address').val(obj[0].shipping_address);
							$('#user_city').val(obj[0].city_name);
							$('#user_state').val(parseInt(obj[0].state_id));
							$('#user_address_id').val(id);

							$('#user_address_'+id).attr('attr-state',obj[0].state_id);
							
							if($('#DShip_address').hasClass('active'))
							{
								type = 'ship';
							}else if($('#DBill_address').hasClass('active'))
							{
								type = 'bill';
							}

							if(type=='ship')
							{
								document.cookie = "shipping_first_name="+obj[0].first_name+";path=/";
								document.cookie = "shipping_last_name="+obj[0].last_name+";path=/";
								document.cookie = "shipping_mobile_no="+obj[0].mobile_no+";path=/";
								document.cookie = "stylecracker_shipping_pincode="+obj[0].pincode+";path=/";
								document.cookie = "shipping_address="+obj[0].shipping_address+";path=/";
								document.cookie = "shipping_city="+obj[0].city_name+";path=/";
								document.cookie = "shipping_state="+obj[0].state_name+";path=/";
								document.cookie = "shipping_mobile="+obj[0].mobile_no+";path=/";

							}else if(type=='bill')
							{
								document.cookie = "billing_first_name="+obj[0].first_name+";path=/";
								document.cookie = "billing_last_name="+obj[0].last_name+";path=/";
								document.cookie = "billing_mobile_no="+obj[0].mobile_no+";path=/";
								document.cookie = "stylecracker_shipping_pincode="+obj[0].pincode+";path=/";
								document.cookie = "billing_address="+obj[0].shipping_address+";path=/";
								document.cookie = "billing_city="+obj[0].city_name+";path=/";
								document.cookie = "billing_state="+obj[0].state_name+";path=/";	
								document.cookie = "billing_mobile="+obj[0].mobile_no+";path=/";							
							}
							//window.location.href=sc_baseurl+'checkout';
							//open_billing_address_panel();
							hide_cart_loader();
						},
						error: function(xhr) {
							// alert('Something goes wrong')
							msg('Something goes wrong')
							hide_cart_loader();
						}
					});
			}

	}else{
		
		var elementList = $(element).closest("li");

		var name = $(elementList).find(".title-3").text();
		var arrName = name.split(" ");
		var first_name =arrName[0]; 
		var last_name =arrName[1];
		
		var strAddress = $(elementList).find(".desc-address").text();
		$("#user_address").val(strAddress);


		var strAddressOtherInfo = $(elementList).find(".desc").text();
		var arrAddress =  strAddressOtherInfo.split(",");		

		var mobile_no = arrAddress[arrAddress.length-1];
		//var shipping_address = arrAddress[arrAddress.length-5];
		var pincode = arrAddress[arrAddress.length-2];
		var shipping_state = arrAddress[arrAddress.length-3];
		var shipping_city = arrAddress[arrAddress.length-4];

		$("#user_name").val(name);
		$("#user_mobile_no").val(mobile_no);
		//$("#user_address").val(shipping_address);
		$("#user_pincode_no").val(pincode);
		$("#user_city").val(shipping_city);

		var selectedIndex = $(elementList).attr('attr-state');
		$("#user_state").val(selectedIndex);

		addressClick();
		
	}
	
	
}

function changeAddress(type)
{
	$('#sc_cart_add_new_address').hide();
	$('#sc_cart_next').addClass('disabled');
	$('#sc_cart_next').removeAttr('onclick');
	$('#add_new_address').hide();
	$('#edit_new_address').show();
	//$('#user_address_billing_shipping_ba').css('display','block');
	if(type=='ship')
	{
		id = intSelectedShippingID;
	}else if(type=='bill')
	{
		id = intSelectedBillingID;
	}else 
	{
		id = type;
	}
	msg('id : '+id);
	
	if(sess_userid>0){
		$.ajax({
				url: sc_baseurl+"stylecart/getUserAddress",
				type: 'POST',
				data: { id : id },
				cache :true,
				async: true,
				success: function(response) { 
					var obj = jQuery.parseJSON(response);	
					var stateName = obj[0].state_name;
					stateName = stateName.replace(/<(?:.|\n)*?>/gm, '');
				

					if(type=='ship')
					{
						document.cookie = "shipping_first_name="+obj[0].first_name+";path=/";
						document.cookie = "shipping_last_name="+obj[0].last_name+";path=/";
						document.cookie = "shipping_mobile_no="+obj[0].mobile_no+";path=/";
						document.cookie = "stylecracker_shipping_pincode="+obj[0].pincode+";path=/";
						document.cookie = "shipping_address="+obj[0].shipping_address+";path=/";
						document.cookie = "shipping_city="+obj[0].city_name+";path=/";
						document.cookie = "shipping_state="+stateName+";path=/";
						document.cookie = "shipping_id="+intSelectedShippingID+";path=/";
						msg('ShipAddress');

						$('#shipping-address').html('<div>'+obj[0].first_name+' '+obj[0].last_name+'</div><div>'+obj[0].shipping_address+', '+
	                          obj[0].city_name+', '+stateName+', '+obj[0].pincode+', '+obj[0].mobile_no+'</div>');

						$('#shipping_first_name').val(obj[0].first_name);					
						$('#shipping_last_name').val(obj[0].last_name);					
						$('#shipping_mobile_no').val(obj[0].mobile_no);
						$('#shipping_pincode_no').val(obj[0].pincode);
						$('#shipping_address').val(obj[0].shipping_address);
						$('#shipping_city').val(obj[0].city_name);
						$('#shipping_state').val(obj[0].stateName);

					}

					 if(type=='bill')
					{
						document.cookie = "billing_first_name="+obj[0].first_name+";path=/";
						document.cookie = "billing_last_name="+obj[0].last_name+";path=/";
						document.cookie = "billing_mobile_no="+obj[0].mobile_no+";path=/";
						document.cookie = "stylecracker_billing_pincode="+obj[0].pincode+";path=/";
						document.cookie = "billing_address="+obj[0].shipping_address+";path=/";
						document.cookie = "billing_city="+obj[0].city_name+";path=/";
						document.cookie = "billing_state="+stateName+";path=/";
						document.cookie = "billing_id="+intSelectedBillingID+";path=/";
						msg('BillAddress');						
						$('#billing-address').html('<div>'+obj[0].first_name+' '+obj[0].last_name+'</div><div>'+obj[0].shipping_address+', '+
	                          obj[0].city_name+', '+stateName+', '+obj[0].pincode+', '+obj[0].mobile_no+'</div>');
						
						$('#billing_first_name').val(obj[0].first_name);					
						$('#billing_last_name').val(obj[0].last_name);				
						$('#billing_mobile_no').val(obj[0].mobile_no);
						$('#billing_pincode_no').val(obj[0].pincode);
						$('#billing_address').val(obj[0].shipping_address);
						$('#billing_city').val(obj[0].city_name);
						$('#billing_state').val(obj[0].stateName);					
					}

					if(type=='add')
					{
						return obj;
					}
					$('#checkout-address-summary').load();
					//location.reload();
					//window.location.href=sc_baseurl+'checkout';				
					hide_cart_loader();
				},
				error: function(xhr) {
					//alert('Something goes wrong')
					msg('Something goes wrong');
					hide_cart_loader();
				}
			});
	}else
	{

	}
}


var user_first_name = '';
var user_last_name = '';

function saveAddress(type)
{

	var user_name = $('#user_name').val().trim();
	var user_mobile_no = $('#user_mobile_no').val().trim();
	var user_address = $('#user_address').val().trim();
	var user_pincode_no = $('#user_pincode_no').val().trim();
	var user_city = $('#user_city').val().trim();
	var user_state = $('#user_state').val();
	var user_statename = $("[name='user_state'] option:selected").text();
	var user_last_name = '';
	var errorflag = 0;

	var setdefault  = 0;
	user_name = user_name.replace(/\s+/g, " ");
	var res = user_name.split(" ");
	user_first_name = res[0];
	if(res[1]=='' || typeof res[1] === "undefined" )
	{	
	 	errorflag = 1;
	}else
	{
		user_last_name = res[1];
		
	}

	var user_address_id = $('#user_address_id').val();
		
	if((user_first_name!='' || user_first_name!=undefined) && (user_last_name!='' || user_last_name!=undefined) && user_mobile_no!='' && user_address!='' && user_pincode_no!='' && user_city!='' && errorflag==0)
	{		
		if($('#DShip_address').hasClass('active') && $('#DBill_address').hasClass('active') )
		{
			setdefault=3;
			$('.ship-bill-address').addClass('hide');
		}else if($('#DBill_address').hasClass('active'))
		{
			setdefault=2;
		}else if($('#DShip_address').hasClass('active'))
		{
			setdefault=1;
		}
		
		if(sess_userid>0)
		{ 
			$.ajax({
					url: sc_baseurl+"stylecart/updateUserAddress",
					type: 'POST',
					data: { 'type' : type, 'user_name' : user_name,'user_first_name' : user_first_name, 'user_last_name' : user_last_name, 'user_mobile_no' : user_mobile_no, 'user_address' : user_address, 'user_pincode_no' : user_pincode_no, 'user_city' : user_city, 'user_state' : user_state, 'user_address_id' : user_address_id, 'setdefault' : setdefault},
					cache :true,
					async: true,
					success: function(response) {
						
						if(response!='')
						{
							
							$('#user-exist-address').html(response);
							//$('.box-address-list').html(response);
							
							addressClick();
							var iSAddAddress = false; 
							var elementAddressContainer;
							var elementShipAddressContainer;
							var elementBillAddressContainer;
							var intAddID = $('#user_address_id').val();
							

							if(type=="add"){

								iSAddAddress = true;
								elementShipAddressContainer = $("#user-exist-address li .btns-wrp label.ship-checkbox");
								elementBillAddressContainer = $("#user-exist-address li .btns-wrp label.bill-checkbox");

							} else{
								iSAddAddress = false;
								elementShipAddressContainer = $("#user_address_"+intAddID).find(".btns-wrp label.ship-checkbox");
								elementBillAddressContainer = $("#user_address_"+intAddID).find(".btns-wrp label.bill-checkbox");

							}

							if(setdefault==1){		
								$(elementShipAddressContainer).trigger("click");
							} else if(setdefault==2){
								$(elementBillAddressContainer).trigger("click");
							} else if(setdefault==3){
								$(elementShipAddressContainer).trigger("click");
								$(elementBillAddressContainer).trigger("click");
							}

							//var object = changeAddress(response);
							//$('#user-exist-address').append(object);
							var user_address_id = parseInt(response);
						}else{
							msg('error');
						}
						
						if($('#DShip_address').hasClass('active'))
						{
							document.cookie = "shipping_first_name="+user_first_name+";path=/";
							document.cookie = "shipping_last_name="+user_last_name+";path=/";
							document.cookie = "shipping_mobile_no="+user_mobile_no+";path=/";
							document.cookie = "stylecracker_shipping_pincode="+user_pincode_no+";path=/";
							document.cookie = "shipping_address="+user_address+";path=/";
							document.cookie = "shipping_city="+user_city+";path=/";
							document.cookie = "shipping_state="+user_statename+";path=/";
							document.cookie = "shipping_id="+user_address_id+";path=/";

							/*$('#shipping-address').html('<div>'+getCookie('shipping_first_name')+' '+getCookie('shipping_last_name')+'</div><div>'+getCookie('shipping_address')+', '+
	                          getCookie('shipping_city')+', '+getCookie('shipping_state')+', '+getCookie('stylecracker_shipping_pincode')+'.</div>');*/
							
						}
						 if($('#DBill_address').hasClass('active'))
						{
							document.cookie = "billing_first_name="+user_first_name+";path=/";
							document.cookie = "billing_last_name="+user_last_name+";path=/";
							document.cookie = "billing_mobile_no="+user_mobile_no+";path=/";
							document.cookie = "stylecracker_shipping_pincode="+user_pincode_no+";path=/";
							document.cookie = "billing_address="+user_address+";path=/";
							document.cookie = "billing_city="+user_city+";path=/";
							document.cookie = "billing_state="+user_statename+";path=/";
							document.cookie = "billing_id="+user_address_id+";path=/";

							/*$('#billing-address').html('<div>'+getCookie('billing_first_name')+' '+getCookie('billing_last_name')+'</div><div>'+getCookie('billing_address')+', '+
	                          getCookie('billing_city')+', '+getCookie('billing_state')+', '+getCookie('stylecracker_billing_pincode')+'.</div>');*/
							
						}
						$('#close_address').trigger('click');

						//window.location.href=sc_baseurl+'checkout';					
						//open_billing_address_panel();
						hide_cart_loader();
					},
					error: function(xhr) {
						//alert('Something went wrong')
						msg('Something goes wrong');
						hide_cart_loader();
					}
				});
		}else{

				if($('#DShip_address').hasClass('active'))
				{				
					
				}
				 if($('#DBill_address').hasClass('active'))
				{
					
				}

				var fillShippingAddress = "";
				var fillBillingAddress = "";
				
				if(setdefault==3) {
					fillShippingAddress = "active";
					fillBillingAddress = "active";
				}else if(setdefault==2)	{
					fillBillingAddress = "active";
				}else if(setdefault==1) {
					fillShippingAddress = "active";
				}
				addcnt++;
				
                response = '<li class=" user_address_'+addcnt+'" id="user_address_'+addcnt+'" attr-state="'+user_state+'" ><div class="title-3">'+user_first_name+' '+user_last_name+'</div><div class="desc-address">'+user_address+'</div><div class="desc">'+user_city+', '+user_statename+', '+user_pincode_no+', '+user_mobile_no+'</div><a class="close111 btn-remove"  onclick="delete_user_address('+addcnt+');" ><i class="icon-close" ></i></a><a class="btn-edit" href="#modal-address" data-toggle="modal" onclick="editAddress(this,'+addcnt+');" attr-value="1" ><i class="icon-edit"></i></a><div class="btns-wrp"><label class="sc-checkbox ship-checkbox"  attr-value="'+addcnt+'"><i class="icon icon-diamond"></i><span>Ship Here</span></label><label class="sc-checkbox bill-checkbox" attr-value="'+addcnt+'"><i class="icon icon-diamond"></i><span>Bill Here</span></label></div></li>';


             if(type=='add')
             {
             	$('#user-exist-address').append(response);
             	msg("Added New...");
             }else if(type=='edit')
             {
             	var addressid = $('#user_address_id').val();
             	var elementEditList= $("#user_address_"+addressid);
             	$(elementEditList).find(".title-3").text (user_first_name+" "+user_last_name);

             	$(elementEditList).find(".desc-address").text(user_address);

             	var descContent = user_city+','+user_statename+','+user_pincode_no+','+user_mobile_no;
             	$(elementEditList).find(".desc").text (descContent);

             }
			
			$('#close_address').trigger('click');

			addressClick();
			//## Selecting the Billing and Shipping Checkboxes===============================================
			if(setdefault==3) {				
				$("#user-exist-address li").last().find(".btns-wrp label").eq(0).trigger('click');
				$("#user-exist-address li").last().find(".btns-wrp label").eq(1).trigger('click');
			}else if(setdefault==2)	{

				$("#user-exist-address li").last().find(".btns-wrp label").eq(1).trigger('click');
			}else if(setdefault==1) {
				$("#user-exist-address li").last().find(".btns-wrp label").eq(0).trigger('click');
			}
			
		}
	}
}


var shipping_stateid = "";
var billing_stateid = "";
/*var shipping_name = "";
var shipping_first_name = "";
var shipping_last_name = "";
var shipping_address = "";
var shipping_city = "";
var shipping_state = "";
var shipping_pincode = "";
var shipping_mobile = "";

var billing_name = "";
var billing_first_name = "";
var billing_last_name = "";
var billing_address = "";
var billing_city = "";
var billing_state = "";
var billing_pincode = "";
var billing_mobile = "";*/


function showOrderSummary()
{
	/*
	$('#shipping-address').html('<div>'+getCookie('shipping_first_name')+' '+getCookie('shipping_last_name')+'</div><div>'+getCookie('shipping_address')+', '+
                          getCookie('shipping_city')+', '+getCookie('shipping_state')+', '+getCookie('stylecracker_shipping_pincode')+'.</div>');
	$('#billing-address').html('<div>'+getCookie('billing_first_name')+' '+getCookie('billing_last_name')+'</div><div>'+getCookie('billing_address')+', '+
                          getCookie('billing_city')+', '+getCookie('billing_state')+', '+getCookie('stylecracker_billing_pincode')+'.</div>');

	*/
	var ship_html = '';
	var bill_html = '';

	$("#user-exist-address li").each(function(e){
		if($(this).find(".btns-wrp label.ship-checkbox").hasClass("active")){
			msg("Shipping add");

			$('#ship-bill-address').addClass('hide');

			var strContainer = $(this).closest("li");
			shipping_name = $(this).find(".title-3").text();
			shipping_name = shipping_name.trim();
			var res = shipping_name.split(" ");
			shipping_first_name = res[0];
			if(res[1]!='')
			{
			  shipping_last_name = res[1];
			}


			shipping_address = $(this).find(".desc").text();
			var arrAddress =  shipping_address.split(",");
			shipping_mobile = arrAddress[arrAddress.length-1].trim();
			shipping_pincode = arrAddress[arrAddress.length-2].trim();
			shipping_city = arrAddress[arrAddress.length-4].trim();
			shipping_state = arrAddress[arrAddress.length-3].trim();

			//shipping_address = arrAddress[arrAddress.length-5].trim();
			var address_other= $(this).find(".desc-address").text();
			shipping_address = address_other;

			var ship_stateid = $(this).attr('attr-state');
			
			$('#shipping_state').val(ship_stateid);

			$('#shipping_first_name').val(shipping_first_name);         
		    $('#shipping_last_name').val(shipping_last_name);         
		    $('#shipping_mobile_no').val(shipping_mobile);
		    $('#shipping_pincode_no').val(shipping_pincode);
		    $('#shipping_address').val(shipping_address);
		    $('#shipping_city').val(shipping_city);

		    ship_html = '<div>'+shipping_name+'</div><div>'+shipping_address+', '+shipping_city+', '+shipping_state+', '+shipping_pincode+', '+shipping_mobile+'</div>';

		}else
		{
			$('#ship-bill-address').html('Select Shipping Address');
			$('#ship-bill-address').removeClass('hide');
		}

		if($(this).find(".btns-wrp label.bill-checkbox").hasClass("active")){
			msg("BILLING add");
			$('#ship-bill-address').addClass('hide');
			var strContainer = $(this).closest("li");

			billing_name = $(this).find(".title-3").text();
			billing_name = billing_name.trim();			
			var res = billing_name.split(" ");			
			billing_first_name = res[0].trim(',');
			if(res[1]!='')
			{
			  billing_last_name = res[1].trim(',');
			}

			billing_address = $(this).find(".desc").text();
			var arrAddress =  billing_address.split(",");
			billing_mobile = arrAddress[arrAddress.length-1].trim();
			billing_pincode = arrAddress[arrAddress.length-2].trim();
			billing_city = arrAddress[arrAddress.length-4].trim();			
			billing_state = arrAddress[arrAddress.length-3].trim();

			//billing_address = arrAddress[arrAddress.length-5].trim();
			var address_other= $(this).find(".desc-address").text();
			billing_address = address_other;


			var bill_stateid = $(this).attr('attr-state');
			$('#billing_state').val(bill_stateid);

		    $('#billing_first_name').val(billing_first_name);          
		    $('#billing_last_name').val(billing_last_name);
		    $('#billing_mobile_no').val(billing_mobile);
		    $('#billing_pincode_no').val(billing_pincode);
		    $('#billing_address').val(billing_address);
		    $('#billing_city').val(billing_city);

		    bill_html = '<div>'+billing_name+'</div><div>'+billing_address+', '+billing_city+', '+billing_state+', '+billing_pincode+', '+billing_mobile+'</div>';

		}else
		{ 
			$('#ship-bill-address').html('Select Billing Address');
			$('#ship-bill-address').removeClass('hide');
		}

	});

	
	$("#final-addresses").html('<div class="row" id="final-addresses"><div class="col-md-6"><div class="box-address"><div class="text-label">Shipping Address</div><div id="shipping-address">');	

 	var final_addresshtml = '<div class="col-md-6"><div class="box-address"><div class="text-label">Shipping Address</div><div id="shipping-address">'+ship_html+'</div><span class="btn-edit hide"><i class="icon-edit"></i></span></div></div><div class="col-md-6"><div class="box-address"><div class="text-label">Billing Address</div>    <div id="billing-address">'+bill_html+'</div><span class="btn-edit hide"><i class="icon-edit"></i></span></div></div>';

 
	$('#final-addresses').html(final_addresshtml);
                       /*   changeAddress('ship');
                          changeAddress('bill');*/

	if($('.ship-checkbox').hasClass('active') && $('.bill-checkbox').hasClass('active'))
	{	
		sc_updatecart(0);
		$('.ship-bill-address').addClass('hide');
		$('#div-checkout-address').removeClass('open');
		$('#desktop_order_summary').addClass('open');
		$('#div-checkout-address').addClass('step-done');
	}else{
		$('.ship-bill-address').removeClass('hide');
	}
	checkoutAccordian();
	
}

function proceed_to_pay()
{
	cartAmt = $('#cart_total').val();
	cartAmt = parseFloat(Math.round(cartAmt * 100) / 100).toFixed(2);
	$('#payAmt').html(cartAmt);
	$('#desktop_order_summary').removeClass('open');
	$('#select_paymode').addClass('open');
	$('#desktop_order_summary').addClass('step-done');
	$('#select_paymode').addClass('step-done');	
  	money_formatter(".price");
	money_formatter(".cart-price");
	checkoutAccordian();
	if(getCookie('discountcoupon_stylecracker')=='BMS15' || getCookie('discountcoupon_stylecracker')=='BMS500' || getCookie('discountcoupon_stylecracker')=='YES300' || getCookie('discountcoupon_stylecracker')=='YESBOX500' || getCookie('discountcoupon_stylecracker')=='YESBOX750' || getCookie('discountcoupon_stylecracker')=='YESBOX1000' )
	{
		$("#cod").addClass("state-disabled");	
	}else{
		$("#cod").removeClass("state-disabled");	
	}
}

function setpincodedata()
{
  $.ajax({
        url: sc_baseurl+'stylecart/getpincodedata',
        type: 'post',
        data: {'pincode' : $('#user_pincode_no').val() },
        success: function(data, status) {        	
          var obj = jQuery.parseJSON(data);
          //console.log(obj);
                 
          if(obj!=0 && obj.id!='' && obj.city!='')
          {
            $('#user_state').val(obj.id);
            $('#user_city').val(obj.city);
            // $('#shipping_state').removeClass('invalid');
            // $('#shipping_city').removeClass('invalid');

          }else if(obj===0)
          {        
            //$('#scbox_pincode-error').html('Please enter valid pincode');
            //$('#shipping_pincode_no').addClass('invalid');
            $('#user_pincode_no_error').html('Please enter valid pincode');
            $('#user_pincode_no_error').removeClass('hide');
            $('#user_pincode_no').val('');
            $('#user_state').val('');
            $('#user_city').val('');            
			//$('#user_pincode_no_error').addClass('hide');
          }         
          
          //event.preventDefault();
        },
        error: function(xhr, desc, err) {
          // console.log(err);
        }
      });
}

function clearModalData()
{
	$('#user_name').val('');	
	$('#user_mobile_no').val('');
	$('#user_pincode_no').val('');
	$('#user_address').val('');
	$('#user_city').val('');
	$('#user_state').val(parseInt(''));
	$('#user_address_id').val('');
	$('#DShip_address').removeClass('active');
	$('#DBill_address').removeClass('active');
	$('#userdata-modal-title').html('Add Address');	
}

function addressClick()
{
	$("#user-exist-address li .btns-wrp label").on("click", function(e){ 
		if($(this).hasClass("ship-checkbox")){
			$("#user-exist-address li .btns-wrp").find(".ship-checkbox").removeClass("active");
			$(this).addClass("active");
			intSelectedShippingID = $(this).attr("attr-value");
			document.cookie = "shipping_id="+intSelectedShippingID+";path=/";
			changeAddress('ship');

		}
		else if($(this).hasClass("bill-checkbox")){
			$("#user-exist-address li .btns-wrp").find(".bill-checkbox").removeClass("active");
			$(this).addClass("active");
			intSelectedBillingID = $(this).attr("attr-value");
			document.cookie = "billing_id="+intSelectedBillingID+";path=/";
			changeAddress('bill');
		}
		msg("intSelectedShippingID: "+intSelectedShippingID);
		msg("intSelectedBillingID: "+intSelectedBillingID);

	});
}


function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function offerOrderSummary(type)
{
	$.ajax({
            type: "post",
            url: sc_baseurl+"stylecart/order_summary",
            data: { pincode:$('#shipping_pincode_no').val(),sc_cart_pay_mode : payment_mode,'sccoupon_code':$('#sccoupon_code').val(),'page_type':$('#page-type').val(), object_id:$('#object_id').val() },
            cache :true,
			async: false,
            success: function (response) {
                //hide_cart_loader(); 
                if(lastparam=='cart' || seclastparam=='cart')
				{	
					
					$('#stylecart_product_view').html(response);
					//location.reload();
				}else
				{
                	$('#desktop_order_summary').html(response);   
                }
                 $('[data-toggle="popover"]').popover();
                money_formatter(".price");
     			money_formatter(".cart-price");
            },
            error: function (response) {
            	//hide_cart_loader();
                //alert(response);
                 msg(response);
            }
        });

		if(type=='clear')
		{
			$('#coupon_error').html('');
			$('#coupon_error').addClass('hide');
		}

		if(type=='apply')
		{			
			$('#coupon_error').removeClass('hide');
		}
		
		 if(getCookie('discountcoupon_stylecracker')!=''){ $('#coupon_clear').removeClass('hide'); }else{ $('#coupon_clear').addClass('hide'); }
}
