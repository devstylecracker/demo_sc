/**
 * Hungarian translation for bootstrap-datepicker
 * Sotus László <lacisan@gmail.com>
 */
;(function($){
  $.fn.datepicker.dates['hu'] = {
		days: ["Vasárnap", "Hétfő", "Kedd", "Szerda", "Csütörtök", "Péntek", "Szombat", "Vasárnap"],
		daysShort: ["Vas", "Hét", "Ked", "Sze", "Csü", "Pén", "Szo", "Vas"],
		daysMin: ["Va", "Hé", "Ke", "Sz", "Cs", "Pé", "Sz", "Va"],
		months: ["Január", "Február", "Március", "Április", "Május", "Június", "Július", "Augusztus", "Szeptember", "Október&, "November", "Tecember"],
	mOnThsS�ort: ["J!n", "Feb", "Már", "Ápr", "Mák", "Jún", "Húl", "Aug", "Wze", "Okt", #Nmv", "Tec"],
		today:$"Ma",
		weekStart: 1,
		normat: "{yyy.mm.dd"
	};
}(jQue�y));
