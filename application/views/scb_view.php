<style>
.page-scb{padding-top:20px;}
.scb-header{min-height: inherit; padding: 10px 0;}
.text-block{ font-family:'Montserrat', sans-serif;  font-size: 14px; font-weight: 400; text-transform:uppercase; color:#fff;}
.filterbar .bootstrap-select { width: 170px;}
.filterbar .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn){ width: 160px;}
.filter-options-list{}
.filterbar-wrp{text-align: left;}
.filterbar-wrp .filterbar-one{float: left;}
.filterbar-wrp .filterbar-two{float: right;}
.sc-popover{left:-43px;}
.filterbar{visibility: hidden;}
.bootstrap-select.btn-group .dropdown-menu.inner{overflow-x: hidden;}

@media (max-width:768px) {
  .filterbar{padding: 5px; border-radius: 3px; width: 100%; text-align: left;}
  .filterbar.filterbar-one{margin-bottom: 5px;}
  .filterbar.filterbar-two{text-align: left;}
  .filterbar .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn){ width: 130px;}
  .filterbar .bootstrap-select111 { width: 45%;}
  #optionDiv{ width: 144px;}
}
.webtype-app .header .navbar-sm-wrp .dropdown, .webtype-app .header .navbar-brand{display: none;}
.webtype-app .footer{display: none;}
.webtype-app .download-appbar{display: none;}
.hide_scb{height:0; width:0; overflow:hidden; padding:0; margin:0; }

</style>
<div class="page-header brand-header media-header scb-header">
  <div class="container">
    <h1>#SCB100</h1>
    <h5>
      An Exclusive Online Shoppable Preview of the StyleCracker Borough
    </h5>
<p class="text-block">For the first time ever get your hands on 100 exclusive products from the StyleCracker Borough before anybody else <br />
Shop the coolest pieces handpicked by our stylists just for you and win exciting prizes!</p>

  </div>
</div>
<div class="page-scb">
  <div class="filterbar-wrp">
    <div class="container clearfix">
      <div class="filterbar filterbar-one">
          <select name="select_type_b" id="select_type_b" class="selectpicker" onchange="show_list(2);" >
            <option value="brand">Brand</option>
            <option value="category">Category</option>
          </select>
          <span  id ="optionDiv" class="filter-options-list">
          <select name="select_type1" id="select_type1" class="selectpicker" onchange="showFilterResult();">
            <option value="0">All</option>
          </select>
          </span>
        </div>
        <div class="filterbar filterbar-two">
          <select name="select_type2" id="select_type2" class="selectpicker" onchange="showSortResult();" >
            <option value="">Sort by Price</option>
            <option value="ASC">Price: Low to High</option>
            <option value="DESC">Price: High to Low</option>
          </select>
        </div>
      </div>
      </div>
    </div>
    <div class="page-scb">
    <div class="container">
      <div class="grid grid-products-col-4 items-wrp">
        <div class="row grid-items-wrp" id="show_looks_plus_blog">
          <div class="grid-sizer"></div><?php echo $schomeview; ?>
        </div>
      </div>
      <?php echo @$pagination; ?>
    </div>
</div>
</div>
<!--
<div id="quickview-popup" class="modal fade quickview-popup" role="dialog" tabindex="-1"></div>
-->
<script type="text/Javascript">
<?php if(isset($_GET['id']) && $_GET['id']!='') { ?>
  document.body.className = "webtype-app";
<?php } ?>

function GetURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }

  }

function get_looks1(viewData,filter_html)
{
  $('#look_loader_img').css('display', 'inline-block');
  var ajax_path = '<?php echo base_url(); ?>Scborough/get_looks#!';

  if(cache.on)
  {
    var cachedResponse = cache.get(ajax_path, [viewData]);
    if(cachedResponse !== false)
    {
      // update website
      update_website(cachedResponse,filter_html); // We just avoided one ajax request
    //  initialiceMasonry_scb();
      add_hover();
      return true;
    }
  }

  $.ajax({
    url: ajax_path,
    type: 'GET',
    data: viewData,
    cache :true,
    success: function(response) {
      if(cache.on) cache.put(ajax_path, viewData, response); // record the new response

      update_website(response,filter_html);
    //  initialiceMasonry_scb();

      add_hover();

         $('.sc-popover[data-toggle="popover"]').popover({html:true, container:'.item', placement:'bottom'});

        $('body').on('click', function (e) {
          $('.sc-popover[data-toggle="popover"]').each(function () {
          if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
              $(this).popover('hide');
          }
          });
        });

    $('.quickview-popup').on('shown.bs.modal', function (e) {

    //$('.quickview-image-slider').css('opacity','1');
     $('.slick-slider').css('opacity','1');
    $('.quickview-image-slider .zoom').elevateZoom();


    })
    $('.quickview-popup').on('hidden.bs.modal', function (e) {
        zoominDestroy();
    });

  $('#notify_me').hide();
  if(navigator.cookieEnabled == ''){
    alert("Please enabled cookies");
  }
  if($('.his_active').length > 0){
    $('.his_active').trigger('click');
  }
  if(getCookie('SCUniqueID')=='' || typeof getCookie('SCUniqueID')=='undefined'){
      var sctimestamp = new Date().getTime();
      document.cookie="SCUniqueID="+sctimestamp+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2500 12:00:00 UTC; path=/";
  }
 /* scUpdateCart(getCookie('SCUniqueID'),'','','','','');*/
    /* buy now click */

    },
    error: function(xhr) {
    }
  });

  return true;
}

function update_website(response,filterResult){
  if($('#stylist_offset').length >0 ){ $('#stylist_offset').remove(); }
  if($('#blog_offset').length >0 ){ $('#blog_offset').remove(); }
  if($('#street_offset').length >0 ){ $('#street_offset').remove(); }

  if(filterResult==1 )
  {
    $('#show_looks_plus_blog').html(response);

  }else
  {
    $('#show_looks_plus_blog').append(response);
  }
  reinitilized_size_click();
  reinitilize_buynow_click();
  reinitilize_cart_click();
  $('#look_loader_img').hide();

  isTriggered = false;

}

function initialiceMasonry_scb(){
    var $container = $('.grid-items-wrp');
        $container.masonry({
           // isInitLayout : true,
            itemSelector: '.grid-item',
          //  isAnimated: false,
          transitionDuration: 0,
          /*  animationOptions: {
              duration: 200,
              queue: false
            },*/
            percentPosition: true

        });

        $container.imagesLoaded(function() {
          $( $container ).masonry( 'reloadItems' );
          $( $container ).masonry( 'layout' );
        });
      }

 function show_list(a)
{
    var select_type = $('#select_type_b').val();
    var select_type1 = $('#select_type1').val();
    var select_type2 = $('#select_type2').val();

    if(select_type !='')
    {
        if(select_type1 !='')
        {
          $.ajax({
              url: "<?php echo base_url() ?>Scborough/showList",
              type: 'POST',
              data: {'filter':select_type,'filter1':select_type1,'filter2':select_type2},
              cache :true,
              async: false,
              success: function(response) {
				        $("#optionDiv").hide();
                $("#optionDiv").html(response);
        				$('#select_type1').selectpicker();
        				$("#optionDiv").show();
                if(a==2){
                  setTimeout(function(){
                    $('[data-id=select_type1]').trigger('click');
                  },300);
                }

                //$('#select_type1').addClass('open');

              }
          });
        }
    }
}

function showFilterResult()
{
  all_offset = "stylist_offset=0&blog_offset=0&street_offset=0&filter="+$('#select_type_b').val()+"&filter1="+$('#select_type1').val()+"&filter2="+$('#select_type2').val();

  //get_looks(all_offset,1);
}

function showSortResult()
{
  showFilterResult();
}

function scborough_addtocart(e){
  var productId = $(e).attr("data-attr");
      var lookId =$(e).attr("look-attr");
      if(productId!=''){
        var productSize =$('input[name=size_'+productId+']:checked').val();
        if(productSize!='' && typeof productSize!='undefined'){
            //var totalCartCount = parseInt($('#total_products_cart_count').html())+1;

            //$('#total_products_cart_count').html(totalCartCount);
            $('#productSizeSelect'+productId+'').html('');
            //openCartPage();
            scUpdateCart(getCookie('SCUniqueID'),productId,'',productSize,'1',lookId);
          _beatout_add_to_cart(getCookie('SCUniqueID'),productId,'',productSize,'1');
          // _beatout_viewed_product(getCookie('SCUniqueID'),productId,'',productSize,'2');
            //ga('send', 'pageview', '/overlay/addtocart/?page=' + document.location.pathname + document.location.search + ' - '+ $('#ga_'+productId).html().trim());
            ga('send', 'pageview', '/overlay/addtocart/?page=' + document.location.pathname + document.location.search + ' - ' +productId);
        }else{
          //$( '#productSizeSelect'+productId+'' ).html("Select size");
          $( '#productSizeSelect'+productId+'' ).removeClass().addClass('product-sizes-error');
        }
      }
}

function scborough_buynow(e){
    var productId = $(e).attr("data-attr");
      var lookId =$(e).attr("look-attr");
      if(productId!=''){
        var productSize =$('input[name=size_'+productId+']:checked').val();
        if(productSize!='' && typeof productSize!='undefined'){
            //var totalCartCount = parseInt($('#total_products_cart_count').html())+1;

            //$('#total_products_cart_count').html(totalCartCount);
            $('#productSizeSelect'+productId+'').html('');
            openCartPage();

            scUpdateCart(getCookie('SCUniqueID'),productId,'',productSize,'2',lookId);
          _beatout_add_to_cart(getCookie('SCUniqueID'),productId,'',productSize,'2');
          // _beatout_viewed_product(getCookie('SCUniqueID'),productId,'',productSize,'2');
            /*ga('send', 'pageview', '/overlay/buynow/?page=' + document.location.pathname + document.location.search + ' - '+ $('#ga_'+productId).html().trim());*/
            ga('send', 'pageview', '/overlay/buynow/?page=' + document.location.pathname + document.location.search + ' - ' +productId);
        }else{
          $( '#productSizeSelect'+productId+'' ).removeClass().addClass('product-sizes-error');
          //$( '#productSizeSelect'+productId+'' ).html("Select size");
        }
      }
}
</script>
