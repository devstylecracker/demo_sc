<?php
$user_id = $this->session->userdata('user_id');
?>
<div class="page-product content-wrp">
  <div class="container">
    <div class="breadcrumb-wrp">
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <?php /*if($products_details['product_cat_id'] == WOMEN_CATEGORY_ID || $products_details['product_cat_id'] == MEN_CATEGORY_ID) { ?>
        <li><a href="<?php echo base_url(); ?>all-products/<?php echo $products_details['product_cat_id']==WOMEN_CATEGORY_ID ? 'women' : 'men'; ?>"><?php if($products_details['product_cat_id']==WOMEN_CATEGORY_ID) { echo 'Women'; }else { echo 'Men'; } ?></a></li>
        <?php } ?>
        <?php if(!empty($sub_category_info)){ ?>
        <li><a href="<?php echo base_url(); ?>all-products/<?php if($products_details['product_cat_id']==WOMEN_CATEGORY_ID) { echo 'women'; }else { echo 'men'; } ?>/subcategory/<?php echo $sub_category_info[0]['slug']; ?>"><?php echo $sub_category_info[0]['name']; ?></a></li>
        <?php }*/ ?>
        <?php echo $cat_info; ?>
        <li class="active"><?php echo str_replace('\\', '',$products_details['product_name']); ?></li>
      </ol>
      <div class="previous-page">
        <?php if(!empty($sub_category_info)){ ?>
        <a href="<?php echo base_url(); ?>all-products/<?php if($products_details['product_cat_id']==WOMEN_CATEGORY_ID) { echo 'women'; }else { echo 'men'; } ?>/subcategory/<?php echo $sub_category_info[0]['slug']; ?>">Back to <?php echo $sub_category_info[0]['name']; ?></a>
        <?php } ?>
      </div>
    </div>

    <div class="box-added-to-cart">
       <span class="text"><i class="fa fa-check-circle"></i> Item is added to your cart</span>
        <a class="btn btn-secondary btn-xs pull-right" href="<?php echo base_url(); ?>cartnew">View Cart</a>
      </div>

    <div class="product-info-section">
      <div class="row">
        <div class="col-md-5 zoom-gallery-wrp">
          <div class="zoom-gallery-wrp-inner">
          <div class="swiper-container gallery-top product-gallery-top">
                <ul class="swiper-wrapper">
                <li class="swiper-slide">
                  <div class="item-img-inner zoom">
                      <a href="<?php echo $this->config->item('product_image').$products_details['image']; ?>">
                      <?php if(file_exists($this->config->item('products_H546').$products_details['image'])) { ?>
                      <img src="<?php echo $this->config->item('products_546').$products_details['image']; ?>"
                         alt="" data-zoom-image="<?php echo $this->config->item('product_image').$products_details['image']; ?>" />
                      <?php }else{ ?>
                      <img src="<?php echo $this->config->item('product_image').$products_details['image']; ?>"
                         alt="" data-zoom-image="<?php echo $this->config->item('product_image').$products_details['image']; ?>" />
                      <?php } ?>
                    </a>
                  </div>
                </li>

                <?php foreach($product_extra_images as $row){ ?>
                <li class="swiper-slide">
                  <div class="item-img-inner">
                    <a href="<?php echo $this->config->item('product_extra_images').$row['product_images'] ?>">
                    <img src="<?php echo $this->config->item('products_extra_546').$row['product_images'] ?>" data-zoom-image="<?php echo $this->config->item('product_extra_images').$row['product_images'] ?>" />
                    </a>
                  </div>
                </li>
                  <?php } ?>
              </ul>
              <div class="swiper-button-next"></div>
              <div class="swiper-button-prev"></div>
              </div>
            </div>
                <div id="zoom-place-holder" class="zoom-place-holder"> </div>

              <?php if(count($product_extra_images) >= 1){ ?>
              <div class="swiper-container gallery-thumbs product-gallery-thumbs">
                <div class="swiper-wrapper">
                <div  class="swiper-slide">
                <div class="item-img-inner">
                  <img src="<?php echo $this->config->item('products_124').$products_details['image'] ?>" />
                </div>
                  </div>
                <?php $i=1; foreach($product_extra_images as $row){ ?>
                <div class="swiper-slide">
                  <div class="item-img-inner">
                    <img src="<?php echo $this->config->item('products_extra_124').$row['product_images'] ?>" />
                </div>
                  </div>
                <?php } ?>
              </div>
              </div>
              <?php } ?>
      </div>
      <div class="col-md-7">
          <div class="product-desc-wrp">
            <h3 class="product-brand"><a href="<?php echo base_url()."brands/brand-details/".$products_details['brand_slug']; ?>" ><?php echo $products_details['brand_name']; ?></a></h3>

            <div class="product-title-wrp">
            <h1 class="product-title"><?php echo str_replace('\\', '',$products_details['product_name']); ?></h1>

           <div class="user-likes">
            <?php
              if($this->session->userdata('user_id')){
                if($this->home_model->get_fav_or_not('product',$products_details['product_id']) == 0){ ?>
                  <span class="label-add-to-wishlist">Add to Wish List</span>
                  <a target="_blank" title="Add to Wish List" id="wishlist<?php echo $products_details['product_id']; ?>" onclick="add_to_fav('product','<?php echo $products_details['product_id']; ?>','<?php echo $user_id; ?>','<?php echo $products_details['price']; ?>','<?php echo $products_details['product_name']; ?>','<?php echo $products_details['product_cat_id']; ?>','add_to_wishlist');fbq('track', 'AddToWishlist');">
                    <i class="ic sc-love"></i>
                   </a>
                <?php }else{ ?>
                  <span class="label-add-to-wishlist">Remove from Wish List</span>
                  <a target="_blank" title="Remove from Wish List" id="wishlist<?php echo $products_details['product_id']; ?>" onclick="add_to_fav('product','<?php echo $products_details['product_id']; ?>','<?php echo $user_id; ?>','<?php echo $products_details['price']; ?>','<?php echo $products_details['product_name']; ?>','<?php echo $products_details['product_cat_id']; ?>','remove_from_wishlist');">
                     <i class="ic sc-love active"></i></a>
                <?php } }else{ ?>
                  <span class="label-add-to-wishlist">Add to Wish List</span>
                <a target="_blank" title="Add to Wish List" data-toggle="modal" href="#myModal"> <i class="ic sc-love"></i></a>
              <?php }
              ?>
            </div>
            </div>


            <div class="product-price">

              <!--<div class="item-price">
                  <?php if($this->Product_desc_model->get_productDiscount_price($products_details['product_id'],$products_details['brand_id'])['discounted_product_price']>0) { ?>
                  <span class="mrp-price text-strike"><i class="fa fa-inr"></i> <?php echo $products_details['price']; ?></span>
                  <span class="price"><i class="fa fa-inr"></i> <?php echo $this->Product_desc_model->get_productDiscount_price($products_details['product_id'],$products_details['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($products_details['product_id'],$products_details['brand_id'])['discounted_product_price'] : ''; ?></span>
                  <span class="discount-price">(<?php echo round($this->Product_desc_model->get_productDiscount_price($products_details['product_id'],$products_details['brand_id'])['discount_percent']); ?>% OFF)</span>
                  <?php }else{ ?>
                  <span class="price"><i class="fa fa-inr"></i><?php echo $products_details['price']; ?></span>
                  <?php } ?>
				  <?php if(count($product_size)==0){ ?>
			   <span class="item-soldout"> (SOLD OUT) </span>
			<?php } ?>
                </div>-->
			<div class="item-price">
                  <?php if($this->Product_desc_model->get_productDiscount_price($products_details['product_id'],$products_details['brand_id'])['discounted_product_price']>0 || $products_details['compare_price']>$products_details['price']) { ?>
                  <span class="mrp-price text-strike"><i class="fa fa-inr"></i> <?php if($products_details['compare_price']>$products_details['price']){ echo $products_details['compare_price']; }else { echo $products_details['price']; $products_details['compare_price'] = $products_details['price'];} ?></span>
				  <span class="price"><i class="fa fa-inr"></i> <?php $discount_price = $this->Product_desc_model->get_productDiscount_price($products_details['product_id'],$products_details['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($products_details['product_id'],$products_details['brand_id'])['discounted_product_price'] : ''; if($discount_price > 0){ echo $discount_price; }else{
					  echo $products_details['price'];
				  } ?></span>
                  <span class="discount-price">(<?php $discount_percent = $this->Product_desc_model->calculate_discount_percent($products_details['compare_price'],$discount_price,$products_details['price']); echo $discount_percent; ?>% OFF)</span>
                  <?php }else{ ?>
                  <span class="price"><i class="fa fa-inr"></i><?php echo $products_details['price']; ?></span>
                  <?php } ?>
				  <?php if(count($product_size)==0){ ?>
			   <span class="item-soldout"> (SOLD OUT) </span>
			<?php } ?>
                </div>

            </div>
            <div class="product-frm-wrp">
              <?php if(count($product_size)!=0){ ?>
              <select class="product-options" id="size" name="size" >
                <!--option>Select Size</option-->
                <?php
                  foreach($product_size as $row){
                    $value = $row['size_id'];
                    $text = $row['size_text'];
                    echo "<option value='$value'>$text</option>";
                  }
                ?>
              </select>
              <?php } ?>
             <div class="product-btns-wrp111 buy-btns-wrp">
                  <div class="row">
                     <div class="col-md-4 col-sm-3 col-xs-6">
                    <a class="btn btn-block btn-secondary btn-add-to-cart"  <?php if(count($product_size)==0){ echo "Disabled"; } else{ ?>onclick="add_to_cart(<?php echo $products_details['product_id']; ?>);fbq('track', 'AddToCart');AddToCartGA('<?php echo addslashes($products_details['brand_name']); ?>','<?php echo addslashes($products_details['product_name']); ?>');"<?php } ?>>
                      <i class="fa fa-cart-plus"></i> Add to Cart
                    </a>
                    </div>
                    <?php if(count($product_size)==0){ ?>
                     <div class="col-md-4 col-sm-5 col-xs-6">
                        <a class="btn btn-primary btn-block btn-nitify-me cart-buy-now111" onclick="notify('<?php echo base64_encode($products_details['product_id']); ?>');NotifyMeGA('<?php echo addslashes($products_details['brand_name']); ?>','<?php echo addslashes($products_details['product_name']); ?>');" >Notify Me</a>
                    </div>
                    <?php }else{ ?>
                    <div class="col-md-4 col-sm-5 col-xs-6">
                        <a class="btn btn-primary btn-block btn-buy-now cart-buy-now" onclick="buySingleOrMultiple(<?php echo $products_details['product_id']; ?>,'<?php echo addslashes($products_details['brand_name']); ?>','<?php echo addslashes($products_details['product_name']); ?>');">Buy Now</a>
                    </div>
                    <?php } ?>
                    
                </div>
            </div>
            <?php $product_desc = strip_tags($products_details['product_description']);
                 /* $brand_desc = strip_tags($products_details['brand_description']);*/
                  $brand_desc = $products_details['brand_description'];
             ?>
            <ul class="nav nav-tabs product-tabs">
              <?php if($product_desc){ ?>
              <li class="active"><a data-toggle="tab" href="#product">Product</a></li>
              <?php } ?>
              <?php if($brand_desc){ ?>
              <li <?php if(!$product_desc){ echo 'class="active"'; } ?>><a data-toggle="tab" href="#brand">Brand</a></li>
              <?php } ?>

            </ul>
            <div class="tab-content product-tab-content">
              <?php if($product_desc){ ?>
              <div id="product" class="tab-pane fade in active">
                <div><?php echo $product_desc; ?></div>
                <?php
                if(strip_tags($product_size_guide['size_guide']) !=1 && $this->Looks_model->get_size_guide_n($product_size_guide['size_guide'],$products_details['product_id'],$products_details['brand_id'])!='')
                {
                ?>
                 <a href="<?php echo strip_tags($this->Looks_model->get_size_guide_n($product_size_guide['size_guide'],$products_details['product_id'],$products_details['brand_id'])); ?>" class="size-guide scmfp">Size Guide</a>
                <?php
                }
                ?>
                <!-- <a href="javascript:void(0)" class="size-guide" onclick="openSizeGuide('<?php echo $product_size_guide['size_guide']; ?>','<?php echo $products_details['product_id']; ?>','<?php echo $products_details['brand_id']; ?>');">Size Guide</a> -->
              </div>
              <?php } ?>
              <?php if($brand_desc){ ?>
              <div id="brand" class="tab-pane fade <?php if(!$product_desc){ echo 'in active"'; } ?>">
                <?php echo $brand_desc; ?>

                <?php if(!$product_desc){ ?>
                <a href="javascript:void(0)" class="size-guide" onclick="openSizeGuide('<?php echo $product_size_guide['size_guide']; ?>','<?php echo $products_details['product_id']; ?>','<?php echo $products_details['brand_id']; ?>');">Size Guide</a>
                <?php }  ?>
				 </br><a target="_blank" href="https://www.stylecracker.com/return-policy" >Return Policy</a>
              </div>
              <?php }  ?>

               <?php if($products_details['return_policy']!=''){ ?>
                <div id="return_policy" class="tab-pane fade <?php if(!$products_details['product_description']){ echo 'in active"'; } ?>">
                      <?php echo $products_details['return_policy']; ?>
              </br>
                  <?php if($products_details['min_del_days']>0 && $products_details['max_del_days']>0){
                $delivery_status = 'Delivery in '.$products_details['min_del_days'].'-'.$products_details['max_del_days'].' working days.';
              }else if(($products_details['min_del_days']>0 && $products_details['max_del_days']=='') || ($products_details['min_del_days']=='' && $products_details['max_del_days']>0)){
                $delivery_status = 'Delivery in '.$products_details['min_del_days'].$products_details['max_del_days'].' working days.';
              } ?>
              <?php echo $delivery_status; ?>
                    </div>


              <?php } ?>
            </div>
          </div>
        </div>

    <div class="look-share-wrp">
                    <span class="label-share">Share</span>
                    <ul class="social-buttons">
                      <?php
                        $short_url = base_url().'product/details/'.$products_details['slug'].'-'.$products_details['product_id'];
                        echo '<li><a onClick="_targetFacebookShare(\''.$this->seo_title.'\',\''.base_url().'product/details/'.$products_details['slug'].'-'.$products_details['product_id'].'\',\''.$this->config->item('product_image').$products_details['image'].'\',\''.$products_details['product_id'].'\',\''.$this->seo_desc.'\');"><i class="fa fa-facebook"></i></a></li>';
                        echo '<li><a onclick="_targetTweet(\''.$short_url.'\',\''.$products_details['product_name'].'\',\''.$products_details['slug'].'-'.$products_details['product_id'].'\');"><i class="fa fa-twitter"></i></a></li>';
                        echo '<li><a onclick="_targetPinterest(\''.$short_url.'\',\''.$products_details['product_name'].'\',\''.$products_details['product_id'].'\',\''.$this->config->item('product_image').$products_details['image'].'\');"><i class="fa fa-pinterest"></i></a></li>';

                     ?>
                    </ul>
                  </div>
    </div>
        </div>
  </div>
  <div class="related-items-section">
<!-- -->
<?php if(count($related_looks)>0 && count($related_product)>0){ ?>
<ul class="nav nav-tabs common-tabs">
  <?php if(count($related_looks)>0){ ?>
  <li class="active"><a data-toggle="tab" href="#looks-with-the-product">Recommended Looks</a></li>
  <?php } ?>
  <?php if(count($related_product)>0){ ?>
  <li <?php  if(!$related_looks){echo 'class="active"';}?>><a  data-toggle="tab" href="#similar-products">Recommended Products</a></li>
  <?php } ?>
</ul>
<?php }else if(count($related_looks)>0){ ?>
  <div class="title-section">
    <h2 class="title"><span>Recommended Looks</span></h2>
  </div>
<?php }else if(count($related_product)>0){ ?>
    <div class="title-section">
      <h2 class="title"><span>Recommended Products</span></h2>
  </div
<?php } ?>

<div class="tab-content common-tab-content">
  <?php if(count($related_looks)>0){ ?>
    <!-- tab content 1 -->
  <div id="looks-with-the-product" class="tab-pane fade in active looks-with-the-product">
      <div class="swiper-container common looks-slider">
          <ul class="swiper-wrapper">
              <?php foreach($related_looks as $value){?>
              <li class="swiper-slide">
              <div class="item">
                <div class="item-img">
                  <div class="item-img-inner">

                    <?php
                      $short_url = base_url().'looks/look-details/'.$value['slug'];
                      $img_title_alt = str_replace('#','',$value['name']);

                      if($value['look_type'] == 1){ ?>
                        <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>" onclick="ga('send', 'event', 'Look', 'clicked', '<?php echo $value['name']; ?>');"><img src="<?php echo $this->config->item('sc_look_image_url').$value['image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                      <?php $img_url = $this->config->item('sc_look_image_url').$value['image'];
                        }else if($value['look_type'] == 2 || $value['look_type'] == 4 || $value['look_type'] == 6){ ?>
                        <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>" onclick="ga('send', 'event', 'Look', 'clicked', '<?php echo $value['name']; ?>');" ><img src="<?php echo $this->config->item('sc_street_style_image_url').$value['look_image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                      <?php $img_url = $this->config->item('sc_street_style_image_url').$value['look_image'];
                      }else if($value['look_type'] == 3){ ?>
                      <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>" onclick="ga('send', 'event', 'Look', 'clicked', '<?php echo $value['name']; ?>');" ><img src="<?php echo $this->config->item('sc_promotional_look_image').$value['product_img']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                      <?php $img_url = $this->config->item('sc_promotional_look_image').$val[0]['product_img'];
                      }
                      ?>
                    </div>
                    </div>

                  <div class="item-desc">
                        <div class="item-title"><a title="<?php echo $img_title_alt; ?>" href="<?php echo $short_url; ?>" ><?php echo $value['name']; ?></a>
                        </div>
                          <div class="item-price-wrp">
                        <div class="item-price"><i class="fa fa-inr"></i> <?php echo $value['look_price']; ?></div>
                        <?php
                          if($this->session->userdata('user_id')){
                            if($this->home_model->get_look_fav_or_not($value['id']) == 0){ ?>
                              <div class="user-likes"><a href="javascript:void(0)" title="Add to Wish List" id="whislist<?php echo $value['id']; ?>" onclick="add_to_wishlist(<?php echo $value['id']; ?>);fbq('track', 'AddToWishlist');"><i class="ic sc-love"></i></a></div>
                            <?php }else{ ?>
                              <div class="user-likes"><a href="javascript:void(0)" title="Remove from Wish List" id="whislist<?php echo $value['id']; ?>" onclick="remove_from_wishlist(<?php echo $value['id']; ?>);"><i class="ic sc-love active"></i></a></div>
                            <?php } }else{ ?>
                            <div class="user-likes"><a title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a></div>
                          <?php }
                          ?>
                  </div>
                    </div>
              </div>
              </li>
              <?php } ?>

          </ul>
          <div class="swiper-button-prev common"></div>
          <div class="swiper-button-next common"></div>
      </div>
</div>
<?php } ?>
  <!-- tab content 2 -->
<?php if(count($related_product)>0){ ?>
<div id="similar-products" class="tab-pane fade similar-products <?php  if(!$related_looks){echo 'active in';}?>">
    <div class="swiper-container common products-slider">
      <?php if($related_product){ ?>
      <ul class="swiper-wrapper">
          <?php foreach($related_product as $val){
			  // echo "<pre>";print_r($related_product);exit;?>
            <li class="swiper-slide">
            <div class="item item-product">
              <?php
                $user_link = 0;
                if($this->session->userdata('user_id')){ $user_link = $this->session->userdata('user_id')!='' ? $this->session->userdata('user_id') : 0 ; }
              ?>
              <div class="item-img">
                  <div class="item-img-inner">
                <a href="<?php echo base_url(); ?>product/details/<?php echo $val['slug'].'-'.$val['id']; ?>">
                 <img src="<?php echo 'https://www.stylecracker.com/sc_admin/assets/products/thumb_160/'.@$val['image']; ?>" alt="<?php echo @$val['name']; ?> - StyleCracker" title="<?php echo @$val['name']; ?> - StyleCracker">
                 </a>
                   </div>

              </div>
              <div class="item-desc">
                    <div class="item-title" title="<?php echo stripslashes($val['name']); ?>">
                  <a href="<?php echo base_url(); ?>product/details/<?php echo $val['id']; ?>"><?php echo stripslashes($val['name']); ?></a>
                    </div>
                      <div class="item-price-wrp">
                    <!--<div class="item-price">
                     <?php if($this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price']>0) { ?>
                  <span class="mrp-price text-strike"><i class="fa fa-inr"></i> <?php echo $val['price']; ?></span>
                  <span class="price"><i class="fa fa-inr"></i> <?php echo $this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price'] : ''; ?></span>
                  <span class="discount-price">(<?php echo round($this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discount_percent']); ?>% OFF)</span>
                  <?php }else{ ?>
                  <span class="price"><i class="fa fa-inr"></i> <?php echo $val['price']; ?></span>
                  <?php } ?>
                    </div>-->
					<div class="item-price">
                     <?php if($this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price']>0 || $val['compare_price']>$val['price']) { ?>
                  <span class="mrp-price text-strike">
					<i class="fa fa-inr"></i> 
					<?php 
						// $current_date =  && $val['discount_start_date']  $val['discount_end_date']
						if($val['compare_price'] > $val['price']){ 
							echo $val['compare_price']; 
						}else { 
							echo $val['price']; $val['compare_price'] = $val['price'];
						}
						?>
				  </span>
				  <span class="price"><i class="fa fa-inr"></i> <?php $discount_price = $this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price'] : ''; if($discount_price > 0){ echo $discount_price; }else{
					  echo $val['price'];
				  } ?></span>
                  <span class="discount-price">(<?php $discount_percent = $this->Product_desc_model->calculate_discount_percent($val['compare_price'],$discount_price,$val['price']); echo $discount_percent; ?>% OFF)</span>
				  
                  <?php }else{ ?>
                  <span class="price"><i class="fa fa-inr"></i> <?php echo $val['price']; ?></span>
                  <?php } ?>
                    </div>
                    <div class="user-likes">
                       <?php
                         if($this->session->userdata('user_id')){
                           if($this->home_model->get_fav_or_not('product',$val['id']) == 0){ ?>
                             <a href="javascript:void(0)" data-toggle="modal" title="Add to Wish List" id="wishlist<?php echo $val['id']; ?>" onclick="add_to_fav('product','<?php echo $val['id']; ?>','');fbq('track', 'AddToWishlist');"><i class="ic sc-love"></i></a>
                           <?php }else{ ?>
                             <a href="javascript:void(0)" data-toggle="modal" title="Add to Wish List" id="wishlist<?php echo $val['id']; ?>" onclick="add_to_fav('product','<?php echo $val['id']; ?>','');"><i class="ic sc-love active"></i></a>
                           <?php } }else{ ?>
                           <a href="#myModal" data-toggle="modal" title="Add to Wish List"><i class="ic sc-love"></i></a>
                         <?php }
                         ?>
                    </div>
                      </div>

              </div>
            </div>
            </li>

          <?php } ?>

      </ul>
      <?php } ?>
      <div class="swiper-button-prev common"></div>
      <div class="swiper-button-next common"></div>

    </div>
</div>
<?php } ?>
</div>
</div><!-- /tabs -->

</div>
</div>

<script type="text/javascript">
var google_tag_params = { ecomm_prodid: [<?php echo $products_details['product_id']; ?>], ecomm_pagetype: 'product', ecomm_totalvalue: <?php echo $products_details['price']; ?>, };
</script>
