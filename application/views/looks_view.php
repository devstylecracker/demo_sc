<div class="page-common page-all-looks content-wrp">
    <div class="container">
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>">Home</a></li>
    <li class="active">Looks of The Week</li>
  </ol>
    </div>
  <!--div class="filterbar-wrp">
    <div class="container">
      <div class="filterbar">
      <span class="filter-label">Filter by: </span>
              <select name="look_filter_type" id="look_filter_type" class="selectpicker">
                <option value="0">Latest</option>
                <option value="1">Looks</option>
                <option value="2">Festive</option>
                <option value="3">Bridal</option>
              </select>

      </div>
    </div>
  </div-->
  <?php  $page_filter_name = $this->uri->segment(2); ?>
  <div class="container">
          <div class="filterbar-wrp">
            <div class="filterbar">
              <!--span class="filter-label"><i class="fa fa-filter"></i> Filter: </span-->
              <a href="<?php echo base_url(); ?>looks/women"><label class="<?php if(@$page_filter_name == 'women') { echo 'active'; } ?>" onclick="deselect_menu();"><i class="fa fa-square"></i> <?php echo $this->lang->line('homepage_filter_women'); ?></label></a>
              <a href="<?php echo base_url(); ?>looks/men"><label class="<?php if(@$page_filter_name == 'men') { echo 'active'; } ?>" onclick="deselect_menu();"><i class="fa fa-square"></i> <?php echo $this->lang->line('homepage_filter_men'); ?></label></a>
              <a href="<?php echo base_url(); ?>looks/all"><label class="<?php if(@$page_filter_name == 'all') { echo 'active'; } ?>" onclick="deselect_menu();"><i class="fa fa-square"></i> <?php echo $this->lang->line('homepage_filter_all'); ?></label></a>
             </div>
            </div>
           <!-- look of the week -->
          <?php if(!empty($looks)) { ?>
          <div class="grid grid-looks-col-3 items-wrp items-section">
            <div class="title-section">
                <h1 class="title"><span>Looks of The Week</span></h1>
            </div>

            <div class="row grid-items-wrp all-looks111" id="get_all_looks">
            <?php foreach ($looks as $value) { ?>
            <div class="col-md-4 col-sm-6 grid-item">
              <div class="item">
                <div class="item-img item-hover">
                    <?php
                      $short_url = base_url().'looks/look-details/'.$value['slug'];
                      $img_title_alt = str_replace('#','',$value['name']);

                      if($value['look_type'] == 1){ ?>
                        <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>"><img src="<?php echo $this->config->item('sc_look_image_url').$value['image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                      <?php $img_url = $this->config->item('sc_look_image_url').$value['image'];
                        }else if($value['look_type'] == 2 || $value['look_type'] == 4 || $value['look_type'] == 6){ ?>
                        <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>"><img src="<?php echo $this->config->item('sc_street_style_image_url').$value['look_image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                      <?php $img_url = $this->config->item('sc_street_style_image_url').$value['look_image'];
                      }else if($value['look_type'] == 3){ ?>
                      <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>"><img src="<?php echo $this->config->item('sc_promotional_look_image').$value['product_img']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                      <?php $img_url = $this->config->item('sc_promotional_look_image').$val['product_img'];
                      }
                      ?>
                      <?php if($this->home_model->look_hv_discount($value['id']) == 1){ ?>
                      <div class="item-tag-sale"></div>
                      <?php } ?>
                  <div class="item-look-hover">
                    <a onclick="ga('send', 'event', 'Look', 'clicked', '<?php echo $value['name']; ?>');" href="<?php echo $short_url; ?>" title="<?php echo $img_title_alt; ?>" target="_blank"><span class="btn btn-primary"><?php echo $this->lang->line('get_this'); ?></span></a>
                  </div>
                  </div>
                  <div class="item-desc">
                    <div class="item-title"><a title="<?php echo $img_title_alt; ?>" href="<?php echo $short_url; ?>" target="_blank"><?php echo $value['name']; ?></a>
                    </div>
                    <div class="item-media">
                      <ul class="social-buttons">
                        <li><a onclick="_targetFacebookShare('<?php echo $value['name']; ?>','<?php echo $short_url; ?>','<?php echo $img_url; ?>','<?php echo $value['id']; ?>'); ga('send', 'social', 'Facebook', 'share', '<?php echo $short_url; ?>');"><i class="fa fa-facebook"></i></a></li>
                        <li><a onclick="_targetTweet('<?php echo $short_url; ?>','<?php echo $value['name']; ?>','<?php echo $value['id']; ?>');ga('send', 'social', 'Twitter', 'tweet', '<?php echo $short_url; ?>');"><i class="fa fa-twitter"></i></a></li>
                        <li><a onclick="_targetPinterest('<?php echo $short_url; ?>','<?php echo $value['name']; ?>','<?php echo $value['id']; ?>','<?php echo $img_url; ?>');ga('send', 'social', 'Pinterest', 'share', '<?php echo $short_url; ?>');"><i class="fa fa-pinterest"></i></a></li>
                      </ul>

                      <?php
                      if($this->session->userdata('user_id')){
                        if($this->home_model->get_look_fav_or_not($value['id']) == 0){ ?>
                          <div class="user-likes"><a target="_blank" title="Add to Wish List" id="whislist<?php echo $value['id']; ?>" onclick="add_to_wishlist(<?php echo $value['id']; ?>);ga('send', 'event', 'Look', 'wishlist', '<?php echo $value['name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love"></i></a></div>
                        <?php }else{ ?>
                          <div class="user-likes"><a target="_blank" title="Remove from Wish List" id="whislist<?php echo $value['id']; ?>" onclick="remove_from_wishlist(<?php echo $value['id']; ?>);ga('send', 'event', 'Look', 'wishlist', '<?php echo $value['name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love active"></i></a></div>
                        <?php } }else{ ?>
                        <div class="user-likes"><a target="_blank" title="Add to Wish List" onclick="ga('send', 'event', 'Sign Up', 'clicked', 'favorite');" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a></div>
                      <?php }
                      ?>

                      </div>
                    </div>
                  </div>
                </div>

       <?php }  ?>

</div>
    </div>
    <?php } ?>
    <?php echo $pagination; ?>
<input type="hidden" name="offset" id="offset" value="1">
<input type="hidden" name="look_type" id="look_type" value="<?php echo @$this->uri->segment(2); ?>">
<input type="hidden" name="total_looks" id="total_looks" value="<?php echo $looks_total; ?>">

   <div class="fa-loader-wrp-outer">
          <div style="display: none;" class="fa-loader-wrp-products">
            <i class="fa fa-circle-o-notch fa-spin-custom"></i> Loading...
          </div>
        </div>

</div>
</div>
