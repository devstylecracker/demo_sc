<?php if($this->session->userdata('gender') == 1) { $gender = 'women'; }
      if($this->session->userdata('gender') == 2) { $gender = 'men'; }
?>
 <!--  <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script> -->
  <script>
    // var OneSignal = window.OneSignal || [];
    // OneSignal.push(["init", {
    //   appId: "774c695f-4257-45ad-bbd7-76a22b12117b",
    //   autoRegister: true,
    //   safari_web_id: 'web.onesignal.auto.4f832ce8-c167-4c63-9514-5546a8912edb',
    //   notifyButton: {
    //     enable: false /* Set to false to hide */
    //   }
    // }]);
   /* OneSignal.push(["sendTags", {gender: "<?php echo $gender; ?>"}]);*/
  </script>



<script>
var noCookie;
noCookie=1;
</script>
<?php $page_filter_name_ = $page_filter_name ; ?>
<div class="page-home content-wrp">
  <div class="container">
      <?php if($this->session->userdata('is_home_page_personalised') != 1){ ?>
    <div class="filterbar-wrp">
      <div class="filterbar">
        <!--span class="filter-label"><i class="fa fa-filter"></i> Filter: </span-->
        <a href="<?php echo base_url(); ?>women"><label class="<?php if($page_filter_name == 'women') { echo 'active'; } ?>" onclick="deselect_menu();"><i class="fa fa-square"></i> <?php echo $this->lang->line('homepage_filter_women'); ?></label></a>
        <a href="<?php echo base_url(); ?>men"><label class="<?php if($page_filter_name == 'men') { echo 'active'; } ?>" onclick="deselect_menu();"><i class="fa fa-square"></i> <?php echo $this->lang->line('homepage_filter_men'); ?></label></a>
        <a href="<?php echo base_url(); ?>all"><label class="<?php if($page_filter_name == 'all') { echo 'active'; } ?>" onclick="deselect_menu();"><i class="fa fa-square"></i> <?php echo $this->lang->line('homepage_filter_all'); ?></label></a>
    </div>
    </div>
     <?php } ?>
    <!-- look of the week -->
    <?php if(!empty($top_looks)) { ?>
    <div class="grid grid-looks-col-3 items-wrp items-section">
      <div class="title-section">
          <h1 class="title"><span><?php echo $this->lang->line('homepage_caption_one'); ?></span></h1>
          <?php if($this->session->userdata('is_home_page_personalised') == 1){ ?>
          <a href="<?php echo base_url(); ?>feed" class="link-view-all">View All Looks<i class="fa fa-caret-right"></i></a>
          <?php }else{ ?>
          <a href="<?php echo base_url(); ?>looks/<?php echo $page_filter_name; ?>" class="link-view-all">View All Looks<i class="fa fa-caret-right"></i></a>
          <?php } ?>
      </div>
      <div class="row grid-items-wrp grid-items-slider">
         <div class="mobile-swiper-wrp">
      <?php foreach ($top_looks as $value) { ?>
      <div class="col-md-4 col-sm-6 grid-item">
        <div class="item">
          <div class="item-img item-hover">
              <?php
                $short_url = base_url().'looks/look-details/'.$value[0]['slug'];
                $img_title_alt = str_replace('#','',$value[0]['name']);

                if($value[0]['look_type'] == 1){ ?>
                  <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value[0]['slug']; ?>"><img src="<?php echo $this->config->item('sc_look_image_url').$value[0]['image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                <?php $img_url = $this->config->item('sc_look_image_url').$value[0]['image'];
                  }else if($value[0]['look_type'] == 2 || $value[0]['look_type'] == 4 || $value[0]['look_type'] == 6){ ?>
                  <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value[0]['slug']; ?>"><img src="<?php echo $this->config->item('sc_street_style_image_url').$value[0]['look_image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                <?php $img_url = $this->config->item('sc_street_style_image_url').$value[0]['look_image'];
                }else if($value[0]['look_type'] == 3){ ?>
                <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value[0]['slug']; ?>"><img src="<?php echo $this->config->item('sc_promotional_look_image').$value[0]['product_img']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                <?php $img_url = $this->config->item('sc_promotional_look_image').$val[0]['product_img'];
                }
                ?>
                 <?php if($this->home_model->look_hv_discount($value[0]['id']) == 1){ ?>
                      <div class="item-tag-sale"></div>
                      <?php } ?>
            <div class="item-look-hover">
              <a href="<?php echo $short_url; ?>" title="<?php echo $img_title_alt; ?>" onclick="ga('send', 'event', 'Look', 'clicked', '<?php echo $value[0]['name']; ?>');"><span class="btn btn-primary"><?php echo $this->lang->line('get_this'); ?></span></a>
            </div>
            </div>
            <div class="item-desc">
              <div class="item-title"><a title="<?php echo $img_title_alt; ?>" href="<?php echo $short_url; ?>"><?php echo $value[0]['name']; ?></a>
              </div>
              <div class="item-media">
                <ul class="social-buttons">
                  <li><a onclick="_targetFacebookShare('<?php echo $value[0]['name']; ?>','<?php echo $short_url; ?>','<?php echo $img_url; ?>','<?php echo $value[0]['id']; ?>','<?php echo 'Buy Online Now on StyleCracker'; ?>'); ga('send', 'social', 'Facebook', 'share', '<?php echo $short_url; ?>');"><i class="fa fa-facebook"></i></a></li>
                  <li><a onclick="_targetTweet('<?php echo $short_url; ?>','<?php echo $value[0]['name']; ?>','<?php echo $value[0]['id']; ?>');ga('send', 'social', 'Twitter', 'tweet', '<?php echo $short_url; ?>');"><i class="fa fa-twitter"></i></a></li>
                  <li><a onclick="_targetPinterest('<?php echo $short_url; ?>','<?php echo $value[0]['name']; ?>','<?php echo $value[0]['id']; ?>','<?php echo $img_url; ?>');ga('send', 'social', 'Pinterest', 'share', '<?php echo $short_url; ?>');"><i class="fa fa-pinterest"></i></a></li>
                </ul>
               <?php
                      if($this->session->userdata('user_id')){
                        if($this->home_model->get_look_fav_or_not($value[0]['id']) == 0){ ?>
                          <div class="user-likes"><a title="Add to Wish List" id="whislist<?php echo $value[0]['id']; ?>" onclick="add_to_wishlist(<?php echo $value[0]['id']; ?>); ga('send', 'event', 'Look', 'wishlist', '<?php echo $value[0]['name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love"></i></a></div>
                        <?php }else{ ?>
                          <div class="user-likes"><a title="Remove from Wish List" id="whislist<?php echo $value[0]['id']; ?>" onclick="remove_from_wishlist(<?php echo $value[0]['id']; ?>);ga('send', 'event', 'Look', 'wishlist', '<?php echo $value[0]['name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love active"></i></a></div>
                        <?php } }else{ ?>
                        <div class="user-likes"><a onclick="ga('send', 'event', 'Sign Up', 'clicked', 'favorite');" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a></div>
                      <?php }
                      ?>
              </div>
            </div>
          </div>
        </div>
       <?php } ?>
       </div>
      </div>
  </div>
    <?php } ?>
    <!-- top categoroes -->
    <?php /*if(!empty($top_categories)) { ?>
    <div class="grid items-wrp items-section">
    <div class="title-section">
      <h1 class="title"><span><?php echo $this->lang->line('homepage_caption_second'); ?></span></h1>
      <a href="<?php echo base_url(); ?>category-list/<?php echo $page_filter_name; ?>" class="link-view-all">View All Categories<i class="fa fa-caret-right"></i></a>
    </div>
      <div class="row grid-items-wrp grid-items-slider">
        <div class="mobile-swiper-wrp">
      <?php foreach ($top_categories as $value) { $category_img_url = ''; $cat_type = ''; ?>
      <div class="col-md-4 col-sm-6 grid-item">
        <div class="item item-cat">
          <div class="item-img item-hover">
            <div class="item-img-inner">
            <?php

            if($value['type'] == 1) {
                $category_img_url_array = @scandir('./assets/images/products_category/category/'.$value['cat_id'],true);
                $header_img = get_headers($this->config->item('products_category_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg");
                if(!empty($category_img_url_array)){
                //if($header_img[2] == "Content-Type: image/jpeg"){
                  $category_img_url = $this->config->item('products_category_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg";//$category_img_url_array[0];//base_url().'assets/images/products_category/category/'.
                }
                $cat_type = 'cat';
              }else if($value['type'] == 2) {
                if($value['product_cat_id'] == WOMEN_CATEGORY_ID ){ $page_filter_name = 'women'; }else{ $page_filter_name ='men'; }
                $category_img_url_array = @scandir('./assets/images/products_category/sub_category/'.$value['cat_id'],true);
                $header_img = get_headers($this->config->item('products_sub_category_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg");
                if(!empty($category_img_url_array)){
                //if($header_img[2] == "Content-Type: image/jpeg"){

                  $category_img_url = $this->config->item('products_sub_category_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg";//$category_img_url_array[0];
                }
                $cat_type = 'subcategory';
              }else if($value['type'] == 3) {
                if($value['product_cat_id'] == WOMEN_CATEGORY_ID ){ $page_filter_name = 'women'; }else{ $page_filter_name ='men'; }
                $category_img_url_array = @scandir('./assets/images/products_category/variation_types/'.$value['cat_id'],true);
                $header_img = get_headers($this->config->item('products_variation_types_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg");
                if(!empty($category_img_url_array)){

                //if($header_img[2] == "Content-Type: image/jpeg"){
                  $category_img_url = $this->config->item('products_variation_types_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg";//$category_img_url_array[0];
                  $filename = './assets/images/products_category/variation_types/'.$value['cat_id'].'/'.$value['cat_id'].'.jpg';
                  if(file_exists($filename) == 1){
                     $category_img_url = $this->config->item('products_variation_types_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg";
                  }else{
                    $category_img_url = base_url().'assets/images/image-na.jpg';
                  }
                }
                $cat_type = 'variation';
              }else if($value['type'] == 4) {
                if($value['product_cat_id'] == WOMEN_CATEGORY_ID ){ $page_filter_name = 'women'; }else{ $page_filter_name ='men'; }
                $category_img_url_array = @scandir('./assets/images/products_category/variation_values/'.$value['cat_id'],true);
                if(!empty($category_img_url_array)){
                $header_img = get_headers($this->config->item('products_variation_values_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg");
                //if($header_img[2] == "Content-Type: image/jpeg"){
                  $category_img_url = $this->config->item('products_variation_values_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg";//$category_img_url_array[0];
                }
                $cat_type = 'variations';
              }
            ?>
            <a href="<?php echo base_url(); ?>all-products/<?php echo $page_filter_name; ?>/<?php echo $cat_type; ?>/<?php echo $value['slug']; ?>">
            <?php if($category_img_url){ ?>
              <img title="<?php echo $value['name']; ?> - StyleCracker" alt="<?php echo $value['name']; ?>- StyleCracker" src="<?php echo @$category_img_url; ?>">

                <?php } else { ?>
                    <img title="<?php echo $value['name']; ?> - StyleCracker" alt="<?php echo $value['name']; ?>- StyleCracker" src="<?php echo base_url(); ?>assets/images/image-na.jpg">
                <?php } ?>
            </a>
          </div>
            <div class="item-look-hover">
              <a onclick="ga('send', 'event', 'ProductCategory', 'clicked', '<?php echo $value['name']; ?>');" href="<?php echo base_url(); ?>all-products/<?php echo $page_filter_name; ?>/<?php echo $cat_type; ?>/<?php echo $value['slug']; ?>" ><span class="btn btn-primary"> View More </span></a>
            </div>
            </div>
            <div class="item-desc">
              <div class="item-title"><a title="<?php echo $value['name']; ?>" href="#" ><?php echo $value['name']; ?></a>
              </div>
              <!--div class="item-media">
                <ul class="social-buttons">
                  <li><a onclick="_categorytargetFacebookShare('<?php echo $value['name']; ?>','<?php echo base_url(); ?>all-products/<?php echo $page_filter_name; ?>/<?php echo $cat_type; ?>/<?php echo $value['slug']; ?>','<?php echo $category_img_url; ?>','<?php echo $value['cat_id']; ?>');ga('send', 'social', 'Facebook', 'share', '<?php echo base_url()."all-products/".$page_filter_name."/".$cat_type."/".$value['slug']; ?>');"><i class="fa fa-facebook"></i></a></li>
                  <li><a onclick="_categorytargetTweet('<?php echo $value['name']; ?>','<?php echo base_url(); ?>all-products/<?php echo $page_filter_name; ?>/<?php echo $cat_type; ?>/<?php echo $value['slug']; ?>','<?php echo $category_img_url; ?>','<?php echo $value['cat_id']; ?>');ga('send', 'social', 'Twitter', 'tweet', '<?php echo base_url()."all-products/".$page_filter_name."/".$cat_type."/".$value['slug']; ?>');"><i class="fa fa-twitter"></i></a></li>
                  <li><a onclick="_categorytargetPinterest('<?php echo $value['name']; ?>','<?php echo base_url(); ?>all-products/<?php echo $page_filter_name; ?>/<?php echo $cat_type; ?>/<?php echo $value['slug']; ?>','<?php echo $category_img_url; ?>','<?php echo $value['cat_id']; ?>');ga('send', 'social', 'Pinterest', 'share', '<?php echo base_url()."all-products/".$page_filter_name."/".$cat_type."/".$value['slug']; ?>');"><i class="fa fa-pinterest"></i></a></li>
                </ul>
              </div-->
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      </div>
    </div>
    <?php }*/ ?>

       <!-- top categoroes -->
    <?php if(!empty($top_categories)) { ?>
    <div class="grid items-wrp items-section">
    <div class="title-section">
      <h1 class="title"><span><?php echo $this->lang->line('homepage_caption_second'); ?></span></h1>
      <a href="<?php echo base_url(); ?>category-list/<?php echo $page_filter_name; ?>" class="link-view-all">View All Categories<i class="fa fa-caret-right"></i></a>
    </div>
      <div class="row grid-items-wrp grid-items-slider">
        <div class="mobile-swiper-wrp">
      <?php foreach ($top_categories as $value) { $category_img_url = ''; $cat_type = ''; ?>
      <div class="col-md-4 col-sm-6 grid-item">
        <div class="item item-cat">
          <div class="item-img item-hover">
            <div class="item-img-inner">
            <?php

            //$category_img_url = $this->config->item('products_category_img_path_new').$value['cat_id'].".jpg";
            //$category_img_url ='assets/images/products_category/'.$value['cat_id'].".jpg";
             $category_img_url = $this->home_model->cat_img_exists($value['cat_id']);
            ?>
            <a href="<?php echo base_url(); ?>all-products/<?php echo $value['slug']; ?>" >
            <?php if(file_exists($category_img_url)){ ?>
              <img title="<?php echo $value['name']; ?> - StyleCracker" alt="<?php echo $value['name']; ?>- StyleCracker" src="<?php echo base_url().$category_img_url; ?>">

                <?php } else { ?>
                    <img title="<?php echo $value['name']; ?> - StyleCracker" alt="<?php echo $value['name']; ?>- StyleCracker" src="<?php echo base_url(); ?>assets/images/image-na.jpg">
                <?php } ?>
            </a>
          </div>
            <div class="item-look-hover">
              <a onclick="ga('send', 'event', 'ProductCategory', 'clicked', '<?php echo $value['name']; ?>');" href="<?php echo base_url(); ?>all-products/<?php echo $value['slug']; ?>" ><span class="btn btn-primary"> View More </span></a>
            </div>
            </div>
            <div class="item-desc">
              <div class="item-title"><a title="<?php echo $value['name']; ?>" href="<?php echo base_url(); ?>all-products/<?php echo $value['slug']; ?>" ><?php echo $value['name']; ?></a>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      </div>
    </div>
    <?php } ?>
    <!-- trending brands -->
    <?php if(!empty($top_brands)) { ?>
    <div class="grid items-wrp items-section">
      <div class="title-section">
      <h1 class="title"><span><?php echo $this->lang->line('homepage_caption_third'); ?></span></h1>
      <a href="<?php echo base_url(); ?>brand-list/<?php echo $page_filter_name_; ?>" class="link-view-all">View All Trending Brands <i class="fa fa-caret-right"></i></a>
    </div>
    <div class="row grid-items-wrp grid-items-slider">
      <div class="mobile-swiper-wrp">
      <?php foreach($top_brands as $value){ if(!empty($value)){
        $brand_url = base_url().'brands/brand-details/'.$value[0]['user_name'];
      ?>
      <div class="col-md-4 col-sm-6 grid-item">
        <div class="item item-brand">
          <div class="item-img item-hover">
              <div class="item-img-inner">
          <?php
              $brand_img_url = '';
                if($value[0]['home_page_promotion_img']!=''){
                  $brand_img_url = $this->config->item('brand_home_page_url').$value[0]['home_page_promotion_img'];//base_url().'assets/images/brands/homepage/'
                }
          ?>
            <a href="<?php echo $brand_url; ?>">
              <?php if($brand_img_url){ ?>
              <img title="<?php echo $value[0]['company_name']; ?>- StyleCracker" alt="<?php echo $value[0]['company_name']; ?> - StyleCracker" src="<?php echo $brand_img_url; ?>">
                <?php } else { ?>
                    <img title="<?php echo $value[0]['company_name']; ?>- StyleCracker" alt="<?php echo $value[0]['company_name']; ?> - StyleCracker" src="<?php echo base_url(); ?>assets/images/image-na.jpg">
                <?php } ?>
            </a>
          </div>
            <div class="item-look-hover">
              <a href="<?php echo $brand_url; ?>" onclick="ga('send', 'event', 'Brand', 'clicked', '<?php echo $value[0]['company_name']; ?>');"><span class="btn btn-primary"> View More </span></a>
            </div>
            </div>
            <div class="item-desc">
              <div class="item-title"><a title="<?php echo $value[0]['company_name']; ?>" href="#"><?php echo $value[0]['company_name']; ?></a>
              </div>
              <div class="item-media">
                <ul class="social-buttons">
                  <li><a onclick="_brandtargetFacebookShare('<?php echo $value[0]['company_name']; ?>','<?php echo $brand_url; ?>','<?php echo $brand_img_url; ?>','<?php echo $value[0]['user_id']; ?>'); ga('send', 'social', 'Facebook', 'share', '<?php echo $brand_url; ?>');"><i class="fa fa-facebook"></i></a></li>
                  <li><a onclick="_brandtargetTweet('<?php echo $value[0]['company_name']; ?>','<?php echo $brand_url; ?>','<?php echo $brand_img_url; ?>','<?php echo $value[0]['user_id']; ?>');ga('send', 'social', 'Twitter', 'tweet', '<?php echo $brand_url; ?>');"><i class="fa fa-twitter"></i></a></li>
                  <li><a onclick="_brandtargetPinterest('<?php echo $value[0]['company_name']; ?>','<?php echo $brand_url; ?>','<?php echo $brand_img_url; ?>','<?php echo $value[0]['user_id']; ?>');ga('send', 'social', 'Pinterest', 'share', '<?php echo $brand_url; ?>');"><i class="fa fa-pinterest"></i></a></li>
                </ul>
                  <?php
                      if($this->session->userdata('user_id')){
                        if($this->home_model->get_fav_or_not('brand',$value[0]['user_id']) == 0){ ?>
                          <div class="user-likes"><a title="Add to Wish List" id="wishlist<?php echo $value[0]['user_id']; ?>" onclick="add_to_fav('brand','<?php echo $value[0]['user_id']; ?>','');ga('send', 'event', 'Brand', 'wishlist', '<?php echo $value[0]['company_name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love"></i></a></div>
                        <?php }else{ ?>
                          <div class="user-likes"><a title="Remove from Wish List" id="wishlist<?php echo $value[0]['user_id']; ?>" onclick="add_to_fav('brand',<?php echo $value[0]['user_id']; ?>,'');ga('send', 'event', 'Brand', 'wishlist', '<?php echo $value[0]['company_name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love active"></i></a></div>
                        <?php } }else{ ?>
                        <div class="user-likes"><a onclick="ga('send', 'event', 'Sign Up', 'clicked', 'favorite');" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a></div>
                      <?php }
                      ?>
                </div>
              </div>
            </div>
          </div>

	  <?php }//if
	  }//foreach ?>
      </div>
      </div>
    </div>
    <?php } ?>

    <!-- trending brands -->
    <?php if(!empty($top_products)) { ?>
    <div class="grid items-wrp items-section">
      <div class="title-section">
      <h1 class="title"><span><?php echo $this->lang->line('homepage_caption_fourth'); ?></span></h1>
      <?php if($page_filter_name_=='women' || $page_filter_name_=='men'){ ?>
      <a href="<?php echo base_url(); ?>all-products/<?php echo $page_filter_name_; ?>" class="link-view-all">View All Trending Products <i class="fa fa-caret-right"></i></a>
      <?php }else{ ?>
      <a href="<?php echo base_url(); ?>all-products" class="link-view-all">View All Trending Products <i class="fa fa-caret-right"></i></a>
      <?php  } ?>
    </div>
    <div class="row grid-items-wrp grid-items-slider">
      <div class="mobile-swiper-wrp">
      <?php foreach($top_products as $value){
        $brand_url = base_url().'product/details/'.$value[0]['slug'].'-'.$value[0]['id'];
      ?>
      <div class="col-md-4 col-sm-6 grid-item">
        <div class="item">
          <div class="item-img item-hover">
          <?php
              $brand_img_url = '';
                if($value[0]['image']!=''){
                  $brand_img_url = $this->config->item('product_image').$value[0]['image'];//base_url().'assets/images/brands/homepage/'
                }
          ?>
            <a href="<?php echo $brand_url; ?>">
              <?php if($brand_img_url){ ?>
              <img title="<?php echo $value[0]['name']; ?>- StyleCracker" alt="<?php echo $value[0]['name']; ?> - StyleCracker" src="<?php echo $brand_img_url; ?>">
                <?php } else { ?>
                    <img title="<?php echo $value[0]['name']; ?>- StyleCracker" alt="<?php echo $value[0]['name']; ?> - StyleCracker" src="<?php echo base_url(); ?>assets/images/image-na.jpg">
                <?php } ?>
            </a>
            <div class="item-look-hover">
              <a href="<?php echo $brand_url; ?>" onclick="ga('send', 'event', 'Products', 'clicked', '<?php echo $value[0]['name']; ?>');"><span class="btn btn-primary"> View More </span></a>
            </div>
            </div>
            <div class="item-desc">
              <div class="item-title"><a title="<?php echo $value[0]['name']; ?>" href="#"><?php echo $value[0]['name']; ?></a>
              </div>
              <div class="item-media">
                <ul class="social-buttons">
                  <li><a onclick="_targetFacebookShare('<?php echo $value[0]['name']; ?>','<?php echo $brand_url; ?>','<?php echo $brand_img_url; ?>','<?php echo $value[0]['id']; ?>'); ga('send', 'social', 'Facebook', 'share', '<?php echo $brand_url; ?>');"><i class="fa fa-facebook"></i></a></li>
                  <li><a onclick="_targetTweet('<?php echo $value[0]['name']; ?>','<?php echo $brand_url; ?>','<?php echo $brand_img_url; ?>','<?php echo $value[0]['id']; ?>');ga('send', 'social', 'Twitter', 'tweet', '<?php echo $brand_url; ?>');"><i class="fa fa-twitter"></i></a></li>
                  <li><a onclick="_targetPinterest('<?php echo $value[0]['name']; ?>','<?php echo $brand_url; ?>','<?php echo $brand_img_url; ?>','<?php echo $value[0]['id']; ?>');ga('send', 'social', 'Pinterest', 'share', '<?php echo $brand_url; ?>');"><i class="fa fa-pinterest"></i></a></li>
                </ul>
                  <?php
                      if($this->session->userdata('id')){
                        if($this->home_model->get_fav_or_not('product',$value[0]['id']) == 0){ ?>
                          <div class="user-likes"><a title="Add to Wish List" id="wishlist<?php echo $value[0]['id']; ?>" onclick="add_to_fav('product','<?php echo $value[0]['id']; ?>','');ga('send', 'event', 'Brand', 'wishlist', '<?php echo $value[0]['name']; ?>', '<?php echo $this->session->userdata('id'); ?>');"><i class="ic sc-love"></i></a></div>
                        <?php }else{ ?>
                          <div class="user-likes"><a title="Remove from Wish List" id="wishlist<?php echo $value[0]['id']; ?>" onclick="add_to_fav('product',<?php echo $value[0]['id']; ?>,'');ga('send', 'event', 'Brand', 'wishlist', '<?php echo $value[0]['name']; ?>', '<?php echo $this->session->userdata('id'); ?>');"><i class="ic sc-love active"></i></a></div>
                        <?php } }else{ ?>
                        <div class="user-likes"><a onclick="ga('send', 'event', 'Sign Up', 'clicked', 'favorite');" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a></div>
                      <?php }
                      ?>
                </div>
              </div>
            </div>
          </div>

        <?php } ?>
      </div>
      </div>
    </div>
    <?php } ?>



</div>
</div>
