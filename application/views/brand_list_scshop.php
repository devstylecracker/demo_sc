<div class="container">
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>">Home</a></li>
    <li class="active">Scshop</li>
  </ol>
 <?php if(!empty($top_brands)) { ?>
    <div class="grid items-wrp items-section">
      <div class="title-section">
      <h1 class="title"><span><?php echo $this->lang->line('scshop_brand_list'); ?></span></h1>
      <h5 class="title-desc">The SCShop at the StyleCracker Borough Mumbai has some super cool products just for you! Come register and win a SCSHOP20 coupon to get a 20% discount on these exclusive brands!</h5>
    </div>
      <div class="row grid-items-wrp" id="">
      <?php foreach($top_brands as $value){ ?>
      <div class="col-md-4 col-sm-6 grid-item">
        <div class="item">
          <div class="item-img item-hover">
          <?php
              $brand_img_url = '';
                if($value['home_page_promotion_img']!=''){
                  $brand_img_url = $this->config->item('brand_home_page_url').$value['home_page_promotion_img'];
                }
               $brand_url = base_url().'all-products/?brand='.$value['user_name'];
          ?>
            <a href="<?php echo $brand_url; ?>" onclick="ga('send', 'event', 'Brand', 'clicked', '<?php echo $value['company_name']; ?>');">
                <?php if($brand_img_url){ ?>
              <img title="<?php echo $value['company_name']; ?>- StyleCracker" alt="<?php echo $value['company_name']; ?> - StyleCracker" src="<?php echo $brand_img_url; ?>">
                <?php } else { ?>
                    <img title="<?php echo $value['company_name']; ?>- StyleCracker" alt="<?php echo $value['company_name']; ?> - StyleCracker" src="<?php echo base_url(); ?>assets/images/image-na.jpg">
                <?php } ?>
            </a>
            <!--div class="item-look-hover">
              <a href="#" target="_blank"><span class="btn btn-primary"> Get This </span></a>
            </div-->
            </div>
            <div class="item-desc">
              <div class="item-title"><a title="<?php echo $value['company_name']; ?>" href="<?php echo $brand_url; ?>" target="_blank"><?php echo $value['company_name']; ?></a>
              </div>
              <div class="item-media">
                <ul class="social-buttons">
                  <li><a onclick="_brandtargetFacebookShare('<?php echo $value['company_name']; ?>','<?php echo $brand_url; ?>','<?php echo $brand_img_url; ?>','<?php echo $value['user_id']; ?>'); ga('send', 'social', 'Facebook', 'share', '<?php echo $brand_url; ?>');"><i class="fa fa-facebook"></i></a></li>
                  <li><a onclick="_brandtargetTweet('<?php echo $value['company_name']; ?>','<?php echo $brand_url; ?>','<?php echo $brand_img_url; ?>','<?php echo $value['user_id']; ?>');ga('send', 'social', 'Twitter', 'tweet', '<?php echo $brand_url; ?>');"><i class="fa fa-twitter"></i></a></li>
                  <li><a onclick="_brandtargetPinterest('<?php echo $value['company_name']; ?>','<?php echo $brand_url; ?>','<?php echo $brand_img_url; ?>','<?php echo $value['user_id']; ?>');ga('send', 'social', 'Pinterest', 'share', '<?php echo $brand_url; ?>');"><i class="fa fa-pinterest"></i></a></li>
                </ul>
                  <?php
                      if($this->session->userdata('user_id')){
                        if($this->home_model->get_fav_or_not('brand',$value['user_id']) == 0){ ?>
                          <div class="user-likes"><a target="_blank" title="Add to Wish List" id="wishlist<?php echo $value['user_id']; ?>" onclick="add_to_fav('brand','<?php echo $value['user_id']; ?>','');ga('send', 'event', 'Brand', 'wishlist', '<?php echo $value['company_name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love"></i></a></div>
                        <?php }else{ ?>
                          <div class="user-likes"><a target="_blank" title="Remove from Wish List" id="wishlist<?php echo $value['user_id']; ?>" onclick="add_to_fav('brand',<?php echo $value['user_id']; ?>,'');ga('send', 'event', 'Brand', 'wishlist', '<?php echo $value['company_name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love active"></i></a></div>
                        <?php } }else{ ?>
                        <div class="user-likes"><a onclick="ga('send', 'event', 'Sign Up', 'clicked', 'favorite');" target="_blank" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a></div>
                      <?php }
                      ?>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>

      </div>
    </div>
    <?php } ?>
    </div>
