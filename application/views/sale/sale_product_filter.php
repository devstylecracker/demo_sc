<?php $max_price=0; $min_price=0; ?>
<div class="sidebar-filter is-sticky">
  <div class="sidebar-filter-title">
    Filter By <?php $url =  $_SERVER['REQUEST_URI'].'/'; ?>
  </div>
  <div class="sidebar-filter-inner" id="sc-filter-box-sale">
  	 <div class="filter-box">
      <div class="box">
        <div class="title clearfix scfiltertitle" data-toggle="collapse" data-target="#collapse-product-gender" aria-expanded="false" aria-controls="collapse-gender">
          <h6>Gender</h6>
          <span class="fa fa-minus"></span>
        </div>
        <div id="collapse-product-gender" class="collapse in" aria-expanded="true">
          <ul>
            <li>
             <label><span class="sc-checkbox">
                  <input type="checkbox" name="sale_gender[]" id="sale_gender_women" label-for="Women" class="ads_Checkbox" value="1"></span>
                  Women
              </label>
            </li>     

            <li>
             <label><span class="sc-checkbox">
                  <input type="checkbox" name="sale_gender[]" id="sale_gender_women" label-for="Men" class="ads_Checkbox" value="3"></span>
                  Men
              </label>
            </li>

            <li>
             <label><span class="sc-checkbox">
                  <input type="checkbox" name="sale_gender[]" id="sale_gender_women" label-for="Unisex" class="ads_Checkbox" value="2"></span>
                  Unisex
              </label>
            </li>

          </ul>
        </div>
           </div>
      

        <?php if(!empty($all_data['filter']['paid_brands'])){  ?>
          <div class="box">
        <div class="title clearfix scfiltertitle" data-toggle="collapse" data-target="#collapse-brands" aria-expanded="false" aria-controls="collapse-brands">
          <h6>Brands</h6>
          <span class="fa fa-minus"></span>
        </div>
        <div id="collapse-brands" class="collapse in collapse-brands-inner custom-scrollbar " aria-expanded="true" style="max-height:312px;">

          <ul>
          <?php foreach($all_data['filter']['paid_brands'] as $val){ ?>
          
             <li>
             <label><span class="sc-checkbox">
                  <input type="checkbox" name="brand" id="brand<?php echo $val['brand_id']; ?>" label-for="Men" class="brands_Checkbox" value="<?php echo $val['brand_id']; ?>"></span>
                  <?php echo $val['company_name']; ?>
              </label>
            </li>  

          <?php } ?>
          </ul>

        </div>
          </div>
        <?php } ?>
  </div>
</div>
