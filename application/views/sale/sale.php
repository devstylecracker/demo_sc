<div class="sc-sidebar-overlay-sm"></div>
<div class="page-all-products content-wrp">
<div class="filterpage-slider-wrp">
  <div class="container">
      <div class="swiper-container filterpage">
          <div class="swiper-wrapper">
          <?php
          $dir = './assets/images/brands/logos/';
            //foreach(glob($dir.'*.jpg') as $file) {
          if(!empty($all_data['filter']['paid_brands'])){
              foreach($all_data['filter']['paid_brands'] as $file) {
              //$only_file_name = str_replace($dir, '', $file);
              //$ar =explode('.', $only_file_name);
                if(file_exists('assets/images/brands/logos/'.$file['brand_id'].'.jpg')){
              ?>
                 <div class="swiper-slide">
              <div class="item-img">
                <div class="item-img-inner">
                  <a href="<?php echo base_url(); ?>brands/brand-details/<?php echo $this->Sale_model->getUsername($file['brand_id']); ?>" target="_blank">
                    <img src="<?php echo base_url().'assets/images/brands/logos/'.$file['brand_id'].'.jpg'; ?>" alt="<?php echo $file['company_name']; ?>">
                  </a>
                </div>
              </div>
            </div>
              <?php
            }
            }
          }
        ?>

          </div>
      </div>
      <div class="swiper-button-prev filterpage"></div>
      <div class="swiper-button-next filterpage"></div>

  </div>
</div>

<style>
.swiper-container{z-index:1;}
.filterpage-slider-wrp {padding: 5px 0; /*background: #F2F2F2; border-bottom: 1px solid #e3e3e3;*/ border-bottom: 1px solid #f1f1f1; overflow: hidden; margin-bottom: 20px;}
.filterpage-slider-wrp .container{position: relative;}
.swiper-container.filterpage .swiper-slide {width:100px;}
.swiper-container.filterpage .item-img {
    height: 100px;
/*    background: #fff none repeat scroll 0 0;*/
    margin-bottom: 10px;
    text-align: center;
    margin:1px;
  /*  margin: 1px 5px;*/
    /*outline: 1px solid #cdcdcf;*/
    padding1: 10px;
    position: relative;
}

.swiper-container.filterpage .item-img:hover{box-shadow:0 0 5px #e1e1e1; background: #fff;}
.swiper-container.filterpage .item-img-inner {
    position: relative;
    top: 50%;
    -webkit-transform: translateY(-50%);
	-moz-transform: translateY(-50%);
	transform: translateY(-50%);
}
.swiper-container.filterpage  img {
    width: auto;
    height: auto;
    max-width: 100%;
    max-height: 100%;     
}
.swiper-button-prev.filterpage, .swiper-button-next.filterpage{background: none; color: #999; border-radius: 50%;  border:1px solid transparent;}
.swiper-button-prev.filterpage:hover, .swiper-button-next.filterpage:hover{ color: #333; border:1px solid #ddd; background: #fff; box-shadow:0 0 5px #e1e1e1;}
.swiper-button-prev.filterpage{left:-20px; }
.swiper-button-next.filterpage{right:-20px}

.sidebar-filter .filter-box li label { 
  overflow: hidden;
  text-overflow: clip;
  white-space: nowrap;
}

@media(max-width:768px) {
	.filterpage-slider-wrp {padding: 5px 0; border:none; }
	.swiper-container.filterpage{display:none;}
	.swiper-container.filterpage .swiper-slide {width:60px;}
	.swiper-container.filterpage .item-img { height: 60px;}
}

</style>
<div class="container">

  <div class="filterbar-wrp sm-only">
          <div class="container1">
              <div class="row">
                <div class="col-sm-4 col-xs-4 filter-btns-wrp-sm">
                <div class="btn btn-primary btn-sm btn-filter-by">
                    Show Filter
                  </div>
                </div>
              <div class="col-md-12 col-sm-8 col-xs-8">
                <div class="filterbar">
                  <span class="filter-label">Sort by: </span>
                    <select name="product_sort_by_sale" id="product_sort_by_sale" class="selectpicker product_sort_by_sale">
                      <option value="0">Latest</option>
                      <option value="1">Price: High to Low</option>
                      <option value="2">Price: Low to High</option>
                    </select>
                </div>
              </div>
            </div>
          </div>
        </div>

    <div class="row layout-2-col">
      <div class="col-md-2 layout-2-col sidebar-filter-wrp sticky-sidebar">
        <?php echo $left_filter; ?>
      </div>
    </div>
  <div class="col-md-10 layout-2-col">
    <div class="title-section">
  <h1 class="title"><span><?php echo 'Sale Products'; ?></span></h1>


    </div>

    <div class="filterbar-wrp searchbar-wrp filterbar-wrp-md md-only">
        <h2 class="filter-result-title hide" id="filter-result-title"><?php echo @$products_total; ?> All Products<?php echo @$products_total > 1 ? 's' : ''; ?></h2>

        <div class="filterbar">
          <span class="filter-label">Sort by: </span>
            <select name="product_sort_by_sale" id="product_sort_by_sale" class="selectpicker product_sort_by_sale">
              <option value="0">Latest</option>
              <option value="1">Price: High to Low</option>
              <option value="2">Price: Low to High</option>
            </select>
        </div>
    </div>
    <div class="filter-selction-box1" id="filter-selction-box1">


    </div>


      <?php if(!empty($all_data['products'])) { ?>
    <div class="grid grid-products-col-4 items-wrp items-section">
      <div class="row grid-items-wrp" id="get_all_cat_products">
      <?php foreach ($all_data['products'] as $value) { ?>
      <div class="col-md-4 col-sm-4 col-xs-6 grid-item">
        <div class="item item-product">
          <div class="item-img item-hover">
            <div class="item-img-inner">
              <?php
                $short_url = base_url().'product/details/'.$value['slug'].'-'.$value['id'];
                $img_title_alt = str_replace('#','',$value['name']);
                $img_url = $this->config->item('product_image').$value['image'];
              ?>
                 <a href="<?php echo $short_url; ?>"  >
                  <?php if(file_exists($this->config->item('products_H340').$value['image'])) { ?>
                 <img class="lazy" data-original="<?php echo $this->config->item('products_340').$value['image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  />
                 <?php }else{ ?>
                  <img class="lazy" data-original="<?php echo $this->config->item('products_310').$value['image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  />

                  <?php } ?>
                 </a>
               </div>
              <div class="item-look-hover">

              <?php if($this->Allproducts_model->product_stock_check($value['id']) != 1){ ?>
                <a href="<?php echo $short_url; ?>" onclick="ga('send', 'event', 'Product', 'clicked', '<?php echo $value['name']; ?>'); _beatout_view_product('<?php echo $value['id']; ?>','<?php echo $user_id; ?>','<?php echo $value['price']; ?>','<?php echo $value['name']; ?>','','view');" title="<?php echo $img_title_alt; ?>" >
                <span class="btn-out-of-stock">Sold Out</span>
                </a>
                <?php }else{ ?>
                  <a href="<?php echo $short_url; ?>" onclick="ga('send', 'event', 'Product', 'clicked', '<?php echo $value['name']; ?>'); _beatout_view_product('<?php echo $value['id']; ?>','<?php echo @$user_id; ?>','<?php echo $value['price']; ?>','<?php echo $value['name']; ?>','','view');" title="<?php echo $img_title_alt; ?>" >
                  <span class="btn btn-primary"><?php echo $this->lang->line('get_this'); ?></span>
                  </a>
              <?php } ?>
              </div>

            </div>
            <div class="item-desc">
            <?php $lookPrdName = stripslashes($value['name']); if (strlen($lookPrdName) >= 84){ $stringCut = substr($lookPrdName, 0, 81); } ?>
              <div class="item-title"><a title="<?php echo $img_title_alt; ?>" href="<?php echo $short_url; ?>"  ><?php echo ucwords(strtolower($lookPrdName)); ?></a>
              </div>
              <div class="item-brand">By <span><?php echo $this->Allproducts_model->get_brand_name($value['brand_id']); ?></span></div>
              <div class="item-price-wrp">
                <!--div class="item-price">
                  <?php if($this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price']>0) { ?>
                  <span class="mrp-price text-strike"><i class="fa fa-inr"></i><?php echo $value['price']; ?></span>
                  <span class="price"><i class="fa fa-inr"></i><?php echo $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price'] : ''; ?></span>
                  <span class="discount-price">(<?php echo round($this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discount_percent']); ?>% OFF)</span>
                  <?php }else{ ?>
                  <span class="price"><i class="fa fa-inr"></i> <?php echo $value['price']; ?></span>
                  <?php } ?>
                </div-->

                  <div class="item-price">
                  <?php if($this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price']>0 || $value['compare_price']>$value['price']) { ?>
                  <span class="mrp-price text-strike"><i class="fa fa-inr"></i><?php if($value['compare_price']>$value['price']){ echo $value['compare_price']; }else { echo $value['price']; $value['compare_price'] = $value['price'];} ?></span>
          <span class="price"><i class="fa fa-inr"></i> <?php $discount_price = $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price'] : ''; if($discount_price > 0){ echo $discount_price; }else{
            echo $value['price'];
          } ?></span>
                  <span class="discount-price">(<?php $discount_percent = $this->Product_desc_model->calculate_discount_percent($value['compare_price'],$discount_price,$value['price']); echo $discount_percent; ?>% OFF)</span>
                  <?php }else{ ?>
                  <span class="price"><i class="fa fa-inr"></i> <?php echo $value['price']; ?></span>
                  <?php } ?>
                </div>
                 
                <?php
                      if($this->session->userdata('user_id')){
                        if($this->home_model->get_fav_or_not('product',$value['id']) == 0){ ?>
                          <div class="user-likes"><a title="Add to Wish List" id="wishlist<?php echo $value['id']; ?>" onclick="add_to_fav('product','<?php echo $value['id']; ?>','<?php echo $user_id; ?>','<?php echo $value['price']; ?>','<?php echo $value['name']; ?>','<?php echo @$value['product_cat_id']; ?>','add_to_wishlist'); ga('send', 'event', 'Product', 'wishlist', '<?php echo $value['name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love"></i></a></div>
                        <?php }else{ ?>
                          <div class="user-likes"><a title="Remove from Wish List" id="wishlist<?php echo $value['id']; ?>" onclick="add_to_fav('product','<?php echo $value['id']; ?>','<?php echo $user_id; ?>','<?php echo $value['price']; ?>','<?php echo $value['name']; ?>','<?php echo @$value['product_cat_id']; ?>','remove_from_wishlist'); ga('send', 'event', 'Product', 'wishlist', '<?php echo $value['name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love active"></i></a></div>
                        <?php } }else{ ?>
                        <div class="user-likes"><a onclick="ga('send', 'event', 'Sign Up', 'clicked', 'favorite');" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a></div>
                      <?php }  ?>
              </div>
            </div>
          </div>
        </div>
       <?php }?>

      </div>
    </div>
    <?php }else{ ?>
        <div class="grid-no-products"> No Products Found </div>

    <?php  } ?>


<div class="fa-loader-wrp-outer">
    <div style="display: none;" class="fa-loader-wrp-products">
      <i class="fa fa-circle-o-notch fa-spin-custom"></i> Loading...
    </div>
 </div>

  </div>
</div>

<?php echo @$pagination; ?>
</div>
</div>
<input id="brand_slider_count" name="brand_slider_count" value="2" type="hidden">
<input id="sort_price" name="sort_price" value="0" type="hidden">