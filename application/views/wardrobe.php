 <script src="<?php echo base_url()?>assets/js/ajaxfileupload.js"></script>
 <div class="page-wardrobe wardrobe-outer">
   <div class="container">
    <div class="page-header wardrobe-header">
      <h1>Wardrobe</h1>
      <h6>
      Upload images from your existing wardrobe and we will help you with a stylish fix.
    </h6>
    <form method="post" action="" id="frmWordrobe" enctype="multipart/form-data">
      <div class="btn btn-primary btn-file-wrp">
        Upload Photo
        <input type="file" id="upload_photoes" class="btn-file" name="upload_photoes[]" multiple>
    </div>
    </form>    
    <?php if(isset($_POST['upload_photoes'])){ ?>
    <div class="alert alert-success">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Success!</strong> File successfully uploaded.
    </div>
    <?php } ?>

    </div>
  </div>
    <div class="container">
      <div class="wardrobe-wrp">
        <div class="grid wardrobe-photos-wrp">
          <div id="wardrobe_images_grid" class="row wardrobe-images-grid">
          </div>
        </div>

      </div>

    </div>
  </div>

  <!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
  <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls wardrobe-popup" data-use-bootstrap-modal="false">
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->

    <h3 class="title"></h3>
    <a class="prev"></a>
    <a class="next"></a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
   <div class="modal fade  wardrobe-popup-dialog false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div>
            <button type="button" class="close" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body next"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-xs pull-left prev">
              <i class="glyphicon glyphicon-chevron-left"></i>
              Previous
            </button>
            <button type="button" class="btn btn-primary btn-xs next">
              Next
              <i class="glyphicon glyphicon-chevron-right"></i>
            </button>
          </div>
        </div>
      </div>
    </div>

  </div>


  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/blueimp-gallery.min.css">
  <script src="<?php echo base_url()?>assets/js/jquery.blueimp-gallery.min.js"></script>
  <script src="<?php echo base_url()?>assets/js/bootstrap-image-gallery.min.js"></script>
<script>
$(document).ready(function() {
//## UserTrack: Page ID=========================================
/*Code ends*/
document.getElementById('wardrobe_images_grid').getElementsByTagName('a').onclick = function (event) {
    event = event || window.event;
    var target = event.target || event.srcElement,
        link = target.src ? target.parentNode : target,
        options = {index: link, event: event},
        links = this.getElementsByTagName('a');
    blueimp.Gallery(links, options);
};
});
</script>
  <!-- / wardrobe popup-->


  <script type="text/javascript">
  	 $(function() {
  	 	$('#upload_photoes').change(function(e){
        	$('#frmWordrobe').submit();
      	});
      	wardrobe_images_grid();
  	 });

  	  $('#frmWordrobe').submit(function(e) {
        //e.preventDefault();
        $.ajaxFileUpload({
          url       :'<?php echo base_url(); ?>profile/upload_wordrobe/',
          fileElementId :'upload_photoes',
          dataType    : 'json',
          success : function (data, status)
          {
            if(data.status == 'success'){
              location.reload();
            }else{

            }

          }
        });
        return false;
      });

function wardrobe_images_grid(){
      $.ajax({
        url: '<?php echo base_url(); ?>profile/display_wardrobe',
        type: 'GET',
        cache :true,
        success: function(response) {
          $('#wardrobe_images_grid').html(response);
        },
        error: function(xhr) {

        }
      });
    }
  function delete_wardrobe(id){
      $.ajax({
        url: '<?php echo base_url(); ?>profile/delete_wardrobe_img',
        type: 'POST',
        data: {'id': id },
        cache :true,
        success: function(response) {
          wardrobe_images_grid();
        },
        error: function(xhr) {

        }
      });
    }
  </script>
