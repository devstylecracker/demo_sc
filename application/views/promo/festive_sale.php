<div class="page-brand-details">
  <div class="container custom">
    <div class="brands-banner-wrp">
        <img src="<?php echo base_url(); ?>assets/images/promo/wedding-pop-up-web-banner-min.jpg">
        <!--<img src="<?php echo base_url(); ?>assets/images/rakhi_special/festive_popup.jpg">-->		
    </div>
  </div>

  <div class="container">
  <!--  <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a></li>
      <li class="active">Rakhi Special</li>
    </ol>-->
<div class="breadcrumb"></div>
    <div class="content-wrp">
     <section class="desc-section" style="text-align: center; font-size: 20px; font-style: italic;">
     <!-- <p>Exclusive Designers | 20 Days <br>			
        </p>-->
      </section>
      <div class="title-section">
        <h1 class="title"><span>Wedding Pop-Up</span></h1>
      </div>

      <ul class="nav nav-tabs common-tabs">
        <li><a data-toggle="tab" href="#brands">Brands</a></li>
        <li class="active"><a data-toggle="tab" href="#category">Categories</a></li>
      </ul>

       <div class="tab-content common-tab-content">
        <div id="brands" class="tab-pane fade">

      <!--div class="title-section">
        <h1 class="title"><span>Brands</span></h1>
      </div-->

      <section class="grid items-wrp items-section">
        <div class="row grid-items-wrp">


   

        <?php if(!empty($brands)){ 
          foreach($brands as $val){
        ?>
          <div class="col-md-3 col-sm-4 grid-item">
            <div class="item item-cat">
              <div class="item-img item-hover">
                <div class="item-img-inner">
                  <?php if($val['user_id'] == 27689){ ?>
                  <a href="<?php echo base_url(); ?>brand/rossoyuki/women/women-accessories/women-accessories-bags">
                  <?php }else{ ?>
                  <a href="<?php echo base_url(); ?>brand/<?php echo $val['user_name']; ?>">
                  <?php } ?>
                  <?php if($val['home_page_promotion_img']!=''){ ?>
                    <img src="<?php echo $this->config->item('brand_home_page_url'); ?>/<?php echo $val['home_page_promotion_img']; ?>" alt="" title="">
                  <?php }else{ ?>
                    <img title="<?php echo $val['home_page_promotion_img']; ?>- StyleCracker" alt="<?php echo $val['home_page_promotion_img']; ?> - StyleCracker" src="<?php echo base_url(); ?>assets/images/image-na.jpg">
                  <?php } ?>
                  </a>
                </div>
                <div class="item-look-hover">
                     <?php if($val['user_id'] == 27689){ ?>
                      <a href="<?php echo base_url(); ?>brand/rossoyuki/women/women-accessories/women-accessories-bags">
                      <?php }else if($val['user_id'] == 20839){ ?>
                      <a href="<?php echo base_url(); ?>brand/RuVya/women/women-indian-wear">
                      <?php }else if($val['user_id'] == 33412){ ?>
                      <a href="<?php echo base_url(); ?>brand/augustline/women/women-accessories/womens-wear-accessories-jewellery">
                      <?php }else if($val['user_id'] == 23348){ ?>
                      <a href="<?php echo base_url(); ?>brand/zaeradesigns/women/women-footwear/women-footwear-footwear/women-footwear-footwear-ethnic">
                      <?php }else{ ?>
                      <a href="<?php echo base_url(); ?>brand/<?php echo $val['user_name']; ?>">
                      <?php } ?>
                  <span class="btn btn-primary"> Get This </span></a>
                </div>
              </div>
              <div class="item-desc">
              <div class="item-title"><a href="javascript:void(0);" title="<?php echo $val['company_name']; ?>"><?php echo $val['company_name']; ?></a></div>
			</div>
        </div>
      </div>
      <?php } } ?>
    </div>
  </section>
</div>
        <div id="category" class="tab-pane fade in active">
     <!--div class="title-section">
        <h1 class="title"><span>Category</span></h1>
      </div-->

      <section class="grid items-wrp items-section">
        <div class="row grid-items-wrp">

        <?php if(!empty($categories)){ 
          foreach($categories as $val){
            $category_img_url = $this->home_model->cat_img_exists($val['id']);
        ?>
          <div class="col-md-3 col-sm-4 grid-item">
            <div class="item item-cat">
              <div class="item-img item-hover">
                <div class="item-img-inner">
                  <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/products/<?php echo $val['category_slug']; ?>-<?php echo $val['id']; ?>">
                   <?php if(file_exists($category_img_url)){ ?>
                    <img src="<?php echo base_url().$category_img_url; ?>" alt="" title="">
                  <?php }else{ ?>
                    <img title="<?php echo $val['category_name']; ?>- StyleCracker" alt="<?php echo $val['category_name']; ?> - StyleCracker" src="<?php echo base_url(); ?>assets/images/image-na.jpg">
                  <?php } ?>
                  </a>
                </div>
                <div class="item-look-hover">
                  <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/products/<?php echo $val['category_slug']; ?>-<?php echo $val['id']; ?>"><span class="btn btn-primary"> Get This </span></a>
                </div>
              </div>
              <div class="item-desc">
              <div class="item-title"><a href="javascript:void(0);" title="<?php echo $val['category_name']; ?>"><?php echo $val['category_name']; ?></a></div>
      </div>
        </div>
      </div>
      <?php } } ?>
    </div>
  </section>
</div>
</div>

</div>
</div>
</div>

<style>
.item.item-cat .item-img {
  height: 276px;
}
</style>
