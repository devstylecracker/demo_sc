<div class="page-brand-details111">
  <div class="container custom">
    <div class="brands-banner-wrp">
      <!--<img src="<?php echo base_url(); ?>assets/images/promo/test1.jpg">-->
        <img src="<?php echo base_url(); ?>assets/images/promo/kavalia_1.jpg">
		<div class="desc-section" style=" bottom: 10%; 
    left: 0; position: absolute; right: 0; text-align:center;">
     <!-- <div style="background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0; color: #fff; font-size: 18px;font-weight: bold;
    padding: 20px;display:inline-block;">
	  <p>
			Vintage inspired men's accessories for the modern guy. In with the old, out with the new.<br/>
			Shop one-of-a-kind accessories for your brother this Rakhi. Turn the tables around.			
        </p>
		<a href="https://www.stylecracker.com/brands/brand-details/thegentlemanscommunity" class="btn btn-primary">Shop Now</a>
		
      </div> -->
	  </div>
    </div>
  </div>

  <div class="container">
  <!--  <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a></li>
      <li class="active">Rakhi Special</li>
    </ol>-->
<div class="breadcrumb"></div>
    <div class="content-wrp">
     <section class="desc-section" style="text-align: center; font-size: 20px; font-style: italic;">
        <p>
			StyleCracker styles the gorgeous Kajal Aggarwal in one of the most awaited films of the year: Kavalai Vendam! Lucky for you, we are making select looks shoppable exclusively! #WearItLikeKajal
        </p>
      </section>
      

      <section class="grid items-wrp items-section">
        <div class="row grid-items-wrp">

          <?php if(!empty($looks)) { ?>
          <div class="grid grid-looks-col-3 items-wrp items-section">
            <div class="title-section">
                <h1 class="title"><span>Shop Looks</span></h1>

                <div class="filterbar">
                <span class="filter-label">Filter by: </span>
                        <select name="price_flt" id="price_flt" class="selectpicker">
                          <option value="0">Low To High</option>
                          <option value="1">High To Low</option>                     
                        </select>
                  </div>
            </div>

            <div class="filterbar-wrp">
              <div class="container">
                
              </div>
            </div>

            <div class="row grid-items-wrp all-looks111" id="get_all_category_wise_looks">
            <?php foreach ($looks as $value) { ?>
            <div class="col-md-4 col-sm-6 grid-item">
              <div class="item item-look">
                <div class="item-img item-hover">
                    <?php
                      $short_url = base_url().'looks/look-details/'.$value['slug'];
                      $img_title_alt = str_replace('#','',$value['name']);

                      if($value['look_type'] == 1){ ?>
                        <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>"><img src="<?php echo $this->config->item('sc_look_image_url').$value['image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                      <?php $img_url = $this->config->item('sc_look_image_url').$value['image'];
                        }else if($value['look_type'] == 2 || $value['look_type'] == 4 || $value['look_type'] == 6){ ?>
                        <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>"><img src="<?php echo $this->config->item('sc_street_style_image_url').$value['look_image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                      <?php $img_url = $this->config->item('sc_street_style_image_url').$value['look_image'];
                      }else if($value['look_type'] == 3){ ?>
                      <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>"><img src="<?php echo $this->config->item('sc_promotional_look_image').$value['product_img']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                      <?php $img_url = $this->config->item('sc_promotional_look_image').$val['product_img'];
                      }
                      ?>

                  <div class="item-look-hover">
                    <a onclick="ga('send', 'event', 'Look', 'clicked', '<?php echo $value['name']; ?>');" href="<?php echo $short_url; ?>" title="<?php echo $img_title_alt; ?>" target="_blank"><span class="btn btn-primary"><?php echo $this->lang->line('get_this'); ?></span></a>
                  </div>
                  </div>
                  <div class="item-desc">
                    <div class="item-title"><a title="<?php echo $img_title_alt; ?>" href="<?php echo $short_url; ?>" target="_blank"><?php echo $value['name']; ?></a>
                    </div>
                    <div class="item-media">
                      <ul class="social-buttons">
                        <li><a onclick="_targetFacebookShare('<?php echo $value['name']; ?>','<?php echo $short_url; ?>','<?php echo $img_url; ?>','<?php echo $value['id']; ?>'); ga('send', 'social', 'Facebook', 'share', '<?php echo $short_url; ?>');"><i class="fa fa-facebook"></i></a></li>
                        <li><a onclick="_targetTweet('<?php echo $short_url; ?>','<?php echo $value['name']; ?>','<?php echo $value['id']; ?>');ga('send', 'social', 'Twitter', 'tweet', '<?php echo $short_url; ?>');"><i class="fa fa-twitter"></i></a></li>
                        <li><a onclick="_targetPinterest('<?php echo $short_url; ?>','<?php echo $value['name']; ?>','<?php echo $value['id']; ?>','<?php echo $img_url; ?>');ga('send', 'social', 'Pinterest', 'share', '<?php echo $short_url; ?>');"><i class="fa fa-pinterest"></i></a></li>
                      </ul>

                      <?php
                      if($this->session->userdata('user_id')){
                        if($this->home_model->get_look_fav_or_not($value['id']) == 0){ ?>
                          <div class="user-likes"><a target="_blank" title="Add to Wish List" id="whislist<?php echo $value['id']; ?>" onclick="add_to_wishlist(<?php echo $value['id']; ?>);ga('send', 'event', 'Look', 'wishlist', '<?php echo $value['name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love"></i></a></div>
                        <?php }else{ ?>
                          <div class="user-likes"><a target="_blank" title="Remove from Wish List" id="whislist<?php echo $value['id']; ?>" onclick="remove_from_wishlist(<?php echo $value['id']; ?>);ga('send', 'event', 'Look', 'wishlist', '<?php echo $value['name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love active"></i></a></div>
                        <?php } }else{ ?>
                        <div class="user-likes"><a target="_blank" title="Add to Wish List" onclick="ga('send', 'event', 'Sign Up', 'clicked', 'favorite');" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a></div>
                      <?php }
                      ?>

                      </div>
                    </div>
                  </div>
                </div>

       <?php }  ?>
</div>
 </div>
  </div>
  </section>
      <?php if(!empty($products)){ ?>
  <div class="title-section">
    <h2 class="title"><span>Shop Products</span></h2>

     <div class="filterbar">
        <span class="filter-label">Filter by: </span>
          <select name="price_flt_for_products" id="price_flt_for_products" class="selectpicker">
            <option value="-1" disabled="" selected>Select</option>
            <option value="0">Low To High</option>
            <option value="1">High To Low</option>                     
          </select>
     </div>

  </div>
  <div class="grid grid-products-col-4 items-wrp"> 
<div class="row grid-items-wrp" id="kvlist">


          <?php  foreach ($products as $val){   ?>
            <div class="col-md-3 col-sm-4 col-xs-6 grid-item listing-item" data-listing-price ="<?php echo $val['price']; ?>">
              <div class="item item-product">
              <?php
                  $user_link = 0;
                  if($this->session->userdata('user_id')){ $user_link = $this->session->userdata('user_id')!='' ? $this->session->userdata('user_id') : 0 ; }
                  ?>
                          <div class="item-img item-hover">
                            <?php if($val['stock_count'] == 0){  ?>
                              <div class="btn-out-of-stock">Sold Out</div>
                              <?php } ?>
                                <div class="item-img-inner">
                                <a href="<?php echo base_url(); ?>product/details/<?php echo $val['slug']; ?>-<?php echo $val['id']; ?>">
                                <?php if(file_exists($this->config->item('products_340').@$val['image'])) { ?>
                                <img src="<?php echo $this->config->item('products_340').@$val['image']; ?>" alt="<?php echo @$val['name']; ?> - StyleCracker" title="<?php echo @$val['name']; ?> - StyleCracker"/>
                                <?php }else{ ?>
                                <img src="<?php echo $this->config->item('products_340').@$val['image']; ?>" alt="<?php echo @$val['name']; ?> - StyleCracker" title="<?php echo @$val['name']; ?> - StyleCracker"/>
                                <?php } ?>
                                </a>
                              </div>
                              <div class="item-look-hover">
                                  <a title="<?php echo @$val['name']; ?>" href="<?php echo base_url(); ?>product/details/<?php echo $val['slug']; ?>-<?php echo $val['id']; ?>" onclick="fbq('track', 'ViewContent');"><span class="btn btn-primary">Get this</span>
                                  </a>
                              </div>

                              <div class="item-look-hover">
                                  <a onclick="fbq('track', 'ViewContent'); " title="<?php echo @$val['name']; ?>" href="<?php echo base_url(); ?>product/details/<?php echo $val['slug']; ?>-<?php echo $val['id']; ?>"><span class="btn btn-primary">Get this</span>
                                  </a>
                            </div>

                          </div>

                          <div class="item-desc" style="height:90px;">
                            <div class="item-title" title="<?php echo stripslashes($val['name']); ?>">
                                <a href="<?php echo base_url(); ?>product/details/<?php echo $val['slug']; ?>-<?php echo $val['id']; ?>"><?php
                                $lookPrdName = stripslashes($val['name']);
                                  if (strlen($lookPrdName) >= 50)
                                  {
                                    // truncate string
                                    $stringCut = substr($lookPrdName, 0, 47);
                                    // make sure it ends in a word so assassinate doesn't become ass...

                                    echo $stringCut.'...';
                                  }else
                                  {
                                    echo $lookPrdName;
                                  }

                                 ?>  </a>
                            </div>
                            <div class="item-price-wrp">
              
        <div class="item-price">
                     <?php if($this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price']>0 || $val['compare_price']>$val['price']) { ?>
                  <span class="mrp-price text-strike"><i class="fa fa-inr"></i> <?php if($val['compare_price']>$val['price']){ echo $val['compare_price']; }else { echo $val['price']; $val['compare_price'] = $val['price'];} ?></span>
          <span class="price"><i class="fa fa-inr"></i> <?php $discount_price = $this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price'] : ''; if($discount_price > 0){ echo $discount_price; }else{
            echo $val['price'];
          } ?></span>
                  <span class="discount-price">(<?php $discount_percent = $this->Product_desc_model->calculate_discount_percent($val['compare_price'],$discount_price,$val['price']); echo $discount_percent; ?>% OFF)</span>
          
                  <?php }else{ ?>
                  <span class="price"><i class="fa fa-inr"></i> <?php echo $val['price']; ?></span>
                  <?php } ?>
                    </div>
                            <?php
                               if($this->session->userdata('user_id')){
                                 if($this->home_model->get_fav_or_not('product',$val['id']) == 0){ ?>
                                   <div class="user-likes"><a target="_blank" title="Add to Wish List" id="wishlist<?php echo $val['id']; ?>" onclick="add_to_fav('product','<?php echo $val['id']; ?>','<?php echo $betaout_user_id; ?>','<?php echo $val['price']; ?>','<?php echo $val['name']; ?>','<?php echo $val['product_cat_id']; ?>','add_to_wishlist'); ga('send', 'event', 'Product', 'wishlist', '<?php echo $val['name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love"></i></a></div>
                                 <?php }else{ ?>
                                   <div class="user-likes"><a target="_blank" title="Remove from Wish List" id="wishlist<?php echo $val['id']; ?>" onclick="add_to_fav('product',<?php echo $val['id']; ?>,'<?php echo $betaout_user_id; ?>','<?php echo $val['price']; ?>','<?php echo $val['name']; ?>','<?php echo $val['product_cat_id']; ?>','remove_from_wishlist');ga('send', 'event', 'Product', 'wishlist', '<?php echo $val['name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love active"></i></a></div>
                                 <?php } }else{ ?>
                                 <div class="user-likes"><a target="_blank" onclick="ga('send', 'event', 'Sign Up', 'clicked', 'favorite');" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a></div>
                               <?php }  ?>
                          </div>
                          </div>
                </div>
              </div>
                <?php } } ?>
    </div>
 </div>
    
    <?php } ?>
</div>
<input type="hidden" name="offset" id="offset" value="0">
<input type="hidden" name="total_looks" id="total_looks" value="<?php echo $total_look; ?>">

   
</div>
</div>
</div>

<style>
.item.item-cat .item-img {
  height: 276px;
}
.title-section .filterbar{position: absolute;; right: 0; top: -10px;}
@media(max-width: 768px){
  .items-section .filterbar{position: static;}
  .title-section{text-align: center;}
}
</style>
