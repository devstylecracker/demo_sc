<?php
/*echo '<pre>';
print_r( $_SESSION);
exit;*/
//if(isset($_SERVER['HTTP_REFERER'])) echo $_SERVER['HTTP_REFERER'];
?>
<style>
  .section-bg {
    background: #EBEFF0;
  }

  .title-section .title {
    color: #080a0b;
    font-size: 32px;
    font-weight: bold;
    line-height: 1.3;
    margin-bottom: 30px;
  }

  .section-steps .section-steps-box {
    text-align: center;
    padding: 20px;
      margin-bottom: 30px;
  }

  .section-steps .section-steps-box .title {
    color: #4d4f50;
    /*font-size: 28px;
font-weight: bold;*/
    font-size: 60px;
    font-weight: 500;
    line-height: 1.1;
    margin-bottom: 15px;
  }

  .section-steps .section-steps-box span {
    border-bottom: 2px solid;
    display: inline-block;
    padding-bottom: 2px;
  }

  .section-steps .section-steps-box .subtitle {
    color: #555;
    font-size: 18px;
    font-weight: bold;
    line-height: 1.5;
    margin-bottom: 15px;
  }

  .section-steps .section-steps-box .desc {
    color: #5e6361;
    font-size: 14px;
    font-weight: 500;

  }

  .section-steps-4 {
    padding: 50px 20px 0 0;
  }

  .section-steps-4 .btns-wrp{margin-top: 30px;}
  .section-steps-4 .title {
    color: #080a0b;
    font-size: 32px;
    font-weight: bold;
    line-height: 1.1;
    margin-bottom: 20px;
  }

  .section-steps-4 .desc {
    font-size: 14px;
    font-weight: 500;
    margin-bottom: 30px;
  }

  .section-steps-4 .desc li {
    margin-bottom: 10px;
    line-height: 1.5;
  }

  .section-steps-4-wrp {
    padding: 60px 0;
  }

  .section-steps-4-wrp .img-wrp {
    text-align: center;
  }

  .section-steps-4-wrp .img-wrp img {
    width: 360px;
    max-width: 100%;
  }
  @media (max-width:768px) {
.title-section .title{margin:0 auto 10px; padding: 5px; font-size: 28px;}
  .section-steps .section-steps-box .title {font-size: 40px; margin-bottom: 10px;}
  .section-steps .section-steps-box{padding: 5px 0;}
  .section-steps-4-wrp { padding: 30px 0;}
  .section-steps-4{padding: 0;}
  .section-steps-4 .title{font-size: 28px;text-align: center;}
  .section-steps-4 .btns-wrp{text-align: center;}
  .section-steps-4 .btns-wrp .form-control{margin-bottom: 15px;display: inline-block; width:80%;}
  }
</style>
<div class="page-brand-details">
  <div class="container custom">
    <div class="brands-banner-wrp">
      <img src="<?php echo base_url(); ?>assets/images/promo/personoalised-box-banner-min.jpg">
	  <!-- <img src="<?php echo base_url(); ?>assets/images/promo/scbox-banner.jpg">-->
    </div>
  </div>
  <div class="section-bg">
    <div class="container">
      <!--  <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a></li>
      <li class="active">SC Box</li>
    </ol>-->
      <div class="breadcrumb"></div>
      <div class="content-wrp111">
        <div class="title-section">
          <h1 class="title">How It Works?</h1>
        </div>
        <section class="section-steps">
          <div class="row">
            <div class="col-md-4 col-sm-4">
              <div class="section-steps-box">
                <div class="title">
                  <span>01</span>
                </div>
                <div class="subtitle">
                  Celebrity stylists pick fashion for you based on your preferences.
                </div>
                <div class="desc">
                  Sign up with a snap of your finger, select a budget and leave it up to your personal stylist to select the best for you.
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-4">
              <div class="section-steps-box">
                <div class="title">
                  <span>02</span>
                </div>
                <div class="subtitle">
                  Super-fast Delivery
                </div>
                <div class="desc">
                  If the door-bell rings, expect a box of surprises! You deserve a delight, so open it quick.
                </div>


                <!--div class="desc">
                  If the door-bell rings, expect a box of surprises! You deserve a delight, so open it quick.
                </div-->


              <?php if(!$this->session->userdata('user_id')){ ?>
                <a data-toggle="modal" href="#myModal" class="btn btn-primary" style="margin-top:50px;">Get Started</a>
                <?php }else{
                ?>
                <a data-toggle="modal" class="btn btn-primary" style="margin-top:50px;" onclick="book_scbox_focus()">Get Started</a>    
                <?php } ?>

              </div>
            </div>
            <div class="col-md-4 col-sm-4">
              <div class="section-steps-box">
                <div class="title">
                  <span>03</span>
                </div>
                <div class="subtitle">
                  Didn’t like something?
                </div>
                <div class="desc">
                  Fret not! In the rare occasion something from your box doesn’t excite you, keep what you like and send the rest back. Get cash-back for the returned items.
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>

  <div class="container">
    <section class="section-steps-4-wrp">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <div class="section-steps-4">
            <div class="title">
              WHAT’S FOR YOU?
            </div>
            <div class="desc">
              <ul>
                <li>
                  <strong>Well, festive pampering doesn’t need a reason.</strong></li>
                <li> Just as effortless as it sounds, you tell us what you need and we’ll bring it to you.</li>
                <li>The most fashionable and stylish finds, exclusively put together for you.</li>
                <li> Rest assured- whatever your budget, we have you covered.</li>
                <li>Any questions or need more suggestions? Simply chat with StyleCracker’s celebrity stylists, available to you 24x7.</li>
                <li>We’re happy to take returns in case our products don’t excite you. (Just saying, you’re going to love them!)</li>
              </ul>
            <div class="btns-wrp">

              <!-- btn -->
              <?php if(!$this->session->userdata('user_id')){ ?>
                <a data-toggle="modal" href="#myModal" class="btn btn-primary">Book one now!</a>

                <?php }else{
        ?>
                  <div class="form-control-wrp">
                    <form id="sc_box" name="sc_box" method="post" action="<?php echo base_url(); ?>Schome/sc_box_savemobilenumber">
                      <div class="row">
                        <div class="col-md-5">
                          <div>
                            <input type="text" name="sc_boxmobile" id="sc_boxmobile" class="form-control" value="" min="10" maxlength="10" placeholder="Enter your phone number">
                          </div>
                        </div>
                        <div class="col-md-4">
                          <button type="button" disabled id="sc_boxsumbit" name="sc_boxsumbit" class="btn btn-primary sign-up-btn" onclick="book_scbox()">Book one now!</button>
                          <!-- <button type="button" id="sc_boxsumbit" name="sc_boxsumbit" class="btn btn-primary sign-up-btn">Book one now!</button> -->
                        </div>
                      </div>
                      <div class="error" id="sc_mobile_error"></div>
                      <div class="error" id="sc_result"> <?php echo @$result; ?></div>
                        <p id="stylistmeassage" style="margin-top:10px;">
                        …and your stylist will call you shortly!
                        </p>

                    </form>
                  </div>
                  <?php } ?>
                    <!-- //btn -->

            </div>
  </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="img-wrp">
            <img src="<?php echo base_url(); ?>assets/images/promo/scbox-4.jpg">
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
</div>


<script>
var sc_user_id = "<?php echo $this->session->userdata('user_id')!='' ? $this->session->userdata('user_id') : ''; ?>";
var url = parseInt(<?php if(isset($_SERVER['HTTP_REFERER'])) {$url= explode('/',$_SERVER['HTTP_REFERER']); if(in_array("scbox", $url) || in_array("pa", $url)){echo 1;}else{ echo 0; } }?>);
//alert(url);
  //window.onload = function() {
    //if(url == 1){ alert('comming'); }
    if(url == 1)document.getElementById("sc_boxmobile").focus();
    //document.getElementById("sc_boxmobile").focus();
    //alert(url);
    document.getElementById("sc_boxsumbit").disabled = false;
  //};

function book_scbox_focus(){
  document.getElementById("sc_boxmobile").focus();
}

  function book_scbox() {
    var sc_boxmobile = $('#sc_boxmobile').val();
    //console.log(sc_boxmobile);
    if (sc_boxmobile != '') {
      var str = sc_boxmobile;
      if ((str.length >= 10) && isNaN(sc_boxmobile)==(false)){
        $.ajax({
          type: "post",
          url: sc_baseurl + "schome/sc_box_savemobilenumber",
          data: {
            'sc_boxmobile': sc_boxmobile
          },
          success: function(response) {
            if (response == '1') {
              $('#sc_result').html('<p class="success" style="margin-top:10px;">Got it! Wait for your stylists call!</p>');
                document.getElementById("sc_boxsumbit").disabled = true;
                $("#stylistmeassage").hide();

            } else {
              $('#sc_mobile_error').html('<span class="error">Error</span>');
            }
          },
          error: function(response) {
            $('#sc_result').html('<span class="error">Error</span>');
          }
        });
      }
       else {
        $('#sc_result').html('Please enter valid mobile number');
      }
    } else {
     $('#sc_result').html('Please enter you mobile number');
    }
  }
</script>
