<?php
$user_name = $this->session->userdata('user_name');
$email = $this->session->userdata('email');
$user_id = $this->session->userdata('user_id');
$bucket = $this->session->userdata('bucket');
?>
<div class="sc-sidebar-overlay-sm"></div>
<div class="page-all-products content-wrp">
  <div class="container">
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a></li>
      <li><a href="<?php echo base_url(); ?>all-things-new">All Things New</a></li>
      <li class="active">Products</li>
    </ol>

  <div class="filterbar-wrp sm-only">
          <div class="container1">
              <div class="row">
                <div class="col-sm-4 col-xs-4 filter-btns-wrp-sm">
                <div class="btn btn-primary btn-sm btn-filter-by">
                    Show Filter
                  </div>
                </div>
              <div class="col-md-12 col-sm-8 col-xs-8">
                <div class="filterbar">
                  <span class="filter-label">Sort by: </span>
                    <select name="product_sort_by" id="product_sort_by_festive1" class="selectpicker product_sort_by">
                      <option value="0">Latest</option>
                      <option value="1">Price: High to Low</option>
                      <option value="2">Price: Low to High</option>
                    </select>
                </div>
              </div>
            </div>
          </div>
        </div>

    <div class="row layout-2-col">
      <div class="col-md-2 layout-2-col sidebar-filter-wrp sticky-sidebar" id="left-sliderbar">
        <?php echo $left_filter; ?>
      </div>
    </div>
  <div class="col-md-10 layout-2-col">
    <div class="title-section">
    <h1 class="title"><span><?php echo 'Products'; ?></span></h1>
    </div>

    <div class="filterbar-wrp searchbar-wrp filterbar-wrp-md md-only">
        <h2 class="filter-result-title hide" id="filter-result-title"><?php echo @$products_total; ?> All Products<?php echo @$products_total > 1 ? 's' : ''; ?></h2>
        
        
        <div class="filterbar">
          <span class="filter-label">Sort by: </span>
            <select name="product_sort_by" id="product_sort_by_festive" class="selectpicker">
              <option value="0">Latest</option>
              <option value="1">Price: High to Low</option>
              <option value="2">Price: Low to High</option>
            </select>
        </div>
    </div>
    <div class="filter-selction-box1" id="filter-selction-box-festive">


    </div>


      <?php if(!empty($all_data['products'])) {  ?>
    <div class="grid grid-products-col-4 items-wrp items-section">
      <div class="row grid-items-wrp" id="get_all_cat_products">
      <?php foreach ($all_data['products'] as $value) { ?>
      <div class="col-md-4 col-sm-4 col-xs-6 grid-item">
        <div class="item item-product">
          <div class="item-img item-hover">
            <div class="item-img-inner">
              <?php
                $short_url = base_url().'product/details/'.$value['slug'].'-'.$value['id'];
                $img_title_alt = str_replace('#','',$value['name']);
                $img_url = $this->config->item('product_image').$value['image'];
              ?>
                 <a href="<?php echo $short_url; ?>"  >
                  <?php if(file_exists($this->config->item('products_H340').$value['image'])) { ?>
                 <img class="lazy" data-original="<?php echo $this->config->item('products_340').$value['image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  />
                 <?php }else{ ?>
                  <img class="lazy" data-original="<?php echo $this->config->item('products_310').$value['image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  />

                  <?php } ?>
                 </a>
               </div>
              <div class="item-look-hover">

              <?php if($this->Allproducts_model->product_stock_check($value['id']) != 1){ ?>
                <a href="<?php echo $short_url; ?>" onclick="ga('send', 'event', 'Product', 'clicked', '<?php echo $value['name']; ?>'); _beatout_view_product('<?php echo $value['id']; ?>','<?php echo $user_id; ?>','<?php echo $value['price']; ?>','<?php echo $value['name']; ?>','','view');" title="<?php echo $img_title_alt; ?>" >
                <span class="btn-out-of-stock">Sold Out</span>
                </a>
                <?php }else{ ?>
                  <a href="<?php echo $short_url; ?>" onclick="ga('send', 'event', 'Product', 'clicked', '<?php echo $value['name']; ?>'); _beatout_view_product('<?php echo $value['id']; ?>','<?php echo $user_id; ?>','<?php echo $value['price']; ?>','<?php echo $value['name']; ?>','','view');" title="<?php echo $img_title_alt; ?>" >
                  <span class="btn btn-primary"><?php echo $this->lang->line('get_this'); ?></span>
                  </a>
              <?php } ?>
              </div>

            </div>
            <div class="item-desc">
            <?php $lookPrdName = stripslashes($value['name']); if (strlen($lookPrdName) >= 84){ $stringCut = substr($lookPrdName, 0, 81); } ?>
              <div class="item-title"><a title="<?php echo $img_title_alt; ?>" href="<?php echo $short_url; ?>"  ><?php echo ucwords(strtolower($lookPrdName)); ?></a>
              </div>
              <div class="item-brand">By <span><?php echo $this->Allproducts_model->get_brand_name($value['brand_id']); ?></span></div>
              <div class="item-price-wrp">
                <!--<div class="item-price">
                  <?php if($this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price']>0) { ?>
                  <span class="mrp-price text-strike"><i class="fa fa-inr"></i><?php echo $value['price']; ?></span>
                  <span class="price"><i class="fa fa-inr"></i><?php echo $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price'] : ''; ?></span>
                  <span class="discount-price">(<?php echo round($this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discount_percent']); ?>% OFF)</span>
                  <?php }else{ ?>
                  <span class="price"><i class="fa fa-inr"></i> <?php echo $value['price']; ?></span>
                  <?php } ?>
                </div>-->
				<div class="item-price">
                  <?php if($this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price']>0 || $value['compare_price']>$value['price']) { ?>
                  <span class="mrp-price text-strike"><i class="fa fa-inr"></i><?php if($value['compare_price']>$value['price']){ echo $value['compare_price']; }else { echo $value['price']; $value['compare_price'] = $value['price'];} ?></span>
                  <!--<span class="price"><i class="fa fa-inr"></i><?php echo $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price'] : ''; ?></span>-->
				  <span class="price"><i class="fa fa-inr"></i> <?php $discount_price = $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price'] : ''; if($discount_price > 0){ echo $discount_price; }else{
					  echo $value['price'];
				  } ?></span>
                  <span class="discount-price">(<?php $discount_percent = $this->Product_desc_model->calculate_discount_percent($value['compare_price'],$discount_price,$value['price']); echo $discount_percent; ?>% OFF)</span>
                  <?php }else{ ?>
                  <span class="price"><i class="fa fa-inr"></i> <?php echo $value['price']; ?></span>
                  <?php } ?>
                </div>
                <?php
                      if($this->session->userdata('user_id')){
                        if($this->home_model->get_fav_or_not('product',$value['id']) == 0){ ?>
                          <div class="user-likes"><a title="Add to Wish List" id="wishlist<?php echo $value['id']; ?>" onclick="add_to_fav('product','<?php echo $value['id']; ?>','<?php echo $user_id; ?>','<?php echo $value['price']; ?>','<?php echo $value['name']; ?>','<?php echo @$value['product_cat_id']; ?>','add_to_wishlist'); ga('send', 'event', 'Product', 'wishlist', '<?php echo $value['name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love"></i></a></div>
                        <?php }else{ ?>
                          <div class="user-likes"><a title="Remove from Wish List" id="wishlist<?php echo $value['id']; ?>" onclick="add_to_fav('product','<?php echo $value['id']; ?>','<?php echo $user_id; ?>','<?php echo $value['price']; ?>','<?php echo $value['name']; ?>','<?php echo @$value['product_cat_id']; ?>','remove_from_wishlist'); ga('send', 'event', 'Product', 'wishlist', '<?php echo $value['name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love active"></i></a></div>
                        <?php } }else{ ?>
                        <div class="user-likes"><a onclick="ga('send', 'event', 'Sign Up', 'clicked', 'favorite');" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a></div>
                      <?php }  ?>
              </div>
            </div>
          </div>
        </div>
       <?php }?>

      </div>
    </div>
    <?php }else{ ?>
        <div class="grid-no-products"> No Products Found </div>

    <?php  } ?>

       <input type="hidden" name="filter_brand_id" id="filter_brand_id" value="<?php echo @$all_data['filter_brand_id']; ?>">
       <input type="hidden" name="filter_color_id" id="filter_color_id" value="">
       <input type="hidden" name="last_cat_id" id="last_cat_id" value="">
       <input type="hidden" name="priceId" id="priceId" value="">
       <input type="hidden" name="isButton" id="isButton" value="">
       <input type="hidden" name="attributesId" id="attributesId" value="">
       <input type="hidden" name="category_id" id="category_id" value="<?php echo @$all_data['cat_slug']; ?>">
       <input type="hidden" name="sizeId" id="sizeId" value="">
       <input type="hidden" name="search_pro" id="search_pro" value="<?php echo @$_GET['search']; ?>">
       <input type="hidden" name="is_recommended" id="is_recommended" value="<?php echo @$_GET['is_recommended']; ?>">
       <input type="hidden" name="load_more_cnt" id="load_more_cnt" value="1">
       <input type="hidden" name="cat_id" id="cat_id" value="<?php echo @$cat_id; ?>">
       

<div class="fa-loader-wrp-outer">
    <div style="display: none;" class="fa-loader-wrp-products">
      <i class="fa fa-circle-o-notch fa-spin-custom"></i> Loading...
    </div>
 </div>

  </div>
</div>

<?php echo @$pagination; ?>
</div>
</div>
