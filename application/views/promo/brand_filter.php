<?php $segment_second_position = @$this->uri->segment(2); ?>
<?php
$color=unserialize(FILTER_COLOR);  $min_price ='0'; $max_price ='0';
$min_price = (int)@$all_data['filter']['price'][0]['min_price'];
$max_price = (int)@$all_data['filter']['price'][0]['max_price'];
?>
<div class="sidebar-filter is-sticky">
  <div class="sidebar-filter-title">
    Filter By <?php $url =  $_SERVER['REQUEST_URI'].'/'; ?>
  </div>
  <div class="sidebar-filter-inner" id="sc-filter-box">
  	 <div class="filter-box">

        <?php if(!empty($all_data['filter']['paid_brands'])){  ?>
          <div class="box">
        <div class="title clearfix scfiltertitle" data-toggle="collapse" data-target="#collapse-brands" aria-expanded="false" aria-controls="collapse-brands">
          <h6>Brands</h6>
          <span class="fa fa-minus"></span>
        </div>
        <div id="collapse-brands" class="collapse in collapse-brands-inner custom-scrollbar " aria-expanded="true">

          <ul>
          <?php foreach($all_data['filter']['paid_brands'] as $val){ ?>
          	<li class="sccheckbox">
            <?php if($val['user_id'] == $all_data['filter_brand_id']){ ?>
            <input type="checkbox" name="brand" id="brand<?php echo $val['user_id']; ?>" class="filter-link" value="<?php echo $val['user_id']; ?>" checked data-attr="<?php echo $url; ?>" title="<?php echo $val['company_name']; ?>">
            <?php }else{ ?>
            <input type="checkbox" name="brand" id="brand<?php echo $val['user_id']; ?>" class="filter-link" value="<?php echo $val['user_id']; ?>" data-attr="<?php echo $url; ?>" title="<?php echo $val['company_name']; ?>">
            <?php } ?>
             <label for="brand<?php echo $val['user_id']; ?>"> <?php echo $val['company_name']; ?></label>
            </li>
          <?php } ?>
          </ul>

        </div>
          </div>
        <?php } ?>


  </div>
</div>
