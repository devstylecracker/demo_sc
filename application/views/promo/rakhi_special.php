<div class="page-brand-details">
  <div class="container custom">
    <div class="brands-banner-wrp">
      <!--<img src="<?php echo base_url(); ?>assets/images/promo/test1.jpg">-->
        <img src="<?php echo base_url(); ?>assets/images/rakhi_special/rakhi-banner.jpg">
		<div class="desc-section" style=" bottom: 10%; 
    left: 0; position: absolute; right: 0; text-align:center;">
     <!-- <div style="background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0; color: #fff; font-size: 18px;font-weight: bold;
    padding: 20px;display:inline-block;">
	  <p>
			Vintage inspired men's accessories for the modern guy. In with the old, out with the new.<br/>
			Shop one-of-a-kind accessories for your brother this Rakhi. Turn the tables around.			
        </p>
		<a href="https://www.stylecracker.com/brands/brand-details/thegentlemanscommunity" class="btn btn-primary">Shop Now</a>
		
      </div> -->
	  </div>
    </div>
  </div>

  <div class="container">
  <!--  <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a></li>
      <li class="active">Rakhi Special</li>
    </ol>-->
<div class="breadcrumb"></div>
    <div class="content-wrp">
     <section class="desc-section" style="text-align: center; font-size: 20px; font-style: italic;">
        <p>
			Vintage inspired men's accessories for the modern guy. In with the old, out with the new.<br>
			Shop one-of-a-kind accessories for your brother this Rakhi. Turn the tables around.
        </p>
      </section>
      <div class="title-section">
        <h1 class="title"><span>Explore Categories</span></h1>
      </div>

      <section class="grid items-wrp items-section">
        <div class="row grid-items-wrp">
          <div class="col-md-3 col-sm-4 grid-item">
            <div class="item item-cat">
              <div class="item-img item-hover">
                <div class="item-img-inner">
                  <a href="<?php echo base_url(); ?>Brand_category/brand_product/thegentlemanscommunity/bowties-286">
                    <img src="<?php echo base_url(); ?>assets/images/rakhi_special/bow-ties.jpg" alt="" title="">

                  </a>
                </div>
                <div class="item-look-hover">
                  <a href="<?php echo base_url(); ?>Brand_category/brand_product/thegentlemanscommunity/bowties-286"><span class="btn btn-primary"> Get This </span></a>
                </div>
              </div>
              <div class="item-desc">
              <div class="item-title"><a href="javascript:void(0);" title="Bow Ties">Bow Ties</a></div>
			</div>
        </div>
      </div>
      <div class="col-md-3 col-sm-4 grid-item">
        <div class="item item-cat">
          <div class="item-img item-hover">
            <div class="item-img-inner">
              <a href="<?php echo base_url(); ?>Brand_category/brand_product/thegentlemanscommunity/bracelets-3314">
                <img src="<?php echo base_url(); ?>assets/images/rakhi_special/braclets.jpg" alt="" title="">

              </a>
            </div>
            <div class="item-look-hover">
              <a href="<?php echo base_url(); ?>Brand_category/brand_product/thegentlemanscommunity/bracelets-3314"><span class="btn btn-primary"> Get This </span></a>
            </div>
          </div>
          <div class="item-desc">
              <div class="item-title"><a href="javascript:void(0);" title="Bracelets">Bracelets</a></div>
			</div>
    </div>
  </div>
  <div class="col-md-3 col-sm-4 grid-item">
    <div class="item item-cat">
      <div class="item-img item-hover">
        <div class="item-img-inner">
          <a href="<?php echo base_url(); ?>Brand_category/brand_product/thegentlemanscommunity/cuff-links-3318">
            <img src="<?php echo base_url(); ?>assets/images/rakhi_special/cufflnks.jpg" alt="" title="">

          </a>
        </div>
        <div class="item-look-hover">
          <a href="<?php echo base_url(); ?>Brand_category/brand_product/thegentlemanscommunity/cuff-links-3318"><span class="btn btn-primary"> Get This </span></a>
        </div>
      </div>
      <div class="item-desc">
            <div class="item-title"><a href="javascript:void(0);" title="Cuff Links">Cuff Links</a></div>
		</div>
</div>
</div>
<div class="col-md-3 col-sm-4 grid-item">
  <div class="item item-cat">
    <div class="item-img item-hover">
      <div class="item-img-inner">
        <a href="<?php echo base_url(); ?>Brand_category/brand_product/thegentlemanscommunity/grooming-345">
          <img src="<?php echo base_url(); ?>assets/images/rakhi_special/grooming.jpg" alt="" title="">

        </a>
      </div>
      <div class="item-look-hover">
        <a href="<?php echo base_url(); ?>Brand_category/brand_product/thegentlemanscommunity/grooming-345"><span class="btn btn-primary"> Get This </span></a>
      </div>
    </div>
     <div class="item-desc">
            <div class="item-title"><a href="javascript:void(0);" title="Grooming">Grooming</a></div>
		</div>
</div>
</div>
<div class="col-md-3 col-sm-4 grid-item">
            <div class="item item-cat">
              <div class="item-img item-hover">
                <div class="item-img-inner">
                  <a href="<?php echo base_url(); ?>Brand_category/brand_product/thegentlemanscommunity/frames">
                    <img src="<?php echo base_url(); ?>assets/images/rakhi_special/optical_frames.jpg" alt="" title="">

                  </a>
                </div>
                <div class="item-look-hover">
                  <a href="<?php echo base_url(); ?>Brand_category/brand_product/thegentlemanscommunity/frames"><span class="btn btn-primary"> Get This</span></a>
                </div>
              </div>
              <div class="item-desc">
            <div class="item-title"><a href="javascript:void(0);" title="Spectacles">Spectacles</a></div>
		</div>
        </div>
      </div>
	  <div class="col-md-3 col-sm-4 grid-item">
            <div class="item item-cat">
              <div class="item-img item-hover">
                <div class="item-img-inner">
                  <a href="<?php echo base_url(); ?>Brand_category/brand_product/thegentlemanscommunity/pocket-squares-289">
                    <img src="<?php echo base_url(); ?>assets/images/rakhi_special/pocket.jpg" alt="" title="">

                  </a>
                </div>
                <div class="item-look-hover">
                  <a href="<?php echo base_url(); ?>Brand_category/brand_product/thegentlemanscommunity/pocket-squares-289"><span class="btn btn-primary"> Get This </span></a>
                </div>
              </div>
              <div class="item-desc">
            <div class="item-title"><a href="javascript:void(0);" title="Pocket Squares">Pocket Squares</a></div>
			</div>
        </div>
      </div>
	  <div class="col-md-3 col-sm-4 grid-item">
            <div class="item item-cat">
              <div class="item-img item-hover">
                <div class="item-img-inner">
                  <a href="<?php echo base_url(); ?>Brand_category/brand_product/thegentlemanscommunity/sunglasses-3326">
                    <img src="<?php echo base_url(); ?>assets/images/rakhi_special/sunglasses.jpg" alt="" title="">

                  </a>
                </div>
                <div class="item-look-hover">
                  <a href="<?php echo base_url(); ?>Brand_category/brand_product/thegentlemanscommunity/sunglasses-3326"><span class="btn btn-primary"> Get This </span></a>
                </div>
              </div>
              <div class="item-desc">
				<div class="item-title"><a href="javascript:void(0);" title="Sunglasses">Sunglasses</a></div>
			</div>
        </div>
      </div>
	  <div class="col-md-3 col-sm-4 grid-item">
            <div class="item item-cat">
              <div class="item-img item-hover">
                <div class="item-img-inner">
                  <a href="<?php echo base_url(); ?>Brand_category/brand_product/thegentlemanscommunity/ties-18">
                    <img src="<?php echo base_url(); ?>assets/images/rakhi_special/tie.jpg" alt="" title="">

                  </a>
                </div>
                <div class="item-look-hover">
                  <a href="<?php echo base_url(); ?>Brand_category/brand_product/thegentlemanscommunity/ties-18"><span class="btn btn-primary"> Get This </span></a>
                </div>
              </div>
              <div class="item-desc">
				<div class="item-title"><a href="javascript:void(0);" title="Ties">Ties</a></div>
			</div>
        </div>
      </div>
    </div>
  </section>
</div>
</div>
</div>

<style>
.item.item-cat .item-img {
  height: 276px;
}
</style>
