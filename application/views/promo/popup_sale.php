<div class="page-brand-details">
  <div class="container custom">
    <div class="brands-banner-wrp">
        <!--<img src="https://www.stylecracker.com/sc_admin/assets/banners/1481203930.gif">-->
        <img src="<?php echo base_url(); ?>assets/images/promo/all-things-black.jpg">		
    </div>
  </div>

  <div class="container">
  <!--  <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a></li>
      <li class="active">Rakhi Special</li>
    </ol>-->
<div class="breadcrumb"></div>
    <div class="content-wrp">
     <section class="desc-section" style="text-align: center; font-size: 20px; font-style: italic;">
     <!-- <p>Exclusive Designers | 20 Days <br>			
        </p>-->
      </section>
      <div class="title-section">
        <h1 class="title"><span>All Things Black</span></h1>
      </div>

      <ul class="nav nav-tabs common-tabs">
        <li><a data-toggle="tab" href="#brands">Brands</a></li>
        <li class="active"><a data-toggle="tab" href="#category">Categories</a></li>
      </ul>

       <div class="tab-content common-tab-content">
        <div id="brands" class="tab-pane fade">

      <!--div class="title-section">
        <h1 class="title"><span>Brands</span></h1>
      </div-->

      <section class="grid items-wrp items-section">
        <div class="row grid-items-wrp">


   

        <?php if(!empty($brands)){ 
          foreach($brands as $val){
        ?>
          <div class="col-md-3 col-sm-4 grid-item">
            <div class="item item-cat">
              <div class="item-img item-hover">
                <div class="item-img-inner">                  
                  <a href="<?php echo base_url(); ?>brand/<?php echo $val['user_name'].'?color=black'; ?>">
                   <?php if($val['home_page_promotion_img']!=''){ ?>
                    <img src="<?php echo $this->config->item('brand_home_page_url'); ?>/<?php echo $val['home_page_promotion_img']; ?>" alt="" title="">
                  <?php }else{ ?>
                    <img title="<?php echo $val['home_page_promotion_img']; ?>- StyleCracker" alt="<?php echo $val['home_page_promotion_img']; ?> - StyleCracker" src="<?php echo base_url(); ?>assets/images/image-na.jpg">
                  <?php } ?>                 
                  </a>
                </div>
                <div class="item-look-hover">                    
                      <a href="<?php echo base_url(); ?>brand/<?php echo $val['user_name'].'?color=black'; ?>">                    
                  <span class="btn btn-primary"> Get This </span></a>
                </div>
              </div>
              <div class="item-desc">
              <div class="item-title"><a href="javascript:void(0);" title="<?php echo $val['company_name']; ?>"><?php echo $val['company_name']; ?></a></div>
			</div>
        </div>
      </div>
      <?php } } ?>
    </div>
  </section>
</div>
        <div id="category" class="tab-pane fade in active">
     <!--div class="title-section">
        <h1 class="title"><span>Category</span></h1>
      </div-->

      <section class="grid items-wrp items-section">
      
     <!--  <div class="title-section">
        <h1 class="title"><span>Women</span></h1>
      </div>
-->
        <div class="row grid-items-wrp">  
        <?php              

        if(!empty($categories)){ 
        // echo '<pre>';print_r($categories);exit;
           /*$women_cat = array('47','49','52','48','320','55','7','68','14');  */  
           $women_cat = array('1');                 
          foreach($categories as $val){
            $category_img_url = $this->home_model->cat_img_exists($val['id']);

            if(in_array($val['id'], $women_cat))
            {           
            
        ?>
          <div class="col-md-3 col-sm-4 grid-item">
            <div class="item item-cat">
              <div class="item-img item-hover">
                <div class="item-img-inner">
                  <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/products/<?php echo $val['category_slug']; ?>-<?php echo $val['id']; ?>">
                   <?php if(file_exists($category_img_url)){ ?>
                    <img src="<?php echo base_url().$category_img_url; ?>" alt="" title="">
                  <?php }else{ ?>
                    <img title="<?php echo $val['category_name']; ?>- StyleCracker" alt="<?php echo $val['category_name']; ?> - StyleCracker" src="<?php echo base_url(); ?>assets/images/image-na.jpg">
                  <?php } ?>
                  </a>
                </div>
                <div class="item-look-hover">
                  <!--<a href="<?php //echo base_url(); ?><?php //echo $this->uri->segment(1); ?>/products/<?php //echo $val['category_slug']; ?>-<?php //echo $val['id']; ?>"><span class="btn btn-primary"> Get This </span></a>-->
                  <a href="<?php echo base_url().'all-black-products/women?color=black'; ?>"><span class="btn btn-primary"> Get This </span></a>
                </div>
              </div>
              <div class="item-desc">
              <div class="item-title"><a href="javascript:void(0);" title="<?php echo $val['category_name']; ?>"><?php echo $val['category_name']; ?></a></div>
      </div>
        </div>
      </div>
      <?php 
        }
      } 
      } ?>
	  <!--
</div>
       <div class="row grid-items-wrp">  
       <div class="title-section">
        <h1 class="title"><span>Men</span></h1>
      </div>-->
      
        <?php              

        if(!empty($categories)){ 
        // echo '<pre>';print_r($categories);exit;           
          /*$men_cat = array('65','20','9');*/  
          $men_cat = array('3');           
          foreach($categories as $val){
            $category_img_url = $this->home_model->cat_img_exists($val['id']);

            if(in_array($val['id'], $men_cat))
            {           
            
        ?>
          <div class="col-md-3 col-sm-4 grid-item">
            <div class="item item-cat">
              <div class="item-img item-hover">
                <div class="item-img-inner">
                  <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/products/<?php echo $val['category_slug']; ?>-<?php echo $val['id']; ?>">
                   <?php if(file_exists($category_img_url)){ ?>
                    <img src="<?php echo base_url().$category_img_url; ?>" alt="" title="">
                  <?php }else{ ?>
                    <img title="<?php echo $val['category_name']; ?>- StyleCracker" alt="<?php echo $val['category_name']; ?> - StyleCracker" src="<?php echo base_url(); ?>assets/images/image-na.jpg">
                  <?php } ?>
                  </a>
                </div>
                <div class="item-look-hover">
                  <!--<a href="<?php //echo base_url(); ?><?php //echo $this->uri->segment(1); ?>/products/<?php //echo $val['category_slug']; ?>-<?php //echo $val['id']; ?>"><span class="btn btn-primary"> Get This </span></a>-->
                   <a href="<?php echo base_url().'all-black-products/men?color=black'; ?>"><span class="btn btn-primary"> Get This </span></a>
                </div>
              </div>
              <div class="item-desc">
              <div class="item-title"><a href="javascript:void(0);" title="<?php echo $val['category_name']; ?>"><?php echo $val['category_name']; ?></a></div>
      </div>
        </div>
      </div>
      <?php 
        }
      } 



      } ?>
    </div>
  </section>
</div>
</div>

</div>
</div>
</div>

<style>
.item.item-cat .item-img {
  height: 276px;
}
</style>
