<div class="content-wrp page-stylist page-common">
  <div class="container">
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a></li>
      <li><a href="<?php echo base_url(); ?>about-us">About</a></li>
      <li class="active"><?php $sty_name = explode(" ",$stylist_data[0]['stylist_name']); echo $sty_name[0]; echo '<span>'.$sty_name[1].'</span>' ?></li>
    </ol>

    <div class="stylist-top-section">
      <div class="row">
        <div class="col-md-4">
          <img src="<?php echo base_url(); ?>assets/images/about/<?php echo $stylist_data[0]['full_img']; ?>" />
        </div>
        <div class="col-md-8">
          <div class="stylist-page-header">
            <div class="row">
              <div class="col-md-8">
            <h1><?php $sty_name = explode(" ",$stylist_data[0]['stylist_name']); echo $sty_name[0]; echo '<span>'.$sty_name[1].'</span>' ?></h1>
          </div>
          <div class="col-md-4">
              <div class="stylist-social-media">
              <ul class="social-buttons">
                <?php if($stylist_data[0]['facebook_link']){ ?>
                  <li><a href="<?php echo $stylist_data[0]['facebook_link']; ?>"><i class="fa fa-facebook"></i></a></li>
                <?php } ?>
                <?php if($stylist_data[0]['twitter_link']){ ?>
                <li><a href="<?php echo $stylist_data[0]['twitter_link']; ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <?php } ?>
                <?php if($stylist_data[0]['insta_link']){ ?>
                <li><a href="<?php echo $stylist_data[0]['insta_link']; ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                <?php } ?>
              </ul>
          </div>
            </div>
              </div>
              </div>
          <div class="personal-desc">
              <h6>Projects undertaken at stylecracker</h6>
          </div>
          <div class="desc">
            <p><?php echo $stylist_data[0]['stylist_bio']; ?>
             </p>
            </div>
            <div class="personal-desc">
            <?php if($stylist_data[0]['style_tip']){ ?>
              <h6>Style Tip</h6>
              <p><?php echo $stylist_data[0]['style_tip']; ?></p>
            <?php } ?>
            <?php if($stylist_data[0]['style_icon']){ ?>
              <h6>Style Icon</h6>
             <p><?php echo $stylist_data[0]['style_icon']; ?></p>
            <?php } ?>
            <?php if($stylist_data[0]['why_sc']){ ?>
              <h6>Why StyleCracker?</h6>
             <p><?php echo $stylist_data[0]['why_sc']; ?></p>
            <?php } ?>
            </div>
          </div>
        </div>
      </div>

<?php if(!empty($stylist_looks)) { ?>
<div>
  <h3 class="section-header">Looks Recommended By <?php echo $stylist_data[0]['stylist_name']; ?>:</h3>
</div>

    <div class="grid grid-looks-col-3 items-wrp">
      <div class="row grid-items-wrp" id="get_all_stylist_looks">

         <?php foreach ($stylist_looks as $value) { ?>
            <div class="col-md-4 col-sm-6 grid-item">
              <div class="item">
                <div class="item-img item-hover">
                    <?php
                      $short_url = base_url().'looks/look-details/'.$value['slug'];
                      $img_title_alt = str_replace('#','',$value['name']);

                      if($value['look_type'] == 1){ ?>
                        <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>"><img src="<?php echo $this->config->item('sc_look_image_url').$value['image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                      <?php $img_url = $this->config->item('sc_look_image_url').$value['image'];
                        }else if($value['look_type'] == 2 || $value['look_type'] == 4 || $value['look_type'] == 6){ ?>
                        <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>"><img src="<?php echo $this->config->item('sc_street_style_image_url').$value['look_image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                      <?php $img_url = $this->config->item('sc_street_style_image_url').$value['look_image'];
                      }else if($value['look_type'] == 3){ ?>
                      <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>"><img src="<?php echo $this->config->item('sc_promotional_look_image').$value['product_img']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                      <?php $img_url = $this->config->item('sc_promotional_look_image').$val['product_img'];
                      }
                      ?>

                  <div class="item-look-hover">
                    <a href="<?php echo $short_url; ?>" title="<?php echo $img_title_alt; ?>" target="_blank"><span class="btn btn-primary"><?php echo $this->lang->line('get_this'); ?></span></a>
                  </div>
                  </div>
                  <div class="item-desc">
                    <div class="item-title"><a title="<?php echo $img_title_alt; ?>" href="<?php echo $short_url; ?>" target="_blank"><?php echo $value['name']; ?></a>
                    </div>
                    <div class="item-media">
                      <ul class="social-buttons">
                        <li><a onclick="_targetFacebookShare('<?php echo $value['name']; ?>','<?php echo $short_url; ?>','<?php echo $img_url; ?>','<?php echo $value['id']; ?>','<?php echo 'Buy Online Now on StyleCracker'; ?>');"><i class="fa fa-facebook"></i></a></li>
                        <li><a onclick="_targetTweet('<?php echo $short_url; ?>','<?php echo $value['name']; ?>','<?php echo $value['id']; ?>');"><i class="fa fa-twitter"></i></a></li>
                        <li><a onclick="_targetPinterest('<?php echo $short_url; ?>','<?php echo $value['name']; ?>','<?php echo $value['id']; ?>','<?php echo $img_url; ?>');"><i class="fa fa-pinterest"></i></a></li>
                      </ul>

                      <?php
                      if($this->session->userdata('user_id')){
                        if($this->home_model->get_look_fav_or_not($value['id']) == 0){ ?>
                          <div class="user-likes"><a target="_blank" title="Add to Wish List" id="whislist<?php echo $value['id']; ?>" onclick="add_to_wishlist(<?php echo $value['id']; ?>);"><i class="ic sc-love"></i></a></div>
                        <?php }else{ ?>
                          <div class="user-likes"><a target="_blank" title="Remove from Wish List" id="whislist<?php echo $value['id']; ?>" onclick="remove_from_wishlist(<?php echo $value['id']; ?>);"><i class="ic sc-love active"></i></a></div>
                        <?php } }else{ ?>
                        <div class="user-likes"><a target="_blank" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a></div>
                      <?php }
                      ?>

                      </div>
                    </div>
                  </div>
                </div>

       <?php }  ?>


      </div>
    </div>

<?php } ?>
    </div>
  </div>

<input type="hidden" name="stylist-id" id="stylist-id" value="<?php echo $stylist_data[0]['id']; ?>">
<input type="hidden" name="offset" id="offset" value="1">
