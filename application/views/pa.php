<script type="text/javascript">var baseurl = '<?php echo base_url(); ?>';</script>
<script src="<?php echo base_url(); ?>/assets/js/pa.js"></script>
<?php
$next_tab = 0;
  if($question_resume_id && $edited_data==''){
    if($question_resume_id ==1){
      $next_tab = 1;
    }else if($question_resume_id ==2){
      $next_tab = 2;
    }else if($question_resume_id ==8){
      $next_tab = 3;
    }else if($question_resume_id ==7){
      $next_tab = 4;
    }
  }else if($question_resume_id && $edited_data=='edit'){
    if($question_resume_id ==1){
      $next_tab = 0;
    }else if($question_resume_id ==2){
      $next_tab = 1;
    }else if($question_resume_id ==8){
      $next_tab = 2;
    }else if($question_resume_id ==7){
      $next_tab = 3;
    }else if($question_resume_id ==9){
      $next_tab = 4;
    }

  }

?>
  <div class="page-pa content-wrp">
    <div class="container">
      <div id="pa-slider" class="pa-slider">

      <div class="pa-selection-wrp pa-nav">
        <ul>
          <li class="control-nav disabled" data-link="slide1">
            <span class="caret"></span>
            Body Type
          </li>
          <li class="control-nav disabled" data-link="slide2">
            <span class="caret"></span>
            Personal Style
          </li>
          <li class="control-nav disabled" data-link="slide3">
            <span class="caret"></span>
            Age
          </li>
          <li class="control-nav disabled" data-link="slide4">
            <span class="caret"></span>
            Budget
          </li>
          <li class="control-nav disabled" data-link="slide5">
            <span class="caret"></span>
            Size
          </li>
        </ul>

      </div>
<?php if(!empty($body_shape)){ ?>

      <div id="slide1" class="slide body-types-wrp active">
        <div class="pa-title">
          <h2>Pick the option that best describes your <span>body shape</span>
          </h2>
          </div>
          <div class="row">
          <?php  foreach($body_shape as $val){ ?>
            <div class="col-md-2 col-sm-6 col-xs-6 <?php if(isset($user_body_shape) && @$user_body_shape[0]['answer_id']==$val['id']){ echo 'selected'; } ?>" data-attr="<?php echo $val['id']; ?>" data-attr2="1" data-attr3="<?php echo $val['answer']; ?>">
              <div class="item-wrp item-hover">
              <div class="item item-body-type">
                <img src="<?php echo base_url(); ?>assets/images/pa/<?php echo $val['image_name']; ?>">
                <div class="title">
                  <?php echo $val['answer']; ?>
                </div>
              </div>
              <div class="desc">
                <p>
                <?php echo $val['description']; ?>
                </p>
              </div>
            </div>
            </div>
            <?php } ?>

          </div>
        </div>

<?php } ?>

<?php if(!empty($style)){ ?>
             <div  id="slide2" class="slide body-styles-wrp">
          <div class="pa-title">
            <h2>Choose the <span>style</span> you best relate to</h2>
            </div>

            <div class="row">
            <?php foreach($style as $val){ ?>
              <div class="col-md-2 col-sm-6 col-xs-6 <?php if(isset($user_style) && @$user_style[0]['answer_id']==$val['id']){ echo 'selected'; } ?>" data-attr="<?php echo $val['id']; ?>" data-attr2="2" data-attr3="<?php echo $val['answer']; ?>">
              <div class="item-wrp item-hover">
                <div class="item item-body-style">
                  <img src="<?php echo base_url(); ?>assets/images/pa/<?php echo $val['image_name']; ?>">
                  <div class="title">
                    <?php echo $val['answer']; ?>
                  </div>
                </div>
                <div class="desc">
                  <p>
                   <?php echo $val['description']; ?>
                  </p>
                </div>
              </div>
              </div>
             <?php } ?>

            </div>
          </div>
<?php } ?>

<?php if(!empty($age)){ ?>
          <div id="slide3" class="slide body-age-wrp">
            <div class="pa-title">
            <h2>Select your <span>age group</span></h2>
              </div>

              <div class="row">
              <?php foreach($age as $val) { ?>
                <div class="col-md-2 col-sm-6 col-xs-6 <?php if(isset($user_age) && @$user_age[0]['answer_id']==$val['id']){ echo 'selected'; } ?>" data-attr="<?php echo $val['id']; ?>" data-attr2="3" data-attr3="<?php echo $val['answer']; ?>">
                <div class="item-wrp item-hover">
                  <div class="item item-age-group">
                    <img src="<?php echo base_url(); ?>assets/images/pa/<?php echo $val['image_name']; ?>">
                    <div class="title">
                      <?php echo $val['answer']; ?> Years
                    </div>

                  </div>
                </div>
              </div>
              <?php } ?>
              </div>
            </div>
<?php } ?>

<?php if(!empty($budget)){ ?>
             <div id="slide4" class="slide body-budget-wrp">
              <div class="pa-title">
                <h2>What is your average shopping <span>Budget</span>?</h2>
                </div>

                <div class="row">
                <?php foreach($budget as $val) { ?>
                  <div class="col-md-2 col-sm-6 col-xs-6 <?php if(isset($user_budget) && @$user_budget[0]['answer_id']==$val['id']){ echo 'selected'; } ?>" data-attr="<?php echo $val['id']; ?>" data-attr2="4" data-attr3="<?php echo $val['answer']; ?>">
                  <div class="item-wrp item-hover">
                    <div class="item item-budget">
                     <img src="<?php echo base_url(); ?>assets/images/pa/<?php echo $val['image_name']; ?>">
                     <div class="title">
                       <?php echo $val['answer']; ?>
                     </div>
                    </div>

                  </div>
                  </div>
                <?php } ?>
                </div>
              </div>

<?php } ?>

<?php if(!empty($size)){ ?>
               <div id="slide5" class="slide body-size-wrp">
                <div class="pa-title">
                  <h2>Choose your <span>size</span> </h2>                
                  </div>

                  <div class="row">
                  <?php foreach($size as $val) { ?>
                    <div class="col-md-2 col-sm-6 col-xs-6 <?php if(isset($user_size) && @$user_size[0]['answer_id']==$val['id']){ echo 'selected'; } ?>" data-attr="<?php echo $val['id']; ?>" data-attr2="5" data-attr3="<?php echo $val['answer']; ?>">
                      <div class="item-wrp item-hover">
                      <div class="item item-size">
                        <img src="<?php echo base_url(); ?>assets/images/pa/<?php echo $val['image_name']; ?>">
                        <div class="title">
                          <?php echo $val['answer']; ?>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php } ?>

                  </div>
                </div>
<?php } ?>
  </div>
  </div>
  </div>
<?php if($this->session->userdata('campaign') == 'yes'){ ?>
<!-- Google Code for Promo Conversions Conversion Page -->
<script type="text/javascript">

/* <![CDATA[ */

var google_conversion_id = 944683658;

var google_conversion_language = "en";

var google_conversion_format = "3";

var google_conversion_color = "ffffff";

var google_conversion_label = "g2_tCJG6mmAQivW6wgM";
var google_remarketing_only = false;

/* ]]> */

</script>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">

</script>

<noscript>

<div style="display:inline;">

<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/944683658/?label=g2_tCJG6mmAQivW6wgM&amp;guid=ON&amp;script=0"/>

</div>

</noscript>




<?php } ?>

<script type="text/javascript">
$( document ).ready(function() {
  ga('send', 'event', 'Sign Up', 'completed');
  <?php if(isset($_GET['ga'])){echo "ga('send', 'event', 'Login', 'source', 'gplus', '".$this->session->userdata('user_id')."');";} ?>
        //## UserTrack: Page ID=========================================
        document.cookie="pid=7";
        /*Code ends*/
  <?php if(empty($user_body_shape)){ ?>
    PAInitialize1(0,'true','','');
    //console.log("ONE");
  <?php }else{ if($edited_data == 'edit'){ ?>
    localStorage.setItem("edit_pa", "yes");
    PAInitialize1('<?php echo $next_tab; ?>','false','');
    <?php }else{ ?>//console.log('no edit');
    ga('send', 'event', 'PA-Women', 'start');
    PAInitialize1('<?php echo $next_tab; ?>','false','');
    <?php } ?>
    //console.log("TWO");
  <?php } ?>
});
</script>
