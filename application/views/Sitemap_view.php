<?php

/*
$datetime->setTimezone(new DateTimeZone('Asia/Calcutta'));
print $datetime->format('Y-m-dTH:i:s (e)');
  // 2015-06-22 15:10:25 (Asia/Calcutta)

  $datetime = new DateTime('2015-06-22T10:40:25', new DateTimeZone('Asia/Calcutta'));
  */
$date = new DateTime("now", new DateTimeZone('Asia/Calcutta') );
$date_time_stamp = $date->format('Y-m-d\TH:i:sP');


    $xml = new DOMDocument('1.0','UTF-8');
    $root = $xml->createElementNS('http://www.sitemaps.org/schemas/sitemap/0.9', "urlset");
    $root = $xml->appendChild($root);
    $xml->appendChild($root);
        /* row*/
           $id = $xml->createElement("loc");
                $idText = $xml->createTextNode(base_url());
                $id->appendChild($idText);

            $change = $xml->createElement("changefreq");
                $changefreq = $xml->createTextNode("daily");
                $change->appendChild($changefreq);

            $date = $xml->createElement("lastmod");
                $lastmod = $xml->createTextNode($date_time_stamp);
                $date->appendChild($lastmod);

            $title = $xml->createElement("priority");
                $titleText = $xml->createTextNode('1.0');
                $title->appendChild($titleText);

            $book = $xml->createElement("url");

                $book->appendChild($id);
                $book->appendChild($date);
                $book->appendChild($title);
                $book->appendChild($change);
                $root->appendChild($book);

        /*row ends*/
        /*row*/
           $id = $xml->createElement("loc");
                $idText = $xml->createTextNode(base_url().'about-us');
                $id->appendChild($idText);
            $change = $xml->createElement("changefreq");
                $changefreq = $xml->createTextNode("daily");
                $change->appendChild($changefreq);

            $date = $xml->createElement("lastmod");
                $lastmod = $xml->createTextNode($date_time_stamp);
                $date->appendChild($lastmod);

            $title = $xml->createElement("priority");
                $titleText = $xml->createTextNode('1.0');
                $title->appendChild($titleText);

            $book = $xml->createElement("url");
                 $book->appendChild($id);
                $book->appendChild($date);
                $book->appendChild($title);
                $book->appendChild($change);
                $root->appendChild($book);

        /*row ends*/
        /*stylist*/
          /*row*/
		  /* this page is not there in new web 
           $id = $xml->createElement("loc");
                $idText = $xml->createTextNode(base_url().'stylist/stylist-detail/prachiti-parakh');
                $id->appendChild($idText);
            $change = $xml->createElement("changefreq");
                $changefreq = $xml->createTextNode("daily");
                $change->appendChild($changefreq);

            $date = $xml->createElement("lastmod");
                $lastmod = $xml->createTextNode($date_time_stamp);
                $date->appendChild($lastmod);

            $title = $xml->createElement("priority");
                $titleText = $xml->createTextNode('1.0');
                $title->appendChild($titleText);

            $book = $xml->createElement("url");
                 $book->appendChild($id);
                $book->appendChild($date);
                $book->appendChild($title);
                $book->appendChild($change);
                $root->appendChild($book);
				*/
        /*row ends*/
        /*row*/
		/* this page is not there in new web 
         $id = $xml->createElement("loc");
              $idText = $xml->createTextNode(base_url().'stylist/stylist-detail/priyanka-bhatt');
              $id->appendChild($idText);
          $change = $xml->createElement("changefreq");
              $changefreq = $xml->createTextNode("daily");
              $change->appendChild($changefreq);

          $date = $xml->createElement("lastmod");
              $lastmod = $xml->createTextNode($date_time_stamp);
              $date->appendChild($lastmod);

          $title = $xml->createElement("priority");
              $titleText = $xml->createTextNode('1.0');
              $title->appendChild($titleText);

          $book = $xml->createElement("url");
               $book->appendChild($id);
              $book->appendChild($date);
              $book->appendChild($title);
              $book->appendChild($change);
              $root->appendChild($book);*/
      /*row ends*/
      /*row*/
	  /* this page is not there in new web 
       $id = $xml->createElement("loc");
            $idText = $xml->createTextNode(base_url().'stylist/stylist-detail/nikita-rawlani');
            $id->appendChild($idText);
        $change = $xml->createElement("changefreq");
            $changefreq = $xml->createTextNode("daily");
            $change->appendChild($changefreq);

        $date = $xml->createElement("lastmod");
            $lastmod = $xml->createTextNode($date_time_stamp);
            $date->appendChild($lastmod);

        $title = $xml->createElement("priority");
            $titleText = $xml->createTextNode('1.0');
            $title->appendChild($titleText);

        $book = $xml->createElement("url");
             $book->appendChild($id);
            $book->appendChild($date);
            $book->appendChild($title);
            $book->appendChild($change);
            $root->appendChild($book);*/
    /*row ends*/
    /*row*/
	/* this page is not there in new web 
     $id = $xml->createElement("loc");
          $idText = $xml->createTextNode(base_url().'stylist/stylist-detail/tasneem-makda');
          $id->appendChild($idText);
      $change = $xml->createElement("changefreq");
          $changefreq = $xml->createTextNode("daily");
          $change->appendChild($changefreq);

      $date = $xml->createElement("lastmod");
          $lastmod = $xml->createTextNode($date_time_stamp);
          $date->appendChild($lastmod);

      $title = $xml->createElement("priority");
          $titleText = $xml->createTextNode('1.0');
          $title->appendChild($titleText);

      $book = $xml->createElement("url");
           $book->appendChild($id);
          $book->appendChild($date);
          $book->appendChild($title);
          $book->appendChild($change);
          $root->appendChild($book);*/
  /*row ends*/

          /*stylist end*/


         /*row*/
             $id = $xml->createElement("loc");
                $idText = $xml->createTextNode(base_url().'blog/');
                $id->appendChild($idText);

            $change = $xml->createElement("changefreq");
                $changefreq = $xml->createTextNode("daily");
                $change->appendChild($changefreq);

            $date = $xml->createElement("lastmod");
                $lastmod = $xml->createTextNode($date_time_stamp);
                $date->appendChild($lastmod);

            $title = $xml->createElement("priority");
                $titleText = $xml->createTextNode('1.0');
                $title->appendChild($titleText);
            $book = $xml->createElement("url");
                 $book->appendChild($id);
                $book->appendChild($date);
                $book->appendChild($title);
                $book->appendChild($change);
                $root->appendChild($book);

        /*row ends*/
        /*row*/
            $id = $xml->createElement("loc");
                $idText = $xml->createTextNode(base_url().'brands/');
                $id->appendChild($idText);

            $change = $xml->createElement("changefreq");
                $changefreq = $xml->createTextNode("daily");
                $change->appendChild($changefreq);

            $date = $xml->createElement("lastmod");
                $lastmod = $xml->createTextNode($date_time_stamp);
                $date->appendChild($lastmod);

            $title = $xml->createElement("priority");
                $titleText = $xml->createTextNode('1.0');
                $title->appendChild($titleText);
            $book = $xml->createElement("url");
                 $book->appendChild($id);
                $book->appendChild($date);
                $book->appendChild($title);
                $book->appendChild($change);
                $root->appendChild($book);

        /*row ends*/

        //------------xml
          $xml->formatOutput = true;
        echo "<xmp>". $xml->saveXML() ."</xmp>";
        $xml->save("sitemap-static.xml") or die("Error");


    $xml = new DOMDocument('1.0','UTF-8');
    $root = $xml->createElementNS('http://www.sitemaps.org/schemas/sitemap/0.9', "urlset");

    $root = $xml->appendChild($root);
    $xml->appendChild($root);

/*
    foreach($ur as $u) {
            $id = $xml->createElement("loc");
                //$idText = $xml->createTextNode('http://www.stylecracker.com/looks/look_details/'. $u['slug']);
                $idText = $xml->createTextNode(base_url().'looks/look-details/'. $u['slug']);
                $id->appendChild($idText);

            $change = $xml->createElement("changefreq");
                $changefreq = $xml->createTextNode("daily");
                $change->appendChild($changefreq);

            $date = $xml->createElement("lastmod");
                $lastmod = $xml->createTextNode($date_time_stamp);
                $date->appendChild($lastmod);

            $title = $xml->createElement("priority");
                $titleText = $xml->createTextNode('1.0');
                $title->appendChild($titleText);
            $book = $xml->createElement("url");
                 $book->appendChild($id);
                $book->appendChild($date);
                $book->appendChild($title);
                $book->appendChild($change);
                $root->appendChild($book);

    }

    //------------xml
    $xml->formatOutput = true;
    echo "<xmp>". $xml->saveXML() ."</xmp>";
    $xml->save("sitemap1.xml") or die("Error");

    $xml = new DOMDocument('1.0','UTF-8');
    $root = $xml->createElementNS('http://www.sitemaps.org/schemas/sitemap/0.9', "urlset");
    $root = $xml->appendChild($root);
    $xml->appendChild($root);
*/


 /*second row*/
    foreach($urlslist as $url) {
            $id = $xml->createElement("loc");
                $idText = $xml->createTextNode( base_url().'brands/'.$url['user_name'].'-'.$url['user_id']);
                $id->appendChild($idText);

            $change = $xml->createElement("changefreq");
                $changefreq = $xml->createTextNode("daily");
                $change->appendChild($changefreq);

            $date = $xml->createElement("lastmod");
                $lastmod = $xml->createTextNode($date_time_stamp);
                $date->appendChild($lastmod);

            $title = $xml->createElement("priority");
                $titleText = $xml->createTextNode('1.0');
                $title->appendChild($titleText);
            $book = $xml->createElement("url");
                 $book->appendChild($id);
                $book->appendChild($date);
                $book->appendChild($title);
                $book->appendChild($change);
                $root->appendChild($book);
    }
 /*row ends*/


    $xml->formatOutput = true;
    echo "<xmp>". $xml->saveXML() ."</xmp>";
    $xml->save("sitemap-brands.xml") or die("Error");

    $xml = new DOMDocument('1.0','UTF-8');
    $root = $xml->createElementNS('http://www.sitemaps.org/schemas/sitemap/0.9', "urlset");

    $root = $xml->appendChild($root);
    $xml->appendChild($root);

 /*second row*/
    foreach($prod as $url) {
            $id = $xml->createElement("loc");
                $idText = $xml->createTextNode( base_url().'product/'.$url['slug'].'-'.$url['id']);
                $id->appendChild($idText);

            $change = $xml->createElement("changefreq");
                $changefreq = $xml->createTextNode("daily");
                $change->appendChild($changefreq);

            $date = $xml->createElement("lastmod");
                $lastmod = $xml->createTextNode($date_time_stamp);
                $date->appendChild($lastmod);

            $title = $xml->createElement("priority");
                $titleText = $xml->createTextNode('1.0');
                $title->appendChild($titleText);
            $book = $xml->createElement("url");
                 $book->appendChild($id);
                $book->appendChild($date);
                $book->appendChild($title);
                $book->appendChild($change);
                $root->appendChild($book);
    }
 /*row ends*/


    $xml->formatOutput = true;
    echo "<xmp>". $xml->saveXML() ."</xmp>";
    $xml->save("sitemap-products.xml") or die("Error");



    $xml = new DOMDocument('1.0','UTF-8');
    $root = $xml->createElementNS('http://www.sitemaps.org/schemas/sitemap/0.9', "urlset");

    $root = $xml->appendChild($root);
    $xml->appendChild($root);

 /*second row*/

 /*category row*/
    foreach($all_categories as $url) {
            $id = $xml->createElement("loc");
                $idText = $xml->createTextNode( base_url().'products/'.$url['category_slug'].'-'.$url['id']);
                $id->appendChild($idText);

            $change = $xml->createElement("changefreq");
                $changefreq = $xml->createTextNode("daily");
                $change->appendChild($changefreq);

            $date = $xml->createElement("lastmod");
                $lastmod = $xml->createTextNode($date_time_stamp);
                $date->appendChild($lastmod);

            $title = $xml->createElement("priority");
                $titleText = $xml->createTextNode('1.0');
                $title->appendChild($titleText);
            $book = $xml->createElement("url");
                 $book->appendChild($id);
                $book->appendChild($date);
                $book->appendChild($title);
                $book->appendChild($change);
                $root->appendChild($book);
    }
 /*row ends*/


    $xml->formatOutput = true;
    echo "<xmp>". $xml->saveXML() ."</xmp>";
    $xml->save("sitemap-categories.xml") or die("Error");



    $xml = new DOMDocument('1.0','UTF-8');
    $root = $xml->createElementNS('http://www.sitemaps.org/schemas/sitemap/0.9', "urlset");

    $root = $xml->appendChild($root);
    $xml->appendChild($root);

 /*category row*/

?>
