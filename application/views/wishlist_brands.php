<div class="page-common content-wrp">
    <div class="container">
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li class="active">My Brand Wishlist</li>
      </ol>
    
	
           <!-- look of the week -->
          <?php if($top_brands <= 0){  ?>
          <h4>Your wishlist is empty.</h4>
          <?php } ?>


    <div class="grid items-wrp items-section">

	    <div class="title-section">
            <?php if($top_brands <= 0){  ?>
                <h1 class="title"><span>OUR STYLISTS RECOMMEND:</span></h1>
            <?php }else{ ?>
                <h1 class="title"><span>My Wishlist - Brands</span></h1>
            <?php } ?>
            </div>
     <?php if(!empty($top_brands)) { ?>
      <div class="row grid-items-wrp" id="">
      <?php foreach($top_brands as $value){ ?>
      <div class="col-md-4 col-sm-6 grid-item">
        <div class="item">
          <div class="item-img item-hover">
          <?php
              $brand_img_url = '';
                if($value['home_page_promotion_img']!=''){
                  $brand_img_url = base_url().'assets/images/brands/homepage/'.$value['home_page_promotion_img'];
                }
               $brand_url = base_url().'brands/brand-details/'.$value['user_name'];
          ?>
            <a href="<?php echo $brand_url; ?>">
                <?php if($brand_img_url){ ?>
              <img title="<?php echo $value['company_name']; ?>- StyleCracker" alt="<?php echo $value['company_name']; ?> - StyleCracker" src="<?php echo $brand_img_url; ?>">
                <?php } else { ?>
                    <img title="<?php echo $value['company_name']; ?>- StyleCracker" alt="<?php echo $value['company_name']; ?> - StyleCracker" src="<?php echo base_url(); ?>assets/images/image-na.jpg">
                <?php } ?>
            </a>
            <div class="item-look-hover">
              <a href="<?php echo $brand_url; ?>"><span class="btn btn-primary"> View More </span></a>
            </div>
            </div>
            <div class="item-desc">
              <div class="item-title"><a title="<?php echo $value['company_name']; ?>" href="<?php echo $brand_url; ?>" target="_blank"><?php echo $value['company_name']; ?></a>
              </div>
              <div class="item-media">
                <ul class="social-buttons">
                  <li><a onclick="_brandtargetFacebookShare('<?php echo $value['company_name']; ?>','<?php echo $brand_url; ?>','<?php echo $brand_img_url; ?>','<?php echo $value['user_id']; ?>');"><i class="fa fa-facebook"></i></a></li>
                  <li><a onclick="_brandtargetTweet('<?php echo $value['company_name']; ?>','<?php echo $brand_url; ?>','<?php echo $brand_img_url; ?>','<?php echo $value['user_id']; ?>');"><i class="fa fa-twitter"></i></a></li>
                  <li><a onclick="_brandtargetPinterest('<?php echo $value['company_name']; ?>','<?php echo $brand_url; ?>','<?php echo $brand_img_url; ?>','<?php echo $value['user_id']; ?>');"><i class="fa fa-pinterest"></i></a></li>
                </ul>
                  <?php
                      if($this->session->userdata('user_id')){
                        if($this->home_model->get_fav_or_not('brand',$value['user_id']) == 0){ ?>
                          <div class="user-likes"><a target="_blank" title="Add to Wish List" id="wishlist<?php echo $value['user_id']; ?>" onclick="add_to__fav('brand','<?php echo $value['user_id']; ?>','');"><i class="ic sc-love"></i></a></div>
                        <?php }else{ ?>
                          <div class="user-likes"><a target="_blank" title="Remove from Wish List" id="wishlist<?php echo $value['user_id']; ?>" onclick="add_to__fav('brand',<?php echo $value['user_id']; ?>,'');reload_page();"><i class="ic sc-love active"></i></a></div>
                        <?php } }else{ ?>
                        <div class="user-likes"><a target="_blank" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a></div>
                      <?php }
                      ?>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>

      </div>
    </div>
    <?php } ?>
    </div>
  </div>
