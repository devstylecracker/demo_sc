<script src="<?php echo base_url()?>assets/js/ajaxfileupload.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/croppic.css">
<!-- <script src="<?php echo base_url()?>assets/js/ajaxfileupload.js"></script> -->
<script src="<?php echo base_url()?>assets/js/croppic.js?ver=2.0"></script>

<script type="text/javascript">
var cropperOptions = {
      //modal:true,
      zoomFactor:10,
      doubleZoomControls:false,
      rotateFactor:10,
      rotateControls:false,
      imgEyecandy:true,
			imgEyecandyOpacity:0.2,

      loaderHtml:'<div class="cropper-loader"></div>',
      uploadUrl:'<?php echo base_url(); ?>profile/cover_image/',
      cropUrl:'<?php echo base_url(); ?>profile/crop_img/',
      outputUrlId:'cover_img',
      onAfterImgCrop:function(){ location.reload(); },
    /*  cropData:{
        cropW:1200,
        cropH:380,
			},*/
    }
    var cropperHeader = new Croppic('cover_img', cropperOptions);
</script>


 <link rel="stylesheet" href="<?php echo base_url()?>assets/css/blueimp-gallery.min.css">
    <script src="<?php echo base_url()?>assets/js/jquery.blueimp-gallery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap-image-gallery.min.js"></script>


    <script type="text/Javascript">
    var cache = new ajaxCache('GET', true, 1800000);
    var stylist_offset,street_offset;
    function test(){ $('#frmWordrobe').submit(); }
    $(document).ready(function(){
        $('#frmWordrobe').submit(function(e) {
          $('#wardrobe_loader').css('display','inline-block');
           //e.preventDefault();
          $.ajaxFileUpload({
            url       :'<?php echo base_url(); ?>profile/upload_wordrobe/',
            fileElementId :'upload_photoes',
            dataType    : 'json',
            success : function (data, status)
            {
              if(data.status == 'success'){
              //  location.reload();
              wardrobe_images_grid();
              $('#wardrobe_loader').hide();
              }else{

              }

            }
          });

          e.stopPropagation();
          return false;
        });



    });

    $(function() {
      wardrobe_images_grid();


    /* document.getElementById('wardrobe_images_grid').getElementsByTagName('a').onclick = function (event) { alert('hi');
      event = event || window.event;
      var target = event.target || event.srcElement,
        link = target.src ? target.parentNode : target,
        options = {index: link, event: event},
        links = this.getElementsByTagName('a');
        blueimp.Gallery(links, options);
      };
*/
      var all_offset = "stylist_offset=0&street_offset=0&filter=0&page_type=wishlist";

      get_looks(all_offset);

      $( "#select_type" ).change(function() {
        $('#show_looks').html('');
        $('#show_looks').html('<div class="grid-sizer"></div>');

        all_offset = "stylist_offset=0&street_offset=0&page_type=wishlist&filter="+$('#select_type').val();

        get_looks(all_offset);
      });

      $( window ).load( function(){
      /*  $('.grid-items-wrp').masonry({
          // set itemSelector so .grid-sizer is not used in layout
          itemSelector: '.grid-item',
          // use element for option
          // columnWidth: '.grid-sizer',
          percentPosition: true
        });*/
      });

      $("#wardrobe_link").click(function(){
    //  alert("dfds");
        if ( $(".wardrobe-outer").hasClass( "hide-wardrobe" ) ) {
            $(".wardrobe-outer").slideDown('slow');
            $(".wardrobe-outer").addClass('show-wardrobe').removeClass('hide-wardrobe');
        }
        else {
            $(".wardrobe-outer").slideUp('slow');
              $(".wardrobe-outer").addClass('hide-wardrobe').removeClass('show-wardrobe');
        }
      });

      $('#upload_file').submit(function(e) {
        e.preventDefault();
        $.ajaxFileUpload({
          url       :'<?php echo base_url(); ?>profile/upload_profile/',
          fileElementId :'profile_img',
          dataType    : 'json',
          success : function (data, status)
          {
            if(data.status == 'success'){
              location.reload();
            }else{

            }

          }
        });
        return false;
      });

      $('#cover_img_form').submit(function(e) {
        e.preventDefault();
        $.ajaxFileUpload({
          url       :'<?php echo base_url(); ?>profile/cover_image/',
          fileElementId :'cover_img',
          dataType    : 'json',
          success : function (data, status)
          {
            if(data.status == 'success'){
              location.reload();
            }else{
              alert('error');
            }

          },error: function(xhr) {
            console.log(xhr);
          }
        });
        //return false;
      });


      $('#profile_img').change(function(e){
        $('#upload_file').submit();
      });

      /*$('#cover_img').change(function(e){
        //$('#cover_img_form').submit();
      });*/


        //wardrobe_images_grid();

      $('#profile_img_mobile').change(function(e){
        $('#upload_file').submit();
      });

    });

    var isTriggered = false;
    $(window).scroll(function() {
      var intTriggerScroll = 50;
      var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());

      if(intCurrentScrollPos<intTriggerScroll && !isTriggered){


        if($('#stylist_offset').length > 0){
          stylist_offset = $('#stylist_offset').val();
        }else{
          stylist_offset = '-1';
        }

        if($('#street_offset').length > 0){
          street_offset = $('#street_offset').val();
        }else{
          street_offset = '-1';
        }

        all_offset = "stylist_offset="+stylist_offset+"&street_offset="+street_offset+"&page_type=wishlist&filter="+$('#select_type').val();
        isTriggered=true;

        if($('#stylist_offset').length >0 || $('#street_offset').length >0){
          get_looks(all_offset);
        }
      }

    });


    function get_looks(viewData)
    {
      var ajax_path = '<?php echo base_url(); ?>Schome/get_all_wishlist_looks';
      $('#look_loader_img').css('display', 'inline-block');
      if(cache.on)
      {
        var cachedResponse = cache.get(ajax_path, [viewData]);
        if(cachedResponse !== false)
        {
          // update website
          update_website(cachedResponse); // We just avoided one ajax request
          initialiceMasonry();
          add_hover();
          return true;
        }
      }

      $.ajax({
        url: ajax_path,
        type: 'GET',
        data: viewData,
        cache :true,
        success: function(response) {
          if(cache.on) cache.put(ajax_path, viewData, response); // record the new response

          update_website(response);
          initialiceMasonry();
          add_hover();
        },
        error: function(xhr) {

        }
      });

      return true;
    }

    function update_website(response){

      if($('#stylist_offset').length >0 ){ $('#stylist_offset').remove(); }
      if($('#street_offset').length >0 ){ $('#street_offset').remove(); }

      $('#show_looks').append(response);
      $('#look_loader_img').hide();
      isTriggered = false;

    }

 function initialiceMasonry(){
          var $container = $('.grid-items-wrp');
            $container.masonry({
                isInitLayout : true,
                itemSelector: '.grid-item',
                isAnimated: false,
                percentPosition: true

            });

            $container.imagesLoaded(function() {
              $( $container ).masonry( 'reloadItems' );
              $( $container ).masonry( 'layout' );
            });
          }

    function wardrobe_images_grid(){
     $.ajax({
        url: '<?php echo base_url(); ?>profile/display_wardrobe',
        type: 'GET',
        cache :true,
        success: function(response) {
          $('.wardrobe-slider').html(response);

        ///  });
          /*$('.flexslider_wardrobe').flexslider({
            selector: ".item-img > a",
            animation: "slide",
            animationLoop: true,
            itemWidth: 100,
            smoothHeight:true,
            maxItems: 4,
            prevText: "Previous",
            nextText: "Next",
            start: function(slider){
                slider.resize();
            }

            });*/
        },
        error: function(xhr) {

        }
      });
    }

    function delete_wardrobe(id){
      if (confirm("Are you sure?")) {
       $.ajax({
        url: '<?php echo base_url(); ?>profile/delete_wardrobe_img',
        type: 'POST',
        data: {'id': id },
        cache :true,
        success: function(response) {
          wardrobe_images_grid();
        },
        error: function(xhr) {

        }
      });
     }
    }

    function remove_from__wishlist(look_id){
    $('#whislist'+look_id).attr("onclick","add_to_wishlist("+look_id+")");
    $('#whislist'+look_id).find('i').removeClass('active');
    $('#'+look_id).remove();
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host;

      $.ajax({
        url: baseUrl+"/profile/remove_from_wishlist",
        type: 'POST',
        data: {'look_id':look_id},
        cache :true,
        success: function(response) {

          initialiceMasonry();

        },
        error: function(xhr) {

        }
      });

  }

   function sc_resetpwd(){

    var oldpwd = $('#oldpwd').val();
    var newpwd = $('#newpwd').val();

    if(oldpwd!='' && newpwd!=''){

      $.ajax({
        url: '<?php echo base_url(); ?>schome/resetPassword',
        type: 'post',
        data: {'type' : 'reset_pwd','oldpwd':oldpwd,'newpwd':newpwd },
        success: function(data, status) {

          $('#loginrp_error').html(data);
          return false;
          event.preventDefault();
        },
        error: function(xhr, desc, err) {
          // console.log(err);
        }
      });
    }
 }
    </script>


  <script>
$(document).ready(function() {
  //## UserTrack: Page ID=========================================
  document.cookie="pid=9";
  /*Code ends*/ 

});
  </script>
