<!-- <link rel="manifest" href="/manifest.json">
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
<script>
  var OneSignal = window.OneSignal || [];
  OneSignal.push(["init", {
    appId: "774c695f-4257-45ad-bbd7-76a22b12117b",
    autoRegister: true,
    notifyButton: {
      enable: false /* Set to false to hide */
    }
  }]);
   //OneSignal.push(["sendTags", {name: "test", email: "test@stylecracker.com"}]);
</script> -->

<link href='<?php echo base_url(); ?>assets/css/home2.css?v=1.111' rel='stylesheet' type='text/css'>
<div class="home-content-wrp">
  <section class="section-main-banners">
    <div class="container custom">
      <div class="home-banner-wrp">
        <div class="swiper-container-front front main">
          <ul class="swiper-wrapper">
            <li class="swiper-slide slide">
              <picture>
                <source srcset="<?php echo base_url(); ?>assets/images/home/main-slider/1m.jpg" media="(max-width: 360px)">
                <source srcset="<?php echo base_url(); ?>assets/images/home/main-slider/1.jpg">
                <img src="<?php echo base_url(); ?>assets/images/home/main-slider/1.jpg">
              </picture>
            </li>
            <li class="swiper-slide slide">
              <picture>
                <source srcset="<?php echo base_url(); ?>assets/images/home/main-slider/2m.jpg" media="(max-width: 360px)">
                <source srcset="<?php echo base_url(); ?>assets/images/home/main-slider/2.jpg">
                <img src="<?php echo base_url(); ?>assets/images/home/main-slider/2.jpg">
              </picture>
            </li>
            <li class="swiper-slide slide">
              <picture>
                <source srcset="<?php echo base_url(); ?>assets/images/home/main-slider/3m.jpg" media="(max-width: 360px)">
                <source srcset="<?php echo base_url(); ?>assets/images/home/main-slider/3.jpg">
                <img src="<?php echo base_url(); ?>assets/images/home/main-slider/3.jpg">
              </picture>
            </li>
            <li class="swiper-slide slide">
              <picture>
                <source srcset="<?php echo base_url(); ?>assets/images/home/main-slider/4m.jpg" media="(max-width: 360px)">
                <source srcset="<?php echo base_url(); ?>assets/images/home/main-slider/4.jpg">
                <img src="<?php echo base_url(); ?>assets/images/home/main-slider/4.jpg">
              </picture>
            </li>
          </ul>

        </div>
        <div class="swiper-pagination front main"></div>

        <div class="box-content">
          <div class="box-content-inner">
            <div class="title">
              <div class="logo-banner-wrp">
                <img src="<?php echo base_url(); ?>assets/images/sc-logo-m.png" class="logo-banner">
              </div>
              Your Style Concierge
            </div>
            <div class="content">
              Clothing, accessories,
              <br /> beauty & grooming handpicked especially
              <br /> for you by celebrity stylists, for free!
            </div>
            <a onclick="ga('send', 'event', 'Sign Up', 'clicked', 'home page banner sign up');" class="btn btn-secondary-two" href="#myModal" data-toggle="modal" title="">
              Sign Up Now
            </a>
          </div>
        </div>

      </div>
    </div>
  </section>
  <section class="section-looks">
    <div class="container">
      <div class="row">
          <!-- block 1 -->
		      <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="item-view-wrp">
            <div class="item view">
              <img src="<?php echo base_url(); ?>assets/images/home/section2/summer-ready-2403-min.jpg" alt="Summer Ready" />
              <div class="mask">
                <div class="inner">
                  <div class="btns-wrp">
                    <a onclick="ga('send', 'event', 'ProductCategory', 'clicked', 'Summer Ready');" href="https://www.stylecracker.com/summer-pop-up" class="info btn btn-primary white">Shop Now</a>
          </div>
                </div>
              </div>
            </div>
          </div>
        </div>
		 <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="item-view-wrp">
            <div class="item view">
              <img src="<?php echo base_url(); ?>assets/images/home/section2/travel-shop-2403-min.jpg" alt="Travel Shop" />
              <div class="mask">
                <div class="inner">
                  <div class="btns-wrp">
                    <a onclick="ga('send', 'event', 'ProductCategory', 'clicked', 'Travel Shop');" href="https://www.stylecracker.com/travel-shop" class="info btn btn-primary white">Shop Now</a>
          </div>
                </div>
              </div>
            </div>
          </div>
        </div>
		 <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="item-view-wrp">
            <div class="item view">
              <img src="<?php echo base_url(); ?>assets/images/home/section2/scbox-2403.jpg" alt="SC Box" />
              <div class="mask">
                <div class="inner">
                  <div class="btns-wrp">
                    <a onclick="ga('send', 'event', 'SCBOX', 'clicked', 'Small Banner');" href="https://www.stylecracker.com/sc-box" class="info btn btn-primary white">Shop Now</a>
          </div>
                </div>
              </div>
            </div>
          </div>
        </div>
		
		
		  <!--
          <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="item-view-wrp">
            <div class="item view">
              <img src="<?php echo base_url(); ?>assets/images/home/section2/all-things-new-b1.jpg" alt="All Things New" />
              <div class="mask">
                <div class="inner">
                  <div class="btns-wrp">
                    <a onclick="ga('send', 'event', 'ProductCategory', 'clicked', 'All Things New');" href="https://www.stylecracker.com/all-things-new#women-section" class="info btn btn-primary white">Shop Now</a>
          </div>
                </div>
              </div>
            </div>
          </div>
        </div>
     
	    <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="item-view-wrp">
          <div class="item view">
            <img src="<?php echo base_url(); ?>assets/images/home/section2/popup-valentine-1.jpg" alt="Valentine Pop Up" />
            <div class="mask">
              <div class="inner">
                <div class="btns-wrp">
                  <a onclick="ga('send', 'event', 'ProductCategory', 'clicked', 'Valentine Pop Up');" href="https://www.stylecracker.com/valentine-pop-up" class="info btn btn-primary white">Shop Now</a>
        </div>
              </div>
            </div>
          </div>
        </div>
      </div>-->

    <!--    <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="item-view-wrp">
          <div class="item view">
            <img src="<?php echo base_url(); ?>assets/images/home/section2/kavalai-vendam-s2.jpg" alt="Kavalai Vendam"  width="270" height="205" />
            <div class="mask">
              <div class="inner">
                <h2>Get looks from Kavalai Vendam! Styled by StyleCracker #WearItLikeKajal</h2>
                <div class="btns-wrp">
                  <a onclick="ga('send', 'event', 'ProductCategory', 'clicked', 'Kavalai Vendam');" href="https://www.stylecracker.com/kavalai-vendam" class="info btn btn-primary white">Get this</a>
        </div>
              </div>
            </div>
          </div>
        </div>
      </div>-->
        <!-- block 2 -->
      <!--  <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="item-view-wrp">
          <div class="item view">
            <img src="<?php echo base_url(); ?>assets/images/home/section2/party-popup-s2.jpg" alt="Fashion for your 2016 exit"  width="270" height="205" />
            <div class="mask">
              <div class="inner">
                <h2>Hey there '17! Curated by celebrity stylists, the coolest fashion for your 2016 exit</h2>
                <div class="btns-wrp">
                  <a onclick="ga('send', 'event', 'ProductCategory', 'clicked', 'Fashion for your 2016 exit');" href="https://www.stylecracker.com/party-pop-up" class="info btn btn-primary white">Get this</a>
        </div>
              </div>
            </div>
          </div>
        </div>
      </div>-->
        <!-- block 3 -->
    <!--    <div class="col-md-4 col-sm-12 col-xs-12">
          <div class="item-view-wrp">
          <div class="item view">
            <img src="<?php echo base_url(); ?>assets/images/home/section2/coffe-t-shirt-min.jpg"  alt="Coffee dates with fashion"  width="270" height="205" />
            <div class="mask">
              <div class="inner">
                <h2>Coffee dates with fashion</h2>
                <div class="btns-wrp">
                  <a onclick="ga('send', 'event', 'Product', 'clicked', 'Coffee dates with fashion');" href="https://www.stylecracker.com/product/details/coffee-addict-185465" class="info btn btn-primary white">Shop Now</a>
				</div>
				 <a onclick="ga('send', 'event', 'Sign Up', 'clicked', 'Will this work for me');" class="info btn btn-secondary-two white" href="#myModal" data-toggle="modal">
							Will this work for me?
						</a>
              </div>
            </div>
          </div>
        </div>
      </div>-->

        <!-- /block 3 -->
        <!-- block 4-->
      <!--  <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="item-view-wrp">
          <div class="item view">
            <img src="<?php echo base_url(); ?>assets/images/home/section2/diwali-box-1.jpg" alt="Shop one of a kind Diwali Gift Boxes curated by celebrity stylists"  width="270" height="205"/>
            <div class="mask">
              <div class="inner">
                <h2>Shop one of a kind Diwali Gift Boxes curated by celebrity stylists</h2>
                <div class="btns-wrp">
                  <a onclick="ga('send', 'event', 'ProductCategory', 'clicked', 'Shop one of a kind Diwali Gift Boxes curated by celebrity stylists');" href="https://www.stylecracker.com/brand/scbox" class="info btn btn-primary white">Get this</a> </div>
              </div>
            </div>
          </div>
        </div>
      </div>-->
        <!-- /block 4 -->
<!--
        <div class="col-md-4 col-sm-12 col-xs-12">
          <div class="item view">
            <img src="<?php echo base_url(); ?>assets/images/home/section2/firecracker-2.jpg" width="366" height="278" alt="StyleCracker FireCracker Contest" />
            <div class="mask">
              <div class="inner">
                <div class="btns-wrp">
                  <a class="info btn btn-primary white" href="https://www.stylecracker.com/blog/stylecracker-firecracker-contest/?utm_source=Facebook&utm_medium=CPC&utm_campaign=Facebook_Firecracker_RegestrationLink_12/09/2016" title="StyleCracker FireCracker Contest">
				Read More
				</a>
                </div>
              </div>
            </div>
          </div>
        </div>
-->
      </div>
    </div>
  </section>

  <section class="section-slider-verticle home-slider-verticle">
    <div class="container1">
      <div class="swiper-container front verticle">
        <div class="container swiper-pagination-wrp">
          <div class="swiper-pagination verticle"></div>
        </div>
        <ul class="swiper-wrapper">
          <li class="swiper-slide slide">
            <div class="container">
              <div class="inner inner-benefits">
                <div class="row">
                  <div class="col-md-4">
                    <div class="img-wrp">
                      <img src="<?php echo base_url(); ?>assets/images/home/banner/how-it-works-3-2.jpg" />
                    </div>
                  </div>
                  <div class="col-md-8">
                    <div class="content">
                      <h3 class="title">Benefits</h3>
                      <div class="desc">
                        <ul>
                          <li><i></i>Personalised selections</li>
                          <li><i></i>Celebrity stylists on live chat 24x7 for free!</li>
                          <li><i></i>All fashion for your budget</li>
                          <li><i></i>Exclusive and popular brands</li>
                          <li><i></i>The more you use us, the better we get</li>
                          <li><i></i>Stay up to date with the best fashion content</li>
                        </ul>
                      </div>
                      <a data-toggle="modal" href="#myModal" class="info btn btn-secondary-two" title="Get Styled">Get Styled</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li class="swiper-slide slide">
            <div class="container">
              <div class="inner inner-how-it-works">
                <div class="row">
                  <div class="col-md-4">
                    <div class="img-wrp">
                      <img src="<?php echo base_url(); ?>assets/images/home/banner/how-it-works-3-1.jpg" />
                    </div>
                  </div>
                  <div class="col-md-8">
                    <div class="content">
                      <h3 class="title">How it works:</h3>
                      <div class="desc">
                        <ul>
                          <li>
                            <span class="fb">Step 1:</span> Take a 30 second personal style quiz
                          </li>
                          <li>
                            <span class="fb">Step 2:</span> Browse looks and products on your personal feed created for you by celebrity stylists </li>
                          <li>
                            <span class="fb">Step 3:</span> Shop different!
                          </li>
                        </ul>
                      </div>
                      <a data-toggle="modal" href="#myModal" class="info btn btn-secondary-two" title="Get Started">Get Started</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>

          <li class="swiper-slide slide">
            <div class="container">
              <div class="inner">
                <div class="row">
                  <div class="col-md-4">
                    <div class="img-wrp">
                      <img src="<?php echo base_url(); ?>assets/images/home/banner/how-it-works-3-3.jpg" />
                    </div>
                  </div>
                  <div class="col-md-8">
                    <div class="content">
                      <h3 class="title">Life isn't perfect, <br /> but your outfit can be</h3>
                      <p class="desc">
                        For busy women & men on the go, StyleCracker acts as your style concierge to pick the best fashion, beauty and grooming tailored to your taste, budget and lifestyle, delivered to your doorstep, for free!
                      </p>
                      <a data-toggle="modal" href="#myModal" class="info btn btn-secondary-two" title="Get Stylish">Get Stylish</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <!--  <div class="swiper-button-next verticle"></div>
      <div class="swiper-button-prev verticle"></div>-->
    </div>
  </section>
  <section class="section-work">
    <div class="container custom">
      <div class="section-work-inner">
        <div class="row">
          <div class="col-md-4">
            <div class="desc">
              <h2 class="title">See how we <br />work it.</h2>
              <p>
                We style the stars. Want to look like a celebrity?
              </p>
              <a data-toggle="modal" href="#myModal" class="btn btn-secondary-two">Sign Up Now</a>
            </div>
          </div>

          <div class="col-md-8">

            <div class="swiper-container work">
              <ul class="swiper-wrapper">
			  <li class="swiper-slide slide">
                  <img src="<?php echo base_url(); ?>assets/images/home/banner/work-ranbir-kapoor-filmfare.jpg" />
                </li>
                <li class="swiper-slide slide">
                  <img src="<?php echo base_url(); ?>assets/images/home/banner/work-shaandaar.jpg" />
                </li>
                <li class="swiper-slide slide">
                  <img src="<?php echo base_url(); ?>assets/images/home/banner/work-filmfare.jpg" />
                </li>
                <li class="swiper-slide slide">
                  <img src="<?php echo base_url(); ?>assets/images/home/banner/work-kapoor-n-sons.jpg" />
                </li>
                <li class="swiper-slide slide">
                  <img src="<?php echo base_url(); ?>assets/images/home/banner/work-ellina.jpg" />
                </li>

              </ul>
              <div class="swiper-pagination work"></div>
              <div class="swiper-button-next front work"></div>
              <div class="swiper-button-prev front work"></div>
            </div>


          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section-products">
    <h1 class="title">Flying off the Racks</h1>
    <div class="container custom">
      <div class="swiper-container front products">
        <ul class="swiper-wrapper">
          <?php foreach ($products as $value) {?>
            <li class="swiper-slide slide">
              <a onclick="ga('send', 'event', 'Product', 'clicked', '<?php echo $value['name']; ?>');" href="<?php echo base_url()."product/details/".$value['slug']."-".$value['id'];?>">
                <div class="item">
                  <div class="item-img">
                    <div class="item-img-inner">
                      <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_124/<?=$value['image'] ?>" />
                    </div>
                  </div>
                  <div class="item-desc">
                    <div class="item-name">
                      <?php echo $value['name'] ?>
                    </div>
                    <div class="item-price">
                      <?php if($this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price']>0 || $value['compare_price']>$value['price']) { ?>
                        <span class="mrp-price text-strike"><i class="fa fa-inr"></i> <?php if($value['compare_price']>$value['price']){ echo $value['compare_price']; }else { echo $value['price']; $value['compare_price'] = $value['price'];} ?></span>
                        <!--<span class="price"><i class="fa fa-inr"></i> <?php echo $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price'] : ''; ?></span>
                  <span class="discount-price">(<?php echo $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discount_percent']; ?>% OFF)</span>-->
                        <span class="price"><i class="fa fa-inr"></i> <?php $discount_price = $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price'] : ''; if($discount_price > 0){ echo $discount_price; }else{
					  echo $value['price'];
				  } ?></span>
                        <span class="discount-price">(<?php $discount_percent = $this->Product_desc_model->calculate_discount_percent($value['compare_price'],$discount_price,$value['price']); echo $discount_percent; ?>% OFF)</span>

                        <?php }else{ ?>
                          <span class="price"><i class="fa fa-inr"></i> <?php echo $value['price']; ?></span>
                          <?php } ?>
                    </div>
                  </div>
                </div>
              </a>
            </li>
            <?php } ?>
        </ul>
      </div>
      <div class="swiper-button-next front products"></div>
      <div class="swiper-button-prev front products"></div>
      <!--  <div class="swiper-pagination front products"></div>-->
    </div>
  </section>
  <section class="box-testimonials-wrp">
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <div class="box-testimonials img-wrp">
            <img src="<?php echo base_url(); ?>assets/images/home/banner/glen.jpg" />
          </div>
        </div>
        <div class="col-md-3">
          <div class="box-testimonials text">
            <p>
              Since I was looking for a personal stylist for myself, having one who’s worked with A-listers and the biggest names in the movie business made StyleCracker my absolute choice! From being style guides to helping me with even ‘what not to wear’, they’ve been extremely helpful in solving all my fashion queries.
            </p>
            <div class="name">
              Glenn Gonsalves
            </div>
          </div>
        </div>
		<!--
		<div class="col-md-3">
          <div class="box-testimonials text">
            <p>
              Absolutely love the stuff on stylecracker! Chic and unique. Alot of pretty cool brands to choose from. Customer service is fab! They literally take care of you and your order.
            </p>
            <div class="name">
              Rajni
            </div>
          </div>
        </div>
		-->

        <div class="col-md-3">
          <!--  <div class="box-testimonials img-wrp video-img-wrp">
            <a class="scmfp-video" href="https://www.youtube.com/watch?v=aITNSU2FDNs">
              <img src="<?php echo base_url(); ?>assets/images/home/banner/t2.jpg"  />
              <div class="mask">
                <i class="fa fa-play-circle-o"></i>
              </div>
            </a>
          </div>-->
          <div class="box-testimonials img-wrp">
            <img src="<?php echo base_url(); ?>assets/images/home/banner/testimonial-stylefiesta.jpg" />
          </div>
        </div>
        <div class="col-md-3">
          <div class="box-testimonials text">
            <p>
              We’re very pleased to be associated with StyleCracker. Their support and co-ordination has been quite impressive & we hope to continue the same relationship in future.
            </p>
            <div class="name">
              Style Fiesta
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3 col-md-push-3">
          <div class="box-testimonials img-wrp">
            <img src="<?php echo base_url(); ?>assets/images/home/banner/ramona.jpg" />
          </div>
        </div>
        <div class="col-md-3 col-md-pull-3">
          <div class="box-testimonials text text-right">
            <p>
              The StyleCracker stylists have been like 3 am friends. They have helped me pick products that suit me and my body type. Every purchase is tailor made and I love how personal it gets. And in rare scenario when a product doesn’t work , the team is extremely
              helpful and prompt in arranging easy returns.
            </p>
            <div class="name">
              Ramona
            </div>


          </div>
        </div>


        <div class="col-md-3 col-md-push-3">
          <div class="box-testimonials img-wrp">
            <img src="<?php echo base_url(); ?>assets/images/home/banner/testimonial-spring-break.jpg" />

          </div>
        </div>
        <div class="col-md-3 col-md-pull-3">
          <div class="box-testimonials text text-right">
            <p>
              Online and offline, StyleCracker has always been exciting and wonderful. It’s refreshing to see a platform that creates and executes concepts for brands to showcase and turn the fashion space a lot more fun and interesting, all at once. Like us, SC too
              comes with a fresh, youthful and very today’s gen vibe and that has always been the core connect between the two brands!
            </p>
            <div class="name">
              Spring Break
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>

  <!--<section class="banner-strip">
    <div class="container">
      <div class="strip">
        Itsy Btsy Teenie Weenie Yellow Polka Dot Bikini
      </div>
    </div>
  </section>-->

  <section class="section-media">
    <h3 class="title">Psst, they're talking</h3>
    <div class="container">
      <ul>
        <li>
          <a href="https://www.youtube.com/watch?v=yyamOd7eaM0&feature=youtu.be" target="_blank">
          <img src="<?php echo base_url(); ?>assets/images/home/logos/logo-bloomberg-tv-india.png" />
          </a>
        </li>
        <li>
          <a href="http://archive.indianexpress.com/news/it-s-personal/1129558/0" target="_blank">
          <img src="<?php echo base_url(); ?>assets/images/home/logos/logo-ie.png" />
          </a>
        </li>
        <!--  <li>
            <a href="#" target="_blank">
          <img src="<?php echo base_url(); ?>assets/images/home/logos/logo-grazia.png" />
          </a>
        </li>-->
        <li>
          <a href="http://economictimes.indiatimes.com/small-biz/startups/stylecracker-gets-rs-6-crore-in-funds-from-hnis/articleshow/48751226.cms  " target="_blank">
          <img src="<?php echo base_url(); ?>assets/images/home/logos/logo-the-economic-times-logo.png" />
          </a>
        </li>
        <li>
          <a href="https://www.youtube.com/watch?v=aITNSU2FDNs&feature=youtu.be" target="_blank">
          <img src="<?php echo base_url(); ?>assets/images/home/logos/logo-cnbc-tv18.png" />
          </a>
        </li>
      </ul>
    </div>
  </section>

</div>
<style>
  .navbar-header-md .navbar-brand {
    display: none;
  }
  .header.fixed .navbar-header-md .navbar-brand {
    display: inherit;
  }

</style>
<script type="text/Javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-64209384-1', 'auto'); ga('require','linkid','linkid.js'); ga('require', 'displayfeatures'); ga('send', 'pageview');
  var sc_baseurl = '<?php echo base_url(); ?>';
  var sc_invite_utm_source = "<?php echo $this->session->userdata('utm_source')!='' ? $this->session->userdata('utm_source') : ''; ?>";
  function invitereferrals_10640(){ var params = { bid: 11257, cid: 10607 }; invite_referrals.widget.inlineBtn(params); } if(sc_invite_utm_source == 'invitereferrals'){ document.getElementById("invite_message").style.display = "block"; /*$('#invite_message').show();*/
      }else { /*document.getElementById("invite_message").style.display = "none"; //$('#invite_message').hide(); */
    } function close_topbar(){ document.getElementById("invite_message").style.display = "none"; }
</script>
