<div class="page-common content-wrp">
    <div class="container">
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li class="active">My Products Wishlist</li>
      </ol>
    
           <!-- look of the week -->
          <?php if($products_total <= 0){  ?>
          <h4>Your wishlist is empty.</h4>
          <?php } ?>
   
   <div class="grid grid-products-col-4 items-wrp items-section">
       <div class="title-section">
                 <?php if($products_total <= 0){  ?>
                <h1 class="title"><span>OUR STYLISTS RECOMMEND:</span></h1>
            <?php }else{ ?>
                <h1 class="title"><span>My Wishlist - Products</span></h1>
            <?php } ?>
            </div>
     <?php if(!empty($products)) { ?>
    <div class="row grid-items-wrp" >
      <?php foreach ($products as $value) { ?>
      <div class="col-md-3 col-sm-6 grid-item">
        <div class="item item-product">
          <div class="item-img item-hover">
            <div class="item-img-inner">
              <?php
                $short_url = base_url().'product/details/'.$value['id'];
                $img_title_alt = str_replace('#','',$value['name']);
                $img_url = $this->config->item('product_image_thumb').$value['image'];

              ?>

                 <a href="<?php echo base_url(); ?>product/details/<?php echo $value['id']; ?>"><img src="<?php echo $this->config->item('product_image_thumb').$value['image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
               </div>
            <div class="item-look-hover">
              <a href="<?php echo $short_url; ?>" title="<?php echo $img_title_alt; ?>" target="_blank"><span class="btn btn-primary"><?php echo $this->lang->line('get_this'); ?></span></a>
            </div>
              </div>
            <div class="item-desc">
              <div class="item-title"><a title="<?php echo $img_title_alt; ?>" href="<?php echo $short_url; ?>" target="_blank"><?php echo $value['name']; ?></a>
              </div>
              <div class="item-price-wrp">
                <div class="item-price"><span class="price"><i class="fa fa-inr"></i><?php echo $value['price']; ?></span></div>

                <?php
                      if($this->session->userdata('user_id')){
                        if($this->home_model->get_fav_or_not('product',$value['id']) == 0){ ?>
                          <div class="user-likes"><a target="_blank" title="Add to Wish List" id="wishlist<?php echo $value['id']; ?>" onclick="add_to_fav('product','<?php echo $value['id']; ?>','');"><i class="ic sc-love"></i></a></div>
                        <?php }else{ ?>
                          <div class="user-likes"><a target="_blank" title="Remove from Wish List" id="wishlist<?php echo $value['id']; ?>" onclick="add_to_fav('product',<?php echo $value['id']; ?>,'');reload_page();"><i class="ic sc-love active"></i></a></div>
                        <?php } }else{ ?>
                        <div class="user-likes"><a target="_blank" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a></div>
                      <?php }
                      ?>
              </div>
            </div>
          </div>
        </div>
       <?php } ?>
      </div>
      <?php } ?>
    </div>
    </div>
    </div>
