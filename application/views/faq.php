<div class="page-header faq-header">
  <div class="container">
    <h1>Frequently Asked Questions</h1>
  </div>
</div>
<div class="content-wrp page-faq content-faq">
  <div class="container">

    <div class="faq-ques"><h4>1) What is StyleCracker.com?</h4></div>
    <div class="faq-ans">
      <p>StyleCracker is an interactive online experience providing personalised styling to all. Our motto - Everyone should embrace their OWN personal style and StyleCracker helps you achieve that.</p>
    </div>

    <div class="faq-ques"><h4>2) How does StyleCracker work?</h4></div>
    <div class="faq-ans"><p>It's simple – you ask us a question, we have an answer for it. Be it your clothes, accessories, hair and make-up even, we have it all. Essentially, we tell you what to buy, where to buy it and how to wear it. If you have two black dresses, don’t worry, we are not going to advise you to buy a third. Instead, we will help you wear the dresses you currently have, differently each time.<br>On the StyleCracker site, shopping is seamlessly synced with getting styled. So, with a simple click on our 'Buy Now' button, you can instantly purchase the recommendations our stylists put forth. Looking good has never been easier!</p></div>

    <div class="faq-ques"><h4>3) Who's the StyleCracker team?</h4></div>
    <div class="faq-ans"><p>The StyleCracker team consists of talented celebrity stylists, headed by Archana Walavalkar. Prior to co-founding StyleCracker, she worked with Vogue and L'Officiel Magazine for nearly 7 years. She has styled the likes of Karan Johar, Aishwarya Rai Bachchan, Deepika Padukone, Sonam Kapoor and continues to do so.</p></div>


    <div class="faq-ques"><h4>4) Can I get in touch with my stylist anytime?</h4></div>
    <div class="faq-ans"><p>We work all day, every day so ask away!</p></div>


    <div class="faq-ques"><h4>5) Do I need to pay to get styled by StyleCracker?</h4></div>
    <div class="faq-ans"><p>Our online styling services are completely free of charge. Sign up and get your very own personal stylist today!</p></div>


    <div class="faq-ques"><h4>6) Can I shop on StyleCracker.com?</h4></div>
    <div class="faq-ans"><p>With our ‘Buy Now’ feature you can instantly purchase the products recommended to you, by our stylists.</p> </div>


    <div class="faq-ques"><h4>7) What if I'm not happy with a purchase suggested by you?</h4></div>
    <div class="faq-ans"><p>StyleCracker is not accountable for any procedures related to the purchase and post-purchase of goods. However, our team will be happy to address any style related queries to ensure that you look crackingly fabulous always!</p></div>


    <div class="faq-ques"><h4>8) What's more on StyleCracker?</h4></div>
    <div class="faq-ans"><p>Apart from getting regular style advice from our expert stylists, you also get-</p>
      <ul>
        <li>Personalized looks, created by our team, just for you!</li>
        <li>Honest reviews from real customers about our partner brands, to help you with your next purchase.</li>
        <li>Exclusive invites to StyleCracker events and workshops.</li>
        <li>Special StyleCracker.com discounts and deals.</li>
        <li>Updates on the latest trends, celebrity style news and designer deals on our blog.</li>
      </ul>
    </div>


    <div class="faq-ques"><h4>9) Is StyleCracker for Women and Men?</h4></div>
    <div class="faq-ans"><p>Currently, our online portal caters to women only. We do undertake offline styling projects for men, though. Mail us at <a href="mailto:support@stylecracker.com">support@stylecracker.com</a> for further information.</p>
      <p>StyleCracker.com for Men will be launched soon.</p></div>


  </div>
</div>
<script>
$( window ).load( function(){
        //## UserTrack: Page ID=========================================
        document.cookie="pid=5";
        /*Code ends*/
  });
</script>
