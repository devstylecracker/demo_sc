<?php
//$user_name = $this->session->userdata('user_name');
//$email = $this->session->userdata('email');
//$user_id = $this->session->userdata('user_id');
//$bucket = $this->session->userdata('bucket');
?>
<div class="page-brand-details">
<div class="container custom">
          <?php if(@$brand_info[0]['cover_image']!=''){ ?>
          <div class="brands-banner-wrp">
              <img src="<?php echo $this->config->item('brandpage_cover_image_url').$brand_info[0]['cover_image']; ?>">
            </div>
          <?php } ?>        
</div>

  <div class="container">
   <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>">Home</a></li>
    <li><a href="<?php echo base_url(); ?>brands">Brands</a></li>
    <li class="active"><?php echo $this->Product_desc_model->get_brand_name($company_name,$products_data[0]['id']); ?></li>
  </ol>

<div class="content-wrp content-brand-details">
    <div class="brand-desc-section">
      <input type="hidden" id="brand_token" name="brand_token" value="<?php echo @$brand_info[0]['user_id']; ?>">
        <div class="brand-desc">
          <?php  if(@$brand_info[0]['short_description']!=''){ ?>
            <div class="brand-desc-inner">
              <?php echo @$brand_info[0]['short_description']; ?>
            </div>
            <?php } ?>
            <div>
              <ul class="brand-contact">
                <?php if(@$emailid!=''){?>
                 <!--li class="email">
                    <i class="fa fa-envelope"></i>
                    <a href="mailto:<?php echo @$emailid; ?>"><?php echo @$emailid; ?></a>
                  </li-->

                  <?php } ?>
                  <?php /*if(@$brand_info[0]['phone_one']!='' || @$brand_info[0]['phone_two']!=''){ ?>
                    <li class="phone">
                      <i class="fa fa-phone"></i> <?php echo @$brand_info[0]['phone_one'];
                      if(@$brand_info[0]['phone_one']!='' && @$brand_info[0]['phone_two']){ echo ', '.@$brand_info[0]['phone_two']; }else{ echo @$brand_info[0]['phone_two']; }
                      ?>
                    </li>
                    <?php }*/ ?>
                  </ul>

                </div>
              </div>
              <div class="ratings-wrp">
                <div class="ratings-label">Brand Rating:
                </div>
                <ul class="ratings rating-<?php echo round(@$avg_ratings); ?>">
                  <li class="star1"></li>
                  <li class="star2"></li>
                  <li class="star3"></li>
                  <li class="star4"></li>
                  <li class="star5"></li>
                </ul>
                <div class="user-likes">
                    <!-- <a href="#myModal" data-toggle="modal" title="Add to Wish List" target="_blank"><i class="ic sc-love"></i></a> -->
                    <?php
                      if($this->session->userdata('user_id')){
                        if($this->home_model->get_fav_or_not('brand',$brand_id) == 0){ ?>
                          <span class="label-add-to-wishlist">Add to Wish List</span>
                          <a href="#" data-toggle="modal" title="Add to Wish List" target="_blank" id="wishlist<?php echo $brand_id; ?>" onclick="add_to_fav('brand','<?php echo $brand_id; ?>','');"><i class="ic sc-love"></i></a>
                        <?php }else{ ?>
                          <span class="label-add-to-wishlist">Remove from Wish List</span>
                          <a href="#" data-toggle="modal" title="Add to Wish List" id="wishlist<?php echo $brand_id; ?>" onclick="add_to_fav('brand','<?php echo $brand_id; ?>','');" target="_blank"><i class="ic sc-love active"></i></a>
                        <?php } }else{ ?>
                        <span class="label-add-to-wishlist">Add to Wish List</span>
                        <a href="#myModal" data-toggle="modal" title="Add to Wish List" target="_blank"><i class="ic sc-love"></i></a>
                      <?php }
                      ?>
                 </div>
              </div>

        </div>

        <ul class="nav nav-tabs common-tabs">
          <li class="active"><a data-toggle="tab" href="#tab-products">Shop <?php echo $this->Product_desc_model->get_brand_name(@$company_name,@$products_data[0]['id']); ?></a></li>
          <li><a data-toggle="tab" href="#tab-about">About <?php echo $this->Product_desc_model->get_brand_name(@$company_name,@$products_data[0]['id']); ?></a></li>
        </ul>

        <div class="tab-content common-tab-content">
          <div id="tab-products" class="tab-pane fade in active">

<?php if(!empty($looks_data)) { ?>
   <div class="grid items-wrp items-section">
         <div class="title-section">
            <h1 class="title"><span>Looks</span></h1>
            <a class="link-view-all" href="<?php echo base_url().'brands/looks/'.$user_name; ?>">View All <i class="fa fa-caret-right"></i></a>
        </div>

          <div class="row grid-items-wrp">
                <?php foreach ($looks_data as $value) { ?>
              <div class="col-md-4 col-sm-6 grid-item">
                <div class="item item-look">
                  <div class="item-img item-hover">
                      <?php
                        $short_url = base_url().'looks/look-details/'.$value['slug'];
                        $img_title_alt = str_replace('#','',$value['name']);

                        if($value['look_type'] == 1){ ?>
                          <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>"  onclick="ga('send', 'event', 'Look', 'clicked', '<?php echo $value['name']; ?>');" ><img src="<?php echo $this->config->item('sc_look_image_url').$value['image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                        <?php $img_url = $this->config->item('sc_look_image_url').$value['image'];
                          }else if($value['look_type'] == 2 || $value['look_type'] == 4 || $value['look_type'] == 6){ ?>
                          <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>"  onclick="ga('send', 'event', 'Look', 'clicked', '<?php echo $value['name']; ?>');" ><img src="<?php echo $this->config->item('sc_street_style_image_url').$value['look_image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                        <?php $img_url = $this->config->item('sc_street_style_image_url').$value['look_image'];
                        }else if($value['look_type'] == 3){ ?>
                        <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>"  onclick="ga('send', 'event', 'Look', 'clicked', '<?php echo $value['name']; ?>');" ><img src="<?php echo $this->config->item('sc_promotional_look_image').$value['product_img']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                        <?php $img_url = $this->config->item('sc_promotional_look_image').$val['product_img'];
                        }
                        ?>

                    <div class="item-look-hover">
                      <a  onclick="ga('send', 'event', 'Product', 'clicked', '<?php echo $value['name']; ?>');fbq('track', 'ViewContent');" href="<?php echo $short_url; ?>" title="<?php echo $img_title_alt; ?>"><span class="btn btn-primary"><?php echo $this->lang->line('get_this'); ?></span></a>
                    </div>
                    </div>
                    <div class="item-desc">
                      <div class="item-title"><a  onclick="ga('send', 'event', 'Product', 'clicked', '<?php echo $value['name']; ?>');fbq('track', 'ViewContent');" title="<?php echo $img_title_alt; ?>" href="<?php echo $short_url; ?>"><?php echo $value['name']; ?></a>
                      </div>
                      <div class="item-media">
                        <ul class="social-buttons">
                          <li><a onclick="_targetFacebookShare('<?php echo $value['name']; ?>','<?php echo $short_url; ?>','<?php echo $img_url; ?>','<?php echo $value['id']; ?>','<?php echo strip_tags($this->seo_title); ?>');"><i class="fa fa-facebook"></i></a></li>
                          <li><a onclick="_targetTweet('<?php echo $short_url; ?>','<?php echo $value['name']; ?>','<?php echo $value['id']; ?>');"><i class="fa fa-twitter"></i></a></li>
                          <li><a onclick="_targetPinterest('<?php echo $short_url; ?>','<?php echo $value['name']; ?>','<?php echo $value['id']; ?>','<?php echo $img_url; ?>');"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                        <?php
                              if($this->session->userdata('user_id')){
                                if($this->home_model->get_look_fav_or_not($value['id']) == 0){ ?>
                                  <div class="user-likes"><a target="_blank" title="Add to Wish List" id="whislist<?php echo $value['id']; ?>" onclick="add_to_wishlist(<?php echo $value['id']; ?>);"><i class="ic sc-love"></i></a></div>
                                <?php }else{ ?>
                                  <div class="user-likes"><a target="_blank" title="Remove from Wish List" id="whislist<?php echo $value['id']; ?>" onclick="remove_from_wishlist(<?php echo $value['id']; ?>);"><i class="ic sc-love active"></i></a></div>
                                <?php } }else{ ?>
                                <div class="user-likes"><a target="_blank" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a></div>
                              <?php }
                              ?>
                        </div>
                      </div>
                    </div>
                  </div>

            <?php } ?>

            </div>
              </div>
              <?php } ?>
              <?php if(!empty($products_data)){ ?>
               <div class="grid items-wrp items-section">
            <div class="title-section">
               <h1 class="title"><span>Products</span></h1>
               <a class="link-view-all" href="<?php echo base_url().'brand/'.$user_name; ?>">View All <i class="fa fa-caret-right"></i></a>
           </div>

             <div class="row grid-items-wrp">
              <?php foreach ($products_data as $value) { ?>
      <div class="col-md-4 col-sm-6 grid-item">
        <div class="item item-product">
          <div class="item-img item-hover">
              <div class="  item-img-inner">

              <?php
                $short_url = base_url().'product/details/'.$value['slug'].'-'.$value['id'];
                $img_title_alt = str_replace('#','',$value['name']);
                $img_url = $this->config->item('products_340').$value['image'];

              ?>

                 <a href="<?php echo base_url(); ?>product/details/<?php echo $value['slug'].'-'.$value['id']; ?>"><img src="<?php echo $this->config->item('products_340').$value['image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
            </div>
            <div class="item-look-hover">
              <a href="<?php echo $short_url; ?>" title="<?php echo $img_title_alt; ?>" onclick="ga('send', 'event', 'Product', 'clicked', '<?php echo $value['name']; ?>');fbq('track', 'ViewContent'); " ><span class="btn btn-primary"><?php echo $this->lang->line('get_this'); ?></span></a>
            </div>

                </div>
            <div class="item-desc">
              <div class="item-title"><a title="<?php echo $img_title_alt; ?>" onclick="ga('send', 'event', 'Product', 'clicked', '<?php echo $value['name']; ?>'); fbq('track', 'ViewContent'); " href="<?php echo $short_url; ?>" ><?php echo $value['name']; ?></a>
              </div>
              <div class="item-price-wrp">
               <!-- <div class="item-price">
                  <?php if($this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price']>0) { ?>
                  <span class="mrp-price text-strike"><i class="fa fa-inr"></i><?php echo $value['price']; ?></span>
                  <span class="price"><i class="fa fa-inr"></i><?php echo $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price'] : ''; ?></span>
                  <span class="discount-price">(<?php echo round($this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discount_percent']); ?>% OFF)</span>
                  <?php }else{ ?>
                  <span class="price"><i class="fa fa-inr"></i> <?php echo $value['price']; ?></span>
                  <?php } ?>
                </div>-->
				<div class="item-price">
                  <?php if($this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price']>0 || $value['compare_price']>$value['price']) { ?>
                  <span class="mrp-price text-strike"><i class="fa fa-inr"></i><?php if($value['compare_price']>$value['price']){ echo $value['compare_price']; }else { echo $value['price']; $value['compare_price'] = $value['price'];} ?></span>
				  <span class="price"><i class="fa fa-inr"></i> <?php $discount_price = $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price'] : ''; if($discount_price > 0){ echo $discount_price; }else{
					  echo $value['price'];
				  } ?></span>
                  <span class="discount-price">(<?php $discount_percent = $this->Product_desc_model->calculate_discount_percent($value['compare_price'],$discount_price,$value['price']); echo $discount_percent; ?>% OFF)</span>
                  <?php }else{ ?>
                  <span class="price"><i class="fa fa-inr"></i> <?php echo $value['price']; ?></span>
                  <?php } ?>
                </div>
                <?php
                      if($this->session->userdata('user_id')){
                        if($this->home_model->get_fav_or_not('product',$value['id']) == 0){ ?>
                          <div class="user-likes"><a target="_blank" title="Add to Wish List" id="wishlist<?php echo $value['id']; ?>" onclick="add_to_fav('product','<?php echo $value['id']; ?>','<?php echo $user_id; ?>','<?php echo $value['price']; ?>','<?php echo $value['name']; ?>','<?php echo $value['product_cat_id']; ?>','add_to_wishlist');"><i class="ic sc-love"></i></a></div>
                        <?php }else{ ?>
                          <div class="user-likes"><a target="_blank" title="Remove from Wish List" id="wishlist<?php echo $value['id']; ?>" onclick="add_to_fav('product',<?php echo $value['id']; ?>,'<?php echo $user_id; ?>','<?php echo $value['price']; ?>','<?php echo $value['name']; ?>','<?php echo $value['product_cat_id']; ?>','remove_from_wishlist');"><i class="ic sc-love active"></i></a></div>
                        <?php } }else{ ?>
                        <div class="user-likes"><a target="_blank" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a></div>
                      <?php }
                      ?>
              </div>
            </div>
          </div>
        </div>
       <?php } ?>
            </div>
  </div> <?php } ?>
          </div>
          <div id="tab-about" class="tab-pane fade">

            <?php  if(@$brand_info[0]['long_description']!=''){ ?>
              <div class="brand-desc-inner">
                <?php echo @$brand_info[0]['long_description']; ?>
              </div>
              <?php } ?>

              <?php if(!$this->session->userdata('user_id')){ ?>
                <div class="write-review-btn-wrp">
                <a class="btn btn-primary" target="blank" data-toggle="modal" href="#myModal">Write a Review</a>

                </div>
                <?php }else{ ?>
                <div class="write-review-section">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="write-review-title">
                        Write a Review
                      </div>
                    </div>
                    <div class="col-md-8">
                      <div class="review-form">
                        <div class="form-group">
                          <textarea name="review_text" id="review_text" class="form-control" placeholder="Please write your comment"></textarea>
                        </div>
                        <div class="user-ratings-selction">
                          <ul class="ratings do-not-del-ratings" id="addratings">
                            <li class="star1" title="1"><input type="radio" value="1" name="ratings" id="ratings"></li>
                            <li class="star2" title="2"><input type="radio" value="2" name="ratings" id="ratings"></li>
                            <li class="star3" title="3"><input type="radio" value="3" name="ratings" id="ratings"></li>
                            <li class="star4" title="4"><input type="radio" value="4" name="ratings" id="ratings"></li>
                            <li class="star5" title="5"><input type="radio" value="5" name="ratings" id="ratings"></li>
                          </ul>
                        </div>
                        <div class="form-group">
                          <div id="review_error"></div>
                        </div>
                        <div class="form-group">
                          <div class="btn btn-primary" name="post_review" id="post_review">Submit</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
        <?php } ?>

      <div class="user-reviews-list-wrp">
        <?php if(!empty($brands_review)) { ?>
            <div class="title-section">
          <h3 class="title"><span>Reviews</span></h3>
        </div>
          <?php } ?>
          <div class="user-reviews-list" id="reviews">
            <?php if(!empty($brands_review)) { ?>
            <?php foreach($brands_review as $val){  ?>
              <div class="user-review">
                <div class="row">
                  <div class="col-md-3">
                <div class="user-info">
                <div class="user-photo"><img src="<?php echo $val['profile_pic']; ?>"></div>

                <div class="user-name-wrp">
                  <div class="user-ratings">
                    <div class="ratings-wrp">
                      <ul class="ratings rating-<?php echo $val['ratings'] ?>">
                        <li class="star1"></li>
                        <li class="star2"></li>
                        <li class="star3"></li>
                        <li class="star4"></li>
                        <li class="star5"></li>
                     </ul>
                    </div>
                  </div>

                <div class="user-name">
                <?php echo @$val['first_name'].' '.@$val['last_name']; ?>
                </div>
                <div class="user-activity">
                <?php echo $val['created_datetime']; ?>
                  </div>
                </div>
                    <div class="clear">
                  </div>
                 </div>
                 </div>
                 <div class="col-md-9">
                  <?php echo strip_tags($val['review_text']); ?>
                  </div>
                  </div>
                </div>


            <?php } } ?>

          </div>
      </div>

    </div> <!--/tab-->

</div>

      </div>

    </div>
</div> <!-- /page -->
            <script>

            function get_reviews(){
              var brand_token = $('#brand_token').val();
              $.ajax({
                url: '<?php echo base_url(); ?>brands/get_reviews',
                type: 'POST',
                data: {'brand_token':brand_token},
                cache :true,
                success: function(response) {

                  $('#reviews').html(response);

                },
                error: function(xhr) {

                }
              });

            }

            function load_more_reviews(){
              var brand_token = $('#brand_token').val();
              $.ajax({
                url: '<?php echo base_url(); ?>brands/get_all_reviews',
                type: 'POST',
                data: {'brand_token':brand_token},
                cache :true,
                success: function(response) {

                  $('#reviews').html(response);

                },
                error: function(xhr) {

                }
              });

            }
            </script>
