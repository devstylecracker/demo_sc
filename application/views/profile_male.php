<div class="profile-top-section">
<div class="container custom">
<div class="profile-banner-wrp">
  <div class="cover-photo-wrp">
    <form method="post" action="" id="cover_img_form" enctype="multipart/form-data">
      <div class="cover-photo-inner"  style="background-image:url('<?php echo @$user_info[0]->profile_cover_pic == '' ? base_url().'assets/images/profile/cover-men.jpg' : $this->config->item('cover_image_path').@$user_info[0]->profile_cover_pic; ?>')">
      <div id="cover_img" class="cover-img"></div>
      </div>
    </form>

    <div class="container">
      <div class="profile-photo-wrp">
        <form method="post" action="" id="upload_file" enctype="multipart/form-data">
        <div class="profile-photo">
            <div class="profile-photo-inner item-hover">
            <?php
              /*Added by Hari for facebook google pic*/
              if($user_info[0]->registered_from == 'facebook'){
                $profilePic = 'https://graph.facebook.com/'.@$user_info[0]->facebook_id.'/picture?type=large';
              }
              elseif($user_info[0]->registered_from == 'google'){
                 $profilePic = @$user_info[0]->profile_pic;

              }
              elseif($user_info[0]->profile_pic == '')
                 $profilePic = base_url().'assets/images/profile/profile-men.jpg';
               else
                $profilePic = $this->config->item('profile_image_path').@$user_info[0]->profile_pic;

            ?>
              <img src="<?php echo $profilePic;?>" id="profile-photo-img">
        </div>
        <div class="btn-file-wrp">
          <span><i class="fa fa-pencil"></i></span>
          <input type="file" id="profile_img" class="btn-file" name="profile_img">
        </div>
        </div>
       </form>
      </div>
    </div>
    </div>
  </div>
</div>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-3">
    </div>
    <div class="col-md-9">
      <div class="profile-info">
        <h1 class="name"><span><?php echo @$user_info[0]->first_name?></span></h1>
        <div class="title">
          <h4 class="pull-left">My Profile:</h4>
            <!--div class="pull-right"><a data-toggle="modal" onclick="HideLoginModal();" href="#myModalResetpwd" class="btn btn-secondary btn-xs">Reset Password</a></div-->
             <div class="gender-outer">
            <div class="gender-wrp">
                <label for="sc_change_gender">Gender</label>
                <select name="sc_change_gender" id="sc_change_gender" class="form-control input-sm">
                  <option value="1" <?php echo $this->session->userdata('gender') == 1 ? 'selected' : ''; ?>>Female</option>
                  <option value="2" <?php echo $this->session->userdata('gender') == 2 ? 'selected' : ''; ?>>Male</option>
                </select>
          </div>
            <a data-toggle="modal" onclick="HideLoginModal();" href="#myModalResetpwd" class="btn btn-secondary btn-xs">Reset Password</a>
          </div>
          <div class="clear">
          </div>
        </div>
        <div class="profile-pa">
          <div class="row seven-cols">
            <div class="col-md-1 col-sm-6 col-xs-6">
                <div class="item-wrp">
                  <div class="pa-name">
                    Body Type
                  </div>
                    <div class="item item-body-type item-hover">
                      <img src="<?php echo base_url(); ?>assets/images/profile/body-type-male.jpg" />
                      <a href="<?php echo base_url().'schome/pa/13'; ?>">
                      <span class="btn-edit-wrp">
                      <i class="fa fa-pencil"></i>
                      </span>
                      </a>
                    </div>
                    <div class="title">
                      <?php echo substr($user_body_shape, 0, strpos($user_body_shape, '<')); ?>
                    </div>
                  </div>
          </div>
          <div class="col-md-1 col-sm-6 col-xs-6">
              <div class="item-wrp">
                <div class="pa-name">
                  Body Height
                </div>
                <div class="item item-body-style item-hover">
                    <img src="<?php echo base_url(); ?>assets/images/profile/height-male.jpg" />
                    <a href="<?php echo base_url().'schome/pa/14'; ?>">
                      <span class="btn-edit-wrp">
                      <i class="fa fa-pencil"></i>
                      </span>
                    </a>
                </div>
                <div class="title">
                  <?php echo substr($user_style, 0, strpos($user_style, '<')); ?>
                </div>
          </div>
        </div>

        <div class="col-md-1 col-sm-6 col-xs-6">
              <div class="item-wrp">
                <div class="pa-name">
                  Work Style
                </div>
            <div class="item item-age-group item-hover">
                <img src="<?php echo base_url(); ?>assets/images/profile/work-style-male.jpg" />
                  <a href="<?php echo base_url().'schome/pa/15'; ?>">
                    <span class="btn-edit-wrp">
                    <i class="fa fa-pencil"></i>
                    </span>
                  </a>
            </div>
            <div class="title">  <?php echo $work_style; ?>
              </div>
            </div>
          </div>
          <div class="col-md-1 col-sm-6 col-xs-6">
              <div class="item-wrp">
                <div class="pa-name">
                  Personal Style
                </div>
            <div class="item item-age-group item-hover">
                <img src="<?php echo base_url(); ?>assets/images/profile/personal-style-male.jpg" />
            <a href="<?php echo base_url().'schome/pa/16'; ?>">
              <span class="btn-edit-wrp">
              <i class="fa fa-pencil"></i>
              </span>
            </a>
            </div>
            <div class="title">  <?php echo $personal_style; ?>
              </div>
            </div>
          </div>

          <div class="col-md-1 col-sm-6 col-xs-6">
              <div class="item-wrp">
                <div class="pa-name">
                    Age
                </div>
            <div class="item item-age-group item-hover">
                <img src="<?php echo base_url(); ?>assets/images/profile/age-male.jpg" />
            <a href="<?php echo base_url().'schome/pa/17'; ?>">
              <span class="btn-edit-wrp">
              <i class="fa fa-pencil"></i>
              </span>
            </a>
            </div>
            <div class="title">  <?php echo $user_age; ?> Years
              </div>
            </div>
          </div>

            <div class="col-md-1 col-sm-6 col-xs-6">
                <div class="item-wrp">
                  <div class="pa-name">
                    Budget
                  </div>
            <div class="item item-budget item-hover">
                <img src="<?php echo base_url(); ?>assets/images/profile/budget-male.jpg" />
            <a href="<?php echo base_url().'schome/pa/18'; ?>">
              <span class="btn-edit-wrp">
              <i class="fa fa-pencil"></i>
              </span>
            </a>
            </div>
            <div class="title">
              <?php echo $user_budget; ?>
            </div>
          </div>
        </div>
          <div class="col-md-1 col-sm-6 col-xs-6">
              <div class="item-wrp">
                <div class="pa-name">
                  Style Quotient
                </div>
            <div class="item item-size item-hover">
                <img src="<?php echo base_url(); ?>assets/images/profile/style-quotient-male.jpg" />
            <a href="<?php echo base_url().'schome/pa/19'; ?>">
               <span class="btn-edit-wrp">
              <i class="fa fa-pencil"></i>
              </span>
            </a>
            </div>
            <div class="title">
              <?php echo $user_size; ?>
            </div>
          </div>
        </div>
      </div><!-- /row-->

      </div>
    </div>
   </div>
  </div>
  </div>

     <div class="container">
        <div class="text-center">
          <div class="btn btn-primary" id="wardrobe_link" ><i class="fa fa-angle-double-down"></i> Wardrobe</div>
        </div>
      </div>
   <div class="page-wardrobe wardrobe-outer hide-wardrobe" style="display:none">
   <div class="wardrobe-wrp-outer">
   <div class="container">
     <div class="wardrobe-wrp">
       <div  class="row">
         <div class="col-md-9 col-md-offset-3">
           <div class="info">
             <div class="title">
               <h4>Wardrobe:</h4>
             </div>
             <p>
              Upload images from your existing wardrobe and we will help you with a stylish fix.
             </p>
             <form method="post" action="" id="frmWordrobe" enctype="multipart/form-data">
               <div class="btn btn-primary btn-file-wrp">
                 Upload Photo
                 <input type="file" id="upload_photoes" class="btn-file" name="upload_photoes[]" multiple onchange="test();">
             </div>

             <div id="wardrobe_loader" class="wardrobe-loader"></div>

             </form>
             <?php if(isset($_POST['upload_photoes'])){ ?>
             <div class="alert alert-success">
               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
               <strong>Success!</strong> File successfully uploaded.
             </div>
             <?php } ?>

           </div>
         </div>
       </div>

      <div class="wardrobe-wrp">
        <div class="grid wardrobe-photos-wrp">
       <div id="wardrobe_images_grid" class="row wardrobe-slider wardrobe-images-grid flexslider_wardrobe">
        </div>
        </div>
      </div>

    </div>
  </div>
  </div>
</div>

<div>
  &nbsp;
</div>

    <!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls wardrobe-popup" data-use-bootstrap-modal="false">
      <!-- The container for the modal slides -->
      <div class="slides"></div>
      <!-- Controls for the borderless lightbox -->

      <h3 class="title"></h3>
      <a class="prev"></a>
      <a class="next"></a>
      <a class="close">×</a>
      <a class="play-pause"></a>
      <ol class="indicator"></ol>
      <!-- The modal dialog, which will be used to wrap the lightbox content -->
     <div class="modal fade  wardrobe-popup-dialog false">
        <div class="modal-dialog">
          <div class="modal-content">
            <div>
              <button type="button" class="close" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body next"></div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary btn-xs pull-left prev">
                <i class="glyphicon glyphicon-chevron-left"></i>
                Previous
              </button>
              <button type="button" class="btn btn-primary btn-xs next">
                Next
                <i class="glyphicon glyphicon-chevron-right"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade signin-signup-popup large" id="myModalResetpwd" tabindex="-1" role="dialog" aria-labelledby="myModalForgotpwdLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
	  <div class="slash-bg"></div>
    <div>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <div class="modal-body signin-signup-body">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6">
            <div class="title">Reset <span>Password</span></div>
          </div>
          <div class="col-md-6">
            <div class="form-outer">
            <form name="resetpwdfrm" id="resetpwdfrm" method="post" action="">
            <div class="form-control-wrp">
              <input type="password" name="oldpwd" id="oldpwd" value="" placeholder="OLD PASSWORD">
              <span id="oldpwd_error"></span>
            </div>
            <div class="form-control-wrp">
              <input type="password" name="newpwd" id="newpwd" value="" placeholder="NEW PASSWORD">
              <span id="newpwd_error"></span>
            </div>
            <div id="loginrp_error" class="form-error error"></div>
            <div class="sign-up-btn-wrp">
              <button  id="sc_rpsubmit" name="sc_rpsubmit" class="btn btn-primary sign-up-btn">Submit</button>
            </div>
            </form>

          </div>
        </div>
        <div class="col-md-1">
        </div>
  </div>
  </div>
  </div>
  </div>
</div>
</div>
 <!--    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/blueimp-gallery.min.css">
    <script src="<?php echo base_url()?>assets/js/jquery.blueimp-gallery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap-image-gallery.min.js"></script>


    <script type="text/Javascript">
    var cache = new ajaxCache('GET', true, 1800000);
    var stylist_offset,street_offset;
    function test(){ $('#frmWordrobe').submit(); }
    $(document).ready(function(){
        $('#frmWordrobe').submit(function(e) {
          $('#wardrobe_loader').css('display','inline-block');
           //e.preventDefault();
          $.ajaxFileUpload({
            url       :'<?php echo base_url(); ?>profile/upload_wordrobe/',
            fileElementId :'upload_photoes',
            dataType    : 'json',
            success : function (data, status)
            {
              if(data.status == 'success'){
              //  location.reload();
              wardrobe_images_grid();
              $('#wardrobe_loader').hide();
              }else{

              }

            }
          });

          e.stopPropagation();
          return false;
        });



    });

    $(function() {
      wardrobe_images_grid();


    /* document.getElementById('wardrobe_images_grid').getElementsByTagName('a').onclick = function (event) { alert('hi');
      event = event || window.event;
      var target = event.target || event.srcElement,
        link = target.src ? target.parentNode : target,
        options = {index: link, event: event},
        links = this.getElementsByTagName('a');
        blueimp.Gallery(links, options);
      };
*/
      var all_offset = "stylist_offset=0&street_offset=0&filter=0&page_type=wishlist";

      get_looks(all_offset);

      $( "#select_type" ).change(function() {
        $('#show_looks').html('');
        $('#show_looks').html('<div class="grid-sizer"></div>');

        all_offset = "stylist_offset=0&street_offset=0&page_type=wishlist&filter="+$('#select_type').val();

        get_looks(all_offset);
      });

      $( window ).load( function(){
      /*  $('.grid-items-wrp').masonry({
          // set itemSelector so .grid-sizer is not used in layout
          itemSelector: '.grid-item',
          // use element for option
          // columnWidth: '.grid-sizer',
          percentPosition: true
        });*/
      });

      $("#wardrobe_link").click(function(){
    //  alert("dfds");
        if ( $(".wardrobe-outer").hasClass( "hide-wardrobe" ) ) {
            $(".wardrobe-outer").slideDown('slow');
            $(".wardrobe-outer").addClass('show-wardrobe').removeClass('hide-wardrobe');
        }
        else {
            $(".wardrobe-outer").slideUp('slow');
              $(".wardrobe-outer").addClass('hide-wardrobe').removeClass('show-wardrobe');
        }
      });

      $('#upload_file').submit(function(e) {
        e.preventDefault();
        $.ajaxFileUpload({
          url       :'<?php echo base_url(); ?>profile/upload_profile/',
          fileElementId :'profile_img',
          dataType    : 'json',
          success : function (data, status)
          {
            if(status == 'success'){
              location.reload();
            }else{

            }

          }
        });
        return false;
      });

      $('#cover_img_form').submit(function(e) {
        e.preventDefault();
        $.ajaxFileUpload({
          url       :'<?php echo base_url(); ?>profile/cover_image/',
          fileElementId :'cover_img',
          dataType    : 'json',
          success : function (data, status)
          {
            if(data.status == 'success'){
              location.reload();
            }else{
              alert('error');
            }

          },error: function(xhr) {
            console.log(xhr);
          }
        });
        //return false;
      });


      $('#profile_img').change(function(e){
        $('#upload_file').submit();
      });

      /*$('#cover_img').change(function(e){
        //$('#cover_img_form').submit();
      });*/


        //wardrobe_images_grid();

      $('#profile_img_mobile').change(function(e){
        $('#upload_file').submit();
      });

    });

    var isTriggered = false;
    $(window).scroll(function() {
      var intTriggerScroll = 50;
      var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());

      if(intCurrentScrollPos<intTriggerScroll && !isTriggered){


        if($('#stylist_offset').length > 0){
          stylist_offset = $('#stylist_offset').val();
        }else{
          stylist_offset = '-1';
        }

        if($('#street_offset').length > 0){
          street_offset = $('#street_offset').val();
        }else{
          street_offset = '-1';
        }

        all_offset = "stylist_offset="+stylist_offset+"&street_offset="+street_offset+"&page_type=wishlist&filter="+$('#select_type').val();
        isTriggered=true;

        if($('#stylist_offset').length >0 || $('#street_offset').length >0){
          get_looks(all_offset);
        }
      }

    });


    function get_looks(viewData)
    {
      var ajax_path = '<?php echo base_url(); ?>Schome/get_all_wishlist_looks';
      $('#look_loader_img').css('display', 'inline-block');
      if(cache.on)
      {
        var cachedResponse = cache.get(ajax_path, [viewData]);
        if(cachedResponse !== false)
        {
          // update website
          update_website(cachedResponse); // We just avoided one ajax request
          initialiceMasonry();
          add_hover();
          return true;
        }
      }

      $.ajax({
        url: ajax_path,
        type: 'GET',
        data: viewData,
        cache :true,
        success: function(response) {
          if(cache.on) cache.put(ajax_path, viewData, response); // record the new response

          update_website(response);
          initialiceMasonry();
          add_hover();
        },
        error: function(xhr) {

        }
      });

      return true;
    }

    function update_website(response){

      if($('#stylist_offset').length >0 ){ $('#stylist_offset').remove(); }
      if($('#street_offset').length >0 ){ $('#street_offset').remove(); }

      $('#show_looks').append(response);
      $('#look_loader_img').hide();
      isTriggered = false;

    }

 function initialiceMasonry(){
          var $container = $('.grid-items-wrp');
            $container.masonry({
                isInitLayout : true,
                itemSelector: '.grid-item',
                isAnimated: false,
                percentPosition: true

            });

            $container.imagesLoaded(function() {
              $( $container ).masonry( 'reloadItems' );
              $( $container ).masonry( 'layout' );
            });
          }

    function wardrobe_images_grid(){
     $.ajax({
        url: '<?php echo base_url(); ?>profile/display_wardrobe',
        type: 'GET',
        cache :true,
        success: function(response) {
          $('.wardrobe-slider').html(response);

        ///  });
          /*$('.flexslider_wardrobe').flexslider({
            selector: ".item-img > a",
            animation: "slide",
            animationLoop: true,
            itemWidth: 100,
            smoothHeight:true,
            maxItems: 4,
            prevText: "Previous",
            nextText: "Next",
            start: function(slider){
                slider.resize();
            }

            });*/
        },
        error: function(xhr) {

        }
      });
    }

    function delete_wardrobe(id){
      if (confirm("Are you sure?")) {
       $.ajax({
        url: '<?php echo base_url(); ?>profile/delete_wardrobe_img',
        type: 'POST',
        data: {'id': id },
        cache :true,
        success: function(response) {
          wardrobe_images_grid();
        },
        error: function(xhr) {

        }
      });
     }
    }

    function remove_from__wishlist(look_id){
    $('#whislist'+look_id).attr("onclick","add_to_wishlist("+look_id+")");
    $('#whislist'+look_id).find('i').removeClass('active');
    $('#'+look_id).remove();
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host;

      $.ajax({
        url: baseUrl+"/profile/remove_from_wishlist",
        type: 'POST',
        data: {'look_id':look_id},
        cache :true,
        success: function(response) {

          initialiceMasonry();

        },
        error: function(xhr) {

        }
      });

  }

   function sc_resetpwd(){

    var oldpwd = $('#oldpwd').val();
    var newpwd = $('#newpwd').val();

    if(oldpwd!='' && newpwd!=''){

      $.ajax({
        url: '<?php echo base_url(); ?>schome/resetPassword',
        type: 'post',
        data: {'type' : 'reset_pwd','oldpwd':oldpwd,'newpwd':newpwd },
        success: function(data, status) {

          $('#loginrp_error').html(data);
          return false;
          event.preventDefault();
        },
        error: function(xhr, desc, err) {
          // console.log(err);
        }
      });
    }
 }
    </script>


  <script>
$(document).ready(function() {
  //## UserTrack: Page ID=========================================
  document.cookie="pid=9";
  /*Code ends*/

});
  </script>
 -->
