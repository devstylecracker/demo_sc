<script type="text/javascript">var baseurl = '<?php echo base_url(); ?>';</script>
<script src="<?php echo base_url(); ?>/assets/js/pa_mens.js"></script>
<link href="<?php echo base_url(); ?>/assets/css/range-slider.css" rel="stylesheet">
<?php
$next_tab = 0;
  if($question_resume_id && $edited_data==''){
    if($question_resume_id ==13){
      $next_tab = 1;
    }else if($question_resume_id ==14){
      $next_tab = 2;
    }else if($question_resume_id ==15){
      $next_tab = 3;
    }else if($question_resume_id ==16){
      $next_tab = 4;
    }else if($question_resume_id ==17){
      $next_tab = 5;
    }else if($question_resume_id ==18){
      $next_tab = 6;
    }
  }else if($question_resume_id && $edited_data=='edit'){
    if($question_resume_id ==13){
      $next_tab = 0;
    }else if($question_resume_id ==14){
      $next_tab = 1;
    }else if($question_resume_id ==15){
      $next_tab = 2;
    }else if($question_resume_id ==16){
      $next_tab = 3;
    }else if($question_resume_id ==17){
      $next_tab = 4;
    }else if($question_resume_id ==18){
      $next_tab = 5;
    }else if($question_resume_id ==19){
      $next_tab = 6;
    }

  }

?>
  <div class="page-pa content-wrp">
    <div class="container">
      <div id="pa-men-slider" class="pa-slider">

      <div class="pa-selection-wrp pa-men-selection-wrp pa-nav">
        <ul>
          <li class="control-nav disabled" data-link="slide1">
            <span class="caret"></span>
            Body Type
          </li>
          <li class="control-nav disabled" data-link="slide2">
            <span class="caret"></span>
            Body Height
          </li>
          <li class="control-nav disabled" data-link="slide3">
            <span class="caret"></span>
            Work Style
          </li>
          <li class="control-nav disabled" data-link="slide4">
            <span class="caret"></span>
            Personal Style
          </li>
          <li class="control-nav disabled" data-link="slide5">
            <span class="caret"></span>
            Age
          </li>
          <li class="control-nav disabled" data-link="slide6">
            <span class="caret"></span>
            Budget
          </li>
          <li class="control-nav disabled" data-link="slide7">
            <span class="caret"></span>
            Style Quotient
          </li>
        </ul>
      </div>

<?php if(!empty($body_shape)){ ?>
      <div id="slide1" class="slide body-types-wrp active">
        <div class="pa-title">
          <h2>What is your <span>Body Type</span>?</h2>
        </div>
          <div class="row">
          <?php  foreach($body_shape as $val){ ?>
            <div class="col-md-4 col-sm-12 col-xs-12">
              <div class="item-wrp  <?php if(isset($user_body_shape) && @$user_body_shape[0]['answer_id']==$val['id']){ echo 'selected'; } ?>" data-attr="<?php echo $val['id']; ?>" data-attr2="1" data-attr3="<?php $answer = strtok($val['answer'], " "); echo $answer; ?>">
              <div class="item">
                <div class="content-inner">
                    <div class="name">
                      <?php echo $val['answer']; ?>
                    </div>
                </div>
                <div class="title">
                  <?php echo $val['description']; ?>
                </div>
              </div>
            </div>
            </div>
            <?php } ?>
          </div>
        </div>
<?php } ?>

<?php if(!empty($style)){ ?>
             <div  id="slide2" class="slide body-height-wrp">
               <div class="pa-title">
                 <h2>What is your <span> Height</span>?</h2>
               </div>
            <div class="row">
            <?php foreach($style as $val){ ?>
              <div class="col-md-4 col-sm-12 col-xs-12">
              <div class="item-wrp <?php if(isset($user_style) && @$user_style[0]['answer_id']==$val['id']){ echo 'selected'; } ?>" data-attr="<?php echo $val['id']; ?>" data-attr2="2" data-attr3="<?php $answer = strtok($val['answer'], " "); echo $answer; ?>">
                <div class="item">
                    <div class="content-inner">
                        <div class="name">
                          <?php echo $val['answer']; ?>
                          </div>
                      </div>
                      <div class="title">
                        <?php echo $val['description']; ?>
                      </div>
                  </div>
              </div>
              </div>
             <?php } ?>
            </div>
          </div>
<?php } ?>

<?php if(!empty($age)){ ?>
          <div id="slide3" class="slide work-style-wrp">
            <div class="pa-title">
              <h2>Pick an outfit for <span>WORK:</span></h2>
            </div>
              <div class="row">
              <?php foreach($age as $val) { ?>
                <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="item-wrp <?php if(isset($user_age) && @$user_age[0]['answer_id']==$val['id']){ echo 'selected'; } ?>" data-attr="<?php echo $val['id']; ?>" data-attr2="3" data-attr3="<?php echo $val['answer']; ?>">
                  <div class="item">
                      <div class="item-inner">
                      <!-- <img src="<?php echo base_url(); ?>assets/images/pa/male/work-style-casual.jpg"> -->
                    <?php if(isset($val['image_name'])){ ?>
                      <img src="<?php echo base_url(); ?>assets/images/pa/male/<?php echo $val['image_name']; ?>">
                    <?php } ?>
                      </div>
                    <div class="title">
                      <?php echo $val['answer']; ?>
                    </div>

                  </div>
                    <div class="desc">
                      <p>
                       <?php echo $val['description']; ?>
                      </p>
                    </div>
                </div>
              </div>
              <?php } ?>
              </div>
            </div>
<?php } ?>

<?php if(!empty($personal_style)){ ?>
             <div id="slide4" class="slide personal-style-wrp">
              <div class="pa-title">
                <h2>Pick your <span>Personal Style:</span></h2>
                </div>
                <div class="row">
                <?php foreach($personal_style as $val) { ?>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                  <div class="item-wrp <?php if(isset($user_personal_style) && @$user_personal_style[0]['answer_id']==$val['id']){ echo 'selected'; } ?>" data-attr="<?php echo $val['id']; ?>" data-attr2="4" data-attr3="<?php echo $val['answer']; ?>">
                    <div class="item">
                      <div class="item-inner">
                      <!-- <img src="<?php echo base_url(); ?>assets/images/pa/male/work-style-office.jpg"> -->
                      <?php if(isset($val['image_name'])){ ?>
                        <img src="<?php echo base_url(); ?>assets/images/pa/male/<?php echo $val['image_name']; ?>">
                      <?php } ?>
                      </div>
                      <div class="title">
                        <?php echo $val['answer']; ?>
                      </div>
                    </div>
                     <div class="desc">
                      <p>
                       <?php echo $val['description']; ?>
                      </p>
                    </div>
                  </div>
                  </div>
                <?php } ?>
                </div>
              </div>

<?php } ?>

<?php if(!empty($size)){ ?>
               <div id="slide5" class="slide age-group-wrp">
                <div class="pa-title">
                  <h2>Define your <span>Age group:</span> </h2>
                  </div>

                  <div class="row five-cols">
                  <?php foreach($size as $val) { ?>
                    <div class="col-md-1">
                      <div class="item-wrp <?php if(isset($user_size) && @$user_size[0]['answer_id']==$val['id']){ echo 'selected'; } ?>" data-attr="<?php echo $val['id']; ?>" data-attr2="5" data-attr3="<?php echo $val['answer']; ?>">
                      <div class="item">
                           <div class="content-inner">
                            <div class="name">
                              <?php echo $val['answer']; ?> <div>years</div>
                              </div>
                            </div>
                      </div>
                    </div>
                  </div>
                  <?php } ?>

                  </div>
                </div>
<?php } ?>

<?php if(!empty($budget)){ ?>
               <div id="slide6" class="slide budget-wrp">
                <div class="pa-title">
                  <h2>How much do you spend on a <span>Shopping Spree</span>? </h2>
                  </div>

                  <div class="row">
                  <?php foreach($budget as $val) { ?>
                    <div class="col-md-3 col-sm-12 col-xs-12">
                      <div class="item-wrp <?php if(isset($user_budget) && @$user_budget[0]['answer_id']==$val['id']){ echo 'selected'; } ?>" data-attr="<?php echo $val['id']; ?>" data-attr2="6" data-attr3="<?php echo $val['answer']; ?>">
                      <div class="item">
                      <div class="content-inner">
                       <div class="name">
                              <?php echo $val['answer']; ?>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <?php } ?>

                  </div>
                </div>
<?php } ?>

<?php if(!empty($quotient)){ ?>
               <div id="slide7" class="slide style-quotient-wrp">
                <div class="pa-title">
                  <h2>What does your <span>Wardrobe</span>  consist of?</h2>
                  </div>

                  <div class="style-quotient-box">
                  <?php
                    $i = 0;
                    foreach($quotient as $val) { ?>
                      <div class="item-box-wrp" data-attr="<?php //echo $val['id']; ?>">
                        <div class="row">
                          <div class="col-md-2">
                          </div>
                      <div class="col-md-4 col-sm-4">
                      <div class="title">
                        <?php echo $val['q']; ?>
                      </div>
                      </div>
                      <div class="col-md-4 col-sm-4">
                        <div class="range-slider-wrp">
                          <?php
                          $ans = "user_quotient".($i+1);
                          $ans = $$ans;
                          if(count($ans)> 0 && $ans[0]['answer_id'] == 4692) $ans[0]['answer_id'] =0; else  if(count($ans)> 0 && $ans[0]['answer_id'] == 4694) $ans[0]['answer_id']=2; else $ans[0]['answer_id']=1;
                          ?>
                          <input type='range' name="quotient_<?php echo $i;?>" id="quotient_<?php echo $i++;?>" min='0' max='2' value='<?php echo $ans[0]['answer_id']; ?>' step='1' class="range-slider" />
                          <ul class="range-slider-labels">
                            <li>
                              <span>Low</span>
                            </li>
                            <li class="active">
                              <span>Medium</span>
                            </li>
                            <li>
                              <span>High</span>
                            </li>
                          </ul>
                        <!--  <input type="range" name="quotient1" id="quotient1" value="3" min="1" max="3">-->
                        </div>
                      </div>
                        <div class="col-md-2">
                        </div>
                    </div>
                  </div>
                  <?php } ?>
                  <div class="box-footer finish">
                        <div class="btn btn-primary btn-block">
                        Get Cracking >>
                        </div>
                  </div>
                </div>
                </div>
<?php } ?>


  </div>
  </div>
  </div>
<?php if($this->session->userdata('campaign') == 'yes'){ ?>
<!-- Google Code for Promo Conversions Conversion Page -->
<script type="text/javascript">

/* <![CDATA[ */

var google_conversion_id = 944683658;

var google_conversion_language = "en";

var google_conversion_format = "3";

var google_conversion_color = "ffffff";

var google_conversion_label = "g2_tCJG6mmAQivW6wgM";
var google_remarketing_only = false;

/* ]]> */

</script>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>

<noscript>

<div style="display:inline;">

<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/944683658/?label=g2_tCJG6mmAQivW6wgM&amp;guid=ON&amp;script=0"/>

</div>

</noscript>




<?php } ?>

<script type="text/javascript">
$( document ).ready(function() {
  ga('send', 'event', 'Sign Up', 'completed');
  <?php if(isset($_GET['ga'])){echo "ga('send', 'event', 'Login', 'source', 'gplus', '".$this->session->userdata('user_id')."');";} ?>
        //## UserTrack: Page ID=========================================
        document.cookie="pid=7";
        /*Code ends*/
  <?php if(empty($user_body_shape)){ ?>
    PAInitialize1(0,'true','','');
    //console.log("ONE");
  <?php }else{ if($edited_data == 'edit'){ ?>
    localStorage.setItem("edit_pa", "yes");
    PAInitialize1('<?php echo $next_tab; ?>','false','');
    <?php }else{ ?>//console.log('no edit');
    ga('send', 'event', 'PA-Men', 'start');
    PAInitialize1('<?php echo $next_tab; ?>','false','');
    <?php } ?>
    //console.log("TWO");
  <?php } ?>

});
</script>
