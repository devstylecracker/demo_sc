<?php
 if($this->session->userdata('user_id')) redirect('feed','refresh');
?>
<div class="page-promo-signup">
<div class="page-header">
  <div class="container">
    <h1>StyleCracker is India's only personalized styling platform.</h1>
    <h2>We transform the way you shop. <span>Sign Up</span> for free and get styled.</h2>
  </div>
</div>
<div class="content-wrp promo-signup promo-signup1">
  <div class="container">

      <div class="row">
        <div class="col-md-7 col-sm-7">
          <div class="ytube-video-wrp">
          <iframe width="645" height="363" src="https://www.youtube.com/embed/3-U5kufNk7A?rel=0" frameborder="0" allowfullscreen></iframe>
        </div>
        <!--  <img src="<?php echo base_url(); ?>assets/images/promo/promo-signup-video.jpg">-->
        </div>

        <div class="col-md-5 col-sm-5">
          <div class="form-outer">
          <form name ="sc_Signuppromo1" id="sc_Signuppromo1" method="post" action="">
              <div class="form-control-wrp">
               <input type="text" name="sc_firstnamesc"  id="sc_firstnamesc" value="" placeholder="First Name" class="form-input111 form-control" >
               <label id="sc_firstnamesc_error1" class="error" ></label>
              </div>
              <div class="form-control-wrp">
               <input type="text" name="sc_lastnamesc" id="sc_lastnamesc" value="" placeholder="Last Name" class="form-input111 form-control" >
               <label  id="sc_lastnamesc_error" class="error" ></label>
              </div>
              <!--  <div class="form-control-wrp">
                  <input type="text" name="sc_username_promo" id="sc_username_promo" class="form-input111 form-control" value="" placeholder="USERNAME" />
                  <span id=""></span>
                </div>-->              
              <div class="form-control-wrp">
                <input type="text" name="sc_emailid_promo" id="sc_emailid_promo" class="form-inpu111 form-control" value="" placeholder="Email" />
                <label id="sc_emailid_promo_error" class="error" ></label>
              </div>
              <div class="form-control-wrp">
                <input type="password" name="sc_password_promo" id="sc_password_promo" class="form-input111 form-control" value="" placeholder="Password " />
                <label id="sc_password_promo_error" class="error" ></label>
                <!-- <div id="" class="form-error error"></div> -->
              </div>
              <div class="form-control-wrp">                      
                  <input class="form-control" type="text" name="sc_mobile_promo" id="sc_mobile_promo" value="" placeholder="Mobile">
                  <span id="sc_mobile_error"></span>                     
               </div>
              <div class="form-control-wrp">
                <select class="form-control" id="sc_gender" name="sc_gender">
                  <option disabled="" value="" selected>
                      Select Gender
                      </option>
                      <option  value="1">
                      Female
                    </option>
                      <option value="2">
                      Male
                    </option>
                  </select>
                   <label id="sc_gender_error" class="error" ></label>
              </div>
               

          <!--  <div class="text-line">
            By clicking 'SIGN UP' you confirm that you have read and accepted our Terms and Conditions, Privacy Policy and Cookie Policy.
          </div>-->

       <div class="sign-up-btn-wrp">
        <button type="submit" id="sc_signuppromo11" name="sc_signuppromo11" class="btn btn-primary btn-block">Sign Up</button>
       </div>
       <div style="padding:0px;margin:0px;margin-left: 30px;font-size: 17px;" class="fa-loader-wrp-products">
          <i class="fa fa-circle-o-notch fa-spin-custom"></i>
        </div>
       <div id="sc_signup_error" class="error"></div>
       <div id="sc_signup_msg" class="success"></div>

    </form>
    </div>
    </div>
    </div>
  </div>
  </div>
</div>
<style>
.form-control-wrp{position: relative;}
.form-control-wrp label.error{position: absolute; top:100%;}
</style>

  <script type="text/Javascript">

	function sc_signInp(){

		var sc_emailid = $('#sc_emailid_promo').val();
		var sc_password = $('#sc_password_promo').val();
		var sc_username = $('#sc_username_promo').val();
    var sc_firstnamesc = $('#sc_firstnamesc').val();
    var sc_lastnamesc = $('#sc_lastnamesc').val();
    var sc_gender = $('#sc_gender').val();
    var sc_mobile = $('#sc_mobile_promo').val();
    $("#sc_signuppromo11").addClass( "disabled" );
    $(".fa-loader-wrp-products").css({"display":"inline"});

     var pathArray = window.location.search;
     var scevent = '';
      if(pathArray == '?event=AambyPun')
      {
        scevent = 'AambyPun';
      }

	  if(sc_emailid!='' && sc_password!='' && sc_username!='' && sc_firstnamesc!='' && sc_lastnamesc!='' && sc_gender!='' && sc_mobile_promo!=''){
		  $.ajax({
			url: '<?php echo base_url(); ?>Schome/register_user',
			type: 'post',
			data: {'type' : 'get_signin','sc_firstname':sc_firstnamesc,'sc_lastname':sc_lastnamesc,'sc_gender':sc_gender,'sc_email_id':sc_emailid,'sc_password':sc_password,'sc_username_name':sc_username,'registered_from':'comp', 'sc_event': scevent, 'sc_mobile':sc_mobile },
			success: function(data, status) {
			  if(data!=''){
  				$('#sc_signup_error').html(data);
          $("#sc_signuppromo11").removeClass( "disabled" );
          $(".fa-loader-wrp-products").css({"display":"none"});
			  }else{
          $('#sc_signup_error').html('');
           $('#sc_signup_msg').html('Sign-Up Successful');
				   window.location.href="<?php echo base_url(); ?>Schome/pa";
			  }

			  return false;
			  event.preventDefault();

			},
			error: function(xhr, desc, err) {
			  // console.log(err);
			}
		  });

		}

	}


  </script>
