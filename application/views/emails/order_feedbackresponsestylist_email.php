<style>
/*.page-scbox-form .list-prints li{width:180px; margin: 10px 15px;}
.page-scbox-form .list-prints li .pallet {height:180px;}
*/
.page-scbox-form .list-skin-tone li .title-3{padding-top: 15px;}
@media(max-width:768px){
  /*
.page-scbox-form .list-prints li{width:100px;}
.page-scbox-form .list-prints li .pallet {height:100px; width:100px;}*/

.page-scbox-form ul.list-skin-tone li { width: 60px;}
}
.control {
    display: block;
    position: relative;
    padding-left: 30px;
    margin-bottom: 0px;
    padding-top: 3px;
    cursor: pointer;
    font-size: 14px;
    color: #777;
    font-weight: 400;
}
.title-2{
    color: #777 !important;
    font-weight: 400 !important;
    margin-bottom: 5px !important;
    text-transform: none; !important;
}
.form-group-md .textarea1{
     text-transform: capitalize !important;
     padding-top: 16px !important;
}
    .control input {
        position: absolute;
        z-index: -1;
        opacity: 0;
    }
.control_indicator {
    position: absolute;
    top: 1px;
    left: 0;
    height: 20px;
    width: 20px;
    background: #DCDCDC;
}
.control-radio .control_indicator {
    border-radius: 50%;
}

.control:hover input ~ .control_indicator,
.control input:focus ~ .control_indicator {
    background: #13D792;
}

.control input:checked ~ .control_indicator {
    background: #13D792;
}
.control:hover input:not([disabled]):checked ~ .control_indicator,
.control input:checked:focus ~ .control_indicator {
    background: #13D792;
}
.control input:disabled ~ .control_indicator {
    background: #e6e6e6;
    opacity: 0.6;
    pointer-events: none;
}
.control_indicator:after {
    box-sizing: unset;
    content: '';
    position: absolute;
    display: none;
}
.control input:checked ~ .control_indicator:after {
    display: block;
}
.control-radio .control_indicator:after {
    left: 7px;
    top: 7px;
    height: 6px;
    width: 6px;
    border-radius: 50%;
    background: #13D792;
}

}
</style>

<?php 
  $feedback_ques = unserialize(FEEDBACK_QUESTIONS);
  //echo '<pre>';print_r($user_response);
 // echo @$success;
?>
  <section class="section section-shadow page-cart">
    <div class="container" id="">            
           <h1 class="title" style="text-align: center;">FEEDBACK</h1>
    <form name="feedback" id="feedback" method="post">
     <div class="box-cart-summary" id="">            
                <div class="box-body row"><?php echo 'Feedback from : '.$Username;?>
                    <div class="col-md-12 col-sm-12 col-xs-12">

<?php
      foreach($feedback_ques as $val)
      {  
          $extremely_dissatisfied = '';
          $dissatisfied = '';
          $average = '';
          $satisfied = '';
          $extremely_satisfied = '';
          $none = '';      
        if($val->is_show==1)
        {
           if(isset($user_response[$val->name]))
           {
              if($user_response[$val->name]=='extremely_dissatisfied')
              {            
                $extremely_dissatisfied = 'checked="checked"';
              }else if($user_response[$val->name]=='dissatisfied')
              {
                $dissatisfied = 'checked="checked"';
              }else if($user_response[$val->name]=='average')
              {
                $average = 'checked="checked"';
              }else if($user_response[$val->name]=='satisfied')
              {
                $satisfied = 'checked="checked"';
              }else if($user_response[$val->name]=='extremely_satisfied')
              {
                $extremely_satisfied = 'checked="checked"';
              }else if($user_response[$val->name]=='none')
              {
                $none = 'checked="checked"';
              }
          
           }

              
?>
                         <div class="row" style="padding-top: 30px;">
                                <div class="col-md-7 col-sm-12 col-xs-12">
                                        <div class="title-2" style="padding-bottom: 2px;"><?php echo $val->question; ?></div>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12">        
                                       <div class="control-group">
                                           <?php echo ucfirst(str_replace('_',' ',@$user_response[$val->name])); ?>                                            
                                        </div>
                                 </div>        
                             </div>
<?php
  }}
?>
                             <div class="row" style="padding-top: 30px;">
                                <div class="col-md-7 col-sm-12 col-xs-12">
                                        <div class="title-2" style="padding-bottom: 2px;">Return process (if applicable)</div>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12">        
                                       <div class="control-group">
                                           <?php echo ucfirst(str_replace('_',' ',$user_response['returnp'])); ?>                                         
                                        </div>
                                 </div>       
                             </div>
                             <div class="row" style="padding-top: 30px;">
                                <div class="col-md-7 col-sm-12 col-xs-12">
                                        <div class="title-2" style="padding-bottom: 2px;">Refund process (if applicable)</div>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12">        
                                       <div class="control-group">
                                           <?php echo ucfirst(str_replace('_',' ',$user_response['refundp'])); ?>                                           
                                        </div>
                                 </div>       
                             </div>
                             <div class="row" style="padding-top: 30px;">
                                <div class="col-md-7 col-sm-12 col-xs-12">
                                        <div class="title-2" style="padding-bottom: 2px;">Would you order an SC Box again?</div>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12">        
                                       <div class="control-group">
                                            <?php echo ucfirst(str_replace('_',' ',$user_response['orderagain'])); ?>                                           
                                        </div>
                                 </div>       
                             </div>
                             <div class="row" style="padding-top: 30px;">
                                <div class="col-md-7 col-sm-12 col-xs-12">
                                        <div class="title-2" style="padding-bottom: 2px;">Would you recommend the SC Box to your friends and family?</div>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12">        
                                       <div class="control-group">
                                          <?php echo ucfirst(str_replace('_',' ',$user_response['recommend'])); ?>                                          
                                        </div>
                                 </div>       
                             </div>
                              
                              <div class="row" style="padding-top: 30px;">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                 <div class="form-group-md">                                         
                                          <div class="title-2" style="padding-bottom: 2px;">Did the stylist go out of the way to make your experience a little extra pleasurable? If yes, please let us know.</div><br/>
                                           <?php echo ucfirst(str_replace('_',' ',$user_response['stylistexperience'])); ?>                                        
                                </div>
                                      
                             </div>
                             </div>
                                <div class="row" style="padding-top: 30px;">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                 <div class="form-group-md">
                                           <div class="title-2" style="padding-bottom: 2px;">Is there anything we could do in the entire process to further enhance your experience?.</div><br/>
                                           <?php echo ucfirst(str_replace('_',' ',$user_response['processexperience'])); ?>
                                        
                                </div>
                                      
                             </div>
                             </div>
                             <div class="row" style="padding-top: 30px;">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                 <div class="form-group-md">                                        
                                    <div class="title-2" style="padding-bottom: 2px;">Any other feedback</div><br/>
                                     <?php echo ucfirst(str_replace('_',' ',$user_response['anyfeedback'])); ?>                                        
                                </div>
                                      
                             </div>
                             </div>
                    </div>
                </div>  
        </div>
       </form>
    </div>
</section>
