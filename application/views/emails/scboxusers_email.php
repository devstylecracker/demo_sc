<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Feedback</title>

</head>
<body style="background:#f3f3f3; color:#333333; text-align:left; font-family:Arial,Helvetica,sans-serif; font-weight: normal; line-height: 1.5;
font-size: 14px; padding:0; margin:0 auto;">
<table style="width:600px;background:#fff;margin-bottom:20px; font-family:Arial,Helvetica,sans-serif;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
          <tbody>
            <tr>
              <td style="text-align:left;border-bottom:3px solid #00b19c;padding:20px">
                <table style="width:100%; font-family:Arial,Helvetica,sans-serif;" align="center" border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="text-align:left"> <img src="https://www.stylecracker.com/assets/images/logo.png" style="min-height:34px"> </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td style="padding:10px">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style=" font-family:Arial,Helvetica,sans-serif;">
                  <tbody>                  
                    <tr>
                       <td style="padding:0px 30px">
                        <div><strong style="font-size: 16px;">Hey there!</strong><br/><br/>
                        Thank you so much for having purchased a <strong style="color: #00b19c;">StyleCracker Box</strong> in the past. As you know, this is the easiest and most fun way to shop where you get styled by the best in the business at the click of a button.<br> We wanted to let you know that we are running a special offer where you will get<br><br><center> <strong style="color: #00b19c;border:1px dashed #00b19c;padding: 5px;margin: 5px;font-size: 18px;"> 50% OFF ANY StyleCracker Box you order. </strong></center> <br/><br/>
                        This offer is only valid for a limited time so we suggest you get shopping!<br><br>
                         All you need to do is visit<a href="https://www.stylecracker.com/" style="text-decoration: none;color: #00b19c;"> <strong> WWW.STYLECRACKER.COM </strong> </a> and your discount will be auto-applied on checkout.<br/><br/>
                         </div>
                       </td>
                    </tr>
                    <tr>
                        <td style="padding:0px 30px 30px;"><div>Have a great day!!</div></td>
                    </tr> 
                    <tr>
                      <td style="padding:0px 30px">
                        <div style="border-bottom:1px solid #ccc;padding-bottom:30px">The StyleCracker Team<br>
                          For any query or assistance, feel free to <a href="mailto:customercare@stylecracker.com" style="color:#00b19c;text-decoration:none" target="_top">Contact
                            Us</a> </div>
                      </td>
                    </tr>                   
                  </tbody>
                </table>               
              </td>
            </tr>           
            <tr>
              <td style="vertical-align:middle;padding:20px 40px"> <span style="color:#888;font-weight:bold;line-height:1.3;vertical-align:top"> Follow Us: </span> <a href="https://www.facebook.com/StyleCracker" title="Facebook" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/facebook.png" alt="Facebook" style="width:24px"></a> <a href="https://twitter.com/Style_Cracker" title="Twitter" target="_blank">
                <img src="https://www.stylecracker.com/assets/images/social-media/twitter.png" alt="Twitter" style="width:24px"></a>
                <a href="https://www.youtube.com/user/StyleCrackerTV" title="Youtube" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/youtube.png" alt="Youtube" style="width:24px"></a> <a href="https://instagram.com/stylecracker" title="Instagram" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/instagram.png" alt="Instagram" style="width:24px"></a>
                 <a href="https://www.pinterest.com/stylecracker" title="Pintrest" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/pintrest.png" alt="Pintrest" style="width:24px"></a> </td>
            </tr>
          </tbody>
</table>
</body>
</html>