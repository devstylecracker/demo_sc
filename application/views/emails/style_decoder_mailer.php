<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Style Decoder</title>
  <link href='https://fonts.googleapis.com/css?family=Delius' rel='stylesheet' type='text/css'>
</head>
<body style="background:#ccc; color:#010101; text-align:left; font-family:Delius,Arial,Helvetica,sans-serif; font-weight: normal; line-height: 1.5; font-size: 14px; padding:0; margin:0 auto;">
  <table width="600" cellpadding="0" cellspacing="0" align="center" border="0" style="width:600px; background:#fff; margin-bottom:20px;">
    <tr>
      <td style="text-align:center; padding:20px 0 10px;">
        <img src="https://www.stylecracker.com/assets/images/logo.png" style="height:34px;">
      </td>
    </tr>
    <tr>
      <td style="text-align:center; padding:0px 0 0px; text-transform:uppercase; font-size:16px; font-weight:500; ">
        <div style="border-top:4px dotted #010101; margin:0px 45px 20px; padding-top:10px;  font-size:16px; ">
        We have decoded your personal style for you
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
          <tr>
            <td align="center" style="width:158px; padding:0px; ">
              <img src="<?php echo base_url() ?>assets/images/style_decoder/womens/pa/<?php echo str_replace("jpg", "png", $user_ansers_images[1]); ?>" style="">
            </td>
            <td align="center" style="padding:0px 10px 0; vertical-align:top;">
              <div style="font-size:18px; line-height:1.2; text-align:left; padding:0px 0 20px; margin-top:40px;">
                Your style spells<br/><b style="text-transform:uppercase; font-family:arial; line-height:1.5;"><?php echo $this->config->item("ans".$user_answers[2]); ?></b><br/> <?php echo $this->config->item("ans".$user_answers[2]."_".$user_answers[8]); ?>
              </div>
              <!-- <div style="font-size:18px; line-height:1.2; text-align:left;  padding:20px 0;">
                <div style="font-weight:bold; text-transform:uppercase; padding-bottom:5px;">Edegy</div>
                You opt for<br />
                modern colours<br />
                and flattering<br />
                shapes
              </div> -->
              <div style="font-size:18px; line-height:1.2; text-align:right;  padding:20px 0; margin-bottom:20px;">
                <span><?php echo $this->config->item("ans".$user_answers[1]);  ?></span>
              </div>
              <div>
                <a href="<?php echo base_url()."feed"; ?>" style="display:inline-block; font-size:14px;  padding:5px 10px; text-transform:uppercase; background:#68cec1; color:#000; font-weight:bold; text-decoration:none; font-family:arial;">VIEW YOUR FEED</a>
              </div>
            </td>
            <td align="center" style="width:158px; padding:0px;">
              <img src="<?php echo base_url() ?>assets/images/style_decoder/womens/pa/<?php echo $user_ansers_images[2] ?>" style="">
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td>
          <div style="border-bottom:4px dotted #010101; margin:20px 5px 5px; font-size:1px;">&nbsp;</div>
      </td>
    </tr>
    <tr>
      <td>
        <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
          <tr>
            <td align="center" style="width:306px; padding:0px 0 15px;; ">
              <div style="border-right:4px dotted #010101; padding-top:10px;">
                <img width="260" height="260" src="<?php echo base_url()."sc_admin/assets/looks/".$user_feeds_letest[0]['image']; ?>" style="margin-bottom:10px;">
                <a href="<?php echo base_url()."looks/look-details/".$user_feeds_letest[0]['slug']; ?>" style="display:inline-block; padding:5px 10px; text-transform:uppercase; background:#68cec1; color:#000; font-weight:bold; text-decoration:none; font-family:arial;">Get This</a>
                <!-- <br/><br/> -->
                <img src="<?php echo base_url(); ?>assets/images/newsletter/looks-to-love.jpg"  style="margin:10px 0;">
                <img width="260" height="260" src="<?php echo base_url()."sc_admin/assets/looks/".$user_feeds_letest[1]['image']; ?>"  style="margin-bottom:10px;">
                <a href="<?php echo base_url()."looks/look-details/".$user_feeds_letest[1]['slug']; ?>" style="display:inline-block; padding:5px 10px; text-transform:uppercase; font-size:14px;  background:#68cec1; color:#000; font-weight:bold; text-decoration:none; font-family:arial;">Get This</a>
              </div>
            </td>
            <td align="center" style="width:294px; padding:0px; vertical-align:top; padding:0 7px;">
              <div style="font-size:26px; line-height:1.2; padding:30px 0; font-weight:500; text-transform:uppercase;">
                You share your <br />spotlight with
              </div>

              <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
                <tr>
                  <td align="center" style="padding:0px; vertical-align:top;">
                    <img src="<?php echo base_url(); ?>assets/images/style_decoder/womens/bollywood/<?php echo $this->config->item("ans".$user_answers[2]."_img1"); ?>" style="margin-bottom:5px;">
                    <div style="font-size:14px;  padding:5px 0;">
                      <a href="<?php echo $this->config->item("ans".$user_answers[2]."_url1");?>" style="display:inline-block; padding:5px 10px; text-transform:uppercase; background:#68cec1; color:#000; font-weight:bold; text-decoration:none;font-family:arial; margin-top:65px;">SHOP NOW</a>
                    </div>
                  </td>
                  <td align="center" style="padding:0px; vertical-align:top;">
                    <img src="<?php echo base_url(); ?>assets/images/style_decoder/womens/bollywood/<?php echo $this->config->item("ans".$user_answers[2]."_img2"); ?>"  style="margin-bottom:5px;">
                    <div style=" font-size:14px;  padding:5px 0; margin-top:65px;">
                      <a href="<?php echo $this->config->item("ans".$user_answers[2]."_url2");?>" style="display:inline-block; padding:5px 10px; text-transform:uppercase; background:#68cec1; color:#000; font-weight:bold; text-decoration:none; font-family:arial;">SHOP NOW</a>
                    </div>
                  </td>
                </tr>
              </table>
              
            </td>
          </tr>

        </table>

      </td>
    </tr>
    <tr>
      <td style="font-size:17px; text-align:center; padding:20px; font-weight:500;">
        Love <?php echo $stylist ?>, Your personal stylist
      </td>
    </tr>
  </table>

</body>
</html>
