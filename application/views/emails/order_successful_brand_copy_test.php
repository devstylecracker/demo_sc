<?php /*echo "<pre>"; print_r($order_product_info);*/
      /*echo "<pre>"; print_r($order_info);*/
 ?>
<?php
$order_info_array = array(); $order_pro_array =array(); $OrderTotal = 0; $totalProductPrice=0;
$brand_id_array = array(); $couponDiscountDiff = array();

/* $banner_image = '';
  if($banner_image!='')
  {
     $email_banner ='<div> <img src="https://www.stylecracker.com/assets/images/email/'.$banner_image.'" ></div>';
  }else
  {
    $email_banner ='';
  }*/

  if(!empty($order_info)){
    foreach ($order_info as $key => $value) {
      $brand_id_array[] = $value['brand_id'];

      $order_info_array[$value['brand_id']][] = array(
          'order_tax_amount' => $value['order_tax_amount'],
          'cod_amount' => $value['cod_amount'],
          'order_sub_total' => $value['order_sub_total'],
          'order_total' => $value['order_total'],
          'shipping_amount' => $value['shipping_amount'],
          'first_name' => $value['first_name'],
          'last_name' => $value['last_name'],
          'first_name' => $value['first_name'],
          'email_id' => $value['email_id'],
          'mobile_no' => $value['mobile_no'],
          'coupon_code' => @$value['coupon_code'],
          'coupon_amount' => @$value['coupon_amount'],
          'confirm_mobile_no' => @$value['confirm_mobile_no'],
          'state_id' => $value['state_id'],
          'it_state' => $value['it_state']
        );

      $OrderTotal = $OrderTotal+$value['order_total'];

    }
  }
  $count = 0;
   if(!empty($res1)){ 
      foreach($res1 as $value)
      { $count++;
        if($value['coupon_code']!='' || $referral_point >0)
        {
         $totalProductPrice = $totalProductPrice+$value['product_price'];
        }
      }
    }

  /*Coupon Discount */
  $coupon_result = $this->cartnew_model->calculateCouponAmount($order_product_info,$totalProductPrice);
  $coupon_discount_type= @$coupon_result['coupon_discount_type'];
  $coupon_discount= @$coupon_result['coupon_discount'];
  $coupon_code = strtoupper($coupon_result['coupon_code']);
  $sc_coupon_discount = @$coupon_result['coupon_amount'];
  $coupon_stylecracker = @$coupon_result['coupon_stylecracker'];
  $coupon_brand = @$coupon_result['coupon_brand'];
  /*Coupon Discount End */

  $coupDiscountPrdPrice = 0;
if(!empty($order_product_info)){
  foreach($order_product_info as $value){
     $coupDiscountPrdPrice = 0; $ordTotal = 0; $prdDiscountAmtDiff = 0;
    $productSku = $this->cartnew_model->getProductSku($value['product_id'],$value['product_size']);
    if($productSku!='')
    {
      $productSkuStr = $productSku;
    }else
    {
      $productSkuStr = '';
    }

    if($value['coupon_code']!='' && $coupon_code!='' )//&& $coupon_stylecracker == 1 && $coupon_brand == 0
    {
      if($coupon_discount_type==1)
      {
        $coupDiscountPrdPrice = $this->cartnew_model->calculate_product_discountAmt($value['product_price'],$totalProductPrice,$coupon_discount_type,($coupon_discount+$referral_point));


      }else if($coupon_discount_type==2)
      {
        $coupDiscountPrdPrice = $this->cartnew_model->calculate_product_discountAmt_percent($value['product_price'],$count,$totalProductPrice,$coupon_discount_type,$sc_coupon_discount);
         $coupDiscountPrdPrice = $value['product_price'] - ($coupDiscountPrdPrice + ($referral_point/$count));
         //echo $count.'<br/>';
        //echo $coupDiscountPrdPrice = $coupDiscountPrdPrice - ($referral_point/$count);


      }else if($coupon_discount_type==3 || $coupon_discount_type==4)
      {
        if($coupon_brand == $value['brand_id'])
        {
         $coupDiscountPrdPrice = $value['product_price']-($coupon_discount+$referral_point);
        }
      }
    }

      if($value['coupon_code']==''  && $referral_point >0)
    {    
        $coupon_discount_type = 1;  
        $coupDiscountPrdPrice = $this->cartnew_model->calculate_product_discountAmt($value['product_price'],$totalProductPrice,$coupon_discount_type,$referral_point);
       
    }

     if($coupDiscountPrdPrice>0)
    {
      $grandPrice = $coupDiscountPrdPrice;
    }else
    {
      $grandPrice = $value['product_price'];
    }

    $order_pro_array[$value['brand_id']][] = array(
        'product_id' => $value['product_id'],
        'name' => $value['name'],
        'size_text' => $value['size_text'],
        'image' => $value['image'],
        'product_qty' => $value['product_qty'],
        'product_price' => $value['product_price'],
        'company_name' => $value['company_name'],
        'product_sku' => $productSkuStr,
        'min_del_days' => @$value['min_del_days'],
        'max_del_days' => @$value['max_del_days'],
        'order_tax_amount' => @$value['order_tax_amount'],
        'cod_amount' => @$value['cod_amount'],
        'shipping_amount' => @$value['shipping_amount'],
        'order_total' => @$value['order_total'],
        'discount_price' => $value['discount_price'],
        'product_size' => $value['product_size'],
        'cart_id' => $value['cart_id'],
        'brand_id' => $value['brand_id'],
        'order_id' => $value['order_id'],
        'user_id' => $value['user_id'],
        'coupon_code' => $value['coupon_code'],
        'coupon_discount_price' => $coupDiscountPrdPrice,
        'grand_price' => $grandPrice
      );

      if($coupDiscountPrdPrice>0)
      {
       $prdDiscountAmtDiff = $value['product_price'] - $coupDiscountPrdPrice;
      }else
      {
        $couponDiscountDiff[$value['brand_id']]['OrderSubTotal'] = $value['order_total'];
      }

      if(isset($couponDiscountDiff[$value['brand_id']]['OrderSubTotal']))
      {
        $couponDiscountDiff[$value['brand_id']]['OrderSubTotal'] = $couponDiscountDiff[$value['brand_id']]['OrderSubTotal']-$prdDiscountAmtDiff ;
        $couponDiscountDiff[$value['brand_id']]['DiffAmt'] = @$couponDiscountDiff[$value['brand_id']]['DiffAmt']+$prdDiscountAmtDiff;
      }else
      {
        $couponDiscountDiff[$value['brand_id']]['OrderSubTotal'] = @$value['order_total']-$prdDiscountAmtDiff ;
        $couponDiscountDiff[$value['brand_id']]['DiffAmt'] = $prdDiscountAmtDiff;
      }
  }
}
/*echo '<pre>';print_r( $couponDiscountDiff);*/

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Order Summary</title>

</head>
<body style="background:#f3f3f3; color:#333333; text-align:left; font-family:Arial,Helvetica,sans-serif; font-weight: normal; line-height: 1.5;
font-size: 14px; padding:0; margin:0 auto;">
<table style="width:600px;background:#fff;margin-bottom:20px; font-family:Arial,Helvetica,sans-serif;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
          <tbody>
            <tr>
              <td style="text-align:left;border-bottom:3px solid #00b19c;padding:20px">
                <table style="width:100%; font-family:Arial,Helvetica,sans-serif;" align="center" border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="text-align:left"> <img src="https://www.stylecracker.com/assets/images/logo.png" style="min-height:34px"> </td>
                      <td style="text-align:right;vertical-align:middle;font-size:14px;color:#00b19c"> <a href="#" style="color:#00b19c;text-decoration:none" target="_blank">My
                          Orders</a> </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td style="padding:10px">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family:Arial,Helvetica,sans-serif;">
                  <tbody>
                    <tr>
                      <td style="padding:15px 0 30px;vertical-align:middle" align="center"> <span style="padding:8px 0;border-bottom:1px solid #cccccc;font-size:18px;text-transform:uppercase">
                        <img src="https://www.stylecracker.com/assets/images/icons/ic-order-processing-1.png" style="vertical-align:middle">
                      <span> Order Processing</span>
                    </td>
                    </tr>
                    <tr>
                      <td style="font-size:15px;line-height:1.7">
                        <p style="margin-bottom:20px"> Hi team <strong><?php echo $company_name; ?>
                          </strong>,<br>
                          Congrats!, We have recevied new order for you. Your Order No is <strong style="color:#00b19c"><?php echo $orderUniqueNo; ?></strong>, further details are as below. </p>
                        <p style="margin-bottom:20px">
                        Please log on to your dashboard at <a href="https://www.stylecracker.com/sc_admin" target="_blank" >https://www.stylecracker.com/sc_admin</a> and update the status accordingly. </p>
                          <?php if($paymentmode == 2)
                                {
                          ?>
                        <p style="margin-bottom:20px"> Payment Mode : <img src="https://www.stylecracker.com/assets/images/icons/ic-online-payment-1.png" style="vertical-align:middle"> Online
                          Payment </p>
                          <?php }
                          if ($paymentmode == 1) {
                           ?>
                          <p style="margin-bottom:20px"> Payment Mode : <img src="https://www.stylecracker.com/assets/images/icons/ic-cod-1.png" style="vertical-align:middle;" style="vertical-align:middle"> Cash on Delivery </p>
                         <?php  } ?>
                        <div style="background:#f3f3f3;border:1px solid #cccccc;padding:20px 20px 15px;display:inline-block">
                          <div style="text-transform:uppercase;font-weight:bold;line-height:1"> Shipping
                            Address </div>
                          <div> <?php echo  $Username.'<br/>'.$shipping_address; ?><br>
                            <?php echo $city_name.' - '.$pincode.'<br/>'.$state_name.' '.$country_name.'<br/> Email: '.$email.'<br/> Mobile No: '.$mobile_no; ?> </div>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <br>
                <br>
                <div style="text-transform:uppercase;font-weight:bold;line-height:1;font-size:16px;margin-bottom:20px">
                  Order Details: </div>
                <table style="font-size:12px; font-family:Arial,Helvetica,sans-serif;" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
                  <tbody>
                    <tr>
                      <th style="background-color:#f3f3f3;font-size:13px; vertical-align:top; font-weight:normal;"><strong>Image</strong></th>
                      <th style="background-color:#f3f3f3;font-size:13px; vertical-align:top; font-weight:normal;"><strong>Product Details</strong></th>
                      <th style="background-color:#f3f3f3;font-size:13px; vertical-align:top; font-weight:normal;"><strong>Seller</strong></th>
                      <th style="background-color:#f3f3f3;font-size:13px;width:60px;text-align:right; vertical-align:top; font-weight:normal;"><strong>Price</strong></th>
                      <!--<th style="background-color:#f3f3f3;font-size:13px;width:60px;text-align:right; vertical-align:top; font-weight:normal;"><strong>Discounted Price</strong></th>-->
                      <th style="background-color:#f3f3f3;font-size:13px;width:60px;text-align:right; vertical-align:top; font-weight:normal;"><strong>Grand Price</strong></th>
                    </tr>
                  <?php if(!empty($brand_id_array)) {
                    foreach($brand_id_array as $b_id){
                      if(!empty($order_pro_array[$b_id])){
                        foreach ($order_pro_array[$b_id] as $value) {

                   ?>

                    <tr>
                      <td><img src="https://www.stylecracker.com/sc_admin/assets/products/<?php echo $value['image']; ?>" style="border:1px solid #cccccc;padding:2px;width:auto;min-height:auto;max-width:50px;max-height:50px" width="50"></td>
                      <td style="vertical-align:top;"><?php echo $value['name']; ?>
                          <div style="font-size:11px;">
                          <strong>SKU: </strong><?php echo $value['product_sku']; ?>| <strong>QTY: </strong><?php echo $value['product_qty']; ?>| <strong>Size: </strong><?php echo $value['size_text']; ?>
                          </div>
                        <?php  if($value['min_del_days']>0 && $value['max_del_days']>0){ ?>
                        <div style="font-size:11px;color:#2db143">Delivery in <?php echo $value['min_del_days'].' - '.$value['max_del_days']; ?> working days.</div>
                        <?php  }else if(($value['min_del_days']>0 && $value['max_del_days']=='') || ($value['min_del_days']=='' && $value['max_del_days']>0)){ ?>
                          <div style="font-size:11px;color:#2db143">Delivery in <?php echo $value['min_del_days'].' - '.$value['max_del_days']; ?> working days.</div>
                          <?php } ?>
                         </td>
                      <td style="vertical-align:top;"><?php echo $value['company_name']; ?></td>
                      <td style="text-align:right;width:60px; vertical-align:top;">&#8377; <?php echo number_format(round($value['product_price'],2),2); ?></td>
                      <!--<td style="text-align:right;width:60px; vertical-align:top;">&#8377; <?php echo number_format(round($value['coupon_discount_price'],2),2); ?></td>-->
                      <td style="text-align:right;width:60px; vertical-align:top;">&#8377; <?php echo number_format(round($value['grand_price'],2),2); ?></td>
                    </tr>

                    <?php } }
                     if(!empty($order_info_array[$b_id])){
                        foreach ($order_info_array[$b_id] as $value) { ?>
                    <?php
                      if($value['order_tax_amount']>0)
                      {
                      ?>
                    <tr>
                      <td colspan="4" style="padding:2px 8px;text-align:right; border-top:1px solid #eee;"><strong>Tax:</strong></td>
                      <td style="padding:2px 8px;text-align:right; border-top:1px solid #eee;"> &#8377; <?php echo number_format(round($value['order_tax_amount'],2),2); ?></td>
                    </tr>
                    <?php
                      }
                      if($value['cod_amount']>0)
                      {
                    ?>
                    <tr>
                      <td colspan="4" style="padding:2px 8px;text-align:right"><strong>COD:</strong></td>
                      <td style="padding:2px 8px;text-align:right"> &#8377; <?php echo number_format(round($value['cod_amount'],2),2); ?> </td>
                    </tr>
                    <?php
                      }
                      if($value['shipping_amount']>0)
                      {
                    ?>
                    <tr>
                      <td colspan="4" style="padding:2px 8px;text-align:right"><strong>Shipping:</strong></td>
                      <td style="padding:2px 8px;text-align:right"> &#8377; <?php echo number_format(round($value['shipping_amount'],2),2); ?> </td>
                    </tr>
                    <?php
                      }
                    ?>
                    <tr>
                      <td colspan="4" style="padding:2px 8px 10px;text-align:right; border-bottom:1px solid #e3e3e3;"><strong>Total Amount:</strong></td>
                      <td style="padding:2px 8px 10px;text-align:right; border-bottom:1px solid #e3e3e3;"> &#8377; <?php echo number_format(round(($couponDiscountDiff[$b_id]['OrderSubTotal']),2),2); ?> </td>
                    </tr>

                    <?php } } ?>
                    <?php } } ?>
                    <?php if($coupon_discount>0 && $referral_point==0)
                          {
                    ?>
                    <tr>
                      <td colspan="4" style="padding:2px 8px; text-align:right"><strong>(Applied Coupon Code <span color="#f87106"><?php echo $coupon_code; ?></span> Discount:</strong> &#8377; <?php echo '<i>'.number_format(($coupon_discount),2).'</i>'.')'; ?> </td>
                      <td style="padding:2px 8px;text-align:right">
                       </td>
                    </tr>
                    <?php } ?>
                    <?php 
                    if($referral_point> 0 && $coupon_discount==0){
                    ?>
                        <tr>
                          <td colspan="4" style="padding:2px 8px; text-align:right;">(<?php echo REDEEM_POINTS_CART_TEXT.': &#8377; <i>'.number_format($couponDiscountDiff[$b_id]['DiffAmt'],2).'</i>)' ; ?>
                          </td>
                         <!-- <td colspan="5" style="padding:2px 8px; text-align:right;"> & (<?php echo REDEEM_POINTS_CART_TEXT.': &#8377; <i>'.number_format($referral_point,2).'</i>)' ; ?>
                          </td>-->
                          <td style="padding:2px 8px; text-align:right;">                           
                          </td>
                        </tr>
                    <?php } ?>
                    <?php
                     if($referral_point> 0 && $coupon_discount>0){
                    ?>
                        <tr>
                          <td colspan="4" style="padding:2px 8px; text-align:right;">(Applied Coupon Code <span color="#f87106"><?php echo $coupon_code; ?></span> Discount:</strong> & <?php echo REDEEM_POINTS_CART_TEXT.': &#8377; <i>'.number_format($couponDiscountDiff[$b_id]['DiffAmt'],2).'</i>)' ; ?>
                          </td>
                         <!-- <td colspan="5" style="padding:2px 8px; text-align:right;"> & (<?php echo REDEEM_POINTS_CART_TEXT.': &#8377; <i>'.number_format($referral_point,2).'</i>)' ; ?>
                          </td>-->
                          <td style="padding:2px 8px; text-align:right;">                           
                          </td>
                        </tr>
                    <?php } ?>
                  </tbody>
                </table>
                <div style="text-align:right;background:#f3f3f3;padding:5px 30px;font-size:18px;text-transform:uppercase;margin-top:20px;border-bottom:2px solid #cccccc"> Total Amount: &#8377;
                  <?php echo number_format(round(($couponDiscountDiff[$b_id]['OrderSubTotal']),2),2); ?> </div>
              </td>
            </tr>
            <tr>
              <td style="padding:0px 30px">
                <div style="border-bottom:1px solid #ccc;padding-bottom:30px"> Thank you for shopping with <strong>StyleCracker</strong>.<br>
                  For any query or assistance, feel free to <a href="#" style="color:#00b19c;text-decoration:none" target="_blank">Contact
                    Us</a> </div>
              </td>
            </tr>
            <tr>
              <td style="vertical-align:middle;padding:20px 30px"> <span style="color:#888;font-weight:bold;line-height:1.3;vertical-align:top"> Follow Us: </span> <a href="#" title="Facebook" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/facebook.png" alt="Facebook" style="width:24px"></a> <a href="#" title="Twitter" target="_blank">
                <img src="https://www.stylecracker.com/assets/images/social-media/twitter.png" alt="Twitter" style="width:24px"></a>
                <a href="#" title="Youtube" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/youtube.png" alt="Youtube" style="width:24px"></a> <a href="#" title="Instagram" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/instagram.png" alt="Instagram" style="width:24px"></a>
                 <a href="#" title="Pintrest" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/pintrest.png" alt="Pintrest" style="width:24px"></a> </td>
            </tr>
          </tbody>
        </table>
         <table width="600" cellpadding="0" cellspacing="0" align="center" border="0" style="width:600px; background:#fff; border:1px solid #cccccc; margin-bottom:20px; font-family:Arial,Helvetica,sans-serif;">
        <tr>
          <td align="left" style="padding:20px 0 0 20px; width:160px;">
              <img src="https://www.stylecracker.com/assets/images/logo.png" style="width:150px; margin-bottom:5px;">
          <div>
            <a href="https://www.stylecracker.com" style="color:#00b19c; text-decoration:none;">www.stylecracker.com</a>
          </div>
          </td>
            <td align="left"  style="padding:20px 0 0; vertical-align:bottom;">
              <img src="https://www.stylecracker.com/assets/images/icons/ic-download-app-1.png">
            <td>

            </td>
          <td align="right"  style="padding:20px 20px  0 0; text-align:center; ">
            <div style="font-size:18px; margin-bottom:10px;">
              StyleCracker App is India\'s only <br /> personalised styling platform.
            </div>

             <a href="https://play.google.com/store/apps/details?id=com.stylecracker.android" target="_blank" style="color:#333; text-decoration:none;">
                  <img src="https://www.stylecracker.com/assets/images/icons/en_generic_rgb_wo_45.png" style="height:30px;" target="_blank"></a>

            <a href="https://itunes.apple.com/us/app/stylecracker/id1061463812?mt=8&uo=4" target="_blank" style="color:#333; text-decoration:none; ">
              <img src="https://www.stylecracker.com/assets/images/icons/apple_store.png" style="height:30px;" target="_blank"></a>


          </td>
        </tr>
      </table>
      </body>
      </html>
