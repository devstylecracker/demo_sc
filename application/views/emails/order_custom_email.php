<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Feedback</title>

</head>
<body style="background:#f3f3f3; color:#333333; text-align:left; font-family:Arial,Helvetica,sans-serif; font-weight: normal; line-height: 1.5;
font-size: 14px; padding:0; margin:0 auto;">
<table style="width:600px;background:#fff;margin-bottom:20px; font-family:Arial,Helvetica,sans-serif;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
          <tbody>
            <tr>
              <td style="text-align:left;border-bottom:3px solid #00b19c;padding:20px">
                <table style="width:100%; font-family:Arial,Helvetica,sans-serif;" align="center" border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="text-align:left"> <img src="https://www.stylecracker.com/assets/images/logo.png" style="min-height:34px"> </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td style="padding:10px">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style=" font-family:Arial,Helvetica,sans-serif;">
                  <tbody>                  
                    <tr>
                       <td style="padding:0px 30px">
                        <div style="padding-bottom:30px">Dear <strong><?php echo $first_name; ?>,</strong><br/><br/>
                         We hope you're having a fabulous October. We love that you are part of the StyleCracker family and we would like to show our appreciation by extending a very special offer to you.<br/><br/>
                         For a limited time only*, you will receive 30% off on your next StyleCracker Box, at any price point, PLUS a surprise gift from us to you!<br/><br/>
                         All you have to do is visit <a href="https://www.stylecracker.com/" title="StyleCracker" target="_blank">www.stylecracker.com </a> and use coupon code <b>BOX30</b> to avail this exciting offer.<br/>
                         </div>
                       </td>
                    </tr>
                    <tr>
                        <td style="padding:0px 30px 30px;"><div>We look forward to styling you!</div></td>
                    </tr> 
                    <tr>
                      <td style="padding:0px 30px">
                        <div style="border-bottom:1px solid #ccc;"> Thank you for shopping with <strong>StyleCracker.</strong><br>
                          For any query or assistance, feel free to <a href="mailto:customercare@stylecracker.com" style="color:#00b19c;text-decoration:none" target="_top">Contact Us</a>
                          <div style="font-size: 11px;padding-top: 30px;">*Offer valid for 30 days from today</div>
                           </div>

                      </td>
              
                    </tr>                  
                  </tbody>
                </table>               
              </td>
            </tr>           
            <tr>
              <td style="vertical-align:middle;padding:20px 40px"> <span style="color:#888;font-weight:bold;line-height:1.3;vertical-align:top"> Follow Us: </span> <a href="https://www.facebook.com/StyleCracker" title="Facebook" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/facebook.png" alt="Facebook" style="width:24px"></a> <a href="https://twitter.com/Style_Cracker" title="Twitter" target="_blank">
                <img src="https://www.stylecracker.com/assets/images/social-media/twitter.png" alt="Twitter" style="width:24px"></a>
                <a href="https://www.youtube.com/user/StyleCrackerTV" title="Youtube" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/youtube.png" alt="Youtube" style="width:24px"></a> <a href="https://instagram.com/stylecracker" title="Instagram" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/instagram.png" alt="Instagram" style="width:24px"></a>
                 <a href="https://www.pinterest.com/stylecracker" title="Pintrest" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/pintrest.png" alt="Pintrest" style="width:24px"></a> </td>
            </tr>
          </tbody>
</table>
</body>
</html>