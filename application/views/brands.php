<div class="container custom">
      <div class="brands-banner-wrp">
          <div class="swiper-container main">
				<ul class="swiper-wrapper">
						<?php $last_id = 0;
						if(!empty($brands_banners)){
							foreach($brands_banners as $row){
						?>
											<li class="swiper-slide slide<?php echo $row['banner_position']; ?>">
												<?php if($row['lable_link'] != ''){ ?><a href="<?php echo $row['lable_link']; ?>"><?php } ?>
												<img src="<?php echo base_url(); ?>sc_admin/assets/brand_banners/<?php echo $row['slide_url']; ?>"  />
												<?php if($row['caption_line_1'] != '' && $row['caption_line_1'] != 'slide1' && $row['caption_line_1'] != 'slide2' && $row['caption_line_1'] != 'slide3' && $row['caption_line_1'] != 'slide4' && $row['caption_line_1'] != 'slide5'){?>
												<div class="container">
													<div class="flex-caption">
											              <span class="line1"><?php echo $row['caption_line_1']; ?></span>
											              <span class="line2"><?php echo $row['caption_line_2']; ?></span>
											    	</div>
												</div>
												<?php }?>
												<?php if($row['lable_link'] != ''){ ?></a> <?php } ?>
											 </li>
											 <?php
 											$last_id = $row['id'];
 											}// end of for loop
 											}//end of if loop
 											?>
						</ul>
						
                 <div class="swiper-button-next main"></div>
                 <div class="swiper-button-prev main"></div>
                 <div class="swiper-pagination main"></div>
        </div>
    </div>
</div>

  <div class="page-brands">
    <div class="container">
     <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a></li>
      <li class="active">Brands</li>
    </ol>
  </div>
    <div class="container">
      <ul class="nav nav-tabs common-tabs">
        <li class="active"><a data-toggle="tab" href="#collection">Collections</a></li>
        <li><a data-toggle="tab" href="#directory">Brands (A-Z)</a></li>
      </ul>
      <div class="tab-content common-tab-content">
        <div id="collection" class="tab-pane fade in active">

<?php foreach($brand_collections['name'] as $row){ ?>
          <div class="collections-section trip-section">
            <div class="title-section">
                <h1 class="title"><span><?php echo $row['category_name'] ?></span></h1>
            </div>
            <div class="swiper-container common">
                <div class="swiper-wrapper">
                  <?php foreach($brand_collections['brands'][$row['category_slug']] as $data_row){ $data_row = (array)$data_row;?>
                    <div class="swiper-slide" style="padding:0px 5px">
                      <a href="<?php echo base_url().'brands/brand-details/'.$data_row['user_name']; ?>" onclick="ga('send', 'event', 'Brand', 'clicked', '<?php echo $data_row['user_name']; ?>');" >
                       <?php if($data_row['cover_image'] != ""){ ?>
                       <img src="<?php echo $this->config->item('brandpage_list_img_url').$data_row['cover_image']; ?>">
                       <?php }else{ ?>
                       <img src="<?php echo base_url(); ?>assets/images/brands/list/default.jpg">
                       <?php } ?>
                      </a>
                    </div>
                    <?php } ?>
                </div>
                <div class="swiper-button-prev common"></div>
                <div class="swiper-button-next common"></div>
            </div>
          </div>
<?php } ?>

    </div>
    <div id="directory" class="tab-pane fade">
      <div class="title-section">
          <h1 class="title"><span>All Brands</span></h1>
      </div>
        <div>
          <input type="text" class="brand-search" id="brand-search" placeholder="SEARCH FOR A BRAND"/>
        </div>

      <div class="content-brands content-brands-list">
    <div class="row">
      <div class="col-md-2">
        <div class="sidebar brand-sidebar">
          <h3 class="title">Categories</h3>
          <ul>
            <li id="brands_a_z" class="active categories" >
              Brands A-Z
            </li>
            <?php
              foreach($brand_categories['name'] as $row){
                echo "<li id='".$row['category_slug']."' onclick=\"get_category_brand_list('".$row['category_slug']."')\" class='categories'>
                        ".$row['category_name']."
                      </li>";
              }
            ?>
          </ul>
        </div>
      </div>
      <div class="col-md-10">
        <div class="brands-content-section">
<h3>Brands A-Z</h3>
<ul class="alphabet-list">
  <li><a href="#number">0-9</a></li>
  <li><a class="A">A</a></li>
  <li><a class="B">B</a></li>
  <li><a class="C">C</a></li>
  <li><a class="D">D</a></li>
  <li><a class="E">E</a></li>
  <li><a class="F">F</a></li>
  <li><a class="G">G</a></li>
  <li><a class="H">H</a></li>
  <li><a class="I">I</a></li>
  <li><a class="J">J</a></li>
  <li><a class="K">K</a></li>
  <li><a class="L">L</a></li>
  <li><a class="M">M</a></li>
  <li><a class="N">N</a></li>
  <li><a class="O">O</a></li>
  <li><a class="P">P</a></li>
  <li><a class="Q">Q</a></li>
  <li><a class="R">R</a></li>
  <li><a class="S">S</a></li>
  <li><a class="T">T</a></li>
  <li><a class="U">U</a></li>
  <li><a class="V">V</a></li>
  <li><a class="W">W</a></li>
  <li><a class="X">X</a></li>
  <li><a class="Y">Y</a></li>
  <li><a class="Z">Z</a></li>

</ul>
<?php $brand_count = count($brands_list);
if($brand_count > 0){
	$intCount = $brand_count / 3 ;
}

$number_array = array("0","1","2","3","4","5","6","7","8","9","!","@");


 if($intCount < 1){ $intCount = 1; }
$i = 1;
?>
<div class="row" id="brandlist">

<?php/*
if(!empty($brands_list)){ $k = 1; $new_name = ''; $old_name = ''; $o = 1;
	foreach($brands_list as $val){ ?>
		<?php if(round($intCount) >= $k){ ?>

			<?php if($k == 1){ ?>
				<div class="col-md-4 col-sm-6">
			<?php } ?>
			<?php $new_name = strtoupper(substr(trim($val->company_name), 0,1)); ?>
			<?php if($new_name != $old_name && !in_array($new_name, $number_array)){ ?>
			<?php if($i != 1){ ?>
				</ul>
			<?php } ?>
				<ul class="brand-listing">
				<li id="<?php echo strtoupper($new_name); ?>" class="alpha"><?php echo strtoupper($new_name); ?>

        <script>var a = document.getElementsByClassName('<?php echo strtoupper($new_name); ?>'); a[0].href = "#<?php echo strtoupper($new_name); ?>";</script>
				</li>
				<li><a href="<?php echo base_url().'brands/brand-details/'.$val->user_name; ?>" onclick="ga('send', 'event', 'Brand', 'clicked', '<?php echo $val->company_name; ?>');" ><?php echo $val->company_name; ?></a></li>
				<?php $old_name = $new_name; ?>
			<?php }else{ ?>
      <?php if(in_array($new_name, $number_array) && $o == 1){ ?>
           <li id="0-9" class="alpha"><?php echo '0-9'; ?>
        <?php } ?>
				<li><a href="<?php echo base_url().'brands/brand-details/'.$val->user_name; ?>"  onclick="ga('send', 'event', 'Brand', 'clicked', '<?php echo $val->company_name; ?>');" ><?php echo $val->company_name; ?></a></li>
			<?php } ?>
		<?php $o++; }else{ ?> </div>
			<?php $k = 0; ?>
		<?php } ?>
<?php $k++; $i++; }
}*/
?>
<?php 
    if(!empty($brands_list)){ $p = 1; $new_name = ''; $old_name = ''; $f = 1;
      foreach($brands_list as $val){ ?>
      <?php if($p == 1) { ?> <div class="col-md-4 col-sm-6"> <?php } ?>
        <?php if(round($intCount) == $f) { $f=1; ?>
          </div>
          <div class="col-md-4 col-sm-6">
        <?php } ?>
        <?php $new_name = strtoupper(substr(trim($val->company_name), 0,1)); ?>
        <?php if($new_name!=$old_name) { ?>
        <?php if($p!=1) { echo '</ul>'; } ?>
        <ul class="brand-listing">
        <li id="<?php echo strtoupper($new_name); ?>" class="alpha"><?php echo strtoupper($new_name); ?>
         <script type="text/Javascript">var a = document.getElementsByClassName('<?php echo strtoupper($new_name); ?>'); a[0].href = "#<?php echo strtoupper($new_name); ?>";</script>
        </li>
        <li><a href="<?php echo base_url().'brands/brand-details/'.$val->user_name.'/'.$val->store_id; ?>"><?php echo $val->company_name; ?></a></li>
        <?php }else{ ?>
        <li><a href="<?php echo base_url().'brands/brand-details/'.$val->user_name.'/'.$val->store_id; ?>"><?php echo $val->company_name; ?></a></li>
        <?php } ?>
        <?php $old_name = $new_name; ?>
        <?php $p++; $f++;
      }
    }
?>
</div>

      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
