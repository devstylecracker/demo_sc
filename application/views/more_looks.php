<?php foreach ($looks as $value) { ?>
            <div class="col-md-4 col-sm-6 grid-item">
              <div class="item">
                <div class="item-img item-hover">
                    <?php 
                      $short_url = base_url().'looks/look-details/'.$value['slug'];
                      $img_title_alt = str_replace('#','',$value['name']);

                      if($value['look_type'] == 1){ ?>
                        <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>"><img src="<?php echo $this->config->item('sc_look_image_url').$value['image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                      <?php $img_url = $this->config->item('sc_look_image_url').$value['image'];
                        }else if($value['look_type'] == 2 || $value['look_type'] == 4 || $value['look_type'] == 6){ ?>
                        <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>"><img src="<?php echo $this->config->item('sc_street_style_image_url').$value['look_image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                      <?php $img_url = $this->config->item('sc_street_style_image_url').$value['look_image'];
                      }else if($value['look_type'] == 3){ ?>
                      <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>"><img src="<?php echo $this->config->item('sc_promotional_look_image').$value['product_img']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                      <?php $img_url = $this->config->item('sc_promotional_look_image').$val['product_img'];
                      }
                      ?>
                      <?php if($this->home_model->look_hv_discount($value['id']) == 1){ ?>
                      <div class="item-tag-sale"></div>
                      <?php } ?>
                  <div class="item-look-hover">
                    <a href="<?php echo $short_url; ?>" title="<?php echo $img_title_alt; ?>" target="_blank"><span class="btn btn-primary"><?php echo $this->lang->line('get_this'); ?></span></a>
                  </div>
                  </div>
                  <div class="item-desc">
                    <div class="item-title"><a title="<?php echo $img_title_alt; ?>" href="<?php echo $short_url; ?>" target="_blank"><?php echo $value['name']; ?></a>
                    </div>
                    <div class="item-media">
                      <ul class="social-buttons">
                        <li><a onclick="_targetFacebookShare('<?php echo $value['name']; ?>','<?php echo $short_url; ?>','<?php echo $img_url; ?>','<?php echo $value['id']; ?>','<?php  echo $this->seo_desc; ?>');"><i class="fa fa-facebook"></i></a></li>
                        <li><a onclick="_targetTweet('<?php echo $short_url; ?>','<?php echo $value['name']; ?>','<?php echo $value['id']; ?>');"><i class="fa fa-twitter"></i></a></li>
                        <li><a onclick="_targetPinterest('<?php echo $short_url; ?>','<?php echo $value['name']; ?>','<?php echo $value['id']; ?>','<?php echo $img_url; ?>');"><i class="fa fa-pinterest"></i></a></li>
                      </ul>
                      <?php
                      if($this->session->userdata('user_id')){
                        if($this->home_model->get_look_fav_or_not($value['id']) == 0){ ?>
                          <div class="user-likes"><a target="_blank" title="Add to Wish List" id="whislist<?php echo $value['id']; ?>" onclick="add_to_wishlist(<?php echo $value['id']; ?>);"><i class="ic sc-love"></i></a></div>
                        <?php }else{ ?>
                          <div class="user-likes"><a target="_blank" title="Remove from Wish List" id="whislist<?php echo $value['id']; ?>" onclick="remove_from_wishlist(<?php echo $value['id']; ?>);"><i class="ic sc-love active"></i></a></div>
                        <?php } }else{ ?>
                        <div class="user-likes"><a target="_blank" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a></div>
                      <?php } 
                      ?>
                    </div>
                  </div>
                </div>
              </div>
       <?php }  ?>