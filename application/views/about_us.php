<div class="page-about page-banner">
  <img src="<?php echo base_url(); ?>assets/images/about/banner-about.jpg" />
  <div class="about-banner-text">
    <h1>StyleCracker is India's only personalized styling platform.</h1>
    <h2>We transform the way you shop.</h2>
  </div>
</div>

<div class="page-about content-wrp">
  <div class="container">

    <div class="about-intro">
      <p>StyleCracker was founded in 2013 by celebrity stylist Archana Walavalkar and former investment banker Dhimaan Shah. As India's only fashion styling platform, we transform the way you shop.</p>
       	<div class="row seven-cols about-intro-boxes">
     <div class="col-md-1">
      <div class="box">
       <img src="<?php echo base_url(); ?>assets/images/about/about-img-1.png">
       <h5>Sign up to get styled on-demand</h5>
       <p>Our stylists dress Bollywood's A-listers. Getting your fave celeb's style just got easier!</p>
 </div>
  </div>
     <div class="col-md-1">
       <div class="box">
       <img src="<?php echo base_url(); ?>assets/images/about/about-img-2.png">
       <h5>Get customised, shoppable looks to suit your body type, personal style and budget</h5>
     <p>We suggest fashion, beauty and personal care products based on your profile. To help us style you better, you can also share images with us via the chat feature.</p> </div>
      </div>
     <div class="col-md-1">
       <div class="box">
       <img src="<?php echo base_url(); ?>assets/images/about/about-img-3.png">
       <h5>Shop instantly with our 'Buy Now' feature</h5>
     <p>Buy what you love with the click of a button or add to your wishlist and save it for later. <br/>

     PS: We send prompt notifications when the brand you want goes on sale!</p> </div>
      </div>
     <div class="col-md-1">
       <div class="box">
       <img src="<?php echo base_url(); ?>assets/images/about/about-img-4.png">
       <h5>Live chat with our celebrity stylists to get a quick fix for your outfits</h5>
     <p>Get styling tips and learn how to put together your existing wardrobe for absolutely free!
	 <!--They are there for you 24X7 and for absolutely free! So, get daily how-to tips and creatively put together what you already own.--></p> </div>
      </div>
     <div class="col-md-1">
       <div class="box">
       <img src="<?php echo base_url(); ?>assets/images/about/about-img-5.png">
       <h5>Find out where your favourite celebrities shop</h5>
     <p>Love a particular on-screen look? We tell you where to get it. That too, the pocket-friendly way!</p>
 </div>
  </div>
     <div class="col-md-1">
       <div class="box">
       <img src="<?php echo base_url(); ?>assets/images/about/about-img-6.png">
       <h5>Read expert brand reviews by fashion industry insiders</h5>
     <p>Get honest feedback on stores' latest collections, their services and lots more.</p>
 </div>
  </div>
     <div class="col-md-1">
       <div class="box">
       <img src="<?php echo base_url(); ?>assets/images/about/about-img-7.png">
       <h5>Rate your shopping experience</h5>
     <p>Share what works for you with other shopaholics!</p>
 </div>
  </div>
  </div>

    </div>
    <div class="page-header about-header">
      <h1>Our <span>Stylists</span></h1>
  </div>

  <div class="stylist-section">
    <div class="row">

      <?php if(!empty($stylist_data)) {
        // $m = 0;
        foreach($stylist_data as $val){
        /*  if($m ==0 || $m%5 == 0){
            $add_class = 'col-md-offset-2';
          }else{
            $add_class = '';
          }*/
          ?>
          <div class="col-md-4">
            <div class="stylist-box">
              <div class="row">
                <div class="col-md-6">
                  <div class="stylist-info-box">
                    <div class="title">
                      <?php echo $val['stylist_name']; ?>
                    </div>
                    <div class="short-info">
                      <?php echo substr($val['style_tip'],0,100); ?>
                    </div>
                    <div class="read-more"><a href="<?php echo base_url(); ?>stylist/stylist-detail/<?php echo $val['slug']; ?>">(Read More)</a></div>
                    <span class="stylist-caret"></span>
                  </div>
                </div>
                <div class="col-md-6">
                  <a href="<?php echo base_url(); ?>stylist/stylist-detail/<?php echo $val['slug']; ?>">
                    <img src="<?php echo base_url(); ?>assets/images/about/<?php echo $val['img_name']; ?>">
                  </a>
                </div>
              </div>
            </div>
          </div>
          <?php // $m++;
         }
        } ?>

          <div class="col-md-12">
            <div class="hr">
            </div>

          </div>
        </div>
      </div>
      <div class="founder-section">
        <div class="row">
          <div class="col-md-6">
            <div class="founder-box first">
              <div class="row">
                <div class="col-md-5">
              <div class="image">
                <img src="<?php echo base_url(); ?>assets/images/about/dhimaan.jpg">
              </div>
                </div>
              <div class="col-md-7">
                  <div class="founder-box-inner">
              <div class="title">Dhimaan <span>Shah</span>
              </div>
              <div class="designation">Founder Partner</div>
              <div class="short-info">
                A voracious reader and a serious coffee addict! Loves all things classic.
              </div>
            </div>
            </div>
              </div>
          </div>
            </div>
          <div class="col-md-6">
            <div class="founder-box">
              <div class="row">
          <div class="col-md-7">
              <div class="founder-box-inner">
              <div class="title">Archana <span>Walavalkar</span></span>
              </div>
              <div class="designation">Founder Partner</div>
              <div class="short-info">
                Food lover who is known to start all her meals with dessert! A pro at switching from sneakers to stilettos in seconds.
              </div>
            </div>
            </div>
            <div class="col-md-5">
          <div class="image">
            <img src="<?php echo base_url(); ?>assets/images/about/archana-1.jpg" style="margin-right:-1px;">
          </div>
        </div>
          </div>
        </div>
      </div>
        </div>
      </div>
    </div>
  </div>


  <div class="about-contact-wrp">
    <div class="container">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <h3>We <span>would</span> love <span>to</span> hear <span>from</span> you.</h3>
          <form name="frmContact" id="frmContact" method="post" action="">
            <div class="row">
              <div class="col-md-5">

                <div class="form-group">
                  <input type="text" class="form-control field-name" name="contact_name" id="contact_name" placeholder="Name">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control email-name" name="email_id" id="email_id" placeholder="Email">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control field-name" name="subject" id="subject" placeholder="Subject">
                </div>
                <div class="form-group">
                  <textarea  class="form-control field-name" name="message" id="message" placeholder="Message"></textarea>
                </div>
                <div class="form-group">
                  <button class="btn" name="subContact" id="subContact">Send Message</button>
                </div>
                <div id="contact_us_msg" class="form-group success"></div>
              </div>
                <div class="col-md-1"></div>
              <div class="col-md-6">
                <div class="contact-info">

                  <ul class="contact-info-list">
                    <li class="phone">
                     <div class="pull-left" style="margin-bottom:30px; margin-right:10px;">
                     <i class="fa fa-map-marker"></i></div>
                   <!--  2nd Floor, Saurabh Building,<br/> Modi Estate, L. B. S. Road, Mumbai - 400086, <br/>Maharashtra, India.-->
					 
					 Impression House,<br/> First  Floor, 42A, G. D. Ambekar Marg, Wadala,<br/> Mumbai - 400031, Maharashtra, India.
					 
                      </li>
                      <li class="phone">
                      <i class="fa fa-phone"></i> 					
						<span onclick="window.open('tel:022-61738500');">
						<span>Phone: +91-22-61738500</span>
						</span>
						
                    </li>
                    <li class="email">

                    <div  class="email">
                        <i class="fa fa-envelope"></i> Send us an Email
                      </div>
                      <div  class="email2">
                        <strong>For business or seller enquiries:</strong> <a href="mailto:business@stylecracker.com">business@stylecracker.com</a>
                      </div>

                      <div  class="email2">
                        <strong>For order related enquiries:</strong> <a href="mailto:customersupport@stylecracker.com">customersupport@stylecracker.com</a>
                      </div>
                      <div  class="email2">
                        <strong>For career opportunities:</strong> <a href="mailto:careers@stylecracker.com">careers@stylecracker.com</a>
                      </div>
                      <div  class="email2">
                        <strong>For general enquiries:</strong> <a href="mailto:support@stylecracker.com">support@stylecracker.com</a>
                      </div>


                    </li>

                  <!--  <li class="share-buttons">
                      <ul class="social-buttons">
                        <li><a href="https://www.facebook.com/StyleCracker" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/Style_Cracker" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://instagram.com/stylecracker" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="https://www.pinterest.com/stylecracker" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                      </ul>
                    </li>-->
                  </ul>


                </div>
              </div>
            </div>
          </form>
        </div>

      </div>
    </div>
  </div>


  <script type="text/Javascript">

  function sc_contactus(){

    var contact_name = $('#contact_name').val();
    var email_id = $('#email_id').val();
    var subject = $('#subject').val();
    var message = $('#message').val();

    if(contact_name!='' && email_id!='' && subject!='' && message!=''){

      $.ajax({
        url: '<?php echo base_url(); ?>schome/contact_us',
        type: 'post',
        data: {'type' : 'contact_us','contact_name':contact_name,'email_id':email_id, 'subject':subject,'message':message },
        success: function(data, status) {

          $('#contact_us_msg').html('Thank you.');
          $('#contact_name').val('');
          $('#email_id').val('');
          $('#subject').val('');
          $('#message').val('');

        },
        error: function(xhr, desc, err) {
          // console.log(err);
        }
      });

    }

  }
  </script>
