<div class="page-promo-signup page-promo-signup2">
<div class="page-header">
  <div class="container">
    <h1>StyleCracker is India's only personalized styling platform.</h1>
    <h2>We transform the way you shop. <span>Sign Up</span> for free and get styled.</h2>
  </div>
</div>
<div class="signup-form-wrp">
<div class="container">
<form id="sc_Signuppromo2" method="post" action="">
  <div class="row">
<div class="col-md-3">
  <div class="form-control-wrp">
   <input type="text" name="sc_firstname2"  id="sc_firstname2" value="" placeholder="First Name" class="form-input111 form-control" >
   <label id="sc_firstname2_error1" class="error" ></label>
 </div>
</div>
<div class="col-md-3">
  <div class="form-control-wrp">
  <input type="text" name="sc_lastname2" id="sc_lastname2" value="" placeholder="Last Name" class="form-input111 form-control" >
   <label id="sc_lastname2_error1" class="error" ></label>
 </div>
</div>
<div class="col-md-3">
<div class="form-control-wrp">
  <input type="text" name="sc_mobile_promo2" id="sc_mobile_promo2" class="form-input111 form-control" value="" placeholder="Mobile" />
  <label id="sc_mobile_promo2_error1" class="error"></label>
</div>
</div>
<div class="col-md-3">
<div class="form-control-wrp">
  <input type="text" name="sc_emailid_promo2" id="sc_emailid_promo2" class="form-input111 form-control" value="" placeholder="Email" />
  <label id="sc_emailid_promo2_error1" class="error"></label>
</div>
</div>


  <div class="col-md-3">
<div class="form-control-wrp">
  <input type="password" name="sc_password_promo2" id="sc_password_promo2" class="form-input11 form-control" value="" placeholder="Password" />
  <label id="sc_password_promo2_error1" class="error" ></label>
</div>
</div>
<div class="col-md-3">
<div class="form-control-wrp">
  <select class="form-control" id="sc_gender2" name="sc_gender2">
    <option disabled="" value="" selected>
    Select Gender
    </option>
    <option  value="1">
    Female
  </option>
    <option value="2">
    Male
  </option>
</select>
<label id="sc_gender2_error1" class="error" ></label>
</div>
</div>
<div class="col-md-3">
<div class="sign-up-btn-wrp">
  <button type="submit" id="sc_signuppromo22" name="sc_signuppromo22" class="btn btn-primary btn-block">Sign Up</button>
</div>
</div>
</div>
<div style="text-align:center; position:relative;">
<div style="padding:0px;margin:0px;margin-left: 30px;font-size: 17px; position:absolute; top:-18px;" class="fa-loader-wrp-products">
   <i class="fa fa-circle-o-notch fa-spin-custom"></i>
 </div>
 </div>
<div id="sc_signup_error1" class="error"></div>
<div id="sc_signup_msg" class="success"></div>

</form>

</div>
</div>
</div>
<div class="content-wrp page-promo-signup2" style="padding: 20px 0;">
  <div class="container">

      <div class="row">
        <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
            <div class="ytube-video-wrp">
          <iframe width="764" height="430" src="https://www.youtube.com/embed/3-U5kufNk7A?rel=0" frameborder="0" allowfullscreen></iframe>
        <!--  <img src="<?php echo base_url(); ?>assets/images/promo/promo-signup-video.jpg">-->
        </div>
        </div>
      </div>

</div>
</div>
</div>
<style>
.page-promo-signup2 .signup-form-wrp .form-control-wrp{position: relative; margin-bottom: 20px;}
.form-control-wrp label.error{position: absolute; top:100%;}

</style>
<script type="text/Javascript">

	function sc_signInp2(){
		var sc_emailid = $('#sc_emailid_promo2').val();
		var sc_password = $('#sc_password_promo2').val();
		var sc_username = $('#sc_username_promo2').val();
    var sc_firstname = $('#sc_firstname2').val();
    var sc_lastname = $('#sc_lastname2').val();
    var sc_gender = $('#sc_gender2').val();
    var sc_mobile = $('#sc_mobile_promo2').val();

    $("#sc_signuppromo22").addClass( "disabled" );
    $(".fa-loader-wrp-products").css({"display":"inline"});

	  if(sc_emailid!='' && sc_password!='' && sc_username!='' && sc_gender!='' && sc_mobile!='' ){

		  $.ajax({
			url: '<?php echo base_url(); ?>Schome/register_user',
			type: 'post',
			data: {'type' : 'get_signin','sc_firstname':sc_firstname,'sc_lastname':sc_lastname,'sc_gender':sc_gender,'sc_email_id':sc_emailid,'sc_password':sc_password,'sc_username_name':sc_username,'registered_from':'comp', 'sc_mobile':sc_mobile },
			success: function(data, status) {

			  if(data!=''){
  				$('#sc_signup_error1').html(data);
          $("#sc_signuppromo22").removeClass( "disabled" );
          $(".fa-loader-wrp-products").css({"display":"none"});
			  }else{
          $('#sc_signup_error1').html('');
          $('#sc_signup_msg').html('Sign-Up Successful');
				window.location.href="<?php echo base_url(); ?>Schome/pa";
			  }

			  return false;
			  event.preventDefault();

			},
			error: function(xhr, desc, err) {
			  // console.log(err);
			}
		  });

		}

	  }


  </script>
