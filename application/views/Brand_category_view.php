<?php
$user_name = $this->session->userdata('user_name');
$email = $this->session->userdata('email');
$user_id = $this->session->userdata('user_id');
$bucket = $this->session->userdata('bucket');
$brand_name = $this->uri->segment(3);
$category_slug = $this->uri->segment(4);
// echo "<pre>";print_r($all_data);exit;
?>
<div class="sc-sidebar-overlay-sm"></div>
<div class="page-all-products content-wrp">
<div class="container">
<ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>rakhi-special">Rakhi Special</a></li>
       </ol>
										
<div class="title-section">
          <h1 class="title"><span>Products</span></h1> 
    </div>

 


      <?php if(!empty($all_data['products'])) { shuffle($all_data['products']); ?>
    <div class="grid grid-products-col-4 items-wrp items-section">
      <div class="row grid-items-wrp" id="get_brand_cat_products">
      <?php foreach ($all_data['products'] as $value) { ?>
      <div class="col-md-3 col-sm-4 col-xs-6 grid-item">
        <div class="item item-product">
          <div class="item-img item-hover">
            <div class="item-img-inner">
              <?php
                $short_url = base_url().'product/details/'.$value['slug'].'-'.$value['id'];
                $img_title_alt = str_replace('#','',$value['name']);
                $img_url = $this->config->item('product_image').$value['image'];
              ?>
                 <a href="<?php echo $short_url; ?>"  >
                  <?php if(file_exists($this->config->item('products_H340').$value['image'])) { ?>
                 <img class="lazy" data-original="<?php echo $this->config->item('products_340').$value['image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  />
                 <?php }else{ ?>
                  <img class="lazy" data-original="<?php echo $this->config->item('products_310').$value['image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  />

                  <?php } ?>
                 </a>
               </div>
              <div class="item-look-hover">

              <?php if($this->Allproducts_model->product_stock_check($value['id']) != 1){ ?>
                <a href="<?php echo $short_url; ?>" onclick="ga('send', 'event', 'Product', 'clicked', '<?php echo $value['name']; ?>'); _beatout_view_product('<?php echo $value['id']; ?>','<?php echo $user_id; ?>','<?php echo $value['price']; ?>','<?php echo $value['name']; ?>','','view');" title="<?php echo $img_title_alt; ?>" >
                <span class="btn-out-of-stock">Sold Out</span>
                </a>
                <?php }else{ ?>
                  <a href="<?php echo $short_url; ?>" onclick="ga('send', 'event', 'Product', 'clicked', '<?php echo $value['name']; ?>'); _beatout_view_product('<?php echo $value['id']; ?>','<?php echo $user_id; ?>','<?php echo $value['price']; ?>','<?php echo $value['name']; ?>','','view');" title="<?php echo $img_title_alt; ?>" >
                  <span class="btn btn-primary"><?php echo $this->lang->line('get_this'); ?></span>
                  </a>
              <?php } ?>
              </div>

            </div>
            <div class="item-desc">
            <?php $lookPrdName = stripslashes($value['name']); if (strlen($lookPrdName) >= 84){ $stringCut = substr($lookPrdName, 0, 81); } ?>
              <div class="item-title"><a title="<?php echo $img_title_alt; ?>" href="<?php echo $short_url; ?>"  ><?php echo ucwords(strtolower($lookPrdName)); ?></a>
              </div>
              <div class="item-brand">By <span><?php echo $this->Allproducts_model->get_brand_name($value['brand_id']); ?></span></div>
              <div class="item-price-wrp">
                <div class="item-price">
                  <?php if($this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price']>0) { ?>
                  <span class="mrp-price text-strike"><i class="fa fa-inr"></i><?php echo $value['price']; ?></span>
                  <span class="price"><i class="fa fa-inr"></i><?php echo $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discounted_product_price'] : ''; ?></span>
                  <span class="discount-price">(<?php echo $this->Product_desc_model->get_productDiscount_price($value['id'],$value['brand_id'])['discount_percent']; ?>% OFF)</span>
                  <?php }else{ ?>
                  <span class="price"><i class="fa fa-inr"></i> <?php echo $value['price']; ?></span>
                  <?php } ?>
                </div>
                <?php
                      if($this->session->userdata('user_id')){
                        if($this->home_model->get_fav_or_not('product',$value['id']) == 0){ ?>
                          <div class="user-likes"><a title="Add to Wish List" id="wishlist<?php echo $value['id']; ?>" onclick="add_to_fav('product','<?php echo $value['id']; ?>','<?php echo $user_id; ?>','<?php echo $value['price']; ?>','<?php echo $value['name']; ?>','<?php echo @$value['product_cat_id']; ?>','add_to_wishlist'); ga('send', 'event', 'Product', 'wishlist', '<?php echo $value['name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love"></i></a></div>
                        <?php }else{ ?>
                          <div class="user-likes"><a title="Remove from Wish List" id="wishlist<?php echo $value['id']; ?>" onclick="add_to_fav('product','<?php echo $value['id']; ?>','<?php echo $user_id; ?>','<?php echo $value['price']; ?>','<?php echo $value['name']; ?>','<?php echo @$value['product_cat_id']; ?>','remove_from_wishlist'); ga('send', 'event', 'Product', 'wishlist', '<?php echo $value['name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love active"></i></a></div>
                        <?php } }else{ ?>
                        <div class="user-likes"><a onclick="ga('send', 'event', 'Sign Up', 'clicked', 'favorite');" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a></div>
                      <?php }  ?>
              </div>
            </div>
          </div>
        </div>
       <?php }?>

      </div>
    </div>
    <?php }else{ ?>
        <div class="grid-no-products"> No Products Found </div>

    <?php  } ?>

     <input type="hidden" name="offset" id="offset" value="1">
	 <input type="hidden" name="brand_name" id="brand_name" value="<?php echo $brand_name; ?>">
	 <input type="hidden" name="category_slug" id="category_slug" value="<?php echo $category_slug; ?>">

<div class="fa-loader-wrp-outer">
    <div style="display: none;" class="fa-loader-wrp-products">
      <i class="fa fa-circle-o-notch fa-spin-custom"></i> Loading...
    </div>
 </div>

  </div>
</div>

<?php //echo @$pagination; ?>

