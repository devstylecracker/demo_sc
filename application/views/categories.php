<div class="sc-sidebar-overlay-sm"></div>
<div class="page-common content-wrp">
  <div class="container">
    <div class="filter-btns-wrp-sm clearfix">
    <div class="btn btn-secondary btn-sm btn-filter-by">
      Filter
    </div>
    <!--div class="filterbar filterbar-sm">
      <select name="select_type" id="select_type" class="selectpicker">
        <option disabled="disabled">Sort by</option>
        <option value="0">Latest</option>
        <option value="looks">Looks</option>
        <option value="sc-live-posts">SC Live</option>
        <option value="festive">Festive</option>
        <option value="bridal">Bridal</option>
        <option value="sale">On Sale</option>
      </select>
  </div-->
    </div>

    <ol class="breadcrumb">
      <li><a href="<?php echo base_url().$page_filter_name; ?>">Home</a></li>
      <li><a href="<?php echo base_url().'category-list/'.$page_filter_name; ?>"><?php echo ucfirst($page_filter_name); ?></a></li>
      <li class="active"><?php echo ucfirst($nav_cat_name); ?></li>
    </ol>
    <div class="row layout-2-col">
      <div class="col-md-2 layout-2-col sidebar-filter-wrp-sm">
        <?php echo $left_filter; ?>
      </div>
    </div>
  <div class="col-md-10  layout-2-col">
    <div class="filter-selction-box" id="filter-selction-box">
        <span class="filter-tag clear-all" onclick="scClearAll();">Clear All</span>
    </div>
    <div class="filterbar-wrp filterbar-wrp-md">
        <h2 class="filter-result-title" id="filter-result-title"><?php echo $products_total; ?> Product<?php echo $products_total > 1 ? 's' : ''; ?></h2>
        <!--div class="filterbar">
          <span class="filter-label">Sort by: </span>
          <select name="select_type" id="select_type" class="selectpicker">
            <option value="0">Latest</option>
            <option value="looks">Looks</option>
            <option value="sc-live-posts">SC Live</option>
            <option value="festive">Festive</option>
            <option value="bridal">Bridal</option>
            <option value="sale">On Sale</option>
          </select>
      </div-->
    </div>

     <?php if(!empty($looks_data)) { ?>
    <div class="grid items-wrp items-section hide">
      <div class="title-section">
          <h1 class="title"><span><?php echo @$this->lang->line('category_caption_one'); ?></span></h1>
          <a href="<?php echo base_url(); ?>looks/<?php echo $page_filter_name; ?>" class="link-view-all">View All</a>
      </div>
      <div class="row grid-items-wrp">
      <?php foreach ($looks_data as $value) { ?>
      <div class="col-md-4 col-sm-6 grid-item">
        <div class="item">
          <div class="item-img item-hover">
              <?php
                $short_url = base_url().'looks/look-details/'.$value['slug'];
                $img_title_alt = str_replace('#','',$value['name']);

                if($value['look_type'] == 1){ ?>
                  <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>"><img src="<?php echo $this->config->item('sc_look_image_url').$value['image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                <?php $img_url = $this->config->item('sc_look_image_url').$value['image'];
                  }else if($value['look_type'] == 2 || $value['look_type'] == 4 || $value['look_type'] == 6){ ?>
                  <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>"><img src="<?php echo $this->config->item('sc_street_style_image_url').$value['look_image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                <?php $img_url = $this->config->item('sc_street_style_image_url').$value['look_image'];
                }else if($value['look_type'] == 3){ ?>
                <a href="<?php echo base_url(); ?>looks/look-details/<?php echo $value['slug']; ?>"><img src="<?php echo $this->config->item('sc_promotional_look_image').$value['product_img']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                <?php $img_url = $this->config->item('sc_promotional_look_image').$val['product_img'];
                }
                ?>

            <div class="item-look-hover">
              <a href="<?php echo $short_url; ?>" title="<?php echo $img_title_alt; ?>" target="_blank"><span class="btn btn-primary"><?php echo $this->lang->line('get_this'); ?></span></a>
            </div>
            </div>
            <div class="item-desc">
              <div class="item-title"><a title="<?php echo $img_title_alt; ?>" href="<?php echo $short_url; ?>" target="_blank"><?php echo $value['name']; ?></a>
              </div>
              <div class="item-media">
                <ul class="social-buttons">
                  <li><a onclick="_targetFacebookShare('<?php echo $value['name']; ?>','<?php echo $short_url; ?>','<?php echo $img_url; ?>','<?php echo $value['id']; ?>');"><i class="fa fa-facebook"></i></a></li>
                  <li><a onclick="_targetTweet('<?php echo $short_url; ?>','<?php echo $value['name']; ?>','<?php echo $value['id']; ?>');"><i class="fa fa-twitter"></i></a></li>
                  <li><a onclick="_targetPinterest('<?php echo $short_url; ?>','<?php echo $value['name']; ?>','<?php echo $value['id']; ?>','<?php echo $img_url; ?>');"><i class="fa fa-pinterest"></i></a></li>
                </ul>
                <?php
                      if($this->session->userdata('user_id')){
                        if($this->home_model->get_look_fav_or_not($value['id']) == 0){ ?>
                          <div class="user-likes"><a target="_blank" title="Add to Wish List" id="whislist<?php echo $value['id']; ?>" onclick="add_to_wishlist(<?php echo $value['id']; ?>);"><i class="ic sc-love"></i></a></div>
                        <?php }else{ ?>
                          <div class="user-likes"><a target="_blank" title="Remove from Wish List" id="whislist<?php echo $value['id']; ?>" onclick="remove_from_wishlist(<?php echo $value['id']; ?>);"><i class="ic sc-love active"></i></a></div>
                        <?php } }else{ ?>
                        <div class="user-likes"><a target="_blank" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a></div>
                      <?php }
                      ?>
                </div>
              </div>
            </div>
          </div>
       <?php } ?>
      </div>
    </div>
    <?php } ?>

      <?php if(!empty($products_data)) { ?>
    <div class="grid items-wrp items-section">
      <div class="title-section">
          <h1 class="title"><span><?php echo @$this->lang->line('category_caption_two'); ?></span></h1>
      </div>
      <div class="row grid-items-wrp" id="get_all_cat_products">
      <?php foreach ($products_data as $value) { ?>
      <div class="col-md-4 col-sm-6 grid-item">
        <div class="item item-product">
          <div class="item-img item-hover">
            <div class="item-img-inner">
              <?php
                $short_url = base_url().'product/details/'.$value['id'];
                $img_title_alt = str_replace('#','',$value['name']);
                $img_url = $this->config->item('product_image_thumb').$value['image'];
              ?>
                 <a href="<?php echo base_url(); ?>product/details/'<?php echo $value['id']; ?>"><img src="<?php echo $this->config->item('product_image_thumb').$value['image']; ?>" alt="<?php echo $img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"  /></a>
                <div class="item-look-hover">
                  <a href="<?php echo $short_url; ?>" title="<?php echo $img_title_alt; ?>" target="_blank"><span class="btn btn-primary"><?php echo $this->lang->line('get_this'); ?></span></a>
                </div>
            </div>
              </div>
            <div class="item-desc">
              <div class="item-title"><a title="<?php echo $img_title_alt; ?>" href="<?php echo $short_url; ?>" target="_blank"><?php echo $value['name']; ?></a>
              </div>
              <div class="item-media">
                <div class="item-price">
                  <span class="price"><i class="fa fa-inr"></i><?php echo $value['price']; ?></span>
                </div>
                <?php
                      if($this->session->userdata('user_id')){
                        if($this->home_model->get_fav_or_not('product',$value['id']) == 0){ ?>
                          <div class="user-likes"><a target="_blank" title="Add to Wish List" id="wishlist<?php echo $value['id']; ?>" onclick="add_to_fav('product','<?php echo $value['id']; ?>','');"><i class="ic sc-love"></i></a></div>
                        <?php }else{ ?>
                          <div class="user-likes"><a target="_blank" title="Remove from Wish List" id="wishlist<?php echo $value['id']; ?>" onclick="add_to_fav('product',<?php echo $value['id']; ?>,'');"><i class="ic sc-love active"></i></a></div>
                        <?php } }else{ ?>
                        <div class="user-likes"><a target="_blank" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a></div>
                      <?php }
                      ?>
              </div>
            </div>
          </div>
        </div>
       <?php } ?>
       <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>">
<input type="hidden" name="look_type" id="look_type" value="<?php echo $page_filter_name; ?>">
<input type="hidden" name="category_slug" id="category_slug" value="<?php echo $category_slug; ?>">
<input type="hidden" name="category_level" id="category_level" value="<?php echo $category_level; ?>">
<input type="hidden" name="products_total" id="products_total" value="<?php echo $products_total; ?>">
<input type="hidden" name="type" id="type" value="">
<input type="hidden" name="type_data" id="type_data" value="">
<input type="hidden" name="color_data" id="color_data" value="">
      </div>
    </div>
    <?php } ?>
  </div>
<?php echo $pagination; ?>
</div>
</div>
