    <script type="text/Javascript">
    var cache = new ajaxCache('GET', true, 1800000);
    var stylist_offset,street_offset;

    $(function() {

      var all_offset = "stylist_offset=0&street_offset=0&filter=0&page_type=profile";
      
      get_looks(all_offset);

      $( "#select_type" ).change(function() {
        $('#show_looks').html('');
        $('#show_looks').html('<div class="grid-sizer"></div>');

        all_offset = "stylist_offset=0&street_offset=0&page_type=profile&filter="+$('#select_type').val();

        get_looks(all_offset);
      });

    });

    var isTriggered = false;
    $(window).scroll(function() {
      var intTriggerScroll = 50;
      var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());

      if(intCurrentScrollPos<intTriggerScroll && !isTriggered){


        if($('#stylist_offset').length > 0){
          stylist_offset = $('#stylist_offset').val();
        }else{
          stylist_offset = '-1';
        }

        if($('#street_offset').length > 0){
          street_offset = $('#street_offset').val();
        }else{
          street_offset = '-1';
        }

        all_offset = "stylist_offset="+stylist_offset+"&street_offset="+street_offset+"&page_type=profile&filter="+$('#select_type').val();
        isTriggered=true;

        if($('#stylist_offset').length >0 || $('#street_offset').length >0){
          get_looks(all_offset);
        }
      }

    });


    function get_looks(viewData)
    {

      console.log("page-overlay");
      $(".page-overlay").show();
      $(".fa-loader-wrp").show();
      
      $('#look_loader_img').css('display', 'inline-block');
      var ajax_path = '<?php echo base_url(); ?>Schome/get_all_stylist_looks';

      if(cache.on)
      {
        var cachedResponse = cache.get(ajax_path, [viewData]);
        if(cachedResponse !== false)
        {
          // update website
          update_website(cachedResponse); // We just avoided one ajax request
          //initialiceMasonry();
          add_hover();
            $(".page-overlay").hide();
            $(".fa-loader-wrp").hide();
          return true;
        }
      }

      $.ajax({
        url: ajax_path,
        type: 'GET',
        async: true,
        data: viewData,
        cache :true,
        success: function(response) {
          if(cache.on) cache.put(ajax_path, viewData, response); // record the new response

          update_website(response);
          //initialiceMasonry();
          add_hover();
            $(".page-overlay").hide();
            $(".fa-loader-wrp").hide();
        },
        error: function(xhr) {
            $(".page-overlay").hide();
            $(".fa-loader-wrp").hide();
        }
      });
      return true;
    }

    function update_website(response){

      if($('#stylist_offset').length >0 ){ $('#stylist_offset').remove(); }
      if($('#street_offset').length >0 ){ $('#street_offset').remove(); }

      $('#show_looks').append(response);
      $('#look_loader_img').hide();
      isTriggered = false;

    }
<?php 
    /*function initialiceMasonry(){
        var $container = $('.grid-items-wrp');
            $container.masonry({
                isInitLayout : true,
                itemSelector: '.grid-item',
                isAnimated: false,
                percentPosition: true

            });

            $container.imagesLoaded(function() {
              $( $container ).masonry( 'reloadItems' );
              $( $container ).masonry( 'layout' );
            });
          }

    */      
?>
    </script>
<?php 
    /*<!-- / wardrobe popup-->


<!--
        <link rel="stylesheet" href="assets/css/blueimp-gallery.min.css">
    <link rel="stylesheet" href="assets/css/blueimp-gallery.min.css">

    <script src="assets/js/jquery.blueimp-gallery.min.js"></script>
    <script src="assets/js/bootstrap-image-gallery.min.js"></script> -->*/
    ?>
  <script>
$(document).ready(function() {
  //## UserTrack: Page ID=========================================
  //geolocateUser();
  document.cookie="pid=9";
  /*Code ends*/
 document.getElementById('feed').onclick = function (event) {
      event = event || window.event;
      var target = event.target || event.srcElement,
          link = target.src ? target.parentNode : target,
          options = {index: link, event: event},
          links = this.getElementsByTagName('a');
      //blueimp.Gallery(links, options);
  };

});
  </script>
    <!-- / wardrobe popup-->