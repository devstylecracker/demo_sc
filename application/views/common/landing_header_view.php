<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $this->seo_title!='' ? $this->seo_title : 'Stylecracker' ; ?></title>
  <meta content="<?php echo $this->seo_desc; ?>" name="Description">
  <meta content="<?php echo $this->seo_keyword; ?>" name="Keywords">
  <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/fav-icon.jpg">
  <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/flexslider.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/bootstrap-select.min.css" rel="stylesheet">

  <link href="<?php echo base_url(); ?>assets/css/custom.css?v=1.224" rel="stylesheet">
  <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300' rel='stylesheet' type='text/css'>-->
  <link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,300' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">

     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <!--script src="<?php echo base_url(); ?>assets/js/jquery.sticky.js"></script-->
    <script src="<?php echo base_url(); ?>assets/js/jquery.flexslider-min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/ajaxCache-0.2.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.masonry.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/imagesloaded.pkgd.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/bootstrap-select.min.js"></script>

    <script async src="<?php echo base_url(); ?>assets/js/custom.js?v=1.04"></script>
    <script async src="<?php echo base_url(); ?>assets/js/analytics.js"></script>
    <script async src="<?php echo base_url(); ?>assets/js/sociallogin.js"></script>
    <!-- added for google location tracking-->
    <script src="https://maps.googleapis.com/maps/api/js?v=3&sensor=true"></script>
    <script src="<?php echo base_url(); ?>assets/js/geoloc.js"></script>
    <!--geolocateUser(); -->
    <!-- code ends -->
    <!-- Added to track GA values -->
     <!--script type="text/javascript" src="<?php echo base_url(); ?>assets/js/gaCookies.js"></script-->
<!-- Added for user action tracking -->
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sc-tracker.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

 <script type="text/javascript"> var CE_SNAPSHOT_NAME = "Looks"; </script>

<script type="text/javascript">

setTimeout(function(){var a=document.createElement("script");

var b=document.getElementsByTagName("script")[0];

a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0040/4266.js?"+Math.floor(new Date().getTime()/3600000);

a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);

</script>


<!-- Facebook Pixel Code (add on 081015)-->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '1073963492614334');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1073963492614334&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->




  </head>
  <body>
   <!-- //FB Initialization
  // FB APP Note -->
    <div id="fb-root"></div>
    <script>
   window.fbAsyncInit = function() {
      FB.init({
      appId      : '1650358255198436',
      xfbml      : true,
      version    : 'v2.4'
      });
    };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
    </script>


<?php include('nav_view.php'); ?>
