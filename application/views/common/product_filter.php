<?php $segment_second_position = @$this->uri->segment(2); ?>
<?php
$color=unserialize(FILTER_COLOR);  $min_price ='0'; $max_price ='0';
$min_price = (int)@$all_data['filter']['price'][0]['min_price'];
$max_price = (int)@$all_data['filter']['price'][0]['max_price'];
?>
<div class="sidebar-filter is-sticky">
  <div class="sidebar-filter-title">
    Filter By <?php $url =  $_SERVER['REQUEST_URI'].'/'; ?>
  </div>
  <div class="sidebar-filter-inner" id="sc-filter-box">
  	 <div class="filter-box">

      <?php if($all_data['filter']['gender']!='') { ?>
          <div class="box">
        <div class="title clearfix scfiltertitle" data-toggle="collapse" data-target="#collapse-product-gender" aria-expanded="false" aria-controls="collapse-gender">
          <h6>Gender</h6>
          <span class="fa fa-minus"></span>
        </div>
        <div id="collapse-product-gender" class="collapse in custom-scrollbar" aria-expanded="true">
          <ul>
            <?php echo $all_data['filter']['gender']; ?>
          </ul>
          </div>
          </div>
      <?php } ?>


        <?php if(!empty($all_data['filter']['category'])) { ?>
          <div class="box">
        <div class="title clearfix scfiltertitle" data-toggle="collapse" data-target="#collapse-product-category" aria-expanded="false" aria-controls="collapse-gender">
          <h6>Category</h6>
          <span class="fa fa-minus"></span>
        </div>

        <div id="collapse-product-category" class="collapse in collapse-brands-inner custom-scrollbar" aria-expanded="true">

          <?php if($this->uri->segment(2)!=''){ ?>
          	<?php echo $all_data['filter']['category']; ?>
          <?php }else{ ?>
              <ul><li>Please select a Gender first</li></ul>
          <?php } ?>
           </div>
        </div>
         <?php } ?>


        <?php if(!empty($all_data['filter']['paid_brands'])){  ?>
          <div class="box">
        <div class="title clearfix scfiltertitle" data-toggle="collapse" data-target="#collapse-brands" aria-expanded="false" aria-controls="collapse-brands">
          <h6>Brands</h6>
          <span class="fa fa-plus"></span>
        </div>
        <div id="collapse-brands" class="collapse collapse-brands-inner custom-scrollbar " aria-expanded="true">

          <ul>
          <?php foreach($all_data['filter']['paid_brands'] as $val){ ?>
          	<li class="sccheckbox">
            <?php if($val['user_id'] == $all_data['filter_brand_id']){ ?>
            <input type="checkbox" name="brand" id="brand<?php echo $val['user_id']; ?>" class="filter-link" value="<?php echo $val['user_id']; ?>" checked data-attr="<?php echo $url; ?>" title="<?php echo $val['company_name']; ?>">
            <?php }else{ ?>
            <input type="checkbox" name="brand" id="brand<?php echo $val['user_id']; ?>" class="filter-link" value="<?php echo $val['user_id']; ?>" data-attr="<?php echo $url; ?>" title="<?php echo $val['company_name']; ?>">
            <?php } ?>
             <label for="brand<?php echo $val['user_id']; ?>"> <?php echo $val['company_name']; ?></label>
            </li>
          <?php } ?>
          </ul>

        </div>
          </div>
        <?php } ?>


        <?php if(!empty($all_data['filter']['color'])){ ?>
            <div class="box">
        <div class="title clearfix scfiltertitle" data-toggle="collapse" data-target="#collapse-colors" aria-expanded="false" aria-controls="collapse-brands">
          <h6>Color</h6>
          <span class="fa fa-plus"></span>
        </div>
        <div id="collapse-colors" class="collapse collapse-brands-inner custom-scrollbar " aria-expanded="true">
          <ul>
           <?php /*if(empty($all_data['filter']['color'])){
           foreach ($color as $key => $value) { $key_dis = explode('-', $key); ?>
              <li><label><input type="checkbox" name="color_name[]" label-for="<?php echo $value; ?>" value="<?php echo $key_dis[0]; ?>"><i class="sc-icon-color color-shade <?php echo strtolower($value); ?>" style="background:<?php echo @$key_dis[1]; ?>"></i><?php echo ucwords(strtolower($value)); ?></label></li>
            <?php } }else{ */
              foreach ($color as $key => $value) { $key_dis = explode('-', $key);
              if(in_array($key_dis[0], $all_data['filter']['color'])){
              ?>
              <li class="sccheckbox"><input type="checkbox" name="color_name[]" value="<?php echo $key_dis[0]; ?>"><!--i class="sc-icon-color color-shade <?php //echo strtolower($value); ?>" style="background:<?php echo @$key_dis[1]; ?>"></i--><label for="<?php echo $key_dis[0]; ?>"><?php echo ucwords(strtolower($value)); ?></label></li>
            <?php } } /*}*/ ?>
          </ul>
        </div>
          </div>
        <?php } ?>


         <?php if($max_price>0 && $min_price>0) { ?>
             <div class="box">
            <div class="title clearfix scfiltertitle" data-toggle="collapse" data-target="#collapse-price" aria-expanded="false" aria-controls="collapse-price">
              <h6>Price</h6><span class="filter-clear" id="price_clear" style="display:none;" onclick="clear_price();">Clear</span>
              <span class="fa fa-plus"></span>
            </div>
            <div id="collapse-price" class="collapse-price collapse" aria-expanded="false">
               <div class="custom-scrollbar">
            <ul>
            <?php if($max_price >= 999) { ?>
              <li class="scradio">
                <input type="radio" value="1" name="price_filter" id="price_filter1" label-for="0 - 999">
                <label for="price_filter1"> 0 - 999 </label>
              </li>
            <?php } ?>
            <?php if($max_price >=1999) { ?>
              <li class="scradio">
              <input type="radio" value="2" name="price_filter" id="price_filter2" label-for="1000 - 1999">
                <label for="price_filter2">1000 - 1999</label>
              </li>
            <?php } ?>
            <?php if($max_price >=3499) { ?>
              <li class="scradio">
              <input type="radio" value="3" name="price_filter" id="price_filter3" label-for="2000- 3499">
                <label for="price_filter3">2000 - 3499</label>
              </li>
              <?php } ?>
              <?php if($max_price >=4999) { ?>
              <li class="scradio">
              <input type="radio" value="4" name="price_filter" id="price_filter4" label-for="3500 - 4999 ">
                <label for="price_filter4">3500 - 4999</label>
              </li>
               <?php } ?>
              <?php if($max_price > 5000) { ?>
              <li class="scradio">
                <input type="radio" value="5" name="price_filter" id="price_filter5" label-for="5000 +">
                <label for="price_filter5">5000 +</label>
              </li>
              <?php } ?>
            </ul>
            </div>
            <div class="price-range">
              <div class="text-1">
                Enter a price range
              </div>
              <div class="form-group">
                <input type="text" class="form-control input-sm" name="min_price" id="min_price" value="<?php echo $min_price; ?>"> <span>-</span>
                <input type="text" class="form-control input-sm" name="max_price" id="max_price" value="<?php echo $max_price; ?>">
                <button class="btn btn-sm btn-secondary" id="go_button"> Go </button>
              </div>
            </div>
            </div>
             </div>
         <?php } ?>

          <?php
          if(!empty($all_data['filter']['global_attributes'])){
            foreach($all_data['filter']['global_attributes'] as $val){
              ?>
               <div class="box">
              <div class="title clearfix scfiltertitle" data-toggle="collapse" data-target="#<?php echo $val['attribute_slug']; ?>" aria-expanded="false" aria-controls="<?php echo $val['attribute_slug']; ?>">
                  <h6><?php echo $val['attribute_name']; ?></h6>
                  <span class="fa fa-plus"></span>
              </div>
              <div id="<?php echo $val['attribute_slug']; ?>" class="collapse scattributes-filter custom-scrollbar" aria-expanded="true">
               <ul>
               <?php foreach($this->Filter_model->attributes_values($val['id'],0,0) as $values){ ?>
                <li class="sccheckbox">
                  <input type="checkbox" name="brand" id="brand<?php echo $values['id']; ?>" class="filter-link" value="<?php echo $values['id']; ?>" data-attr="<?php echo $url; ?>" title="<?php echo $values['attr_value_name']; ?>">
                  <label for="brand<?php echo $values['id']; ?>"> <?php echo $values['attr_value_name']; ?></label>
                </li>
              <?php }  ?>
              </ul>
            </div>
              </div>
              <?php
            }

          }

          ?>

           <?php
          if(!empty($all_data['filter']['category_attributes'])){
            foreach($all_data['filter']['category_attributes'] as $val){
              ?>
                <div class="box">
              <div class="title clearfix scfiltertitle" data-toggle="collapse" data-target="#<?php echo $val['attribute_slug']; ?>" aria-expanded="false" aria-controls="<?php echo $val['attribute_slug']; ?>">
                  <h6><?php echo $val['attribute_name']; ?></h6>
                  <span class="fa fa-plus"></span>
              </div>
              <div id="<?php echo $val['attribute_slug']; ?>" class="collapse scattributes-filter custom-scrollbar" aria-expanded="true">
               <ul>
               <?php foreach($this->Filter_model->attributes_values($val['id'],$all_data['is_recommended'],$val['attribute_parent']) as $values){ ?>
                <li class="sccheckbox">
                  <input type="checkbox" name="brand" id="brand<?php echo $values['id']; ?>" class="filter-link" value="<?php echo $values['id']; ?>" data-attr="<?php echo $url; ?>" title="<?php echo $values['attr_value_name']; ?>">
                  <label for="brand<?php echo $values['id']; ?>"> <?php echo $values['attr_value_name']; ?></label>
                </li>
              <?php }  ?>
              </ul>
            </div>
          </div>
              <?php
            }

          }

          ?>

       <?php if(!empty($all_data['filter']['size']) && $this->uri->segment(3)!=''){  ?>
           <div class="box">
        <div class="title clearfix scfiltertitle" data-toggle="collapse" data-target="#collapse-size" aria-expanded="false" aria-controls="collapse-brands">
          <h6>Size</h6>
          <span class="fa fa-plus"></span>
        </div>
        <div id="collapse-size" class="collapse collapse-brands-inner custom-scrollbar " aria-expanded="true">
          <ul>
          <?php foreach($all_data['filter']['size'] as $val){ ?>
            <li class="sccheckbox">
            <input type="checkbox" name="brand" id="size<?php echo $val['id']; ?>" class="filter-link" value="<?php echo $val['id']; ?>" data-attr="<?php echo $url; ?>" title="<?php echo $val['size_text']; ?>">

             <label for="size<?php echo $val['id']; ?>"> <?php echo $val['size_text']; ?></label>
            </li>
          <?php } ?>
          </ul>
        </div>
        </div>
        <?php } ?>


          <!--div class="filter-box">
            <div class="title clearfix scfiltertitle" data-toggle="collapse" data-target="#collapse-discounts" aria-expanded="false" aria-controls="collapse-discounts">
              <h6>Discounts</h6><span class="filter-clear" id="discount_clear" style="display:none;" onclick="clear_discount();">Clear</span>
              <span class="fa fa-plus"></span>
            </div>
            <div id="collapse-discounts" class="collapse-discounts collapse" aria-expanded="false">
            <ul>
              <li>
                <label>
                <span class="sc-radio">
              <input type="radio" name="discount" id="discount" value="1" onchange="show_products();" label-for="Discount">
                </span>
              Discounted Products
                </label>
              </li>
              <li>
                <label>
                <span class="sc-radio">
              <input type="radio" name="discount" id="discount" value="2" onchange="show_products();" label-for="Non-Discount">
                </span>
              Non-discounted Products
            </label>
              </li>
            </ul>
            </div>
          </div-->

  </div>
</div>
