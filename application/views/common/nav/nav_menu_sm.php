  <!-- men submenu -->
<?php
  $menu_id = '';
  $mm_menu = array();
  if($type=='men')
  {
    $menu_id = 'nav_men_sm';
    $mm_menu = $this->data['megamenu_mens'];     
    $mm_image = base_url().'assets/images/menu/megamenu_img_men.jpeg'; 
  }else if($type=='women')
  {
     $menu_id = 'nav_women_sm';
     $mm_menu = $this->data['megamenu_womens'];    
     $mm_image = base_url().'assets/images/menu/megamenu_img_women.jpeg';
  }
?>

<div class="sc-menu-collapse-content">
  <div class="sub-menu-sm" id="<?php echo $menu_id;?>" >
      <div class="sub-menu-sm-inner">
<?php 

if(!empty($mm_menu))
{
        foreach($mm_menu as $key=>$val)
        {          
?>
          <div class="sc-menu-collapse-wrp">
              <div class="title <?php if(count($val['data'])> 1){ ?>sc-menu-collapse <?php } ?>"><a href="<?php echo base_url(); ?>all-products/<?php echo $type; ?>/variation/<?php echo @$val['slug']; ?>"><?php echo @$val['label']; ?></a></div>
              <ul class="sc-menu-collapse-content">
               <?php $i=0;
                  $count = sizeof($val);
                  if(!empty($val['data'])){
                foreach($val['data'] as $k => $v)
                {
                  if($i > 0){
               ?>
              <li><a href="<?php echo base_url(); ?>all-products/<?php echo $type; ?>/variations/<?php echo @$v[3]; ?>"><?php echo $v[2]; ?></a></li>
               <?php } $i++;
                } }
            ?>              
            </ul>
          </div>
<?php
        }
}
?>
          <div>
              <img src="<?php echo $mm_image; ?>">   
          </div>         
  </div>
</div>
</div>
