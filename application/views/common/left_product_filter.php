 <div class="sidebar-filter">
          <div class="sidebar-filter-title">
            Filter By
          </div>
            <div class="sidebar-filter-inner">
          <div class="filter-box">
            <div class="title clearfix">
              <h6>Category</h6>
              <span class="filter-collapsible fa fa-minus" data-toggle="collapse" data-target="#collapse-categories" aria-expanded="false" aria-controls="collapse-categories"></span>
            </div>
            <div id="collapse-categories" class="collapse-product-category collapse" aria-expanded="true">
            <ul>

              <?php if(!empty($filter_data)) {
                foreach($filter_data as $filter_values){ ?>
                <?php if($filter_values['slug'] == $category_slug){ ?>
                  <li class="level-2 active"><span class="level-2-title"><?php echo $filter_values['name']; ?></span>
                <?php }else{ ?>
                   <li class="level-2"><span class="level-2-title"><a href="<?php echo base_url(); ?>categories/<?php echo $page_filter_name; ?>/<?php echo @$filter_values['slug']; ?>/variations_type"><?php echo $filter_values['name']; ?></a></span>
                <?php } ?>
                      <?php if(!empty($filter_values['variation_values'])){ ?>
                      <ul>
                        <?php foreach($filter_values['variation_values'] as $val) { ?>
                        <?php if(@$val['name']!='' && $val['product_count']>0) { ?>
                        <?php if($val['slug'] == $category_slug){ ?>
                          <li class="active"><?php echo @$val['name']; ?><span class="prod-count">(<?php echo @$val['product_count']; ?>)</span></li>
                        <?php }else{ ?>
                        <li><a href="<?php echo base_url(); ?>categories/<?php echo $page_filter_name; ?>/<?php echo @$val['slug']; ?>/variations"><?php echo @$val['name']; ?><span class="prod-count">(<?php echo @$val['product_count']; ?>)</span></a></li>
                        <?php } ?>
                        <?php } ?>
                        <?php } ?>
                      </ul>
                      <?php } ?>
                  </li>
                <?php }
              }
              ?>

            </ul>
          </div>
          </div>
          <?php if(!empty($coupon_offer)) { ?>
          <div class="filter-box">
            <div class="title clearfix">
              <h6>Offers</h6><span class="filter-clear" id="offers_clear" style="display:none;" onclick="clear_offers();">Clear</span>
              <span class="filter-collapsible fa fa-plus" data-toggle="collapse" data-target="#collapse-offers" aria-expanded="false" aria-controls="collapse-offers"></span>
            </div>
            <div id="collapse-offers" class="collapse-offers collapse" aria-expanded="false">
            <ul>
            <?php foreach($coupon_offer as $val) { ?>
              <li>
               <input type="radio" name="sc_offers" id="sc_offers" value="<?php echo $val['id']; ?>" onchange="showClear('offers_clear',this,'offers');product_price_filter();" lable-for='<?php echo $val['coupon_code']; ?> <?php echo $val['coupon_desc']; ?>'><?php echo $val['coupon_code']; ?> <?php echo $val['coupon_desc']; ?>
              </li>
            <?php } ?>

            </ul>
            </div>
          </div>
          <?php } ?>

          <?php if(!empty($paid_brands)){ ?>
          <div class="filter-box">
            <div class="title clearfix">
              <h6>Brands</h6><span class="filter-clear" id="brand_clear" style="display:none;" onclick="clear_brands();">Clear</span>
              <span class="filter-collapsible fa fa-plus" data-toggle="collapse" data-target="#collapse-brands" aria-expanded="false" aria-controls="collapse-brands"></span>
            </div>
            <div id="collapse-brands" class="collapse-brands collapse" aria-expanded="false">
            <ul>
            <?php

                foreach($paid_brands as $val){ ?>
                  <li data-brand-id="<?php echo $val['user_id']; ?>" onmouseup="showClear('brand_clear','','');filter_products(this,'brand');" lable-for="<?php echo $val['company_name']; ?>"><?php echo $val['company_name']; ?></li>
                <?php }

            ?>
            </ul>
            <!--div class="more">
              150 More
            </div-->
          </div>
          </div>
          <?php } ?>

          <?php if(!empty($color)) { ?>
          <div class="filter-box">
            <div class="title clearfix">
              <h6>Colors</h6><span class="filter-clear" id="color_clear" style="display:none;" onclick="clear_color();">Clear</span>
              <span class="filter-collapsible fa fa-plus" data-toggle="collapse" data-target="#collapse-colors" aria-expanded="false" aria-controls="collapse-colors"></span>
            </div>
            <div id="collapse-colors" class="collapse-colors collapse" aria-expanded="false">
            <ul>
            <?php foreach ($color as $key => $value) { ?>
              <li data-color-id="<?php echo $key; ?>" onmouseup="showClear('color_clear',this,'color');product_color_filter(this,'color');" lable-for="<?php echo $value; ?>"><label><i class="color-shade black"></i><?php echo $value; ?></label></li>
            <?php } ?>
            </ul>

            </div>
          </div>
          <?php } ?>


          <div class="filter-box">
            <div class="title clearfix">
              <h6>Price</h6><span class="filter-clear" id="price_clear" style="display:none;" onclick="clear_price();">Clear</span>
              <span class="filter-collapsible fa fa-plus" data-toggle="collapse" data-target="#collapse-price" aria-expanded="false" aria-controls="collapse-price"></span>
            </div>
            <div id="collapse-price" class="collapse-price collapse" aria-expanded="false">
            <ul>
              <li>
              <input type="radio" value="1" name="price_filter" id="price_filter" onchange="showClear('price_clear',this,'price');product_price_filter();" lable-for="Below 500">Below 500
              </li>
              <li>
              <input type="radio" value="2" name="price_filter" id="price_filter" onchange="showClear('price_clear',this,'price');product_price_filter();" lable-for="500 - 999 ">
                500 - 999
              </li>
              <li>
              <input type="radio" value="3" name="price_filter" id="price_filter" onchange="showClear('price_clear',this,'price');product_price_filter();" lable-for="1000 - 1999 ">
                1000 - 1999
              </li>
              <li>
              <input type="radio" value="4" name="price_filter" id="price_filter" onchange="showClear('price_clear',this,'price');product_price_filter();" lable-for="2000 - 2999 ">
                2000 - 2999
              </li>
              <li>
              <input type="radio" value="5" name="price_filter" id="price_filter" onchange="showClear('price_clear',this,'price');product_price_filter();" lable-for="above 2999 ">
                above 2999
              </li>
            </ul>
            <div class="price-range">
              <div class="text-1">
                Enter a price range
              </div>
              <div class="form-group">
                <input type="text" class="form-control input-sm" name="min_price" id="min_price"> <span>-</span>
                <input type="text" class="form-control input-sm" name="max_price" id="max_price">
                <button class="btn btn-sm btn-secondary" onclick="showClear('price_clear','','');product_price_filter()"> Go </button>
              </div>
            </div>
            </div>
          </div>

          <div class="filter-box">
            <div class="title clearfix">
              <h6>Discounts</h6><span class="filter-clear" id="discount_clear" style="display:none;" onclick="clear_price();">Clear</span>
              <span class="filter-collapsible fa fa-plus" data-toggle="collapse" data-target="#collapse-discounts" aria-expanded="false" aria-controls="collapse-discounts"></span>
            </div>
            <div id="collapse-discounts" class="collapse-discounts collapse" aria-expanded="false">
            <ul>
              <li>
              <input type="radio" name="discount" id="discount" value="1" onchange="showClear('discount_clear',this,'discount');product_price_filter();" lable-for="Discount">Discount
              </li>
              <li>
              <input type="radio" name="discount" id="discount" value="2" onchange="showClear('discount_clear',this,'discount');product_price_filter();" lable-for="Non-Discount">Non-Discount
              </li>
            </ul>
            </div>
          </div>
</div>
