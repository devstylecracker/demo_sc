<script type="text/Javascript">
function msg(m){ console.log(m); }
var OSName="Unknown OS";
if (navigator.appVersion.indexOf("Win")!=-1) OSName="Windows";
if (navigator.appVersion.indexOf("Mac")!=-1) OSName="MacOS";
if (navigator.appVersion.indexOf("X11")!=-1) OSName="UNIX";
if (navigator.appVersion.indexOf("Linux")!=-1) OSName="Linux";
function CheckURL(){
	if(OSName=="Windows" || OSName=="Linux") {
		window.open('https://play.google.com/store/apps/details?id=com.stylecracker.android&hl=en?type=individual','_blank');
	}
	else if(OSName=="MacOS") {
		window.open('https://itunes.apple.com/bj/app/stylecracker/id1061463812?mt=8?type=individual','_blank');
	}
}
</script>
<div class="slider-wrp">
  <div class="flexslider">
    <ul class="slides">

	<li class="slide1">
		<img src="<?php echo base_url(); ?>assets/images/home/livello.jpg"  />
	</li>
  
   <li class="slide6">
    <a href="<?php echo base_url(); ?>brands/brand-details/stylefiesta" target="_blank" >
    <img src="<?php echo base_url(); ?>assets/images/home/stylefiesta_discountBanner.jpg" />
    <div class="container">
      <!--div class="flex-caption">
        <span class="line1">A Glittering Start to 2016 <span class="btn btn-secondary">Shop Now</span></span>
      </div-->
    </div>
  </a>
  </li>
	
	<li class="slide2">
	<a onclick="CheckURL();" href="javascript:void(0);">
      <img src="<?php echo base_url(); ?>assets/images/home/banner-apps-download.jpg" /></a>
      <div class="container">
          <div class="row">
          <div class="col-md-8 col-sm-8"></div>
        <div class="col-md-4 col-sm-4">
            <div class="btn-banner-wrp">
                    <a href="javascript:void(0);" class="btn-banner">
                      <img src="<?php echo base_url(); ?>assets/images/home/btn-app-store.png">
                    </a>
                    <a href="javascript:void(0);" class="btn-banner">
                      <img src="<?php echo base_url(); ?>assets/images/home/btn-google-play.png">
                    </a>
              </div>
            </div>
          </div> 
     </div>
  </li>  
        
	<!-- FOR REFERENCE (DONT DELETE)
	
        <li class="slide3">
          <img src="<?php echo base_url(); ?>assets/images/home/karl-lagerfeld.jpg" />
          <div class="container">
            <div class="flex-caption">
              <span class="line1"><a class="highlight" href="http://www.stylecracker.com/blog/10-times-karl-lagerfeld-wowed-us/" target="_blank">Read more</a> to know the 10 times this iconic designer has wowed us.</span>
            </div>
          </div>
        </li>
	
	-->

    <li class="slide4">
      <a href="#myModal" data-toggle="modal"><img src="<?php echo base_url(); ?>assets/images/home/team-banner.jpg" />
      <div class="container">
        <div class="flex-caption">
          <span class="line1">Let us curate the latest fashion for you.</span>
          <?php if($this->session->userdata('user_id')) { ?>
            <span class="line2"></span>
            <?php }else{ ?>
              <span class="line2"><span class="highlight">Sign up</span>, browse and shop for the season's top picks.</span>
              <?php } ?>
        </div>
      </div>
      </a>
    </li>

    <li class="slide3">
      <a href="https://www.stylecracker.com/search/index/ResortWear">
      <img src="<?php echo base_url(); ?>assets/images/home/livello.jpg" />
      <div class="container">
        <div class="flex-caption">              
        </div>
      </div>
      </a>
    </li> 

    </ul>
  </div>
</div>

<style>
  .slides .btn-banner-wrp{position:absolute;  bottom:100px; width:100%; text-align:center; padding-left: 60px;}
    .slides .btn-banner-wrp img{width:100%;}
  .slides .btn-banner{display:inline-block; width:130px; margin:0 10px;}
  @media (max-width:768px){
      .slides .btn-banner-wrp{bottom:10px; text-align: right;}
      .slides .btn-banner{ width:50px;  margin:0 1px;}
      .slides .btn-banner:hover img{opacity:0.69;}
  }
</style>

      