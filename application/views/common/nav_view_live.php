  <header id="header" class="header is-sticky">
    <div class="container">
      <div class="navbar-header">
        <button aria-expanded="false" aria-controls="bs-navbar" data-target="#bs-navbar" data-toggle="collapse" type="button" class="navbar-toggle collapsed">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo base_url(); ?>">
          <img id="logo-desktop" class="logo logo-desktop" src="<?php echo base_url(); ?>assets/images/logo.png"/>
          <!--<img class="desktop-logo" src='img/logo-small.png' width="46" height="66" />-->
          <img id="logo-phone" class="logo logo-phone" src='<?php echo base_url(); ?>assets/images/logo-small.jpg' />
        </a>
      </div>
      <nav class="collapse navbar-collapse navbar-right " id="bs-navbar">
        <ul class="nav navbar-nav">
          <li class="active">
            <a href="<?php echo base_url().'about-us'; ?>">About</a>
          </li>
          <li>
            <a href="http://www.stylecracker.com/blog/" target="_blank">SC Live</a>
          </li>
          <li>
            <a href="<?php echo base_url().'brands'; ?>">Brands</a>
          </li>
          <li>
            <a href="<?php echo base_url().'media'; ?>">Media</a>
          </li>
        <!--  <li>
           <a href="#">Press</a>
         </li>-->
          <?php if(!$this->session->userdata('user_id')){ ?>
          <li>
            <a href="#myLoginModal" data-toggle="modal">Log in</a>
          </li>
          <li class="menu-highlight">
		  <a href="#myModal" onclick="SignUpClick()" data-toggle="modal">Sign up</a>
          </li>
          <?php }else{ ?>
          <li class="dropdown">
            <a class="menu-highlight" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <?php echo $this->session->userdata('first_name').' '.$this->session->userdata('last_name'); ?> <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url().'feed'; ?>">My Feed</a></li>
                <li><a href="<?php echo base_url().'profile'; ?>">Profile</a></li>
                <li><a href="<?php echo base_url().'myOrders'; ?>">My Orders</a></li>
                <li><a href="<?php //echo base_url().'schome/logout'; ?>" data-toggle="modal" id="sclogout">Sign out</a></li>
              </ul>
            </li>
          <?php } ?>
          <li class="menu-cart dropdown">
            <a class="menu-cart-item" onclick="openCartPage();" data-toggle="modal" title="Shopping Cart"> <i class="fa fa-shopping-cart"></i>
              <span id="total_products_cart_count">
                
              </span>
            </a>
          </li>
          <li class="menu-search dropdown">
            <a class="menu-search-item" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-search"></i></a>
            <ul class="dropdown-menu">
              <li><input type="text" class="form-control" id="sc_search" name="sc_search" placeholder="SEARCH BY KEYWORD" /></li>
            </ul>
          </li>
        </ul>
      </nav>
    </div>
  </header>
  <?php include('signup_view.php'); ?>
  <script type="text/javascript">
    $(document).ready(function(e){

        $("#sc_search").keyup(function(event) {
            event.preventDefault()
            var search = $("#sc_search").val();
            search = search.replace(/[^a-zA-Z0-9]/g, '');
            
            if(event.keyCode == 13 && ($("#sc_search").val().trim() !='')) {
                location.href='<?php echo base_url(); ?>search/index/'+search;
             }

        });
		
		
    });
	//##GA Event add for Signup Link on 15-Jan-16 (as per Veelas email) 
	function SignUpClick(){
		ga('send', 'pageview', '/overlay/menu-signup/?page=' + document.location.pathname + document.location.search + ' - Sign Up');
	}
  </script>
