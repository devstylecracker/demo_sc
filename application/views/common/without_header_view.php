<?php
//ini_set("zlib.output_compression", "On");
//ini_set("zlib.output_compression_level", "-1");
$betaout_email = $this->session->userdata('email');
$betaout_user_id = $this->session->userdata('user_id');
$betaout_bucket = $this->session->userdata('bucket');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>StyleCracker</title>

  <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/flexslider.min.css" rel="stylesheet">

  <link href="<?php echo base_url(); ?>assets/css/custom.css?v=1.27" rel="stylesheet">
  <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300' rel='stylesheet' type='text/css'>-->
  <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,300' rel='stylesheet' type='text/css'>
  <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">

     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <!--script src="<?php echo base_url(); ?>assets/js/jquery.sticky.js"></script-->
    <script src="<?php echo base_url(); ?>assets/js/jquery.flexslider-min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/ajaxCache-0.2.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.masonry.js"></script>
    <!--script src="<?php echo base_url(); ?>assets/js/custom.js"></script-->




    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  <script type="text/javascript">

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-64209384-1', 'auto');
    ga('send', 'pageview');
    var baseUrl = '<?php echo base_url(); ?>';
	
	var _bout = _bout || [];
  var _boutAKEY = "v4nhzasejhq4nlz8quj4m2w2cad0zuq8gjy4y0dawu", _boutPID = "30021"; 
  var d = document, f = d.getElementsByTagName("script")[0], _sc = d.createElement("script"); _sc.type = "text/javascript"; _sc.async = true; _sc.src = "https://d22vyp49cxb9py.cloudfront.net/jal-v2.min.js"; f.parentNode.insertBefore(_sc, f);
 _bout.push(["identify",{
   "customer_id": "<?php echo $betaout_user_id; ?>",
   "email":  "<?php echo $betaout_email; ?>"
}]);
       var sc_beatout_email_id = "<?php echo $this->session->userdata('email')!='' ? $this->session->userdata('email') : ''; ?>";
       var sc_beatout_username = "<?php echo $this->session->userdata('user_name')!='' ? $this->session->userdata('user_name') : ''; ?>";
	   var sc_beatout_user_id = "<?php echo $this->session->userdata('user_id')!='' ? $this->session->userdata('user_id') : ''; ?>";
	   var sc_gender = "<?php echo $this->session->userdata('gender')!='' ? $this->session->userdata('gender') : ''; ?>";
	   if(sc_gender == 1) {sc_beatout_gender ='women'; } else sc_beatout_gender ='men'; 
	   
	   _bout.push(["properties", {
							"customer_id": sc_beatout_user_id,
							"email": sc_beatout_email_id
						}, {
							'update': {
								"gender": sc_beatout_gender,
							}
						}
					]);
  </script>



  </head>
  <body class="without-header">
 <!-- Google Code for Website Campaign-Sign Up Conversion Page added on 20-01-2016-->
  <script type="text/javascript">
  /* <![CDATA[ */
  var google_conversion_id = 950917036;
  var google_conversion_language = "en";
  var google_conversion_format = "3";
  var google_conversion_color = "ffffff";
  var google_conversion_label = "ziMrCOaTl2MQrK-3xQM";
  var google_remarketing_only = false;
  /* ]]> */
  </script>
  <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
  </script>
  <noscript>
  <div style="display:inline;">
  <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/950917036/?label=ziMrCOaTl2MQrK-3xQM&amp;guid=ON&amp;script=0"/>
  </div>
  </noscript>
  <!-- (END:)-->
