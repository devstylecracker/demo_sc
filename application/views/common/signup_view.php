<div class="modal fade signin-signup-popup large" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
	<div class="slash-bg"></div>
      <div class="close-button">
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>

      <div class="modal-body signin-signup-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-5">
              <div style="color: rgb(255, 255, 255); font-size: 26px; font-weight: 600; font-style: italic;" class="fav-txt"></div>
              <div class="title">Sign <span>Up</span></div>
              <div class="signup-social-media">
                <div class="login-btns-inner">
                  <a href="<?php //echo $this->data['facebook_login_url']; ?>" title="SC facebook" ><span id="facebook_login"  class="btn-login-fb"><i class="fa fa-facebook"></i> <span>Sign Up with Facebook</span></span></a>
                  <a href="<?php //echo @$this->data['google_login_url']; ?>"><span class="btn-login-google"><i class="fa fa-google-plus"></i> <span>Sign Up with Google</span></span></a>
                  <div class="clear"></div>
                </div>
              </div>
            </div>
            <div class="col-md-7">
              <div class="form-outer">
                <form name="scSignup" id="scSignup" method="post" action="">

                  <!-- <div class="form-control-wrp">
                    <input type="text" name="sc_username" id="sc_username" value="" placeholder="USERNAME">
                    <span id="sc_username_error"></span>
                  </div> -->
                  <div class="row">
                    <div class="col-md-6">
                     <div class="form-control-wrp">
                      <input type="text" name="sc_firstname" id="sc_firstname" value="" placeholder="First Name">
                      <span id="sc_firstname_error"></span>
                    </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-control-wrp">
                      <input type="text" name="sc_lastname" id="sc_lastname" value="" placeholder="Last Name">
                      <span id="sc_lastname_error"></span>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-control-wrp">
                      <input type="text" name="sc_mobile" id="sc_mobile" value="" placeholder="Mobile">
                      <span id="sc_mobile_error"></span>
                      </div>

                    </div>
                    <div class="col-md-6">
                      <div class="form-control-wrp">
                        <select class="form-control" id="sc_gender" name="sc_gender">
                          <option disabled="" value="" selected>
                          Select Gender
                          </option>
                          <option  value="1">
                          Female
                        </option>
                          <option value="2">
                          Male
                        </option>
                      </select>
                      <span id="sc_gender_error"></span>
                      <div id="sc_signup_error"></div>
                      </div>

                    </div>
                  </div>
                  <div class="form-control-wrp">
                    <input type="text" name="sc_emailid" id="sc_emailid" value="" placeholder="Email">
                    <span id="sc_emailid_error" class="custom-error error"></span>
                  </div>
                  <div class="form-control-wrp">
                    <input type="password" name="sc_password" id="sc_password" value="" placeholder="Password (At least 6 characters)">
                    <span id="sc_password_error"></span>
                  </div>



                  <div class="text-line">
                    By clicking 'SIGN UP' you confirm that you have read and accepted our Terms and Conditions, Privacy Policy and Cookie Policy.
                  </div>

                  <div class="sign-up-btn-wrp">
                    <button type="submit" id="sc_signup" name="sc_signup" class="btn btn-primary sign-up-btn">Sign Up</button>
                    <div style="padding:0px;margin:0px;margin-left: 30px;font-size: 17px;" class="fa-loader-wrp-products">
                      <i class="fa fa-circle-o-notch fa-spin-custom"></i>
                    </div>
                  </div>

                  <div class="login-text">
                    Already a member? <a id="sc_login" onclick="HideModal();" name="sc_login" class="signin-link" href="#myLoginModal" data-toggle="modal">LOG IN</a> now.
                  </div>

                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade signin-signup-popup large" id="myLoginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
	<div class="slash-bg"></div>
      <div class="close-button">
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>

      <div class="modal-body signin-signup-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-5">
              <div class="title">Log <span>In</span></div>
              <div class="signup-social-media">
                <div class="login-btns-inner">
                  <a href="<?php //echo $this->data['facebook_login_url']; ?>" title="SC facebook" ><span id="facebook_login_in"  class="btn-login-fb"><i class="fa fa-facebook"></i> <span>Log In with Facebook</span></span></a>
                  <a href="<?php echo $this->data['google_login_url']; ?>"><span class="btn-login-google"><i class="fa fa-google-plus"></i> <span>Log In with Google</span></span></a>
                  <div class="clear"></div>
                </div>
              </div>
            </div>
            <div class="col-md-7">
              <div class="form-outer">
                <form name="scnewLogin" id="scnewLogin" method="post" action="">
                  <div class="form-control-wrp">
                    <input type="text" name="logEmailid" id="logEmailid" value="" placeholder="EMAIL">
                    <span id="logEmailid_error"></span>
                  </div>
                  <div class="form-control-wrp">
                    <input type="password" name="logPassword" id="logPassword" value="" placeholder="PASSWORD">
                    <span id="logPassword_error"></span>
                    <div id="login_error" class="custom-error form-error error"></div>
                  </div>
                  <label class="check-rembember">
                    <input type="checkbox" id="rememberMe" name="rememberMe" checked="true" > Remember me
                  </label>
                  <div class="sign-up-btn-wrp">
                    <button type="submit" id="sc_Login" name="sc_Login" class="btn btn-primary sign-up-btn">Log In</button>

                      <div class="fa-loader-wrp-products">
                        <i class="fa fa-circle-o-notch fa-spin-custom"></i>
                      </div>
                  </div>

                  <div class="login-text">
                    <a data-toggle="modal" onclick="HideLoginModal();" href="#myModalForgotpwd" class="pw-link">Forgot Password?</a> | New to StyleCracker? <a data-toggle="modal" onclick="HideLoginModal();" href="#myModal" class="signin-link">SIGN UP</a> now.
                  </div>
              </form>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
</div>


<div class="modal fade signin-signup-popup large" id="myModalForgotpwd" tabindex="-1" role="dialog" aria-labelledby="myModalForgotpwdLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
	<div class="slash-bg"></div>
      <div>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>

      <div class="modal-body signin-signup-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6">
              <div class="title">Forgot <span>Password</span></div>
            </div>
            <div class="col-md-6">
              <div class="form-outer">
                <form name="forgotpwdfrm" id="forgotpwdfrm" method="post" action="">
                  <div class="form-control-wrp">
                    <input type="text" name="Emailid" id="Emailid" value="" placeholder="EMAIL">
                    <span id="Emailid_error"></span>
                    <div id="loginfp_error" class="form-error error"></div>
                  </div>

                  <div class="sign-up-btn-wrp">
                    <button  id="sc_submit" name="sc_submit" class="btn btn-primary sign-up-btn">Submit</button>
                  </div>
                </form>

              </div>
            </div>
            <div class="col-md-1">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>




<div class="modal fade mycart-popup" id="mycart" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- prompt popup  -->
    <div class="page-dialog-overlay"></div>
    <div class="prompt-popup prompt-popup-otp" id="prompt-popup-otp">
      <div class="close">x</div>
      <div class="prompt-popup-inner">
        <div class="form-group">
          <label>Enter OTP</label>
          <input type="text" class="form-control input-sm" name="singleOTPNumber" id="singleOTPNumber">
          <div class="message hide error" id="otp_error">Please enter valid OTP.</div>
        </div>
      </div>
      <div class="btns-wrp">
        <button class="btn btn-primary btn-sm" id="sc_otp_enter" onclick="sc_check_otp();">OK</button>
        <div class="cart-item-meta"><br/>Enter the OTP number that has been sent to your mobile phone</div>
      </div>

    </div>
    <div class="cart-loader"><img src="<?php echo base_url(); ?>assets/images/loading-2.gif"></div>
    <!-- //prompt popup  -->

     <!-- prompt popup For Email -->
    <div class="prompt-popup prompt-popup-coupemail" id="prompt-popup-coupemail">
      <div class="close">x</div>
      <div class="prompt-popup-inner">
        <div class="form-group">
          <label>Enter Email</label>
          <input type="text" class="form-control input-sm" name="couponEmail" required id="couponEmail" >
          <div class="message hide error" id="coupon_email_error">Please enter Email.</div>
        </div>
      </div>
      <div class="btns-wrp">
        <button class="btn btn-primary btn-sm" id="sc_coupon_email" >OK</button>
      </div>

    </div>

    <!-- //prompt popup For Email -->


    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Shopping Cart</h4>
      </div>
      <form name="sc_uc_shopping_cart" id="sc_uc_shopping_cart" method="post">
        <div class="modal-body mycart-body">

          <div class="cart-with-items">
            <div class="row custom">
              <div class="col-md-6 custom" id="cart-items-wrp">

              </div>

              <div class="col-md-6 custom">
                <div class="adress-box">
                  <h4 class="mycart-title">Address Details</h4>

                  <div class="panel-group adress-accordion" id="adress-accordion">

                    <?php if($this->session->userdata('user_id') && !empty($this->data['user_existing_address'])){ ?>
                      <div class="panel panel-default">
                        <div class="panel-heading mycart-subtitle">
                          <div data-toggle="collapse" data-parent="#adress-accordion" id="block5" href="#collapse5" class="collapsed111" aria-expanded="true">Select Shipping Address</div>

                        </div>
                        <div id="collapse5" class="panel-collapse collapse <?php if($this->session->userdata('user_id')){ echo 'in'; } ?>" aria-expanded="<?php if($this->session->userdata('user_id')){ echo 'true'; } else { echo 'false';} ?>" >
                          <div class="panel-body">
                            <ul class="address-list">
                              <?php
                              if(!empty($this->data['user_existing_address'])){
                                foreach($this->data['user_existing_address'] as $val){
                                  ?>
                                  <li>
                                    <label data-parent="#accordion" href="#collapse4"><input type="radio" class="radio-address" name="existing_shipping_add" value="<?php echo $val['id']; ?>" onclick="existingAddress(this);">
                                      <div class="name">
                                        <?php echo $val['first_name'].' '.$val['last_name']; ?>
                                      </div>
                                      <?php echo nl2br($val['shipping_address']).'<br/> '.trim($val['city_name']).' '.trim($val['states_name']).' '.trim($val['pincode']); ?>
                                    </label>
                                  </li>
                                  <?php
                                }
                              }
                              ?>

                            </ul>
                            <div class="btn-add-address-wrp"><div id="add_new_address" class="btn-add-address" data-toggle="collapse" data-parent="#adress-accordion" href="#collapse4" aria-expanded="false">Add New Address</div></div>
                          </div>
                        </div>
                      </div>
                      <?php } ?>

                      <div class="panel panel-default">
                        <div class="panel-heading mycart-subtitle">
                          <div data-toggle="collapse" data-parent="#adress-accordion" href="#collapse4" id="block4" aria-expanded="false" class="collapsed111">Shipping Details</div>
                        </div>

                        <div id="collapse4" class="panel-collapse collapse <?php if(!$this->session->userdata('user_id') || empty($this->data['user_existing_address'])){ echo 'in'; } ?>" aria-expanded="false">
                          <div class="panel-body panel-body-shipping">   <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" name="uc_first_name" id="uc_first_name" placeholder="First Name" value="<?php echo $this->session->userdata('first_name'); ?>" class="form-control input-sm">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" placeholder="Last Name" name="uc_last_name" id="uc_last_name" value="<?php echo $this->session->userdata('last_name'); ?>" class="form-control input-sm">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" placeholder="Email" name="uc_email_id" id="uc_email_id" value="<?php echo $this->session->userdata('email'); ?>" class="form-control input-sm">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" placeholder="Mobile" name="uc_mobile" id="uc_mobile" value="" class="form-control input-sm" maxlength="10">
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                <textarea placeholder="Shipping Address" name="uc_address" id="uc_address" class="form-control input-sm" maxlength="100"></textarea>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <input type="text" placeholder="PIN" name="uc_pincode" id="uc_pincode" value="" class="form-control input-sm" maxlength="6">
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <input type="text" placeholder="City" value="" name="uc_city" id="uc_city" class="form-control input-sm">
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <select class="form-control input-sm" name="uc_state" id="uc_state">
                                  <option value="">Select State</option>
                                  <?php if(!empty($this->data['all_uc_states'])){
                                    foreach($this->data['all_uc_states'] as $state){
                                      echo '<option value="'.(int)$state['id'].'">'.$state['state_name'].'</option>';
                                    }
                                  } ?>
                                </select>
                              </div>
                            </div>
							 <div class="col-md-4">
                              <div class="form-group">
                               <input type="text" class="uc_state2" placeholder="state" value="" name="uc_state2" id="uc_state2" class="form-control input-sm">
                              </div>
                            </div>
							 <div class="col-md-4">
                              <div class="form-group">
                                <input type="text" class="country" placeholder="country" value="" name="country" id="country" class="form-control input-sm">
                              </div>
                            </div>
                          </div></div>


                        </div>
                      </div>

                    </div>


                  </div>
                  <div class="payment-options-box">
                    <h4 class="mycart-title">Payment Options</h4>
                    <div>
                      <ul class="cart-payment-modes"  id="sc_uc_payment_mode">
                        <li><label><input type="radio" name="pay_mode" id="btn-payment-cod" value="cod" onclick="paymentModeSel(this);"> <span>Cash on Delivery</span></label></li>
                        <li><label><input type="radio" name="pay_mode" id="btn-payment-online" value="card" onclick="paymentModeSel(this);" checked> <span>Online Payment</span></label></li>
                        <!--li onclick="paymentModeSel(this);" mode="cod" class="active"><a data-toggle="tab" href="#payment-cod">COD</a></li>
                        <li onclick="paymentModeSel(this);" mode="card" ><a data-toggle="tab" href="#payment-online">Credit, Debit Card / Netbanking
                      </a></li-->
                    </ul>

                    <div class="cart-payment-modes-content hide">
                      <div id="payment-cod" class="payment-mode-content" style="display:block">
                        Please make sure the details entered are correct and place the order. You can make payment by Cash when the item is delivered to you. <
                      </div>
                      <div id="payment-online" class="payment-mode-content">
                        You will be redirected to our Payment Gateway provider to make the payment. Make sure the details entered are correct before placing the order.
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>

          </div>
          <div class="cart-empty-wrp">
            <div class="text-label">Your shopping cart is empty</div>
            <div>
              <span class="btn btn-primary" data-dismiss="modal">Start shopping now</span>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button name="uc-close-modal" id="uc-close-modal" type="button" class="btn btn-secondary btn-md btn-sm">Continue Shopping</button>
          <button type="Place Order" name="ucsubmit" id="ucsubmit" class="btn btn-primary btn-md btn-sm">Place My Order</button>
        </div>
      </div>
    </form>
  </div>
</div>

<div class="modal fade mycart-popup sizechart-popup" id="mysize" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Size Chart</h4>
      </div>

      <div class="modal-body">
          <img src="" id="product_size_guide">
      </div>
    </div>
  </div>
</div>


<!-- /sizechart popup -->

<!-- quick view -->
<!-- Size Chart Pop up -->


<!-- prompt popup  -->

<div id="notify_me" class="prompt-popup prompt-popup-otp">
  <div class="close">x</div>
  <div class="prompt-popup-inner">
    <div class="form-group">
      <label>Enter Email</label>
      <input type="text" class="form-control input-sm" name="notify_emailid" id="notify_emailid" value="<?php echo $this->session->userdata('email'); ?>">
      <input type="hidden" name="notify_me_id" id="notify_me_id">
      <div class="message hide error" id="notify_error">Please enter email.</div>
    </div>
  </div>
  <div class="btns-wrp">
    <button class="btn btn-primary btn-sm" id="sc_otp_enter" onclick="notify_me();">OK</button>
  </div>

</div>
<!-- //prompt popup  -->

<!-- // quick view -->

<form action="<?php echo PAYMENT_URL; ?>" method="post" name="payuForm">
  <input type="hidden" name="key" value="<?php echo MERCHANT_KEY; ?>" />
  <input type="hidden" name="productinfo" value='{"paymentParts":[{"name":"splitID1","value":"6","merchantId":"396446","description":"test description","commission":"2"}]}' />
  <input type="hidden" name="hash" id="hash" value=""/>
  <input type="hidden" name="txnid" id="txnid" value="" />
  <input type="hidden" name="amount" id="amount" value="" />
  <input type="hidden" name="surl" id="surl" value="<?php echo SURL; ?>" />
  <!--input type="hidden" name="service_provider" id="service_provider" value="payu_paisa" /-->
  <input type="hidden" name="firstname" id="firstname" value="" />
  <input type="hidden" name="email" id="email" value="" />
  <input type="hidden" name="phone" id="phone" value="" />
  <input type="hidden" name="udf1" id="udf1" value="1,2,3" />
  <input type="hidden" name="udf2" id="udf2" value="" />
  <input type="hidden" name="udf3" id="udf3" value="" />
  <input type="hidden" name="udf4" id="udf4" value="" />
  <input type="hidden" name="udf5" id="udf5" value="" />

</form>


<!-- pa gender popup -->
<div class="modal fade popup-gender" id="popup-gender" tabindex="-1" role="dialog" aria-labelledby="popup-gender">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body popup-gender-body">
        <div class="title-1">
          <span class="text-1">Hey dude</span> or <span class="text-2">Hello dudette!</span>
        </div>
        <div class="title-2">
          Please tell your gender for best offers!
        </div>
		<div style="position:relative;">
		  <div class="gender-btn" onclick = "check_gender(2)"></div>
		  <div class="gender-btn" onclick = "check_gender(1)" style="right:0;"></div>
			 <img src="<?php echo base_url(); ?>assets/images/pa/gender-box.jpg">
		</div>
        <!-- <img src="<?php echo base_url(); ?>assets/images/pa/gender-box.jpg"> -->

        <div class="btns-wrp clearfix">
          <button onclick = "check_gender(2)" class="btn btn-primary btn-gender-male">Male</button>
          <button onclick = "check_gender(1)" class="btn btn-primary btn-gender-female">Female</button>
        </div>
      </div>
    </div>
  </div>
</div>



<div class="modal fade popup-buynow1" id="popup-newsclookbuynow" tabindex="-1" role="dialog" aria-labelledby="popup-buynow1">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body popup-buynow">

        <div class="popup-buynow-inner">
            You already have other products into your cart, you want to keep those product while checkout or remove other products from cart ?
        </div>

        <div class="btns-wrp clearfix">
          <a class="btn btn-secondary" onclick="remove_looks_products();" href="javascript:void(0);">Remove</a>
          <a onclick="buylook();" href="javascript:void(0);" class="btn btn-primary">Keep</a>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- pa gender popup -->
<div class="modal fade popup-buynow" id="popup-newscbuynow" tabindex="-1" role="dialog" aria-labelledby="popup-buynow">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body popup-buynow">

        <div class="popup-buynow-inner">
            You already have other products into your cart, you want to keep those product while checkout or remove other products from cart ?
        </div>
        <input type="hidden" name="nonremoval" id="nonremoval" >
        <input type="hidden" name="nonremovalproname" id="nonremovalproname" >
        <input type="hidden" name="nonremovalbrandname" id="nonremovalbrandname" >
        <div class="btns-wrp clearfix">
          <a class="btn btn-secondary" onclick="remove_products_otherthan_nonremoval(1);" href="javascript:void(0);">Remove</a>
          <a onclick="remove_products_otherthan_nonremoval(2);" href="javascript:void(0);" class="btn btn-primary">Keep</a>
        </div>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">
function check_gender(attr){
	$.ajax({
            url: "<?php echo base_url() ?>schome/check_gender",
            type: 'POST',
            data: {'attr':attr},
            cache :true,
            async: false,
            success: function(response) {
			if(attr != parseInt(response)){
				window.open('http://localhost/scproject/schome/pa','_blank');
			}else{
				// $('#popup-gender').hide();
				$('#popup-gender').modal('hide');
			}

            }
        });
}
</script>
<style>
.gender-btn { position:absolute;z-index:1;width:50%;height:320px; cursor:pointer;}
</style>
