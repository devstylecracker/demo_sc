<?php 
$onesignaldata = array();
// $onesignaldata['os_fname'] = (@$this->session->userdata('first_name')!='') ? $this->session->userdata('first_name') : ' ';
// $onesignaldata['os_lname'] =  (@$this->session->userdata('last_name')!='') ? $this->session->userdata('last_name') : ' ';
// $onesignaldata['os_email'] =   (@$this->session->userdata('email')!='') ? $this->session->userdata('email') : ' ';
$userosdata = '';
// $userosdata = $this->session->userdata('onesignaldata');
$userosdata = $this->home_model->senddata_onesignal();
?>


<link rel="manifest" href="/manifest.json">
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
<script>
  var OneSignal = window.OneSignal || [];
  OneSignal.push(["init", {
    appId: "774c695f-4257-45ad-bbd7-76a22b12117b",
    autoRegister: true,
    safari_web_id: 'web.onesignal.auto.4f832ce8-c167-4c63-9514-5546a8912edb',
    notifyButton: {
      enable: false /* Set to false to hide */
    }
  }]);

  OneSignal.push(["sendTags", <?php echo $userosdata; ?>]);

</script>
<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <ul class="list-inline quicklinks footer-nav">
          <li><a href="<?php echo base_url().'about-us'; ?>">About</a></li>
          <li><a href="<?php echo base_url().'media'; ?>">Media</a></li>
          <li><a href="<?php echo base_url(); ?>terms-and-conditions">Terms & Conditions</a></li>
		   <li><a href="<?php echo base_url(); ?>return-policy">Return Policy</a></li>
          <li><a href="<?php echo base_url(); ?>privacy-policy">Privacy Policy</a></li>
          <li><a href="<?php echo base_url(); ?>faq">FAQ</a></li>
          <li><a href="<?php echo base_url(); ?>sitemap">Sitemap</a></li>
        </ul>
      </div>
      <div class="col-md-3 text-right">
        <ul class="social-buttons">
          <li><a target="_blank" href="https://www.facebook.com/StyleCracker"><i class="fa fa-facebook"></i></a></li>
          <li><a target="_blank" href="https://twitter.com/Style_Cracker"><i class="fa fa-twitter"></i></a></li>
          <li><a target="_blank" href="https://www.youtube.com/user/StyleCrackerTV"><i class="fa fa-youtube"></i></a></li>         <li><a target="_blank" href="https://instagram.com/stylecracker"><i class="fa fa-instagram"></i></a></li>
          <li><a target="_blank" href="https://www.pinterest.com/stylecracker"><i class="fa fa-pinterest"></i></a></li>
        </ul>
      </div>
      <div class="col-md-12">
        <div class="copyright111" style="color:#bcbcbc">All Rights Reserved &copy;<?php echo date("Y") ?> StyleCracker India.</div>
      </div>

    </div>
  </div>
<div class="chat-to-stay">
</div>
<!--<div class="sticy-call-us sm-only">
  <a href="tel:+91-22-61738515" style=" "><i class="fa fa-phone" style=""></i> Call a Stylist</a>
</div>-->
 <div class="back-to-top scroll-bottom">
      <i class="fa fa-angle-down"></i>
</div>
<div class="back-to-top scroll-top">
     <i class="fa fa-arrow-up11 fa-chevron-up11 fa-angle-up"></i>
</div>

</footer>
<div class="fa-loader-wrp">
  <i class="fa fa-circle-o-notch fa-spin-custom"></i> Loading...
</div>
<div class="page-overlay"></div>
<div class="device-info"></div>
<a id="invite_copy" class="btn btn-refer" href="#myModal" data-toggle="modal" title=""><i class="fa fa-gift"></i> Refer & Earn</a>
 <?php include('signup_view.php'); ?>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 950917036;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<div style="position:absolute; bottom:0;">
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/950917036/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</div>
<!--End of Google Code for Remarketing Tag --> 
 <!-- js -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/hoverintent/hoverIntent.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/swiper/js/swiper.jquery.min.js"></script>
    <?php if($this->uri->segment(1) == 'all-products' || $this->uri->segment(1) == 'brand' || $this->uri->segment(1) == 'brands' || $this->uri->segment(1) == 'Brand_category' || $this->uri->segment(1) == 'media' || $this->uri->segment(1) =='sale' || $this->uri->segment(1) =='wedding-pop-up' || $this->uri->segment(1) =='' || $this->uri->segment(1) =='daydream' || $this->uri->segment(1) =='party-pop-up' || $this->uri->segment(1) =='all-things-new' || $this->uri->segment(1) == 'all-black-products' || $this->uri->segment(1) == 'valentine-pop-up' || $this->uri->segment(1) == 'summer-pop-up' || $this->uri->segment(1) == 'travel-shop'){ ?>
    <script src="<?php echo base_url(); ?>assets/plugins/lazyload/jquery.lazyload.min.js"></script>
    <?php } ?>
    <script src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/ajaxCache-0.2.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
    <?php if($this->uri->segment(1) == 'media' || $this->uri->segment(1) ==''){ ?>
    <script src="<?php echo base_url(); ?>assets/js/jquery.masonry.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/imagesloaded.pkgd.min.js"></script>
    <?php } ?>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-select.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
    <?php if($this->uri->segment(1) == 'product' || $this->uri->segment(1) == 'looks' || $this->uri->segment(1) ==''){ ?>
    <script src="<?php echo base_url(); ?>assets/plugins/elevatezoom/jquery.elevateZoomCustom.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/magnifik/magnifik.js"></script>
    <?php } ?>
    <script src="<?php echo base_url(); ?>assets/plugins/magnificpopup/magnific-popup.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/theia-sticky-sidebar/theia-sticky.min.js"></script>
	<!-- datepicker -->
	<script src="<?php echo base_url(); ?>sc_admin/assets/seventeen/plugins/datepicker/bootstrap-datepicker.js"></script>
   
    <script src="<?php echo base_url(); ?>assets/js/custom.js?v=1.21"></script>
    <?php if($this->uri->segment(1) =='home' || $this->uri->segment(1) ==''){ ?>
    <script src="<?php echo base_url(); ?>assets/js/custom-front.js?v=1.04"></script> 
    <?php } ?>
    <script src="<?php echo base_url(); ?>assets/js/analytics.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/sociallogin.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/cart_v1.js?v=1.31"></script>
    <script src="<?php echo base_url(); ?>assets/js/sc-tracker.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/scrollpagination.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/common.js?v=5.71"></script>
    <script src="<?php echo base_url(); ?>assets/js/stylecracker-cart.js?v=1.65"></script>
    <?php /*if($this->uri->segment(1) =='sale'){ ?>
    <script src="<?php echo base_url(); ?>assets/js/sale.js?v=1.38"></script>
    <?php }*/ ?>
    <?php if($this->uri->segment(1) =='sale'){ ?>
    <script src="<?php echo base_url(); ?>assets/js/mega-sale.js"></script>
    <?php } ?>
    <?php if($this->uri->segment(1) =='daydream'){ ?>
    <script src="<?php echo base_url(); ?>assets/js/single-brand.js"></script>
    <?php } ?>
    <?php if($this->uri->segment(1) =='wedding-pop-up'){ ?>
    <script src="<?php echo base_url(); ?>assets/js/festive-sale.js?v=2.0"></script>
    <?php } ?>
    <?php if($this->uri->segment(1) == 'all-products' || $this->uri->segment(1) == 'brand'){ ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sc.js?v=1.90"></script>
    <?php } ?>
    <?php if( $this->uri->segment(1) == 'all-black-products'){ ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sc-black.js"></script>
    <?php } ?>
	<?php if($this->uri->segment(1) =='party-pop-up'){ ?>
    <script src="<?php echo base_url(); ?>assets/js/party-sale.js?v=1.0"></script>
    <?php } ?>
	<?php if($this->uri->segment(1) =='all-things-new' ){ ?>
    <script src="<?php echo base_url(); ?>assets/js/newyear-sale.js"></script>
    <?php } ?>
    <?php if($this->uri->segment(1) =='all-things-black'){ ?>
    <script src="<?php echo base_url(); ?>assets/js/party-sale.js?v=1.0"></script>
    <?php } ?>
    <?php if($this->uri->segment(1) == 'valentine-pop-up' || $this->uri->segment(1) == 'travel-shop'){ ?>
    <script src="<?php echo base_url(); ?>assets/js/popup-sale.js?v=1.1"></script>
    <?php } ?>
	<?php if($this->uri->segment(1) == 'summer-pop-up' ){ ?>
    <script src="<?php echo base_url(); ?>assets/js/summer-sale.js?v=1.1"></script>
    <?php } ?>
    <?php  if($this->uri->segment(1) =='sc-box' || $this->uri->segment(1) =='book-scbox' ){ ?>

      <script src="<?php echo base_url(); ?>assets/seventeen/js-scbox/plugins.settings.js?v=1.000005"></script>
      <script src="<?php echo base_url(); ?>assets/seventeen/js-scbox/custom.js?v=1.000006"></script>
    <script src="<?php echo base_url(); ?>assets/seventeen/js-scbox/scbox.js?v=1.000018"></script>
      <?php } ?>
   
<!--
	<script id="_webengage_script_tag" type="text/javascript">
	var webengage; !function(e,t,n){function o(e,t){e[t[t.length-1]]=function(){r.__queue.push([t.join("."),arguments])}}var i,s,r=e[n],g=" ",l="init options track screen onReady".split(g),a="feedback survey notification".split(g),c="options render clear abort".split(g),p="Open Close Submit Complete View Click".split(g),u="identify login logout setAttribute".split(g);if(!r||!r.__v){for(e[n]=r={__queue:[],__v:"5.0",user:{}},i=0;i<l.length;i++)o(r,[l[i]]);for(i=0;i<a.length;i++){for(r[a[i]]={},s=0;s<c.length;s++)o(r[a[i]],[a[i],c[s]]);for(s=0;s<p.length;s++)o(r[a[i]],[a[i],"on"+p[s]])}for(i=0;i<u.length;i++)o(r.user,["user",u[i]]);setTimeout(function(){var f=t.createElement("script"),d=t.getElementById("_webengage_script_tag");f.type="text/javascript",f.async=!0,f.src=("https:"==t.location.protocol?"https://ssl.widgets.webengage.com":"http://cdn.widgets.webengage.com")+"/js/widget/webengage-min-v-5.0.js",d.parentNode.insertBefore(f,d)})}}(window,document,"webengage");
	webengage.init("82618045");
	</script>
-->
  <script type="text/javascript">
  <!-- Facebook Pixel Code -->    

    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?

    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;

    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;

    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,

    document,'script','https://connect.facebook.net/en_US/fbevents.js');     

    fbq('init', '889940927729546');

    fbq('track', "PageView");</script>

    <noscript><img height="1" width="1" style="display:none"

    src="https://www.facebook.com/tr?id=889940927729546&ev=PageView&noscript=1"

    /></noscript>
<!-- End Facebook Pixel Code -->

<!-- Invite Code Start -->
 <div id='invtrflfloatbtn'></div>
<script>	
var sc_user_id = "<?php echo $this->session->userdata('user_id')!='' ? $this->session->userdata('user_id') : ''; ?>";
if(sc_user_id>0){
	$('#invite_copy').hide();
	$('#invite_message').hide();
	var invite_referrals = window.invite_referrals || {}; (function() { 
		invite_referrals.auth = { bid_e : 'AC70A50D47DD5C5FF203ED7121693FC4', bid : '11257', t : '420', email : sc_beatout_email_id, fname : sc_beatout_username };	
	var script = document.createElement('script');script.async = true;
	script.src = (document.location.protocol == 'https:' ? "//d11yp7khhhspcr.cloudfront.net" : "//cdn.invitereferrals.com") + '/js/invite-referrals-1.0.js';
	var entry = document.getElementsByTagName('script')[0];entry.parentNode.insertBefore(script, entry); })();
}else {
	$('#invite_copy').show();
}

	//If the site opens on mobile app container the header will hide. 09-Dec-2016

	var is_app = "<?php echo $this->session->userdata('is_app')!='' ? $this->session->userdata('is_app') : ''; ?>";
	if(is_app ==1){ $('#header').hide(); }
	
</script>
<!-- Invite Code END -->

<?php if($this->session->userdata('user_id') > 0) { $gender = ''; 
  if($this->session->userdata('gender') == 1){ $gender = 'Women'; }
  if($this->session->userdata('gender') == 2){ $gender = 'Men'; }
 ?>
<script>
  window.intercomSettings = {
    app_id: "oz2nj7x5",
    name: "<?php echo @$this->session->userdata('user_name'); ?>", // Full name
    email: "<?php echo @$this->session->userdata('email'); ?>", // Email address
    user_id: "<?php echo @$this->session->userdata('user_id'); ?>", // User ID
    created_at: "<?php echo strtotime(date('Y-m-d H:i:s')) ?>", // Signup date as a Unix timestamp
    "Bucket" : "<?php echo @$this->session->userdata('bucket'); ?>",
    "Gender" : "<?php echo $gender; ?>",
    user_hash: "<?php echo hash_hmac('sha256',$this->session->userdata('user_id'),'rK2JpjpInEcvXCtXb9yRzOFRLTmR4wqLfVuVPTal'); ?>"
  };
</script>
<?php }else{ ?>
<script>
  window.intercomSettings = {app_id: "oz2nj7x5"};
</script>
<?php } ?>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/oz2nj7x5';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>

</body>
</html>
<?php if($this->session->userdata('utm_source1') == 'invitereferrals'){  ?>
 <img style="position:absolute; visibility:hidden" src="https://www.ref-r.com/campaign/t1/settings?bid_e=AC70A50D47DD5C5FF203ED7121693FC4&bid=11257&t=420&event=register&email=<?php echo $this->session->userdata("email"); ?>&orderID=<?php echo $this->session->userdata("user_id"); ?>&fname=<?php echo $this->session->userdata("first_name"); ?>" />
 <?php //$this->session->unset_userdata('utm_source1'); 
} ?>