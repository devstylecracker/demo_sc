<?php
ini_set("zlib.output_compression", "On");
ini_set("zlib.output_compression_level", "1");

$user_name = $this->session->userdata('user_name');
$email = $this->session->userdata('email');
$user_id = $this->session->userdata('user_id');
$bucket = $this->session->userdata('bucket');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <meta name="robots" content="noindex">
  <title><?php echo $this->seo_title!='' ? $this->seo_title : 'StyleCracker' ; ?></title>
  <meta content="<?php echo $this->seo_desc; ?>" name="Description">
  <meta content="<?php echo $this->seo_keyword; ?>" name="Keywords">
  <meta property="og:title" content="<?php echo $this->seo_title!='' ? $this->seo_title : 'StyleCracker' ; ?>" />
  <meta property="og:image" content="<?php echo $this->seo_image!='' ? $this->seo_image : 'https://www.stylecracker.com/assets/images/sc-logo-m.png'; ?>"/>
  <meta property="og:description" content="<?php echo $this->seo_desc; ?>" />
  <meta property="og:type" content="website" />
  <meta property="og:site_name" content="StyleCracker" />

  <?php 
        if($this->seo_canonical!='')
        {
  ?>
  <link rel="canonical"  href="<?php echo $this->seo_canonical; ?>" />
  <meta property="og:url" content="<?php echo $this->seo_canonical; ?>" />
  <?php
        }
  ?>
  <?php 
        if($this->seo_prev!='')
        {
  ?>
          <link rel="prev" href="<?php echo $this->seo_prev; ?>" />
  <?php
        }

         if($this->seo_next!='')
        {
  ?>
          <link rel="next" href="<?php echo $this->seo_next; ?>" />
  <?php
        }
  ?>
  <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/fav-icon.jpg">


  <?php // add css files
   /* $this->minify->css(array('bootstrap.min.css', 'flexslider.min.css','bootstrap-select.min.css','../plugins/chosen/bootstrap-chosen.css','font-awesome.css'));
    echo $this->minify->deploy_css();*/
  ?>
  <link href='<?php echo base_url(); ?>assets/css/base.min.css' rel='stylesheet' type='text/css'>
  <link href='<?php echo base_url(); ?>assets/plugins/swiper/css/swiper.min.css' rel='stylesheet' type='text/css'>
  <link href='<?php echo base_url(); ?>assets/plugins/magnificpopup/magnific-popup.css' rel='stylesheet' type='text/css'>
  <link href='<?php echo base_url(); ?>assets/plugins/perfect-scrollbar/css/perfect-scrollbar.min.css' rel='stylesheet' type='text/css'>
  <link href='<?php echo base_url(); ?>assets/css/custom.css?v=1.2315' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,300' rel='stylesheet' type='text/css'>

  <?php  if($this->uri->segment(1) =='sc-box' || $this->uri->segment(1) =='book-scbox'  ){?>
  <link href='<?php echo base_url(); ?>assets/seventeen/iconfonts/style.css' rel='stylesheet' type='text/css'>
  <link href='<?php echo base_url(); ?>assets/seventeen/css-scbox/common.css?v=1.003' rel='stylesheet' type='text/css'>
  <link href='<?php echo base_url(); ?>assets/seventeen/css-scbox/btns.css?v=1.003' rel='stylesheet' type='text/css'>
  <link href='<?php echo base_url(); ?>assets/seventeen/css-scbox/custom.css?v=1.009' rel='stylesheet' type='text/css'>
  <link href='<?php echo base_url(); ?>assets/seventeen/css-scbox/mobile.css?v=1.003' rel='stylesheet' type='text/css'>
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>sc_admin/assets/seventeen/plugins/datepicker/datepicker3.css" type='text/css'>
  <?php } ?>
  
  <link rel="manifest" href="/manifest.json">
  <script type="text/javascript">
     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-64209384-1', 'auto');
          ga('require','linkid','linkid.js');
          ga('require', 'displayfeatures');
          ga('send', 'pageview');
          
          <?php if(isset($_GET['search']) && $_GET['search'] != ""){echo "ga('send', 'event', 'Search', 'keyword', '".$_GET['search']."');";} ?>

         var sc_beatout_email_id = "<?php echo $this->session->userdata('email')!='' ? $this->session->userdata('email') : ''; ?>";
         var sc_beatout_username = "<?php echo $this->session->userdata('user_name')!='' ? $this->session->userdata('user_name') : ''; ?>";
		 var sc_user_id = "<?php echo $this->session->userdata('user_id')!='' ? $this->session->userdata('user_id') : ''; ?>";
		 var sc_invite_utm_source = "<?php echo $this->session->userdata('utm_source')!='' ? $this->session->userdata('utm_source') : ''; ?>";
         var CE_SNAPSHOT_NAME = "Looks";

          setTimeout(function(){var a=document.createElement("script");

          var b=document.getElementsByTagName("script")[0];

          a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0040/4266.js?"+Math.floor(new Date().getTime()/3600000);

          a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
          
  </script>
  </head>
  <body>
   <!-- //FB Initialization
  // FB APP Note -->
    <div id="fb-root"></div>
    <script>
   window.fbAsyncInit = function() {
      FB.init({
      appId      : '1650358255198436', // Live
      //appId      : '781978078605154', // SC Nest
      xfbml      : true,
      version    : 'v2.4' // Live
      //version    : 'v2.5' // SC nest
      });
    };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
/*
  var _bout = _bout || [];
  var _boutAKEY = "v4nhzasejhq4nlz8quj4m2w2cad0zuq8gjy4y0dawu", _boutPID = "30021"; 
  var d = document, f = d.getElementsByTagName("script")[0], _sc = d.createElement("script"); _sc.type = "text/javascript"; _sc.async = true; _sc.src = "https://d22vyp49cxb9py.cloudfront.net/jal-v2.min.js"; f.parentNode.insertBefore(_sc, f);
  */
       var sc_beatout_email_id = "<?php echo $this->session->userdata('email')!='' ? $this->session->userdata('email') : ''; ?>";
       var sc_beatout_username = "<?php echo $this->session->userdata('user_name')!='' ? $this->session->userdata('user_name') : ''; ?>";
	   var sc_beatout_user_id = "<?php echo $this->session->userdata('user_id')!='' ? $this->session->userdata('user_id') : ''; ?>";
	   var sc_gender = "<?php echo $this->session->userdata('gender')!='' ? $this->session->userdata('gender') : ''; ?>";
	   if(sc_gender == 1) {sc_beatout_gender ='women'; } else sc_beatout_gender ='men'; 
    </script>

<!-- popover -->

<?php

$popup = $this->home_model->get_popup();

if(!empty($popup)){
  foreach($popup as $val){
    if(current_url() == $val['page_link'] || $val['page_link']==''){
      if($val['popup_interval_chosen'] == 1){
        setcookie('popup_timer',30*60000, time() + (86400 * 30), "/");
      }else if($val['popup_interval_chosen'] == 2){
        setcookie('popup_timer',40*60000, time() + (86400 * 30), "/");
      }else if($val['popup_interval_chosen'] == 3){
        setcookie('popup_timer',50*60000, time() + (86400 * 30), "/");
      }else if($val['popup_interval_chosen'] == 4){
        setcookie('popup_timer',60*60000, time() + (86400 * 30), "/");
      }else if($val['popup_interval_chosen'] == 5){
        setcookie('popup_timer',90*60000, time() + (86400 * 30), "/");
      }
	
?>
<div id="slideover" class="sc-slideover top" style="display:none;">
  <div class="container11">
    <a href="<?php echo $val['target_link']; ?>" onclick="ga('send', 'event', 'Web Popup', 'clicked', '<?php echo $val['popup_name']; ?>');" target="_blank"><img src="<?php echo base_url(); ?>assets/images/popups/desktop/<?php echo $val['desktop_img']; ?>" /></a>
  </div>
  <div class="close">
    <i class="fa fa-close1 fa-times-circle"></i>
  </div>
</div>
<?php  } } } ?>

<!-- //popover -->
<?php include('nav_view.php'); ?>
