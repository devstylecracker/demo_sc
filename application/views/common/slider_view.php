<script type="text/Javascript">
function msg(m){ console.log(m); }
var OSName="Unknown OS";
if (navigator.appVersion.indexOf("Win")!=-1) OSName="Windows";
if (navigator.appVersion.indexOf("Mac")!=-1) OSName="MacOS";
if (navigator.appVersion.indexOf("X11")!=-1) OSName="UNIX";
if (navigator.appVersion.indexOf("Linux")!=-1) OSName="Linux";


function ScrollDown(){
		$("html, body").animate({ scrollTop: "350px"}, 600);
		return false;
}
</script>

<div class="container custom">
	<div class="home-banner-wrp">
			<div class="swiper-container main">
					<ul class="swiper-wrapper">
						<?php $last_id = 0;
						if(!empty($slider_data)){
							foreach($slider_data as $row){
						?>
											<li class="swiper-slide slide<?php echo $row['banner_position']; ?>">
												<?php if($row['lable_link'] != ''){ ?><a onclick="<?php echo $row['banner_url']; ?>" href="<?php echo $row['lable_link']; ?>" ><?php } ?>
												<img src="<?php echo $this->config->item('homepage_banner_url').$row['slide_url']; ?>"  />
												<?php if($row['caption_line_1'] != '' && $row['caption_line_1'] != 'slide1' && $row['caption_line_1'] != 'slide2' && $row['caption_line_1'] != 'slide3' && $row['caption_line_1'] != 'slide4' && $row['caption_line_1'] != 'slide5'){?>
												<div class="container">
													<div class="flex-caption">
											              <span class="line1"><?php echo $row['caption_line_1']; ?></span>
											              <span class="line2"><?php echo $row['caption_line_2']; ?></span>
											    	</div>
												</div>
												<?php }?>
												<?php if($row['lable_link'] != ''){ ?></a> <?php } ?>
											 </li>
											 <?php
 											$last_id = $row['id'];
 											}// end of for loop
 											}//end of if loop
 											?>
						</ul>
						<div class="swiper-button-next main"></div>
						<div class="swiper-button-prev main"></div>
						<div class="swiper-pagination main"></div>
		</div>
</div>
</div>
