<div class="page-sitemap">
<section class="section section-shadow">
  <div class="container">
   <div class="title-section">
    <h1 class="title">Sitemap</h1>
  </div>
   
    <div class="sitemap-listing">
    <ul class="row">
		<li class="col-md-4"><a href="https://www.stylecracker.com/">Home</a></li>
		<li class="col-md-4"><a href="https://www.stylecracker.com/brands/">Brands</a></li>
		<li class="col-md-4"><a href="https://www.stylecracker.com/blog/">SC Live</a></li>
		<li class="col-md-4"><a href="https://www.stylecracker.com/about-us">About Us</a></li>
		<li class="col-md-4"><a href="https://www.stylecracker.com/faq">FAQ</a></li>
		<li class="col-md-4"><a href="https://www.stylecracker.com/login">Login</a></li>
		<li class="col-md-4"><a href="https://www.stylecracker.com/signup">Signup</a></li>
		<li class="col-md-4"><a href="https://www.stylecracker.com/sc-box">SC BOX</a></li>
		<li class="col-md-4"><a href="https://www.stylecracker.com/categories/women">WOMEN</a></li>
		<li class="col-md-4"><a href="https://www.stylecracker.com/categories/men">MEN</a></li>
		<li class="col-md-4"><a href="https://www.stylecracker.com/contact-us">CONTACT US</a></li>
      </ul>
    </div>

    <div class="sitemap-listing">
	  <div class="title-section">
    <h2 class="subtitle">Products</h2>
  </div>       
      <ul class="row">
<?php
    $i=0;
    foreach($products as $u) {
                $i++;
?>
    <li class="col-md-4"><a href="<?php echo base_url().'product/'. $u['slug'].'-'.$u['id']; ?>"><?php echo trim(str_replace('#', " ", $u['name'])); ?></a></li>

<?php            if($i == 100)
                break;
    }
    ?>
  </ul>
</div>
    <div class="sitemap-listing">
	  <div class="title-section">
    <h2 class="subtitle">Brands</h2>
  </div>  

  <ul class="row">
      <?php
    foreach($brands as $url) {$i++;
 ?>
    <li class="col-md-4"><a href="<?php echo base_url().'brands/'.$url['user_name'].'-'.$url['user_id']; ?>"><?php echo trim($url['company_name']); ?></a></li>
<?php
             if($i == 200)
                break;
    }
    ?>
</ul>
</div>


</div>
</section>
</div>
