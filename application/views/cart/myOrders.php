<?php //echo '<pre>';print_r($myProductOrders);
?>
<div class="page-design-1 page-orders page-common">
    <div class="container">
      <div class="title-section">
          <h1 class="title"><span>Order History</span></h1>
      </div>
      <div class="content-wrp">
      <div class="content-inner order-details">
        <div class="panel-group">
          <?php if(!empty($myOrders)){  $i = 0;
                                $priceProduct =0;
                        $scDiscountPrice = 0;
            foreach($myOrders as $val){
              ?>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="row">
                    <div class="col-md-3"> <a data-toggle="collapse" href="#order<?php echo $val['id']; ?>" class="btn btn-sm btn-secondary <?php echo $i == 0 ? 'his_active':'' ; ?>"><span class="view-order">View Order</span> <?php if($i == 0){ ?><i class="fa fa-angle-double-down"></i><?php }else{ ?><i class="fa fa-angle-double-right"></i><?php } ?></a></div>
                    <div class="col-md-3"><span class="font-bold">Order ID:</span> <?php echo $val['order_display_no']; ?></div>
                    <div class="col-md-2"><span class="font-bold">Date:</span> <?php echo date_format(date_create($val['created_datetime']),'d/m/Y'); ?></div>
                    <div class="col-md-2"><span class="font-bold" id="order_amount_<?php echo $i; ?>"></span></div>
                    <!--div class="col-md-2"><span class="font-bold">Status:</span> <span class="text-orange"></span></div-->
                  </div>
                </div>
              </div>
              <div id="order<?php echo $val['id']; ?>" class="panel-collapse collapse">
                <div class="panel-body">
                  <div class="order-details-inner">
                    <div class="table-responsive">
                      <table class="table table-striped">
                        <tbody>
                          <tr>
                            <th class="text-center">Sr. No.</th>
                            <th>Image</th>
                            <th>Product Name</th>
                            <th>Size</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Seller</th>
                            <th>Status</th>
                          </tr>
                          <?php $k = 1; if(!empty($myProductOrders)){
                            foreach ($myProductOrders as $pro_info) {
                              if($pro_info['order_unique_no'] == $val['order_unique_no']){
                                $data =array();
                                $data['coupon_discount'] = 0;
                                $data['coupon_code'] = '';
                                $coupon_code = $val['coupon_code'];
                                $coupon_brand = $this->Cart_model->getMyOrderCoupon($pro_info['coupon_code'],$val['created_datetime'],$val['brand_id'])['brand_id'];
                                 $coupon_discount_type = $this->Cart_model->getMyOrderCoupon($pro_info['coupon_code'],$val['created_datetime'],$val['brand_id'])['coupon_discount_type'];
                                 $coupon_products = $this->Cart_model->getMyOrderCoupon($coupon_code,$val['created_datetime'],$val['brand_id'])['coupon_products'];

                                  $price = round($pro_info['product_qty']*($pro_info['price']-($pro_info['price']*$pro_info['discount_percent']/100)));
                                 //$price = $pro_info['price'];
                                 $coupon_product_price = 0;
                                 $coupon_min_spend = $this->Cart_model->getMyOrderCoupon($coupon_code,$val['created_datetime'],$val['brand_id'])['coupon_min_spend'];
                                 $coupon_max_spend = $this->Cart_model->getMyOrderCoupon($coupon_code,$val['created_datetime'],$val['brand_id'])['coupon_max_spend'];
                                 $coupon_discount_type = $this->Cart_model->getMyOrderCoupon($coupon_code,$val['created_datetime'],$val['brand_id'])['coupon_discount_type'];
                                 $coupon_stylecracker =  $this->Cart_model->getMyOrderCoupon($coupon_code,$val['created_datetime'],$val['brand_id'])['stylecrackerDiscount'];
                                 $individual_use_only =  $this->Cart_model->getMyOrderCoupon($coupon_code,$val['created_datetime'],$val['brand_id'])['individual_use_only'];  
                                 $coupon_products =  $this->Cart_model->getMyOrderCoupon($coupon_code,$val['created_datetime'],$val['brand_id'])['coupon_products'];  
                                 $appliedcoupon_code = '';
                                 
                                 if($coupon_brand==$val['brand_id'] && $coupon_brand!=0 )
                                {
                                  if($coupon_products!=''&& $coupon_products!=0)
                                  {
                                     $coupon_products_arr = explode(',',$coupon_products);

                                     //coupon_discount_type =3 (Product discount)
                                    if($coupon_discount_type==3)
                                    { 
                                      if(in_array($pro_info['product_id'], $coupon_products_arr))
                                      {
                                        if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                                        {
                                          $data['coupon_discount'] = $val['coupon_amount'];
                                          $data['coupon_code'] = $val['coupon_code'];
                                        }
                                      }
                                    }
                                     //coupon_discount_type =4 (Product % discount)
                                    if($coupon_discount_type==4)
                                    {
                                      if(in_array($pro_info['product_id'], $coupon_products_arr))
                                      {
                                        if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                                        {
                                          $coupon_percent = $val['coupon_amount'];
                                          $data['coupon_discount'] = $price*($coupon_percent/100);
                                          $data['coupon_code'] = $val['coupon_code'];
                                        }
                                      }
                                    }

                                  }else
                                  {                                    
                                     //coupon_discount_type =1 (cart discount)
                                    if($coupon_discount_type==1)
                                    {                                      
                                      if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                                      {
                                        'coupon_discount--'.$data['coupon_discount'] = $val['coupon_amount'];
                                        'coupon_code--'.$data['coupon_code'] = $val['coupon_code'];
                                      }
                                    }
                                     //coupon_discount_type =2 (cart % discount)
                                    if($coupon_discount_type==2)
                                    {
                                      $coupon_product_price = $coupon_product_price+$price;
                                      if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                                      {
                                        $coupon_percent = $val['coupon_amount'];
                                        $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                                        $data['coupon_code'] = $val['coupon_code'];
                                      }
                                    }
                                  }
                                }

                                if(($coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only!=1  )|| ( $coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only==1 ))
                                { 
                                    //coupon_discount_type =1 (cart discount)
                                   $data['coupon_discount'] = $val['coupon_amount'];
                                  /*echo 'OrderIdProductSum--'.$pro_info['OrderIdProductSum'].' OrderProductTotal--'.$pro_info['OrderProductTotal'].' coupon_discount_type-'.$coupon_discount_type.' coupon_discount--'.$data['coupon_discount'];*/
                                     $coupDiscountPrdPrice = $this->Cart_model->calculate_product_discountAmt($pro_info['OrderIdProductSum'],$pro_info['OrderProductTotal'],$coupon_discount_type,$data['coupon_discount']);
                                    
                                        if($coupon_discount_type==1)
                                        {
                                         /* $coupon_product_price = $coupon_product_price+$price;*/
                                          $coupon_product_price = $pro_info['OrderProductTotal'];                         
                                          if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                                          {    
                                            //$data['coupon_discount'] = round($coupDiscountPrdPrice) ;
                                            $data['coupon_discount'] = $val['coupon_amount'];
                                            $data['coupon_code'] = $val['coupon_code'];                   
                                          }
                                        }
                                      //coupon_discount_type =2 (cart % discount)
                                        if($coupon_discount_type==2)
                                        {                                          
                                          /*$coupon_product_price = $pro_info['OrderProductTotal'];
                                          if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                                          {
                                            $coupon_percent = $data['coupon_discount'];  
                                            $coupDiscountPrdPrice = $coupon_product_price*($coupon_percent/100);
                                            $data['coupon_discount'] = $coupDiscountPrdPrice;
                                            //$data['coupon_discount'] = $val['coupon_amount'];
                                            $data['coupon_code'] = $val['coupon_code'];                      
                                          }*/
                                          $coupon_discount_type = 2;
                                          $coupDiscountPrdPrice = $this->cartnew_model->calculate_product_discountAmt_percent($pro_info['product_price'],0,$coupon_product_price,$coupon_discount_type,$coupon_percent);
                                             if($coupon_product_price>0)
                                             {       
                                                $product_percent = round((($pro_info['product_price']*100)/$totalProductPrice),2);
                                             }
                                             $referalDiscount= round(($product_percent*$referral_point)/100,2);
                                             $coupDiscountPrdPrice = ($coupDiscountPrdPrice-$referalDiscount);
                                        }
                                }    

                                ?>
                                <tr>
                                  <td class="text-center"><?php echo $k; ?></td>
                                  <td><div class="item-img">
                                    <img src="<?php echo $this->config->item('sc_promotional_look_image').$pro_info['image']; ?>">
                                  </div>
                                </td>
                                <td> <?php echo $pro_info['name']; ?> </td>
                                <td> <?php echo $pro_info['size_text']; ?> </td>
                                <td> <?php echo $pro_info['product_qty']; ?> </td>
                                 <td><?php
                                      $priceProduct = $priceProduct+$price;
                                      echo number_format($pro_info['product_price'],2);
                                 ?></td>
                                <td> <?php echo $pro_info['company_name']; ?> </td>
                                <td><span class="text-orange"><?php if($pro_info['order_status']==0 || $pro_info['order_status']==1){ echo 'Processing'; }else if($pro_info['order_status'] == 2){ echo 'Confirmed'; }else if($pro_info['order_status'] == 3){ echo 'Dispatched';  }else if($pro_info['order_status'] == 4){ echo 'Delivered'; }else if($pro_info['order_status'] == 5){ echo 'Cancelled'; }else if($pro_info['order_status'] == 6){ echo 'Return'; } ?></span></td>
                              </tr>
                              <?php $k++; }  } } ?>
                              <tr class="seller-amount">
                                <td colspan="8">
                                  <?php if($val['order_tax_amount'] > 0){ ?>
                                    <div class="seller-tax">
                                      <span class="text-label">Tax Charges:</span>
                                      <span class="cart-price">  <span class="fa fa-inr"></span> <?php echo number_format(round($val['order_tax_amount'],2),2); ?></span></div>
                                      <?php } ?>
                                      <?php if($val['cod_amount'] > 0){ ?>
                                        <div class="seller-tax"><span class="text-label"> COD Charges:</span>
                                          <span class="cart-price">  <span class="fa fa-inr"></span> <?php echo $val['cod_amount']!='' ? number_format(round($val['cod_amount'],2),2) : 0; ?></span></div>
                                          <?php } ?>
                                          <?php if($val['shipping_amount'] > 0){ ?>
                                            <div class="seller-tax"><span class="text-label"> Shipping Charges:</span>
                                              <span class="cart-price">  <span class="fa fa-inr"></span> <?php echo $val['shipping_amount']!='' ? number_format(round($val['shipping_amount'],2),2) : 0; ?></span>
                                            </div>
                                            <?php } ?>
                                            <?php if($val['coupon_code'] !=''){ ?>
                                            <div class="seller-tax"><span class="text-label"> Coupon Code <?php echo strtoupper($val['coupon_code']); ?> Discount: </span>
                                              <span class="cart-price"> - <span class="fa fa-inr"></span> <?php echo $data['coupon_discount']!='' ? number_format($data['coupon_discount'],2) : 0; ?></span>
                                            </div>
                                            <?php } ?>
                                            <!--div class="seller-subtotal"><span class="text-label"><?php echo $val['company_name']; ?> Sub Total:</span> <span class="cart-price"><span class="fa fa-inr"></span> 311</span></div-->
                                            <?php $redeem_amt = $this->cartnew_model->get_refre_amt($val['order_unique_no']); 
                                             if($redeem_amt>0){ ?>
                                            <div class="seller-tax"><span class="text-label"> <?php echo REDEEM_POINTS_CART_TEXT; ?> :</span>
                                              <span class="cart-price"> - <span class="fa fa-inr"></span> <?php echo $redeem_amt!='' ? number_format(round($redeem_amt),2) : 0; ?></span>
                                            </div>
                                            <?php } ?>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                  <div class="row">
                                      <?php if($val['billing_address']!=NULL) { ?>
                                    <div class="col-md-3">
                                  <?php }else{ ?>
                                    <div class="col-md-6">
                                  <?php } ?>
                                      <div class="address-box">
                                        <div class="title">
                                          Shipping Address:
                                        </div>
                                        <div class="address">
                                          <?php echo $val['first_name'].' '.$val['last_name']; ?><br>
                                          <?php echo $val['shipping_address']; ?><br>
                                          <?php echo $val['state_name']; ?> ,<?php echo $val['city_name']; ?>  - <?php echo $val['pincode']; ?>

                                        </div>
                                      </div>
                                    </div>

                                     <?php if($val['billing_address']!=NULL) { ?>
                                      <div class="col-md-3">
                                       <div class="address-box">
                                            <div class="title">
                                                Billing Address:
                                            </div>
                                            <div class="address">
                                            <?php $billing_address = json_decode(@$val['billing_address']); ?>
                                             <?php echo $billing_address->billing_first_name.' '.$billing_address->billing_last_name; ?><br>
                                             <?php echo $billing_address->billing_address; ?><br>
                                             <?php echo $billing_address->billing_city; ?> - <?php echo $billing_address->billing_pincode_no; ?>
                                            </div>
                                            </div>
                                      </div>
                                      <?php } ?>

                                    <div class="col-md-6">
                                      <div class="order-amount">
                                        <div class="order-total">Order Total : <span class="cart-price" id="order_total_<?php echo $i; ?>"><span class="fa fa-inr"></span> <?php echo number_format((round($val['order_total'],2)-round($data['coupon_discount'],2)-$redeem_amt),2); ?></span></div>
                                      </div>
                                      <?php if(isset($sc_discount_info))
                                    {
                                        if($sc_discount_info['discount_type_id'] == 4 && $priceProduct > $sc_discount_info['max_amount'])
                                            {
                                  ?>
                                  <div class="order-total">Stylecracker Discount <?php echo $sc_discount_info['discount_percent'];?> % OFF on Amount > <?php echo $sc_discount_info['max_amount'];?>: <span class="cart-price"><span class="fa fa-inr"></span>
                                    <?php
                                      $scDiscountPrice = ($priceProduct*$sc_discount_info['discount_percent'])/100;
                                    echo number_format($scDiscountPrice,2); ?></span></div>
                                  <div class="order-total">Net Total : <span class="cart-price"><span class="fa fa-inr"></span>
                                    <?php echo number_format(($val['order_total']-$scDiscountPrice),2); ?></span></div>
                                  <?php
                                            }
                                      }
                                   ?>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <?php $i++;  }  }else{ echo "<h5>You have not placed any orders.</h5>"; } ?>
                          </div>
                        </div>


                      </div>

                      <br><br>

                    </div>

</div>

<input type="hidden" name="order_count" id="order_count" value="<?php echo $i; ?>">

<script>

window.onload = function(){
  calculate_order_amount();
};

function calculate_order_amount(){
  var total_orders = $('#order_count').val();

  while(total_orders>=0){

    $('#order_amount_'+total_orders).html('Amount: ' + $('#order_total_'+total_orders).html());

    total_orders--;
  }
}
</script>
