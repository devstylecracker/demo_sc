<?php $brandwise_cost = array(); $storetax = 0; $brand_tax_cal = 0; $total_tax = 0; $coupon_discount=0; $total_cod = 0; $cod_charges = 0; $shipping_charges = 0; $cod_error = 0; $shina_error =0; $stock=0; $sreferral_point=0; ?>
<div class="panel-wrp">
<ul class="cart-items">
 <?php if(!empty($cart_info)){ $product_price = 0; ?>
 <?php foreach($cart_info as $val) { $product_price = $product_price + $val['discount_price']; ?>
  <li>
    <div class="row">
      <div class="col-md-2 col-sm-2 col-xs-2 ">
        <div class="cart-item-img">
          <img src="<?php echo $this->config->item('sc_promotional_look_image_url').$val['image']; ?>">
        </div>
      </div>
      <div class="col-md-10 col-sm-10 col-xs-10">
        <div class="row">
          <div class="col-md-9 col-sm-6 col-xs-12">
            <div class="cart-item-name"><?php echo $val['name']; ?> (<?php echo $val['size_text']; ?>)</div>
          </div>
          <?php if(strchr($val['cod_exc_location'],$pincode)!='' || $val['cod_available'] != 1){ ?>
                             <div class="delivery-status cod-status error">COD is not available</div>
                             <?php } ?>
                             <?php if(strchr($val['shipping_exc_location'],$pincode)!=''){ ?>
                             <div class="delivery-status cod-status error">Shipping is not available</div>
                             <?php } ?>
                             <?php if($val['in_stock'] == 1) { ?>
                                  <div class="text-red">
                                    Product Is Out of Stock
                                  </div>
                              <?php  $stock =  $stock + 1; }  ?>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="item-price">
                <div class="price"><i class="fa fa-inr"></i><?php echo number_format((float)$val['discount_price'], 2, '.', ''); ?></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </li>
     <?php $brandwise_cost[$val['user_id']][] =  $val['discount_price']; } ?> 
      </ul>
    <?php } ?>
        <div class="order-charges-wrp">
      <div class="order-charges order-subtotal">
        <span class="text-label">Price</span>
        <span class="cart-price"> <span class="fa fa-inr"></span> <?php echo number_format((float)$product_price, 2, '.', ''); ?></span>
      </div>
       <?php if(!empty($brandwise_cost)) { 
                          foreach($brandwise_cost as $key=>$v){ 
                            $brand_tax_cal =  array_sum($brandwise_cost[$key]);
                            $storetax = $this->cartnew_model->getBrandExtraInfo($key)[0]['store_tax'];
                            $total_tax = $total_tax + $brand_tax_cal*($storetax/100);

                            /* COD charges start*/
                          if($this->cartnew_model->getBrandExtraInfo($key)[0]['is_cod'] == 1 && $sc_cart_pay_mode=='cod'){    
                              if($this->cartnew_model->getBrandExtraInfo($key)[0]['cod_min_value'] == 0 && $this->cartnew_model->getBrandExtraInfo($key)[0]['code_max_value'] == 0){

                                $cod_charges = $cod_charges + $this->cartnew_model->getBrandExtraInfo($key)[0]['cod_charges'];

                              }else if($this->cartnew_model->getBrandExtraInfo($key)[0]['cod_min_value'] >= 0 && $this->cartnew_model->getBrandExtraInfo($key)[0]['code_max_value'] > 0 && $this->cartnew_model->getBrandExtraInfo($key)[0]['cod_min_value']<=$brand_tax_cal && $this->cartnew_model->getBrandExtraInfo($key)[0]['code_max_value']>=$brand_tax_cal){
                                
                                $cod_charges = $cod_charges + $this->cartnew_model->getBrandExtraInfo($key)[0]['cod_charges'];
                              }
                          }

                          /* Shipping charges start*/
                          if($this->cartnew_model->getBrandExtraInfo($key)[0]['is_shipping'] == 1){    
                              if($this->cartnew_model->getBrandExtraInfo($key)[0]['shipping_min_values'] == 0 && $this->cartnew_model->getBrandExtraInfo($key)[0]['shipping_max_values'] == 0){

                                $shipping_charges = $shipping_charges + $this->cartnew_model->getBrandExtraInfo($key)[0]['shipping_charges'];

                              }else if($this->cartnew_model->getBrandExtraInfo($key)[0]['shipping_min_values'] >= 0 && $this->cartnew_model->getBrandExtraInfo($key)[0]['shipping_max_values'] > 0 && $this->cartnew_model->getBrandExtraInfo($key)[0]['shipping_min_values']<=$brand_tax_cal && $this->cartnew_model->getBrandExtraInfo($key)[0]['shipping_max_values']>=$brand_tax_cal){
                                
                                $shipping_charges = $shipping_charges + $this->cartnew_model->getBrandExtraInfo($key)[0]['shipping_charges'];
                              }
                          }
                          }
                      } ?>

         <?php if(@$coupon_code_msg['coupon_discount_amount'] > 0) { ?>
            <div class="order-charges order-tax">
              <span class="text-label">Coupon Discount</span>
              <span class="cart-price"> - <span class="fa fa-inr"></span> <?php echo $coupon_discount = number_format((float)$coupon_code_msg['coupon_discount_amount'], 2, '.', ''); ?> </span>
              <div class="discount-name"><?php echo @$_COOKIE['discountcoupon_stylecracker']; ?></div>
            </div>
          <?php } ?>

               <?php if($referral_point > 0 && ($product_price-$coupon_discount)>=MIN_CART_VALUE) { ?>
                          <div class="order-charges order-tax">
                          <input type="checkbox" name="redeem" id="redeem" class="redeem" onchange="redeem_point(this)" <?php if(isset($_COOKIE['is_redeem_point'])==true && $_COOKIE['is_redeem_point'] == 1){ echo 'checked'; } ?>>
                          <span class="text-label"><?php echo REDEEM_POINTS_CART_TEXT; ?></span>
                          <span class="cart-price"> - <span class="fa fa-inr"></span> <?php echo $sreferral_point = number_format((float)$referral_point, 2, '.', ''); ?> </span>
                          <?php if(isset($_COOKIE['is_redeem_point'])==true && $_COOKIE['is_redeem_point'] == 1){
                              $sreferral_point = $sreferral_point;
                            }else{ $sreferral_point = 0; }
                            ?>
                          </div>
                      <?php } ?>

      <div class="order-charges order-tax">
        <span class="text-label">Tax</span>
        <span class="cart-price"> <span class="fa fa-inr"></span> <?php echo $total_tax  = number_format((float)$total_tax, 2, '.', ''); ?></span>
      </div>

      <!--div class="order-charges order-shipping-charges">
        <span class="text-label">Shipping</span>
        <span class="cart-price"> <span class="fa fa-inr"></span> 450</span>
      </div-->
         
          <?php if($cod_charges > 0) { ?>
                    <div class="order-charges order-tax">
                      <span class="text-label">COD Charges</span>
                      <span class="cart-price"> <span class="fa fa-inr"></span> <?php echo $cod_charges  = number_format((float)$cod_charges, 2, '.', ''); ?></span>
                    </div>
                    <?php } ?>

                    <div class="order-charges order-tax">
                      <span class="text-label">Shipping Charges</span>
                      <span class="cart-price"> <span class="fa fa-inr"></span> <?php echo $shipping_charges  = number_format((float)$shipping_charges, 2, '.', ''); ?></span>
                    </div>

                    <!--div class="order-charges order-shipping-charges">
                      <span class="text-label">Shipping</span>
                      <span class="cart-price"> <span class="fa fa-inr"></span> 450</span>
                    </div-->


                    <?php $cart_total = $product_price + $total_tax + $shipping_charges + $cod_charges - $coupon_discount- $sreferral_point; ?>
                    
                    <div class="order-charges order-total-amount">
                      <span class="text-label">Order Total</span>
                      <span class="cart-price"> <span class="fa fa-inr"></span> <?php echo number_format((float)$cart_total, 2, '.', ''); ?></span>
                    </div>
        <div class="panel-bottom">
          <button class="btn btn-primary" onclick="mobile_hide_order_summary();">Next</button>
        </div>
    </div>
        </div>