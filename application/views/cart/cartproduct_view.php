<?php // echo "<pre>"; print_r($cart_info);
$product_price = 0; $brandwise_cost = array(); $storetax = 0; $brand_tax_cal = 0; $total_tax = 0; $coupon_discount = 0; $order_total = 0; $cod_error = 0; $shina_error =0;  $qty=0; $stock =0; $sreferral_point= 0;
?>
    <?php if(!empty($cart_info)){ ?>
      <form name="sccart_frm" id="sccart_frm" method="post">
      <div class="col-md-9 col-sm-9 col-xs-12 custom">

        <div class="cart-box-wrp">
          <div class="panel-wrp">
              <ul class="cart-items">
                <li class="cart-items-title">
                  <div class="row">
                    <div class="col-md-2 col-sm-2 col-xs-2 ">
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-10">
                      <div class="row">
                        <div class="col-md-5 col-sm-6 col-xs-6 text-left">
                          Item
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2 ">
                          Size
                        </div>
                        <div class="col-md-3 col-sm-2 col-xs-2 ">
                          QTY
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                          Price
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <?php $c = 0; foreach($cart_info as $val){  $qty = $qty+$val['product_qty']; ?>
                    <li id="cart_<?php echo $val['id']; ?>">
                      <div class="row">
                        <div class="col-md-2 col-sm-2 col-xs-2 ">
                          <div class="cart-item-img">
                            <img src="<?php echo $this->config->item('products_310').$val['image']; ?>">
                          </div>
                        </div>
                        <div class="col-md-10 col-sm-10 col-xs-10">
                        <div class="cart-row-wrp">
                          <div class="row">
                            <div class="col-md-5 col-sm-6 col-xs-12">
                              <div class="cart-item-name"><a href="<?php echo base_url(); ?>product/details/<?php echo $val['slug']; ?>-<?php echo $val['product_id']; ?>" target="_blank"><?php echo $val['name']; ?></a></div>
                              <span class="cart-item-seller">
                                <span class="text-label">Seller:</span> <a target="_blank" href="<?php echo base_url(); ?>brands/brand-details/<?php echo $val['brand_unique_name']; ?>"> <?php echo $val['company_name']; ?> </a>
                              </span>
                              <div class="return-policy-wrp">
                              <?php if(trim($val['return_policy'])!=''){ ?>
                                <span class="sc-popover-btn" title="" data-toggle="sc-popover" data-content="<div class='return-policy-box'><p>
                                  <?php echo $val['return_policy']; ?></p></div>">Return Policy </span>
                              <?php } ?>
                              <?php if(trim($val['return_policy'])!='' && ($val['min_del_days']>0 || $val['max_del_days']>0)){ ?>
                                |
                              <?php } ?>
                              <?php if($val['min_del_days']>0 && $val['max_del_days']>0){ ?>
                                 <span class="shipping-status">
                                  Delivery in <?php echo $val['min_del_days'].'-'.$val['max_del_days']; ?> working days.
                                </span>
                              <?php }else if(($val['min_del_days']>0 && $val['max_del_days']=='') || ($val['min_del_days']=='' && $val['max_del_days']>0)){ ?>
                                 <span class="shipping-status">
                                  Delivery in <?php echo $val['min_del_days'].$val['max_del_days']; ?> working days.
                                </span>
                              <?php } ?>

                                  </div>
                              <?php if($val['in_stock'] == 1) { ?>
                                  <div class="text-red">
                                    Product Is Out of Stock
                                  </div>
                              <?php  $stock =  $stock + 1; }  ?>
                              </div>
                              <div class="col-md-2 col-sm-2 col-xs-5">
                              <?php if(!empty($val['product_all_size'])) { ?>
                                <select class="form-control input-sm change-size" name="scsize<?php echo $val['id']; ?>" id="scsize<?php echo $val['id']; ?>">
                                  <?php foreach($val['product_all_size'] as $sizes){ ?>
                                  <option value="<?php echo $sizes['id']; ?>" <?php echo $sizes['id'] == $val['product_size'] ? 'selected' : ''; ?>><?php echo $sizes['size_text']; ?></option>
                                  <?php } ?>
                                </select>
                              <?php } ?>
                              </div>
                              
                              <div class="col-md-3 col-sm-2 col-xs-7">
                              <?php if($val['in_stock'] != 1) { ?>
                                <div class="qty-control-wrp">
                                 <span class="qtyminus" field='quantity'><i onclick="change_qty('min',<?php echo $val['id']; ?>);" class="fa fa-minus-circle"></i></span>
                                 <input type='text' name='quantity<?php echo $val['id']; ?>' id='quantity<?php echo $val['id']; ?>' value='<?php echo $val['product_qty']; ?>' class='form-control input-sm input-qty' readonly />
                                 <span class="qtyplus" field='quantity'><i onclick="change_qty('plus',<?php echo $val['id']; ?>);" class="fa fa-plus-circle"></i></span>
                               </div>
                                <?php } ?>
                              </div>
                             
                              <div class="col-md-2 col-sm-3 col-xs-12">
                                <div class="item-price">
                                  <?php if($val['price'] == $val['discount_price']){
                                    $product_price = $product_price + $val['price'];
                                    $brandwise_cost[$val['user_id']][] =  $val['price'];
                                  ?>
                                  <div class="price"><i class="fa fa-inr"></i> <?php echo number_format((float)$val['price'], 2, '.', ''); ?></div>
                                  <?php }else{
                                    $product_price = $product_price + $val['discount_price'];
                                    $brandwise_cost[$val['user_id']][] =  $val['discount_price'];
                                   ?>
                                  <div class="price"><i class="fa fa-inr"></i> <?php echo number_format((float)$val['discount_price'], 2, '.', ''); ?></div>
                                  <div class="mrp-price"><i class="fa fa-inr"></i> <?php echo number_format((float)$val['price'], 2, '.', ''); ?></div>
                                  <?php } ?>
                                  <?php if($val['discount_percent'] > 0) { ?>
                                  <div class="discount-price">(<?php echo round($val['discount_percent']); ?>% OFF)</div>
                                  <?php } ?>
                                  <span class="remove-item"><i class="fa fa-times-circle" onclick="scremovecart(<?php echo $val['id']; ?>);"></i></span>
                                </div>
                              </div>
                            </div>


                            <?php if($pincode!='' && strchr($val['cod_exc_location'],$pincode)!='' || $val['cod_available'] != 1){ ?>
                             <!--<div class="delivery-status cod-status error">COD is not available</div>-->
                             <?php $cod_error = $cod_error + 1; } ?>
                             <?php if($pincode!='' && strchr($val['shipping_exc_location'],$pincode)!=''){ ?>
                             <?php $shina_error = $shina_error + 1; ?>
                             <div class="delivery-status cod-status error">Shipping is not available</div>
                             <?php } ?>
                             </div>
                          </div>
                        </div>
                      </li>
                    <?php $c++; ?>
                      <input type="hidden" name="scitem<?php echo $c; ?>" id="scitem<?php echo $c; ?>" value="<?php echo $val['id']; ?>">
                    <?php } ?>
                    </ul>

                    <div class="panel-bottom cart-box-footer clearfix">
                      <a  class="btn btn-tertiary" href="<?php echo base_url(); ?>">Continue Shopping</a>
                       <button class="btn btn-primary hide" id="scupdatecart1" disabled onclick="sc_updatecart(0);">Update Cart <i class="fa fa-refresh"></i> </button>
                        <a class="btn btn-primary" onclick="check_cart_availability();" id="proceed_to_checkout1">Proceed to Checkout <i class="fa fa-arrow-right"></i></a>
                    </div>
                  </div>
                  </div>
                </div>

                <div class="col-md-3 col-sm-3 col-xs-12 custom">

                  <div class="coupon-wrp">
                    <label> Enter Coupon Code
                      <span class="sccolor btn-link <?php if(@$_COOKIE['discountcoupon_stylecracker']=='') { ?>hide<?php } ?>" id="coupon_clear" onclick="clear_coupon_code();">(Clear)</span>
                    </label> <input type="text" class="form-control input-sm" name="sccoupon_code" id="sccoupon_code" placeholder="Coupon" value="<?php echo @$_COOKIE['discountcoupon_stylecracker']; ?>"/> <button class="btn btn-sm btn-secondary" onclick="sc_updatecart(1);">Apply Coupon</button>
                    <?php if(@$coupon_code_msg['msg_type'] == 1){ ?>
                    <span id="coupon_error" class="success"><?php echo @$coupon_code_msg['msg']; ?></span>
                    <?php }else{ ?>
                    <span id="coupon_error" class="error"><?php echo @$coupon_code_msg['msg']; ?></span>
                    <?php } ?>
                  </div>


                  <div class="order-summary-box">
                    <div class="title"><span>Order Summary</span></div>

                    <div class="order-charges-wrp">
                      <div class="order-charges order-subtotal">
                        <span class="text-label">Price</span>
                        <span class="cart-price"> <span class="fa fa-inr"></span> <?php echo $product_price = number_format((float)$product_price, 2, '.', ''); ?></span>
                      </div>
                      <?php if(!empty($brandwise_cost)) {
                          foreach($brandwise_cost as $key=>$v){
                            $brand_tax_cal =  array_sum($brandwise_cost[$key]);
                            $storetax = $this->cartnew_model->getBrandExtraInfo($key)[0]['store_tax'];
                            $total_tax = $total_tax + $brand_tax_cal*($storetax/100);
                          }
                      } ?>
                      <?php if(@$coupon_code_msg['coupon_code'] != '' ) { ?>
                      <div class="order-charges order-tax">
                        <span class="text-label">Coupon Discount</span>
                        <span class="cart-price"> -  <span class="fa fa-inr"></span> <?php echo $coupon_discount = number_format((float)$coupon_code_msg['coupon_discount_amount'], 2, '.', ''); ?> </span>
                        <div class="discount-name"><?php echo @$coupon_code_msg['coupon_code']; ?></div>
                      </div>
                      <?php } ?>

                      <?php if($referral_point > 0 && ($product_price-$coupon_discount)>=MIN_CART_VALUE) { ?>
                          <div class="order-charges order-tax">
                          <input type="checkbox" name="redeem" id="redeem" class="redeem" onchange="redeem_point_cart(this)" <?php if(isset($_COOKIE['is_redeem_point'])==true && $_COOKIE['is_redeem_point'] == 1){ echo 'checked'; } ?>> 
                          <span class="text-label"><?php echo REDEEM_POINTS_CART_TEXT; ?></span>
                          <span class="cart-price"> - <span class="fa fa-inr"></span> <?php echo $sreferral_point = number_format((float)$referral_point, 2, '.', ''); ?> </span>
                          <?php if(isset($_COOKIE['is_redeem_point'])==true && $_COOKIE['is_redeem_point'] == 1){
                              $sreferral_point = $sreferral_point;
                            }else{ $sreferral_point = 0; }
                            ?>
                          </div>
                      <?php } ?>

                      <div class="order-charges order-tax">
                        <span class="text-label">Tax</span>
                        <span class="cart-price"> <span class="fa fa-inr"></span> <?php echo $total_tax = number_format((float)$total_tax, 2, '.', ''); ?> </span>
                      </div>


                      <?php $order_total = ($product_price + $total_tax) - $coupon_discount - $sreferral_point; ?>
                      <div class="order-charges order-total-amount">
                        <span class="text-label">Order Total</span>
                        <span class="cart-price"> <span class="fa fa-inr"></span> <?php echo number_format((float)($order_total), 2, '.', ''); ?></span>
                      </div>
                    </div>


                    <div class="box-check-pincode form-inline">
                            <div class="form-group">
                            <label>Enter shipping pincode</label>
                            <input id="cart_pincode" class="form-control input-sm cart-pincode" placeholder="Pincode" type="text" name="cart_pincode" maxlength="6" value="<?php if(isset($_COOKIE['stylecracker_shipping_pincode'])){ echo $_COOKIE['stylecracker_shipping_pincode']; }else{ echo $this->session->userdata('pincode'); } ?>">
                            <div id="cart_pincode_error" class="error"></div>
                          </div>
                    </div>

                      <div class="btns-wrp">
                          <button class="btn btn-primary btn-block hide" id="scupdatecart" disabled onclick="sc_updatecart(0);">Update Cart <i class="fa fa-refresh"></i> </button>
                         <a class="btn btn-primary btn-block" onclick="check_cart_availability();" id="proceed_to_checkout">Proceed to Checkout <i class="fa fa-arrow-right"></i> </a>
                      </div>
                  </div>
                  <input type="hidden" name="item_count" id="item_count" value="<?php echo $qty; ?>" />
                  </form>
                </div>
                <?php } else{ 
                  ?>
                  <script type="text/javascript">clearCartCount();</script>
                     <div class="box-empty-cart">
                      <h4>Your shopping cart is empty. </h4>
                      <a  class="btn btn-secondary btn-sm" href="<?php echo base_url(); ?>">Shop Now</a>
                  </div>
				            <input type="hidden" name="item_count" id="item_count" value="0" />
                <?php } ?>
                <input name="shina" id="shina" type="hidden" value="<?php echo $shina_error; ?>">
                <?php

                ?>
