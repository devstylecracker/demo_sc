<link href='<?php echo base_url(); ?>assets/css/cart.css?v=0.2' rel='stylesheet' type='text/css'>
<?php $brandwise_cost = array(); $storetax = 0; $brand_tax_cal = 0; $total_tax = 0; $coupon_discount=0; $add_accordian_class = ''; $style_panel = 'display:block;'; $hide_add = '';
 $first_name = $last_name = $mobile_no = $shipping_address = $pincode = $city_name = $states_name =''; $ga_product_id = '';
 $sreferral_point = 0;
?>
<div class="page-checkout">
  <div class="container">
      <div class="content-wrp">
    <ol class="breadcrumb">
     <li><a href="<?php echo base_url(); ?>">Home</a></li>
      <li><a href="<?php echo base_url(); ?>cartnew">Cart</a></li>
     <li class="active">Checkout</li>
   </ol>
<?php if($referral_point > 0){ ?>
  <div class="red_msg1">
  <?php if( $product_total_for_msg >= MIN_CART_VALUE) { ?>
     <div class="box-mred_msg box-message fb success">Rejoice! Your referral amount( Rs. <?php echo POINT_VAlUE; ?>) has been discounted from your total cart value.
     </div>
  <?php }else{ ?>
     <div class="red_msg_cond box-message fb success">Your cart will be discounted by Rs. <?php echo POINT_VAlUE; ?> on a minimum purchase of Rs.<?php echo MIN_CART_VALUE; ?></div>
    </div>
<?php } } ?>
   <div class="title-section">
     <h1 class="title"><span>Secured Checkout</span></h1>
   </div>

  <?php 
      if(isset($_GET['error']) && $_GET['error']!=''){
        echo '<div class="box-message box-error"><div><b>'.$_GET['error'].'</b></div></div>';
      }
    ?>
    <div class="row custom">
      <div class="col-md-9 col-sm-9 col-xs-12 custom">

        <div class="cart-checkout-wrp">
            <?php if($this->session->userdata('user_id')=='') { ?>
                <div class="panel-wrp">
                  <div class="title clearfix">
                    <h3><span class="panel-tab panel-tab-login" id="login"> Login/Sign Up</span></h3>
                    <?php if(isset($_COOKIE['cart_email']) && $_COOKIE['cart_email']!=''){ $style_panel='display:none;'; ?>
                    <div class="cart-selected-value">
                      <span class="text-label">Email:</span> <span id="cart_email_c"><?php echo $_COOKIE['cart_email'];  ?></span> <a class="btn-link" href="javascript:void(0);" onclick="change_email();">Change</a>
                    </div>
                    <?php }else{ ?>
                     <div class="cart-selected-value hide">
                      <span class="text-label">Email:</span> <span id="cart_email_c"></span> <a class="btn-link" href="javascript:void(0);" onclick="change_email();">Change</a>
                    </div>
                    <?php } ?>
                  </div>

                  <div class="panel-tab-content panel-tab-content-login" id="collapse-options1" style="<?php echo $style_panel; ?>">
                  <?php
                   // if($style_panel=='display:none;' && !isset($_GET['error'])){ $style_panel='display:block;'; }
                  if(!isset($_COOKIE['cart_email']) || $_COOKIE['cart_email']==''){ $style_panel='display:none;'; }else{
                    $style_panel='display:block;';
                  }
                  ?>
                    <!-- signin/signup -->
                      <div class="row">
                        <div class="col-md-12">
                            <div class="box-login-email" id="sc_checkout_login_panel">

                              <label>Please enter your email address</label>
                              <div class="form-control-wrp">
                                <input class="form-control" placeholder="Email Address" type="text" id="cart_email" name="cart_email" value="<?php if(isset($_COOKIE['cart_email'])){ echo $_COOKIE['cart_email']; } ?>">
                                <button type="submit" class="btn btn-primary" onclick="cart_check_email();">Continue</button>
                                <span class="label-or">OR</span> <!--<label class="label-login-with">Login with</label>-->
                               <button class="btn btn-fb" id="cart_facebook_login"><i class="fa fa-facebook"></i> <span >Facebook</span></button>
                               <a href="<?php echo $this->data['google_login_url']; ?>"><button type="submit" class="btn btn-gplus"><i class="fa fa-google-plus"></i> Google +</button></a>
                                <div class="error error-msg" id="email_id_error"></div>
                              </div>
                        </div>
                        <div class="box-password hide">
                            <p class="text-line">You already have an account with us. Please login  </p>
                            <p><span class="email-label"></span>
                               <span class="btn1 btn-primary-two1 btn-xs1 btn-link" onclick="change_email();">Change</span></p>

                              <label>Enter Password</label>
                              <div class="form-control-wrp">
                                <input class="form-control" placeholder="Password" type="password" id="cart_password" name="cart_password">
                                  <button type="submit" class="btn btn-primary" onclick="cart_login();">Continue</button> <span class="label-or">OR</span>
                                  <button type="submit" class="btn btn-secondary" onclick="checkout_as_guest();">Checkout as Guest</button>
                                <div class="error error-msg" id="login_error"></div>
                              </div>
                        </div>

                      </div>
                      </div>
                    <!-- /signin -->
                  </div>
                </div>
                <?php }else{ ?>
                    <div class="panel-wrp">
                        <div class="title clearfix">
                          <h3><span class="panel-tab1 panel-tab-login" id="login1"> Hi <?php echo $this->session->userdata('first_name').' '.$this->session->userdata('last_name'); ?></span></h3>
                          <div class="cart-selected-value"><span class="text-label">Email:</span> <?php echo $this->session->userdata('email'); ?></div>
                        </div>
                    </div>
                <?php } ?>

                <?php
                  if((isset($_GET['error']) && $_GET['error']!='') || @$_COOKIE['shipping_address']!=''){ $style_panel='display:none;'; }
                  ///if($style_panel == 'display:block') { $style_panel='display:none;'; }
                   ?>

                <div class="panel-wrp">

                  <div class="title clearfix">
                    <h3><span class="panel-tab panel-tab-payment" id="address"> Deliver To</span></h3>
                    <div class="cart-selected-value" id="user_address_billing_shipping">
                    <?php if($style_panel=='display:none;') { if(@$_COOKIE['shipping_address']!=''){ ?>
                        <div class="fb"><?php echo @$_COOKIE['shipping_first_name'].' '.@$_COOKIE['shipping_last_name']; ?></div>
                        <div><?php echo @$_COOKIE['shipping_address']; ?></div>
                        <div><?php echo @$_COOKIE['shipping_city']; ?>,
                        <?php echo @$_COOKIE['shipping_state']; ?>,
                        <?php echo @$_COOKIE['stylecracker_shipping_pincode']; ?></div>
                        <a class="btn-link" onclick="open_address_panel();" href="javascript:void(0);">Change</a>
                    <?php }else{ //$style_panel = 'display:block;'; 
                    } } ?>
                    </div>

                  </div>


                  <div class="panel-tab-content panel-tab-content-address" id="collapse-options2" style="<?php echo $style_panel; ?>">
                  <?php if(!empty($this->data['user_existing_address'])){  ?><strong>Shipping Address</strong><?php } ?>
              <input class="form-control" placeholder="Email Address" type="hidden" value="<?php echo $this->session->userdata('email'); ?>" id="cart_email" name="cart_email">
                    <?php if(!empty($this->data['user_existing_address'])){  ?>

                    <div class="address-list-wrp">
                    <div class="row">
                    <?php $ad=1;  foreach($this->data['user_existing_address'] as $uadd){
                          $first_name = $uadd['first_name'];
                          $last_name = $uadd['last_name'];
                          $mobile_no = $uadd['mobile_no'];
                          $shipping_address = $uadd['shipping_address'];
                          $pincode = $uadd['pincode'];
                          $city_name = $uadd['city_name'];
                          $states_name = $uadd['state_id'];
                      ?>

                      <div class="col-md-6 user_address_<?php echo $uadd['id']; ?>" id="user_address_<?php echo $uadd['id']; ?>">
                        <div id="suser_address_<?php echo $uadd['id']; ?>" class="item-address <?php if($uadd['is_default'] == 1) { /*echo 'active';*/ } ?>">
                        <div class="item-address-inner">
                          <?php echo $uadd['shipping_address']; ?>
                          <?php echo $uadd['city_name']; ?> -
                          <?php echo $uadd['states_name']; ?> -
                          <?php echo $uadd['pincode']; ?>
                        </div>

                          <div class="btns-wrp">
                          <?php if($uadd['is_default'] == 1) { 
							$_COOKIE['billing_first_name'] = $first_name;
							$_COOKIE['billing_last_name'] = $last_name;
							$_COOKIE['billing_mobile_no'] = $mobile_no;
							$_COOKIE['billing_address'] = $shipping_address;
							$_COOKIE['shipping_pincode_no'] = $pincode;
							$_COOKIE['billing_city'] = $city_name;
							$_COOKIE['billing_state'] = $states_name;
							
							$_COOKIE['shipping_first_name'] = $first_name;
							$_COOKIE['shipping_last_name'] = $last_name;
							$_COOKIE['shipping_mobile_no'] = $mobile_no;
							$_COOKIE['shipping_address'] = $shipping_address;
							$_COOKIE['stylecracker_shipping_pincode'] = $pincode;
							$_COOKIE['shipping_city'] = $city_name;
							$_COOKIE['shipping_state'] = $states_name;
						  ?>
                            <span class="label-selected"><i class="fa fa-check"></i> Default Address</span>
                          <?php }else{ ?>
                            <button class="btn btn-xs btn-ship-here btn-primary-two" onclick="user_old_address(<?php echo $uadd['id']; ?>,'SHIP');">Ship Here</button>
                          <?php } ?>
                            <div class="pull-right">
                            <button class="btn btn-xs btn-tertiary btn-edit" onclick="edit_user_address(<?php echo $uadd['id']; ?>)">Edit</button>
                            <button class="btn btn-xs btn-tertiary btn-delete" onclick="delete_user_address(<?php echo $uadd['id']; ?>);">Delete</button>
                            </div>
                          </div>
                        </div>
                      </div>

                    <?php $hide_add = 'display:none;'; $ad++; } ?>
                    </div>
                    <div id="address_error" class="hide error">Please select existing address otherwise add new address</div>
                    <div class="" id="sc_cart_add_new_address">
                      <label class="btn btn-sm btn-secondary show-address-box-wrp" onclick="add_new_address();"> Add New Address</label>
                    </div>
                    </div>
                    <?php } ?>


                    <div class="row" id="add_new_address" style="<?php echo $hide_add; ?>">
                     

                      <div class="col-md-6">
                        <div class="address-box billing-address-box" style="display:block;">
                          <div class="title">
                            Delivery Address
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name" name="shipping_first_name" id="shipping_first_name" value="<?php if(isset($_COOKIE['shipping_first_name'])){ echo $_COOKIE['shipping_first_name']; }else{ echo $first_name; } ?>">
                                <span id="shipping_first_name_error" class="error error-msg"></span>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="Last Name" name="shipping_last_name" id="shipping_last_name" value="<?php if(isset($_COOKIE['shipping_last_name'])){ echo $_COOKIE['shipping_last_name']; }else{ echo $last_name; } ?>">
                                <span id="shipping_last_name_error" class="error error-msg"></span>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" maxlength="10" class="form-control" placeholder="Mobile" name="shipping_mobile_no" id="shipping_mobile_no" value="<?php if(isset($_COOKIE['shipping_mobile_no'])){ echo $_COOKIE['shipping_mobile_no']; }else{ echo $mobile_no; } ?>">
                                <span id="shipping_mobile_no_error" class="error error-msg"></span>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" maxlength="6" class="form-control" placeholder="Area Pincode"  name="shipping_pincode_no" id="shipping_pincode_no" value="<?php echo @$_COOKIE['stylecracker_shipping_pincode']; ?>" onblur="check_pincode_shipping();">
                                <span id="shipping_pincode_no_error" class="error error-msg"></span>
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                <textarea maxlength="100" class="form-control" placeholder="Address" name="shipping_address" id="shipping_address"><?php if(isset($_COOKIE['shipping_address'])){ echo $_COOKIE['shipping_address']; }else{ echo $shipping_address; } ?></textarea>
                                <span id="shipping_address_error" class="error error-msg"></span>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="City"  name="shipping_city" id="shipping_city"  value="<?php if(isset($_COOKIE['shipping_city'])){ echo $_COOKIE['shipping_city']; }else{ echo $city_name; } ?>">
                                <span id="shipping_city_error" class="error error-msg"></span>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <select name="shipping_state" id="shipping_state" class="form-control">
                                  <option value="">Select State</option>
                                   <?php if(!empty($this->data['all_uc_states'])){
                                    foreach($this->data['all_uc_states'] as $state){
                                      if(isset($_COOKIE['shipping_state']) && ($_COOKIE['shipping_state']==$state['id'] || $states_name==$state['id'])){
                                        echo '<option value="'.(int)$state['id'].'" selected>'.$state['state_name'].'</option>';
                                      }else{
                                        echo '<option value="'.(int)$state['id'].'">'.$state['state_name'].'</option>';
                                      }
                                    }
                                  } ?>
                                </select>
                                <span id="shipping_state_error" class="error error-msg"></span>
                              </div>
                            </div>
                          </div>

                          <label class="sc-checkbox111 active"><input id="allow_same_shipp" name="allow_same_shipp" type="checkbox" class="check-billing-address1" checked/>Use this for my billing address</label>
                        </div>
                      </div>
                    </div>


                        <!-- Edit Address Start -->
                     
                     <div class="row" id="edit_new_address" style="display:none;">
                      <div class="col-md-6">
                        <div class="address-box add-address-box">
                          <div class="title">
                            Edit Address
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name" name="edit_first_name" id="edit_first_name" value="">
                                <span id="edit_first_name_error" class="error error-msg"></span>
                              </div>

                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="Last Name" name="edit_last_name" id="edit_last_name" value="">
                                <span id="edit_last_name_error" class="error error-msg"></span>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" maxlength="10" class="form-control" placeholder="Mobile" name="edit_mobile_no" id="edit_mobile_no" value="">
                                <span id="edit_mobile_no_error" class="error error-msg"></span>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" maxlength="6" class="form-control" placeholder="Area Pincode" name="edit_pincode_no" id="edit_pincode_no" value="" onblur="edit_pincode_check();">
                                <span id="edit_pincode_no_error" class="error error-msg"></span>
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                <textarea maxlength="100" class="form-control" placeholder="Address" name="edit_address" id="edit_address"></textarea>
                                <span id="edit_address_error" class="error error-msg"></span>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="City" name="edit_city" id="edit_city" value="">
                                <span id="edit_city_error" class="error error-msg"></span>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <select name="edit_state" id="edit_state" class="form-control">
                                  <option value="">Select State</option>
                                 <?php if(!empty($this->data['all_uc_states'])){
                                    foreach($this->data['all_uc_states'] as $state){
                                     
                                        echo '<option value="'.(int)$state['id'].'">'.$state['state_name'].'</option>';
                                     }
                                  } ?>
                                </select>
                                <span id="edit_state_error" class="error error-msg"></span>
                              </div>
                            </div>
                          </div>
                          <input type="hidden" name="edit_address_id" id="edit_address_id" value="">
                      </div>
                      <div>
                        
                    <button class="btn btn-secondary btn-sm" type="button" name="sc_cart_cancel" id="sc_cart_cancel" onclick="cancel_edit_address();">Cancel</button>
                      <button class="btn btn-secondary btn-sm" type="button" name="sc_cart_save" id="sc_cart_save" onclick="edit_existing_address();">Save</button>
                      </div>
                      </div>

                      </div>
                    <!-- Edit Address End -->

                    <div class="panel-bottom">
                        <!--a class="btn btn-secondary pull-left" href="<?php echo base_url(); ?>cartnew">View Cart</a-->
                    <button class="btn btn-primary" type="button" name="sc_cart_next" id="sc_cart_next" onclick="valiate_shipping_address();">Next &nbsp;<i class="fa fa-angle-right"></i></button>
                    </div>

                  </div>
                </div>

                <div class="panel-wrp sm-only">
                  <div class="title clearfix">
                      <h3><span class="panel-tab panel-tab-summary" id="panel-tab-summary">Order Summary</span></h3>
                      <a href="<?php echo base_url(); ?>cartnew" class="btn-link" title="Edit Cart">Edit</a>
                  </div>
                  <div class="panel-tab-content panel-tab-content-summary" id="order_summary_mobile">
<!-- -->
<div class="cart-box-wrp" id="mobile_order_summary">
<div class="panel-wrp">
<ul class="cart-items">
 <?php if(!empty($cart_info)){ $t = 0; $product_price = 0; $stock=0; ?>
 <?php foreach($cart_info as $val) { $product_price = $product_price + $val['discount_price']; 
 if($t!=0){ $ga_product_id = $ga_product_id.','; }
    $ga_product_id = $ga_product_id.$val['product_id'];
 ?>
  <li>
    <div class="row">
      <div class="col-md-2 col-sm-2 col-xs-2 ">
        <div class="cart-item-img">
          <img src="<?php echo $this->config->item('sc_promotional_look_image_url').$val['image']; ?>">
        </div>
      </div>
      <div class="col-md-10 col-sm-10 col-xs-10">
        <div class="row">
          <div class="col-md-9 col-sm-6 col-xs-12">
            <div class="cart-item-name"><?php echo $val['name']; ?> (<?php echo $val['size_text']; ?>)</div>
          </div>
          <?php if($val['in_stock'] == 1) { ?>
                                  <div class="text-red">
                                    Product Is Out of Stock
                                  </div>
                              <?php  $stock =  $stock + 1; }  ?>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="item-price">
                <div class="price"><i class="fa fa-inr"></i><?php echo number_format((float)$val['discount_price'], 2, '.', ''); ?></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </li>
     <?php $brandwise_cost[$val['user_id']][] =  $val['discount_price']; $t++; } ?>
      </ul>
    <?php } ?>
        <div class="order-charges-wrp">
      <div class="order-charges order-subtotal">
        <span class="text-label">Price</span>
        <span class="cart-price"> <span class="fa fa-inr"></span> <?php echo number_format((float)$product_price, 2, '.', ''); ?></span>
      </div>
       <?php if(!empty($brandwise_cost)) {
                          foreach($brandwise_cost as $key=>$v){
                            $brand_tax_cal =  array_sum($brandwise_cost[$key]);
                            $storetax = $this->cartnew_model->getBrandExtraInfo($key)[0]['store_tax'];
                            $total_tax = $total_tax + $brand_tax_cal*($storetax/100);
                          }
       } ?>

         <?php if(@$coupon_code_msg['coupon_discount_amount'] > 0) { ?>
            <div class="order-charges order-tax">
              <span class="text-label">Coupon Discount(<?php echo @$_COOKIE['discountcoupon_stylecracker']; ?>)</span>
              <span class="cart-price"> - <span class="fa fa-inr"></span> <?php echo $coupon_discount = number_format((float)$coupon_code_msg['coupon_discount_amount'], 2, '.', ''); ?> </span>
            </div>
          <?php } ?>


          <?php if($referral_point > 0 && ($product_price-$coupon_discount)>=MIN_CART_VALUE) { ?>
                          <div class="order-charges order-tax">
                          <input type="checkbox" name="redeem" id="redeem" class="redeem" onchange="redeem_point(this)" <?php if(isset($_COOKIE['is_redeem_point'])==true && $_COOKIE['is_redeem_point'] == 1){ echo 'checked'; } ?>>
                          <span class="text-label"><?php echo REDEEM_POINTS_CART_TEXT; ?></span>
                          <span class="cart-price"> - <span class="fa fa-inr"></span> <?php echo $sreferral_point = number_format((float)$referral_point, 2, '.', ''); ?> </span>
                          <?php if(isset($_COOKIE['is_redeem_point'])==true && $_COOKIE['is_redeem_point'] == 1){
                              $sreferral_point = $sreferral_point;
                            }else{ $sreferral_point = 0; }
                            ?>
                          </div>
                      <?php } ?>

      <div class="order-charges order-tax">
        <span class="text-label">Tax</span>
        <span class="cart-price"> <span class="fa fa-inr"></span> <?php echo $total_tax  = number_format((float)$total_tax, 2, '.', ''); ?></span>
      </div>

      <!--div class="order-charges order-shipping-charges">
        <span class="text-label">Shipping</span>
        <span class="cart-price"> <span class="fa fa-inr"></span> 450</span>
      </div-->

      <?php $cart_total = $product_price + $total_tax - $coupon_discount-$sreferral_point; ?>
                    <div class="order-charges order-total-amount">
                      <span class="text-label">Order Total</span>
                      <span class="cart-price"> <span class="fa fa-inr"></span> <?php echo number_format((float)$cart_total, 2, '.', ''); ?></span>
                    </div>
        <div class="panel-bottom">
          <!--a class="btn btn-secondary pull-left" href="<?php echo base_url(); ?>cartnew">View Cart</a-->
          <button class="btn btn-primary" onclick="mobile_hide_order_summary();">Next</button>
        </div>
    </div>
        </div>
    </div>

<!-- -->
                  </div>
                </div>
                <?php 
                  /*if(isset($_GET['error']) && $_GET['error']!=''){ $style_panel='display:block;'; }else{ $style_panel='display:none'; }*/
                  if(@$_COOKIE['shipping_address']==''){ $style_panel='display:none;'; }else{ $style_panel='display:block;'; }
                  if(@$_COOKIE['billing_address']!='' && @$_COOKIE['shipping_address']!=''){ $style_panel='display:block;'; }else{ $style_panel='display:none;'; }
                  ?>
                <div class="panel-wrp">
                  <div class="title clearfix">
                    <h3><span class="panel-tab panel-tab-payment" id="pay_by"> Billing and Payment</span></h3>
                    <div class="cart-selected-value" id="user_address_billing_shipping_ba">
                    <?php if($style_panel=='display:none;') { if(@$_COOKIE['billing_address']!=''){ ?>
                        <div class="fb"><?php echo @$_COOKIE['billing_first_name'].' '.@$_COOKIE['billing_last_name']; ?></div>
                        <div><?php echo @$_COOKIE['billing_address']; ?></div>
                        <?php echo @$_COOKIE['billing_city']; ?>,
                        <?php echo @$_COOKIE['billing_state']; ?>,
                        <?php echo @$_COOKIE['billing_pincode_no']; ?></div>
                        <a class="btn-link" onclick="open_billing_address_panel();" href="javascript:void(0);">Change</a>
                    <?php } } ?>
                    </div>
                  </div>


                  <div class="panel-tab-content panel-tab-content-payment collapse-options111" id="collapse-options4" style="<?php echo $style_panel; ?> padding-top:0;">
                  <div style="padding:0 10px;">
                     <?php if(!empty($this->data['user_existing_address'])){  ?><div  style="font-weight:bold;font-size:14px;margin-bottom:10px;">Billing Address</div><?php } ?>
                    <?php if(!empty($this->data['user_existing_address'])){  ?>

                    <div class="address-list-wrp">
                    <div class="row">
                    <?php $ad=1;  foreach($this->data['user_existing_address'] as $uadd){
                          $first_name = $uadd['first_name'];
                          $last_name = $uadd['last_name'];
                          $mobile_no = $uadd['mobile_no'];
                          $shipping_address = $uadd['shipping_address'];
                          $pincode = $uadd['pincode'];
                          $city_name = $uadd['city_name'];
                          $states_name = $uadd['state_id'];
                      ?>

                      <div class="col-md-6 user_address_<?php echo $uadd['id']; ?>" id="user_address_<?php echo $uadd['id']; ?>">
                        <div id="buser_address_<?php echo $uadd['id']; ?>" class="item-address <?php if($uadd['is_default'] == 1) { /*echo 'active';*/ } ?>">
                        <div class="item-address-inner">
                          <?php echo $uadd['shipping_address']; ?>
                          <?php echo $uadd['city_name']; ?> -
                          <?php echo $uadd['states_name']; ?> -
                          <?php echo $uadd['pincode']; ?>
                        </div>

                          <div class="btns-wrp">
                          <?php if($uadd['is_default'] == 1) { 
              $_COOKIE['billing_first_name'] = $first_name;
              $_COOKIE['billing_last_name'] = $last_name;
              $_COOKIE['billing_mobile_no'] = $mobile_no;
              $_COOKIE['billing_address'] = $shipping_address;
              $_COOKIE['shipping_pincode_no'] = $pincode;
              $_COOKIE['billing_city'] = $city_name;
              $_COOKIE['billing_state'] = $states_name;
              
              $_COOKIE['shipping_first_name'] = $first_name;
              $_COOKIE['shipping_last_name'] = $last_name;
              $_COOKIE['shipping_mobile_no'] = $mobile_no;
              $_COOKIE['shipping_address'] = $shipping_address;
              $_COOKIE['stylecracker_shipping_pincode'] = $pincode;
              $_COOKIE['shipping_city'] = $city_name;
              $_COOKIE['shipping_state'] = $states_name;
              ?>
                            <span class="label-selected"><i class="fa fa-check"></i> Default Address</span>
                          <?php }else{ ?>
                            <button class="btn btn-xs btn-ship-here btn-primary-two" onclick="user_old_address(<?php echo $uadd['id']; ?>,'BILL');">Bill Here</button>
                          <?php } ?>
                            <!--div class="pull-right">
                            <button class="btn btn-xs btn-tertiary btn-edit" onclick="edit_user_address(<?php echo $uadd['id']; ?>)">Edit</button>
                            <button class="btn btn-xs btn-tertiary btn-delete" onclick="delete_user_address(<?php echo $uadd['id']; ?>);">Delete</button>
                            </div-->
                          </div>
                        </div>
                      </div>

                    <?php $hide_add = 'display:none;'; $ad++; } ?>
                    </div>
                    <div id="address_error" class="hide error">Please select existing address otherwise add new address</div>
                    <div class="" id="sc_cart_add_new_address">
                      <label class="btn btn-sm btn-secondary show-address-box-wrp" onclick="add_new_billing_address();"> Add New Address</label>
                    </div>
                    </div>
                    <?php } ?>

                    <div>
                      <div class="row">
                            <div class="col-md-6">
                      <div class="address-box add-address-box" id="new_ba" style="<?php echo $hide_add; ?>">
                          <div class="title">
                            Billing Address
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name" name="billing_first_name" id="billing_first_name" value="<?php if(isset($_COOKIE['billing_first_name'])){ echo $_COOKIE['billing_first_name']; }else{ echo $first_name; } ?>">
                                <span id="billing_first_name_error" class="error error-msg"></span>
                              </div>

                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="Last Name" name="billing_last_name" id="billing_last_name" value="<?php if(isset($_COOKIE['billing_last_name'])){ echo $_COOKIE['billing_last_name']; }else{ echo $last_name; } ?>">
                                <span id="billing_last_name_error" class="error error-msg"></span>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" maxlength="10" class="form-control" placeholder="Mobile" name="billing_mobile_no" id="billing_mobile_no" value="<?php if(isset($_COOKIE['billing_mobile_no'])){ echo $_COOKIE['billing_mobile_no']; }else{ echo $mobile_no; } ?>">
                                <span id="billing_mobile_no_error" class="error error-msg"></span>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" maxlength="6" class="form-control" placeholder="Area Pincode" name="billing_pincode_no" id="billing_pincode_no" value="<?php if(isset($_COOKIE['stylecracker_shipping_pincode'])){ echo $_COOKIE['stylecracker_shipping_pincode']; }else{ echo $pincode; } ?>">
                                <span id="billing_pincode_no_error" class="error error-msg"></span>
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                <textarea maxlength="100" class="form-control" placeholder="Address" name="billing_address" id="billing_address"><?php if(isset($_COOKIE['billing_address'])){ echo $_COOKIE['billing_address']; }else{ echo $shipping_address; } ?></textarea>
                                <span id="billing_address_error" class="error error-msg"></span>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="City" name="billing_city" id="billing_city" value="<?php if(isset($_COOKIE['billing_city'])){ echo $_COOKIE['billing_city']; }else{ echo $city_name; } ?>">
                                <span id="billing_city_error" class="error error-msg"></span>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <select name="billing_state" id="billing_state" class="form-control">
                                  <option value="">Select State</option>
                                 <?php if(!empty($this->data['all_uc_states'])){
                                    foreach($this->data['all_uc_states'] as $state){
                                      if(isset($_COOKIE['billing_state']) && ($_COOKIE['billing_state']==$state['id'] || $states_name==$state['id'])){
                                        echo '<option value="'.(int)$state['id'].'" selected>'.$state['state_name'].'</option>';
                                      }else{
                                        echo '<option value="'.(int)$state['id'].'">'.$state['state_name'].'</option>';
                                      }
                                    }
                                  } ?>
                                </select>
                                <span id="billing_state_error" class="error error-msg"></span>
                              </div>
                            </div>
                          </div>
                           <div>                        
                          <!--button class="btn btn-secondary btn-sm" type="button" name="sc_cart_cancel" id="sc_cart_cancel" onclick="cancel_edit_address();">Cancel</button>
                            <button class="btn btn-secondary btn-sm" type="button" name="sc_cart_save" id="sc_cart_save" onclick="edit_existing_address();">Save</button-->
                            </div>
              
                          <div>
                                                     </div>

                      

                        </div>
                         </div>
                       </div>

                         
                  <!--        <br>
                      
                    <div class="title clearfix">
                      <h3><span class="panel-tab panel-tab-payment" id="pay_opt"> Payment Methods</span></h3>
                    </div>
                   -->
                   <div class="title111" id="pay_opt111" style="font-weight:bold;font-size:14px;margin-top:30px;margin-bottom:10px;"> Payment Methods</div>
                      <ul class="cart-payment-modes">
                          <?php if(isset($_GET['error']) && $_GET['error']!=''){ ?>
                             <li><label class="sc-radio"><input type="radio" name="sc_cart_pay_mode" id="sc_cart_pay_mode_cod" value="cod" onclick="pay_valiation();"> <span>Cash on Delivery</span></label></li>

                             <li>
                              <label class="sc-radio"><input type="radio" name="sc_cart_pay_mode" id="sc_cart_pay_mode_online" value="card" onclick="pay_valiation();" checked> <span>Online Payment</span></label>
                             </li>

                          <?php }else{ ?>
                             <li><label class="sc-radio"><input type="radio" name="sc_cart_pay_mode" id="sc_cart_pay_mode_cod" value="cod" onclick="pay_valiation();"> <span>Cash on Delivery</span></label></li>

                             <li>
                              <label class="sc-radio"><input type="radio" name="sc_cart_pay_mode" id="sc_cart_pay_mode_online" value="card" onclick="pay_valiation();"> <span>Online Payment</span></label>
                             </li>
                          <?php } ?>
                      </ul>                 


                        </div>
                        <div class="error error-msg hide" id="cart_place_order_msg"></div>
                        <div class="error error-msg hide" id="cart_error_msg">Sorry Shipping is not avaliable for some products</div>
                         </div>
                        <div class="panel-bottom">
                          <!--a class="btn btn-secondary pull-left" href="<?php echo base_url(); ?>cartnew">View Cart</a-->
                          <button class="btn btn-primary" id="place_order_sc_cart" name="place_order_sc_cart" onclick="place_stylecracker_order();">Place Order</button>
                        </div>
                      </div>

                    </div>

                  </div>
                </div>
<?php $brandwise_cost = array(); $storetax = 0; $brand_tax_cal = 0; $total_tax = 0; $coupon_discount=0;  $stock = 0; ?>
                <div class="col-md-3 col-sm-3 col-xs-12 custom md-only">

                      <div class="coupon-wrp">
                          <label> Enter Coupon Code
                            <span class="sccolor btn-link <?php if(@$_COOKIE['discountcoupon_stylecracker']=='') { ?>hide<?php } ?>" id="coupon_clear" onclick="clear_coupon_code();">(Clear)</span>
                          </label>
                           <input type="text" class="form-control input-sm" name="sccoupon_code" id="sccoupon_code" placeholder="Coupon" value="<?php echo @$_COOKIE['discountcoupon_stylecracker']; ?>"/>
                           <button class="btn btn-sm btn-secondary" onclick="apply_coupon_code();">Apply Coupon</button>
                          <?php if(@$coupon_code_msg['msg_type'] == 1){ ?>
                          <span id="coupon_error" class="success"><?php echo @$coupon_code_msg['msg']; ?></span>
                          <?php }else{ ?>
                          <span id="coupon_error" class="error"><?php echo @$coupon_code_msg['msg']; ?></span>
                          <?php } ?>
                      </div>

                  <div class="order-summary-box">
                    <div class="title"><span>Order Summary</span><a href="<?php echo base_url(); ?>cartnew" class="btn-link">Edit</a></div>

                  <div class="order-charges-wrp" id="desktop_order_summary">
                  <?php if(!empty($cart_info)){
                    $product_price = 0;
                  ?>
                    <ul class="cart-summary-items">
                      <?php foreach($cart_info as $val) { $product_price = $product_price + $val['discount_price']; ?>
                       <li>
                         <div class="row">
                           <div class="col-md-3 col-sm-2 col-xs-2 ">
                             <div class="cart-item-img">
                               <img src="<?php echo $this->config->item('sc_promotional_look_image_url').$val['image']; ?>">
                             </div>
                           </div>
                           <div class="col-md-9 col-sm-7 col-xs-7">
                             <div class="cart-item-name"><?php echo $val['name']; ?> (<?php echo $val['size_text']; ?>)</div>
                             <div class="item-price">
                               <span class="price"><i class="fa fa-inr"></i> <?php echo number_format((float)$val['discount_price'], 2, '.', ''); ?></span>
                             </div>
                           </div>
                            <?php if($val['in_stock'] == 1) { ?>
                                  <div class="text-red">
                                    Product Is Out of Stock
                                  </div>
                              <?php  $stock =  $stock + 1; }  ?>
                         </div>
                       </li>
                    <?php $brandwise_cost[$val['user_id']][] =  $val['discount_price']; } ?>
                     </ul>
                  <?php } ?>
                    <div class="order-charges order-subtotal">
                      <span class="text-label">Price</span>
                      <span class="cart-price"> <span class="fa fa-inr"></span> <?php echo number_format((float)$product_price, 2, '.', ''); ?></span>
                    </div>
                    <?php if(!empty($brandwise_cost)) {
                          foreach($brandwise_cost as $key=>$v){
                            $brand_tax_cal =  array_sum($brandwise_cost[$key]);
                            $storetax = $this->cartnew_model->getBrandExtraInfo($key)[0]['store_tax'];
                            $total_tax = $total_tax + $brand_tax_cal*($storetax/100);
                          }
                      } ?>

                     <?php if(@$coupon_code_msg['coupon_discount_amount'] > 0) { ?>
                      <div class="order-charges order-tax">
                        <span class="text-label">Coupon Discount</span>
                        <span class="cart-price"> - <span class="fa fa-inr"></span> <?php echo $coupon_discount = number_format((float)$coupon_code_msg['coupon_discount_amount'], 2, '.', ''); ?> </span>
                        <div class="discount-name"><?php echo @$_COOKIE['discountcoupon_stylecracker']; ?></div>
                      </div>
                      <?php } ?>

                        <?php if($referral_point > 0 && ($product_price-$coupon_discount)>=MIN_CART_VALUE) { ?>
                          <div class="order-charges order-tax">
                          <input type="checkbox" name="redeem" id="redeem" class="redeem" onchange="redeem_point(this)" <?php if(isset($_COOKIE['is_redeem_point'])==true && $_COOKIE['is_redeem_point'] == 1){ echo 'checked'; } ?>>
                          <span class="text-label"><?php echo REDEEM_POINTS_CART_TEXT; ?></span>
                          <span class="cart-price"> - <span class="fa fa-inr"></span> <?php echo $sreferral_point = number_format((float)$referral_point, 2, '.', ''); ?> </span>
                          <?php if(isset($_COOKIE['is_redeem_point'])==true && $_COOKIE['is_redeem_point'] == 1){
                              $sreferral_point = $sreferral_point;
                            }else{ $sreferral_point = 0; }
                            ?>
                          </div>
                      <?php } ?>


                    <div class="order-charges order-tax">
                      <span class="text-label">Tax</span>
                      <span class="cart-price"> <span class="fa fa-inr"></span> <?php echo $total_tax  = number_format((float)$total_tax, 2, '.', ''); ?></span>
                    </div>

                    <?php $cart_total = $product_price + $total_tax - $coupon_discount - $sreferral_point; ?>
                    <div class="order-charges order-total-amount">
                      <span class="text-label">Order Total</span>
                      <span class="cart-price"> <span class="fa fa-inr"></span> <?php echo number_format((float)$cart_total, 2, '.', ''); ?></span>
                    </div>

                  </div>

<!-- -->
                  </div>
				    <div>
                    <a href="#"><i class="fa fa-arrow-circle-up"></i> Proceed to secured checkout </a>
					</div>
</div>
</div>
</div>
</div>
</div>

<input name="stock_exist" id="stock_exist" type="hidden" value="<?php echo $stock; ?>">
<input name="cart_total" id="cart_total" type="hidden" value="<?php echo $cart_total; ?>">

<form action="<?php echo PAYMENT_URL; ?>" method="post" name="stylecracker_style">
  <input type="hidden" name="key" value="<?php echo MERCHANT_KEY; ?>" />
  <input type="hidden" name="productinfo" value='{"paymentParts":[{"name":"splitID1","value":"6","merchantId":"396446","description":"test description","commission":"2"}]}' />
  <input type="hidden" name="hash" id="hash" value=""/>
  <input type="hidden" name="txnid" id="txnid" value="" />
  <input type="hidden" name="amount" id="amount" value="" />
  <input type="hidden" name="surl" id="surl" value="<?php echo SURL; ?>" />
  <!--input type="hidden" name="service_provider" id="service_provider" value="payu_paisa" /-->
  <input type="hidden" name="firstname" id="firstname" value="" />
  <input type="hidden" name="email" id="email" value="" />
  <input type="hidden" name="phone" id="phone" value="" />
  <input type="hidden" name="udf1" id="udf1" value="1,2,3" />
  <input type="hidden" name="udf2" id="udf2" value="" />
  <input type="hidden" name="udf3" id="udf3" value="" />
  <input type="hidden" name="udf4" id="udf4" value="" />
  <input type="hidden" name="udf5" id="udf5" value="" />

</form>
<style>
.page-checkout .panel-wrp .title h3{ float:none; }
.page-checkout .title .cart-selected-value{float:none; }
.page-checkout .panel-wrp {
    margin-bottom: 20px;
}
#invite_copy, .back-to-top{display: none !important;}
</style>
<script type="text/javascript"> 
 var google_tag_params = { ecomm_prodid:<?php echo $ga_product_id; ?>, ecomm_pagetype: 'checkout', ecomm_totalvalue:<?php echo $cart_total; ?>, };

if(('#redeem_msg').length>0){
    var cost = document.getElementsByClassName("red_cond_cost").innerText;
    console.log(cost);
    if(parseInt(cost) < 600){
      $('.redeem_cart_msg').removeClass('hide');
    }
  }
</script>