<!doctype html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta name="Generator" content="EditPlus®">
  <meta name="Author" content="">
  <meta name="Keywords" content="">
  <meta name="Description" content="">
  <title>Document</title>

 </head>
  <body onload="remove_address_cookies();">
<?php
//-------------------------------------GA Ecommerce Tracking Code -----------------------------------------------------------------------------------------------------------
// code for ga_ecommerce 
$order_id = $this->uri->segment(3);
$paymod = $brand_price[0]['pay_mode'];
if($paymod == 1) $paymentMethod = 'cod'; else $paymentMethod = 'online';
$trans = '';
$items = array();
$shipping_amt = $brand_price[0]['shipping_amount']>0 ? $brand_price[0]['shipping_amount'] : 0 ;
$tax = $brand_price[0]['order_tax_amount']>0 ? $brand_price[0]['order_tax_amount'] : 0 ;


$i=0;
$affiliation = '';
$brandName = '';
foreach($res as $val)
{
	
    if($affiliation!='')
    {
      if($brandName != $val['company_name'])
      {
          $affiliation = $affiliation."-".$val['company_name'];
      }
       $brandName = $val['company_name'];
      
    }else
    {
        $brandName = $val['company_name'];
       $affiliation = $val['company_name'];
    }
	// code for items array 
	if(empty($items))
    {
      $items[$i] = array('sku'=>$val['product_id'], 'name'=>$val['name'], 'category'=>'', 'price'=>$val['product_price'], 'quantity'=>$val['product_qty'],'currency'=> 'INR');
    }else
    {
         $items[$i] = array('sku'=>$val['product_id'], 'name'=>$val['name'], 'category'=>'', 'price'=>$val['product_price'], 'quantity'=>$val['product_qty'],'currency'=> 'INR');
    }
    $i++;
}
// code for trans array 
$trans = array('id'=>$order_id, 'affiliation'=>$affiliation,'revenue'=>$brand_price[0]['order_total'], 'shipping'=>$shipping_amt, 'tax'=>$tax,'currency'=> 'INR');
?>
<?php

// Function to return the JavaScript representation of a TransactionData object.
function getTransactionJs(&$trans) {
  return <<<HTML
ga('ecommtracker.ecommerce:addTransaction', {
  'id': '{$trans['id']}',
  'affiliation': '{$trans['affiliation']}',
  'revenue': '{$trans['revenue']}',
  'shipping': '{$trans['shipping']}',
   'tax': '{$trans['tax']}',
  'currency': '{$trans['currency']}'
});
HTML;
}

// Function to return the JavaScript representation of an ItemData object.
function getItemJs(&$transId, &$item) {
  return <<<HTML
ga('ecommtracker.ecommerce:addItem', {
  'id': '$transId',
  'name': '{$item['name']}',
  'sku': '{$item['sku']}',
  'category': '{$item['category']}',
  'price': '{$item['price']}',
   'quantity': '{$item['quantity']}',
	'currency': '{$item['currency']}'
});
HTML;
}
?>
<!-- Begin HTML -->
<script>
ga('create', 'UA-64209384-1', 'auto', {'name': 'ecommtracker'});
ga('require', 'ecommerce');
ga('ecommtracker.require', 'ecommerce');

<?php
echo getTransactionJs($trans);

foreach ($items as &$item) {
  echo getItemJs($trans['id'], $item);
}
?>

ga('ecommtracker.ecommerce:send');
ga('ecommtracker.send', 'pageview');

//-------------------------------------End of GA Ecommerce Tracking Code -----------------------------------------------------------------------------------------------------------
</script>
</body>
</html>
<?php 

// code for beataout 
$order_id = $this->uri->segment(3);
$paymod = $brand_price[0]['pay_mode'];
if($paymod == 1) $paymentMethod = 'cod'; else $paymentMethod = 'online';
$products = '';
foreach($res as $value)
{
  if($products=='')
  {
      $products = '{
					"id": "'.$value['product_id'].'",
					"sku": "'.$value['product_id'].'",
					"name": "'.$value['name'].'",
					"price": '.$value['product_price'].',
					"quantity": '.$value['product_qty'].',
					"categories":[{ 
					   "cat_name":"String",
					   "cat_id":"String",//required
					   "parent_cat_id":"String"//optional
					   },{ 
					   "cat_name":"Board Games",
					   "cat_id":"String",//required
					   "parent_cat_id":"String"
					   }]
				  }';
				  
	  $cart_info = '{
                       
                       "abandon_cart_url":"String",
                       "abandon_cart_deeplink_android":"String",
                       "abandon_cart_deeplink_ios":"String",
                       "total":'.$value['product_price'].',
                       "revenue":'.$value['product_price'].',
                       "currency": "INR"
                      }';

  }
  else
  {

    $products = $products .',{
							"id": "'.$value['product_id'].'",
							"sku": "'.$value['product_id'].'",
							"name": "'.$value['name'].'",
							"price": '.$value['product_price'].',
							"quantity": '.$value['product_qty'].',
							"categories":[{ 
							   "cat_name":"String",
							   "cat_id":"String",//required
							   "parent_cat_id":"String"//optional
							   },{ 
							   "cat_name":"Board Games",
							   "cat_id":"String",//required
							   "parent_cat_id":"String"
							   }]
						  }';					  

  }
}


$betaOut_string = '["customerActivity", "'.$brand_price[0]['email_id'].'", "purchased", {
                    "products":['.$products.'],
                    "orderInfo": {
                        "orderId": "'.$order_id.'",
                        "currency": "INR",
                        "shoppingCartNo": "",
                        "subtotalPrice": '.$brand_price[0]['order_sub_total'].',
                        "totalShipping": '.$brand_price[0]['shipping_amount'].',
                        "totalTaxes": '.$brand_price[0]['order_tax_amount'].',
                        "totalDiscount": 0,
                        "totalPrice": '.$brand_price[0]['order_total'].',
                        "promocode": "XXX",
                        "couponcode": "XXX",
                        "financialStatus": "paid",
                        "discountPercentage": 0,
                        "appType": "web",
                        "paymentMethod": "'.$paymentMethod.'"
                    }
            }]';

?>



<!-- Start: Google Code for Livello Campaign Purchase Conversion Page, From : Rajesh on 12-Feb-2016 -->
<script type="text/javascript">
/* <![CDATA[ */
/*var google_conversion_id = 950917036;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "3zgiCK2l9mMQrK-3xQM";

if (<?php echo $brand_price[0]['order_total']; ?>) {
var google_conversion_value = <?php echo $brand_price[0]['order_total']; ?>
}
else {
var google_conversion_value = 1.00;
}
var google_conversion_currency = "INR";
var google_remarketing_only = false;*/
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/950917036/?value=<?php echo $brand_price[0]['order_total']; ?>&amp;currency_code=INR&amp;label=3zgiCK2l9mMQrK-3xQM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Google Code for Livello Campaign Purchase Conversion Page Ends -->


<div class="page-design-1111 page-orders">

  <div class="title-section">
          <h1 class="title"><span>Order Successful</span></h1>


    </div>

  <div class="content-wrp">
    <div class="container">
      <div class="content-inner order-details order-summary">


      <?php if($paymentMethod == 'cod' && $brand_price[0]['confirm_mobile_no']==0) { ?>
        <div class="order-summary-info">
          <p class="success">Your order <strong><?php echo $order_display_no; ?></strong> has been successfully placed. </p>
          <p class="text-orange">Please verify your mobile number: <?php echo $brand_price[0]['mobile_no']; ?> to process your order.</p>
          <div class="form-inline"><label>Enter OTP</label> <input type="text" name="scopt" id="scopt" placeholder="Enter OTP" class="form-control input-sm">
          <input type="hidden" value="<?php echo $brand_price[0]['mobile_no']; ?>" name="mobile_no" id="mobile_no">
          <input type="hidden" value="<?php echo $this->uri->segment(3); ?>" name="oid" id="oid">
            <div id="scsendotp" class="btn btn-primary btn-sm" onclick="sccartcheckotp();"> Submit </div>
          </div>
          <a id="reload_page" style="display:none;" href="javascript:void(0);" onclick="reg_otp();">Regenerate OTP ?</a>
        </div>
        <?php }else{ ?>
          <p class="success">Your order <strong><?php echo $order_display_no; ?></strong> has been successfully placed.</p>
        <?php } ?>

        <?php if($paymentMethod != 'cod' || $brand_price[0]['confirm_mobile_no']==1) { ?>
        <div class="order-details-inner">
          <div class="table-responsive">
            <table class="table table-striped">
              <tbody>
                <tr>
                  <th width="60">Sr. No.</th>
                  <th>Image</th>
                  <th>Product Name</th>
                  <th>QTY</th>
                  <th>Size</th>
                  <th>Seller</th>
                  <th class="text-right">Price</th>
                </tr>
                <?php  $i = 0;
                        $price =0;
                        $scDiscountPrice = 0;
                  if(!empty($res)){
                  foreach($res as $val){
                    $i++;
                    ?>
                    <tr>
                      <td class="text-center"><?php echo $i; ?></td>
                      <td>
                        <div class="item-img">
                          <img src="<?php echo $this->config->item('sc_promotional_look_image').$val['image'] ?>">
                      </div>
                    </td>
                      <td><?php echo $val['name']; ?> </td>
                      <td><?php echo $val['product_qty']; ?> </td>
                      <td><?php echo  $val['size_text']; ?></td>
                      <td><?php echo $val['company_name']; ?> </td>
                      <td class="text-right"><i class="fa fa-inr"></i> <?php
                      if($val['discount_price']!='' || $val['discount_price']!=0)
                      {
                        echo number_format((float)$val['discount_price'], 2, '.', '');
                       
                        $price = $price+$val['discount_price'];
                      }else
                      {
                       
                         echo number_format((float)$val['product_price'], 2, '.', '');
                        $price = $price+$val['product_price'];
                      }

                      ?></td>
                    </tr>
                    <?php
                  }
                } ?>


              </tbody>
            </table>
          </div>
          <div class="order-summary-amount">
          <div class="row">
            <?php if($brand_price[0]['billing_address']!=NULL) { ?>
            <div class="col-md-3">
          <?php }else{ ?>
            <div class="col-md-6">
          <?php } ?>
             <div class="address-box">
                  <div class="title">
                      Shipping Address:
                  </div>
                  <div class="address">
                    <?php echo $brand_price[0]['first_name'].' '.$brand_price[0]['last_name']; ?><br>
                    <?php echo $brand_price[0]['shipping_address']; ?><br>
                    <?php echo $brand_price[0]['state_name']; ?> ,<?php echo $brand_price[0]['city_name']; ?>  - <?php echo $brand_price[0]['pincode']; ?>

                  </div>
                  </div>
            </div>
            <?php if($brand_price[0]['billing_address']!=NULL) { ?>
            <div class="col-md-3">
             <div class="address-box">
                  <div class="title">
                      Billing Address:
                  </div>
                  <div class="address">
                  <?php $billing_address = json_decode(@$brand_price[0]['billing_address']); ?>
                   <?php echo $billing_address->billing_first_name.' '.$billing_address->billing_last_name; ?><br>
                   <?php echo $billing_address->billing_address; ?><br>
                   <?php echo $billing_address->billing_city; ?> - <?php echo $billing_address->billing_pincode_no; ?>
                  </div>
                  </div>
            </div>
            <?php } ?>
            <div class="col-md-6">
              <div class="order-amount">
                <?php    $cod_amount = $brand_price[0]['cod_amount']>0 ? $brand_price[0]['cod_amount'] : 0 ;
                $shipping_amount = $brand_price[0]['shipping_amount']>0 ? $brand_price[0]['shipping_amount'] : 0 ; ?>
                <div class="order-charges-wrp">
                <?php if($brand_price[0]['order_tax_amount'] > 0) { ?>
                  <div class="seller-tax"><span class="text-label">Tax :</span>
                    <span class="order-summary-price"><span class="fa fa-inr"></span> <?php echo number_format((float)$brand_price[0]['order_tax_amount'], 2, '.', ''); ?></span></div>
                  <?php } ?>
                  <?php if($cod_amount > 0) { ?>
                    <div class="seller-subtotal"><span class="text-label">COD Charges:</span>
                      <span class="order-summary-price"><span class="fa fa-inr"></span> <?php echo number_format((float)$cod_amount, 2, '.', ''); ?></span></div>
                  <?php } ?>
                  <?php if($shipping_amount > 0) { ?>
                      <div class="seller-subtotal"><span class="text-label">Shipping Charges:</span>
                        <span class="order-summary-price"><span class="fa fa-inr"></span> <?php echo number_format((float)$shipping_amount, 2, '.', ''); ?></span></div>
                  <?php } ?>
                   <?php if($coupon_discount > 0) {
                    //$coupon_discount =$this->cartnew_model->cart_coupon($order_id);
                    ?>
                      <div class="seller-subtotal"><span class="text-label">Coupon Code <?php echo $coupon_code; ?> Discount:</span>
                        <span class="order-summary-price"><span class="fa fa-inr"></span> 
                        <?php echo number_format((float)$coupon_discount, 2, '.', ''); ?>
                        </span></div>
                  <?php } ?>
                      </div>

                         <?php $redeem_amt = 0; $redeem_amt =$this->cartnew_model->get_refre_amt($this->uri->segment(3)); ?>
                      <?php if($redeem_amt > 0) { ?>
                          <div class="seller-tax"><span class="text-label"><?php echo REDEEM_POINTS_CART_TEXT; ?> :</span>
                          <span class="order-summary-price"><span class="fa fa-inr"></span> <?php echo number_format((float)$redeem_amt, 2, '.', ''); ?></span></div>
                      <?php } ?>
                      
                      <div class="order-total">Order Total : <span class="cart-price"><span class="fa fa-inr"></span>
                        <?php
                               $order_total = $price -$coupon_discount+$cod_amount+$shipping_amount+round($brand_price[0]['order_tax_amount'],2)-$redeem_amt;
                         ?>
                        <?php echo number_format((float)$order_total, 2, '.', ''); ?>
                        </span></div>
                      </div>

                      <?php if(isset($sc_discount_info))
                            {
                                if($sc_discount_info['discount_type_id'] == 4 && $price > $sc_discount_info['max_amount'])
                                {
                      ?>
                      <div class="order-total">Stylecracker Discount <?php echo $sc_discount_info['discount_percent'];?> % OFF on Amount > <?php echo $sc_discount_info['max_amount'];?>: <span class="cart-price"><span class="fa fa-inr"></span>
                        <?php
                          $scDiscountPrice = ($price*$sc_discount_info['discount_percent'])/100;
                        echo round($scDiscountPrice,2); ?></span></div>
                      <div class="order-total">Net Total : <span class="cart-price"><span class="fa fa-inr"></span>
                        <?php echo round(($brand_price[0]['order_total']-$scDiscountPrice),2); ?></span></div>

                      <?php
                                }
                            }
                       ?>

                    </div>
                  </div>

</div>
                </div>
         <script type="text/javascript">localStorage.setItem('newcartcount', 0);</script>
        <div class="bottom clearfix">
                      <a class="btn btn-secondary pull-left" href="<?php echo base_url(); ?>">Continue Shopping</a>
                      <a class="btn btn-primary pull-right" href="<?php echo base_url(); ?>myOrders">Order History</a>
                </div>
        
                <?php } ?>

                

              </div>
            </div>    </div>
          </div>

<script type="text/javascript">
  
  setTimeout(function(){ 

    document.getElementById('reload_page').style.display='block'; 

   }, 10000);


function reg_otp(){
  location.reload();
}
</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '889940927729546'); // Insert your pixel ID here.
fbq('track', 'PageView');
fbq('track', 'Purchase', {value: '<?php echo round(($brand_price[0]['order_total']-$scDiscountPrice),2); ?>', currency:'INR'});
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=889940927729546&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->