<div class="page-design-1 page-orders">
 <div class="page-header">
   <div class="container">
     <h1>Order History/Order Details</h1>
   </div>
 </div>
 <div class="content-wrp">
   <div class="container">
     <div class="content-inner order-details">
       <div class="panel-group">
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="row">
                <div class="col-md-12"> <a data-toggle="collapse" href="#order1" class="btn btn-primary"><span class="font-bold text-capitalize">Order Id:</span> #65767</a></div>
              <!--  <div class="col-md-3"><span class="font-bold">Date:</span> 30|10|2015</div>
                <div class="col-md-3"><span class="font-bold">Amount:</span> <i class="fa fa-inr"></i> 3617</div>
                <div class="col-md-3"><span class="font-bold">Status:</span> <span class="text-orange">Pending</span></div>-->
            </div>
            </div>
              </div>
            <div id="order1" class="panel-collapse collapse">
              <div class="panel-body">
                <div class="order-details-inner">
              <div class="table-responsive">
              <table class="table table-striped">
                <tbody>
                  <tr>
                  <th>Sr. No.</th>
                  <th>Image</th>
                  <th>Product Name</th>
                  <th>Qty</th>
                  <th>Price</th>
                  <th>Seller</th>
                  <th>Status</th>
                </tr>
                <tr>
                  <td>1</td>
                  <td><img class="item-img" src="assets/images/products/p1.jpg"></td>
                  <td> RenkaGrey Solid Sweater </td>
                  <td> 2 </td>
                  <td><i class="fa fa-inr"></i> 3617</td>
                  <td> Jabong </td>
                  <td><span class="text-orange">Transit</span></td>
                </tr>
                <tr>
                  <td>2</td>
                  <td><img class="item-img" src="assets/images/products/p1.jpg"></td>
                  <td>RG RenkaGrey Solid Sweater </td>
                  <td> 2 </td>
                  <td><i class="fa fa-inr"></i> 3617</td>
                  <td> Jabong </td>
                  <td><span class="text-orange">Pending</span></td>
                </tr>
                <tr class="seller-amount">
                  <td colspan="7">
                    <div class="seller-tax">
                      <span class="text-label">Fashionara Shipping Charges:</span>
                      <span class="cart-price">  <span class="fa fa-inr"></span> 49</span></div>
                      <div class="seller-subtotal"><span class="text-label">Fashionara Sub Total:</span> <span class="cart-price"><span class="fa fa-inr"></span> 311</span></div>

                  </td>
                </tr>
              </tbody>
              </table>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="address-box">
                  <div class="title">
                      Shipping Address:
                  </div>
                  <div class="address">
                    Stylecracker <br>
                    Saurabh, 1 Floor,<br>
                    Ghatkopar(w), Mumbai - 400078

                  </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="order-amount">
                    <div class="order-total">Order Total : <span class="cart-price"><span class="fa fa-inr"></span> 3860</span></div>
                  </div>
                </div>
              </div>


                </div>



              </div>

            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-md-12"> <a data-toggle="collapse" href="#order2" class="btn btn-primary"><span class="font-bold text-capitalize">Order Id:</span> #65767</a></div>
              <!--    <div class="col-md-3"><span class="font-bold">Date:</span> 30|10|2015</div>
                  <div class="col-md-3"><span class="font-bold">Amount:</span> <i class="fa fa-inr"></i> 3617</div>
                  <div class="col-md-3"><span class="font-bold">Status:</span> <span class="text-orange">Pending</span></div>-->
              </div>
              </div>
                </div>
              <div id="order2" class="panel-collapse collapse">
                <div class="panel-body">
                  <div class="order-details-inner">
                <div class="table-responsive">
                <table class="table table-striped">
                  <tbody>
                    <tr>
                    <th>Sr. No.</th>
                    <th>Image</th>
                    <th>Product Name</th>
                    <th>Qty</th>
                    <th>Price</th>
                    <th>Seller</th>
                    <th>Status</th>
                  </tr>
                  <tr>
                    <td>1</td>
                    <td><img class="item-img" src="assets/images/products/p1.jpg"></td>
                    <td> RenkaGrey Solid Sweater </td>
                    <td> 2 </td>
                    <td><i class="fa fa-inr"></i> 3617</td>
                    <td> Jabong </td>
                    <td><span class="text-orange">Transit</span></td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td><img class="item-img" src="assets/images/products/p1.jpg"></td>
                    <td>RG RenkaGrey Solid Sweater </td>
                    <td> 2 </td>
                    <td><i class="fa fa-inr"></i> 3617</td>
                    <td> Jabong </td>
                    <td><span class="text-orange">Pending</span></td>
                  </tr>
                  <tr class="seller-amount">
                    <td colspan="7">
                      <div class="seller-tax">
                        <span class="text-label">Fashionara Shipping Charges:</span>
                        <span class="cart-price">  <span class="fa fa-inr"></span> 49</span></div>
                        <div class="seller-subtotal"><span class="text-label">Fashionara Sub Total:</span> <span class="cart-price"><span class="fa fa-inr"></span> 311</span></div>

                    </td>
                  </tr>
                </tbody>
                </table>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="address-box">
                    <div class="title">
                        Shipping Address:
                    </div>
                    <div class="address">
                      Stylecracker <br>
                      Saurabh, 1 Floor,<br>
                      Ghatkopar(w), Mumbai - 400078

                    </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="order-amount">
                      <div class="order-total">Order Total : <span class="cart-price"><span class="fa fa-inr"></span> 3860</span></div>
                    </div>
                  </div>
                </div>


                  </div>



                </div>

              </div>


          </div>
        </div>


     </div>

<br><br>


<div class="page-header">
  <div class="container">
    <h1>Order History</h1>
  </div>
</div>
<div class="content-wrp">
  <div class="container">
    <div class="content-inner">
    <div class="table-responsive">
  <table class="table table-striped">
    	<tbody><tr>
    		<th>Order Id</th>
    		<th>Date</th>
    		<th>Amount</th>
        <th>Status</th>
    	</tr>
    	<tr>
    		<td><a href="#">R00565648</a></td>
    		<td>30|10|2015</td>
        <td><i class="fa fa-inr"></i> 3617</td>
        <td><span class="text-orange">Pending</span></td>
    	</tr>
      <tr>
        <td><a href="#">D8678678776</a></td>
        <td>30|10|2015</td>
        <td><i class="fa fa-inr"></i> 3617</td>
        <td><span class="text-green">Delivered</span></td>
      </tr>
      <tr>
        <td><a href="#">W657657657</a></td>
        <td>30|10|2015</td>
        <td><i class="fa fa-inr"></i> 3617</td>
        <td><span class="text-red">Canceled</span></td>
      </tr>

    </tbody></table>
  </div>
    </div>

<br>
<div class="page-header">
  <div class="container">
    <h1>Order Details</h1>
  </div>
</div>
  <div class="content-inner order-details">
<div class="table-responsive">
<table class="table table-striped">
  <tbody>
    <tr>
    <th>Sr. No.</th>
    <th>Image</th>
    <th>Product Name</th>
    <th>Qty</th>
    <th>Price</th>
    <th>Seller</th>
    <th>Status</th>
  </tr>
  <tr>
    <td>1</td>
    <td><img class="item-img" src="assets/images/products/p1.jpg"></td>
    <td> RenkaGrey Solid Sweater </td>
    <td> 2 </td>
    <td><i class="fa fa-inr"></i> 3617</td>
    <td> Jabong </td>
    <td><span class="text-orange">Transit</span></td>
  </tr>
  <tr>
    <td>2</td>
    <td><img class="item-img" src="assets/images/products/p1.jpg"></td>
    <td>RG RenkaGrey Solid Sweater </td>
    <td> 2 </td>
    <td><i class="fa fa-inr"></i> 3617</td>
    <td> Jabong </td>
    <td><span class="text-orange">Pending</span></td>
  </tr>
  <tr class="seller-amount">
    <td colspan="7">
      <div class="seller-tax">
        <span class="text-label">Fashionara Shipping Charges:</span>
        <span class="cart-price">  <span class="fa fa-inr"></span> 49</span></div>
        <div class="seller-subtotal"><span class="text-label">Fashionara Sub Total:</span> <span class="cart-price"><span class="fa fa-inr"></span> 311</span></div>

    </td>
  </tr>
</tbody>
</table>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="address-box">
    <div class="title">
        Shipping Address:
    </div>
    <div class="address">
      Stylecracker <br>
      Saurabh, 1 Floor,<br>
      Ghatkopar(w), Mumbai - 400078

    </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="order-amount">
      <div class="order-total">Order Total : <span class="cart-price"><span class="fa fa-inr"></span> 3860</span></div>
    </div>
  </div>
</div>


  </div>
  <a href="#mycart" data-toggle="modal">Mycart</a>
  <br>
  <a href="#order-summary" data-toggle="modal">OrderSummary</a>
    </div>
</div>
</div>


<!-- order success -->

<div class="modal fade mycart-popup order-summary-popup" id="order-summary" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header modal-header-success">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Order Successfull</h4>
      </div>

      <div class="modal-body mycart-body">
        <div class="order-summary-info">
          Your order <strong>R000778248</strong> has been successfully placed. Please check your email for order detail.
        </div>
        <div class="cart-items-wrp">
          <ul class="cart-items">
            <li>
              <div class="row">
                <div class="col-md-2">
                  <img class="item-img" src="assets/images/products/p1.jpg">
                </div>
                <div class="col-md-10">
                  <div class="cart-item-inner">
                    <div class="row">
                      <div class="col-md-8">
                        <a class="cart-item-name" target="_blank" href="#">RenkaGrey Solid Sweater</a>
                        <div class="cart-item-meta">
                          <span class="size-wrp">
                            <span class="text-label">Size:</span> L
                          </span>
                          <span class="qty-wrp">
                            <span class="text-label">Qty:</span>
                            2
                          </span>
                        </div>
                        <div>
                        </div>
                      </div>
                      <div class="col-md-4 cart-item-right">
                        <span class="cart-item-price cart-price">
                          <span class="fa fa-inr"></span> 699
                        </span>

                      </div>
                      <div class="col-md-12">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="cart-item-seller">
                              <span class="text-label">Seller:</span> <a target="_blank" href="/shopping/jabong"> Jabong </a></div>
                            </div>
                            <div class="col-md-6 cart-item-right">
                              <div class="delivery-status">
                                Delivery in 6-8 working days.
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>

                </div>
              </li>
              <li>
                <div class="row">
                  <div class="col-md-2">
                    <img class="item-img" src="assets/images/products/p1.jpg">
                  </div>
                  <div class="col-md-10">
                    <div class="cart-item-inner">
                      <div class="row">
                        <div class="col-md-8">
                          <a class="cart-item-name" target="_blank" href="#">RenkaGrey Solid Sweater</a>
                          <div class="cart-item-meta">
                            <span class="size-wrp">
                              <span class="text-label">Size:</span> L
                            </span>
                            <span class="qty-wrp">
                              <span class="text-label">Qty:</span>
                              3
                            </span>
                          </div>
                          <div>
                          </div>
                        </div>
                        <div class="col-md-4 cart-item-right">
                          <span class="cart-item-price cart-price">
                            <span class="fa fa-inr"></span> 699
                          </span>

                        </div>
                        <div class="col-md-12">
                          <div class="row">
                            <div class="col-md-6">
                              <div class="cart-item-seller">
                                <span class="text-label">Seller:</span> <a target="_blank" href="/shopping/jabong"> Jabong </a></div>
                              </div>
                              <div class="col-md-6 cart-item-right">
                                <div class="delivery-status">
                                  Delivery in 6-8 working days.
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>

                  </div>
                </li>
                <li class="seller-amount">
                  <div class="seller-tax"><span class="text-label">Fashionara Shipping Charges:</span>
                    <span class="cart-price">  <span class="fa fa-inr"></span> 49</span></div>
                    <div class="seller-subtotal"><span class="text-label">Fashionara Sub Total:</span> <span class="cart-price"><span class="fa fa-inr"></span> 311</span></div>
                  </li>
                  <li>
                    <div class="row">
                      <div class="col-md-2">
                        <img class="item-img" src="assets/images/products/p1.jpg">
                      </div>
                      <div class="col-md-10">
                        <div class="cart-item-inner">
                          <div class="row">
                            <div class="col-md-8">
                              <a class="cart-item-name" target="_blank" href="#">RenkaGrey Solid Sweater</a>
                              <div class="cart-item-meta">
                                <span class="size-wrp">
                                  <span class="text-label">Size:</span> L
                                </span>
                                <span class="qty-wrp">
                                  <span class="text-label">Qty:</span>
                                  5
                                </span>
                              </div>
                              <div>
                              </div>
                            </div>
                            <div class="col-md-4 cart-item-right">
                              <span class="cart-item-price cart-price">
                                <span class="fa fa-inr"></span> 699
                              </span>

                            </div>
                            <div class="col-md-12">
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="cart-item-seller">
                                    <span class="text-label">Seller:</span> <a target="_blank" href="/shopping/jabong"> Jabong </a></div>
                                  </div>
                                  <div class="col-md-6 cart-item-right">
                                    <div class="delivery-status">
                                      Delivery in 6-8 working days.
                                    </div>
                                  </div>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>

                      </div>
                    </li>
                    <li>
                      <div class="row">
                        <div class="col-md-2">
                          <img class="item-img" src="assets/images/products/p1.jpg">
                        </div>
                        <div class="col-md-10">
                          <div class="cart-item-inner">
                            <div class="row">
                              <div class="col-md-8">
                                <a class="cart-item-name" target="_blank" href="#">RenkaGrey Solid Sweater</a>
                                <div class="cart-item-meta">
                                  <span class="size-wrp">
                                    <span class="text-label">Size:</span> L
                                  </span>
                                  <span class="qty-wrp">
                                    <span class="text-label">Qty:</span>
                                    2
                                  </span>
                                </div>
                                <div>
                                </div>
                              </div>
                              <div class="col-md-4 cart-item-right">
                                <span class="cart-item-price cart-price">
                                  <span class="fa fa-inr"></span> 699
                                </span>

                              </div>
                              <div class="col-md-12">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="cart-item-seller">
                                      <span class="text-label">Seller:</span> <a target="_blank" href="/shopping/jabong"> Jabong </a></div>
                                    </div>
                                    <div class="col-md-6 cart-item-right">
                                      <div class="delivery-status">
                                        Delivery in 6-8 working days.
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>
                            </div>
                          </div>

                        </div>
                      </li>
                      <li>
                        <div class="row">
                          <div class="col-md-2">
                            <img class="item-img" src="assets/images/products/p1.jpg">
                          </div>
                          <div class="col-md-10">
                            <div class="cart-item-inner">
                              <div class="row">
                                <div class="col-md-8">
                                  <a class="cart-item-name" target="_blank" href="#">RenkaGrey Solid Sweater</a>
                                  <div class="cart-item-meta">
                                    <span class="size-wrp">
                                      <span class="text-label">Size:</span> L
                                    </span>
                                    <span class="qty-wrp">
                                      <span class="text-label">Qty:</span>
                                      1
                                    </span>
                                  </div>
                                  <div>
                                  </div>
                                </div>
                                <div class="col-md-4 cart-item-right">
                                  <span class="cart-item-price cart-price">
                                    <span class="fa fa-inr"></span> 699
                                  </span>

                                </div>
                                <div class="col-md-12">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="cart-item-seller">
                                        <span class="text-label">Seller:</span> <a target="_blank" href="/shopping/jabong"> Jabong </a></div>
                                      </div>
                                      <div class="col-md-6 cart-item-right">
                                        <div class="delivery-status">
                                          Delivery in 6-8 working days.
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                </div>
                                <div class="cart-message">  Hurry up!! 2 Items in a stock.
                                </div>
                              </div>
                            </div>

                          </div>
                        </li>
                        <li class="seller-amount">
                          <div class="seller-tax"><span class="text-label">Fashionara Shipping Charges:</span>
                            <span class="cart-price"><span class="fa fa-inr"></span> 49</span></div>
                            <div class="seller-subtotal"><span class="text-label">Fashionara Sub Total:</span>
                              <span class="cart-price"><span class="fa fa-inr"></span> 311</span></div>
                            </li>
                          </ul>
                        </div>
                        <div class="order-amount">
                          <div class="order-total">Order Total : <span class="cart-price"><span class="fa fa-inr"></span> 3860</span></div>
                        </div>

                      </div>

                    </div>
                  </div>
                </div>


                <!-- my cart popup -->
                <div class="modal fade mycart-popup" id="mycart" tabindex="-1" role="dialog">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Shopping Cart</h4>
                      </div>

                      <div class="modal-body mycart-body">
                        <div>
                          <div class="row custom">
                            <div class="col-md-6 custom">
                              <h4 class="mycart-title">Review your Order</h4>
                              <div class="cart-items-wrp">
                                <ul class="cart-items">
                                  <li>
                                    <div class="row">
                                      <div class="col-md-2">
                                        <img class="item-img" src="assets/images/products/p1.jpg">
                                      </div>
                                      <div class="col-md-10">
                                        <div class="cart-item-inner">
                                          <div class="row">
                                            <div class="col-md-8">
                                              <a class="cart-item-name" target="_blank" href="#">RenkaGrey Solid Sweater</a>
                                              <div class="cart-item-meta">
                                                <span class="size-wrp">
                                                  <span class="text-label">Size:</span> L
                                                </span>
                                                <span class="qty-wrp">
                                                  <span class="text-label">Qty:</span>
                                                  <input type="text" class="form-control input-sm input-qty" />
                                                </span>
                                              </div>
                                              <div>
                                              </div>
                                            </div>
                                            <div class="col-md-4 cart-item-right">
                                              <span class="cart-item-price cart-price">
                                                <span class="fa fa-inr"></span> 699
                                              </span>

                                              <span class="cart-delete"><i class="fa fa-times-circle-o"></i> </span>

                                            </div>
                                            <div class="col-md-12">
                                              <div class="row">
                                                <div class="col-md-6">
                                                  <div class="cart-item-seller">
                                                    <span class="text-label">Seller:</span> <a target="_blank" href="/shopping/jabong"> Jabong </a></div>
                                                  </div>
                                                  <div class="col-md-6 cart-item-right">
                                                    <div class="delivery-status">
                                                      Delivery in 6-8 working days.
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>

                                            </div>
                                          </div>
                                        </div>

                                      </div>
                                    </li>
                                    <li>
                                      <div class="row">
                                        <div class="col-md-2">
                                          <img class="item-img" src="assets/images/products/p1.jpg">
                                        </div>
                                        <div class="col-md-10">
                                          <div class="cart-item-inner">
                                            <div class="row">
                                              <div class="col-md-8">
                                                <a class="cart-item-name" target="_blank" href="#">RenkaGrey Solid Sweater</a>
                                                <div class="cart-item-meta">
                                                  <span class="size-wrp">
                                                    <span class="text-label">Size:</span> L
                                                  </span>
                                                  <span class="qty-wrp">
                                                    <span class="text-label">Qty:</span>
                                                    <input type="text" class="form-control input-sm input-qty" />
                                                  </span>
                                                </div>
                                                <div>
                                                </div>
                                              </div>
                                              <div class="col-md-4 cart-item-right">
                                                <span class="cart-item-price cart-price">
                                                  <span class="fa fa-inr"></span> 699
                                                </span>

                                                <span class="cart-delete"><i class="fa fa-times-circle-o"></i> </span>

                                              </div>
                                              <div class="col-md-12">
                                                <div class="row">
                                                  <div class="col-md-6">
                                                    <div class="cart-item-seller">
                                                      <span class="text-label">Seller:</span> <a target="_blank" href="/shopping/jabong"> Jabong </a></div>
                                                    </div>
                                                    <div class="col-md-6 cart-item-right">
                                                      <div class="delivery-status">
                                                        Delivery in 6-8 working days.
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                              </div>
                                            </div>
                                          </div>

                                        </div>
                                      </li>
                                      <li class="seller-amount">
                                        <div class="seller-tax"><span class="text-label">Fashionara Shipping Charges:</span>
                                          <span class="cart-price">  <span class="fa fa-inr"></span> 49</span></div>
                                          <div class="seller-subtotal"><span class="text-label">Fashionara Sub Total:</span> <span class="cart-price"><span class="fa fa-inr"></span> 311</span></div>
                                        </li>
                                        <li>
                                          <div class="row">
                                            <div class="col-md-2">
                                              <img class="item-img" src="assets/images/products/p1.jpg">
                                            </div>
                                            <div class="col-md-10">
                                              <div class="cart-item-inner">
                                                <div class="row">
                                                  <div class="col-md-8">
                                                    <a class="cart-item-name" target="_blank" href="#">RenkaGrey Solid Sweater</a>
                                                    <div class="cart-item-meta">
                                                      <span class="size-wrp">
                                                        <span class="text-label">Size:</span> L
                                                      </span>
                                                      <span class="qty-wrp">
                                                        <span class="text-label">Qty:</span>
                                                        <input type="text" class="form-control input-sm input-qty" />
                                                      </span>
                                                    </div>
                                                    <div>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-4 cart-item-right">
                                                    <span class="cart-item-price cart-price">
                                                      <span class="fa fa-inr"></span> 699
                                                    </span>

                                                    <span class="cart-delete"><i class="fa fa-times-circle-o"></i> </span>

                                                  </div>
                                                  <div class="col-md-12">
                                                    <div class="row">
                                                      <div class="col-md-6">
                                                        <div class="cart-item-seller">
                                                          <span class="text-label">Seller:</span> <a target="_blank" href="/shopping/jabong"> Jabong </a></div>
                                                        </div>
                                                        <div class="col-md-6 cart-item-right">
                                                          <div class="delivery-status">
                                                            Delivery in 6-8 working days.
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>

                                                  </div>
                                                </div>
                                              </div>

                                            </div>
                                          </li>
                                          <li>
                                            <div class="row">
                                              <div class="col-md-2">
                                                <img class="item-img" src="assets/images/products/p1.jpg">
                                              </div>
                                              <div class="col-md-10">
                                                <div class="cart-item-inner">
                                                  <div class="row">
                                                    <div class="col-md-8">
                                                      <a class="cart-item-name" target="_blank" href="#">RenkaGrey Solid Sweater</a>
                                                      <div class="cart-item-meta">
                                                        <span class="size-wrp">
                                                          <span class="text-label">Size:</span> L
                                                        </span>
                                                        <span class="qty-wrp">
                                                          <span class="text-label">Qty:</span>
                                                          <input type="text" class="form-control input-sm input-qty" />
                                                        </span>
                                                      </div>
                                                      <div>
                                                      </div>
                                                    </div>
                                                    <div class="col-md-4 cart-item-right">
                                                      <span class="cart-item-price cart-price">
                                                        <span class="fa fa-inr"></span> 699
                                                      </span>

                                                      <span class="cart-delete"><i class="fa fa-times-circle-o"></i> </span>

                                                    </div>
                                                    <div class="col-md-12">
                                                      <div class="row">
                                                        <div class="col-md-6">
                                                          <div class="cart-item-seller">
                                                            <span class="text-label">Seller:</span> <a target="_blank" href="/shopping/jabong"> Jabong </a></div>
                                                          </div>
                                                          <div class="col-md-6 cart-item-right">
                                                            <div class="delivery-status">
                                                              Delivery in 6-8 working days.
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>

                                                    </div>
                                                  </div>
                                                </div>

                                              </div>
                                            </li>
                                            <li>
                                              <div class="row">
                                                <div class="col-md-2">
                                                  <img class="item-img" src="assets/images/products/p1.jpg">
                                                </div>
                                                <div class="col-md-10">
                                                  <div class="cart-item-inner">
                                                    <div class="row">
                                                      <div class="col-md-8">
                                                        <a class="cart-item-name" target="_blank" href="#">RenkaGrey Solid Sweater</a>
                                                        <div class="cart-item-meta">
                                                          <span class="size-wrp">
                                                            <span class="text-label">Size:</span> L
                                                          </span>
                                                          <span class="qty-wrp">
                                                            <span class="text-label">Qty:</span>
                                                            <input type="text" class="form-control input-sm input-qty" />
                                                          </span>
                                                        </div>
                                                        <div>
                                                        </div>
                                                      </div>
                                                      <div class="col-md-4 cart-item-right">
                                                        <span class="cart-item-price cart-price">
                                                          <span class="fa fa-inr"></span> 699
                                                        </span>

                                                        <span class="cart-delete"><i class="fa fa-times-circle-o"></i> </span>

                                                      </div>
                                                      <div class="col-md-12">
                                                        <div class="row">
                                                          <div class="col-md-6">
                                                            <div class="cart-item-seller">
                                                              <span class="text-label">Seller:</span> <a target="_blank" href="/shopping/jabong"> Jabong </a></div>
                                                            </div>
                                                            <div class="col-md-6 cart-item-right">
                                                              <div class="delivery-status">
                                                                Delivery in 6-8 working days.
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>

                                                      </div>
                                                      <div class="cart-message">  Hurry up!! 2 Items in a stock.
                                                      </div>
                                                    </div>
                                                  </div>

                                                </div>
                                              </li>
                                              <li class="seller-amount">
                                                <div class="seller-tax"><span class="text-label">Fashionara Shipping Charges:</span>
                                                  <span class="cart-price"><span class="fa fa-inr"></span> 49</span></div>
                                                  <div class="seller-subtotal"><span class="text-label">Fashionara Sub Total:</span>
                                                    <span class="cart-price"><span class="fa fa-inr"></span> 311</span></div>
                                                  </li>
                                                </ul>
                                              </div>
                                              <div class="order-amount">
                                                <div class="order-total">Order Total : <span class="cart-price"><span class="fa fa-inr"></span> 3860</span></div>
                                              </div>

                                            </div>

                                            <div class="col-md-6 custom">
                                              <div class="adress-box">
                                                <h4 class="mycart-title">Shipping and Billing </h4>

                                                <div class="row">
                                                  <div class="col-md-6">
                                                    <div class="form-group">
                                                      <input type="text" placeholder="First Name" value="" class="form-control input-sm">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                    <div class="form-group">
                                                      <input type="text" placeholder="Last Name" value="" class="form-control input-sm">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                    <div class="form-group">
                                                      <input type="text" placeholder="Email" value="" class="form-control input-sm">
                                                    </div>
                                                  </div>


                                                  <div class="col-md-6">
                                                    <div class="form-group">
                                                      <input type="text" placeholder="Mobile" value="" class="form-control input-sm">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-12">
                                                    <div class="form-group">
                                                      <textarea placeholder="Address" class="form-control input-sm"></textarea>
                                                    </div>

                                                  </div>
                                                  <div class="col-md-4">
                                                    <div class="form-group">
                                                      <input type="text" placeholder="PIN" value="" class="form-control input-sm">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-4">
                                                    <div class="form-group">
                                                      <input type="text" placeholder="City" value="" class="form-control input-sm">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-4">
                                                    <div class="form-group">
                                                      <select class="form-control input-sm">
                                                        <option>
                                                          Maharashtra
                                                        </option>
                                                        <option>
                                                          Delhi
                                                        </option>
                                                        <option>
                                                          MP
                                                        </option>
                                                      </select>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="payment-options-box">
                                                <h4 class="mycart-title">Payment Options</h4>
                                                <div>
                                                  <ul class="nav nav-tabs">
                                                    <li class="active"><a data-toggle="tab" href="#payment-cod">COD</a></li>
                                                    <li><a data-toggle="tab" href="#payment-online">Credit, Debit Card / Netbanking
                                                    </a></li>
                                                  </ul>

                                                  <div class="tab-content">
                                                    <div id="payment-cod" class="tab-pane fade in active">
                                                      <p>Please make sure the details entered are correct and place the order. You can make payment by Cash when the item is delivered to you. </p>
                                                    </div>
                                                    <div id="payment-online" class="tab-pane fade">
                                                      <p>You will be redirected to our Payment Gateway provider to make the payment. Make sure the details entered are correct before placing the order. </p>
                                                    </div>
                                                  </div>

                                                </div>
                                              </div>
                                            </div>
                                          </div>

                                        </div>

                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary">Place Order</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
<style>
/*order details*/
.font-bold{font-weight: bold;}
.order-details .panel-group .panel{border-radius: 0; margin-bottom: 3px;}
.order-details .panel-body{padding:20px 15px; border:1px solid #ddd; margin-bottom: 10px;}
.order-details .panel-group{margin-bottom: 0;}
.order-details .panel-default > .panel-heading{background: #f2f2f2;}

</style>
                                <script src="assets/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
                                <script>
                                $('.cart-items-wrp').slimScroll({
                                  height: '340px',
                                  size : '4px',
                                  alwaysVisible: true
                                });
                                </script>
