<div class="page-design-1 page-orders">
  <div class="page-header">
    <div class="container">
      <h1>Payment Unsuccessful</h1>
    </div>
  </div>

  <div class="content-wrp">
    <div class="container">
      <div class="content-inner order-unsuccessful">
        <div class="order-error-info">
          <div class="title"><i class="fa fa-exclamation-circle"></i> We are sorry</div>
        <p>
          An error has been occurred during transaction. Please contact our support team.
        </p>
        </div>
      </div>
    </div>
  </div>
