<div class="page-header brand-header media-header">
  <div class="container">
    <h1>Media</h1>
  </div>
</div>
<div class="content-wrp page-media">
  <div class="container">
    <div class="media-content">
        <div class="row grid-items-wrp masonry">
          <div class="grid-sizer"></div>
          <div class="col-xs-12 col-md-4 col-lg-4 grid-item">
            <div class="media-video-outer">
            <div class="media-video-logo">
              <a href="https://youtu.be/yyamOd7eaM0" target="_blank"><img src="assets/images/media/bbtv-video.jpg"></a>
            </div>
            <div class="media-video-inner">
              <p>Through Rising Stars, hosted by Abha Bakaya, Bloomberg TV India spotlights gen-next innovators who are bringing about disruptive development in their respective fields. Bloomberg TV India is renowned for reporting global business and financial on-goings by covering top companies and industries.</p>

              <p>Catch Dhimaan Shah, Co- Founder and Managing Director of StyleCracker, as he talks about what sets our platform apart, the projects underway and future plans to look forward to.</p>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-md-4 col-lg-4 grid-item">
          <div class="media-video-outer">
          <div class="media-video-logo">
          <a href="https://youtu.be/aITNSU2FDNs" target="_blank"> <img src="assets/images/media/yt-cnbc-video.jpg"></a>
          </div>
          <div class="media-video-inner">
            <p>Young Turks is one of the oldest shows on CNBC TV 18. It focuses on entrepreneurial ventures that hold promise. Launched in 1999, CNBC TV 18 is the foremost channel of its kind  and thus a pioneer in the field of business news.</p>
            <p>Watch Archana Walavalkar and Dhimaan Shah, the co-founders of StyleCracker, as they talk about what makes their business model unique. There is much to look forward to as the duo also elaborates on their vision for the future, throwing spotlight on their plans to expand the portal's functions to allow users to shop from, rate and review its partner brands.</p>
          </div>
        </div>
      </div>

    <div class="col-xs-12 col-md-4 col-lg-4 grid-item">
      <div class="media-outer">

        <div class="media-logo">
          <img src="assets/images/media/logo-economic-times.png">
        </div>
        <div class="media-inner">

          <p>Mumbai-based StyleCracker, started by investment banker Dhimaan Shah and former vogue stylist Archana Walavalkar in May 2013, similarly offers a personalized styling platform where people can get their fashion related queries answered with advice tailored for them.</p>
          <p>"We decided that if we combine styling with intelligent technology, we would have a hugely scalable business model," cofounder Shah said.</p>
          <p>With an undisclosed angel investment of about Rs 1 crore, the company expects to earn Rs 12 crore in two years.</p>
          <div class="media-read-more">	<a target="_blank" href="http://articles.economictimes.indiatimes.com/2014-05-31/news/50229127_1_fashion-portals-ashish-jhalani-myntra">Read more</a></div>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-md-4 col-lg-4 grid-item">
      <div class="media-outer">

        <div class="media-logo">
          <img src="assets/images/media/logo-toi.png">
        </div>
        <div class="media-inner">

          <p>Style Cracker's creative team includes former Vogue staffer Archana Walavalkar and Paranjape, stylist for actor Imran Khan.</p>
          <p>
            There's a big demand from corporate wives, eager to build a power couple image. "We have a lot of members from Tier 2 cities," says Paranjape. While most of these services target women Paranjape says some men have opted for their wardrobe revamp service.</p>

            <div class="media-read-more"><a target="_blank" href="http://timesofindia.indiatimes.com/home/sunday-times/Stylist-Not-just-for-the-A-listers/articleshow/25914534.cms?">Read more</a></div>
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-md-4 col-lg-4 grid-item">
        <div class="media-outer">
          <div class="media-logo">
            <img src="assets/images/media/logo-indian-express.png">
          </div>
          <div class="media-inner">
            <p>Dhimaan Shah, who has founded the personal styling portal StyleCracker with fashion industry insider Archana Walavalkar, says, "The tricky part is picking the right look for you &mdash; that's where we come in. At StyleCracker, we tell you what to buy and how to wear it, taking into account factors such as your personality, body type, aspirations, social life, geographical limitations and budget."</p>

            <div class="media-read-more"><a target="_blank" href="http://archive.indianexpress.com/news/it-s-personal/1129558/0">Read more</a></div>
          </div>
        </div>
      </div>


      <div class="col-xs-12 col-md-4 col-lg-4 grid-item">
        <div class="media-outer">
          <div class="media-logo">
            <img src="assets/images/media/logo-bebeautiful.png">
          </div>
          <div class="media-inner">
            <p>Just like some classic brands you must own, there are events you must attend. Dressed to the nines, feeling stylish and with ample doses of excitement, we were oh-so-kicked about one such event, the StyleCracker Borough, which was held at the Palladium Hotel recently.</p>
            <p>Organised and hosted by the bevy of top notch stylists that run StyleCracker, the exhibition had 60 participating brands, including Payal Singhal, Koecsh, Le Mill, Bombay Shirt Company, Akaliko, Sole Fry and dozens more.&nbsp;</p>

            <div class="media-read-more"><a target="_blank" href="http://www.bebeautiful.in/fashion/trends/the-stylecracker-borough-enlivens-mumbai">Read more</a></div>
          </div>

        </div>
      </div>

      <div class="col-xs-12 col-md-4 col-lg-4 grid-item">
        <div class="media-outer">
          <div class="media-logo">
            <img src="assets/images/media/logo-dna.png">
          </div>
          <div class="media-inner">
            <p>Celebrity stylist, co-founder&nbsp;stylecracker.com&nbsp;gives you tips on how to dress right.
              The scorching days of&nbsp;summer&nbsp;are here. Time to revamp your wardrobe &mdash; discard those unwanted items and stock some essentials.&nbsp;</p>

              <div class="media-read-more"><a target="_blank" href="http://www.dnaindia.com/lifestyle/column-what-not-to-wear-this-summer-1829301">Read more</a></div>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-md-4 col-lg-4 grid-item">
          <div class="media-outer">

            <div class="media-logo">
              <img src="assets/images/media/logo-afternoon.png">
            </div>
            <div class="media-inner">
              <p>
                Shopaholics in the city shouldn’t miss out on StyleCracker Borough, India's first personalized styling platform, organized in association with Palladium Hotel. The event will be a day of fashion, food and fun. You can look forward to collections by 60 designers, all curated by StyleCracker. It will also include brands such as Koëcsh, Payal Singhal, Akaliko, LeMill and Ruchika Sachdeva.</p>
                <p>StyleCracker offers end-to-end styling services to anyone who needs it. From editorials and films to commercials, celebrity styling and personal styling…</p>

                <div class="media-read-more"><a target="_blank" href="http://www.afternoondc.in/48-hrs/event-the-stylecracker-borough-at-palladium/article_106790">Read more</a></div>
              </div>
            </div>
          </div>

          <div class="col-xs-12 col-md-4 col-lg-4 grid-item">
            <div class="media-outer">
              <div class="media-logo">
                <img src="assets/images/media/logo-spicy-sangria.png">
              </div>
              <div class="media-inner">
                <p>
                  Recently, we caught up with Archana Walawalkar and Dhimaan Shah who have come together to start Style Cracker a personal styling website! This weekend they have a Style Cracker Borough read on to know the details and to shop more!
                </p><p>
                  StyleCracker firmly believes that each person has an individuality which ought to be expressed through their personal style.
                </p>



                <div class="media-read-more"><a target="_blank" href="http://www.spicysangria.com/fashion-forward/catching-up-with-the-style-cracker-team.html">Read more</a></div>
              </div>
            </div>
          </div>

          <div class="col-xs-12 col-md-4 col-lg-4 grid-item">
            <div class="media-outer">
              <div class="media-logo">
                <img src="assets/images/media/logo-inonit.png">
              </div>
              <div class="media-inner">
                <p>We got celebrity stylist Archana Walavalkar to speak to us and give some inside styling secrets. Previously the fashion editor at Vogue, she is the brain child behind Style Cracker, the online styling website. So ladies, brace yourselves for some cool and fun wardrobe help.</p>

                <div class="media-read-more"><a target="_blank" href="http://www.inonit.in/wear-cap-tank-top-superb-selfies-stylists-secrects-archana-walavalkar">Read more</a></div>
              </div>
            </div>
          </div>

          <div class="col-xs-12 col-md-4 col-lg-4 grid-item">
            <div class="media-outer">
              <div class="media-logo">
                <img src="assets/images/media/logo-fashionalized.png">
              </div>
              <div class="media-inner">
                <p>With over 35 designers including Arpita Mehta, Bansri Mehta, Daniel Syeim, Harshita Deshpande, JADE by Monica &amp; Karishma, Nishka Lulla, Shift by Nimish Shah, Valliyan&nbsp;by Nitya and many more showcasing their ready-to-wear (and might we add cheap and cheerful) collections, the StyleCracker Borough is an out-an-out shopper's fest!</p>
                <p>Archana Walavalkar from StyleCracker to give us a sneak peek and share her favourite items on sale at the Borough and here’s what she picked...</p>

                <div class="media-read-more"><a target="_blank" href="http://www.fashionalized.com/2013/10/the-stylecracker-borough.html">Read more</a></div>
              </div>

            </div>
          </div>

          <div class="col-xs-12 col-md-4 col-lg-4 grid-item">
            <div class="media-outer">
              <div class="media-logo">
                <img src="assets/images/media/logo-a-shopaholic.png">
              </div>
              <div class="media-inner">
                <p>Lakmé Absolute Salon along with Style Cracker presents the much awaited exclusive pop-up&nbsp;‘The Style Cracker Borough’&nbsp;&ndash; 14 hours of shopping, styling and makeover, live music and much more. With over 60 designers showcasing their latest collections, a complimentary makeover by Lakmé Absolute Salon and advice from styling experts; this borough is definitely a must visit this summer!</p>
                <p>Stand a chance to meet the popular designers:&nbsp;Arpita Mehta, Harshita Deshpande Chatterjee, Nishika Lulla&nbsp;and many more to try out their unique creations.</p>

                <div class="media-read-more"><a target="_blank" href="http://www.a-shopaholic.com/2014/04/lakme-absolute-salon-presents-style.html">Read more</a></div>
              </div>
            </div>
          </div>


          <div class="col-xs-12 col-md-4 col-lg-4 grid-item">
            <div class="media-outer">
              <div class="media-logo">
                <img src="assets/images/media/logo-fibre2fashion.png">
              </div>
              <div class="media-inner">

                <p>Stylecracker Hood, a live stylist unit comprising young talent that puts together a platform of style covering, everything from a wardrobe full of labels, hair and makeup, brought out the final designer look.
                </p><p>
                  The garments were sourced personally by Stylecracker Hood from several designers and high fashion stores.</p>

                  <div class="media-read-more"><a target="_blank" href="http://www.fibre2fashion.com/news/fashion-news/newsdetails.aspx?news_id=161027">Read more</a></div>
                </div>

              </div>
            </div>

            <div class="col-xs-12 col-md-4 col-lg-4 grid-item">
              <div class="media-outer">
                <div class="media-logo">
                  <img src="assets/images/media/logo-love-and-other-bugs.png">
                </div>
                <div class="media-inner">
                  <p>Our amazing friends at&nbsp;StyleCracker,&nbsp;Tia Paranjape and Dhimaan Shah&nbsp;showed us around the&nbsp;SC Hood; the season showcased an array of designers. This year was all about what city your personal style belonged to; from New York Glam, to Parisian Lady-Chic, Boho Goa Girl, to Quirky, Out-of-the-box Hong Kong Street Style, the SC Hood had a line up of designers and stylists waiting for you to ‘come get styled’ in three easy steps: Walk in. Pick a city. Get styled! </p>

                  <div class="media-read-more"><a target="_blank" href="http://www.loveandotherbugs.com/lookbook/schood/">Read more</a></div>
                </div>
              </div>
            </div>

            <div class="col-xs-12 col-md-4 col-lg-4 grid-item">
              <div class="media-outer">
                <div class="media-logo">
                  <img src="assets/images/media/logo-paperblog.png">
                </div>
                <div class="media-inner">
                  <p>Shop, get styled, groove to the dance tunes and binge into some mouth-watering treats&nbsp;@ The Style Cracker Borough by Lakmé Absolute Salon!</p>

                  <div class="media-read-more"><a target="_blank" href="http://en.paperblog.com/lakme-absolute-salon-presents-the-style-cracker-borough-mumbai-only-850563/">Read more</a></div>
                </div>
              </div>
            </div>



            <div class="col-xs-12 col-md-4 col-lg-4 grid-item">
              <div class="media-outer">
                <div class="media-logo">
                  <img src="assets/images/media/logo-eenadu.png">
                </div>
                <div class="media-inner">
                  <p>Eenadu is a Telugu daily newspaper ranked 6<sup>th</sup> in a nation-wide survey for dailies printed in regional languages, with a total readership of over 50 lakhs. It boosts of the largest circulation in Andhra Pradesh and Telangana.<br>In this exclusive interview, Archana Walavalkar talks about her StyleCracker journey- from the initial inspiration and challenges faced to the platform's present success as the country's only web platform to have blended high end personal styling with e-shopping.</p>

                  <!--	<div class="media-read-more"><a href="http://www.eenadu.net/vasundara/Vasundarainner.aspx?item=vasumain1&no=1" target="_blank">Read more &raquo;</a></div>	-->
                </div>
              </div>
            </div>


            <div class="col-xs-12 col-md-4 col-lg-4 grid-item">
              <div class="media-outer">
                <div class="media-logo">
                  <img src="assets/images/media/logo-economic-times.png">
                </div>
                <div style="height:auto" class="media-inner">
                  <p>StyleCracker, a Mumbai-based fashion styling platform that offers personalised fashion experience to users, has raised $1 million from four high net worth individuals in its first round of funding. The two-year-old venture will use the funding on tech development and for taking the platform to global markets. "In the last two years, we have seen 1,00,000 registered users on our platform, and are targeting to have two million users in the next 12 months," said Dhimaan Shah, co-founder and managing director of the company. Shah founded StyleCracker with celebrity stylist, Archana Walavalkar.</p>

                  <div class="media-read-more"><a target="_blank" href="http://economictimes.indiatimes.com/small-biz/startups/stylecracker-gets-rs-6-crore-in-funds-from-hnis/articleshow/48751226.cms">Read more</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script>
    /*$(document).ready(function() {
      var $container = $('.grid-items-wrp');
       $container.masonry({
           isInitLayout : true,
           itemSelector: '.grid-item',
           transitionDuration: 0,
           percentPosition: true
       });
       });*/
       </script>
