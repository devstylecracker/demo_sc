<?php 
  //echo '<pre>';print_r($profiledata);
//echo '<pre>';print_r($user_pa_data);
//echo '<pre>';print_r($user_pa_data->user_brand_list)
//echo 'profile--'.$user_pa_data->user_style_p1['image_name'];
/*if($_GET['test']==1)
{
  echo '<pre>';print_r($this->session->userdata());
}*/
?>
<style type="text/css">
  fieldset.date { 
  margin: 0; 
  padding-top: 10px;  
  display: block; 
  border: none; 
} 

fieldset.date legend { 
  margin: 0; 
  padding: 0; 
  margin-top: .25em; 
  font-size: 100%; 
} 


fieldset.date label { 
  position: absolute; 
  top: -20em; 
  left: -200em; 
} 

fieldset.date select { 
margin: 0; 
padding: 0; 
font-size: 100%; 
display: inline; 
width: 30%;
} 

.hwidth{
  width: 20%;

}
span.inst { 
  font-size: 75%; 
  color: #13D792; 
  padding-left: .25em; 
  text-transform: uppercase;
} 

.form-group-md{
  padding: 0 0 !important;
}
.form-group-md select{
  border-bottom: 1px solid #13D792 !important;
}
fieldset.date select:active, 
fieldset.date select:focus, 
fieldset.date select:hover 
{ 
  border-color: gray; 
  
} 

</style>
<div class="page-profile" attr="">
  <section class="section section-shadow">
    <div class="container">
      <div class="title-section">
        <h1 class="title">Profile Settings</h1>
        <h2 class="subtitle">Account Settings</h2>
      </div>
      <div class="row">
	  
        <div class="col-md-4 col-sm-4">
          <div class="img-wrp edit-photo-wrp">
            <img src="<?php echo $profiledata->profile_pic; ?>" />
            <span class="shape-diamond scbtn-diamond"><a href="<?php echo base_url().'pa/2'; ?>" alt="Edit ProfilePic"><i class="icon icon-edit"></i></a></span>
          </div>
        </div>
        <div class="col-md-8 col-sm-8"><?php //echo base_url().'Schome_new/register_user'; ?>
        <form name="sc_Signupedit" id="scSignupedit" method="post" action="<?php echo base_url().'profile'; ?>">
          <div class="box-edit-profile">
            <ul class="gender-wrp">
              <li id="gender-male" class="<?php echo $profiledata->gender=='men' ? 'active':''; ?>">
                <span class="ic-gender ic-male "><i class="fa fa-user"></i>  </span>
                <span class="text-label">M</span>
              </li>
              <li id="gender-female" class="<?php echo $profiledata->gender=='women' ? 'active':''; ?>">
                <span class="ic-gender ic-female "><i class="fa fa-user"></i></span>
                <span class="text-label">F</span>
              </li>
              <input type="hidden" id="sc_gender1" name="sc_gender1" value="<?php echo $profiledata->gender=='men' ? '2':'1'; ?>"  >
            </ul>
            <div class="form-group">
              <input type="text" name="sc_name1" id="sc_name1" class="form-control" placeholder="NAME" value="<?php echo $profiledata->name; ?>">
              <div class="message text-error hide"></div>
            </div>
            <div class="form-group-md">
			<?php
				$dob = explode('-',$profiledata->birth_date);
				
				$user_dob_day 	= $dob[0];
				$user_dob_month = $dob[1];
				$user_dob_year 	= $dob[2];
			?>
                <input type="hidden" name="scbox_age" id="scbox_age" value="" >
                <fieldset class="date"> 
                  <label for="day_start"></label> 
                  <select id="day_start" name="day_start" onchange="validateform(this);" >
                    <option value="" >DAY</option>  
                  <?php for($i=1;$i<32;$i++){ ?>
                    <option value="<?php echo $i; ?>" <?php echo ($user_dob_day==$i ) ? 'selected' : ''; ?> ><?php echo $i; ?></option> 
                  <?php } ?>                     
                  </select> - <label for="month_start"></label> 
                  <select id="month_start" name="month_start" onchange="validateform(this);" >
                    <option value="" >MONTH</option>    
                    <option value="1" <?php echo ($user_dob_month==1 ) ? 'selected' : ''; ?> >January</option>       
                    <option value="2" <?php echo ($user_dob_month==2 ) ? 'selected' : ''; ?> >February</option>       
                    <option value="3" <?php echo ($user_dob_month==3 ) ? 'selected' : ''; ?> >March</option>       
                    <option value="4" <?php echo ($user_dob_month==4 ) ? 'selected' : ''; ?> >April</option>       
                    <option value="5" <?php echo ($user_dob_month==5 ) ? 'selected' : ''; ?> >May</option>       
                    <option value="6" <?php echo ($user_dob_month==6 ) ? 'selected' : ''; ?> >June</option>       
                    <option value="7" <?php echo ($user_dob_month==7 ) ? 'selected' : ''; ?> >July</option>       
                    <option value="8" <?php echo ($user_dob_month==8 ) ? 'selected' : ''; ?> >August</option>       
                    <option value="9" <?php echo ($user_dob_month==9 ) ? 'selected' : ''; ?> >September</option>       
                    <option value="10" <?php echo ($user_dob_month==10 ) ? 'selected' : ''; ?> >October</option>       
                    <option value="11" <?php echo ($user_dob_month==11 ) ? 'selected' : ''; ?> >November</option>       
                    <option value="12" <?php echo ($user_dob_month==12 ) ? 'selected' : ''; ?> >December</option>      
                  </select> -
                  <label for="year_start"></label> 
                  <select id="year_start" name="year_start" onchange="validateform(this);" >
                    <option value="" >YEAR</option>  
                  <?php for($i=date("Y");$i>=1947;$i--){ ?>
                    <option value="<?php echo $i; ?>" <?php echo ($user_dob_year==$i ) ? 'selected' : ''; ?> ><?php echo $i; ?></option> 
                  <?php } ?>
                  </select> 
                  <span class="inst">Date of Birth*</span>  
                 
                </fieldset>
                 <label id="scbox_age-error" class="error" for="scbox_age">Please enter your date of birth</label>
               <!--    <label id="month_start-error" class="error" for="month_start">Please enter your month of birth</label>
                  <label id="year_start-error" class="error" for="year_start">Please enter your year of birth</label> -->               
            </div>
            <div class="form-group">
              <input type="email" class="form-control" disabled placeholder="EMAIL ID" value="<?php echo $profiledata->email; ?>">
              <div class="message text-error hide"></div>
            </div>

            <div class="form-group form-group-password">
              <input type="text" name="sc_password1" id="sc_password1" class="form-control" placeholder="PASSWORD" value="&#x2666;&#x2666;&#x2666;&#x2666;&#x2666;&#x2666;&#x2666;&#x2666;">
              <a onclick="resetPassword();" alt="Reset Password"><i class="icon icon-edit"></i></a>
              <div class="message text-danger hide"></div>
            </div>
            <div class="form-group">
              <input type="tel" name="sc_mobile1" id="sc_mobile1" class="form-control" placeholder="MOBILE NUMBER" value="<?php echo $profiledata->contact_no; ?>" maxlength="10" minlength="10">
              <div class="message text-error hide"></div>
            </div>
            <div>
              <!--<a class="scbtn" href="#"><span>Update</span></a>-->
              <button type="submit" id="sc_signupedit" name="sc_signupedit" value="edit" class="scbtn scbtn-primary"><span>Update</span></button>
            </div>
          </div>
        </form>
        </div>
      </div>
    </div>
  </section>
  <!-- <section class="section section-shadow style-setting">
    <div class="container">
      <div class="title-section">
        <h2 class="subtitle">Style Profile Settings</h2>
      </div>
	<?php if($user_info->bucket > 0){ ?>
      <div class="row">
        <div class="col-md-3 col-sm-3">
          <div class="item">
            <div class="heading">
              Body Shape <span><a href="<?php echo base_url().'pa/3'; ?>" alt="Edit PA"><span class="btn-edit"><i class="icon icon-edit"></i></span></a></span>
            </div>
                <div class="img-wrp">
				  <img src="<?php echo str_replace(".svg","-selected.svg",$user_pa_data->user_body_shape['image_name']); ?>">
                </div>
                <div class="title">
                  <?php echo $user_pa_data->user_body_shape['answer']; ?>
                </div>
              </div>
        </div>
        <div class="col-md-3 col-sm-4">
          <div class="item">
            <div class="heading">
              Style Preference <span><a href="<?php echo base_url().'pa/4'; ?>" alt="Edit PA"><span class="btn-edit"><i class="icon icon-edit"></i></span></a></span>
            </div>
            <div class="box">
              <div class="img-wrp">
                <img src="<?php echo $user_pa_data->user_style_p1['image_name']; ?>">
              </div>
				<div class="title">
					<?php echo $user_pa_data->user_style_p1['answer']; ?>
				</div>
              <span class="active-content">
              <span class="shape-diamond">1</span>
              </span>
            </div>
            <div class="box">
              <div class="img-wrp">
                <img src="<?php echo $user_pa_data->user_style_p2['image_name']; ?>">
              </div>
			  <div class="title">
					<?php echo $user_pa_data->user_style_p2['answer']; ?>
				</div>
              <span class="active-content">
              <span class="shape-diamond">2</span>
              </span>
            </div>
            <div class="box">
              <div class="img-wrp">
                <img src="<?php echo $user_pa_data->user_style_p3['image_name']; ?>">
              </div>
			    <div class="title">
					<?php echo $user_pa_data->user_style_p3['answer']; ?>
				</div>
              <span class="active-content">
              <span class="shape-diamond">3</span>
              </span>
            </div>
          </div>
      </div>
      <div class="col-md-3 col-sm-4">
        <div class="item">
          <div class="heading">
            Wardrobe <span><a href="<?php echo base_url().'pa/5'; ?>" alt="Edit Wardrobe"> <span class="btn-edit"><i class="icon icon-edit"></i></span></a></span>
          </div>
          <div class="box-brands-list available-brands">
            <?php if(!empty($user_pa_data->user_brand_list)){ $brand = 1;
              foreach($user_pa_data->user_brand_list as $val){
            ?>
              <span class="tag" value="<?php echo $brand; ?>" id="brand_list_<?php echo $brand; ?>" onclick="add_in_list(<?php echo $brand; ?>,<?php echo "'".$val->brand_name."'"; ?>)"><?php echo $val->brand_name; ?></span>
            <?php $brand++;}//foreach
            }//if ?>
          </div>
        </div>
    </div>
    </div>
	<?php }else{ ?>
<a class="scbtn scbtn-primary" onclick="CheckNumber();" href="<?php //echo base_url().'pa?type=add'; ?>"><span>Add PA</span></a> 

  <a class="scbtn scbtn-primary" id="add_pa" onclick="CheckNumber();" ><span>Add PA</span></a>
	<?php } ?>
        </div>
  </section> -->
</div>
<script type="text/javascript">
function CheckNumber(){
  if($("#sc_mobile1").val()==""){
    console.log("NO phone number");
    alert('Enter your mobile number');
    $("#add_pa").attr("href","javascript:void();");
  } else {
    console.log("YES phone number");
    //$("#add_pa").attr("href",);
    console.log($("#sc_mobile1").val().length);
    var str = '<?php echo base_url().'pa?type=add'; ?>';
    console.log(str);
    window.location.href = str;
  }
}

</script>
