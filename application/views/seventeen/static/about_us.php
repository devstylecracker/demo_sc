<div class="page-about">
  <section class="section section-shadow">
    <div class="container">
      <div class="title-section">
        <h1 class="title">StyleCracker at a glance</h1>
      </div>
  <!--  <div class="about-tagline">
        <h2 class="title-2"><span>StyleCracker is India's only personalized styling platform.</span></h1>
        <h3 class="title-2"><span>We transform the way you shop.</span></h2>
      </div>-->

      <div class="about-intro">
      <!--  <p>StyleCracker was founded in 2013 by celebrity stylist Archana Walavalkar and former investment banker Dhimaan Shah. As India's only fashion styling platform, we transform the way you shop.</p>-->
<p>
StyleCracker was founded in 2013 by Dhimaan Shah (ex Investment Banker) and Archana Walavalkar (ex Fashion Editor – Vogue) with a vision to make India stylish. StyleCracker offers convenience to its users and empowers them to be the best version of themselves. StyleCracker helps each individual look great and fashionable by curating personalized boxes full of clothes and accessories, taking the stress out of the shopping process.
</p>
<p>
The StyleCracker Box is a stylist curated, customized box of fashion that gets delivered to your doorstep. All you have to do is visit our website, answer a few simple questions about yourself and our stylists will get to work. The StyleCracker Box is tailored to your tastes, budget and body type so that you receive a box of everything you love. Keep what you like and return the rest for a full refund, it's that easy!
</p>
      </div>
    </div>
  </section>

  <!--
       	<div class="row seven-cols about-intro-boxes">
     <div class="col-md-1">
      <div class="box">
       <img src="<?php echo base_url(); ?>assets/images/about/about-img-1.png">
       <h5>Sign up to get styled on-demand</h5>
       <p>Our stylists dress Bollywood's A-listers. Getting your fave celeb's style just got easier!</p>
 </div>
  </div>

     <div class="col-md-1">
       <div class="box">
       <img src="<?php echo base_url(); ?>assets/images/about/about-img-2.png">
       <h5>Get customised, shoppable looks to suit your body type, personal style and budget</h5>
     <p>We suggest fashion, beauty and personal care products based on your profile. To help us style you better, you can also share images with us via the chat feature.</p> </div>
      </div>
     <div class="col-md-1">
       <div class="box">
       <img src="<?php echo base_url(); ?>assets/images/about/about-img-3.png">
       <h5>Shop instantly with our 'Buy Now' feature</h5>
     <p>Buy what you love with the click of a button or add to your wishlist and save it for later. <br/>

     PS: We send prompt notifications when the brand you want goes on sale!</p> </div>
      </div>
     <div class="col-md-1">
       <div class="box">
       <img src="<?php echo base_url(); ?>assets/images/about/about-img-4.png">
       <h5>Live chat with our celebrity stylists to get a quick fix for your outfits</h5>
     <p>They are there for you 24X7 and for absolutely free! So, get daily how-to tips and creatively put together what you already own.</p> </div>
      </div>
     <div class="col-md-1">
       <div class="box">
       <img src="<?php echo base_url(); ?>assets/images/about/about-img-5.png">
       <h5>Find out where your favourite celebrities shop</h5>
     <p>Love a particular on-screen look? We tell you where to get it. That too, the pocket-friendly way!</p>
 </div>
  </div>
     <div class="col-md-1">
       <div class="box">
       <img src="<?php echo base_url(); ?>assets/images/about/about-img-6.png">
       <h5>Read expert brand reviews by fashion industry insiders</h5>
     <p>Get honest feedback on stores' latest collections, their services and lots more.</p>
 </div>
  </div>
     <div class="col-md-1">
       <div class="box">
       <img src="<?php echo base_url(); ?>assets/images/about/about-img-7.png">
       <h5>Rate your shopping experience</h5>
     <p>Share what works for you with other shopaholics!</p>
 </div>
  </div>
</div>-->

  <section class="section section-shadow">
    <div class="container">
      <div class="title-section">
        <h1 class="title">Our Stylists</h1>
      </div>

      <div class="stylist-section">
        <div class="row">
          <div class="col-md-3">
            <div class="stylist-box">
              <div class="img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/about/priyanka2.jpg">
              </div>
              <div class="stylist-info-box">
                <div class="title">
                  Priyanka
                </div>
                <div class="short-info">
                  The edgy experimental girl with an endless love for dainty jewelry. Loves her basics as much as her statement pieces.
                </div>
                <!--  <div class="read-more"><a href="https://www.stylecracker.com/stylist/stylist-detail/priyanka-bhatt">(Read More)</a></div>-->
              </div>
            </div>

          </div>


          <div class="col-md-3">
            <div class="stylist-box">
              <div class="img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/about/nikita2.jpg">
              </div>
              <div class="stylist-info-box">
                <div class="title">
                  Nikita
                </div>
                <div class="short-info">
                  A comfortable dresser with lots of black and grey in her wardrobe. She’s an avid Instagram fanatic who loves clicking pictures aesthetically.
                </div>
                <!--  <div class="read-more"><a href="https://www.stylecracker.com/stylist/stylist-detail/nikita-rawlani">(Read More)</a></div>-->
              </div>
            </div>

          </div>

          <div class="col-md-3">
            <div class="stylist-box">
              <div class="img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/about/ashna2.jpg">
              </div>
              <div class="stylist-info-box">
                <div class="title">
                  Ashna
                </div>
                <div class="short-info">
                  With an incessant urge for exploring new things, she is a moody dresser who loves experimenting with silhouettes. Wears her heart on her sleeve while she loves what she does and does what she loves. </div>
                <!--  <div class="read-more"><a href="https://www.stylecracker.com/stylist/stylist-detail/priyanka-bhatt">(Read More)</a></div>-->
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="stylist-box">
              <div class="img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/about/shalini2.jpg">
              </div>
              <div class="stylist-info-box">
                <div class="title">
                  Shalini
                </div>
                <div class="short-info">
                She's the unassuming indie girl with a relentless love for jhumkaas. Constantly takes inspiration from everything around the country having utmost faith in talents who support her every day. </div>
                <!--  <div class="read-more"><a href="https://www.stylecracker.com/stylist/stylist-detail/priyanka-bhatt">(Read More)</a></div>-->
              </div>
            </div>
          </div>
        </div>
		
		<div class="" style="text-align:right; padding-top:20px;">
       and many more...
    </div>
	
      </div>
    </div>
  </section>
  <!-- <section class="section section-shadow">
    <div class="container">
      <div class="title-section">
        <h1 class="title">The Founder</h1>
      </div>

      <div class="founder-section">
        <div class="row">
          <div class="col-md-6">
            <div class="founder-box first">
              <div class="row">
                <div class="col-md-5">
                  <div class="img-wrp">
                    <img src="<?php echo base_url(); ?>assets/images/about/dhimaan.jpg">
                  </div>
                </div>
                <div class="col-md-7">
                  <div class="founder-box-inner">
                    <div class="title">Dhimaan Shah</div>
                    <div class="designation">Co-Founder & Managing Director</div>
                    <div class="short-info">
                      Former investment banker, entered the fashion space to disrupt the market with his goal of shaping people’s buying decisions. Fun, exuberant and fast paced - he is the managerial backbone of the company.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="founder-box second">
              <div class="row">
                <div class="col-md-5">
                  <div class="img-wrp">
                    <img src="<?php echo base_url(); ?>assets/images/about/archana-1.jpg" style="margin-right:-1px;">
                  </div>
                </div>
                <div class="col-md-7">
                  <div class="founder-box-inner">
                    <div class="title">Archana Walavalkar
                    </div>
                    <div class="designation">Co-Founder & Creative Director</div>
                    <div class="short-info">
                    The creative genius of the company, she brings glamour and sophistication to everything she does – with an aim of helping people be the best version of themselves.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section> -->
</div>
