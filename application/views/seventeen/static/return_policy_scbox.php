<div class="page-policies page-static">
  <section class="section section-shadow">
    <div class="container">
      <div class="title-section">
        <h1 class="title">Return Policy - SC BOX</h1>
      </div>

      <div class="content-wrp">
    <p>
      StyleCracker.com brings to you utmost quality and the coolest choices in fashion, all at prices unparalleled on its platform. In the rare occasion that the fit, colour or quality of the product doesn’t flatter you the most, lucky for you, we would be
      happy to take a return within 4 days of purchase and in addition, you would also be refunded appropriately. However, do call our customer service executives within 48 hours of receiving the product, to help us offer the best, most effective and
      instant services to you.</p>

    <h5>What condition should the product be in?</h5>
    <p>As a courtesy to the next customer, the returned items should be unwashed and unused. Please keep the product intact as they need to be returned in their original packaging with the tags.</p>
    <h5>What if I receive a damaged product?</h5>
    <p>We strive to deliver the very best! But unfortunately, if you receive a damaged/defective product, do notify us within 48 hours of receipt. Share a picture with us at <a href="mailto:scbox@stylecracker.com">scbox@stylecracker.com</a> and
      we’ll make it our top priority! </p>
    <h5>Non-refundable products?</h5>
    <p>Following product categories fall under "No Return/Exchange" unless there is an inherent damage/quality issue at the time of delivery due to a miss from our quality team. </p>
    <ul>
      <li>Lingerie / Shapewear</li>
      <li>Briefs / Vests / Trunks</li>
      <li>Swimwear</li>
      <li>Beachwear</li>
      <li>Accessories (Jewellery, Eyewear and Shoe Accessories)</li>
      <li>Beauty products</li>
      <li>Cutomized products (Alterations will be accepted)</li>
      <li>Stationary</li>
      <li>Socks</li>
      <li>Any freebies</li>
      <li>Sale products (Incase of any size discrepancies, the product can be exchanged for another size)</li>
    </ul>
    <h5>What if the returned products don't meet the conditions?</h5>
    <p>We try our best to make the return process seamless for you! However, the product being returned needs to reach the above mentioned requirements, or else, we would not be able to offer a return/refund to you from our end. </p>
    <h5>What are my return options?</h5>
    <ul>
      <li>A Coupon Code of the refund value, which can be redeemed against any purchase.</li>
      <li>Cash refund via bank transfer into your account for all COD orders within 10 working days.</li>
      <li>Money-back to the paid account/credit card for all online orders.</li>
      <li>The customer needs to request for a return/exchange within 4 days of delivery of the box</li>
    </ul>
    Do feel free to write to us with any further queries at <a href="mailto:scbox@stylecracker.com">scbox@stylecracker.com</a>.

  </div>
</div>
</section>
</div>

<style>
.page-policies h5{margin:20px 0 5px; font-weight: 700;}
.page-policies ul{list-style:inside; margin-bottom: 20px;}
</style>
