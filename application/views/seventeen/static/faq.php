<div class="page-policies page-static">
  <section class="section section-shadow">
    <div class="container">
      <div class="title-section">
        <h1 class="title">Frequently Asked Questions</h1>
      </div>
      <div class="content-wrp">
        <div class="faq-wrp" style="margin-bottom:50px;">
          <div class="faq-ques">
            <h4>1) What is StyleCracker.com?</h4></div>
          <div class="faq-ans">
            <p>StyleCracker is an interactive online experience providing personalised styling to all. Our motto - Everyone should embrace their OWN personal style and StyleCracker helps you achieve that.</p>
          </div>

          <div class="faq-ques">
            <h4>2) How does StyleCracker work?</h4></div>
          <div class="faq-ans">
            <p>It's simple – you ask us a question, we have an answer for it. Be it your clothes, accessories, hair and make-up even, we have it all. Essentially, we tell you what to buy, where to buy it and how to wear it. If you have two black dresses,
              don’t worry, we are not going to advise you to buy a third. Instead, we will help you wear the dresses you currently have, differently each time.</p>
            <p>On the StyleCracker site, shopping is seamlessly synced with getting styled. So, with a simple click on our 'Buy Now' button, you can instantly purchase the recommendations our stylists put forth. Looking good has never been easier!</p>
          </div>

          <div class="faq-ques">
            <h4>3) Who's the StyleCracker team?</h4></div>
          <div class="faq-ans">
            <p>The StyleCracker team consists of talented celebrity stylists, headed by Archana Walavalkar. Prior to co-founding StyleCracker, she worked with Vogue and L'Officiel Magazine for nearly 7 years. She has styled the likes of Karan Johar, Aishwarya
              Rai Bachchan, Deepika Padukone, Sonam Kapoor and continues to do so.</p>
          </div>


          <!--<div class="faq-ques"><h4>4) Can I get in touch with my stylist anytime?</h4></div>
    <div class="faq-ans"><p>We work all day, every day so ask away!</p></div>-->


          <div class="faq-ques">
            <h4>4) Do I need to pay to get styled by StyleCracker?</h4></div>
          <div class="faq-ans">
            <p>Our online styling services are completely free of charge. Sign up and get your very own personal stylist today!</p>
          </div>

          <!--
    <div class="faq-ques"><h4>5) Can I shop on StyleCracker.com?</h4></div>
    <div class="faq-ans"><p>With our ‘Buy Now’ feature you can instantly purchase the products recommended to you, by our stylists.</p> </div>-->


          <div class="faq-ques">
            <h4>5) Can I shop on StyleCracker.com?</h4></div>
          <div class="faq-ans">
            <p>With our ‘Buy Now’ feature, you can instantly purchase products recommended to you by our stylists.</p>
            <p>
              Another easy and hassle-free way to shop is with the StyleCracker Box, which includes our celebrity stylists handpicking the best in fashion for you, based on your preferences and budget, delivered to you at your doorstep.
            </p>
          </div>


          <div class="faq-ques">
            <h4>6) What if I'm not happy with a purchase suggested by you?</h4></div>
          <div class="faq-ans">
            <p>StyleCracker is not accountable for any procedures related to the purchase and post-purchase of goods. However, our team will be happy to address any style related queries to ensure that you look crackingly fabulous always!</p>
          </div>


          <div class="faq-ques">
            <h4>7) What's more on StyleCracker?</h4></div>
          <div class="faq-ans">
            <p>Apart from getting regular style advice from our celebrity stylists, you also get –</p>
            <ul>
              <li>Collections created by our team, based on what’s trending.</li>
              <li>Get a StyleCracker Box curated for yourself based on your preferences, body type and budget.</li>
              <li>Exclusive invites to StyleCracker events and workshops.</li>
              <li>Special StyleCracker.com discounts and deals.</li>
              <li>Updates on the latest trends, celebrity style news and designer deals on our <a href="https://www.stylecracker.com/blog/" target="_blank">blog</a>.
              </li>
            </ul>
          </div>
        </div>
        <div class="faq-wrp">
          <div class="title-section">
            <h1 class="subtitle">STYLECRACKER BOX</h1>
          </div>
          <div class="faq-ques">
            <h4>What is it about?</h4></div>
          <div class="faq-ans">
            <p>This is our way of making shopping a more fulfilling experience for you.</p>
            <p>The SC Box includes a personalized set of products that are curated by our team of stylists, as per an individual’s likes, dislikes, body scale and budget.</p>
            <p>Know more about the SCBox <a href="https://www.stylecracker.com/blog/box-solution-shopping-worries/" target="_blank">here</a></p>
          </div>

          <div class="faq-ques">
            <h4>How does it work?</h4></div>
          <div class="faq-ans">
            <p>Sign up and answer a quick list of questions about your preferences.</p>
            <p>Our stylists curate and send the box to you within no time.</p>
            <p>Keep what you like, return what you don’t.</p>
          </div>

          <div class="faq-ques">
            <h4>What if I don’t like something?</h4></div>
          <div class="faq-ans">
            <p>Fret not! You always have the option of exchanging it for something else or getting a refund. </p>
          </div>

          <div class="faq-ques">
            <h4>Which brands will be included in my SC Box?</h4></div>
          <div class="faq-ans">
            <p>This solely depends on your preferences and budget.</p>
          </div>

          <div class="faq-ques">
            <h4>Do you ship worldwide?</h4></div>
          <div class="faq-ans">
            <p>Yes, we do.</p>
          </div>

          <div class="faq-ques">
            <h4>How can I make my payment?</h4></div>
          <div class="faq-ans">
            <p>You can choose to make an online transfer or opt for cash on delivery. </p>
          </div>

          <div class="faq-ques">
            <h4>Will my personal account information be confidential?</h4></div>
          <div class="faq-ans">
            <p>Yes absolutely. Any information you share with us will only be used to provide a better and more personalized shopping experience to you, and would not be disclosed elsewhere under any circumstances. </p>
          </div>

        </div>
      </div>
    </div>
  </section>
</div>
<style>
  .faq-ques h4 {
    margin-bottom: 10px;
    font-weight: 700;
    font-size: 15px;
  }

  .faq-ans {
    margin-bottom: 20px;
  }

  .faq-ans li {
    list-style: disc;
    line-height: 1.7;
    list-style-position: outside;
    margin-left: 20px;
  }
</style>
