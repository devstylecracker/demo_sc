<style type="text/css">
   .extrapad-row{
   margin-top: 40px;
   }
</style>
<div class="page-about">
   <section id="section-testimonials" class="section section-shadow section-testimonials">
      <div class="container">
         <div class="title-section text-center">
            <h1 class="title">TESTIMONIALS</h1>
            <h2 style="font-weight:normal;font-size:14px;text-transform: uppercase;
               letter-spacing: 2px;">We asked a few of our customers about their first experience with the StyleCracker Box</h2>
         </div>
         <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="<?php echo base_url(); ?>assets/images/home/banner/esha.jpg" alt="" style="width:127px;">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>First of all I would like to say thank you!Not only did the stylist get the gist of what I would like, she sent me stuff that I absolutely loved! The order came by very quickly, and reached in time for my birthday too, you guys totally made my day!Keep up the amazing work, and I'm definitely going to subscribe for another box in the near future.</p>
                        <div class="name">
                           - Esha
                        </div>
                     </div>
                  </div>
               </div>

                
                <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="<?php echo base_url(); ?>assets/images/home/banner/shru.jpg" alt="" style="width:127px;">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p> Love the quality of the products, all questions were asked accurately also the delivery was quick.</p>
                        <div class="name">
                           - Shruti
                        </div>
                     </div>
                  </div>
               </div>


              
                <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="<?php echo base_url(); ?>assets/images/home/banner/suhani.jpg" alt="" style="width:127px;">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p> I loved all the stuff sent by StyleCracker. The fit was perfect and the accessories suited my taste. Overall, it was a great experience.</p>
                        <div class="name">
                           - Suhani Dwiedi
                        </div>
                     </div>
                  </div>
               </div>



               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="<?php echo base_url(); ?>assets/images/home/banner/8.jpg" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>Thank you so much StyleCracker, I really appreciate you making an effort to make my experience feel special and personal. I would love to purchase more boxes from you in the future and am super happy with everything I received- STYLECRACKER ROCKS!</p>
                        <div class="name">
                           - Sana Syed
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="<?php echo base_url(); ?>assets/images/home/banner/2.jpg" style="width:127px;" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>I really enjoyed my first experience with StyleCracker and will definitely be trying this out again!</p>
                        <div class="name">
                           - Garima Shandilya
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="<?php echo base_url(); ?>assets/images/home/banner/priyanka1.jpg" style="width:127px;" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>I received my surprise box of goodies from StyleCracker and it just couldn't get better! In this day and age of online shopping, a box of goodies suited to my style and preference was something very outstanding. Not to mention how flattering it is to have a box personalized to my taste and style. Way to go StyleCracker!</p>
                        <div class="name">
                           - Priyanka Talreja
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="<?php echo base_url(); ?>assets/images/home/banner/ramona.jpg" style="width:127px;" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>The StyleCracker stylists have been like 3 am friends. They have helped me pick products that suit me and my body type. Every purchase is tailor made and I love how personal it gets. And in rare scenario when a product doesn’t work , the team is extremely helpful and prompt in arranging easy returns.</p>
                        <div class="name">
                           - Ramona
                        </div>
                     </div>
                  </div>
               </div>
               
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="<?php echo base_url(); ?>assets/images/home/banner/rajni.jpg" alt="" style="width:127px;">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>Absolutely love the stuff on stylecracker! Chic and unique. Alot of pretty cool brands to choose from. Customer service is fab! They literally take care of you and your order.</p>
                        <div class="name">
                           - Rajni Rodrigues
                        </div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12">
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="<?php echo base_url(); ?>assets/images/home/banner/rivoli.jpg" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>I am impressed how they called and understood what I wanted, but was super impressed by how they got my fit right. I have ordered a few boxes now and have never returned or disliked anything. I am very glad I signed up for the StyleCracker box and tell all my friends to do the same too. It’s because I personally love what they curate for me.</p>
                        <div class="name">
                           - Rivoli
                        </div>
                     </div>
                  </div>
               </div>
               
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="<?php echo base_url(); ?>assets/images/home/banner/ved.jpg" alt="" style="width: 127px;">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>It is a good concept that the stylists call and takes the details. Amazing style really liked everything; appreciate the efforts of the stylist who had curated the box. Overall, I am happy with the service.</p>
                        <div class="name">
                           - Vedika
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="<?php echo base_url(); ?>assets/images/home/banner/riya.jpg" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>StyleCracker is my first and foremost personal stylist. The surprise box was indeed a pleasant one to receive. The products were totally up to the mark. I honestly cherished my first ever experience with stylecracker.  Looking forward to many more surprises!</p>
                        <div class="name">
                           - Riya Sinha
                        </div>
                     </div>
                  </div>
               </div>
              <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="<?php echo base_url(); ?>assets/images/home/banner/5.jpg" style="width:127px;" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>Thank you so much for this incredible experience! I loved all my products and found the service so helpful and quick. I will be posting all about the StyleCracker Box online as well.</p>
                        <div class="name">
                           - Muskan Jain
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="<?php echo base_url(); ?>assets/images/home/banner/4.jpg" style="width:127px;" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>I looooooooooved my StyleCracker Box! Each product was so cool and totally my style. Can’t wait to order my next box again!!! MUAH to the StyleCracker team.</p>
                        <div class="name">
                           - Priya Mandal
                        </div>
                     </div>
                  </div>
               </div>
                <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="<?php echo base_url(); ?>assets/images/home/banner/glen.jpg" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>Since I was looking for a personal stylist for myself, having one who's worked with A-listers and the biggest names in the movie business made StyleCracker my absolute choice! From being style guides to helping me with even 'what not to wear', they've been extremely helpful in solving all my fashion queries.</p>
                        <div class="name">
                           - Glenn Gonsalves
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="<?php echo base_url(); ?>assets/images/home/banner/1.jpg" style="width:127px;" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>I was so happy with my StyleCracker Box. When I opened the Box, my products were exceptional. They were all branded and very well suited to my tastes. Shalini is a great listener and understood exactly what I wanted. I was very happy with everything from the quality of the products to customer service to customer satisfaction. Thumbs up!</p>
                        <div class="name">
                           - Dolly Dharmshaktu
                        </div>
                     </div>
                  </div>
               </div>
               
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="<?php echo base_url(); ?>assets/images/home/banner/7.jpg" style="width:127px;" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>Thank you to Shalini for being so responsive and helpful! I loved my products and the entire experience with the StyleCracker team</p>
                        <div class="name">
                           - Krutika
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
       </div>
   </section>
</div>