<div class="page-policies page-static">
  <section class="section section-shadow">
    <div class="container">
      <div class="title-section">
        <h1 class="title">Terms & Conditions</h1>
      </div>

      <div class="content-wrp">
        <div class="ques">
          <h4>Legalese Introduction</h4></div>
        <div class="ans">
          <p>This is a legal agreement between you, the potential user of this website - www.stylecracker.com ("you"), and Kanvas Consultancy Services Private Limited ("StyleCracker"). Your use of StyleCracker's products, services and website, or any of
            the products and services offered on this website (collectively, the "Services") is conditioned on your acceptance, without modification, of these Terms of Use ("Terms"). Please read these Terms carefully. If you do not agree to these Terms,
            you should not use the Services. You can accept the Terms simply by using the StyleCracker Services. In this case, you understand and agree that StyleCracker will consider your use of the Services as agreement to the Terms. StyleCracker may,
            at its sole discretion, modify these Terms at any time, and such modifications will be effective immediately upon being posted on this Site.</p>
        </div>
        <div class="ques">
          <h4>About StyleCracker</h4></div>
        <div class="ans">
          <p><b>Our motto - Everyone should embrace their OWN personal style and StyleCracker helps you achieve that.</b>
            <br/>StyleCracker is India’s first online personalized styling platform which not only provides personalized styling tips and advice but also allows users to purchase various branded products available for sale on the website.</p>
        </div>

        <div class="ques">
          <h4>Description of Products and Services</h4></div>
        <br/>

        <div class="ques">
          <h5>Products</h5></div>
        <div class="ans">
          <p>StyleCracker provides you the opportunity to purchase products from a plethora of brands as enlisted on the ‘Brands’ section of the website. The branded products available for sale on the website include clothing, footwear, jewellery, eyewear
            and other accessories.
            <br/>
            <br/>Further, StyleCracker offers you the opportunity to choose a look / ensemble / assortment of branded products already available on the website based on occasions, seasons or latest fashion trends such as traditional styles for weddings, winter
            wear and accessories, products assorted based on celebrity fashion, etc.</p>
        </div>

        <div class="ques">
          <h5>StyleChat</h5></div>
        <div class="ans">
          <p>StyleCracker also allows you to chat with a StyleCracker stylist to deal with your style and fashion related queries. Such a StyleCracker stylist may also suggest a look / ensemble / assortment of branded products already available on the website
            based on your preferences or may design a new look / ensemble / assortment of branded products especially for you.</p>
        </div>

        <div class="ques">
          <h5>Deals, Discounts and Offers</h5></div>
        <div class="ans">
          <p>StyleCracker provides you with various deals, discounts and offers from time to time along with the option of registering with the website for receiving updates about such promotional offers, deals and discounts. All promotional offers and discounts
            shall bear a unique ‘Promo Code’ which must be specified by you at the time of Checkout in order to avail the benefit of the offer. The applicability and duration of the use of a particular Promo Code shall be decided at the sole discretion
            of StyleCracker and StyleCracker reserves the right to refuse the usage of such Promo Code(s) at any point of time. Please note that Promo Codes cannot be used along with any other special offers on the website. </p>
        </div>

        <div class="ques">
          <h5>Events and Workshops</h5></div>
        <div class="ans">
          <p>StyleCracker organises various workshops and events such as The StyleCracker Borough, the SwapShop, etc. Registering with the StyleCracker website makes you eligible to be among the few lucky ones to receive special invites to such events. </p>
        </div>

        <div class="ques">
          <h5>SC Live</h5></div>
        <div class="ans">
          <p>SC Live is the StyleCracker blog which features a number of articles on diverse topics such as those pertaining to the fashion industry, cuisines, travel, celebrity fashion, latest trends and other information dealing with styling and fashion.</p>
        </div>

        <div class="ques">
          <h5>Media</h5></div>
        <div class="ans">
          <p>The Media section of the website provides information about the latest news on StyleCracker and its founders, stylists, events, etc. and provides links which redirect you to the source of such news such as a link to the website of the TV channel
            which features StyleCracker and its founders, stylists, events, etc.</p>
        </div>



        <div class="ques">
          <h5>Feedback</h5></div>
        <div class="ans">
          <p>We at StyleCracker believe in providing personalized and customized products and services and welcome views and comments from our users. The ‘About’ section of the website also allows you to give us feedback regarding the products and services
            available on the website either by filling the form available on the website or via email. You may also contact StyleCracker telephonically by calling the number provided on the ‘About’ page.</p>
        </div>







        <div class="ques">
          <h4>Provision Of Services</h4></div>
        <div class="ans">
          <p>StyleCracker is constantly innovating in order to provide the best possible experience for its users. You acknowledge and agree that the form and nature of the Services which StyleCracker provides may change from time to time without prior notice
            to you. As part of this continuing innovation, you acknowledge and agree that StyleCracker may stop (permanently or temporarily) providing any of the Services to you or to users generally at StyleCracker's sole discretion, without prior notice
            to you. You may stop using the Services at any time. You acknowledge and agree that if StyleCracker disables access to your account, you may be prevented from accessing the Services, your account details or any files or other content that
            is contained in your account.</p>
        </div>


        <div class="ques">
          <h4>Use Of The Services By You</h4></div>
        <div class="ans">
          <p>In order to access certain Services, you may be required to provide information about yourself (such as identification or contact details) as part of the registration process for the Service, or as part of your continued use of the Services.
            You agree that any registration information you give to StyleCracker will always be accurate, correct, and up to date. You agree to use the Services only for purposes that are permitted by (a) these Terms and (b) any applicable law, regulation,
            or generally accepted practices or guidelines in the relevant jurisdictions (including any laws regarding the export of data or software to and from the United States, United Kingdom, India, and other relevant countries). You agree not to
            access (or attempt to access) any of the Services by any means other than through the interface that is provided by StyleCracker, unless you have been specifically allowed to do so in a separate agreement with StyleCracker. You specifically
            agree not to access (or attempt to access) any of the Services through any automated means (including use of scripts or web crawlers) and shall ensure that you comply with the instructions set out in any robots.txt file present on the Services.
            You agree that you will not engage in any activity that interferes with or disrupts the Services (or the servers and networks which are connected to the Services). Unless you have been specifically permitted to do so in a separate agreement
            with StyleCracker, you agree that you will not reproduce, duplicate, copy, sell, trade, or resell the Services for any purpose. You agree that you are solely responsible for (and that StyleCracker has no responsibility to you or to any third
            party for) any breach of your obligations under the Terms and for the consequences (including any loss or damage which StyleCracker may suffer) of any such breach.</p>
          <br/>


          <div class="ques">
            <h4>Registration, Passwords, And Security</h4></div>
          <div class="ans">
            <p>StyleCracker allows you to register with the website in order to create your account with the website.
              <br/>
              <br/> Please note that the Registration process requires you to provide personal information such as your full name, email ID and password, the use of which is governed by the StyleCracker Privacy Policy. You may also sign up and register with
              StyleCracker through your Facebook or Google Account. StyleCracker will in no way misuse your Facebook/Google accounts and data. We will treat the details provided by you with high confidentiality and in accordance with the StyleCracker
              Privacy Policy.
              <br/>
              <br/> Registration is a one-time process post which you may log in to your account on the website and enjoy the advantages of being a registered user.
              <br/>
              <br/> You agree and understand that you are responsible for not automating account creation. You are also responsible for maintaining the confidentiality of passwords associated with any account you use to access the Services. Accordingly, you
              agree that you will be solely responsible to StyleCracker for all activities that occur under your account. StyleCracker is not responsible for any actions whatsoever taken under your account. If you become aware of any unauthorized use
              of your password or of your account, you agree to notify StyleCracker immediately at support@StyleCracker.com.</p>
          </div>



          <div class="ques">
            <h4>Wishlist, ‘Get This’ and ‘Buy Now’ Options and Checkout</h4></div>
          <div class="ans">
            <p>StyleCracker provides you with the option of adding a look / ensemble / assortment of products to your Wishlist whereby you can save products that you may like, for viewing and / or purchasing at a later date. You may add a look / ensemble
              / assortment of products to your Wishlist by clicking on the heart shaped icon on the bottom right of the image of a look / ensemble / assortment of products. You may browse your Wishlist at any time.
              <br/> In order to select a look / ensemble / assortment of products for purchase, you must select the ‘Get This’ option on the website. Upon selecting a look / ensemble / assortment of products, you will be provided with an option to purchase
              each item constituting such look / ensemble / assortment of products individually by selecting the ‘Buy Now’ option.
              <br/> Upon selecting the Buy Now option, you will be redirected to the Checkout page whereby you can make the payment for initiating purchase of the required product using the Payment Methods enlisted below.</p>
          </div>


          <div class="ques">
            <h4>Payment Methods</h4></div>
          <div class="ans">
            <p>StyleCracker has engaged the services of renowned payment gateway, PayU Payments Private Limited operating under its trade name PayU, for the purpose of the payment transactions. Once the order has been placed, you will be redirected to the
              service webpage of PayU. Any information provided on the webpage of PayU shall be governed by the Privacy Policy of PayU and StyleCracker shall not be responsible for any use of information collected through such webpage.
              <br/> The various modes of payment permitted by StyleCracker are as follows:</p>
          </div>


          <ol>
            <li><b>Credit Card</b>
              <br/>
              <p>Payments may be made using Visa, MasterCard and American Express credit cards.
                <br/>To pay using your credit card, you will need your card number, the expiry date of the card and the three-digit CVV number of the card. You will then be redirected to your bank's secure page for entering your online password to complete
                the payment.</p>
            </li>

            <li><b>Debit Card</b>
              <br/>
              <p>Payments may be made using Visa, MasterCard and American Express debit cards.
                <br/>To pay using your debit card, you will need your card number, the expiry date of the card and the three-digit CVV number of the card. You will then be redirected to your bank's secure page for entering your online password to complete
                the payment.</p>
            </li>

            <li><b>Net banking</b>
              <br/>
              <p>You may also avail your bank's Internet banking services to make payments on this website if such bank is displayed in the list of banks displayed by PayU.</p>
            </li>


            <li><b>Cash on Delivery</b>
              <br/>
              <p>StyleCracker also allows you the option to make payments by cash upon the delivery of the products by the brand(s) whose products are purchased by you. However, please note that the availability of this option will be subject to the policies
                of the brand(s). The availability of this option will be specified to you at the time of Checkout along with the other information regarding the product. Please note that StyleCracker reserves the right, at its sole discretion, to limit
                the quantity of products purchased per person, per household or per order. These restrictions may be applicable to orders placed by the same account, the same debit/credit card and also to orders that use the same billing and/ or shipping
                address. We will notify you should such limit be applied.</p>
            </li>

          </ol>
        </div>





        <div class="ques">
          <h4>Shipping and Delivery Policy</h4></div>
        <div class="ans">
          <p>Please note that the shipping and delivery of the products will be carried out by the brand whose product has been purchased by you. The timelines for the shipping and delivery of the products shall be mentioned with the other product details
            at the time of Checkout. You may track the delivery of the purchased product on the website of the brand whose product has been purchased by you.
            <br/>
            <br/>Please note that StyleCracker is not responsible for the shipping and delivery of the purchased products in any manner whatsoever and shall, therefore, not be liable for any loss, damages, action, claim or liability arising out of or in connection
            with any act, omission or negligence of the brand whose product has been purchased by you.</p>
        </div>



        <div class="ques">
          <h4>Cancellation, Returns and Refund</h4></div>
        <br/>

        <div class="ques">
          <h5>Cancellation</h5></div>
        <div class="ans">
          <p>Once an order for purchase of products available for sale on the website is received by StyleCracker and the Checkout procedure is completed, a cancellation of such order can only be made at any time before you receive an email containing confirmation
            of dispatch of the product from the brand whose product has been purchased by you. No cancellations will be accepted post such time and any cancellation request received post such time shall be deemed to be a request for return of the product
            and shall be subject to the Return and Refund policy.</p>
        </div>


        <div class="ques">
          <h5>Return and Refund</h5></div>
        <div class="ans">
          <p>Please note that all returns and related procedures shall be carried out by the brand(s) whose products have been purchased by you and shall, therefore, not be liable for any loss, damages, action, claim or liability arising out of or in connection
            with any act, omission or negligence of such brand with respect to the return of purchased product(s).
            <br/>
            <br/> A request for return of a purchased product must be communicated to the brand within 15 (fifteen) days of the receipt of the purchased product by you. Any request for return of a product shall be subject to verification of purchase and examination
            of the condition of the product at the time of return by the brand(s) and may only be made within 15 (fifteen) days from the date of purchase of the product. Upon acceptance of such a request, the brand may arrange for a pick up of the product
            subject to the return policy of the brand. Please note that certain brands do not arrange for a pick-up of the product and in such case, you may be required to deliver the product to the brand in accordance with the terms of the return policy
            of the brand. Hence, you are hereby advised to read the return policy of the brand carefully as the return of a product shall be subject to the return policy of the product.
            <br/>
            <br/> Any refund against a request for a return shall be processed only upon receipt of the product by the brand along with the original invoice. A refund shall be processed by StyleCracker within 15(fifteen) working days from the receipt of the
            returned product by the brand. The refund shall be processed through the same method as that used by you for making the payment of the price of a product. However, please note that any refund of an amount which had been paid by cash will be
            made to your bank account vide RTGS / by cheque / by demand draft.
            <br/>
            <br/><b>Please note that certain brands do not permit a cancellation or return of products. You will be informed at the time of Checkout if such cancellation and return services are not available for the products of a particular brand. You understand that the cancellation and return policy mentioned herein shall not be applicable to such products and brands.<br/><br/>It is clarified that in the event of any discrepancy between the terms as mentioned in this clause and the return policy of the brand, the policy of the brand shall be applicable.</b>


          </p>
        </div>




        <div class="ques">
          <h4>User Content and Conduct</h4></div>
        <div class="ans">
          <p>The Site may include interactive areas or services such as personal profile pages, user-to-user messaging features, rating tools, forums, musical reviews, chat rooms, message boards, and other areas or services in which users can create, send,
            post, share or store content, data, information, messages, text, music, sound, photos, video, graphics, applications, code or other materials or other items on or through the website ("User Content"). You are solely responsible for your use
            of such interactive areas and user content and are advised to use them at your own risk. By using the website or the Services, you agree not to post, upload to, transmit, distribute, store, create or otherwise publish through the website or
            the Services any of the following:</p>
          <ol type="a">
            <li>User Content that is unlawful, libelous, defamatory, obscene, pornographic, indecent, suggestive, harassing, threatening, invasive of privacy or publicity rights, abusive, inflammatory, fraudulent or otherwise objectionable;</li>
            <li>User Content that would constitute, encourage or provide instructions for a criminal offence, violate the rights of any person, or otherwise violate any local, state, national or international law, rule, or regulation.</li>
            <li>User Content that may infringe or violate any patent, trademark, trade secret, copyright or other intellectual or proprietary right of any party. By posting any User Content, you represent and warrant that you have the lawful right to distribute
              and reproduce such User Content;</li>
            <li>Unsolicited promotions, political campaigning, advertising or solicitations;</li>
            <li>Private information of any third party, including, without limitation, addresses, phone numbers, email addresses and credit card numbers;</li>
            <li>Viruses, corrupted data or other harmful, disruptive or destructive files;</li>
            <li>User Content that, in the sole judgment of StyleCracker, is objectionable or which restricts or inhibits any other person from using or enjoying the Interactive Areas of the Site;</li>
            <li>Classified or sensitive aviation, military or government information of any jurisdiction;</li>
            <li>User Content that encourages or glorifies any reckless, irresponsible, or illegal activity or behavior;</li>
          </ol>
        </div>


        <div class="ques">
          <h4>Content In The Services</h4></div>
        <div class="ans">
          <p>ou understand that all information (such as files, media, images, text) which you may have access to as part of, or through your use of, the Services is the sole responsibility of the source from which such content originated. All such information
            is referred to here as the "Content". StyleCracker does its best to give proper credit to the source from which the data was indexed. If some content is cached, it is done for the sole purpose of providing users with the best possible service.
            If you think that any content is objectionable, please inform StyleCracker at the earliest. StyleCracker respects the intellectual property of others, and we ask our users to do the same. If you believe that your work is used in a way that
            constitutes a violation or aninfringement of your intellectual property rights, please provide StyleCracker the following information:</p>
          <ol type="a">
            <li>A signature (electronic or physical) of the person authorized to act on behalf of the holder of the intellectual property right.</li>
            <li>A description of the intellectual property that you claim has been infringed.</li>
            <li>A description of where the material that you claim is infringing is located on the website.</li>
            <li>Your address, telephone number and email address.</li>
            <li>A statement by you that you have good faith belief that the disputed use is not authorized by the person holding the intellectual property rights, its agent, or the law.</li>
            <li>A statement by you, made under penalty of perjury, that all the above information is accurate, and that you are the person holding the intellectual property rights or authorized to act on the behalf of such person holding the intellectual
              property rights.</li>
          </ol>

          <p>You agree that you are solely responsible for (and that StyleCracker has no responsibility to you or to any third party for) any Content that you create, transmit, or display while using the Services and for the consequences of your actions
            (including any loss or damage which StyleCracker may suffer) by doing so. You understand that by using the Services you may be exposed to Content that you may find offensive, indecent, or objectionable and that, in this respect, you use the
            Services at your own risk.</p>
        </div>





        <div class="ques">
          <h4>StyleCracker's And Your Proprietary Rights</h4></div>
        <div class="ans">
          <p>Unless you have agreed otherwise in writing with StyleCracker, nothing in these Terms gives you a right to use any of StyleCracker's trade names, trade marks, service marks, logos, domain names, and other distinctive brand features. You agree
            that you shall not remove, obscure, or alter any proprietary rights notices (including copyright and trade mark notices) which may be affixed to or contained within the Services. Unless you have been expressly authorized to do so in writing
            by StyleCracker, you agree that in using the Services, you will not use any trade mark, service mark, trade name, or logo of any company or organization in a way that is likely or intended to cause confusion about the owner or authorized user
            of such marks, names or logos.</p>
        </div>



        <div class="ques">
          <h4>Termination Of Relationship With StyleCracker</h4></div>
        <div class="ans">
          <p>StyleCracker may, at any time, terminate its legal agreement with you, including termination of your account, if you breach any provision of these Terms, as per the sole discretion of StyleCracker. StyleCracker holds the right to deny you Services
            without any express reason. StyleCracker may also terminate the relationship with you if StyleCracker is legally required to do so (e.g., when provision of Services provided by StyleCracker is, or becomes, unlawful); or if the brand(s) / partner(s)
            with whom StyleCracker offered the Services to you has terminated its relationship with StyleCracker or ceased to offer the Services to you; or the provision of the Services to you by StyleCracker is, in StyleCracker's opinion, no longer commercially
            viable.</p>
        </div>



        <div class="ques">
          <h4>Exclusion Of Warranties</h4></div>
        <div class="ans">
          <p>You expressly understand and agree that your use of the Services is at your sole risk and that the Services are provided on an "as is" and "as available" basis. In particular, StyleCracker, its subsidiaries, affiliates and its licensors do not
            represent or warrant to you that:</p>
          <ol type="a">
            <li>Your use of the Services will meet your requirements,</li>
            <li>Your use of the Services will be uninterrupted, timely, secure, or free from error,</li>
            <li>Any information obtained by you as a result of your use of the services will be accurate or reliable, and</li>
            <li>Any material downloaded or otherwise obtained through the use of the Services is done at your own discretion and risk and you will be solely responsible for any damage to your computer system or other device, or loss of data that results from
              the download of any such material.</li>
            <li>No advice or information, whether oral or written, obtained by you from StyleCracker, or through or from the Services shall create any warranty not expressly stated in the terms.</li>
            <li>StyleCracker further expressly disclaims all warranties and conditions of any kind, whether express or implied, including, but not limited to, the implied warranties and conditions of merchantability, fitness for a particular purpose, and
              non-infringement.</li>
          </ol>

        </div>

        <div class="ques">
          <h4>Limitation Of Liability</h4></div>
        <div class="ans">
          <p>StyleCracker shall not be liable for any direct, indirect, incidental, special, consequential, or exemplary damages which may be incurred by you, however caused and under any theory of liability. This shall include, but not be limited to:</p>

          <ol>
            <li>any loss of profit (whether incurred directly or indirectly), </li>
            <li>any loss of goodwill or business reputation,</li>
            <li>any loss of data suffered, cost of procurement of substitute goods or services, or other intangible loss;
              <ol type="a">
                <li>Any other loss or damage which may be incurred by you, including but not limited to loss or damage as a result of:any reliance placed by you on the completeness, accuracy or existence of any advertisement, or</li>
                <li>as a result of any relationship or transaction between you and any advertiser or sponsor whose advertisement appears on the Services; </li>
                <li>any changes which StyleCracker may make to the Services, or </li>
                <li>for any permanent or temporary cessation in the provision of the Services (or any features within the Services);</li>
                <li>the deletion of, corruption of, or failure to store any content and other communications data maintained or transmitted by or through your use of the services.</li>
                <li>your failure to provide StyleCracker with accurate account information;</li>
                <li>your failure to keep your password or account details secure and confidential.</li>
              </ol>
            </li>
          </ol>
          <div class="ques">
            <h4>Liability as to Product Quality</h4></div>
          <div class="ans">
            <p>StyleCracker shall take reasonable measures to ensure the merchantable quality of the products available for sale on its website / mobile application. Further, information regarding the material of the Products will be provided to you along
              with the other product information. You are hereby advised and you hereby agree to exercise due care and caution with respect to any allergies that you may be suffering from or have suffered from in the past which may be caused on account
              of a particular product material. StyleCracker shall not be liable for any loss, cost, action, damages, claim or liability arising out of or in connection with the use of a product available for sale on the website / mobile application.
              </p>
          </div>
          <p>Please note that the above mentioned limitations on StyleCracker's liability to you shall apply whether or not StyleCracker has been advised of or should have been aware of the possibility of any such losses arising.</p>
        </div>



        <div class="ques">
          <h4>Advertisements</h4></div>
        <div class="ans">
          <p>Some of the Services may be supported by advertising revenue and may display advertisements and promotions of third parties. These advertisements may be targeted to the content of information stored on the website, queries made through the website,
            or other information. The manner, mode, and extent of advertising by StyleCracker on the website are subject to change without specific notice to you. In consideration for StyleCracker granting you access to and use of the Services, you agree
            that StyleCracker may place such advertisements on the Services.</p>
        </div>

        <div class="ques">
          <h4>Third Party Content and Applications</h4></div>
        <div class="ans">
          <p>StyleCracker may provide/display any third-party content and links in the site (the "Third Party Content") and may make third party software applications available to the users (the "Third Party Applications"). StyleCracker does not monitor
            or does not have any control over such Third Party links or contents, and is not responsible or liable for, any Third Party Content or Third Party Applications. StyleCracker makes no representations or warranties of any kind on such Third
            Party Content or Third Party Applications, including without limitation, the content, functionality, accuracy, completeness, fitness for a particular purpose, merchantability, legality or safety thereof. Your use of Third Party content and
            Third Party applications will be at your own risk.</p>
        </div>



        <div class="ques">
          <h4>Privacy</h4></div>
        <div class="ans">
          <p>StyleCracker collects, stores, processes and uses your information in accordance with StyleCrackerPrivacy Policy. By using the website and/ or by providing your information, you consent to the collection and use of the information you disclose
            on the website by StyleCrackerin accordance with the StyleCrackerPrivacy Policy. StyleCracker may from time to time contract with third party payment service providers including banks to open nodal bank account under applicable Indian laws,
            to facilitate the payment between users i.e. buyers and sellers and for collection of any fees and other charges. These third party payment service providers may include third party banking or credit card payment gateways, payment aggregators,
            cash on delivery or demand draft / pay order on delivery service providers, mobile payment service providers or through any facility as may be authorized by the Reserve Bank of India for collection, refund and remittance, as the case may be
            of payment or supporting the same in any manner. "StyleCracker shall be under no liability whatsoever in respect of any loss or damage arising directly or indirectly out of the decline of authorization for any transaction, on account of a
            cardholder having exceeded the preset limit mutually agreed by us with our acquiring bank from time to time."</p>
        </div>




        <div class="ques">
          <h4>Other Content</h4></div>
        <div class="ans">
          <p>The Services may include hyperlinks to other web sites or content or resources. StyleCracker has no control over any websites or resources which are provided by companies or persons other than StyleCracker. You acknowledge and agree that StyleCracker
            is not responsible for the availability of any such external sites or resources, and does not endorse any advertisement, products, or other materials on or available from such websites or resources. You acknowledge and agree that StyleCracker
            is not liable for any loss or damage which may be incurred by you as a result of the availability of those external sites or resources, or as a result of any reliance placed by you on the completeness, accuracy or existence of any advertisement,
            products or other materials on, or available from, such web sites or resources.</p>
        </div>

        <div class="ques">
          <h4>Grievance Officer</h4></div>
        <div class="ans">
          <p>In accordance with Information Technology Act 2000 and rules made there under, the Grievance Officer for the purpose of your personal sensitive information as governed by the StyleCracker Privacy Policy is Dhimaan Shah and can be reached at
            support@stylecracker.com.</p>
        </div>



        <div class="ques">
          <h4>Governing Law</h4></div>
        <div class="ans">
          <p>These Terms and all the rules and policies contained herein and any usage of the website by you and your dealings with StyleCracker shall be governed and construed in accordance with the laws of India. If any dispute arises between you and StyleCracker
            during your use of the website or your dealing with the StyleCracker in relation to any activity on the website, in connection with the validity, interpretation, implementation or alleged breach of any provision of these Terms and all the
            rules and policies contained herein, the dispute shall be referred to a sole arbitrator who shall be an independent and neutral third party appointed by StyleCracker. The Arbitration & Conciliation Act, 1996 shall govern the arbitration proceedings.
            The arbitration proceedings shall be in the English language. The place of arbitration shall be Mumbai.</p>
        </div>

        <div class="ques">
          <h4>General Legal Terms</h4></div>
        <div class="ans">
          <p>Sometimes when you use the Services, you may (as a result of, or through your use of the Services) use a service or download a piece of software, or media, or purchase goods, which are provided by another person or company. Your use of these
            other services, software, or goods may be subject to separate terms between you and the company or person concerned. If so, these Terms do not affect your legal relationship with these other companies or individuals. The Terms constitute the
            whole legal agreement between you and StyleCracker and govern your use of the Services (but excluding any services which StyleCracker may provide to you under a separate written agreement), and completely replace any prior agreements between
            you and StyleCracker in relation to the Services. You agree that StyleCracker may provide you with notices, including those regarding changes to these Terms, by emailor postings on the Services. You agree that if StyleCracker does not exercise
            or enforce any legal right or remedy which is contained in these Terms(or which StyleCracker has the benefit of under any applicable law), this will not be taken to be a formal waiver of StyleCracker's rights and that those rights or remedies
            will still be available to StyleCracker. If any court of law, having the jurisdiction to decide on this matter, rules that any provision of these Terms is invalid, then that provision will be removed from the Terms without affecting the rest
            of the Terms. The remaining provisions of the Terms will continue to be valid and enforceable.</p>
        </div>

      </div>
    </div>
  </section>
</div>
