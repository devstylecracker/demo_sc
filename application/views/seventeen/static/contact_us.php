<div class="page-contact">
    <section class="section section-shadow">
      <div class="container">
    <div class="title-section">
                <h1 class="title">Contact Us</h1>
              </div>

  <div class="contact-box">

      <div class="row">
        <div class="col-md-10">
          <h3 class="title-2">We would love to hear from you</h3>
          <!--<form name="frmContact" id="frmContact" method="post" action="">-->
            <div class="row">
              <div class="col-md-6">

                <div class="form-group">
                  <input type="text" class="form-control field-name" name="contact_name" id="contact_name" placeholder="Name" required>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control email-name" name="email_id" id="email_id" placeholder="Email" required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control field-name" name="subject" id="subject" placeholder="Subject" required>
                </div>
                <div class="form-group">
                  <textarea  class="form-control field-name" name="message" id="message" placeholder="Message" required></textarea>
                </div>
                <div class="form-group">
                  <button class="scbtn scbtn-primary" onclick="sc_contactus();"><span>Send Message</span></button>
                </div>
                <div id="contact_us_msg" class="form-group success message"></div>
              </div>
			<div class="col-md-1"></div>
              <div class="col-md-5">
                <div class="contact-info">

                  <ul class="contact-info-list">
                    <li>
                     <i class="fa fa-map-marker ic"></i>
                    Impression House,<br/> First  Floor, 42A, G. D. Ambekar Marg, Wadala,<br/> Mumbai - 400031, Maharashtra, India.
                      </li>
                      <li>
                      <i class="fa fa-phone ic"></i>
                      Phone: <span onclick="window.open('tel:+918048130591');">+91 8048130591</span>
                    </li>
                    <li>
                      <i class="fa fa-envelope ic"></i>
                      <div class="title-3">
                        Send us an Email
                       </div>

                      <p>
                        <strong>For business or seller enquiries:</strong> <a href="mailto:business@stylecracker.com">business@stylecracker.com</a>
                      </p>

                      <p>
                        <strong>For order related enquiries:</strong> <a href="mailto:customercare@stylecracker.com">customercare@stylecracker.com</a>
                      </p>
                      <p>
                        <strong>For career opportunities:</strong> <a href="mailto:careers@stylecracker.com">careers@stylecracker.com</a>
                      </p>
                      <p>
                        <strong>For general enquiries:</strong> <a href="mailto:support@stylecracker.com">support@stylecracker.com</a>
                      </p>
                    </li>
                  </ul>


                </div>
              </div>
            </div>
          <!--</form>-->
        </div>

      </div>
    </div>
  </div>
</section>
  </div>

  <script type="text/Javascript">

  function sc_contactus(){

    var contact_name = $('#contact_name').val();
    var email_id = $('#email_id').val();
    var subject = $('#subject').val();
    var message = $('#message').val();

	if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email_id))){
		var msg = 'Please enter valid email ID';
		$('#common-message-text').html(msg);
		$("#common-modal-alert").modal('show');
	}else if(contact_name!='' && email_id!='' && subject!='' && message!=''){

      $.ajax({
        url: '<?php echo base_url(); ?>schome_new/save_contact_us_data',
        type: 'post',
        data: {'type' : 'contact_us','contact_name':contact_name,'email_id':email_id, 'subject':subject,'message':message },
        success: function(data, status) {

          $('#contact_us_msg').html('Thank you, we will get back to you shortly.');
          $('#contact_name').val('');
          $('#email_id').val('');
          $('#subject').val('');
          $('#message').val('');

        },
        error: function(xhr, desc, err) {
          // console.log(err);
        }
      });

    }else {
		var msg = 'please fill all details';
		$('#common-message-text').html(msg);
		$("#common-modal-alert").modal('show');
	}

  }
  </script>
