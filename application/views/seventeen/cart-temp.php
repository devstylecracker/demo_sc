<section class="section section-shadow page-cart">
  <div class="container">
    <div class="title-section">
      <h1 class="title">Your Cart</h1>
    </div>
    <!--test-->
    <div class="page-cart">
      <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12">
          <div class="cart-items-wrp">
            <div class="item">
              <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="item-img-wrp">
                    <img src="<?php echo base_url(); ?>assets/images/demo/p.jpeg" alt="">
                  </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-8">
                  <div class="item-desc-wrp">
                    <div class="title">Geometric Print Bodycon</div>
                    <div class="brand">By <a href="#">The Label Life</a></div>

                    <div class="item-size-qty-wrp">
                      <div class="size-wrp">
                        <label>Size:</label> Medium
                      </div>
                      <div class="qty-wrp">
                        <label>Quantity:</label>
                        <span class="btn-diamond btn-plus"><i class="fa fa-minus"></i></span>
                        <span class="qty-value">1</span>
                        <span class="btn-diamond btn-minus"><i class="fa fa-plus"></i></span>
                       </div>
                    </div>
                    <div class="item-price-wrp">
                      <div class="price"><i class="fa fa-inr"></i> 9862</div>
                    </div>
                    <div class="message"> COD is available on this product</div>
                    <a href="#" class="return-policy"> view return policy</a>
                    <span class="ic ic-close"><i class="fa1 fa-close1"></i>+</span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="cart-items-wrp">
            <div class="item">
              <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="item-img-wrp">
                    <img src="<?php echo base_url(); ?>assets/images/demo/p.jpeg" alt="">
                  </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-8">
                  <div class="item-desc-wrp">
                    <div class="title">Geometric Print Bodycon</div>
                    <div class="brand">By <a href="#">The Label Life</a></div>

                    <div class="item-size-qty-wrp">
                      <div class="size-wrp">
                        <label>Size:</label> Medium</div>
                      <div class="qty-wrp">
                        <label>Quantity:</label>
                        <span class="btn-diamond btn-plus"><i class="fa fa-minus"></i></span>
                        <span class="qty-value">1</span>
                        <span class="btn-diamond btn-minus"><i class="fa fa-plus"></i></span> </div>
                    </div>
                    <div class="item-price-wrp">
                      <div class="price"><i class="fa fa-inr"></i> 9862</div>
                    </div>
                    <div class="message"> COD is available on this product</div>
                    <a href="#" class="return-policy"> view return policy</a>
                    <span class="ic ic-close"><i class="fa1 fa-close1"></i>+</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
  </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="fixed-content111">
                  <div class="theiaStickySidebar111">
                <div class="box-cart-summary">
                  <div class="box-body">
              <div class="copoun-wrp">
                  <div class="form-group">
                <input class="form-control" type="text" placeholder="Apply Discount Coupon" />
              </div>
              <button class="scbtn"><span>Apply Coupon</span></button>
                </div>

              <div class="pincode-wrp">
                <input class="form-control" type="text" placeholder="Enter Pincode" />
              </div>
              <!--  <div class="payment-methods-wrp">
              <ul>
                <li>
                  <label class="method payment-online active">
                    <input class="radio" type="radio"> <span>Online Payment</span>
                  </label>
                </li>
                <li>
                  <label class="method payment-cod">
                    <input class="radio" type="radio"> <span>COD</span>
                  </label>
                </li>
              </ul>
            </div>-->
                  <div class="title">Bill Details</div>
                  <ul class="inner">
<li>
<span class="text-label">Your Total</span>
<span class="value"> <span class="fa fa-inr"></span> 4,300.00</span>
</li>
<li>
<span class="text-label">Coupon Discount</span>
<span class="value"> <span class="fa fa-inr"></span> 0.00 </span>
</li>
<li>
<span class="text-label">VAT</span>
<span class="value"> <span class="fa fa-inr"></span> 4,300.00</span>
</li>
<li>
<span class="text-label">Referral Amount</span>
<span class="value"> <span class="fa fa-inr"></span> 4,300.00</span>
</li>
<li>
<span class="text-label">Delivery Charges</span>
<span class="value"> <span class="fa fa-inr"></span> 4,300.00</span>
</li>
<li>
<span class="text-label text-right">Total:</span>
<span class="value sc-color"> <span class="fa fa-inr"></span> 4,300.00</span>
</li>
</ul>
</div>
<div class="footer">
      <button class="scbtn"><span>Checkout</span></button>
</div>

              </div>
            </div>
              </div>
              </div>
      </div>
    </div>
  </div>

</section>
