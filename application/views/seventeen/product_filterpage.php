<?php
$filterlist_active = '';
?>
  <div class="page-collection">
<section class="section section-shadow">
  <div class="container">
	<div id="filter_product">
    <div class="title-section">
	<h1 class="title"><?php echo @$cat_name[0]['category_name']; ?></h1>

  <!-- search result title -->
  <?php if(@$selected_search != ''){ ?><h1 class="title search-terms">Search Results</h1><?php } ?>
  <!-- //.search result title -->

	  <!-- //filterbox  filter sort by -->
		<div class="dropdown category-filter-wrp ic-sortby ic-right sortby" >
			<span class="dropdown-toggle" data-toggle="dropdown">
			     <i class="icon icon-sort"></i>
			</span>
			<div class="dropdown-menu">
			  <div class="box-filter" id="sortby">
				<div class="title">
				  Sort By
				</div>
          <div class="box-filter-content box-sortby-content">
    				<ul>
    				 <?php $sort=1; foreach($filter['sort_by'] as $key=>$val){ ?><li><label class="sc-checkbox <?php if($sort==1){ echo "active"; }$sort++; ?>" attr-value="<?php echo $key; ?>"><i class="icon icon-diamond"></i> <input type="checkbox" /> <span><?php echo $val; ?></span></label> </li>
    				 <?php } ?>
    				</ul>
        </div>
				    <div class="btns-wrp">
						        <button class="scbtn scbtn-primary" onclick="filter_products();"><span>Apply</span></button>
			       </div>
        	</div>
         </div>
        </div>
      	  <!-- ///.filterbox  filter sort by -->

     <!-- //filterbox  filter filterby -->
		<div class="dropdown category-filter-wrp ic-filter ic-right filterby">
			<span class="dropdown-toggle" data-toggle="dropdown">
			     <i class="icon icon-filter"></i>
			</span>
			<div class="dropdown-menu">
			  <div class="box-filter">
				<div class="title">
				  Filter By
				  <a class="btn-clear" href="javascript:void(0)" onclick="clear_filter('product_filter')">Clear</a>
				</div>
          <div class="box-filter-content box-filterby-content">
          <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4">
    				<ul class="nav nav-tabs">
    					<?php if(count($filter['category'])>1){ ?><li class="active"><a data-toggle="tab" href="#content-categories">Categories</a></li><?php }//check category list ?>
    					<?php if(!empty($filter['paid_brands'])){ ?><li class="<?php if(count($filter['category'])==0 || count($filter['category'])==1){ echo "active"; }?>"><a data-toggle="tab" href="#content-brands">Brands</a></li><?php }//check brand list ?>
    					<li class="<?php if((count($filter['category'])==0 || count($filter['category'])==1) && empty($filter['paid_brands'])){ echo "active"; }?>"><a data-toggle="tab" href="#content-price">Price</a></li>
    					<li><a data-toggle="tab" href="#content-size">Size</a></li>
    					<li><a data-toggle="tab" href="#content-color">Color</a></li>
    					<?php if(!empty($filter['category_attributes'])){ foreach($filter['category_attributes'] as $val){ ?>
    						<li><a data-toggle="tab" href="#content-<?php echo $val['attribute_slug']?>"><?php echo $val['attribute_name']?></a></li>
    					<?php }//foreach
    					}//if ?>
    				</ul>
      </div>
      <div class="col-md-8 col-sm-8 col-xs-8 tab-content-wrp">
				<div class="tab-content">
					<div id="content-categories" class="tab-pane fade <?php if(count($filter['category'])>1){ echo "in active"; } ?> content-categories scrollbar">
						<ul>
							<?php $i=1;if(!empty($filter['category'])){ foreach($filter['category'] as $value){ if($i!=1){?>
								<li><label class="sc-checkbox" attr-value="<?php echo $value['category_id']; ?>" ><i class="icon icon-diamond"></i><span><?php echo $value['category_name']; ?></span></label> </li>
							<?php }else{ ?>
								<!--<li> <input type="checkbox" />  <span><?php echo $value['category_name']; ?></span></li>-->
							<?php } ?>

							<?php $i++;}//foreach
							}//if price list ?>
						</ul>
					</div>
					<div id="content-brands" class="tab-pane fade <?php if((count($filter['category'])==0 || count($filter['category'])==1) && !empty($filter['paid_brands'])){ echo "in active"; }?> content-brands scrollbar">
						<ul>
							<?php if(!empty($filter['paid_brands'])){ foreach($filter['paid_brands'] as $value){ ?>
							<!--<li attr-value="<?php echo $value['brand_id']; ?>"> <i class="ic icon-correct"></i> <span><?php echo $value['company_name']; ?></span></li>-->
							<li><label class="sc-checkbox price-list" attr-value="<?php echo $value['brand_id']; ?>" ><i class="icon icon-diamond"></i><span><?php echo $value['company_name']; ?></span></label> </li>
							<?php }//foreach
							}//if price list ?>
						</ul>
					</div>
					<!-- price list start-->
					<div id="content-price" class="tab-pane fade <?php if((count($filter['category'])==0 || count($filter['category'])==1) && empty($filter['paid_brands'])){ echo "in active"; }?> content-price scrollbar">
						<ul>
							<?php if(!empty($filter['price_list'])){ foreach($filter['price_list'] as $key=>$value){ ?>

							<li><label class="sc-checkbox price-list" attr-value="<?php echo $key+1; ?>" id="price_list<?php echo $key; ?>"><i class="icon icon-diamond"></i> <input type="checkbox" /> <span><?php echo $value; ?></span></label> </li>

							<?php }//foreach
							}//if price list ?>
						</ul>
					</div>
					<!-- price list end-->

					<!-- size list start-->
					<div id="content-size" class="tab-pane fade content-size scrollbar">
						<ul>
						<?php if(!empty($filter['size'])){ foreach($filter['size'] as $value){ ?>
								<!--<li attr-value="<?php echo $value['id']; ?>" > <i class="ic icon-correct"></i> <span><?php echo $value['size_text']; ?></span></li>-->
								<li><label class="sc-checkbox" attr-value="<?php echo $value['id']; ?>" ><i class="icon icon-diamond"></i><span><?php echo $value['size_text']; ?></span></label> </li>
							<?php }//foreach
							}//if price list ?>
						</ul>
					</div>
					<!-- size list end-->

					<!-- color list start-->
					<div id="content-color" class="tab-pane fade content-color content-category-attr scrollbar">
						<ul>
						<?php if(!empty($filter['colour'])){ foreach($filter['colour'] as $value){ ?>
							<!--<li attr-value="<?php echo $value['id']; ?>"> <i class="ic icon-correct"></i> <span><?php echo $value['attr_value_name']; ?></span></li>-->
							<li><label class="sc-checkbox" attr-value="<?php echo $value['id']; ?>"><i class="icon icon-diamond-s" style="color:<?php echo $value['attr_hashtag']?>;"></i>
              <!--  <span><?php echo $value['attr_value_name']; ?></span>-->
              </label> </li>
							<?php }//foreach
							}//if colour list ?>
						</ul>
					</div>
					<!-- color list end-->

					<!-- category-attr list start-->
					<?php if(!empty($filter['category_attributes'])){ foreach($filter['category_attributes'] as $value){ ?>
						<div id="content-<?php echo $value['attribute_slug']; ?>" class="tab-pane fade content-category-attr content-col">
							<ul>
							<?php foreach($filter[$value['attribute_slug']] as $val){ ?>
								<!-- <li attr-value="<?php echo $val['id']; ?>"> <i class="ic icon-correct"></i> <span><?php echo $val['attr_value_name']; ?></span></li>-->
								<li><label class="sc-checkbox" attr-value="<?php echo $val['id']; ?>" ><i class="icon icon-diamond"></i><span><?php echo $val['attr_value_name']; ?></span></label> </li>
							<?php }//foreach attr?>

							</ul>
						</div>
					<?php	}//foreach
							}//if colour list ?>
					<!-- category-attr list end-->
        </div>
            </div>
           </div>
       </div>
      <div class="btns-wrp">
            <button class="scbtn scbtn-primary" onclick="filter_products();"><span>Apply</span></button>
         </div>
			</div>
    </div>
    </div>
	  <!-- //filterbox  filter filterby -->

    </div>
     <!-- ///. title section -->

      <!-- search result title -->
      <?php if(@$selected_search != ''){ ?><div class="result-count-wrp search-terms">We've got <span class="result-count"> <?php echo $total_row; ?></span> results for <span>'<?php echo @$selected_search; ?>'</span></div><?php } ?>
      <!-- //.search result title -->


  <?php if(@$selected_search == ''){ ?>
    <ol class="breadcrumb">
    <?php echo $category_breadcrumps; ?>
    </ol>


       <!--<div class="result-count-wrp"> <span class="result-count"> <?php echo $total_row; ?></span> results found </div>-->
<?php } ?>


	<div class="grid grid-items products">
	<div class="row" id="get_all_cat_products">
    <!--test-->
	<?php if(!empty($products)){
			foreach($products as $val){  
				$product_name = $val->product_name; 
				$val->product_name = preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_name);
				$val->product_slug = preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_slug);
	?>
			<div class="col-md-3 col-sm-6 col-xs-6 grid-col">
			  <div class="item-wrp">
				<div class="item">
				  <div class="item-img-wrp" onclick="_targetProductClick(<?php echo $val->product_id; ?>,'6','<?php echo @$val->product_name; ?>');">
					<a href="<?php echo base_url().PRODUCT_URL.$val->product_slug.'-'.$val->product_id; ?>"><img src="<?php echo $val->product_img; ?>" alt=""></a>
				  </div>
				  <div class="item-desc-wrp">
					<a href="<?php echo CAT_URL.$val->product_category_slug.'-'.$val->product_category_id; ?>"><div class="category"><?php echo $val->product_category; ?></div></a>
					<div class="title" onclick="_targetProductClick(<?php echo $val->product_id; ?>,'6','<?php echo @$val->product_name; ?>');" ><a href="<?php echo base_url().PRODUCT_URL.$val->product_slug.'-'.$val->product_id; ?>"><?php echo $product_name; ?></a></div>
					<div class="brand">By <a href="<?php echo BRAND_URL.$val->brand_slug.'-'.$val->brand_id; ?>"><?php echo $val->brand_name; ?></a></div>
					<div class="item-price-wrp">
					  <?php if($val->price > $val->discount_price){ ?>
								<div class="mrp"><i class="fa fa-inr"></i><?php echo $val->price; ?></div>
								<div class="price"><i class="fa fa-inr"></i><?php echo $val->discount_price; ?></div>
								<div class="discount"><?php echo $val->discount_percent; ?>% OFF</div>
					  <?php }else { ?>
								<div class="price"><i class="fa fa-inr"></i><?php echo $val->price; ?></div>
					  <?php }?>
					</div>
				  </div>
				  <div class="item-media-wrp">
					<?php $size_ids='';$size_texts='';$size_id= array();$size_text= array();if(!empty($val->product_size)){foreach($val->product_size as $size){
						$size_id[] = $size->size_id;
						$size_text[] = $size->size_text;
						}// foreach
						}//if
						if(!empty($size_id))$size_ids = implode(',',$size_id);
						if(!empty($size_text))$size_texts = implode(',',$size_text);
					?>
					<span class="ic ic-plus" data-toggle="modal"  id="<?php echo @$val->product_id; ?>" value="<?php echo @$val->product_id; ?>" name="prod_size" data-sizes="<?php echo $size_ids; ?>" data-label="<?php echo $size_texts; ?>" data-productname="<?php echo $val->product_name; ?>" data-productprice="<?php echo $val->discount_price; ?>" data-brandname="<?php echo $val->brand_name; ?>" data-category="<?php echo $val->product_category; ?>" onclick="GetProductSize(this);">
					  <i class="icon-plus"></i>
					</span>
					<span class="ic ic-share" onclick="send_message('product','<?php echo @$val->product_id; ?>','<?php echo @$val->product_name; ?>','<?php echo @$val->product_img; ?>','<?php echo base_url().PRODUCT_URL.$val->product_slug.'-'.$val->product_id; ?>',<?php if($user_id > 0){ echo @$user_id; }else { echo '0'; } ?>);">
					  <i class="icon-send"></i>
					</span>
				  </div>
				<span class="ic-heart <?php if(@$val->is_wishlisted == '1') echo "active"; ?>" id="wishlist_product<?php echo @$val->product_id; ?>" onclick="add_to_fav('product',<?php echo @$val->product_id; ?>,<?php if($user_id > 0){ echo @$user_id; }else { echo '0'; } ?>,'<?php echo @$val->product_name; ?>');">
					  <i class="icon-heart"></i>
				</span>
				</div>
			  </div>
			</div>

		<?php }//foreach
		}else{  ?>
      <div class="col-md-12">
         <div class="grid-no-products result-count-wrp"> No Products Found </div>
     </div>
		<?php }  ?>
	</div>
	</div>
	</div>

	<input type="hidden" name="offset" id="offset" value="0">
	<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
	<input type="hidden" name="bucket" id="bucket" value="<?php echo $bucket; ?>">
	<input type="hidden" name="cat_id" id="cat_id" value="<?php echo $cat_id; ?>">
	<input type="hidden" name="total_filter_products" id="total_filter_products" value="<?php echo $total_row; ?>">
	<input type="hidden" name="arrCategorySelected" id="arrCategorySelected" value="">
	<input type="hidden" name="arrSizeSelected" id="arrSizeSelected" value="">
	<input type="hidden" name="arrBrandSelected" id="arrBrandSelected" value="">
	<input type="hidden" name="arrCatAttrSelected" id="arrCatAttrSelected" value="">
	<input type="hidden" name="intPriceSelected" id="intPriceSelected" value="">
	<input type="hidden" name="intSortBy" id="intSortBy" value="">
	<input type="hidden" name="search" id="search" value="<?php echo $selected_search; ?>">

	<div id="load-more" class="fa-loader-item-wrp">
		<div class="fa-loader-item">
		<i class="fa fa-circle-o-notch fa-spin-custom"></i> Loading...
		</div>
	</div>
  </div>
</section>
</div>
<script type="text/Javascript">
	ga('send', 'event', 'ProductCategory', 'clicked', '<?php echo @$cat_name[0]['category_name']; ?>');
</script>