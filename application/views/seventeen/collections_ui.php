<section class="section section-shadow section-collections">
  <div class="container">
    <div class="title-section">
      <h1 class="title">Collections</h1>
      <a href="#" class="link-view-all">View All Collections</a>
    </div>
    <div class="grid-items-slider">
      <div class="row grid-section-items grid-items-slider-inner">
        <div class="col-md-3 col-sm-12 col-xs-12 grid-item">
          <div class="item-view-wrp">
            <div class="item item-hover-mask">
              <img src="<?php echo base_url(); ?>assets/images/demo/1.jpg" alt="">
              <div class="mask">
                <div class="inner">
                  <div class="top">
                    <h2 class="title">The Home Collection</h2>
                    <div class="desc">Cras in diam volutpat metus dapibus ultrices id sed ipsum</div>
                  </div>
                  <div class="bottom">
                    <div class="btns-wrp">
                      <a href="#" class="btn btn-sm button btn-primary">View Collection</a>
                    </div>
                    <div class="desc-2">Recommended collection by <strong>Head Stylist, Priti Chandra</strong></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12 grid-item">
          <div class="item-view-wrp">
            <div class="item item-hover-mask">
              <img src="<?php echo base_url(); ?>assets/images/demo/2.jpg" alt="">
              <div class="mask">
                <div class="inner">
                  <div class="top">
                    <h2 class="title">The Home Collection</h2>
                    <div class="desc">Cras in diam volutpat metus dapibus ultrices id sed ipsum</div>
                  </div>
                  <div class="bottom">
                    <div class="btns-wrp">
                      <a href="#" class="btn btn-sm button btn-primary">View Collection</a>
                    </div>
                    <div class="desc-2">Recommended collection by <strong>Head Stylist, Priti Chandra</strong></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12 grid-item">
          <div class="item-view-wrp">
            <div class="item item-hover-mask">
              <img src="<?php echo base_url(); ?>assets/images/demo/13.jpg" alt="">
              <div class="mask">
                <div class="inner">
                  <div class="top">
                    <h2 class="title">The Home Collection</h2>
                    <div class="desc">Cras in diam volutpat metus dapibus ultrices id sed ipsum</div>
                  </div>
                  <div class="bottom">
                    <div class="btns-wrp">
                      <a href="#" class="btn btn-sm button btn-primary">View Collection</a>
                    </div>
                    <div class="desc-2">Recommended collection by <strong>Head Stylist, Priti Chandra</strong></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12 grid-item">
          <div class="item-view-wrp">
            <div class="item item-hover-mask">
              <img src="<?php echo base_url(); ?>assets/images/demo/12.jpg" alt="">
              <div class="mask">
                <div class="inner">
                  <div class="top">
                    <h2 class="title">The Home Collection</h2>
                    <div class="desc">Cras in diam volutpat metus dapibus ultrices id sed ipsum</div>
                  </div>
                  <div class="bottom">
                    <div class="btns-wrp">
                      <a href="#" class="btn btn-sm button btn-primary">View Collection</a>
                    </div>
                    <div class="desc-2">Recommended collection by <strong>Head Stylist, Priti Chandra</strong></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12 grid-item">
          <div class="item-view-wrp">
            <div class="item item-hover-mask">
              <img src="<?php echo base_url(); ?>assets/images/demo/2.jpg" alt="">
              <div class="mask">
                <div class="inner">
                  <div class="top">
                    <h2 class="title">The Home Collection</h2>
                    <div class="desc">Cras in diam volutpat metus dapibus ultrices id sed ipsum</div>
                  </div>
                  <div class="bottom">
                    <div class="btns-wrp">
                      <a href="#" class="btn btn-sm button btn-primary">View Collection</a>
                    </div>
                    <div class="desc-2">Recommended collection by <strong>Head Stylist, Priti Chandra</strong></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12 grid-item">
          <div class="item-view-wrp">
            <div class="item item-hover-mask">
              <img src="<?php echo base_url(); ?>assets/images/demo/13.jpg" alt="">
              <div class="mask">
                <div class="inner">
                  <div class="top">
                    <h2 class="title">The Home Collection</h2>
                    <div class="desc">Cras in diam volutpat metus dapibus ultrices id sed ipsum</div>
                  </div>
                  <div class="bottom">
                    <div class="btns-wrp">
                      <a href="#" class="btn btn-sm button btn-primary">View Collection</a>
                    </div>
                    <div class="desc-2">Recommended collection by <strong>Head Stylist, Priti Chandra</strong></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12 grid-item">
          <div class="item-view-wrp">
            <div class="item item-hover-mask">
              <img src="<?php echo base_url(); ?>assets/images/demo/2.jpg" alt="">
              <div class="mask">
                <div class="inner">
                  <div class="top">
                    <h2 class="title">The Home Collection</h2>
                    <div class="desc">Cras in diam volutpat metus dapibus ultrices id sed ipsum</div>
                  </div>
                  <div class="bottom">
                    <div class="btns-wrp">
                      <a href="#" class="btn btn-sm button btn-primary">View Collection</a>
                    </div>
                    <div class="desc-2">Recommended collection by <strong>Head Stylist, Priti Chandra</strong></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12 grid-item">
          <div class="item-view-wrp">
            <div class="item item-hover-mask">
              <img src="<?php echo base_url(); ?>assets/images/demo/13.jpg" alt="">
              <div class="mask">
                <div class="inner">
                  <div class="top">
                    <h2 class="title">The Home Collection</h2>
                    <div class="desc">Cras in diam volutpat metus dapibus ultrices id sed ipsum</div>
                  </div>
                  <div class="bottom">
                    <div class="btns-wrp">
                      <a href="#" class="btn btn-sm button btn-primary">View Collection</a>
                    </div>
                    <div class="desc-2">Recommended collection by <strong>Head Stylist, Priti Chandra</strong></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
