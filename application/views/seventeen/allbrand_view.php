<section class="section section-shadow section-collections">
  <div class="container">
    <div class="title-section">
      <h1 class="title">Brands</h1>
    </div>

    <div class="grid grid-items collections thumb-collection">
      <div class="row grid-row" id="get_all_brands">
		  <?php
		  if(!empty($top_brands)){ foreach($top_brands as $val){?>
			<div class="col-md-6 col-sm-12 col-xs-12 grid-col">
              <div class="item-wrp overlay111">
                <div class="item">
                  <div class="item-img-wrp" style="border:1px solid #f5f5f5;">
                    <a href="<?php echo BRAND_URL.$val['brand_slug'].'-'.$val['brand_id']; ?>"><img src="<?php echo @$val['home_page_promotion_img']; ?>" alt=""></a>
                  </div>
				 <!-- <span class="ic-share">
				      <i class="icon-send"></i>
				   </span>-->
                </div>
              </div>
            </div>
		<?php }//foreach
			}//if	?>



      </div>
		<input type="hidden" name="offset" id="offset" value="1">
		<input type="hidden" name="total_brands" id="total_brands" value="<?php echo $total_brands; ?>">

		<div id="load-more" class="fa-loader-item-wrp">
			<div class="fa-loader-item">
			<i class="fa fa-circle-o-notch fa-spin-custom"></i> Loading...
			</div>
		</div>
    </div>
  </div>
</section>
