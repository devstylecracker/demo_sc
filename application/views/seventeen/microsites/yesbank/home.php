<link href='<?php echo base_url(); ?>assets/seventeen/css/yesbank.css' rel='stylesheet' type='text/css'>
<div class="site-yesbank page-yesbank">
  <section class="section section-shadow" style="padding:20px 0 0 0">
    <div class="container">
      <div class="banner-wrp img-wrp">
        <img src="<?php echo base_url(); ?>/assets/seventeen/images/yesbank/sc-yes-bank-banner.jpg" alt="">
      </div>
    </div>

    <div class="packages-wrp">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="box">
              <div class="title"><span>SCBOX</span></div>
              <div class="desc">
                <p>Save yourself a trip to the store.</p>
                <p>
                  Get the best in fashion delivered right to your doorstep, handpicked by celebrity stylists to suit your style and budget.
                </p>
              </div>
              <div class="offer">
                <p>Flat <i class="fa fa-inr"></i>500 off on boxes worth <i class="fa fa-inr"></i>2,999</p>
                <p>Flat <i class="fa fa-inr"></i>750 off on boxes worth <i class="fa fa-inr"></i>4,999</p>
                <p>Flat <i class="fa fa-inr"></i>1,000 off on boxes worth <i class="fa fa-inr"></i>6,999</p>
              </div>
              <div class="btns-wrp">
                <a class="scbtn scbtn-primary" href="<?php echo base_url(); ?>yesbank/scbox" target="_blank"><span>Know More</span></a>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="box">
              <div class="title"><span>STYLIST</span></div>
              <div class="desc">
                <p>Never have a fashion emergency!</p>
                <p>
                  We manage clothes, closets and clients.</p>
                  <p>
                    Get your own celebrity stylist to curate the perfect wardrobe for you.
                  </p>
              </div>
              <div class="offer">
                <p>Video Session <i class="fa fa-inr"></i>1,499
                </p>
              </div>
              <a class="scbtn scbtn-primary" href="<?php echo base_url(); ?>yesbank/personal-stylists" target="_blank"><span>Know More</span></a>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="box">
              <div class="title"><span>SHOP</span></div>
              <div class="desc">
                <p>The best money you will ever spend.</p>
                <p>Keep up with the latest trends and shop the coolest brands curated especially by celebrity stylists.</p>
              </div>
              <div class="offer">
                <p>Flat <i class="fa fa-inr"></i>300 off on orders worth <i class="fa fa-inr"></i>1,500 and above</p>
                <p class="sc-color">USE CODE: YES300</p>
              </div>
              <a class="scbtn scbtn-primary" href="<?php echo base_url(); ?>" target="_blank"><span>Shop Now</span></a>
            </div>
          </div>
        </div>

        <div class="text-block">
        <p class="sc-color" style="font-size:16px;">
          All payments must be made via YES BANK Debit/Credit Cards
        </p>
          <a href="<?php echo base_url(); ?>yesbank/terms-and-conditions" target="_blank">Terms & Conditions</a> |
          <a href="<?php echo base_url(); ?>contact-us" target="_blank">Contact Us</a>
        </div>
      </div>

    </div>
  </section>
</div>
