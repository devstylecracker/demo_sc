<div class="page-policies page-static">
  <section class="section section-shadow">
    <div class="container">
      <div class="title-section">
        <h1 class="title">Terms & Conditions</h1>
      </div>

      <div class="content-wrp">
        <ul>
          <li>
            All the offers are exclusive to YES BANK Debit/Credit card customers.
          </li>
          <li>
            All payments must be made via a YES BANK Debit/Credit card only.
          </li>
          <li>
            Each discount code can be used only once per transaction.
          </li>
          <li>
            Each offer is independent and cannot be combined with any other offer or discount code.
          </li>
          <li>
            All offers are exclusive of taxes.
          </li>
        </ul>



        <h5> SC Box</h5>
        <ul>
          <li>
            Go through the regular procedure of selecting a StyleCracker Box of your choice.
          </li>
          <li>Once you’ve selected your most preferred SC Box package, click on Register. </li>
          <li>Answer a quick questionnaire asking about your personal style and preferences. </li>
          <li>Once you’ve filled out the details, click on Place Order. </li>
          <li>To avail offer, check Avail Offers and select an offer according to the package you have opted for
            <br /> INR 500 off on SC Box worth INR 2,999
            <br /> INR 750 off on SC Box worth INR 4,999
            <br /> INR 1,000 off on SC Box worth INR 6,999
          </li>
          <li>Proceed to the payments page. </li>
          <li>Enter your YES BANK Credit/Debit card details in order to make the final payment.
          </li>
        </ul>

        <h5>Video Call</h5>
        <ul>
          <li>
            Select Video Call on the homepage and click on Schedule under the Stylist section.</li>
          <li>Select a time slot from the available slots for your video call.</li>
          <li>Once you’ve selected your slot, you will receive an email with the details of the booking.</li>
          <li>A booking for a video call will be taken 48 hours in advance.</li>
          <li>Expect a call from our stylists within the next working day, following which you will receive an email with a payment link.</li>
          <li>Click on the above mentioned link which will redirect you to the payment gateway.</li>
          <li>The payment must be made at least 24 hours prior to the booking slot.</li>
          <li>Enter your YES BANK Credit/Debit card details in order to make the final payment.</li>
          <li>Failure to attend the video call at the scheduled time will not result in any form of cashback or refund.
          </li>
        </ul>
      </div>
    </div>
  </section>
</div>

<style>
  .page-policies h5 {
    margin: 20px 0 5px;
    font-weight: 700;
  }

  .page-policies ul {
    list-style: outside;
    ;
    margin: 0 0 20px 20px;
    padding: 0
  }

  .page-policies ul li {
    padding: 0;
    line-height: 1.7;
  }
</style>
