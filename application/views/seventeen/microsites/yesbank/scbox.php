<link href='<?php echo base_url(); ?>assets/seventeen/css/yesbank.css' rel='stylesheet' type='text/css'>
<div class="site-yesbank page-yesbank-scbox">
  <section class="section section-shadow">
    <div class="container">
      <div class="row section-intro">
        <div class="col-md-4 col-sm-12 col-xs-12">
          <div class="img-wrp">
            <img src="<?php echo base_url(); ?>/assets/seventeen/images/yesbank/scbox.jpg" alt="">
          </div>
        </div>
        <div class="col-md-8 col-sm-12 col-xs-12">
          <div class="title-section">
            <h1 class="title">SCBox</h1>
          </div>
          <div class="desc">
            <p>
              <strong>The solution to all your shopping woes - </strong> </p>
            <p><strong>StyleCracker's</strong> celebrity stylists find the
              <span>best clothes and accessories that are
    ideal for you, without you having to go
    shopping. Highly curated set of products
    selected for you on the basis of your
    budget, personal style, preferences,
    body type and much more.<span>
  </p>
        </div>

      </div>
    </div>
  </div>
</section>
    <section class="section section-shadow">
      <div class="container">

    <div class="packages-wrp">
      <div class="title-section text-center" style="margin-bottom:20px; ">
          <h2 class="subtitle">Women</h2>
      </div>
      <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="box">
            <div class="title"><span>Package 1</span></div>
          <div class="desc">
            <p>Flat <i class="fa fa-inr"></i>500 off on boxes worth <i class="fa fa-inr"></i>2,999</p>
            <p>
				Accessories eg. Belt, Scarf <br/>
				Eyewear <br/>
				Footwear <br/>
				Jewellery <br/>
				(Choose any 4 items)

            </p>
          </div>
          <div class="price"><i class="fa fa-inr"></i>2,999</div>
          <div class="btns-wrp">
            <a class="scbtn scbtn-primary" href="<?php echo base_url(); ?>book-scbox/1?step=1&f=yb&g=1" onclick="setdata();"><span>Get Your Box</span></a>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="box">
          <div class="title"><span>Package 2</span></div>
          <div class="desc">
            <p>Flat <i class="fa fa-inr"></i>750 off on boxes worth <i class="fa fa-inr"></i>4,999</p>
            <p>
				Apparel<br/>
				Bag OR Footwear<br/>
				Beauty Product<br/>
				Jewellery<br/>
				&nbsp;
            </p>
          </div>
          <div class="price"><i class="fa fa-inr"></i>4,999</div>
          <div class="btns-wrp">
            <a class="scbtn scbtn-primary" href="<?php echo base_url(); ?>book-scbox/2?step=1&f=yb&g=1" onclick="setdata();"><span>Get Your Box</span></a>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="box">
          <div class="title"><span>Package 3</span></div>
          <div class="desc">
            <p>Flat <i class="fa fa-inr"></i>1,000 off on boxes worth <i class="fa fa-inr"></i>6,999</p>
            <p>
				Apparel<br/>
				Bag <br/>
				Beauty Product <br/>
				Footwear <br/>
				Jewellery <br/>
            </p>
          </div>
          <div class="price"><i class="fa fa-inr"></i>6,999</div>
          <div class="btns-wrp">
            <a class="scbtn scbtn-primary" href="<?php echo base_url(); ?>book-scbox/3?step=1&f=yb&g=1" onclick="setdata();"><span>Get Your Box</span></a>
          </div>
        </div>
      </div>
    </div>
</div>
</div>
</section>
<section class="section section-shadow">
  <div class="container">
    <div class="packages-wrp">
      <div class="title-section text-center" style="margin-bottom:20px; ">
        <h2 class="subtitle">Men</h2>
      </div>

      <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="box">
            <div class="title"><span>Package 1</span></div>
            <div class="desc">
              <p>Flat <i class="fa fa-inr"></i>500 off on boxes worth <i class="fa fa-inr"></i>2,999</p>
              <p>
                Accessories eg. Belt, Wallet, Jewels, Pocket square, etc.<br/>
				Eyewear<br/>Grooming Products <br/><br/>
				(Choose any 4 items)
              </p>
            </div>
            <div class="price"><i class="fa fa-inr"></i>2,999</div>
            <div class="btns-wrp">
              <a class="scbtn scbtn-primary" href="<?php echo base_url(); ?>book-scbox/1?step=1&f=yb&g=2" onclick="setdata();"><span>Get Your Box</span></a>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="box">
            <div class="title"><span>Package 2</span></div>
            <div class="desc">
              <p>Flat <i class="fa fa-inr"></i>750 off on boxes worth <i class="fa fa-inr"></i>4,999</p>
              <p>
				Accessories eg. Belt, Wallet, Jewels, Pocket square, etc.<br/>
				Eyewear <br/>
				Grooming Products <br/><br/>

				(Choose any 4 items)

              </p>
            </div>
            <div class="price"><i class="fa fa-inr"></i>4,999</div>
            <div class="btns-wrp">
              <a class="scbtn scbtn-primary" href="<?php echo base_url(); ?>book-scbox/2?step=1&f=yb&g=2" onclick="setdata();"><span>Get Your Box</span></a>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="box">
            <div class="title"><span>Package 3</span></div>
            <div class="desc">
              <p>Flat <i class="fa fa-inr"></i>1,000 off on boxes worth <i class="fa fa-inr"></i>6,999</p>
              <p>
                Apparel <br/>
				Accessories eg. Belt, Wallet, Jewels, Pocket square, etc. <br/>
				Bag <br/>
				Footwear <br/>
				Grooming Products <br/>
              </p>
            </div>
            <div class="price"><i class="fa fa-inr"></i>6,999</div>
            <div class="btns-wrp">
              <a class="scbtn scbtn-primary" href="<?php echo base_url(); ?>book-scbox/3?step=1&f=yb&g=2" onclick="setdata();"><span>Get Your Box</span></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="text-block">
      <p class="sc-color" style="font-size:16px;">
        Exclusive offers for YES BANK Debit Card & Credit Card Customers
    <br />
        All payments must be made via YES BANK Debit/Credit Cards
      </p>
      <a href="<?php echo base_url(); ?>yesbank/terms-and-conditions" target="_blank">Terms & Conditions</a> |
      <a href="<?php echo base_url(); ?>contact-us" target="_blank">Contact Us</a>
    </div>
  </div>
</section>
<section class="section section-shadow">
  <div class="container">

    <div class="btns-wrp text-center ">
      <div>
        Call + 91 7045135438 / 022-40007777 to order your box today.
      </div>

    </div>

  </div>
</section>
</div>

<script type="text/javascript">
  function setdata()
  {
    document.cookie = "scfr=yb;path=/";
  }
</script>
