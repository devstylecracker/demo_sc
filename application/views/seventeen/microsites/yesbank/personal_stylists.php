<link href='<?php echo base_url(); ?>assets/seventeen/css/yesbank.css' rel='stylesheet' type='text/css'>
<div class="site-yesbank page-yesbank-styling">
  <section class="section section-shadow">
    <div class="container">
      <div class="row section-intro">
        <div class="col-md-4 col-sm-12 col-xs-12">
          <div class="img-wrp">
            <img src="<?php echo base_url(); ?>/assets/seventeen/images/yesbank/personal-stylists.jpg" alt="">
          </div>
        </div>
        <div class="col-md-8 col-sm-12 col-xs-12">
          <div class="title-section">
            <h1 class="title">Personal Stylists</h1>
          </div>
          <div class="desc">
            <p>
              <strong>Talk your wardrobe woes away in just 30 minutes.</strong>
            </p>
            <p>
              Trust our stylists to be your personal wardrobe guides. From walking them through your closet and getting styling tips, to outfit run-throughs and instant makeovers – make the most of your virtual wardrobe revamp with your celebrity stylist.
            </p>
          </div>

        </div>
      </div>
          </div>
        </section>
      <section class="section section-shadow">
        <div class="container">
      <div class="packages-wrp">
        <div class="row">
          <div class="col-md-6 col-sm-4 col-xs-12 col-md-offset-3">
            <div class="box">
              <div class="title"><span>Video call</span></div>
              <div class="desc">
                Talk your wardrobe
                <br /> woes away in just 30 minutes
              </div>
              <div class="price">Video session <i class="fa fa-inr"></i>1,499</div>
              <div class="btns-wrp">
                <a class="scbtn scbtn-primary" href="https://www.timify.com/in/profile/stylecracker"><span>Schedule</span></a>
              </div>
            </div>
          </div>

        </div>
      </div>

      <div class="text-block">
        <p class="sc-color" style="font-size:16px;">
          Exclusive Offer for YES BANK Debit Card & Credit Card Customers
      <br />
          All payments must be made via YES BANK Debit/Credit Cards
        </p>
        <a href="<?php echo base_url(); ?>yesbank/terms-and-conditions" target="_blank">Terms & Conditions</a> |
        <a href="<?php echo base_url(); ?>contact-us" target="_blank">Contact Us</a>
      </div>

  </section>
  </div>
