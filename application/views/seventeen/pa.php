<?php
//echo "<pre>";print_r($style);

//echo '<pre>';print_r($this->session->userdata());
//echo $confirm_mobile;
$gender = $this->session->userdata('gender');
$user_id = $this->session->userdata('user_id');
$user_info = $this->User_info_model->get_users_info_mobile($user_id);
$ga_gender = '';
// echo "<pre>";print_r($user_info);
$default_profile_img = $user_info->profile_pic;
if($gender == 1 || $gender == 'women'){
	 $default_profile_img = base_url()."assets/seventeen/images/pa/female/profile-default-female.png";
	$body_shape_class = 'female';
  $ga_gender = 'PA-Women';
}else {
	 $default_profile_img = base_url()."assets/seventeen/images/pa/male/profile-default-male.png";
	$body_shape_class = 'male';
   $ga_gender = 'PA-Men';
}
if(@$this->uri->segment(2)!='')
{
  $slide = $this->uri->segment(2);
}else
{
  $slide = '';
}

$user_bodyshape = '';
$user_style = '';
$user_brand = '';
$user_brandname = '';
$user_profilepic = '';
if($slide!='')
{

  if($slide==2)
  {
    $user_profilepic=2;
  }else if($slide==3)
  {
     $user_bodyshape = $user_pa_data->user_body_shape['id'];
   }else if($slide==4)
   {
      $user_style = $user_pa_data->user_style_p1['id'].','.$user_pa_data->user_style_p2['id'].','.$user_pa_data->user_style_p3['id'];
   }else if($slide==5)
   {
      foreach($user_pa_data->user_brand_list as $val)
      {	
	  // echo "<pre>";print_r($user_pa_data->user_brand_list);exit;
        if(empty($user_brand) & $val->brand_name !='')
        {
           $user_brand = $val->brand_key;
           $user_brandname = $val->brand_name;
        }else
        {
          if($val->brand_name!='')
          {
              $user_brand =  $user_brand.','.$val->brand_key;
              $user_brandname =  $user_brandname.','.$val->brand_name;
          }
        }
      }
    }
}

if($registered_from=='google' || $registered_from=='facebook')
{
  $confirm_mobile=1;
}

if($slide==10)
{
  $confirm_mobile = 0;
}

//echo '$user_brand--<pre>';print_r($user_pa_data->user_brand_list);
?>
<div class="page-pa">
  <div id="slide1" class="pa-slide-box verify <?php echo (@$confirm_mobile == 1 || ($slide==2 || $slide==3 || $slide==4 || $slide==5 )) ? 'hide':''; ?>">
    <section class="section section-shadow">
      <div class="container">
        <!-- pa verify mobile -->
        <div class="title-section">
          <h1 class="title">Verify Your Account</h1>
        </div>
        <div class="row">
          <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
            <div class="box-pa">
              <p>A verification code has been sent to
                <br /> <?php echo @$mobileno; ?>.</p>
              <p class="text-bold">Kindly enter it below</p>
              <div class="form-group">
                <input type="text" id="otp" class="form-control input-control-mobile error222 success222" placeholder="Code"  maxlength="4" minlength="4">
                <div class="message text-error hide" id="otp-error"></div>
              </div>
              <div class="text-center">
                <button id="checkOtp"  onclick="generateOtp(<?php echo @$mobileno; ?>,'check_otp',<?php echo $this->session->userdata('user_id'); ?>);" class="scbtn scbtn-primary"><span>Verify</span></button>
              </div>

            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section section-shadow">
      <div class="container">
        <div class="text-center">
          <div class="mobile-verify-message hide">
            <p class="text-success">
              <i class="fa fa-check"></i>
            </p>
            <p class="text-danger">
              <i class="fa fa-close"></i>
              <br /> The number you entered was incorrect.
            </p>
            <p>
              Please re-enter the correct OTP
            </p>
          </div>
          <button  onclick="generateOtp(<?php echo @$mobileno; ?>,'resend_otp');" class="scbtn scbtn-tertiary"><span>Resend OTP</span></button>
        </div>
      </div>
    </section>
  </div>
  <!-- /. pa verify mobile -->
  <!-- pa upload photo -->
  <div id="slide2" class="pa-slide-box photo hide">
    <section class="section section-shadow">
      <div class="container">
        <div class="title-section">
          <h1 class="title">Upload Photo</h1>
        </div>
        <div class="box-pa">
          <p>Upload your most recent picture so that our stylists can help you find fashion that are just for you.</p>
          <!--<p class="text-bold">Note: Upload a full body picture so that we can better help you find clothes.</p>-->
        </div>
      </div>
    </section>
    <section class="section section-shadow">
      <div class="container">
       <form method="post" action="" name="upload_file" id="upload_file" enctype="multipart/form-data">
        <!--  cropper -->
        <div id="image-cropper" class="pa-photo-wrp loading222">
          <div class="cropit-preview" id="cropit-preview">
            <div id="default-img" class="cropit-preview-image-container">
              <img src="<?php echo $default_profile_img; ?>" class="cropit-preview-image default-img"/>
            </div>
          </div>
              <div class="cropit-range-wrp">
              	<i class="fa fa-picture-o ic-less"></i>
                <input type="range" class="cropit-image-zoom-input" />
              	<i class="fa fa-picture-o ic-more"></i>
              </div>

                <label class="file-upload ">
                  <input type="file" class="file-input cropit-image-input">
                 <img class="" id="add_btn" src="<?php echo base_url() ?>assets/seventeen/images/btns/ic-add.svg" />
                 <img class="hide" id="edit_btn" src="<?php echo base_url() ?>assets/seventeen/images/btns/ic-edit.svg" />

                </label>
                <div class="btns-wrp text-center">
                  <a  class="scbtn scbtn-tertiary btn-save" onclick="save_img();"><span>Crop</span></a>
                </div>
          </div>
          <!-- //. cropper -->
      </form>
      </div>
      </section>
      <section class="section section-shadow">
        <div class="container scbtns-wrp">
          <?php if(empty($user_profilepic))
              {
          ?>
    		    <a class="scbtn scbtn-tertiary" id="skip_img" href="javascript:void(0);"><span>Skip</span></a>
            <a class="scbtn scbtn-primary" id="save_img" href="<?php echo base_url(); ?>"><span>Next</span></a>
          <?php }else
            {
          ?>
            <a class="scbtn scbtn-primary" onclick="updatepa('profilepic')" href="javascript:void(0);"><span>Save</span></a>
          <?php
                }
          ?>
        </div>
      </section>
  </div>
  <!-- /. pa upload photo -->

  <!-- pa take the quiz -->
  <!-- <div  id="slide3" class="pa-slide-box quiz hide">
    <section class="section section-shadow section-take-quiz">
      <div class="container">
        <div class="title-section">
          <h1 class="title">Take the Quiz</h1>
        </div>
        <div class="box-pa">
          <h1 class="title-big"> Help us<br />style<br /> you better</h1>
          <p>Tell us more about yourself, so that we can style you in a manner that suits you best.</p>
        </div>
      </div>
    </section>
    <section class="section section-shadow">
      <div class="container">
        <div class="scbtns-wrp">
    	      <a class="scbtn scbtn-tertiary" id="skip_pa" href="<?php echo base_url(); ?>schome_new" onclick="ga('send', 'event', '<?php echo $ga_gender; ?>', 'Skipped');"><span>Skip</span></a>
            <a class="scbtn scbtn-primary" id="takeQuiz" href="#" ><span>Take the Quiz</span></a>
        </div>
      </div>
    </section>
  </div> -->
  <!-- /. pa take the quiz -->

  <!-- pa take the body shape --><?php //echo (@$confirm_mobile == 1) ? '':'hide'; ?>
<!--   <div  id="slide4" class="pa-slide-box body-shape <?php echo (@$confirm_mobile != 1 || $slide==2 ||  $slide==4 || $slide==5) ? 'hide':''; ?> " user-body-attr="<?php echo $user_bodyshape; ?>" >
    <section class="section section-shadow">
      <div class="container">
        <div class="title-section">
          <h1 class="title">Body Shape</h1>
        </div>
        <div class="box-pa">
          <div class="pa-que"><span>What kind of body shape best defines your physique?</span></div>
        </div>
      </div>
    </section>
    <section class="section section-shadow">
      <div class="container">
       <div class="box-body-shape box-body-shape <?php echo $body_shape_class; ?>">
          <div class="row">
          <?php if(!empty($body_shape)){
              foreach($body_shape as $val){
			?>
				<div class="col-md-2 col-sm-4 col-xs-6 item-col " data-attr="<?php echo $val['id']; ?>" data-attr2="1" data-attr3="<?php echo $val['answer']; ?>" >
				  <div class="item quiz" onclick="add_active_class('body_shape',<?php echo $val['id']; ?>);" >
					<div class="img-wrp">
					   <img class="img-default" id="oignal_img<?php echo $val['id']; ?>" src="<?php echo $val['image_name']; ?>">
					   <img class="img-selected hide" id="selected_img<?php echo $val['id']; ?>" src="<?php echo str_replace(".svg","-selected.svg",$val['image_name']); ?>">
					</div>
					<div class="title"><?php echo $val['answer']; ?></div>
					<div class="desc">
					  <?php echo $val['description']; ?>
					</div>
				  </div>
				</div>
			<?php }//foreach
                }//if ?>
			<input type="hidden" name="body_shape_ans" id="body_shape_ans" value="<?php echo $user_bodyshape; ?>"/>
            </div>
        </div>
      </div>

    </section>
    <section class="section section-shadow">
      <div class="container scbtns-wrp" >

      <?php if(empty($user_bodyshape))
            {
       ?>
	     <?php if($this->input->get('type') != 'add'){ ?> <a class="scbtn scbtn-tertiary" id="body_shape_back" href="#"><span>Back</span></a><?php } ?>
        <a class="scbtn scbtn-primary" id="body_shape_next" href="#" onclick="ga('send', 'event', '<?php echo $ga_gender; ?>', 'start');" ><span>Next</span></a>
      <?php }else
            {
        ?>
        <a class="scbtn scbtn-primary" onclick="updatepa('body_shape')" href="javascript:void(0);"><span>Save</span></a>
      <?php
            }
      ?>

      </div>
    </section>
  </div> -->
  <!-- /. pa take the body-shape -->
  <!-- pa take the style -->
  <!-- <div  id="slide5"  class="pa-slide-box style hide" user-style-attr="<?php echo $user_style; ?>">
    <section class="section section-shadow">
      <div class="container">
        <div class="title-section">
          <h1 class="title">Style Preference</h1>
        </div>
        <div class="box-pa">
          <p class="pa-que"><span>What ARE YOUR MOST PREFERRED STYLES?</span></p>
          <p>
            Select your top 3 style preferences in the boxes below
          </p>
          <ul id="sortable" class="box-style-selected">
            <li class="pref pref-1 active">
              <span class="text">Your First Preference</span>
              <span class="active-content">
                <img src="" onmousedown="return false">
              </span>
              <span class="active-content">
              <span class="shape-diamond">1</span>
						<span class="close"><i class="icon-close"></i></span>
              </span>

            </li>
            <li class="pref pref-2 active">
              <span class="text">Your Second Preference</span>
              <span class="active-content">
                <img src="" onmousedown="return false">
              </span>
              <span class="active-content">
              <span class="shape-diamond">2</span>
					<span class="close"><i class="icon-close"></i></span>
              </span>
            </li>
            <li class="pref pref-3 active">
              <span class="text">Your Third Preference</span>
              <span class="active-content">
                <img src="" onmousedown="return false">
              </span>
              <span class="active-content">
              <span class="shape-diamond">3</span>
					<span class="close"><i class="icon-close"></i></span>
              </span>
            </li>
          </ul>
        </div>
      </div>
    </section>
    <section class="section section-shadow" >
      <div class="container">
    <div class="box-style box-style-male">
  <div class="grid grid-items grid-items-slider sm-slider">
    <div class="row grid-row">
	   <?php if(!empty($style)){
				foreach($style as $val){
		?>
            <div class="col-md-2 col-sm-4 col-xs-6 item-col grid-col <?php //echo (($user_style_p1==$val['id'])||($user_style_p2==$val['id'])|| ($user_style_p3==$val['id']) ) ? 'active' : ''; ?>" id="<?php echo $val['id']; ?>">
              <div class="item quiz">
                <div class="img-wrp">
                  <img src="<?php echo $val['image_name']; ?>">
                </div>
                <div class="title"><?php echo $val['answer']; ?></div>
                <div class="desc"><?php echo $val['description']; ?></div>
              </div>
            </div>
          <?php }//foreach
			}//if ?>
          </div>
        </div>
        </div>
      </div>
    </section>
    <section class="section section-shadow">
      <div class="container">
        <div class="scbtns-wrp">
        <?php if(empty($user_style))
              {
          ?>
      			<a class="scbtn scbtn-tertiary" id="body_style_back" href="#"><span>Back</span></a>
      			<a class="scbtn scbtn-primary" id="body_style_next" href="#"><span>Next</span></a>
          <?php }else
            {
          ?>
            <a class="scbtn scbtn-primary" onclick="updatepa('body_style')" href="javascript:void(0);"><span>Save</span></a>
          <?php
              }
          ?>
        </div>
      </div>
    </section>
  </div> -->

  <!-- /. pa take the body-shape -->

  <!-- pa take the wardrobe -->
<!--   <div id="slide6" class="pa-slide-box wardrobe hide" user-brand-attr="<?php echo $user_brand; ?>" user-brand-name="<?php echo $user_brandname; ?>" >
    <section class="section section-shadow">
      <div class="container">
        <div class="title-section">
          <h1 class="title">Your Wardrobe</h1>
        </div>
        <div class="box-pa">
          <div class="pa-que">WHICH OF THE FOLLOWING BRANDS DO YOU HAVE IN YOUR WARDROBE?</div>

        </div>
      </div>
    </section>
    <section class="section section-shadow">
      <div class="container">
        <div class="box-brands-list selected-brands">
          <p class="text">
            CHOOSE YOUR BRANDS FROM THE LIST BELOW
            <br />
            <span> Zara,Westside</span>
          </p>
		<div id="selected_brands">
		</div>
        </div>
        <div class="box-brands-list available-brands">
			<?php if(!empty($brand_list)){ 
				foreach($brand_list as $val){
					if($val->is_show == 1){ 
			?>
				<span class="tag" value="<?php echo $val->brand_key; ?>" id="brand_list_<?php echo $val->brand_key; ?>" onclick="add_in_list(<?php echo $val->brand_key; ?>,<?php echo "'".$val->brand_name."'"; ?>)"><?php echo $val->brand_name; ?></span>
					<?php }//if 
					 }//foreach
			}//if ?>
        </div>
      </div>
    </section>
    <section class="section section-shadow">
      <div class="container">
        <div class="scbtns-wrp">
          <?php if(!empty($slide) && $slide==5)
              {
          ?>
      		      <a class="scbtn scbtn-primary" onclick="updatepa('brandlist')" href="javascript:void(0);"><span>Save</span></a>
          <?php }else
            {
          ?>
            <a class="scbtn scbtn-tertiary" id="brand_back_btn" href="#"><span>Back</span></a>
            <a id="get_started" class="scbtn scbtn-primary" href="javascript:void(0);" onclick="ga('send', 'event', '<?php echo $ga_gender; ?>', 'completed');"><span>Get Started</span></a>
          <?php
              }
          ?>
        </div>
      </div>

  </section>
	</div> -->
  <!-- /. pa take the wardrobe -->

</div>
<!-- /. page -->

<script type="text/javascript">
$( document ).ready(function() {
  ga('send', 'event', 'Sign Up', 'completed');
  <?php if(isset($_GET['ga'])){echo "ga('send', 'event', 'Login', 'source', 'gplus', '".$this->session->userdata('user_id')."');";} ?>
        //## UserTrack: Page ID=========================================
        document.cookie="pid=7";
        /*Code ends*/
  <?php if(empty($user_body_shape)){ ?>    
    //console.log("ONE");
  <?php }else{ if(@$edited_data == 'edit'){ ?>
    localStorage.setItem("edit_pa", "yes");   
    <?php }else{ ?>//console.log('no edit');
    ga('send', 'event', 'PA-Women', 'start');  
    <?php } ?>
    //console.log("TWO");
  <?php } ?>
});
</script>
