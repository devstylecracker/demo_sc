<?php
$scbox_pack = unserialize(SCBOX_PACKAGE);
$objectid = $this->uri->segment(2);
// $objectid_str = $this->uri->segment(2);
// $objectid_arr = explode('?',@$objectid_str);
// $objectid = @$objectid_arr[0];
$username_ = '';
if((@$this->session->userdata('first_name')!='') || (@$this->session->userdata('last_name')!='')){
$username_ = $this->session->userdata('first_name').' '.$this->session->userdata('last_name');
}
$email_ = $this->session->userdata('email');
$gender_ = $this->session->userdata('gender');
$filterArray = $scbox_pack;
$scboxSelectKey = $this->Schome_model->get_array_keyValue($filterArray,'id',$objectid);
$order_message = "";
$order_msgclass = "";
if(@$_GET['order']!=''){
$order_data = $this->Cart_model->scbox_get_order_info($_GET['order']);
if(@$order_data[0]['order_total']!=''){
$ordersuccesstotal =  number_format((float)@$order_data[0]['order_total'], 2, '.', '');
setcookie('sc_order_id',@$order_data[0]['order_display_no'], time() + (86400 * 30), "/");
$scbox_cartotal_amt = number_format(@$_COOKIE['scbox_cartotal'], 2, '.', ',');
if(!empty($order_data)){
    echo '<script type="text/javascript">
    ga("send", "event", "SCBOX","clicked","Step3c-Order Completed");
    </script>';
    if(@$order_data[0]['pay_mode']==2){
        $order_message = "Bring out the wine!<br> Your SCBOX of &#x20B9; ".@$scbox_cartotal_amt." with Order Id ".@$order_data[0]['order_display_no']." has been booked.<br> Please fill the details below to help us understand your personal style better.";
            }else if(@$order_data[0]['pay_mode']==1)
            {
            $order_message = "Bring out the wine!<br> Your SCBOX of &#x20B9; ".@$scbox_cartotal_amt." with Order Id ".@$order_data[0]['order_display_no']." has been booked.<br> Please fill the details below to help us understand your personal style better.";
            }
        }
    }
    $order_msgclass = 1;
}
if(@$_GET['error']!=''){
$order_message = $_GET['error'];
}
?>

<style>
       ul.lespad {
   display: inline-flex !important;
   }

    @media(max-width:768px) {

.page-scbox-form .list-occassion li {
   width:100px !important;
   }

   }
   .form-control{
   text-transform: capitalize !important;
   }
   /*.page-scbox-form .list-prints li{width:180px; margin: 10px 15px;}
   .page-scbox-form .list-prints li .pallet {height:180px;}
   */
   .page-scbox-form .list-skin-tone li .title-3 {
   padding-top: 15px !important;
   }
   .page-scbox-form .list-skin-tone li{width: 60px;} 
   @media(max-width:768px) {
   /*
   .page-scbox-form .list-prints li{width:100px;}
   .page-scbox-form .list-prints li .pallet {height:100px; width:100px;}*/
   .page-scbox-form ul.list-skin-tone li {
   width: 60px !important;
   }
   }
   .btn-group.special {
   display: flex !important;
   }
   .page-scbox-form .list-skin-tone li .pallet {
   height: 40px !important;
   width: 40px !important;
   }
   .special .btn {
   flex: 1 !important;
   }
   /*select{
   width: 30% !important;
   }*/
   .list-skin-tone li {
   width: 76px !important;
   }
   .page-scbox-form .list-bags li.active,
   .page-scbox-form .list-footweartype li.active,
   .page-scbox-form .list-heeltype li.active {
   outline: 2px solid #13D792 !important;
   cursor: default !important;
   }
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li,
   .page-scbox-form .list-dneed li {
   display: inline-block !important;
   width: 146px !important;
   margin: 10px 4px !important;
   cursor: pointer !important;
   }
   .page-scbox-form .list-fabrics li.active,
   .page-scbox-form .list-prints li.active,
   .page-scbox-form .list-dneed li.active {
   outline: 2px solid #ed1b2e !important;
   cursor: default !important;
   }
   .page-scbox-form .list-fabrics li {
   display: inline-block !important;
   width: 140px !important;
   margin: 10px 4px !important;
   cursor: pointer !important;
   }
   .btn {
   border: 1px solid #000 !important;
   color: #333 !important;
   border-radius: 0px !important;
   outline: none !important;
   }
   .btn-green.active,
   .btn-green.active.focus,
   .btn-green:active {
   border: 1px solid #13D792 !important;
   background: #13D792 !important;
   color: #fff !important;
   outline: none !important;
   }
   .btn-red.active,
   .btn-red.active.focus,
   .btn-red:active {
   border: 1px solid #ed1b2e !important;
   background: #ed1b2e !important;
   color: #fff !important;
   outline: none !important;
   }
   .choose_file {
   position: relative !important;
   display: inline-block !important;
   border-bottom: #13D792 solid 1px;
   width: 300px !important;
   padding: 4px 6px 4px 8px !important;
   color: #000 !important;
   margin-top: 2px !important;
   background: white !important;
   }
   .choose_file input[type="file"] {
   -webkit-appearance: none !important;
   position: absolute !important;
   top: 0 !important;
   left: 0 !important;
   opacity: 0 !important;
   }
   
   .hwidth {
   width: 15% !important;
   text-transform: uppercase !important;
   }
   @media (max-width: 768px) {
   .hwidth,.hwidth1 {
   width: 80% !important;
   text-transform: uppercase !important;
   }
   .hwidth1 {
   width: 35% !important;
   text-transform: uppercase !important;
   }
   }
   
   @media (max-width: 768px) {
   .page-scbox-form .list-colors li,
   .page-scbox-form .list-prints li,
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li {
   width: 80px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   }
   }
   @media (max-width: 768px) {
   .page-scbox-form .list-bags li img,
   .page-scbox-form .list-footweartype li img,
   .page-scbox-form .list-heeltype li img {
   max-width: 100% !important;
   }
   }
   @media (max-width: 768px) {
   .page-scbox-form ..list-bags li .pallet,
   .page-scbox-form .list-footweartype li .pallet,
   .page-scbox-form .list-heeltype li .pallet {
   width: 50px !important;
   height: 50px !important;
   margin: 5px auto 5px !important;
   }
   }
   .form-control {
   display: inline !important;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   height: 280px !important;
   }
   .pallet img {
   width: 146px !important;
   }
   @media (max-width: 768px) {
   .page-scbox-form .list-colors li,
   .page-scbox-form .list-prints li,
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li {
   width: 154px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   width: 100% !important;
   }
   }
   .extrapad {
   margin-top: 40px !important;
   margin-bottom: 40px !important;
   }
   /*.extrapad {
  margin-top: 60px;
   margin-bottom: 40px;
   padding-top: 60px;
   }
   .extrapad1 {
  margin-top: 30px;
   margin-bottom: 40px;
   }*/

   .page-scbox-form .pa-que{
      padding: 0 0 !important;
   }
   .page-scbox-form .bigimg li{
   display: inline-block !important;
   width: 180px !important;
   margin: 0px 4px 20px !important;
   cursor: pointer !important;
   background: #fafafa !important;
   border:1px solid #dfdfdf !important;
   }
   .noborder{
   border:none !important;
   }
   ul.list-categories{
   text-transform: capitalize !important;
   }
   .page-scbox-form .item.quiz .title {
   text-transform: capitalize !important;
   }
   .title-3{
   font-size: 13px !important;
   }
   @media (max-width: 768px)
   {
   .page-scbox-form .bigimg li{
   display: inline-block !important;
   width: 146px !important;
   margin: 0px 8px 20px !important;
   cursor: pointer !important;
   background: #fafafa !important;
   border:1px solid #dfdfdf !important;
   }
   .noborder{
   border:none !important;
   }
   }
   .title-section-changed .title-changed{
   margin: 0;
   line-height: 1;
   color: #333;
   display: inline-block;
   font-weight:normal;
   font-size:13px;
   letter-spacing: 2px;
   font-style:italic;
   }
   @media (max-width: 768px)
   {
   .title-section-changed .title-changed {
   font-size: 13px;
   line-height: 1.3;
   }
   }



   .mobile-align {
   width: 28% !important;
   text-align: left !important;
   margin-left:38% !important; 
   }

   .desktop-align{
      width: 25% !important;
   text-align: left !important;
   margin-left:46% !important;
   }

   .mobile-shade {
   width: 68% !important;
   text-align: left !important;
   margin-left:22% !important; 
   }

   .desktop-shade{
      width: 25% !important;
   text-align: left !important;
   margin-left:38% !important;
   }
 label{
      margin-top: 5px !important;
   }
   



    @media only screen and (min-device-width: 320px)
and (max-device-width: 480px) and (-webkit-device-pixel-ratio: 2) and (device-aspect-ratio: 2/3)
{
   .page-scbox-form .list-colors li,
   .page-scbox-form .list-prints li,
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li {
   width: 135px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   border: 1px solid #eaeaea;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   width: 100% !important;
   }
    .page-scbox-form .bigimg li{
   display: inline-block !important;
   width: 135px !important;
   /*margin: 0px 8px 20px !important;*/
   cursor: pointer !important;
   background: #fafafa !important;
   border:1px solid #dfdfdf !important;
   }
   .noborder{
   border:none !important;
   }
}

@media only screen and (min-device-width: 320px) and (max-device-width: 568px) 
and (-webkit-device-pixel-ratio: 2) and (device-aspect-ratio: 40/71)
{
   .page-scbox-form .list-colors li,
   .page-scbox-form .list-prints li,
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li {
   width: 135px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   border: 1px solid #eaeaea;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   width: 100% !important;
   }
    .page-scbox-form .bigimg li{
   display: inline-block !important;
   width: 135px !important;
   /*margin: 0px 8px 20px !important;*/
   cursor: pointer !important;
   background: #fafafa !important;
   border:1px solid #dfdfdf !important;
   }
   .noborder{
   border:none !important;
   }
}
@media screen and (device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2){

   .page-scbox-form .list-colors li,
   .page-scbox-form .list-prints li,
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li {
   width: 160px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   border: 1px solid #eaeaea;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   width: 100% !important;
   }
    .page-scbox-form .bigimg li{
   display: inline-block !important;
   width: 135px !important;
   /*margin: 0px 8px 20px !important;*/
   cursor: pointer !important;
   background: #fafafa !important;
   border:1px solid #dfdfdf !important;
   }
   .noborder{
   border:none !important;
   }
}
@media (min-width:320px) and (max-width:400px) { 
   .page-scbox-form .list-colors li,
   .page-scbox-form .list-prints li,
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li {
   width: 125px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   border: 1px solid #eaeaea;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   width: 100% !important;
   }
    .page-scbox-form .bigimg li{
   display: inline-block !important;
   width: 125px !important;
   /*margin: 0px 8px 20px !important;*/
   cursor: pointer !important;
   background: #fafafa !important;
   border:1px solid #dfdfdf !important;
   }
   .noborder{
   border:none !important;
   }

}
    </style>
    <div class="page-scbox-form">

        <!-- action="<?php //echo base_url(); ?>book-scbox/<?php //echo @$userdata['scbox_objectid']; ?>"-->

        <form name="scbox-form" id="scbox-form" method="post" action="">

            <input type='hidden' id="utm_source" name="utm_source" value="<?php echo @$_GET['utm_source']; ?>" />

            <input type='hidden' id="utm_medium" name="utm_medium" value="<?php echo @$_GET['utm_medium']; ?>" />

            <input type='hidden' id="utm_campaign" name="utm_campaign" value="<?php echo @$_GET['utm_campaign']; ?>" />

            <input type='hidden' id="scbox_package" name="scbox_package" value="<?php echo @$userdata['scbox_pack'];?>" />

            <input type='hidden' id="scbox_price" name="scbox_price" value="<?php echo @$userdata['scbox_price'];?>" />

            <input type='hidden' id="scbox_userid" name="scbox_userid" value="<?php echo @$userid;?>" />

            <input type='hidden' id="scbox_objectid" name="scbox_objectid" value="<?php echo @$objectid!='' ? $objectid : @$userdata['scbox_objectid'];?>" />

            <input type='hidden' id="scbox_productid" name="scbox_productid" value="<?php echo @$userdata['scbox_productid'];?>" />

            <input type='hidden' id="scbox_sessiongender" name="scbox_sessiongender" value="<?php echo @$this->session->userdata('gender');?>" />

            <input type='hidden' id="scbox_slide" name="scbox_slide" value="<?php echo @$_GET['step'];?>" />
            <input type='hidden' id="order_no" name="order_no" value="<?php echo @$_GET['order'];?>" />

            <div id="slide_1" class="array_afterpayment">

                <section class="section section-shadow ">
                    <div  class="box-message <?php  echo @$order_msgclass==1  ? 'success':'error'; ?> text-bold" id="order-message"><?php echo $order_message; ?></div>

                    <!-- <div class="title-2">You Are Almost Done! Just A Few More Questions To Go</div> -->

                    <div class="men_afterpayment category-apparel">

                        <div class="product-item extrapad section-height">

                            <div class="title-2">How Tall Are You?</div>

 <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick one)
                        </h1>
                     </div>
                            <div class="item-sizes-wrp">

                                <div class="form-group">

                                    <select name="height-height_feet" type="height-feet" class="form-control hwidth height_feet">

                                        <option value="">Select height</option>

                                        <option value="3 ft" answer-value="517">3 ft</option>

                                        <option value="4 ft" answer-value="518">4 ft</option>

                                        <option value="5 ft" answer-value="519">5 ft</option>

                                        <option value="6 ft" answer-value="520">6 ft</option>

                                    </select>

                                    <select name="height-height_inches" type="height-inches" class="form-control hwidth height_inches">

                                        <option value="">Select height</option>

                                        <option value="0 inch" answer-value="521">0 inch</option>

                                        <option value="1 inch" answer-value="522">1 inch</option>

                                        <option value="2 inch" answer-value="523">2 inch</option>

                                        <option value="3 inch" answer-value="524">3 inch</option>

                                        <option value="4 inch" answer-value="525">4 inch</option>

                                        <option value="5 inch" answer-value="526">5 inch</option>

                                        <option value="6 inch" answer-value="527">6 inch</option>

                                        <option value="7 inch" answer-value="528">7 inch</option>

                                        <option value="8 inch" answer-value="529">8 inch</option>

                                        <option value="9 inch" answer-value="530">9 inch</option>

                                        <option value="10 inch" answer-value="531">10 inch</option>

                                        <option value="11 inch" answer-value="532">11 inch</option>

                                    </select>

                                </div>

                            </div>

                            <label id="height-error" class="message error hide" for="height_ans">Please select height</label>

                        </div>

                    </div>

                    <div class="men_bags_afterpayment category-bags">

                              
 <div class="container bag-color-avoid">

    <div class="box-pa">

        <div class="pa-que">
             <span>You Selected A Bag! Could You Tell Us A Little More About The Kind Of Bag You Want?</span>
            
        </div>
        <div class="pa-que">
             <span>Colors I <i style="color: #ed1b2e;"> AVOID</i></span>
 <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
        </div>

    </div>

    <ul class="add-event list-skin-tone list-colordneed actdeact color-to-avoid-afterpayment bag-color-avoid" type="bag-color-avoid">
 <li answer-value="-421" class="other-colors">
                     <div class="pallet" color="#8bf5cd" style="background-color:#8bf5cd;border:1px solid #8bf5cd;"></div>
                  </li>
                  <li answer-value="-422" class="other-colors">
                     <div class="pallet" color="#89aad5" style="background-color:#89aad5;border:1px solid #89aad5;"></div>
                  </li>
                  <li answer-value="-423" class="other-colors">
                     <div class="pallet" color="#bf94e4" style="background-color:#bf94e4;border:1px solid #bf94e4;"></div>
                  </li>
                  <li answer-value="-424" class="other-colors">
                     <div class="pallet" color="#aeb0ae" style="background-color:#aeb0ae;border:1px solid #aeb0ae;"></div>
                  </li>
                   <li answer-value="-405" class="other-colors">
                     <div class="pallet" color="#734021" style="background-color:#734021;border:1px solid #734021;"></div>
                  </li>
                   <li answer-value="-425" class="other-colors">
                     <div class="pallet" color="#2082ef" style="background-color:  #2082ef;border:1px solid #2082ef;"></div>
                  </li>
                   <li answer-value="-407" class="other-colors">
                     <div class="pallet" color="#0b0706" style="background-color:#0b0706;border:1px solid #0b0706;"></div>
                  </li>
                  <li answer-value="-426" class="other-colors">
                     <div class="pallet" color="#68dbd6"
                        style="background-color:  #68dbd6;border:1px solid #68dbd6;"></div>
                  </li>
                  <li answer-value="-427" class="other-colors">
                     <div class="pallet" color="#f5dc05" style="background-color:#f5dc05;border:1px solid #f5dc05;"></div>
                  </li>
                  <li answer-value="-428" class="other-colors">
                     <div class="pallet" color="#fffefe" style="background-color:#fffefe;border:1px solid #fffefe;"></div>
                  </li>
                  <li answer-value="-429" class="other-colors">
                     <div class="pallet" color="#0c4c8a" style="background-color:#0c4c8a;border:1px solid #0c4c8a;"></div>
                  </li>
                  <li answer-value="-430" class="other-colors">
                     <div class="pallet" color="#576331" style="background-color:#576331;border:1px solid #576331;"></div>
                  </li>
                  <li answer-value="-431" class="other-colors">
                     <div class="pallet" color="#041531" style="background-color:#041531;border:1px solid #041531;"></div>
                  </li>
                  <li answer-value="-432" class="other-colors">
                     <div class="pallet" color="#de443a" style="background-color:#de443a;border:1px solid #de443a;"></div>
                  </li>
                  <li answer-value="-433" class="other-colors">
                     <div class="pallet" color="#bde4f5"
                        style="background-color:#bde4f5;border:1px solid #bde4f5;"></div>
                  </li>
                  <li answer-value="-434" class="other-colors">
                     <div class="pallet" color="#ffbbc7"
                        style="background-color:#ffbbc7;border:1px solid #ffbbc7;"></div>
                  </li>
                  <li answer-value="-435" class="other-colors">
                     <div class="pallet" color="#0a6148"
                        style="background-color:#0a6148;border:1px solid #0a6148;"></div>
                  </li>
                 
                  <br>
                      <li class="sc-checkbox all-colors" style="width:220px !important;" answer-value="516">
                         <div class="title-3" color="I like all colors">
                            <i class="icon icon-diamond"></i>I like all colors
                               <input type="checkbox">
                         </div>
                      </li> 

    </ul>
    
    <label id="coloravoid-error" class="message error hide" for="coloravoid">Please select a color</label>

</div>

                        <div class="container extrapad bag-print-avoid">

    <div class="box-pa">

        <div class="pa-que">

            <span>Prints and Patterns I <i style="color: #ed1b2e;">Avoid </i>

<!-- <i style="font-size:90%;">(Select 1 or more)</i> -->

</span>
 <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>

        </div>

    </div>

    <ul class="add-event list-colors list-dneed other-type bag-print-avoid" type="bag-print-avoid">

      <li answer-value="-198">
                     <div class="pallet">
                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/prints/polka.jpg');?>" alt="Polka dots">
                     </div>
                     <div class="title-3">polka dots</div>
                  </li>
                  <li answer-value="-184">
                     <div class="pallet">
                        <img src="<?php echo base_url('assets/seventeen/images/pa/male/prints/1floral.jpg');?>" alt="Floral">
                     </div>
                     <div class="title-3">floral</div>
                  </li>
                  <li answer-value="-199">
                     <div class="pallet">
                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/prints/stripes.jpg');?>" alt="Stripes">
                     </div>
                     <div class="title-3">stripes</div>
                  </li>
                 <!--  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/prints/animal.jpg');?>" alt="Animal">
                     </div>
                     <div class="title-3">animal</div>
                  </li> -->
                  <li answer-value="-535">
                     <div class="pallet">
                        <img src="<?php echo base_url('assets/seventeen/images/pa/male/prints/1quirky.jpg');?>" alt="QUIRKY">
                     </div>
                     <div class="title-3">quirky</div>
                  </li>
                  <li answer-value="-253">
                     <div class="pallet">
                        <img src="<?php echo base_url('assets/seventeen/images/pa/male/prints/1check.jpg');?>" alt="Checks">
                     </div>
                     <div class="title-3">checks</div>
                  </li>
                 <!--  <li>
                     <div class="pallet">
                        <img src="<?php echo base_url('assets/seventeen/images/pa/male/prints/polka.jpg');?>" alt="TARTANS">
                     </div>
                     <div class="title-3">tartans</div>
                  </li> -->
                  <li answer-value="-190">
                     <div class="pallet">
                        <img src="<?php echo base_url('assets/seventeen/images/pa/male/prints/1hound.jpg');?>" alt="HOUNDSTOOTH">
                     </div>
                     <div class="title-3">houndstooth</div>
                  </li>
                  <li answer-value="-255">
                     <div class="pallet">
                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/prints/solid.jpg');?>" alt="Solid">
                     </div>
                     <div class="title-3">solid</div>
                  </li>

    </ul>

    <label id="printavoid-error" class="message error hide" for="printavoid">Please select print to avoid</label>

</div>

                        

                        

                        

                    </div>


                    <div class="men_sunglasses_afterpayment category-accessories">

                        <div class="container accessories-sunglasses-container">

                            <div class="box-pa">

                                <div class="pa-que"><span>You Are Looking For Sunglasses ! Tell Us What Kind? <br>sunglasses</span>
                                 <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                   </div>

                            </div>

                            <ul class="add-event list-colors accessories-sunglasses-types" type="accessories-sunglasses-types">

                                <li answer-value="97">

                                    <div class="pallet">

                                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/sunglass/aviator.jpg');?>">

                                    </div>

                                    <div class="title-3">aviators</div>

                                </li>

                                <li answer-value="98">

                                    <div class="pallet">

                                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/sunglass/wayfarer.jpg');?>">

                                    </div>

                                    <div class="title-3">wayfarers</div>

                                </li>

                                <li answer-value="99">

                                    <div class="pallet">

                                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/sunglass/round.jpg');?>">

                                    </div>

                                    <div class="title-3">round</div>

                                </li>

                                <li answer-value="100">

                                    <div class="pallet">

                                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/sunglass/square.jpg');?>">

                                    </div>

                                    <div class="title-3">square</div>

                                </li>


                            </ul>

                            <label id="sunglass-error" class="message error hide" for="sunglass">Please select atleast one sunglass</label>

                        </div>

                         <div class="product-item extrapad accessories-refelector-container">
                             <div class="box-pa">

                            <div class="pa-que">

                                <span> 
                                Reflectors</span>
                                 <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick yes or no)
                     </div>
                            </div>

                            </div>
                           

                            <ul class="add-event list-occassion purpose lespad accessories-refelector-types" type="accessories-refelector-types">

                                <li  style="text-align: center;">

                                    <label class="sc-checkbox" answer-value="486"><i class="icon icon-diamond"></i>

                                        <input type="checkbox"> <span>yes</span></label>

                                </li>

                                <li style="text-align: center;">

                                    <label class="sc-checkbox" answer-value="-487"><i class="icon icon-diamond"></i>

                                        <input type="checkbox"> <span>no</span></label>

                                </li>

                            </ul>

                            <label id="reflector-error" class="message error hide" for="reflector">Please select atleast one</label>

                        </div>

                    </div>


                </section>

            </div>

            <section class="section section-shadow">

                <div class="btns-wrp">

                    <!-- <a class="scbtn scbtn-primary" href="javascript:void(0);" onclick="ChangeStep(33);"><span>Previous</span></a> -->

                    <a class="scbtn scbtn-primary" href="javascript:void(0);" onclick="ChangeStep(5);"><span>Next</span></a>

                    <!-- <button class="scbtn scbtn-primary" onclick="slideprev(4);"><span>Previous</span></button>

<button class="scbtn scbtn-primary"  onclick="slidenext(4);"><span>Next</span></button> -->

                </div>

            </section>

    </div>

    </form>

    </div>

    <style>

        .scbox-payment-options li {

            display: inline-block;

            margin-right: 10px;

        }

        

        .scbox-payment-options .sc-checkbox .icon {

            position: relative;

            top: 10px !important;

        }

        

        input[type=radio] {

            display: none;

        }

    </style>

    <script>

        

        

        

        /*

        function AddCheckAttributes(type, container) {

            //##Select reflectors

            $(container).parent().find("li").each(function(e) {

                if (type == "scbox_reflectors") {

                    $(this).removeClass("active");

                    $(container).addClass('active');

                    var arr_list_reflector = $('.list-reflector li.active').map(function() {

                        return this.id;

                    }).get();

                }

                console.log(arr_list_reflector);

            });

            //##Select finish

            $(container).parent().find("li").each(function(e) {

                if (type == "scbox_finish") {

                    $(this).removeClass("active");

                    $(container).addClass('active');

                    var arr_list_finish = $('.list-finish li.active').map(function() {

                        //arr_list_coloravoid = [];

                        return this.id;

                    }).get();

                }

                console.log(arr_list_finish);

            });

        }



        function ShowHideprofession() {

            var ddlPassport = document.getElementById("scbox_profession");

            var dvPassport = document.getElementById("dvPassport");

            dvPassport.style.display = scbox_profession.value == "other" ? "block" : "none";

        }



        function ShowHideDiv() {

            var chkYes = document.getElementById("chkYes");

            var recipient_address = document.getElementById("recipient_address");

            recipient_address.style.display = chkYes.checked ? "block" : "none";

        }

        */

        

        //## SHOW HIDE CATEGORY CONTAINERS function initiated -------------

        document.addEventListener("DOMContentLoaded", function(event) { 

            msg("LOADED STEP 2ddddd...... page");

            ShowHideCategorySections();
             GetUserAnswerID();
            

        });

        

        

        // This is called with the results from from FB.getLoginStatus().

        function statusChangeCallback(response) {

            if (response.status === 'connected') {

                // Logged into your app and Facebook.

                testAPI();

            } else {

                // The person is not logged into your app or we are unable to tell.

                document.getElementById('status').innerHTML = 'Please log ' +

                    'into this app.';

            }

        }

        // This function is called when someone finishes with the Login

        // Button.  See the onlogin handler attached to it in the sample

        // code below.

        function checkLoginState() {

            FB.getLoginStatus(function(response) {

                statusChangeCallback(response);

            });

        }

        window.fbAsyncInit = function() {

            FB.init({

                appId: '1650358255198436',

                cookie: true, // enable cookies to allow the server to access

                // the session

                xfbml: true, // parse social plugins on this page

                version: 'v2.8' // use graph api version 2.8

            });

            // Now that we've initialized the JavaScript SDK, we call

            // FB.getLoginStatus().  This function gets the state of the

            // person visiting this page and can return one of three states to

            // the callback you provide.  They can be:

            //

            // 1. Logged into your app ('connected')

            // 2. Logged into Facebook, but not your app ('not_authorized')

            // 3. Not logged into Facebook and can't tell if they are logged into

            //    your app or not.

            //

            // These three cases are handled in the callback function.

            FB.getLoginStatus(function(response) {

                statusChangeCallback(response);

            });

        };

        // Load the SDK asynchronously

        (function(d, s, id) {

            var js, fjs = d.getElementsByTagName(s)[0];

            if (d.getElementById(id)) return;

            js = d.createElement(s);

            js.id = id;

            js.src = "//connect.facebook.net/en_US/sdk.js";

            fjs.parentNode.insertBefore(js, fjs);

        }(document, 'script', 'facebook-jssdk'));

        // Here we run a very simple test of the Graph API after login is

        // successful.  See statusChangeCallback() for when this call is made.

        function testAPI() {

            response = new Array();

            var accessToken = 0;

            FB.getLoginStatus(function(response) {

                if (response.status === 'connected') {

                    accessToken = response.authResponse.accessToken;

                }

            });

            $.ajax({

                url: 'https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=1650358255198436&client_secret=403b4cbf601b9a67b6594c7be781e8b9&fb_exchange_token=' + accessToken,

                type: 'get',

                data: {

                    'response': response

                },

                success: function(data, status) {

                    save_fb_token(data);

                }

            });

        }



        function save_fb_token(data) {

            $.ajax({

                url: sc_baseurl + 'Schome_new/save_fb_token',

                type: 'post',

                data: {

                    'data': data

                },

                success: function(data, status) {}

            });

        }



        function fb_login() {

            FB.login(function(response) {

                if (response.authResponse) {

                    checkLoginState();

                } else {

                    //user hit cancel button

                    console.log('User cancelled login or did not fully authorize.');

                }

            }, {

                scope: 'public_profile,email,user_photos'

            });

        }

       
    </script>