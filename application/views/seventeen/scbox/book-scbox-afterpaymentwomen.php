<?php
$scbox_pack = unserialize(SCBOX_PACKAGE);
$objectid_str = $this->uri->segment(2);
$objectid_arr = explode('?',@$objectid_str);
$objectid = @$objectid_arr[0];
$user_style = '';$styledata='';
$username_ = '';
if((@$this->session->userdata('first_name')!='') || (@$this->session->userdata('last_name')!=''))
{
    $username_ = $this->session->userdata('first_name').' '.$this->session->userdata('last_name');
}
$email_ = $this->session->userdata('email');
$gender_ = $this->session->userdata('gender');
$filterArray = $scbox_pack;
$scboxSelectKey = $this->Schome_model->get_array_keyValue($filterArray,'id',$objectid);
$order_message = "";
$order_msgclass = "";

if(@$_GET['order']!='')
{
    $order_data = $this->Cart_model->scbox_get_order_info($_GET['order']);
    if(@$order_data[0]['order_total']!=''){
        $ordersuccesstotal =  number_format((float)@$order_data[0]['order_total'], 2, '.', '');
        setcookie('sc_order_id',@$order_data[0]['order_display_no'], time() + (86400 * 30), "/");
        
        $scbox_cartotal_amt = number_format(@$_COOKIE['scbox_cartotal'], 2, '.', ',');
        if(!empty($order_data)){
        echo '<script type="text/javascript">
        ga("send", "event", "SCBOX","clicked","Step3c-Order Completed");
        </script>';
        if(@$order_data[0]['pay_mode']==2){
            $order_message = "Bring out the wine!<br> Your SCBOX of &#x20B9; ".@$scbox_cartotal_amt." with Order Id ".@$order_data[0]['order_display_no']." has been booked.<br><br> Please fill the details below to help us understand your personal style better.";
        }else if(@$order_data[0]['pay_mode']==1){
            $order_message = "Bring out the wine!<br> Your SCBOX of &#x20B9; ".@$scbox_cartotal_amt." with Order Id ".@$order_data[0]['order_display_no']." has been booked. <br><br>Please fill the details below to help us understand your personal style better.";
        }
        }
}
$order_msgclass = 1;
}

if(@$_GET['error']!=''){
    $order_message = $_GET['error'];
}
?>
<style>
       ul.lespad {
   display: inline-flex !important;
   }

    @media(max-width:768px) {

.page-scbox-form .list-occassion li {
   width:100px !important;
   }

   }
   .form-control{
   text-transform: capitalize !important;
   }
   /*.page-scbox-form .list-prints li{width:180px; margin: 10px 15px;}
   .page-scbox-form .list-prints li .pallet {height:180px;}
   */
   .page-scbox-form .list-skin-tone li .title-3 {
   padding-top: 15px !important;
   }
   .page-scbox-form .list-skin-tone li{width: 60px;} 
   @media(max-width:768px) {
   /*
   .page-scbox-form .list-prints li{width:100px;}
   .page-scbox-form .list-prints li .pallet {height:100px; width:100px;}*/
   .page-scbox-form ul.list-skin-tone li {
   width: 60px !important;
   }
   }
   .btn-group.special {
   display: flex !important;
   }
   .page-scbox-form .list-skin-tone li .pallet {
   height: 40px !important;
   width: 40px !important;
   }
   .special .btn {
   flex: 1 !important;
   }
   /*select{
   width: 30% !important;
   }*/
   .list-skin-tone li {
   width: 76px !important;
   }
   .page-scbox-form .list-bags li.active,
   .page-scbox-form .list-footweartype li.active,
   .page-scbox-form .list-heeltype li.active {
   outline: 2px solid #13D792 !important;
   cursor: default !important;
   }
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li,
   .page-scbox-form .list-dneed li {
   display: inline-block !important;
   width: 146px !important;
   margin: 10px 4px !important;
   cursor: pointer !important;
   }
   .page-scbox-form .list-fabrics li.active,
   .page-scbox-form .list-prints li.active,
   .page-scbox-form .list-dneed li.active {
   outline: 2px solid #ed1b2e !important;
   cursor: default !important;
   }
   .page-scbox-form .list-fabrics li {
   display: inline-block !important;
   width: 140px !important;
   margin: 10px 4px !important;
   cursor: pointer !important;
   }
   .btn {
   border: 1px solid #000 !important;
   color: #333 !important;
   border-radius: 0px !important;
   outline: none !important;
   }
   .btn-green.active,
   .btn-green.active.focus,
   .btn-green:active {
   border: 1px solid #13D792 !important;
   background: #13D792 !important;
   color: #fff !important;
   outline: none !important;
   }
   .btn-red.active,
   .btn-red.active.focus,
   .btn-red:active {
   border: 1px solid #ed1b2e !important;
   background: #ed1b2e !important;
   color: #fff !important;
   outline: none !important;
   }
   .choose_file {
   position: relative !important;
   display: inline-block !important;
   border-bottom: #13D792 solid 1px;
   width: 300px !important;
   padding: 4px 6px 4px 8px !important;
   color: #000 !important;
   margin-top: 2px !important;
   background: white !important;
   }
   .choose_file input[type="file"] {
   -webkit-appearance: none !important;
   position: absolute !important;
   top: 0 !important;
   left: 0 !important;
   opacity: 0 !important;
   }
   
   .hwidth, .hwidth1 {
   width: 15% !important;
   text-transform: uppercase !important;
   }
   @media (max-width: 768px) {
   .hwidth {
   width: 80% !important;
   text-transform: uppercase !important;
   }
   .hwidth1 {
   width: 35% !important;
   text-transform: uppercase !important;
   }
   }
   
   @media (max-width: 768px) {
   .page-scbox-form .list-colors li,
   .page-scbox-form .list-prints li,
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li {
   width: 80px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   }
   }
   @media (max-width: 768px) {
   .page-scbox-form .list-bags li img,
   .page-scbox-form .list-footweartype li img,
   .page-scbox-form .list-heeltype li img {
   max-width: 100% !important;
   }
   }
   @media (max-width: 768px) {
   .page-scbox-form ..list-bags li .pallet,
   .page-scbox-form .list-footweartype li .pallet,
   .page-scbox-form .list-heeltype li .pallet {
   width: 50px !important;
   height: 50px !important;
   margin: 5px auto 5px !important;
   }
   }
   .form-control {
   display: inline !important;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   height: 280px !important;
   }
   .pallet img {
   width: 144px !important;
   }
   @media (max-width: 768px) {
   .page-scbox-form .list-colors li,
   .page-scbox-form .list-prints li,
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li {
   width: 154px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   width: 100% !important;
   }
   }
   .extrapad {
   margin-top: 40px !important;
   margin-bottom: 40px !important;
   }
   /*.extrapad {
  margin-top: 60px;
   margin-bottom: 40px;
   padding-top: 60px;
   }
   .extrapad1 {
  margin-top: 30px;
   margin-bottom: 40px;
   }*/

   .page-scbox-form .pa-que{
      padding: 0 0 !important;
   }
   .page-scbox-form .bigimg li{
   display: inline-block !important;
   width: 180px !important;
   margin: 0px 4px 20px !important;
   cursor: pointer !important;
   background: #fafafa !important;
   border:1px solid #dfdfdf !important;
   }
   .noborder{
   border:none !important;
   }
   ul.list-categories{
   text-transform: capitalize !important;
   }
   .page-scbox-form .item.quiz .title {
   text-transform: capitalize !important;
   }
   .title-3{
   font-size: 13px !important;
   }
   @media (max-width: 768px)
   {
   .page-scbox-form .bigimg li{
   display: inline-block !important;
   width: 146px !important;
   margin: 0px 8px 20px !important;
   cursor: pointer !important;
   background: #fafafa !important;
   border:1px solid #dfdfdf !important;
   }
   .noborder{
   border:none !important;
   }
   }
   .title-section-changed .title-changed{
   margin: 0;
   line-height: 1;
   color: #333;
   display: inline-block;
   font-weight:normal;
   font-size:13px;
   letter-spacing: 2px;
   font-style:italic;
   }
   @media (max-width: 768px)
   {
   .title-section-changed .title-changed {
   font-size: 13px;
   line-height: 1.3;
   }
   }



   .mobile-align {
   width: 28% !important;
   text-align: left !important;
   margin-left:38% !important; 
   }

   .desktop-align{
      width: 25% !important;
   text-align: left !important;
   margin-left:46% !important;
   }

   .mobile-shade {
   width: 68% !important;
   text-align: left !important;
   margin-left:22% !important; 
   }

   .desktop-shade{
      width: 25% !important;
   text-align: left !important;
   margin-left:38% !important;
   }
 label{
      margin-top: 5px !important;
   }
   



    @media only screen and (min-device-width: 320px)
and (max-device-width: 480px) and (-webkit-device-pixel-ratio: 2) and (device-aspect-ratio: 2/3)
{
   .page-scbox-form .list-colors li,
   .page-scbox-form .list-prints li,
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li {
   width: 135px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   border: 1px solid #eaeaea;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   width: 100% !important;
   }
    .page-scbox-form .bigimg li{
   display: inline-block !important;
   width: 135px !important;
   /*margin: 0px 8px 20px !important;*/
   cursor: pointer !important;
   background: #fafafa !important;
   border:1px solid #dfdfdf !important;
   }
   .noborder{
   border:none !important;
   }
}

@media only screen and (min-device-width: 320px) and (max-device-width: 568px) 
and (-webkit-device-pixel-ratio: 2) and (device-aspect-ratio: 40/71)
{
   .page-scbox-form .list-colors li,
   .page-scbox-form .list-prints li,
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li {
   width: 135px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   border: 1px solid #eaeaea;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   width: 100% !important;
   }
    .page-scbox-form .bigimg li{
   display: inline-block !important;
   width: 135px !important;
   /*margin: 0px 8px 20px !important;*/
   cursor: pointer !important;
   background: #fafafa !important;
   border:1px solid #dfdfdf !important;
   }
   .noborder{
   border:none !important;
   }
}
@media screen and (device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2){

   .page-scbox-form .list-colors li,
   .page-scbox-form .list-prints li,
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li {
   width: 160px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   border: 1px solid #eaeaea;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   width: 100% !important;
   }
    .page-scbox-form .bigimg li{
   display: inline-block !important;
   width: 135px !important;
   /*margin: 0px 8px 20px !important;*/
   cursor: pointer !important;
   background: #fafafa !important;
   border:1px solid #dfdfdf !important;
   }
   .noborder{
   border:none !important;
   }
}
@media (min-width:320px) and (max-width:400px) { 
   .page-scbox-form .list-colors li,
   .page-scbox-form .list-prints li,
   .page-scbox-form .list-bags li,
   .page-scbox-form .list-footweartype li,
   .page-scbox-form .list-heeltype li {
   width: 125px !important;
   text-align: center !important;
   margin: 5px 4px !important;
   border: 1px solid #eaeaea;
   }
   .page-scbox-form .list-colors li .pallet,
   .page-scbox-form .list-prints li .pallet {
   width: 100% !important;
   }
    .page-scbox-form .bigimg li{
   display: inline-block !important;
   width: 125px !important;
   /*margin: 0px 8px 20px !important;*/
   cursor: pointer !important;
   background: #fafafa !important;
   border:1px solid #dfdfdf !important;
   }
   .noborder{
   border:none !important;
   }

}
    </style>

    <div class="page-scbox-form">

        <!-- action="<?php //echo base_url(); ?>book-scbox/<?php //echo @$userdata['scbox_objectid']; ?>"-->

        <form name="scbox-form" id="scbox-form" method="post" action="">

            <input type='hidden' id="utm_source" name="utm_source" value="<?php echo @$_GET['utm_source']; ?>" />

            <input type='hidden' id="utm_medium" name="utm_medium" value="<?php echo @$_GET['utm_medium']; ?>" />

            <input type='hidden' id="utm_campaign" name="utm_campaign" value="<?php echo @$_GET['utm_campaign']; ?>" />

            <input type='hidden' id="scbox_package" name="scbox_package" value="<?php echo @$userdata['scbox_pack'];?>" />

            <input type='hidden' id="scbox_price" name="scbox_price" value="<?php echo @$userdata['scbox_price'];?>" />

            <input type='hidden' id="scbox_userid" name="scbox_userid" value="<?php echo @$userid;?>" />

            <input type='hidden' id="scbox_objectid" name="scbox_objectid" value="<?php echo @$objectid!='' ? $objectid : @$userdata['scbox_objectid'];?>" />

            <input type='hidden' id="scbox_productid" name="scbox_productid" value="<?php echo @$userdata['scbox_productid'];?>" />

            <input type='hidden' id="scbox_sessiongender" name="scbox_sessiongender" value="<?php echo @$this->session->userdata('gender');?>" />

            <input type='hidden' id="scbox_slide" name="scbox_slide" value="<?php echo @$_GET['step'];?>" />
            <input type='hidden' id="order_no" name="order_no" value="<?php echo @$_GET['order'];?>" />

            <div id="slide_1" class="array_afterpayment">

                <section class="section section-shadow ">
                 <div  class="box-message <?php  echo @$order_msgclass==1  ? 'success':'error'; ?> text-bold" id="order-message"><?php echo $order_message; ?></div>

                   <!--  <div class="title-2">You Are Almost Done !<br> Just A Few More Questions To Go<br><br><br></div> -->

                    <div class="women_afterpayment category-apparel">

                        <div class="product-item section-height">
                        

                            <div class="pa-que">

                                <span>How Tall Are You?</span>
                                <div class="title-section-changed text-center"  >
                                <h1 class="title-changed">
                                   (Pick one)
                                </h1>
                             </div>

                            </div>

                       

                           
                            <div class="item-sizes-wrp">

                                <div class="form-group">

                                    <select name="height-height_feet" type="height-feet" class="form-control hwidth1 height_feet">

                                        <option value="">Select height</option>

                                        <option value="3 ft" answer-value="517">3 ft</option>

                                        <option value="4 ft" answer-value="518">4 ft</option>

                                        <option value="5 ft" answer-value="519">5 ft</option>

                                        <option value="6 ft" answer-value="520">6 ft</option>

                                    </select>

                                    <select name="height-height_inches" type="height-inches" class="form-control hwidth1 height_inches">

                                        <option value="">Select height</option>

                                        <option value="0 inch" answer-value="521">0 inch</option>

                                        <option value="1 inch" answer-value="522">1 inch</option>

                                        <option value="2 inch" answer-value="523">2 inch</option>

                                        <option value="3 inch" answer-value="524">3 inch</option>

                                        <option value="4 inch" answer-value="525">4 inch</option>

                                        <option value="5 inch" answer-value="526">5 inch</option>

                                        <option value="6 inch" answer-value="527">6 inch</option>

                                        <option value="7 inch" answer-value="528">7 inch</option>

                                        <option value="8 inch" answer-value="529">8 inch</option>

                                        <option value="9 inch" answer-value="530">9 inch</option>

                                        <option value="10 inch" answer-value="531">10 inch</option>

                                        <option value="11 inch" answer-value="532">11 inch</option>

                                    </select>

                                </div>

                            </div>

                            <label id="height-error" class="message error hide" for="height_ans">Please select height</label>

                        </div>

                    </div>

                    <div class="women_bags_afterpayment category-bags">

                        <div class="product-item">

                            <div class="pa-que">

                                <span>You Selected A Bag! Could You Tell Us A Little More About The Kind Of Bag You Want?</span></div>

                            <div class="pa-que">

                               <span> I Want It For</span>
                                <div class="title-section-changed text-center"  >
                                    <h1 class="title-changed">
                                       (Pick one)
                                    </h1>
                                 </div>

                            </div>

                        <ul class="add-event list-occassion purpose lespad section-bag-i-want-for" type="section-bag-i-want-for">

                            <li  style="text-align: center;" >

                                <label class="sc-checkbox" answer-value="363"><i class="icon icon-diamond"></i>

                                    <input type="checkbox"> <span>everyday</span></label>

                            </li>

                            <li style="text-align: center;" >

                                <label class="sc-checkbox" answer-value="364"><i class="icon icon-diamond"></i>

                                    <input type="checkbox"> <span>work</span></label>

                            </li>

                            <li style="text-align: center;">

                                <label class="sc-checkbox"  answer-value="365"><i class="icon icon-diamond"></i>

                                    <input type="checkbox"> <span>party</span></label>

                            </li>

                        </ul>

                        <label id="occassion-error" class="message error hide" for="occassion">Please select atleast one</label>

                        <div class="product-item extrapad">

                            <div class="box-pa">

                            <div class="pa-que">

                                <span>I Want The Size Of My Bag To Be</span>
                                <div class="title-section-changed text-center"  >
                                    <h1 class="title-changed">
                                       (Pick one)
                                    </h1>
                                 </div>
                            </div>

                            </div>
                           
                            <div class="item-sizes-wrp">

                                <div class="form-group">

                                    <select class="form-control hwidth bag-size" type="bag-size">

                                        <option value="">Select bag size</option>

                                        <option value="small" answer-value="366">small</option>

                                        <option value="medium" answer-value="367">medium</option>

                                        <option value="large" id="large" answer-value="368">large</option>

                                    </select>

                                </div>

                            </div>

                            <label id="scbox_sizebag-error" class="message error hide" for="scbox_sizebag">Please select Bag size</label>

                        </div>

                        <div class="container bag-color-avoid">

    <div class="box-pa">

        <div class="pa-que">

            <span>Colors I <i style="color: #ed1b2e;"> Avoid</i></span>
           <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                     </div>
        </div>

     <ul class="add-event list-skin-tone list-colordneed actdeact color-to-avoid-afterpayment bag-color-avoid" type="bag-color-avoid">
                     <li answer-value="-419" class="other-colors">
                        <div class="pallet" color="#90140c" style="background-color:#90140c;border:1px solid #90140c;"></div>
                     </li>
                      <li answer-value="-424" class="other-colors">
                        <div class="pallet" color="#b8bebe" style="background-color:#b8bebe;border:1px solid #b8bebe;"></div>
                     </li>
                      <li answer-value="-403" class="other-colors">
                        <div class="pallet" color="#be914a" style="background-color:#be914a;border:1px solid #be914a;"></div>
                     </li>
                     <li answer-value="-404" class="other-colors">
                        <div class="pallet" color="#ff0080" style="background-color:#FF0080;border:1px solid #FF0080;"></div>
                     </li>
                     <li answer-value="-405" class="other-colors">
                        <div class="pallet" color="#734021" style="background-color:  #734021;border:1px solid #734021;"></div>
                     </li>

                     <li answer-value="-406" class="other-colors">
                        <div class="pallet" color="#fec4c3" style="background-color:#fec4c3;border:1px solid #fec4c3;"></div>
                     </li>
                     <li answer-value="-407" class="other-colors">
                        <div class="pallet" color="#0b0706" style="background-color:#0b0706;border:1px solid #0b0706;"></div>
                     </li>
                     <li answer-value="-408" class="other-colors">
                        <div class="pallet" color="#98012e" style="background-color:  #98012e;border:1px solid #98012e;"></div>
                     </li>
                     <li answer-value="-409" class="other-colors">
                        <div class="pallet" color="#ffffff" style="background-color:#FFFFFF;border:1px solid #FFFFFF;"></div>
                     </li>
                     <li answer-value="-410" class="other-colors">
                        <div class="pallet" color="#f57d31" style="background-color:#f57d31;border:1px solid #f57d31;"></div>
                     </li>
                     <li answer-value="-411" class="other-colors">
                        <div class="pallet" color="#87ceeb" style="background-color:#87ceeb;border:1px solid #87ceeb;"></div>
                     </li>
                     <li answer-value="-412" class="other-colors">
                        <div class="pallet" color="#cdd1d1" style="background-color:#cdd1d1;border:1px solid #cdd1d1;"></div>
                     </li>
                     <li answer-value="-413" class="other-colors">
                        <div class="pallet" color="#d5011a" style="background-color:#d5011a;border:1px solid #d5011a;"></div>
                     </li>
                      <li answer-value="-414" class="other-colors">
                        <div class="pallet" color="#800080" style="background-color:#800080;border:1px solid #800080;"></div>
                     </li>
                     <li answer-value="-415" class="other-colors">
                        <div class="pallet" color="#bcd2ee" style="background-color:#bcd2ee;border:1px solid #bcd2ee;"></div>
                     </li>
                    
                     <li answer-value="-416" class="other-colors">
                        <div class="pallet" color="#576331" style="background-color:#576331;border:1px solid #576331;"></div>
                     </li>
                     <li answer-value="-417" class="other-colors">
                        <div class="pallet" color="#041531" style="background-color:#041531;border:1px solid #041531;"></div>
                     </li>
                     <li answer-value="-418" class="other-colors">
                        <div class="pallet" color="#f7b719" style="background-color:#f7b719;border:1px solid #f7b719;"></div>
                     </li><br>
                      <li class="sc-checkbox all-colors" style="width:220px !important;" answer-value="516">
                         <div class="title-3" color="I am a rainbow I like all colors">
                            <i class="icon icon-diamond"></i>I'm a rainbow, I like all colors
                               <input type="checkbox">
                         </div>
                      </li> 
                  </ul>

           

    <label id="coloravoid-error" class="message error hide" for="coloravoid">Please select a color</label>

</div>

                        <div class="container extrapad bag-print-avoid">

    <div class="box-pa">

        <div class="pa-que">

            <span>Prints and Patterns I <i style="color: #ed1b2e;">Avoid</i>

<!-- <i style="font-size:90%;">(Select 1 or more)</i> -->

</span>
    <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>

        </div>

    </div>

    <ul class="add-event list-colors list-prints list-dneed other-type bag-print-avoid" type="bag-print-avoid">

         <li answer-value="-198">
                        <div class="pallet">
                           <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/prints/polka.jpg');?>" alt="Polka dots">
                        </div>
                        <div class="title-3">polka dots</div>
                     </li>
                     <li answer-value="-184">
                        <div class="pallet">
                           <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/prints/floral.jpg');?>" alt="Floral">
                        </div>
                        <div class="title-3">floral</div>
                     </li>
                     <li answer-value="-199">
                        <div class="pallet">
                           <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/prints/stripes.jpg');?>" alt="Stripes">
                        </div>
                        <div class="title-3">stripes</div>
                     </li>
                     <li answer-value="-251">
                        <div class="pallet">
                           <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/prints/animal.jpg');?>" alt="Animal">
                        </div>
                        <div class="title-3">animal</div>
                     </li>
                     <li answer-value="-253">
                        <div class="pallet">
                           <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/prints/check.jpg');?>" alt="Checks">
                        </div>
                        <div class="title-3">checks</div>
                     </li>
                     <li answer-value="-255">
                        <div class="pallet">
                           <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/prints/solid.jpg');?>" alt="Solid">
                        </div>
                        <div class="title-3">solid</div>
                     </li>

    </ul>

    <label id="printavoid-error" class="message error hide" for="printavoid">Please select print to avoid</label>

</div>

                        

                        

                        

                    </div>
                  </div>

                    <div class="women_footwear_afterpayment category-footwear">

                        <div class="container extrapad">

                            <div class="box-pa">

                                <div class="pa-que">

                                    <span>You’re Looking For A Pair Of Heels! Could You Tell Us What Kind Of Heels You’d Like ?<br>The Type Of Heels I Am Looking For</span>
                                     <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                                </div>

                            </div>

                            <ul class="list-colors add-event footwear-heel-type" type="footwear-heel-type">

                                <li answer-value="389">

                                    <div class="pallet">

                                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/footwear/1wedge.jpg');?>">

                                    </div>

                                    <div class="title-3">wedge</div>

                                </li>

                                <li answer-value="390">

                                    <div class="pallet">

                                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/footwear/1block.jpg');?>">

                                    </div>

                                    <div class="title-3">block</div>

                                </li>

                                <li answer-value="391">

                                    <div class="pallet">

                                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/footwear/1stilettoes.jpg');?>">

                                    </div>

                                    <div class="title-3">stilletos</div>

                                </li>

                            </ul>

                            <label id="heeltype-error" class="message error hide" for="heeltype">Please select heel type</label>

                        </div>

                        <div class="product-item">
                             <div class="box-pa">

                            <div class="pa-que">

                                <span>  How Tall Would You Like Your Heels To be ?</span>
                                 <div class="title-section-changed text-center"  >
                                    <h1 class="title-changed">
                                       (Pick one)
                                    </h1>
                                 </div>
                            </div>

                            </div>
                            

                            <div class="item-sizes-wrp" id="sizeheelh">

                                <select class="form-control hwidth footwear-heel-height" type="footwear-heel-height">

                                    <option value="">Select heel height</option>

                                    <option value='low:1.5"-2.5"' answer-value="392">low: 1.5”-2.5”</option>

                                    <option value='medium:2.5-3.5"' answer-value="393">medium: 2.5”-3.5” </option>

                                    <option value='high:3.5" and above' answer-value="394">high: 3.5” and above</option>

                                </select>

                            </div>

                            <label id="sizeheelh-error" class="message error hide" for="sizeheelh">Please select heel height</label>

                        </div>

                    </div>

                    <div class="women_sunglasses_afterpayment category-accessories">

                        <div class="container accessories-sunglasses-container">

                            <div class="box-pa">

                                <div class="pa-que"><span>You Are Looking For Sunglasses ! Tell Us What Kind? <br>Sunglasses</span>
                                <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                                </div>

                            </div>

                            <ul class="add-event list-colors accessories-sunglasses-types" type="accessories-sunglasses-types">

                                <li answer-value="24">

                                    <div class="pallet">

                                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/sunglass/aviator.jpg');?>">

                                    </div>

                                    <div class="title-3">aviators</div>

                                </li>

                                <li answer-value="25">

                                    <div class="pallet">

                                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/sunglass/cat.jpg');?>">

                                    </div>

                                    <div class="title-3">cat eyes</div>

                                </li>

                                <li answer-value="26">

                                    <div class="pallet">

                                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/sunglass/round.jpg');?>">

                                    </div>

                                    <div class="title-3">round</div>

                                </li>

                                <li answer-value="27">

                                    <div class="pallet">

                                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/sunglass/square.jpg');?>">

                                    </div>

                                    <div class="title-3">square</div>

                                </li>

                                <li answer-value="28">

                                    <div class="pallet">

                                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/sunglass/wayfarer.jpg');?>">

                                    </div>

                                    <div class="title-3">wayfarers</div>

                                </li>

                            </ul>

                            <label id="sunglass-error" class="message error hide" for="sunglass">Please select atleast one sunglass</label>

                        </div>

                        <div class="product-item extrapad accessories-refelector-container">
                             <div class="box-pa">

                            <div class="pa-que">

                                <span> 
                                Reflectors</span>
                                 <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick yes or no)</h1>
                     </div>
                            </div>

                            </div>
                           

                            <ul class="add-event list-occassion purpose lespad accessories-refelector-types" type="accessories-refelector-types">

                                <li style="text-align: center;">

                                    <label class="sc-checkbox"  answer-value="533"><i class="icon icon-diamond"></i>

                                        <input type="checkbox"> <span>yes</span></label>

                                </li>

                                <li style="text-align: center;">

                                    <label class="sc-checkbox" answer-value="534"><i class="icon icon-diamond"></i>

                                        <input type="checkbox"> <span>no</span></label>

                                </li>

                            </ul>

                            <label id="reflector-error" class="message error hide" for="reflector">Please select atleast one</label>

                        </div>

                        <div class="container accessories-belt-container">

                            <div class="box-pa">

                                <div class="title-2" id="">

                                    <div class="pa-que">

                                        <span>You Are Looking For A Belt! Could You Tell Us What Kind Of Belt ?<br>Belts</span>
                                    <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                 </div>

                                </div>

                            </div>

                            <ul class="add-event accessories-belt-types list-colors" type="accessories-belt-types">

                                <li answer-value="371">

                                    <div class="pallet">

                                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/belts/1slim.jpg');?>">

                                    </div>

                                    <div class="title-3">slim</div>

                                </li>

                                <li answer-value="372">

                                    <div class="pallet">

                                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/belts/1medium.jpg');?>">

                                    </div>

                                    <div class="title-3">medium</div>

                                </li>

                                <li answer-value="373">

                                    <div class="pallet">

                                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/belts/1chunky.jpg');?>">

                                    </div>

                                    <div class="title-3">chunky</div>

                                </li>

                            </ul>

                            <label id="belts-error" class="message error hide" for="belts">Please select a belt</label>

                        </div>

                    </div>

                    

                    

                    <div class="women_jewellery_afterpayment category-jewellery">

                        <div class="container">

                            <div class="box-pa">

                                <div class="title-2" id="">

                                    <div class="pa-que"><span>

You Are Looking For Jewellery! Could You Tell Us The Tone Of Jewellery You Are Looking For

<!-- <i style="font-size:90%;">(Select 1 or more)</i> -->

<br>The Tone Of Jewellery I Am looking For</span>
 <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick 1 Or More)
                        </h1>
                     </div>
                     </div>

                                </div>

                            </div>

                            <ul class="add-event jewellery-tone list-colors" type="jewellery-tone">

                                <li answer-value="398">

                                    <div class="pallet">

                                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/jewellery/1gold.jpg');?>">

                                    </div>

                                    <div class="title-3">gold</div>

                                </li>

                                <li answer-value="399">

                                    <div class="pallet">

                                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/jewellery/1silver.jpg');?>">

                                    </div>

                                    <div class="title-3">silver</div>

                                </li>

                                <li answer-value="400">

                                    <div class="pallet">

                                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/jewellery/1oxidized.jpg');?>">

                                    </div>

                                    <div class="title-3">oxidised</div>

                                </li>

                                <li answer-value="401">

                                    <div class="pallet">

                                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/jewellery/1multicolored.jpg');?>">

                                    </div>

                                    <div class="title-3">multicolored</div>

                                </li>

                                <li answer-value="402">

                                    <div class="pallet">

                                        <img src="<?php echo base_url('assets/seventeen/images/pa/female/style/jewellery/1tonal.jpg');?>">

                                    </div>

                                    <div class="title-3">tonal</div>

                                </li>

                            </ul>

                            <label id="jewellerytone-error" class="message error hide" for="jewellerytone">Please select jewellery tone</label>

                        </div>

                    </div>

                    <div class="women_beauty_afterpayment category-beauty">

                        <div class="container extrapad">

                            <div class="title-2" id="">

                                <div class="pa-que"><span>You Are Looking For A Beauty Product! Could You Tell Us More ? <br>The Finish Of The Beauty Product I’m Looking For</span>
                               <div class="title-section-changed text-center"  >
                        <h1 class="title-changed">
                           (Pick one)
                        </h1>
                     </div>
                 </div>

                            </div>

                            <ul class="add-event list-occassion purpose lespad beauty-finishing-type" type="beauty-finishing-type">

                                <li style="text-align: center;">

                                    <label class="sc-checkbox"  answer-value="378"><i class="icon icon-diamond"></i>

                                        <input type="checkbox"> <span>matte</span></label>

                                </li>

                                <li style="text-align: center;">

                                    <label class="sc-checkbox"  answer-value="379"><i class="icon icon-diamond"></i>

                                        <input type="checkbox"> <span>shiny</span></label>

                                </li>

                            </ul>

                            <label id="finish-error" class="message error hide" for="finish">Please select the type of finish</label>

                        </div>

                    </div>

                </section>

            </div>

            <section class="section section-shadow">

                <div class="btns-wrp">

                   <!--  <a class="scbtn scbtn-primary" href="javascript:void(0);" onclick="ChangeStep(33);"><span>Previous</span></a> -->

                    <a class="scbtn scbtn-primary" href="javascript:void(0);" onclick="ChangeStep(5);"><span>Next</span></a>

                    <!-- <button class="scbtn scbtn-primary" onclick="slideprev(4);"><span>Previous</span></button>

<button class="scbtn scbtn-primary"  onclick="slidenext(4);"><span>Next</span></button> -->

                </div>

            </section>

    </div>

    </form>

    </div>

    <style>

        .scbox-payment-options li {

            display: inline-block;

            margin-right: 10px;

        }

        

        .scbox-payment-options .sc-checkbox .icon {

            position: relative;

            top: 10px !important;

        }

        

        input[type=radio] {

            display: none;

        }

    </style>

    <script>

        

        

        

        /*

        function AddCheckAttributes(type, container) {

            //##Select reflectors

            $(container).parent().find("li").each(function(e) {

                if (type == "scbox_reflectors") {

                    $(this).removeClass("active");

                    $(container).addClass('active');

                    var arr_list_reflector = $('.list-reflector li.active').map(function() {

                        return this.id;

                    }).get();

                }

                console.log(arr_list_reflector);

            });

            //##Select finish

            $(container).parent().find("li").each(function(e) {

                if (type == "scbox_finish") {

                    $(this).removeClass("active");

                    $(container).addClass('active');

                    var arr_list_finish = $('.list-finish li.active').map(function() {

                        //arr_list_coloravoid = [];

                        return this.id;

                    }).get();

                }

                console.log(arr_list_finish);

            });

        }



        function ShowHideprofession() {

            var ddlPassport = document.getElementById("scbox_profession");

            var dvPassport = document.getElementById("dvPassport");

            dvPassport.style.display = scbox_profession.value == "other" ? "block" : "none";

        }



        function ShowHideDiv() {

            var chkYes = document.getElementById("chkYes");

            var recipient_address = document.getElementById("recipient_address");

            recipient_address.style.display = chkYes.checked ? "block" : "none";

        }

        */

        

        //## SHOW HIDE CATEGORY CONTAINERS function initiated -------------

        document.addEventListener("DOMContentLoaded", function(event) { 

            msg("LOADED STEP 2ddddd...... page");

            ShowHideCategorySections();

            
            GetUserAnswerID();
            msg('arrUserAnswerAfterSelected');

        });

        

        

        // This is called with the results from from FB.getLoginStatus().

        function statusChangeCallback(response) {

            if (response.status === 'connected') {

                // Logged into your app and Facebook.

                testAPI();

            } else {

                // The person is not logged into your app or we are unable to tell.

                document.getElementById('status').innerHTML = 'Please log ' +

                    'into this app.';

            }

        }

        // This function is called when someone finishes with the Login

        // Button.  See the onlogin handler attached to it in the sample

        // code below.

        function checkLoginState() {

            FB.getLoginStatus(function(response) {

                statusChangeCallback(response);

            });

        }

        window.fbAsyncInit = function() {

            FB.init({

                appId: '1650358255198436',

                cookie: true, // enable cookies to allow the server to access

                // the session

                xfbml: true, // parse social plugins on this page

                version: 'v2.8' // use graph api version 2.8

            });

            // Now that we've initialized the JavaScript SDK, we call

            // FB.getLoginStatus().  This function gets the state of the

            // person visiting this page and can return one of three states to

            // the callback you provide.  They can be:

            //

            // 1. Logged into your app ('connected')

            // 2. Logged into Facebook, but not your app ('not_authorized')

            // 3. Not logged into Facebook and can't tell if they are logged into

            //    your app or not.

            //

            // These three cases are handled in the callback function.

            FB.getLoginStatus(function(response) {

                statusChangeCallback(response);

            });

        };

        // Load the SDK asynchronously

        (function(d, s, id) {

            var js, fjs = d.getElementsByTagName(s)[0];

            if (d.getElementById(id)) return;

            js = d.createElement(s);

            js.id = id;

            js.src = "//connect.facebook.net/en_US/sdk.js";

            fjs.parentNode.insertBefore(js, fjs);

        }(document, 'script', 'facebook-jssdk'));

        // Here we run a very simple test of the Graph API after login is

        // successful.  See statusChangeCallback() for when this call is made.

        function testAPI() {

            response = new Array();

            var accessToken = 0;

            FB.getLoginStatus(function(response) {

                if (response.status === 'connected') {

                    accessToken = response.authResponse.accessToken;

                }

            });

            $.ajax({

                url: 'https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=1650358255198436&client_secret=403b4cbf601b9a67b6594c7be781e8b9&fb_exchange_token=' + accessToken,

                type: 'get',

                data: {

                    'response': response

                },

                success: function(data, status) {

                    save_fb_token(data);

                }

            });

        }



        function save_fb_token(data) {

            $.ajax({

                url: sc_baseurl + 'Schome_new/save_fb_token',

                type: 'post',

                data: {

                    'data': data

                },

                success: function(data, status) {}

            });

        }



        function fb_login() {

            FB.login(function(response) {

                if (response.authResponse) {

                    checkLoginState();

                } else {

                    //user hit cancel button

                    console.log('User cancelled login or did not fully authorize.');

                }

            }, {

                scope: 'public_profile,email,user_photos'

            });

        }

       
    </script>