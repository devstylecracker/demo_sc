<div class="page-order-summary">
<section class="section section-shadow text-center">
  <div class="container">
    <div class="title-section">
      <h1 class="title">Order Summary</h1>
    </div>
    <div>
    <p>
      Your order <strong>#45662</strong> has been confirmed. <br />
Below are the details of your order. Your order will be delivered to 2 - 3 working days.
    </p>
    </div>
  </div>
</section>
<section class="section section-shadow page-order-summary">
  <div class="container">
    <div class="title-2">Order #987576</div>
    <div class="title-2">Address</div>
      <div>
    <p>
      899, Dedric Point Apt. 353, Aaram Society Road, Vakola,<br />
  Santacruz (East), Mumbai
    </p>
    </div>
  </div>
</section>
<section class="section section-shadow">
  <div class="container">
    <div class="order-items">
          <div class="item">
                          <div class="row">
                            <div class="col-md-1 col-sm-2 col-xs-2">
                              <div class="item-img-wrp">
                                <img src="http://192.168.1.68/projectbitb/assets/images/demo/p.jpeg" alt="" style="width:50px;height:70px;">
                              </div>
                            </div>
                            <div class="col-md-7 col-sm-6 col-xs-6">
                              <div class="item-desc-wrp">
                                <div class="title">Geometric Print Bodycon</div>
                                <div class="brand">By  Buy Apparel </div>
                                <span class="size-wrp"><label>Size: </label>FREE </span>
                                <span class="size-wrp"><label>Quantity: </label>1</span>
                              </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 text-right">
                                <div class="item-price-wrp">
                                  <div class="price"><i class="fa fa-inr"></i> 1304.00</div>
                              </div>
                            </div>

                          </div>
                        </div>



                        <ul class="order-total">
                    <li>
                      <span class="text-label">Your Total</span>
                      <span class="value"> <span class="fa fa-inr"></span> 4,300.00</span>
                    </li>
                    <li>
                      <span class="text-label">Coupon Discount</span>
                      <span class="value"> <span class="fa fa-inr"></span> 0.00 </span>
                    </li>
                    <li>
                      <span class="text-label">VAT</span>
                      <span class="value"> <span class="fa fa-inr"></span> 4,300.00</span>
                    </li>
                    <li>
                      <span class="text-label">Referral Amount</span>
                      <span class="value"> <span class="fa fa-inr"></span> 4,300.00</span>
                    </li>
                    <li>
                      <span class="text-label">Delivery Charges</span>
                      <span class="value"> <span class="fa fa-inr"></span> 4,300.00</span>
                    </li>
                    <li>
                      <span class="text-label text-right">Total:</span>
                      <span class="value sc-color"> <span class="fa fa-inr"></span> 4,300.00</span>
                    </li>
                  </ul>


                          </div>
  </div>
</section>
</div>
