<section class="section section-shadow section-collections">
<div class="container">
  <div class="title-section">
    <h1 class="title">Collections</h1>
    <a href="#" class="link-view-all">View All Collections</a>
  </div>
  <div class="grid grid-items grid-items-slider sm-slider">
    <div class="row grid-row grid-section-items11 grid-items-slider-inner111">
      <div class="col-md-3 col-sm-12 col-xs-12 grid-col grid-item222">
        <div class="item-wrp mask item-view-wrp222">
          <div class="item item-hover-mask222">
            <div class="item-img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/demo/1.jpg" alt="">
            </div>
              <div class="item-desc-wrp mask222">
                  <h2 class="title">The Home Collection</h2>
                  <div class="desc">Cras in diam volutpat metus dapibus ultrices id sed ipsum</div>
                  <div class="btns-wrp">
                    <a href="#" class="btn btn-sm button btn-primary">View Collection</a>
                  </div>
                  <div class="desc-2">Recommended collection by <strong>Head Stylist, Priti Chandra</strong></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-12 col-xs-12 grid-item">
        <div class="item-view-wrp">
          <div class="item item-hover-mask">
            <img src="<?php echo base_url(); ?>assets/images/demo/2.jpg" alt="">
            <div class="mask">
              <div class="inner">
                <div class="top">
                  <h2 class="title">The Home Collection</h2>
                  <div class="desc">Cras in diam volutpat metus dapibus ultrices id sed ipsum</div>
                </div>
                <div class="bottom">
                  <div class="btns-wrp">
                    <a href="#" class="btn btn-sm button btn-primary">View Collection</a>
                  </div>
                  <div class="desc-2">Recommended collection by <strong>Head Stylist, Priti Chandra</strong></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-12 col-xs-12 grid-item">
        <div class="item-view-wrp">
          <div class="item item-hover-mask">
            <img src="<?php echo base_url(); ?>assets/images/demo/13.jpg" alt="">
            <div class="mask">
              <div class="inner">
                <div class="top">
                  <h2 class="title">The Home Collection</h2>
                  <div class="desc">Cras in diam volutpat metus dapibus ultrices id sed ipsum</div>
                </div>
                <div class="bottom">
                  <div class="btns-wrp">
                    <a href="#" class="btn btn-sm button btn-primary">View Collection</a>
                  </div>
                  <div class="desc-2">Recommended collection by <strong>Head Stylist, Priti Chandra</strong></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-12 col-xs-12 grid-item">
        <div class="item-view-wrp">
          <div class="item item-hover-mask">
            <img src="<?php echo base_url(); ?>assets/images/demo/12.jpg" alt="">
            <div class="mask">
              <div class="inner">
                <div class="top">
                  <h2 class="title">The Home Collection</h2>
                  <div class="desc">Cras in diam volutpat metus dapibus ultrices id sed ipsum</div>
                </div>
                <div class="bottom">
                  <div class="btns-wrp">
                    <a href="#" class="btn btn-sm button btn-primary">View Collection</a>
                  </div>
                  <div class="desc-2">Recommended collection by <strong>Head Stylist, Priti Chandra</strong></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>

<section class="section section-shadow">
  <div class="container">
    <div class="title-section">
      <h1 class="title">Products</h1>
    </div>
<!--test-->
<div class="grid grid-items products">
  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="item-wrp">
        <div class="item">
          <div class="item-img-wrp">
            <img src="<?php echo base_url(); ?>assets/images/demo/20.jpg" alt="">
          </div>
          <div class="item-desc-wrp">
                <div class="category">Dresses</div>
                <div class="title">Geometric Print Bodycon Dress</div>
                <div class="brand">The Label Life</div>
                <div class="item-price-wrp">
                    <div class="mrp">Rs. 9862</div>
                    <div class="price">Rs. 9862</div>
                    <div class="discount">12%</div>
                </div>
                <div class="date">29th Jan, 2017</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

    <!-- -->
    <div class="grid items-wrp">
      <div class="row grid-items-wrp">
        <div class="col-md-3 col-sm-12 col-xs-12 grid-item">
          <div class="item-view-wrp">
            <div class="item item-product">
              <div class="item-img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/demo/20.jpg" alt="">
            </div>
              <div class="item-desc-wrp">
                    <div class="item-category">Geometric Print Bodycon Dress</div>
                    <div class="item-title">Geometric Print Bodycon Dress</div>
                    <div class="brand">Geometric Print Bodycon Dress</div>
                    <div class="price">Geometric Print Bodycon Dress</div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12 grid-item">
          <div class="item-view-wrp">
            <div class="item item-product">
              <div class="item-img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/demo/21.jpg" alt="">
            </div>
              <div class="item-desc-wrp">
                    <div class="item-title">Ritu Advani & Maiti Shani - Cream Web Embroidered Dress</div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12 grid-item">
          <div class="item-view-wrp">
            <div class="item item-product">
              <div class="item-img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/demo/23.jpg" alt="">
            </div>
              <div class="item-desc-wrp">
                    <div class="item-title">Geometric Print Bodycon Dress</div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12 grid-item">
          <div class="item-view-wrp">
            <div class="item item-product">
              <div class="item-img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/demo/28.jpg" alt="">
            </div>
              <div class="item-desc-wrp">
                    <div class="item-title">Ritu Advani & Maiti Shani - Cream Web Embroidered Dress</div>
              </div>
            </div>
          </div>
        </div>


      </div>
    </div>
  </div>
</section>
