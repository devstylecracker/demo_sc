<section class="section section-shadow">
  <div class="container">
    <div class="page-product">
      <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Winter Collection</a></li>
        <li><a href="#">Dress</a></li>
        <li class="active">Geometric Print Bodycon</li>
      </ol>
      <div class="product-item">

        <div class="row">
          <!-- product image -->
          <div class="col-md-5 col-sm-4 col-xs-12 zoom-gallery-wrp">
            <div class="zoom-gallery-wrp-inner111" style=" position:relative;">
              <div class="slider-top product">
                <div>
                  <div class="img-wrp zoom">
                    <img src="http://www.stylecracker.com/sc_admin/assets/products/14649566074787.JPG" data-zoom-image="http://www.stylecracker.com/sc_admin/assets/products/14649566074787.JPG">
                  </div>
                </div>
                <div>
                  <div class="img-wrp">
                    <img src="https://www.stylecracker.com/sc_admin/assets/products_extra/1453454902264_sc_2.jpg" data-zoom-image="https://www.stylecracker.com/sc_admin/assets/products_extra/1453454902264_sc_2.jpg">
                  </div>
                </div>
              </div>
            </div>
            <!--  <div id="zoom-place-holder" class="zoom-place-holder"> </div>-->
            <!-- thumb -->
            <div class="slider-thumbs product">
              <div class="slide">
                <div class="item-img-inner">
                  <img src="http://www.stylecracker.com/sc_admin/assets/products/14649566074787.JPG">
                </div>
              </div>
              <div class="slide">
                <div class="item-img-inner">
                  <img src="https://www.stylecracker.com/sc_admin/assets/products_extra/1453454902264_sc_2.jpg">
                </div>
              </div>
            </div>

            <!-- /thumb -->
          </div>
          <!-- /. product image -->
          <div class="col-md-7 col-sm-8 col-xs-12">
            <div class="item-desc-wrp">
              <h1 class="title">Geometric Print Bodycon</h1>
              <div class="brand">By <a href="#">The Label Life</a></div>
              <div class="item-price-wrp">
                <div class="price"><i class="fa fa-inr"></i> 9862</div>
              </div>
              <div class="item-colors-wrp">
                <span class="btn-color bg-info"></span>
                <span class="btn-color bg-danger"></span>
                <span class="btn-color bg-success"></span>
                <span class="btn-color bg-danger"></span>
              </div>

              <div class="item-sizes-wrp">
                <span class="btn-diamond btn-size">S</span>
                <span class="btn-diamond btn-size">M</span>
                <span class="btn-diamond btn-size">XXL</span>
                <span class="btn-diamond btn-size">XL</span>
              </div>

              <div class="size-chart">
                <a href="#" class="size-chart111">View size chart</a>
              </div>
              <div class="btns-wrp">
                <button class="sc-btn"><span>Add to Cart</span></button>
                <button class="sc-btn"><span>Share with Stylist</span></button>
              </div>

              <h2 class="subtitle">
                  Description
                </h2>
              <div class="desc">
                Bacon ipsum dolor amet pork beef tail, meatball venison pork loin hamburger. Prosciutto beef ribs swine jerky burgdoggen ribeye, cupim hamburger flank doner. Prosciutto beef ribs swine jerky burgdoggen ribeye, cupim hamburger flank doner. cupim hamburger
                flank doner. Prosciutto beef ribs swine jerky burgdoggen ribeye, cupim hamburger flank doner. cupim hamburger flank doner. Prosciutto beef ribs swine jerky burgdoggen ribeye, cupim hamburger flank doner.
              </div>
              <div class="read-more">
                <a href="#">Show more details</a>
              </div>

              <!--   <div class="item-media-wrp">
                   <span class="ic ic-plus">
                 <i class="fa fa-plus"></i>
               </span>
                   <span class="ic ic-share">
                 <i class="fa fa-paper-plane-o"></i>
               </span>
             </div>-->

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section section-shadow">
  <div class="container">
    <div class="title-section">
      <h1 class="title">Recommended Products</h1>
      <a href="#" class="link-view-all">View All Products</a>
    </div>
    <div class="grid grid-items products">
      <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-6 grid-col">
          <div class="item-wrp">
            <div class="item">
              <div class="item-img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/demo/p.jpeg" alt="">
              </div>
              <div class="item-desc-wrp">
                <div class="category"><a href="#">Dresses</a></div>
                <div class="title">Geometric Print Bodycon Dress</div>
                <div class="brand">By <a href="#">The Label Life</a></div>
                <div class="item-price-wrp">
                  <div class="price"><i class="fa fa-inr"></i> 9862</div>
                </div>
              </div>
              <div class="item-media-wrp">
                <span class="ic ic-plus">
              <i class="fa fa-plus"></i>
            </span>
                <span class="ic ic-share">
              <i class="fa fa-paper-plane-o"></i>
            </span>
              </div>
              <span class="ic-heart">
            <i class="fa fa-heart-o"></i>
          </span>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 grid-col">
          <div class="item-wrp">
            <div class="item">
              <div class="item-img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/demo/22.jpg" alt="">
              </div>
              <div class="item-desc-wrp">
                <div class="category"><a href="#">Dresses</a></div>
                <div class="title">Geometric Print Bodycon Dress</div>
                <div class="brand">By <a href="#">The Label Life</a></div>
                <div class="item-price-wrp">
                  <div class="mrp"><i class="fa fa-inr"></i> 9862</div>
                  <div class="price"><i class="fa fa-inr"></i> 9862</div>

                </div>
              </div>
              <div class="item-media-wrp">
                <span class="ic ic-plus">
              <i class="fa fa-plus"></i>
            </span>
                <span class="ic ic-share">
              <i class="fa fa-paper-plane-o"></i>
            </span>
              </div>
              <span class="ic-heart">
            <i class="fa fa-heart-o"></i>
          </span>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 grid-col">
          <div class="item-wrp">
            <div class="item">
              <div class="item-img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/demo/21.jpg" alt="">
              </div>
              <div class="item-desc-wrp">
                <div class="category"><a href="#">Dresses</a></div>
                <div class="title">Geometric Print Bodycon Dress</div>
                <div class="brand">By <a href="#">The Label Life</a></div>
                <div class="item-price-wrp">
                  <div class="mrp"><i class="fa fa-inr"></i> 9862</div>
                  <div class="price"><i class="fa fa-inr"></i> 9862</div>
                  <div class="discount">12% OFF</div>
                </div>
              </div>
              <div class="item-media-wrp">
                <span class="ic ic-plus">
              <i class="fa fa-plus"></i>
            </span>
                <span class="ic ic-share">
              <i class="fa fa-paper-plane-o"></i>
            </span>
              </div>
              <span class="ic-heart">
            <i class="fa fa-heart-o"></i>
          </span>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 grid-col">
          <div class="item-wrp">
            <div class="item">
              <div class="item-img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/demo/20.jpg" alt="">
              </div>
              <div class="item-desc-wrp">
                <div class="category"><a href="#">Dresses</a></div>
                <div class="title">Geometric Print Bodycon Dress</div>
                <div class="brand">By <a href="#">The Label Life</a></div>
                <div class="item-price-wrp">
                  <div class="mrp"><i class="fa fa-inr"></i> 9862</div>
                  <div class="price"><i class="fa fa-inr"></i> 9862</div>
                  <div class="discount">12% OFF</div>
                </div>
              </div>
              <div class="item-media-wrp">
                <span class="ic ic-plus">
              <i class="fa fa-plus"></i>
            </span>
                <span class="ic ic-share">
              <i class="fa fa-paper-plane-o"></i>
            </span>
              </div>
              <span class="ic-heart">
            <i class="fa fa-heart-o"></i>
          </span>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>
<section class="section section-shadow">
  <div class="container">
    <div class="title-section">
      <h1 class="title">Review</h1>
    </div>
    <div class="product-review">
      <ul>
        <li>
          <div class="ratings">
            <span class="shape-diamond">5</span>
          </div>
          <div class="desc">
            Bacon ipsum dolor amet pork beef tail, meatball venison pork loin hamburger. Prosciutto beef ribs swine jerky burgdoggen ribeye, cupim hamburger flank doner. Prosciutto beef ribs swine jerky burgdoggen ribeye, cupim hamburger flank doner. cupim hamburger
            flank doner.
          </div>
          <div class="meta">Jack Nicholson | 12th May 2016 <i class="fa fa-heart-o"></i></div>
        </li>
        <li>
          <div class="ratings">
            <span class="shape-diamond">5</span>
          </div>
          <div class="desc">
            Bacon ipsum dolor amet pork beef tail, meatball venison pork loin hamburger. Prosciutto beef ribs swine jerky burgdoggen ribeye, cupim hamburger flank doner. Prosciutto beef ribs swine jerky burgdoggen ribeye, cupim hamburger flank doner. cupim hamburger
            flank doner.
          </div>
          <div class="meta">Jack Nicholson | 12th May 2016 <i class="fa fa-heart-o"></i></div>
        </li>
      </ul>
    </div>
    <div class="text-center">
      <a class="btn btn-sm button btn-primary" data-toggle="collapse" data-target="#section-write-review">Rate & Review</a>
      <a href="#" class="btn btn-sm button btn-secondary">Show More</a>
    </div>
  </div>
</section>

<section id="section-write-review" class="section section-shadow section-write-review collapse">
  <div class="container">
    <div class="title-section">
      <h1 class="title">Rate & Review the product</h1>
    </div>

    <h3 class="subtitle">Rate this product</h3>
    <ul class="btn-diamond-group rate-product">
      <li class="btn-diamond btn-rating">1</li>
      <li class="btn-diamond btn-rating">2</li>
      <li class="btn-diamond btn-rating">3</li>
      <li class="btn-diamond btn-rating">4</li>
      <li class="btn-diamond btn-rating">5</li>
    </ul>

    <h3 class="subtitle">Review this product</h3>
    <div class="product-write-review">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Name" />
      </div>
      <div class="form-group">
        <textarea class="form-control" placeholder="Write a comment..."></textarea>
      </div>
      <div class="text-center">
        <button class="btn btn-primary">Submit</button>
      </div>
    </div>

  </div>
</section>
