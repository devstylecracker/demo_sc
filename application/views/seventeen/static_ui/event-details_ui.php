<section class="section section-shadow">
  <div class="container">
    <div class="page-product">
      <ol class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="#">Events</a></li>
    <li class="active">5 PAIRS OF FOOTWEAR THAT ARE COOL</li>
  </ol>
        <div class="event-item">
          <div class="row">
                <!-- event image -->
            <div class="col-md-5 col-sm-4 col-xs-12">
                <div class="item-img-wrp img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/demo/2.jpg">

              <span class="ic-heart sc-heart">
            <i class="fa fa-heart-o"></i>
          </span>
  </div>
            </div>
              <!-- /. event image -->
            <div class="col-md-7 col-sm-8 col-xs-12">
              <div class="item-desc-wrp">
                <h1 class="title">5 PAIRS OF FOOTWEAR THAT ARE COOL</h1>
                <div class="date">5:00 PM, Taj Vivanta, Banglore</div>
                <div class="item-btns-wrp">
                  <a href="#" class="sc-btn"><span>Attend the Event</span></a>
                  <span class="ic ic-share">
                    <i class="fa fa-paper-plane-o"></i>
                </span>
                <span class="ic">
                  <i class="fa fa-share"></i>
              </span>
                </div>

                <div class="desc">
                  Bacon ipsum dolor amet pork beef tail, meatball venison pork loin hamburger. Prosciutto beef ribs swine jerky burgdoggen ribeye, cupim hamburger flank doner. Prosciutto beef ribs swine jerky burgdoggen ribeye, cupim hamburger flank doner. cupim hamburger flank doner. Prosciutto beef ribs swine jerky burgdoggen ribeye, cupim hamburger flank doner. cupim hamburger flank doner. Prosciutto beef ribs swine jerky burgdoggen ribeye, cupim hamburger flank doner.
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
</section>
<section class="section section-shadow">
  <div class="container">
    <div class="title-section">
      <h1 class="title">Clothes for this event</h1>
      <a href="#" class="link-view-all">View All Products</a>
    </div>
    <div class="grid grid-items products">
      <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-6 grid-col">
          <div class="item-wrp">
            <div class="item">
              <div class="item-img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/demo/p.jpeg" alt="">
              </div>
              <div class="item-desc-wrp">
                <div class="category"><a href="#">Dresses</a></div>
                <div class="title">Geometric Print Bodycon Dress</div>
                <div class="brand">By <a href="#">The Label Life</a></div>
                <div class="item-price-wrp">
                  <div class="price"><i class="fa fa-inr"></i> 9862</div>
                </div>
              </div>
              <div class="item-media-wrp">
                <span class="ic ic-plus">
              <i class="fa fa-plus"></i>
            </span>
                <span class="ic ic-share">
              <i class="fa fa-paper-plane-o"></i>
            </span>
              </div>
              <span class="ic-heart">
            <i class="fa fa-heart-o"></i>
          </span>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 grid-col">
          <div class="item-wrp">
            <div class="item">
              <div class="item-img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/demo/22.jpg" alt="">
              </div>
              <div class="item-desc-wrp">
                <div class="category"><a href="#">Dresses</a></div>
                <div class="title">Geometric Print Bodycon Dress</div>
                <div class="brand">By <a href="#">The Label Life</a></div>
                <div class="item-price-wrp">
                  <div class="mrp"><i class="fa fa-inr"></i> 9862</div>
                  <div class="price"><i class="fa fa-inr"></i> 9862</div>

                </div>
              </div>
              <div class="item-media-wrp">
                <span class="ic ic-plus">
              <i class="fa fa-plus"></i>
            </span>
                <span class="ic ic-share">
              <i class="fa fa-paper-plane-o"></i>
            </span>
              </div>
              <span class="ic-heart">
            <i class="fa fa-heart-o"></i>
          </span>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 grid-col">
          <div class="item-wrp">
            <div class="item">
              <div class="item-img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/demo/21.jpg" alt="">
              </div>
              <div class="item-desc-wrp">
                <div class="category"><a href="#">Dresses</a></div>
                <div class="title">Geometric Print Bodycon Dress</div>
                <div class="brand">By <a href="#">The Label Life</a></div>
                <div class="item-price-wrp">
                  <div class="mrp"><i class="fa fa-inr"></i> 9862</div>
                  <div class="price"><i class="fa fa-inr"></i> 9862</div>
                    <div class="discount">12% OFF</div>
                </div>
              </div>
              <div class="item-media-wrp">
                <span class="ic ic-plus">
              <i class="fa fa-plus"></i>
            </span>
                <span class="ic ic-share">
              <i class="fa fa-paper-plane-o"></i>
            </span>
              </div>
              <span class="ic-heart">
            <i class="fa fa-heart-o"></i>
          </span>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 grid-col">
          <div class="item-wrp">
            <div class="item">
              <div class="item-img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/demo/20.jpg" alt="">
              </div>
              <div class="item-desc-wrp">
                <div class="category"><a href="#">Dresses</a></div>
                <div class="title">Geometric Print Bodycon Dress</div>
                <div class="brand">By <a href="#">The Label Life</a></div>
                <div class="item-price-wrp">
                  <div class="mrp"><i class="fa fa-inr"></i> 9862</div>
                  <div class="price"><i class="fa fa-inr"></i> 9862</div>
                    <div class="discount">12% OFF</div>
                </div>
              </div>
              <div class="item-media-wrp">
                <span class="ic ic-plus">
              <i class="fa fa-plus"></i>
            </span>
                <span class="ic ic-share">
              <i class="fa fa-paper-plane-o"></i>
            </span>
              </div>
              <span class="ic-heart">
            <i class="fa fa-heart-o"></i>
          </span>
            </div>
          </div>
        </div>
       </div>
    </div>

  </div>
</section>
