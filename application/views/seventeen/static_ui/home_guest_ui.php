<div class="page-home-guest">
  <section class="section section-shadow section-main-slider">
    <div class="container-fluid111">
      <div class="home-banner-wrp">
        <div class="swiper-container swiper-container-front front main">
          <ul class="swiper-wrapper">
            <li class="swiper-slide slide">
              <img src="https://www.stylecracker.com/sc_admin/assets/banners/default-home-5.jpg">
            </li>
            <li class="swiper-slide">
              <img src="https://www.stylecracker.com/sc_admin/assets/banners/1483093501.jpg">
            </li>
            <li class="swiper-slide">
              <img src="https://www.stylecracker.com/sc_admin/assets/banners/1483093535.jpg">
            </li>
          </ul>

          <!--  <div class="swiper-button-next front main"></div>
      <div class="swiper-button-prev front main"></div>-->
          <div class="swiper-pagination front main"></div>
        </div>
      </div>

    </div>
  </section>
  <section class="section section-shadow section-collections">
    <div class="container">
      <div class="grid grid-items grid-items-slider sm-slider">
        <div class="row grid-row">
          <div class="col-md-6 col-sm-12 col-xs-12 grid-col">
            <div class="item-wrp overlay">
              <div class="item">
                <div class="item-img-wrp">
                  <img src="<?php echo base_url(); ?>sc_admin/assets/event_img/9.jpg" alt="">
                </div>
                <div class="item-desc-wrp">
                  <div class="inner">
                    <h2 class="title">collection name</h2>
                    <div class="desc">
                      cover desc </div>
                    <div class="btns-wrp">
                      <a href="#" class="scbtn scbtn-secondary white"><span>View Collection</span></a>
                    </div>
                  </div>
                </div>
                <span class="ic-heart sc-heart active">
					  <i class="icon-heart"></i>
					</span>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-12 col-xs-12 grid-col">
            <div class="item-wrp overlay">
              <div class="item">
                <div class="item-img-wrp">
                  <img src="<?php echo base_url(); ?>sc_admin/assets/event_img/9.jpg" alt="">
                </div>
                <div class="item-desc-wrp">
                  <div class="inner">
                    <h2 class="title">collection name</h2>
                    <div class="desc">
                      cover desc </div>
                    <div class="btns-wrp">
                      <a href="#" class="scbtn scbtn-secondary white"><span>View Collection</span></a>
                    </div>
                  </div>
                </div>
                <span class="ic-heart sc-heart active">
                      <i class="icon-heart"></i>
                    </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <section class="section section-shadow section-slider-verticle home-slider-verticle">
    <div>
      <div class="swiper-container front verticle">
        <div class="container swiper-pagination-wrp">
          <div class="swiper-pagination verticle"></div>
        </div>
        <ul class="swiper-wrapper">
          <li class="swiper-slide slide">
            <div class="container">
              <div class="inner inner-benefits">
                <div class="row">
                  <div class="col-md-4 col-sm-4 col-xs-3">
                    <div class="img-wrp">
                      <img src="<?php echo base_url(); ?>assets/images/home/banner/how-it-works-3-2.jpg" />
                    </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-9">
                    <div class="content">
                      <div class="title-section">
                        <h3 class="title">Benefits</h3>
                      </div>
                      <div class="desc">
                        <ul>
                          <li><i class="icon-user"></i>Personalised selections</li>
                          <li><i class="icon-bag"></i>Stay up to date on fashion</li>
                          <li><i class="icon-setting"></i>The more you use us, the better we get</li>
                          <li><i class="icon-heart"></i>Exclusive and popular brands you will love</li>
                          <li><i class="icon-chat"></i>Celebrity stylists 24x7</li>

                        </ul>
                      </div>
                      <a data-toggle="modal" href="#myModal" class="scbtn scbtn-primary" title="Get Styled"><span>Get Styled</span></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li class="swiper-slide slide">
            <div class="container">
              <div class="inner inner-how-it-works">
                <div class="row">
                  <div class="col-md-4 col-sm-4 col-xs-3">
                    <div class="img-wrp">
                      <img src="<?php echo base_url(); ?>assets/images/home/banner/how-it-works-3-1.jpg" />
                    </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-9">
                    <div class="content">
                      <div class="title-section">
                        <h3 class="title">How it works</h3>
                      </div>
                      <div class="desc">
                        <ul>
                          <li>
                            <span class="text-bold">Step 1:</span> Take a 30 second personal style quiz
                          </li>
                          <li>
                            <span class="text-bold">Step 2:</span> Browse looks and products on your personal feed created for you by celebrity stylists </li>
                          <li>
                            <span class="text-bold">Step 3:</span> Shop different!
                          </li>
                        </ul>
                      </div>
                      <a data-toggle="modal" href="#myModal" class="scbtn scbtn-primary" title="Get Started"><span>Get Started</span></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>

          <li class="swiper-slide slide">
            <div class="container">
              <div class="inner inner-outfit">
                <div class="row">
                  <div class="col-md-4 col-sm-4 col-xs-3">
                    <div class="img-wrp">
                      <img src="<?php echo base_url(); ?>assets/images/home/banner/how-it-works-3-3.jpg" />
                    </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-9">
                    <div class="content">
                      <div class="title-section">
                        <h3 class="title">Life isn't perfect, <br /> but your outfit can be</h3>
                      </div>
                      <p class="desc">
                        For busy women & men on the go, StyleCracker acts as your style concierge to pick the best fashion, beauty and grooming tailored to your taste, budget and lifestyle, delivered to your doorstep, for free!
                      </p>
                      <a data-toggle="modal" href="#myModal" class="scbtn scbtn-primary" title="Get Stylish"><span>Get Stylish</span></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </section>
  <section class="section section-shadow section-work">
    <div class="container">
      <div class="section-work-inner">
        <div class="row">
          <div class="col-md-5">
            <div class="desc">
              <div class="title-section">
                <h2 class="title">See how we work it</h2>
              </div>
              <p class="subtitle">
                We style the stars. Want to look like a celebrity?
              </p>
              <a data-toggle="modal" href="#myModal" class="scbtn scbtn-primary"><span>Sign Up Now</span></a>
            </div>
          </div>

          <div class="col-md-7">

            <div class="swiper-container work">
              <ul class="swiper-wrapper">
                <li class="swiper-slide slide">
                  <img src="<?php echo base_url(); ?>assets/images/home/banner/work-ranbir-kapoor-filmfare.jpg" />
                </li>
                <li class="swiper-slide slide">
                  <img src="<?php echo base_url(); ?>assets/images/home/banner/work-shaandaar.jpg" />
                </li>
                <li class="swiper-slide slide">
                  <img src="<?php echo base_url(); ?>assets/images/home/banner/work-filmfare.jpg" />
                </li>
                <li class="swiper-slide slide">
                  <img src="<?php echo base_url(); ?>assets/images/home/banner/work-kapoor-n-sons.jpg" />
                </li>
                <li class="swiper-slide slide">
                  <img src="<?php echo base_url(); ?>assets/images/home/banner/work-ellina.jpg" />
                </li>

              </ul>
              <div class="swiper-pagination work"></div>
              <div class="swiper-button-next front work"></div>
              <div class="swiper-button-prev front work"></div>
            </div>


          </div>
        </div>
      </div>
    </div>
  </section>


  <section class="section section-shadow section-products">
    <div class="container">
      <div class="title-section">
        <h1 class="title">RIGHT OF THE RACK</h1>
        <a href="#" class="link-view-all">view all products</a>
      </div>
      <div class="grid grid-items products swiper-container front">
        <ul class="row swiper-wrapper">
          <li class="col-md-3 col-sm-6 col-xs-6 grid-col swiper-slide slide">
            <div class="item-wrp">
              <div class="item">
                <div class="item-img-wrp">
                  <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_310/14596011057367.png" alt="">
                </div>
                <div class="item-desc-wrp">
                  <div class="category">Others-153</div>
                  <div class="title">Deep Blue Embellished Crop Top paired with Metallic Brocade Skirt</div>
                  <div class="brand">By <a href="http://192.168.1.68/projectbitb/brands/brand-details/test">KALKI</a></div>
                  <div class="item-price-wrp">
                    <div class="price"><i class="fa fa-inr"></i>15990</div>
                  </div>
                </div>
                <span class="ic-heart">
  					<a title="Add to Wish List">
  					  <i class="icon-heart"></i>
  					 </a>
  				</span>
              </div>
            </div>
          </li>
          <li class="col-md-3 col-sm-6 col-xs-6 grid-col swiper-slide slide">
            <div class="item-wrp">
              <div class="item">
                <div class="item-img-wrp">
                  <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_310/14596011057367.png" alt="">
                </div>
                <div class="item-desc-wrp">
                  <div class="category">Others-153</div>
                  <div class="title">Deep Blue Embellished Crop Top paired with Metallic Brocade Skirt</div>
                  <div class="brand">By <a href="http://192.168.1.68/projectbitb/brands/brand-details/test">KALKI</a></div>
                  <div class="item-price-wrp">
                    <div class="price"><i class="fa fa-inr"></i>15990</div>
                  </div>
                </div>
                <span class="ic-heart">
          <a title="Add to Wish List">
            <i class="icon-heart"></i>
           </a>
        </span>
              </div>
            </div>
          </li>
          <li class="col-md-3 col-sm-6 col-xs-6 grid-col swiper-slide slide">
            <div class="item-wrp">
              <div class="item">
                <div class="item-img-wrp">
                  <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_310/14596011057367.png" alt="">
                </div>
                <div class="item-desc-wrp">
                  <div class="category">Others-153</div>
                  <div class="title">Deep Blue Embellished Crop Top paired with Metallic Brocade Skirt</div>
                  <div class="brand">By <a href="http://192.168.1.68/projectbitb/brands/brand-details/test">KALKI</a></div>
                  <div class="item-price-wrp">
                    <div class="price"><i class="fa fa-inr"></i>15990</div>
                  </div>
                </div>
                <span class="ic-heart">
        <a title="Add to Wish List">
          <i class="icon-heart"></i>
         </a>
      </span>
              </div>
            </div>
          </li>
          <li class="col-md-3 col-sm-6 col-xs-6 grid-col swiper-slide slide">
            <div class="item-wrp">
              <div class="item">
                <div class="item-img-wrp">
                  <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_310/14596011057367.png" alt="">
                </div>
                <div class="item-desc-wrp">
                  <div class="category">Others-153</div>
                  <div class="title">Deep Blue Embellished Crop Top paired with Metallic Brocade Skirt</div>
                  <div class="brand">By <a href="http://192.168.1.68/projectbitb/brands/brand-details/test">KALKI</a></div>
                  <div class="item-price-wrp">
                    <div class="price"><i class="fa fa-inr"></i>15990</div>
                  </div>
                </div>
                <span class="ic-heart">
      <a title="Add to Wish List">
        <i class="icon-heart"></i>
       </a>
    </span>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </section>
  <section class="section section-shadow">
    <div class="container">
      <div class="title-section">
        <h1 class="title">TESTIMONIALS</h1>
      </div>
      <div class="box-testimonial">
        <div class="row">
          <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="img-wrp">
              <img src="https://www.stylecracker.com/assets/images/home/banner/ramona.jpg" alt="">
            </div>
          </div>
          <div class="col-md-9 col-sm-8 col-xs-12">
            <div class="title">
              I FELL IN LOVE WITH MY NEW CLOTHES
            </div>
            <div class="desc">
              <p>Conversations can be a tricky business. Sometimes, decoding what is said with what is meant is difficult at best. However, communication is a necessary tool in todays world. And it’s not only speaking that can be difficult, but trying to
                interpret body language, and other language barriers are just a few of the obstacles barring effective communication. It’s often been the case were one party completely miscommunicates to another due to a misunderstanding between parties.
              </p>
              <p>
                Most people learn to talk when they are relatively young. Gaining in verbal capacity as the get older and engage in conversation with more and more people throughout their lives. And while most people will spend their time constantly speaking, some never
                really learn to listen, which is the king of effective communication. It may seem strange to hear such a thing as this, but it’s true.
              </p>
              <div class="name">
                - Katie Gomez
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="box-testimonial">
        <div class="row">
          <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="img-wrp">
              <img src="https://www.stylecracker.com/assets/images/home/banner/ramona.jpg" alt="">
            </div>
          </div>
          <div class="col-md-9 col-sm-8 col-xs-12">
            <div class="title">
              I FELL IN LOVE WITH MY NEW CLOTHES
            </div>
            <div class="desc">
              <p>Conversations can be a tricky business. Sometimes, decoding what is said with what is meant is difficult at best. However, communication is a necessary tool in todays world. And it’s not only speaking that can be difficult, but trying to
                interpret body language, and other language barriers are just a few of the obstacles barring effective communication. It’s often been the case were one party completely miscommunicates to another due to a misunderstanding between parties.
              </p>
              <p>
                Most people learn to talk when they are relatively young. Gaining in verbal capacity as the get older and engage in conversation with more and more people throughout their lives. And while most people will spend their time constantly speaking, some never
                really learn to listen, which is the king of effective communication. It may seem strange to hear such a thing as this, but it’s true.
              </p>
              <div class="name">
                - Katie Gomez
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section section-shadow section-media">
    <div class="container">
      <div class="title-section">
        <h3 class="title">Psst...they are talking about us</h3>
      </div>
      <ul>
        <li>
          <a href="https://www.youtube.com/watch?v=yyamOd7eaM0&feature=youtu.be" target="_blank">
          <img src="<?php echo base_url(); ?>assets/images/home/logos/logo-bloomberg-tv-india.png" />
          </a>
        </li>
        <li>
          <a href="http://archive.indianexpress.com/news/it-s-personal/1129558/0" target="_blank">
          <img src="<?php echo base_url(); ?>assets/images/home/logos/logo-ie.png" />
          </a>
        </li>
        <!--  <li>
            <a href="#" target="_blank">
          <img src="<?php echo base_url(); ?>assets/images/home/logos/logo-grazia.png" />
          </a>
        </li>-->
        <li>
          <a href="http://economictimes.indiatimes.com/small-biz/startups/stylecracker-gets-rs-6-crore-in-funds-from-hnis/articleshow/48751226.cms  " target="_blank">
          <img src="<?php echo base_url(); ?>assets/images/home/logos/logo-the-economic-times-logo.png" />
          </a>
        </li>
        <li>
          <a href="https://www.youtube.com/watch?v=aITNSU2FDNs&feature=youtu.be" target="_blank">
          <img src="<?php echo base_url(); ?>assets/images/home/logos/logo-cnbc-tv18.png" />
          </a>
        </li>
      </ul>
    </div>
  </section>

</div>
<style>
  .header.fixed {
    position: static !important;
  }
</style>
