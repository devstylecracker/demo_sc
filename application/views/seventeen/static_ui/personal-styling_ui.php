<div class="page-personal-styling">
  <section class="section section-shadow">
    <div class="container">
      <div class="title-section text-center">
        <h1 class="title">PERSONAL STYLING</h1>
      </div>
    </div>
  </section>
  <section class="section section-shadow">
    <div class="container">
      Indulge yourself in a Stylecracker Personalised Box and brighten up your wardrobe with the best in fashion , curated exclusively for you, by celebrity stylists.Indulge yourself in a Stylecracker Personalised Box and brighten up your wardrobe with the
      best in fashion , curated exclusively for you, by celebrity stylists.
    </div>
  </section>

  <section class="section section-shadow section-packages">
    <div class="container">
      <div class="title-section">
        <h1 class="title"><span>OUR STYLE PACKAGES</span></h1>
      </div>
      <div class="row">
        <div class="col-md-4 col-sm-12 col-xs-12">
          <div class="box-package">
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/demo/1.jpg" alt="">
            </div>
            <div class="title">PACKAGE 1</div>
            <div class="desc">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus euismod varius ligula ut maximus. Vestibulum tristique eros id massa ornare euismod. Curabitur mauris purus, dignissim
            </div>
            <div class="btns-wrp">
              <a class="scbtn scbtn-primary" href="#"><span>Register</span></a>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
          <div class="box-package">
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/demo/1.jpg" alt="">
            </div>
            <div class="title">PACKAGE 1</div>
            <div class="desc">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus euismod varius ligula ut maximus. Vestibulum tristique eros id massa ornare euismod. Curabitur mauris purus, dignissim
            </div>
            <div class="btns-wrp">
              <a class="scbtn scbtn-primary" href="#"><span>Register</span></a>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
          <div class="box-package">
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/demo/1.jpg" alt="">
            </div>
            <div class="title">PACKAGE 1</div>
            <div class="desc">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus euismod varius ligula ut maximus. Vestibulum tristique eros id massa ornare euismod. Curabitur mauris purus, dignissim
            </div>
            <div class="btns-wrp">
                <a class="scbtn scbtn-primary" href="#"><span>Register</span></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section section-shadow section-testimonial">
    <div class="container">
      <div class="title-section">
        <h1 class="title"><span>TESTIMONIALS</span></h1>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12">
          <div class="box-testimonial">
            <div class="row">
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="img-wrp">
                  <img src="https://www.stylecracker.com/assets/images/home/banner/ramona.jpg" alt="">
                </div>
              </div>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="title">
                  I FELL IN LOVE WITH MY NEW CLOTHES
                </div>
                <div class="name">
                  - Katie Gomez
                </div>

              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
          <div class="box-testimonial">
            <div class="row">
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="img-wrp">
                  <img src="https://www.stylecracker.com/assets/images/home/banner/ramona.jpg" alt="">
                </div>
              </div>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="title">
                  I FELL IN LOVE WITH MY NEW CLOTHES
                </div>
                <div class="desc">
                  <div class="name">
                    - Katie Gomez
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>
  <section class="section section-shadow section-testimonial">
    <div class="container">
      <div class="title-section">
        <h1 class="title"><span>Register at</span></h1>
      </div>
      <div>
        <p>
          https://www.stylecracker.com/
        </p>
      </div>
  </section>
  </div>
