<div class="page-order-history">
  <section class="section section-shadow text-center">
    <div class="container">
      <div class="title-section">
        <h1 class="title">My Orders</h1>
      </div>
      <div>
        <ul class="tabs orders-tabs">
          <li class="active tab-ongoing">
            Ongoing
          </li>
          <li class="tab-completed">
            Completed
          </li>
        </ul>
      </div>
    </div>
  </section>
  <section class="section section-shadow page-order-summary">
    <div class="container">
      <div class="tab-content tab-content-ongoing">
        <div class="title-2">Order #987576</div>
        <div>
          <p>
            <strong>Order Placed: </strong> 15/02/2017
          </p>
          <p>
            <strong>Order to be Delivered: </strong> 15/02/2017
          </p>
          <p>
            <strong>Delivery Address: </strong>
            <br /> 899, Dedric Point Apt. 353, Aaram Society Road, Vakola,
            <br /> Santacruz (East), Mumbai
          </p>
          <p>
            <strong>Products: </strong></p>
          <ul class="order-products">
            <li>
              <div class="img-wrp">
                <img src="http://192.168.1.68/projectbitb/assets/images/demo/p.jpeg" alt="">
              </div>
            </li>
            <li>
              <div class="img-wrp">
                <img src="http://192.168.1.68/projectbitb/assets/images/demo/p.jpeg" alt="">
              </div>
            </li>
            <li>
              <div class="img-wrp">
                <img src="http://192.168.1.68/projectbitb/assets/images/demo/p.jpeg" alt="">
              </div>
            </li>
          </ul>

          <p class="sc-color">
            <strong>Total: <i class="fa fa-inr"></i> 7896</strong> </p>
          <br />
          <div class="btns-wrp">
            <a class="scbtn scbtn-primary"><span>Track Order</span></a>
            <a class="scbtn scbtn-primary"><span>Cancel</span></a>
          </div>
        </div>
      </div>

      <div class="tab-content tab-content-completed hide">
        <div class="title-2">Order #987576</div>
        <div>
          <p>
            <strong>Order Placed: </strong> 15/02/2017
          </p>
          <p>
            <strong>Order to be Delivered: </strong> 15/02/2017
          </p>
          <p>
            <strong>Delivery Address: </strong>
            <br /> 899, Dedric Point Apt. 353, Aaram Society Road, Vakola,
            <br /> Santacruz (East), Mumbai
          </p>
          <p>
            <strong>Products: </strong></p>
          <ul class="order-products">
            <li>
              <div class="img-wrp">
                <img src="http://192.168.1.68/projectbitb/assets/images/demo/p.jpeg" alt="">
              </div>
            </li>
            <li>
              <div class="img-wrp">
                <img src="http://192.168.1.68/projectbitb/assets/images/demo/p.jpeg" alt="">
              </div>
            </li>
            <li>
              <div class="img-wrp">
                <img src="http://192.168.1.68/projectbitb/assets/images/demo/p.jpeg" alt="">
              </div>
            </li>
          </ul>

          <p class="sc-color">
            <strong>Total: <i class="fa fa-inr"></i> 7896</strong> </p>
          <br />
          <div class="btns-wrp">
            <a class="scbtn scbtn-primary"><span>Rate Your Experience</span></a>
            <a class="scbtn scbtn-primary"><span>Return Products</span></a>
          </div>
        </div>
      </div>
  </section>
  </div>
