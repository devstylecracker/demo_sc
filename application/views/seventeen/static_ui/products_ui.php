<link href="<?php echo base_url(); ?>assets/seventeen/css/common-temp.css" rel="stylesheet" type="text/css">


<section class="section section-shadow">
  <div class="container">
    <div class="title-section">
      <h1 class="title">Products</h1>
      <div class="ic ic-filter">
        <i class="icon-filter"></i>
      </div>
    </div>
    <!--test-->
    <div class="grid grid-items products">
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-6 grid-col">
          <div class="item-wrp">
            <div class="item">
              <div class="item-img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/demo/p.jpeg" alt="">
              </div>
              <div class="item-desc-wrp">
                <div class="category"><a href="#">Dresses</a></div>
                <div class="title">Geometric Print Bodycon Dress</div>
                <div class="brand">By <a href="#">The Label Life</a></div>
                <div class="item-price-wrp">
                  <div class="price"><i class="fa fa-inr"></i> 9862</div>
                </div>
              </div>
              <div class="item-media-wrp" style="position:relative;">
                <a class="ic" href="#demo" data-toggle="collapse">
             <i class="icon-plus"></i>
            </a>
                <span class="ic ic-share">
              <i class="fa fa-paper-plane-o"></i>
            </span>
			
		
			
              </div>
              <span class="ic-heart">
            <i class="fa fa-heart-o"></i>
          </span>
            </div>
			
				<!-- 
			 <a href="#demo" data-toggle="collapse">Collapsible</a>-->

<div id="demo" class="collapse" style="position:absolute; background:#fff; padding:10px; width:100%;">
Lorem ipsum dolor text....
</div>

			<!-- -->
			
			
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-6 grid-col">
          <div class="item-wrp">
            <div class="item">
              <div class="item-img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/demo/22.jpg" alt="">
              </div>
              <div class="item-desc-wrp">
                <div class="category"><a href="#">Dresses</a></div>
                <div class="title">Geometric Print Bodycon Dress</div>
                <div class="brand">By <a href="#">The Label Life</a></div>
                <div class="item-price-wrp">
                  <div class="mrp"><i class="fa fa-inr"></i> 9862</div>
                  <div class="price"><i class="fa fa-inr"></i> 9862</div>

                </div>
              </div>
              <div class="item-media-wrp">
                <span class="ic ic-plus">
              <i class="fa fa-plus"></i>
            </span>
                <span class="ic ic-share">
              <i class="fa fa-paper-plane-o"></i>
            </span>
              </div>
              <span class="ic-heart">
            <i class="fa fa-heart-o"></i>
          </span>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-6 grid-col">
          <div class="item-wrp">
            <div class="item">
              <div class="item-img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/demo/21.jpg" alt="">
              </div>
              <div class="item-desc-wrp">
                <div class="category"><a href="#">Dresses</a></div>
                <div class="title">Geometric Print Bodycon Dress</div>
                <div class="brand">By <a href="#">The Label Life</a></div>
                <div class="item-price-wrp">
                  <div class="mrp"><i class="fa fa-inr"></i> 9862</div>
                  <div class="price"><i class="fa fa-inr"></i> 9862</div>
                    <div class="discount">12% OFF</div>
                </div>
              </div>
              <div class="item-media-wrp">
                <span class="ic ic-plus">
              <i class="fa fa-plus"></i>
            </span>
                <span class="ic ic-share">
              <i class="fa fa-paper-plane-o"></i>
            </span>
              </div>
              <span class="ic-heart">
            <i class="fa fa-heart-o"></i>
          </span>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-6 grid-col">
          <div class="item-wrp">
            <div class="item">
              <div class="item-img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/demo/20.jpg" alt="">
              </div>
              <div class="item-desc-wrp">
                <div class="category"><a href="#">Dresses</a></div>
                <div class="title">Geometric Print Bodycon Dress</div>
                <div class="brand">By <a href="#">The Label Life</a></div>
                <div class="item-price-wrp">
                  <div class="mrp"><i class="fa fa-inr"></i> 9862</div>
                  <div class="price"><i class="fa fa-inr"></i> 9862</div>
                    <div class="discount">12% OFF</div>
                </div>
              </div>
              <div class="item-media-wrp">
                <span class="ic ic-plus">
              <i class="fa fa-plus"></i>
            </span>
                <span class="ic ic-share">
              <i class="fa fa-paper-plane-o"></i>
            </span>
              </div>
              <span class="ic-heart">
            <i class="fa fa-heart-o"></i>
          </span>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-6 grid-col">
          <div class="item-wrp">
            <div class="item">
              <div class="item-img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/demo/20.jpg" alt="">
              </div>
              <div class="item-desc-wrp">
                <div class="category"><a href="#">Dresses</a></div>
                <div class="title">Geometric Print Bodycon Dress</div>
                <div class="brand">By <a href="#">The Label Life</a></div>
                <div class="item-price-wrp">
                  <div class="mrp"><i class="fa fa-inr"></i> 9862</div>
                  <div class="price"><i class="fa fa-inr"></i> 9862</div>
                    <div class="discount">12% OFF</div>
                </div>
              </div>
              <div class="item-media-wrp">
                <span class="ic ic-plus">
              <i class="fa fa-plus"></i>
            </span>
                <span class="ic ic-share">
              <i class="fa fa-paper-plane-o"></i>
            </span>
              </div>
              <span class="ic-heart">
            <i class="fa fa-heart-o"></i>
          </span>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>
