<?php
// echo "<pre>";print_r($product_data);exit; ?>
<section class="section section-shadow">
  <div class="container">
    <div class="page-product">
      <ol class="breadcrumb">
    <li><a href="<?php echo base_url();?>">Home</a></li>
    <li><a href="<?php echo base_url().'events';?>">Events</a></li>
    <li class="active"><?php echo $product_data['event_name']; ?></li>
  </ol>
        <div class="event-item">
          <div class="row">
                <!-- event image -->
            <div class="col-md-5 col-sm-4 col-xs-12">
                <div class="item-img-wrp img-wrp">
              <img src="<?php echo $product_data['event_image']; ?>">

			<span class="ic-heart sc-heart <?php if(@$product_data['is_wishlisted'] == '1') echo "active"; ?>" id="wishlist_e<?php echo @$product_data['event_id']; ?>" onclick="add_to_fav('event',<?php echo @$product_data['event_id']; ?>,<?php if($user_id > 0){ echo @$user_id; }else { echo '0'; } ?>,'<?php echo $product_data['event_name']; ?>');">
				<i class="icon-heart"></i>
			</span>
  </div>
            </div>
              <!-- /. event image -->
            <div class="col-md-7 col-sm-8 col-xs-12">
              <div class="item-desc-wrp">
                <h1 class="title"><?php echo $product_data['event_name']; ?></h1>
                <div class="date"><?php echo $product_data['event_start_date'].' ,'.$product_data['event_start_time']; ?>, <span class="fa fa-map-marker"></span> <?php echo $product_data['event_venue']; ?></div>
                <div class="item-btns-wrp">
				<?php if(@$product_data['event_link'] != ''){$href=$product_data['event_link']; ?>
                  <a href="<?php echo $href; ?>" class="scbtn scbtn-primary"><span>Attend the Event</span></a>
				<?php } ?>
                  <span class="ic ic-share" onclick="send_message('event','<?php echo @$product_data['event_id']; ?>','<?php echo @$product_data['event_name']; ?>','<?php echo @$product_data['event_image']; ?>','<?php echo base_url().'events/'.$product_data['object_slug'].'-'.$product_data['event_id']; ?>',<?php if($user_id > 0){ echo @$user_id; }else { echo '0'; } ?>);" >
                    <i class="icon-send"></i>
                </span>
                <span class="dropdown ic-share-wrp">
                <span class="ic dropdown-toggle" data-toggle="dropdown">
                  <i class="icon-share"></i>
               </span>
               <ul class="dropdown-menu">

					<?php
					$short_url = base_url().'event_web/get_single_event/'.$product_data['event_id'].'';
					echo '<li><a onClick="_targetFacebookShare(\''.$product_data['event_id'].'\',\''.$short_url.'\',\''.$product_data['event_image'].'\',\''.$product_data['event_id'].'\');"><i class="fa fa-facebook"></i></a></li>';
					echo '<li><a onclick="_targetTweet(\''.$short_url.'\',\''.$product_data['event_name'].'\',\''.$product_data['event_id'].'\');"><i class="fa fa-twitter"></i></a></li>';
					echo '<li><a onclick="_targetPinterest(\''.$short_url.'\',\''.$product_data['event_name'].'\',\''.$product_data['event_id'].'\',\''.$product_data['event_image'].'\');"><i class="fa fa-pinterest"></i></a></li>';

					?>
                </ul>
                </span>

                </div>

                <div class="desc">
                  <?php echo $product_data['event_short_desc']; ?>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
</section>
 <?php if(!empty($product_data['event_products'])){ ?>
<section class="section section-shadow">
  <div class="container">
    <div class="title-section">
      <h2 class="subtitle">Clothes for this event</h2>
      <!--<a href="<?php echo base_url(); ?>event_web/get_event_products/<?php echo @$product_data['event_id']; ?>" class="link-view-all">View All Products</a>-->
    </div>
    <div class="grid grid-items products">
      <div class="row" id="get_event_products">
        <?php if(!empty($product_data['event_products'])){
			foreach($product_data['event_products'] as $val){  
				$product_name = $val->product_name; 
				$val->product_name = preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_name);
				$val->product_slug = preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_slug);
	?>
			<div class="col-md-3 col-sm-6 col-xs-6 grid-col">
			  <div class="item-wrp">
				<div class="item">
				  <div class="item-img-wrp" onclick="_targetProductClick(<?php echo $val->product_id; ?>,'6','<?php echo @$val->product_name; ?>');" >
					<a href="<?php echo base_url().PRODUCT_URL.$val->product_slug.'-'.$val->product_id; ?>"><img src="<?php echo $val->product_img; ?>" alt=""></a>
				  </div>
				  <div class="item-desc-wrp">
					<div class="category"><a href="<?php echo CAT_URL.$val->product_category_slug.'-'.$val->product_category_id; ?>"><?php echo $val->product_category; ?></a></div>
					<div class="title" onclick="_targetProductClick(<?php echo $val->product_id; ?>,'6','<?php echo @$val->product_name; ?>');" ><a href="<?php echo base_url().PRODUCT_URL.$val->product_slug.'-'.$val->product_id; ?>"><?php echo $product_name; ?></a></div>
					<div class="brand">By <a href="<?php echo BRAND_URL.$val->brand_slug.'-'.$val->brand_id; ?>"><?php echo $val->brand_name; ?></a></div>
					<div class="item-price-wrp">
					  <?php if($val->price > $val->discount_price){ ?>
								<div class="mrp"><i class="fa fa-inr"></i><?php echo $val->price; ?></div>
								<div class="price"><i class="fa fa-inr"></i><?php echo $val->discount_price; ?></div>
								<div class="discount"><?php echo $val->discount_percent; ?>% OFF</div>
					  <?php }else { ?>
								<div class="price"><i class="fa fa-inr"></i><?php echo $val->price; ?></div>
					  <?php }?>
					</div>
				  </div>
				  <div class="item-media-wrp">
					<?php $size_ids='';$size_texts='';$size_id= array();$size_text= array();
						if(!empty($val->product_size)){
							foreach($val->product_size as $size){
								$size_id[] = $size->size_id;
								$size_text[] = $size->size_text;
							}// foreach
						}//if
						if(!empty($size_id))$size_ids = implode(',',$size_id);
						if(!empty($size_text))$size_texts = implode(',',$size_text);
					?>
					<span class="ic ic-plus" data-toggle="modal"  id="<?php echo @$val->product_id; ?>" value="<?php echo @$val->product_id; ?>" name="prod_size" data-sizes="<?php echo $size_ids; ?>" data-label="<?php echo $size_texts; ?>" data-productname="<?php echo $val->product_name; ?>" data-productprice="<?php echo $val->discount_price; ?>" data-brandname="<?php echo $val->brand_name; ?>" onclick="GetProductSize(this);">
					  <i class="icon-plus"></i>
					</span>
					<span class="ic ic-share" onclick="send_message('product','<?php echo @$val->product_id; ?>','<?php echo @$val->product_name; ?>','<?php echo @$val->product_img; ?>','<?php echo base_url().PRODUCT_URL.$val->product_slug.'-'.$val->product_id; ?>',<?php if($user_id > 0){ echo @$user_id; }else { echo '0'; } ?>);">
					  <i class="icon-send"></i>
					</span>
				  </div>
				 <span class="ic-heart <?php if(@$val->is_wishlisted == '1') echo "active"; ?>" id="wishlist_product<?php echo @$val->product_id; ?>" onclick="add_to_fav('product',<?php echo @$val->product_id; ?>,<?php if($user_id > 0){ echo @$user_id; }else { echo '0'; } ?>,'<?php echo @$val->product_name; ?>');">
					  <i class="icon-heart"></i>
				</span>
				</div>
			  </div>
			</div>

		<?php }//foreach
		} //if ?>
       </div>
    </div>

	<input type="hidden" name="offset" id="offset" value="1">
	<input type="hidden" name="object_id" id="object_id" value="<?php echo $object_id; ?>">
	<input type="hidden" name="total_products" id="total_products" value="<?php echo $total_products; ?>">
	<div id="load-more" class="fa-loader-item-wrp">
		<div class="fa-loader-item">
		<i class="fa fa-circle-o-notch fa-spin-custom"></i> Loading...
		</div>
	</div>

  </div>
</section>
 <?php } ?>