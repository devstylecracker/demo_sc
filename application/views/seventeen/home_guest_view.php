<?php
// echo "<pre>";print_r($home_collections);exit;
?>
<div class="page-home-guest">
  <section class="section section-shadow section-main-slider">
      <div class="home-banner-wrp home-main-wrp">
        <div class="swiper-container swiper-container-front front main">
          <ul class="swiper-wrapper">
			<?php	if(!empty($slider_data)){ foreach($slider_data as $row){ ?>
        <li class="swiper-slide slide">
        <a onclick="<?php echo $row['banner_url']; ?>" href="<?php echo $row['lable_link']; ?>" >
          <picture class="img-wrp">
            <source srcset="<?php echo $this->config->item('homepage_banner_url').'mobile_banner/'.$row['slide_url']; ?>" media="(max-width: 380px)">
            <source srcset="<?php echo $this->config->item('homepage_banner_url').$row['slide_url']; ?>">
            <img src="<?php echo $this->config->item('homepage_banner_url').$row['slide_url']; ?>">
          </picture>
        	</a>
        </li>
			 <?php	}// end of for loop
				}//end of if loop
				?>
          </ul>
    		<div class="swiper-button-next front main"></div>
    		<div class="swiper-button-prev front main"></div>
        <div class="swiper-pagination front main"></div>
    </div>
      </div>
  </section>
  <section class="section section-shadow section-collections">
    <div class="container">
      <div class="grid grid-items grid-items-slider sm-slider thumb-collection">
        <div class="row grid-row">
          <?php
		  if(!empty($home_collections)){ foreach($home_collections as $val){?>
			<div class="col-md-6 col-sm-12 col-xs-12 grid-col">
			  <div class="item-wrp overlay">
				<div class="item">
				  <div class="item-img-wrp">
					<img src="<?php echo @$val['collection_image']; ?>" alt="">
				  </div>
				  <div class="item-desc-wrp">
					<div class="inner">
					<h2 class="title"><?php echo @$val['collection_name']; ?></h2>
					<div class="desc"><?php echo @$val['collection_short_desc']; ?></div>
					<div class="btns-wrp">
					  <a href="<?php echo base_url(); ?>collections/<?php echo @$val['object_slug'].'-'.@$val['collection_id']; ?>" class="scbtn scbtn-secondary white" onclick="_targetClickTrack('<?php echo @$val['collection_id']; ?>','8','<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['collection_name']); ?>','collection')"><span>View Collection</span></a>
					</div>

				  </div>
					</div>
					<span class="ic-heart <?php if(@$val['is_wishlisted'] == '1') echo "active"; ?>" id="wishlist_c<?php echo @$val['collection_id']; ?>" onclick="add_to_fav('collection',<?php echo @$val['collection_id']; ?>,<?php if(@$user_id > 0){ echo @$this->session->userdata('user_id'); }else { echo '0'; } ?>,'<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['collection_name']); ?>');">
					  <i class="icon-heart"></i>
					</span>
				</div>
			  </div>
			</div>
		<?php }//foreach
			}//if	?>

        </div>
      </div>
    </div>
  </section>
  <section class="section section-shadow">
            <div class="container">
              <div class="box-benefits-wrp">
                <div class="inner inner-benefits">
                <div class="row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="img-wrp md-only">
                      <img src="<?php echo base_url(); ?>assets/seventeen/images/home/banners/benifits.jpg" />
                    </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="content">
                      <div class="title-section">
                        <h3 class="title">Benefits</h3>
                      </div>
                      <div class="desc">
                        <div class="img-wrp sm-only">
                          <img src="<?php echo base_url(); ?>assets/seventeen/images/home/banners/benifits.jpg" />
                        </div>
                        <ul>
                          <li><i class="icon-user"></i>Personalised selections</li>
                          <li><i class="icon-bag"></i>Stay up to date on fashion</li>
                          <li><i class="icon-setting"></i>The more you use us, the better we get</li>
                          <li><i class="icon-heart"></i>Exclusive and popular brands you will love</li>
                          <li><i class="icon-chat"></i> Chat with celebrity stylists</li>
                        </ul>
                      </div>
                      <a data-toggle="modal" href="<?php echo base_url().'sc-box?f=1';?>" class="scbtn scbtn-primary" title="Get Styled"><span>Get Styled</span></a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="inner inner-how-it-works">
                <div class="row">
                  <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="content">
                      <div class="title-section">
                        <h3 class="title">How it works</h3>
                      </div>
                      <div class="desc">
                        <div class="img-wrp sm-only">
                          <img src="<?php echo base_url(); ?>assets/seventeen/images/home/banners/how-it-works.jpg" />
                        </div>
                        <ul>
                          <li>
                            <span class="text-bold">Step 1:</span> Take a 30 second personal style quiz
                          </li>
                          <li>
                            <span class="text-bold">Step 2:</span> Shop from our collection or buy a StyleCracker Box </li>
                          <li>
                            <span class="text-bold">Step 3:</span> Shop different!
                          </li>
                        </ul>
                      </div>
                      <a data-toggle="modal" href="<?php echo base_url().'sc-box?f=1';?>" class="scbtn scbtn-primary" title="Get Started"><span>Get Started</span></a>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="img-wrp md-only">
                      <img src="<?php echo base_url(); ?>assets/seventeen/images/home/banners/how-it-works.jpg" />
                    </div>
                  </div>
                </div>
              </div>

              <div class="inner inner-outfit">
                <div class="row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="img-wrp md-only">
                      <img src="<?php echo base_url(); ?>assets/seventeen/images/home/banners/outfit.jpg" />
                    </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="content">
                      <div class="title-section">
                        <h3 class="title">Life isn't perfect, <br /> but your outfit can be</h3>
                      </div>
                      <div class="desc">
                        <div class="img-wrp sm-only">
                          <img src="<?php echo base_url(); ?>assets/seventeen/images/home/banners/outfit.jpg" />
                        </div>
                        <p>
                        For busy women & men on the go, StyleCracker acts as your style concierge to pick the best fashion, beauty and grooming tailored to your taste, budget and lifestyle, delivered to your doorstep!
                      </p>
                    </div>
                      <a data-toggle="modal" href="<?php echo base_url().'sc-box?f=1';?>" class="scbtn scbtn-primary" title="Get Stylish"><span>Get Stylish</span></a>
                    </div>
                  </div>
                </div>
              </div>
              </div>
            </div>
  </section>
  <section class="section section-shadow section-work">
    <div class="container">
      <div class="section-work-inner">
        <div class="row">
          <div class="col-md-5">
            <div class="desc">
              <div class="title-section">
                <h2 class="title"> Want to look like a celebrity?</h2>
              </div>
              <p class="subtitle">
              See how we work it

              </p>
              <a data-toggle="modal" href="<?php echo base_url().'sc-box?f=1';?>" class="scbtn scbtn-primary"><span>Sign Up Now</span></a>
            </div>
          </div>

          <div class="col-md-7">

            <div class="swiper-container work">
              <ul class="swiper-wrapper">
                <li class="swiper-slide slide">
                  <img src="<?php echo base_url(); ?>assets/seventeen/images/home/banners/work-ranbir-kapoor-filmfare.jpg" />
                </li>
                <li class="swiper-slide slide">
                  <img src="<?php echo base_url(); ?>assets/seventeen/images/home/banners/work-shaandaar.jpg" />
                </li>
                <li class="swiper-slide slide">
                  <img src="<?php echo base_url(); ?>assets/seventeen/images/home/banners/work-filmfare.jpg" />
                </li>
                <li class="swiper-slide slide">
                  <img src="<?php echo base_url(); ?>assets/seventeen/images/home/banners/work-kapoor-n-sons.jpg" />
                </li>
                <li class="swiper-slide slide">
                  <img src="<?php echo base_url(); ?>assets/seventeen/images/home/banners/work-ellina.jpg" />
                </li>

              </ul>
              <div class="swiper-pagination work"></div>
              <div class="swiper-button-next front work"></div>
              <div class="swiper-button-prev front work"></div>
            </div>


          </div>
        </div>
      </div>
    </div>
  </section>


  <section class="section section-shadow section-products">
    <div class="container">
      <div class="title-section">
        <h1 class="title">RIGHT OFF THE RACK</h1>
        <a href="<?php echo base_url(); ?>home-products" class="link-view-all">View Trending Products</a>
      </div>
      <div class="grid grid-items products swiper-container front">
        <ul class="row swiper-wrapper">
			<?php if(!empty($products)){
				foreach($products as $val){  
					$product_name = $val->product_name; 
					$val->product_name = preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_name);
					$val->product_slug = preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_slug);
			?>
				<li class="col-md-3 col-sm-6 col-xs-6 grid-col swiper-slide slide">
				  <div class="item-wrp">
					<div class="item">

					  <div class="item-img-wrp" onclick="_targetProductClick(<?php echo $val->product_id; ?>,'6','<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_name) ?>');">

						<a href="<?php echo base_url().PRODUCT_URL.$val->product_slug.'-'.$val->product_id; ?>"><img src="<?php echo $val->product_img; ?>" alt=""></a>
					  </div>
					  <div class="item-desc-wrp">
						<div class="category"><a href="<?php echo CAT_URL.$val->product_category_slug.'-'.$val->product_category_id; ?>"><?php echo $val->product_category; ?></a></div>

					<div class="title" onclick="_targetProductClick(<?php echo $val->product_id; ?>,'6','<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_name); ?>');" ><a href="<?php echo base_url().PRODUCT_URL.$val->product_slug.'-'.$val->product_id; ?>"><?php echo $product_name; ?></a></div>

					<div class="brand">By <a href="<?php echo BRAND_URL.$val->brand_slug.'-'.$val->brand_id; ?>"><?php echo $val->brand_name; ?></a></div>
						<div class="item-price-wrp">
						  <?php if($val->price > $val->discount_price){ ?>
									<div class="mrp"><i class="fa fa-inr"></i><?php echo $val->price; ?></div>
									<div class="price"><i class="fa fa-inr"></i><?php echo $val->discount_price; ?></div>
									<div class="discount"><?php echo $val->discount_percent; ?>% OFF</div>
						  <?php }else { ?>
									<div class="price"><i class="fa fa-inr"></i><?php echo $val->price; ?></div>
						  <?php }?>
						</div>
					  </div>
					  <div class="item-media-wrp">
					  <?php $size_ids='';$size_texts='';$size_id= array();$size_text= array();if(!empty($val->product_size)){foreach($val->product_size as $size){
							$size_id[] = $size->size_id;
							$size_text[] = $size->size_text;
							}// foreach
							}//if
							if(!empty($size_id))$size_ids = implode(',',$size_id);
							if(!empty($size_text))$size_texts = implode(',',$size_text);
						?>
						<span class="ic ic-plus" data-toggle="modal"  id="<?php echo @$val->product_id; ?>" value="<?php echo @$val->product_id; ?>" name="prod_size" data-sizes="<?php echo $size_ids; ?>" data-label="<?php echo $size_texts; ?>" data-productname="<?php echo $val->product_name; ?>" data-productprice="<?php echo $val->discount_price; ?>" data-brandname="<?php echo $val->brand_name; ?>" data-category="<?php echo $val->product_category; ?>"  onclick="GetProductSize(this);">
						  <i class="icon-plus"></i>
						</span>
						<span class="ic ic-share" onclick="send_message('product','<?php echo @$val->product_id; ?>','<?php echo @$val->product_name; ?>','<?php echo @$val->product_img; ?>','<?php echo base_url().PRODUCT_URL.$val->product_slug.'-'.$val->product_id; ?>',<?php if(@$user_id > 0){ echo @$user_id; }else { echo '0'; } ?>);">
						  <i class="icon-send"></i>
						</span>

					  </div>
					<span class="ic-heart <?php if(@$val->is_wishlisted == '1') echo "active"; ?>" id="wishlist_product<?php echo @$val->product_id; ?>" onclick="add_to_fav('product',<?php echo @$val->product_id; ?>,<?php if(@$user_id > 0){ echo @$user_id; }else { echo '0'; } ?>,'<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_name); ?>');">
						  <i class="icon-heart"></i>
					</span>
					</div>
				  </div>
				 </li>

			<?php }//foreach
			}else{  ?>
			 <div class="grid-no-products"> No Products Found </div>
			<?php }  ?>

        </ul>
      </div>
    </div>
  </section>
  <section class="section section-shadow">
    <div class="container">
      <div class="title-section">
        <h1 class="title">TESTIMONIALS</h1>
      </div>
      <div class="box-testimonial">
        <div class="row">
          <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/seventeen/images/home/banners/priyanka1.jpg" alt="">
            </div>
          </div>
          <div class="col-md-9 col-sm-8 col-xs-12">
            <div class="title">
              Perfectly personalised as per my taste
            </div>
            <div class="desc">
              <p>I received my surprise box of goodies from StyleCracker and it just couldn't get better! In this day and age of online shopping, a box of goodies suited to my style and preference was something very outstanding. Not to mention how flattering it is to have a box personalized to my taste and style. Way to go StyleCracker!
              </p>
              <div class="name">
                - Priyanka Talreja
              </div>
            </div>
          </div>
        </div>
      </div>
	  <div class="box-testimonial">
        <div class="row">
          <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/seventeen/images/home/banners/glen.jpg" alt="">
            </div>
          </div>
          <div class="col-md-9 col-sm-8 col-xs-12">
            <div class="title">
             They even helped me with 'what not to wear'
            </div>
            <div class="desc">
              <p>Since I was looking for a personal stylist for myself, having one who’s worked with A-listers and the biggest names in the movie business made StyleCracker my absolute choice! From being style guides to helping me with even ‘what not to wear’, they’ve been extremely helpful in solving all my fashion queries.
              </p>
              <div class="name">
                - Glenn Gonsalves
              </div>
            </div>
          </div>
        </div>
      </div>
	  <div class="box-testimonial">
        <div class="row">
          <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/seventeen/images/home/banners/ramona.jpg" alt="">
            </div>
          </div>
          <div class="col-md-9 col-sm-8 col-xs-12">
            <div class="title">
             Handpicked to suit my body type
            </div>
            <div class="desc">
              <p>The StyleCracker stylists have been like 3 am friends. They have helped me pick products that suit me and my body type. Every purchase is tailor made and I love how personal it gets. And in rare scenario when a product doesn’t work , the team is extremely helpful and prompt in arranging easy returns.
              </p>
              <div class="name">
               - Ramona
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="" style="text-align:right; padding-top:20px;">
       and many more...
      </div>
    </div>
  </section>

  <section class="section section-shadow section-media">
    <div class="container">
      <div class="title-section">
        <h3 class="title">Psst...they are talking about us</h3>
      </div>
      <ul>
        <li>
          <a href="https://www.youtube.com/watch?v=yyamOd7eaM0&feature=youtu.be" target="_blank">
          <img src="<?php echo base_url(); ?>assets/seventeen/images/home/logos/logo-bloomberg-tv-india.png" />
          </a>
        </li>
        <li>
          <a href="http://archive.indianexpress.com/news/it-s-personal/1129558/0" target="_blank">
          <img src="<?php echo base_url(); ?>assets/seventeen/images/home/logos/logo-ie.png" />
          </a>
        </li>
        <li>
          <a href="http://economictimes.indiatimes.com/small-biz/startups/stylecracker-gets-rs-6-crore-in-funds-from-hnis/articleshow/48751226.cms" target="_blank">
          <img src="<?php echo base_url(); ?>assets/seventeen/images/home/logos/logo-the-economic-times-logo.png" />
          </a>
        </li>
        <li>
          <a href="https://www.youtube.com/watch?v=aITNSU2FDNs&feature=youtu.be" target="_blank">
          <img src="<?php echo base_url(); ?>assets/seventeen/images/home/logos/logo-cnbc-tv18.png" />
          </a>
        </li>
      </ul>
    </div>
  </section>

</div>
<style>
  .header.fixed {
    position: static !important;
  }
</style>
