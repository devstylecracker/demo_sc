<?php
// echo "<pre>";print_r($top_event);exit;?>
<section class="section section-shadow section-collections">
  <div class="container">
    <div class="title-section">
      <h1 class="title">Collections</h1>
      <a href="#" class="link-view-all">View All Collections</a>
    </div>

    <div class="grid grid-items grid-items-slider sm-slider">
      <div class="row grid-row">
	   <div class="col-md-6 col-sm-12 col-xs-12 grid-col">
          <div class="item-wrp overlay overlay-style-2">
            <div class="item">
              <div class="item-img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/demo/1.jpg" alt="">
              </div>
              <div class="item-desc-wrp">
                  <div class="inner">
                <h2 class="title">The Home Collection</h2>
              <!--  <div class="desc">Cras in diam volutpat metus dapibus ultrices id sed ipsum</div>
                <div class="btns-wrp">
                  <a href="#" class="btn btn-sm button btn-primary">View Collection</a>
                </div>-->
              <!--   <div class="desc-2">Recommended collection by <strong>Head Stylist, Priti Chandra</strong></div>-->
              </div>
            </div>
                <span class="ic-heart">
              <i class="fa fa-heart-o"></i>
            </span>
              </div>
            </div>
          </div>
		  <?php
		  if(!empty($top_collection)){ foreach($top_collection as $val){?>
			<div class="col-md-6 col-sm-12 col-xs-12 grid-col">
			  <div class="item-wrp overlay">
				<div class="item">
				  <div class="item-img-wrp">
					<img src="<?php echo @$val['collection_image']; ?>" alt="">
				  </div>
				  <div class="item-desc-wrp">
					<div class="inner">
					<h2 class="title"><?php echo @$val['collection_name']; ?></h2>
					<div class="desc"><?php echo @$val['collection_short_desc']; ?></div>
					<div class="btns-wrp">
					  <a href="#" class="sc-btn btn-secondary"><span>View Collection</span></a>
					</div>

				  </div>
					</div>
					<span class="ic-heart <?php if(@$val['is_wishlisted'] == '1') echo "active"; ?>" id="wishlist_c<?php echo @$val['collection_id']; ?>">
					<a id="whislist<?php echo @$val['collection_id']; ?>" title="Add to Wish List" onclick="add_to_fav('collection',<?php echo @$val['collection_id']; ?>,<?php echo @$this->session->userdata('user_id'); ?>);">
					  <i class="fa fa-heart-o"></i>
					 </a>
					</span>
				</div>
			  </div>
			</div>
		<?php }//foreach
			}//if	?>
      </div>

    </div>
  </div>
</section>
<section class="section section-shadow section-promotion">
  <div class="container">
    <div class="grid grid-items grid-items-slider sm-slider">
      <div class="row grid-row">
        <div class="col-md-6 col-sm-12 col-xs-12 grid-col">
          <div class="item-wrp overlay">
            <div class="item">
              <div class="item-img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/demo/1.jpg" alt="">
              </div>
              <div class="item-desc-wrp">
                  <div class="inner">
                <h2 class="title">Cras in diam volutpat metus dapibus ultrices id sed ipsum</h2>
                <div class="btns-wrp">
                  <a href="#" class="scbtn scbtn-primary"><span>Read More</span></a>
                </div>
                <div class="btns-wrp">
                  <a href="#" class="scbtn scbtn-primary"><span>Book Your Stylist</span></a>
                </div>

              </div>
            </div>
              </div>
            </div>
          </div>
        <div class="col-md-6 col-sm-12 col-xs-12 grid-col">
          <div class="item-wrp overlay">
            <div class="item">
              <div class="item-img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/demo/2.jpg" alt="">
              </div>
              <div class="item-desc-wrp">
                  <div class="inner">
                <h2 class="title">Cras in diam volutpat metus dapibus ultrices id sed ipsum</h2>
                <div class="btns-wrp">
                  <a href="#" class="sc-btn"><span>Read More</span></a>
                </div>
                <div class="btns-wrp">
                  <a href="#" class="sc-btn"><span>Book Your Stylist</span></a>
                </div>

              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section section-shadow">
  <div class="container">
    <div class="title-section">
      <h1 class="title">Events</h1>
        <a href="#" class="link-view-all">View All Events</a>
    </div>
    <!--test-->
    <div class="grid grid-items grid-items-slider sm-slider events">
      <div class="row grid-row">
       <?php
		  if(!empty($top_event)){ foreach($top_event as $val){ ?>
		  <div class="col-md-6 col-sm-6 col-xs-12 grid-col">
          <div class="item-wrp">
            <div class="item">
              <div class="item-img-wrp">
                <img src="<?php echo @$val['event_image']; ?>" alt="">
              </div>
              <div class="item-desc-wrp">
                <div class="title"><?php echo @$val['event_name']; ?></div>
                <div class="date"><?php echo @$val['event_start_date']; ?>,<?php echo @$val['event_venue']; ?></div>
                <div class="desc"><?php echo @$val['event_short_desc']; ?></div>
              </div>
                <div class="item-btns-wrp">
                  <a href="#" class="sc-btn"><span>Read More</span></a>
                  <span class="ic ic-share">
                    <i class="fa fa-paper-plane-o"></i>
                </span>
                </div>
                <span class="ic-heart <?php if(@$val['is_wishlisted'] == '1') echo "active"; ?>" id="wishlist_e<?php echo @$val['event_id']; ?>">
				<a id="whislist_ev<?php echo @$val['event_id']; ?>" title="Add to Wish List" onclick="add_to_fav('event',<?php echo @$val['event_id']; ?>,<?php echo @$this->session->userdata('user_id'); ?>);">
				  <i class="fa fa-heart-o"></i>
				 </a>
				</span>
            </div>
          </div>
        </div>
		<?php }//foreach
			}//if	?>
      </div>
    </div>
  </div>
</section>

<section class="section section-shadow">
  <div class="container">
    <div class="title-section">
      <h1 class="title">SC Live</h1>
      <a href="#" class="link-view-all">View All Posts</a>
    </div>
    <!--test-->
    <div class="grid grid-items grid-items-slider sm-slider blog">
      <div class="row grid-row">
        <div class="col-md-4 col-sm-6 col-xs-12 grid-col grid-col">
          <div class="item-wrp">
            <div class="item">
              <div class="item-img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/demo/5.jpg" alt="">
              </div>
              <div class="item-desc-wrp">
                <div class="title">5 pairs of footwear that are cool</div>
                <div class="desc">
                Cras in diam volutpat metus dapibus ultrices id sed ipsum
                </div>
              </div>
                <div class="item-btns-wrp">
                  <a href="#" class="sc-btn"><span>Read More</span></a>
                </div>
                <span class="ic-heart">
              <i class="fa fa-heart-o"></i>
            </span>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 grid-col grid-col">
          <div class="item-wrp">
            <div class="item">
              <div class="item-img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/demo/6.jpg" alt="">
              </div>
              <div class="item-desc-wrp">
                <div class="title">5 pairs of footwear that are cool</div>
                <div class="desc">
                Cras in diam volutpat metus dapibus ultrices id sed ipsum
                </div>
              </div>
                <div class="item-btns-wrp">
                  <a href="#" class="sc-btn"><span>Read More</span></a>
                </div>
                <span class="ic-heart">
              <i class="fa fa-heart-o"></i>
            </span>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 grid-col grid-col">
          <div class="item-wrp">
            <div class="item">
              <div class="item-img-wrp">
                <img src="<?php echo base_url(); ?>assets/images/demo/1.jpg" alt="">
              </div>
              <div class="item-desc-wrp">
                <div class="title">5 pairs of footwear that are cool</div>
                <div class="desc">
                Cras in diam volutpat metus dapibus ultrices id sed ipsum
                </div>
              </div>
                <div class="item-btns-wrp">
                  <a href="#" class="sc-btn"><span>Read More</span></a>
                </div>
                <span class="ic-heart">
              <i class="fa fa-heart-o"></i>
            </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section section-shadow section-book-stylist">
  <div class="container">
      <div class="row">
        <div class="col-md-1 col-sm-2 col-xs-2">
            <div class="img-wrp">
          <img src="<?php echo base_url(); ?>assets/images/demo/21.jpg" alt="">
        </div>
          </div>
          <div class="col-md-7 col-sm-10 col-xs-10">
          <div class="desc">
            Hi, my name is Pratichi Borkar, a stylist at Stylecracker if you need any help with your styling.
            </div>
          </div>
            <div class="col-md-4 col-sm-10 col-xs-10 col-md-offset-0 col-sm-offset-2 col-xs-offset-2">
              <a href="#" class="sc-btn"><span>Book a Stylist</span></a>
              </div>
        </div>
      </div>
  </div>
</section>
<script>

function add_to_wishlist(object_id){
	alert(object_id);
}
function add_to_fav(fav_for, fav_id, user_id){
    var data = {fav_for : fav_for, fav_id : fav_id, user_id : user_id};
    var url = sc_baseurl+'schome_new/fav_add';
    $.ajax({
      type: "POST",
      url: url,
      data: data,
      dataType: "json",
      success: function (response) {
          //your success code

          if(response.msg == "Successful Added"){

			 if(fav_for == 'collection'){
				$('#wishlist_c'+fav_id).addClass('active');
			 }else {
				 $('#wishlist_e'+fav_id).addClass('active');
			 }
            console.log(response);
          }else if(response.msg == "Successful deleted"){

			if(fav_for == 'collection'){
				$('#wishlist_c'+fav_id).removeClass('active');
			 }else {
				$('#wishlist_e'+fav_id).removeClass('active');
			 }

            console.log(response);
          }
          console.log(response);



      },
      error: function (response) {
          //your error code
          console.log(response);
      }
    });
  }
</script>
