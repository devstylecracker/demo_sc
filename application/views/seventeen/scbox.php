<style type="text/css">
  @media (min-width: 768px){
.page-scbox .section-banner {
    /*background: #F4F3F8 url(https://www.stylecracker.com/assets/images/scbox/box-banner.jpg) right center no-repeat;*/

     background: #F4F3F8 url(https://www.stylecracker.com/assets/images/scbox/web-banner.jpg) right center no-repeat;

}
}
.page-scbox .title-section .title {
    font-size: 24px;
    line-height: 1.3;
}


.navbar-collapse{
  padding-right: 0 !important;
  padding-left: 0 !important;
}

.price-wrapper{
  position: relative; 
  display: inline-block;
}

.price-slash{
    position: relative;
    width: 120%;
    height: 0;
    left: -3px;
    border-top: 1px solid red;
    transform: rotate(180deg);
    top: 7px;
}
</style>
<style type="text/css">
   .extrapad-row{
   margin-top: 40px;
   }
   .press-img img{
    width: 100%;
   }
   .press-img{
    padding: 10px
   }

/*
* Custom CSS custom_scbox.css starts here
*/
/***************************************************************************
1.0 Common CSS
2.0 Header & footer
****************************************************************************/


/***************************************************************************
* 1.1 Reset CSS
****************************************************************************/

html{
  height: 100%;
}
 body {
  font-family: 'Nunito Sans', sans-serif;
  color: #333;
  font-size: 13px;
  height: 100%;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  padding-right: 0 !important;
}

a{color: #333;}
a:visited{color: #333;}
a:hover{color: #333;}
a:focus{outline: none;}
a:focus, a:hover{text-decoration: underline; color:#111;}
ul{list-style: none; padding: 0}
.wrapper { min-height: 100%;}
.container { width: 100%; max-width: 940px;}

::-webkit-input-placeholder {  color: #9C9C9C; font-weight: 600; text-transform: uppercase;}
::-moz-placeholder { color: #9C9C9C;  font-weight: 600; text-transform: uppercase;}
:-ms-input-placeholder { color: #9C9C9C; font-weight: 600; text-transform: uppercase;}
:-moz-placeholder { color: #9C9C9C; font-weight: 600; text-transform: uppercase;}


.img-wrp img{width: auto; max-width: 100%;}
.text-bold{font-weight: bold;}
.text-semibold{font-weight: 600;}
.text-danger{color: #E03232}
.text-success{color:#13D792;}
.sc-color{color:#13D792; }
.bg-sccolor{background-color:#13D792; }
.state-disabled{ cursor: default !important; opacity: 0.84; }

.position-static{position: static;}
.box-message{padding: 20px 0; text-align: center; margin-bottom: 20px;}
.box-message.success{color:#13D792; }
.box-message.error{color:#E03232; }

.facebook .fa{color: #4460A0;}
.twitter .fa{color: #00AAEC;}
.instagram .fa{color: #EB2780;}
.pinterest .fa{color: #CC2127;}
.snapchat .fa{color: #FFFC00;}

.message.success{color:#13D792; }
.message.error{color:#E03232; }

.form-control{border-radius: 0; border: none; border-bottom: 1px solid #13D792; padding-left:0px; padding-right: 0;  box-shadow: none; height: auto; color: #333; font-weight: 300;}
.form-control:focus{box-shadow: none; border-bottom: 1px solid #15c184;}
.form-group{position: relative; margin-bottom: 20px;}
.form-group .message, .form-group label.error{position: absolute; bottom:-20px; font-size: 95%; width: 100%; display: block; margin: 0; font-weight: 300; text-transform: none; font-weight: 500; -webkit-transition: all 0.5s;  -moz-transition: all 0.5s;  transition: all 0.5s;}
.form-group .message.text-error, .form-group label.error{color: #d61b1b;}

@media (max-width:768px) {
  .form-group .message, .form-group label.error{ bottom: initial;}
  .home-patch { background-color:rgba(255,255,255, 0.6); }
}

[class^="icon-"]:before, [class*=" icon-"]:before { display: inline-block; text-decoration: none;}

/*modals*/
.modal-content{border-radius: 0; border:none;
   -moz-box-shadow: 0 5px 10px -1px rgba(0,0,0,0.21);
   -webkit-box-shadow: 0 5px 10px -1px rgba(0,0,0,0.21);
    box-shadow: 0 5px 10px -1px rgba(0,0,0,0.21);
  }
.modal button.close{font-size: 20px; color:#13D792; opacity: 1; position: relative; z-index:5;   right: 15px; top: 10px;}
.modal button.close:hover{color:#13D792;  opacity: 0.8;}
.modal-backdrop{background: #fff;}
.modal-backdrop.in { filter: alpha(opacity=90); opacity: .9;}
.modal { text-align: center; padding: 0!important;}

.modal:before { content: ''; display: inline-block; height: 100%; vertical-align: middle;  margin-right: -4px;}
.modal-dialog { display: inline-block; text-align: left; vertical-align: middle;}
.modal-open { overflow: auto;}

@media (max-width:768px) {
  .modal button.close{right:0; top:0; font-size: 14px;}
}

.modal-sc .modal-header{border-bottom: none; text-align: center; padding: 20px; }
.modal-sc .modal-body{padding: 20px;}
.modal-sc .modal-title{font-size: 28px;font-weight: 200;  margin: 0;  text-transform: uppercase; line-height: 1;  color: #333; display: inline-block; }
.modal-sc .form-group > label{text-transform: uppercase;}
.modal-sc .form-group .form-control{border: 1px solid #ddd; padding-left: 6px; padding-right: 6px;
-webkit-transition: all 0.5; -moz-transition: all 0.5; transition: all 0.5;}
.modal-sc .form-group .form-control:focus{border: 1px solid #13d792;}
.modal-sc .modal-footer{border-top: none; padding: 20px;}
/* /. modals*/

/*tabs*/
.nav-tabs.sc-tabs{border: none; display: inline-block;}
.nav-tabs.sc-tabs>li>a{border: none; padding:4px  0px; margin: 10px 30px 10px 0; font-weight: 700; letter-spacing: 1px; text-transform: uppercase;}
.nav-tabs.sc-tabs>li:last-child>a{margin-right: 0;}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{border: none; border-bottom:1px solid #13D792;}

/* /.tabs */

/********************************************************************
 // header
********************************************************************/

.header{padding: 20px 0 10px; padding: 20px 0 0;  z-index: 3; position: relative;  background: #fff; top: 0; width: 100%;
    -webkit-transition: transform 0.2s ease-in-out;
    -moz-transition: transform 0.2s ease-in-out;
    -ms-transition: transform 0.2s ease-in-out;
    -o-transition: transform 0.2s ease-in-out;
    transition: transform 0.2s ease-in-out;
}
.navbar-brand{padding: 10px 15px 10px 0;}
.navbar-toggle{float: left; display: none; padding: 5px; font-size: 24px; font-weight: normal; line-height: 1;}
.navbar-toggle .icon-menu{transition: all 0.5s;}
.open .navbar-toggle .icon-menu{color: #13d792;}
.open .navbar-toggle .icon-menu:before { content: "\e938";}
.nav > li{position: static;}
.sc-navbar-primary{font-size: 14px; font-weight: 600;  text-align: center; text-transform: uppercase; margin-top: 10px;}
.sc-navbar-primary .nav{display: inline-block; float: none;}

.sc-navbar-primary .nav > li{padding: 5px 10px;}

.sc-navbar-primary .nav > li > a {padding: 2px; font-weight: 700; border-bottom: 1px solid transparent;}
.sc-navbar-primary .nav > li >a:focus, .nav > li >a:hover{background: inherit; color: #13D792;  }
.sc-navbar-primary .nav > li >a:focus{ border-bottom:1px solid #13D792; }

.sc-navbar-secondary{font-size: 32px; margin-right: 10px;}
.header .sc-navbar-secondary .navbar-nav > li{padding: 10px 15px;}
.header .sc-navbar-secondary .navbar-nav > li > a{padding: 0; position: relative; display: inline-block;}
.header .sc-navbar-secondary .navbar-nav > li > a[aria-expanded="true"]{color:#13D792;}
.header .sc-navbar-secondary .navbar-nav > li .shape-diamond, .header .sc-navbar-secondary .navbar-nav > li .shape-diamond::before{width:20px; height:20px; font-size: 12px; line-height: 20px; }
.header .sc-navbar-secondary .navbar-nav > li .shape-diamond{ position: absolute; right:-10px; top:-10px; z-index: 2;}

.nav>li>a:focus, .nav>li>a:hover{background: none; outline: none;}
.nav .open>a, .nav .open>a:focus, .nav .open>a:hover { background-color:inherit; border-color: inherit; color: #13D792;}

.dropdown-menu{ -moz-box-shadow: 0 19px 15px 0 rgba(0,0,0,0.21); -webkit-box-shadow: 0 19px 15px 0 rgba(0,0,0,0.21);box-shadow: 0 19px 15px 0 rgba(0,0,0,0.21); padding: 20px;  border: none; border-radius: 0; z-index: 5}

.sc-navbar-left-wrp .dropdown-menu{top:40px; width: 100%;}
.sc-navbar-left-wrp .dropdown-menu li{font-size: 14px; font-weight: 700; color: #333; letter-spacing: 2px; text-transform: uppercase; display: block;   vertical-align: top; margin-bottom: 10px;}
.sc-navbar-left-wrp .dropdown-menu li a{color: #333; text-decoration: none;}
.sc-navbar-left-wrp .dropdown-menu li a:hover{color:#13D792;}

.fixed.header { z-index: 101; position: fixed;  box-shadow: 0px 7px 20px 0px rgba(0, 0, 0, 0.2); -moz-transform: translate(0, -20px);
-webkit-transform: translate(0, -20px);
-o-transform: translate(0, -20px);
-ms-transform: translate(0, -20px);
 transform: translate(0, -20px);}


 .header .navbar-nav > li{padding-top: 6px; padding-bottom: 18px;}
 .header .navbar-nav > li.dropdown{position: relative;}
 .header .navbar-nav > li.dropdown .dropdown-menu{top:36px;}
 .fixed.header .navbar-nav > li{padding-bottom: 6px;}
 .header .navbar-nav > li.hover > a{ border-bottom: 1px solid #13D792;}
.scrollbar{ position: relative;}
@media (max-width:768px) {
.sm-static{position: static !important;}
.navbar-toggle{ display: inline-block;}
.sc-navbar-left-wrp .dropdown-menu{width: 100%; padding-top: 40px;}
.sc-navbar-left-wrp .dropdown-menu li{font-size: 12px; }
}

/********************************************************************
 // footer
********************************************************************/

.footer { position: relative; clear:both;}
.footer ul li{  font-size: 14px; font-weight: 700; line-height: 1.7;  text-transform: uppercase; margin-bottom: 10px;}
.footer .copyright{font-size: 11px;}



/********************************************************************
 // common
 ********************************************************************/
.section{padding: 30px 0 30px; }
.section-shadow{box-shadow: inset 0px 7px 20px 0px rgba(0, 0, 0, 0.2);}
.title-section{position: relative; margin-bottom: 10px;}
.title-section > .title{font-size: 48px;  font-weight: 200; margin: 0; text-transform: uppercase;  line-height:1; color:#333; display: inline-block;}
.title-section > .title span{border-bottom: 1px solid; padding-bottom: 10px; display: inline-block;}
.title-section .title-desc{line-height: 1.7; text-align: center;}
.title-section .link-view-all{position: absolute; color:#000; right:0px; bottom:5px; font-size: 22px; text-transform: lowercase; line-height: 1;}
.title-section .ic-filter, .title-section .ic-right{position: absolute; color:#000; right:0px; bottom:10px; }
.title-section .subtitle{font-size: 20px; font-weight: 700;  margin: 0; text-transform: uppercase;  line-height:1.5; color:#333; display: block;}
.title-section .subtitle span{border-bottom: 1px solid #13D792; display: inline-block; padding-bottom: 0px;}

.sub-title-section{position: relative;}
.sub-title-section .title{font-size: 18px; text-align: left; font-weight: 600; text-transform: uppercase; margin-bottom: 10px; line-height: 1; color:#161617;}
.sub-title-section .title span{ display: inline-block;}
.sub-title-section .link-view-all{position: absolute; color:#00b19c; right:0; top:-5px; font-size: 16px; }
.section-book-stylist .desc{font-size: 14px; font-weight: 600; line-height: 1.7;}
.title-2{font-weight: 700; font-size: 14px; color: #000; text-transform: uppercase; margin-bottom: 10px;}
.title-3{font-weight: 700; font-size: 12px; color: #000; text-transform: uppercase; margin-bottom: 10px;}

.md-only{display: block;}
.sm-only{display: none;}

@media (max-width:768px) {
.title-section .title{font-size: 26px;}
.header{padding: 5px 0;}
.header.fixed{transform: translate(0, -5px);}
.md-only{display: none;}
.sm-only{display: block;}

.section{padding: 15px 0 15px;}
.footer.section{padding: 10px 0 15px;}
.title-section .title{font-size: 22px;}
.title-section .link-view-all{position: static; margin-left: 5px; font-size: 13px;}
.footer .container-fluid{padding-right: 15px; padding-left: 15px;}

.page-scbox .section-banner{background:#F4F3F8 url('https://www.stylecracker.com/assets/images/web-mob.jpg')  right center no-repeat;}
}

@media (min-width:768px) {
  .page-scbox .section-banner{background:#F4F3F8 url('https://www.stylecracker.com/assets/images/web-desk.jpg')  right center no-repeat;}
}
/*/.*/

/********************************************************************
 // page: sc home
 ********************************************************************/
 /*.page-scbox .section-banner{background:#F4F3F8 url('http://www.thescbox.com/assets/images/scbox/box-banner.jpg')  right center no-repeat;}*/

 .page-scbox .section{padding: 60px 0 65px;}
 .page-scbox .title-section{margin-bottom: 30px;}
 .page-scbox .subtitle{font-size: 15px; font-weight: 900;; margin: 0px 0 10px; text-transform: uppercase; letter-spacing: 2px;}
 .page-scbox .title-section .subtitle{margin: 15px 0 15px; padding-bottom: 15px; }

 .page-scbox .section-banner .title-section{margin-bottom: 0;}
 .page-scbox .section-banner .title-section .title{font-size: 24px; line-height: 1.5;}
 .page-scbox .title-section .title span{border-bottom: 1px solid #13D792;  padding-bottom: 15px;}

 .page-scbox .btns-wrp{text-align: center;}
 .page-scbox .desc{margin-bottom: 20px;}
 .page-scbox .video-wrapper { margin-bottom: 14px; position: relative; padding-bottom: 56.25%; height: 0;
     overflow: hidden;}
   .page-scbox .video-wrp{margin-bottom: 40px;}

 .page-scbox .section-curated .box-curated .desc{margin-bottom: 20px;}
 .page-scbox .section-works .box, .page-scbox .section-commitment .box, .page-scbox .section-pricing .box{text-align: center;}
 .page-scbox .section-works .box .subtitle, .page-scbox .section-commitment .box .subtitle, .page-scbox .section-pricing .box .subtitle{line-height: 1.5; margin-bottom: 20px; font-weight: 700; letter-spacing: normal;}
 .page-scbox .section-works .box .img-wrp, .page-scbox .section-commitment .box .img-wrp, .page-scbox .section-pricing .box .img-wrp{margin-bottom: 10px;}
 .page-scbox .section-commitment .box .subtitle{min-height: 44px;}

 .page-scbox .section-packages .title-section .subtitle{font-weight: 700; font-size: 12px; border: none; padding-bottom: 0;}
 .page-scbox .box-package{text-align: center; margin-bottom: 20px; padding: 5px; cursor: pointer; -moz-transition: all 0.2s;
 -webkit-transition: all 0.2s;
 -o-transition: all 0.2s;
 -ms-transition: all 0.2s; transition: all 0.2s;}
 .page-scbox .box-package:hover{outline:1px solid #13D792; }
 .page-scbox .box-package.active{outline:1px solid #13D792; cursor: default; opacity:0.99;}
 
 
.page-scbox .box-package:hover,  .page-scbox .box-package.active{
  -webkit-box-shadow: 0px 0px 0px 1px rgba(19,215,146,1);
  -moz-box-shadow: 0px 0px 0px 1px rgba(19,215,146,1);
  box-shadow: 0px 0px 0px 1px rgba(19,215,146,1);}

 
 
 .page-scbox .box-package.deactive{ opacity:0.55}

 .page-scbox .box-package .img-wrp{ position: relative; margin-bottom: 20px;}
 .page-scbox .box-package .img-wrp img{width:auto; max-width: 100%; height:auto; }
 .page-scbox .box-package .title{font-size: 12px; font-weight: 700; margin-bottom: 10px; color: #000;}
 .page-scbox .box-package .price{font-size: 14px; font-weight: 700; margin-bottom: 10px; color: #000;}
 .page-scbox .box-package .desc{min-height: 60px;}
 .page-scbox .section-packages .btns-wrp{text-align: center;}
 .page-scbox-form .sc-checkbox .icon-diamond{font-size: 26px;}
 .page-scbox-form .sc-checkbox.active .icon-diamond{color: #13D792;}

 @media (max-width:768px) {
   .page-scbox .title-section{margin-bottom: 10px;}
   .page-scbox .title-section.main{margin-bottom: 10px;}
   .page-scbox .title-section .title { font-size: 22px; line-height: 1.3; margin-top: 10px;  }

   .page-scbox .title-section .title span{padding-bottom: 5px;}
   .page-scbox .title-section .subtitle {padding-bottom: 5px; line-height: 1.3; margin: 5px 0 10px;}
   .page-scbox .subtitle{font-size: 12px;}
   .page-scbox .section-banner .title-section .title{font-size: 12px;}
   .page-scbox .img-wrp, .page-scbox  .img-wrp.text-right{text-align: center;}
   .page-scbox .btns-wrp{text-align: center;}

   .page-scbox .section-curated .box-video{margin-bottom: 20px;}
   .video-wrp iframe { max-width: 100%; max-height: 100%; height: auto;}
   .page-scbox .video-wrp{margin-bottom: 20px;}
   .page-scbox .section-works .box .subtitle, .page-scbox .section-commitment .box .subtitle, .page-scbox .section-pricing .box .subtitle{margin-bottom: 10px;}
   .page-scbox .section-commitment .box .subtitle{min-height: inherit;}
   .page-scbox .section-works .box{margin-bottom: 0;}
   .page-scbox .box-package{margin-bottom: 30px;}
   .page-scbox .box-package .desc{min-height: 74px;}
   .page-scbox .box-package .img-wrp{}
   .page-scbox .box-package .img-wrp img{}
   .page-scbox .section{padding: 15px 0 15px;}
   .page-scbox .section-testimonials .img-wrp{margin-bottom: 10px;}
 }

 /* /. page scbox*/

/*mobile-scbox.css starts here***********************************************************************************************/
   @media (max-width:768px) {
  body{font-size: 10px;}
  .form-control{font-size: 10px;}
  .section-shadow { box-shadow: inset 0px 11px 20px -6px rgba(0, 0, 0, 0.2);}

  .title-section .title{font-size: 26px; /*padding-right: 50px;*/line-height: 1.3;}
  .title-section .subtitle{font-size: 12px;}

  .title-section .link-view-all{font-size: 12px;}
  .grid.grid-items.events .item-desc-wrp .date{font-size: 12px;}

  .grid.grid-items.products .item-desc-wrp .category{font-size: 10px;}
  .grid.grid-items .item-desc-wrp .title{font-size: 10px;}
  .grid.grid-items.products .item-desc-wrp .brand{font-size: 10px;}
  .grid.grid-items .item-desc-wrp .price,   .grid.grid-items .item-desc-wrp .mrp,   .grid.grid-items .item-desc-wrp .mrp-price,   .grid.grid-items .item-desc-wrp .discount{font-size: 10px;}
  .grid.grid-items .item .item-media-wrp .ic{font-size: 22px;}
  .grid.grid-items.products .item-desc-wrp{height: 114px;}

  .box-testimonial .title{font-size: 12px;}
  .box-testimonial .name{font-size: 10px;}

  .form-group .message, .form-group label.error{font-size: 9px; margin-top: 5px;}

    .scbtn-tertiary{-moz-transform: translateY(-3px);
-webkit-transform: translateY(-3px);
-o-transform: translateY(-3px);
-ms-transform: translateY(-3px); transform: translateY(-3px);}
    .scbtn-tertiary span{    height: 38px;}

    .form-group-md input + label,
    .form-group-md input.empty + label,
    .form-group-md textarea + label,
    .form-group-md textarea.empty + label,
    .form-group-md select + label,
    .form-group-md select.empty + label{ font-size:10px;}

    .form-group-md input:focus + label, .form-group-md111 input:valid + label,
    .form-group-md input.not-empty + label,
    .form-group-md textarea:focus + label,
    .form-group-md textarea.not-empty + label{   font-size:9px;}

    .form-group-md input ~ label.error,
    .form-group-md textarea ~ label.error,
    .form-group-md select ~ label.error{  font-size:9px;}

    .form-group-md input:focus + label, .form-group-md111 input:valid + label, .form-group-md input.not-empty + label, .form-group-md textarea:focus + label, .form-group-md textarea.not-empty + label{top:0;}

  .swiper-button-prev.front.main{display: none;}

}


</style>
  <!-- <link href='<?php echo base_url(); ?>assets/seventeen/css/custom_scbox.css?v=1.100' rel='stylesheet' type='text/css'>
  <link href='<?php echo base_url(); ?>assets/seventeen/css/mobile_scbox.css?v=1.100' rel='stylesheet' type='text/css'> -->
<?php
  $scbox_pack = unserialize(SCBOX_PACKAGE);
  if(!empty(@$_COOKIE['scbox-gender']) && @$_COOKIE['scbox-gender']!='')
  {
    $tab_type = @$_COOKIE['scbox-gender'];
  }else
  {
    setcookie('scbox-gender', 'women', time() + (86400 * 30), "/");
    $tab_type = @$_COOKIE['scbox-gender']; 
  }
 setcookie('sc_order_id','', -1, "/", "",  0); 
 setcookie('discountcoupon_stylecracker', '', -1, "/"); 
 if(!empty(@$_GET['utm_source']) && !empty(@$_GET['utm_medium']) && !empty(@$_GET['utm_campaign'] ))
{
  setcookie('sc_source',$_GET['utm_source'],time() + 86400, "/");
  setcookie('sc_medium',$_GET['utm_medium'],time() + 86400, "/");
  setcookie('sc_campaign',$_GET['utm_campaign'],time() + 86400, "/"); 
}

?>
<div class="page-scbox">
  <section id="section-banner" class="section section-shadow section-banner">
    <div class="container">
      <div class="box-banner">
        <div class="row">
          <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="title-section home-patch">
              <h1 class="title">
            <span>  <b>A Box Full of Fashion.<br /> That you are Guaranteed to Love.</b><br />
            Put together by our celebrity Stylists.<br /> DELIVERED STRAIGHT TO YOUR DOORSTEP.
          </span></h1>
              <h2 class="subtitle" style="font-weight:normal;"> Simple. Stylish. Stress Free.</h2>
            </div>
            <div>
              <a class="scbtn scbtn-primary link-scrolling" href="#section-pricing" class="link-scrolling" onclick="ga('send', 'event', 'SCBOX-OrderNow','clicked','1-Home');"><span>Order Now</span></a>
            </div>
             <h2 style="font-weight:normal;font-size:14px;text-transform: uppercase;letter-spacing: 2px;"> Upto 30% off on your first box.</h2>
          </div>
          <div class="col-md-4 col-sm-12 col-xs-12">
          </div>
        </div>
      </div>
  </section>
  <section id="section-curated" class="section section-shadow section-curated">
    <div class="container">
      <div class="title-section text-center">
        <h1 class="title"><span>Curated for You</span></h1>
      </div>
      <div class="row">
        <div class="col-md-9 col-sm-12 col-xs-12 text-center">
          <div class="box-video">
              <div class="video-wrp">
        <iframe width="640" height="360" src="https://www.youtube.com/embed/dHzTnCwvkxQ?rel=0&autoplay=1&controls=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
       <!-- <iframe width="640" height="360" src="https://www.youtube.com/embed/dHzTnCwvkxQ" frameborder="0" allowfullscreen></iframe> -->
          <!-- <iframe width="640" height="360" src="https://www.youtube.com/embed/NYQIaHWpARY?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe> -->
          </div>
          <div class="btns-wrp">
            <a class="scbtn scbtn-primary link-scrolling" href="#section-pricing"  onclick="ga('send', 'event', 'SCBOX-OrderNow','clicked','2-Get Started');" ><span>Get Started</span></a>
          </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="box-curated">
            <h3 class="subtitle">Your Style</h3>
            <div class="desc">
              One size does not fit all and we get that. At StyleCracker we curate fashion for you keeping in mind your personality, likes, dislikes, body shape, <b>budget</b> and other important details. Yes, we've got you covered every step of the way.
            </div>
            <h3 class="subtitle">Your Budget</h3>
            <div class="desc">
              StyleCracker is for everyone! Once we've understood your <b>style</b>, our stylists will curate a box for you within your budget. While we do have pre-set amounts and quantities to get you started on your selection, you could also customize your order.
            </div>
            <h3 class="subtitle">Your Convenience</h3>
            <div class="desc">
              StyleCracker is designed to make life easier and more convenient for you. All you need to do is fill out a form, speak to our stylists and get styled. It's that simple! Leave the fashion to the experts and enjoy the stress free shopping experience!
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="section-works" class="section section-shadow section-works" style="background-color: rgba(19,215,146, .1);">
    <div class="container">
      <div class="title-section text-center">
        <h1 class="title"><span>How it Works</span></h1>
      </div>
      <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="box">
            <h3 class="subtitle">We get to know you</h3>
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/scbox/how-it-works-1.png" alt="">
            </div>
            <div class="desc">
             To get started, you'll need to fill out a simple online form. This helps us decode your style preferences. Next, a StyleCracker celebrity stylist will call you to understand your requirements. Once this is done, the stylist will get to work to curate your box.
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="box">
            <h3 class="subtitle">WE SEND YOU THE BOX</h3>
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/scbox/how-it-works-2.png" alt="">
            </div>
            <div class="desc">
              Once our stylist has curated the box keeping in mind your likes, dislikes, body shape and budget we put it all together and ship you, your box of style. Shipping is absolutely free and it gets delivered straight to your doorstep.
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="box">
            <h3 class="subtitle">Keep only what you like</h3>
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/scbox/how-it-works-3.png" alt="">
            </div>
            <div class="desc">
              When you receive the box, check out the selection we've sent you. Try it on for size and to see how it works for you. Keep everything you like. If there's something that doesn't suit you, send it back to us. No questions asked.
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="box">
            <h3 class="subtitle">Give us your feedback</h3>
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/scbox/how-it-works-4.png" alt="">
            </div>
            <div class="desc">
              Our goal is to ensure that you, and every other <b>user</b>, loves every item curated personally for you. Your constant feedback is most valuable to make sure we get even better. So please take a couple of minutes to tell us what you liked and what you didn't.
            </div>
          </div>
        </div>
      </div>

      <div class="btns-wrp">
        <a class="scbtn scbtn-primary link-scrolling" href="#section-pricing" class="link-scrolling" onclick="ga('send', 'event', 'SCBOX-OrderNow','clicked','3-How it works');" ><span>Order Now</span></a>
      </div>
    </div>
  </section>

<section id="section-testimonials" class="section section-shadow section-testimonials">
<div class="container">
<div class="title-section text-center">
<h1 class="title"><span>TESTIMONIALS</span></h1>
<h2 style="font-weight:normal;font-size:14px;text-transform: uppercase;
    letter-spacing: 2px;">We asked a few of our customers about their first experience with the StyleCracker Box</h2>
</div>
  <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="<?php echo base_url(); ?>assets/images/home/banner/esha.jpg" alt="" style="width: 127px;">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>First of all I would like to say thank you!Not only did the stylist get the gist of what I would like, she sent me stuff that I absolutely loved! The order came by very quickly, and reached in time for my birthday too, you guys totally made my day!Keep up the amazing work, and I'm definitely going to subscribe for another box in the near future.</p>
                        <div class="name">
                           - Esha
                        </div>
                     </div>
                  </div>
               </div>

                
                <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="<?php echo base_url(); ?>assets/images/home/banner/shru.jpg" alt="" style="width: 127px;">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p> Love the quality of the products, all questions were asked accurately also the delivery was quick.</p>
                        <div class="name">
                           - Shruti
                        </div>
                     </div>
                  </div>
               </div>
              
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12">
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="<?php echo base_url(); ?>assets/images/home/banner/rivoli.jpg" alt="">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>I am impressed how they called and understood what I wanted, but was super impressed by how they got my fit right. I have ordered a few boxes now and have never returned or disliked anything. I am very glad I signed up for the StyleCracker box and tell all my friends to do the same too. It’s because I personally love what they curate for me.</p>
                        <div class="name">
                           - Rivoli
                        </div>
                     </div>
                  </div>
               </div>
               
               <div class="row extrapad-row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="img-wrp">
                        <img src="<?php echo base_url(); ?>assets/images/home/banner/ved.jpg" alt="" style="width: 127px;">
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     <div class="desc">
                        <p>It is a good concept that the stylists call and takes the details. Amazing style really liked everything; appreciate the efforts of the stylist who had curated the box. Overall, I am happy with the service.</p>
                        <div class="name">
                           - Vedika
                        </div>
                     </div>
                  </div>
               </div>
              
              
            </div>
         </div>
 <div class="btns-wrp extrapad-row">
        <a class="scbtn scbtn-primary" href="<?php echo base_url(); ?>testimonial" target="_blank"><span>View More</span></a>
      </div>
</div>
</section>
  
  <input type='hidden' id="scbox-page" name="scbox-page" value="scbox" />

  <input type='hidden' id="scbox-package" name="scbox-package" value="" />
  <input type='hidden' id="scbox-price" name="scbox-price" value="" />
  <input type='hidden' id="scbox-objectid" name="scbox-objectid" value="" />
  <input type='hidden' id="scbox-productid" name="scbox-productid" value="" />
  <section id="section-commitment" class="section section-shadow section-commitment" style="background-color: rgba(19,215,146, .1);">
    <div class="container">
      <div class="title-section text-center">
        <h1 class="title"><span>Our Commitment</span></h1>
      </div>
      <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="box">
            <h3 class="subtitle">It's all about you</h3>
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/scbox/our-comm-1.png" alt="">
            </div>
            <div class="desc">
              The StyleCracker box is about you. While our stylists put together and curate each item in the box, it's always done keeping your style in mind.
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="box">
            <h3 class="subtitle">Experienced Stylists</h3>
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/scbox/our-comm-2.png" alt="">
            </div>
            <div class="desc">
              All stylists part of the StyleCracker family come with years of experience. From styling celebrities, editorials and films to individuals for weddings and day-to-day requirements, we do it all. So worry not! You are in great hands.
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="box">
            <h3 class="subtitle">Transparent Pricing</h3>
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/scbox/our-comm-3.png" alt="">
            </div>
            <div class="desc">
             Every product you get comes with the official label and price tag, so you'll get what you paid for. More often than not, you'll get stuff worth a lot more.
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="box">
            <h3 class="subtitle">Returns, Replacements and Refunds</h3>
            <!--  <div>
      No Questions Asked
    </div>-->
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/scbox/our-comm-4.png" alt="">
            </div>
            <div class="desc">
              If you don't like something we send in the StyleCracker box, just <b>return it</b>. No questions asked.
            </div>
          </div>
        </div>
      </div>

      <div class="btns-wrp">
        <a class="scbtn scbtn-primary link-scrolling" href="#section-pricing" class="link-scrolling" onclick="ga('send', 'event', 'SCBOX-OrderNow','clicked','5-Our Commitment');" ><span>Order Now</span></a>
      </div>
    </div>
  </section>

  <section id="section-pricing" class="section section-shadow section-pricing">
    <div class="container">
      <div class="title-section text-center">
        <h1 class="title"><span>Pricing</span></h1>
         <h2 style="font-weight:normal;font-size:15px;text-transform: uppercase;
          letter-spacing: 2px;"> Upto 30% off on your first order.</h2>
          <ul class="nav nav-tabs sc-tabs">
        <li class="<?php echo (!empty($tab_type) && $tab_type=='women') ? 'active' : ''; ?>" ><a data-toggle="tab" href="#women" id="womentab" onclick="selecttab('women');" >Women</a></li>
        <li class="<?php echo (!empty($tab_type) && $tab_type=='men') ? 'active' : ''; ?>" ><a data-toggle="tab" href="#men" id="mentab" onclick="selecttab('men');" >Men</a></li>
      </ul>


      </div>

    <!-- MEN Women --> 
     
   <div class="tab-content">
    <div id="women"  class="tab-pane fade in <?php echo (!empty($tab_type) && $tab_type=='women') ? 'active' : ''; ?>">
      <div class="row">
        <?php
            $pricehtml = '';            
            //<input type="tel" class="form-control" name="customboxprice" id="customboxprice" onblur="getcustomprice();" required placeholder="Give us your budget" />
          foreach($scbox_pack as $val){
             if($val->show==1 )
             {
                    if($val->packname=='package4')
                    {
                     $pricehtml ='<div class="form-group">
                        <input type="text" class="form-control" name="customboxprice" id="customboxprice" onblur="getcustomprice();" maxlength="20" placeholder="Give us your budget" />
                        <label id="customboxprice-error" class="message error hide" for="customboxprice">Please enter your budget</label>
                     </div>';
                   }else
                   {
                       $pricehtml = '<div class="price-wrapper"><div class="price-slash"></div><div><i class="fa fa-inr"  ></i>'.$val->price.'</div></div>';
                   }
                   if($val->type=='women' || $val->type=='a' )
                   {
            ?>
          <div class="col-md-3 col-sm-6 col-xs-6">
            <div class="box-package " data-pack="<?php echo $val->packname; ?>" data-scbox-price="<?php echo $val->price; ?>" data-scbox-objectid="<?php echo $val->id; ?>" data-scbox-productid="<?php echo $val->productid; ?>" onclick="SetPackage(this);" style="background: #e4fbf3;">
              <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/scbox/women/<?php echo $val->packname.'.jpg'; ?>" alt="">
              </div>
              <div class="title"><?php echo $val->name; ?></div>
              <!-- <div class="desc">
               <?php //echo $val->desc; ?>
              </div> -->
                <?php echo $pricehtml; ?>
                <div class="price" style="font-size: 18px;"><?php echo $val->notice; ?></div>
            </div>
          </div>

        <?php } } }?>
    </div> 
  </div>

   <div id="men"  class="tab-pane fade in  <?php echo (!empty($tab_type) && $tab_type=='men') ? 'active' : ''; ?>">
      <div class="row">
        <?php
            $pricehtml = '';        
            //<input type="tel" class="form-control" name="customboxprice" id="customboxprice" onblur="getcustomprice();" required placeholder="Give us your budget" />
          foreach($scbox_pack as $val){
             if($val->show==1 )
             {
                    if($val->packname=='package4')
                    {
                     $pricehtml ='<div class="form-group">
                        <input type="text" class="form-control" name="customboxprice1" id="customboxprice1" onblur="getcustomprice();" maxlength="20" placeholder="Give us your budget" />
                        <label id="customboxprice1-error" class="message error hide" for="customboxprice1">Please enter your budget</label>
                     </div>';
                   }else
                  {
                       $pricehtml = '<div class="price-wrapper"><div class="price-slash"></div><div><i class="fa fa-inr"  ></i>'.$val->price.'</div></div>';
                   }
                   if($val->type=='men' || $val->type=='a' )
                   {
            ?>
          <div class="col-md-3 col-sm-6 col-xs-6">
            <div class="box-package " data-pack="<?php echo $val->packname; ?>" data-scbox-price="<?php echo $val->price; ?>" data-scbox-objectid="<?php echo $val->id; ?>" data-scbox-productid="<?php echo $val->productid; ?>" onclick="SetPackage(this);" style="background: #e4fbf3;">
              <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/scbox/men/<?php echo $val->packname.'.jpg'; ?>" alt="">
              </div>
              <div class="title"><?php echo $val->name; ?></div>
             <!--  <div class="desc">
               <?php echo $val->desc; ?>
              </div> -->
                <?php echo $pricehtml; ?>
                <div class="price" style="font-size: 18px;"><?php echo $val->notice; ?></div>
            </div>
          </div>

        <?php } } }?>
    </div> 
  </div>
</div>    

    <!-- MEN Women -->     
    <div class="text-center">
      <a class="scbtn scbtn-primary link-scrolling" href="javascript:void(0);" onclick="scboxregister();" ><span>Order Now</span></a>
      <div class="form-group" ><label class="hide" id="scboxregistererror">Select your scbox package</label></div>
    </div>

      <input type="hidden"  name="utm_source" id="utm_source" value="<?php echo @$_GET['utm_source']; ?>" />
      <input type="hidden"  name="utm_medium" id="utm_medium" value="<?php echo @$_GET['utm_medium']; ?>" />
      <input type="hidden"  name="utm_campaign" id="utm_campaign" value="<?php echo @$_GET['utm_campaign']; ?>" />

      <input type="hidden"  name="tab_type" id="tab_type" value="women" />

    </div>
  </section>
  <section id="section-testimonials" class="section section-shadow section-testimonials">
<div class="container">
<div class="title-section text-center">
<h1 class="title"><span>PRESS</span></h1>
<h2 style="font-weight:normal;font-size:14px;text-transform: uppercase;
    letter-spacing: 2px;"></h2>
</div>
<div class="row">
  <div class="col-md-4 col-sm- col-xs-6">
  <div class="press-img">
    <a href="http://epaper2.mid-day.com/epaperpdf/mumbai/16122017/16122017-md-mn-32.pdf" target="_blank"><img src="<?php echo base_url(); ?>assets/images/press/logo611.jpg"></a>
  </div>
</div>
<div class="col-md-4 col-sm-6 col-xs-6">
  <div class="press-img">
    <a href="https://economictimes.indiatimes.com/small-biz/startups/newsbuzz/alia-bhatt-buys-stake-in-startup-stylecracker/articleshow/61717480.cms?utm_source=WAPusers&utm_medium=whatsappshare&utm_campaign=socialsharebutton&from=mdr" target="_blank"><img src="<?php echo base_url(); ?>assets/images/press/logo11.jpg"></a>
  </div>
</div>
<div class="col-md-4 col-sm-6 col-xs-6">
  <div class="press-img">
    <a href="http://www.timesnownews.com/business-economy/video/alia-bhatt-turns-investor-buys-stake-in-fashion-startup-stylecracker/129737" target="_blank"><img src="<?php echo base_url(); ?>assets/images/press/logo21.jpg"></a>
  </div>
</div>
<div class="col-md-4 col-sm- col-xs-6">
  <div class="press-img">
    <a href="http://www.btvi.in/m/videos/watch/24881/style-cracker--the-story-so-far" target="_blank"><img src="<?php echo base_url(); ?>assets/images/press/logo411.jpg"></a>
  </div>
</div>
<div class="col-md-4 col-sm- col-xs-6">
  <div class="press-img">
    <a href="https://www.vogue.in/content/alia-bhatt-invests-indias-first-styling-portal-stylecracker/" target="_blank"><img src="<?php echo base_url(); ?>assets/images/press/logo3.jpg"></a>
  </div>
</div>

<div class="col-md-4 col-sm- col-xs-6">
  <div class="press-img">
    <a href="https://www.loksatta.com/lekhaa-news/digital-fashion-fashion-design-designer-house-1597117/" target="_blank"><img src="<?php echo base_url(); ?>assets/images/press/logo511.jpg"></a>
  </div>
</div>

</div>
</div>
</section>
<section id="section-curated" class="section section-shadow section-curated">
    <div class="container">
      <div class="title-section text-center">
        <h1 class="title"><span>SC Box Unboxing Videos</span></h1>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
          <div class="box-video">
              <div class="video-wrp">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLIaCEd5ZXw-TULv-52Ce7lMwRvK2TeWSY" frameborder="0" gesture="media" allowfullscreen></iframe>
       <!-- <iframe width="640" height="360" src="https://www.youtube.com/embed/dHzTnCwvkxQ" frameborder="0" allowfullscreen></iframe> -->
          <!-- <iframe width="640" height="360" src="https://www.youtube.com/embed/NYQIaHWpARY?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe> -->
          </div>
            </div>
        </div>
      </div>
    </div>
  </section>
  </div>

 <script type="text/javascript">
  // $( document ).ready(function() {
  //   console.log( "ready!" );
  //   //$("#womentab").trigger( "click" );
  // });
  function selecttab(type)
  { 
    //$('.tab-find').attr('id',type); 
    if(type=='women'){
      $('#'+type).addClass('active');
      $('#men').removeClass('active');
    }else{
       $('#'+type).addClass('active');
       $('#women').removeClass('active');
    }     
    document.cookie="scbox-gender="+type+";path=/";
  }
</script>