<?php
// echo "<pre>";print_r($filter);exit;
?>
  <div class="page-collection">
<section class="section section-shadow">
  <div class="container">
	<div id="filter_collection_product">
    <div class="title-section">
      <h1 class="title"><?php echo @$product_data['collection_data']['collection_name']; ?></h1>
		<!-- //filterbox  filter sort by -->
		<div class="dropdown category-filter-wrp ic-sortby ic-right sortby" >
			<span class="dropdown-toggle" data-toggle="dropdown">
			     <i class="icon icon-sort"></i>
			</span>
			<div class="dropdown-menu">
			  <div class="box-filter" id="sortby">
				<div class="title">
					Sort By
				</div>
				<div class="box-filter-content box-sortby-content">
					<ul>
						<?php $sort=1; foreach($filter['sort_by'] as $key=>$val){ ?><li><label class="sc-checkbox <?php if($sort==1){ echo "active"; }$sort++; ?>" attr-value="<?php echo $key; ?>"><i class="icon icon-diamond"></i> <input type="checkbox" /> <span><?php echo $val; ?></span></label> </li>
						<?php } ?>
					</ul>
				</div>
				    <div class="btns-wrp">
						<button class="scbtn scbtn-primary" onclick="filter_collection_products();"><span>Apply</span></button>
			       </div>
        	</div>
         </div>
        </div>
      	  <!-- ///.filterbox  filter sort by -->

     <!-- //filterbox  filter filterby -->
		<div class="dropdown category-filter-wrp ic-filter ic-right filterby">
			<span class="dropdown-toggle" data-toggle="dropdown">
			     <i class="icon icon-filter"></i>
			</span>
			<div class="dropdown-menu">
			  <div class="box-filter">
				<div class="title">
				  Filter By
				  <a class="btn-clear" href="javascript:void(0)" onclick="clear_filter('collection_filter')">Clear</a>
				</div>
          <div class="box-filter-content box-filterby-content">
          <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4">
    				<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#content-gender">Gender</a></li>
    					<?php if(count($filter['category'])>0){ ?><li><a data-toggle="tab" href="#content-categories">Categories</a></li><?php }//check category list ?>
    					<?php if(!empty($filter['paid_brands'])){ ?><li><a data-toggle="tab" href="#content-brands">Brands</a></li><?php }//check brand list ?>
    					<li><a data-toggle="tab" href="#content-price">Price</a></li>
    					<?php if(!empty($filter['size'])){ ?> <li><a data-toggle="tab" href="#content-size">Size</a></li><?php }//check size list ?>
    				</ul>
      </div>
      <div class="col-md-8 col-sm-8 col-xs-8 tab-content-wrp">
				<div class="tab-content">
					
					<!-- gender list start -->
					<div id="content-gender" class="tab-pane fade in active content-gender scrollbar">
						<ul>
							<?php if(!empty($filter['gender'])){ foreach($filter['gender'] as $key=>$value){ ?>

							<li><label class="sc-checkbox gender-list <?php if($gender_cat_id == $key || count($filter['gender']) == 1){ echo 'active'; } ?>" attr-value="<?php echo $key; ?>" id="gender_list<?php echo $key; ?>"><i class="icon icon-diamond"></i> <input type="checkbox" /> <span><?php echo $value; ?></span></label> </li>

							<?php }//foreach
							}//if gender list ?>
						</ul>
					</div>
					<!-- gender list end -->
					
					<div id="content-categories" class="tab-pane fade content-categories scrollbar">
						<ul>
							<?php $i=1;if(!empty($filter['category'])){ foreach($filter['category'] as $value){ if($i!=1){?>
								<li><label class="sc-checkbox" attr-value="<?php echo $value['category_id']; ?>" ><i class="icon icon-diamond"></i><span><?php echo $value['category_name']; ?></span></label> </li>
							<?php }else{ $active='';if(!empty($selected_lastlevelcat)){if(in_array($value['category_id'],$selected_lastlevelcat)){ $active = 'active'; }} ?>
								<li><label class="sc-checkbox <?php echo $active; ?>" attr-value="<?php echo $value['category_id']; ?>" ><i class="icon icon-diamond"></i><span><?php echo $value['category_name']; ?></span></label> </li>
							<?php } ?>

							<?php $i++;}//foreach
							}//if categories list ?>
						</ul>
					</div>

					<div id="content-brands" class="tab-pane fade content-brands scrollbar">
						<ul>
							<?php if(!empty($filter['paid_brands'])){ foreach($filter['paid_brands'] as $value){ ?>
							<!--<li attr-value="<?php echo $value['brand_id']; ?>"> <i class="ic icon-correct"></i> <span><?php echo $value['company_name']; ?></span></li>-->
							<li><label class="sc-checkbox price-list" attr-value="<?php echo $value['brand_id']; ?>" ><i class="icon icon-diamond"></i><span><?php echo $value['company_name']; ?></span></label> </li>
							<?php }//foreach
							}//if brands list ?>
						</ul>
					</div>

					<!-- price list start-->
					<div id="content-price" class="tab-pane fade content-price scrollbar">
						<ul>
							<?php if(!empty($filter['price_list'])){ foreach($filter['price_list'] as $key=>$value){ ?>

							<li><label class="sc-checkbox price-list" attr-value="<?php echo $key+1; ?>" id="price_list<?php echo $key; ?>"><i class="icon icon-diamond"></i> <input type="checkbox" /> <span><?php echo $value; ?></span></label> </li>

							<?php }//foreach
							}//if price list ?>
						</ul>
					</div>
					<!-- price list end-->

					<!-- size list start-->
					<div id="content-size" class="tab-pane fade content-size scrollbar">
						<ul>
						<?php if(!empty($filter['size'])){ foreach($filter['size'] as $value){ ?>
								<!--<li attr-value="<?php echo $value['id']; ?>" > <i class="ic icon-correct"></i> <span><?php echo $value['size_text']; ?></span></li>-->
								<li><label class="sc-checkbox" attr-value="<?php echo $value['id']; ?>" ><i class="icon icon-diamond"></i><span><?php echo $value['size_text']; ?></span></label> </li>
							<?php }//foreach
							}//if size list ?>
						</ul>
					</div>
					<!-- size list end-->

					<!-- color list start-->
					<!--<div id="content-color" class="tab-pane fade content-color content-category-attr scrollbar">
						<ul>
						<?php if(!empty($filter['colour'])){ foreach($filter['colour'] as $value){ ?>
							<li><label class="sc-checkbox" attr-value="<?php echo $value['id']; ?>">
								<i class="icon icon-diamond-s" style="color:<?php echo $value['attr_hashtag']?>;"></i>
							</label> </li>
						<?php }//foreach
						}//if colour list ?>
						</ul>
					</div>-->
					<!-- color list end-->

				</div>
            </div>
           </div>
       </div>
      <div class="btns-wrp">
            <button class="scbtn scbtn-primary" onclick="filter_collection_products();"><span>Apply</span></button>
         </div>
			</div>
    </div>
    </div>
	  <!-- //filterbox  filter filterby -->
    </div>
    <div class="collection-desc">
      <p>
       <?php echo @$product_data['collection_data']['collection_short_desc']; ?>
      </p>
      <!--<div class="result-count"> <?php echo $total_products; ?> results found  </div>-->
    </div>

	<div class="grid grid-items products">
	<div class="row" id="get_all_products">
    <!--test-->
	<?php if(!empty($products)){
			foreach($products as $val){  
				$product_name = $val->product_name; 
				$val->product_name = preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_name);
				$val->product_slug = preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_slug);
	?>
			<div class="col-md-3 col-sm-6 col-xs-6 grid-col">
			  <div class="item-wrp">
				<div class="item">
				  <div class="item-img-wrp" onclick="_targetProductClick(<?php echo $val->product_id; ?>,'6','<?php echo @$val->product_name; ?>');" >
					<a href="<?php echo base_url().PRODUCT_URL.$val->product_slug.'-'.$val->product_id; ?>"><img src="<?php echo $val->product_img; ?>" alt=""></a>
				  </div>
				  <div class="item-desc-wrp">
					<div class="category"><a href="<?php echo CAT_URL.$val->product_category_slug.'-'.$val->product_category_id; ?>"><?php echo $val->product_category; ?></a></div>
					<div class="title" onclick="_targetProductClick(<?php echo $val->product_id; ?>,'6','<?php echo @$val->product_name; ?>');" ><a href="<?php echo base_url().PRODUCT_URL.$val->product_slug.'-'.$val->product_id; ?>"><?php echo $product_name; ?></a></div>
					<div class="brand">By <a href="<?php echo BRAND_URL.$val->brand_slug.'-'.$val->brand_id; ?>"><?php echo $val->brand_name; ?></a></div>
					<div class="item-price-wrp">
					  <?php if($val->price > $val->discount_price){ ?>
								<div class="mrp"><i class="fa fa-inr"></i><?php echo $val->price; ?></div>
								<div class="price"><i class="fa fa-inr"></i><?php echo $val->discount_price; ?></div>
								<div class="discount"><?php echo $val->discount_percent; ?>% OFF</div>
					  <?php }else { ?>
								<div class="price"><i class="fa fa-inr"></i><?php echo $val->price; ?></div>
					  <?php }?>
					</div>
				  </div>
				  <div class="item-media-wrp">
						<?php $size_ids='';$size_texts='';$size_id= array();$size_text= array();
						if(!empty($val->product_size)){
							foreach($val->product_size as $size){
								$size_id[] = $size->size_id;
								$size_text[] = $size->size_text;
							}// foreach
						}//if
						if(!empty($size_id))$size_ids = implode(',',$size_id);
						if(!empty($size_text))$size_texts = implode(',',$size_text);
					?>
					<span class="ic ic-plus" title="Add to Cart">
					<span data-toggle="modal"  id="<?php echo @$val->product_id; ?>" value="<?php echo @$val->product_id; ?>" name="prod_size" data-sizes="<?php echo $size_ids; ?>" data-label="<?php echo $size_texts; ?>" data-productname="<?php echo $val->product_name; ?>" data-productprice="<?php echo $val->discount_price; ?>" data-brandname="<?php echo $val->brand_name; ?>" data-category="<?php echo $val->product_category; ?>" onclick="GetProductSize(this);">
					<i class="icon-plus"></i>
					</span>
					</span>
					<span class="ic ic-share" title="Share with Stylist" onclick="send_message('product','<?php echo @$val->product_id; ?>','<?php echo @$val->product_name; ?>','<?php echo @$val->product_img; ?>','<?php echo base_url().PRODUCT_URL.$val->product_slug.'-'.$val->product_id; ?>',<?php if($user_id > 0){ echo @$user_id; }else { echo '0'; } ?>);">

					  <i class="icon-send"></i>
					</span>
				  </div>
				<span title="Add to Wishlist" class="ic-heart <?php if(@$val->is_wishlisted == '1') echo "active"; ?>" id="wishlist_product<?php echo @$val->product_id; ?>" onclick="add_to_fav('product',<?php echo @$val->product_id; ?>,<?php if($user_id > 0){ echo @$user_id; }else { echo '0'; } ?>,'<?php echo @$val->product_name; ?>');">
					  <i class="icon-heart"></i>
				</span>
				</div>
			  </div>
			</div>

		<?php }//foreach
		} //if ?>
	</div>
	</div>
	</div>
	<input type="hidden" name="offset" id="offset" value="1">
	<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
	<input type="hidden" name="object_id" id="object_id" value="<?php echo $object_id; ?>">
	<input type="hidden" name="total_products" id="total_products" value="<?php echo $total_products; ?>">
	<input type="hidden" name="gender_cat_id" id="gender_cat_id" value="<?php echo $gender_cat_id; ?>">
	<input type="hidden" name="arrCategorySelected" id="arrCategorySelected" value="">
	<input type="hidden" name="arrSizeSelected" id="arrSizeSelected" value="">
	<input type="hidden" name="arrBrandSelected" id="arrBrandSelected" value="">
	<input type="hidden" name="arrCatAttrSelected" id="arrCatAttrSelected" value="">
	<input type="hidden" name="intPriceSelected" id="intPriceSelected" value="">
	<input type="hidden" name="intSortBy" id="intSortBy" value="">
	<div id="load-more" class="fa-loader-item-wrp">
			<div class="fa-loader-item">
			<i class="fa fa-circle-o-notch fa-spin-custom"></i> Loading...
			</div>
	</div>
  </div>
</section>
<section class="section section-shadow">
  <div class="container">
    <div class="title-section">
      <h1 class="subtitle">Recommended Collections</h1>
	  </div>

    <div class="grid grid-items grid-items-slider sm-slider recommended collections">
      <div class="row grid-row">
 <?php
		  if(!empty($recommended_collection)){ foreach($recommended_collection as $val){?>
            <div class="col-md-3 col-sm-12 col-xs-12 grid-col">
              <div class="item-wrp overlay">
			     <a href="<?php echo base_url(); ?>collections/<?php echo trim(@$val['object_slug']).'-'.trim(@$val['collection_id']); ?>">
                <div class="item">
                  <div class="item-img-wrp">
                    <img src="<?php echo @$val['collection_image']; ?>" alt="">
                  </div>
                  <div class="item-desc-wrp">

                    <div class="inner">
                     <!-- <h2 class="title"><?php echo @$val['collection_name']; ?></h2>-->
                      <div class="title">
					  <?php echo @$val['collection_name']; ?>
                        <?php //echo @$val['collection_short_desc']; ?>
                      </div>
                     <!-- <div class="btns-wrp">
                        <a href="<?php echo base_url(); ?>collection_web/single_collection/<?php echo @$val['collection_id']; ?>" class="scbtn scbtn-secondary white"><span>View Collection</span></a>
                      </div>-->
                    </div>

                  </div>
					<!--<span class="ic-heart <?php if(@$val['is_wishlisted'] == '1') echo " active "; ?>" id="wishlist_c<?php echo @$val['collection_id']; ?>" onclick="add_to_fav('collection',<?php echo @$val['collection_id']; ?>,<?php echo @$this->session->userdata('user_id'); ?>);">
						<i class="icon-heart"></i>
					</span>-->
                </div>
				</a>
              </div>
            </div>
            <?php }//foreach
			}//if	?>
      </div>
	  </div>
	  </div>
	  </section>
</div>