<style>


.form-group-md textarea + label,
.form-group-md textarea.empty + label{
  color:#777 !important;
  font-size:15px !important;
  font-weight:500 !important;
  letter-spacing: 1px !important;
  text-transform: none !important;
}


/*.page-scbox-form .list-prints li{width:180px; margin: 10px 15px;}
.page-scbox-form .list-prints li .pallet {height:180px;}
*/
.page-scbox-form .list-skin-tone li .title-3{padding-top: 15px;}
@media(max-width:768px){
  /*
.page-scbox-form .list-prints li{width:100px;}
.page-scbox-form .list-prints li .pallet {height:100px; width:100px;}*/

.page-scbox-form ul.list-skin-tone li { width: 60px;}
}
.control {
    display: block;
    position: relative;
    padding-left: 30px;
    margin-bottom: 8px;
    padding-top: 3px;
    cursor: pointer;
    font-size: 14px;
    color: #777;
    font-weight: 400;
}
.title-2{
    color: #777 !important;
    font-weight: 500 !important;
    margin-bottom: 5px !important;
    text-transform: none !important;
    font-size: 15px;
    letter-spacing: 1px;
}
.form-group-md .textarea1{
     text-transform: capitalize !important;
     padding-top: 16px !important;
}
    .control input {
        position: absolute;
        z-index: -1;
        opacity: 0;
    }
.control_indicator {
    position: absolute;
    top: 1px;
    left: 0;
    height: 20px;
    width: 20px;
    background: #DCDCDC;
}
.control-radio .control_indicator {
    border-radius: 50%;
}

.control:hover input ~ .control_indicator,
.control input:focus ~ .control_indicator {
    background: #d1f7ea;
}

.control input:checked ~ .control_indicator {
    background: #13D792;
}
.control:hover input:not([disabled]):checked ~ .control_indicator,
.control input:checked:focus ~ .control_indicator {
    background: #13D792;
}
.control input:disabled ~ .control_indicator {
    background: #e6e6e6;
    opacity: 0.6;
    pointer-events: none;
}
.control_indicator:after {
    box-sizing: unset;
    content: '';
    position: absolute;
    display: none;
}
.control input:checked ~ .control_indicator:after {
    display: block;
}
.control-radio .control_indicator:after {
    left: 7px;
    top: 7px;
    height: 6px;
    width: 6px;
    border-radius: 50%;
    background: #13D792;
}
.error{color:red;}
</style>


<?php 
  $feedback_ques = unserialize(FEEDBACK_QUESTIONS);
  //echo '<pre>';print_r($user_response);
 // echo @$success;

  
?>
  <section class="section section-shadow page-cart">
  <?php 
              if($success==1)
            {
              echo '<div><h4 class="title" style="text-align: center;">Thank you for your feedback.</h4></div>';
            }  

  ?>
    <div class="container <?php echo ($success==1) ? 'hide' : '';?>" id="sccart_product_view">            
           <div class="title-section text-center">
            <h1 class="title"><span style="border-bottom:1px solid #13D792;">Feedback</span></h1>
            </div>
            <div class="box-body" style="margin: 25px;">
               <h2 style="font-weight:normal;font-size:14px;text-transform: none;letter-spacing: 1px;">Hi <b><?php echo $first_name; ?></b>, </h2>
               <h2 style="font-weight:normal;font-size:14px;text-transform: none;letter-spacing: 1px;line-height: 20px;">Thank you for sharing your feedback with the StyleCracker box experience with us. To express how grateful we are, we'll be sending you a coupon code via e-mail to get 20% off your next order on submitting the form below.</h2>
           </div>
    <form name="feedback" id="feedback" method="post">
     <div class="box-cart-summary" id="">            
                <div class="box-body row">
                    <div class="col-md-12 col-sm-12 col-xs-12">

	<?php
      foreach($feedback_ques as $val)
      {  
          $extremely_dissatisfied = '';
          $dissatisfied = '';
          $average = '';
          $satisfied = '';
          $extremely_satisfied = '';
          $none = '';      
        if($val->is_show==1)
        {
           if(isset($user_response[$val->name]))
           {
              if($user_response[$val->name]=='extremely_dissatisfied')
              {            
                $extremely_dissatisfied = 'checked="checked"';
              }else if($user_response[$val->name]=='dissatisfied')
              {
                $dissatisfied = 'checked="checked"';
              }else if($user_response[$val->name]=='average')
              {
                $average = 'checked="checked"';
              }else if($user_response[$val->name]=='satisfied')
              {
                $satisfied = 'checked="checked"';
              }else if($user_response[$val->name]=='extremely_satisfied')
              {
                $extremely_satisfied = 'checked="checked"';
              }else if($user_response[$val->name]=='none')
              {
                $none = 'checked="checked"';
              }
          
           }

              
?>
                         <div class="row" style="padding-top: 30px;">
                                <div class="col-md-7 col-sm-12 col-xs-12">
                                        <div class="title-2" style="padding-bottom: 2px;"><?php echo $val->question; ?></div>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12">        
                                       <div class="control-group">
                                           
                                            <label class="control control-radio">
                                               Extremely Satisfied
                                                    <input type="radio" name="<?php echo $val->name; ?>" value="extremely_satisfied" 
                                                    <?php echo  $extremely_satisfied; ?> required />
                                                <div class="control_indicator"></div>
                                            </label>
                                            <label class="control control-radio">
                                               Satisfied
                                                    <input type="radio" name="<?php echo $val->name; ?>" value="satisfied" <?php echo  $satisfied; ?> required />
                                                <div class="control_indicator"></div>
                                            </label>
                                            <label class="control control-radio">
                                              Average
                                                    <input type="radio" name="<?php echo $val->name; ?>" value="average" <?php echo  $average; ?> required />
                                                <div class="control_indicator"></div>
                                            </label>
                                            <label class="control control-radio">
                                               Dissatisfied
                                                    <input type="radio" name="<?php echo $val->name; ?>" value="dissatisfied" <?php echo  $dissatisfied; ?> required />
                                                <div class="control_indicator"></div>
                                            </label>
                                        </div>
										<div class="text-danger feedback_error"></div>
                                 </div>        
                             </div>
							<?php
							  }}
							?>
                             <div class="row" style="padding-top: 30px;">
                                <div class="col-md-7 col-sm-12 col-xs-12">
                                        <div class="title-2" style="padding-bottom: 2px;">Return process (if applicable)</div>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12">        
                                       <div class="control-group">
                                           
                                            <label class="control control-radio">
                                               Extremely Satisfied
                                                    <input type="radio" name="returnp" value="extremely_satisfied" <?php echo (isset($user_response['returnp']) && @$user_response['returnp']=='extremely_satisfied') ? 'checked="checked"'  : ''; ?> required />
                                                <div class="control_indicator"></div>
                                            </label>
                                            <label class="control control-radio">
                                               Satisfied
                                                    <input type="radio" name="returnp" value="satisfied" <?php echo (isset($user_response['returnp']) && @$user_response['returnp']=='satisfied') ? 'checked="checked"'  : ''; ?> required />
                                                <div class="control_indicator"></div>
                                            </label>
                                            <label class="control control-radio">
                                              Average
                                                    <input type="radio" name="returnp" value="average" <?php echo (isset($user_response['returnp']) && @$user_response['returnp']=='average') ? 'checked="checked"'  : ''; ?> required />
                                                <div class="control_indicator"></div>
                                            </label>
                                            <label class="control control-radio">
                                               Dissatisfied
                                                    <input type="radio" name="returnp"  value="dissatisfied" <?php echo (isset($user_response['returnp']) && @$user_response['returnp']=='dissatisfied') ? 'checked="checked"'  : ''; ?> required />
                                                <div class="control_indicator"></div>
                                            </label>  
                                            <label class="control control-radio">
                                               None
                                                    <input type="radio" name="returnp" value="none" <?php echo (isset($user_response['returnp']) && @$user_response['returnp']=='none') ? 'checked="checked"'  : ''; ?> required />
                                                <div class="control_indicator"></div>
                                            </label>
                                        </div>
										<div class="text-danger feedback_error"></div>
                                 </div>       
                             </div>
                             <div class="row" style="padding-top: 30px;">
                                <div class="col-md-7 col-sm-12 col-xs-12">
                                        <div class="title-2" style="padding-bottom: 2px;">Refund process (if applicable)</div>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12">        
                                       <div class="control-group">
                                            <label class="control control-radio">
                                               Extremely Satisfied
                                                    <input type="radio" name="refundp" value="extremely_satisfied" <?php echo (isset($user_response['refundp']) && @$user_response['refundp']=='extremely_satisfied') ? 'checked="checked"'  : ''; ?> required />
                                                <div class="control_indicator"></div>
                                            </label>
                                            <label class="control control-radio">
                                               Satisfied
                                                    <input type="radio" name="refundp" value="satisfied" <?php echo (isset($user_response['refundp']) && @$user_response['refundp']=='satisfied') ? 'checked="checked"'  : ''; ?>  required />
                                                <div class="control_indicator"></div>
                                            </label>
                                            <label class="control control-radio">
                                              Average
                                                    <input type="radio" name="refundp" value="average" <?php echo (isset($user_response['refundp']) && @$user_response['refundp']=='average') ? 'checked="checked"'  : ''; ?>  required />
                                                <div class="control_indicator"></div>
                                            </label>
                                            <label class="control control-radio">
                                               Dissatisfied
                                                    <input type="radio" name="refundp" value="dissatisfied" <?php echo (isset($user_response['refundp']) && @$user_response['refundp']=='dissatisfied') ? 'checked="checked"'  : ''; ?>  required />
                                                <div class="control_indicator"></div>
                                            </label>
                                            <label class="control control-radio">
                                               None
                                                    <input type="radio" name="refundp" value="none" <?php echo (isset($user_response['refundp']) && @$user_response['refundp']=='none') ? 'checked="checked"'  : ''; ?> required />
                                                <div class="control_indicator"></div>
                                            </label>
                                        </div>
										<div class="text-danger feedback_error"></div>
                                 </div>       
                             </div>
                              <div class="row" style="padding-top: 30px;">
                                <div class="col-md-7 col-sm-12 col-xs-12">
                                        <div class="title-2" style="padding-bottom: 2px;">Would you order an SC Box again?</div>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12">        
                                       <div class="control-group">
                                            <label class="control control-radio">
                                               Yes
                                                    <input type="radio" name="orderagain" value="yes" <?php echo (isset($user_response['orderagain']) && @$user_response['orderagain']=='yes') ? 'checked="checked"'  : ''; ?> required />
                                                <div class="control_indicator"></div>
                                            </label>
                                            <label class="control control-radio">
                                               No
                                                    <input type="radio" name="orderagain" value="no" <?php echo (isset($user_response['orderagain']) && @$user_response['orderagain']=='no') ? 'checked="checked"'  : ''; ?> required />
                                                <div class="control_indicator"></div>
                                            </label>
                                        </div>
										<div class="text-danger feedback_error"></div>
                                 </div>       
                             </div>
                             <div class="row" style="padding-top: 30px;">
                                <div class="col-md-7 col-sm-12 col-xs-12">
                                        <div class="title-2" style="padding-bottom: 2px;">Would you recommend the SC Box to your friends and family?</div>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12">        
                                       <div class="control-group">
                                            <label class="control control-radio">
                                               Yes
                                                    <input type="radio" name="recommend" value="yes" <?php echo (isset($user_response['recommend']) && @$user_response['recommend']=='yes') ? 'checked="checked"'  : ''; ?> required />
                                                <div class="control_indicator"></div>
                                            </label>
                                            <label class="control control-radio">
                                               No
                                                    <input type="radio" name="recommend" value="no" <?php echo (isset($user_response['recommend']) && @$user_response['recommend']=='no') ? 'checked="checked"'  : ''; ?> required />
                                                <div class="control_indicator"></div>
                                            </label>
                                        </div>
										<div class="text-danger feedback_error"></div>
                                 </div>       
                             </div>
                             
                              <div class="row" style="padding-top: 30px;">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                 <div class="form-group-md">
                                          <textarea class="textarea1" placeholder=""  name="stylistexperience" id="" rows="3"><?php echo (isset($user_response['stylistexperience']) && @$user_response['stylistexperience']!='') ? $user_response['stylistexperience']  : ''; ?></textarea>
                                           <label>Did the stylist go out of the way to make your experience a little extra pleasurable? If yes, please let us know.</label>
                                        
                                </div>
                                      
                             </div>
                             </div>
                                <div class="row" style="padding-top: 30px;">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                 <div class="form-group-md">
                                          <textarea placeholder=""  name="processexperience" id="" rows="3"><?php echo (isset($user_response['processexperience']) && @$user_response['processexperience']!='') ? $user_response['processexperience']  : ''; ?></textarea>
                                           <label>Is there anything we could do in the entire process to further enhance your experience?.</label>
                                        
                                </div>
                                      
                             </div>
                             </div>
                             <div class="row" style="padding-top: 30px;">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                 <div class="form-group-md">
                                          <textarea placeholder=""  name="anyfeedback" id="anyfeedback" rows="3"><?php echo (isset($user_response['anyfeedback']) && @$user_response['anyfeedback']!='') ? $user_response['anyfeedback']  : ''; ?></textarea>
                                           <label>Any other feedback</label>
                                        
                                </div>
                                      
                             </div>
                             </div>
                                             
                                    
                    </div>
                </div>           
             <center><div class="btns-wrp" style="padding-bottom:20px; ">
              <button type="submit" class="scbtn scbtn-primary" id="feedback_btn"><span>Submit</span></button>
           <!--  <a class="scbtn scbtn-primary" href="javascript:void(0);"><span>Submit</span></a> -->
      </div></center>
        </div>
       </form>
    </div>
</section>


<script>
/* $(document).ready(function(){
	  

$("#feedback_btn").click(function(){
	var isEmailSent = false;
		alert('fcgfg');
	if(isEmailSent==false){
	isEmailSent = true;
	//$("#feedback_btn").prop('disabled','disabled');
}
else {
		alert('feedback email already send');
		console.log("feedback email already send");
	}
});
 
}); */
</script>
