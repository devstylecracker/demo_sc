<?php
 // echo "<pre>";print_r($product_data);exit;
 $product_data['size_guide_url'] = 'https://www.stylecracker.com/assets/images/size_guide/stylecracker/footwear_men.jpg';
 // echo $product_data['name'];exit;
?>
<section class="section section-shadow">
  <div class="container">
    <div class="page-product">
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <?php echo $product_breadcrumps; ?>
      </ol>
      <div class="product-item">

        <div class="row">
          <!-- product image -->
          <div class="col-md-5 col-sm-4 col-xs-12 zoom-gallery-wrp">
            <div class="product-gallery-wrp">
              <div class="slider-top product">

			  <?php $i=1;if(!empty(@$product_data['extra_images'])){
						foreach($product_data['extra_images'] as $val){ ?>
							<div>
							  <div class="item-img-wrp img-wrp <?php if($i==1){ echo "zoom"; } ?>">
                  <a href="<?php echo $val['image_url']; ?>">
								<img src="<?php echo $val['image_url']; ?>" data-zoom-image="<?php echo $val['zoom_image']; ?>">

              </a>
							  </div>
							</div>
			 <?php
						$i++;
					}//foreach
				} ?>
              </div>
              <span class="ic ic-heart sc-heart <?php if(@$product_data['is_wishlisted'] == '1') echo "active"; ?>" id="wishlist_product<?php echo @$product_data['product_id']; ?>" onclick="add_to_fav('product',<?php echo @$product_data['product_id']; ?>,<?php if($user_id > 0){ echo @$user_id; }else { echo '0'; } ?>,'<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$product_data['name']); ?>');">
                <i class="icon-heart"></i></span>

              <span class="ic ic-zoom-s hide"><i class="icon-zoom-s">
                <span class="path1"></span><span class="path2"></span><span class="path3"></span>
              </i></span>
            </div>
            <!--  <div id="zoom-place-holder" class="zoom-place-holder"> </div>-->
            <!-- thumb -->
            <div class="slider-thumbs product">
			<?php if(!empty(@$product_data['extra_images']) && count(@$product_data['extra_images'])>1){
						foreach($product_data['extra_images'] as $val){ ?>
						  <div class="slide">
							<div class="item-img-inner">
							  <img src="<?php echo $val['thumbnail_image']; ?>">
							</div>
						  </div>
			  <?php }//foreach
				} //if ?>
            </div>

            <!-- /thumb -->
          </div>
          <!-- /. product image -->
          <div class="col-md-7 col-sm-8 col-xs-12">
            <div class="item-desc-wrp">
              <h1 class="title"><?php echo $product_data['name']; ?></h1>
			  <div class="brand">By <a href="<?php echo BRAND_URL.$product_data['brand_slug'].'-'.$product_data['brand_id']; ?>"><?php echo $product_data['brand_name']; ?></a></div>
              <div class="item-price-wrp">
					  <?php $product_price=0; if($product_data['price'] > $product_data['discount_price']){ ?>
								<span class="mrp"><i class="fa fa-inr"></i><?php echo $product_data['price']; if(empty($product_data['product_size'])){ echo "  (OUT OF STOCK) "; } ?></span>
								<span class="price"><i class="fa fa-inr"></i><?php echo $product_price = $product_data['discount_price']; ?></span>
								<span class="discount">(<?php echo $product_data['discount_percent']; ?>% OFF)</span>
					  <?php }else { ?>
								<div class="price"><i class="fa fa-inr"></i><?php echo $product_price = $product_data['price']; if(empty($product_data['product_size'])){ echo " (OUT OF STOCK) "; } ?></div>
					  <?php }?>
					</div>
				<div id="size" class="item-sizes-wrp">
            <div class="text-label">
              Size
            </div>

					<?php if(!empty($product_data['product_size'])){
							foreach($product_data['product_size'] as $value){ if(strlen($value->size_text) < 3){ ?>
								<span class="btn-diamond btn-size productsize" attr-size="<?php echo $value->size_id; ?>" ><?php echo $value->size_text; ?></span>
					<?php }else{ ?>
						<span class="btn-diamond-secondary btn-size productsize" attr-size="<?php echo $value->size_id; ?>" ><span><?php echo $value->size_text; ?></span></span>
					<?php	}//else
						}//foreach
					} //if ?>

			   </div>

              <div class="btns-wrp">
              	<button class="scbtn scbtn-primary <?php if(count($product_data['product_size'])==0){ echo "Disabled"; } ?>"  <?php if(count($product_data['product_size'])==0){ echo "Disabled"; } else{ ?>onclick="add_to_cart(<?php echo $product_data['product_id']; ?>,'0','from_prodct_page','<?php echo $product_data['name']; ?>','<?php echo $product_data['product_category']; ?>','<?php echo $product_price; ?>');ga('send', 'event', 'Product', 'add_to_cart', '<?php echo $product_data['brand_name']; ?>-<?php echo $product_data['name']; ?>','<?php echo $product_data['discount_price']; ?>');"<?php } ?> ><span>Add to Cart</span></button>
                <button class="scbtn scbtn-primary btn-share" onclick="send_message('product','<?php echo @$product_data['product_id']; ?>','<?php echo @$product_data['name']; ?>','<?php echo @$product_data['extra_images']['0']['thumbnail_image']; ?>','<?php echo base_url().PRODUCT_URL.$product_data['product_slug'].'-'.$product_data['product_id']; ?>',<?php if($user_id > 0){ echo @$user_id; }else { echo '0'; } ?>);" ><span>Share with Stylist</span></button>

<!--
                <span class="ic ic-share " data-toggle="tooltip" data-placement="right" title="Share with Stylist" onclick="send_message('product','<?php echo @$product_data['product_id']; ?>','<?php echo @$product_data['name']; ?>','<?php echo @$product_data['extra_images']['0']['thumbnail_image']; ?>','<?php echo base_url().PRODUCT_URL.$product_data['product_slug'].'-'.$product_data['product_id']; ?>',<?php if($user_id > 0){ echo @$user_id; }else { echo '0'; } ?>);" ">
					             <i class="icon-send"></i>
					       </span> -->
              </div>
			  
			<?php if($product_data['size_category'] > 0){ ?>
              <div class="size-chart">
                <a href="#" data-toggle="modal" data-target="#size_guide<?php echo @$product_data['product_id']; ?>">View size chart</a>
              </div>
			<?php }//check size_guide exist or not ?>
			
              <div class="tabs-wrp">
                <div>
                  <ul class="nav nav-tabs sc-tabs">
                    <li class="active"><a data-toggle="tab" href="#description">Description</a></li>
                    <?php if($product_data['brand_desc'] != ''){ ?><li><a data-toggle="tab" href="#brand">Brand</a></li><?php } ?>
                  </ul>
                </div>
                <div class="tab-content sc-tab-content">
                  <div id="description" class="tab-pane fade in active">
                    <div class="desc">
                     <?php echo $product_data['description']; ?>
                    </div>
                  </div>
                  <div id="brand" class="tab-pane fade">
                    <div class="desc">
                     <?php echo $product_data['brand_desc']; ?>
                    </div>
                  </div>
                </div>
              </div>
<!--
              <h2 class="subtitle">
                  Description
                </h2>
              <div class="desc">
               <?php echo $product_data['description']; ?>
              </div>
            -->
              <!--<div class="read-more">
                <a href="#">Show more details</a>
              </div>-->

              <!--   <div class="item-media-wrp">
                   <span class="ic ic-plus">
                 <i class="fa fa-plus"></i>
               </span>
                   <span class="ic ic-share">
                 <i class="fa fa-paper-plane-o"></i>
               </span>
             </div>-->

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php if(!empty($related_product)){ ?>
<section class="section section-shadow">
  <div class="container">
    <div class="title-section">
      <h2 class="subtitle">Recommended Products</h2>
      <!--<a href="#" class="link-view-all">View All Products</a>-->
    </div>
    <div class="grid grid-items products">
      <div class="row">

        <?php foreach($related_product as $val){  
				$product_name = $val->product_name; 
				$val->product_name = preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_name);
				$val->product_slug = preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_slug);
		?>
			<div class="col-md-3 col-sm-6 col-xs-6 grid-col">
			  <div class="item-wrp">
				<div class="item">

				  <div class="item-img-wrp" onclick="_targetProductClick(<?php echo $val->product_id; ?>,'6','<?php echo @$val->product_name; ?>');">

					<a href="<?php echo base_url().PRODUCT_URL.$val->product_slug.'-'.$val->product_id; ?>"><img src="<?php echo $val->product_img; ?>" alt=""></a>
				  </div>
				  <div class="item-desc-wrp">
					<div class="category"><?php echo $val->product_category; ?></div>
					<div class="title" onclick="_targetProductClick(<?php echo $val->product_id; ?>,'6','<?php echo @$val->product_name; ?>');"><?php echo $product_name; ?></div>

					<div class="brand">By <a href="<?php echo BRAND_URL.$val->brand_slug.'-'.$val->brand_id; ?>"><?php echo $val->brand_name; ?></a></div>
					<div class="item-price-wrp">
					  <?php if($val->price > $val->discount_price){ ?>
								<div class="mrp"><i class="fa fa-inr"></i><?php echo $val->price; ?></div>
								<div class="price"><i class="fa fa-inr"></i><?php echo $val->discount_price; ?></div>
								<div class="discount"><?php echo $val->discount_percent; ?>% OFF</div>
					  <?php }else { ?>
								<div class="price"><i class="fa fa-inr"></i><?php echo $val->price; ?></div>
					  <?php }?>
					</div>
				  </div>
				  <div class="item-media-wrp">
					<?php $size_ids='';$size_texts='';$size_id= array();$size_text= array();
						if(!empty($val->product_size)){
							foreach($val->product_size as $size){
								$size_id[] = $size->size_id;
								$size_text[] = $size->size_text;
							}// foreach
						}//if
						if(!empty($size_id))$size_ids = implode(',',$size_id);
						if(!empty($size_text))$size_texts = implode(',',$size_text);
					?>
					<span class="ic ic-plus" data-toggle="modal"  id="<?php echo @$val->product_id; ?>" value="<?php echo @$val->product_id; ?>" name="prod_size" data-sizes="<?php echo $size_ids; ?>" data-label="<?php echo $size_texts; ?>" data-productname="<?php echo $val->product_name; ?>" data-productprice="<?php echo $val->discount_price; ?>" data-brandname="<?php echo $val->brand_name; ?>" data-category="<?php echo $val->product_category; ?>" onclick="GetProductSize(this);">
					  <i class="icon-plus"></i>
					</span>
					<span class="ic ic-share" onclick="send_message('product','<?php echo @$val->product_id; ?>','<?php echo @$val->product_name; ?>','<?php echo @$val->product_img; ?>','<?php echo base_url().PRODUCT_URL.$val->product_slug.'-'.$val->product_id; ?>',<?php if($user_id > 0){ echo @$user_id; }else { echo '0'; } ?>);">
					  <i class="icon-send"></i>
					</span>
				  </div>
				<span class="ic-heart <?php if(@$val->is_wishlisted == '1') echo "active"; ?>" id="wishlist_product<?php echo @$val->product_id; ?>" onclick="add_to_fav('product',<?php echo @$val->product_id; ?>,<?php if($user_id > 0){ echo @$user_id; }else { echo '0'; } ?>,'<?php echo @$val->product_name; ?>');">
					  <i class="icon-heart"></i>
				</span>
				</div>
			  </div>
			</div>

		<?php }//foreach ?>

      </div>
    </div>

  </div>
</section>
<?php } //if ?>
<section class="section section-shadow">
  <div class="container">
	
	 <div class="box-reviews-list <?php if(count($review)<1){ echo "hide"; } ?>" id="review_list">
    <div class="title-section">
      <h1 class="subtitle">Review</h1>
    </div>
    <div class="product-review">
	
          <ul id="reviews">
      <?php if(!empty($review)){
					foreach($review as $value){ ?>
				<li>
				  <div class="ratings">
					<span class="shape-diamond"><?php echo $value['ratings'];?></span>
				  </div>
				  <div class="desc">
					<?php echo $value['review_text'];?>
				  </div>
				  <div class="meta"><?php echo $value['user_name']; ?> | <?php echo date("Y-m-d",strtotime($value['created_datetime'])); ?></div>
				</li>
		  <?php }//foreach
			}//if ?>
        </ul>
	</div>
    </div>
	
      <div class="btns-wrp text-center">

	  <?php if($user_id > 0){ $target_html = ' data-target="#section-write-review" '; }else { $target_html = ' onclick="show_alert(\''.REVIEW_MESSAGE.'\');" '; } ?>
      <a class="scbtn scbtn-primary" data-toggle="collapse" <?php echo $target_html; ?> ><span>Rate & Review</span></a>

       <?php if(count($review) >= 2){?> <a onclick="get_all_review(<?php echo @$product_data['product_id']; ?>);" class="scbtn scbtn-secondary"><span>Show More</span></a>
	   <?php
		  }//if	?>
    </div>
  </div>
</section>

<section id="section-write-review" class="section section-shadow section-write-review collapse">

  <div class="container">
    <div class="title-section">
      <h1 class="subtitle">Rate & Review the product</h1>
    </div>

    <h3 class="title-2">Rate this product</h3>
    <ul class="btn-diamond-group rate-product">
      <li class="btn-diamond btn-rating" id="1" value="1" onclick="change_rating(1);">1</li>
      <li class="btn-diamond btn-rating" id="2" value="2" onclick="change_rating(2);">2</li>
      <li class="btn-diamond btn-rating" id="3" value="3" onclick="change_rating(3);">3</li>
      <li class="btn-diamond btn-rating" id="4" value="4" onclick="change_rating(4);">4</li>
      <li class="btn-diamond btn-rating" id="5" value="5" onclick="change_rating(5);">5</li>
    </ul>

    <h3 class="title-2">Review this product</h3>
    <div class="product-write-review">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Name" name="name" id="name" />
		<input type="hidden" class="form-control" name="product_id" id="product_id" value="<?php echo @$product_data['product_id']; ?>"/>
      </div>
      <div class="form-group">
        <textarea class="form-control" placeholder="Write a comment..." name="comment" id="comment" ></textarea>

			<div id="review_error" class="message error"></div>
			<div id="review_success" class="message success"></div>
		</div>
      <div class="btns-wrp text-center">
        <button class="scbtn scbtn-primary" onclick="add_product_review();"><span>Submit</span></button>
      </div>
    </div>

  </div>
</section>
<script type="text/Javascript">
var ratings = 0;
function change_rating(new_rating){
	if(ratings>0){ $('#'+ratings).removeClass('active'); }
	$('#'+new_rating).addClass('active');
	ratings = new_rating;
}
function add_product_review(){
	var name = $("#name").val();
	var comment = $("#comment").val();
	var product_id = $("#product_id").val();
	if(name.trim() == ''){
		$('#review_error').html('Please enter the name');
		$('#review_error').addClass("error");
		$('#review_error').removeClass("success");
	}else if(comment.trim() == ''){
		$('#review_error').html('Please enter the Review');
		$('#review_error').addClass("error");
		$('#review_error').removeClass("success");
	}else if(ratings <= 0 || !ratings){
		$('#review_error').html('Please give the ratings');
		$('#review_error').addClass("error");
		$('#review_error').removeClass("success");
	}else{
		$.ajax({
			  url: sc_baseurl+'Product_web/post_review',
			  type: 'POST',
			  data: {'product_id':product_id,'ratings':ratings,'name':name,'comment':comment},
			  cache :true,
			  success: function(response) {
				$('#review_success').html('Thanks for your review');
				$('#review_error').addClass("hide");
				$('#review_list').removeClass('hide');
				get_all_review(product_id);
				$('#comment').val('');
				// location.reload();

			  }
		});
	}
}

function get_reviews(product_id){
	 $.ajax({
			url: '<?php echo base_url(); ?>Product_web/get_reviews',
			type: 'POST',
			data: {'product_id':product_id},
			cache :true,
			success: function(response) {

			  $('#reviews').html(response);

			}
	  });
}

function get_all_review(product_id){
	$.ajax({
			url: '<?php echo base_url(); ?>Product_web/get_all_reviews',
			type: 'POST',
			data: {'product_id':product_id},
			cache :true,
			success: function(response) {

			  $('#reviews').html(response);

			}
	  });
}
</script>
<script>
fbq('track', 'ViewContent', {
	content_type: 'product',
    content_ids: '<?php echo @$product_data['product_id']; ?>',
    content_name: '<?php echo $product_data['name']; ?>',
    content_category: '<?php echo $product_data['product_category']; ?>',
	value: <?php echo $product_price; ?>,
	currency: 'INR'
});
</script>