<section class="section section-shadow page-bookmystylist">
  <div class="container">
    <div class="title-section">
      <h1 class="title">Book My Stylist</h1>
    </div>
    <div class="row">
      <div class="col-md-6 col-sm-12 col-xs-12">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="First Name" />
      </div>
      </div>
      <div class="col-md-6 col-sm-12 col-xs-12">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Last Name" />
      </div>
      </div>
      <div class="col-md-6 col-sm-12 col-xs-12">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Email" />
      </div>
      </div>
      <div class="col-md-6 col-sm-12 col-xs-12">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Mobile" />
      </div>
      </div>
      <div class="col-md-6 col-sm-12 col-xs-12">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Birthday" />
      </div>
      </div>
      <div class="col-md-6 col-sm-12 col-xs-12">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="City" />
      </div>
      </div>
    </div>

    <div class="text-center">
      <a class="scbtn scbtn-primary" href="#"><span>Confirm Booking</span></a>
    </div>
  </div>
</section>
