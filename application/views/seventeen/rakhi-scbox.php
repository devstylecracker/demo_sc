<?php
  $scbox_pack = unserialize(SCBOX_PACKAGE);
?>
<div class="page-scbox"> 

  <input type='hidden' id="scbox-page" name="scbox-page" value="rakhi" />

  <input type='hidden' id="scbox-package" name="scbox-package" value="" />
  <input type='hidden' id="scbox-price" name="scbox-price" value="" />
  <input type='hidden' id="scbox-objectid" name="scbox-objectid" value="" />
  <input type='hidden' id="scbox-productid" name="scbox-productid" value="" />  

  <section id="section-pricing" class="section section-shadow section-pricing">
    <div class="container">
        <div class="title-section text-center">
        <h1 class="title"><span>WHAT’S IN MY STYLECRACKER RAKHI BOX?</span></h1>
      </div>

       <div class="row">
      
        <div class="col-md-6 col-sm-6 col-xs-6">
          <div class="box-package" data-pack="package5" data-scbox-price="999" data-scbox-objectid="5" data-scbox-productid="192378" onclick="SetPackage(this);">
            
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/scbox/rakhi-box-1000.jpg" alt="">
            </div>
            <h3 class="subtitle" style="text-align:center;">999 Box</h3>
            <div class="desc" style="text-align:center;">
            
              Sling Bag<br>

              Scarf<br>

              Tassel Earrings<br>

              Snackible Snack<br><br>

              <strong><i class="fa fa-inr"></i> 999</strong>
            </div>

          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6">
          <div class="box-package" data-pack="package6" data-scbox-price="1999" data-scbox-objectid="6" data-scbox-productid="192379" onclick="SetPackage(this);">
        
            <div class="img-wrp">
              <img src="<?php echo base_url(); ?>assets/images/scbox/rakhi-box-2000.jpg" alt="">
            </div>
            <h3 class="subtitle" style="text-align:center;">1999 Box</h3>
            <div class="desc" style="text-align:center;">
             Necklace<br>

              Pouch<br>

              Tote Bag<br>

              Dupatta<br>

              Beauty Product<br>

              Snackible Snack<br><br>

              <strong><i class="fa fa-inr"></i> 1999</strong>
            </div>
          </div>
        </div>

        <div class="text-center">
          <a class="scbtn scbtn-primary link-scrolling" href="javascript:void(0);" onclick="scboxregister();" ><span>Order Now</span></a>
          <div class="form-group" ><label class="hide" id="scboxregistererror1">Select your scbox package</label></div>
        </div> 
        
      </div>


      <div class="title-section text-center">
        <h1 class="title"><span>BUY GIFT CARD</span></h1>
      </div>
      <div class="row">
    <?php
        $pricehtml = '';
        //<input type="tel" class="form-control" name="customboxprice" id="customboxprice" onblur="getcustomprice();" required placeholder="Give us your budget" />
      foreach($scbox_pack as $val){
        if($val->show==1 && $val->packname!='package4')
        {
              if($val->packname=='package4')
              {
                
               }else
               {
                   $pricehtml = '<div class="price"><i class="fa fa-inr"  ></i>'.$val->price.'</div>';
               }
        ?>
      
      <div class="col-md-4 col-sm-6 col-xs-6">
        <div class="box-package " data-pack="<?php echo $val->packname; ?>" data-scbox-price="<?php echo $val->price; ?>" data-scbox-objectid="<?php echo $val->id; ?>" data-scbox-productid="<?php echo $val->productid; ?>" onclick="SetPackage(this);">
          <div class="img-wrp">
          <img src="<?php echo base_url(); ?>assets/images/scbox/<?php echo $val->packname.'.jpg'; ?>" alt="">
          </div>
          <div class="title"><?php echo $val->name; ?></div>
          <div class="desc">
           <?php echo $val->desc; ?>
          </div>
            <?php echo $pricehtml; ?>
        </div>
      </div>

    <?php }}?>
    </div>      
   

     

    <div class="text-center">
      <a class="scbtn scbtn-primary link-scrolling" href="javascript:void(0);" onclick="scboxregister();" ><span>Order Now</span></a>
      <div class="form-group" ><label class="hide" id="scboxregistererror">Select your scbox package</label></div>
    </div> 

      <input type="hidden"  name="utm_source" id="utm_source" value="<?php echo @$_GET['utm_source']; ?>" />
      <input type="hidden"  name="utm_medium" id="utm_medium" value="<?php echo @$_GET['utm_medium']; ?>" />
      <input type="hidden"  name="utm_campaign" id="utm_campaign" value="<?php echo @$_GET['utm_campaign']; ?>" />
      
    </div>
  </section>
  </div>
