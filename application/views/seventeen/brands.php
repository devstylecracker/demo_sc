<div class="page-brands">
  <section class="section section-shadow">
    <div class="container">
      <div class="title-section">
        <h1 class="title">Brands</h1>
      </div>
		<ul class="brands-listing ">
			<?php 
					if(!empty($brands_dictionary))
					{
					  foreach($brands_dictionary as $key=>$value)
					  {
					  		echo '<li class="title">'.$key.'</li>';
					  		if(!empty($value))
					  		{
					  			foreach($value as $k=>$v)
					  			{
					  				echo '<li><a href="'.BRAND_URL.$v['user_name'].'-'.$v['user_id'].'">'.strtoupper($k).'</a></li>';
					  			}
					  		}				  		
					  }
					}
			?>
		</ul>
    </div>
  </section>
</div>
