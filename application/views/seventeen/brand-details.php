<div class="page-brand-details">
  <section class="section-shadow">
    <div class="banner-wrp img-wrp">
      <img src="https://www.stylecracker.com/assets/images/brands/25032.jpg" />
    </div>
  </section>
  <section class="section section-shadow">
    <div class="container">
      <div class="title-section">
        <h1 class="title">Zaamor Diamonds</h1>
      </div>
      <div class="title-section">
        <h2 class="subtitle"><span>About</span></h2>
      </div>
      <div>
        <p>
          Zaamor Diamonds is your perfect online portal for buying exquisite Solitaire Diamonds and diamond jewellery. Zaamor Diamonds is your perfect online portal for buying exquisite Solitaire Diamonds and diamond jewellery. Zaamor Diamonds is your perfect online
          portal for buying exquisite Solitaire Diamonds and diamond jewellery. Zaamor Diamonds is your perfect online portal for buying exquisite Solitaire Diamonds and diamond jewellery. Zaamor Diamonds is your perfect online portal for buying exquisite
          Solitaire Diamonds and diamond jewellery.
        </p>
      </div>
    </div>
  </section>
  <section class="section section-shadow">
    <div class="container">
      <div class="title-section">
        <h1 class="subtitle">Products Designed</h1>
      </div>
      <div class="grid grid-items products">
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-6 grid-col">
            <div class="item-wrp">
              <div class="item">
                <div class="item-img-wrp">
                  <a href="http://www.scnest.com/Product_web/product_details/98871">
                    <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_310/14683300781519.png" alt=""></a>
                </div>
                <div class="item-desc-wrp">
                  <div class="category"><a href="#">Casual Belts</a></div>
                  <div class="title"><a href="#">The Bro Code Orange Braided Belt</a></div>
                  <div class="brand">By <a href="http://www.scnest.com/brands/brand-details/thebrocode">The Brocode</a></div>
                  <div class="item-price-wrp">
                    <div class="price"><i class="fa fa-inr"></i>1599</div>
                  </div>
                </div>
                <div class="item-media-wrp">
                  <span class="ic ic-plus">
                  <i class="icon-plus"></i>
                </span>
                  <span class="ic ic-share">
                  <i class="icon-send"></i>
                </span>
                </div>
                <span class="ic-heart">
                  <i class="icon-heart"></i>
              </span>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-6 grid-col">
            <div class="item-wrp">
              <div class="item">
                <div class="item-img-wrp">
                  <a href="http://www.scnest.com/Product_web/product_details/98871">
                    <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_310/14683300781519.png" alt=""></a>
                </div>
                <div class="item-desc-wrp">
                  <div class="category"><a href="#">Casual Belts</a></div>
                  <div class="title"><a href="#">The Bro Code Orange Braided Belt</a></div>
                  <div class="brand">By <a href="http://www.scnest.com/brands/brand-details/thebrocode">The Brocode</a></div>
                  <div class="item-price-wrp">
                    <div class="price"><i class="fa fa-inr"></i>1599</div>
                  </div>
                </div>
                <div class="item-media-wrp">
                  <span class="ic ic-plus">
                        <i class="icon-plus"></i>
                      </span>
                  <span class="ic ic-share">
                        <i class="icon-send"></i>
                      </span>
                </div>
                <span class="ic-heart">
                        <i class="icon-heart"></i>
                    </span>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-6 grid-col">
            <div class="item-wrp">
              <div class="item">
                <div class="item-img-wrp">
                  <a href="http://www.scnest.com/Product_web/product_details/98871">
                    <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_310/14683300781519.png" alt=""></a>
                </div>
                <div class="item-desc-wrp">
                  <div class="category"><a href="#">Casual Belts</a></div>
                  <div class="title"><a href="#">The Bro Code Orange Braided Belt</a></div>
                  <div class="brand">By <a href="http://www.scnest.com/brands/brand-details/thebrocode">The Brocode</a></div>
                  <div class="item-price-wrp">
                    <div class="price"><i class="fa fa-inr"></i>1599</div>
                  </div>
                </div>
                <div class="item-media-wrp">
                  <span class="ic ic-plus">
                              <i class="icon-plus"></i>
                            </span>
                  <span class="ic ic-share">
                              <i class="icon-send"></i>
                            </span>
                </div>
                <span class="ic-heart">
                              <i class="icon-heart"></i>
                          </span>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-6 grid-col">
            <div class="item-wrp">
              <div class="item">
                <div class="item-img-wrp">
                  <a href="http://www.scnest.com/Product_web/product_details/98871">
                    <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_310/14683300781519.png" alt=""></a>
                </div>
                <div class="item-desc-wrp">
                  <div class="category"><a href="#">Casual Belts</a></div>
                  <div class="title"><a href="#">The Bro Code Orange Braided Belt</a></div>
                  <div class="brand">By <a href="http://www.scnest.com/brands/brand-details/thebrocode">The Brocode</a></div>
                  <div class="item-price-wrp">
                    <div class="price"><i class="fa fa-inr"></i>1599</div>
                  </div>
                </div>
                <div class="item-media-wrp">
                  <span class="ic ic-plus">
                                <i class="icon-plus"></i>
                              </span>
                  <span class="ic ic-share">
                                <i class="icon-send"></i>
                              </span>
                </div>
                <span class="ic-heart">
                                <i class="icon-heart"></i>
                            </span>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </section>
</div>
