<?php
/*echo @$inputData;*/
@$inputData_post = json_decode($inputData);
//echo '<pre>';print_r(@$inputData_post);
//echo @$inputData_post['sc_name'][0];
$maleActive= '';
$femaleActive= '';$scname = '';
if(@$inputData_post->sc_gender==1)
{
  $femaleActive= 'active';
}else if(@$inputData_post->sc_gender==2)
{
  $maleActive= 'active';
}
$scname = (@$inputData_post->sc_name[0]!='' && @$inputData_post->sc_name[1]!='' ) ? @$inputData_post->sc_name[0].' '.@$inputData_post->sc_name[1] : '';
if(!empty($error))
{
  //echo '<pre>';print_r(@$error);
 
}

?>
<style type="text/css">
  fieldset.date { 
  margin: 0; 
  padding-top: 10px;  
  display: block; 
  border: none; 
} 

fieldset.date legend { 
  margin: 0; 
  padding: 0; 
  margin-top: .25em; 
  font-size: 100%; 
} 


fieldset.date label { 
  position: absolute; 
  top: -20em; 
  left: -200em; 
} 

fieldset.date select { 
margin: 0; 
padding: 0; 
font-size: 100%; 
display: inline; 
width: 30%;
} 

.hwidth{
  width: 20%;

}
span.inst { 
  font-size: 75%; 
  color: #13D792; 
  padding-left: .25em; 
  text-transform: uppercase;
} 

.form-group-md{
  padding: 0 0 !important;
}
.form-group-md select{
  border-color: #13D792 !important;
}


.form-group-md input + label:before, .form-group-md input + label:after, .form-group-md textarea + label:before, .form-group-md textarea + label:after, .form-group-md select + label:before, .form-group-md select + label:after

{
  background: transparent;
}
fieldset.date select:active, 
fieldset.date select:focus, 
fieldset.date select:hover 
{ 
  border-color: #13D792; 
  
} 

label#day_start-error{

   top: 46px !important;
       font-size: 13px;
    letter-spacing: 0px;
   }

  label#month_start-error{

   top: 46px !important;
    left: 173px !important;
        font-size: 13px;
    letter-spacing: 0px;
}

label#year_start-error  { 

    top: 46px !important;
    left: 320px !important;
        font-size: 13px;
    letter-spacing: 0px;
}

</style>
<div class="page-signup-wrp">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
        <div class="signup-box-wrp page-login111 page-signup">

          <div class="box-signup">
            <div class="title-section">
              <h1 class="title">Sign Up</h1>
            </div>

          <div class="box-signup-inner signup-options">
            <div class="box-title">
            SIGN UP USING ANY OF THE FOLLOWING
            </div>
            <div>
                <a href="#" title="Sign in using Facebook" class="btn-facebook-wrp" id="facebook_login_in" ><i class="icon-facebook"></i></a>
                <a href="<?php echo $this->data['google_login_url']; ?>" title="Sign in using Google" class="btn-google-wrp" onclick="ga('send', 'event', 'Sign Up', 'source', 'gplus');"><i class="icon-google">
                  <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i>
              </a>
              <span class="btn-email-wrp">
                  <img src="<?php  echo base_url(); ?>assets/seventeen/images/icons/ic-email.png" />
              </span>


            </div>
          </div>
          <!-- action="<?php// echo base_url().'Schome_new/register_user'; ?>"-->
          <form name="scSignup" id="scSignup" method="post" action="">
          <div class="signup-by-email">
          <div class="box-signup-inner">
            <div class="box-title">
              Select your Gender*
            </div>
            <ul class="gender-wrp">
              <li id="gender-male" class="<?php echo $maleActive; ?>">
                <span class="ic-gender ic-male "><i class="icon-male"></i>  </span>
                <span class="text-label">M</span>
              </li>
              <li id="gender-female"  class="<?php echo $femaleActive; ?>" >
                <span class="ic-gender ic-female "><i class="icon-female"></i></span>
                <span class="text-label">F</span>
              </li>
            </ul>
            <div class="form-group">
              <input type="hidden" id="sc_gender" name="sc_gender" class="form-control" placeholder="gender"  value="<?php echo @$inputData_post->sc_gender; ?>" >
             <!--<label id="sc_gender-error" class="error message hide" for="sc_gender">Please select gender</label>-->
            </div>
          </div>
          <div class="section section-shadow">
            <div class="box-signup-inner">
              <div class="box-title">
                Fill in your Details
              </div>
              <div class="form-group">
                <input type="text" name="sc_name" id="sc_name" class="form-control"  maxlength="100" placeholder="NAME*"  value="<?php echo $scname; ?>" onblur="validateSignup(this);" >
                <div class="message text-error hide" id="sc_lastname-error">Please enter your full name</div>
				
              
              </div>
           
            <div class="form-group-md">
                <input type="hidden" name="scbox_age" id="scbox_age" value="" >
                <fieldset class="date">				
                  <label for="day_start"></label> 
                  <select id="day_start" name="day_start" onchange="validateform(this);" >
                    <option value="">DAY</option>  
                  <?php for($i=1;$i<32;$i++){ ?>
                    <option value="<?php echo $i; ?>" <?php echo  set_select('day_start', $i); ?>><?php echo $i; ?></option> 
                  <?php } ?>                     
                  </select> - <label for="month_start"></label> 
                  <select id="month_start" name="month_start" onchange="validateform(this);" >
                    <option value="">MONTH</option>    
                    <option value="1" <?php echo  set_select('month_start', 1); ?>>January</option>       
                    <option value="2" <?php echo  set_select('month_start', 2); ?>>February</option>       
                    <option value="3" <?php echo  set_select('month_start', 3); ?>>March</option>       
                    <option value="4" <?php echo  set_select('month_start', 4); ?>>April</option>       
                    <option value="5" <?php echo  set_select('month_start', 5); ?>>May</option>       
                    <option value="6" <?php echo  set_select('month_start', 6); ?>>June</option>       
                    <option value="7" <?php echo  set_select('month_start', 7); ?>>July</option>       
                    <option value="8" <?php echo  set_select('month_start', 8); ?>>August</option>       
                    <option value="9" <?php echo  set_select('month_start', 9); ?>>September</option>       
                    <option value="10" <?php echo  set_select('month_start', 10); ?>>October</option>       
                    <option value="11" <?php echo  set_select('month_start', 11); ?>>November</option>       
                    <option value="12" <?php echo  set_select('month_start', 12); ?>>December</option>      
                  </select> -
                  <label for="year_start"></label> 
                  <select id="year_start" name="year_start" onchange="validateform(this);" >
                    <option value="" >YEAR</option>  
                  <?php for($i=date("Y");$i>=1947;$i--){ ?>
                    <option value="<?php echo $i; ?>" <?php echo  set_select('year_start', $i); ?>><?php echo $i; ?></option> 
                  <?php } ?>
                  </select> 
                  <span class="inst">Date of Birth*</span>  
                 
                </fieldset>
                 <!--<label id="scbox_age-error" class="error" for="scbox_age">Please enter your date of birth</label>
                   <label id="month_start-error" class="error" for="month_start">Please enter your month of birth</label>
                  <label id="year_start-error" class="error" for="year_start">Please enter your year of birth</label>-->               
            </div>
        
              <div class="form-group">
                <input type="email" name="sc_emailid" id="sc_emailid" class="form-control" placeholder="EMAIL ID*" maxlength="100" onblur="validateform();" value="<?php echo @$inputData_post->scemail_id; ?>">
                <div class="message text-error hide"></div>
              </div>

              <div class="form-group">
                <input type="password" name="sc_password" id="sc_password" class="form-control" placeholder="PASSWORD*" maxlength="100">
                <div class="message text-error hide"></div>
              </div>
              <div class="form-group">
                <input type="tel" name="sc_mobile" id="sc_mobile" class="form-control" placeholder="MOBILE NUMBER*" maxlength="10"  value="<?php echo @$inputData_post->scmobile; ?>">
                <div class="message text-error hide"></div>
              </div>
               <div id="signup_error" class="message text-danger" signup-error="<?php echo @$error; ?>" ><?php echo @$error; ?></div>
              <div class="scbtns-wrp text-center">
                <!--<button type="submit" id="sc_signup" name="sc_signup" class="scbtn scbtn-primary"><span>Sign Up</span></button>-->
                <button  id="sc_signup" name="sc_signup" class="scbtn scbtn-primary"><span>Sign Up</span></button>
              </div>
            </div>
          </div>
        </div>
        </form>

          </div>
          <section class="section section-shadow page-signup">
            <div class="container">
              <div class="box-singn-signup-link">
                <div class="box-title">
                  Already have an account?
                </div>
                <div class="scbtns-wrp text-center" onclick="ga('send', 'event', 'Login', 'clicked','header');">
                  <a class="scbtn scbtn-tertiary" href="login"><span>Login</span></a>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
</div>

