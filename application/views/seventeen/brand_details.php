<div class="page-brand-details">
  <section class="section-shadow">
    <div class="banner-wrp img-wrp">
      <img src="<?php echo $brand_info['cover_image']; ?>" />
    </div>
  </section>
  <section class="section section-shadow">
    <div class="container">
      <div class="title-section">
        <h1 class="title"><?php echo $brand_info['company_name']; ?></h1>
      </div>
      <div class="title-section">
        <h2 class="subtitle"><span>About</span></h2>
      </div>
      <div>
        <p>
          <?php echo $brand_info['long_description']; ?>
        </p>
      </div>
    </div>
  </section>
  <section class="section section-shadow">
    <div class="container">
      <div class="title-section">
        <h1 class="subtitle">Products</h1>
      </div>
    <div class="grid grid-items products">
	<div class="row" id="get_all_brand_products">
    
	<?php if(!empty($products)){ 
			foreach($products as $val){ 
				$product_name = $val->product_name; 
				$val->product_name = preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_name);
				$val->product_slug = preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_slug);
	?>
			<div class="col-md-3 col-sm-6 col-xs-6 grid-col">
			  <div class="item-wrp">
				<div class="item">
				  <div class="item-img-wrp" onclick="_targetProductClick(<?php echo $val->product_id; ?>,'6','<?php echo @$val->product_name; ?>');">
					<a href="<?php echo base_url().PRODUCT_URL.$val->product_slug.'-'.$val->product_id; ?>"><img src="<?php echo $val->product_img; ?>" alt=""></a>
				  </div>
				  <div class="item-desc-wrp">
					<a href="<?php echo CAT_URL.$val->product_category_slug.'-'.$val->product_category_id; ?>"><div class="category"><?php echo $val->product_category; ?></div></a>
					<a href="<?php echo base_url().PRODUCT_URL.$val->product_slug.'-'.$val->product_id; ?>"><div class="title" onclick="_targetProductClick(<?php echo $val->product_id; ?>,'6','<?php echo @$val->product_name; ?>');" ><?php echo $product_name; ?></div></a>
					<div class="brand">By <a href="<?php echo BRAND_URL.$val->brand_slug.'-'.$val->brand_id; ?>"><?php echo $val->brand_name; ?></a></div>
					<div class="item-price-wrp">
					  <?php if($val->price > $val->discount_price){ ?>
								<div class="mrp"><i class="fa fa-inr"></i><?php echo $val->price; ?></div>
								<div class="price"><i class="fa fa-inr"></i><?php echo $val->discount_price; ?></div>
								<div class="discount"><?php echo $val->discount_percent; ?>% OFF</div>
					  <?php }else { ?>
								<div class="price"><i class="fa fa-inr"></i><?php echo $val->price; ?></div>
					  <?php }?>
					</div>
				  </div>
				  <div class="item-media-wrp">
					<?php $size_ids='';$size_texts='';$size_id= array();$size_text= array();if(!empty($val->product_size)){foreach($val->product_size as $size){
						$size_id[] = $size->size_id;
						$size_text[] = $size->size_text;
						}// foreach
						}//if
						if(!empty($size_id))$size_ids = implode(',',$size_id);
						if(!empty($size_text))$size_texts = implode(',',$size_text);
					?>
					<span class="ic ic-plus" data-toggle="modal"  id="<?php echo @$val->product_id; ?>" value="<?php echo @$val->product_id; ?>" name="prod_size" data-sizes="<?php echo $size_ids; ?>" data-label="<?php echo $size_texts; ?>"  data-productname="<?php echo $val->product_name; ?>" data-productprice="<?php echo $val->discount_price; ?>" data-brandname="<?php echo $val->brand_name; ?>" data-category="<?php echo $val->product_category; ?>" onclick="GetProductSize(this);">
					  <i class="icon-plus"></i>
					</span>
					<span class="ic ic-share" onclick="send_message('product','<?php echo @$val->product_id; ?>','<?php echo @$val->product_name; ?>','<?php echo @$val->product_img; ?>','<?php echo base_url().PRODUCT_URL.$val->product_slug.'-'.$val->product_id; ?>',<?php if($user_id > 0){ echo @$user_id; }else { echo '0'; } ?>);">
					  <i class="icon-send"></i>
					</span>
				  </div>
				<span class="ic-heart <?php if(@$val->is_wishlisted == '1') echo "active"; ?>" id="wishlist_product<?php echo @$val->product_id; ?>" onclick="add_to_fav('product',<?php echo @$val->product_id; ?>,<?php if($user_id > 0){ echo @$user_id; }else { echo '0'; } ?>,'<?php echo @$val->product_name; ?>');">
					  <i class="icon-heart"></i>
				</span>
				</div>
			  </div>
			</div>

		<?php }//foreach
		}else{  ?>
		 <div class="grid-no-products"> No Products Found </div>
		<?php }  ?>
	</div>
	</div>
	
	<input type="hidden" name="offset" id="offset" value="1">
	<input type="hidden" name="brand_id" id="brand_id" value="<?php echo $brand_id; ?>">
	<input type="hidden" name="total_products" id="total_products" value="<?php echo $total_products; ?>">
	<!-- load-more" -->
	<div id="load-more" class="fa-loader-item-wrp">
		<div class="fa-loader-item">
			<i class="fa fa-circle-o-notch fa-spin-custom"></i> Loading...
		</div>
	</div>
	<!-- load-more" -->
    </div>
	
  </section>
</div>
<script type="text/Javascript">
	ga('send', 'event', 'Brand', 'clicked', '<?php echo @$brand_info['company_name']; ?>');
</script>