<!--<link href="<?php echo base_url(); ?>assets/seventeen/css/common-temp.css" rel="stylesheet" type="text/css">-->

  <section class="section section-shadow">
    <div class="container">
      <div class="title-section">
        <h1 class="title">Categories</h1>

        <div class="dropdown category-filter-wrp ic-right filterby-gender" id="dropdown">
          <span class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon icon-filter"></i>
          </span>
        <div class="dropdown-menu">
          <div class="box-filter box-filter-gender">
            <div class="title">
              Gender
            </div>
              <div class="box-filter-content box-filter-content-gender" id="all_category_type">
            <ul>
              <li><label class="sc-checkbox <?php if(@$gender == 1){ echo "active"; }?>" onclick="add_active_category(1,'1-Women');" id="checkbox1"><i class="icon icon-diamond"></i><span>Women</span></label></li>
              <li><label class="sc-checkbox <?php if(@$gender == 2){ echo "active"; }?>" onclick="add_active_category(3,'3-Men');" id="checkbox3"><i class="icon icon-diamond"></i> <span>Men</span></label></li>
              <li><label class="sc-checkbox <?php if(@$gender == 3){ echo "active"; }?>" onclick="add_active_category(2,'2-Unisex');" id="checkbox2"><i class="icon icon-diamond"></i> <span>Unisex</span></label></li>
            </ul>
              </div>
          </div>
          </div>
        </div>
      </div>

      <div id="category_list">
      <ol class="breadcrumb">
			<?php echo $category_breadcrumps; ?>
			</ol>

      <div class="grid grid-items categories">
      <?php if(!empty($category_list)) { ?>
          <div class="row">
			<?php foreach ($category_list as $value) {
				if(@$value['is_last'] == 0){
					$cat_name = $value['category_id'].'-'.$value['category_name'];
					$href = ' href="javascript:void(0)" onclick="get_child_cat('.$value['category_id'].',\''.$cat_name.'\');"';
				}else{
					$cat_slug = $value['category_slug'].'-'.$value['category_id'];
					$href = ' href="'.CAT_URL.''.$cat_slug.'" ';
				}
			?>
            <div class="col-md-3 col-sm-6 col-xs-6 grid-col">
              <div class="item-wrp">
                <div class="item">
                  <div class="item-img-wrp">
                    <a id="category_image" value="<?php echo $value['category_name']; ?>" <?php echo $href; ?> >
						<img title="<?php echo $value['category_name']; ?> - StyleCracker" alt="<?php echo $value['category_name']; ?>- StyleCracker" src="<?php echo $value['category_image']; ?>" />
                    </a>
                  </div>
                  <div class="item-desc-wrp">
                    <div class="title">
                    <a id="category_name" value="<?php echo $value['category_name']; ?>" title="<?php echo $value['category_name']; ?>" <?php echo $href; ?> ><?php echo $value['category_name']; ?></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php }//foreach ?>
        </div>
      <?php }//if ?>
      </div>
		<input type="hidden" name="gender_cat" id="gender_cat" value="<?php echo @$gender; ?>">
		<input type="hidden" name="old_cat_id" id="old_cat_id" value="">
    </div>
      </div>
  </section>
