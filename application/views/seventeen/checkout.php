<section class="section section-shadow page-cart111 page-checkout">
  <div class="container">
    <div class="title-section">
      <h1 class="title">Checkout</h1>
    </div>
    <div class="page-cart111">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <!-- panel -->
          <div class="box-checkout box-checkout-login open111">

            <h2 class="box-title">1. LOGIN OR SIGN UP <span class="btn-edit"><i class="icon-edit"></i></span></h2>
            <div class="box-checkout-summary email">
              <span class="text-label">Login ID:</span> rajendra@stylecracker.com
            </div>
            <div class="box-checkout-body">
              <div class="box-checkout-content">

                <div class="box-enter-email">
                  <input type="email" name="" id="" class="form-control" placeholder="ENTER EMAIL ADDRESS">
                  <button type="submit" class="scbtn scbtn-primary"><span>Continue</span></button>
              <!--    <span class="or-wrp">OR</span>-->

                  <div class="hr-or"><i></i><span>OR</span><i></i></div>

                  <div class="signin-social-wrp">
                    <span class="sub-title">
                        Sign in using
                      </span>
                    <a href="#" title="Sign in using Facebook" class="btn-facebook-wrp" id="facebook_login"><i class="icon icon-facebook"></i></a>
                    <a href="#" title="Sign in using Google" class="btn-google-wrp"><i class="icon icon-google">
                      <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i>
                    </a>
                  </div>
                  <div class="message text-error hide"></div>
                </div>

                <!-- -->

                <div class="box-enter-password hide">
                  <input type="password" name="" id="" class="form-control" placeholder="PASSWORD">
                  <button type="submit" class="scbtn scbtn-primary"><span>Continue</span></button>
                  <span class="or-wrp">OR</span>
                  <button type="submit" class="scbtn scbtn-tertiary"><span>Checkout as Guest</span></button>
                  <div class="message text-error hide"></div>
                </div>
                <!-- -->
              </div>
            </div>
          </div>
          <!-- //.panel -->

          <!-- panel -->
          <div class="box-checkout box-checkout-address open11 hide111">
            <h2 class="box-title">2. SHIPPING & BILLING ADDRESSES    <span class="btn-edit"><i class="icon-edit"></i></span></h2>

            <div class="box-checkout-summary address hide111">
              <div class="row">
                <div class="col-md-6">
                    <div class="box-address">
                      <div class="text-label">
                      Shipping Address
                      </div>
                      <div>
                        899, Dedric Point Apt. 353, Aaram Society Road,Vakola, Santacruz (East), Mumbai
                      </div>
                      <span class="btn-edit hide"><i class="icon-edit"></i></span>
                    </div>
                </div>
                <div class="col-md-6">
                  <div class="box-address">
                    <div class="text-label">
                      Billing Address
                  </div>
                  <div>
                    899, Dedric Point Apt. 353, Aaram Society Road,Vakola, Santacruz (East), Mumbai
                  </div>
                  <span class="btn-edit hide"><i class="icon-edit"></i></span>
                </div>
              </div>
            </div>
            </div>

            <div class="box-checkout-body">
              <div class="box-checkout-content hide111" style="padding-top:20px;">

                <div class="box-checkout-summary111 address hide111">
                  <div class="address-selected-wrp">
                  <div class="row">
                    <div class="col-md-6">
                        <div class="box-address">
                          <div class="text-label">
                          Shipping Address
                          </div>
                          <div>
                            899, Dedric Point Apt. 353, Aaram Society Road,Vakola, Santacruz (East), Mumbai
                          </div>
                          <span class="btn-edit hide"><i class="icon-edit"></i></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                      <div class="box-address">
                        <div class="text-label">
                          Billing Address
                      </div>
                      <div>
                        899, Dedric Point Apt. 353, Aaram Society Road,Vakola, Santacruz (East), Mumbai
                      </div>
                      <span class="btn-edit hide"><i class="icon-edit"></i></span>
                    </div>
                  </div>
                </div>
              </div>

                <div class="box-address-list">
                  <ul>
                    <li class="active">
                      <div class="title-3">
                        Home
                      </div>
                      <div>
                        899, Dedric Point Apt. 353, Aaram Society Road,Vakola, Santacruz (East), Mumbai
                      </div>
                      <span class="btn-edit"><i class="icon-edit"></i></span>

                      <div class="btns-wrp">
                        <label class="sc-checkbox active" attr-value="85"><i class="icon icon-diamond"></i><span>Ship Here</span></label>
                        <label class="sc-checkbox" attr-value="85"><i class="icon icon-diamond"></i><span>Bill Here</span></label>
                      </div>
                    </li>
                    <li>
                      <div class="title-3">
                        Work
                      </div>
                      <div>
                        899, Dedric Point Apt. 353, Aaram Society Road,Vakola, Santacruz (East), Mumbai
                      </div>
                      <span class="btn-edit"><i class="icon-edit"></i></span>


                      <div class="btns-wrp">
                        <label class="sc-checkbox " attr-value="85"><i class="icon icon-diamond"></i><span>Ship Here</span></label>
                        <label class="sc-checkbox active" attr-value="85"><i class="icon icon-diamond"></i><span>Bill Here</span></label>
                      </div>
                    </li>
                    <li>
                      <div class="title-3">
                        Rajendra Gamare
                      </div>
                      <div>
                        899, Dedric Point Apt. 353, Aaram Society Road,Vakola, Santacruz (East), Mumbai
                      </div>
                      <a class="btn-edit" href="#modal-address" data-toggle="modal"><i class="icon-edit"></i></a>
                      <div class="btns-wrp">
                        <label class="sc-checkbox " attr-value="85"><i class="icon icon-diamond"></i><span>Ship Here</span></label>
                        <label class="sc-checkbox" attr-value="85"><i class="icon icon-diamond"></i><span>Bill Here</span></label>
                      </div>
                    </li>
                  </ul>
                </div>
                <a class="scbtn scbtn-tertiary btn-add-address111" href="#modal-address" data-toggle="modal"><span>Add New Address</span></a>
                  </div>

            </div>
            <div class=" box-checkout-footer">
              <div class="btns-wrp">
                <button type="submit" class="scbtn scbtn-primary"><span>Proceed</span></button>
              </div>
            </div>
          </div>
            </div>
          <!-- //.panel -->

          <!--  panel -->
          <div class="box-checkout box-checkout-overview open">
            <h2 class="box-title">3. Order Summary <span class="btn-edit"><i class="icon-edit"></i></span></h2>
            <div class="box-checkout-summary">
              <div >
                <span class="text-label">Payble Amount:</span> <i class="fa fa-inr"></i> 871,199.00
              </div>
            </div>
            <div class="box-checkout-body">

              <div class="box-checkout-content">
                <!-- cart-->
                <div class="cart-box-wrp">
                  <div class="panel-wrp">
                    <ul class="cart-items">
                      <li class="cart-items-title">
                        <div class="row">
                          <div class="col-md-2 col-sm-2 col-xs-2 ">
                          </div>
                          <div class="col-md-10 col-sm-10 col-xs-10">
                            <div class="row">
                              <div class="col-md-5 col-sm-6 col-xs-6 text-left">
                                Item
                              </div>
                              <div class="col-md-2 col-sm-2 col-xs-2 ">
                                Size
                              </div>
                              <div class="col-md-2 col-sm-2 col-xs-2 ">
                                Quantity
                              </div>
                              <div class="col-md-3 col-sm-2 col-xs-2">
                                Price
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="row">
                          <div class="col-md-2 col-sm-2 col-xs-3 ">
                            <div class="cart-item-img img-wrp">
                              <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_310/14629520317501.png">
                            </div>
                          </div>
                          <div class="col-md-10 col-sm-10 col-xs-9">
                            <div class="cart-row-wrp">
                              <div class="row">
                                <div class="col-md-5 col-sm-6 col-xs-12">
                                  <div class="cart-item-name">Rider Strike Ad-Black
                                    <span class="ic ic-close sm-only"><i class="icon-close"></i></span>
                                  </div>
                                  <div class="cart-item-seller">
                                    <span class="text-label">Seller:</span> Rider </a>
                                  </div>
                                  <div class="shipping-status md-only">
                                    Delivery in 5-7 working days.
                                  </div>

                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-5">
                                  <div class="cart-item-size">
                                    <span class="text-label">Size:</span> US 10
                                  </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-7">
                                    <div class="qty-wrp-outer">
                                  <span class="text-label sm-only">Qty:</span>
                                  <div class="qty-wrp">
                                    <span class="icon icon-minus"></span>
                                    <span class="qty-value">1</span>
                                    <span class="icon icon-add"></span>
                                  </div>
                                  </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                  <div class="item-price">
                                    <div class="price"><i class="fa fa-inr"></i> 871,199.00</div>
                                    <span class="ic ic-close md-only"><i class="icon-close"></i></span>
                                  </div>
                                </div>
                              </div>
                              <div class="shipping-status sm-only">
                                Delivery in 5-7 working days.
                              </div>
                            </div>

                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="row">
                          <div class="col-md-2 col-sm-2 col-xs-3 ">
                            <div class="cart-item-img img-wrp">
                              <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_310/14629520317501.png">
                            </div>
                          </div>
                          <div class="col-md-10 col-sm-10 col-xs-9">
                            <div class="cart-row-wrp">
                              <div class="row">
                                <div class="col-md-5 col-sm-6 col-xs-12">
                                  <div class="cart-item-name">Rider Strike Ad-Black
                                    <span class="ic ic-close sm-only"><i class="icon-close"></i></span>
                                  </div>
                                  <div class="cart-item-seller">
                                    <span class="text-label">Seller:</span> Rider </a>
                                  </div>
                                  <div class="shipping-status md-only">
                                    Delivery in 5-7 working days.
                                  </div>

                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-5">
                                  <div class="cart-item-size">
                                    <span class="text-label">Size:</span> US 10
                                  </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-7">
                                    <div class="qty-wrp-outer">
                                  <span class="text-label sm-only">Qty:</span>
                                  <div class="qty-wrp">
                                    <span class="icon icon-minus"></span>
                                    <span class="qty-value">1</span>
                                    <span class="icon icon-add"></span>
                                  </div>
                                  </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                  <div class="item-price">
                                    <div class="price"><i class="fa fa-inr"></i> 871,199.00</div>
                                    <span class="ic ic-close md-only"><i class="icon-close"></i></span>
                                  </div>
                                </div>
                              </div>
                              <div class="shipping-status sm-only">
                                Delivery in 5-7 working days.
                              </div>
                            </div>

                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <!--total-->
                  <div class="row">
                    <div class="col-md-6 col-md-offset-6">
                      <div class="box-cart-total">
                        <ul class="inner111">
                          <li>
                            <span class="text-label">Your Total</span>
                            <span class="value"> <span class="fa fa-inr"></span> 4,300.00</span>
                          </li>
                          <li>
                            <span class="text-label">Coupon Discount</span>
                            <span class="value"> <span class="fa fa-inr"></span> 0.00 </span>
                          </li>
                          <li>
                            <span class="text-label">VAT</span>
                            <span class="value"> <span class="fa fa-inr"></span> 4,300.00</span>
                          </li>
                          <li>
                            <span class="text-label"> <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                              Referral Amount</label></span>
                            <span class="value"> <span class="fa fa-inr"></span> 4,300.00</span>
                          </li>
                          <li>
                            <span class="text-label">Delivery Charges</span>
                            <span class="value"> <span class="fa fa-inr"></span> 4,300.00</span>
                          </li>
                          <li class="total">
                            <span class="text-label text-right">Total:</span>
                            <span class="value sc-color"> <span class="fa fa-inr"></span> 4,300.00</span>
                          </li>
                        </ul>
                      </div>
                    </div>

                  </div>
                  <!--/total-->
                </div>
                <!-- -->
              </div>

              <div class=" box-checkout-footer">
                <div class="btns-wrp">
                  <button type="submit" class="scbtn scbtn-primary"><span>Proceed</span></button>
                </div>
              </div>
            </div>
          </div>
          <!-- //.panel -->

          <!--  panel -->
          <div class="box-checkout box-checkout-overview">
            <h2 class="box-title">4. Payment Details <span class="btn-edit"><i class="icon-edit"></i></span></h2>
            <div class="box-checkout-body">
              <div class="box-checkout-content">
                <div class="box-payment-options">
                  <ul>
                    <li class="active">
                      <label>
                        <span class="ic"><i class="icon-diamond"></i></span> Online Payment
                      </label>
                    </li>
                    <li>
                      <label>
                        <span class="ic"><i class="icon-diamond"></i></span> Cash On Delivery
                      </label>
                    </li>
                  </ul>
                </div>
              </div>

              <div class="box-checkout-footer">
                <div class="btns-wrp">
                  <button type="submit" class="scbtn scbtn-primary"><span>Place Order</span></button>
                </div>
              </div>
            </div>

          </div>
        </div>

      </div>
    </div>

</section>


<!-- popup -->
<div id="modal-address" class="modal fade modal-address" role="dialog">
  <div class="modal-dialog modal-md modal-sm">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><i class="icon-close"></i></button>

        <div class="box-add-address">
          <div class="title-section">
            <h1 class="title">Add Address</h1>
          </div>
          <div class="box-inner">
            <div class="form-group">
              <input type="email" name="" id="" class="form-control" placeholder="NAME">
              <div class="message text-error hide"></div>
            </div>
            <div class="form-group">
              <textarea name="" id="" class="form-control" placeholder="ADDRESS"></textarea>
              <div class="message text-error hide"></div>
            </div>
            <div class="form-group">
              <input type="text" name="" id="" class="form-control" placeholder="PINCODE">
              <div class="message text-error hide"></div>
            </div>
            <div class="form-group">
              <input type="text" name="" id="" class="form-control" placeholder="CITY">
              <div class="message text-error hide"></div>
            </div>
            <div class="form-group">
              <input type="text" name="" id="" class="form-control form-control-address" placeholder="STATE">
              <div class="message text-error hide"></div>
            </div>
          </div>
          <div class="btns-wrp">
            <button type="submit" class="scbtn scbtn-primary"><span>Save</span></button>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<!-- //. popup -->
