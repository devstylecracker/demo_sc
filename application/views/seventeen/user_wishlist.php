<section class="section section-shadow">
  <div class="container">
    <div class="title-section">
      <h1 class="title">Products</h1>
	  <?php if(@$total_products > 4){ ?><a class="link-view-all" href="<?php echo base_url(); ?>bookmarks-products">View All Products</a> <?php } ?>
    </div>

	<div class="grid grid-items products">
	<div class="row" id="get_all_home_products">
    <!--test-->
	<?php if(!empty($products)){
			foreach($products as $val){ 
				$product_name = $val->product_name; 
				$val->product_name = preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_name);
				$val->product_slug = preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_slug);
	?>
			<div class="col-md-3 col-sm-6 col-xs-6 grid-col">
			  <div class="item-wrp">
				<div class="item">

				  <div class="item-img-wrp" onclick="_targetProductClick(<?php echo $val->product_id; ?>,'6','<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_name); ?>');">

					<a href="<?php echo base_url().PRODUCT_URL.@$val->product_slug.'-'.$val->product_id; ?>"><img src="<?php echo $val->product_img; ?>" alt=""></a>
				  </div>
				  <div class="item-desc-wrp">
					<div class="category"><?php echo $val->product_category; ?></div>

					<div class="title" onclick="_targetProductClick(<?php echo $val->product_id; ?>,'6','<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_name); ?>');"><a href="<?php echo base_url().PRODUCT_URL.@$val->product_slug.'-'.$val->product_id; ?>"><?php echo $product_name; ?></a></div>

					<div class="brand">By <a href="<?php echo base_url(); ?>brands/brand-details/<?php echo $val->brand_slug; ?>"><?php echo $val->brand_name; ?></a></div>
					<div class="item-price-wrp">
					  <?php if($val->price > $val->discount_price){ ?>
								<div class="mrp"><i class="fa fa-inr"></i><?php echo $val->price; ?></div>
								<div class="price"><i class="fa fa-inr"></i><?php echo $val->discount_price; ?></div>
								<div class="discount"><?php echo $val->discount_percent; ?>% OFF</div>
					  <?php }else { ?>
								<div class="price"><i class="fa fa-inr"></i><?php echo $val->price; ?></div>
					  <?php }?>
					</div>
				  </div>
				  <div class="item-media-wrp">
				 <?php $size_ids='';$size_texts='';$size_id= array();$size_text= array();if(!empty($val->product_size)){foreach($val->product_size as $size){
						$size_id[] = $size->size_id;
						$size_text[] = $size->size_text;
						}// foreach
						}//if
						if(!empty($size_id))$size_ids = implode(',',$size_id);
						if(!empty($size_text))$size_texts = implode(',',$size_text);
					?>
					<span class="ic ic-plus" data-toggle="modal"  id="<?php echo @$val->product_id; ?>" value="<?php echo @$val->product_id; ?>" name="prod_size" data-sizes="<?php echo $size_ids; ?>" data-label="<?php echo $size_texts; ?>" data-productname="<?php echo $val->product_name; ?>" data-productprice="<?php echo $val->discount_price; ?>" data-brandname="<?php echo $val->brand_name; ?>" onclick="GetProductSize(this);">
					  <i class="icon-plus"></i>
					</span>
					<span class="ic ic-share" onclick="send_message('product','<?php echo @$val->product_id; ?>','<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_name); ?>','<?php echo @$val->product_img; ?>','<?php echo base_url().PRODUCT_URL.@$val->product_slug.'-'.$val->product_id; ?>',<?php if($user_id > 0){ echo @$user_id; }else { echo '0'; } ?>);">
					  <i class="icon-send"></i>
					</span>
				  </div>
				<span class="ic-heart <?php if(@$val->is_wishlisted == '1') echo "active"; ?>" id="wishlist_product<?php echo @$val->product_id; ?>" onclick="add_to_fav('product',<?php echo @$val->product_id; ?>,<?php if(@$user_id > 0){ echo @$user_id; }else { echo '0'; } ?>,'<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val->product_name)); ?>');">
					  <i class="icon-heart"></i>
				</span>
				</div>
			  </div>
			</div>

		<?php }//foreach
			} else{	?>
				<div class="col-md-12 ">
					<span><?php echo $product_message1; ?> </span><span class="ic-heart"><i class="icon-heart"></i></span>
					<span><?php echo $product_message2; ?> </span>
				</div>
			<?php } ?>
	</div>
	</div>
  </div>
</section>
<section class="section section-shadow section-collections">
  <div class="container">
    <div class="title-section">
      <h1 class="title">Collections</h1>
	  <?php if(@$total_collections > 2){ ?><a class="link-view-all" href="<?php echo base_url(); ?>bookmarks-collections">View All Collections</a> <?php } ?>
    </div>

    <div class="grid grid-items collections thumb-collection">
      <div class="row grid-row" id="get_all_collections">
		  <?php
		  if(!empty($collection_data)){ foreach($collection_data as $val){?>
			<div class="col-md-6 col-sm-12 col-xs-12 grid-col">
			  <div class="item-wrp overlay">
				<div class="item">
				  <div class="item-img-wrp">
					<img src="<?php echo @$val['collection_image']; ?>" alt="">
				  </div>
				  <div class="item-desc-wrp">
					<div class="inner">
					<h2 class="title"><?php echo @$val['collection_name']; ?></h2>
					<div class="desc"><?php echo @$val['collection_short_desc']; ?></div>
					<div class="btns-wrp">
					  <a href="<?php echo base_url(); ?>collections/<?php echo @$val['object_slug'].'-'.@$val['collection_id']; ?>" class="scbtn scbtn-secondary white" onclick="_targetClickTrack('<?php echo @$val['collection_id']; ?>','8','<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['collection_name']); ?>','collection')" ><span>View Collection</span></a>
					</div>

				  </div>
					</div>
					<span class="ic-heart <?php if(@$val['is_wishlisted'] == '1') echo "active"; ?>" id="wishlist_c<?php echo @$val['collection_id']; ?>" onclick="add_to_fav('collection',<?php echo @$val['collection_id']; ?>,<?php if(@$user_id > 0){ echo @$this->session->userdata('user_id'); }else { echo '0'; } ?>,'<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['collection_name']); ?>');">
					  <i class="icon-heart"></i>
					</span>
				</div>
			  </div>
			</div>
		<?php }//foreach
			}else{	?>
      <div class="col-md-12 ">
			<span><?php echo $collection_message1; ?> </span><span class="ic-heart"><i class="icon-heart"></i></span>
			<span><?php echo $collection_message2; ?> </span>
    </div>
			<?php } ?>
      </div>
    </div>
  </div>
</section>

<section class="section section-shadow">
  <div class="container">
    <div class="title-section">
      <h1 class="title">Sc Live</h1>
	  <?php if(@$total_blogs > 3){ ?><a class="link-view-all" href="<?php echo base_url(); ?>bookmarks-blogs">View All Posts</a> <?php } ?>
    </div>

    <div class="grid grid-items blog">
      <div class="row grid-row" id="get_all_blogs">
	  <?php
		  if(!empty($blog_data)){ foreach($blog_data as $val){ ?>
			<div class="col-md-4 col-sm-6 col-xs-12 grid-col grid-col">
			  <div class="item-wrp">
				<div class="item">
				<a href="<?php echo $val['blog_url']; ?>" onclick="_targetClickTrack('<?php echo @$val['id']; ?>','9','<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['post_title']); ?>','blog');" >
				  <div class="item-img-wrp">
					<img src="<?php echo @$val['image']; ?>" alt=""/>
				  </div>
				  </a>
				  <div class="item-desc-wrp">
					<div class="title"><a href="<?php echo $val['blog_url']; ?>" onclick="_targetClickTrack('<?php echo @$val['id']; ?>','9','<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['post_title']); ?>','blog');" ><?php echo @$val['post_title']; ?></a></div>
					<!--<div class="desc"><?php echo @$val['post_desc']; ?></div>-->
					<div class="category"><?php echo @$val['post_type']; ?></div>
				  </div>
					<div class="item-btns-wrp">
					  <a href="<?php echo $val['blog_url']; ?>" class="scbtn scbtn-primary" onclick="_targetClickTrack('<?php echo @$val['id']; ?>','9','<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['post_title']); ?>','blog');" ><span>Read More</span></a>
					</div>
					<span class="ic-heart sc-heart <?php if(@$val['is_wishlisted'] == '1') echo "active"; ?>" id="wishlist_blog<?php echo @$val['id']; ?>" onclick="add_to_fav('blog',<?php echo @$val['id']; ?>,<?php echo @$this->session->userdata('user_id'); ?>,'<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['post_title']); ?>');">
					 <i class="icon-heart"></i>
					</span>

				</div>
			  </div>
			</div>
		<?php }//foreach
			}else{	?>
        <div class="col-md-12 ">
			<span><?php echo $blog_message1; ?> </span><span class="ic-heart"><i class="icon-heart"></i></span>
			<span><?php echo $blog_message2; ?> </span>
        </div>
			<?php } ?>
      </div>
    </div>
  </div>
</section>

<!--<section class="section section-shadow section-collections">
  <div class="container">
    <div class="title-section">
      <h1 class="title">EXPERIENCES</h1>
	  <?php if(@$total_events > 2){ ?><a class="link-view-all" href="<?php echo base_url(); ?>bookmarks-events">View All</a> <?php } ?>
    </div>

    <div class="grid grid-items thumb-collection">
      <div class="row grid-row" id="get_all_events">
		  <?php
		  if(!empty($event_data)){ foreach($event_data as $val){?>
			<div class="col-md-6 col-sm-12 col-xs-12 grid-col">
			  <div class="item-wrp overlay">
				<div class="item">
				  <div class="item-img-wrp">
					<img src="<?php echo @$val['event_image']; ?>" alt="">
				  </div>
				  <div class="item-desc-wrp">
					<div class="inner">
					<h2 class="title"><?php echo @$val['event_name']; ?></h2>
					<div class="desc"><?php echo @$val['event_short_desc']; ?></div>
					<div class="btns-wrp">
					  <a href="<?php echo base_url(); ?>events/<?php echo $val["object_slug"].'-'.@$val['event_id']; ?>" class="scbtn scbtn-secondary white" onclick="_targetClickTrack('<?php echo @$val['event_id']; ?>','10','<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['event_name']); ?>','event');" ><span>Read More</span></a>
					</div>

				  </div>
					</div>
					<span class="ic-heart <?php if(@$val['is_wishlisted'] == '1') echo "active"; ?>" id="wishlist_e<?php echo @$val['event_id']; ?>"onclick="add_to_fav('event',<?php echo @$val['event_id']; ?>,<?php if(@$user_id > 0){ echo @$this->session->userdata('user_id'); }else { echo '0'; } ?>, '<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['event_name']); ?>');">
					  <i class="icon-heart"></i>
					</span>
				</div>
			  </div>
			</div>
		<?php }//foreach
			}else{	?>
			<div class="col-md-12 ">
				<span><?php echo @$event_message1; ?> </span><span class="ic-heart"><i class="icon-heart"></i></span>
				<span><?php echo @$event_message2; ?> </span>
			</div>
			<?php } ?>
      </div>
    </div>
  </div>
</section>-->
