<?php
		  if(!empty($top_events)){ foreach($top_events as $val){?>
			<div class="col-md-6 col-sm-12 col-xs-12 grid-col">
			  <div class="item-wrp overlay">
				<div class="item">
				  <div class="item-img-wrp">
					<img src="<?php echo @$val['event_image']; ?>" alt="">
				  </div>
				  <div class="item-desc-wrp">
					<div class="inner">
					<h2 class="title"><?php echo @$val['event_name']; ?></h2>
					<div class="desc"><?php echo @$val['event_short_desc']; ?></div>
					<div class="btns-wrp">
					  <a href="<?php echo base_url(); ?>events/<?php echo $val["object_slug"].'-'.@$val['event_id']; ?>" class="scbtn scbtn-secondary white"onclick="_targetClickTrack('<?php echo @$val['event_id']; ?>','10','<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['event_name']); ?>','event');" ><span>Read More</span></a>
					</div>

				  </div>
					</div>
					<span class="ic-heart <?php if(@$val['is_wishlisted'] == '1') echo "active"; ?>" id="wishlist_e<?php echo @$val['event_id']; ?>"onclick="add_to_fav('event',<?php echo @$val['event_id']; ?>,<?php if($user_id > 0){ echo @$this->session->userdata('user_id'); }else { echo '0'; } ?>, '<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['event_name']); ?>');">
					  <i class="icon-heart"></i>
					</span>
				</div>
			  </div>
			</div>
		<?php }//foreach
			}//if	?> 