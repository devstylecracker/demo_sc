<?php
//ini_set("zlib.output_compression", "On");
//ini_set("zlib.output_compression_level", "1");

$user_name = $this->session->userdata('user_name');
$email = $this->session->userdata('email');
$user_id = $this->session->userdata('user_id');
$bucket = $this->session->userdata('bucket');
$trans = array();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <meta name="robots" content="index, follow">
  <title><?php echo $this->seo_title!='' ? $this->seo_title : 'StyleCracker' ; ?></title>
  <meta content="<?php echo $this->seo_desc; ?>" name="Description">
  <meta content="<?php echo $this->seo_keyword; ?>" name="Keywords">
  <?php if($this->seo_canonical!=''){   ?>
        <link rel="canonical"  href="<?php echo $this->seo_canonical; ?>" />
  <?php }   ?>
  <?php if($this->seo_prev!=''){   ?>
        <link rel="prev" href="<?php echo $this->seo_prev; ?>" />
  <?php }
        if($this->seo_next!=''){    ?>
        <link rel="next" href="<?php echo $this->seo_next; ?>" />

  <?php }   ?>

  <style>html{font-family:sans-serif}body{margin:0}header,nav,section{display:block}a{background-color:transparent}b{font-weight:700}h1{margin:.67em 0;font-size:2em}img{border:0}button,input{margin:0;font-family:inherit;font-size:inherit;font-style:inherit;font-variant:inherit;font-weight:inherit;line-height:inherit;color:inherit}button{overflow:visible}button{text-transform:none}button{-webkit-appearance:button}input{line-height:normal}*{box-sizing:border-box}::after,::before{box-sizing:border-box}html{font-size:10px}body{font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;line-height:1.42857143;color:rgb(51,51,51);background-color:rgb(255,255,255)}button,input{font-family:inherit;font-size:inherit;line-height:inherit}a{color:rgb(51,122,183);text-decoration:none}img{vertical-align:middle}h1,h2,h3{font-family:inherit;font-weight:500;line-height:1.1;color:inherit}h1,h2,h3{margin-top:20px;margin-bottom:10px}h1{font-size:36px}h2{font-size:30px}h3{font-size:24px}.text-center{text-align:center}ul{margin-top:0;margin-bottom:10px}.container{padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}@media (min-width:768px){.container{width:750px}}@media (min-width:992px){.container{width:970px}}@media (min-width:1200px){.container{width:1170px}}.container-fluid{padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}.row{margin-right:-15px;margin-left:-15px}.col-md-1,.col-md-2,.col-md-3,.col-md-4,.col-md-7,.col-md-8,.col-md-9,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-7,.col-xs-12,.col-xs-2,.col-xs-3,.col-xs-7{position:relative;min-height:1px;padding-right:15px;padding-left:15px}.col-xs-12,.col-xs-2,.col-xs-3,.col-xs-7{float:left}.col-xs-12{width:100%}.col-xs-7{width:58.33333333%}.col-xs-3{width:25%}.col-xs-2{width:16.66666667%}@media (min-width:768px){.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-7{float:left}.col-sm-12{width:100%}.col-sm-7{width:58.33333333%}.col-sm-3{width:25%}.col-sm-2{width:16.66666667%}}@media (min-width:992px){.col-md-1,.col-md-2,.col-md-3,.col-md-4,.col-md-7,.col-md-8,.col-md-9{float:left}.col-md-9{width:75%}.col-md-8{width:66.66666667%}.col-md-7{width:58.33333333%}.col-md-4{width:33.33333333%}.col-md-3{width:25%}.col-md-2{width:16.66666667%}.col-md-1{width:8.33333333%}}label{display:inline-block;max-width:100%;margin-bottom:5px;font-weight:700}.form-control{display:block;width:100%;height:34px;padding:6px 12px;font-size:14px;line-height:1.42857143;color:rgb(85,85,85);background-color:rgb(255,255,255);background-image:none;border:1px solid rgb(204,204,204);border-top-left-radius:4px;border-top-right-radius:4px;border-bottom-right-radius:4px;border-bottom-left-radius:4px;-webkit-box-shadow:rgba(0,0,0,.0745098) 0 1px 1px inset;box-shadow:rgba(0,0,0,.0745098) 0 1px 1px inset}.form-control::-webkit-input-placeholder{color:rgb(153,153,153)}.form-group{margin-bottom:15px}.fade{opacity:0}.collapse{display:none}.dropdown{position:relative}.dropdown-menu{position:absolute;top:100%;left:0;z-index:1000;display:none;float:left;min-width:160px;padding:5px 0;margin:2px 0 0;font-size:14px;text-align:left;list-style:none;background-color:rgb(255,255,255);-webkit-background-clip:padding-box;background-clip:padding-box;border:1px solid rgba(0,0,0,.14902);border-top-left-radius:4px;border-top-right-radius:4px;border-bottom-right-radius:4px;border-bottom-left-radius:4px;-webkit-box-shadow:rgba(0,0,0,.172549) 0 6px 12px;box-shadow:rgba(0,0,0,.172549) 0 6px 12px}.nav{padding-left:0;margin-bottom:0;list-style:none}.nav>li{position:relative;display:block}.nav>li>a{position:relative;display:block;padding:10px 15px}@media (min-width:768px){.navbar-header{float:left}}.navbar-collapse{padding-right:15px;padding-left:15px;overflow-x:visible;border-top-width:1px;border-top-style:solid;border-top-color:transparent;-webkit-box-shadow:rgba(255,255,255,.0980392) 0 1px 0 inset;box-shadow:rgba(255,255,255,.0980392) 0 1px 0 inset}@media (min-width:768px){.navbar-collapse{width:auto;border-top-width:0;-webkit-box-shadow:none;box-shadow:none}.navbar-collapse.collapse{padding-bottom:0;display:block!important;height:auto!important;overflow:visible!important}}.navbar-brand{float:left;height:50px;padding:15px;font-size:18px;line-height:20px}.navbar-brand>img{display:block}.navbar-toggle{position:relative;float:right;padding:9px 10px;margin-top:8px;margin-right:15px;margin-bottom:8px;background-color:transparent;background-image:none;border:1px solid transparent;border-top-left-radius:4px;border-top-right-radius:4px;border-bottom-right-radius:4px;border-bottom-left-radius:4px}@media (min-width:768px){.navbar-toggle{display:none}}.navbar-nav{margin:7.5px -15px}.navbar-nav>li>a{padding-top:10px;padding-bottom:10px;line-height:20px}@media (min-width:768px){.navbar-nav{float:left;margin:0}.navbar-nav>li{float:left}.navbar-nav>li>a{padding-top:15px;padding-bottom:15px}}@media (min-width:768px){.navbar-right{margin-right:-15px;float:right!important}}.close{float:right;font-size:21px;font-weight:700;line-height:1;color:rgb(0,0,0);text-shadow:rgb(255,255,255) 0 1px 0;opacity:.2}button.close{-webkit-appearance:none;padding:0;border:0;background-position:0 0;background-repeat:initial initial}.modal{position:fixed;top:0;right:0;bottom:0;left:0;z-index:1050;display:none;overflow:hidden;outline:rgb(0,0,0)}.modal.fade .modal-dialog{-webkit-transform:translate(0,-25%)}.modal-dialog{position:relative;width:auto;margin:10px}.modal-content{position:relative;background-color:rgb(255,255,255);-webkit-background-clip:padding-box;background-clip:padding-box;border:1px solid rgba(0,0,0,.2);border-top-left-radius:6px;border-top-right-radius:6px;border-bottom-right-radius:6px;border-bottom-left-radius:6px;outline:rgb(0,0,0);-webkit-box-shadow:rgba(0,0,0,.498039) 0 3px 9px;box-shadow:rgba(0,0,0,.498039) 0 3px 9px}.modal-body{position:relative;padding:15px}.modal-footer{padding:15px;text-align:right;border-top-width:1px;border-top-style:solid;border-top-color:rgb(229,229,229)}@media (min-width:768px){.modal-dialog{width:600px;margin:30px auto}.modal-content{-webkit-box-shadow:rgba(0,0,0,.498039) 0 5px 15px;box-shadow:rgba(0,0,0,.498039) 0 5px 15px}.modal-sm{width:300px}}.container-fluid::after,.container-fluid::before,.container::after,.container::before,.modal-footer::after,.modal-footer::before,.nav::after,.nav::before,.navbar-collapse::after,.navbar-collapse::before,.navbar-header::after,.navbar-header::before,.row::after,.row::before{display:table;content:' '}.container-fluid::after,.container::after,.modal-footer::after,.nav::after,.navbar-collapse::after,.navbar-header::after,.row::after{clear:both}.hide{display:none!important}@media (max-width:991px) and (min-width:768px){.hidden-sm{display:none!important}}
</style>

<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/fav-icon.jpg">
<link href='<?php echo base_url(); ?>assets/seventeen/bootstrap/css/bootstrap.min.css' rel='stylesheet' type='text/css'>

  <!--  <link href='<?php echo base_url(); ?>assets/seventeen/css/combined.css' rel='stylesheet' type='text/css'> -->


  <link href='<?php echo base_url(); ?>assets/seventeen/fonts/font-awesome/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
  <link href='<?php echo base_url(); ?>assets/seventeen/iconfonts/style.css' rel='stylesheet' type='text/css'>

  <!--DO NOT UNCOMMENT-->
   <link href='<?php echo base_url(); ?>assets/seventeen/plugins/swiper/css/swiper.min.css' rel='stylesheet' type='text/css'>
  <link href='<?php echo base_url(); ?>assets/seventeen/plugins/slick/slick.css' rel='stylesheet' type='text/css'>
  <link href='<?php echo base_url(); ?>assets/seventeen/plugins/slick/slick-theme.css' rel='stylesheet' type='text/css'>
  <link href='<?php echo base_url(); ?>assets/seventeen/plugins/perfect-scrollbar/css/perfect-scrollbar.min.css' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/seventeen/plugins/datepicker/datepicker3.css">
  <!---->

  <link href='<?php echo base_url(); ?>assets/seventeen/css/plugins-settings.css?v=1.006' rel='stylesheet' type='text/css'>
  <!-- <link href='<?php echo base_url(); ?>assets/seventeen/css/common_combined.css' rel='stylesheet' type='text/css'> -->


<!--DO NOT UNCOMMENT-->
   <link href='<?php echo base_url(); ?>assets/seventeen/css/common.css?v=1.0012' rel='stylesheet' type='text/css'>
  <link href='<?php echo base_url(); ?>assets/seventeen/css/btns.css?v=1.006' rel='stylesheet' type='text/css'>
  <link href='<?php echo base_url(); ?>assets/seventeen/css/custom.css?v=1.043' rel='stylesheet' type='text/css'>
  <link href='<?php echo base_url(); ?>assets/seventeen/css/mobile.css?v=1.013' rel='stylesheet' type='text/css'>
 
<!---->

   <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900" rel="stylesheet">

  <link rel="manifest" href="/manifest.json">

<style type="text/css">
  .form-group .message, .form-group label.error{
    font-size: 80% !important;
  }

</style>
<!-- GA Ecommerce Tracking Code -->
<?php 
// code for ga_ecommerce 
$order_id = @$_GET['order'];
//if(@$_GET['order']!='' && @$_GET['step']==6)
if(@$_GET['order']!='' && trim(@$this->uri->segment(2))=='step4')
{
  $res = $this->cartnew_model->su_get_order_info($order_id);

  $brand_price = $this->cartnew_model->su_get_order_price_info($order_id);
    //echo '<pre>';print_r($brand_price);exit;
    $paymod = $brand_price[0]['pay_mode'];
    if($paymod == 1) $paymentMethod = 'cod'; else $paymentMethod = 'online';
    
    $items = array();
    $shipping_amt = $brand_price[0]['shipping_amount']>0 ? $brand_price[0]['shipping_amount'] : 0 ;
    $tax = $brand_price[0]['order_tax_amount']>0 ? $brand_price[0]['order_tax_amount'] : 0 ;

    //-------------------------------------GA Ecommerce Tracking Code -----------------------------------------------------------------------------------------------------------
    $i=0;
    $affiliation = '';
    $brandName = '';
    foreach($res as $val)
    {
        if($affiliation!='')
        {
          if($brandName != $val['company_name'])
          {
              $affiliation = $affiliation."-".$val['company_name'];
          }
           $brandName = $val['company_name'];
          
        }else
        {
            $brandName = $val['company_name'];
           $affiliation = $val['company_name'];
        }
      // code for items array 
      if(empty($items))
        {
          $items[$i] = array('sku'=>$val['product_id'], 'name'=>$val['name'], 'category'=>'scbox', 'price'=>$val['product_price'], 'quantity'=>$val['product_qty'],'currency'=> 'INR');
        }else
        {
             $items[$i] = array('sku'=>$val['product_id'], 'name'=>$val['name'], 'category'=>'scbox', 'price'=>$val['product_price'], 'quantity'=>$val['product_qty'],'currency'=> 'INR');
        }
        $i++;
    }
    // code for trans array 
    $trans = array('id'=>$order_id, 'affiliation'=>$affiliation,'revenue'=>$brand_price[0]['order_total'], 'shipping'=>$shipping_amt, 'tax'=>$tax,'city'=>$brand_price[0]['city_name'],'state'=>$brand_price[0]['city_name'],'currency'=> 'INR');
  }
    ?>


<script type="text/javascript">
     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-97550861-1', 'auto');
          ga('send', 'pageview');

          ga('require','linkid','linkid.js');
          ga('require', 'displayfeatures');

          ga('require', 'ecommerce','ecommerce.js');

         <?php 
            if ($_SERVER['SCRIPT_NAME'] == '/book-scbox/step4?order=') {}
          ?>

<?php if(!empty($trans))
      {
?>
         ga('ecommerce:addTransaction', {
          'id': '<?=$trans['id']?>',
          'affiliation': '<?=$trans['affiliation']?>',
          'revenue': '<?=$trans['revenue']?>',
          'tax': '<?=$trans['tax']?>',
          'shipping': '<?=$trans['shipping']?>',
          'city': '<?=$trans['city']?>',
          'state': '<?=$trans['state']?>',
          'currency':'<?=$trans['currency']?>'
        });

        ga('ecommerce:addItem', {
          'id':'<?=$trans['id']?>',           // transaction ID - required
           'sku': '<?=$items[0]['sku']?>',           // SKU/code - required
            'name':'<?=$items[0]['name']?>',        // product name
            'category':'<?=$items[0]['category']?>',   // category or variation
            'price':'<?=$items[0]['price']?>',          // unit price - required
            'quantity':'<?=$items[0]['quantity']?>'               // quantity - required
        });

        ga('ecommerce:send');

  <?php }
  ?>       
          
          <?php if(isset($_GET['search']) && $_GET['search'] != ""){echo "ga('send', 'event', 'Search', 'keyword', '".$_GET['search']."');";} ?>

         var sc_beatout_email_id = "<?php echo $this->session->userdata('email')!='' ? $this->session->userdata('email') : ''; ?>";
         var sc_beatout_username = "<?php echo $this->session->userdata('user_name')!='' ? $this->session->userdata('user_name') : ''; ?>";
         var sc_user_id = "<?php echo $this->session->userdata('user_id')!='' ? $this->session->userdata('user_id') : ''; ?>";
         var sc_invite_utm_source = "<?php echo $this->session->userdata('utm_source')!='' ? $this->session->userdata('utm_source') : ''; ?>";
         var CE_SNAPSHOT_NAME = "Looks";
         var sc_baseurl = "<?php echo base_url(); ?>";
          /*setTimeout(function(){var a=document.createElement("script");

          var b=document.getElementsByTagName("script")[0];

          a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0040/4266.js?"+Math.floor(new Date().getTime()/3600000);

          a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);*/
          
  </script>
  
  
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '889940927729546');
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" width="1"
src="https://www.facebook.com/tr?id=889940927729546&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->





<!--GA Ecommerce Tracking Code -->
 <style type="text/css">
    .no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .se-pre-con {
      position: fixed;
      left: 0px;
      top: 0px;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background: url(https://www.stylecracker.com/assets/images/press/rolling.gif) center no-repeat transparent;
    }
  </style>
<!-- <script language="JavaScript">

//////////F12 disable code////////////////////////
    document.onkeypress = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
           //alert('No F-12');
            return false;
        }
    }
    document.onmousedown = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
            //alert('No F-keys');
            return false;
        }
    }
document.onkeydown = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
            //alert('No F-keys');
            return false;
        }
    }
/////////////////////end///////////////////////


//Disable right click script 
//visit http://www.rainbow.arch.scriptmania.com/scripts/ 
var message="Sorry, right-click has been disabled"; 
/////////////////////////////////// 
function clickIE() {if (document.all) {(message);return false;}} 
function clickNS(e) {if 
(document.layers||(document.getElementById&&!document.all)) { 
if (e.which==2||e.which==3) {(message);return false;}}} 
if (document.layers) 
{document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;} 
else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;} 
document.oncontextmenu=new Function("return false") 
// 
function disableCtrlKeyCombination(e)
{
//list all CTRL + key combinations you want to disable
var forbiddenKeys = new Array('a', 'n', 'c', 'x', 'v', 'j' , 'w');
var key;
var isCtrl;
if(window.event)
{
key = window.event.keyCode;     //IE
if(window.event.ctrlKey)
isCtrl = true;
else
isCtrl = false;
}
else
{
key = e.which;     //firefox
if(e.ctrlKey)
isCtrl = true;
else
isCtrl = false;
}
//if ctrl is pressed check if other key is in forbidenKeys array
if(isCtrl)
{
for(i=0; i<forbiddenKeys.length; i++)
{
//case-insensitive comparation
if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())
{
alert('Key combination CTRL + '+String.fromCharCode(key) +' has been disabled.');
return false;
}
}
}
return true;
}
</script> -->
  </head>
  <body>
  <!-- Google Code for Remarketing Tag -->
<!--
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
-->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 944683658;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/944683658/?guid=ON&amp;script=0"/>
</div>
</noscript>


    <div class="se-pre-con"></div>
    <!-- //FB Initialization
  // FB APP Note -->
    <div id="fb-root"></div>
    <script>
         window.fbAsyncInit = function() {
            FB.init({
            appId      : '1650358255198436', // Live
            //appId      : '781978078605154', // SC Nest
            xfbml      : true,
            version    : 'v2.10' // Live
            //version    : 'v2.4' // Live
            //version    : 'v2.5' // SC nest
            });
          };

        (function(d, s, id){
           var js, fjs = d.getElementsByTagName(s)[0];
           if (d.getElementById(id)) {return;}
           js = d.createElement(s); js.id = id;
           js.src = "//connect.facebook.net/en_US/sdk.js";
           fjs.parentNode.insertBefore(js, fjs);
         }(document, 'script', 'facebook-jssdk'));
    </script>
<script>
var base_url ="<?php echo base_url(); ?>";
</script>
<!-- popover -->
      <?php
      $popup = $this->home_model->get_popup();
      if(!empty($popup)){
        foreach($popup as $val){
          if(current_url() == $val['page_link'] || $val['page_link']==''){
            if($val['popup_interval_chosen'] == 1){
              setcookie('popup_timer',30*60000, time() + (86400 * 30), "/");
            }else if($val['popup_interval_chosen'] == 2){
              setcookie('popup_timer',40*60000, time() + (86400 * 30), "/");
            }else if($val['popup_interval_chosen'] == 3){
              setcookie('popup_timer',50*60000, time() + (86400 * 30), "/");
            }else if($val['popup_interval_chosen'] == 4){
              setcookie('popup_timer',60*60000, time() + (86400 * 30), "/");
            }else if($val['popup_interval_chosen'] == 5){
              setcookie('popup_timer',90*60000, time() + (86400 * 30), "/");
            }

      ?>
      <div id="slideover" class="sc-slideover top" style="display:none;">
        <div class="slideover-inner">
          <a href="<?php echo $val['target_link']; ?>" onclick="ga('send', 'event', 'Web Popup', 'clicked', '<?php echo $val['popup_name']; ?>');" target="_blank"><img src="<?php echo base_url(); ?>assets/images/popups/desktop/<?php echo $val['desktop_img']; ?>" /></a>
        </div>
        <div class="close">
          <i class="fa fa-close1 fa-times-circle"></i>
        </div>
      </div>
      <?php  } } } ?>
<!-- //popover -->

<div class="wrapper" style="min-height: 0;">
<?php //include('nav_view.php'); ?>
<?php if($this->uri->segment(1)=='sc-box' || $this->uri->segment(1)=='schome_new' || $this->uri->segment(1)=='')
      {
        include('nav_view_scbox.php');
      }else
      {
        include('nav_view.php');
      }
?>

