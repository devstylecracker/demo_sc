<style type="text/css">
  .footer .box-social {
    border-right: none;
    height: 60px;
}

@media (max-width: 768px){
  .mobile-footer{
    display: none;
  }
}
</style>
<footer id="footer" class="footer footer-bottom section-shadow section mobile-footer">
  <!-- <div class="sm-only sm-footer">
    <nav class="sm-footer-navbar" id="sm-footer-navbar">
      <ul class="footer-nav">
        <li class="active111">
          <a href="<?php //echo base_url(); ?>">
          <i class="icon icon-home"></i>
          </a>
        </li>
        <li><a href="<?php //echo base_url(); ?>category-list"><i class="icon icon-tag"></i></a></li> 
        <li><a id="my_custom_link"><i class="icon icon-message"></i>  <span class="ic-count shape-diamond solid">5</span></a></li>
        <li><a href="#" data-toggle="collapse" data-target="#search-panel"><i class="icon icon-search"></i></a></li>
        <li><a href="<?php //echo base_url(); ?>cart"><i class="icon icon-bag"></i>
              <span id="total_products_cart_count_mob" class="ic-count shape-diamond solid">0</span></a>
        </li>
      </ul>
    </nav>
  </div> -->

  <div class="container-fluid md-only">
    <div class="row">
	    <div class="col-md-4">
        <!-- <div class="box-categories">
          <div class="title">
            Top Categories
          </div>

          <ul>
            <?php
            $footer_cat = '';
            $footer_cat = $this->data['footer_categories'];

            if(!empty($footer_cat))
            {
                foreach($footer_cat as $key=>$val)
                {
            ?>
                  <li ><a href="<?php echo CAT_URL; ?><?php echo @$val['slug'].'-'.@$val['id']; ?>"><?php echo @$val['label']; ?></a></li>

            <?php
                }
            }
            ?>
          </ul>
        </div> -->
      </div>

      <div class="col-md-4">
       <!--  <div class="box-categories">
          <div class="title">
            Top Brands
          </div>

          <ul>
            <?php
            $footer_brands = '';
            $footer_brands = $this->data['footer_brands'];
// echo "<pre>";print_r($footer_brands);exit;
            if(!empty($footer_brands))
            {
                foreach($footer_brands as $val)
                {//echo "<pre>";print_r($val);exit;
            ?>
                  <li ><a href="<?php echo BRAND_URL; ?><?php echo @$val->user_name.'-'.@$val->user_id; ?>"><?php echo @$val->company_name; ?></a></li>

            <?php
                }
            }
            ?>
          </ul>
        </div> -->
      </div>
      <div class="col-md-12" style="text-align: center;">
        <div class="box-social1">
          <div class="title">
            Follow us on
          </div>
          <ul class="list-inline">
            <li class="facebook">
              <a target="_blank" href="https://www.facebook.com/StyleCracker">
                <i class="fa fa-facebook-official"></i> Facebook</a>
            </li>
            <li class="instagram">
              <a target="_blank" href="https://instagram.com/stylecracker"> <i class="fa fa-instagram"></i> Instagram</a>
            </li>
            <li class="twitter">
              <a target="_blank" href="https://twitter.com/Style_Cracker"> <i class="fa fa-twitter"></i> Twitter</a>
            </li>
          <!--  <li class="pinterest">
              <a target="_blank" href="https://www.pinterest.com/stylecracker"> <i class="fa fa-pinterest"></i> Pinterest</a>
            </li>-->
            <li class="snapchat">
              <a target="_blank" href="https://www.snapchat.com/add/stylecracker"> <i class="fa fa-snapchat"></i> Snapchat</a>
            </li>

          </ul>
        </div>
      </div>
      <!-- <div class="col-md-2">
        <div class="box-nav">
          <div class="title">
            &nbsp;
          </div>
          <ul>
            <li>
              <a href="<?php echo base_url(); ?>contact-us"> Contact Us</a>
            </li>
            <li>
              <a href="#"> Help & Feedback</a>
            </li>
            <li>
              <a href="<?php echo base_url(); ?>terms-and-conditions"> Terms & Conditions  </a>
            </li>
            <li>
              <a href="<?php echo base_url(); ?>return-policy"> Return Policy  </a>
            </li>
            <li>
              <a href="<?php echo base_url(); ?>privacy-policy"> Privacy Policy  </a>
            </li>
            <li>
              <a href="<?php echo base_url(); ?>faq"> FAQ</a>
            </li>
          </ul>
        </div>
      </div> -->
       <div class="row" style="text-align: center;">
      <div class="col-md-12">
        <div class="box-nav">
         <!--  <div class="title">
            &nbsp;
          </div> -->
          <ul class="list-inline">
            <li>
              <a href="<?php echo base_url(); ?>contact-us"> Contact Us  |</a>
            </li>
          <!--  <li>
              <a href="#">Help & Feedback  |</a>
            </li> -->
            <li>
              <a href="<?php echo base_url(); ?>terms-and-conditions">Terms & Conditions  | </a>
            </li>
            <li>
              <a href="<?php echo base_url(); ?>return-policy">Return Policy  | </a>
            </li>
            <li>
              <a href="<?php echo base_url(); ?>privacy-policy">Privacy Policy  | </a>
            </li>
            <li>
              <a href="<?php echo base_url(); ?>faq">FAQ</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    </div>
  </div>
</footer>
<!--
<div id="load-more hide" class="load-more fa-loader-wrp">
  <i class="fa fa-circle-o-notch fa-spin-custom"></i> Loading...
</div>-->
<div class="page-loader" id="page-loader">
<div class="fa-loader-wrp">
  <i class="fa fa-circle-o-notch fa-spin-custom"></i> Loading...
</div>
<div class="page-overlay" id="page-overlay"></div>
</div>
<?php include('popups.php'); ?>



  <!-- js -->
  <script src="<?php echo base_url(); ?>assets/seventeen/plugins/devicejs/js/device.min.js"></script>
   <script>
    var devicejs = device.noConflict();
  </script>

  <script src="<?php echo base_url(); ?>assets/seventeen/js/jquery.min.js"></script>
 <!--  <script src="<?php echo base_url(); ?>assets/seventeen/js/jquery-ui.js"></script> -->

  <script src="<?php echo base_url(); ?>assets/seventeen/bootstrap/js/bootstrap.min.js"></script>
 <!-- <script src="<?php echo base_url(); ?>assets/seventeen/plugins/shrinked.js"></script> -->
 
  <script src="<?php echo base_url(); ?>assets/seventeen/plugins/jqueryvalidate/jquery.validate.min.js"></script> 
  <!-- <script src="<?php echo base_url(); ?>assets/seventeen/plugins/customjs.js?v=1.04"></script>  -->
  <script src="<?php echo base_url(); ?>assets/seventeen/js/custom-new.js?v=1.29"></script>
  <script src="<?php echo base_url(); ?>assets/seventeen/plugins/hoverintent/hoverIntent.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/seventeen/plugins/swiper/js/swiper.jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/seventeen/plugins/slick/slick.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/seventeen/plugins/elevatezoom-plus/jquery.ez-plus.js"></script>
  <script src="<?php echo base_url(); ?>assets/seventeen/plugins/magnifik/magnifik.js"></script>
  <script src="<?php echo base_url(); ?>assets/seventeen/plugins/jquerycropit/jquery.cropit.js"></script>
  <script src="<?php echo base_url(); ?>assets/seventeen/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>

  <script src="<?php echo base_url(); ?>assets/seventeen/plugins/datepicker/bootstrap-datepicker.js"></script>

  <script src="<?php echo base_url(); ?>assets/seventeen/js/plugins.settings.js?v=1.004"></script>
  <script src="<?php echo base_url(); ?>assets/seventeen/js/home.js"></script>
  <script src="<?php echo base_url(); ?>assets/seventeen/js/custom.js?v=1.015"></script>
  <script src="<?php echo base_url(); ?>assets/seventeen/js/common_v1.js?v=1.0014"></script>
  <script src="<?php echo base_url(); ?>assets/seventeen/js/analytics.js?v=1.008"></script>   

  <!--<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.js"></script>-->

 <script type="text/javascript">
  $(window).load(function() {
    // Animate loader off screen
    $(".se-pre-con").fadeOut("slow");
  });
</script>

  <?php
  /* dont remove*/
  /*
  <script src="<?php echo base_url(); ?>assets/seventeen/dist/js/main.min.js"></script>
  */
    /* //.dont remove*/
?>

  <?php if($this->uri->segment(1)=='pa' || $this->uri->segment(2)=='pa'){ ?>
  <script src="<?php echo base_url(); ?>assets/seventeen/js/pa.js?v=1.0013"></script>
  <?php } ?>

  <?php  //if($this->uri->segment(1) =='sc-box' || $this->uri->segment(1) =='book-scbox' || $this->uri->segment(1) =='rakhi-special' || $this->uri->segment(1) =='' ){ ?>  
<!--   <script src="<?php echo base_url(); ?>assets/seventeen/js-scbox/scbox.js?v=1.000091"></script> -->
  <?php //} ?>



  <script src="<?php echo base_url(); ?>assets/seventeen/js/sc-box_v1.js?v=1.257"></script>
  <script src="<?php echo base_url(); ?>assets/seventeen/js/scbox_validate.js?v=1.0196"></script> 

  
   <?php  if($this->uri->segment(1) =='stylistscbox' || $this->uri->segment(1) =='stylistbookscbox' ){ ?>  
  <script src="<?php echo base_url(); ?>assets/seventeen/js-scbox/stylistscbox.js?v=.000008"></script>
  <?php } ?>



	<!-- added for share  -->
	<!-- <script src="<?php echo base_url(); ?>assets/js/analytics.js"></script> -->
	<!--added for share  -->
	<!--<script src="<?php echo base_url(); ?>assets/seventeen/js/category_filter.js?v=1.023"></script>-->

  <script src="<?php echo base_url(); ?>assets/seventeen/js/style-cart.js?v=0.88"></script>

  <script src="<?php echo base_url(); ?>assets/seventeen/js/stylecracker-cart.js?v=1.7"></script>

  <!-- <script src="<?php echo base_url(); ?>assets/js/analytics.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/sociallogin.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/cart_v1.js?v=1.31"></script>
  <script src="<?php echo base_url(); ?>assets/js/sc-tracker.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/scrollpagination.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/common.js?v=5.71"></script>
  <script src="<?php echo base_url(); ?>assets/js/stylecracker-cart.js?v=1.65"></script> -->
<script src="<?php echo base_url(); ?>assets/ImageTools.js"></script>
  <!-- /.custom js -->

  <!-- <script src="<?php echo base_url(); ?>assets/js/sc-tracker.js"></script> -->
  <!-- /.js -->
  <!--<script src="<?php echo base_url(); ?>assets/seventeen/js/test.js?v=0.1"></script>-->
<?php if($this->session->userdata('user_id') > 0) { $gender = '';
  if($this->session->userdata('gender') == 1){ $gender = 'Women'; }
  if($this->session->userdata('gender') == 2){ $gender = 'Men'; }
 ?>
<script>
if($('html').hasClass("mobile") || $('html').hasClass("tablet")){
  window.intercomSettings = {
    app_id: "oz2nj7x5",
    name: "<?php echo @$this->session->userdata('user_name'); ?>", // Full name
    email: "<?php echo @$this->session->userdata('email'); ?>", // Email address
    user_id: "<?php echo @$this->session->userdata('user_id'); ?>", // User ID
    created_at: "<?php echo strtotime(date('Y-m-d H:i:s')) ?>", // Signup date as a Unix timestamp
    "Bucket" : "<?php echo @$this->session->userdata('bucket'); ?>",
    "Gender" : "<?php echo $gender; ?>",
    user_hash: "<?php echo hash_hmac('sha256',$this->session->userdata('user_id'),'rK2JpjpInEcvXCtXb9yRzOFRLTmR4wqLfVuVPTal'); ?>",
	custom_launcher_selector: '#my_custom_link',
	hide_default_launcher: true
  };
}else{
	window.intercomSettings = {
    app_id: "oz2nj7x5",
    name: "<?php echo @$this->session->userdata('user_name'); ?>", // Full name
    email: "<?php echo @$this->session->userdata('email'); ?>", // Email address
    user_id: "<?php echo @$this->session->userdata('user_id'); ?>", // User ID
    created_at: "<?php echo strtotime(date('Y-m-d H:i:s')) ?>", // Signup date as a Unix timestamp
    "Bucket" : "<?php echo @$this->session->userdata('bucket'); ?>",
    "Gender" : "<?php echo $gender; ?>",
    user_hash: "<?php echo hash_hmac('sha256',$this->session->userdata('user_id'),'rK2JpjpInEcvXCtXb9yRzOFRLTmR4wqLfVuVPTal'); ?>"
  };
}


</script>
<?php }else{ ?>
<script>
if($('html').hasClass("mobile") || $('html').hasClass("tablet")){
  window.intercomSettings = {app_id: "oz2nj7x5",
							custom_launcher_selector: '#my_custom_link',
							hide_default_launcher: true};
}else{
	window.intercomSettings = {app_id: "oz2nj7x5"};
}
</script>
<?php } ?>
<?php if($this->session->userdata('is_app') == 1 || @$this->input->get('is_app') == 1){ $is_app = 1; }else { $is_app = 0; } ?>
<?php if( $is_app == 0){

  ?>

<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/oz2nj7x5';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>

<?php } ?>


<!-- If the site opens on mobile app container the header will hide Start -->
<?php if($this->session->userdata('is_app') == 1 || @$this->input->get('is_app') == 1){ $is_app = 1; }else { $is_app = 0; } ?>
<script>
	var is_app = "<?php echo $is_app; ?>";
	if(is_app ==0){ $('#header').show(); $('#footer').show(); $('.fixed.header').show(); }
</script>
<!-- If the site opens on mobile app container the header will hide END -->


<!-- Invite Code Start -->

<!--<a id="invite_copy" class="btn btn-refer" href="https://www.stylecracker.com/login" data-toggle="modal" title=""><i class="fa fa-gift"></i> Refer & Earn</a>-->

<div id='invtrflfloatbtn'></div>

<?php if( $is_app == 0){

  ?>

<script>	
	var sc_user_id = "<?php echo $this->session->userdata('user_id')!='' ? $this->session->userdata('user_id') : ''; ?>";
	if(sc_user_id>0){
		$('#invite_copy').hide();
		$('#invite_message').hide();
		var invite_referrals = window.invite_referrals || {}; (function() { 
			invite_referrals.auth = { bid_e : 'AC70A50D47DD5C5FF203ED7121693FC4', bid : '11257', t : '420', email : sc_beatout_email_id, fname : sc_beatout_username };	
		var script = document.createElement('script');script.async = true;
		script.src = (document.location.protocol == 'https:' ? "//d11yp7khhhspcr.cloudfront.net" : "//cdn.invitereferrals.com") + '/js/invite-referrals-1.0.js';
		var entry = document.getElementsByTagName('script')[0];entry.parentNode.insertBefore(script, entry); })();
	}else {
		$('#invite_copy').show();
	}	
</script>
 <?php
} ?>

<!-- Invite Code END -->




</body>

</html>

<?php if($this->session->userdata('utm_source1') == 'invitereferrals'){  ?>
 <img style="position:absolute; visibility:hidden" src="https://www.ref-r.com/campaign/t1/settings?bid_e=AC70A50D47DD5C5FF203ED7121693FC4&bid=11257&t=420&event=register&email=<?php echo $this->session->userdata("email"); ?>&orderID=<?php echo $this->session->userdata("user_id"); ?>&fname=<?php echo $this->session->userdata("first_name"); ?>" />
 <?php //$this->session->unset_userdata('utm_source1'); 
} ?>

<?php
// added for redirection of user after login 
$url_array = explode('/',$_SERVER['REQUEST_URI']);
$count = count($url_array);
if(@$url_array[$count -1] == 'signup' || @$url_array[$count -1] == 'login' || @$url_array[$count -1] == 'pa' ){
	//do nothing
}else{
	//$array_field=array('url' => $_SERVER['REQUEST_URI']);
  $array_field=array('url' => '');
	$this->session->set_userdata($array_field);
}
?>