<style type="text/css">
  @media (max-width: 768px){
  .mobile-header{
    display: none;
  }

  .fixed.header{
    display: none;
  }
}
</style>
<?php
    $selectionClasswomen = '';
    $selectionClassmen = '';
    $selectionClassbrand = '';
    if(strtolower(trim($this->uri->segment(1)) == 'all-products'))
    {
      if(strtolower(trim($this->uri->segment(2)) == 'men'))
      {
        $selectionClassmen = 'menu-highlight';
      }else
      {
        $selectionClasswomen = 'menu-highlight';
      }
    }else if( (strtolower(trim($this->uri->segment(1)) == 'brands')) || (strtolower(trim($this->uri->segment(1)) == 'brand')) )
    {
      $selectionClassbrand = 'menu-highlight';
    }

  $device='web';
  $useragent=$_SERVER['HTTP_USER_AGENT'];
  if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
    $device='mob';
  }else{ $device='web'; }
?>
<header id="header" class="header sticky mobile-header">
<div class="container-fluid">
  <div class="row">
    <div class="col-md-1 col-sm-2 col-xs-2 sm-static">
  <div class="dropdown sc-navbar-left-wrp sm-static">
    <button type="button" class="navbar-toggle dropdown-toggle" data-toggle="dropdown">
      <i class="icon-menu"></i>
    </button>
  <div class="dropdown-menu" style="z-index:1001;">
  <ul>
        <?php if(!($this->session->userdata('user_id'))){ ?>
    <li onclick="ga('send', 'event', 'Login', 'clicked','header');" ><a href="<?php echo base_url(); ?>login"><img src="<?php echo base_url(); ?>assets/seventeen/images/icons/login.png" style="height:25px;" /> <span>Login</span></a></li>
    <li onclick="ga('send', 'event', 'Sign Up', 'clicked','header');" ><a href="<?php echo base_url(); ?>signup"><img src="<?php echo base_url(); ?>assets/seventeen/images/icons/signup.png" style="height:25px;" /> <span>Sign up</span></a></li>
    	<?php } ?>
    <li><a href="<?php echo base_url(); ?>about-us"><i class="icon icon-user"></i> <span>About<br /> Us</span></a></li>

    <?php if($this->session->userdata('user_id')){ ?>
	<li><a href="<?php echo base_url(); ?>my-orders"><i class="icon icon-cart"></i> <span>My <br /> Orders</span></a></li>
   <!--  <li><a href="<?php echo base_url(); ?>bookmarks"><i class="icon icon-heart"></i> <span>Bookmarks</span></a></li> -->
	<!-- <li><a href="<?php echo base_url(); ?>invite-and-win"><i class="icon icon-document"></i> <span>Referral Amount</span></a></li> -->
	<?php } ?>
	
   <!--  <li><a href="<?php //echo base_url(); ?>brands"><i class="icon icon-star"></i> <span>Brands</span></a></li> -->
  <!--  <li><a href="<?php //echo base_url(); ?>help-feedback"><i class="icon icon-help"></i> <span>Help & <br />Feedback</span></a></li>-->
	<?php if($this->session->userdata('user_id')){ ?>
    <li><a href="<?php echo base_url(); ?>profile"><i class="icon icon-setting"></i> <span>Profile <br />Settings</span></a></li>
	<?php } ?>
  <!--  <li><a href="#"><i class="icon icon-pinterest"></i> <span>Connect to <br />Pinterest</span></a></li>-->
    <li><a href="<?php echo base_url(); ?>blog/" target="_blank"><i class="icon icon-sclive"></i> <span>SC <br />Live</span></a></li>
	<!-- <li><a href="<?php //echo base_url(); ?>yesbank"><i class="ic-yesbank"></i> <span>Yes Bank</span></a></li> -->
    <li><a href="<?php echo base_url(); ?>terms-and-conditions"><i class="icon icon-document"></i> <span>Terms & <br />Conditions</span></a></li>
    <li class="sm-only"><a href="<?php echo base_url(); ?>return-policy"><i class="icon icon-document"></i> <span>Return Policy</span></a></li>
    <li class="sm-only"><a href="<?php echo base_url(); ?>privacy-policy"><i class="icon icon-document"></i> <span>Privacy Policy</span></a></li>
    <li class="sm-only"><a href="<?php echo base_url(); ?>faq"><i class="icon icon-help"></i> <span>FAQ</span></a></li>
    <li class="sm-only"><a href="<?php echo base_url(); ?>contact-us"><i class="icon icon-document"></i> <span>Contact<br />Us</span></a></li>


    <?php if($this->session->userdata('user_id')){ ?>
      <li><a href="<?php echo base_url().'schome_new/logout'; ?>" id="sclogout"><i class="icon icon-logout"></i>  <span>Log out</span></a></li>
    <?php } ?>
  </ul>
  </div>
</div>


    </div>
    <div class="col-md-2 col-sm-4 col-xs-4">
      <div class="navbar-header">
        <a class="navbar-brand" href="<?php echo base_url(); ?>">
        <img id="logo-desktop" class="logo logo-desktop" src="<?php echo base_url(); ?>assets/images/logo.png"/>
      </a>
      </div>
    </div>
     <div class="col-md-2 hidden-md hidden-lg  col-sm-6 col-xs-6">
      <button type="button" class="navbar-toggle" style="display: block;font-size: 18px;float: right;">
     <?php if(!$this->session->userdata('user_id')){ ?>
      <a href="<?php echo base_url().'login'; ?>" style="color: #13d792;font-weight: 600;">Login</a>
       <?php } ?>
    </button>
    </div>
    <div class="col-md-6 hidden-sm position-static">
      <nav class="collapse navbar-collapse sc-navbar-primary" id="sc-navbar-primary">
        <ul class="nav navbar-nav" style="height: 40px;">
          <!--<li><a href="<?php //echo base_url(); ?>categories/women">Women</a>
            <?php// $this->load->view('seventeen/common/nav/nav_menu_sm',array('type'=>'women')); ?>
          </li>
          <li><a href="<?php //echo base_url(); ?>categories/men">Men</a>
          <?php //$this->load->view('seventeen/common/nav/nav_menu_sm',array('type'=>'men')); ?>
          </li> -->		  
          <!--<li><a href="<?php //echo base_url(); ?>collections/the-shop432c7cd3dc-189">The Store</a></li>	-->	  
		  <!-- <li><a href="<?php //echo base_url(); ?>collections/summer-pop-up-135">Summer Store</a></li>-->		  
          <!-- <li><a href="<?php //echo base_url(); ?>collections/travel-pop-up-136">Travel Shop</a></li> -->
          <!-- <li><a href="<?php //echo base_url(); ?>sc-box">SC Box</a></li> -->
          <li><a href="<?php echo base_url(); ?>#section-pricing" class="link-scrolling">Order Now</a></li>
          <li><a href="<?php echo base_url(); ?>blog/" target="_blank" >SC Live</a></li>
          <?php if(!$this->session->userdata('user_id')){ ?>
          <li onclick="ga('send', 'event', 'Login', 'clicked','header');" ><a href="<?php echo base_url().'login'; ?>">Login</a></li>
          <?php }else{ ?>

          <li class="dropdown sm-static">
              <a class="dropdown-toggle" data-toggle="dropdown"  href="#">Hi, <?php echo substr($this->session->userdata('first_name'),0,9);/*.' '.$this->session->userdata('last_name')*/ ?>  <span class="caret"></span>
              </a>
              <div class="dropdown-menu">
                <ul>
                  <li><a href="<?php echo base_url(); ?>profile">Profile Settings</a></li><br>
                   <li><a href="<?php echo base_url(); ?>my-orders">My Orders</a></li><br>
                  <li><a href="<?php echo base_url().'schome_new/logout'; ?>" id="sclogout">Log out</a></li>
                </ul>
              </div>
          </li>

          <?php } ?>
        </ul>
      </nav>
    </div>
    <!-- <div class="col-md-3 col-sm-1 col-xs-1 sm-static">
      <nav class="collapse navbar-collapse sc-navbar-secondary" id="sc-navbar-secondary">
        <ul class="nav navbar-nav navbar-right">
          <li class="md-only"><a href="<?php echo base_url(); ?>cart"><i class="icon-bag"></i>
          <span id="total_products_cart_count" class="ic-count shape-diamond solid">0</span>
          </a>
          </li>
          <?php if($this->session->userdata('user_id')>0){ ?><li class="dropdown sm-static" id="notification_html" onclick="ga('send', 'event', 'Notification-Button', 'clicked');">
            <i class="icon-notification"></i>
          </li>
		  <?php } ?>
           <li class="md-only"><a href="#" data-toggle="collapse" data-target="#search-panel"><i class="icon-search"></i></a></li>
        </ul>
      </nav>
    </div> -->
  </div>
</div>
<!-- search form -->
<div id="search-panel" class="collapse search-panel">
  <div class="search-panel-inner">
 <input type="text" name="search_keyword" id="search_keyword" value="" class="form-control form-control-search" placeholder="SEARCH" autofocus="">
 <span class="close"><i class="icon-close"></i></span>
</div>
</div>
<!--//. -->

<!-- /.container-fluid -->
</header>

<script type="text/Javascript">
  var sc_baseurl = '<?php echo base_url(); ?>';
  function invitereferrals_10640(){
    var params = { bid: 11257, cid: 10607 };
    invite_referrals.widget.inlineBtn(params);
    }
  </script>
