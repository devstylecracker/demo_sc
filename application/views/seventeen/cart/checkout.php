<?php
  //echo '<pre>';print_r($this->data['user_existing_address']);
 $brandwise_cost = array(); $storetax = 0; $brand_tax_cal = 0; $total_tax = 0; $coupon_discount=0; $add_accordian_class = ''; $style_panel = 'display:block;'; $hide_add = '';
 $first_name = $last_name = $mobile_no = $shipping_address = $pincode = $city_name = $states_name =''; $ga_product_id = '';
 $sreferral_point = 0;
 $product_price = 0; $order_total = 0; $cod_error=0; $shina_error=0; $qty = 0; $stock = 0;$ga_product_id = '';$total_cod = 0; $cod_charges = 0; $shipping_charges = 0; $cod_error = 0; $shina_error =0; $stock=0;
$user_emailid = '';  $couponOfferMsg = '';
$offer_codes = unserialize(OFFER_CODES);
 $scboxoffer_codes = unserialize(SCBOXOFFER_CODES);
// echo "<pre>"; print_r($cart_info);

// echo '<pre>';print_r($this->data['user_existing_address']);
if(!empty($this->session->userdata()) && @$this->session->userdata('user_id')!='')
{
  $user_emailid = $this->cartnew_model->get_user_info(@$this->session->userdata('user_id'))[0]['email'];
}
  if($this->uri->segment(2)=='')
  {
    setcookie('scfr', null, -1, '/');
  }

   $imagify_brandid = unserialize(IMAGIFY_BRANDID); 
?>

<section class="section section-shadow page-checkout">
  <div class="container">
    <div class="title-section">
      <h1 class="title">Checkout</h1>
    </div>

    <?php if(!empty($cart_info)){ ?>
      <?php if(isset($_GET['error']) && $_GET['error']!=''){ ?>
      <div class="box-message box-error message error">
       <div><b><?php echo $_GET['error']; ?></b></div>
      </div>
    <?php } }?>

    <div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <!-- panel -->
        <div class="box-checkout box-checkout-login  <?php echo ($this->session->userdata('user_id')!='') ? 'step-done' : 'open'; ?>" id="div-checkout-login">

            <h2 class="box-title" >1. LOGIN OR SIGN UP <span class="btn-edit"> <?php if($this->session->userdata('user_id')==''){ ?><i class="icon-edit"></i> </span><?php }else { }?></span></h2>
            <div class="box-checkout-summary email" >
              <span class="text-label" >Login ID:</span><span class="email-label" id="email-label" ><?php echo ($user_emailid!='') ? $_COOKIE['cart_email']=$user_emailid : @$_COOKIE['cart_email']; ?></span>
            </div>
            <div class="box-checkout-body">
              <div class="box-checkout-content">
              <?php if($this->session->userdata('user_id')=='')
                    {
              ?>
                <div class="box-enter-email" id="sc_checkout_login_panel" >
                    <div class="inner">
                  <input type="email" id="cart_email" name="cart_email" value="<?php if(isset($_COOKIE['cart_email'])){ echo $_COOKIE['cart_email']; } ?>" class="form-control" placeholder="ENTER EMAIL ADDRESS">
                  <button type="submit" class="scbtn scbtn-primary" onclick="cart_check_email();" ><span>Continue</span></button>

                  <div class="hr-or"><i></i><span>OR</span><i></i></div>
                  <div class="signin-social-wrp">
                      <span class="sub-title">
                        Sign in using
                      </span>
                      <a href="javascript:void(0);" title="Sign in using Facebook" class="btn-facebook-wrp" id="cart_facebook_login" ><i class="icon icon-facebook"></i></a>
                      <a href="<?php echo $this->data['google_login_url']; ?>" title="Sign in using Google" class="btn-google-wrp"><i class="icon icon-google">
                        <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i>
                      </a>
                  </div>
                    </div>
                  <div class="message error hide"></div>
                </div>
                <?php
                  }else
                  {
                ?>
                <div class="box-enter-email">
                  <input type="email" id="cart_email" name="cart_email" value="<?php echo ($this->session->userdata('user_id')!='') ? $this->session->userdata('email') : @$_COOKIE['cart_email']; ?>" class="form-control" placeholder="ENTER EMAIL ADDRESS" disabled>
                  <div class="message error hide"></div>
                </div>
                <?php } ?>


                <div class="box-enter-password hide" id="box-password" >
                  <p class="text-line1">You already have an account with us. Please login </p>

                  <div class="text-line2"><span class="email-label"></span>
                     <a class="link" onclick="change_email();">Change</a>
                   </div>

                     <div class="inner">
                  <input type="password" name="cart_password" id="cart_password" class="form-control" placeholder="ENTER PASSWORD">
                  <button type="submit" class="scbtn scbtn-primary" onclick="cart_login();" ><span>Continue</span></button>
                <!--  <span class="or-wrp">OR</span>-->
                    <div class="hr-or"><i></i><span>OR</span><i></i></div>
                  <button type="submit" class="scbtn scbtn-tertiary" onclick="checkout_as_guest();" ><span>Checkout as Guest</span></button>
                </div>

                  <div class="message error hide" id="login_error" ></div>
                </div>

              </div>
            </div>
          </div>
          <!-- //.panel -->

          <!-- panel -->
     <div class="box-checkout box-checkout-address <?php echo ($this->session->userdata('user_id')!='') ? 'open step-done' : ''; ?>" id="div-checkout-address">
          <h2 class="box-title">2. SHIPPING & BILLING ADDRESSES    <span class="btn-edit"><i class="icon-edit"></i></span></h2>

           <div class="box-checkout-summary address" id="checkout-address-summary" >
             <div class="row" id="final-addresses">
               <?php  if(@$_COOKIE['shipping_address']!='' &&  @$_COOKIE['billing_address']!=''){ ?>
                <div class="col-md-6">
                    <div class="box-address">
                      <div class="text-label">
                      Shipping Address
                      </div>
                      <div id="shipping-address">
                          <div><?php echo trim(@$_COOKIE['shipping_first_name']).' '.trim(@$_COOKIE['shipping_last_name']); ?></div>
                          <div><?php echo trim(@$_COOKIE['shipping_address']); ?>
                          <?php echo trim(@$_COOKIE['shipping_city']); ?>,
                          <?php echo trim(@$_COOKIE['shipping_state']); ?>,
                          <?php echo trim(@$_COOKIE['stylecracker_shipping_pincode']); ?>,
                           <?php echo trim(@$_COOKIE['shipping_mobile']); ?>
                          </div>
                      </div>
                      <span class="btn-edit hide"><i class="icon-edit"></i></span>
                    </div>
                </div>
                <div class="col-md-6">
                  <div class="box-address">
                    <div class="text-label">
                      Billing Address
                    </div>
                    <div id="billing-address">
                            <div><?php echo trim(@$_COOKIE['billing_first_name']).' '.trim(@$_COOKIE['billing_last_name']); ?></div>
                            <div><?php echo trim(@$_COOKIE['billing_address']); ?>
                            <?php echo trim(@$_COOKIE['billing_city']); ?>,
                            <?php echo trim(@$_COOKIE['billing_state']); ?>,
                            <?php echo trim(@$_COOKIE['stylecracker_billing_pincode']); ?>,
                             <?php echo trim(@$_COOKIE['billing_mobile']); ?>
                            </div>
                    </div>
                    <span class="btn-edit hide"><i class="icon-edit"></i></span>
                  </div>
                </div>
                <?php
                   }else{ }
              ?>
              </div>
            </div>

            <div class="box-checkout-body">
              <div class="box-checkout-content" style="padding-top:20px;">
                <div class="box-checkout-summary111 address hide">
                  <div class="address-selected-wrp">
                  <div class="row">
                  <?php  if(@$_COOKIE['shipping_address']!=''){ ?>
                    <div class="col-md-6">
                        <div class="box-address">
                          <div class="text-label">
                          Shipping Address
                          </div>
                          <div>
                                <div><?php echo trim(@$_COOKIE['shipping_first_name']).' '.trim(@$_COOKIE['shipping_last_name']); ?></div>
                                <div><?php echo trim(@$_COOKIE['shipping_address']); ?>
                                <?php echo trim(@$_COOKIE['shipping_city']); ?>,
                                <?php echo trim(@$_COOKIE['shipping_state']); ?>,
                                <?php echo trim(@$_COOKIE['stylecracker_shipping_pincode']); ?></div>
                          </div>
                          <span class="btn-edit hide"><i class="icon-edit"></i></span>
                        </div>
                    </div>
                   <?php }else{ //$style_panel = 'display:block;';
                            }  ?>

                  <?php  if(@$_COOKIE['billing_address']!=''){ ?>
                    <div class="col-md-6">
                      <div class="box-address">
                        <div class="text-label">
                          Billing Address
                      </div>
                      <div>
                            <div><?php echo trim(@$_COOKIE['billing_first_name']).' '.trim(@$_COOKIE['billing_last_name']); ?></div>
                            <div><?php echo trim(@$_COOKIE['billing_address']); ?>
                            <?php echo trim(@$_COOKIE['billing_city']); ?>,
                            <?php echo trim(@$_COOKIE['billing_state']); ?>,
                            <?php echo trim(@$_COOKIE['stylecracker_billing_pincode']); ?></div>
                      </div>
                      <span class="btn-edit hide"><i class="icon-edit"></i></span>
                    </div>
                  </div>
                <?php }else{ //$style_panel = 'display:block;';
                            }  ?>
                </div>
              </div>
              </div>

              <div class="box-address-list">
                <?php if($this->session->userdata('user_id')!='')
                      {
                ?>
                  <ul id="user-exist-address">
                  <?php if(!empty($this->data['user_existing_address'])){  ?>
                  <?php $ad=1;  foreach($this->data['user_existing_address'] as $uadd){
                          $first_name = $uadd['first_name'];
                          $last_name = $uadd['last_name'];
                          $mobile_no = $uadd['mobile_no'];
                          $shipping_address = $uadd['shipping_address'];
                          $pincode = $uadd['pincode'];
                          $city_name = $uadd['city_name'];
                          $states_name = $uadd['state_id'];

                          if($uadd['is_default'] == 1) {
                          $_COOKIE['billing_first_name'] = $first_name;
                          $_COOKIE['billing_last_name'] = $last_name;
                          $_COOKIE['billing_mobile_no'] = $mobile_no;
                          $_COOKIE['billing_address'] = $shipping_address;
                          $_COOKIE['shipping_pincode_no'] = $pincode;
                          $_COOKIE['billing_city'] = $city_name;
                          $_COOKIE['billing_state'] = $states_name;

                          $_COOKIE['shipping_first_name'] = $first_name;
                          $_COOKIE['shipping_last_name'] = $last_name;
                          $_COOKIE['shipping_mobile_no'] = $mobile_no;
                          $_COOKIE['shipping_address'] = $shipping_address;
                          $_COOKIE['stylecracker_shipping_pincode'] = $pincode;
                          $_COOKIE['shipping_city'] = $city_name;
                          $_COOKIE['shipping_state'] = $states_name;
                        }
                      ?>
                    <li class="<?php echo ($uadd['is_default'] == 1) ? 'active' : '';?> user_address_<?php echo $uadd['id']; ?>" id="user_address_<?php echo $uadd['id']; ?>"  attr-state="<?php echo (int)$uadd['state_id']; ?>" >
                      <div class="title-3">
                        <?php echo trim($uadd['first_name']).' '.$uadd['last_name']; ?>
                      </div>
                      <div class="desc-address">
                          <?php echo trim($uadd['shipping_address']); ?>
                      </div>
                      <div class="desc">
                          <?php echo trim($uadd['city_name']); ?>,
                          <?php echo trim($uadd['states_name']); ?>,
                          <?php echo trim($uadd['pincode']); ?>,
                           <?php echo trim($uadd['mobile_no']); ?>
                      </div>
                      <a class="close111 btn-remove"  onclick="delete_user_address(<?php echo $uadd['id']; ?>);" ><i class="icon-close" ></i></a>
                      <a class="btn-edit" href="#modal-address" data-toggle="modal" onclick="editAddress(this,<?php echo $uadd['id']; ?>);" attr-value="<?php echo $uadd['id']; ?>" ><i class="icon-edit"></i></a>

                      <div class="btns-wrp">
                        <label class="sc-checkbox ship-checkbox <?php echo (isset($_COOKIE['shipping_id']) && ($_COOKIE['shipping_id']==$uadd['id'])) ? 'active' : ''; ?>"  attr-value="<?php echo $uadd['id']; ?>"><i class="icon icon-diamond"></i><span>Ship Here</span></label>
                        <label class="sc-checkbox bill-checkbox <?php echo (isset($_COOKIE['billing_id']) && ($_COOKIE['billing_id']==$uadd['id'])) ? 'active' : ''; ?>" attr-value="<?php echo $uadd['id']; ?>"><i class="icon icon-diamond"></i><span>Bill Here</span></label>
                      </div>
                    </li>
                    <?php } ?>
                  </ul>

                <?php } ?>
                <?php }else
                     { ?>

                        <ul id="user-exist-address">

                        </ul>

                <?php } ?>
                </div>
                <a class="scbtn scbtn-tertiary btn-add-address111" href="#modal-address" onclick="clearModalData()"  data-toggle="modal"><span>Add New Address</span></a>
              </div>

            <div class=" box-checkout-footer">
              <div class="ship-bill-address message error hide">Select Shipping and Billing Address</div>
              <div class="btns-wrp">

                <button type="submit" class="scbtn scbtn-primary" onclick="showOrderSummary();"><span>Proceed</span></button>
              </div>
            </div>
        </div>
     </div>

          <!-- //.panel -->
          <!--  panel -->
          <div class="box-checkout box-checkout-overview " id="desktop_order_summary">
            <h2 class="box-title">3. Order Summary <span class="btn-edit"><i class="icon-edit"></i></span></h2>
            <div class="box-checkout-summary">
              <div>
                <span class="text-label">Payble Amount:</span> <i class="fa fa-inr"></i> <span id="payAmt" class="price" ></span>
              </div>
            </div>
               <form name="sccart_frm" id="sccart_frm" method="post">
               <input type="hidden" id="page-type" name="page-type" value="checkout" />
            <div class="box-checkout-body" id="sccart_product_view">
              <div class="box-checkout-content ">
                <!-- cart-->
                <div class="cart-box-wrp">
                  <div class="panel-wrp">
                    <ul class="cart-items">
                      <li class="cart-items-title">
                        <div class="row">
                          <div class="col-md-2 col-sm-2 col-xs-2 ">
                          </div>
                          <div class="col-md-10 col-sm-10 col-xs-10">
                            <div class="row">
                              <div class="col-md-5 col-sm-6 col-xs-6 text-left">
                                Item
                              </div>
                              <div class="col-md-2 col-sm-2 col-xs-2 ">
                                Size
                              </div>
                              <div class="col-md-2 col-sm-2 col-xs-2 ">
                                Quantity
                              </div>
                              <div class="col-md-3 col-sm-2 col-xs-2">
                                Price
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                    <!-- Order data through Ajax-->

                    <?php $c = 0; foreach($cart_info as $val){
                    if($c!=0){ $ga_product_id=$ga_product_id.','; }
                    $ga_product_id=$ga_product_id.$val['product_id'];
                    $qty = $qty+$val['product_qty']; ?>
                      <li id="cart_<?php echo $val['id']; ?>">
                        <div class="row">
                          <div class="col-md-2 col-sm-2 col-xs-2 ">
                            <div class="cart-item-img img-wrp">
                               <?php 
                                 if(in_array($val['user_id'],$imagify_brandid))
                                {
                                  $prod_img_url = $this->config->item('products_small').$val['image'];
                                }else{
                                  $prod_img_url = $this->config->item('products_310').$val['image'];
                                }
                              ?>
                              <img src="<?php echo $prod_img_url; ?>" />
                            </div>
                          </div>
                          <div class="col-md-10 col-sm-10 col-xs-10">
                            <div class="cart-row-wrp">
                              <div class="row">
                                <div class="col-md-5 col-sm-6 col-xs-12">
                                  <div class="cart-item-name">
                                     <a href="<?php echo base_url(); ?>product/details/<?php echo $val['slug']; ?>-<?php echo $val['product_id']; ?>" target="_blank"><?php echo $val['name']; ?></a>
                                    <?php //echo $val['name']; ?>
                                      <span class="remove-item ic ic-close sm-only" data-productname="<?php echo $val['name']; ?>" data-productprice="<?php echo $val['discount_price']; ?>" data-brandname="<?php echo $val['company_name']; ?>"  onclick="scremovecart(<?php echo $val['id']; ?>,this);" ><i class="icon-close" ></i></span>
                                  </div>
                                  <div class="cart-item-seller">
                                    <span class="text-label">Seller:</span><a target="_blank" href="<?php echo base_url(); ?>brands/<?php echo $val['company_name'].'-'.$val['user_id']; ?>"> <?php echo $val['company_name']; ?> </a> <?php //echo $val['company_name']; ?>
                                  </div>
                                  <div class="return-policy-wrp md-only">
                                    <?php if(trim($val['return_policy'])!=''){ ?>
                                        <a href="<?php echo base_url();?>return-policy" target="_blank" class="return-policy"> view return policy</a>
                                    <?php } ?>
                                     <?php if($pincode!='' && strchr($val['cod_exc_location'],$pincode)!='' || $val['cod_available'] != 1){  ?>
                                       <div class="delivery-status1 cod-status1 message error">COD is not available</div>
                                       <?php $cod_error = $cod_error + 1; } ?>
                                       <?php if($pincode!='' && strchr($val['shipping_exc_location'],$pincode)!=''){ ?>
                                       <?php $shina_error = $shina_error + 1; ?>
                                       <div class="delivery-status1 cod-status1 message error">Shipping is not available</div>
                                    <?php } ?>
                                     <?php if($val['in_stock'] == 1) { ?>
                                        <div class="message error">
                                          Product Is Out of Stock
                                        </div>
                                    <?php  $stock =  $stock + 1; }  ?>
                                  </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-5">
                                  <div class="cart-item-size">
                                    <span class="text-label sm-only">Size:</span>
                                    <?php echo $val['size_text']; ?>
                                      <input type="hidden" name="scsize<?php echo $val['id']; ?>" id="scsize<?php echo $val['id']; ?>"  value="<?php echo $val['product_size']; ?>"  class="form-control input-sm input-qty qty-value"  readonly />
                                  </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-7">
                                  <?php if($val['in_stock'] != 1) { ?>
                                    <div class="qty-wrp-outer">
                                  <span class="text-label sm-only">Qty:</span>
                                    <div class="qty-wrp">
                                        <button class="btn-qty qtyminus" field='quantity' onclick="change_qty('min',<?php echo $val['id']; ?>);" ><i  class="icon-minus"></i></button>
                                        <input type="text" name="quantity<?php echo $val['id']; ?>"  id="quantity<?php echo $val['id']; ?>"  value="<?php echo $val['product_qty']; ?>"  class="form-control input-sm input-qty qty-value"   readonly />
                                        <button class="btn-qty qtyplus" field='quantity' onclick="change_qty('plus',<?php echo $val['id']; ?>);" ><i class="icon-add"></i></button>
                                    </div>
                                    </div>
                                   <?php } ?>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                  <div class="item-price">
                                      <?php if($val['price'] == $val['discount_price']){
                                        $product_price = $product_price + $val['price'];
                                        $brandwise_cost[$val['user_id']][] =  $val['price'];
                                      ?>
                                      <div class="price"><i class="fa fa-inr"></i> <?php echo number_format((float)$val['price'], 2, '.', ''); ?></div>
                                      <?php }else{
                                        $product_price = $product_price + $val['discount_price'];
                                        $brandwise_cost[$val['user_id']][] =  $val['discount_price'];
                                       ?>
                                      <div class="price"><i class="fa fa-inr"></i> <?php echo number_format((float)$val['discount_price'], 2, '.', ''); ?></div>
                                      <!--<div class="mrp-price"><i class="fa fa-inr"></i> --><?php //echo number_format((float)$val['price'], 2, '.', ''); ?><!--</div>-->
                                      <?php } ?>
                                      <?php if($val['discount_percent'] > 0) { ?>
                                      <!--<div class="discount-price">(<?php echo round($val['discount_percent']); ?>% OFF)</div>-->
                                      <?php } ?>

                                      <span class="remove-item ic ic-close md-only" data-productname="<?php echo $val['name']; ?>" data-productprice="<?php echo $val['discount_price']; ?>" data-brandname="<?php echo $val['company_name']; ?>"  onclick="scremovecart(<?php echo $val['id']; ?>,this);"><i class="icon-close" ></i></span>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="return-policy-wrp sm-only">
                              <?php if(trim($val['return_policy'])!=''){ ?>
                                  <a href="<?php echo base_url();?>return-policy" target="_blank" class="return-policy"> view return policy</a>
                              <?php } ?>
                              <?php if($pincode!='' && strchr($val['cod_exc_location'],$pincode)!='' || $val['cod_available'] != 1){  ?>
                               <div class="delivery-status1 cod-status1 message error">COD is not available</div>
                               <?php $cod_error = $cod_error + 1; } ?>
                               <?php if($pincode!='' && strchr($val['shipping_exc_location'],$pincode)!=''){ ?>
                               <?php $shina_error = $shina_error + 1; ?>
                               <div class="delivery-status1 cod-status1 message error">Shipping is not available</div>
                              <?php } ?>
                              <?php if($val['in_stock'] == 1) { ?>
                                  <div class="message error">
                                    Product Is Out of Stock
                                  </div>
                              <?php  $stock =  $stock + 1; }  ?>
                            </div>
                          </div>
                        </div>
                      </li>
                      <?php $c++; ?>
                        <input type="hidden" name="scitem<?php echo $c; ?>" id="scitem<?php echo $c; ?>" value="<?php echo $val['id']; ?>">
                      <?php } ?>
                       <!-- End of Order data through Ajax-->

                    </ul>
  </div>
                  </div>
                  <!--total-->
                  <div class="row">
                    <div class="col-md-4">
                    <!--

                      <div class="box-coupon-wrp">
                        <div class="form-group">
                         <input type="text" class="form-control input-sm" name="sccoupon_code" id="sccoupon_code" placeholder="Enter Discount Coupon" value="<?php echo @$_COOKIE['discountcoupon_stylecracker']; ?>"/>

                           <span class="btn-link <?php if(@$_COOKIE['discountcoupon_stylecracker']=='') { ?>hide<?php } ?>" id="coupon_clear" onclick="clear_coupon_code();">Remove Coupon</span>
                         </div>
                          <button class="scbtn scbtn-tertiary" id="coupon-btn" onclick="apply_coupon_code('apply');"><span>Apply</span></button>

                        <?php if(@$coupon_code_msg['msg_type'] == 1){ ?>
                        <div id="coupon_error" class="message success"><?php echo @$coupon_code_msg['msg']; ?></div>
                        <?php }else{ ?>
                        <div id="coupon_error" class="message error"><?php echo @$coupon_code_msg['msg']; ?></div>
                        <?php } ?>
                      </div>-->

                      <div class="offers-section">
                        <a class="scbtn scbtn-tertiary" href="#modal-offers" data-toggle="modal" onclick="get_available_offers();"><span>Avail Offers</span></a>
                      </div>                  


                    </div>
                      <div class="col-md-2">  </div>
                    <div class="col-md-6">
                      <div class="box-cart-total">
                        <ul>
                          <li>
                            <span class="text-label">Total</span>
                            <span class="value cart-price"> <span class="fa fa-inr"></span> <?php echo $product_price = number_format((float)$product_price, 2, '.', ''); ?></span>
                          </li>
                            <?php if(!empty($brandwise_cost)) {
                          foreach($brandwise_cost as $key=>$v){
                                $brand_tax_cal =  array_sum($brandwise_cost[$key]);
                                $storetax = $this->cartnew_model->getBrandExtraInfo($key)[0]['store_tax'];
                                $total_tax = $total_tax + $brand_tax_cal*($storetax/100);

                                /* COD charges start*/
                              if($this->cartnew_model->getBrandExtraInfo($key)[0]['is_cod'] == 1 && @$sc_cart_pay_mode=='cod'){
                                  if($this->cartnew_model->getBrandExtraInfo($key)[0]['cod_min_value'] == 0 && $this->cartnew_model->getBrandExtraInfo($key)[0]['code_max_value'] == 0){

                                    $cod_charges = $cod_charges + $this->cartnew_model->getBrandExtraInfo($key)[0]['cod_charges'];

                                  }else if($this->cartnew_model->getBrandExtraInfo($key)[0]['cod_min_value'] >= 0 && $this->cartnew_model->getBrandExtraInfo($key)[0]['code_max_value'] > 0 && $this->cartnew_model->getBrandExtraInfo($key)[0]['cod_min_value']<=$brand_tax_cal && $this->cartnew_model->getBrandExtraInfo($key)[0]['code_max_value']>=$brand_tax_cal){

                                    $cod_charges = $cod_charges + $this->cartnew_model->getBrandExtraInfo($key)[0]['cod_charges'];
                                  }
                              }

                              /* Shipping charges start*/
                              if($this->cartnew_model->getBrandExtraInfo($key)[0]['is_shipping'] == 1){
                                  if($this->cartnew_model->getBrandExtraInfo($key)[0]['shipping_min_values'] == 0 && $this->cartnew_model->getBrandExtraInfo($key)[0]['shipping_max_values'] == 0){

                                    $shipping_charges = $shipping_charges + $this->cartnew_model->getBrandExtraInfo($key)[0]['shipping_charges'];

                                  }else if($this->cartnew_model->getBrandExtraInfo($key)[0]['shipping_min_values'] >= 0 && $this->cartnew_model->getBrandExtraInfo($key)[0]['shipping_max_values'] > 0 && $this->cartnew_model->getBrandExtraInfo($key)[0]['shipping_min_values']<=$brand_tax_cal && $this->cartnew_model->getBrandExtraInfo($key)[0]['shipping_max_values']>=$brand_tax_cal){

                                    $shipping_charges = $shipping_charges + $this->cartnew_model->getBrandExtraInfo($key)[0]['shipping_charges'];
                                  }
                              }
                              }
                          } ?>
                           <?php if(@$coupon_code_msg['coupon_code'] != '' ) { ?>
                             <?php if(in_array($coupon_code_msg['coupon_code'],$offer_codes) && !in_array($coupon_code_msg['coupon_code'],$scboxoffer_codes)) { 
                                 if(in_array($coupon_code_msg['coupon_code'],array(0=>'BMS500',1=>'BMS15')))
                                  {
                                    $couponOfferMsg = 'BMS Wallet (Virtual Visa Card)';
                                  }else
                                  {
                                    $couponOfferMsg = 'YES BANK Credit/Debit Card';
                                  }

                              ?>
                              <li>
                              <div class="discount-message">                     
                                  <span class="text-label">Coupon Discount </span>
                                  <!--<span class="discount-name"> 
                                  <i class="fa fa-inr"></i> <?php echo $coupon_discount = number_format((float)$coupon_code_msg['coupon_discount_amount'], 2, '.', ''); ?>  discount applicable only on payment through <?php echo $couponOfferMsg; ?>
                                  </span>-->                         
                                   <span class="discount-name">(<?php echo @$coupon_code_msg['coupon_code']; ?>)</span>   
                                   <span class="value cart-price"> -  <span class="fa fa-inr"></span> <?php echo $coupon_discount = number_format((float)$coupon_code_msg['coupon_discount_amount'], 2, '.', ''); ?> </span>
                                    <div class="text-danger">This offer is applicable only if the final payment is made through <?php echo $couponOfferMsg; ?>
                                        
                                        &nbsp;<a  title="" data-toggle="popover" data-placement="bottom" data-html="true" data-content="<div class='box-popover'>
                                    <p>The discount will be deducted and reflected at the payment gateway page.</p>
                                    Click on Checkout > Online payment > Enter card details > Check offer </p>
                                    <p>A green notification will appear stating your debit/credit card is eligible for the discount.</p>
                                    <p>Click on Pay now, discount will reflect and payment will be processed.</p> </div>"><span class="fa fa-info-circle"></span></a>                                    
                                    </div>
                                
                                   <span class="btn-link " id="coupon_clear" onclick="clear_coupon_code();"><i class="icon-close" ></i></span>
                                  </div>
                                </li>
                              <?php }else{ 
                                  if(!in_array($coupon_code_msg['coupon_code'],$scboxoffer_codes))
                                  {
                                ?>
                                <li>
                                 <div class="discount-message"> 
                                  <span class="text-label">Coupon Discount </span> 
                                  <span class="discount-name">(<?php echo @$coupon_code_msg['coupon_code']; ?>)</span>
                                  
                                  <span class="value cart-price"> -  <span class="fa fa-inr"></span> <?php echo $coupon_discount = number_format((float)$coupon_code_msg['coupon_discount_amount'], 2, '.', ''); ?> </span>

                                  <span class="btn-link " id="coupon_clear" onclick="clear_coupon_code();"><i class="icon-close" ></i></span>
                                   </div>

                                </li>
                          <?php } } } ?>
                          <?php if(@$total_tax != '' && $total_tax > 0 ) { ?>
                          <li>
                            <span class="text-label">Taxes</span>
                            <span class="value cart-price"> <span class="fa fa-inr"></span> <?php echo $total_tax = number_format((float)$total_tax, 2, '.', ''); ?></span>
                          </li>
                          <?php } ?>
                          <?php if($referral_point > 0 && ($product_price-$coupon_discount)>=MIN_CART_VALUE  && !in_array($coupon_code_msg['coupon_code'],$offer_codes)) { ?>
                          <li>
                            <span class="text-label">
                             <label class="sc-checkbox redeem <?php echo (isset($_COOKIE['is_redeem_point'])==true && $_COOKIE['is_redeem_point'] == 1) ? 'active' : ''; ?>" name="redeem" id="redeem" onclick="redeem_point_cart(this)" >
                                 <i class="icon icon-diamond"></i>
                             <?php echo REDEEM_POINTS_CART_TEXT; ?></label>
                           </span>

                            <span class="value price" id="referral-price" > - <span class="fa fa-inr"></span> <?php echo $sreferral_point = number_format((float)$referral_point, 2, '.', ''); ?> </span>
                          <?php if(isset($_COOKIE['is_redeem_point'])==true && $_COOKIE['is_redeem_point'] == 1){
                              $sreferral_point = $sreferral_point;
                            }else{ $sreferral_point = 0; }
                            ?>
                          </li>
                          <?php } ?>

                           <?php if($cod_charges > 0) { ?>
                          <li class="order-charges order-tax">
                            <span class="text-label">COD Charges</span>
                            <span class="value cart-price"> <span class="fa fa-inr"></span> <?php echo $cod_charges  = number_format((float)$cod_charges, 2, '.', ''); ?></span>
                          </li>
                          <?php } ?>

                          <?php if($shipping_charges > 0) { ?>
                          <li class="order-charges order-tax">
                            <span class="text-label">Shipping Charges</span>
                            <span class="value cart-price"> <span class="fa fa-inr"></span> <?php echo $shipping_charges  = number_format((float)$shipping_charges, 2, '.', ''); ?></span>
                          </li>
                          <?php } ?>

                          <?php $order_total = ($product_price + $total_tax) - $coupon_discount - $sreferral_point; ?>
                           <?php $cart_total = $product_price + $total_tax + $shipping_charges + $cod_charges - $coupon_discount - $sreferral_point; ?>
                          <li>
                            <span class="text-label text-right">Total:</span>
                            <span class="value sc-color cart-price"> <span class="fa fa-inr"></span> <?php echo number_format((float)($cart_total), 2, '.', ''); ?></span>
                          </li>
                    </ul>
                      <!--     </div>
                    </div> -->
                  </div>
                  <!--/total-->
                </div>
                <!-- -->
              </div>
                </div>

              <div class="box-checkout-footer">
                <div class="btns-wrp">
                  <!-- <button class="scbtn scbtn-primary hide" id="scupdatecart1" disabled onclick="sc_updatecart(0);" ><span>Update Cart</span></button> -->
                  <a class="scbtn scbtn-primary" onclick="proceed_to_pay();" id="proceed_to_checkout1" ><span>Make Payment</span></a>
                </div>
                  <input type="hidden" name="item_count" id="item_count" value="<?php echo $qty; ?>">
                  <input type="hidden" name="stock_exist" id="stock_exist" value="<?php echo $stock; ?>">
                  <input type="hidden" name="sc_cart_pay_mode" id="sc_cart_pay_mode" value="card"  >
                  <input name="cart_total" id="cart_total" type="hidden" value="<?php echo $cart_total; ?>">
                   <input name="codna" id="codna" type="hidden" value="<?php echo $cod_error; ?>">
                  <input name="shina" id="shina" type="hidden" value="<?php echo $shina_error; ?>">
                  <input name="product_pricess" id="product_pricess" type="hidden" value="<?php echo $product_price; ?>">

              </div>
                </div>
             
          </form>

          </div>

          <!-- //.panel -->

          <!--  panel -->
          <div class="box-checkout box-checkout-payment" id="select_paymode">
            <h2 class="box-title">4. Payment Details <span class="btn-edit"><i class="icon-edit"></i></span></h2>
            <div class="box-checkout-body">
              <div class="box-checkout-content">
                <div class="box-payment-options">
                  <ul class="">
                      <!--<li class="paymode active" attr-value="card" >-->
                      <li >
                      <label class="sc-checkbox paymode active" attr-value="card" id="card" onclick="pay_valiation(this);" ><i class="icon icon-diamond"></i><span> Online Payment</span></label>
                        <!--<label>
                          <span class="ic" onclick="pay_validation();" ><i class="icon-diamond"  ></i></span> Online Payment
                        </label>-->
                      </li>
                      <!--<li class="paymode" attr-value="cod">-->
                      <li>
                      <label class="sc-checkbox paymode" attr-value="cod" id="cod" onclick="pay_valiation(this);" ><i class="icon icon-diamond"></i><span> Cash On Delivery</span></label>
                        <!--<label>
                          <span class="ic" onclick="pay_validation();" ><i class="icon-diamond"  ></i></span> Cash On Delivery
                        </label>-->
                      </li>
                  </ul>
                  <div class="error error-msg hide message error" id="cart_place_order_msg"></div>
                    <div class="error-msg hide message error" id="cart_error_msg">Sorry Shipping is not avaliable for some products</div>
  </div>

              </div>

              <div class="box-checkout-footer">

                <div class="btns-wrp">
                 <!--  <button type="submit" class="scbtn scbtn-primary"><span>Place Order</span></button> -->
                  <button class="scbtn scbtn-primary" id="place_order_sc_cart" name="place_order_sc_cart" onclick="place_stylecracker_order();"><span>Place Order</span></button>
                </div>
              </div>
            </div>
          </div>
            <!-- //.panel -->


        </div>

      </div>
    </div>  <!-- //.row -->
    </div>
</section>


<!-- popup -->
<div id="modal-address" class="modal fade modal-address" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" id="close_address"  data-dismiss="modal"><i class="icon-close"></i></button>

        <div class="box-add-address">
          <div class="title-section">
            <h1 class="title" id="userdata-modal-title">Add Address</h1>
          </div>
          <div class="box-inner">
            <input type="hidden" name="user_address_id" id="user_address_id" value="" >
            <div class="form-group">
              <input type="text" name="user_name" id="user_name" class="form-control" required maxlength="100" placeholder="NAME" onblur="valiate_shipping_address(this);" >
              <div id="user_name_error" class="message text-error hide"></div>
            </div>
            <div class="form-group">
              <input type="tel" name="user_mobile_no" id="user_mobile_no" class="form-control" required maxlength="10" placeholder="MOBILE NUMBER" onblur="valiate_shipping_address(this);" onkeypress="return isNumber(event);" >
              <div id="user_mobile_no_error" class="message text-error hide" ></div>
            </div>
            <div class="form-group">
              <textarea name="user_address" id="user_address" class="form-control" maxlength="500" required placeholder="ADDRESS" onblur="valiate_shipping_address(this);"></textarea>
              <div id="user_address_error" class="message text-error hide"></div>
            </div>
            <div class="form-group">
              <input type="tel" name="user_pincode_no" id="user_pincode_no" class="form-control" required maxlength="6" placeholder="PINCODE" onblur="valiate_shipping_address(this);" onkeypress="return isNumber(event);" >
              <div id="user_pincode_no_error" class="message text-error hide"></div>
            </div>
            <div class="form-group">
              <input type="text" name="user_city" id="user_city" class="form-control" required maxlength="100" placeholder="CITY" onblur="valiate_shipping_address(this);" disabled="true" >
              <div id="user_city_error" class="message text-error hide"></div>
            </div>
            <div class="form-group">
              <select name="user_state" id="user_state" class="form-control" onblur="valiate_shipping_address(this);" disabled="true">
                  <option value="">Select State</option>
                   <?php if(!empty($this->data['all_uc_states'])){
                    foreach($this->data['all_uc_states'] as $state){
                      if(isset($_COOKIE['shipping_state']) && ($_COOKIE['shipping_state']==$state['id'] || $states_name==$state['id'])){
                        echo '<option value="'.(int)$state['id'].'" selected>'.$state['state_name'].'</option>';
                      }else{
                        echo '<option value="'.(int)$state['id'].'">'.$state['state_name'].'</option>';
                      }
                    }
                  } ?>
                </select>
            <div id="user_state_error" class="message text-error hide"></div>
           <div class="error error-msg hide" id="cart_place_order_msg"></div>
              <div class="error-msg hide message error" id="cart_error_msg">Sorry Shipping is not avaliable for some products</div>
            </div>
            <!-- <label class="sc-checkbox111 active"><input id="allow_same_shipp" name="allow_same_shipp" type="checkbox" class="check-billing-address1" checked/>Use this for my billing address</label> -->
            <div class="default-checkbox-wrp" id="default-checkbox">
              <label class="sc-checkbox default-address <?php if(isset($_COOKIE['shipping_id']) && is_numeric($_COOKIE['shipping_id'])==true){ echo ''; }else{ echo 'active'; } ?>" id="DShip_address"><i class="icon icon-diamond"></i><span>Default Shipping Address</span></label>

              <label class="sc-checkbox default-address <?php if(isset($_COOKIE['billing_id']) && is_numeric($_COOKIE['shipping_id'])==true ){ echo ''; }else{ echo 'active'; } ?>" id="DBill_address"><i class="icon icon-diamond"></i><span>Default Billing Address</span></label>
            </div>
          </div>
          <div class="btns-wrp">
            <!-- <button class="scbtn scbtn-primary" onclick="add_new_address();" ><span>Save</span></button> -->
            <a class="scbtn scbtn-primary" onclick="add_new_address(this);" attr-addressid=""><span>Save</span></a>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<!-- //. popup -->

<!-- Shipping address -->

  <input id="cart_pincode" class="form-control input-sm cart-pincode" placeholder="Pincode" type="hidden" name="cart_pincode" maxlength="6" value="<?php if(isset($_COOKIE['stylecracker_shipping_pincode'])){ echo $_COOKIE['stylecracker_shipping_pincode']; }else{ echo $this->session->userdata('pincode'); } ?>">
  <input type="hidden" name="shipping_first_name" id="shipping_first_name" value="<?php if(isset($_COOKIE['shipping_first_name'])){ echo $_COOKIE['shipping_first_name']; }else{ echo $first_name; } ?>">
  <input type="hidden" name="shipping_last_name" id="shipping_last_name" value="<?php if(isset($_COOKIE['shipping_last_name'])){ echo $_COOKIE['shipping_last_name']; }else{ echo $last_name; } ?>">
  <input type="hidden" maxlength="10"  name="shipping_mobile_no" id="shipping_mobile_no" value="<?php if(isset($_COOKIE['shipping_mobile_no'])){ echo $_COOKIE['shipping_mobile_no']; }else{ echo $mobile_no; } ?>">
  <input type="hidden" maxlength="6"  name="shipping_pincode_no" id="shipping_pincode_no" value="<?php echo @$_COOKIE['stylecracker_shipping_pincode']; ?>" onblur="check_pincode_shipping();">
  <input type="hidden" class="form-control" placeholder="Address" name="shipping_address" id="shipping_address" value="<?php if(isset($_COOKIE['shipping_address'])){ echo $_COOKIE['shipping_address']; }else{ echo $shipping_address; } ?>" >
  <input type="hidden" class="form-control" placeholder="City"  name="shipping_city" id="shipping_city"  value="<?php if(isset($_COOKIE['shipping_city'])){ echo $_COOKIE['shipping_city']; }else{ echo $city_name; } ?>">
  <input type="hidden" class="form-control" placeholder="State" name="shipping_state" id="shipping_state" value="<?php if(isset($_COOKIE['billing_state'])){ echo $_COOKIE['billing_state']; }else{ echo @$state_name; } ?>">
 <!-- // Shipping address -->

 <!--  billing address -->
 <input type="hidden" class="form-control" placeholder="First Name" name="billing_first_name" id="billing_first_name" value="<?php if(isset($_COOKIE['billing_first_name'])){ echo $_COOKIE['billing_first_name']; }else{ echo $first_name; } ?>">
 <input type="hidden" class="form-control" placeholder="Last Name" name="billing_last_name" id="billing_last_name" value="<?php if(isset($_COOKIE['billing_last_name'])){ echo $_COOKIE['billing_last_name']; }else{ echo $last_name; } ?>">
 <input type="hidden" maxlength="10" class="form-control" placeholder="Mobile" name="billing_mobile_no" id="billing_mobile_no" value="<?php if(isset($_COOKIE['billing_mobile_no'])){ echo $_COOKIE['billing_mobile_no']; }else{ echo $mobile_no; } ?>">
 <input type="hidden" maxlength="6" class="form-control" placeholder="Area Pincode" name="billing_pincode_no" id="billing_pincode_no" value="<?php if(isset($_COOKIE['stylecracker_shipping_pincode'])){ echo $_COOKIE['stylecracker_shipping_pincode']; }else{ echo $pincode; } ?>">
 <input type="hidden"  class="form-control" placeholder="Address" name="billing_address" id="billing_address" value="<?php if(isset($_COOKIE['billing_address'])){ echo $_COOKIE['billing_address']; }else{ echo $shipping_address; } ?>" >
 <input type="hidden" class="form-control" placeholder="City" name="billing_city" id="billing_city" value="<?php if(isset($_COOKIE['billing_city'])){ echo $_COOKIE['billing_city']; }else{ echo $city_name; } ?>">
 <input type="hidden" class="form-control" placeholder="State" name="billing_state" id="billing_state" value="<?php if(isset($_COOKIE['billing_state'])){ echo $_COOKIE['billing_state']; }else{ echo @$state_name; } ?>">
<!-- // billing address -->
<input type="hidden"  name="sess_userid" id="sess_userid" value="<?php echo ($this->session->userdata('user_id')!='') ? $this->session->userdata('user_id') : 0; ?>">

<form action="<?php echo PAYMENT_URL; ?>" method="post" name="stylecracker_style">
  <input type="hidden" name="key" value="<?php echo MERCHANT_KEY; ?>" />
  <input type="hidden" name="productinfo" value='{"paymentParts":[{"name":"splitID1","value":"6","merchantId":"396446","description":"test description","commission":"2"}]}' />
  <input type="hidden" name="hash" id="hash" value=""/>
  <input type="hidden" name="txnid" id="txnid" value="" />
  <input type="hidden" name="amount" id="amount" value="" />
  <input type="hidden" name="surl" id="surl" value="<?php echo SURL; ?>" />
  <!--input type="hidden" name="service_provider" id="service_provider" value="payu_paisa" /-->
  <input type="hidden" name="firstname" id="firstname" value="" />
  <input type="hidden" name="email" id="email" value="" />
  <input type="hidden" name="phone" id="phone" value="" />
  <input type="hidden" name="udf1" id="udf1" value="1,2,3" />
  <input type="hidden" name="udf2" id="udf2" value="" />
  <input type="hidden" name="udf3" id="udf3" value="" />
  <input type="hidden" name="udf4" id="udf4" value="" />
  <input type="hidden" name="udf5" id="udf5" value="" />
 <input type="hidden" name="offer_key" id="offer_key" value="" />


</form>

<script type="text/javascript">
 /*var google_tag_params = { ecomm_prodid:<?php echo $ga_product_id; ?>, ecomm_pagetype: 'checkout', ecomm_totalvalue:<?php echo $cart_total; ?>, };

if(('#redeem_msg').length>0){
    var cost = document.getElementsByClassName("red_cond_cost").innerText;
    console.log(cost);
    if(parseInt(cost) < 600){
      $('.redeem_cart_msg').removeClass('hide');
    }
  }*/
</script>
 <div id="modal-offers" class="modal fade modal-offers show11 in" role="dialog">   
</div>
