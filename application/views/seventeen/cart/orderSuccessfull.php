<?php
$product_ids = '';
//echo '<pre>';print_r($res);
// code for ga_ecommerce 
$order_id = $this->uri->segment(3);
$paymod = $brand_price[0]['pay_mode'];
if($paymod == 1) $paymentMethod = 'cod'; else $paymentMethod = 'online';
$trans = '';
$items = array();
$shipping_amt = $brand_price[0]['shipping_amount']>0 ? $brand_price[0]['shipping_amount'] : 0 ;
$tax = $brand_price[0]['order_tax_amount']>0 ? $brand_price[0]['order_tax_amount'] : 0 ;

//-------------------------------------GA Ecommerce Tracking Code -----------------------------------------------------------------------------------------------------------
$i=0;
$affiliation = '';
$brandName = '';
foreach($res as $val)
{
  
    if($affiliation!='')
    {
      if($brandName != $val['company_name'])
      {
          $affiliation = $affiliation."-".$val['company_name'];
      }
       $brandName = $val['company_name'];
      
    }else
    {
        $brandName = $val['company_name'];
       $affiliation = $val['company_name'];
    }
  // code for items array 
  if(empty($items))
    {
      $items[$i] = array('sku'=>$val['product_id'], 'name'=>$val['name'], 'category'=>'', 'price'=>$val['product_price'], 'quantity'=>$val['product_qty'],'currency'=> 'INR');
    }else
    {
         $items[$i] = array('sku'=>$val['product_id'], 'name'=>$val['name'], 'category'=>'', 'price'=>$val['product_price'], 'quantity'=>$val['product_qty'],'currency'=> 'INR');
    }
    $i++;
}
// code for trans array 
$trans = array('id'=>$order_id, 'affiliation'=>$affiliation,'revenue'=>$brand_price[0]['order_total'], 'shipping'=>$shipping_amt, 'tax'=>$tax,'currency'=> 'INR');
?>
<?php
// Function to return the JavaScript representation of a TransactionData object.
function getTransactionJs(&$trans) {
  return <<<HTML
ga('ecommtracker.ecommerce:addTransaction', {
  'id': '{$trans['id']}',
  'affiliation': '{$trans['affiliation']}',
  'revenue': '{$trans['revenue']}',
  'shipping': '{$trans['shipping']}',
   'tax': '{$trans['tax']}',
  'currency': '{$trans['currency']}'
});
HTML;
}

// Function to return the JavaScript representation of an ItemData object.
function getItemJs(&$transId, &$item) {
  return <<<HTML
ga('ecommtracker.ecommerce:addItem', {
  'id': '$transId',
  'name': '{$item['name']}',
  'sku': '{$item['sku']}',
  'category': '{$item['category']}',
  'price': '{$item['price']}',
   'quantity': '{$item['quantity']}',
  'currency': '{$item['currency']}'
});
HTML;
}
?>
<!-- Begin HTML -->
<script>
/* Donot Remove This Code --------------------------------Used For Ecommerce Tracking----------------------------------------------------------------------*/
ga('create', 'UA-64209384-1', 'auto', {'name': 'ecommtracker'});
ga('require', 'ecommerce');
ga('ecommtracker.require', 'ecommerce');
/* Donot Remove This Code --------------------------------Used For Ecommerce Tracking----------------------------------------------------------------------*/
<?php
echo getTransactionJs($trans);

foreach ($items as &$item) {
  echo getItemJs($trans['id'], $item);
}
?>

/* Donot Remove This Code --------------------------------Used For Ecommerce Tracking----------------------------------------------------------------------*/
ga('ecommtracker.ecommerce:send');
ga('ecommtracker.send', 'pageview');
/* Donot Remove This Code --------------------------------Used For Ecommerce Tracking----------------------------------------------------------------------*/

//-------------------------------------End GA Ecommerce Tracking Code -----------------------------------------------------------------------------------------------------------
</script>
</body>
</html>

<!-- Start: Google Code for Livello Campaign Purchase Conversion Page, From : Rajesh on 12-Feb-2016 -->
<script type="text/javascript">
/* <![CDATA[ */
/*var google_conversion_id = 950917036;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "3zgiCK2l9mMQrK-3xQM";

if (<?php echo $brand_price[0]['order_total']; ?>) {
var google_conversion_value = <?php echo $brand_price[0]['order_total']; ?>
}
else {
var google_conversion_value = 1.00;
}
var google_conversion_currency = "INR";
var google_remarketing_only = false;*/
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/950917036/?value=<?php echo $brand_price[0]['order_total']; ?>&amp;currency_code=INR&amp;label=3zgiCK2l9mMQrK-3xQM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Google Code for Livello Campaign Purchase Conversion Page Ends -->


<div class="page-order-summary">
<section class="section section-shadow text-center">


  <div class="container">
    <div class="title-section">
      <h1 class="title">Order Summary</h1>
    </div>
    <div>
    <p>
      Your order <strong><?php echo $order_display_no; ?></strong> has been confirmed. <br />
      Below are the details of your order.
    </p>
    </div>
  </div>
</section>
<section class="section section-shadow page-order-summary">
  <div class="container">

  <div class="row">
    <div class="col-md-4">
    <div class="box" style="margin-bottom:20px;">
      <div class="title-2">Order : <?php echo $order_display_no; ?></div>
      <div class="title-2">Shipping Address</div>
      <div>
        <p>
          <?php echo $brand_price[0]['first_name'].' '.$brand_price[0]['last_name']; ?><br>
          <?php echo $brand_price[0]['shipping_address']; ?><br>
          <?php echo $brand_price[0]['state_name']; ?> ,<?php echo $brand_price[0]['city_name']; ?>  - <?php echo $brand_price[0]['pincode']; ?>
        </p>
      </div>
    </div>
    </div>
    <div class="col-md-4">
      <div class="box">
        <div class="title-2">&nbsp;</div>
          <div class="title-2">Billing Address</div>
            <div>
            <p>
             <?php $billing_address = json_decode(@$brand_price[0]['billing_address']); ?>
             <?php echo $billing_address->billing_first_name.' '.$billing_address->billing_last_name; ?><br>
             <?php echo $billing_address->billing_address; ?><br>
             <?php echo $billing_address->billing_city; ?> - <?php echo $billing_address->billing_pincode_no; ?>
            </p>
          </div>
      </div>
    </div>
  </div>

    <div class="order-items" style="margin-top:20px;">
       
              <?php  $i = 0;
                        $price =0;
                        $scDiscountPrice = 0;
                  if(!empty($res)){
                  foreach($res as $val){
                    $i++;
                    ?>
                     <div class="item">
                          <div class="row">
                            <div class="col-md-1 col-sm-2 col-xs-2">
                              <div class="item-img-wrp">
                                <img src="<?php echo $this->config->item('sc_promotional_look_image').$val['image'] ?>" alt="">
                              </div>
                            </div>
                            <div class="col-md-7 col-sm-6 col-xs-6">
                              <div class="item-desc-wrp">
                                <div class="title"><?php echo $val['name']; ?></div>
                                <div class="brand">By  <?php echo $val['company_name']; ?> </div>
                                <span class="size-wrp"><label>Size: </label> <?php echo  $val['size_text']; ?> </span>
                                <span class="size-wrp"><label>Quantity: </label> <?php echo $val['product_qty']; ?></span>
                              </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 text-right">
                                <div class="item-price-wrp">
                                  <div class="price"><i class="fa fa-inr"></i> <?php
                                      if($val['discount_price']!='' || $val['discount_price']!=0)
                                      {
                                        echo number_format((float)$val['discount_price'], 2, '.', '');
                                       
                                        $price = $price+$val['discount_price'];
                                      }else
                                      {
                                       
                                         echo number_format((float)$val['product_price'], 2, '.', '');
                                        $price = $price+$val['product_price'];
                                      }

                                      ?>
                                  </div>
                              </div>
                            </div>
                          </div>
                          </div>
                    <?php
                  }
                } ?>
                      
                 

            <?php    $cod_amount = $brand_price[0]['cod_amount']>0 ? $brand_price[0]['cod_amount'] : 0 ;
                $shipping_amount = $brand_price[0]['shipping_amount']>0 ? $brand_price[0]['shipping_amount'] : 0 ; ?>

                <ul class="order-total">
                    <?php if($brand_price[0]['order_tax_amount'] > 0) { ?>
                    <!--<li>
                      <span class="text-label">Your Total</span>
                      <span class="value"> <span class="fa fa-inr"></span> <?php //echo number_format((float)$brand_price[0]['order_tax_amount'], 2, '.', ''); ?></span>
                    </li>-->
                    <?php } ?>

                    <?php if($coupon_discount > 0) {
                    //$coupon_discount =$this->cartnew_model->cart_coupon($order_id);
                    ?>
                    <li>
                      <span class="text-label text-right">Coupon Discount <i class="discount-name">(<?php echo $coupon_code; ?>)</i> </span>
                      <span class="value price"> - <span class="fa fa-inr"></span> <?php echo number_format((float)$coupon_discount, 2, '.', ''); ?> </span>
                    </li>
                    <?php } ?>

                    <?php if($brand_price[0]['order_tax_amount'] > 0) { ?>
                    <li>
                      <span class="text-label text-right">Taxes</span>
                      <span class="value price"> <span class="fa fa-inr"></span>  <?php echo number_format((float)$brand_price[0]['order_tax_amount'], 2, '.', ''); ?></span>
                    </li>
                    <?php } ?>

                     <?php $redeem_amt = 0; $redeem_amt =$this->cartnew_model->get_refre_amt($this->uri->segment(3)); ?>
                      <?php if($redeem_amt > 0) { ?>
                    <li>
                      <span class="text-label text-right"><?php echo REDEEM_POINTS_CART_TEXT; ?></span>
                      <span class="value price">- <span class="fa fa-inr"></span> <?php echo number_format((float)$redeem_amt, 2, '.', ''); ?></span>
                    </li>
                    <?php } ?>
                    <?php if($cod_amount > 0) { ?>
                    <li>
                      <span class="text-label text-right">COD Charges</span>
                      <span class="value price"> <span class="fa fa-inr"></span> <?php echo number_format((float)$cod_amount, 2, '.', ''); ?></span>
                    </li>
                    <?php } ?>
                    <?php if($shipping_amount > 0) { ?>
                      <li>
                      <span class="text-label text-right">Shipping Charges</span>
                      <span class="value price"> <span class="fa fa-inr"></span> <?php echo number_format((float)$shipping_amount, 2, '.', ''); ?></span>
                    </li>
                    <?php } ?>

                    <?php if(isset($sc_discount_info))
                            {
                                if($sc_discount_info['discount_type_id'] == 4 && $price > $sc_discount_info['max_amount'])
                                {
                      ?>
                      <li>
                      <span class="text-label">Stylecracker Discount <?php echo $sc_discount_info['discount_percent'];?> % OFF on Amount > <?php echo $sc_discount_info['max_amount'];?>:</span>
                      <?php
                          $scDiscountPrice = ($price*$sc_discount_info['discount_percent'])/100;
                        echo round($scDiscountPrice,2); ?>
                      <span class="value price"> <span class="fa fa-inr"></span>  <?php echo round(($brand_price[0]['order_total']-$scDiscountPrice),2); ?></span>
                      </li>
                    <?php
                                }
                            }
                       ?>
                    <li>
                      <span class="text-label text-right">Total:</span>
                      <span class="value sc-color price"> <span class="fa fa-inr"></span>  <?php
                               $order_total = $price -$coupon_discount+$cod_amount+$shipping_amount+round($brand_price[0]['order_tax_amount'],2)-$redeem_amt;
                         ?>
                        <?php echo number_format((float)$order_total, 2, '.', ''); ?></span>
                    </li>
                  </ul>


      </div>
      <br/>
      <a href="https://www.stylecracker.com/sc-box" target="_blank">
      <img src="https://www.stylecracker.com/assets/seventeen/images/scbox/scbox-promo-order.jpg" />
      </a>
  </div>
</section>
</div>
<style>
  .item-img-wrp{background: #f5f5f5; text-align: center;}
  .item-img-wrp img{max-width: 100%; max-height: 80px; }
  .order-total{margin-top: 20px;}
  .discount-name{color: #13d792; }
</style>

<script type="text/javascript">
fbq('track', 'Purchase', {
  content_type: 'product',
  content_ids: [<?php echo rtrim($product_ids, ','); ?>],
  value: '<?php echo number_format((float)$order_total, 2, '.', ''); ?>', 
  currency:'INR'
}); 
</script>