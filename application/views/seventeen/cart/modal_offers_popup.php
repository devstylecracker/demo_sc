<?php //echo '<pre>'; print_r($couponcodes);exit;?>
<div class="modal-dialog modal-md111">
        <div class="modal-content">
          <button type="button" class="close" data-dismiss="modal"><i class="icon-close"></i></button>
          <div class="modal-header">
            <h2 class="box-title">Avail Offers & Discounts</h2>

          </div>
          <div class="modal-body">

            <div class="offers-popup-wrp">
              <div class="box-offers-list">

                <div class="box-input">
                  <div class="input-control-wrp">
                    <div class="form-group-wrp">
                      <div class="form-group">
                        <input class="form-control" type="text" placeholder="Enter coupon code" name="sccoupon_code" id="sccoupon_code" value="" maxlength="100">
                        <span id="coupon_error" class="error error-msg message"></span>
                      </div>
                      <a class="scbtn scbtn-primary" onclick="apply_coupon_code('apply');" ><span>Apply</span></a>
                    </div>
					 <?php
                          if(!empty($couponcodes))
                          {
                    ?>
                    <div class="title-2">OR</div>
                    <div class="text-2">
                      Choose offers from below
                    </div>
					<?php
                          }
                    ?>
                  </div>
                </div>
                <div class="offers-list-wrp scrollbar " >
                  <ul class="offers-list">                   
                     <?php foreach($couponcodes as $val)
                        {
                          $couponf3 = strtolower(substr($val['coupon_code'], 0, 3));
                          if(isset($_COOKIE['scfr']) && $_COOKIE['scfr']=='yb' && $page_type='scboxcart')
                          {
                            if( $couponf3=='yes')
                            {
                  ?>
                    <li onclick="selectOffer(this);" id="<?php echo $val['id']; ?>" data-coupontype-attr="<?php echo $val['coupon_type']; ?>" data-couponcode-attr="<?php echo $val['coupon_code']; ?>" >
                      <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                        <span><?php echo $val['coupon_title']; ?></span>
                        <span class="text2"><?php echo $val['coupon_desc']; ?></span>
                        <span class="text3">Valid till : <?php echo $val['coupon_end_datetime']; ?></span>
                      </label>
                    </li>  
                  <?php } }else{  ?>
                      <li onclick="selectOffer(this);" id="<?php echo $val['id']; ?>" data-coupontype-attr="<?php echo $val['coupon_type']; ?>" data-couponcode-attr="<?php echo $val['coupon_code']; ?>" >
                      <label class="sc-checkbox"><i class="icon icon-diamond"></i>
                        <span><?php echo $val['coupon_title']; ?></span>
                        <span class="text2"><?php echo $val['coupon_desc']; ?></span>
                        <span class="text3">Valid till : <?php echo $val['coupon_end_datetime']; ?></span>
                      </label>
                    </li>  
                  <?php } } ?>      
                  </ul>
                </div>
              </div>

              <?php foreach($couponcodes as $val)
                      {
              ?>
                <div class="box-offers-details" id="details_<?php echo $val['id']; ?>" >
                  <div class="back-wrp" >
                    <span class="back" onclick="AvailPopUpBackButton();" ><i class="fa fa-arrow-circle-left"></i> Back</span>
                  </div>
                  <div class="box-input">
                    <div class="title-2">
                      <?php echo $val['coupon_title']; ?>
                    </div>
                    <div class="input-control-wrp">
                      <div class="form-group-wrp">
                        <div class="form-group">
                          <input class="form-control" type="text" placeholder="<?php echo $val['place_holder']; ?>" name="binno_<?php echo $val['id']; ?>" id="binno_<?php echo $val['id']; ?>" value="" maxlength="6">
                          <span id="couponbinno_error" class="error error-msg111 hide message">Please enter valid numbers</span>
                        </div>
                        <a class="scbtn scbtn-primary" onclick="apply_coupon_code('applyoffer');"><span>Apply</span></a>
                      </div>
                    </div>
                  </div>
                  <div class="desc">
                    <div class="title">
                      Note:
                    </div>
                   <?php echo $val['long_desc']; ?>
                  </div>
                </div>              
            <?php } ?>     
           </div>

          </div>

          <div class="modal-footer">
            <!-- <button class="scbtn scbtn-primary"><span>Proceed</span></button> -->
           <!--  <span id="coupon_modal_message" class="error error-msg"></span> -->
          </div>
        </div>
      </div>
