<?php
// echo "<pre>";print_r($top_events);exit;?>
<section class="section section-shadow">
  <div class="container">
    <div class="title-section">
      <h1 class="title">Bookmarked Posts</h1>
    </div>

    <div class="grid grid-items blog">
      <div class="row grid-row" id="get_all_wishlist_blogs">
	  <?php
		  if(!empty($top_blogs)){ foreach($top_blogs as $val){ ?>
			<div class="col-md-4 col-sm-6 col-xs-12 grid-col grid-col">
			  <div class="item-wrp">
				<div class="item">
				<a href="<?php echo $val['blog_url']; ?>" onclick="_targetClickTrack('<?php echo @$val['id']; ?>','9','<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['post_title']); ?>','blog');" >
				  <div class="item-img-wrp">
					<img src="<?php echo @$val['image']; ?>" alt=""/>
				  </div>
				  </a>
				  <div class="item-desc-wrp">
					<div class="title"><a href="<?php echo $val['blog_url']; ?>" onclick="_targetClickTrack('<?php echo @$val['id']; ?>','9','<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['post_title']); ?>','blog');" ><?php echo @$val['post_title']; ?></a></div>
					<!--<div class="desc"><?php echo @$val['post_desc']; ?></div>-->
					<div class="category"><?php echo @$val['post_type']; ?></div>
				  </div>
					<div class="item-btns-wrp">
					  <a href="<?php echo $val['blog_url']; ?>" onclick="_targetClickTrack('<?php echo @$val['id']; ?>','9','<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['post_title']); ?>','blog');" class="scbtn scbtn-primary"><span>Read More</span></a>
					</div>
					<span class="ic-heart sc-heart <?php if(@$val['is_wishlisted'] == '1') echo "active"; ?>" id="wishlist_blog<?php echo @$val['id']; ?>" onclick="add_to_fav('blog',<?php echo @$val['id']; ?>,<?php echo @$this->session->userdata('user_id'); ?>, '<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['post_title']); ?>');">
					 <i class="icon-heart"></i>
					</span>

				</div>
			  </div>
			</div>
		<?php }//foreach
			}//if	?>
      </div>
		<input type="hidden" name="offset" id="offset" value="1">
		<input type="hidden" name="blog_type" id="blog_type" value="<?php echo 'test'; ?>">
		<input type="hidden" name="total_blogs" id="total_blogs" value="<?php echo '30'; ?>">
		<div id="load-more" class="fa-loader-item-wrp">
			<div class="fa-loader-item">
			<i class="fa fa-circle-o-notch fa-spin-custom"></i> Loading...
			</div>
		</div>
    </div>
  </div>
</section>
