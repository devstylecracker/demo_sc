  <div class="page-404-wrp">
    <section class="section section-shadow">
  <div class="container">
  <div class="content-wrp">
    <div class="row">
      <div class="col-md-1">
      </div>
      <div class="col-md-6">
        <img class="img-404" src="<?php echo base_url().'assets/images/errors/404-bg.png' ?>">
      </div>
      <div class="col-md-4">
        <h1>Sorry</h1>
        <h3>Page not found</h3>
        <p>
          <a href="https://www.stylecracker.com/" class="scbtn scbtn-primary btn-sm"><span>Go to Homepage</span></a>
        </p>
      </div>
      <div class="col-md-1">
      </div>
    </div>
      </div>
  </div>
</section>
</div>
<style>
.content-wrp{padding: 80px 0;}
.page-404-wrp h1{font-weight: 500; color:#13d792; text-transform:uppercase; font-size:78px;  line-height: 1.01;}
.page-404-wrp h3{ font-weight: 500; color:#4c4d4f; text-transform:uppercase; margin-bottom: 30px; font-size: 28px;  line-height: 1;}
.page-404-wrp .img-404{width:auto; max-width: 100%;}
@media(max-width:768px)
{
  .page-404-wrp{text-align:center; padding: 20px 0 20px; min-height: inherit;}
  .page-404-wrp h1{font-size: 28px;}
  .page-404-wrp h3{font-size: 16px;}
}
</style>
