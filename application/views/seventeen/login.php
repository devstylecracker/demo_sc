<style type="text/css">
  .alert-danger {
    color: #ed1b2e !important;
   background-color: unset !important; 
    padding-top: 15px;
}
.box-login .link-forgot-pw{
	bottom: -33px;
}
</style>
<div class="page-login-wrp">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
        <div class="login-box-wrp page-login">
          <?php
            $frmUrl = $this->session->userdata('scform');
            if(!empty($this->session->flashdata('order_login_msg')))
            { 
              echo '<div class="alert-danger"><strong>'.
                $this->session->flashdata('order_login_msg')
              .'</strong></div>';
            } 
            ?>
          <div class="box-login">
            <div class="title-section">
              <h1 class="title">Login</h1>
            </div>
            <form name="scnewLogin" id="scnewLogin" method="post" action="<?php echo base_url().'schome_new/login' ?>">
            <div>
              <div class="form-group">
                 <input type="hidden" id="scform_data" name="scform_data" class="" value="<?php echo @$_GET['scform']?@$_GET['scform']:$frmUrl;?>">
                <input type="text" id="logEmailid" name="logEmailid" class="form-control" placeholder="EMAIL ID">
				<input type="hidden" name="token" id="token" value="<?php echo @$token; ?>">
                <div class="message text-error hide">Password is invalid. </div>
              </div>

              <div class="form-group has-error111">
                <input type="password" id="logPassword" name="logPassword" class="form-control" placeholder="PASSWORD">
                <div class="message text-danger hide">Password is invalid. </div>
                <a href="#modal-forgot-password" data-toggle="modal" class="link-forgot-pw">Forgot Password?</a>
              </div>
              <div id="logIn_error" class="message text-danger "><?php echo @$error; ?></div>
              <div class="btns-wrp text-center">
                 <button type="submit" id="sc_Login" name="sc_Login" class="scbtn scbtn-primary"><span>Login</span></button>
              </div>
            </div>
            </form>

            <div class="hr-or"><i></i><span>OR</span><i></i></div>

            <div class="box-title">
              Sign in using
            </div>
            <div>

              <a href="<?php //echo $this->data['facebook_login_url']; ?>" title="Sign in using Facebook" class="btn-facebook-wrp" id="facebook_login" ><i class="icon-facebook"></i></a>

              <a href="<?php echo $this->data['google_login_url']; ?>" title="Sign in using Google" class="btn-google-wrp"><i class="icon-google">
                <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i>
              </a>
            </div>

          </div>
          <section class="section section-shadow page-login">
            <div class="box-singn-signup-link">
              <div class="box-title">
                Don't have an Account?
              </div>
              <div class="text-center"  onclick="ga('send', 'event', 'Sign Up', 'clicked','header');" >
                <a class="scbtn scbtn-tertiary" href="<?php echo base_url(); ?>signup"><span>Sign Up</span></a>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
</div>
