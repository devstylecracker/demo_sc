<style type="text/css">
  .offers-popup-wrp .offers-list li {
    width: 100%;
    display: block;
    height: 40px;
  }
.modalfooter {
    padding: 15px;
    text-align: right;
    border-top: 1px solid #e5e5e5;
}
.scbtn span{
  padding: 0 0;
  font-size: 11px;

}

.offers-popup-wrp .offers-list li {
    width: 100% !important;
    height: 40px !important;
}
</style>
<?php
  //echo '<pre>';print_r($order_list);
?>
<div class="page-order-history">
  <section class="section section-shadow ">
    <div class="container">
      <div class="title-section text-center">
        <h1 class="title">My Orders</h1>
      </div>
      <div class="tabs-wrp">
        <div class="text-center">
          <ul class="nav nav-tabs sc-tabs">
            <li class="active">
              <a data-toggle="tab" href="#ongoing">Ongoing</a>
            </li>
           <!--  <li>
              <a data-toggle="tab" href="#completed">Completed</a>
            </li> -->
          </ul>
        </div>
        <input type="hidden" id="product_id" name="product_id" value="" />
        <input type="hidden" id="order_id" name="order_id" value="" />
        <input type="hidden" id="response_type" name="response_type" value="" />
        <input type="hidden" id="user_response" name="user_response" value="" />
        <input type="hidden" id="order_unique_no" name="order_unique_no" value="" />
        <div class="tab-content sc-tab-content">
          <div id="ongoing" class="tab-pane fade in active tab-content-ongoing">
            <?php if(!empty($order_list)){
        foreach($order_list as $val){ if($val['is_completed'] != 1){ ?>
          <div 
style="-webkit-box-shadow: 2px 2px 17px -3px rgba(0,0,0,0.49);
-moz-box-shadow: 2px 2px 17px -3px rgba(0,0,0,0.49);
box-shadow: 2px 2px 17px -3px rgba(0,0,0,0.49);
padding: 20px;margin-bottom: 20px;">        
            <div class="order-wrp">
              <div class="grid grid-items">
                <div class="row">
                  <div class="col-md-2 col-sm-4 col-xs-4">
                    <div class="order-products-wrp">
                      <div class="title">
                        <strong>Box Details: </strong>
                      </div>
                      <ul class="order-products">
                        <?php foreach($val['product_info'] as $value){ ?>
                        <li style="height: 140px;">
                          <div class="img-wrp">
                            <img src="<?php echo $value->image; ?>" alt="">
                            </div>
                          </li>
                          <?php } ?>
                        </ul>
                      </div>
                    </div>
                    <div class="col-md-5 col-sm-8 col-xs-8">
                      <div class="item">
                        <div class="title-2">Order #
                          <?php echo $val['order_display_no']; ?>
                        </div>
                        <p>
                          <strong>Order Placed: </strong>
                          <?php echo $val['created_datetime']; ?>
                        </p>
                        <div class="address">
                          <div class="title">
                            <strong>Delivery Address: </strong>
                          </div>
                          <?php if(@$val['is_gift']==1)
                              {
                          ?>
                                <p>
                                <?php echo @$val['shipping_address']['shipping_address']; ?>
                                <br />
                                <?php echo @$val['shipping_address']['pincode']; ?>,
                                <?php echo @$val['shipping_address']['city_name']; ?>,
                                <?php echo @$val['shipping_address']['state_name']; ?>
                              </p>
                          <?php
                                }else
                                {
                          ?>
                          <p>
                            <?php echo @$val['shipping_address']->shipping_address; ?>
                            <br />
                            <?php echo @$val['shipping_address']->pincode; ?>,
                            <?php echo @$val['shipping_address']->city_name; ?>,
                            <?php echo @$val['shipping_address']->state_name; ?>
                          </p>
                        <?php
                              }
                        ?>
                          <div class="desc">
                            <div class="status">
                              <?php if($value->order_status_id == '1'){ echo "Processing";}
                                    else if($value->order_status_id == '2') echo 'Confirmed';
                                    else if($value->order_status_id == '8') echo 'Curated';
                                    else if($value->order_status_id == '3') echo 'Dispatched';
                                    else if($value->order_status_id == '4') echo 'Delivered';
                                    else if($value->order_status_id == '5') echo 'Cancelled';
                                    else if($value->order_status_id == '6') echo 'Return';
                                    else if($value->order_status_id == '7') echo 'Fake';
                              ?>
                            </div>
                          <?php 
              if($value->order_status_id >= '3' && $value->order_status_id != '8')
              {
                $cartAwbNumber = $this->cartnew_model->get_cartid_for_awbnumber($val['id']);
                $cartMetaData  = $this->cartnew_model->get_cart_meta_data($cartAwbNumber['id']);
                $delpartner  = $this->cartnew_model->get_del_partner($cartMetaData[0]['cart_meta_value']);
                  if($cartMetaData[1]['cart_meta_value']!='')
                  {  
              ?>
                <div><strong>AWB No. :</strong> <?php echo $cartMetaData[1]['cart_meta_value'];?></div>
                <div><strong>Track Your Order <a href="<?php echo $delpartner['tracking_link'];?>" target="_blank" style="color:#13d792;font-weight: 600;"><!-- <?php //echo $delpartner['delivery_partner_name'];?> -->Here</strong></a></div>
                            <?php
                  }
              }     
                             if($value->order_status_id == '4'){
                            ?>
                            <div class="date">
                              <strong>Date: </strong>
                              <?php echo $value->delivered_datetime; ?>
                            </div>
                            <?php }?>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-12 col-xs-12">
                      <div class="order-products-wrp">
                        <?php if(@$val['is_gift']==1)
                              {
                        ?>
                         <p class="amount">
                          <strong>GIFT </strong>
                        </p>
                        <?php
                              }
                        ?>
                        <p class="amount">
                          <strong>Total: 
                            <i class="fa fa-inr"></i>
                            <?php echo number_format((float)$val['totals']->grand_total, 2, '.', ''); ?>
                          </strong>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <?php }//if check is_completed
   ?>
              </div>
              <div class="order-wrp">
                <div class="grid grid-items">
                 <?php 
                 foreach($val['scbox_prd_data'] as $k => $v)
                 {
                  if($v['status']=='return' || $v['status']=='exchange'){                    
                  ?>
                  <div class="row" style="opacity: 0.4;">
                   <div class="col-md-2">
                      
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-4">
                      <div class="order-products-wrp">
                        <ul class="order-products">
                          <li style="height: 100px;">
                            <div class="img-wrp">
                              <img src="<?php echo $this->config->item('products_thumb').@$v['image']; ?>" alt="">
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="col-md-5 col-sm-8 col-xs-8">
                        <div class="item">
                          <div class="title-2"> # <?php echo $k+1; ?></div>
                          <div class="title">
                            <strong><?php echo @$v['name']; ?></strong>
                          </div>
                           <div class="title-2"></div>
                            <!-- <a href="#myreturn" data-toggle="modal" disabled="<?php echo (@$v['status']=='return' || @$v['status']=='exchange') ? 'disabled' : ''; ?>" onclick="userResponse(<?php echo @$v['id']; ?>,<?php echo "'".$val['order_display_no']."'"; ?>,'return',<?php echo "'".$val['order_no']."'"; ?>);" >
                                <span style="color: #13d792;text-decoration: underline;">Click here to Return.</span>
                              </a> -->
                          <div class="title-2"></div>
                          <!-- <div class="title-2">Order # <?php echo $k+1; ?></div>  -->  
                          <input type="hidden" name="responseType" id="responseType" > 
                          <?php                                 
                                if($v['status']=='return' || $v['status']=='exchange')
                                { 
                                  echo 'Your Order '.$v['status'].' request is in process';
                                }else{
                          ?>                     
                          <div class="btns-wrp">
                           <!--  <a class="scbtn scbtn-primary" href="#myreturn" data-toggle="modal" disabled="<?php echo ($v['status']=='return' || $v['status']=='exchange') ? 'disabled' : ''; ?>" onclick="userResponse(<?php echo $v['id']; ?>,<?php echo "'".$val['order_display_no']."'"; ?>,'return',<?php echo "'".$val['order_no']."'"; ?>);" >
                              <span>Return</span>
                            </a> -->
                            <a class="scbtn scbtn-primary" href="#myreturn" data-toggle="modal" disabled="<?php echo ($v['status']=='return' || $v['status']=='exchange') ? 'disabled' : ''; ?>" onclick="userResponse(<?php echo $v['id']; ?>,<?php echo "'".$val['order_display_no']."'"; ?>,'exchange',<?php echo "'".$val['order_no']."'"; ?>);" >
                              <span>Exchange</span>
                            </a>                            
                          </div>
                          <?php } ?>
                    </div>
                  </div>
                </div>

                 <?php 
                  }else
                  {
                 ?> 
                  <div class="row">
                   <div class="col-md-2">
                      
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-4">
                      <div class="order-products-wrp">
                        <ul class="order-products">
                          <li style="height: 100px;">
                            <div class="img-wrp">
                              <img src="<?php echo @$this->config->item('products_thumb').@$v['image']; ?>" alt="">
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-8 col-xs-8">
                        <div class="item">
                          <div class="title-2"> # <?php echo $k+1; ?></div>
                          <div class="title">
                            <strong><?php echo @$v['name']; ?></strong>
                          </div>
                           <div class="title-2"></div>
                            <a href="#myreturn" data-toggle="modal" disabled="<?php echo (@$v['status']=='return' || @$v['status']=='exchange') ? 'disabled' : ''; ?>" onclick="userResponse(<?php echo @$v['id']; ?>,<?php echo "'".$val['order_display_no']."'"; ?>,'return',<?php echo "'".$val['order_no']."'"; ?>);" >
                                <span style="color: #13d792;text-decoration: underline;">Click here to Return.</span>
                              </a>
                          <div class="title-2"></div>
                          <!-- <div class="title-2">Order # <?php echo $k+1; ?></div>  -->  
                          <input type="hidden" name="responseType" id="responseType" > 
                          <?php                                 
                                if($v['status']=='return' || $v['status']=='exchange')
                                { 
                                  echo 'Your Order '.$v['status'].' request is in process';
                                }else{
                          ?>                     
                          <!-- <div class="btns-wrp">
                            <a class="scbtn scbtn-primary" href="#myreturn" data-toggle="modal" disabled="<?php echo ($v['status']=='return' || $v['status']=='exchange') ? 'disabled' : ''; ?>" onclick="userResponse(<?php echo $v['id']; ?>,<?php echo "'".$val['order_display_no']."'"; ?>,'return',<?php echo "'".$val['order_no']."'"; ?>);" >
                              <span>Return</span>
                            </a>
                            <a class="scbtn scbtn-primary" href="#myreturn" data-toggle="modal" disabled="<?php echo ($v['status']=='return' || $v['status']=='exchange') ? 'disabled' : ''; ?>" onclick="userResponse(<?php echo $v['id']; ?>,<?php echo "'".$val['order_display_no']."'"; ?>,'exchange',<?php echo "'".$val['order_no']."'"; ?>);" >
                              <span>Exchange</span>
                            </a>                            
                          </div> -->
                          
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-4">
                     <div class="btns-wrp">
                           <!--  <a class="scbtn scbtn-primary" href="#myreturn" data-toggle="modal" disabled="<?php echo (@$v['status']=='return' || @$v['status']=='exchange') ? 'disabled' : ''; ?>" onclick="userResponse(<?php echo @$v['id']; ?>,<?php echo "'".$val['order_display_no']."'"; ?>,'return',<?php echo "'".$val['order_no']."'"; ?>);" >
                              <span>Return</span>
                            </a> -->
                            <a class="scbtn scbtn-primary" href="#myreturn" data-toggle="modal" disabled="<?php echo (@$v['status']=='return' || @$v['status']=='exchange') ? 'disabled' : ''; ?>" onclick="userResponse(<?php echo @$v['id']; ?>,<?php echo "'".$val['order_display_no']."'"; ?>,'exchange',<?php echo "'".$val['order_no']."'"; ?>);" >
                              <span>Exchange</span>
                            </a>                            
                          </div>
                      </div>
                      <?php } ?>
                </div>
                <?php }} ?>
               </div> 
              </div> 
            </div>  
     <?php

     }//foreach
      }//if ?>
             </div> 
            <div id="completed" class="tab-pane fade tab-content-completed">
                  <?php if(!empty($order_list)){
        foreach($order_list as $val){ if($val['is_completed'] == 1){ ?>
                  <div class="order-wrp">
                    <div class="title-2">Order #
                      <?php echo $val['order_display_no']; ?>
                    </div>
                    <div>
                      <p>
                        <strong>Order Placed: </strong>
                        <?php echo $val['created_datetime']; ?>
                      </p>
                      <div class="address">
                        <div class="title">
                          <strong>Delivery Address: </strong>
                        </div>
                        <p>
                          <?php echo $val['shipping_address']->shipping_address; ?>
                          <br />
                          <?php echo $val['shipping_address']->pincode; ?>,
                          <?php echo $val['shipping_address']->city_name; ?>,
                          <?php echo $val['shipping_address']->state_name; ?>
                        </p>
                      </div>
                      <div class="order-products-wrp">
                        <div class="title">
                          <strong>Product Details: </strong>
                        </div>
                        <ul class="order-products">
                          <?php foreach($val['product_info'] as $value){ ?>
                          <li>
                            <div class="img-wrp">
                              <img src="<?php echo $value->image; ?>" alt="">
                              </div>
                              <div class="desc">
                                <div class="status">
                                  <?php if($value->order_status_id == '1'){ echo "Processing";}
        else if($value->order_status_id == '2') echo 'Confirmed';
        else if($value->order_status_id == '3') echo 'Dispatched';
        else if($value->order_status_id == '4') echo 'Delivered';
        else if($value->order_status_id == '5') echo 'Cancelled';
        else if($value->order_status_id == '6') echo 'Return';
        else if($value->order_status_id == '7') echo 'Fake';
        ?>
                                </div>
  <?php 
    /* if($value->order_status_id >= '3' && $value->order_status_id != '8')
    {
      $cartAwbNumber = $this->cartnew_model->get_cartid_for_awbnumber($val['id']);
      $cartMetaData  = $this->cartnew_model->get_cart_meta_data($cartAwbNumber['id']);
      $delpartner  = $this->cartnew_model->get_del_partner($cartMetaData[0]['cart_meta_value']); */ 
    ?>
      <!--<div><strong>AWB No. :</strong> <?php echo $cartMetaData[1]['cart_meta_value'];?></div>
      <div><strong>Track Your Order <a href="<?php echo $delpartner['tracking_link'];?>" target="_blank" style="color:#13d792;font-weight: 600;"> <?php //echo $delpartner['delivery_partner_name'];?> Here</strong></div>-->
    <?php
    //}     
        if($value->order_status_id == '4'){
      ?>
                                <div class="date">
                                  <?php echo $value->delivered_datetime; ?>
                                </div>
                                <?php } ?>
                              </div>
                            </li>
                            <?php } ?>
                          </ul>
                          <p class="amount">
                            <strong>Total: 
                              <i class="fa fa-inr"></i>
                              <?php echo $val['totals']->grand_total; ?>
                            </strong>
                          </p>
                        </div>
                      </div>
                    </div>
                    <?php }//if check is_completed
    }//foreach
  }//if ?>
                  </div>
                </div>
               </div>
              </div>    
                  <!--//tab-->
                </section>

                <div id="myreturn" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <div class="modal-content">
                          <button type="button" class="close" data-dismiss="modal"><i class="icon-close"></i></button>
                            <div class="modal-header">
                              <h4 class="box-title" id="modal-box-title"> Oh no! We're sorry you didn't love your product. Let us fix  <br> that for you! <br>Tell us what went wrong:</h4>
                            </div>
                            <div class="modal-body">
                              <div class="offers-popup-wrp">
                                <div class="box-return-list">
                                  <div class="offers-list-wrp scrollbar">
                                    <ul class="offers-list">
                                      <li onclick="SetActive(this);">
                                        <label class="sc-checkbox">
                                          <i class="icon icon-diamond"></i>
                                          <span> It didn't fit</span>
                                        </label>
                                      </li>
                                      <li onclick="SetActive(this);">
                                        <label class="sc-checkbox">
                                          <i class="icon icon-diamond"></i>
                                          <span> I don't love the quality</span>
                                        </label>
                                      </li>
                                      <li onclick="SetActive(this);" >
                                        <label class="sc-checkbox ">
                                          <i class="icon icon-diamond"></i>
                                          <span>The product is damaged</span>
                                        </label>
                                      </li>
                                      <li onclick="SetActive(this);">
                                        <label class="sc-checkbox">
                                          <i class="icon icon-diamond"></i>
                                          <span> It doesn't suit my style</span>
                                        </label>
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                              <div style="margin: 10px;">
                                <div class="form-group-md">
                                  <textarea type="text" placeholder="" id="user_recomment" name="user_recomment" onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9. ]/g, '')" ></textarea>
                                  <label>IS THERE ANYTHING WE MISSED ? </label>
                                </div>
                              </div>
                            </div>
                            <div class="modalfooter">
                              <button class="scbtn scbtn-primary" onclick="saveUserResponse();" >
                                <span>SUBMIT</span>
                              </button>
                            </div>
                          </div>
                        </div>
                  </div>
              </div>

<script type="text/javascript">
  
  var strOptionSelected = '';
  function SetActive(obj)
  {
    $(obj).closest("ul").find("li").each(function(e) {
        $(this).removeClass('active');
        $(this).find('.sc-checkbox').removeClass('active');
    });
    $(obj).addClass('active');
    $(obj).find('.sc-checkbox').addClass('active');
    strOptionSelected = $(obj).find('span').text().trim();
    $('#user_response').val(strOptionSelected);
    console.log('strOptionSelected: '+strOptionSelected);
  }

  function userResponse(pid,oid,type,ouid)
  {    
    if(type=='return')
    {
      $('#modal-box-title').html('Oh no! We\'re sorry you didn\'t love your product. Tell us what went wrong so we can fix it when you come back: <br/><br>Reasons for returning');
    }else
    {
      $('#modal-box-title').html('Oh no! We\'re sorry you didn\'t love your product. Let us fix <br> that for you! <br><br>Tell us what went wrong:');
    }
    $('#product_id').val(pid);
    $('#order_id').val(oid);
    $('#response_type').val(type);
    $('#order_unique_no').val(ouid);
  }

  function saveUserResponse()
  {  
    var product_id = $('#product_id').val();
    var order_id = $('#order_id').val();
    var response_type = $('#response_type').val();
    var order_unique_no = $('#order_unique_no').val();
    var user_recomment = $('#user_recomment').val();   
    var user_response = strOptionSelected;

    if(product_id!='' && order_id!='' && response_type!='' && user_response!='')
    { 
      if (confirm('Are you sure you want to '+response_type+' the product ?')) {
          $.ajax({
              url: sc_baseurl+'Sc_orders/updateUserResponse',
              type: 'post',
              data: {'response_type' : response_type,'product_id':product_id,'order_id':order_id, 'user_response':user_response,'order_unique_no':order_unique_no,'user_recomment':user_recomment},
              success: function(data, status) {
                 data = data.trim();
                  msg(data);

                if(data!=''){ 
                  if(data=='success'){ 
                      //alert('Success');
                     $("#myreturn").modal("hide"); 
                     window.location.href=sc_baseurl+'my-orders';   
                  }else if(data == 'error'){                
                  }
                }          
              },
              error: function(xhr, desc, err) {
                // console.log(err);
              }
            });
        }
    }
  }

</script>