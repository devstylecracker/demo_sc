<div class="page-home">
<?php if($this->session->userdata('utm_source') == 'invitereferrals'){ ?>
	  <div class="box-message fb f16"> Thank you for signing up. Check your <a class="btn-link111" href="<?php echo base_url().'invite-and-win'; ?>">Referral Amount</a> AND <a class="btn-link111" href="#" onclick="invitereferrals_10640()"> Refer & WIN </a> </div>
	  <?php } ?>
</div>
  <section class="section section-shadow section-collections">
    <div class="container">
      <div class="title-section">
        <h1 class="title">Collections</h1>
        <a href="<?php echo base_url(); ?>collections" class="link-view-all">View All Collections</a>
      </div>

      <div class="grid grid-items grid-items-slider sm-slider collections thumb-collection">
        <div class="row grid-row">
          <div class="col-md-6 col-sm-12 col-xs-12 grid-col">
            <div class="item-wrp overlay overlay-style-2">
              <div class="item">
                <div class="item-img-wrp">
                  <img src="http://stg.stylecracker.com/assets/images/scbox/sc-box-cover.jpg" alt="">
                </div>
                <div class="item-desc-wrp">
                  <div class="inner">
                    <a href="<?php echo base_url(); ?>sc-box" onclick="ga('send', 'event', 'SCBOX', 'clicked', 'SCBOX-Home');" class="scbtn scbtn-secondary white"><h2 class="title">Get your own personal SC Box</h2></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php
		  if(!empty($top_collection)){ foreach($top_collection as $val){?>
            <div class="col-md-6 col-sm-12 col-xs-12 grid-col">
              <div class="item-wrp overlay">
                <div class="item">
                  <div class="item-img-wrp">
                    <img src="<?php echo @$val['collection_image']; ?>" alt="">
                  </div>
                  <div class="item-desc-wrp">
                    <div class="inner">
                      <h2 class="title"><?php echo @$val['collection_name']; ?></h2>
                      <div class="desc">
                        <?php echo @$val['collection_short_desc']; ?>
                      </div>
                      <div class="btns-wrp">
                        <a href="<?php echo base_url(); ?>collections/<?php echo @$val['object_slug'].'-'.@$val['collection_id']; ?>" class="scbtn scbtn-secondary white" onclick="_targetClickTrack('<?php echo @$val['collection_id']; ?>','8','<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['collection_name']); ?>','collection')" ><span>View Collection</span></a>
                      </div>
                    </div>
                  </div>
					<span class="ic-heart <?php if(@$val['is_wishlisted'] == '1') echo " active "; ?>" id="wishlist_c<?php echo @$val['collection_id']; ?>" onclick="add_to_fav('collection',<?php echo @$val['collection_id']; ?>,<?php echo @$this->session->userdata('user_id'); ?>,'<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['collection_name']); ?>');">
						<i class="icon-heart"></i>
					</span>
                </div>
              </div>
            </div>
            <?php }//foreach
			}//if	?>
        </div>

      </div>
    </div>
  </section>
  <section class="section section-shadow section-blog">
    <div class="container">
      <div class="title-section">
        <h1 class="title">SC Live</h1>
        <a href="<?php echo base_url(); ?>sc-live" class="link-view-all">View All Posts</a>
      </div>
      <div class="grid grid-items grid-items-slider sm-slider blog">
      <div class="row grid-row">
	  <?php
		  if(!empty($top_blogs)){ foreach($top_blogs as $val){ ?>
			<div class="col-md-4 col-sm-6 col-xs-12 grid-col grid-col">
			  <div class="item-wrp image-overlay">
				<div class="item">
				<a href="<?php echo $val['blog_url']; ?>" onclick="_targetClickTrack('<?php echo @$val['id']; ?>','9','<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['post_title']); ?>','blog');">
				  <div class="item-img-wrp">
					<img src="<?php echo @$val['image']; ?>" alt=""/>
				  </div>
				  </a>
				  <div class="item-desc-wrp">
					<div class="title"><a href="<?php echo $val['blog_url']; ?>" onclick="_targetClickTrack('<?php echo @$val['id']; ?>','9','<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['post_title']); ?>','blog');"><?php echo @$val['post_title']; ?></a></div>
					<div class="desc"><?php echo @$val['post_desc']; ?>...</div>
					<div class="category"><?php echo @$val['post_type']; ?></div>
				  </div>
					<div class="item-btns-wrp">
					  <a href="<?php echo $val['blog_url']; ?>" onclick="_targetClickTrack('<?php echo @$val['id']; ?>','9','<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['post_title']); ?>','blog');" class="scbtn scbtn-primary"><span>Read More</span></a>
					</div>
					<span class="ic-heart <?php if(@$val['is_wishlisted'] == '1') echo "active"; ?>" id="wishlist_blog<?php echo @$val['id']; ?>" onclick="add_to_fav('blog',<?php echo @$val['id']; ?>,<?php echo @$this->session->userdata('user_id'); ?>, '<?php echo preg_replace('/[^A-Za-z0-9\-]/', '', @$val['post_title']); ?>');">
					<i class="icon-heart"></i>
					</span>

				</div>
			  </div>
			</div>
		<?php }//foreach
			}//if	?>
      </div>
    </div>
    </div>
  </section>
  <?php if(!empty($top_event)){ ?>
  <section class="section section-shadow section-events">
    <div class="container">
      <div class="title-section">
        <h1 class="title">EXPERIENCES</h1>
        <a href="<?php echo base_url(); ?>events" class="link-view-all">View All Events</a>
      </div>
      <div class="grid grid-items grid-items-slider sm-slider events thumb-collection">
        <div class="row grid-row">
          <?php
		  if(!empty($top_event)){ foreach($top_event as $val){ ?>
            <div class="col-md-6 col-sm-6 col-xs-12 grid-col">
              <div class="item-wrp">
                <div class="item">
                  <div class="item-img-wrp">
                    <img src="<?php echo @$val['event_image']; ?>" alt="">
                  </div>
                  <div class="item-desc-wrp">
                    <div class="title">
                      <?php echo @$val['event_name']; ?>
                    </div>
                    <div class="date">
                      <?php echo @$val['event_start_date'].' ,'.@$val['event_start_time']; ?>,
                      <span class="fa fa-map-marker"></span>  <?php echo @$val['event_venue']; ?>
                    </div>
                    <div class="desc">
                      <?php echo @$val['event_short_desc']; ?>
                    </div>
                  </div>
                  <div class="item-btns-wrp">
                    <a href="<?php echo base_url(); ?>events/<?php echo @$val['object_slug'].'-'.@$val['event_id']; ?>" onclick="_targetClickTrack('<?php echo @$val['event_id']; ?>','10','<?php echo preg_replace('/[^A-Za-z0-9\-\s]/', '', @$val['event_name']); ?>','event');" class="scbtn"><span>Read More</span></a>
                    <span class="ic ic-share" onclick="send_message('event','<?php echo @$val['event_id']; ?>','<?php echo @$val['event_name']; ?>','<?php echo @$val['event_image']; ?>','<?php echo base_url().'events/'.$val['object_slug'].'-'.$val['event_id']; ?>',<?php if($user_id > 0){ echo @$user_id; }else { echo '0'; } ?>);" >
                    <i class="icon-send"></i>
                </span>
                <span class="dropdown ic-share-wrp">
                <span class="ic dropdown-toggle" data-toggle="dropdown">
                  <i class="icon-share"></i>
               </span>
                <ul class="dropdown-menu">

					<?php
					$short_url = base_url().'event_web/get_single_event/'.$val['event_id'].'';
					echo '<li><a onClick="_targetFacebookShare(\''.$val['event_id'].'\',\''.$short_url.'\',\''.$val['event_image'].'\',\''.$val['event_id'].'\');"><i class="fa fa-facebook"></i></a></li>';
					echo '<li><a onclick="_targetTweet(\''.$short_url.'\',\''.$val['event_name'].'\',\''.$val['event_id'].'\');"><i class="fa fa-twitter"></i></a></li>';
					echo '<li><a onclick="_targetPinterest(\''.$short_url.'\',\''.$val['event_name'].'\',\''.$val['event_id'].'\',\''.$val['event_image'].'\');"><i class="fa fa-pinterest"></i></a></li>';

					?>
                </ul>
                </span>
                  </div>
                  <span class="ic-heart <?php if(@$val['is_wishlisted'] == '1') echo " active "; ?>" id="wishlist_e<?php echo @$val['event_id']; ?>" onclick="add_to_fav('event',<?php echo @$val['event_id']; ?>,<?php echo @$this->session->userdata('user_id'); ?>,'<?php echo preg_replace('/[^A-Za-z0-9\-]/', '', @$val['event_name']); ?>');">
				               <i class="icon-heart"></i>
				           </span>

                </div>
              </div>
            </div>
            <?php }//foreach
			}//if	?>
        </div>
      </div>
    </div>
  </section>
  <?php }//event ?>
  
  <?php if(!empty($top_brands)){ ?>
  <section class="section section-shadow section-collections">
    <div class="container">
      <div class="title-section">
        <h1 class="title">Brands</h1>
        <a href="<?php echo base_url(); ?>all-brands" class="link-view-all">View All Brands</a>
      </div>

      <div class="grid grid-items grid-items-slider sm-slider collections thumb-collection">
        <div class="row grid-row">
          <?php
		  if(!empty($top_brands)){ foreach($top_brands as $val){?>
            <div class="col-md-6 col-sm-12 col-xs-12 grid-col">
              <div class="item-wrp overlay111">
                 <div class="item">
                  <div class="item-img-wrp" style="border:1px solid #f5f5f5;">
                    <a href="<?php echo BRAND_URL.$val['brand_slug'].'-'.$val['brand_id']; ?>"><img src="<?php echo @$val['home_page_promotion_img']; ?>" alt=""></a>
                  </div>
				 <!-- <span class="ic-share">
				      <i class="icon-send"></i>
				   </span>-->
                </div>
              </div>
            </div>
            <?php }//foreach
			}//if	?>
        </div>

      </div>
    </div>
  </section>
<?php }//brands ?>
</div>
<?php if($this->session->userdata('utm_source') == 'invitereferrals'){ $this->session->unset_userdata('utm_source'); ?>
 <img style="position:absolute; visibility:hidden" src="https://www.ref-r.com/campaign/t1/settings?bid_e=AC70A50D47DD5C5FF203ED7121693FC4&bid=11257&t=420&event=register&email=<?php echo $this->session->userdata("email"); ?>&orderID=<?php echo $this->session->userdata("user_id"); ?>&fname=<?php echo $this->session->userdata("first_name"); ?>" />
 <?php } ?>
 <script>
 function invitereferrals_10640(){ 
		var params = { bid: 11257, cid: 10607 };
		invite_referrals.widget.inlineBtn(params);
    }
 </script>