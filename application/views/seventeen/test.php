<?php ?>
<!DOCTYPE html>
<html>
<head>
<title>Facebook Login JavaScript Example</title>
<meta charset="UTF-8">
</head>
<body>
<script>
var response = ''; var accessToken='';
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else {
      // The person is not logged into your app or we are unable to tell.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '1650358255198436',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.8' // use graph api version 2.8
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
	if (response.status === 'connected') {
		accessToken = response.authResponse.accessToken;
	 }
  });
	
	FB.login(function(response) {
		// alert(JSON.stringify(response));
		if(response.authResponse) {
			uid = response.authResponse.userID;
			accessToken = response.authResponse.accessToken;
			FB.api(
					"/"+uid+"/photos?type=uploaded",
					function (response) {
					  if (response && !response.error) {
						alert(JSON.stringify(response));
					  }
					}
				);
		  }
	}, {scope: 'public_profile,email,user_photos'});
	
  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
	response = new Array();
	var accessToken = 0;
	FB.getLoginStatus(function(response) {
	  if (response.status === 'connected') {
		accessToken = response.authResponse.accessToken;
	  }
	});
	
	$.ajax({
			url: 'https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=1650358255198436&client_secret=403b4cbf601b9a67b6594c7be781e8b9&fb_exchange_token='+accessToken,
			type: 'get',
			data: { 'response' : response },
			success: function(data, status) {
				// alert(JSON.stringify(data));
				save_fb_token(data);
			}
		});
	
  }
  
	function save_fb_token(data){
		$.ajax({
			url: sc_baseurl+'Schome_new/save_fb_token',
			type: 'post',
			data: { 'data' : data},
			success: function(data, status) {
			}
		});	  
	}
</script>

<!--
  Below we include the Login Button social plugin. This button uses
  the JavaScript SDK to present a graphical Login button that triggers
  the FB.login() function when clicked.
-->

<fb:login-button  data-scope="public_profile,email,user_photos" onlogin="checkLoginState();">
</fb:login-button>

<div id="status">
</div>

</body>
</html>