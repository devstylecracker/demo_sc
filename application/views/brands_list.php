 <div class="page-header brand-header">
    <div class="container">
      <h1>Brands</h1>
      <!--<h5>Discover this season's must have looks. Curated just for you.</h5>-->
      <input type="text" class="brand-search" id="brand-search" placeholder="SEARCH FOR A BRAND"/>
    </div>
  </div>
<div class="content-wrp content-brands">
    <div class="container">
    <div class="row">
      <div class="col-md-2">
        <div class="sidebar brand-sidebar">
          <h3 class="title">Show</h3>
          <ul>
            <li id="brands_a_z">
              Brands A-Z
            </li>
            <li id="brands_popular">
              Most Popular
            </li>
          </ul>
        </div>
      </div>
      <div class="col-md-10">
        <div class="brands-content-section">
<h3>Brands A-Z</h3>
<ul class="alphabet-list">
  <li><a href="#number">0-9</a></li>
  <li><a class="A">A</a></li>
  <li><a class="B">B</a></li>
  <li><a class="C">C</a></li>
  <li><a class="D">D</a></li>
  <li><a class="E">E</a></li>
  <li><a class="F">F</a></li>
  <li><a class="G">G</a></li>
  <li><a class="H">H</a></li>
  <li><a class="I">I</a></li>
  <li><a class="J">J</a></li>
  <li><a class="K">K</a></li>
  <li><a class="L">L</a></li>
  <li><a class="M">M</a></li>
  <li><a class="N">N</a></li>
  <li><a class="O">O</a></li>
  <li><a class="P">P</a></li>
  <li><a class="Q">Q</a></li>
  <li><a class="R">R</a></li>
  <li><a class="S">S</a></li>
  <li><a class="T">T</a></li>
  <li><a class="U">U</a></li>
  <li><a class="V">V</a></li>
  <li><a class="W">W</a></li>
  <li><a class="X">X</a></li>
  <li><a class="Y">Y</a></li>
  <li><a class="Z">Z</a></li>

</ul>
<?php $brand_count = count($brands_list);
if($brand_count > 0){
	$intCount = $brand_count / 3 ;
}

$number_array = array("0","1","2","3","4","5","6","7","8","9","!","@");


 if($intCount < 1){ $intCount = 1; }
$i = 1;
?>
<div class="row" id="brandlist">

<?php

if(!empty($brands_list)){ $k = 1; $new_name = ''; $old_name = ''; $o = 1;
	foreach($brands_list as $val){ ?>
		<?php if(round($intCount) >= $k){ ?>

			<?php if($k == 1){ ?>
				<div class="col-md-4 col-sm-6">
			<?php } ?>
			<?php $new_name = strtoupper(substr(trim($val->company_name), 0,1)); ?>
			<?php if($new_name != $old_name && !in_array($new_name, $number_array)){ ?>
			<?php if($i != 1){ ?>
				</ul>
			<?php } ?>
				<ul class="brand-listing">
				<li id="<?php echo strtoupper($new_name); ?>" class="alpha"><?php echo strtoupper($new_name); ?>
				<!--script type="text/Javascript">$('.<?php echo strtoupper($new_name); ?>').attr("href","#<?php echo strtoupper($new_name); ?>");</script-->
 
        <script type="text/Javascript">var a = document.getElementsByClassName('<?php echo strtoupper($new_name); ?>'); a[0].href = "#<?php echo strtoupper($new_name); ?>";</script>
				</li>
				<li><a href="<?php echo base_url().'brands/brand-details/'.$val->user_name; ?>"><?php echo $val->company_name; ?></a></li>
				<?php $old_name = $new_name; ?>
			<?php }else{ ?>
      <?php if(in_array($new_name, $number_array) && $o == 1){ ?>
           <li id="0-9" class="alpha"><?php echo '0-9'; ?>
        <?php } ?>
				<li><a href="<?php echo base_url().'brands/brand-details/'.$val->user_name; ?>"><?php echo $val->company_name; ?></a></li>
			<?php } ?>
		<?php $o++; }else{ ?> </div>
			<?php $k = 0; ?>
		<?php } ?>
<?php $k++; $i++; }
}


?>
</div>

      </div>
    </div>
  </div>
</div>
</div>
</div>
<script type="text/Javascript">
/*$( document ).ready(function() {
  //## UserTrack: Page ID=========================================
  document.cookie="pid=3";
  /*Code ends*/
	
/*});*/

</script>
