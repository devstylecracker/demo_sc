<div class="container">
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>">Home</a></li>
    <li class="active">Categories</li>
  </ol>
<?php if(!empty($top_categories)) { ?>
    <div class="grid items-wrp items-section">
    <div class="title-section">
      <h1 class="title"><span><?php echo $this->lang->line('category-list-page'); ?></span></h1>

    </div>
      <div class="row grid-items-wrp" id="">
      <?php foreach ($top_categories as $value) { $category_img_url = ''; $cat_type = ''; $cat_url = '';
      $cat_url = base_url().'all-products/'.$value['category_slug'];
      
      $category_img_url = $this->home_model->cat_img_exists($value['id']);
      if(file_exists($category_img_url)){
      ?>
      <div class="col-md-4 col-sm-6 grid-item">
        <div class="item item-cat">
          <div class="item-img item-hover">
            <div class="item-img-inner">
          
            <a onclick="ga('send', 'event', 'ProductCategory', 'clicked', '<?php echo $value['name']; ?>');" href="<?php echo $cat_url; ?>">
              <?php if($category_img_url){ ?>
              <img title="<?php echo $value['name']; ?> - StyleCracker" alt="<?php echo $value['name']; ?>- StyleCracker" src="<?php echo base_url().$category_img_url; ?>">

                <?php } else { ?>
                    <img title="<?php echo $value['name']; ?> - StyleCracker" alt="<?php echo $value['name']; ?>- StyleCracker" src="<?php echo base_url(); ?>assets/images/image-na.jpg">
                <?php } ?>
            </a>
            <!--div class="item-look-hover">
              <a href="#" target="_blank"><span class="btn btn-primary"> <?php echo $this->lang->line('get_this'); ?> </span></a>
            </div-->
            </div>
              </div>
            <div class="item-desc">
              <div class="item-title"><a title="<?php echo $value['category_name']; ?>" href="<?php echo $cat_url; ?>" ><?php echo $value['category_name']; ?></a>
              </div>
              <!--div class="item-media">
                <ul class="social-buttons">
                  <li><a onclick="_categorytargetFacebookShare('<?php echo $value['name']; ?>','<?php echo base_url(); ?>all-products/<?php echo $page_filter_name; ?>/<?php echo $cat_type; ?>/<?php echo $value['slug']; ?>','<?php echo $category_img_url; ?>','<?php echo $value['id']; ?>');"><i class="fa fa-facebook"></i></a></li>
                  <li><a onclick="_categorytargetTweet('<?php echo $value['name']; ?>','<?php echo base_url(); ?>all-products/<?php echo $page_filter_name; ?>/<?php echo $cat_type; ?>/<?php echo $value['slug']; ?>','<?php echo $category_img_url; ?>','<?php echo $value['id']; ?>');"><i class="fa fa-twitter"></i></a></li>
                  <li><a onclick="_categorytargetPinterest('<?php echo $value['name']; ?>','<?php echo base_url(); ?>all-products/<?php echo $page_filter_name; ?>/<?php echo $cat_type; ?>/<?php echo $value['slug']; ?>','<?php echo $category_img_url; ?>','<?php echo $value['id']; ?>');"><i class="fa fa-pinterest"></i></a></li>
                </ul>
              </div-->
            </div>
          </div>
        </div>
        <?php } } ?>
      </div>
    </div>
    <?php } ?>
    </div>
