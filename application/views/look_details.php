<?php $betaout_user_id = $this->session->userdata('user_id');
$img_title_alt = str_replace('#','',@$look['look_info']['0']['name']); $look_cost=0;
$brand_return_policy = '';
if(!empty($look['products_info']))
{ $brand_id = '';
  foreach($look['products_info'] as $val)
  {
    if($val['return_policy']!='')
    {
      if($brand_id != $val['brand_id'])
      {
       $brand_return_policy = $brand_return_policy.' <b>'.$val['company_name'].'</b>-  '.$val['return_policy'].'<br/>';
      }
    }
    $brand_id = $val['brand_id'];
  }
}
?>
<script type="text/javascript">
    var productlook_array = [];
    var productlookNames_array = [];
</script>
<div class="page-look-details content-wrp">
  <div class="container">
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a></li>
      <li><a href="<?php echo base_url(); ?>looks/<?php echo $look['look_info']['0']['looks_for'] == '1' ? 'women' : 'men'; ?>">Looks</a></li>
      <li class="active"><?php echo @str_replace('\\','',$look['look_info']['0']['name']); ?></li>
    </ol>

    <div class="box-added-to-cart">
         <span class="text"><i class="fa fa-check-circle"></i> Item(s) added to your cart</span>
          <a class="btn btn-secondary btn-xs pull-right" href="<?php echo base_url(); ?>cartnew">View Cart</a>
        </div>

    <div class="look-info-section">
      <div class="row custom">
        <div class="col-md-5 zoom-gallery-wrp">
          <div class="zoom-gallery-wrp-inner">
          <div class="swiper-container gallery-top look-gallery-top">
                <ul class="swiper-wrapper">
                <?php
                    if(!empty($look['look_info']))
                    {
                      foreach($look['look_info'] as $val)
                      {
                ?>
                  <li class="swiper-slide">
                    <div class="item-img-inner zoom">
                       <?php if($look['look_info']['0']['look_type'] == 1){
                          $img_url = $this->config->item('sc_look_image_url').$look['look_info']['0']['image'];
                        ?>
                          <a href="<?php echo $this->config->item('sc_look_image_url').@$look['look_info']['0']['image']; ?>">
                    <img src="<?php echo $this->config->item('sc_look_image_url').@$look['look_info']['0']['image']; ?>" data-zoom-image="<?php echo $this->config->item('sc_look_image_url').@$look['look_info']['0']['image']; ?>" alt="<?php echo @$img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"/>
                  </a>
                      <?php }else if($look['look_info']['0']['look_type'] == 2 || $look['look_info']['0']['look_type'] == 4 || $look['look_info']['0']['look_type'] == 6){
                        $img_url = $this->config->item('sc_street_style_image_url').$look['look_info']['0']['look_image'];
                      ?>
                      <a href="<?php echo $this->config->item('sc_street_style_image_url').@$look['look_info']['0']['look_image']; ?>">
                    <img src="<?php echo $this->config->item('sc_street_style_image_url').@$look['look_info']['0']['look_image']; ?>" data-zoom-image="<?php echo $this->config->item('sc_street_style_image_url').@$look['look_info']['0']['look_image']; ?>" alt="<?php echo @$img_title_alt; ?> - StyleCracker" title="<?php echo @$img_title_alt; ?> - StyleCracker"/>
                  </a>
                      <?php }else if($look['look_info']['0']['look_type'] == 3){
                      $img_url = $this->config->item('sc_promotional_look_image').$look['look_info']['0']['product_img'];
                      ?>
                        <a href="<?php echo $this->config->item('sc_promotional_look_image_url').@$look['look_info']['0']['product_img']; ?>">
                    <img src="<?php echo $this->config->item('sc_promotional_look_image_url').@$look['look_info']['0']['product_img']; ?>" data-zoom-image="<?php echo $this->config->item('sc_promotional_look_image_url').@$look['look_info']['0']['product_img']; ?>" alt="<?php echo @$img_title_alt; ?> - StyleCracker" title="<?php echo @$img_title_alt; ?> - StyleCracker"/>
                    </a>
                      <?php } ?>
                    </div>
                   </li>
                <?php
                    }
                  }

                  if(!empty($look['products_info']))
                    {
                      foreach($look['products_info'] as $val)
                      {
                ?>
                  <li class="swiper-slide">
                    <div class="item-img-inner">
                      <a href="<?php echo $this->config->item('sc_promotional_look_image_url').@$val['image']; ?>">
                    <img src="<?php echo $this->config->item('sc_promotional_look_image_url').'thumb_546/'.@$val['image']; ?>" data-zoom-image="<?php echo $this->config->item('sc_promotional_look_image_url').@$val['image']; ?>"
                    alt="<?php echo @$val['name']; ?> - StyleCracker" title="<?php echo @$val['name']; ?> - StyleCracker">
                    </a>
                  </div>
                  </li>
                <?php
                    }
                  }
                ?>
                </ul>
                  <div class="swiper-button-next"></div>
                  <div class="swiper-button-prev"></div>
                </div>
              </div>
                <div id="zoom-place-holder" class="zoom-place-holder"> </div>
                <div class="swiper-container gallery-thumbs look-gallery-thumbs">
                  <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="item-img-inner">
                     <?php if($look['look_info']['0']['look_type'] == 1){
                      $img_url = $this->config->item('sc_look_image_url').$look['look_info']['0']['image'];
                      ?>
                            <img src="<?php echo $this->config->item('sc_look_image_url').@$look['look_info']['0']['image']; ?>" alt="<?php echo @$img_title_alt; ?> - StyleCracker" title="<?php echo $img_title_alt; ?> - StyleCracker"/>
                            <?php }else if($look['look_info']['0']['look_type'] == 2 || $look['look_info']['0']['look_type'] == 4 || $look['look_info']['0']['look_type'] == 6){
                       $img_url = $this->config->item('sc_street_style_image_url').$look['look_info']['0']['look_image'];
                      ?>
                            <img src="<?php echo $this->config->item('sc_street_style_image_url').@$look['look_info']['0']['look_image']; ?>" alt="<?php echo @$img_title_alt; ?> - StyleCracker" title="<?php echo @$img_title_alt; ?> - StyleCracker"/>
                            <?php }else if($look['look_info']['0']['look_type'] == 3){
                      $img_url = $this->config->item('sc_promotional_look_image').$look['look_info']['0']['product_img'];
                      ?>
                      <img src="<?php echo $this->config->item('sc_promotional_look_image_url').@$look['look_info']['0']['product_img']; ?>" alt="<?php echo @$img_title_alt; ?> - StyleCracker" title="<?php echo @$img_title_alt; ?> - StyleCracker"/>
                      <?php } ?>
                     </div>
                     </div>
                  <?php
                    if(!empty($look['products_info']))
                    {
                      foreach($look['products_info'] as $val)
                      {
                ?>
                  <div class="swiper-slide">
                      <div class="item-img-inner">
                    <img src="<?php echo $this->config->item('sc_promotional_look_image_url').'thumb_160/'.@$val['image']; ?>" alt="<?php echo @$val['name']; ?> - StyleCracker" title="<?php echo @$val['name']; ?> - StyleCracker">
                 </div>
                 </div>
                <?php
                    }
                  }
                ?>
                  </div>                  
              </div>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-12">
                <div class="look-desc-wrp">
                  <div class="look-title-wrp">
                      <h1 class="look-title">
                        <?php echo stripslashes(@$look['look_info']['0']['name']); ?>
                      </h1>
                      <?php
                      if($this->session->userdata('user_id')){
                        if($this->home_model->get_look_fav_or_not($look['look_info']['0']['id']) == 0){ ?>
                          <div class="user-likes"><span class="label-add-to-wishlist">Add to Wish List</span> <a target="_blank" title="Add to Wish List" id="whislist<?php echo $look['look_info']['0']['id']; ?>" onclick="add_to_wishlist(<?php echo $look['look_info']['0']['id']; ?>);ga('send', 'event', 'Look', 'wishlist', '<?php echo $look['look_info']['0']['name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love"></i> </a></div>
                        <?php }else{ ?>
                          <div class="user-likes"><span class="label-add-to-wishlist">Remove from Wish List</span> <a target="_blank" title="Remove from Wish List" id="whislist<?php echo $look['look_info']['0']['id']; ?>" onclick="remove_from_wishlist(<?php echo $look['look_info']['0']['id']; ?>);ga('send', 'event', 'Look', 'wishlist', '<?php echo $look['look_info']['0']['name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love active"></i></a></div>
                        <?php } }else{ ?>
                        <div class="user-likes"><span class="label-add-to-wishlist">Add to Wish List</span> <a target="_blank" title="Add to Wish List" onclick="ga('send', 'event', 'Sign Up', 'clicked', 'favorite');" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i> </a></div>
                      <?php }
                      ?>
              </div>

                      <div class="look-details-btns-wrp111 buy-btns-wrp">
                        <div class="row">
                          <div class="col-md-4 col-sm-4 col-xs-6">
                            <a class="btn btn-block btn-secondary btn-add-to-cart buy-look-add-to-cart" data-attr="<?php echo $look['look_info']['0']['id']; ?>" ><i class="fa fa-cart-plus"></i>Add to Cart</a>
                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-6">
                            <a class="btn btn-block btn-primary btn-buy-now buy-look" data-attr="<?php echo $look['look_info']['0']['id']; ?>" onclick="buy_entire_look(this);">Buy The Look</a>
                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <div class="look-price">
                            <span class="price" id="look_cost-">  </span>
                          </div>
                          </div>
                        </div>
                      </div>


                     <ul class="prod-features-list">
                     <?php
                      if(!empty($look['products_info']))
                          {
                        foreach($look['products_info'] as $val)
                            { ?>
                             <?php if($val['is_paid']==1 && (!empty($look['products_inventory'][$val['id']]['size_text'])) ){ ?>
                          <li>
                           <div class="row">
                            <div class="col-md-6">
                               <div class="product-title" >
                                 <!-- checkbox -->
                              <?php if($val['is_paid']==1 && (!empty($look['products_inventory'][$val['id']]['size_text'])) ){ ?>
                                <label id="label_<?php echo $val['id']; ?>" class="add-check active">
                                <input type="checkbox" value="<?php echo $val['id']; ?>" data-price="<?php echo $this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price'] : $val['price'];?>"  data-product-name="<?php echo $val['company_name']." - ".$val['name']; ?>" id="prd_select" checked name="look_product[]" onchange=" product_selected(this);">
                                <i class="fa fa-square"></i>
                                </label>
                              <?php }else{ ?>
                                <label id="label_<?php echo $val['id']; ?>" class="add-check add-check-disabled ">
                                <input type="checkbox" value=""  class="disabled" disabled="true">
                                <i class="fa fa-square"></i>
                                </label>
                              <?php } ?>
                               <!-- /checkbox -->

                             <?php if($val['sc_product_color'] > 0) { ?>
                             <?php echo str_replace('\\','',$val['name']).' '.unserialize(ALL_FILTER_COLOR)[$val['sc_product_color']]; ?>
                             <?php }else{ ?>
                               <?php echo str_replace('\\','',$val['name']); ?>
                             <?php } ?>
                           </div>
                            </div>
                             <?php if($val['is_paid']==1 && (!empty($look['products_inventory'][$val['id']]['size_text'])) ){ ?>
                             <script type="text/javascript">
                             productlook_array.push("<?php echo $val['id']; ?>");
                             productlookNames_array.push("<?php echo $val['company_name']." - ".$val['name']; ?>");   
                             </script>
                             <div class="col-md-2 col-sm-6  col-xs-6">
                             <?php if(!empty($look['products_inventory'][$val['id']]['size_text'])) { ?>
                               <select class="product-options size-options" name="size_<?php echo $val['id']; ?>" id="size_<?php echo $val['id']; ?>">
                               <?php foreach($look['products_inventory'][$val['id']]['size_text'] as $key=>$value) { ?>
                                <!--option>Select Size</option-->
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>

                                <?php }  ?>
                              </select>


                             <?php } ?> </div> <?php }else{ ?>
                             <div class="col-md-2 "><span class="text-red">Sold Out</span></div>
                             <?php } ?>
                             <div class="col-md-4 col-sm-6  col-xs-6">
                               <div class="product-price">
                              <?php /*$look_cost = $look_cost + $val['price']; */
                                    if($val['is_paid']==1 && (!empty($look['products_inventory'][$val['id']]['size_text'])) ){
                                      $product_price =  (int)$this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price'] : $val['price'];
                                      $look_cost = $look_cost + $product_price;
                                  }
                              ?>

                              <?php if($this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price']>0 || $val['compare_price']>$val['price']) { ?>
                              <span class="mrp-price text-strike"><i class="fa fa-inr"></i> <?php if($val['compare_price']>$val['price']){ echo $val['compare_price']; }else { echo $val['price']; $val['compare_price'] = $val['price'];} ?></span>
                              <!--<span class="price"><i class="fa fa-inr"></i> <?php echo $this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price'] : ''; ?></span>
                              <span class="discount-price">(<?php echo round($this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discount_percent']); ?>% OFF)</span>-->
							   <span class="price"><i class="fa fa-inr"></i> <?php $discount_price = $this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price'] : ''; if($discount_price > 0){ echo $discount_price; }else{
								  echo $val['price'];
							  } ?></span>
							  <span class="discount-price">(<?php $discount_percent = $this->Product_desc_model->calculate_discount_percent($val['compare_price'],$discount_price,$val['price']); echo $discount_percent; ?>% OFF)</span>
                              <?php }else{ ?>
                              <span class="price"><i class="fa fa-inr"></i> <?php echo $val['price']; ?></span>
                              <?php } ?>
                            </div>

                             </div>
                         </li>
                      <?php
                            }
                          }
                        }
                      ?>
                  </ul>
                   <div class="look-details-btns-wrp111 buy-btns-wrp">
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-6">
                        <a class="btn btn-block btn-secondary btn-add-to-cart buy-look-add-to-cart" data-attr="<?php echo $look['look_info']['0']['id']; ?>"><i class="fa fa-cart-plus"></i>Add to Cart</a>
                      </div>
                       <div class="col-md-4 col-sm-4 col-xs-6">
                        <a class="btn btn-block btn-primary btn-buy-now buy-look" data-attr="<?php echo $look['look_info']['0']['id']; ?>" onclick="buy_entire_look(this);">Buy The Look</a>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                          <div class="look-price">
                          <span class="price" id="look_cost--">  </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="look-share-wrp">
                    <span class="label-share">Share</span>
                    <ul class="social-buttons">
                      <?php 
                        $short_url = base_url().'looks/look-details/'.$look['look_info']['0']['slug'];
                        echo '<li><a onClick="_targetFacebookShare(\''.$this->seo_desc.'\',\''.base_url().'looks/look-details/'.$look['look_info']['0']['slug'].'\',\''.$this->config->item('sc_look_image_url').$look['look_info']['0']['image'].'\',\''.$look['look_info']['0']['id'].'\',\''.$look['look_info']['0']['name'].'\');"><i class="fa fa-facebook"></i></a></li>';
                        echo '<li><a onclick="_targetTweet(\''.$short_url.'\',\''.$look['look_info']['0']['name'].'\',\''.$look['look_info']['0']['id'].'\');"><i class="fa fa-twitter"></i></a></li>';
                        echo '<li><a onclick="_targetPinterest(\''.$short_url.'\',\''.$look['look_info']['0']['name'].'\',\''.$look['look_info']['0']['id'].'\',\''.$img_url.'\');"><i class="fa fa-pinterest"></i></a></li>';

                      ?>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>


  <div class="look-details-panels">
    <div class="section-title">
       <!--  <h2 class="title">
        Look Details
      </h2> -->
    </div>
  <div class="panel-wrp">
    <div data-toggle="collapse" data-target="#collapse-products" aria-expanded="false" aria-controls="collapse-products" class="title clearfix">
      <h3 data-toggle="collapse" data-target="#collapse-products" aria-expanded="false" aria-controls="collapse-products">Product Details</h3>
      <span class="panel-collapsible fa fa-minus collapsed" data-toggle="collapse" data-target="#collapse-products" aria-expanded="false" aria-controls="collapse-products"></span>
    </div>
    <div id="collapse-products" class="collapse-products collapse" aria-expanded="false">
      <div class="row">
        <div class="col-md-12">
        <ul class="prod-details-list">
          <li class="title-wrp">
             <div class="row">
               <div class="col-md-6">
                 <div class="title">
                 Product Name
               </div>
               </div>
               <div class="col-md-4">
                 <div class="title">
                 Brand
               </div>
               </div>
               <div class="col-md-2">
                 <div class="title">
                 Size Chart
               </div>
               </div>
             </div>
          </li>
           <?php
            if(!empty($look['products_info'])){
              foreach($look['products_info'] as $val)
                { ?>
                <li>
                  <div class="row">
                    <div class="col-md-6">
                      <?php echo $val['name']; ?>
                    </div>
                    <div class="col-md-4">
                      <?php echo $val['company_name']; ?>
                    </div>
                    <div class="col-md-2" >
                    <?php if($this->Looks_model->get_size_guide_n($val['size_guide'],$val['id'],$val['brand_id'])){ ?>
                    <a  href="<?php echo $this->Looks_model->get_size_guide_n($val['size_guide'],$val['id'],$val['brand_id']); ?>" class="size-guide scmfp">Size Guide</a>
                    <?php } ?>
                    <!-- <a href="javascript:void(0)" class="size-guide scmfp111" onclick="openSizeGuide('<?php echo $val['size_guide']; ?>','<?php echo $val['id']; ?>','<?php echo $val['brand_id']; ?>');">Size Guide</a> -->
                    </div>
                  </div>
                </li>
            <?php } }  ?>
         </ul>
       </div>
      </div>
    </div>
  </div>
<?php
      if($brand_return_policy!="")
      {
?>
 <div class="panel-wrp">
  <div data-toggle="collapse" data-target="#collapse-options" aria-expanded="false" aria-controls="collapse-options"  class="title clearfix">
    <h3  data-toggle="collapse" data-target="#collapse-options" aria-expanded="false" aria-controls="collapse-options">Return Policy</h3>
    <span class="panel-collapsible fa fa-minus collapsed" data-toggle="collapse" data-target="#collapse-options" aria-expanded="false" aria-controls="collapse-options"></span>
  </div>
   <div id="collapse-options" class="collapse-options collapse" aria-expanded="false">
   		<?php echo $brand_return_policy; ?>
    </div>
  </div>
   <?php
        }
  ?>
  <?php if($look['look_info']['0']['look_tag']!='') { ?>
  <div class="panel-wrp">
    <div data-toggle="collapse" data-target="#collapse-description" aria-expanded="false" aria-controls="collapse-description"  class="title clearfix">
      <h3 data-toggle="collapse" data-target="#collapse-description" aria-expanded="false" aria-controls="collapse-description">Description</h3>
      <span class="panel-collapsible fa fa-minus collapsed" data-toggle="collapse" data-target="#collapse-description" aria-expanded="false" aria-controls="collapse-description"></span>
    </div>
    <div id="collapse-description" class="collapse-description collapse" aria-expanded="false">
          <?php echo @$look['look_info']['0']['look_tag']; ?>
    </div>
  </div>
  <?php } ?>

    <div class="panel-wrp">
      <div data-toggle="collapse" data-target="#collapse-reviews" aria-expanded="false" aria-controls="collapse-reviews"  class="title clearfix">
        <h3 data-toggle="collapse" data-target="#collapse-reviews" aria-expanded="false" aria-controls="collapse-reviews">Reviews</h3>
        <span class="panel-collapsible fa fa-minus collapsed" data-toggle="collapse" data-target="#collapse-reviews" aria-expanded="false" aria-controls="collapse-reviews"></span>
      </div>
      <div id="collapse-reviews" class="collapse-reviews collapse" aria-expanded="false">
              <?php if(!$this->session->userdata('user_id')){ ?>
              <div class="write-review-btn-wrp">
              <div class="row">
              <div class="col-md-8 col-md-offset-4">
              <div>
              <a class="btn btn-primary" target="blank" data-toggle="modal" href="#myModal">Write a Review</a>
              </div>
              </div>
              </div>
              </div>
              <?php }else{ ?>
              <div class="write-review-section">
              <div class="row">
              <div class="col-md-4">
              <div class="write-review-title">
              Write a Review
              </div>
              </div>
              <div class="col-md-8">
              <div class="review-form">
              <div class="form-group">
              <textarea name="review_text" id="review_text" class="form-control" placeholder="Please write your comment"></textarea>
              </div>
              <div class="user-ratings-selction">
              <ul class="ratings do-not-del-ratings" id="addratings">
                <li class="star1" title="1"><input type="radio" value="1" name="ratings" id="ratings"></li>
                <li class="star2" title="2"><input type="radio" value="2" name="ratings" id="ratings"></li>
                <li class="star3" title="3"><input type="radio" value="3" name="ratings" id="ratings"></li>
                <li class="star4" title="4"><input type="radio" value="4" name="ratings" id="ratings"></li>
                <li class="star5" title="5"><input type="radio" value="5" name="ratings" id="ratings"></li>
              </ul>
              </div>
              <div class="form-group">
              <div id="review_error"></div>
              </div>
              <div class="form-group">
              <div class="btn btn-primary" name="post_review" id="post_look_review">Submit</div>
              </div>
              </div>
              </div>
              </div>
              </div>
              <?php } ?>

              <?php if(!empty($brands_review)) { ?>
              <div class="user-reviews-list-wrp">
                <div class="title-section">
                  <h3 class="title"><span>Reviews</span></h3>
                </div>
                <div class="user-reviews-list" id="reviews">

                <?php foreach($brands_review as $val){  ?>
                <div class="user-review">
                  <div class="row">
                    <div class="col-md-3">
                <div class="user-info">
                <div class="user-photo">
                  <img src="<?php echo base_url(); ?>assets/images/brands/profile.png">
                </div>
                  <div class="user-name-wrp">
                    <div class="user-ratings">
                      <div class="ratings-wrp">
                        <ul class="ratings rating-<?php echo $val['ratings'] ?>">
                          <li class="star1"></li>
                          <li class="star2"></li>
                          <li class="star3"></li>
                          <li class="star4"></li>
                          <li class="star5"></li>
                        </ul>
                      </div>
                    </div>
                  <div class="user-name">
                  <?php echo $val['user_name'] ?>
                  </div>
                  <div class="user-activity">
                  <?php echo $val['created_datetime'] ?>
                  </div>
                  </div>
                <div class="clear"></div>
                </div>
              </div>
              <div class="col-md-9">
                <?php echo strip_tags($val['review_text']); ?>
                </div>
                  </div>
                 </div>
                <?php }  ?>
                </div>
              </div>
              <?php }else{ ?>
			  <div class="user-reviews-list" id="reviews"></div>
			  <?php }  ?>
      </div>
    </div>
  </div>
  <div class="title-section">
    <h2 class="title"><span>Products from this Look</span></h2>
  </div>
    <div class="row five-cols products-list grid items-wrp">
    <?php if(!empty($look['products_info'])){
            foreach ($look['products_info'] as $val) {   ?>
              <?php if($val['is_paid']==1 && !empty($look['products_inventory'][$val['id']]['size_text'])){ ?>
            <div class="col-md-1 col-sm-4 col-xs-6 grid-item">
              <div class="item item-look-product">
              <?php
                  $user_link = 0;
                  if($this->session->userdata('user_id')){ $user_link = $this->session->userdata('user_id')!='' ? $this->session->userdata('user_id') : 0 ; }
                  ?>
                          <div class="item-img item-hover">
                            <?php if($val['is_paid']!=1 || empty($look['products_inventory'][$val['id']]['size_text'])){ ?>
                              <div class="btn-out-of-stock">Sold Out</div>
                              <?php } ?>
                                <div class="item-img-inner">
                                <a href="<?php echo base_url(); ?>product/details/<?php echo $val['slug']; ?>-<?php echo $val['id']; ?>">
                                <?php if(file_exists($this->config->item('products_340').@$val['image'])) { ?>
                                <img src="<?php echo $this->config->item('products_340').@$val['image']; ?>" alt="<?php echo @$val['name']; ?> - StyleCracker" title="<?php echo @$val['name']; ?> - StyleCracker"/>
                                <?php }else{ ?>
                                <img src="<?php echo $this->config->item('product_image_thumb').@$val['image']; ?>" alt="<?php echo @$val['name']; ?> - StyleCracker" title="<?php echo @$val['name']; ?> - StyleCracker"/>
                                <?php } ?>
                                </a>
                              </div>
                              <div class="item-look-hover">
                                  <a title="<?php echo @$val['name']; ?>" href="<?php echo base_url(); ?>product/details/<?php echo $val['slug']; ?>-<?php echo $val['id']; ?>" onclick="ga('send', 'event', 'Product', 'clicked', '<?php echo @$val['name']; ?>');fbq('track', 'ViewContent');"><span class="btn btn-primary">Get this</span>
                                  </a>
                              </div>

                              <div class="item-look-hover">
                                  <a onclick="ga('send', 'event', 'Product', 'clicked', '<?php echo $val['name']; ?>');fbq('track', 'ViewContent'); " title="<?php echo @$val['name']; ?>" href="<?php echo base_url(); ?>product/details/<?php echo $val['slug']; ?>-<?php echo $val['id']; ?>"><span class="btn btn-primary">Get this</span>
                                  </a>
                            </div>

                          </div>

                          <div class="item-desc">
                            <div class="item-title" title="<?php echo stripslashes($val['name']); ?>">
                                <a href="<?php echo base_url(); ?>product/details/<?php echo $val['slug']; ?>-<?php echo $val['id']; ?>"><?php
                                $lookPrdName = stripslashes($val['name']);
                                  if (strlen($lookPrdName) >= 50)
                                  {
                                    // truncate string
                                    $stringCut = substr($lookPrdName, 0, 47);
                                    // make sure it ends in a word so assassinate doesn't become ass...

                                    echo $stringCut.'...';
                                  }else
                                  {
                                    echo $lookPrdName;
                                  }

                                /*echo stripslashes($val['name']);*/ ?>  </a>
                            </div>
                            <div class="item-price-wrp">
                             <!-- <div class="item-price">
                              <?php if($this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price']>0) { ?>
                              <span class="mrp-price text-strike"><i class="fa fa-inr"></i> <?php echo $val['price']; ?></span>
                              <span class="price"><i class="fa fa-inr"></i> <?php echo $this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price'] : ''; ?></span>
                              <span class="discount-price">(<?php echo round($this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discount_percent']); ?>% OFF)</span>
                              <?php }else{ ?>
                              <span class="price"><i class="fa fa-inr"></i> <?php echo $val['price']; ?></span>
                              <?php } ?>
                            </div>-->
							
							<div class="item-price">
                     <?php if($this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price']>0 || $val['compare_price']>$val['price']) { ?>
                  <span class="mrp-price text-strike"><i class="fa fa-inr"></i> <?php if($val['compare_price']>$val['price']){ echo $val['compare_price']; }else { echo $val['price']; $val['compare_price'] = $val['price'];} ?></span>
                 
				  <span class="price"><i class="fa fa-inr"></i> <?php $discount_price = $this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price']>0 ? $this->Product_desc_model->get_productDiscount_price($val['id'],$val['brand_id'])['discounted_product_price'] : ''; if($discount_price > 0){ echo $discount_price; }else{
					  echo $val['price'];
				  } ?></span>
                  <span class="discount-price">(<?php $discount_percent = $this->Product_desc_model->calculate_discount_percent($val['compare_price'],$discount_price,$val['price']); echo $discount_percent; ?>% OFF)</span>
				  
                  <?php }else{ ?>
                  <span class="price"><i class="fa fa-inr"></i> <?php echo $val['price']; ?></span>
                  <?php } ?>
                    </div>
                            <?php
                               if($this->session->userdata('user_id')){
                                 if($this->home_model->get_fav_or_not('product',$val['id']) == 0){ ?>
                                   <div class="user-likes"><a target="_blank" title="Add to Wish List" id="wishlist<?php echo $val['id']; ?>" onclick="add_to_fav('product','<?php echo $val['id']; ?>','<?php echo $betaout_user_id; ?>','<?php echo $val['price']; ?>','<?php echo $val['name']; ?>','<?php echo $val['product_cat_id']; ?>','add_to_wishlist'); ga('send', 'event', 'Product', 'wishlist', '<?php echo $val['name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love"></i></a></div>
                                 <?php }else{ ?>
                                   <div class="user-likes"><a target="_blank" title="Remove from Wish List" id="wishlist<?php echo $val['id']; ?>" onclick="add_to_fav('product',<?php echo $val['id']; ?>,'<?php echo $betaout_user_id; ?>','<?php echo $val['price']; ?>','<?php echo $val['name']; ?>','<?php echo $val['product_cat_id']; ?>','remove_from_wishlist');ga('send', 'event', 'Product', 'wishlist', '<?php echo $val['name']; ?>', '<?php echo $this->session->userdata('user_id'); ?>');"><i class="ic sc-love active"></i></a></div>
                                 <?php } }else{ ?>
                                 <div class="user-likes"><a target="_blank" onclick="ga('send', 'event', 'Sign Up', 'clicked', 'favorite');" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a></div>
                               <?php }  ?>
                          </div>
                          </div>
                </div>
              </div>
                <?php } } } ?>
      </div>
    </div>
  </div>
  </div>
<input type="hidden" id="brand_token" name="brand_token" value="<?php echo $look['look_info']['0']['id']; ?>">
<input type="hidden" name="look_cost_" id="look_cost_" value="<?php echo $look_cost; ?>">
<script type="text/javascript">
  document.getElementById('look_cost-').innerHTML = '<i class="fa fa-inr"></i> '+document.getElementById('look_cost_').value;
  document.getElementById('look_cost--').innerHTML = '<i class="fa fa-inr"></i> '+document.getElementById('look_cost_').value;
  function get_reviews(){
        var brand_token = $('#brand_token').val();
        $.ajax({
          url: '<?php echo base_url(); ?>looks/get_reviews',
          type: 'POST',
          data: {'brand_token':brand_token},
          cache :true,
          success: function(response) {
            $('#reviews').html(response);
          },
          error: function(xhr) {
          }
        });
      }

      function load_more_reviews(){
        var brand_token = $('#brand_token').val();
        $.ajax({
          url: '<?php echo base_url(); ?>looks/get_all_reviews',
          type: 'POST',
          data: {'brand_token':brand_token},
          cache :true,
          success: function(response) {
            $('#reviews').html(response);
          },
          error: function(xhr) {
          }
        });
      }

      function product_selected(obj)
      {
        var luk_cust = 0;
        document.getElementById('look_cost-').innerHTML = '';
        document.getElementById('look_cost--').innerHTML = '';
        /*if (document.getElementById('prd_select').checked) */

        if (obj.checked)
        {
             productlook_array.push(obj.value);
             productlookNames_array.push($(obj).attr('data-product-name'));
             luk_cust =  parseInt(document.getElementById('look_cost_').value);
             luk_cust =  luk_cust + parseInt($(obj).attr('data-price'));
             document.getElementById('look_cost_').value = luk_cust;
              $('#label_'+obj.value).addClass('active');


        } else {

              var index = productlook_array.indexOf(obj.value);
              var index1 = productlookNames_array.indexOf($(obj).attr('data-product-name'));

              if (index > -1) {
                  productlook_array.splice(index, 1);
                  productlookNames_array.splice(index1, 1);    
                  luk_cust =  parseInt(document.getElementById('look_cost_').value);
                  luk_cust =  luk_cust - parseInt($(obj).attr('data-price'));
                  document.getElementById('look_cost_').value = luk_cust;
                 $('#label_'+obj.value).removeClass('active');
              }
        }

       //console.log(productlookNames_array);

        document.getElementById('look_cost-').innerHTML = '<i class="fa fa-inr"></i>'+document.getElementById('look_cost_').value;
        document.getElementById('look_cost--').innerHTML = '<i class="fa fa-inr"></i>'+document.getElementById('look_cost_').value;
        money_formatter(".price");
      }

</script>
