<div class="page-header search-header">
  <div class="container">
    <h1><?php echo $search_data; ?></h1>
    <h5><?php echo $search_count; ?> results</h5>
  </div>
</div>
<div class="content-wrp page-search">
  <div class="filterbar-wrp">
    <div class="container">
      
          

              <div class="filterbar">
      <span class="filter-label">Filter by: </span>
              <select name="select_type" id="select_type" class="selectpicker">
                <option value="0">Latest</option>
                <!--<option value="hot-right-now">Hot right now</option>-->
                <option value="looks">Looks</option>
                <!--<option value="street-style">Street style</option>-->
                <option value="sc-live-posts">SC Live posts</option>
                <!--<option value="sc-celeb-style">Celeb Style</option>-->
              </select>

            </div>


        </div>
        </div>
       <div class="container">
          <div class="grid items-wrp">
            <div class="row grid-items-wrp" id="show_looks_plus_blog">
            <div class="grid-sizer"></div>

  </div>
</div>
</div>

<style>
/*  .menu-search .dropdown-menu{display: block;}
  .menu-search-item{ background: #38383a none repeat scroll 0 0; color: #00b19c;}*/
</style>

<script type="text/Javascript">
var cache = new ajaxCache('GET', true, 1800000);
var stylist_offset,street_offset;

$(function() {

      var all_offset = "stylist_offset=0&street_offset=0&filter=0&page_type=search&page_type_id=<?php echo $search_data; ?>";

      get_looks(all_offset);

      $( "#select_type" ).change(function() { 
          $('#show_looks_plus_blog').html('');
          $('#show_looks_plus_blog').html('<div class="grid-sizer"></div>');

          all_offset = "stylist_offset=0&street_offset=0&page_type=search&page_type_id=<?php echo $search_data; ?>&filter="+$('#select_type').val();

          get_looks(all_offset);
      });

      $( window ).load( function(){
        //## UserTrack: Page ID=========================================
        document.cookie="pid=2";
        /*Code ends*/
        $('.grid-items-wrp').masonry({
            // set itemSelector so .grid-sizer is not used in layout
            itemSelector: '.grid-item',
            // use element for option
            // columnWidth: '.grid-sizer',
            percentPosition: true
        });
      });
});

      var isTriggered = false;
          $(window).scroll(function() {
          var intTriggerScroll = 50;
          var intCurrentScrollPos = $(document).height()-(window.innerHeight+$(this).scrollTop());

          if(intCurrentScrollPos<intTriggerScroll && !isTriggered){


          if($('#stylist_offset').length > 0){
            stylist_offset = $('#stylist_offset').val();
          }else{
            stylist_offset = '-1';
          }

          if($('#street_offset').length > 0){
            street_offset = $('#street_offset').val();
          }else{
            street_offset = '-1';
          }

           all_offset = "stylist_offset="+stylist_offset+"&street_offset="+street_offset+"&page_type=search&page_type_id=<?php echo $search_data; ?>&filter="+$('#select_type').val();
           isTriggered=true;

           if($('#stylist_offset').length >0 || $('#street_offset').length >0){
              get_looks(all_offset);
           }
          }

      });


function get_looks(viewData)
{
   $('#look_loader_img').css('display', 'inline-block');
  var ajax_path = '<?php echo base_url(); ?>Schome/get_all_stylist_looks';

  if(cache.on)
  {
    var cachedResponse = cache.get(ajax_path, [viewData]);
    if(cachedResponse !== false)
    {
      // update website
      update_website(cachedResponse); // We just avoided one ajax request
      initialiceMasonry();
      add_hover();
      return true;
    }
  }

  $.ajax({
    url: ajax_path,
    type: 'GET',
    data: viewData,
    cache :true,
    success: function(response) {
      if(cache.on) cache.put(ajax_path, viewData, response); // record the new response

      update_website(response);
      initialiceMasonry();
      add_hover();
    },
    error: function(xhr) {

    }
  });

  return true;
}

function update_website(response){

  if($('#stylist_offset').length >0 ){ $('#stylist_offset').remove(); }
  if($('#street_offset').length >0 ){ $('#street_offset').remove(); }

  $('#show_looks_plus_blog').append(response);
  $('#look_loader_img').hide();
  isTriggered = false;

}

    function initialiceMasonry(){
        var $container = $('.grid-items-wrp');
            $container.masonry({
                isInitLayout : true,
                itemSelector: '.grid-item',
                isAnimated: false,
                percentPosition: true

            });

            $container.imagesLoaded(function() {
              $( $container ).masonry( 'reloadItems' );
              $( $container ).masonry( 'layout' );
            });
          }

</script>
