<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'libraries/REST_Controller.php';
/**
 * TailoredTech CI Rest Controller
 *
 * An extension of Phil Sturgeon's Rest API.
 *
 * @package        	CodeIgniter
 * @subpackage    	YoSell
 * @category    	Libraries
 * @author        	Tailored Tech Dev Team
 * @version 		1.0
 */
abstract class TT_REST_Controller extends REST_Controller
{
	protected $open_methods = array();
	protected $user_details = null;
	 /**
	 * Failed Response
	 *
	 * Returns an object containing a standard success response.
	 * Error Codes:
	 * 1000 - Element not found
	 * 1001 - Insert Failed
	 * 1002 - Malformed Input
	 * 1003 - Authentication Failiure
	 * 1004 - Email verification Failiure
	 * 1020 - Unknown Error
	 * @access	public
	 * @param	Int containing error code
	 * @param	String containing error description
	 */
	function failed_response($code=0, $message='') 
	{
		$data = new stdClass();
		$data->status = "fail";
		$data->error = $code;
		if(strlen($message)>0)
		{
			$data->message = $message;
		}
		else
		{
			switch ($code)
			{
			case 1000:
			  $data->message = "Element not found";
			  break;
			case 1001:
			  $data->message = "No Record(s) Found";
			  break;
			case 1002:
			  $data->message = "Malformed Input: Maybe you are missing some required fields";
			  break;
			case 1003:
			  $data->message = "Authentication Failure";
			  break;
			case 1004:
			  $data->message = "Email Not Verified";
			  break;
			case 1005:
			  $data->message = "Please enter the username";
			  break;	
			case 1006:
			  $data->message = "Please enter the Email-id";
			  break;	
			case 1007:
			  $data->message = "Please enter the Password";
			  break;	
			case 1008:
			  $data->message = "Please enter valid Email-id";
			  break;	
			case 1009:
			  $data->message = "Username already exist";
			  break;
			case 1010:
			  $data->message = "Email id already exist";
			  break;
            case 1011:
			  $data->message = "Registration failed!";
			  break;			  
			 case 1012:
			  $data->message = "Authorization failed!";
			  break;
			 case 1013:
			  $data->message = "Password should have atleast 8 character";
			  break;
			 case 1014:
			  $data->message = "Password should not greater than 20 character";
			 case 1015:
			  $data->message = "Please enter the bucket_ids";
			  break;
			 case 1200:
			  $data->message = "Process falied. Please try again!";
			  break; 
			 case 1200:
			  $data->message = "Process falied. Please try again!";
			 case 1201:
			  $data->message = "Please Upload Photo and try again!";
			  break;   
			default:
			  $data->message = "Unknown Error";
			}
		}
	    $this->response($data);
	}
	/**
	 * Success Response
	 *
	 * Returns an object containing a standard success response
	 *
	 * @access	public
	 * @param	Object containing response
	 */
	function success_response($response) 
	{
		$data = new stdClass();
		$data->status = "success";
		$data->response = $response;
		if(is_array($response))
		{
			$data->type = "array";
		}
		else if(is_object($response))
		{
			$data->type = "object";
		}
		else
		{
			$data->type = "string";
		}
	    $this->response($data);
	}
	public function _remap($object_called, $arguments)
	{
		$controller_method = $object_called.'_'.$this->request->method;
		if(!in_array($controller_method, $this->open_methods) && $this->request->method!="get")
		{
			$this->_authenticate();
		}
		parent::_remap($object_called, $arguments);
	}
	/*public function _authenticate()
	{	
		$user_id = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		if($user_id && $auth_token)
		{
			//authenticate & pull the user details from your model here
			$details = new stdClass(); 
			$details->name = "Aakash";
			if($details)
			{
				$this->user_details = $details;
			}
			else
			{
				$this->failed_response(1003);
			}
		}
		else
		{
			$this->failed_response(1002,'Please enter both a user_id and auth token');
		}
	}*/
    public function validate_api_token($user_id, $auth_token)
	{
		$CI =& get_instance();
		$CI->load->model('Mobile_model');
		$result = $CI->Mobile_model->login_user_token($user_id,$auth_token);
		return $result;
	}
	public function _authenticate()
	{	

			// Get the user_id & auth_token from the POST Params.
		$user_id = $this->input->post('user_id');
		$auth_token  = $this->input->post('auth_token');
		if($user_id && $auth_token)
		{

			if($this->validate_api_token($user_id, $auth_token)){

			}
			else
			{
				$this->failed_response(1000, "Please pass a valid user id and auth token");
			}
			


		 }		
		else
		{
			$this->failed_response(1000, "Please pass both the user id and auth token");
		}
	}
}