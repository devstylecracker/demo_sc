<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 */
class MY_Controller extends CI_Controller {

	public $data = array();
	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	var $seo_title = '';
	var $seo_desc = '';
	var $seo_keyword = '';
	var $megamenu = '';
	var $seo_canonical = '';
	var $seo_next = '';
	var $seo_prev = '';
	var $seo_image = '';
	public function __construct()
	{
		parent::__construct();	
		$this->load->driver('cache');
		//$this->register_user_facebook();
		$this->seo_title = '';
		$this->seo_desc = '';
		$this->seo_keyword = '';
		$this->megamenu = '';
		$this->seo_canonical = '';
		$this->seo_next = '';
		$this->seo_prev = '';
		$this->seo_image = '';
    	$this->register_user_google();    
		$this->get_uc_states();
		$this->get_user_address();
		$this->get_megamenu_mens();
		$this->get_megamenu_men_html();
		$this->get_megamenu_womens();
		$this->get_megamenu_women_html();		
		$this->get_footer_category();		
		$this->get_footer_brands();
	}
// public function register_user_facebook(){
//     if(isset($_REQUEST['type'])) echo $_REQUEST['type'];
//     $this->load->library('facebook');
//     $user=$this->facebook->getuser();
//     if($user && $_REQUEST['type']=='get_facebook_signin'){
//       echo "coming in";
//       print_r($user);
//       exit;
//       try{
//         $data['user_info']=$this->facebook->api('/me');
//         $ip = $this->input->ip_address();
//         $this->signup('facebook',$scusername,$scemail_id,$scpassword,$ip,2);
//       }catch (FacebookApiException $e){
//         $user=null;
//       }
//     }
//     else{
//       $this->data['facebook_login_url']=$this->facebook->getLoginUrl(array(
//             'redirect_url'=>site_url('schome/register_user_facebook'),
//           'scope'=>array('email')
//       ));
//     }
//       //$this->load->view('login',$data);
// }

public function register_user_google(){
    
    $this->data['google_login_url'] = $this->googleplus->loginURL();
    //$this->load->view('welcome_message',$contents);
}

public function get_uc_states(){
		$all_uc_states = array();
		$all_uc_states=$this->cart_model->get_all_states();
		
		$this->data['all_uc_states'] = $all_uc_states;


	}

	public function get_user_address(){
		$user_existing_address = array();
		$user_existing_address= $this->cart_model->get_user_existing_address();
		
		$this->data['user_existing_address'] = $user_existing_address;

	}

	public function get_megamenu_mens()
	{
		$megamenu =array();		
		//$megamenu = $this->home_model->get_filters('men');
		if ( $this->cache->file->get('get_megamenu_mens')  == FALSE ) {
			$megamenu_mens = $this->home_model->get_option_data('mega_menu_men');						
			$this->data['megamenu_mens'] = unserialize(@$megamenu_mens[0]['option_value']);  
			$this->cache->file->save('get_megamenu_mens', $this->data['megamenu_mens'], 500000000000000000);
		}else{
			$this->data['megamenu_mens'] = $this->cache->file->get('get_megamenu_mens');
		} 
	}

	public function get_megamenu_men_html()
	{
		$megamenu =array();		
		if ( $this->cache->file->get('get_megamenu_men_html')  == FALSE ) {
			$megamenu_womens = $this->home_model->get_option_data('mega_menu_men_html');					
			$this->data['megamenu_mens_html'] = $megamenu_womens; 
			$this->cache->file->save('get_megamenu_men_html', $this->data['megamenu_mens_html'], 500000000000000000);
		}else{
			$this->data['megamenu_mens_html'] = $this->cache->file->get('get_megamenu_men_html');
		}
	}

	public function get_megamenu_womens()
	{
		$megamenu =array();		
		//$megamenu = $this->home_model->get_filters('women');	
		if ( $this->cache->file->get('get_megamenu_womens')  == FALSE ) {
			$megamenu_womens = $this->home_model->get_option_data('mega_menu_women');		
			$this->data['megamenu_womens'] = unserialize(@$megamenu_womens[0]['option_value']);  
			$this->cache->file->save('get_megamenu_womens', $this->data['megamenu_womens'], 500000000000000000);  
		}else{
			$this->data['megamenu_womens'] = $this->cache->file->get('get_megamenu_womens');
		}   
	}

	public function get_megamenu_women_html()
	{
		$megamenu =array();		
		if ( $this->cache->file->get('get_megamenu_women_html')  == FALSE ) {	
			$megamenu = $this->home_model->get_option_data('mega_menu_women_html');				
			$this->data['megamenu_womens_html'] = $megamenu; 
			$this->cache->file->save('get_megamenu_women_html', $this->data['megamenu_womens_html'], 500000000000000000);  
		}else{
			$this->data['megamenu_womens_html'] = $this->cache->file->get('get_megamenu_women_html');
		}
	}

	function encryptOrDecrypt($mprhase, $crypt) {
	     $MASTERKEY = "STYLECRACKERAPI";
	     $td = mcrypt_module_open('tripledes', '', 'ecb', '');
	     $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
	     mcrypt_generic_init($td, $MASTERKEY, $iv);
	     if ($crypt == 'encrypt')
	     {
	         $return_value = base64_encode(mcrypt_generic($td, $mprhase));
	     }
	     else
	     {
	         $return_value = mdecrypt_generic($td, base64_decode($mprhase));
	     }
	     mcrypt_generic_deinit($td);
	     mcrypt_module_close($td);
	     return $return_value;
	}
	
	public function get_footer_category()
	{	
		$footer_cat =array();		
		if ( $this->cache->file->get('footer_categories')  == FALSE ) {
			$footer_cat = $this->home_model->category_data();						
			$this->data['footer_categories'] = unserialize(@$footer_cat);  
			$this->cache->file->save('footer_categories', $this->data['footer_categories'], 5);
		}else{
			$this->data['footer_categories'] = $this->cache->file->get('footer_categories');
		} 
	}
	
	public function get_footer_brands(){	
		$footer_brands =array();	
		if ( $this->cache->file->get('footer_brands')  == FALSE ) {
			$footer_brands = $this->home_model->footer_brand_data();						
			$this->data['footer_brands'] = unserialize(@$footer_brands);  
			$this->cache->file->save('footer_brands', $this->data['footer_brands'], 5);
		}else{
			$this->data['footer_brands'] = $this->cache->file->get('footer_brands');
		} 
	}
	
	public function create_token($token_type){
		$token = $this->User_info_model->create_token($token_type);
		return $token;
	}
	
	public function validate_token($token,$token_type){
		$validate = $this->User_info_model->validate_token($token,$token_type);
		return $validate;
	}
	
	public function delete_old_token($token,$token_type){
		$validate = $this->User_info_model->delete_old_token($token,$token_type);
	}


}
