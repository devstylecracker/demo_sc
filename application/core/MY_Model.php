<?php
class MY_Model extends CI_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
       
    }
    
    function get_sc_options($option_name){
        if($option_name!=''){
            
            $query = $this->db->get_where('sc_options', array('option_name' => $option_name));
            $res = $query->result_array();
            if(!empty($res)){
                return unserialize($res[0]['option_value']);
            }else{ return ''; }
        }
    }
 } 
?>
