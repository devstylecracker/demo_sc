<?php
class Scproductinventorycronnew_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();        
        /*echo "asdfsdfsadfsdf";
        exit;*/
    }

    function get_brand_id($api_key,$api_secret_key){ 
        $query = $this->db->get_where('user_login', array('email' => trim($api_key),'password'=>trim($api_secret_key),'status'=>1));
        $result = $query->result_array();
        if(!empty($result)){
            return $result[0]['id'];
        }else{
            return 0;
        }
    }

    function get_product_id($product_url, $product_sku=""){//$product_sku,
        
        $query = $this->db->query("select id, product_sku from product_desc where `url`='".$product_url."' order by id desc limit 1");//`product_sku`='".$product_sku."' OR
        $result = $query->result_array();

       
        if(!empty($result)){
            return $result;
        }else{
            return 0;
        }
    }

    function get_product_id_color($product_url, $product_color){//$product_sku, and product_color = '$product_color'
        $query = $this->db->query("select id, product_sku from product_desc where `url`='".$product_url."' order by id desc limit 1");//`product_sku`='".$product_sku."' OR
        $result = $query->result_array();

        if(!empty($result)){
            return $result;
        }else{
            $prod_exist[0]['id']=0;
            return $prod_exist;
        }
    }

    function get_product_id_sku($product_sku,$product_url){

        $query = $this->db->query("select id from product_desc where `product_sku`='".$product_sku."' OR `url`='".$product_url."'");
        $result = $query->result_array();
        if(!empty($result)){
            return $result[0]['id'];
        }else{
            return 0;
        }
    }

    function getbrand_code($brand_id){
        if($brand_id > 0){
            $query = $this->db->query("select brand_code from `brand_info` where user_id='".$brand_id."'");
            $result = $query->result_array();
            if(!empty($result)){
                return $result[0]['brand_code'];
            }else{
                return '';
            }
        }else{
            return '';
        }
    }

    function get_category_id($category_name){
        /*echo "asdfasdfasdf";
        exit;*/
        $query = $this->db->query("select id from category where category_name = '$category_name'");
        $result = $query->result_array();
        return $result[0]['id'];
            
    }
    
    function get_categoryID($category_name){
        if(trim($category_name)!=''){
            $query = $this->db->query("select id from product_category where lower(name) =lower('".$category_name."')");
            $res = $query->result_array();
            if(!empty($res)){ return $res[0]['id']; }else{ return 1; }
        }else{
            return 1;
        }
    }

    function get_subcategoryID($category_name){
        if(trim($category_name)!=''){
            $query = $this->db->query("select id from product_sub_category where lower(name) =lower('".$category_name."')");
            $res = $query->result_array();
            if(!empty($res)){ return $res[0]['id']; }else{ return 1; }
        }else{
            return 1;
        }
    }

    function add_product($data){
        if(!empty($data)){
                if(!empty($data['product_desc'])){
                    $this->db->insert('product_desc', $data['product_desc']); 

                    $product_insert_id = $this->db->insert_id(); 

                    /* Product Inventory */
                    if(!empty($data['product_stock']['size'])){
                       $this->db->where('product_id', $product_insert_id);
                       $this->db->delete('product_inventory'); 

                        if(!empty($data['product_stock']['size'])){  $o = 0;  
                            foreach($data['product_stock']['size'] as $val){ 
                                $size = $data['product_stock']['size'][$o]; 
                                $qty = $data['product_stock']['stock'][$o]; 

                                $size_id = $this->get_size_id($size);
                                if($size_id>0 && $product_insert_id>0){
                                    $data1 = array(
                                        'product_id'=>$product_insert_id,
                                        'size_id'=>$size_id,
                                        'stock_count'=>$qty,
                                    );
                                    $this->db->insert('product_inventory', $data1); 
                                }
                             $o++;
                            }
                            
                        }
                }
                /* Product Inventory end*/

                /* Product Extra image starts*/
                    if(!empty($data['product_extra_images'])){
                        $this->db->where('product_id', $product_insert_id);
                        $this->db->delete('extra_images'); 
                        foreach($data['product_extra_images'] as $val){
                            $image_info = array(
                                'product_id'=>$product_insert_id,
                                'product_images'=>$val,
                                'created_datetime'=>date('Y-m-d H:i:s')
                                );
                             $this->db->insert('extra_images', $image_info); 
                        }
                    }
                    
                /* Product Extra image end*/

                /* Products tags start*/
                     if(!empty($data['product_tags'])){
                       
                       foreach($data['product_tags'] as $val){
                            $tag_id = $this->get_tag_id($val);
                            $this->add_tag_to_product($tag_id,$product_insert_id,$data['product_desc']['brand_id']);
                        }
                    }
                   
                /* Products tags end*/                
            }
        }
    }

    function update_product($data){
        if(!empty($data)){
            if($data['product_id'] > 0){
               $product_id = $data['product_id']; 
                if(!empty($data['product_desc'])){
                    $this->db->where('id', $product_id);
                    $this->db->update('product_desc', $data['product_udesc']); 
                }

                /* Product Inventory starts*/
                if(!empty($data['product_stock']['size'])){
                       $this->db->where('product_id', $product_id);
                       $this->db->delete('product_inventory'); 

                        if(!empty($data['product_stock']['size'])){  $o = 0;  
                            foreach($data['product_stock']['size'] as $val){ 
                                $size = $data['product_stock']['size'][$o]; 
                                $qty = $data['product_stock']['stock'][$o]; 

                                $size_id = $this->get_size_id($size);
                                if($size_id>0 && $product_id>0){
                                    $data1 = array(
                                        'product_id'=>$product_id,
                                        'size_id'=>$size_id,
                                        'stock_count'=>$qty,
                                    );
                                    $this->db->insert('product_inventory', $data1); 
                                }
                             $o++;
                            }
                            
                        }
                }
                /* Product Inventory end*/

                /* Product Extra image starts*/
                 /*   if(!empty($data['product_extra_images'])){
                        $this->db->where('product_id', $product_id);
                        $this->db->delete('extra_images'); 
                        foreach($data['product_extra_images'] as $val){
                            $image_info = array(
                                'product_id'=>$product_id,
                                'product_images'=>$val,
                                'created_datetime'=>date('Y-m-d H:i:s')
                                );
                             $this->db->insert('extra_images', $image_info); 
                        }
                    }
                    */
                /* Product Extra image end*/
            }
        }
    }

    //---- Bulk Add product
    function add_product_bulk($data){
        if(!empty($data['product_desc_add'])){
                if(!empty($data['product_desc_add']['data'])){
                    //print_r($data['product_desc_add']['data']);//exit;
                    foreach($data['product_desc_add']['data'] as $row_data){
                        $dataset[] = $row_data;
                    }
                    $this->db->trans_start();
                    $this->db->insert_batch('product_desc', $dataset); 
                    $this->db->trans_complete();
                    //echo $this->db->last_query();

                    /* Product Inventory */
                    if(!empty($data['sql_stock_inventory_add'])){
                        $this->db->query(substr($data['sql_stock_inventory_add'],0,-2));
                    }
                /* Product Inventory end*/

                /* Product Extra image starts*/
                    if(!empty($data['sql_extra_img'])){ 
                        $this->db->query(substr($data['sql_extra_img'],0,-2));
                    }
                    
                /* Product Extra image end*/

                /* Products tags start*/
                     if(!empty($data['sql_product_tag_add'])){
                       foreach($data['sql_product_tag_add'] as $row){
                            $this->db->query(substr($row,0,-2)); 
                            //$this->db->last_query();
                        }
                    }
                   
                /* Products tags end*/

                /* Products Categories Add start*/
                     if(!empty($data['sql_product_category_add'])){
                       foreach($data['sql_product_category_add'] as $row){
                            $this->db->query(substr($row,0,-2));
                            //$this->db->last_query();
                        }
                    }                   
                /* Products Categories Add end*/

                /* Products discount start*/
                     if(!empty($data['sql_product_discount_add'])){
                       foreach($data['sql_product_discount_add'] as $row){
                            $this->db->query(substr($row,0,-2)); 
                            //$this->db->last_query();
                        }
                    }                   
                /* Products discount end*/
            }
        }
    }

    //---- Bulk update product
    function update_product_bulk($data){
        if(!empty($data)){
            if(isset($data['product_desc_update']) && count($data['product_desc_update']) > 0){
               //$product_id = $data['product_id']; 
                if(!empty($data['product_desc_update']['product_udesc'])){
                    //$this->db->where_in('id', $product_id);
                    $this->db->update_batch('product_desc', $data['product_desc_update']['product_udesc'],'id'); 
                    //echo $this->db->last_query();
                }                

                /* Product Inventory starts*/
                if(!empty($data['sql_stock_inventory_update'])){
                       //$this->db->where_in('product_id', $data['product_desc_update']['product_stock']['delete']);
                       //$this->db->delete('product_inventory'); 
                       
                       // echo $this->db->last_query();

                        /*if(!empty($data['product_desc_update']['product_stock']['data'])){  $o = 0;  
                             $this->db->insert_batch('product_inventory', $data['product_desc_update']['product_stock']['data']);  */
                        //echo $data['sql_stock_inventory_update'];//exit;
                        if(!empty($data['sql_stock_inventory_update'])){   
                            $this->db->query(substr($data['sql_stock_inventory_update'], 0, -1));
                            //echo $data['sql_stock_inventory_update'];
                            // mysqli_multi_query($db_con, $data['sql_stock_inventory_update'] );

                             //echo "<br/><br/><br/>";
                            //echo  $this->db->last_query();
                        }
                }
                /* Product Inventory end*/

                /* Product discount starts*/                
                if(!empty($data['sql_product_discount_update'])){                    
                       //$this->db->where_in('product_id', $data['product_desc_update']['product_stock']['delete']);
                       //$this->db->delete('product_inventory');                        
                       //echo $this->db->last_query();
                        /*if(!empty($data['product_desc_update']['product_stock']['data'])){  $o = 0;  
                             $this->db->insert_batch('product_inventory', $data['product_desc_update']['product_stock']['data']);  */
                        //echo $data['sql_stock_inventory_update'];//exit;
                        if(!empty($data['sql_product_discount_update'])){   
                            //$this->db->query(substr($data['sql_product_discount_update'], 0, -1));
                            mysqli_multi_query($this->db->conn_id, $data['sql_product_discount_update']);                                          
                            //echo $data['sql_stock_inventory_update'];
                            // mysqli_multi_query($db_con, $data['sql_stock_inventory_update'] );
                             //echo "<br/><br/><br/>";
                            //echo  $this->db->last_query();
                        }
                }/* Product discount end*/


            }
        }
    }


      function get_size_id($size){
        if($size!=''){
            $query = $this->db->query("select count(*) as cnt,id from product_size where size_text = '".strtoupper($size)."'");
            $result = $query->result_array();
            if($result[0]['cnt'] == 0){
                $data = array(
                    'size_text' => strtoupper($size) ,
                );
                $this->db->insert('product_size', $data); 
                return $this->db->insert_id();
            }else {
                return $result[0]['id']; 
            }
        }
    }

    

      function get_tag_id($tag){
        if($tag!=''){
            $query = $this->db->query("select count(*) as cnt,id from tag where name = '".strtoupper(trim($tag))."'");
            $result = $query->result_array();
            if($result[0]['cnt'] == 0){
                $data = array(
                    'name' => strtolower(trim($tag)) ,
                    'status' => 1 ,
                    'is_delete' => 0 ,
                    'created_datetime' => date('Y-m-d H:i:s') ,
                    'slug' => str_replace(' ','-',strtolower($tag)).'-'.time().mt_rand(),
                );
                $this->db->insert('tag', $data); 
                return $this->db->insert_id();
            }else {
                return $result[0]['id']; 
            }
        }
    }

    function add_tag_to_product($tag_id,$product_insert_id,$brand_id){
        if($tag_id > 0 && $product_insert_id>0){
            $tags = array();
            $tags = array(
                        'product_id'=>$product_insert_id,
                        'tag_id' => $tag_id,
                        'status' => 1,
                        'created_by' =>$brand_id,
                        'created_datetime' => date('Y-m-d H:i:s')
                    );
            $this->db->insert('product_tag', $tags); 
        }
    }


    function cronvalue_information($brand_name,$start_time,$end_time,$add_product,$update_product){            
        $data = array('brand_name'=>$brand_name,'start_time'=>$start_time,'end_time'=>$end_time,'product_updated' => $add_product,'product_added' => $update_product);
        $this->db->insert('cron_details', $data);
        exit;
    }
}

?>