<?php

class Mobile_home_model extends MY_Model {
	
	public $budget_for_homepage = "";
	public $bucket_for_homepage = "";
	public $age_for_homepage = "";
	public $slug = "";
	
	function get_sc_options($option_name){
        if($option_name!=''){
            $query = $this->db->get_where('sc_options', array('option_name' => $option_name));
            $res = $query->result_array();
            if(!empty($res)){
                return unserialize($res[0]['option_value']);
            }else{ return ''; }
        }
    }
	
	function get_home_look($type=NULL,$user_id=NULL){
        $look_data = array();
        if($user_id > 0){
            $look_ids = $this->top_looks_depends_on_pa($user_id);
        }
        
        if(empty($look_ids)){
            $look_ids = $this->get_sc_options($type);
        }
       
        // if(!empty($look_ids)){
            // foreach ($look_ids as $look_id) {
                // $look_data[] = $this->get_look_info($look_id);
            // }
        // }
        return $look_ids;
    }
	
	function get_look_info($lookId){
        if($lookId > 0){
        $query = $this->db->query('select z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.description,z.status,z.deflag,z.slug from 
(SELECT a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.description,a.status,a.deflag,a.slug FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 and a.id = '.$lookId.') as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 OR z.look_type = 6) ');
            $res = $query->result_array();
            return $res;
        }else{
        return false;
        }
    }
	
	
	 // get top 3 category
	function get_home_categories($type=NULL,$user_id=NULL){
        $cat_data = array();
        $option_key_name = 'top_cat_'.$type;
		
		if($user_id > 0){
            $cat_ids = $this->top_cat_depends_on_pa($user_id);
        }
		
        if(empty($cat_ids)){
            $cat_ids = $this->get_sc_options($option_key_name);
        }
		
        if(!empty($cat_ids)){
            foreach ($cat_ids as $cat_id) {
                $cat_data[] = $this->get_category_info($cat_id['type'],$cat_id['cat']);
            }
        }
        return $cat_data;
    }
	
	function get_category_info($cat_id,$preselected){
        $cat_table_name = ''; $cat_data = array();
        if($cat_id == 1){ $cat_table_name = 'category'; }
        if($cat_table_name !=''){
            $query = $this->db->get_where($cat_table_name, array('id'=>$preselected));
            $res = $query->result_array(); 
            if(!empty($res)){
                foreach ($res as $val) {
                    $cat_data['name'] = $val['category_name'];
                    $cat_data['cat_id'] = $val['id'];
                    $cat_data['product_cat_id'] = @$val['product_cat_id'];
                    $cat_data['type'] = $cat_id;
                    $cat_data['slug'] = $this->get_slug($val['id']);
                    $cat_data['product_count'] = $this->product_count($cat_id,$val['id'],'');
					$cat_data['url'] = '';
					$cat_data['cat_type'] = '';
					$cat_data['share_url'] = '';
                    $this->slug ='';
                }
            }
        }
        return $cat_data;
    }
	/*
	function get_category_info($cat_id,$preselected){
        $cat_table_name = ''; $cat_data = array();
        if($cat_id == 1){ $cat_table_name = 'product_category'; }
        else if($cat_id == 2){ $cat_table_name = 'product_sub_category'; }
        else if($cat_id == 3){ $cat_table_name = 'product_variation_type'; }
        else if($cat_id == 4){ $cat_table_name = 'product_variation_value'; }

        if($cat_table_name !=''){
             if( $cat_table_name !='product_variation_value' && $cat_table_name !='product_variation_type'){
                $query = $this->db->get_where($cat_table_name, array('status' => '1','id'=>$preselected));
            }else if( $cat_table_name =='product_variation_type'){
                $query = $this->db->query('select b.*,c.product_cat_id from product_variation_type as b,product_sub_category as c where b.status = 1 and c.id = b.prod_sub_cat_id and b.id = '.$preselected);
            }else{
                $query = $this->db->query('select a.*,c.product_cat_id from product_variation_value as a,product_variation_type as b,product_sub_category as c where a.    product_variation_type_id = b.id and a.status = 1 and c.id = b.prod_sub_cat_id and a.id = '.$preselected);
            }
            $res = $query->result_array();
			// print_r($res);;exit;
            if(!empty($res)){
                foreach ($res as $val) {
                    $cat_data['name'] = $val['name'];
                    $cat_data['cat_id'] = $val['id'];
					$cat_data['product_cat_id'] = @$val['product_cat_id'];
                    $cat_data['type'] = $cat_id;
					$cat_data['slug'] = $val['slug'];
					$cat_data['url'] = '';
					$cat_data['cat_type'] = '';
					$cat_data['share_url'] = '';
                }
            }
        }

        return $cat_data;
    }*/
	
	// get top 3 brands
    function get_home_brands($type=NULL,$user_id){
        $brands_data = array();
        $option_key_name = 'trending_brand_'.$type;
		
		if($user_id > 0){
            $brand_ids = $this->top_brands_depends_on_pa($user_id);
        }
        if(empty($brand_ids)){
            $brand_ids = $this->get_sc_options($option_key_name);
        }
		
		$brand_data = array();
        if(!empty($brand_ids)){
            foreach ($brand_ids as $brand_id) {
                $brand_data[] = $this->get_brands_info($brand_id);
            }
        }
        return $brand_data;
    }

    function get_brands_info($brandId){
        if($brandId > 0){
			$brand_image_url = base_url().'assets/images/brands/homepage/';
			$query = $this->db->query("select a.user_id,a.company_name,IF(a.home_page_promotion_img='','nothing',CONCAT('https://www.stylecracker.com/assets/images/brands/homepage/',a.home_page_promotion_img)) as home_page_promotion_img,CONCAT('".base_url()."brands/brand-details/',a.company_name) as share_url from brand_info as a where a.user_id = $brandId ");
			$res = $query->result_array();
			return $res[0];
        }else{
            return false;
        }
    }
	
	function get_slug($id){
        
        if($id!='-1'){
        $query = $this->db->query('select category_slug,category_parent from category where id = "'.$id.'"');
        $res = $query->result_array();
        if(!empty($res)){
            foreach($res as $val){

                $this->slug = $val['category_slug'].'/'.$this->slug; 
                
                $this->get_slug($val['category_parent']);

            }
            return  $this->slug ;
        }
        }
        
    }
	
	function top_looks_depends_on_pa($user_id){
         $query = $this->db->query('select bucket from user_login where id='.$user_id." limit 0,1");
         $res = $query->result_array($query);
         if(!empty($res)){
            $bucket = $res[0]['bucket']; $age = ''; $budget = ''; $question_id = 0; $where_condition='';
            // $this->session->set_userdata("bucket_for_homepage",$bucket);
			$this->bucket_for_homepage = $bucket;
            $query = $this->db->query('select question_id,answer_id from `users_answer` where user_id='.$user_id.' and question_id IN (7,8,15,18)');
            $res = $query->result_array($query);

            if(!empty($res)){
                foreach ($res as $val) {
                    if($val['question_id'] == '7' || $val['question_id'] == '18'){
                        $budget = $val['answer_id']; $question_id = $val['question_id'];
                    }
                    if($val['question_id'] == '8' || $val['question_id'] == '15'){
                        $age = $val['answer_id']; $question_id = $val['question_id'];
                    }

                }
            }
          
            if($question_id == '18'){
                if($budget == '4688')
                {
                    $where_condition = $where_condition.' and a.bucket_id='.$bucket.' AND a.budget = '.$budget;
                    // $this->session->set_userdata("budget_for_homepage",$budget);
					$this->budget_for_homepage = $budget;
                }else if($budget == '4689'){
                    $where_condition = $where_condition.' and a.bucket_id='.$bucket.' AND a.budget in (4688,4689)';
                    // $this->session->set_userdata("budget_for_homepage",'4688,4689');
					$this->budget_for_homepage = '4688,4689';
                }else if($budget == '4690'){
                    $where_condition = $where_condition.' and a.bucket_id='.$bucket.' AND a.budget in (4688,4689,4690)';
                    // $this->session->set_userdata("budget_for_homepage",'4688,4689,4690');
					$this->budget_for_homepage = '4688,4689,4690';
                }else if($budget == '4691'){
                    $where_condition = $where_condition.' and a.bucket_id='.$bucket.' AND a.budget in (4688,4689,4690,4691)';
                    // $this->session->set_userdata("budget_for_homepage",'4688,4689,4690,4691');
					$this->budget_for_homepage = '4688,4689,4690,4691';
                }
            }else if($question_id == '7'){
                if($budget == '36')
                {
                    $where_condition = $where_condition.' and a.bucket_id='.$bucket.' AND a.budget = '.$budget;
                    // $this->session->set_userdata("budget_for_homepage",$budget);
					$this->budget_for_homepage = $budget;
                }else if($budget == '37'){
                    $where_condition = $where_condition.' and a.bucket_id='.$bucket.' AND a.budget in (36,37)';
                    // $this->session->set_userdata("budget_for_homepage",'36,37');
					$this->budget_for_homepage = '36,37';
                }else if($budget == '38'){
                    $where_condition = $where_condition.' and a.bucket_id='.$bucket.' AND a.budget in (36,37,38)';
                    // $this->session->set_userdata("budget_for_homepage",'36,37,38');
					$this->budget_for_homepage = '36,37,38';
                }else if($budget == '4670'){
                    $where_condition = $where_condition.' and a.bucket_id='.$bucket.' AND a.budget in (36,37,38,4670)';
                    // $this->session->set_userdata("budget_for_homepage",'36,37,38,4670');
					$this->budget_for_homepage = '36,37,38,4670';
                }
            }else{
                $where_condition = $where_condition.' and a.bucket_id='.$bucket;
            }
			/*
            if($age!=''){
                $where_condition = $where_condition.' and (a.age='.$age.')';
                // $this->session->set_userdata("age_for_homepage",$age);
				$this->age_for_homepage = $age;
            }*/

            $query = $this->db->query('select distinct a.look_id from look_bucket_mapping as a,look as b where 1 and a.look_id = b.id and b.deflag = 0 and b.status = 1 '.$where_condition.' order by b.modified_datetime desc limit 0,3');
            $res = $query->result_array($query);
            
            $look_ids = array();
            if(!empty($res)){
                foreach ($res as $value) {
                    $look_ids[] = $value['look_id'];
                }
            }

            return $look_ids;
         }

    }

    function top_cat_depends_on_pa($user_id){
        $bucket = $this->bucket_for_homepage; 
        // $budget = $this->budget_for_homepage;
        // $age = $this->age_for_homepage; 
		$budget = 0;$age = 0;
        $data = array();

     
        $query = $this->db->query("select distinct object_id from (select object_id from pa_object_relationship where object_type='category' and pa_type='bucket' and object_value =".$bucket." union select object_id from pa_object_relationship where object_type='category' and pa_type='age' and object_value =".$age." union select object_id from pa_object_relationship where object_type='category' and pa_type='budget' and object_value IN (".$budget.")) as a,category as b where a.object_id=b.id and b.count >0 order by RAND() limit 0,3");
        $res = $query->result_array($query);
        //echo $this->db->last_query();

        if(!empty($res)){ $i = 0;
            foreach($res as $val){
                $data[$i]['cat'] = $val['object_id'];
                $data[$i]['type'] = 1;
                $i++;
            }
        }


        return $data;
    }


    function top_brands_depends_on_pa($user_id){
        $bucket = $this->bucket_for_homepage; 
        // $budget = $this->budget_for_homepage;
        // $age = $this->age_for_homepage; 
		$budget = 0;$age = 0;
        $data = array();


        $query = $this->db->query("select distinct object_id from (select object_id from pa_object_relationship where object_type='brand' and pa_type='bucket' and object_value =".$bucket." union select object_id from pa_object_relationship where object_type='brand' and pa_type='age' and object_value =".$age." union select object_id from pa_object_relationship where object_type='brand' and pa_type='budget' and object_value IN (".$budget.")) as a order by RAND() limit 0,3");
        $res = $query->result_array($query);
       
        if(!empty($res)){ 
            foreach($res as $val){
                $data[] = $val['object_id'];
                
            }
        }


        return $data;
    }

    function get_latest_home_page_products(){
        $query = $this->db->query("SELECT id,name,price,image,slug FROM  product_desc where status = 1 and is_delete = 0  order by RAND(), modified_datetime desc limit 10;");

        return $query->result_array($query);
    }
	
	function product_count($cat_type,$category_id,$look_type){
        $where_cond = '';
        if($cat_type == 1){ $cat_table_name = 'product_category'; $product_table='product_desc'; }
        else if($cat_type == 2){ $cat_table_name = 'product_sub_category'; $product_table='product_desc'; }
        else if($cat_type == 3){ $cat_table_name = 'product_variation_type'; $product_table='product_variations'; }
        else if($cat_type == 4){ $cat_table_name = 'product_variation_value'; $product_table='product_variations'; }

        if($category_id > 0 && $product_table != ''){
               if($product_table == 'product_desc'){
                    if($cat_type == '1'){ 
                        $where_cond = $where_cond.' and product_cat_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and product_sub_cat_id='.$category_id; 
                    }
                    if($look_type == 'men') { $where_cond = $where_cond.' and product_cat_id=12';   }
                    else if($look_type == 'women') { $where_cond = $where_cond.' and product_cat_id=1';   }
                    $query = $this->db->query('select count(id) as cnt  from product_desc WHERE  approve_reject = "A" and is_delete = 0 '.$where_cond.'');
                    $product_data = $query->result_array();
                    
                }else if($product_table == 'product_variations'){
                    if($cat_type == '3'){ 
                        $where_cond = $where_cond.' and b.product_var_type_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and b.product_var_val_id='.$category_id; 
                    }
                    if($look_type == 'men') { $where_cond = $where_cond.' and a.product_cat_id=12';   }
                    else if($look_type == 'women') { $where_cond = $where_cond.' and a.product_cat_id=1';   }
                    $query = $this->db->query('select count(a.id) as cnt from product_desc as a,product_variations as b WHERE a.approve_reject = "A" and a.is_delete = 0 and a.id = b.product_id '.$where_cond.'');
                    $product_data = $query->result_array();
                } 
                if(!empty($product_data)){
                    return $product_data[0]['cnt'];
                }else{
                    return 0;
                }
        }

    }
	
 }