<?php
class Scborough_model extends MY_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->model('Looks_model');
    }

    function get_homepage_looks($stylist_offset,$street_offset,$blog_offset,$filter_value,$filter_value1,$filter_value2,$filter_value3){
        
         $orderSort = '';
         $sortby = '';
         $where_cond = '';
         $where_cond_cat ='';

        $where_cond_cat = '';
        $where_cond = '';
        if(BOROUGH_BRANDS!='')
        {
            $brand = BOROUGH_BRANDS;
        }else
        {
            $brand = '';
        }

        if(BOROUGH_PRODUCTS!='')
        {
            $products = BOROUGH_PRODUCTS;
        }else
        {
            $products = '';
        }

       /* if($filter_value1 == 'ASC')
        {
            $orderSort = 'a.price ASC,';
        }else if($filter_value1 == 'DESC')        
        {
            $orderSort = 'a.price DESC,';
        }else
        {
            $orderSort = '';
        }*/
        

         /*
            PRODUCT WITHOUT FILTER
        */

        if($blog_offset >=0 && $filter_value == ''){

        //$blog_off = $this->config->item('product_look')*$blog_offset;  
        $blog_off = 0;      

        $query = $this->db->query('Select distinct a.`id`,a.brand_id,a.name,a.url,a.price,a.image,c.company_name,c.`is_paid`,a.`description`,a.`product_sub_cat_id`,a.`slug`,(select n.`size_guide` from `product_variations` as m,`product_variation_type` as n where m.`product_var_type_id` = n.`id` and m.`product_id` = a.`id` order by m.id desc limit 0,1) as size_guide from product_desc as a,brand_info as c where a.status = 1  and a.price>0 and c.`user_id` = a.`brand_id`  and  a.`id` IN ('.$products.') '.$where_cond.' '.$where_cond_cat.'   order by '.$sortby.' c.is_paid DESC');

        $result_blog = $query->result_array();
       
        $result['offset']['product'] = ($blog_offset+1);

        }

        /*
            PRODUCT WITH FILTER
        */

        if($blog_offset >=0 && $filter_value == 5){

        //$blog_off = $this->config->item('product_look')*$blog_offset;
             $blog_off = 0;    
       

        if($filter_value3 == 'ASC')
        {
            $sortby = ' a.price ASC, ';
        }else if($filter_value3 == 'DESC')
        {
            $sortby = ' a.price DESC, ';
        }else
        {
           $sortby = '';
        }      

        if($filter_value1 == 'brand')
        {
            if($filter_value2!='')
            {              
              $brand = $filter_value2; 
              $where_cond = '';
            }else
            {
                $where_cond = '';
            }
            
        }else if($filter_value1 == 'category')
        {
            if($filter_value2!='')
            {
                $where_cond_cat = 'and a.`product_sub_cat_id`='.$filter_value2;
             }else
            {
                $where_cond_cat = '';
            }
        }else
        {
            $where_cond = '';
        }

       
         $query = $this->db->query('Select distinct a.`id`,a.brand_id,a.name,a.url,a.price,a.image,c.company_name,c.`is_paid`,a.`description`,a.`product_sub_cat_id`,a.`slug`,(select n.`size_guide` from `product_variations` as m,`product_variation_type` as n where m.`product_var_type_id` = n.`id` and m.`product_id` = a.`id` order by m.id desc limit 0,1) as size_guide from product_desc as a,brand_info as c where a.status = 1  and a.price>0 and c.`user_id` = a.`brand_id`  and  a.`id` IN ('.$products.') '.$where_cond.' '.$where_cond_cat.'   order by '.$sortby.' c.is_paid DESC ');        
        
        $result_blog = $query->result_array();
        $result['offset']['product'] = ($blog_offset+1);

        }
    
        if(!empty($result_blog)) { $o = 0;
            foreach($result_blog as $val){
                $result['product'][$o]['id'] = $val['id'];
                $result['product'][$o]['name'] =$val['name'];
                $result['product'][$o]['url'] =$val['url'];
                $result['product'][$o]['price'] =$val['price'];
                $result['product'][$o]['image'] =$val['image'];
                $result['product'][$o]['company_name'] =$val['company_name'];
                $result['product'][$o]['is_paid'] =$val['is_paid'];
                $result['product'][$o]['description'] =$val['description'];
                $result['product'][$o]['discount_percent'] = $this->Looks_model->get_discount_percentage($val['brand_id'],$val['id']);
                $result['product'][$o]['discount_price'] =($val['price']*$result['product'][$o]['discount_percent'])/100;
                $result['product'][$o]['size_guide'] = $this->Looks_model->get_size_guide($val['id']);
                $result['product'][$o]['brand_id'] = $val['brand_id'];
                $result['product'][$o]['product_sub_cat_id'] = $val['product_sub_cat_id'];
                $result['product'][$o]['slug'] = $val['slug'];
               /* $query = $this->db->query("select distinct a.`product_id`,c.`size_text`,b.stock_count,c.`id` from `look_products` as a,`product_inventory` as b,`product_size` as c where a.`product_id` = b.`product_id` and b.`size_id` = c.`id`  and b.stock_count > 0");*/
                $query = $this->db->query("select distinct a.`id` as product_id,c.`size_text`,b.stock_count,c.`id` from `product_desc` as a,`product_inventory` as b,`product_size` as c where a.`id` = b.`product_id` and b.`size_id` = c.`id`  and b.stock_count > 0");
                $res = $query->result_array();
                 $product_inv = array();
                if(!empty($res)){                    
                    foreach($res as $val)
                    {
                        $product_inv[$val['product_id']]['size_text'][$val['id']] = $val['size_text'];                                              
                    }
                }
                $result['products_inventory'] =$product_inv;                
            $o++; }
        }

 		return $result;
	}
   
    function get_product_count(){
        if(BOROUGH_PRODUCTS!='')
        {
            $products = BOROUGH_PRODUCTS;
        }else
        {
            $products = '';
        }
        $query = $this->db->query('Select distinct a.`id`,a.brand_id,a.name,a.url,a.price,a.image,c.company_name,c.`is_paid`,a.`description`,a.`slug`,(select n.`size_guide` from `product_variations` as m,`product_variation_type` as n where m.`product_var_type_id` = n.`id` and m.`product_id` = a.`id` order by m.id desc limit 0,1) as size_guide from product_desc as a,brand_info as c where a.status = 1  and a.price>0 and c.`user_id` = a.`brand_id` and  a.`id` IN ('.$products.')  order by c.is_paid desc ');
        $stylist_look = $query->result_array();
        return $stylist_look;
    }   

    public function category_list()
    {
        $cat ='';
        if($cat== 'women')
        {
             $where_cond = "AND b.product_cat_id = '".WOMEN_CATEGORY_ID."'";
        }else if($cat== 'men')
        {
             $where_cond = "AND b.product_cat_id = '".MEN_CATEGORY_ID."'";
        }else
        {
            $where_cond = "";
        }

        if(BOROUGH_PRODUCTS!='')
        {
            $products = BOROUGH_PRODUCTS;
        }else
        {
            $products = '';
        }
     
        $query = $this->db->query('select distinct a.id,a.name,a.slug from `product_sub_category` as a,`product_desc` as b WHERE a.`id` = b.`product_sub_cat_id` and  b.`id` IN ('.$products.') '.$where_cond.' and a.`status`=1 and a.`is_delete`=0');
        $result  = $query->result();  
        return  $result; 
    }

    public function get_all_brands(){
        $brand = BOROUGH_BRANDS;
        if(BOROUGH_PRODUCTS!='')
        {
            $products = BOROUGH_PRODUCTS;
        }else
        {
            $products = '';
        }
        $query = $this->db->query("select distinct a.`user_id`, a.`company_name`,b.`user_name` from `brand_info` as a,`user_login` as b,`user_info` as c, `product_desc` as d where a.`user_id` = b.`id` and a.`user_id` = c.`user_id` and d.`brand_id`=  a.`user_id` and  d.`id` IN (".$products.")   order by a.company_name");        
        $result = $query->result();
        return $result;
    }
	
	function get_user_info($user_id){

        $query = $this->db->get_where('user_login', array('id' => $user_id,'status'=>1));
        $result = $query->result_array();
		return $result;
	}
	
	function login_user($email,$password){

        $query = $this->db->get_where('user_login', array('email' => $email,'status'=>1));
        $result = $query->result();
        if(!empty($result)){
        $salt = substr($result[0]->password, 0, 10);

        $db_password =  $salt . substr(sha1($salt . $password), 0, -10);

        if($password == $result[0]->password){

            $query = $this->db->query("select user_role_mapping.role_id,user_info.user_id,user_info.created_datetime,user_info.first_name,user_info.last_name,user_login.email,user_login.bucket,user_login.question_comp,user_login.question_resume_id,user_login.user_name,user_info.gender,user_info.gender_c,user_info.contact_no from user_info,user_role_mapping,user_login where user_info.user_id = ".$result[0]->id." and user_info.user_id = user_role_mapping.user_id and user_info.user_id = user_login.id");
            $res = $query->result();
            if(!empty($res)){

                $data_attempts = array('user_id'=>$res[0]->user_id,'login_time'=>$this->config->item('sc_date'));
                $this->db->insert('user_login_attempts', $data_attempts);
                $login_attempts_id = $this->db->insert_id();

                $array_field=array('user_name' => $res[0]->user_name,'user_id' => $res[0]->user_id,'role_id'=>$res[0]->role_id,'first_name'=>$res[0]->first_name,'last_name'=>$res[0]->last_name,'bucket'=>$res[0]->bucket,'question_comp'=>$res[0]->question_comp,'question_resume_id'=>$res[0]->question_resume_id,'login_attempts_id'=>$login_attempts_id,'email' => $res[0]->email,'gender' => $res[0]->gender,'gender_c' => $res[0]->gender_c,'contact_no' => $res[0]->contact_no,'utm_source1'=>$this->session->userdata('utm_source1'),'is_app'=>1);

                $this->session->set_userdata($array_field);



                return 1;
            }else{
                return 2;
            }
        }else{
                return 2;
            }
        }else{ return 2; }
    }

 }
?>
