<?php
class Scbeatout_model extends MY_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function add_to_cart($uniquecookie,$productid,$user_id){
       
       $query = $this->db->query('select a.id as cart_id,c.company_name,a.product_id,a.product_qty as quantity,a.product_size,b.name as productname,b.price,b.image as image,b.slug from `cart_info` as a ,`product_desc` as b,brand_info as c where b.brand_id = c.user_id AND a.`product_id`=b.`id` AND (a.SCUniqueID = "'.$uniquecookie.'" AND a.`product_id` ="'.$productid.'")');
        $res = $query->result_array();
        return $res;
    } 

	function removed_from_cart($uniquecookie,$cartid){
       
       $query = $this->db->query('select a.id as cart_id,c.company_name,a.product_id as product_id,a.product_qty as quantity,a.product_size,b.name as productname,b.price,b.image as image,b.slug from `cart_info` as a ,`product_desc` as b,brand_info as c where b.brand_id = c.user_id AND a.`product_id`=b.`id` and (a.SCUniqueID = "'.$uniquecookie.'" and a.`id` ="'.$cartid.'")');
        $res = $query->result_array();
        return $res;
    }
}

?>
