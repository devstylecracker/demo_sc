<?php
class Product_desc_model extends MY_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_product($product_id){
        //----Product description
       
        $this->db->select("'$product_id' product_id,ul.user_name brand_slug, pd.price, bi.user_id brand_id,pd.slug, bi.company_name brand_name, bi.long_description brand_description, pd.`name` product_name, pd.description product_description,pd.image,pd.product_cat_id,pd.product_sub_cat_id,bi.`return_policy`,bi.min_del_days,bi.max_del_days,CASE 
        WHEN  pd.discount_start_date <= CURRENT_TIMESTAMP() and pd.discount_end_date >= CURRENT_TIMESTAMP() then pd.compare_price
        ELSE 0
    END AS compare_price");
        $this->db->from('product_desc pd');
        $this->db->join('brand_info bi', 'bi.user_id = pd.brand_id','left');
        $this->db->join('user_login ul', 'pd.brand_id = ul.id','left');
        $this->db->where("pd.id = '$product_id' and pd.status = 1");
        $result = $this->db->get()->result_array();
        if($result)
        $data['products_details'] = $result[0];
        /*echo "<pre>";
        print_r($data['products_details'] );
        exit;*/
        //----Product Extra images
        $this->db->select('product_images');
        $this->db->from('extra_images');
        $this->db->where("product_id = '$product_id'");
        $data['product_extra_images'] = $this->db->get()->result_array();

        //----Product Inventory
        $this->db->select('DISTINCT(pi.id) as id, pi.product_sku, ps.id size_id, size_text');
        $this->db->from('product_inventory pi');
        $this->db->join('product_size ps', 'ps.id= pi.size_id','left');
        $this->db->where("pi.product_id='$product_id' and pi.stock_count > 0");
        $this->db->order_by("ps.order", "asc"); 
        $data['product_size'] = $this->db->get()->result_array();

        //----Product Size Guid
        $this->db->select('n.size_guide');
        $this->db->from('product_variations as m');
        $this->db->join('product_variation_type as n', 'm.product_var_type_id = n.id','left');
        $this->db->where("m.product_id = '$product_id'");
        $this->db->order_by("n.size_guide", "desc"); 
        $this->db->limit(1);

        /* get categories */
        $query = $this->db->query("select a.category_id,b.category_name,b.size_guide from category_object_relationship as a,category as b where a.category_id = b.id and a.object_type = 'product' and a.object_id =$product_id order by a.category_id asc");
        $res = $query->result_array();
        $product_breadcrumps = '';
        $size_guide_id = '';

        if(!empty($res)){
            foreach ($res as $value) {
               $product_breadcrumps = $product_breadcrumps.'<li><a href="'.base_url().'all-products/'.$this->Product_desc_model->get_slug($value['category_id']).'">'.$value['category_name'].'</a></li>';
                if($value['size_guide']!=0){
                    $size_guide_id = $value['size_guide'];
                }
               $this->slug = '';
            }
          }

        $data['cat_info'] = $product_breadcrumps;

        $data['product_size_guide'] = $this->db->get()->result_array();
        /*if(!isset($data['product_size_guide'][0]))
            $data['product_size_guide']['size_guide'] = "";
        else
            $data['product_size_guide'] = $data['product_size_guide'][0];*/
        $data['product_size_guide']['size_guide'] = $size_guide_id;
        /*echo $this->db->last_query();
        echo "<pre>";
        print_r($data);
        exit;*/

        return $data;
    }

    function add_users_product_click_track($data){
        if(!isset($data['user_id']))
            @$user_id = $this->session->userdata('user_id');
        else
            @$user_id = $data['user_id'];
        @$product_id = $data['product_id'];
        $click_count = 1;
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $ip .= " | ".$_SERVER ['HTTP_USER_AGENT'];
        if($product_id)
            $this->db->query("insert into users_product_click_track (`user_id`,`product_id`,`click_source`,`click_count`,`unique_id`) values('$user_id','$product_id','5','$click_count','$ip')");
    }

    function get_product_related_looks($product_id){
        $sql = "select z.id,(select sum(pds.price) from look_products lp,product_desc pds where lp.product_id = pds.id and lp.look_id = z.id and pds.approve_reject='A' and pds.status=1) look_price,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.description,z.status,z.deflag,z.slug from(SELECT a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.description,a.status,a.deflag,a.slug FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 and a.id in (SELECT look_id FROM look_products where product_id='$product_id')and a.`status`=1) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 OR z.look_type = 6) limit 0,6";
        $result = $this->db->query($sql);
        $result = $result->result_array();
        
        if(empty($result)){

            $cat_id = ''; $productids = ''; $where_cond = '';
            $sql = $this->db->query("select category_id from category_object_relationship where object_id=".$product_id." and object_type='product' order by category_id desc");
            $result = $sql->result_array();
        
            if(!empty($result) && $result[0]['category_id']){
                $cat_id = ' and d.category_id IN ('.$result[0]['category_id'].')';
            }

            $sql ="select distinct a.`id`,a.brand_id,a.name,a.slug,a.url,a.price,a.image,c.company_name,c.`is_paid`,a.`description`,a.compare_price  from product_desc as a,brand_info as c,category_object_relationship as d where a.status = 1 and a.is_delete = 0 and a.price>0 and c.`user_id` = a.`brand_id` and d.object_id=a.id and d.object_type='product' ".$cat_id." order by a.modified_datetime desc";

            $result = $this->db->query($sql);
            $result = $result->result_array();
            if(!empty($result)){ $i = 0;
                foreach($result as $val){
                    if($i!=0){ $productids = $productids.','; }
                    $productids = $productids.$val['id'];
                    $i++;
                }
            }

            if($productids!='') { $where_cond = ' and a.id in (SELECT look_id FROM look_products where 1 and product_id IN ('.$productids.'))'; }
              $sql = "select z.id,(select sum(pds.price) from look_products lp,product_desc pds where lp.product_id = pds.id and lp.look_id = z.id and pds.approve_reject='A' and pds.status=1) look_price,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.description,z.status,z.deflag,z.slug from(SELECT a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.description,a.status,a.deflag,a.slug FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 ".$where_cond." and a.`status`=1) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 OR z.look_type = 6) limit 0,6";

            $result = $this->db->query($sql);
            $result = $result->result_array();
            return $result;

        }else{
           return $result;
        }

    }

    function get_product_related_products($product_id,$product_related){
        $cat_id = '';
        /*$sql = $this->db->query("select GROUP_CONCAT(category_id) as category_id from category_object_relationship where object_id=".$product_id." and object_type='product' group by object_id");*/

        $sql = $this->db->query("select category_id from category_object_relationship where object_id=".$product_id." and object_type='product' order by category_id desc");

        $result = $sql->result_array();
        
        if(!empty($result) && $result[0]['category_id']){
            $cat_id = ' and d.category_id IN ('.$result[0]['category_id'].')';
        }

        $sql ="select distinct a.`id`,a.brand_id,a.name,a.slug,a.url,a.price,a.image,c.company_name,c.`is_paid`,a.`description`,a.compare_price  from product_desc as a,brand_info as c,category_object_relationship as d where a.status = 1 and a.is_delete = 0 and a.price>0 and c.`user_id` = a.`brand_id` and d.object_id=a.id and d.object_type='product' and a.id !='".$product_id."' ".$cat_id." order by a.modified_datetime desc limit 6";

        $result = $this->db->query($sql);
        return $result->result_array();
    }
    
    function set_product(){
        
    }

    function update_product(){
        
    }

    function get_sub_category_info($cat_id=null){
        if($cat_id!=''){
            $sql = "select id,slug,name,product_cat_id from `product_sub_category` where id=".$cat_id;
            $result = $this->db->query($sql);
            return $result->result_array();
        }
    }

	function get_productDiscount_price($product_id,$brand_id)
    {
        $discount_price = 0;
        $product_discount = array();
        if($product_id !='' && $brand_id !=''){
            $query = $this->db->query("select (select price from product_desc where id = '".$product_id."') as product_price,discount_percent from discount_availed as da where (da.brand_id = '".$brand_id."' OR da.product_id = '".$product_id."') AND da.end_date >= NOW() AND da.start_date<= NOW()  AND da.status=1");
            /*echo $this->db->last_query();*/
            $result = $query->result_array();

            if(!empty($result)){
                $discount_percent = $result[0]['discount_percent'];
                $product_price = $result[0]['product_price'];
                $discount_price =($product_price*$discount_percent)/100;
                $discounted_product_price = $product_price - $discount_price;

                $product_discount['discount_percent'] = $discount_percent;
                $product_discount['discounted_product_price'] = round($discounted_product_price);
                return $product_discount;
            }else{
                return 0;
            }
        }else{ return 0; }
    }

      function get_slug($id){
        
        if($id!='-1'){
        $query = $this->db->query('select category_slug,category_parent from category where id = "'.$id.'"');
        $res = $query->result_array();
        if(!empty($res)){
            foreach($res as $val){

                $this->slug = $val['category_slug'].'/'.@$this->slug; 
                
                $this->get_slug($val['category_parent']);

            }
            return  $this->slug ;
        }
        }
        
    }
	
	function calculate_discount_percent($compare_price,$discount_price,$product_price=null){
		// echo "comming".$compare_price.'----'.$discount_price;exit;
		$discount_percent = 0;
		if($compare_price > 0 && $discount_price > 0){
			$discount_percent = round((1-($discount_price/$compare_price))*100);
		}else if($compare_price > 0 && $product_price > 0){
			$discount_percent = round((1-($product_price/$compare_price))*100);
		}
		
		return $discount_percent;
	}
	
	function get_brand_name($brand_name,$product_id,$store_id=null){
		
		if($brand_name != ''){	
		$res = $this->db->get_where('brand_info', array('company_name' => $brand_name));
	    $data = $res->result_array();	
		}
		if($store_id>0){
			$query = $this->db->query("SELECT a.brand_store_name FROM `brand_store` as a where a.id = '".$store_id."' ");
			$res = $query->result_array();
			if(!empty($res)){ 
					return $res[0]['brand_store_name']; 
				}else{
					return $brand_name;
				}
		}
		// echo $brand_name;exit;
		if(!empty($data)){
			if(@$data[0]['store_type'] == 3){
				$query = $this->db->query("select a.brand_store_name from brand_store as a,product_store as b where b.product_id = '".$product_id."' AND b.store_id = a.id AND a.brand_id = '".$data[0]['user_id']."' ");
				$res = $query->result_array();
				if(!empty($res)){ 
					return $res[0]['brand_store_name']; 
				}else{
					return $brand_name;
				}
			}else{
				return $brand_name;
			}
		}else return $brand_name;
	}
	
	function get_product_id($product_id,$size_id){
		$query = $this->db->query("select  a.id as product_id,a.price,a.compare_price,a.name from product_desc as a,attribute_object_relationship as b,attribute_values_relationship as c where ( a.id = ".$product_id." || a.parent_id = ".$product_id.") AND a.id = b.object_id AND b.attribute_id = c.id AND c.attribute_id = ".ATTRIBUTE_SIZE_ID." AND c.id = ".$size_id."");
		$res = $query->result_array();
		if(!empty($res)) return $res; else return 0;
	}
		
	function insert_review($data){
		if($data['user_id'] >0){
			$this->db->insert('review_table', $data);
			return 1;
		}else return 0;
		
	}
	
	function get_review($object_id,$limit){
		if($object_id >0){
			$query = $this->db->query('select * from review_table where object_id = '.$object_id.' order by id desc '.$limit.' ');
			$res = $query->result_array();
			if(!empty($res)){ return $res; }else return null;
		}else{
			return null;
		}
		
		
	}
	
	/* This function will create the Category Url for the website. The URL basically show all the parents of that catgory which will help us for the SEO score */
    function product_breadcrumps($product_id){
		
    	/* get categories */
        $query = $this->db->query("select a.category_id,b.category_name,b.size_guide,b.category_slug from category_object_relationship as a,category as b where a.category_id = b.id and a.object_type = 'product' and a.object_id =$product_id order by a.category_id asc");
        $res = $query->result_array();
        $product_breadcrumps = '';

        if(!empty($res)){
            foreach ($res as $value) {
               $product_breadcrumps = $product_breadcrumps.'<li><a href="'.base_url().'category/product-filter/'.$value['category_slug'].'-'.$value['category_id'].'">'.$value['category_name'].'</a></li>';
			   $this->slug = '';
            }
          }
		
        return $product_breadcrumps;
    }
 }
?>
