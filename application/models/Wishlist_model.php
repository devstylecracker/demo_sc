<?php
class Wishlist_model extends MY_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function show_my_wishlist_looks($user_id){
    	if($user_id > 0){
    		  $query = $this->db->query('select o.slug,o.id,o.product_img,o.look_type,o.image,o.look_image,name,o.created_datetime,o.modified_datetime,o.status,o.deflag from
(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) )as o,look_fav as h where o.`id` = h.`look_id` and h.`user_id` = '.$user_id.' order by o.modified_datetime limit 0,12');
    		  $res = $query->result_array();
    		  return $res;
    	}
    }

    function show_my_wishlist_looks_total($user_id){
    	if($user_id > 0){
    		  $query = $this->db->query('select count(distinct o.id) as cnt from
(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) )as o,look_fav as h where o.`id` = h.`look_id` and h.`user_id` = '.$user_id.' order by o.modified_datetime limit 0,12');
    		  $res = $query->result_array();
    		  if(!empty($res)){
    		  	return $res[0]['cnt'];
    		  }else{
    		  	return 0; 
    		  }
    	}
    }

    function show_my_more_wishlist_looks($offset,$user_id){
    	if($offset < 0){ $offset = 0; }
    	$offset = $offset*12;
    	if($user_id > 0){
    		  $query = $this->db->query('select o.slug,o.id,o.product_img,o.look_type,o.image,o.look_image,name,o.created_datetime,o.modified_datetime,o.status,o.deflag from
(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) )as o,look_fav as h where o.`id` = h.`look_id` and h.`user_id` = '.$user_id.' order by o.modified_datetime limit '.$offset.',12');
    		  $res = $query->result_array();
    		  return $res;
    	}
    }

    function show_my_wishlist_brands($user_id){

    	if($user_id > 0){
	    	$query = $this->db->query('SELECT a.*,b.`user_name` FROM `brand_info` as a,user_login as b,favorites as c WHERE 1 and a.`is_paid` = 1 and a.`user_id` = b.`id` and c.`fav_id` = a.`user_id` and c.`fav_for` = "brand" and c.`is_deleted` = 0 and c.`user_id` = "'.$user_id.'" order by a.`user_id` desc');
	        $res = $query->result_array(); 
	        return $res;
    	}
    }

    function show_my_wishlist_products($user_id){
    	if($user_id > 0){
	    	$query = $this->db->query('select a.id,a.name,a.price,a.image,a.slug from product_desc as a,favorites as c WHERE (a.approve_reject = "A" ||a.approve_reject = "D") and a.is_delete = 0 and a.id = c.fav_id and c.`fav_for` = "product" and c.`is_deleted` = 0 and c.`user_id` = "'.$user_id.'" order by a.id desc');
	        $res = $query->result_array(); 
	        return $res;
    	}
    }

    function show_recommended_looks($user_id){
        $bucket = $this->session->userdata('bucket');
        if($bucket > 0){
            $where_condition = ' and m.bucket_id='.$bucket;
            $query = $this->db->query('select distinct k.id,k.slug,product_img,k.look_type,k.image,look_image,name,k.created_datetime,k.modified_datetime,k.status,k.deflag,k.stylist_id from
(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag,z.`stylist_id` from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag,a.`stylist_id` FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc) as k,look_bucket_mapping as m where k.id = m.look_id '.$where_condition.' order by k.modified_datetime desc limit 0,3');
            $res = $query->result_array(); 
            return $res;

        }
    }

    function show_recommended_products($user_id){
         $bucket = $this->session->userdata('bucket');
        if($bucket > 0){
            $where_condition = ' and m.bucket_id='.$bucket;
            $query = $this->db->query('select distinct k.id,k.slug,product_img,k.look_type,k.image,look_image,name,k.created_datetime,k.modified_datetime,k.status,k.deflag,k.stylist_id from
(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag,z.`stylist_id` from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag,a.`stylist_id` FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc) as k,look_bucket_mapping as m where k.id = m.look_id '.$where_condition.' order by k.modified_datetime desc limit 0,3');
            $res = $query->result_array(); 
            $look_id = ''; $i = 0;
            if(!empty($res)){
                foreach ($res as $value) {
                  
                    if($i != 0) { $look_id = $look_id.','; }
                    $look_id = $look_id.$value['id'];
                    $i++;

                }
            }

            if($look_id!=''){
                $query = $this->db->query('select a.id,a.name,a.price,a.image,a.slug from product_desc as a,look_products as c WHERE a.approve_reject = "A" and a.is_delete = 0 and a.id = c.product_id and c.look_id in ('.$look_id.') order by a.id desc limit 0,3');
                $res = $query->result_array(); 
                return $res;
            }

        }
    }

    function show_recommended_brands($user_id){
          $bucket = $this->session->userdata('bucket'); 
        if($bucket > 0){
            $where_condition = ' and m.bucket_id='.$bucket;
            $query = $this->db->query('select distinct k.id,k.slug,product_img,k.look_type,k.image,look_image,name,k.created_datetime,k.modified_datetime,k.status,k.deflag,k.stylist_id from
(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag,z.`stylist_id` from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag,a.`stylist_id` FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc) as k,look_bucket_mapping as m where k.id = m.look_id '.$where_condition.' order by k.modified_datetime desc limit 0,3');
            $res = $query->result_array(); 
            $look_id = ''; $i = 0; $brand_id=0;
            if(!empty($res)){
                foreach ($res as $value) {
                  
                    if($i != 0) { $look_id = $look_id.','; }
                    $look_id = $look_id.$value['id'];
                    $i++;

                }
            }

            if($look_id!=''){
                $query = $this->db->query('select distinct a.brand_id from product_desc as a,look_products as c WHERE a.approve_reject = "A" and a.is_delete = 0 and a.id = c.product_id and c.look_id in ('.$look_id.') order by a.id desc');
                $res = $query->result_array(); 
                if(!empty($res)){ $k = 0; 
                    foreach($res as $val){
                        if($k != 0){ $brand_id = $brand_id .','; }
                        $brand_id = $brand_id.$val['brand_id'];
                        $k++;
                    }   
                }
            }

            if($brand_id!=''){
                $query = $this->db->query('SELECT a.*,b.`user_name` FROM `brand_info` as a,user_login as b WHERE 1 and a.`is_paid` = 1 and a.`user_id` = b.`id` and a.`user_id` IN ('.$brand_id.') order by a.`user_id` desc limit 0,3');
                $res = $query->result_array(); 
                return $res;
            }

        }
    }
}
?>