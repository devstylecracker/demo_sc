<?php
class Pa_model extends MY_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


     function get_question_img_answers($id,$gender=0,$style=null){
        $result = array();
        /*if($id){
			$img_link = '';
			$pa_image_sets = array("set1/", "set2/", "set3/");
			$rand_set = $pa_image_sets[array_rand($pa_image_sets)];
			if($gender != 0 && $gender == 1){ if($id == '1'){ $img_link = PA_FEMALE; }else { $img_link = PA_FEMALE_STYLE.$rand_set; } }else { if($id == '24'){ $img_link = PA_MALE; }else { $img_link = PA_MALE_STYLE.$rand_set; } }
            if($style!='')
            {
                $query = $this->db->query('SELECT a.id,a.answer,a.description,CONCAT("'.$img_link.'",c.image_name) as image_name FROM `answers` as a, `question_answer_assoc`  as b, `answer_assoc_image` as c where a.`id` = b.`answer_id` AND a.`id`= c.`answer_id` AND a.`status` =1 AND b.`question_id` ='.$id.' and a.id ='.$style);
            }else
            {
                $query = $this->db->query('SELECT a.id,a.answer,a.description,CONCAT("'.$img_link.'",c.image_name) as image_name FROM `answers` as a, `question_answer_assoc`  as b, `answer_assoc_image` as c where a.`id` = b.`answer_id` AND a.`id`= c.`answer_id` AND a.`status` =1 AND b.`question_id` ='.$id);
            }

            $result = $query->result_array();          
        }*/
        return $result;
    }

    function get_question_answers($id){
        $result = array();
        /*if($id){
            $query = $this->db->query('SELECT a.id,a.answer,a.description FROM `answers` as a, `question_answer_assoc`  as b where a.`id` = b.`answer_id` AND a.`status` =1 AND b.`question_id` ='.$id);

            $result = $query->result_array();           
        }*/
         return $result;
    }

    function get_questions($id){
        if($id){
            $query = $this->db->query('select question from questions where `id` ='.$id);
            $result = $query->result_array();
            return @$result[0]['question'];
        }

    }

    function get_users_answer($question_id,$user_id=null,$question=0){
        $result = array();
        /*if($question_id){
            if($this->session->userdata('user_id') > 0) $user_id = $this->session->userdata('user_id');
            // $query = $this->db->query('select answer_id from users_answer where user_id = '.$user_id.' and question_id = '.$question_id);
			$query = $this->db->query('select a.answer_id,b.answer from users_answer as a,answers as b where a.user_id = '.$user_id.' and a.question_id = '.$question_id.' AND a.answer_id = b.id  AND a.user_pref = "'.$question.'" ');
            $result = $query->result_array();
        }*/
         return $result;
    }

    function update_pa($question_id,$intCurrentTypeID,$user_id=0,$gender=0,$question=0){
        if($question_id!='' && $intCurrentTypeID!=''){
            if($this->session->userdata('user_id') > 0) $user_id = $this->session->userdata('user_id');

            if(!empty($this->get_users_answer($question_id,$user_id,$question))){
                // Update the PA

                $data = array('answer_id'=>$intCurrentTypeID,'modified_by'=>$user_id,'is_active'=>1);
               // $this->db->update('users_answer', $data, array('question_id' => $question_id,'user_id' => $user_id,'user_pref' => $question ));
                if($question_id == 1 || $question_id == 2 || $question_id == 13 || $question_id == 14 || $question_id == 22){
                    $this->assign_bucket($user_id,$gender);
                    $this->update_question_resume_id($question_id,$user_id);
                    return true;
                }else{
                     $this->update_question_resume_id($question_id,$user_id);
                     return true;

                }

            }else{
                /*Insert the PA*/

                $data = array('answer_id'=>$intCurrentTypeID,'question_id' => $question_id,'user_id' => $user_id,'user_pref' => $question,'created_by'=>$user_id,'created_datetime'=>$this->config->item('sc_date'),'is_active'=>1);

                //$this->db->insert('users_answer', $data);
                if($question_id == 1 || $question_id == 2 || $question_id == 13 || $question_id == 14 || $question_id == 15 || $question_id == 16 || $question_id == 22){
                    $this->assign_bucket($user_id,$gender);
                    $this->update_question_resume_id($question_id,$user_id);
                    return true;
                }else{
                    $this->update_question_resume_id($question_id,$user_id);
                    return true;
                }

            }
        }
    }



    function assign_bucket($user_id=0,$gender=0){
         if($this->session->userdata('user_id') > 0) $user_id = $this->session->userdata('user_id');
         if($user_id!=''){
            $body_shape_ans = $this->get_users_answer(1,$user_id);
            $body_style_ans = $this->get_users_answer(2,$user_id);
            
            if(!empty($body_shape_ans) && !empty($body_style_ans)){
                $bucket_id = $this->get_bucket_id($body_shape_ans[0]['answer_id'],$body_style_ans[0]['answer_id']);

                $data = array('bucket'=>$bucket_id,'modified_by'=>$user_id);
                $this->db->update('user_login', $data, array('id' => $user_id ));
                $this->session->set_userdata('bucket', $bucket_id);
            }
            
            $body_shape_ans_mens = $this->get_users_answer(13,$user_id);
            $body_style_ans_mens = $this->get_users_answer(14,$user_id);
            $body_work_style = $this->get_users_answer(15,$user_id);
            $body_personal_style = $this->get_users_answer(16,$user_id);
            if(!empty($body_shape_ans_mens) && !empty($body_style_ans_mens) && !empty($body_work_style) && !empty($body_personal_style)){
                $bucket_id = $this->get_bucket_id($body_shape_ans_mens[0]['answer_id'],$body_style_ans_mens[0]['answer_id'],$body_work_style[0]['answer_id'],$body_personal_style[0]['answer_id']);
                $data = array('bucket'=>$bucket_id,'modified_by'=>$user_id);
                $this->db->update('user_login', $data, array('id' => $user_id ));
                $this->session->set_userdata('bucket', $bucket_id);
            }

         }
    }

    function get_gender($id){
        $result = $this->db->query("SELECT *FROM user_info where user_id = ".$id);
        $result = $result->result_array();
        $result[0]['gender'];
        return $result[0]['gender'];

    }
    function get_bucket_id($body_shape_ans,$body_style_ans,$body_work_style,$body_personal_style){
		
		if($body_shape_ans!='' && $body_style_ans!='' && $body_work_style!='' && $body_personal_style!=''){
			
            $query = $this->db->query('select id from bucket where `body_shape`="'.$body_shape_ans.'" and `style`="'.$body_style_ans.'" AND work_style = "'.$body_work_style.'" AND personal_style = "'.$body_personal_style.'" ');
            $result = $query->result_array();
            if(!empty($result)){ return $result[0]['id']; }else{ return 0; }
			
        }else if($body_shape_ans!='' && $body_style_ans!=''){
			
            $query = $this->db->query('select id from bucket where `body_shape`="'.$body_shape_ans.'" and `style`="'.$body_style_ans.'"');
            $result = $query->result_array();
            if(!empty($result)){ return $result[0]['id']; }else{ return 0; }
			
        }

    }

    function update_question_resume_id($question_id,$user_id=0){
         if($this->session->userdata('user_id') > 0) $user_id = $this->session->userdata('user_id');
         if($question_id!='' && $user_id!=''){
            $data = array('question_resume_id'=>$question_id,'modified_by'=>$user_id);
            $this->db->update('user_login', $data, array('id' => $user_id ));

            if($question_id == 9 || $question_id == 22){
                $data = array('question_comp'=>1,'modified_by'=>$user_id);
                $this->db->update('user_login', $data, array('id' => $user_id ));
            }
         }
    }

    function get_question_resume_id($user_id=0){
		if($this->session->userdata('user_id') > 0) $user_id = $this->session->userdata('user_id');
        if($user_id!=''){

            $query = $this->db->query('select question_resume_id from user_login where id="'.$user_id.'"');

            $result = $query->result_array();
            if(!empty($result)){ return $result[0]['question_resume_id']; }else{ return 0; }
        }
    }

    function answer_caption($answer_id){
        $result = array();
        if($answer_id){
            $query = $this->db->query('select answer from answers where id="'.$answer_id.'"');

            $result = $query->result_array();
            if(!empty($result)){ $result = $result[0]['answer']; }else{ return 0; }
        }

        return $result;
    }

    function update_profile_img($filename){
        $user_id = $this->session->userdata('user_id');
        if($user_id!='' && $filename!=''){
            $data = array('profile_pic'=>$filename,'modified_by'=>$user_id);
            $this->db->update('user_info', $data, array('user_id' => $user_id ));
            return true;
        }
    }

    function update_cover_img($filename){
        $user_id = $this->session->userdata('user_id');
        if($user_id!='' && $filename!=''){
            $data = array('profile_cover_pic'=>$filename,'modified_by'=>$user_id);
            $this->db->update('user_info', $data, array('user_id' => $user_id ));
            return true;
        }
    }

    function upload_wordrobe_img($filename){
        $user_id = $this->session->userdata('user_id');
        if($user_id!='' && $filename!=''){
           $data = array(
                    'is_active' => 1 ,
                    'image_name' => $filename ,
                    'created_by' => $user_id,
                    'created_datetime' => $this->config->item('sc_date'),
                    'user_id' => $user_id
            );

            $this->db->insert('users_wardrobe', $data);

        }
    }

    function display_wardrobe(){
        $user_id = $this->session->userdata('user_id');
        $query = $this->db->query("select id,image_name from users_wardrobe where user_id='".$user_id."' and is_active = 1 order by id desc");
        $result = $query->result_array();

        $wordrobehtml = '';
        if(!empty($result)){
            foreach($result as $val){
                $wordrobehtml =$wordrobehtml.'<div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 item-wrp"><div class="item item-hover"><div class="item-img">
                  <a href="'.$this->config->item('wordrobe_path_url').$val['image_name'].'" data-gallery><img src="'.$this->config->item('wordrobe_thumb_path_url').$val['image_name'].'" /></a></div></div><div onclick="delete_wardrobe('.$val['id'].');" class="btn-delete" title="Delete">x</div></div>';
            }
        }
        echo $wordrobehtml;

    }

    function delete_wardrobe_img($id){
        $user_id = $this->session->userdata('user_id');
        if($user_id!='' && $id!=''){
            $data = array('is_active'=>0,'modified_by'=>$user_id);
            $this->db->update('users_wardrobe', $data, array('user_id' => $user_id,'id'=>$id));
        }
    }

     function get_wishlist_count(){
        $user_id = $this->session->userdata('user_id');
        if($user_id!=''){
            
           $query = $this->db->query('select count(look_id) as look_id from look_fav where user_id="'.$user_id.'"');

            $result = $query->result_array(); 
            return count($result);
        }
    }

    function sc_change_gender($gender){
        $user_id = $this->session->userdata('user_id');
        $data = array();
        if($user_id>0){
            $this->db->where('user_id', $user_id);
            $this->db->delete('users_answer'); 

            $data=array('bucket'=>0,'question_comp'=>0,'question_resume_id'=>'');
            $this->db->where('id',$user_id);
            $this->db->update('user_login',$data);

            $data=array('gender'=>$gender);
            $this->db->where('user_id',$user_id);
            $this->db->update('user_info',$data);

            $this->session->set_userdata('gender', $gender);

            return '1';
        }
    }


    function push_scbox_data($mobile_number){
         $sc_user_id = $this->session->userdata('user_id');
         $sc_meta_key = "SC_BOX_Mobile";
       if($mobile_number != '' && $sc_user_id != '' && $sc_meta_key != '') { 
            $query1 = $this->db->query('select id from user_metadata where user_id="'.$sc_user_id.'" and meta_key = "'.$sc_meta_key.'"');
            $res = $query1->result_array();       
            
            if(!empty($res)){        
                $result = $this->db->query('UPDATE `user_metadata` SET `meta_value` = "'.$mobile_number.'" WHERE `user_id` = "'.$sc_user_id.'" and meta_key = "'.$sc_meta_key.'"');            
            }else{
                $data = array(                    
                    'user_id' => $sc_user_id,
                    'meta_key' => $sc_meta_key,
                    'meta_value' => $mobile_number,
                    'created_datetime' => $this->config->item('sc_date')
                );
                $query1 = $this->db->insert('user_metadata', $data);                    
            }
            return  1;
        }else{
            return 0;
        }
    }
	
	function save_user_answer($question_id,$answer_id,$user_id=0,$gender=0,$question=0){
		
		if(!empty($this->get_users_answer_obj($user_id,$question,$gender))){
                /*Update the PA*/                
                $data = array('object_meta_value'=>$answer_id,'created_by'=>$user_id,'object_status'=>1);
                $this->db->update('object_meta', $data, array('object_id' => $user_id,'created_by' => $user_id,'object_meta_key' => $question ));

		}else if(!empty($this->get_users_brand_list($user_id,$question,'update_pa')) && $question == 'user_brand_list'){
                /*Update the PA*/

                $data = array('object_meta_value'=>$answer_id,'created_by'=>$user_id,'object_status'=>1);
                $this->db->update('object_meta', $data, array('object_id' => $user_id,'created_by' => $user_id,'object_meta_key' => $question ));

		}else{
			/*Insert the PA*/
			
			$data = array('object_id'=>$user_id,'object_meta_key'=>$question,'object_meta_value'=>$answer_id,'created_datetime'=>date('Y-m-d H:i:s'),'created_by'=>$user_id,'modified_by'=>$user_id);
			$this->db->insert('object_meta', $data);
		}
	}
	
	function get_users_answer_obj($user_id,$question,$gender,$rand_set=''){
        $result = array();
		/*if($question){
			
			$img_link = '';
			if($gender != 0 && $gender == 1){ if($question == 'user_body_shape'){ $img_link = PA_FEMALE; }else { $img_link = PA_FEMALE_STYLE.$rand_set; } }else { if($question == 'user_body_shape'){ $img_link = PA_MALE; }else { $img_link = PA_MALE_STYLE.$rand_set; } }
			
            $query = $this->db->query('select b.id,b.answer,CONCAT("'.$img_link.'",c.image_name) as image_name from object_meta as a,answers as b,answer_assoc_image as c where a.object_id = '.$user_id.' and a.object_meta_key = "'.$question.'" AND a.object_meta_value = b.id AND b.`id`= c.`answer_id`');
			// echo $this->db->last_query();exit;
            $result = $query->result_array();
            return $result;

        }*/
        return $result;
	}
	
	/*function get_users_brand_list($user_id,$question,$type=''){
		if($question){
			$query = $this->db->query('select a.id,a.object_meta_value from object_meta as a where a.object_id = '.$user_id.' and a.object_meta_key = "'.$question.'" ');
			$result = $query->result_array();
			if($type == 'update_pa'){ return $result; }
			if(@$result[0]['object_meta_value'] != ''){
				$query = $this->db->query('select a.user_id as brand_id,a.company_name as brand_name from brand_info as a,object_meta as b where a.user_id IN('.$result[0]['object_meta_value'].') AND b.object_meta_key = "'.$question.'" AND b.object_id = '.$user_id.' ');
				$result = $query->result_array();
				return $result;
			}else return $result = array();
           

        }
	}*/
	
	function get_users_brand_list($user_id,$question,$type='',$gender=''){
		if($question){
            $result = array();
			$query = $this->db->query('select a.id,a.object_meta_value from object_meta as a where a.object_id = '.$user_id.' and a.object_meta_key = "'.$question.'" ');
			$result = $query->result_array();
			if($type == 'update_pa'){ return $result; }
            if($result[0]['object_meta_value']!='')
            {
				$user_list = array();$budget_id=0;
				if($gender == '1'){
					$brand_list = unserialize(PA_BRAND_LIST_WOMEN);
				}else{
					$brand_list = unserialize(PA_BRAND_LIST_MEN);
				}
                $objectmetavalue = trim($result[0]['object_meta_value'],',');
				$user_brand_list_arr = explode(',',$objectmetavalue);
				foreach($user_brand_list_arr as $val){
					$user_list[] = $brand_list[$val];
					if($brand_list[$val]->budget_id > $budget_id){
						$budget_id = $brand_list[$val]->budget_id;
					}
				}
				
				$this->update_user_answer($question_id=28,$budget_id,$user_id,$gender,$question='user_budget');
				return $user_list;
				
			}else return $result = array();
           

        }
	}
	
	function assign_buckets($body_shape_ans,$user_style_p1,$user_id){
		if($body_shape_ans > 0 && $user_style_p1 > 0){
			$query = $this->db->query('select a.id from bucket as a where a.body_shape = '.$body_shape_ans.' AND a.style = '.$user_style_p1.' ');
			$result = $query->result_array();
			if(@$result[0]['id']>0){
				$query = $this->db->query('UPDATE user_login SET bucket = '.$result[0]['id'].' where id = '.$user_id.' ');
			}
		}
		
		return true;
	}

     function get_size_data($type)
    {
        $filter='';       
        if(!empty($type))
        {
            if($type=='womentopsize')
            {
                $filter = '('.SCBOX_WOMENTOP_LIST.')';
            }else if($type=='womenbottomsize')
            {
                $filter = '('.SCBOX_WOMENBOTTOM_LIST.')';
            }else if($type=='womenfootsize')
            {
                $filter = '('.SCBOX_WOMENFOOT_LIST.')';
            }else if($type=='womenbandsize')
            {
                $filter = '('.SCBOX_BANDSIZE_LIST.')';
            }else if($type=='womencupsize')
            {
                $filter = '('.SCBOX_CUPSIZE_LIST.')';
            }else if($type=='mentopsize')
            {
                $filter = '('.SCBOX_MENTOP_LIST.')';
            }else if($type=='menbottomsize')
            {
                $filter = '('.SCBOX_MENBOTTOM_LIST.')';
            }else if($type=='menfootsize')
            {
                $filter = '('.SCBOX_MENFOOT_LIST.')';
            }

            $query = $this->db->query('select a.id,a.size_text as answer from product_size as a where a.size_text IN '.$filter.' order by a.order ');            
            $result = $query->result_array();
            return $result;
        }
    }

    function save_scboxuser_answer($question_id,$answer_id,$user_id=0,$objectid=0,$question=0){

        
        $scbox_pack = unserialize(SCBOX_PACKAGE);
		
        // $query = $this->db->get_where('object_meta', array('object_id' => $objectid,'created_by' => $user_id,'object_meta_key' => $question ));
       // $query = $this->db->get_where('object_meta', array('created_by' => $user_id,'object_meta_key' => $question,'object_id' => $objectid ));
		
		$this->db->from('object_meta');
		$this->db->where('created_by', $user_id);
		$this->db->where('object_meta_key', $question);
		$this->db->where('object_id IN ('.SCBOX_ID.')');
		$query = $this->db->get();
	
        $count = $query->num_rows(); //counting result from query

        
        if($count>0){
                /*Update the PA*/
                $data = array(
							'object_meta_value'=>$answer_id,
							'created_by'=>$user_id,
							'object_status'=>1
							);   
				$this->db->where('created_by', $user_id);		
				$this->db->where('object_meta_key', $question);		
				$this->db->where('object_id IN ('.SCBOX_ID.')'); 
                $this->db->update('object_meta', $data);            
                //$this->db->update('object_meta', $data, array('created_by' => $user_id,'object_meta_key' => $question,'object_id' => $objectid  ));       
               // echo $this->db->last_query();exit;          

        }else{
            /*Insert the PA*/
            
            $data = array(
					'object_id'			=> $objectid,
					'object_meta_key'	=> $question,
					'object_meta_value'	=> $answer_id,
					'created_datetime'	=> date('Y-m-d H:i:s'),
					'created_by'		=> $user_id,
					'modified_by'		=> $user_id
				);
            $this->db->insert('object_meta', $data);
			$query = $this->db->query('update user_login set question_comp = 1,question_resume_id = 1 where id = '.$user_id.' ');
           //echo $this->db->last_query();exit;      
        }

        return true;
    }

    function get_scbox_userdata($objectid,$user_id)
    {  
        $scboxids_arr = array();$n = 0;
        $scboxids = SCBOX_ID;
        $scboxids_arr = explode(',',$scboxids);
        foreach($scboxids_arr as $val)
        {
            $scboxids_arr[] = trim($val,"'");
        }
        if(in_array($objectid, $scboxids_arr))
        {
            //$query = $this->db->get_where('object_meta', array('object_id' => $objectid,'created_by' => $user_id));
            $query = $this->db->get_where('object_meta', array('created_by' => $user_id));
            $result = $query->result_array();
            $n = sizeof($result);
            if(empty($result) || $n<=5)
            {  
                $data['user_data'] = $this->get_user_data($user_id);           
                $data['pa_data'] = $this->get_user_pa($user_id);
                $result[$n]['object_id'] = $objectid;
                $result[$n]['object_meta_key'] = 'user_name';
                $result[$n]['object_meta_value'] = ($data['user_data'][0]['first_name']!='') ? $data['user_data'][0]['first_name'].' '.$data['user_data'][0]['last_name'] : $data['user_data'][0]['user_name'];

                $result[$n+1]['object_id'] = $objectid;
                $result[$n+1]['object_meta_key'] = 'user_gender';
                $result[$n+1]['object_meta_value'] = $data['user_data'][0]['gender'];

                $result[$n+2]['object_id'] = $objectid;
                $result[$n+2]['object_meta_key'] = 'user_emailid';
                $result[$n+2]['object_meta_value'] = $data['user_data'][0]['email'];

                $originalDate = $data['user_data'][0]['birth_date'];
                $newDate = date("d-m-Y", strtotime($originalDate));

                $result[$n+3]['object_id'] = $objectid;
                $result[$n+3]['object_meta_key'] = 'user_dob';
                $result[$n+3]['object_meta_value'] = '';

                $result[$n+4]['object_id'] = $objectid;
                $result[$n+4]['object_meta_key'] = 'user_mobile';
                $result[$n+4]['object_meta_value'] = $data['user_data'][0]['contact_no'];

                $result[$n+5]['object_id'] = $objectid;
                $result[$n+5]['object_meta_key'] = 'user_body_shape';
                $result[$n+5]['object_meta_value'] = @$data['pa_data'][0]['key_id'];

                $result[$n+6]['object_id'] = $objectid;
                $result[$n+6]['object_meta_key'] = 'user_style_p1';
                $result[$n+6]['object_meta_value'] = @$data['pa_data'][0]['key_id'];

                $result[$n+7]['object_id'] = $objectid;
                $result[$n+7]['object_meta_key'] = 'user_style_p2';
                $result[$n+7]['object_meta_value'] = @$data['pa_data'][0]['key_id'];

                $result[$n+8]['object_id'] = $objectid;
                $result[$n+8]['object_meta_key'] = 'user_style_p3';
                $result[$n+8]['object_meta_value'] = @$data['pa_data'][0]['key_id'];
                
            }    
            return $result;
        }
         
    } 

    function get_objectmeta($objectid,$objectmetakey,$userid)
    {
        //$query = $this->db->get_where('object_meta', array('object_id' => $objectid,'object_meta_key' => $objectmetakey ,'created_by' => $userid ));
        $query = $this->db->get_where('object_meta', array('object_meta_key' => $objectmetakey ,'created_by' => $userid ));
        $result = $query->result_array();
        return $result;
    }

    function get_scboxdata($objectid,$objectmeta)
    {
        /*$query = $this->db->query('select a.object_id,a.object_meta_key,a.object_meta_value from object_meta as a where a.object_id ='.$objectid.' and a.object_meta_key Like "'.$objectmeta.'"  and a.created_by=0 and a.modified_by=0 ');  
        //echo $this->db->last_query();exit;          
        $result = $query->result_array();
        return $result;*/
        $result = '';
        $scbox_pack = unserialize(SCBOX_PACKAGE);  
        foreach($scbox_pack as $val)
        {
            if($objectid==$val->id)
            {
                $result = $val;
            }
        }        
        return $result;
    }

    function get_user_data($id){
        //$query = $this->db->query("select a.id,a.user_name, a.email,b.`first_name`,b.`last_name`,c.`role_id`,d.`role_name`,b.`gender` from user_login as a,user_info as b,user_role_mapping as c,role_master as d where a.`id`=b.`user_id` and b.`user_id` = c.`user_id` and c.`role_id` = d.`id` and a.`id`=".$id);
        $query = $this->db->query("select a.id,a.user_name, a.email,b.`first_name`,b.`last_name`,b.`contact_no`,c.`role_id`,d.`role_name`,b.`gender`, b.`registered_from`,b.`facebook_id`,b.`profile_pic`,b.`profile_cover_pic`,b.`birth_date` from user_login as a,user_info as b,user_role_mapping as c,role_master as d where a.`id`=b.`user_id` and b.`user_id` = c.`user_id` and c.`role_id` = d.`id` and a.`id`=".$id);
        $res = $query->result_array();
        return $res;
        
    } 

    function get_user_pa($user_id){
        $res = array();
        // $query = $this->db->query('SELECT a.`question_id`,b.`answer` FROM `users_answer` AS a, `answers` AS b WHERE 1 AND b.`id` = a.`answer_id` and a.`user_id`= '.$user_id);
      /*  $query = $this->db->query('SELECT a.`object_meta_key`,b.`answer`,b.`id` as key_id,(select c.`object_meta_value` from object_meta as c where c.`object_meta_key` = "user_brand_list" AND c.`object_id`= '.$user_id.' limit 0,1) as brand_ids FROM `object_meta` a, `answers` AS b WHERE b.`id` = a.`object_meta_value` AND a.`object_id`= '.$user_id);
        $res = $query->result_array();*/
        return $res;
    }

    function get_question_img_answers_scbox($id,$gender=0,$style=null){
         $result = array();
        /*if($id){
            $img_link = '';
            $pa_image_sets = array("set1/", "set2/", "set3/");
            $rand_set = $pa_image_sets[array_rand($pa_image_sets)];
            if($gender != 0 && $gender == 1){ if($id == '1'){ $img_link = PA_FEMALE; }else { $img_link = PA_FEMALE_STYLE.$rand_set; } }else { if($id == '24'){ $img_link = PA_MALE; }else { $img_link = PA_MALE_STYLE.$rand_set; } }
            if($style!='')
            {
                $query = $this->db->query('SELECT a.id,a.answer,a.description,CONCAT("'.$img_link.'",c.image_name) as image_name FROM `answers` as a, `question_answer_assoc`  as b, `answer_assoc_image` as c where a.`id` = b.`answer_id` AND a.`id`= c.`answer_id` AND a.`status` =1 AND b.`question_id` ='.$id.' and a.id ='.$style);
            }else
            {
                $query = $this->db->query('SELECT a.id,a.answer,a.description,CONCAT("'.$img_link.'",c.image_name) as image_name FROM `answers` as a, `question_answer_assoc`  as b, `answer_assoc_image` as c where a.`id` = b.`answer_id` AND a.`id`= c.`answer_id` AND a.`status` =1 AND b.`question_id` ='.$id);
            }

            $result = $query->result_array();         
        }*/
        return $result;
    }
 
	
	function get_user_img_set($user_id){
		$query = $this->db->query('select pa_img_set from user_login where id = '.$user_id.' ');
		$result = $query->result_array();
		if($result[0]['pa_img_set'] != '') return $result[0]['pa_img_set'];else return 'set1/';
	}
	
	function update_user_answer($question_id,$answer_id,$user_id,$gender,$question){
		
		if(!empty($this->get_users_answer($question_id,$user_id,$question))){
			
			/*Update the PA*/
			$data = array('answer_id'=>$answer_id,'modified_by'=>$user_id,'is_active'=>1);
			$this->db->update('users_answer', $data, array('question_id' => $question_id,'user_id' => $user_id,'user_pref' => $question ));
			return true;

		}else{
			
			/*Insert the PA*/
			$data = array('answer_id'=>$answer_id,'question_id' => $question_id,'user_id' => $user_id,'user_pref' => $question,'created_by'=>$user_id,'created_datetime'=>$this->config->item('sc_date'),'is_active'=>1);
			$this->db->insert('users_answer', $data);
			return true;
		}
			
	}

     function get_todayloggedin_user(){
        $userid = $this->session->userdata('user_id');       
        $query = $this->db->query("select a.`id`,a.`user_name`, a.`email` from user_login as a where a.`created_datetime` LIKE '%".date('Y-m-d h:i:s')."%' and a.`id`=".$userid);
        $res = $query->result_array();
        if(!empty($res))
        {
            return true;
        }else
        {
            return false;
        }       
        
    }
	
	// addded for scbox API
	function load_questAns($id){
        $result = array();
		/*$query = $this->db->query("SELECT A.id, A.answer FROM answers AS A INNER JOIN question_answer_assoc AS Q ON Q.answer_id=A.id AND (Q.question_id = ".$id.") ORDER BY A.answer");
		$query_result[0] = 'Select';
		foreach ($query->result() as $row){
			$query_result[$row->id] = $row->answer;
		}
		$result = $query_result;*/
        return $result;
	}
	
	function get_scbox_userdata_mobile($user_id){
		$query = $this->db->get_where('object_meta', array('created_by' => $user_id));
		$result = $query->result_array();
		$scbox_array = array();$i=0;
		
		if(!empty($result)){
			foreach($result as $val){
				$scbox_array[$val['object_meta_key']]['object_id'] 			= $val['object_id'];
				$scbox_array[$val['object_meta_key']]['object_meta_key'] 	= $val['object_meta_key'];
				$scbox_array[$val['object_meta_key']]['object_meta_value'] 	= $val['object_meta_value'];
				$scbox_array[$val['object_meta_key']]['object_id'] 			= $val['object_id'];
				$i++;
			}
		}
		return $scbox_array;
	}
	
	function getpincodedata($pincode){
		
        if($pincode!='')
        {
            $query = $this->db->query(' select a.id,a.state_name,b.city from states as a,city as b,pincode as c where a.id = b.state_id AND b.id = c.city_id AND c.pincode = "'.$pincode.'" ');
            $result = $query->result_array();
            return @$result[0];
        }
        
    }
	
	function save_scboxuser_answer_mobile($answer_id,$user_id=0,$objectid=0,$question=0){


        $scbox_pack = unserialize(SCBOX_PACKAGE);
        // $query = $this->db->get_where('object_meta', array('object_id' => $objectid,'created_by' => $user_id,'object_meta_key' => $question ));
        $query = $this->db->get_where('object_meta', array('created_by' => $user_id,'object_meta_key' => $question ));

        $count = $query->num_rows(); //counting result from query

        if($count>0){
                /*Update the PA*/

                $data = array('object_meta_value'=>$answer_id,'created_by'=>$user_id,'object_status'=>1);
                //$this->db->update('object_meta', $data, array('object_id' => $objectid,'created_by' => $user_id,'object_meta_key' => $question ));    
                $this->db->update('object_meta', $data, array('created_by' => $user_id,'object_meta_key' => $question ));  
               // echo $this->db->last_query();exit;          
        }else{
            /*Insert the PA*/
            
            $data = array('object_id'=>$objectid,'object_meta_key'=>$question,'object_meta_value'=>$answer_id,'created_datetime'=>date('Y-m-d H:i:s'),'created_by'=>$user_id,'modified_by'=>$user_id);
            $this->db->insert('object_meta', $data);
        }
    }

    function save_objectmeta_data($answer_id,$user_id=0,$objectid=0,$question=0)
	{
        $scbox_pack = unserialize(SCBOX_PACKAGE);
		
        $query = $this->db->get_where(
							'object_meta', array(
								'object_id' 		=> $objectid,
								'created_by' 		=> $user_id,
								'object_meta_key' 	=> $question 
							)
						);       
        $count = $query->num_rows(); //counting result from query
        if($count>0){
                /*Update the PA*/
                $data = array(
					'object_meta_value'	=> $answer_id,
					'created_by'		=> $user_id,
					'modified_by' 		=> $user_id ,
					'object_status'		=> 1
					);
                $this->db->update('object_meta', $data, array('object_id' => $objectid,'created_by' => $user_id,'object_meta_key' => $question ));         
        }else{
            /*Insert the PA*/
			
            $data = array(
					'object_id'			=> $objectid,
					'object_meta_key'	=> $question,
					'object_meta_value'	=> $answer_id,
					'created_datetime'	=> date('Y-m-d H:i:s'),
					'created_by'		=> $user_id,
					'modified_by'		=> $user_id
				);
            $this->db->insert('object_meta', $data);
        }
    }
	
	public function update_cartid_to_order_unique_no($order_unique_no='',$user_id='',$cart_id='')
	{
		if(!empty($cart_id) && !empty($user_id) && !empty($order_unique_no))
		{
			$this->db->set('object_meta_key',$order_unique_no);
			$this->db->where('object_meta_key', $cart_id);
			$this->db->where('created_by', $user_id);
			$this->db->update('object_meta');
			if($this->db->affected_rows())
			{
				return true;
			}
		}
		return false;
	}

     function get_object($objectslug)
    {
        $query = $this->db->get_where('object', array('object_slug' => $objectslug ));
        $result = $query->result_array();
        return $result;
    }

    function get_orderdisplayno($order_no)
    {
        $query = $this->db->get_where('order_info', array('order_unique_no' => $order_no ));
        $result = $query->result_array();
        return $result;
    }

    function user_pa_data($type,$category,$attribute,$cartid)
    {

       /* if(!empty($category))
        {
            foreach($category as $val)
            {
                $query = $this->db->get_where('category_object_relationship', array('object_id' => $val['object_id'],'category_id' => $val['category_id']));
                $result = $query->result_array();
                if(empty($result))
                {
                     $this->db->insert('category_object_relationship',array('object_id' => $val['object_id'],'category_id' => $val['category_id'],'object_type' => 'order','modified_by' => $val['modified_by']));
                }else
                {
                   
                }
            }
        }

        if(!empty($attribute))
        {
            foreach($attribute as $val)
            {
                $query = $this->db->get_where('attribute_object_relationship', array('object_id' => $val['object_id'],'attribute_id' => $val['attribute_id']));
                $result = $query->result_array();
                if(empty($result))
                {
                     $this->db->insert('attribute_object_relationship',array('object_id' => $val['object_id'],'attribute_id' => $val['attribute_id'],'object_type' => 'order','modified_by' => $val['modified_by']));
                }else
                {
                   
                }
            }
        }*/
            
        
        if(!empty($category))
        {
            $this->db->where('object_id', $cartid);
			$this->db->where('object_type', 'order');
            $this->db->delete('category_object_relationship');
            $this->db->insert_batch('category_object_relationship',$category);
        }

        if(!empty($attribute))
        {
            $this->db->where('object_id', $cartid);
			$this->db->where('object_type', 'order');
            $this->db->delete('attribute_object_relationship');
            $this->db->insert_batch('attribute_object_relationship',$attribute);
        }

    }

    public function get_user_profile_pic($user_id='')
    {
        $this->db->select('ui.id,ui.user_id,ui.first_name,ui.last_name,ui.gender,ui.registered_from,ui.facebook_id,ui.google_id,ui.profile_pic,ui.birth_date');
        $this->db->from('user_info as ui');
        $this->db->where('ui.user_id',$user_id);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->row_array();
        }
        return false;
    }

    function save_scboxuser_answer_batch($question_id,$answer_id,$user_id=0,$objectid=0,$question=0){
        $this->db->select('id');
        $this->db->from('object_meta');
        $this->db->where('created_by', $user_id);
        $this->db->where('object_meta_key', $question);
        $this->db->where('object_id IN ('.SCBOX_ID.')');
        $query = $this->db->get();
    
        $count = $query->num_rows(); //counting result from query
        //echo $this->db->last_query();exit;   
        if($count>0){
                /*Update the PA*/             
                $this->db->where('created_by', $user_id); 
                $this->db->update_batch('object_meta', $answer_id,'object_meta_key');
               // echo $this->db->last_query();exit;          

        }else{
            /*Insert the PA*/ 
            $this->db->insert_batch('object_meta', $answer_id);
            $query = $this->db->query('update user_login set question_comp = 1,question_resume_id = 2 where id = '.$user_id.' ');
           //echo $this->db->last_query();exit;      
        }

        return true;
    }


 }
?>
