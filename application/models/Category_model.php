<?php
class Category_model extends MY_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_looks_via_cat($look_type,$category_slug,$category_level){
        $product_table =''; $where_cond = ''; $look_id = '';
        if($category_level!=''){
            if($category_level == 'cat'){ $cat_table_name = 'product_category'; $product_table='product_desc'; }
            else if($category_level == 'sub_cat'){ $cat_table_name = 'product_sub_category'; $product_table='product_desc'; }
            else if($category_level == 'variations_type'){ $cat_table_name = 'product_variation_type'; $product_table='product_variations'; }
            else if($category_level == 'variations'){ $cat_table_name = 'product_variation_value'; $product_table='product_variations'; }
            if($category_slug!=''){
                $query = $this->db->get_where($cat_table_name, array('status' => '1','slug'=>$category_slug));
                $res = $query->result_array();
                $category_id = $res[0]['id'];
            }

            if($category_id > 0 && $product_table != ''){
                if($look_type == 'men') { $where_cond = $where_cond.' and a.looks_for=2';   }
                else if($look_type == 'women') { $where_cond = $where_cond.' and a.looks_for=1';   }
                if($product_table == 'product_desc'){
                    if($category_level == 'cat'){ 
                        $where_cond = $where_cond.' and c.product_cat_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and c.product_sub_cat_id='.$category_id; 
                    }
                    $query = $this->db->query('select distinct a.id as id from look as a,look_products as b,product_desc as c where a.id = b.look_id and b.product_id = c.id and a.status = 1 '.$where_cond.' order by a.modified_datetime desc limit 0,3');
                    $lookIds = $query->result_array();
                    if(!empty($lookIds)){ $i = 0;
                        foreach($lookIds as $val){
                            if($i!=0){ $look_id = $look_id.','; }
                            $look_id = $look_id.$val['id'];
                            $i++;
                        }
                    }
                }else if($product_table == 'product_variations'){
                    if($category_level == 'variations_type'){ 
                        $where_cond = $where_cond.' and c.product_var_type_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and c.product_var_val_id='.$category_id; 
                    }
                    $query = $this->db->query('select distinct a.id as id from look as a,look_products as b,product_variations as c where a.id = b.look_id and b.product_id = c.product_id and a.status = 1 '.$where_cond.' order by a.modified_datetime desc limit 0,3');
                    $lookIds = $query->result_array();
                    if(!empty($lookIds)){ $i = 0;
                        foreach($lookIds as $val){
                            if($i!=0){ $look_id = $look_id.','; }
                            $look_id = $look_id.$val['id'];
                            $i++;
                        }
                    }
                }
            }

            if($look_id!=''){
                $query = $this->db->query('select z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.description,z.status,z.deflag,z.slug,z.looks_for from (SELECT a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.description,a.status,a.deflag,a.slug,a.looks_for FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 OR z.look_type = 6) WHERE 1 and z.status=1 and z.id IN('.$look_id.') order by z.modified_datetime desc');
                $look_data = $query->result_array();
                return $look_data;
            }
        }
    }

    function get_products_via_cat($offset,$look_type,$category_slug,$category_level){
        $offset = ($offset - 1) * 12;
        $product_table =''; $where_cond = ''; $look_id = '';
        if($category_level!=''){
            if($category_level == 'cat'){ $cat_table_name = 'product_category'; $product_table='product_desc'; }
            else if($category_level == 'sub_cat'){ $cat_table_name = 'product_sub_category'; $product_table='product_desc'; }
            else if($category_level == 'variations_type'){ $cat_table_name = 'product_variation_type'; $product_table='product_variations'; }
            else if($category_level == 'variations'){ $cat_table_name = 'product_variation_value'; $product_table='product_variations'; }
            if($category_slug!=''){
                $query = $this->db->get_where($cat_table_name, array('status' => '1','slug'=>$category_slug));
                $res = $query->result_array();
                $category_id = $res[0]['id'];
            }

            if($category_id > 0 && $product_table != ''){
               if($product_table == 'product_desc'){
                    if($category_level == 'cat'){ 
                        $where_cond = $where_cond.' and product_cat_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and product_sub_cat_id='.$category_id; 
                    }
                    if($look_type == 'men') { $where_cond = $where_cond.' and product_cat_id=12';   }
                    else if($look_type == 'women') { $where_cond = $where_cond.' and product_cat_id=1';   }
                    $query = $this->db->query('select id,name,price,image,slug from product_desc WHERE approve_reject = "A" and is_delete = 0 '.$where_cond.' order by id desc limit '.$offset.',12');
                    $product_data = $query->result_array();
                    
                }else if($product_table == 'product_variations'){
                    if($category_level == 'variations_type'){ 
                        $where_cond = $where_cond.' and b.product_var_type_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and b.product_var_val_id='.$category_id; 
                    }
                    if($look_type == 'men') { $where_cond = $where_cond.' and a.product_cat_id=12';   }
                    else if($look_type == 'women') { $where_cond = $where_cond.' and a.product_cat_id=1';   }
                    $query = $this->db->query('select a.id,a.name,a.price,a.image,a.slug from product_desc as a,product_variations as b WHERE a.approve_reject = "A" and a.is_delete = 0 and a.id = b.product_id '.$where_cond.' order by a.id desc limit '.$offset.',12');
                    $product_data = $query->result_array();
                } 
                return $product_data;
            }
        }
    }

    function get_products_via_cat_total($look_type,$category_slug,$category_level){
          $product_table =''; $where_cond = ''; $look_id = '';
        if($category_level!=''){
            if($category_level == 'cat'){ $cat_table_name = 'product_category'; $product_table='product_desc'; }
            else if($category_level == 'sub_cat'){ $cat_table_name = 'product_sub_category'; $product_table='product_desc'; }
            else if($category_level == 'variations_type'){ $cat_table_name = 'product_variation_type'; $product_table='product_variations'; }
            else if($category_level == 'variations'){ $cat_table_name = 'product_variation_value'; $product_table='product_variations'; }
            if($category_slug!=''){
                $query = $this->db->get_where($cat_table_name, array('status' => '1','slug'=>$category_slug));
                $res = $query->result_array();
                $category_id = $res[0]['id'];
            }

            if($category_id > 0 && $product_table != ''){
               if($product_table == 'product_desc'){
                    if($category_level == 'cat'){ 
                        $where_cond = $where_cond.' and product_cat_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and product_sub_cat_id='.$category_id; 
                    }
                    if($look_type == 'men') { $where_cond = $where_cond.' and product_cat_id=12';   }
                    else if($look_type == 'women') { $where_cond = $where_cond.' and product_cat_id=1';   }
                    $query = $this->db->query('select count(id) as cnt  from product_desc WHERE  approve_reject = "A" and is_delete = 0 '.$where_cond.'');
                    $product_data = $query->result_array();
                    
                }else if($product_table == 'product_variations'){
                    if($category_level == 'variations_type'){ 
                        $where_cond = $where_cond.' and b.product_var_type_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and b.product_var_val_id='.$category_id; 
                    }
                    if($look_type == 'men') { $where_cond = $where_cond.' and a.product_cat_id=12';   }
                    else if($look_type == 'women') { $where_cond = $where_cond.' and a.product_cat_id=1';   }
                    $query = $this->db->query('select count(a.id) as cnt from product_desc as a,product_variations as b WHERE a.approve_reject = "A" and a.is_delete = 0 and a.id = b.product_id '.$where_cond.'');
                    $product_data = $query->result_array();
                } 
                if(!empty($product_data)){
                    return $product_data[0]['cnt'];
                }else{
                    return 0;
                }
            }
        }
    }

    function filter_products($offset,$look_type,$category_slug,$category_level,$type,$id,$price,$min_price,$max_price,$color,$discount,$sc_offers){
        $product_table =''; $where_cond = ''; 
        $offset = ($offset - 1) * 12;
          if($category_level!=''){
            if($category_level == 'cat'){ $cat_table_name = 'product_category'; $product_table='product_desc'; }
            else if($category_level == 'sub_cat'){ $cat_table_name = 'product_sub_category'; $product_table='product_desc'; }
            else if($category_level == 'variations_type'){ $cat_table_name = 'product_variation_type'; $product_table='product_variations'; }
            else if($category_level == 'variations'){ $cat_table_name = 'product_variation_value'; $product_table='product_variations'; }
            if($category_slug!=''){
                $query = $this->db->get_where($cat_table_name, array('status' => '1','slug'=>$category_slug));
                $res = $query->result_array();
                $category_id = $res[0]['id'];
            }

            if($category_id > 0 && $product_table!='' && $discount==''){

                if($product_table == 'product_desc'){
                    if($category_level == 'cat'){ 
                        $where_cond = $where_cond.' and product_cat_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and product_sub_cat_id='.$category_id; 
                    }
                    if($look_type == 'men') { $where_cond = $where_cond.' and product_cat_id=12';   }
                    if($type == 'brand' && $id!='') { $where_cond = $where_cond.' and brand_id IN ('.$id.')';   }

                    if($price == 1) { $where_cond = $where_cond.' and price < 500 ';  }
                    else if($price == 2) { $where_cond = $where_cond.' and (price >= 500 ) and (price <= 999 )';  }
                    else if($price == 3) { $where_cond = $where_cond.' and (price >= 1000 ) and (price <= 1999 ) ';  }
                    else if($price == 4) { $where_cond = $where_cond.' and (price >= 2000 ) and (price <= 2999 ) ';  }
                    else if($price == 5) { $where_cond = $where_cond.' and price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                        $where_cond = $where_cond.' and (price >= '.$min_price.' ) and (price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and price >= '.$max_price;
                    }

                    else if($look_type == 'women') { $where_cond = $where_cond.' and product_cat_id=1';   }
                    if($color!=''){ $where_cond = $where_cond.' and sc_product_color IN ('.$color.')';  }
                    if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ $where_cond = $where_cond.' and id IN ('.$this->get_offer_products($sc_offers).')'; }

                   $query = $this->db->query('select id,name,price,image,slug from product_desc WHERE  approve_reject = "A" and is_delete = 0 '.$where_cond.' order by id desc limit '.$offset.',12');
                    $product_data = $query->result_array();
                    
                }else if($product_table == 'product_variations'){
                    if($category_level == 'variations_type'){ 
                        $where_cond = $where_cond.' and b.product_var_type_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and b.product_var_val_id='.$category_id; 
                    }
                    if($look_type == 'men') { $where_cond = $where_cond.' and a.product_cat_id=12';   }
                    if($type == 'brand' && $id!='') { $where_cond = $where_cond.' and a.brand_id IN ('.$id.')';   }

                    else if($look_type == 'women') { $where_cond = $where_cond.' and a.product_cat_id=1';   }
                    
                    if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                    else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                    else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                    else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                    else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                        $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$max_price;
                    }
                    if($color!=''){ $where_cond = $where_cond.' and a.sc_product_color IN ('.$color.')';  }
                    if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; }
                   $query = $this->db->query('select a.id,a.name,a.price,a.image,a.slug from product_desc as a,product_variations as b WHERE a.approve_reject = "A" and a.is_delete = 0 and a.id = b.product_id '.$where_cond.' order by a.id desc limit '.$offset.',12');
                    $product_data = $query->result_array();
                } 
                
                return $product_data;

            }else if($category_id > 0 && $product_table!='' && $discount >0){

                if($product_table == 'product_desc'){
                    if($category_level == 'cat'){ 
                        $where_cond = $where_cond.' and a.product_cat_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and a.product_sub_cat_id='.$category_id; 
                    }
                    if($look_type == 'men') { $where_cond = $where_cond.' and a.product_cat_id=12';   }
                    if($type == 'brand' && $id!='') { $where_cond = $where_cond.' and a.brand_id IN ('.$id.')';   }

                    if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                    else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                    else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                    else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                    else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                        $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$max_price;
                    }

                    else if($look_type == 'women') { $where_cond = $where_cond.' and a.product_cat_id=1';   }
                    if($color!=''){ $where_cond = $where_cond.' and a.sc_product_color IN ('.$color.')';  }
                    if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; }
                    if($discount == 1){
                        $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug from product_desc as a,discount_availed as b WHERE CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id = b.product_id and a.approve_reject = "A" and a.is_delete = 0 '.$where_cond.' order by a.id desc limit '.$offset.',12');
                    }else if($discount == 2){
                        $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug from product_desc as a,discount_availed as b WHERE CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id != b.product_id and a.approve_reject = "A" and a.is_delete = 0 '.$where_cond.' order by a.id desc limit '.$offset.',12');
                    }
                    $product_data = $query->result_array();
                    
                }else if($product_table == 'product_variations'){
                    if($category_level == 'variations_type'){ 
                        $where_cond = $where_cond.' and b.product_var_type_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and b.product_var_val_id='.$category_id; 
                    }
                    if($look_type == 'men') { $where_cond = $where_cond.' and a.product_cat_id=12';   }
                    if($type == 'brand' && $id!='') { $where_cond = $where_cond.' and a.brand_id IN ('.$id.')';   }

                    else if($look_type == 'women') { $where_cond = $where_cond.' and a.product_cat_id=1';   }
                    
                    if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                    else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                    else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                    else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                    else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                        $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$max_price;
                    }
                    if($color!=''){ $where_cond = $where_cond.' and a.sc_product_color IN ('.$color.')';  }
                    if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; }
                    if($discount == 1){
                        $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug from product_desc as a,product_variations as b,`discount_availed` as c WHERE CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id = c.product_id and a.approve_reject = "A" and a.is_delete = 0 and a.id = b.product_id '.$where_cond.' order by a.id desc limit '.$offset.',12');
                    }else if($discount == 2){
                        $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug from product_desc as a,product_variations as b,`discount_availed` as c WHERE CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id != c.product_id and a.approve_reject = "A" and a.is_delete = 0 and a.id = b.product_id '.$where_cond.' order by a.id desc limit '.$offset.',12');
                    }
                    $product_data = $query->result_array();
                } 
                
                return $product_data;


            }
        }
    }

        function filter_products_total($offset,$look_type,$category_slug,$category_level,$type,$id,$price,$min_price,$max_price,$color,$discount,$sc_offers){
        $product_table =''; $where_cond = ''; 
        //$offset = ($offset - 1) * 12;
          if($category_level!=''){
            if($category_level == 'cat'){ $cat_table_name = 'product_category'; $product_table='product_desc'; }
            else if($category_level == 'sub_cat'){ $cat_table_name = 'product_sub_category'; $product_table='product_desc'; }
            else if($category_level == 'variations_type'){ $cat_table_name = 'product_variation_type'; $product_table='product_variations'; }
            else if($category_level == 'variations'){ $cat_table_name = 'product_variation_value'; $product_table='product_variations'; }
            if($category_slug!=''){
                $query = $this->db->get_where($cat_table_name, array('status' => '1','slug'=>$category_slug));
                $res = $query->result_array();
                $category_id = $res[0]['id'];
            }

            if($category_id > 0 && $product_table!='' && $discount ==''){

                if($product_table == 'product_desc'){
                    if($category_level == 'cat'){ 
                        $where_cond = $where_cond.' and product_cat_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and product_sub_cat_id='.$category_id; 
                    }
                    if($look_type == 'men') { $where_cond = $where_cond.' and product_cat_id=12';   }
                    if($type == 'brand' && $id!='') { $where_cond = $where_cond.' and brand_id IN ('.$id.')';   }
                    
                    if($price == 1) { $where_cond = $where_cond.' and price < 500 ';  }
                    else if($price == 2) { $where_cond = $where_cond.' and (price >= 500 ) and (price <= 999 )';  }
                    else if($price == 3) { $where_cond = $where_cond.' and (price >= 1000 ) and (price <= 1999 ) ';  }
                    else if($price == 4) { $where_cond = $where_cond.' and (price >= 2000 ) and (price <= 2999 ) ';  }
                    else if($price == 5) { $where_cond = $where_cond.' and price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                        $where_cond = $where_cond.' and (price >= '.$min_price.' ) and (price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and price >= '.$max_price;
                    }

                    else if($look_type == 'women') { $where_cond = $where_cond.' and product_cat_id=1';   }
                    if($color!=''){ $where_cond = $where_cond.' and sc_product_color IN ('.$color.')';  }
                    if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ $where_cond = $where_cond.' and id IN ('.$this->get_offer_products($sc_offers).')'; }
                   $query = $this->db->query('select count(id) as cnt from product_desc WHERE  approve_reject = "A" and is_delete = 0 '.$where_cond.' order by id desc');
                    $product_data = $query->result_array();
                    
                }else if($product_table == 'product_variations'){
                    if($category_level == 'variations_type'){ 
                        $where_cond = $where_cond.' and b.product_var_type_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and b.product_var_val_id='.$category_id; 
                    }
                    if($look_type == 'men') { $where_cond = $where_cond.' and a.product_cat_id=12';   }
                    if($type == 'brand' && $id!='') { $where_cond = $where_cond.' and a.brand_id IN ('.$id.')';   }
                    
                    if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                    else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                    else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                    else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                    else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                        $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$max_price;
                    }
                    if($color!=''){ $where_cond = $where_cond.' and a.sc_product_color IN ('.$color.')';  }
                    else if($look_type == 'women') { $where_cond = $where_cond.' and a.product_cat_id=1';   }
                    if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; }
                   $query = $this->db->query('select count(a.id) as cnt from product_desc as a,product_variations as b WHERE a.approve_reject = "A" and a.is_delete = 0 and a.id = b.product_id '.$where_cond.' order by a.id desc');
                    $product_data = $query->result_array();
                } 
                
                if(!empty($product_data)){
                    return $product_data[0]['cnt'];
                }else{
                    return 0;
                }

            }else if($category_id > 0 && $product_table!='' && $discount>0){

                  if($product_table == 'product_desc'){
                    if($category_level == 'cat'){ 
                        $where_cond = $where_cond.' and product_cat_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and product_sub_cat_id='.$category_id; 
                    }
                    if($look_type == 'men') { $where_cond = $where_cond.' and product_cat_id=12';   }
                    if($type == 'brand' && $id!='') { $where_cond = $where_cond.' and brand_id IN ('.$id.')';   }
                    
                    if($price == 1) { $where_cond = $where_cond.' and price < 500 ';  }
                    else if($price == 2) { $where_cond = $where_cond.' and (price >= 500 ) and (price <= 999 )';  }
                    else if($price == 3) { $where_cond = $where_cond.' and (price >= 1000 ) and (price <= 1999 ) ';  }
                    else if($price == 4) { $where_cond = $where_cond.' and (price >= 2000 ) and (price <= 2999 ) ';  }
                    else if($price == 5) { $where_cond = $where_cond.' and price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                        $where_cond = $where_cond.' and (price >= '.$min_price.' ) and (price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and price >= '.$max_price;
                    }

                    else if($look_type == 'women') { $where_cond = $where_cond.' and product_cat_id=1';   }
                    if($color!=''){ $where_cond = $where_cond.' and sc_product_color IN ('.$color.')';  }

                  if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; }

                    if($discount == 1){
                        $query = $this->db->query('select count(DISTINCT a.id) as cnt WHERE CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id = b.product_id and a.approve_reject = "A" and a.is_delete = 0 '.$where_cond.' order by a.id desc');
                    }else if($discount == 2){
                        $query = $this->db->query('select count(DISTINCT a.id) as cnt from product_desc as a,discount_availed as b WHERE CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id != b.product_id and a.approve_reject = "A" and a.is_delete = 0 '.$where_cond.' order by a.id desc');
                    }

                    $product_data = $query->result_array();
                    
                }else if($product_table == 'product_variations'){
                    if($category_level == 'variations_type'){ 
                        $where_cond = $where_cond.' and b.product_var_type_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and b.product_var_val_id='.$category_id; 
                    }
                    if($look_type == 'men') { $where_cond = $where_cond.' and a.product_cat_id=12';   }
                    if($type == 'brand' && $id!='') { $where_cond = $where_cond.' and a.brand_id IN ('.$id.')';   }
                    
                    if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                    else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                    else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                    else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                    else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                        $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$max_price;
                    }
                    if($color!=''){ $where_cond = $where_cond.' and a.sc_product_color IN ('.$color.')';  }
                    else if($look_type == 'women') { $where_cond = $where_cond.' and a.product_cat_id=1';   }
                  if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; }
                   if($discount == 1){
                        $query = $this->db->query('select count( DISTINCT a.id) as cnt from product_desc as a,product_variations as b,`discount_availed` as c WHERE CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id = c.product_id and a.approve_reject = "A" and a.is_delete = 0 and a.id = b.product_id '.$where_cond.' order by a.id desc');
                    }else if($discount == 2){
                        $query = $this->db->query('select count( DISTINCT a.id) as cnt from product_desc as a,product_variations as b,`discount_availed` as c WHERE CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id != c.product_id and a.approve_reject = "A" and a.is_delete = 0 and a.id = b.product_id '.$where_cond.' order by a.id desc');
                    }

                    $product_data = $query->result_array();
                } 
                //echo $this->db->last_query();
                if(!empty($product_data)){
                    return $product_data[0]['cnt'];
                }else{
                    return 0;
                }
            }
        }
    }

    function get_offer_products($sc_offers){
        if($sc_offers>0){
            $query = $this->db->query('select products from `coupon_settings` WHERE id='.$sc_offers);
            $res = $query->result_array();
            if(!empty($res)){
                return $res[0]['products'];
            }
        }
    }

 }
?>
