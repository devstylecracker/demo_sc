<?php
class Referal_model extends MY_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function update_referrals($conversion,$referrer_name,$referrer_email,$referee_name,$referee_email,$orderID,$purchaseValue,$referrer_converts,
	$referrer_customValue,$goal_complete){
		
		$referee_user_id = $this->getuser_id($referee_email);
		$referrer_user_id = $this->getuser_id($referrer_email);
		
		if($referee_user_id[0]['confirm_email'] == '1'){ $referee_status = 'success'; } else $referee_status = 'pending';
		if($referrer_user_id[0]['confirm_email'] == '1'){ $referrer_status = 'success'; } else $referrer_status = 'pending';
		
		// credit for referee
		$data_referee = array(
               'user_id' => $referee_user_id[0]['id'],
			   'user_email'=> $referee_email,
               'type' => 'credit',
               'description' => 'desc',
               'points' => '1',
			   'source' => 'referee',
			   'source_id' => $referrer_user_id[0]['id'],
			   'source_email'=> $referrer_email,
			   'status' => $referee_status,
			   'invite_order_id' => $orderID,
			   
            );
		$this->db->insert('referral_info', $data_referee);
		
		// credit for referrer
		$data_referrer = array(
               'user_id' => $referrer_user_id[0]['id'],
			   'user_email'=> $referrer_email,
               'type' => 'credit',
               'description' => 'desc',
               'points' => '1',
			   'source' => 'referrer',
			   'source_id' => $referee_user_id[0]['id'],
			   'source_email'=> $referee_email,
			   'status' => $referrer_status,
			   'invite_order_id' => $orderID,
            );
		$this->db->insert('referral_info', $data_referrer);
	
	}
	
	function getuser_id($email){
		$query = $this->db->query('select id,confirm_email from user_login where email = "'.$email.'" ');
		$result = $query->result_array();
		return $result;
		
	}
	
	function getuser_data($user_id){
		
		// $query = $this->db->query("select a.*,b.confirm_email,SUM(a.points) as user_points from referral_info as a,user_login as b where a.user_id = $user_id AND b.id = a.user_id AND a.status = 'success' AND a.type = 'credit' ");
		$query = $this->db->query("select a.*,b.confirm_email as confirm_email,(select c.confirm_email from user_login as c where c.email = source_email ) as source_email_confirm from referral_info as a,user_login as b where a.user_id = '".$user_id."' AND b.id = a.user_id AND a.status != 'done' ");
		$result = $query->result_array();
		/*  echo $this->db->last_query();
		 exit(); */
		return $result; 
		
	}
	
	function get_confirm_email($email){
		$query = $this->db->query('select confirm_email from user_login where email = "'.$email.'" ');
		$result = $query->result_array();
		if(!empty($result)){ 
			return $result[0]['confirm_email']; 
		}else {
			return 0;
		} 
	}
	
}