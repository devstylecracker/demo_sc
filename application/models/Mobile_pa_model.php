<?php

class Mobile_pa_model extends Pa_model {
	
	function __construct()
		{
			// Call the Model constructor
			parent::__construct();
		}
	
	
 function get_question_img_answers_mobile($id,$user_id){
        //,CONCAT('http://www.stylecracker.com/assets/images/pa/',a.image) as url
        if($id){
            $query = $this->db->query('SELECT a.id,a.answer,a.description,CONCAT("https://www.stylecracker.com/assets/images/pa/mobile/",c.image_name) as image_name FROM `answers` as a, `question_answer_assoc`  as b, `answer_assoc_image` as c where a.`id` = b.`answer_id` AND a.`id`= c.`answer_id` AND a.`status` =1 AND b.`question_id` ='.$id);

            $result = $query->result_array();
            return $result;
        }
    }
	
	function get_question_img_answers_mobile_men($id,$user_id){
        //,CONCAT('http://www.stylecracker.com/assets/images/pa/mobile_male/',a.image) as url
        if($id){
            $query = $this->db->query('SELECT a.id,a.answer,a.description,CONCAT("https://www.stylecracker.com/assets/images/pa/mobile_male/",c.image_name) as image_name FROM `answers` as a, `question_answer_assoc`  as b, `answer_assoc_image` as c where a.`id` = b.`answer_id` AND a.`id`= c.`answer_id` AND a.`status` =1 AND b.`question_id` ='.$id);

            $result = $query->result_array();
            return $result;
        }
    }
	
	function get_question_answers_mobile($id,$user_id){
        if($id){
			
            $query = $this->db->query('SELECT a.id,a.answer,a.description FROM `answers` as a, `question_answer_assoc`  as b where a.`id` = b.`answer_id` AND a.`status` =1 AND b.`question_id` ='.$id);

            $result = $query->result_array();
			// foreach($result as $row){
				// $result_data['id'] = $row['id'];
				// $result_data['answer'] = strip_tags($row['answer']);
				// $result_data['description'] = strip_tags($row['description']);
				// $result_out[] = $result_data;
			// }
            return $result;
        }

    }
	
	
    function get_users_answer_mobile($question_id,$user_id){
        if($question_id){
            
			
			// $query = $this->db->query('select answer_id from users_answer where user_id = '.$user_id.' and question_id = '.$question_id);
			$query = $this->db->query('select a.answer_id,b.answer from users_answer as a,answers as b where a.user_id = '.$user_id.' and a.question_id = '.$question_id.' AND a.answer_id = b.id');
			$result = $query->result_array();
			return $result;

        }
    }
	
	
	function get_question_resume_id_mobile($user_id){

        if($user_id!=''){

            $query = $this->db->query('select question_resume_id from user_login where id="'.$user_id.'"');

            $result = $query->result_array();
            if(!empty($result)){ return $result[0]['question_resume_id']; }else{ return 0; }
        }
    }
	
	function update_question_resume_id_mobile($question_id,$user_id){
         if($question_id!='' && $user_id!=''){
            $data = array('question_resume_id'=>$question_id,'modified_by'=>$user_id);
            $this->db->update('user_login', $data, array('id' => $user_id ));

            if($question_id == 9 || $question_id == 22){
                $data = array('question_comp'=>1,'modified_by'=>$user_id);
                $this->db->update('user_login', $data, array('id' => $user_id ));
            }
         }
    }
    /* old assign bucket 
	function assign_bucket_mobile($user_id){
         if($user_id!=''){
            $body_shape_ans = $this->get_users_answer_mobile(1,$user_id);
            $body_style_ans = $this->get_users_answer_mobile(2,$user_id);

            if(!empty($body_shape_ans) && !empty($body_style_ans)){
                $bucket_id = $this->get_bucket_id($body_shape_ans[0]['answer_id'],$body_style_ans[0]['answer_id']);

                $data = array('bucket'=>$bucket_id,'modified_by'=>$user_id);
                $this->db->update('user_login', $data, array('id' => $user_id ));
            }

         }
    }
     */
	 
	 function assign_bucket_mobile($user_id,$gender){
         if($user_id!=''){
			$bucket_id = 0;
			if($gender == '1'){
				// added for women pa 
				$body_shape_ans = $this->get_users_answer_mobile(1,$user_id);
				$body_style_ans = $this->get_users_answer_mobile(2,$user_id);
				$bucket_id = $this->get_bucket_id($body_shape_ans[0]['answer_id'],$body_style_ans[0]['answer_id']);
			}else{
				// added for men pa 
				$body_shape_ans = $this->get_users_answer_mobile(13,$user_id);
				$body_style_ans = $this->get_users_answer_mobile(14,$user_id);
				$body_work_style = $this->get_users_answer_mobile(15,$user_id);
				$body_personal_style = $this->get_users_answer_mobile(16,$user_id);
				$bucket_id = $this->get_bucket_id($body_shape_ans[0]['answer_id'],$body_style_ans[0]['answer_id'],$body_work_style[0]['answer_id'],$body_personal_style[0]['answer_id']);
			}
            if(!empty($body_shape_ans) && !empty($body_style_ans)){
                
                $data = array('bucket'=>$bucket_id,'modified_by'=>$user_id);
                $this->db->update('user_login', $data, array('id' => $user_id ));
            }

         }
    }
	
    function update_mobile_pa($question_id,$intCurrentTypeID,$user_id,$gender){
        if($question_id!='' && $intCurrentTypeID!=''){
            if(!empty($this->get_users_answer_mobile($question_id,$user_id))){
                /*Update the PA*/
                $data = array('answer_id'=>$intCurrentTypeID,'modified_by'=>$user_id,'is_active'=>1);
                $this->db->update('users_answer', $data, array('question_id' => $question_id,'user_id' => $user_id ));
                if($question_id == 1 || $question_id == 2 || $question_id == 13 || $question_id == 14 || $question_id == 22){
                    $this->assign_bucket_mobile($user_id,$gender);
                    $this->update_question_resume_id_mobile($question_id,$user_id);
                    return true;
                }else{
                     $this->update_question_resume_id_mobile($question_id,$user_id);
                     return true;

                }

            }else{
                /*Insert the PA*/

                $data = array('answer_id'=>$intCurrentTypeID,'question_id' => $question_id,'user_id' => $user_id,'created_by'=>$user_id,'created_datetime'=>$this->config->item('sc_date'),'is_active'=>1);

                $this->db->insert('users_answer', $data);
                if($question_id == 1 || $question_id == 2 || $question_id == 13 || $question_id == 14 || $question_id == 22){
                    /* Added by saylee - function name was wrong*/
                    $this->assign_bucket_mobile($user_id,$gender);
                    $this->update_question_resume_id_mobile($question_id,$user_id);
                    return true;
                }else{
                    $this->update_question_resume_id_mobile($question_id,$user_id);
                    return true;
                }

            }
        }
    }
    
    /* Added by Saylee - Below function was missing*/
    function get_bucket_id($body_shape_ans,$body_style_ans,$body_work_style,$body_personal_style){

        if($body_shape_ans!='' && $body_style_ans!='' && $body_work_style!='' && $body_personal_style!=''){
			
            $query = $this->db->query('select id from bucket where `body_shape`="'.$body_shape_ans.'" and `style`="'.$body_style_ans.'" AND work_style = "'.$body_work_style.'" AND personal_style = "'.$body_personal_style.'" ');
            $result = $query->result_array();
            if(!empty($result)){ return $result[0]['id']; }else{ return 0; }
			
        }else if($body_shape_ans!='' && $body_style_ans!=''){
			
            $query = $this->db->query('select id from bucket where `body_shape`="'.$body_shape_ans.'" and `style`="'.$body_style_ans.'"');
            $result = $query->result_array();
            if(!empty($result)){ return $result[0]['id']; }else{ return 0; }
			
        }

    }
	
	function get_questions($id){
        if($id){
            $query = $this->db->query('select question from questions where `id` ='.$id);

            $result = $query->result_array();
            return $result[0]['question'];
        }

    } 
	
	
}
?>