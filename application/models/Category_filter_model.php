<?php
class Category_filter_model extends MY_Model {
	function __construct()
    {
        // Call the Model constructor
        $this->slug = '';
        parent::__construct();
    }

	function get_gender($level){
		
		$gender = array();$i=0;
        // $query = $this->db->order_by('sort_order', 'ASC')->get_where('category', array('category_parent' => $level));
		$query = $this->db->query('select *,@type:="1" as is_last from category where category_parent = '.$level.' order by field(id,1,3,2) ');
        $res = $query->result_array();
        if(!empty($res)){
            foreach($res as $val){
				if($val['category_image'] == ''){ 
					$val['category_image']= base_url().'assets/images/image-na.jpg'; }
				else { 
					$val['category_image'] = PRODUCT_CAT_URL.$val['category_image'];
				}
				$gender[$i]['category_id'] = $val['id'];
				$gender[$i]['category_name'] = $val['category_name'];
				$gender[$i]['category_slug'] = $val['category_slug'];
				$gender[$i]['category_image'] = $val['category_image'];
				$gender[$i]['level'] = $val['level'];
				$cat_list[$i]['has_children'] = $this->has_children($val['id']);
				$i++;
            }
        }
		
        return $gender;
           
    }
	
	function get_child_list($cat_id){
		$cat_list = array();$i=1;
		
		$query = $this->db->query('SELECT a.id,a.category_name,a.category_slug,a.category_image,a.level from category a WHERE a.id = '.$cat_id.' OR a.category_parent = '.$cat_id.' and `count` > 0 and status=1 ORDER BY id,sort_order ASC ');
		$res = $query->result_array();
		if(!empty($res)){
			foreach($res as $val){
				
				if($cat_id != $val['id']){
					if($val['category_image'] == ''){ 
					$val['category_image']= base_url().'assets/images/image-na.jpg'; }
					else { 
						$val['category_image'] = PRODUCT_CAT_URL.$val['category_image'];
					}
					$cat_list[$i]['category_id'] = $val['id'];
					$cat_list[$i]['category_name'] = $val['category_name'];
					$cat_list[$i]['category_slug'] = $val['category_slug'];
					$cat_list[$i]['category_image'] = $val['category_image'];
					$cat_list[$i]['level'] = $val['level'];
					$cat_list[$i]['has_children'] = $this->has_children($val['id']);
					$cat_list[$i]['is_last'] = (string)$this->check_is_lastlvl($val['id']);
					$i++;
				}else{
					if($val['category_image'] == ''){ 
					$val['category_image']= base_url().'assets/images/image-na.jpg'; }
					else { 
						$val['category_image'] = PRODUCT_CAT_URL.$val['category_image'];
					}
					$cat_list[0]['category_id'] = $val['id'];
					$cat_list[0]['category_name'] = $val['category_name'].' (all)';
					$cat_list[0]['category_slug'] = $val['category_slug'];
					$cat_list[0]['category_image'] = $val['category_image'];
					$cat_list[0]['level'] = $val['level'];
					$cat_list[0]['has_children'] = $this->has_children($val['id']);
					$cat_list[0]['is_last'] = '1';
				}
			}
		}
		
        return $cat_list;
	}
	
	function has_children($id){
        $query = $this->db->query('select count(id) as cnt from category where category_parent IN ("'.$id.'")');
        $res = $query->result_array();

        if(!empty($res)){
            return $res[0]['cnt'];
        }else { return 0; }
    }
	
	function check_is_lastlvl($id){
        $query = $this->db->query('select id from category where category_parent = '.$id.' ');
        $res = $query->result_array();
		$child = 1;	
        if(!empty($res)){
            foreach($res as $val){
				$child = $this->has_children($val['id']);
				if($child > 0){
					return 0;
				}
			}
			return 1;
        }else { return 1; }
    }
	
	function get_products($user_id=0,$bucket=0,$cat_id=0,$lastlevelcat=null,$brandId=null,$priceId=null,$min_price=0,$max_price=0,$sort_by=0,$attributesId=null,$sizeId=null,$search=null,$limit=0,$offset=0,$is_recommended=0,$is_all=0){$start = microtime(true);
		$data = array();$product_id = '0';$w_c1 = '';$w_c = '';$w_c_1 = '';$w_c_r1 = '';$product_id_cat = '';$rc_product = '';
		$product_array = array(); 
		$order_by = 'a.name desc,a.description desc,a.modified_datetime desc';
		
		//category filter
		if($cat_id > 0){ $w_c1 = ' and category_id='.$cat_id; }
		if($lastlevelcat){ $w_c1 = ' and category_id IN('.trim($lastlevelcat).')';  }
		
		//is_recommended filter
		if($is_recommended == 1){ 
			$rc_product = $this->recommended_products_via_attributes($bucket);
			$w_c_1 = $w_c_1.' and object_id IN(0'.$rc_product.')';
		}
		// echo $rc_product;exit;
		//is_recommended filter
		if($attributesId==null){
            $get_pro_id = $this->db->query("select distinct object_id from category_object_relationship where object_type='product' ".$w_c1." ".$w_c_1." order by object_id desc");
            $result = $get_pro_id->result_array();

            if(!empty($result)){
                foreach($result as $val){
                    $product_ids[] = $val['object_id'];
                }

                $product_id = implode(',', $product_ids);
            }
			// echo $product_id;exit;
			$w_c_r1 = $w_c_r1.' and object_id NOT IN(0'.$product_id.')';
			$get_pro_id = $this->db->query("select distinct object_id from category_object_relationship where object_type='product' ".$w_c1." ".$w_c_r1." order by object_id desc");
            $result = $get_pro_id->result_array();

            if(!empty($result)){
                foreach($result as $val){
                    $product_ids_cat[] = $val['object_id'];
                }

                $product_id_cat = implode(',', $product_ids_cat);
            }
        }else if($attributesId!=null){
            $product_id = '0';
            $w_c2 = ''; $w_c3 = '';

             if($cat_id>0){ $w_c2 = ' and b.category_id='.$cat_id; }
             if($lastlevelcat){ $w_c2 = ' and b.category_id IN('.trim($lastlevelcat).')';  }

             $get_pro_id1 = $this->db->query("select GROUP_CONCAT(id) as att from attribute_values_relationship where id IN (".$attributesId.") group by attribute_id");
             $resul1 = $get_pro_id1->result_array();
             
             $att = array();

             
             if(!empty($resul1)){ $f = 0;
                $w_c3 = ''; $w_c3 = $w_c3.' Having (';
                foreach($resul1 as $vall){ 
                    $att = explode(',', $vall['att']);
                    if($f!=0){ $w_c3 = $w_c3.') and ('; }
                    $k=0;
                    foreach($att as $val){
                        if($k!=0){ $w_c3 = $w_c3.' or '; }
                        $w_c3 = $w_c3.' Find_In_Set('.$val.',attr)';
                        $k++;
                    }
                    $f++;

                }

                $w_c3 = $w_c3.')';
                $get_pro_id = $this->db->query("select object_id,GROUP_CONCAT(attribute_id) as attr from attribute_object_relationship group by object_id".$w_c3);
                $resul = $get_pro_id->result_array();
                
                $product_id = ''; $product_ids = array();
                if(!empty($resul)){
                    foreach($resul as $val){
                        $product_ids[] = $val['object_id'];
                    }

                    $product_id = implode(',', $product_ids);
                    $w_c2 = $w_c2.' and b.object_id IN('.$product_id.')';
                }

             }

            if($product_id!=''){$w_c_2='';$w_c_r2='';
				if($is_recommended == 1){ 
					$w_c_2 = $w_c_2.' and b.object_id IN(0'.$rc_product.')'; 
				}
				$get_pro_id = $this->db->query("select distinct b.object_id from category_object_relationship as b where 1 ".$w_c2." ".$w_c_2." order by b.object_id desc");
				$resul = $get_pro_id->result_array();

				$product_id = ''; $product_ids = array();
				if(!empty($resul)){
					foreach($resul as $val){
						$product_ids[] = $val['object_id'];
					}
					$product_id = implode(',', $product_ids);
				}
				
				//to get non recommended product
				$w_c_r2 = $w_c_r2.' AND b.object_id NOT IN(0'.$product_id.')';
				$get_pro_id = $this->db->query("select distinct b.object_id from category_object_relationship as b where 1 ".$w_c2." ".$w_c_r2." order by b.object_id desc");
				$resul = $get_pro_id->result_array();

				$product_id_cat = ''; $product_ids_cat = array();
				if(!empty($resul)){
					foreach($resul as $val){
						$product_ids_cat[] = $val['object_id'];
					}
					$product_id_cat = implode(',', $product_ids_cat);
				}
            }
        }
		
		//brand filter 
		if($brandId!=''){ $w_c .= ' and a.brand_id IN('.$brandId.')'; }
		
		// price filter min max
		if($min_price >= 0 && $max_price > 0){ $w_c .= ' and a.price between '.$min_price.' and '.$max_price; }
		
		// price filter by priceId
        else if($priceId == 1) { $w_c .= ' and a.price <= 999 ';  }
        else if($priceId == 2) { $w_c .= ' and (a.price >= 1000 ) and (a.price <= 1999 )';  }
        else if($priceId == 3) { $w_c .= ' and (a.price >= 2000 ) and (a.price <= 3499 ) ';  }
        else if($priceId == 4) { $w_c .= ' and (a.price >= 3500 ) and (a.price <= 4999 ) ';  }
        else if($priceId == 5) { $w_c .= ' and a.price > 5000 ';  }
		
		// sort by price
		if($sort_by == '2'){
            $order_by = 'a.price asc';
			$prod_data['order_by'] = 'order by p.price asc';
        }else if($sort_by == '1'){
            $order_by = 'a.price desc';
			$prod_data['order_by'] = 'order by p.price desc';
        }
		
		// filter by size
		if($sizeId!=null){
            $w_c .= ' and b.size_id IN('.$sizeId.')';
        }
		
		if($search!=null){
            // $product_id = '0';$product_id_cat='0';
			//$product_id = '0'.$product_id.',0'.$product_id_cat.'';
			$product_id_cat='0';
            $product_id = $this->search_product($search,$product_id);
			//echo '<pre>'; print_r($product_id);exit;
        }
		// $data['filter']['rec'] = $product_id;
        // $data['filter']['not_rec'] = $product_id_cat;


		if($product_id!='' && $product_id_cat!=''){			
            $say_prod_ids =  explode(',', $product_id);
            $say_prod_ids = array_unique($say_prod_ids);
            $product_id = implode(',', $say_prod_ids);
			
			$say_prod_ids2 =  explode(',', $product_id_cat);
            $say_prod_ids2 = array_unique($say_prod_ids2);
            $product_id_cat = implode(',', $say_prod_ids2);
			$product_id = ''.$product_id.',0'.$product_id_cat.'';
			$product_id = '0'.$this->product_availability($product_id);
            $w_c .= ' and a.id IN('.$product_id.')';
			if($sort_by<1)$order_by = 'FIELD(a.id,'.$product_id.')';
        }else if($product_id!=''){			
            $say_prod_ids =  explode(',', $product_id);
            $say_prod_ids = array_unique($say_prod_ids);
            $product_id = implode(',', $say_prod_ids);
			$product_id = '0'.$this->product_availability($product_id);			
            $w_c .= ' and a.id IN('.$product_id.')';
        }else if($product_id_cat!=''){
			
            $say_prod_ids =  explode(',', $product_id_cat);
            $say_prod_ids = array_unique($say_prod_ids);
            $product_id = implode(',', $say_prod_ids);
			$product_id = '0'.$this->product_availability($product_id);
            $w_c .= ' and a.id IN('.$product_id.')';
        }else{
            $w_c .= ' and a.id IN(0)';
        }
		
		//$get_products_qry = $this->db->query("select distinct a.id from product_desc as a where 1 and a.status = 1 and a.parent_id = '0' and a.is_delete=0 and a.stock_count > 0 ".$w_c." order by ".$order_by."");
		$get_products_qry = $this->db->query("select distinct a.id from product_desc as a where 1 and a.status = 1 and a.is_delete=0 and a.stock_count > 0 ".$w_c." order by ".$order_by."");
        $tot = $get_products_qry->result_array();

        $data['total_row'] = (string)count($tot);

       // $get_products = $this->db->query("select distinct a.id from product_desc as a where 1 and a.status = 1 and a.parent_id = '0'  and a.is_delete=0 and a.stock_count > 0 ".$w_c." order by ".$order_by." limit ".$offset.",".$limit);
        // $get_products = $this->db->query("select distinct a.id from product_desc as a where 1 and a.status = 1 and a.is_delete=0 and a.stock_count > 0 ".$w_c." order by ".$order_by." limit ".$offset.",".$limit);
          $get_products = $this->db->query("select distinct a.id from product_desc as a where 1 and a.status = 1 and a.is_delete=0 and a.stock_count > 0 ".$w_c." order by ".$order_by);
        $res = $get_products->result_array();
		// echo $this->db->last_query();exit;
		// $end = microtime(true);echo ($end - $start).'seconds';exit;
		$product_ids = array();$i = 0;
		if(!empty($res)){
			foreach($res as $row){
				$product_ids[$i] = $row['id'];
				$i++;
			}
		}
		//echo '<pre>';print_r($product_ids);exit;
		$prod_data['limit_cond'] = '';
		$prod_data['offset'] = $offset;
		$prod_data['limit'] = '48';
		$prod_data['user_id'] = $user_id;
		//$prod_data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
		$prod_data['product_link'] = base_url().'sc_admin/assets/products/thumb_124/';
		if(!empty($product_ids))$product_array = $this->Mobile_look_model->get_products_list_data($product_ids,$prod_data);
        //echo '<pre>';print_r($product_array);exit;
        $data['filter']['paid_brands'] = $this->get_paid_brand_list($product_id);
        if($is_all != 1 && $cat_id > 0){
			$data['filter']['category'] = $this->get_child_list($cat_id);
		}else{
			$data['filter']['category'] = array();
		}
		
        $data['filter']['global_attributes'] = $this->global_attributes();
        if($cat_id>0){
            
            $data['filter']['category_attributes'] = $this->category_attributes($cat_id);
        }
       
        $data['filter']['size'] = $this->get_product_size($product_id);
        $data['cat_id'] = $cat_id;
        $data['filter_brand_id'] = $brandId;
        $data['products'] = $product_array;       
        //echo '<pre>';print_r($data['products']);exit;
        return $data;
		
        
	}
	
	 function get_paid_brand_list($product_ids){
        if($product_ids!=''){
    	$query = $this->db->query('select distinct b.user_id as brand_id,b.company_name from product_desc as a,brand_info as b,product_inventory as d where b.user_id = a.brand_id and b.is_paid = 1 and a.approve_reject IN("D","A","B") and a.is_delete=0 and a.status = 1 and a.id=d.product_id and d.stock_count>0 and a.id IN('.$product_ids.') order by b.company_name');
        }else{
        $query = $this->db->query('select distinct b.user_id as brand_id,b.company_name from product_desc as a,brand_info as b,product_inventory as d where b.user_id = a.brand_id and b.is_paid = 1 and a.approve_reject IN("D","A","B") and a.is_delete=0 and a.status = 1 and a.id=d.product_id and d.stock_count>0 order by b.company_name' );
        }
    	$res = $query->result_array();
    	return $res;
    }
	
	function recommended_products_via_attributes($bucket){
        $product_id = ''; 
        if($bucket>0){
			
            $query = $this->db->query("select a.attribute_value_ids from (select object_id,attribute_value_ids from pa_object_relationship where object_type='category' and pa_type='bucket' and object_value =".$bucket." and attribute_value_ids!='') as a ");
            $res = $query->result_array($query);
			
            if(!empty($res)){ $i=0;$cat_id = '';
				foreach($res as $val){
					if($val['attribute_value_ids']!=''){
						if($i!=0){ $cat_id = $cat_id.','; }

						$dt = unserialize($val['attribute_value_ids']);
						$cat_ids = implode(',', $dt);
						$cat_id = $cat_id.$cat_ids;
						$i++;
					}
					$w_c3='';
					if($cat_id==''){ $cat_id = '0'; }
					$get_pro_id1 = $this->db->query("select GROUP_CONCAT(id) as att from attribute_values_relationship where id IN (".$cat_id.") group by attribute_id");
					$resul1 = $get_pro_id1->result_array();
					$att = array();

				 
					if(!empty($resul1)){ $f = 0;
						$w_c3 = ''; $w_c3 = $w_c3.' Having (';
						foreach($resul1 as $vall){ 
							$att = explode(',', $vall['att']);
							if($f!=0){ $w_c3 = $w_c3.') and ('; }
							$k=0;
							foreach($att as $val){
								if($k!=0){ $w_c3 = $w_c3.' or '; }
								$w_c3 = $w_c3.' Find_In_Set('.$val.',attr)';
								$k++;
							}
							$f++;

						}

						$w_c3 = $w_c3.')'; 

						if($w_c3!=''){
						   $query11 = $this->db->query("select object_id ,GROUP_CONCAT(attribute_id) as attr from attribute_object_relationship group by object_id".$w_c3);
							$res11 = $query11->result_array();
							if(!empty($res11)){
								foreach($res11 as $val1){
									$product_id = $product_id.','.$val1['object_id'];
								   
								}
							}
						}
					}
				}
			}
        }
        $product_id = $this->product_availability($product_id);
        if($product_id==''){
            // $product_id1 = $this->recommended_products_via_category($bucket);
            // $product_id = $this->product_availability($product_id1);
        }
        
        return $product_id;
    }
	
	 function recommended_products_via_category($bucket){

        if($bucket){
            $pro_ids = '';
            if($bucket!=''){
    
            $query = $this->db->query("select object_id from pa_object_relationship where object_type='category' and pa_type='bucket' and object_value =".$bucket." ");

            $res = $query->result_array();

            $cat_id =''; $product_id = '';
            if(!empty($res)){ 
                foreach($res as $val){
                    $cat_id = $cat_id.','.$val['object_id'];
                }
            

                $query1 = $this->db->query("select distinct object_id from category_object_relationship where category_id IN(-1".$cat_id.") and object_type='product'");
                $res1 = $query1->result_array();
                if(!empty($res1)){
                    foreach($res1 as $val){
                        $product_id = $product_id.','.$val['object_id'];
                    }
                }
                
                return $product_id; 

            }else{
                return '';    
            }
            }
        }else{
            return '';
        }

    }

    function product_availability($product_ids){
        $pro_id = '';
        if($product_ids!=''){

			//$query11 = $this->db->query('select a.id as id from product_desc as a where a.status=1 and a.id in(0'.$product_ids.') AND a.stock_count > 0 and a.parent_id = "0" ORDER BY FIELD(a.id,0'.$product_ids.')');
			$query11 = $this->db->query('select a.id as id from product_desc as a where a.status=1 and a.id in('.$product_ids.') AND a.stock_count > 0  ');
			$res11 = $query11->result_array();
			//echo $this->db->last_query();exit;
			if(!empty($res11)){
				foreach($res11 as $val){
					$pro_id = $pro_id.','.$val['id'];
				}
			}			
            return $pro_id;
        }else{
            return $pro_id;
        }
    }
	
	 function global_attributes(){
        $query = $this->db->query("SELECT `id`, `attribute_name`, `attribute_content`, `attribute_parent`, `attribute_type`, `attribute_slug`, `show_on_web`, `order`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `attributes` WHERE 1 and `show_on_web` = 1 and `attribute_parent` = '-1' order by sort asc ");
        $res = $query->result_array();
        return $res;
    }
	
	function category_attributes($cat_id){
        
        $query = $this->db->query("SELECT `id`, `attribute_name`, `attribute_content`, `attribute_parent`, `attribute_type`, `attribute_slug`, `show_on_web`, `order`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `attributes` WHERE 1 and `show_on_web` = 1 and find_in_set('".$cat_id."',`attribute_parent`) <> 0 and `attribute_parent` != '-1' order by sort asc ");
        $res = $query->result_array();
        return $res;
    }
	
	function get_product_size($product_id){
        if($product_id){
			$query = $this->db->query('select distinct b.id,b.size_text from product_inventory as a,product_size as b where a.size_id = b.id and a.stock_count >0 and a.product_id IN ('.$product_id.') order by b.order');
			$res = $query->result_array();
			return $res;
        }else{ 
			return ''; 
		}
    }
	
	function attributes_values($id){

        
        $query = $this->db->query("SELECT `id`, `attribute_id`, `attr_value_name`, `attr_value_content`, `attr_value_slug`, `attr_hashtag`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `attribute_values_relationship` WHERE 1 and `attribute_id`='".$id."' and `display` <> 2 and `count` >0 order by attr_value_name");
        $res = $query->result_array();
        return $res;   
    }
	
	function search_product($search='',$product_id=0){
		$wh_con = '';$tag_product_ids = '';$product_ids_in = '';$product_array = array();$tag_ids = array();
		$search = preg_replace('/\s+/', ' ',$search);
		$search_keyword = explode(" ",trim($search));
		foreach($search_keyword as $val){
			$val_ser = strtolower($val);
			$wh_con = $wh_con.',\''.$val_ser.'\'';
		}
		$tag_name = trim($wh_con,',');
		//echo $tag_name = REPLACE($tag_name,'-','+');exit;
		$tag_name = '"'.$tag_name.'"';
		//$query = $this->db->query('SELECT GROUP_CONCAT(DISTINCT a.id) as tag_ids FROM `tag` as a WHERE a.name in('.$tag_name.') ');
		$query = $this->db->query('SELECT GROUP_CONCAT(DISTINCT id) as tag_ids FROM `tag` WHERE MATCH(name) AGAINST (+'.$tag_name.' IN NATURAL LANGUAGE MODE)');
		
		$res = $query->result_array();
		//echo $this->db->last_query();exit;
		//echo 'product_ids---'.$product_id;exit;
		if(@$res[0]['tag_ids'] != ''){
			$tag_ids = explode(',',$res[0]['tag_ids']);
			//echo 'tagids--';print_r($tag_ids);exit;
			foreach($tag_ids as $val){
				$query2 = $this->db->query('SELECT DISTINCT b.product_id as product_ids FROM product_tag as b WHERE b.tag_id = '.$val.' ');
				$res2 = $query2->result_array();
				//echo $this->db->last_query();exit;
				foreach($res2 as $val){
					$product_ids_in[] = $val['product_ids'];
				}
			}
			
		}
		//echo 'product_ids_in---';print_r($product_ids_in);exit;
		//if(count($tag_ids) > 1)$product_ids_in = array_unique(array_diff_assoc($product_ids_in, array_unique($product_ids_in)));
		//echo 'product_ids_in11---';print_r($product_ids_in);exit;
		if($product_ids_in != ''){
			//echo 'product_ids_in---';print_r($product_ids_in);exit;
			$product_ids = array_filter(explode(',',$product_id));
			$product_ids_in = array_intersect($product_ids, $product_ids_in);
			$product_ids_in = implode(',',array_filter($product_ids_in));
			return trim($product_ids_in,',');
			
		}else{
			$get_pro_id1 = $this->db->query("select id from product_desc where LOWER(name) like '%".strtolower($search)."%' AND id IN(".$product_id.")  ");
            $resul1 = $get_pro_id1->result_array();
            $product_ids = array();
			$product_id_cat='0';
            if(!empty($resul1)){
                foreach($resul1 as $val){
                    $product_ids[] = $val['id'];
                }

                $product_id = implode(',', $product_ids);
				return trim($product_id,',');
            }else {
				return $product_id = '';
			}
			
		}
	}
	
	function search_products($search='',$product_id=0){
		$product_ids = array();
		$search = preg_replace('/\s+/', ' ',$search);
		$query = $this->db->query("select a.id from product_desc as a,product_inventory as c where LOWER(a.name) like '%".strtolower($search)."%' AND a.approve_reject IN('A','D','B') AND a.status = 1 AND a.id = c.product_id AND c.stock_count > 0 ");
		$result = $query->result_array();
		if(!empty($result)){
			echo "product found";
			// product name found
			foreach($result as $val){
				$product_ids[] = $val['id'];
			}
			$product_id = implode(',', $product_ids);
			return trim($product_id,',');
		}else{
			$query = $this->db->query("select a.id from product_desc as a,brand_info as b,product_inventory as c where a.brand_id = b.user_id AND LOWER(b.company_name) like '%".strtolower($search)."%' AND a.approve_reject IN('A','D','B') AND a.status = 1 AND a.id = c.product_id AND c.stock_count > 0 ");
			$result = $query->result_array();
			if(!empty($result)){
				echo "brand found";
				//brand name found
				foreach($result as $val){
					$product_ids[] = $val['id'];
				}
				$product_id = implode(',', $product_ids);
				return trim($product_id,',');
			}else{
				$wh_con = '';$tag_product_ids = '';$product_ids_in = '';$product_array = array();$tag_ids = array();
				// tag wise search
				echo "tag found";
				$search_keyword = explode(" ",trim($search));
				foreach($search_keyword as $val){
					$wh_con = $wh_con.',\''.$val.'\'';
				}
				$tag_name = trim($wh_con,',');
				$query = $this->db->query('SELECT GROUP_CONCAT(DISTINCT a.id) as tag_ids FROM `tag` as a WHERE a.name in('.$tag_name.') ');
				$res = $query->result_array();
				
				
				if(@$res[0]['tag_ids'] != ''){
					$tag_ids = explode(',',$res[0]['tag_ids']);
					foreach($tag_ids as $val){
						$query2 = $this->db->query('SELECT DISTINCT b.product_id as product_ids FROM product_tag as b WHERE b.tag_id = '.$val.' AND b.product_id > 3000 ');
						$res2 = $query2->result_array();
						foreach($res2 as $val){
							$product_ids_in[] = $val['product_ids'];
						}
					}
					
				}
				
				if(count($tag_ids) > 1)$product_ids_in = array_unique(array_diff_assoc($product_ids_in, array_unique($product_ids_in)));
				
				if($product_ids_in != ''){
					$product_ids = array_filter(explode(',',$product_id));
					// $product_ids_in = array_intersect($product_ids, $product_ids_in);
					$product_ids_in = implode(',',array_filter($product_ids_in));
					return trim($product_ids_in,',');
					
				}else {
					return $product_id = '';//no product found
				}
				
			}
		}
	}
	
	function get_cat_name($cat_id){
        $query = $this->db->query("SELECT `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `category` WHERE 1 and `id` = '".$cat_id."' limit 0,1");
        $res = $query->result_array();
        return $res;
    }
	
	function get_breadcrump($cat_id,$page_type=''){
		$query = $this->db->query("SELECT Concat(T2.category_slug,-T2.id) as slug,T2.category_name as cat_name,T2.id as category_id FROM (SELECT @r AS _id, (SELECT @r := category_parent FROM category WHERE id = _id) AS category_parent, @l := @l + 1 AS lvl FROM (SELECT @r := $cat_id) vars, category m WHERE @r <> 0) T1 JOIN category T2 ON T1._id = T2.id ORDER BY T1.lvl ASC");	
		$result = $query->result_array();
		krsort($result);
		$category_breadcrumps = '';
		// echo "<pre>";print_r($result);exit;
		if(!empty($result)){
			foreach($result as $value){
				if($page_type == 'cat_list'){
					$cat_name = $value['category_id'].'-'.$value['cat_name'];
					$category_breadcrumps = $category_breadcrumps.'<li><a onclick="get_child_cat('.@$value['category_id'].',\''.$cat_name.'\');" >'.@$value['cat_name'].'</a></li>';
				}else{
					$category_breadcrumps = $category_breadcrumps.'<li><a href="'.base_url().'category/product-filter/'.@$value['slug'].'">'.@$value['cat_name'].'</a></li>';	
				}
				
			}
		}
		return $category_breadcrumps;
	}
	
	function get_all_product_category($product_ids){
		
		$query = $this->db->query('SELECT DISTINCT a.id,a.category_name,a.category_slug,a.category_image,a.level from category a,category_object_relationship as b WHERE a.id = b.category_id AND a.category_parent != -1 and `count` > 0 and status=1 AND b.object_type = "product" AND b.object_id IN ('.$product_ids.') AND (a.level = 3 || a.level = 4)ORDER BY id,sort_order ASC ');
		$res = $query->result_array();
		
		$cat_list = array(); $i = 0;
		if(!empty($res)){
			foreach($res as $val){
				if($val['category_image'] == ''){
					$val['category_image']= base_url().'assets/images/image-na.jpg'; }
				else { 
					$val['category_image'] = PRODUCT_CAT_URL.$val['category_image'];
				}
				$cat_list[$i]['category_id'] = $val['id'];
				$cat_list[$i]['category_name'] = $val['category_name'];
				$cat_list[$i]['category_slug'] = $val['category_slug'];
				$cat_list[$i]['category_image'] = $val['category_image'];
				$cat_list[$i]['level'] = $val['level'];
				$cat_list[$i]['has_children'] = $this->has_children($val['id']);
				$cat_list[$i]['is_last'] = (string)$this->check_is_lastlvl($val['id']);
				$i++;
			}
		}
		if(@$_GET['test'] == 1){
			echo "<pre>";print_r($cat_list);exit;
		}
		return $cat_list;
	}

	function get_order_category($cartid)
	{
		$this->db->select('cor.category_id,c.category_name,c.category_parent,c.category_slug');
		$this->db->from('category_object_relationship as cor');
		$this->db->join('category as c', 'c.id = ABS(cor.category_id)','LEFT');
		$this->db->where('cor.object_type','order');		
		$this->db->where('cor.object_id',$cartid);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}

	function get_order_attributes($cartid)
	{
		$this->db->select('aor.attribute_id,a.attr_value_name,a.attribute_id as attribute_parent,a.attr_value_slug');
		$this->db->from('attribute_object_relationship as aor');
		$this->db->join('attribute_values_relationship as a', 'a.id = ABS(aor.attribute_id)','LEFT');
		$this->db->where('aor.object_type','order');		
		$this->db->where('aor.object_id',$cartid);
		$query = $this->db->get();		
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}

	
	function get_product_recommendation($positive_category_str,$positive_attribute_str,$negative_category_str,$negative_attribute_str)
	{
		$product_array=array(); $cat_prodn=array(); $cat_prodp=array();
		$wc_catp = '';$wc_catn = '';$wc_attrp = '';$wc_attrn = '';$wc_cat_prod = '';

		if($positive_category_str!=''){
			$wc_catp = 'category_id in ('.$positive_category_str.')';
		}else{
			$wc_catp = '1=1';
		}

		if($negative_category_str!=''){
			$wc_catn = 'category_id not in ('.$negative_category_str.')';
		}else{
			$wc_catn = '1=1';
		}

		if($positive_attribute_str!=''){
			$wc_attrp = 'attribute_id in ('.$positive_attribute_str.')';
		}else{
			$wc_attrp = '1=1';
		}

		//$attributSizeList =  $this->attributes_values_list('24,26,18,25,27,28,29,30,33,38');
		$attributSizeList =  $this->attributes_values_list('24,26,18,25,27,28,29,30,33,38,56,63,36,37');
		$attrSizeList = array_column($attributSizeList, 'id');
		$positive_attribute_arr = explode(',', $positive_attribute_str);
		$attrSizeList_diff = array_diff($attrSizeList, $positive_attribute_arr);
		$attrSizeList_diff_str = implode(',', $attrSizeList_diff);

		if($negative_attribute_str!=''){
			$wc_attrn = 'attribute_id in ('.$negative_attribute_str.','.$attrSizeList_diff_str.')';
		}else{
			$wc_attrn = '1=1';
		}
		
		
		$query = $this->db->query('Select object_id from category_object_relationship where '.$wc_catp.' and '.$wc_catn.' and object_type="product" order by object_id desc');
		$res = $query->result_array();		
		$cat_prod = array_column($res, 'object_id');
		$cat_prod_str = implode(',',$cat_prod);		

		if($cat_prod_str!=''){
			$wc_cat_prod = 'object_id in ('.$cat_prod_str.')';
		}else{
			$wc_cat_prod = '1=1';
		}


		//echo '<pre>';print_r($attrSizeList_diff);
		$query_attrp = $this->db->query('Select object_id from attribute_object_relationship where '.$wc_attrp.' and object_type="product" and '.$wc_cat_prod.' order by object_id desc');
		//echo $this->db->last_query();
		$res_attrp = $query_attrp->result_array();
		$attr_prodp = array_column($res_attrp, 'object_id');		

		$query_attrn = $this->db->query('Select object_id from attribute_object_relationship where '.$wc_attrn.' and object_type="product" and '.$wc_cat_prod.' order by object_id desc');
		//echo $this->db->last_query();
		$res_attrn = $query_attrn->result_array();
		$attr_prodn = array_column($res_attrn, 'object_id');
		$attr_prod = array_diff($attr_prodp,$attr_prodn);	

		$result_product = array_intersect($cat_prod, $attr_prod);
		//$result_product = $attr_prod;
		//echo '<pre>';print_r($result_product);
		
		$prod_data['offset'] = '0';
		$prod_data['limit_cond'] = '10';
		$prod_data['user_id'] = '';
		//$prod_data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
		$prod_data['product_link'] = base_url().'sc_admin/assets/products/thumb_124/';

		$product_ids = implode(',',$result_product);
		if(!empty($product_ids))$product_array = $this->Mobile_look_model->get_products_list($result_product,$prod_data);
		//echo $this->db->last_query();exit;
		//$query_res = $this->db->last_query();
		//$query_hidden = '<div id="hidden_qu" style="display:none;">'..'</div>'
		//echo '<pre>';print_r($product_array);exit;
		return $product_array;
	}

	function getLastLevelCategory()
	{
		//$query = $this->db->query('SELECT t1.id,t1.category_name FROM category AS t1 LEFT JOIN category as t2 ON t1.id = t2.category_parent WHERE t2.id IS NULL');
		$this->db->select('c1.id,c1.category_name,c1.category_slug,c1.category_parent');
		$this->db->from('category as c1');
		$this->db->join('category as c2', 'c1.id = c2.category_parent','LEFT');
		$this->db->where('c2.id',NULL);		
		$query = $this->db->get();		
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}

	function get_product_category_level2($product_ids,$product_result){

		if($product_ids!='')
		{
			$w_c_prod = ' b.object_id IN ('.$product_ids.') ';
		}else
		{
			$w_c_prod = '1=1';
		}
		
		
		$query = $this->db->query('SELECT DISTINCT a.id,a.category_name,a.category_slug,a.category_image,a.level,b.object_id from category a,category_object_relationship as b WHERE a.id = b.category_id AND status=1 AND b.object_type = "product" AND '.$w_c_prod.'  AND (a.level = 2 )ORDER BY id,sort_order ASC ');
		$res = $query->result_array();
		//echo '<pre>';print_r($res);exit;
		//echo '<pre>';print_r($product_result);exit;
		$cat_list = array(); $i = 0;
		if(!empty($res)){
			foreach($res as $val){			
				//$product_key = array_search($val['object_id'], array_column($product_result, 'product_id'));				
				$product_key = array_search($val['object_id'], array_map(function ($product_result) {return $product_result->product_id;}, $product_result));						
				$cat_list[$val['id']]['category_id'] = $val['id'];
				$cat_list[$val['id']]['category_name'] = $val['category_name'];
				$cat_list[$val['id']]['category_slug'] = $val['category_slug'];	
				$cat_list[$val['id']]['product_list'][$val['object_id']] = $product_result[$product_key];								
				$i++;
			}
		}			
		return $cat_list;
	}

	function get_cart_data($cartid)
	{
		$this->db->select('ci.*');
		$this->db->from('cart_info as ci');		
		$this->db->where('ci.id',$cartid);		
		$query = $this->db->get();		
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}

	function get_scboxdata_byproduct($productid)
    {        
        $result = '';
        $scbox_pack = unserialize(SCBOX_PACKAGE);  
        foreach($scbox_pack as $val)
        {
            if($productid==$val->productid)
            {
                $result = $val;
            }
        }        
        return $result;
    }

    function get_category_level2(){
		$query = $this->db->query('SELECT DISTINCT a.id,a.category_name,a.category_slug,a.category_image,a.level from category as a WHERE a.status="1"  AND (a.level = 2 ) ORDER BY a.id,a.sort_order ASC ');
		$res = $query->result_array();
		//echo '<pre>';print_r($res);exit;
		//echo '<pre>';print_r($product_result);exit;
		$cat_list = array(); $i = 0;
		if(!empty($res)){
			foreach($res as $val){					
									
				$cat_list[$val['id']]['category_id'] = $val['id'];
				$cat_list[$val['id']]['category_name'] = $val['category_name'];
				$cat_list[$val['id']]['category_slug'] = $val['category_slug'];												
				$i++;
			}
		}			
		return $cat_list;
	}

	function attributes_values_list($ids){
        
        $query = $this->db->query("SELECT `id`, `attribute_id`, `attr_value_name`, `attr_value_content`, `attr_value_slug`, `attr_hashtag`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `attribute_values_relationship` WHERE 1 and `attribute_id` IN (".$ids.")  order by attr_value_name");
        $res = $query->result_array();
        //echo $this->db->last_query();
        return $res;   
    }
	
}