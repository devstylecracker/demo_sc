<?php
 
class Sitemap_model extends MY_Model{
 
    public function __construct(){
        $this->load->database();
    }
    public function getURLS(){
  
    $query= $this->db->query("select a.`user_id`, a.`company_name`,b.`user_name` from `brand_info` as a,`user_login` 
      as b,`user_info` as c 
      where a.`user_id` = b.`id` and a.`user_id` = c.`user_id` and a.`show_in brand_list` = 1 order by a.modified_datetime desc ");
    
       $result= $query->result_array();
     
     return $result;
   }



       


     public  function getPosts(){
      //count(z.id) as total,
     $query = $this->db->query('select  z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as
      look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag from 
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,
  a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1) 
    as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6)
    order by z.modified_datetime desc');

 //echo $this->db->last_query(); exit
 
 
 $result = $query->result_array();
  return $result;

   }

  
    public function getProd(){
       $query = $this->db->query('SELECT a.`id`, a.`product_cat_id`, a.`product_sub_cat_id`, a.`brand_id`, a.`designer_id`, a.`name`, a.`url`, a.`description`, a.`slug`, a.`price`, a.`price_unit`, a.`price_range`, a.`consumer_type`, a.`target_market`, a.`rating`, a.`body_part`, a.`body_type`, a.`body_shape`, a.`personality`, a.`age_group`, a.`image`, a.`approve_reject`, a.`reason`, a.`hits`, a.`referred_count`, a.`status`, a.`is_promotional`, a.`is_delete`, a.`product_used_count`, a.`product_stock`, a.`created_by`, a.`modified_by`, a.`created_datetime`, a.`modified_datetime`, a.`product_sku`, a.`product_discount`, a.`discount_start_date`, a.`discount_end_date`, a.`product_color`, a.`sc_product_color` FROM `product_desc` as a,brand_info as b WHERE 1 and a.`status` = 1 and a.`is_delete`=0 and (a.`approve_reject` = "A" || a.`approve_reject` = "D") and a.`brand_id` = b.`user_id` and b.`is_paid`=1 AND b.`show_in brand_list` = 1 order by a.`id` desc');
      $result = $query->result_array();
      return $result;
    }
    
    public function getSubCategory(){
        $query = $this->db->query('SELECT `id`, `product_cat_id`, `name`, `slug`, `status`, `is_delete`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `product_sub_category` WHERE 1 and status =1 and is_delete = 0 and product_cat_id IN(1,12)');
        $result = $query->result_array();
        return $result;
    }

   public function getvarition(){
         $query = $this->db->query('SELECT a.`id`,b.`product_cat_id`, a.`prod_sub_cat_id`, a.`name`, a.`slug`, a.`status`, a.`is_delete`, a.`size_guide`, a.`show_in_filter`, a.`created_by`, a.`modified_by`, a.`created_datetime`, a.`modified_datetime` FROM `product_variation_type` as a,product_sub_category as b WHERE 1 and a.`show_in_filter` = 1 and a.`status` = 1 and a.`is_delete` = 0 and a.`prod_sub_cat_id` = b.`id`');
        $result = $query->result_array();
        return $result;
   }

    public function getvaritions(){
         $query = $this->db->query('SELECT a.`id`,c.`product_cat_id`, a.`product_variation_type_id`, a.`name`, a.`slug`, a.`status`, a.`is_delete`, a.`show_in_filter`, a.`created_by`, a.`modified_by`, a.`created_datetime`, a.`modified_datetime` FROM `product_variation_value` as a,`product_variation_type` as b,`product_sub_category` as c WHERE 1 and a.`product_variation_type_id` = b.`id` and b.`prod_sub_cat_id` = c.`id` and c.`product_cat_id` IN(1,12) and a.`status` = 1 and a.`is_delete` = 0 and a.`show_in_filter` = 1');
        $result = $query->result_array();
        return $result;
   }
   
	public function getallcategories(){
		$query = $this->db->query('SELECT a.id,a.category_slug from category as a');
		$result = $query->result_array();
		return $result;
	}
   

}
?>


