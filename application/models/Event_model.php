<?php

class Event_model extends MY_Model {
	
	
	function get_all_event($data){
		
		if(@$data['search_by'] == '1'){ $search_cond = " AND a.object_id IN(".strip_tags(trim(@$data['table_search'])).")"; }
		else{ $search_cond = ''; }
		
		$where_cond = '';
		//gender
		if(@$data['gender'] == 'men' || @$data['gender'] == '2'){
			$data['gender'] = '2'; 
			$where_cond = $where_cond." AND b.object_meta_key = 'gender' AND ( b.object_meta_value = '".$data['gender']."' ||  b.object_meta_value = '3' ) ";
		}else if(@$data['gender'] == 'women' || @$data['gender'] == '1'){
			$data['gender'] = '1';
			$where_cond = $where_cond." AND b.object_meta_key = 'gender' AND ( b.object_meta_value = '".$data['gender']."' ||  b.object_meta_value = '3' ) ";
		}else{ 
			$data['gender'] = '0';
		}
		
		if($data['body_shape'] >0 && $data['pa_pref1'] >0){
			
			$query = $this->db->query("select Distinct a.object_id as id,a.object_name as name,CONCAT('".COLLECTION_AND_EVENT_URL."',a.object_image) as image from object as a,object_meta as b where a.object_id = b.object_id AND a.object_type = 'event' AND b.object_status != 0 AND (b.object_meta_key = 'body_shape' && FIND_IN_SET('".$data['body_shape']."',b.object_meta_value)) AND a.object_id IN(select obj.object_id from object_meta as obj where a.object_id = obj.object_id AND (obj.object_meta_key = 'pref1' && FIND_IN_SET('".$data['pa_pref1']."',obj.object_meta_value))) AND a.object_status = 3 ".$search_cond." order by a.object_id desc ".$data['limit_cond']);
			
		}else{
			
			$query = $this->db->query("select Distinct a.object_id as id,a.object_name as name,CONCAT('".COLLECTION_AND_EVENT_URL."',a.object_image) as image from object as a,object_meta as b where a.object_id = b.object_id AND a.object_type = 'event' AND b.object_status != 0 AND a.object_status = 3 ".$where_cond." ".$search_cond." order by a.object_id desc ".$data['limit_cond']);
		}
		// echo $this->db->last_query();exit;
		$result = $query->result_array();
		// echo "<pre>";print_r($result);exit;
		$event_array = array();
		foreach($result as $val){
			$event_array[] = $this->get_single_event($val['id'],$data);
		}
		return $event_array;
		
		
	}
	
	function get_single_event($event_id,$data){
		$query = $this->db->query("select Distinct a.object_id as id,a.object_name as name,b.object_meta_key,b.object_meta_value,CONCAT('".COLLECTION_AND_EVENT_URL."',a.object_image) as object_image,a.object_slug as object_slug from object as a,object_meta as b where a.object_id = b.object_id AND a.object_type = 'event' AND b.object_status != 0 AND b.object_id = $event_id AND a.object_status = 3 order by a.object_id desc ");
		$result = $query->result_array();
		$single_event = array();
		if(!empty($result)){
			
			$single_event['event_id'] = $event_id;
			$single_event['event_name'] = $result[0]['name'];
			$single_event['event_image'] = $result[0]['object_image'];
			$single_event['object_slug'] = $result[0]['object_slug'];
			$single_event['is_wishlisted'] = $this->is_wishlisted('event',$event_id,@$data['user_id']);
			$single_event['share_url'] = base_url().'event_web/get_single_event/'.$event_id;
			foreach($result as $val){
				if(@$data['type'] != 'single' || (@$data['type'] == 'single' && $val['object_meta_key'] != 'event_products')){
					if($val['object_meta_key'] != 'event_start_date' && $val['object_meta_key'] != 'event_end_date'){
						$single_event[$val['object_meta_key']] = $val['object_meta_value'];
					}else if($val['object_meta_key'] == 'event_start_date'){
						$date = explode(' ',$val['object_meta_value']);
						$time  = date("g:i a", strtotime(@$date[1]));
						$newDate = date("d-m-Y", strtotime(@$date[0]));
						$single_event['event_start_date'] = $newDate;
						$single_event['event_start_time'] = strtoupper($time);
					}else if($val['object_meta_key'] == 'event_end_date'){
						$date = explode(' ',$val['object_meta_value']);
						$time  = date("g:i a", strtotime(@$date[1]));
						$newDate = date("d-m-Y", strtotime(@$date[0]));
						$single_event['event_end_date'] = $newDate;
						$single_event['event_end_time'] = strtoupper($time);
					}
					
				}else if(@$data['type'] == 'single' && $val['object_meta_key'] == 'event_products'){
					$event_products = @unserialize($val['object_meta_value']);
					if(!empty($event_products)){
						$single_event['event_products'] = $this->Mobile_look_model->get_products_list($event_products,$data);
					}else{
						$single_event['event_products'] = array();
					}
				}
				
			}
			
		}
		
		return $single_event;
	}
	
	function get_products_list($product_ids,$data){
		$data['limit_cond'] = " LIMIT ".$data['offset'].",".$data['limit'];
		if($product_ids != ''){
			$product_id_in = implode(',',$product_ids);
			$query = $this->db->query("select a.id as product_id,CONCAT('".$data['product_link']."',a.image) as product_image,a.name,(select cat.category_name from category_object_relationship as co,category as cat where co.category_id = cat.id and co.object_type = 'product' and co.object_id = a.id order by co.category_id desc limit 1) product_category,b.company_name as brand_name,a.price,CASE 
							WHEN  a.discount_start_date <= CURRENT_TIMESTAMP() and a.discount_end_date >= CURRENT_TIMESTAMP() then a.compare_price
							ELSE 0 END AS compare_price,b.user_id as brand_id from product_desc as a,brand_info as b where a.brand_id = b.user_id AND a.id IN(".$product_id_in.") AND a.id = a.parent_id ".$data['limit_cond']);
			$result = $query->result_array();
			// print_r($result);exit;
			$product_array = array();
			
			foreach($result as $val){
				$productobject = new stdClass(); 
				$productobject->product_id = $val['product_id'];
				$productobject->product_image = $val['product_image'];
				$productobject->product_name = $val['name'];
				$productobject->product_category = $val['product_category'];
				$productobject->brand_name = $val['brand_name'];
				/* calculate discount and discount % */
				$discount_price = 0;$discount_percent =0;$new_discount_percent=0;
				$brand_discount = $this->brand_discount($val['brand_id']);
				$product_discount = $this->product_discount($val['product_id']);
				// print_r($brand_discount);exit;
				if(!empty($brand_discount)){
					$productobject->discount_percent = $brand_discount['discount_percent'];
					$discount_price = $val['price'] - ($val['price'] * ($brand_discount['discount_percent']/100));
					$productobject->discount_price = (string)($val['price'] - ($val['price'] * ($brand_discount['discount_percent']/100)));
				}else if(!empty($product_discount)){
					$productobject->discount_percent = $product_discount['discount_percent'];
					$discount_price = $val['price'] - ($val['price'] * ($product_discount['discount_percent']/100));
					$productobject->discount_price = (string)($val['price'] - ($val['price'] * ($product_discount['discount_percent']/100)));
				}else {
					$productobject->discount_percent = '0';
					$discount_price = 0;
					$productobject->discount_price = '';
				}
				
			/* calculate discount and discount % */
			
			/*  added for new discount compare price code start*/
			
			// $val['compare_price'] = 100;$val['price']=80;$discount_percent = 0;$discount_price=72;
			// echo $val['product_id'].'----'.$val['compare_price'].'----'.$val['price'].'</br>';
			if($discount_percent > 0 || $val['compare_price']>$val['price']){
				$productobject->price = (string)$val['compare_price'];
				$productobject->discount_price = (string)$discount_price;
				$new_discount_percent = $this->calculate_discount_percent($val['compare_price'],$discount_price,$val['price']);
				$productobject->discount_percent = (string)$new_discount_percent;
			}else {
				$productobject->price = (string)$val['price'];
				$productobject->discount_price = (string)$discount_price;
				$val['compare_price'] = $val['price'];
				$new_discount_percent = $this->calculate_discount_percent($val['compare_price'],$discount_price,$val['price']);
				$productobject->discount_percent = (string)$new_discount_percent;
			}
				
			
			/*  added for new discount compare price code end*/
			
				$productobject->is_wishlisted = $this->is_wishlisted('product',$val['product_id'],$data['user_id']);
				$productobject->is_available = $this->is_available($val['product_id']);
				$product_array[] = $productobject;
			}
			
			return $product_array;
		}else {
			return array();
		}
	}
	
	function brand_discount($brand_id){
		$query = $this->db->query(" select IF(a.discount_percent is null, '0', a.discount_percent) as discount_percent FROM discount_availed AS a WHERE a.brand_id = $brand_id AND a.discount_type_id = 1 AND NOW() >= a.start_date AND NOW() <= a.end_date AND a.status=1 ");
		$res = $query->result_array();
		if (!empty($res)) return $res[0]; else return null;
	}
	
	function product_discount($product_id){
		$query = $this->db->query("SELECT  IF(sum(a.discount_percent) is null, '0', sum(a.discount_percent)) as discount_percent FROM discount_availed AS a WHERE a.product_id = $product_id AND a.discount_type_id = 2 AND NOW() >= a.start_date AND NOW() <= a.end_date AND a.status=1 ");
		$res = $query->result_array();
		if (!empty($res)) return $res[0]; else return null;
	}
	
	function calculate_discount_percent($compare_price,$discount_price,$product_price=null){
		$discount_percent = 0;
		if($compare_price > 0 && $discount_price > 0){
			$discount_percent = round((1-($discount_price/$compare_price))*100);
		}else if($compare_price > 0 && $product_price > 0){
			$discount_percent = round((1-($product_price/$compare_price))*100);
		}
		
		return $discount_percent;
	}
	
	function is_wishlisted($fav_for,$fav_id,$user_id){
		if($user_id>0){
			$query = $this->db->query("select id,user_id from favorites where fav_id=$fav_id AND user_id=$user_id AND fav_for = '$fav_for' ");
			$res = $query->result_array();
			if(count($res)>0){
				return '1';
			}else return '0';
		}else return 0;
		
	}
	
	function is_available($product_id){
		
		// $query = $this->db->query("select a.stock_count,a.id from product_desc as a,attribute_object_relationship as b,attribute_values_relationship as c where (a.id = $product_id OR a.parent_id = $product_id) AND b.object_id in(select pd.id from product_desc pd where pd.id = $product_id OR pd.parent_id = $product_id) AND c.attribute_id = ".ATTRIBUTE_SIZE_ID." AND b.attribute_id = c.id order by stock_count desc");
		$query = $this->db->query("select a.stock_count from product_desc as a where a.id = $product_id OR a.parent_id = $product_id order by stock_count desc");
		$res = $query->result_array();
		// echo $this->db->last_query();
		// print_r($res);exit;
		if(!empty($res)){
			if($res[0]['stock_count'] > 0){ return '1';}else return '0';
		}else return '0';
	}
	
	function user_wishlist($user_id,$fav_for,$fav_id){          
        
        $query = $this->db->query("select id,user_id from favorites where fav_id=$fav_id AND user_id=$user_id AND fav_for = '$fav_for' ");
		$res = $query->result_array();
		if(!empty($res)){
			$id = $res[0]['id'];
			$query = $this->db->query(" Delete from favorites where id=$id ");
			return 1;
		}else{ 
			$data = array('user_id'=>$user_id,'fav_id'=>$fav_id,'fav_for'=>$fav_for);
			$result = $this->db->insert('favorites', $data);         
			if(!empty($result)){
				return 1;
			}else{ 
				return 2;
			}
		}
		
    }
	
	
 }