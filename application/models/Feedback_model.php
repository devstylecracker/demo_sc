<?php
class Feedback_model extends MY_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


    function save_feedback_answer($order_id,$answer,$user_id=0,$objectid=0){

        $feedback_qs = unserialize(FEEDBACK_QUESTIONS);        
        $query = $this->db->get_where('object_meta', array('object_id' => $objectid,'created_by' => $user_id,'object_meta_key' => $order_id ));

        $count = $query->num_rows(); //counting result from query
        
        if($count>0){
                /*Update the PA*/

                $data = array('object_meta_value'=>$answer,'created_by'=>$user_id,'object_status'=>1);
                //$this->db->update('object_meta', $data, array('object_id' => $objectid,'created_by' => $user_id,'object_meta_key' => $question ));    
                $this->db->update('object_meta', $data, array('created_by' => $user_id,'object_meta_key' => $order_id, 'object_id' => $objectid ));  
               // echo $this->db->last_query();exit;          
                return 1;
        }else{
            /*Insert the PA*/
            
            $data = array('object_id'=>$objectid,'object_meta_key'=>$order_id,'object_meta_value'=>$answer,'created_datetime'=>date('Y-m-d H:i:s'),'created_by'=>$user_id,'modified_by'=>$user_id);
            $this->db->insert('object_meta', $data);

            return 1;
			
			//$query = $this->db->query('update user_login set question_comp = 1,question_resume_id = 1 where id = '.$user_id.' ');
        }


    }

    function get_objectmeta($objectid,$objectmetakey,$userid)
    {
        //$query = $this->db->get_where('object_meta', array('object_id' => $objectid,'object_meta_key' => $objectmetakey ,'created_by' => $userid ));
        $query = $this->db->get_where('object_meta', array('object_meta_key' => $objectmetakey ,'created_by' => $userid ));
        $result = $query->result_array();
        return $result;
    }

    function get_scboxdata($objectid,$objectmeta)
    {
        /*$query = $this->db->query('select a.object_id,a.object_meta_key,a.object_meta_value from object_meta as a where a.object_id ='.$objectid.' and a.object_meta_key Like "'.$objectmeta.'"  and a.created_by=0 and a.modified_by=0 ');  
        //echo $this->db->last_query();exit;          
        $result = $query->result_array();
        return $result;*/
        $result = '';
        $scbox_pack = unserialize(SCBOX_PACKAGE);  
        foreach($scbox_pack as $val)
        {
            if($objectid==$val->id)
            {
                $result = $val;
            }
        }        
        return $result;
    }

    function get_object($objectslug)
    {
        $query = $this->db->get_where('object', array('object_slug' => $objectslug ));
        $result = $query->result_array();
        return $result;
    }
	
	function get_userfeedbackdata()
	{
		 $object = $this->get_object('order_feedback');
		 //echo'<pre>';print_r($object_id); exit;$object[0]['object_id']
		 $object_id = $object[0]['object_id'];
		$query = $this->db->query('Select a.object_meta_key,a.object_meta_value,a.created_datetime,b.first_name,b.last_name,b.email_id,b.mobile_no,b.user_id from object_meta as a inner join order_info as b on a.object_meta_key = b.order_display_no and a.object_id='.$object_id.' ');  
        //echo $this->db->last_query();exit;          
        $result = $query->result_array();
        return $result;
		
	}

    function getscboxusers()
    {  
        $query = $this->db->query("Select distinct(a.user_id),a.first_name,a.last_name,a.email_id,a.mobile_no from order_info as a, order_product_info as b where a.id = b.order_id and a.order_status!=9 and brand_id IN (".SCBOX_BRAND_ID.") group by a.user_id ");
        //echo $this->db->last_query();exit;
        $result = $query->result_array();       
        return $result;
    }
   
    function getnonscboxusers()
    {      
              
        $query = $this->db->query("Select b.id, b.email,c.role_id  from user_login as b, user_role_mapping as c where b.id = c.user_id and b.id not in (Select a.user_id from order_info as a, order_product_info as b where a.id = b.order_id and a.order_status!=9 and brand_id IN (".SCBOX_BRAND_ID.") ) and c.role_id not in (6,7,8) and b.test_user=0 ");
       // echo $this->db->last_query();exit;
        $result = $query->result_array();
        return $result;
    }

    function getusersdata($userids)
    {     
        $result = array(); 
        if(!empty($userids))
        {
            $query = $this->db->query("Select  b.id, b.email from user_login as b where b.id in (".$userids.") ");            
            $result = $query->result_array();           
        }    
        return $result;    
    }

    function getscboxusersConfirm()
    {  
        $query = $this->db->query("Select distinct(a.user_id),a.first_name,a.last_name,a.email_id,a.mobile_no from order_info as a, order_product_info as b where a.id = b.order_id and a.order_status=1 and b.order_status=1 and a.created_datetime >= '2017-10-23 00:00:00' and a.created_datetime <= '2017-11-3 23:59:59' and a.brand_id IN (".SCBOX_BRAND_ID.") group by a.user_id ");
       // echo $this->db->last_query();exit;
        $result = $query->result_array();       
        return $result;
    }
 }
?>
