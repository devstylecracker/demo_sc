<?php

class Blog_model extends MY_Model {	

	public function get_all_blog($data)
    {
	      /*Curl to get blog data */
	      if (empty($data['limit']))
	      $data['limit']=3;
	  		if (empty($data['offset']))
	      $data['offset']=1;
	        $domain = $this->config->item('blog_url');
	        $endpoint = $domain."/wp-json/wp/v2/posts?per_page=".$data['limit']."&page=".$data['offset'];
			// echo $endpoint;exit;
	       /* if (isset($data['meta_query'])){
	        	$args = array (
    				'filter' => array (
        			'meta_query' => $data['meta_query']
    				),
				);
				$field_string = http_build_query( $args );
				$endpoint =$domain."/wp-json/wp/v2/posts?".$field_string;
				echo $endpoint;
	        }*/
	        $curl = curl_init($endpoint);

	        curl_setopt_array($curl, [
	          CURLOPT_RETURNTRANSFER => true,
	          CURLOPT_URL            => $endpoint,
	        ]);
	        $response = curl_exec($curl);
	        $decoderesponse = json_decode($response, true);
	       /* echo '<pre>';
	          print_r($decoderesponse);
	        echo '</pre>';*/
	      /*End Curl to get blog data */

	      $blog_data = array();
	      // echo "<pre>";
	      // print_r($decoderesponse);
	      // echo "</pre>";
	      // exit;
	      $post_num = 3;
	      $i=0;
	      foreach($decoderesponse as $value)
	      {
	          $blog_date = date_create($value['date']);
	          $blog_posted_date = date_format($blog_date, 'd F,Y');
	          $blog_data[$i]['post_id'] = $value['id'];
	          $blog_data[$i]['post_date'] = $blog_posted_date;
	          $blog_data[$i]['post_title'] = $value['title']['rendered'];
	          $blog_data[$i]['post_slug'] = $value['slug'];
	          $blog_data[$i]['post_link'] = $value['link']; 
	          $blog_data[$i]['post_img'] = $this->getblogmedia($value['featured_media']); 
	          $blog_data[$i]['post_type'] = $this->getblogCategory($value['categories'][0]); 
	          $i++;
	      }
	      return $blog_data;
    }
	
	/*function get_blog_data($data){
		$blog_db = $this->load->database('blog_db', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
		if(@$data['search_by'] == '1'){ $search_cond = " AND a.ID IN(".strip_tags(trim(@$data['table_search'])).")"; }
		else{ $search_cond =''; }
		$query = $this->db->query("SELECT a.ID,a.post_date,a.post_title,a.post_status,a.post_name,a.guid,CONCAT('http://www.stylecracker.com/blog/',a.post_name) as blog_url FROM wp_posts as a WHERE a.post_status = 'publish' and a.post_type = 'post' ".$search_cond." order by a.post_date desc ".$data['limit_cond']);

		$result = $query->result_array();
		$blog_array = array();$i=0;
		if(!empty($result)){
			foreach($result as $val){
				$blog_array[$i]['id'] = $val['ID'];
				$blog_array[$i]['post_title'] = $val['post_title'];
				$blog_array[$i]['post_name'] = $val['post_name'];
				$blog_array[$i]['blog_url'] = $val['blog_url'];
				$blog_array[$i]['image'] = $this->get_post_thumb($val['ID']);
				$blog_array[$i]['post_type'] = $this->get_blog_Category($val['ID']); 
				$blog_array[$i]['is_wishlisted'] = $this->is_wishlisted('blog',$val['ID'],$data['user_id']);
				$i++;
			}
		}
		return $blog_array;
	}*/

	function get_blog_data($data){
		$blog_db = $this->load->database('blog_db', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
		if(@$data['search_by'] == '1'){ $search_cond = " AND a.ID IN(".strip_tags(trim(@$data['table_search'])).")"; }
		else{ $search_cond =''; }
		$result1 = array();
		
		$gender = '';$bodyshape = '';$personality = '';
		if($data['user_id'] > 0){
			$gender = ($this->Pa_model->get_gender($data['user_id'])!='' && $this->Pa_model->get_gender($data['user_id'])==1) ? 'Female' : 'Male';
			$bodyshape = $this->get_userpa_answer($data['user_id'],'1')['answer'];			
			$personality = $this->get_userpa_answer($data['user_id'],'2')['answer'];		
			$bodyshape_arr = explode(' ',$bodyshape);
			$bodyshape = $bodyshape_arr[0];
		}
		
		if($gender!='' && $bodyshape!='' && $personality!='')
		{
			$query1 = $this->db->query("SELECT a.ID,a.post_date,a.post_title,a.post_status,a.post_name,a.guid,CONCAT('https://www.stylecracker.com/blog/',a.post_name) as blog_url,LEFT(a.post_content , 150) as post_desc FROM stylecra_stylecracker_v1_blogbeta.wp_posts as a 
				INNER JOIN stylecra_stylecracker_v1_blogbeta.wp_postmeta as bodymeta ON a.ID = bodymeta.post_id 
				INNER JOIN stylecra_stylecracker_v1_blogbeta.wp_postmeta as stylemeta ON a.ID=stylemeta.post_id  
				INNER JOIN 	stylecra_stylecracker_v1_blogbeta.wp_postmeta as gendermeta ON a.ID = gendermeta.post_id
				WHERE a.post_status = 'publish' and a.post_type = 'post' 
				AND gendermeta.meta_key = 'gender' AND gendermeta.meta_value LIKE '%".$gender."%'
				AND bodymeta.meta_key = 'body_shape_male' AND bodymeta.meta_value LIKE '%".$bodyshape."%' 
				AND stylemeta.meta_key = 'style_preference_1_male' and stylemeta.meta_value LIKE '%".$personality."%' 
				$search_cond order by a.post_date desc ".$data['limit_cond']);
			$result1 = $query1->result_array();
			/*if($_GET['test'] == 1){
					echo "<pre>";print_r($result1);exit;
				}*/
				if(empty($result1))
				{
					$query = $this->db->query("SELECT a.ID,a.post_date,a.post_title,a.post_status,a.post_name,a.guid,CONCAT('https://www.stylecracker.com/blog/',a.post_name) as blog_url,LEFT(a.post_content , 150) as post_desc FROM stylecra_stylecracker_v1_blogbeta.wp_posts as a WHERE a.post_status = 'publish' and a.post_type = 'post' $search_cond order by a.post_date desc ".$data['limit_cond']);
					// echo $this->db->last_query();exit;
					$result = $query->result_array();
				}
		}else if(empty($result1))
		{
			$query = $this->db->query("SELECT a.ID,a.post_date,a.post_title,a.post_status,a.post_name,a.guid,CONCAT('https://www.stylecracker.com/blog/',a.post_name) as blog_url,LEFT(a.post_content , 150) as post_desc FROM stylecra_stylecracker_v1_blogbeta.wp_posts as a WHERE a.post_status = 'publish' and a.post_type = 'post' $search_cond order by a.post_date desc ".$data['limit_cond']);
			// echo $this->db->last_query();exit;
			$result = $query->result_array();
		}
		
		$blog_array = array();$i=0;
		if(!empty($result)){
			foreach($result as $val){
				$blog_array[$i]['id'] = $val['ID'];
				$blog_array[$i]['post_title'] = $val['post_title'];
				$blog_array[$i]['post_name'] = $val['post_name'];
				$blog_array[$i]['blog_url'] = $val['blog_url'];
				$blog_array[$i]['post_desc'] = strip_tags($val['post_desc']);
				// $blog_array[$i]['image'] = $this->get_post_thumb($val['ID']);
				// added for image
				$blog_img_ext = strrchr($this->get_post_thumb($val['ID']),'.');
                $add_blog_thumb_dim = '-374x212'.$blog_img_ext;
                $new_blog_img = str_replace($blog_img_ext,$add_blog_thumb_dim,$this->get_post_thumb($val['ID']));
				
                $url = $new_blog_img;
                $headers = @get_headers($url);
                if(strpos($headers[0],'404') === false){
                    $blog_array[$i]['image'] =$url;
                }else{
                    $blog_array[$i]['image'] =$this->get_post_thumb($val['ID']);
                }
				// added for image
				$blog_array[$i]['post_type'] = $this->get_blog_Category($val['ID']); 
				$blog_array[$i]['is_wishlisted'] = $this->is_wishlisted('blog',$val['ID'],$data['user_id']);
				$i++;
			}
		}
		return $blog_array;
	}
	
	function get_post_thumb($id){

        $query = $this->db->query('select b.guid from stylecra_stylecracker_v1_blogbeta.`wp_postmeta` as a,stylecra_stylecracker_v1_blogbeta.wp_posts as b where a.`meta_key` = "_thumbnail_id" and a.`meta_value` = b.`ID` and a.`post_id`='.$id);

        $result = $query->result_array();
        if(!empty($result)){
            return $result[0]['guid'];
        }else{
            return '';
        }
    }
	
	function get_blog_Category($id){
		/*$query = $this->db->query('select t.name from stylecra_stylecracker_v1_blogbeta.wp_terms t, stylecra_stylecracker_v1_blogbeta.wp_term_taxonomy tt, stylecra_stylecracker_v1_blogbeta.wp_term_relationships tr,stylecra_stylecracker_v1_blogbeta.wp_posts as wp where t.term_id=tt.term_id AND tt.term_taxonomy_id=tr.term_taxonomy_id and tr.object_id='.$id.' ORDER BY `t`.`name` DESC');*/
		$query = $this->db->query("SELECT stylecra_stylecracker_v1_blogbeta.wp_terms.* FROM stylecra_stylecracker_v1_blogbeta.wp_term_relationships 
									INNER JOIN stylecra_stylecracker_v1_blogbeta.wp_term_taxonomy ON stylecra_stylecracker_v1_blogbeta.wp_term_relationships.term_taxonomy_id = stylecra_stylecracker_v1_blogbeta.wp_term_taxonomy.term_taxonomy_id 
									INNER JOIN stylecra_stylecracker_v1_blogbeta.wp_terms ON stylecra_stylecracker_v1_blogbeta.wp_term_relationships.term_taxonomy_id = stylecra_stylecracker_v1_blogbeta.wp_terms.term_id
									WHERE stylecra_stylecracker_v1_blogbeta.wp_term_relationships.object_id = ".$id." AND stylecra_stylecracker_v1_blogbeta.wp_term_taxonomy.taxonomy='category'");
		$result = $query->result_array();
		if(!empty($result)){
			return $result[0]['name'];
		}else return '';
	}
	
	function is_wishlisted($fav_for,$fav_id,$user_id){
		if($user_id>0){
			$query = $this->db->query("select id,user_id from favorites where fav_id=$fav_id AND user_id=$user_id AND fav_for = '$fav_for' ");
			$res = $query->result_array();
			// print_r($res);exit;
			if(count($res)>0){
				return '1';
			}else return '0';
		}else return 0;
	}

    public function getblogmedia($postid)
   {
      /*Curl to get blogmedia data */
        $domain = $this->config->item('blog_url');
        $endpoint = $domain."/wp-json/wp/v2/media/".$postid;
        
        $curl = curl_init($endpoint);

        curl_setopt_array($curl, [
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_URL            => $endpoint,
        ]);
        $response = curl_exec($curl);
        $decoderesponse = json_decode($response, true);       
      /*End Curl to get blogmedia data */
      return $decoderesponse['media_details']['sizes']['medium']['source_url'];
   }

   public function getblogCategory($catid)
   {
   		if($catid!='')
   		{
	   		/*Curl to get blogcategory data */
	        $domain = $this->config->item('blog_url');
	        $endpoint = $domain."/wp-json/wp/v2/categories/".$catid;
	        
	        $curl = curl_init($endpoint);

	        curl_setopt_array($curl, [
	          CURLOPT_RETURNTRANSFER => true,
	          CURLOPT_URL            => $endpoint,
	        ]);
	        $response = curl_exec($curl);
	        $decoderesponse = json_decode($response, true);	        
	      /*End Curl to get blogcategory data */
	      return $decoderesponse['name'];	   		
	    }
   }

   function user_wishlist($user_id,$fav_for,$fav_id){          
        
        $query = $this->db->query("select id,user_id from favorites where fav_id=$fav_id AND user_id=$user_id AND fav_for = '$fav_for' ");
		$res = $query->result_array();
		if(!empty($res)){
			$id = $res[0]['id'];
			$query = $this->db->query(" Delete from favorites where id=$id ");
			return 1;
		}else{ 
			$data = array('user_id'=>$user_id,'fav_id'=>$fav_id,'fav_for'=>$fav_for);
			$result = $this->db->insert('favorites', $data);         
			if(!empty($result)){
				return 1;
			}else{ 
				return 2;
			}
		}
		
    }

    function getUserPA($userid)
    {
    	if($userid!='')
    	{
    		$query = $this->db->query('SELECT a.`question_id`,b.`answer`,c.`gender` FROM `users_answer` AS a, `answers` AS b,user_info as c WHERE 1 AND b.`id` = a.`answer_id` and a.`user_id`= c.`user_id` and a.`user_id`= '.$userid);
			$res = $query->result_array();
			return $res;
    	}    	
    }

    function get_userpa_answer($user_id,$question_id){		
		$query = $this->db->query("select ua.answer_id,a.answer from users_answer as ua,answers as a where ua.answer_id=a.id AND ua.user_id = $user_id AND ua.question_id = $question_id ");
		//echo $this->db->last_query();exit;
		$res = $query->result_array();
		if(!empty($res)){
			return $res[0];
		}else return null;
	}

	
	
 }