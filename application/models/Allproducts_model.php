<?php
class Allproducts_model extends MY_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
    }

    function get_gender_list($param=NULL){
        $where_cond = '';
        if($param==NULL){
            $where_cond = ' and id IN('.WOMEN_CATEGORY_ID.','.MEN_CATEGORY_ID.')';
        }

        $query = $this->db->query('SELECT id,slug,name FROM `product_category` WHERE status = 1 and is_delete = 0 '.$where_cond);
        $res = $query->result_array();
        return $res;
    }

    /* Product Count */
    function get_product_count($type=NULL,$select_value=NULL){
        if($type == 'gender' && $select_value>0){
            $query = $this->db->query('SELECT count(id) as cnt FROM product_desc WHERE approve_reject IN ("A","D") and is_delete = 0 and `product_cat_id`='.$select_value);
            $res = $query->result_array();
        }else
        if($type == 'sub_cat' && $select_value>0){
            $query = $this->db->query('SELECT count(id) as cnt FROM product_desc WHERE approve_reject IN ("A","D") and is_delete = 0 and`product_sub_cat_id`='.$select_value);
            $res = $query->result_array();
        }else
        if($type == 'variation' && $select_value>0){
            $query = $this->db->query('SELECT count(distinct a.product_id) as cnt FROM product_variations as a, product_desc as b WHERE a.`product_id` = b.`id` and b.`approve_reject` IN ("A","D") and b.`is_delete` = 0 and a.`product_var_type_id`='.$select_value);
            $res = $query->result_array();
        }else
        if($type == 'variations' && $select_value>0){
            $query = $this->db->query('SELECT count(distinct a.product_id) as cnt FROM product_variations as a, product_desc as b WHERE a.`product_id` = b.`id` and b.`approve_reject` IN ("A","D") and b.`is_delete` = 0 and a.`product_var_val_id`='.$select_value);
            $res = $query->result_array();
        }


        if(!empty(@$res)){
            return $res[0]['cnt'];
        }
    }

    function get_sub_category_list($param=NULL,$sub_cat_slug=NULL){
        $where_cond = '';
        $where_cond = $where_cond.' and product_cat_id IN('.WOMEN_CATEGORY_ID.','.MEN_CATEGORY_ID.')';
        if($param!='' && $param!='all' && $param > 0){
            $where_cond = $where_cond.' and product_cat_id ='.$param;
        }
        if($sub_cat_slug!=''){
            $where_cond = $where_cond.' and slug ='.$this->db->escape($sub_cat_slug).'';
        }

        $query = $this->db->query('SELECT id,slug,name,product_cat_id FROM `product_sub_category` WHERE status = 1 and is_delete = 0 '.$where_cond);
        $res = $query->result_array();

        return $res;
    }

    function get_variation_list($param=NULL,$slug=NULL,$sub_cat_id=NULL,$var_id=NULL){
        $where_cond = '';
        if($var_id!=''){
            $where_cond = $where_cond.' and id ='.$var_id;
        }
        if($slug!=''){
            $where_cond = $where_cond.' and slug ='.$this->db->escape($slug).'';

        }if($sub_cat_id!=''){
            $where_cond = $where_cond.' and prod_sub_cat_id ="'.$sub_cat_id.'"';
        }

        $query = $this->db->query('SELECT id,slug,name FROM `product_variation_type` WHERE status = 1 and is_delete = 0 and show_in_filter = 1 '.$where_cond);
        $res = $query->result_array();

        return $res;
    }

    function get_subCat_by_variation($varId){
        if($varId>0){
            $query = $this->db->query('SELECT b.id,b.name,b.slug FROM `product_variation_type` as a,product_sub_category as b WHERE a.status = 1 and a.is_delete = 0 and a.prod_sub_cat_id=b.id and a.id="'.$varId.'"');
            $res = $query->result_array();
            return $res;
        }
    }

    function get_variations_list($varId=NULL,$slug=NULL){
        if($varId>0){
            $query = $this->db->query('SELECT id,name,slug,product_variation_type_id FROM product_variation_value WHERE status = 1 and is_delete = 0 and show_in_filter = 1 and product_variation_type_id="'.$varId.'"');
            $res = $query->result_array();
            
            return $res;
        }else if($slug!=''){
            $query = $this->db->query('SELECT id,name,slug,product_variation_type_id FROM product_variation_value WHERE status = 1 and is_delete = 0  and show_in_filter = 1 and slug="'.$slug.'"');
            $res = $query->result_array();

            
            return $res;
        }

    }

    function get_paid_brands(){
            //$query = $this->db->get_where('brand_info', array('is_paid' => '1'));
            $query = $this->db->query('select * from brand_info where is_paid = 1 order by company_name asc');
            $res = $query->result_array();
            return $res;
    }

    function get_looks_via_cat($look_type,$category_slug,$category_level){
        
        $product_table =''; $where_cond = ''; $look_id = ''; $category_id=0;
        if($category_level!='' && !is_numeric($category_level)){
            if($category_level == 'cat'){ $cat_table_name = 'product_category'; $product_table='product_desc'; }
            else if($category_level == 'subcategory'){ $cat_table_name = 'product_sub_category'; $product_table='product_desc'; }
            else if($category_level == 'variation'){ $cat_table_name = 'product_variation_type'; $product_table='product_variations'; }
            else if($category_level == 'variations'){ $cat_table_name = 'product_variation_value'; $product_table='product_variations'; }
            if($category_slug!='' && $cat_table_name!=''){
                $query = $this->db->get_where($cat_table_name, array('status' => '1','slug'=>$category_slug));
                $res = $query->result_array();
                $category_id = $res[0]['id'];
            }

            if($category_id > 0 && $product_table != ''){
                if($look_type == 'men') { $where_cond = $where_cond.' and a.looks_for=2';   }
                else if($look_type == 'women') { $where_cond = $where_cond.' and a.looks_for=1';   }
                if($product_table == 'product_desc'){
                    if($category_level == 'cat'){ 
                        $where_cond = $where_cond.' and c.product_cat_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and c.product_sub_cat_id='.$category_id; 
                    }
                    $query = $this->db->query('select distinct a.id as id from look as a,look_products as b,product_desc as c where a.id = b.look_id and b.product_id = c.id and a.status = 1 '.$where_cond.' order by a.modified_datetime desc limit 0,3');
                    $lookIds = $query->result_array();
                    if(!empty($lookIds)){ $i = 0;
                        foreach($lookIds as $val){
                            if($i!=0){ $look_id = $look_id.','; }
                            $look_id = $look_id.$val['id'];
                            $i++;
                        }
                    }
                }else if($product_table == 'product_variations'){
                    if($category_level == 'variation'){ 
                        $where_cond = $where_cond.' and c.product_var_type_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and c.product_var_val_id='.$category_id; 
                    }
                    $query = $this->db->query('select distinct a.id as id from look as a,look_products as b,product_variations as c where a.id = b.look_id and b.product_id = c.product_id and a.status = 1 '.$where_cond.' order by a.modified_datetime desc limit 0,3');
                    $lookIds = $query->result_array();
                    if(!empty($lookIds)){ $i = 0;
                        foreach($lookIds as $val){
                            if($i!=0){ $look_id = $look_id.','; }
                            $look_id = $look_id.$val['id'];
                            $i++;
                        }
                    }
                }
            }

            if($look_id!=''){
                $query = $this->db->query('select z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.description,z.status,z.deflag,z.slug,z.looks_for from (SELECT a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.description,a.status,a.deflag,a.slug,a.looks_for FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 OR z.look_type = 6) WHERE 1 and z.status=1 and z.id IN('.$look_id.') order by z.modified_datetime desc');
                $look_data = $query->result_array();
                return $look_data;
            }
        }else if($look_type!=''){
            $query = $this->db->query('select z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.description,z.status,z.deflag,z.slug,z.looks_for from (SELECT a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.description,a.status,a.deflag,a.slug,a.looks_for FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 OR z.look_type = 6) WHERE 1 and z.status=1 and z.looks_for = '.$look_type.' order by z.modified_datetime desc limit 0,3');
                $look_data = $query->result_array();
                return $look_data;
        }else{
            $query = $this->db->query('select z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.description,z.status,z.deflag,z.slug,z.looks_for from (SELECT a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.description,a.status,a.deflag,a.slug,a.looks_for FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 OR z.look_type = 6) WHERE 1 and z.status=1 order by z.modified_datetime desc limit 0,3');
                $look_data = $query->result_array();
                return $look_data;
        }
    }

    function get_all_products($offset,$product_cat=NULL,$category_slug=NULL,$category_level=NULL,$brandIds=NULL,$products=NULL){
        $offset = ($offset-1)*12; if($offset < 0) { $offset = 0; }
        $where_cond=''; $cat_table_name=''; $category_id='';

            if($category_level == 'cat'){ $cat_table_name = 'product_category'; $product_table='product_desc'; }
            else if($category_level == 'subcategory'){ $cat_table_name = 'product_sub_category'; $product_table='product_desc'; }
            else if($category_level == 'variation'){ $cat_table_name = 'product_variation_type'; $product_table='product_variations'; }
            else if($category_level == 'variations'){ $cat_table_name = 'product_variation_value'; $product_table='product_variations'; }

            if($category_slug!=''){
                $query = $this->db->get_where($cat_table_name, array('status' => '1','slug'=>$category_slug));
                $res = $query->result_array();
                $category_id = $res[0]['id'];
            }

        if($category_level!='variation' && $category_level != 'variations'){
            /* Prodcut Category condition - Level 1 main category currently consider only men and women*/
            if($product_cat!=NULL){
                $where_cond = $where_cond.' and product_cat_id = '.$product_cat;
            }

            if($category_id!='' && $category_level == 'subcategory'){
                $where_cond = $where_cond.' and product_sub_cat_id = '.$category_id;
            }

            if($brandIds!=NULL){
                $where_cond = $where_cond.' and brand_id IN ('.$brandIds.') ';
            }

            if($products!=NULL){
                $where_cond = $where_cond.' and id IN ('.$products.') ';
            }

            $query = $this->db->query('select id,name,price,image,slug from product_desc WHERE  approve_reject = "A" and is_delete = 0 and status = 1 '.$where_cond.'  limit '.$offset.',12');//order by (id desc)
        }else{

            /* Prodcut Category condition - Level 1 main category currently consider only men and women*/
            if($product_cat!=NULL){
                $where_cond = $where_cond.' and product_cat_id = '.$product_cat;
            }

            if($category_level == 'variation'){ 
                $where_cond = $where_cond.' and b.product_var_type_id='.$category_id; 
            }else{
                $where_cond = $where_cond.' and b.product_var_val_id='.$category_id; 
            }

            if($brandIds!=NULL){
                $where_cond = $where_cond.' and a.brand_id IN ('.$brandIds.') ';
            }

            if($products!=NULL){
                $where_cond = $where_cond.' and a.id IN ('.$products.') ';
            }

            $query = $this->db->query('select a.id,a.name,a.price,a.image,a.slug from product_desc as a,product_variations as b WHERE a.approve_reject = "A" and a.status = 1 and a.is_delete = 0 and a.id = b.product_id '.$where_cond.' order by a.id desc limit '.$offset.',12');
        }
        $product_data = $query->result_array();
        return $product_data;
    }

    function total_get_all_products($product_cat=NULL,$category_slug=NULL,$category_level=NULL,$brandIds=NULL,$products=NULL){
        $where_cond=''; $cat_table_name=''; $category_id='';

            if($category_level == 'cat'){ $cat_table_name = 'product_category'; $product_table='product_desc'; }
            else if($category_level == 'subcategory'){ $cat_table_name = 'product_sub_category'; $product_table='product_desc'; }
            else if($category_level == 'variation'){ $cat_table_name = 'product_variation_type'; $product_table='product_variations'; }
            else if($category_level == 'variations'){ $cat_table_name = 'product_variation_value'; $product_table='product_variations'; }

            if($category_slug!=''){
                $query = $this->db->get_where($cat_table_name, array('status' => '1','slug'=>$category_slug));
                $res = $query->result_array();
                $category_id = $res[0]['id'];
            }

        if($category_level!='variation' && $category_level != 'variations'){
            /* Prodcut Category condition - Level 1 main category currently consider only men and women*/
            if($product_cat!=NULL){
                $where_cond = $where_cond.' and product_cat_id = '.$product_cat;
            }

            if($category_id!='' && $category_level == 'subcategory'){
                $where_cond = $where_cond.' and product_sub_cat_id = '.$category_id;
            }

            if($brandIds!=NULL){
                $where_cond = $where_cond.' and brand_id IN ('.$brandIds.') ';
            }

            if($products!=NULL){
                $where_cond = $where_cond.' and id IN ('.$products.') ';
            }

            $query = $this->db->query('select count(id) as cnt from product_desc WHERE  approve_reject = "A" and status=1 and is_delete = 0 '.$where_cond.' order by id desc');
        }else{

            /* Prodcut Category condition - Level 1 main category currently consider only men and women*/
            if($product_cat!=NULL){
                $where_cond = $where_cond.' and product_cat_id = '.$product_cat;
            }

            if($category_level == 'variation'){ 
                $where_cond = $where_cond.' and b.product_var_type_id='.$category_id; 
            }else{
                $where_cond = $where_cond.' and b.product_var_val_id='.$category_id; 
            }

            if($brandIds!=NULL){
                $where_cond = $where_cond.' and a.brand_id IN ('.$brandIds.') ';
            }

            if($products!=NULL){
                $where_cond = $where_cond.' and a.id IN ('.$products.') ';
            }

            $query = $this->db->query('select count(a.id) as cnt from product_desc as a,product_variations as b WHERE a.approve_reject = "A" and a.is_delete = 0 and a.status=1 and a.id = b.product_id '.$where_cond.' order by a.id desc');
        }
        $product_data = $query->result_array();
        if(!empty($product_data)){
            return $product_data[0]['cnt'];
        }else{
            return 0;
        }   
    }


     function filter_products($offset=NULL,$brandIds=NULL,$colorId=NULL,$sc_offers=NULL,$price=NULL,$min_price=NULL,$max_price=NULL,$discount=NULL,$look_type=NULL,$category_slug=NULL,$category_level=NULL,$product_sort_by=NULL,$products=NULL){
       
        $offset = ($offset-1)*12;
        if($offset<0) { $offset = 0; } 
        $product_data = array(); $where_cond = ''; $order_by = '';

        if(($category_slug==NULL && $category_level==NULL) || ($category_slug=='subcategory' && $category_level!=NULL)){

            if($discount == NULL){
                /* Main category selection from URL or From Filter selection*/
                if($look_type == 'women'){ $where_cond = $where_cond.' and product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and product_cat_id='.MEN_CATEGORY_ID; } 

                /* Sub-category selection */
                if($this->get_cat_id($category_slug,$category_level) > 0 && $category_slug == 'subcategory'){ $where_cond = $where_cond.' and product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                /* Brands Selection*/
                if($brandIds!=''){
                    $where_cond = $where_cond.' and brand_id IN('.$brandIds.')';
                }

                /* Color Selection */
                if($colorId!=''){
                    $where_cond = $where_cond.' and sc_product_color IN('.$colorId.')';
                }

                if($products!=NULL){
                $where_cond = $where_cond.' and id IN ('.$products.') ';
                }

                /* Price Selection */
                if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                    if($price == 1) { $where_cond = $where_cond.' and price < 500 ';  }
                        else if($price == 2) { $where_cond = $where_cond.' and (price >= 500 ) and (price <= 999 )';  }
                        else if($price == 3) { $where_cond = $where_cond.' and (price >= 1000 ) and (price <= 1999 ) ';  }
                        else if($price == 4) { $where_cond = $where_cond.' and (price >= 2000 ) and (price <= 2999 ) ';  }
                        else if($price == 5) { $where_cond = $where_cond.' and price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                       $where_cond = $where_cond.' and (price >= '.$min_price.' ) and (price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and price >= '.$max_price;
                    }
                }

                /* Offers Selection*/
                if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                    $where_cond = $where_cond.' and id IN ('.$this->get_offer_products($sc_offers).')'; 
                }

                /* Order by*/
                $inventory_query="";
                $sql_query = 'select distinct id from product_desc WHERE 
                     (approve_reject = "A" || approve_reject = "D") and is_delete = 0 and status=1 and price > 0 '.$where_cond.' ';//limit '.$offset.',50000
                $result = $this->db->query($sql_query);
                $sql_query = $result->result_array();
                /*print_r($sql_query);
                exit;*/

                $pro_id="";
                foreach($sql_query as $pro){
                    $pro_id .= $pro['id'].", ";
                }
                $pro_id = substr($pro_id,0,-2);
                
                if($pro_id){
                    $sql_query = "select product_id id from product_inventory where stock_count > 0 and product_id in($pro_id) group by product_id";
                    
                    $result = $this->db->query($sql_query);
                    $sql_query = $result->result_array();
                    /*print_r($sql_query);
                    exit;*/
                    
                    $pro_id="";
                    foreach($sql_query as $pro){
                        $pro_id .= $pro['id'].", ";
                    }
                    $pro_id = substr($pro_id,0,-2);
                    if($pro_id)
                        $inventory_query = "id in ($pro_id) and";
                }
                
                
                //if($_GET['test'] == 1)
                $order_by = ' order by id desc';
                if($product_sort_by > 0){
                    if($products!=''){
                        if($product_sort_by == 1) { $order_by = ' order by FIELD(id, '.$products.'), price desc'; }else{ $order_by = ' order by  FIELD(id, '.$products.'), price asc'; }
                    }else{
                        if($product_sort_by == 1) { $order_by = ' order by price desc'; }else{ $order_by = ' order by price asc'; }
                    }
                }else if($products){
                    if($products!=''){
                        $order_by = ' order by  FIELD(id, '.$products.')';

                        $inventory_query = "id in (select product_id from product_inventory where stock_count > 0 and product_id in($products) group by product_id) and";
                    }else{
                        $order_by = ' order by id desc';
                    }
                }

                
                $query = $this->db->query('select distinct id,name,price,image,slug,brand_id,product_cat_id from product_desc WHERE '.$inventory_query.'
                     (approve_reject = "A" || approve_reject = "D") and is_delete = 0 and status=1 and price > 0 '.$where_cond.$order_by.' limit '.$offset.',12');

            }else { 

                if($discount != NULL){


                        /* Main category selection from URL or From Filter selection*/
                    if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                    /* Sub-category selection */
                    if($this->get_cat_id($category_slug,$category_level) > 0 && $category_slug == 'subcategory'){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                    /* Brands Selection*/
                    if($brandIds!=''){
                        $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                    }

                    /* Color Selection */
                    if($colorId!=''){
                        $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                    }

                    if($products!=NULL){
                        $where_cond = $where_cond.' and a.id IN ('.$products.') ';
                    }

                    /* Price Selection */
                    if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                        if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                            else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                            else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                            else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                            else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                        if($min_price > 0 && $max_price>0){
                           $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                        }else if($min_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$min_price;
                        }else if($max_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$max_price;
                        }
                    }

                    /* Offers Selection*/
                    if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                        $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                    }

                    /* Order by*/
                    $inventory_query="";
                    
                    /*print_r($sql_query);
                    exit;*/

                    if($discount == 1){
                        $sql_query = 'select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id,a.product_cat_id from product_desc as a,discount_availed as b WHERE 
                     CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id = b.product_id and (a.approve_reject = "A" || a.approve_reject = "D") and a.price > 0 and a.is_delete = 0 and a.status=1 '.$where_cond.' ';//limit '.$offset.',50000
                        $result = $this->db->query($sql_query);
                        $sql_query = $result->result_array();
                    }else if($discount == 2){
                         $sql_query = 'select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id,a.product_cat_id from product_desc as a,discount_availed as b WHERE CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id != b.product_id and (a.approve_reject = "A" || a.approve_reject = "D") and a.price > 0 and a.is_delete = 0 and a.status=1 '.$where_cond.' ';//limit '.$offset.',50000
                        $result = $this->db->query($sql_query);
                        $sql_query = $result->result_array();
                    }

                    $pro_id="";
                    foreach($sql_query as $pro){
                        $pro_id .= $pro['id'].", ";
                    }
                    $pro_id = substr($pro_id,0,-2);
                    
                    if($pro_id){
                        $sql_query = "select product_id id from product_inventory where stock_count > 0 and product_id in($pro_id) group by product_id";
                        
                        $result = $this->db->query($sql_query);
                        $sql_query = $result->result_array();
                        /*print_r($sql_query);
                        exit;*/
                        
                        $pro_id="";
                        foreach($sql_query as $pro){
                            $pro_id .= $pro['id'].", ";
                        }

                        $pro_id = substr($pro_id,0,-2);
                        if($pro_id)
                        $inventory_query = "a.id in ($pro_id) and";
                    }
                    
                           $order_by = ' order by a.id desc';  
                          if($product_sort_by > 0){
                            if($products!=''){
                                if($product_sort_by == 1) { $order_by = ' order by FIELD(a.id, '.$products.'), a.price desc'; }else{ $order_by = ' order by  FIELD(a.id, '.$products.'), a.price asc'; }
                                }else{
                                    if($product_sort_by == 1) { $order_by = ' order by a.price desc'; }else{ $order_by = ' order by a.price asc'; }
                             }
                            }else if($products){
                                if($products!=''){
                                    $order_by = ' order by  FIELD(a.id, '.$products.')';
                                    $inventory_query = "a.id in (select product_id from product_inventory where stock_count > 0 and product_id in($products) group by product_id) and";
                                }else{
                                    $order_by = ' order by a.id desc';
                                }
                            }

                    if($discount == 1){
                    $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id,a.product_cat_id from product_desc as a,discount_availed as b WHERE '.$inventory_query.'  
                     CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id = b.product_id and (a.approve_reject = "A" || a.approve_reject = "D") and a.price > 0 and a.is_delete = 0 and a.status=1 '.$where_cond.$order_by.' limit '.$offset.',12');
                    }else if($discount == 2){


                        $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id,a.product_cat_id from product_desc as a,discount_availed as b WHERE '.$inventory_query .' CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id != b.product_id and (a.approve_reject = "A" || a.approve_reject = "D") and a.price > 0 and a.is_delete = 0 and a.status=1 '.$where_cond.$order_by.' limit '.$offset.',12');
                    }
                }
            }
            $product_data = $query->result_array();

        }else{
            if($this->get_cat_id($category_slug,$category_level) >0){
                if($category_slug == 'variation'){
                    $where_cond = $where_cond.' and b.product_var_type_id='.$this->get_cat_id($category_slug,$category_level); 
                }else if($category_slug == 'variations'){
                    $where_cond = $where_cond.' and b.product_var_val_id='.$this->get_cat_id($category_slug,$category_level); 
                }
            }
            /* Else condition started */
                if($discount == NULL){
                /* Main category selection from URL or From Filter selection*/
                if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                /* Sub-category selection */ 
                if($this->get_cat_id($category_slug,$category_level) > 0 && $category_slug == 'subcategory'){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                /* Brands Selection*/
                if($brandIds!=''){
                    $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                }

                /* Color Selection */
                if($colorId!=''){
                    $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                }

                if($products!=NULL){
                    $where_cond = $where_cond.' and a.id IN ('.$products.') ';
                }

                /* Price Selection */
                if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                    if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                        else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                        else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                        else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                        else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                       $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$max_price;
                    }
                }

                /* Offers Selection*/
                if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                    $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                }



    
                   /* Order by*/
                   $inventory_query="";
                    
                    /*print_r($sql_query);
                    exit;*/

                    $sql_query = 'select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id,a.product_cat_id from product_desc as a,product_variations as b WHERE  (a.approve_reject = "A" || a.approve_reject = "D") and a.price > 0 and a.is_delete = 0 and a.status=1 and a.id = b.product_id '.$where_cond.$order_by.' ';//limit '.$offset.',50000
                    $result = $this->db->query($sql_query);
                    $sql_query = $result->result_array();
                    
                    $pro_id="";
                    foreach($sql_query as $pro){
                        $pro_id .= $pro['id'].", ";
                    }
                    $pro_id = substr($pro_id,0,-2);
                    
                    if($pro_id){
                        $sql_query = "select product_id id from product_inventory where stock_count > 0 and product_id in($pro_id) group by product_id";
                        
                        $result = $this->db->query($sql_query);
                        $sql_query = $result->result_array();
                        /*print_r($sql_query);
                        exit;*/
                        
                        $pro_id="";
                        foreach($sql_query as $pro){
                            $pro_id .= $pro['id'].", ";
                        }
                        $pro_id = substr($pro_id,0,-2);
                        if($pro_id)
                            $inventory_query = "a.id in ($pro_id) and";
                    }
                    $order_by = ' order by id desc';
                if($product_sort_by > 0){
                    if($products!=''){
                        if($product_sort_by == 1) { $order_by = ' order by FIELD(id, '.$products.'), price desc'; }else{ $order_by = ' order by  FIELD(id, '.$products.'), price asc'; }
                    }else{
                        if($product_sort_by == 1) { $order_by = ' order by price desc'; }else{ $order_by = ' order by price asc'; }
                    }
                }else if($products){
                    if($products!=''){
                        $order_by = ' order by  FIELD(id, '.$products.')';
                        $inventory_query = "a.id in (select product_id from product_inventory where stock_count > 0 and product_id in($products) group by product_id) and";
                    }else{
                        $order_by = ' order by id desc';
                    }
                }

                $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id,a.product_cat_id from product_desc as a,product_variations as b WHERE '.$inventory_query.'  (a.approve_reject = "A" || a.approve_reject = "D") and a.price > 0 and a.is_delete = 0 and a.status=1 and a.id = b.product_id '.$where_cond.$order_by.' limit '.$offset.',12');

            }else { 

                if($discount > 0){


                        /* Main category selection from URL or From Filter selection*/
                    if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                    /* Sub-category selection */
                    if($this->get_cat_id($category_slug,$category_level) > 0 && $category_slug == 'subcategory'){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                    /* Brands Selection*/
                    if($brandIds!=''){
                        $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                    }

                    /* Color Selection */
                    if($colorId!=''){
                        $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                    }

                    if($products!=NULL){
                        $where_cond = $where_cond.' and a.id IN ('.$products.') ';
                    }

                    /* Price Selection */
                    if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                        if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                            else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                            else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                            else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                            else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                        if($min_price > 0 && $max_price>0){
                           $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                        }else if($min_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$min_price;
                        }else if($max_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$max_price;
                        }
                    }

                    /* Offers Selection*/
                    if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                        $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                    }

                      /* Order by*/

                    if($discount == 1){
                       $sql_query = 'select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id,a.product_cat_id from product_desc as a,product_variations as b,`discount_availed` as c WHERE CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id = c.product_id and (a.approve_reject = "A" || a.approve_reject = "D") and a.price > 0 and a.is_delete = 0 and a.status=1 and a.id = b.product_id '.$where_cond.' ';
                    }else if($discount == 2){
                       $sql_query = 'select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id,a.product_cat_id from product_desc as a,product_variations as b,`discount_availed` as c WHERE CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id != c.product_id and a.price > 0 and (a.approve_reject = "A" || a.approve_reject = "D") and a.status=1 and a.is_delete = 0 and a.id = b.product_id '.$where_cond.' ';
                    }

                    $result = $this->db->query($sql_query);
                    $sql_query = $result->result_array();
                    
                    $pro_id="";
                    foreach($sql_query as $pro){
                        $pro_id .= $pro['id'].", ";
                    }
                    $pro_id = substr($pro_id,0,-2);
                    
                    if($pro_id){
                        $sql_query = "select product_id id from product_inventory where stock_count > 0 and product_id in($pro_id) group by product_id";
                        
                        $result = $this->db->query($sql_query);
                        $sql_query = $result->result_array();
                        /*print_r($sql_query);
                        exit;*/
                        
                        $pro_id="";
                        foreach($sql_query as $pro){
                            $pro_id .= $pro['id'].", ";
                        }
                        if($pro_id)
                            $pro_id = substr($pro_id,0,-2);
                        $inventory_query = "a.id in ($pro_id) and";
                    }
                    $order_by = ' order by a.id desc';

                   if($product_sort_by > 0){
                            if($products!=''){
                                if($product_sort_by == 1) { $order_by = ' order by FIELD(a.id, '.$products.'), a.price desc'; }else{ $order_by = ' order by  FIELD(a.id, '.$products.'), a.price asc'; }
                                }else{
                                    if($product_sort_by == 1) { $order_by = ' order by a.price desc'; }else{ $order_by = ' order by a.price asc'; }
                             }
                            }else if($products){
                                if($products!=''){
                                    $order_by = ' order by  FIELD(a.id, '.$products.')';
                                    $inventory_query = "a.id in (select product_id from product_inventory where stock_count > 0 and product_id in($products) group by product_id) and";
                                }else{
                                    $order_by = ' order by a.id desc';
                                }
                    }

                    if($discount == 1){
                     $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id,a.product_cat_id from product_desc as a,product_variations as b,`discount_availed` as c WHERE '.$inventory_query.' CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id = c.product_id and (a.approve_reject = "A" || a.approve_reject = "D") and a.price > 0 and a.is_delete = 0 and a.status=1 and a.id = b.product_id '.$where_cond.$order_by.' limit '.$offset.',12');
                    }else if($discount == 2){
                        $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id,a.product_cat_id from product_desc as a,product_variations as b,`discount_availed` as c WHERE '.$inventory_query.' CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id != c.product_id and a.price > 0 and (a.approve_reject = "A" || a.approve_reject = "D") and a.status=1 and a.is_delete = 0 and a.id = b.product_id '.$where_cond.$order_by.' limit '.$offset.',12');
                    }
                }
            }
            $product_data = $query->result_array();

        }

        //echo $this->db->last_query();  
        return $product_data;


    }

        function filter_products_total_($offset=NULL,$brandIds=NULL,$colorId=NULL,$sc_offers=NULL,$price=NULL,$min_price=NULL,$max_price=NULL,$discount=NULL,$look_type=NULL,$category_slug=NULL,$category_level=NULL,$product_sort_by=NULL,$products=NULL){
        $product_data = array(); $where_cond = '';

        if(($category_slug==NULL && $category_level==NULL) || ($category_slug=='subcategory' && $category_level!=NULL)){

            if($discount == NULL){
                /* Main category selection from URL or From Filter selection*/
                if($look_type == 'women'){ $where_cond = $where_cond.' and product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and product_cat_id='.MEN_CATEGORY_ID; } 

                /* Sub-category selection */
                if($this->get_cat_id($category_slug,$category_level) > 0 && $category_slug == 'subcategory'){ $where_cond = $where_cond.' and product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                /* Brands Selection*/
                if($brandIds!=''){
                    $where_cond = $where_cond.' and brand_id IN('.$brandIds.')';
                }

                /* Color Selection */
                if($colorId!=''){
                    $where_cond = $where_cond.' and sc_product_color IN('.$colorId.')';
                }

                if($products!=NULL){
                    $where_cond = $where_cond.' and id IN ('.$products.') ';
                }

                /* Price Selection */
                if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                    if($price == 1) { $where_cond = $where_cond.' and price < 500 ';  }
                        else if($price == 2) { $where_cond = $where_cond.' and (price >= 500 ) and (price <= 999 )';  }
                        else if($price == 3) { $where_cond = $where_cond.' and (price >= 1000 ) and (price <= 1999 ) ';  }
                        else if($price == 4) { $where_cond = $where_cond.' and (price >= 2000 ) and (price <= 2999 ) ';  }
                        else if($price == 5) { $where_cond = $where_cond.' and price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                       $where_cond = $where_cond.' and (price >= '.$min_price.' ) and (price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and price >= '.$max_price;
                    }
                }

                /* Offers Selection*/
                if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                    $where_cond = $where_cond.' and id IN ('.$this->get_offer_products($sc_offers).')'; 
                }

                $query = $this->db->query('select count(distinct id) as cnt from product_desc WHERE  approve_reject = "A" and status=1 and is_delete = 0 '.$where_cond.' order by id desc');
            }else { 

                if($discount != NULL){


                        /* Main category selection from URL or From Filter selection*/
                    if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                    /* Sub-category selection */
                    if($this->get_cat_id($category_slug,$category_level) > 0 && $category_slug == 'subcategory'){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                    /* Brands Selection*/
                    if($brandIds!=''){
                        $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                    }

                    /* Color Selection */
                    if($colorId!=''){
                        $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                    }

                    if($products!=NULL){
                        $where_cond = $where_cond.' and a.id IN ('.$products.') ';
                    }

                    /* Price Selection */
                    if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                        if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                            else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                            else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                            else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                            else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                        if($min_price > 0 && $max_price>0){
                           $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                        }else if($min_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$min_price;
                        }else if($max_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$max_price;
                        }
                    }

                    /* Offers Selection*/
                    if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                        $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                    }
                    if($discount == 1){
                    $query = $this->db->query('select count(distinct a.id) as cnt from product_desc as a,discount_availed as b WHERE CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id = b.product_id and a.approve_reject = "A" and a.status=1 and a.is_delete = 0 '.$where_cond.' order by a.id desc');
                    }else if($discount == 2){


                        $query = $this->db->query('select count(distinct a.id) as cnt from product_desc as a,discount_availed as b WHERE CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id != b.product_id and a.approve_reject = "A" and a.status=1 and a.is_delete = 0 '.$where_cond.' order by a.id desc ');
                    }
                }
            }
            $product_data = $query->result_array();

        }else{
            if($this->get_cat_id($category_slug,$category_level) >0){
                if($category_slug == 'variation'){
                    $where_cond = $where_cond.' and b.product_var_type_id='.$this->get_cat_id($category_slug,$category_level); 
                }else if($category_slug == 'variations'){
                    $where_cond = $where_cond.' and b.product_var_val_id='.$this->get_cat_id($category_slug,$category_level); 
                }
            }
            /* Else condition started */
                if($discount == NULL){
                /* Main category selection from URL or From Filter selection*/
                if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                /* Sub-category selection */
                if($this->get_cat_id($category_slug,$category_level) > 0 && $category_slug == 'subcategory'){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                /* Brands Selection*/
                if($brandIds!=''){
                    $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                }

                /* Color Selection */
                if($colorId!=''){
                    $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                }

                if($products!=NULL){
                    $where_cond = $where_cond.' and a.id IN ('.$products.') ';
                }

                /* Price Selection */
                if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                    if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                        else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                        else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                        else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                        else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                       $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$max_price;
                    }
                }

                /* Offers Selection*/
                if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                    $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                }

                $query = $this->db->query('select count(distinct a.id) as cnt from product_desc as a,product_variations as b WHERE a.approve_reject = "A" and a.status=1 and a.is_delete = 0 and a.id = b.product_id '.$where_cond.' order by a.id desc');
            }else { 

                if($discount > 0){


                        /* Main category selection from URL or From Filter selection*/
                    if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                    /* Sub-category selection */
                    if($this->get_cat_id($category_slug,$category_level) > 0 && $category_slug == 'subcategory'){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                    /* Brands Selection*/
                    if($brandIds!=''){
                        $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                    }

                    /* Color Selection */
                    if($colorId!=''){
                        $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                    }

                    if($products!=NULL){
                        $where_cond = $where_cond.' and a.id IN ('.$products.') ';
                    }

                    /* Price Selection */
                    if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                        if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                            else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                            else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                            else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                            else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                        if($min_price > 0 && $max_price>0){
                           $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                        }else if($min_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$min_price;
                        }else if($max_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$max_price;
                        }
                    }

                    /* Offers Selection*/
                    if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                        $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                    }
                    if($discount == 1){
                     $query = $this->db->query('select count(distinct a.id) as cnt from product_desc as a,product_variations as b,`discount_availed` as c WHERE CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id = c.product_id and a.approve_reject = "A" and a.status=1 and a.is_delete = 0 and a.id = b.product_id '.$where_cond.' order by a.id desc');
                    }else if($discount == 2){
                        $query = $this->db->query('select count(distinct a.id) as cnt from product_desc as a,product_variations as b,`discount_availed` as c WHERE CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id != c.product_id and a.approve_reject = "A" and a.status=1 and a.is_delete = 0 and a.id = b.product_id '.$where_cond.' order by a.id desc');
                    }
                }
            }
        }
            $product_data = $query->result_array();
            if(!empty($product_data)){
                return $product_data[0]['cnt'];
            }else{
                return 0;
            }

    }


    function filter_products_total($offset=NULL,$brandIds=NULL,$colorId=NULL,$sc_offers=NULL,$price=NULL,$min_price=NULL,$max_price=NULL,$discount=NULL,$look_type=NULL,$category_slug=NULL,$category_level=NULL,$product_sort_by=NULL,$products=NULL){
       
        $offset = ($offset-1)*12;
        if($offset<0) { $offset = 0; } 
        $product_data = array(); $where_cond = ''; $order_by = '';

        if(($category_slug==NULL && $category_level==NULL) || ($category_slug=='subcategory' && $category_level!=NULL)){

            if($discount == NULL){
                /* Main category selection from URL or From Filter selection*/
                if($look_type == 'women'){ $where_cond = $where_cond.' and product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and product_cat_id='.MEN_CATEGORY_ID; } 

                /* Sub-category selection */
                if($this->get_cat_id($category_slug,$category_level) > 0 && $category_slug == 'subcategory'){ $where_cond = $where_cond.' and product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                /* Brands Selection*/
                if($brandIds!=''){
                    $where_cond = $where_cond.' and brand_id IN('.$brandIds.')';
                }

                /* Color Selection */
                if($colorId!=''){
                    $where_cond = $where_cond.' and sc_product_color IN('.$colorId.')';
                }

                if($products!=NULL){
                $where_cond = $where_cond.' and id IN ('.$products.') ';
                }

                /* Price Selection */
                if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                    if($price == 1) { $where_cond = $where_cond.' and price < 500 ';  }
                        else if($price == 2) { $where_cond = $where_cond.' and (price >= 500 ) and (price <= 999 )';  }
                        else if($price == 3) { $where_cond = $where_cond.' and (price >= 1000 ) and (price <= 1999 ) ';  }
                        else if($price == 4) { $where_cond = $where_cond.' and (price >= 2000 ) and (price <= 2999 ) ';  }
                        else if($price == 5) { $where_cond = $where_cond.' and price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                       $where_cond = $where_cond.' and (price >= '.$min_price.' ) and (price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and price >= '.$max_price;
                    }
                }

                /* Offers Selection*/
                if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                    $where_cond = $where_cond.' and id IN ('.$this->get_offer_products($sc_offers).')'; 
                }

                /* Order by*/
                $inventory_query="";
                $sql_query = 'select distinct id from product_desc WHERE 
                     (approve_reject = "A" || approve_reject = "D") and is_delete = 0 and status=1 and price > 0 '.$where_cond.' ';//limit '.$offset.',50000
                $result = $this->db->query($sql_query);
                $sql_query = $result->result_array();
                /*print_r($sql_query);
                exit;*/

                $pro_id="";
                foreach($sql_query as $pro){
                    $pro_id .= $pro['id'].", ";
                }
                $pro_id = substr($pro_id,0,-2);
                
                if($pro_id){
                    $sql_query = "select product_id id from product_inventory where stock_count > 0 and product_id in($pro_id) group by product_id";
                    
                    $result = $this->db->query($sql_query);
                    $sql_query = $result->result_array();
                    /*print_r($sql_query);
                    exit;*/
                    
                    $pro_id="";
                    foreach($sql_query as $pro){
                        $pro_id .= $pro['id'].", ";
                    }
                    $pro_id = substr($pro_id,0,-2);
                    if($pro_id)
                        $inventory_query = "id in ($pro_id) and";
                }
                
                
                //if($_GET['test'] == 1)
                $order_by = ' order by id desc';
                if($product_sort_by > 0){
                    if($products!=''){
                        if($product_sort_by == 1) { $order_by = ' order by FIELD(id, '.$products.'), price desc'; }else{ $order_by = ' order by  FIELD(id, '.$products.'), price asc'; }
                    }else{
                        if($product_sort_by == 1) { $order_by = ' order by price desc'; }else{ $order_by = ' order by price asc'; }
                    }
                }else if($products){
                    if($products!=''){
                        $order_by = ' order by  FIELD(id, '.$products.')';

                        $inventory_query = "id in (select product_id from product_inventory where stock_count > 0 and product_id in($products) group by product_id) and";
                    }else{
                        $order_by = ' order by id desc';
                    }
                }

                
                $query = $this->db->query('select count(distinct id) as cnt from product_desc WHERE '.$inventory_query.'
                     (approve_reject = "A" || approve_reject = "D") and is_delete = 0 and status=1 and price > 0 '.$where_cond.$order_by.' ');

            }else { 

                if($discount != NULL){


                        /* Main category selection from URL or From Filter selection*/
                    if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                    /* Sub-category selection */
                    if($this->get_cat_id($category_slug,$category_level) > 0 && $category_slug == 'subcategory'){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                    /* Brands Selection*/
                    if($brandIds!=''){
                        $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                    }

                    /* Color Selection */
                    if($colorId!=''){
                        $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                    }

                    if($products!=NULL){
                        $where_cond = $where_cond.' and a.id IN ('.$products.') ';
                    }

                    /* Price Selection */
                    if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                        if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                            else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                            else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                            else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                            else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                        if($min_price > 0 && $max_price>0){
                           $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                        }else if($min_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$min_price;
                        }else if($max_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$max_price;
                        }
                    }

                    /* Offers Selection*/
                    if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                        $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                    }

                    /* Order by*/
                    $inventory_query="";
                    
                    /*print_r($sql_query);
                    exit;*/

                    if($discount == 1){
                        $sql_query = 'select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,discount_availed as b WHERE 
                     CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id = b.product_id and (a.approve_reject = "A" || a.approve_reject = "D") and a.price > 0 and a.is_delete = 0 and a.status=1 '.$where_cond.' ';//limit '.$offset.',50000
                        $result = $this->db->query($sql_query);
                        $sql_query = $result->result_array();
                    }else if($discount == 2){
                         $sql_query = 'select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,discount_availed as b WHERE CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id != b.product_id and (a.approve_reject = "A" || a.approve_reject = "D") and a.price > 0 and a.is_delete = 0 and a.status=1 '.$where_cond.' ';//limit '.$offset.',50000
                        $result = $this->db->query($sql_query);
                        $sql_query = $result->result_array();
                    }

                    $pro_id="";
                    foreach($sql_query as $pro){
                        $pro_id .= $pro['id'].", ";
                    }
                    $pro_id = substr($pro_id,0,-2);
                    
                    if($pro_id){
                        $sql_query = "select product_id id from product_inventory where stock_count > 0 and product_id in($pro_id) group by product_id";
                        
                        $result = $this->db->query($sql_query);
                        $sql_query = $result->result_array();
                        /*print_r($sql_query);
                        exit;*/
                        
                        $pro_id="";
                        foreach($sql_query as $pro){
                            $pro_id .= $pro['id'].", ";
                        }

                        $pro_id = substr($pro_id,0,-2);
                        if($pro_id)
                        $inventory_query = "a.id in ($pro_id) and";
                    }
                    
                        
                          if($product_sort_by > 0){
                            if($products!=''){
                                if($product_sort_by == 1) { $order_by = ' order by FIELD(a.id, '.$products.'), a.price desc'; }else{ $order_by = ' order by  FIELD(a.id, '.$products.'), a.price asc'; }
                                }else{
                                    if($product_sort_by == 1) { $order_by = ' order by a.price desc'; }else{ $order_by = ' order by a.price asc'; }
                             }
                            }else if($products){
                                if($products!=''){
                                    $order_by = ' order by  FIELD(a.id, '.$products.')';
                                    $inventory_query = "a.id in (select product_id from product_inventory where stock_count > 0 and product_id in($products) group by product_id) and";
                                }else{
                                    $order_by = ' order by a.id desc';
                                }
                            }

                    if($discount == 1){
                    $query = $this->db->query('select count(distinct a.id) as cnt from product_desc as a,discount_availed as b WHERE '.$inventory_query.'  
                     CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id = b.product_id and (a.approve_reject = "A" || a.approve_reject = "D") and a.price > 0 and a.is_delete = 0 and a.status=1 '.$where_cond.$order_by.'');
                    }else if($discount == 2){


                        $query = $this->db->query('select count(distinct a.id) as cnt from product_desc as a,discount_availed as b WHERE '.$inventory_query .' CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id != b.product_id and (a.approve_reject = "A" || a.approve_reject = "D") and a.price > 0 and a.is_delete = 0 and a.status=1 '.$where_cond.$order_by.'');
                    }
                }
            }
            $product_data = $query->result_array();

        }else{
            if($this->get_cat_id($category_slug,$category_level) >0){
                if($category_slug == 'variation'){
                    $where_cond = $where_cond.' and b.product_var_type_id='.$this->get_cat_id($category_slug,$category_level); 
                }else if($category_slug == 'variations'){
                    $where_cond = $where_cond.' and b.product_var_val_id='.$this->get_cat_id($category_slug,$category_level); 
                }
            }
            /* Else condition started */
                if($discount == NULL){
                /* Main category selection from URL or From Filter selection*/
                if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                /* Sub-category selection */ 
                if($this->get_cat_id($category_slug,$category_level) > 0 && $category_slug == 'subcategory'){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                /* Brands Selection*/
                if($brandIds!=''){
                    $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                }

                /* Color Selection */
                if($colorId!=''){
                    $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                }

                if($products!=NULL){
                    $where_cond = $where_cond.' and a.id IN ('.$products.') ';
                }

                /* Price Selection */
                if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                    if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                        else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                        else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                        else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                        else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                       $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$max_price;
                    }
                }

                /* Offers Selection*/
                if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                    $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                }



    
                   /* Order by*/
                   $inventory_query="";
                    
                    /*print_r($sql_query);
                    exit;*/

                    $sql_query = 'select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,product_variations as b WHERE (a.approve_reject = "A" || a.approve_reject = "D") and a.price > 0 and a.is_delete = 0 and a.status=1 and a.id = b.product_id '.$where_cond.$order_by.' ';//limit '.$offset.',50000
                    $result = $this->db->query($sql_query);
                    $sql_query = $result->result_array();
                    
                    $pro_id="";
                    foreach($sql_query as $pro){
                        $pro_id .= $pro['id'].", ";
                    }
                    $pro_id = substr($pro_id,0,-2);
                    
                    if($pro_id){
                        $sql_query = "select product_id id from product_inventory where stock_count > 0 and product_id in($pro_id) group by product_id";
                        
                        $result = $this->db->query($sql_query);
                        $sql_query = $result->result_array();
                        /*print_r($sql_query);
                        exit;*/
                        
                        $pro_id="";
                        foreach($sql_query as $pro){
                            $pro_id .= $pro['id'].", ";
                        }
                        $pro_id = substr($pro_id,0,-2);
                        if($pro_id)
                            $inventory_query = "a.id in ($pro_id) and";
                    }

                if($product_sort_by > 0){
                    if($products!=''){
                        if($product_sort_by == 1) { $order_by = ' order by FIELD(id, '.$products.'), price desc'; }else{ $order_by = ' order by  FIELD(id, '.$products.'), price asc'; }
                    }else{
                        if($product_sort_by == 1) { $order_by = ' order by price desc'; }else{ $order_by = ' order by price asc'; }
                    }
                }else if($products){
                    if($products!=''){
                        $order_by = ' order by  FIELD(id, '.$products.')';
                        $inventory_query = "a.id in (select product_id from product_inventory where stock_count > 0 and product_id in($products) group by product_id) and";
                    }else{
                        $order_by = ' order by id desc';
                    }
                }

                $query = $this->db->query('select count(distinct a.id) as cnt from product_desc as a,product_variations as b WHERE '.$inventory_query.'  (a.approve_reject = "A" || a.approve_reject = "D") and a.price > 0 and a.is_delete = 0 and a.status=1 and a.id = b.product_id '.$where_cond.$order_by.'');

            }else { 

                if($discount > 0){


                        /* Main category selection from URL or From Filter selection*/
                    if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                    /* Sub-category selection */
                    if($this->get_cat_id($category_slug,$category_level) > 0 && $category_slug == 'subcategory'){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                    /* Brands Selection*/
                    if($brandIds!=''){
                        $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                    }

                    /* Color Selection */
                    if($colorId!=''){
                        $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                    }

                    if($products!=NULL){
                        $where_cond = $where_cond.' and a.id IN ('.$products.') ';
                    }

                    /* Price Selection */
                    if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                        if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                            else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                            else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                            else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                            else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                        if($min_price > 0 && $max_price>0){
                           $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                        }else if($min_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$min_price;
                        }else if($max_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$max_price;
                        }
                    }

                    /* Offers Selection*/
                    if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                        $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                    }

                      /* Order by*/

                    if($discount == 1){
                       $sql_query = 'select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,product_variations as b,`discount_availed` as c WHERE CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id = c.product_id and (a.approve_reject = "A" || a.approve_reject = "D") and a.price > 0 and a.is_delete = 0 and a.status=1 and a.id = b.product_id '.$where_cond.' ';
                    }else if($discount == 2){
                       $sql_query = 'select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,product_variations as b,`discount_availed` as c WHERE CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id != c.product_id and a.price > 0 and (a.approve_reject = "A" || a.approve_reject = "D") and a.status=1 and a.is_delete = 0 and a.id = b.product_id '.$where_cond.' ';
                    }

                    $result = $this->db->query($sql_query);
                    $sql_query = $result->result_array();
                    
                    $pro_id="";
                    foreach($sql_query as $pro){
                        $pro_id .= $pro['id'].", ";
                    }
                    $pro_id = substr($pro_id,0,-2);
                    
                    if($pro_id){
                        $sql_query = "select product_id id from product_inventory where stock_count > 0 and product_id in($pro_id) group by product_id";
                        
                        $result = $this->db->query($sql_query);
                        $sql_query = $result->result_array();
                        /*print_r($sql_query);
                        exit;*/
                        
                        $pro_id="";
                        foreach($sql_query as $pro){
                            $pro_id .= $pro['id'].", ";
                        }
                        if($pro_id)
                            $pro_id = substr($pro_id,0,-2);
                        $inventory_query = "a.id in ($pro_id) and";
                    }


                   if($product_sort_by > 0){
                            if($products!=''){
                                if($product_sort_by == 1) { $order_by = ' order by FIELD(a.id, '.$products.'), a.price desc'; }else{ $order_by = ' order by  FIELD(a.id, '.$products.'), a.price asc'; }
                                }else{
                                    if($product_sort_by == 1) { $order_by = ' order by a.price desc'; }else{ $order_by = ' order by a.price asc'; }
                             }
                            }else if($products){
                                if($products!=''){
                                    $order_by = ' order by  FIELD(a.id, '.$products.')';
                                    $inventory_query = "a.id in (select product_id from product_inventory where stock_count > 0 and product_id in($products) group by product_id) and";
                                }else{
                                    $order_by = ' order by a.id desc';
                                }
                    }

                    if($discount == 1){
                     $query = $this->db->query('select count(distinct a.id) as cnt from product_desc as a,product_variations as b,`discount_availed` as c WHERE '.$inventory_query.' CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id = c.product_id and (a.approve_reject = "A" || a.approve_reject = "D") and a.price > 0 and a.is_delete = 0 and a.status=1 and a.id = b.product_id '.$where_cond.$order_by.'');
                    }else if($discount == 2){
                        $query = $this->db->query('select count(distinct a.id) as cnt from product_desc as a,product_variations as b,`discount_availed` as c WHERE '.$inventory_query.' CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id != c.product_id and a.price > 0 and (a.approve_reject = "A" || a.approve_reject = "D") and a.status=1 and a.is_delete = 0 and a.id = b.product_id '.$where_cond.$order_by.'');
                    }
                }
            }
            $product_data = $query->result_array();
            


        }
        if(!empty($product_data)){
                return $product_data[0]['cnt'];
            }else{
                return 0;
            }
     }

    function get_offer_products($sc_offers){ 
        if($sc_offers>0){
            $query = $this->db->query('select products,brand_id from `coupon_settings` WHERE id='.$sc_offers);
            $res = $query->result_array();            
            $input =array();
            if($res[0]['products']==0)
            {
                $query_prod = $this->db->query('select id from `product_desc` WHERE `brand_id`='.$res[0]['brand_id'].' AND status=1 AND approve_reject = "A" AND  is_delete=0 ');
                $res_product = $query_prod->result_array();                
                $arr = array_map(function($input){ return $input['id']; }, $res_product);               
                $products = implode(',', $arr);
              
            }else
            {
                 $products =  $res[0]['products'];
            }
            
            if(!empty($res)){

                return $products;
            }
        }
    }

    function get_cat_id($category_level=NULL,$category_slug=NULL){
        $cat_table_name = ''; $product_table='';
          if($category_level == 'cat'){ $cat_table_name = 'product_category'; $product_table='product_desc'; }
            else if($category_level == 'subcategory'){ $cat_table_name = 'product_sub_category'; $product_table='product_desc'; }
            else if($category_level == 'variation'){ $cat_table_name = 'product_variation_type'; $product_table='product_variations'; }
            else if($category_level == 'variations'){ $cat_table_name = 'product_variation_value'; $product_table='product_variations'; }
            if($product_table!=''){
                $query = $this->db->get_where($cat_table_name, array('status' => '1','slug'=>$category_slug));
                $res = $query->result_array();
                return $category_id = $res[0]['id'];
            }else{
                return 0;
            }
    }

    function check_brand($brand_name){
        if(trim($brand_name) !=''){
            $brand_name = $this->db->escape(strtolower(trim($brand_name)));
            $query = $this->db->query('select user_id from brand_info where is_paid = 1 and LOWER(company_name) like "'.$brand_name.'"');
            $res = $query->result_array();
            if(!empty($res)){
                return $res[0]['user_id'];
            }else{
                return '';
            }
        }
    }

    function check_look($look_name){
        $looks_product_array = array(); $products = ''; $look_id='';
        if(trim($look_name) !=''){
            $look_name = $this->db->escape(strtolower(trim($look_name)));
            $query = $this->db->query('select distinct id from look where 1 and status = 1 and deflag = 0 and LOWER(name) like "'.$look_name.'"');
            $res = $query->result_array();
            if(!empty($res)){ $j = 0;
                foreach($res as $val){
                    if($j != 0){ $look_id = $look_id.','; }
                    $look_id = $look_id.$val['id'];
                    $j++;
                }
                $looks_product_array['look'] = $look_id;

                $query = $this->db->query('select distinct product_id from look_products where look_id IN ('.$look_id.')');
                $res = $query->result_array();
                if(!empty($res)){ $p = 0;
                    foreach($res as $val){
                        if($p!=0){  $products =  $products. ','; }
                        $products = $products.$val['product_id'];
                        $p++;
                    }
                    $looks_product_array['products'] = $products;
                }
                return $looks_product_array;
            }else{
                return '';
            }
        }
    }

    function check_product($product_name){

        if((string)strtolower($product_name) == (string)strtolower("Eid Special") || (string)strtolower($product_name) == (string)strtolower("Eid")){
            $sql = "select distinct product_id as id from product_variations where product_var_val_id in 
                        (SELECT id FROM product_variation_value where product_variation_type_id =  361 or product_variation_type_id =  357);";
            $query = $this->db->query($sql);//`name` REGEXP ('$my_start_end_char_search')

                //echo $this->db->last_query();exit;
                $res = $query->result_array();
            if(!empty($res)){ $p = 0;
                foreach($res as $val){
                    if($p!=0){  $products = $products.','; }
                    $products = $products.$val['id'];
                    $p++;
                }
            }    
           
            $looks_product_array['products'] = $products;
            return $looks_product_array;
        }

        $looks_product_array = array(); $products = '';
        $my_search = str_replace(" ", "|", trim($product_name));
        $my_search1 = str_replace(" ", "+", trim($product_name));
        $my_first_char_search = "^".str_replace(" ", "|^", trim($product_name));
        $my_start_end_char_search = "^".str_replace(" ", "$+^", trim($product_name))."$";
        $arr = explode(" ", trim($product_name));
        $search = $this->get_variations_of_search($arr);
        //print_r($search);exit;
        $my_and_search="";
        $my_and_search1="";
         $my_order_by = "1 ";
        if($search['search_parameter']){
            $my_and_search = "and (name REGEXP ' ".str_replace(" ", "' or name REGEXP ' ", trim($search['search_parameter']))."' or name REGEXP '^".str_replace(" ", "' or name REGEXP '^", trim($search['search_parameter']))."')";
            $my_order_by .= " and (name REGEXP ' ".str_replace(" ", "' and name REGEXP ' ", trim($search['search_parameter']))."')";

        }

        if($search['search_variations']){

            $my_order_by .= " and (name REGEXP ' ".str_replace(" ", "' and name REGEXP ' ", trim($search['search_variations']))."')";
            $my_and_search1 = "and (pd.name REGEXP ' ".str_replace(" ", "' or pd.name REGEXP ' ", trim($search['search_variations']))."')";
            $my_and_search_or = "or (pd.name REGEXP ' ".str_replace(" ", "' or pd.name REGEXP ' ", trim($search['search_variations']))."')";
        }
        
        $ii=1;
            if(!strpos(trim($product_name)," ")){
                $query = $this->db->query("select distinct pd.id from product_desc pd
                                        where 1 and LOWER(name) REGEXP LOWER(' $my_search') or LOWER(name) REGEXP LOWER('^$my_search')");
                $res = $query->result_array();
            }else{
                //$search_order = "ORDER BY CASE when name REGEXP '".str_replace(" ", "'  then ".$ii++." WHEN name REGEXP '", trim($product_name))."' then ".$ii++." END";
                //str_replace(" ", "'  then ".$ii++." WHEN name REGEXP '", trim($product_name))
/*                $query = $this->db->query("select distinct pd.id from product_desc pd
                                            right join product_variations pav on pav.product_id = pd.id
                                            where 1 and LOWER(name) REGEXP LOWER('$my_search') and pav.product_var_type_id in 
                                            (select id from product_variation_type where `name` REGEXP ('$my_first_char_search'))");

*/              $query = "select distinct pd.id from product_desc pd right join product_variations pav on pav.product_id = pd.id 
                                            where pd.id > 0 $my_and_search $my_and_search1";

                if($my_and_search1){
                    $query .= " or pav.product_var_type_id in 
                                 (select id from product_variation_type where 1 $my_and_search1) 
                               order by case when ".$my_order_by." then  0 else 2 end, name";//('name', '".str_replace(' ', "','", $product_name)."')";
                }
                /*echo $query;
                exit;*/
                $query = $this->db->query($query);//`name` REGEXP ('$my_start_end_char_search')

                //echo $this->db->last_query();exit;
                $res = $query->result_array();
            }
            if(!empty($res)){ $p = 0;
                foreach($res as $val){
                    if($p!=0){  $products = $products.','; }
                    $products = $products.$val['id'];
                    $p++;
                }
                
            }

        $looks_product_array['products'] = $products;
        return $looks_product_array;
    }


    function get_variations_of_search($arr){
        $data['search_parameter']="";
        $data['search_variations']="";
        foreach($arr as $row){
            $color = array("red","blue","black","brown","green","pink","gray","multicolor");
            $cnt = $this->db->query("select count(id) cnt from product_desc where name REGEXP ' $row' or name REGEXP '^$row'");
            $product_cnt = $cnt->result_array();
            if(!in_array($row, $color) and $product_cnt[0]['cnt'] > 0){
                //echo $row;
                $cnt = $this->db->query("select count(id) cnt from product_variation_type  where name REGEXP '^$row' or name REGEXP ' $row'");
                $cnt = $cnt->result_array();

                if($cnt[0]['cnt'] > 0){
                    $data['search_variations'] .= " ".$row;
                    continue;
                }else{
                    $cnt = $this->db->query("select count(id) cnt from product_variation_value  where name REGEXP ' $row' or  name REGEXP '^$row'");
                    $cnt = $cnt->result_array();
                    if($cnt[0]['cnt'] > 0){
                        $data['search_variations'] .= " ".$row;
                        continue;
                    }
                }
            }
            if($product_cnt[0]['cnt'] > 0){
                 $data['search_parameter'] .= " ".$row;
            }
        }
        return $data;
    }

    public function get_paid_brands_new($product_cat=NULL,$category_slug=NULL,$category_level=NULL,$brandIds=NULL,$products=NULL){
        $where_cond=''; $cat_table_name=''; $category_id='';

            if($category_level == 'cat'){ $cat_table_name = 'product_category'; $product_table='product_desc'; }
            else if($category_level == 'subcategory'){ $cat_table_name = 'product_sub_category'; $product_table='product_desc'; }
            else if($category_level == 'variation'){ $cat_table_name = 'product_variation_type'; $product_table='product_variations'; }
            else if($category_level == 'variations'){ $cat_table_name = 'product_variation_value'; $product_table='product_variations'; }

            if($category_slug!='' && $cat_table_name!=''){
                $query = $this->db->get_where($cat_table_name, array('status' => '1','slug'=>$category_slug));
                $res = $query->result_array();
                $category_id = $res[0]['id'];
            }

        if($category_level!='variation' && $category_level != 'variations'){
            /* Prodcut Category condition - Level 1 main category currently consider only men and women*/
            if($product_cat!=NULL && $product_cat>0){
                $where_cond = $where_cond.' and a.product_cat_id = '.$product_cat;
            }

            if($category_id!='' && $category_level == 'subcategory'){
                $where_cond = $where_cond.' and a.product_sub_cat_id = '.$category_id;
            }

            if($brandIds!=NULL){
                $where_cond = $where_cond.' and a.brand_id IN ('.$brandIds.') ';
            }

            if($products!=NULL){
                $where_cond = $where_cond.' and a.id IN ('.$products.') ';
            }

            $query = $this->db->query('select distinct a.brand_id as user_id,b.company_name from product_desc as a,brand_info as b WHERE (a.approve_reject = "A" || a.approve_reject = "D" ) and a.is_delete = 0 and a.brand_id=b.user_id and b.is_paid = 1 '.$where_cond.' order by b.company_name asc');
        }else{

            /* Prodcut Category condition - Level 1 main category currently consider only men and women*/
            if($product_cat!=NULL && $product_cat>0){
                $where_cond = $where_cond.' and product_cat_id = '.$product_cat;
            }

            if($category_level == 'variation' && $category_id>0){ 
                $where_cond = $where_cond.' and b.product_var_type_id='.$category_id; 
            }elseif($category_id){
                $where_cond = $where_cond.' and b.product_var_val_id='.$category_id; 
            }

            if($brandIds!=NULL){
                $where_cond = $where_cond.' and a.brand_id IN ('.$brandIds.') ';
            }

            if($products!=NULL){
                $where_cond = $where_cond.' and a.id IN ('.$products.') ';
            }

            $query = $this->db->query('select distinct a.brand_id as user_id,c.company_name from product_desc as a,product_variations as b,brand_info as c WHERE (a.approve_reject = "A" || a.approve_reject = "D" ) and a.is_delete = 0 and a.id = b.product_id and a.brand_id=c.user_id and c.is_paid = 1 '.$where_cond.' order by c.company_name asc');
        }
        $product_data = $query->result_array();
        return $product_data;

    }

    function get_brand_id($slug){
        //$slug = $this->db->escape($slug);
        $query = $this->db->query('select a.id as id from user_login as a,brand_info as b where b.is_paid = 1 and a.user_name = '.$this->db->escape($slug).'');
        $product_data = $query->result_array();
        if(!empty($product_data)){
            return $product_data[0]['id'];
        }else{
            return 0;
        }
    }

    function get_brand_name($brand_id){
        $query = $this->db->query('select company_name from brand_info where  user_id = "'.$brand_id.'" ');       
        $brand_data = $query->result_array();
        if(!empty($brand_data)){
            return strtoupper($brand_data[0]['company_name']);
        }else{
            return 0;
        }
    }

    function get_brand_url($brand_id){
        $query = $this->db->query('select user_name from user_login where  id = "'.$brand_id.'" ');       
        $brand_data = $query->result_array();
        if(!empty($brand_data)){
            return strtolower($brand_data[0]['user_name']);
        }else{
            return 0;
        }
    }

    function product_stock_check($product_id = NULL){
        if($product_id > 0){
            $query = $this->db->query('select stock_count from product_inventory where product_id = "'.$product_id.'" and stock_count > 0'); 
            $stock_count = $query->result_array();
            if(!empty($stock_count)){
                return 1;
            }else{
                return 0;
            }
        }
    }
}