<?php

class Collection_model extends MY_Model {
	
	
	function get_all_collection($data){
		//echo '<pre>';print_r($data);
		$search_cond = '';$collection_array = array();
		if(@$data['search_by'] == '1' && @$data['table_search'] != ''){ $search_cond = " AND a.object_id IN(".strip_tags(trim(@$data['table_search'])).")"; }
		else if(@$data['type'] == 'single'){ $data['type']=''; if(isset($data['object_id']) && @$data['object_id']!=''){$search_cond = " AND a.object_id NOT IN(".strip_tags(trim(@$data['object_id'])).")";} }
		else{ $search_cond = ''; }
		
		$where_cond = '';
		//gender
		if(@$data['gender'] == 'men' || @$data['gender'] == '2'){
			$data['gender'] = '2'; 
			$where_cond = $where_cond." AND b.object_meta_key = 'gender' AND b.object_meta_value = '".$data['gender']."' ";
		}else if(@$data['gender'] == 'women' || @$data['gender'] == '1'){
			$data['gender'] = '1';
			$where_cond = $where_cond." AND b.object_meta_key = 'gender' AND b.object_meta_value = '".$data['gender']."' ";
		}else{ 
			$data['gender'] = '0';
			
		}
		
		if(@$data['body_shape'] >0 && @$data['pa_pref1'] >0){
			
			$query = $this->db->query("select Distinct a.object_id as id,a.object_name as name,CONCAT('".COLLECTION_AND_EVENT_URL."',a.object_image) as image from object as a,object_meta as b where a.object_id = b.object_id AND a.object_type = 'collection' AND b.object_status != 0 AND (b.object_meta_key = 'body_shape' && FIND_IN_SET('".$data['body_shape']."',b.object_meta_value)) AND a.object_id IN(select obj.object_id from object_meta as obj where a.object_id = obj.object_id AND (obj.object_meta_key = 'pref1' && FIND_IN_SET('".$data['pa_pref1']."',obj.object_meta_value))) AND a.object_status = 3 ".$search_cond." order by a.object_id desc ".$data['limit_cond']);
			
		}else{
			
			$query = $this->db->query("select Distinct a.object_id as id,a.object_name as name,CONCAT('".COLLECTION_AND_EVENT_URL."',a.object_image) as image from object as a,object_meta as b where a.object_id = b.object_id AND a.object_type = 'collection' AND b.object_status != 0 AND a.object_status = 3 ".$where_cond." ".$search_cond." order by a.object_id desc ".$data['limit_cond']);
		}
		// echo $this->db->last_query();exit;
		$result = $query->result_array();
		$collection_array = array();
		foreach($result as $val){
			$collection_array[] = $this->get_single_collection($val['id'],$data);
		}
		
		return $collection_array;
		
	}
	
	function get_single_collection($collection_id,$data){
		$query = $this->db->query("select Distinct a.object_id as id,a.object_name as name,b.object_meta_key,b.object_meta_value,CONCAT('".COLLECTION_AND_EVENT_URL."',a.object_image) as object_image,a.object_slug from object as a,object_meta as b where a.object_id = b.object_id AND a.object_type = 'collection' AND b.object_status != 0 AND b.object_id = $collection_id AND a.object_status = 3 order by a.object_id desc ");
		$result = $query->result_array();
		$single_collection = array();
		$single_collection['collection_id'] = $collection_id;
		$single_collection['collection_name'] = @$result[0]['name'];
		$single_collection['collection_image'] = @$result[0]['object_image'];
		$single_collection['object_slug'] = @$result[0]['object_slug'];
		$single_collection['is_wishlisted'] = $this->is_wishlisted('collection',$collection_id,$data['user_id']);
		$single_collection['share_url'] = base_url().'collection_web/single_collection/'.$collection_id;
		foreach($result as $val){
			if(@$data['type'] != 'single' || (@$data['type'] == 'single' && @$val['object_meta_key'] != 'collection_products')){
				$single_collection[$val['object_meta_key']] = $val['object_meta_value'];
			}else if(@$data['type'] == 'single' && $val['object_meta_key'] == 'collection_products'){
				$single_collection['collection_products_array'] = unserialize($val['object_meta_value']);
				$single_collection['collection_products'] = $this->Mobile_look_model->get_products_list(unserialize($val['object_meta_value']),$data);
				$single_collection['total_row'] = (string)count(unserialize($val['object_meta_value']));
			}
			
		}
		return $single_collection;
	}
	
	function filter_collection_products($user_id=0,$object_id=0,$lastlevelcat=null,$brandId=null,$priceId=null,$sort_by=0,$attributesId=null,$sizeId=null,$limit=0,$offset=0,$gender_cat_id=0,$source=null){
		$start = microtime(true);
		$query = $this->db->query('select b.object_meta_key,b.object_meta_value from object_meta as b where b.object_id = '.$object_id.' AND  b.object_meta_key = "collection_products" ');
		$result = $query->result_array();
		
		$product_ids = implode(',',unserialize($result[0]['object_meta_value']));
		$data['filter']['paid_brands'] = $this->Category_filter_model->get_paid_brand_list($product_ids);
		$w_c = ''; $w_c1 = '';$product_array = array();$order_by='';$web_product_ids='';
		
		if($lastlevelcat!=null){
			
			$w_c1 = ' and category_id IN('.trim($lastlevelcat).') AND object_id IN('.trim($product_ids).') ';
			
            $get_pro_id = $this->db->query("select distinct object_id from category_object_relationship where object_type='product' ".$w_c1." order by object_id desc");
            $result = $get_pro_id->result_array();
			$object_product_ids = array();
            if(!empty($result)){
                foreach($result as $val){
                    $object_product_ids[] = $val['object_id'];
                }

                $product_ids = implode(',', $object_product_ids);
            }
			
        }else if($gender_cat_id > 0 && $source == 'web'){
			
			$w_c1 = ' and category_id IN('.trim($gender_cat_id).') AND object_id IN('.trim($product_ids).') ';
			
            $get_pro_id = $this->db->query("select distinct object_id from category_object_relationship where object_type='product' ".$w_c1." order by object_id desc");
            $result = $get_pro_id->result_array();
			$object_product_ids = array();
            if(!empty($result)){
                foreach($result as $val){
                    $object_product_ids[] = $val['object_id'];
                }

                $product_ids = implode(',', $object_product_ids);
				$web_product_ids = $product_ids;
            }
			
		}
		
		//brand filter 
		if($brandId!=''){ $w_c .= ' and a.brand_id IN('.$brandId.')'; }
		
		// price filter by priceId
		if($priceId == 1) { $w_c .= ' and a.price <= 999 ';  }
        else if($priceId == 2) { $w_c .= ' and (a.price >= 1000 ) and (a.price <= 1999 )';  }
        else if($priceId == 3) { $w_c .= ' and (a.price >= 2000 ) and (a.price <= 3499 ) ';  }
        else if($priceId == 4) { $w_c .= ' and (a.price >= 3500 ) and (a.price <= 4999 ) ';  }
        else if($priceId == 5) { $w_c .= ' and a.price > 5000 ';  }
		
		// filter by size
		if($sizeId!=null){
            $w_c .= ' and b.size_id IN('.$sizeId.')';
        }
		
		// sort by price
		// if($sort_by<1)$order_by = 'FIELD(a.id,'.$product_ids.')';
		
		if($sort_by == '2'){
            $order_by = 'order by a.price asc';
			$prod_data['order_by'] = 'order by p.price asc';
        }else if($sort_by == '1'){
            $order_by = 'order by a.price desc';
			$prod_data['order_by'] = 'order by p.price desc';
        }
		
		$w_c .= ' and a.id IN('.$product_ids.')';
		
		$total_product_query = $this->db->query("select distinct a.id from product_desc as a,product_inventory as b where 1 and a.id = b.product_id and a.status = 1 and a.is_delete=0 and a.approve_reject IN ('A','D') and b.stock_count > 0 ".$w_c." ".$order_by." ");
        $total = $total_product_query->result_array();
		$total_collection_product = count($total);
		
		$get_products = $this->db->query("select distinct a.id from product_desc as a,product_inventory as b where 1 and a.id = b.product_id and a.status = 1 and a.is_delete=0 and a.approve_reject IN ('A','D') and b.stock_count > 0 ".$w_c." ".$order_by." limit ".$offset.",".$limit);
        $res = $get_products->result_array();
		// echo $this->db->last_query();exit;
		$product_ids = array();$i = 0;
		if(!empty($res)){
			foreach($res as $row){
				$product_ids[$i] = $row['id'];
				$i++;
			}
		}
		$prod_data['limit_cond'] = '';
		$prod_data['user_id'] = $user_id;
		$prod_data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
		if(!empty($product_ids))$product_array = $this->Mobile_look_model->get_products_list($product_ids,$prod_data);
		$product_id = implode(',',$product_ids);
		if($sort_by != '2' && $sort_by != '1'){ shuffle($product_array); }
		$data['products'] = $product_array;
		if($product_id != ''){
			// $data['filter']['category'] = $this->Category_filter_model->get_all_product_category($product_id); //category list
			$data['filter']['size'] = $this->Category_filter_model->get_product_size($product_id);
		}else{
			// $data['filter']['category'] = array(); //category list
			$data['filter']['size'] = array();
		}
		if($web_product_ids == ''){ $web_product_ids = $product_id; }
		$data['web_product_ids'] = $web_product_ids;
		$data['total_products'] = $total_collection_product;
		return $data;
		
	}
	
	function get_collection_info($object_id){
		$query = $this->db->query("select Distinct a.object_id as id,a.object_name as name,b.object_meta_key,b.object_meta_value,CONCAT('".base_url()."sc_admin/assets/event_img/',a.object_image) as object_image,a.object_slug from object as a,object_meta as b where a.object_id = b.object_id AND a.object_type = 'collection' AND b.object_status != 0 AND b.object_id = $object_id AND a.object_status = 3 order by a.object_id desc ");
		$result = $query->result_array();
		
		$single_collection = array();
		$single_collection['collection_name'] = @$result[0]['name'];
		foreach($result as $val){
			$single_collection[$val['object_meta_key']] = $val['object_meta_value'];
		}
		// echo "<pre>";print_r($single_collection);exit;
		return $single_collection;
	}
	
	function get_products_list($product_ids,$data){
		$data['limit_cond'] = " LIMIT ".$data['offset'].",".$data['limit'];
		if($product_ids != ''){
			$product_id_in = implode(',',$product_ids);
			$query = $this->db->query("select a.id as product_id,CONCAT('".$data['product_link']."',a.image) as product_image,a.name,(select cat.category_name from category_object_relationship as co,category as cat where co.category_id = cat.id and co.object_type = 'product' and co.object_id = a.id order by co.category_id desc limit 1) product_category,b.company_name as brand_name,a.price,CASE 
							WHEN  a.discount_start_date <= CURRENT_TIMESTAMP() and a.discount_end_date >= CURRENT_TIMESTAMP() then a.compare_price
							ELSE 0 END AS compare_price,b.user_id as brand_id from product_desc as a,brand_info as b where a.brand_id = b.user_id AND a.id IN(".$product_id_in.") AND a.id = a.parent_id ".$data['limit_cond']);
			$result = $query->result_array();
			// print_r($result);exit;
			$product_array = array();
			
			foreach($result as $val){
				$productobject = new stdClass(); 
				$productobject->product_id = $val['product_id'];
				$productobject->product_image = $val['product_image'];
				$productobject->product_name = $val['name'];
				$productobject->product_category = $val['product_category'];
				$productobject->brand_name = $val['brand_name'];
				/* calculate discount and discount % */
				$discount_price = 0;$discount_percent =0;$new_discount_percent=0;
				$brand_discount = $this->brand_discount($val['brand_id']);
				$product_discount = $this->product_discount($val['product_id']);
				// print_r($brand_discount);exit;
				if(!empty($brand_discount)){
					$productobject->discount_percent = $brand_discount['discount_percent'];
					$discount_price = $val['price'] - ($val['price'] * ($brand_discount['discount_percent']/100));
					$productobject->discount_price = (string)($val['price'] - ($val['price'] * ($brand_discount['discount_percent']/100)));
				}else if(!empty($product_discount)){
					$productobject->discount_percent = $product_discount['discount_percent'];
					$discount_price = $val['price'] - ($val['price'] * ($product_discount['discount_percent']/100));
					$productobject->discount_price = (string)($val['price'] - ($val['price'] * ($product_discount['discount_percent']/100)));
				}else {
					$productobject->discount_percent = '0';
					$discount_price = 0;
					$productobject->discount_price = '';
				}
				
			/* calculate discount and discount % */
			
			/*  added for new discount compare price code start*/
			
			// $val['compare_price'] = 100;$val['price']=80;$discount_percent = 0;$discount_price=72;
			// echo $val['product_id'].'----'.$val['compare_price'].'----'.$val['price'].'</br>';
			if($discount_percent > 0 || $val['compare_price']>$val['price']){
				$productobject->price = (string)$val['compare_price'];
				$productobject->discount_price = (string)$discount_price;
				$new_discount_percent = $this->calculate_discount_percent($val['compare_price'],$discount_price,$val['price']);
				$productobject->discount_percent = (string)$new_discount_percent;
			}else {
				$productobject->price = (string)$val['price'];
				$productobject->discount_price = (string)$discount_price;
				$val['compare_price'] = $val['price'];
				$new_discount_percent = $this->calculate_discount_percent($val['compare_price'],$discount_price,$val['price']);
				$productobject->discount_percent = (string)$new_discount_percent;
			}
				
			
			/*  added for new discount compare price code end*/
			
				$productobject->is_wishlisted = $this->is_wishlisted('product',$val['product_id'],$data['user_id']);
				$productobject->is_available = $this->is_available($val['product_id']);
				$product_array[] = $productobject;
			}
			
			return $product_array;
		}else {
			return array();
		}
	}
	
	function brand_discount($brand_id){
		$query = $this->db->query(" select IF(a.discount_percent is null, '0', a.discount_percent) as discount_percent FROM discount_availed AS a WHERE a.brand_id = $brand_id AND a.discount_type_id = 1 AND NOW() >= a.start_date AND NOW() <= a.end_date AND a.status=1 ");
		$res = $query->result_array();
		if (!empty($res)) return $res[0]; else return null;
	}
	
	function product_discount($product_id){
		$query = $this->db->query("SELECT  IF(sum(a.discount_percent) is null, '0', sum(a.discount_percent)) as discount_percent FROM discount_availed AS a WHERE a.product_id = $product_id AND a.discount_type_id = 2 AND NOW() >= a.start_date AND NOW() <= a.end_date AND a.status=1 ");
		$res = $query->result_array();
		if (!empty($res)) return $res[0]; else return null;
	}
	
	function calculate_discount_percent($compare_price,$discount_price,$product_price=null){
		$discount_percent = 0;
		if($compare_price > 0 && $discount_price > 0){
			$discount_percent = round((1-($discount_price/$compare_price))*100);
		}else if($compare_price > 0 && $product_price > 0){
			$discount_percent = round((1-($product_price/$compare_price))*100);
		}
		
		return $discount_percent;
	}
	
	function is_wishlisted($fav_for,$fav_id,$user_id){
		if($user_id>0){
			$query = $this->db->query("select id,user_id from favorites where fav_id=$fav_id AND user_id=$user_id AND fav_for = '$fav_for' ");
			$res = $query->result_array();
			if(count($res)>0){
				return '1';
			}else return '0';
		}else return 0;
		
	}
	
	function is_available($product_id){
		
		// $query = $this->db->query("select a.stock_count,a.id from product_desc as a,attribute_object_relationship as b,attribute_values_relationship as c where (a.id = $product_id OR a.parent_id = $product_id) AND b.object_id in(select pd.id from product_desc pd where pd.id = $product_id OR pd.parent_id = $product_id) AND c.attribute_id = ".ATTRIBUTE_SIZE_ID." AND b.attribute_id = c.id order by stock_count desc");
		$query = $this->db->query("select a.stock_count from product_desc as a where a.id = $product_id OR a.parent_id = $product_id order by stock_count desc");
		$res = $query->result_array();
		// echo $this->db->last_query();
		// print_r($res);exit;
		if(!empty($res)){
			if($res[0]['stock_count'] > 0){ return '1';}else return '0';
		}else return '0';
	}
	
	function user_wishlist($user_id,$fav_for,$fav_id){          
        
        $query = $this->db->query("select id,user_id from favorites where fav_id=$fav_id AND user_id=$user_id AND fav_for = '$fav_for' ");
		$res = $query->result_array();
		if(!empty($res)){
			$id = $res[0]['id'];
			$query = $this->db->query(" Delete from favorites where id=$id ");
			return 1;
		}else{ 
			$data = array('user_id'=>$user_id,'fav_id'=>$fav_id,'fav_for'=>$fav_for);
			$result = $this->db->insert('favorites', $data);         
			if(!empty($result)){
				return 1;
			}else{ 
				return 2;
			}
		}
		
    }
	
	
 }