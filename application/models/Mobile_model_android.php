<?php
class Mobile_model_android extends MY_Model {
	
	
 function registerUserMobile($firstname,$medium,$medium_id,$scusername,$scemail_id,$scpassword,$gender,$ip,$role,$contact_no,$birth_date,$last_name){
        /*added by Hari for mobile_facebook and google login*/
        $new_auth_token = substr(md5(uniqid(rand(1,6))), 0, 15);//added for generate token globally
        $insert = 0;
        $picture = '';
        $facebook_id = '';
        $google_id = '';
        if(isset($medium) && ($medium ==='mobile_facebook' || $medium === 'mobile_google' || $medium === 'mobile_ios_facebook' || $medium === 'mobile_android_facebook')){
            if($medium === 'mobile_google'){
                //$picture = $json_array['picture'];
                $google_id = $medium_id;
            }
            elseif($medium === 'mobile_facebook' || $medium === 'mobile_ios_facebook' || $medium === 'mobile_android_facebook') {
                //$picture = $json_array['picture']['data']['url'];
                $facebook_id = $medium_id;
            }
            if($this->usernameUnique($scusername) != NULL){
                 $scusername = $scusername.rand(1,1000);
            }
            if($this->emailUnique($scemail_id) != NULL){
                 $query = $this->db->query('select id from user_login where email="'.$scemail_id.'"');
                 $result = $query->result_array();
                 $insert = 1;
            }
        }
        if($insert===0){
        /***Code Ends***/
            
           $salt = substr(md5(uniqid(rand(), true)), 0, 10);
           $password   =$salt . substr(sha1($salt . $scpassword), 0, -10);
            $data = array(
               'user_name' => $scusername,
               'email' => $scemail_id,
               'password' => $password,
               'auth_token'=> $new_auth_token,
               'ip_address' => $ip,
               'status' => 1,
               'created_datetime' => $this->config->item('sc_date'),
               'modified_datetime' => $this->config->item('sc_date')

            );
            $this->db->insert('user_login', $data); 
            $Lst_ins_id = $this->db->insert_id();
            if($Lst_ins_id){
                $data = array('gender'=>$gender,'facebook_id'=>$facebook_id,'google_id'=>$google_id,'profile_pic'=>$picture, 'user_id' => $Lst_ins_id, 'first_name' => $firstname,  'created_datetime' => $this->config->item('sc_date'),
               'modified_datetime' => $this->config->item('sc_date'), 'registered_from'=>$medium,'created_by' => $Lst_ins_id, 'modified_by' => $Lst_ins_id,'contact_no'=>$contact_no,'birth_date'=>$birth_date,'last_name'=>$last_name);

                $this->db->insert('user_info', $data); 

                $data = array('user_id'=>$Lst_ins_id,'role_id'=>$role,'created_by'=>$Lst_ins_id,'modified_by'=>$Lst_ins_id);

                $this->db->insert('user_role_mapping', $data); 

                $res = $this->login_user_mobile($scemail_id,$scpassword);
                return $res;
                //return true;
            }
        }else{
            /*edit section written by Hari on 08 Sep 2015 */
            $query = $this->db->query("select profile_pic from user_info where user_id = ".$result[0]['id']);
            $res = $query->result();
            if(empty($res[0]->profile_pic) ){
                $data = array('facebook_id'=>$facebook_id,'google_id'=>$google_id,'profile_pic'=>$picture, 'registered_from'=>$medium,'modified_by' => $result[0]['id'],'modified_datetime' => $this->config->item('sc_date'),'contact_no'=>$contact_no,'birth_date'=>$birth_date,'first_name' => $firstname,'last_name'=>$last_name);
            }else{
                $data = array('facebook_id'=>$facebook_id,'google_id'=>$google_id,'registered_from'=>$medium,'modified_by' => $result[0]['id'],'modified_datetime' => $this->config->item('sc_date'),'contact_no'=>$contact_no,'birth_date'=>$birth_date,'first_name' => $firstname,'last_name'=>$last_name);
            }
            $this->db->update('user_info', $data, array('user_id' => $result[0]['id'] ));
           // $new_auth_token = substr(md5(uniqid(rand(1,6))), 0, 15);//added for generate token globally

            $query = $this->db->query("select user_role_mapping.role_id,user_info.user_id,user_info.created_datetime,user_info.first_name,user_info.last_name,user_login.bucket,user_login.question_comp,user_login.question_resume_id from user_info,user_role_mapping,user_login where user_info.user_id = ".$result[0]['id']." and user_info.user_id = user_role_mapping.user_id and user_info.user_id = user_login.id");
            $res = $query->result();
            
            if(!empty($res)){
                $data = array('auth_token' => $new_auth_token);
                $this->db->update('user_login', $data, array('id' =>$res[0]->user_id));//update token
                $data_attempts = array('user_id'=>$res[0]->user_id,'login_time'=>$this->config->item('sc_date'));
                $this->db->insert('user_login_attempts', $data_attempts);
                $login_attempts_id = $this->db->insert_id();

               $array_field[] =array('user_id' => $res[0]->user_id,'bucket_mobile'=>$res[0]->bucket,'question_comp_mobile'=>$res[0]->question_comp);
               return ($array_field);
            }else{
                return 2;   
            }
            
        }
    }
	
    function get_users_info_mobile($user_id){
         
         $query = $this->db->query("select user_role_mapping.role_id,user_info.user_id,user_info.created_datetime,user_info.first_name,user_info.last_name,user_login.bucket,user_login.auth_token,user_login.question_comp,user_login.question_resume_id,user_info.registered_from,user_info.facebook_id,user_info.google_id,IF(user_info.profile_pic='', IF(user_info.gender = 1,'https://www.stylecracker.com/assets/images/profile/profile-women.jpg','https://www.stylecracker.com/assets/images/profile/profile-men.jpg'), CONCAT('https://www.stylecracker.com/sc_admin/assets/user_profiles/',user_info.profile_pic)) as profile_pic , IF(user_info.profile_cover_pic IS NULL, 'https://www.stylecracker.com/assets/images/profile/cover.jpg', CONCAT('https://www.stylecracker.com/sc_admin/assets/full_pic/',user_info.profile_cover_pic)) as profile_cover_pic,IF(user_info.gender=1 ,'women', 'men') as gender from user_info,user_role_mapping,user_login where user_info.user_id = ".$user_id." and user_info.user_id = user_role_mapping.user_id and user_info.user_id = user_login.id");
            $res = $query->result();

            return $res[0];
    }
	
	
	function login_user_mobile($email,$password){
        $new_auth_token = substr(md5(uniqid(rand(1,6))), 0, 15);//added for generate token globally
        $query = $this->db->get_where('user_login', array('email' => $email,'status'=>1));
        $result = $query->result();
        if(!empty($result)){
        $salt = substr($result[0]->password, 0, 10);
        $db_password =  $salt . substr(sha1($salt . $password), 0, -10); 
        if($db_password == $result[0]->password){
            $query = $this->db->query("select user_role_mapping.role_id,user_info.user_id,user_info.created_datetime,user_info.first_name,user_info.last_name,user_login.bucket,user_login.question_comp,user_login.question_resume_id from user_info,user_role_mapping,user_login where user_info.user_id = ".$result[0]->id." and user_info.user_id = user_role_mapping.user_id and user_info.user_id = user_login.id");
            $res = $query->result();
            if(!empty($res)){
                $data = array('auth_token' => $new_auth_token);
                $this->db->update('user_login', $data, array('id' =>$res[0]->user_id));//update token
                $data_attempts = array('user_id'=>$res[0]->user_id,'login_time'=>$this->config->item('sc_date'));
                $this->db->insert('user_login_attempts', $data_attempts);
                $login_attempts_id = $this->db->insert_id();

                $array_field[] =array('user_id' => $res[0]->user_id,'bucket_mobile'=>$res[0]->bucket,'question_comp_mobile'=>$res[0]->question_comp);
                return ($array_field);
                //return 1;
            }else{
                return 2;   
            }
        }else{ 
                return 2;
            }
        }else{ return 2; } 
    }
	
    function get_look_id($user_id,$auth_token)
	{
        $query = $this->db->query("select look_fav.look_id from look_fav where look_fav.user_id = ".$user_id." ");
        $res = $query->result_array();
        return $res;
	}
	
	function get_wishlist_looks($user_id,$limit,$offset)
	{
		
		$query = $this->db->query('select o.slug,o.id,o.product_img,o.look_type,o.image,o.look_image,name,o.created_datetime as timestamp,o.modified_datetime,o.status,o.deflag,@type:="look" as type from
(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) )as o,look_fav as h where o.`id` = h.`look_id` and h.`user_id` = '.$user_id.' order by o.modified_datetime '.$limit.$offset);
        $res = $query->result_array();
		
		$looks_array = array();
			
			$i = 0; 	
			if(!empty($res)){
				foreach($res as $val){
	
					$alllooksarrayobject = new stdClass(); 
					$alllooksarrayobject->id = $val['id'];
					$alllooksarrayobject->name = $val['name'];
					$alllooksarrayobject->timestamp = $val['timestamp'];
					if($val['look_type'] == 1){
						$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
					}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
						$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
					}else if($val['look_type'] == 3){
						$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
					}
					$info = getimagesize($alllooksarrayobject->url);
					//if(isset($info) && ($info[1] == NULL || $info[1] == '') ) $info[1] = 0;
					//if(isset($info) && ($info[0] == NULL || $info[0] == '' )) $info[0] = 0;
					$alllooksarrayobject->height = $info[1];
					$alllooksarrayobject->width  = $info[0];
					$alllooksarrayobject->type = $val['type'];
					$alllooksarrayobject->look_type = $val['look_type'];
					$alllooksarrayobject->user_share = $this->get_user_share($val['id']);
					$alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
					$alllooksarrayobject->share_url = 'https://www.stylecracker.com/looks/look-details/'.$val['slug'];
					$alllooksarrayobject->is_discount = (string)$this->get_discount($val['id']);
					$alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
					$looks_array[$i] = $alllooksarrayobject;
					$i++;
				}
			}
			
			return $looks_array;
        // return $res;
	}
	
    function login_user_token($user_id, $auth_token){
        $query = $this->db->get_where('user_login', array('id' => $user_id,'auth_token'=>$auth_token));
        $result = $query->result();
        if(!empty($result)){

            return $result;
        }
    }
	 
	
    function forget_user_email_mobile($sc_email_id){
	$query = $this->db->get_where('user_login', array('email'=>$sc_email_id));
	$result = $query->result();
	if(sizeof($result)>0)
        {
            $scpassword =$this->randomPassword_mobile();
            $salt = substr(md5(uniqid(rand(), true)), 0, 10);
            $password   =$salt . substr(sha1($salt . $scpassword), 0, -10);
            $data=array('password'=>$password);
            $this->db->update('user_login', $data, array('email' => $sc_email_id ));

            return  $scpassword;
        }else
        {
            return false;
        } 
	/*if(!empty($result)){
	return 1;
	}else{
	return 2;
	}
	*/
    }

    function single_look_id_mobile($single_look_id){   
	
		$query = $this->db->query("select distinct k.id,k.slug,product_img,k.look_type,k.image,look_image,name,k.created_datetime,k.modified_datetime as timestamp,k.status,k.deflag,k.stylist_id,@type:='look' as type from
(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag,z.`stylist_id` from 
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag,a.`stylist_id` FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 and a.id = '".$single_look_id."' ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc) as k,look_bucket_mapping as m where k.id = m.look_id order by k.modified_datetime desc ");

		/* Added by saylee*/
			$res = $query->result_array();
			
			$looks_array = array();
			
			$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
					$alllooksarrayobject = new stdClass(); 
					$alllooksarrayobject->id = $val['id'];
					$alllooksarrayobject->name = $val['name'];
					$alllooksarrayobject->timestamp = $val['timestamp'];
					if($val['look_type'] == 1){
						$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
						$info = getimagesize($alllooksarrayobject->url);
					}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
						$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
						$info = getimagesize($alllooksarrayobject->url);
					}else if($val['look_type'] == 3){
						$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
					}
					/*
					$info = getimagesize($alllooksarrayobject->url);
					$alllooksarrayobject->height = $info[1];
					$alllooksarrayobject->width  = $info[0];*/
					$alllooksarrayobject->height = 530;
					$alllooksarrayobject->width  = 530;
					$alllooksarrayobject->type = $val['type'];
					$alllooksarrayobject->look_type = $val['look_type'];
					// $alllooksarrayobject->user_share = $this->get_user_share($val['id']);
					// $alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
					// $alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
					$alllooksarrayobject->user_share = "0";
					$alllooksarrayobject->user_likes = "0";
					$alllooksarrayobject->look_type_txt = "Travel";
					$alllooksarrayobject->share_url = 'https://www.stylecracker.com/looks/look-details/'.$val['slug'];
					$looks_array[$i] = $alllooksarrayobject;
					$i++;
				} 
		}
			
		return $looks_array[0];
		
		
    }

    function looks_by_stylist_id_mobile($stylist_id,$offset,$limit){   
	
	$query = $this->db->query("select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime as timestamp,z.status,z.deflag,@type:='look' as type from 
	(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 and a.stylist_id=$stylist_id ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime DESC $limit $offset");
	$res = $query->result_array();
	// print_r($res);
	// exit;

	$looks_array = array();
			
			$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
					$alllooksarrayobject = new stdClass(); 
					$alllooksarrayobject->id = $val['id'];
					$alllooksarrayobject->name = $val['name'];
					$alllooksarrayobject->timestamp = $val['timestamp'];
					if($val['look_type'] == 1){
						$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
					}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
						$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
					}else if($val['look_type'] == 3){
						$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
					}/*
					$info = getimagesize($alllooksarrayobject->url);
					$alllooksarrayobject->height = $info[1];
					$alllooksarrayobject->width  = $info[0];*/
					$alllooksarrayobject->height = 530;
					$alllooksarrayobject->width  = 530;
					$alllooksarrayobject->type = $val['type'];
					$alllooksarrayobject->look_type = $val['look_type'];
					// $alllooksarrayobject->user_share = $this->get_user_share($val['id']);
					// $alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
					// $alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
					$alllooksarrayobject->user_share = "0";
					$alllooksarrayobject->user_likes = "0";
					$alllooksarrayobject->look_type_txt = "Travel";
					$alllooksarrayobject->share_url = 'https://www.stylecracker.com/looks/look-details/'.$val['slug'];
					$looks_array[$i] = $alllooksarrayobject;
					$i++;
				}
			}
			
		//print_r($looks_array);exit;
		
       if(!empty($res)){
            return $looks_array;
        }else{
            return NULL;
        }
    }
	
	function single_look_mobile($look_id){
	 
	 $query = $this->db->query("Select DISTINCT p.id as product_id,CASE 
        WHEN  p.discount_start_date <= CURRENT_TIMESTAMP() and p.discount_end_date >= CURRENT_TIMESTAMP() then p.compare_price
        ELSE 0
    END AS compare_price,b.brand_store_name as sold_by,@type:='' as category,lp.look_id, p.brand_id,p.name,p.url,p.description,p.slug,p.price,p.url as buy_now_url,CONCAT('https://www.stylecracker.com/sc_admin/assets/products/',p.image) as url,(select bi.company_name from brand_info bi where bi.user_id=p.brand_id) AS brand_name, (select bi.redirect_sep from brand_info bi where bi.user_id=p.brand_id) AS redirect_sep from look_products as lp, product_desc as p,product_store a,brand_store b where p.id = lp.product_id AND p.approve_reject='A' AND look_id =$look_id AND p.id = a.product_id AND a.store_id = b.id");
	 $res = $query->result_array();
	 
	 $product_array = array();
		$i=0;
		if(!empty($res)){
			foreach($res as $val){
				
				$productobject = new stdClass(); 
				$productobject->product_id = $val['product_id'];
				$productobject->product_category = $val['category'];
				$productobject->look_id = $val['look_id'];
				$productobject->brand_id = $val['brand_id'];
				$productobject->name = $val['name'];
				$productobject->sold_by = $val['sold_by'];
				$productobject->return_policy = 'As Per Brand';
				$productobject->description = $val['description'];
				$productobject->slug = $val['slug'];
				$productobject->price = $val['price'];
				$productobject->buy_now_url = $val['buy_now_url'];
				$productobject->is_available = $this->is_available($val['product_id']);
				$productobject->url = $val['url'];
				$productobject->brand_name = $val['brand_name'];
				//$productobject->redirect_sep = $val['redirect_sep'];
				$size_category = $this->size_category($val['product_id']);
				$productobject->size_category = $size_category['size_guide'];
				$productobject->product_size = $this->product_size($val['product_id']);
				$brand_discount = $this->brand_discount($val['brand_id']);
				$product_discount = $this->product_discount($val['product_id']);
				$discount_price = 0;
				if(!empty($brand_discount)){
					$productobject->discount_percent = $brand_discount['discount_percent'];
					$discount_price = $val['price'] - ($val['price'] * ($brand_discount['discount_percent']/100));
					$productobject->discount_price = (string)($val['price'] - ($val['price'] * ($brand_discount['discount_percent']/100)));
				}else if(!empty($product_discount)){
					$productobject->discount_percent = $product_discount['discount_percent'];
					$discount_price = $val['price'] - ($val['price'] * ($product_discount['discount_percent']/100));
					$productobject->discount_price = (string)($val['price'] - ($val['price'] * ($product_discount['discount_percent']/100)));
				}else {
					$productobject->product_discount = '0';
					$discount_price = 0;
					$productobject->discount_price = '';
				}
				
				if($discount_price < $val['price']){ $productobject->on_sale = 'ON SALE'; }
				else $productobject->on_sale = '';
				
				/*  added for new discount compare price code start*/
				
				// $val['compare_price'] = 100;$val['price']=80;$discount_percent = 10;$discount_price=72;
				if($discount_percent > 0 || $val['compare_price']>$val['price']){
					$productobject->price = (string)$val['compare_price'];
					$productobject->discount_price = (string)$discount_price;
					$new_discount_percent = $this->calculate_discount_percent($val['compare_price'],$discount_price,$val['price']);
					$productobject->discount_percent = (string)$new_discount_percent;
				}else {
					$productobject->price = (string)$val['price'];
					$productobject->discount_price = (string)$discount_price;
					$val['compare_price'] = $val['price'];
					$new_discount_percent = $this->calculate_discount_percent($val['compare_price'],$discount_price,$val['price']);
					$productobject->discount_percent = (string)$new_discount_percent;
				}
					
				
				/*  added for new discount compare price code end*/
				
				$productobject->color = '';
				$productobject->fabric = '';
				$productobject->primary_color = '';
				$productobject->share_url = base_url().'product/details/'.$val['slug'].'-'.$val['product_id'];
				$productobject->size_guide_url = $this->get_size_guide_n($size_category['size_guide'],$val['product_id'],$val['brand_id']);
				$product_array[$i] = $productobject;
				$i++;
			}
				
		}
		
	//print_r($product_array);exit;
	
	 if(!empty($product_array)){
	    return $product_array;
        }else{
            return NULL;
        }
		
	}
	
	function brand_discount($brand_id){
		$query = $this->db->query(" select IF(a.discount_percent is null, '0', a.discount_percent) as discount_percent FROM discount_availed AS a WHERE a.brand_id = $brand_id AND a.discount_type_id = 1 AND NOW() >= a.start_date AND NOW() <= a.end_date AND a.status=1 ");
		$res = $query->result_array();
		if (!empty($res)) return $res[0]; else return null;
	}
	
	function product_discount($product_id){
		$query = $this->db->query("SELECT  IF(sum(a.discount_percent) is null, '0', sum(a.discount_percent)) as discount_percent FROM discount_availed AS a WHERE a.product_id = $product_id AND a.discount_type_id = 2 AND NOW() >= a.start_date AND NOW() <= a.end_date AND a.status=1 ");
		$res = $query->result_array();
		if (!empty($res)) return $res[0]; else return null;
	}
	
	function is_available($product_id){
		$query = $this->db->query("SELECT a.stock_count FROM product_inventory a WHERE a.product_id = $product_id order by a.stock_count desc ");
		$res = $query->result_array();
		// echo $res[0]['stock_count'];
		// print_r($res);exit;
		if($res != NULL && $res[0]['stock_count']>0)
			return 1;
		else return 0;
		return $res;
	}
	
	function product_size($product_id){       
	$size_array =  $this->db->query("select b.size_text from product_inventory a,product_size b where a.product_id = $product_id And a.size_id = b.id AND a.stock_count>0 GROUP BY b.id");
	$res1 = $size_array->result();
	return $res1;
	}
	
	function size_category($product_id){       
	$size_array =  $this->db->query("select n.`size_guide` from `product_variations` as m,`product_variation_type` as n,product_desc c where m.`product_var_type_id` = n.`id` and m.`product_id` = c.id AND c.id = $product_id order by n.size_guide DESC ");
	$res1 = $size_array->result_array();
	
	if(!empty($res1)) return $res1[0]; else return null;
	}
	
    function user_like_mobile($user_id,$auth_token,$look_id){          
        
        $query = $this->db->query("select user_id,look_id from look_fav where look_id=$look_id AND user_id=$user_id");
		$res = $query->result();
		if($res != NULL){
			$query = $this->db->query(" Delete from look_fav where look_id=$look_id AND user_id=$user_id");
			return 1;
		}else{
			$created_by = date('Y-m-d G:i:s'); 
			$data = array('user_id'=>$user_id,'look_id'=>$look_id,'created_by'=>$created_by);
			$result = $this->db->insert('look_fav', $data);         
			if(!empty($result)){
				return 1;
			}else{
				return 2;
			}
		}
		
    }
	/*
    function user_search_mobile($user_search,$limit,$offset){
      
	  if($user_search!= null){
		$count_search = count($user_search);
		for($i=0;$i<$count_search;$i++){
			if($i == 0) $query_search = 'LOWER(a.`name`) LIKE LOWER("%'.urldecode($user_search[$i]).'%")';
			else $query_search .= ' AND LOWER(a.`name`) LIKE LOWER("%'.urldecode($user_search[$i]).'%")';
		}

        // $where_condition = '';
        // $where_condition = $where_condition.' and '.$query_search.'';
        // $query = $this->db->query('select z.id,z.slug,z.look_type,CONCAT("http://www.stylecracker.com/sc_admin/assets/looks/",z.image) as url,name,z.created_datetime as timestamp,z.description,like_count,share_count,look_type_txt from 
           // (SELECT a.slug,a.id,a.stylist_id,a.description,a.referred_count,a.look_time_limit,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,(select count(look_fav.look_id) as likes from look INNER JOIN look_fav where a.id=look_fav.look_id AND look.id = look_fav.look_id ) as like_count,(select count(user_share.look_id) as share from look INNER JOIN user_share where a.id=user_share.look_id AND look.id = user_share.look_id) as share_count,(select look_sub_category.category_name from look JOIN look_category_mapping ON look.id=look_category_mapping.look_id JOIN look_sub_category ON look_category_mapping.cat_id=look_sub_category.id WHERE a.id=look_category_mapping.look_id AND look.id = look_category_mapping.look_id LIMIT 0,1) as look_type_txt FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 AND a.status=1 '.$where_condition.') as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4) order by z.id  desc '.$limit.$offset);
		   
			
			
			$where_condition = '';
			$where_condition = $where_condition.' and '.$query_search.'';
			$query = $this->db->query('select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime as timestamp ,z.status,z.deflag from 
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 and a.status=1 '.$where_condition.') as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4) order by z.id desc '.$limit.$offset);
             $res = $query->result_array();
			   $a = count($res); 

              if($a == 0 ){
            for($i=0;$i<$count_search;$i++){
                        if($i == 0) $query_search = 'LOWER(a.`name`) LIKE LOWER("%'.urldecode($user_search[$i]).'%")';
                        else $query_search .= ' OR LOWER(a.`name`) LIKE LOWER("%'.urldecode($user_search[$i]).'%")';
                    }
            $where_condition = '';
            $where_condition = $where_condition.' and '.$query_search.'';
            $query = $this->db->query('select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime as timestamp ,z.status,z.deflag from 
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 and a.status=1 '.$where_condition.') as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4) order by z.id desc '.$limit.$offset);
             $res = $query->result_array();

              }
			 $looks_array = array();
			
			$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
					$alllooksarrayobject = new stdClass(); 
					$alllooksarrayobject->id = $val['id'];
					$alllooksarrayobject->name = $val['name'];
					$alllooksarrayobject->timestamp = $val['timestamp'];
					if($val['look_type'] == 1){
						$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
						$info = getimagesize($alllooksarrayobject->url);
					}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
						$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
						$info = getimagesize($alllooksarrayobject->url);
					}else if($val['look_type'] == 3){
						$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
					}
					$info = getimagesize($alllooksarrayobject->url);
					//if($info[1] == NULL || $info[1] == '' ) $info[1] = 0;
					//if($info[0] == NULL || $info[0] == '' ) $info[0] = 0;
					$alllooksarrayobject->height = $info[1];
					$alllooksarrayobject->width  = $info[0];
					$alllooksarrayobject->type = 'looks';//$val['type'];added by Hari
					$alllooksarrayobject->look_type = $val['look_type'];
					$alllooksarrayobject->user_share = $this->get_user_share($val['id']);
					$alllooksarrayobject->share_url = 'https://www.stylecracker.com/looks/look_details/'.$val['slug'];
					$alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
					$alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
					$looks_array[$i] = $alllooksarrayobject;
					$i++;
				} 
		}
		return $looks_array;
           //  return $res;
    }
    // else 
       // $query_search = 'LOWER(a.`name`) LIKE LOWER("%'.urldecode($user_search[$i]).'%")';

        // $query = $this->db->query('select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime as timestamp ,z.status,z.deflag from 
// (SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 and a.status=1 ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4),look_tags a where a.look_tags like "%'.$tag.'%" AND a.look_id=z.id order by z.id desc '.$limit.$offset);
        // $res = $query->result_array();
        // return $res;


    }
	*/
   function user_search_mobile($user_search,$limit,$offset){
		$where_condition = '';
		$where_condition = $where_condition.'where `name` LIKE LOWER("%'.urldecode($user_search).'%") or `look_tags` LIKE LOWER
        ("%'.urldecode($user_search).'%")';
		 $query = $this->db->query('select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime as timestamp,z.status,z.deflag,LOWER(e.look_tags) as look_tags from 
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 and a.status=1 ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 || z.look_type = 6) LEFT JOIN look_tags as e ON z.id = e.look_id  
 '.$where_condition.' order by z.id desc '.$limit.' '.$offset.' ');
		$res = $query->result_array();
		
		$looks_array = array();
		$i = 0; 
			if(!empty($res)){
				foreach($res as $val){
					$alllooksarrayobject = new stdClass(); 
					$alllooksarrayobject->id = $val['id'];
					$alllooksarrayobject->name = $val['name'];
					$alllooksarrayobject->timestamp = $val['timestamp'];
					if($val['look_type'] == 1){
						$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
						$info = getimagesize($alllooksarrayobject->url);
					}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
						$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
						$info = getimagesize($alllooksarrayobject->url);
					}else if($val['look_type'] == 3){
						$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
					}
					$info = getimagesize($alllooksarrayobject->url);
					$alllooksarrayobject->height = $info[1];
					$alllooksarrayobject->width  = $info[0];
					$alllooksarrayobject->type = 'looks';//$val['type'];added by Hari
					$alllooksarrayobject->look_type = $val['look_type'];
					$alllooksarrayobject->user_share = $this->get_user_share($val['id']);
					$alllooksarrayobject->share_url = 'https://www.stylecracker.com/looks/look-details/'.$val['slug'];
					$alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
					$alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
					$looks_array[$i] = $alllooksarrayobject;
					$i++;
				} 
		}
		
		return $looks_array;
	}
	
	function get_answer_id2($question_id,$user_id){
		
		$query = $this->db->query("select answer_id from users_answer where user_id = $user_id AND question_id = $question_id ");
		$res = $query->result_array();
		if(!empty($res)){
			return $res[0];
		}else return null;
	}   
   
   
	function get_my_feed($user_id,$auth_token,$limit,$offset,$filter_value,$bucket_id,$timestamp,$is_up){
		
		$budget_id_query = '';
		$question_id = '7';
		$CI =& get_instance();
		$CI->load->model('Pa_model');
		if($this->Pa_model->get_gender($user_id) == 2){
			 $question_id = '18';
		}
		$budget_id = $this->get_answer_id2($question_id,$user_id);
		if(!empty($budget_id)){
			if($question_id == '18'){
				if($budget_id['answer_id'] == '4688')
				{
					$budget_id_query = $budget_id_query.' AND m.budget = '.$budget_id['answer_id'];
				}else if($budget_id['answer_id'] == '4689'){
					$budget_id_query = $budget_id_query.' AND m.budget in (4688,4689) ';
				}else if($budget_id['answer_id'] == '4690'){
					$budget_id_query = $budget_id_query.' AND m.budget in (4688,4689,4690) ';
				}else if($budget_id['answer_id'] == '4691'){
					$budget_id_query = $budget_id_query.' AND m.budget in (4688,4689,4690,4691) ';
				}
			}else if($question_id == '7'){
				if($budget_id['answer_id'] == '36')
				{
					$budget_id_query = $budget_id_query.' AND m.budget = '.$budget_id['answer_id'];
				}else if($budget_id['answer_id'] == '37'){
					$budget_id_query = $budget_id_query.' AND m.budget in (36,37) ';
				}else if($budget_id['answer_id'] == '38'){
					$budget_id_query = $budget_id_query.' AND m.budget in (36,37,38) ';
				}else if($budget_id['answer_id'] == '4670'){
					$budget_id_query = $budget_id_query.' AND m.budget in (36,37,38,4670) ';
				}
			}
		}else {
			$budget_id_query = '';
		}
		if($is_up == 1){
			
			if($timestamp === 0){
				$time_stamp_look = '';
				$time_stamp_look_new = " ";
			}
			  
		    else{ 
			 $time_stamp_look = "AND a.created_datetime>$timestamp ";
			  $time_stamp_look_new = " and a.modified_datetime>$timestamp ";
			}
		}  
		else {
			if($timestamp === 0){
			  $time_stamp_look = '';
			  $time_stamp_look_new = '';
			}
		    else{
			 $time_stamp_look = "AND a.created_datetime<$timestamp ";
			  $time_stamp_look_new = " and a.modified_datetime<$timestamp ";
			}
		}
		
		if($filter_value == 6){

			$query = $this->db->query("select z.id, z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime as timestamp,@type:='look' as type  from 
			(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 $time_stamp_look_new and a.id IN ((select distinct j.look_id from look_category_mapping as j,look_bucket_mapping as m where j.look_id = m.look_id and j.cat_id = 5 and m.bucket_id=$bucket_id $budget_id_query ) )) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc $limit $offset");

			$res = $query->result_array();

		}else if($filter_value == 7){
			$query = $this->db->query("select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,@type:='look' as type,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime as timestamp from (SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 and a.id IN ((select distinct j.look_id from look_category_mapping as j,look_bucket_mapping as m where j.look_id = m.look_id and j.cat_id = 40 and m.bucket_id=$bucket_id $budget_id_query ) )) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc
			$limit $offset");

			$res = $query->result_array();
		}
		else{
			$filter_query = '';
			if($filter_value >= 1){ $filter_query = $filter_query." and a.look_type = '".$filter_value."' " ; }
			$query = $this->db->query("select distinct k.id, k.slug,product_img,k.look_type,k.image,look_image,name,k.created_datetime,k.modified_datetime as timestamp,k.status,k.deflag,k.stylist_id,@type:='look' as type from
			(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag,z.`stylist_id` from 
			(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag,a.`stylist_id` FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 $time_stamp_look_new $filter_query) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc) as k,look_bucket_mapping as m where k.id = m.look_id and m.bucket_id = '".$bucket_id."' $budget_id_query order by k.modified_datetime desc $limit $offset");
			
			$res = $query->result_array();
		}
		
		$looks_array = $this->looks_array($res);
		return $looks_array;
		
	}
	
	function view_all_looks($limit,$offset,$filter_value,$timestamp,$is_up,$look_for){
		if($is_up == 1){
			
			if($timestamp === 0){
				$time_stamp_look_new = " AND a.looks_for=$look_for ";
			}
			  
		    else{ 
			  $time_stamp_look_new = " AND a.modified_datetime>$timestamp AND a.looks_for=$look_for ";
			}
		}  
		else {
			if($timestamp === 0){
			  $time_stamp_look_new = " AND a.looks_for=$look_for ";
			}
		    else{
			  $time_stamp_look_new = " AND a.modified_datetime<$timestamp AND a.looks_for=$look_for ";
			}
		}
		if($filter_value === '0'){
			$query = $this->db->query("select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime as timestamp,z.status,z.deflag,@type:='look' as type from 
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 $time_stamp_look_new ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime DESC $limit $offset");
				$res = $query->result_array();
		}else if($filter_value == 6){

				$query = $this->db->query("select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,@type:='look' as type ,z.modified_datetime as timestamp from 
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 $time_stamp_look_new and a.id IN ((select distinct j.look_id from look_category_mapping as j,look_bucket_mapping as u where j.look_id = u.look_id and j.cat_id = 5) )) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc $limit $offset ");

				$res = $query->result_array();
			   
				}else if($filter_value == 7){
				$query = $this->db->query("select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,@type:='look' as type,z.modified_datetime  as timestamp from 
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 $time_stamp_look_new and a.id IN ((select distinct look_id from look_category_mapping where cat_id = 40)) ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4) order by z.modified_datetime desc $limit $offset");

				$res = $query->result_array();
			   
				}
				else if($filter_value != 0){
		$query = $this->db->query("select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime as timestamp,z.status,z.deflag,@type:='look' as type from 
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 $time_stamp_look_new and a.look_type = $filter_value ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime DESC $limit $offset");

					$res = $query->result_array();
				}
			$looks_array = $this->looks_array($res);
			return $looks_array;
	}
	
	function get_wishlist($look_id,$user_id){
		$query = $this->db->query("select COUNT(look_fav.look_id) as wishlisted_looks from look_fav where look_fav.look_id='".$look_id."' AND look_fav.user_id='".$user_id."' ");
		$res = $query->result_array();
		return $res[0]['wishlisted_looks'];
		
	}
	
	function get_looks_by_brand($brand_id,$limit,$offset){
            //$query = $this->db->query("select product_desc.brand_id, a.id as id,a.name as name,CONCAT('http://www.stylecracker.com/sc_admin/assets/looks/',a.image) as url,a.created_datetime as timestamp,@type:='look' as type,(select COUNT(look_fav.look_id) from look_fav where look_fav.look_id=a.id) as user_likes,(select COUNT(user_share.look_id) from user_share where user_share.look_id=a.id) as user_shares,(select ls.category_name  from look_category_mapping lcm, look_sub_category ls   WHERE a.id = lcm.look_id and lcm.cat_id=ls.id) as look_type_txt from `look_products` inner join look a ON a.id=look_products.look_id JOIN product_desc  ON look_products.product_id = product_desc.id  WHERE product_desc.brand_id= ".$brand_id." AND a.status=1 order by a.modified_datetime desc ".$limit.$offset);
			$query = $this->db->query("select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime as timestamp,@type:='look' as type from 
 (SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN (select distinct u.`look_id` from `look_products` as u,`product_desc` as i where u.`product_id` = i.`id` and i.`brand_id` = $brand_id )) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4|| z.look_type = 6) order by z.modified_datetime DESC $limit $offset");

			//$query = $this->db->query("select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime as timestamp,z.status,z.deflag,@type:='look' as type from 
			//(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime DESC $limit $offset");

           $res = $query->result_array();
		   
		   $looks_array = array();
			
			$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
					$alllooksarrayobject = new stdClass(); 
					$alllooksarrayobject->id = $val['id'];
					$alllooksarrayobject->name = $val['name'];
					$alllooksarrayobject->timestamp = $val['timestamp'];
					if($val['look_type'] == 1){
						$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
					}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
						$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
					}else if($val['look_type'] == 3){
						$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
					}
					$info = getimagesize($alllooksarrayobject->url);
					//if($info[1] == NULL || $info[1] == '' ) $info[1] = 0;
					//if($info[0] == NULL || $info[0] == '' ) $info[0] = 0;
					$alllooksarrayobject->height = $info[1];
					$alllooksarrayobject->width  = $info[0];
					$alllooksarrayobject->type = $val['type'];
					$alllooksarrayobject->look_type = $val['look_type'];
					$alllooksarrayobject->user_share = $this->get_user_share($val['id']);
					$alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
					$alllooksarrayobject->is_discount = (string)$this->get_discount($val['id']);
					$alllooksarrayobject->share_url = 'https://www.stylecracker.com/looks/look-details/'.$val['slug'];
					$alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
					$looks_array[$i] = $alllooksarrayobject;
					$i++;
				}
			}
          return $looks_array;
		
		
	}
	
	
	function get_discover($filter_value,$timestamp,$limit,$offset,$is_up,$look_for){
	
		//$y = 5;
		$str =  $limit;
		$a = explode(" ",$str);
		
		//$look_limit = floor(0.8*$a[2]);
		//$blog_limit = floor(0.2*$a[2]);
		$blog_limit = floor($a[2]/5);
		$look_limit = $a[2]-$blog_limit;
		//echo $look_limit.$blog_limit;exit; 
		if($is_up == 1){
			
			if($timestamp === 0){
				$time_stamp_look = 'WHERE status=1';
				$time_stamp_look_new = " AND a.looks_for=$look_for ";
				$time_stamp_blog = " WHERE a.post_status = 'publish' and a.post_type = 'post' ";
			}
			  
		    else{ 
			 $time_stamp_look = " WHERE a.created_datetime>$timestamp AND status=1";
			 $time_stamp_look_new = " and a.modified_datetime>$timestamp AND a.looks_for=$look_for ";
			 $time_stamp_blog = " WHERE a.post_date>$timestamp and a.post_status = 'publish' and a.post_type = 'post' ";
			}
		}  
		else {
			if($timestamp === 0){
			  $time_stamp_look = 'WHERE status=1';
			  $time_stamp_look_new = " AND a.looks_for=$look_for ";
			  $time_stamp_blog = " WHERE a.post_status = 'publish' and a.post_type = 'post' ";
			}
		    else{
			 $time_stamp_look = " WHERE a.created_datetime<$timestamp AND status=1";
			 $time_stamp_look_new = " and a.modified_datetime<$timestamp AND a.looks_for=$look_for  ";
			 $time_stamp_blog = " WHERE a.post_date<$timestamp and a.post_status = 'publish' and a.post_type = 'post' ";
			}
		}
		
		if($filter_value === '0'){
			/*All types of Looks related query*/	
			/* Added by Saylee */
			$query = $this->db->query("select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime as timestamp,z.status,z.deflag,@type:='look' as type from 
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 $time_stamp_look_new ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime DESC LIMIT $look_limit $offset");
				
			/*Blog related query*/
			$query1 = $this->db->query("select a.id as id,a.post_title as name,(select b.guid from stylecra_stylecracker_v1_blogbeta.`wp_postmeta` as a,stylecra_stylecracker_v1_blogbeta.wp_posts as b where a.`meta_key` = '_thumbnail_id' and a.`meta_value` = b.`ID` and a.`post_id`=a.id limit 0,1)as url,CONCAT('https://www.stylecracker.com/blog/',a.post_name) as blog_url,a.post_date as timestamp,@type:='blog' as type from stylecra_stylecracker_v1_blogbeta.wp_posts a $time_stamp_blog order by a.post_date desc LIMIT $blog_limit $offset ");			
			
			$data = array();
			/* Added by saylee*/
			$res = $query->result_array();
			
			$looks_array = array();
			
			$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
					$alllooksarrayobject = new stdClass(); 
					$alllooksarrayobject->id = $val['id'];
					$alllooksarrayobject->name = $val['name'];
					$alllooksarrayobject->timestamp = $val['timestamp'];
					if($val['look_type'] == 1){
						$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
					}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
						$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
					}else if($val['look_type'] == 3){
						$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
					}
					$info = getimagesize($alllooksarrayobject->url);
					//if($info[1] == NULL || $info[1] == '' ) $info[1] = 0;
					//if($info[0] == NULL || $info[0] == '' ) $info[0] = 0;
					$alllooksarrayobject->height = $info[1];
					$alllooksarrayobject->width  = $info[0];
					$alllooksarrayobject->type = $val['type'];
					$alllooksarrayobject->look_type = $val['look_type'];
					$alllooksarrayobject->user_share = $this->get_user_share($val['id']);
					$alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
					$alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
					$alllooksarrayobject->share_url = 'https://www.stylecracker.com/looks/look-details/'.$val['slug'];
					$alllooksarrayobject->is_discount = (string)$this->get_discount($val['id']);
					$looks_array[$i] = $alllooksarrayobject;
					$i++;
				}
			}
	
			
			$res1 = $query1->result_array();
			
			$blogs_array = array();
			
			$i = 0; 
			
			if(!empty($res1)){
				foreach($res1 as $val){
			
					$allblogsarrayobject = new stdClass(); 
					$allblogsarrayobject->id = $val['id'];
					$allblogsarrayobject->name = $val['name'];
					
					$allblogsarrayobject->url  = $val['url'];
					$allblogsarrayobject->blog_url  = $val['blog_url'];
					$allblogsarrayobject->share_url = $val['blog_url'];
					$allblogsarrayobject->timestamp = $val['timestamp'];
					//echo $allblogsarrayobject->url.'test';
					//exit;
					if(isset($allblogsarrayobject->url)){
					$info = getimagesize($allblogsarrayobject->url);
					//if($info[1] == NULL || $info[1] == '' ) $info[1] = 0;
					//if($info[0] == NULL || $info[0] == '' ) $info[0] = 0;
					$allblogsarrayobject->height = $info[1];
					$allblogsarrayobject->width  = $info[0];
					}
					$allblogsarrayobject->type = $val['type'];
					
					$blogs_array[$i] = $allblogsarrayobject;
					$i++;
				}
			}

			$a = count($looks_array);
			$b = count($blogs_array);

			if($looks_array != NULL) $lookArray = floor($a/2); else $lookArray = '';
			if($blogs_array != NULL) $blogArray = floor($b/2); else $blogArray = '';
			if($a >0){
				for($i=0;$i<$lookArray;$i++){
					$lookArraySplit[] = $looks_array[$i];
				}
			}
			if($b >0){
				for($i=0;$i<$blogArray;$i++){
					$lookArraySplit[] = $blogs_array[$i];
				}
			}
			if($a >0){
				for($i=($lookArray);$i<$a;$i++){
					$lookArraySplit[] = $looks_array[$i];
				}
			}
			if($b >0){
				for($i=($blogArray);$i<$b;$i++){
					$lookArraySplit[] = $blogs_array[$i];
				}
			}
			// $looks[0][$a-1]= $looks_array;
			// $blogs[0][$b-1]= $blogs_array;
			// print_r($looks);
			// print_r($blogs);exit;
			// echo $c.'++++++'.$d; exit;
			// $res[0][1]= $looks[0][$c-1];
			// $res[0][2] = $blogs[0][$d-1];
			// $res[0][3] = $looks[$c][$a-1];
			// $res[0][4] = $blogs[$d][$b-1];
			// print_r($res);
			//$data1 =array_merge($looks_array,$blogs_array);
			return $lookArraySplit;
			
		}
		
		else if($filter_value === '5'){
			$query = $this->db->query("select a.id as id,a.post_title as name,(select b.guid from stylecra_stylecracker_v1_blogbeta.`wp_postmeta` as a,stylecra_stylecracker_v1_blogbeta.wp_posts as b where a.`meta_key` = '_thumbnail_id' and a.`meta_value` = b.`ID` and a.`post_id`=a.id limit  0,1)as url,CONCAT('http://www.stylecracker.com/blog/',a.post_name) as blog_url,a.post_date as timestamp,@type:='blog' as type from stylecra_stylecracker_v1_blogbeta.wp_posts a $time_stamp_blog order by a.post_date desc $limit $offset ");
			$res = $query->result_array();
			
			$blogs_array = array();
			
			$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
			
					$allblogsarrayobject = new stdClass(); 
					$allblogsarrayobject->id = $val['id'];
					$allblogsarrayobject->name = $val['name'];
					
					$allblogsarrayobject->url  = $val['url'];
					$allblogsarrayobject->blog_url  = $val['blog_url'];
					$allblogsarrayobject->timestamp = $val['timestamp'];
					//$alllooksarrayobject->share_url = $val['blog_url'];
					if(isset($allblogsarrayobject->url)){
					$info = getimagesize($allblogsarrayobject->url);
					$allblogsarrayobject->height = $info[1];
					$allblogsarrayobject->width  = $info[0];
					
					}
					$allblogsarrayobject->type = $val['type'];
					
					$blogs_array[$i] = $allblogsarrayobject;
					$i++;
				}
			}
			
			return $blogs_array;
		}
		
		else if($filter_value == 6){

        $query = $this->db->query("select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,@type:='look' as type ,z.modified_datetime as timestamp from 
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN ((select distinct j.look_id from look_category_mapping as j,look_bucket_mapping as u where j.look_id = u.look_id and j.cat_id = 5) )) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc $limit $offset ");

        $res = $query->result_array();
		
		$looks_array = $this->looks_array($res);
		return $looks_array;
       
        }else if($filter_value == 8){

        $query = $this->db->query("select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,@type:='look' as type,z.modified_datetime as timestamp from 
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN ((select distinct lp.look_id from discount_availed as da,look_products as lp,product_desc as pd where NOW() >= da.start_date AND NOW() <= da.end_date AND lp.product_id = pd.id  AND da.status=1 and (da.product_id = lp.product_id))) ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4) order by z.modified_datetime desc $limit $offset ");

        $res = $query->result_array();
		
		$looks_array = $this->looks_array($res);
		return $looks_array;
       
        }
		else if($filter_value == 7){

        $query = $this->db->query("select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,@type:='look' as type,z.modified_datetime  as timestamp from 
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN ((select distinct look_id from look_category_mapping where cat_id = 40)) ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4) order by z.modified_datetime desc $limit $offset ");

        $res = $query->result_array();
		$looks_array = $this->looks_array($res);
		return $looks_array;
       
        }
		
		//sample datetime = 2014-07-02 19:10:19 
		else if($filter_value != '0'){
			//echo "select a.id as id,a.name as name,CONCAT('http://www.stylecracker.com/sc_admin/assets/looks/',a.image) as url,a.created_datetime as timestamp,@type:='look' as type,(select COUNT(look_fav.look_id) from look_fav where look_fav.look_id=a.id) as user_likes,(select COUNT(user_share.look_id) from user_share where user_share.look_id=a.id) as user_shares,(select ls.category_name  from look_category_mapping lcm, look_sub_category ls   WHERE a.id = lcm.look_id and lcm.cat_id=ls.id) as look_type_txt from look a  $time_stamp_look ORDER BY a.created_datetime DESC $query_limit $query_offset";
			/*$query = $this->db->query("select a.id as id,a.name as name,CONCAT('http://www.stylecracker.com/sc_admin/assets/looks/',a.image) as url,a.created_datetime as timestamp,@type:='look' as type,(select COUNT(look_fav.look_id) from look_fav where look_fav.look_id=a.id) as user_likes,(select COUNT(user_share.look_id) from user_share where user_share.look_id=a.id) as user_shares,(select ls.category_name  from look_category_mapping lcm, look_sub_category ls   WHERE a.id = lcm.look_id and lcm.cat_id=ls.id limit 0,1) as look_type_txt from look a  $time_stamp_look ORDER BY a.created_datetime DESC $limit $offset ");*/
			
			$query = $this->db->query("select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime as timestamp,z.status,z.deflag,@type:='look' as type from 
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 $time_stamp_look_new and a.look_type = $filter_value ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime DESC $limit $offset");

			//$res = $query->result();  
			
			/* Added by saylee*/
			$res = $query->result_array();
			
			$looks_array = $this->looks_array($res);
			return $looks_array;
		}
		
		
	
	
	}
	
	function looks_array($res){
		
		$looks_array = array();
			
			$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
					$alllooksarrayobject = new stdClass(); 
					$alllooksarrayobject->id = $val['id'];
					$alllooksarrayobject->name = $val['name'];
					$alllooksarrayobject->timestamp = $val['timestamp'];
					if($val['look_type'] == 1){
						$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
					}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
						$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
					}else if($val['look_type'] == 3){
						$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
					}
					/*  hardcoded height and width bcoz API working slow
					$info = getimagesize($alllooksarrayobject->url);
					$alllooksarrayobject->height = $info[1];
					$alllooksarrayobject->width  = $info[0];
					*/
					$alllooksarrayobject->height = 530;
					$alllooksarrayobject->width  = 530;
					$alllooksarrayobject->type = $val['type'];
					$alllooksarrayobject->look_type = $val['look_type'];
					// $alllooksarrayobject->user_share = $this->get_user_share($val['id']);
					// $alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
					// $alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
					$alllooksarrayobject->user_share = "5";
					$alllooksarrayobject->user_likes = "13";
					$alllooksarrayobject->look_type_txt = "Cocktail";
					$alllooksarrayobject->share_url = 'https://www.stylecracker.com/looks/look-details/'.$val['slug'];
					$alllooksarrayobject->is_discount = (string)$this->get_discount($val['id']);
					$looks_array[$i] = $alllooksarrayobject;
					$i++;
				}
			}
			
			return $looks_array;
		
	}
	
	function show_recommended_looks($bucket){
        if($bucket > 0){
            $where_condition = ' and m.bucket_id='.$bucket;
            $query = $this->db->query('select distinct k.id,k.slug,product_img,k.look_type,k.image,look_image,name,k.created_datetime,k.modified_datetime as timestamp,k.status,k.deflag,k.stylist_id,@type:="look" as type from
(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag,z.`stylist_id` from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag,a.`stylist_id` FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc) as k,look_bucket_mapping as m where k.id = m.look_id '.$where_condition.' order by k.modified_datetime desc limit 0,3');

		$res = $query->result_array();
		$looks_array = $this->looks_array($res);
		return $looks_array;

        }
    }
	
	// added for checking discount on look
	function get_discount($look_id){
		$query = $this->db->query("select a.look_id,a.product_id,c.user_id as brand_id from look_products as a,product_desc as b,brand_info as c where a.look_id= $look_id AND a.product_id = b.id AND b.brand_id = c.user_id");
		$res = $query->result_array();
		$is_discount = 0;
		$i = 0;
		if(!empty($res)){
			foreach($res as $val){
				$discount = $this->get_discount_id($val['brand_id'],$val['product_id']);
				//print_r($discount);exit;
				if(!empty($discount)){
					$is_discount = 1;
				}//else if($is_discount == 1) $is_discount = 1;
				$i++;
			}
		}
		if($is_discount == 1){ return 1; } else return 0;
	}

	function get_discount_id($brand_id,$product_id){
          $query = $this->db->query("select id, discount_type_id from discount_availed as da where (da.brand_id = '".$brand_id."' OR da.product_id = '".$product_id."' ) AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1 ");
		  $result = $query->result_array();
		  if(!empty($result)) return $result; else return null;
	}
	
	/* Added by saylee start*/
	function get_user_share($look_id){
		$query = $this->db->query("select COUNT(user_share.look_id) as user_share from user_share where user_share.look_id='".$look_id."'");
		$res = $query->result_array();
		return $res[0]['user_share'];
		
	}
	
	function get_user_likes($look_id){
		$query = $this->db->query("select COUNT(look_fav.look_id) as look_fav from look_fav where look_fav.look_id='".$look_id."'");
		$res = $query->result_array();
		return $res[0]['look_fav'];
		
	}
	
	function get_sub_category($look_id){
		$query = $this->db->query("select ls.category_name from look_category_mapping lcm,look_sub_category ls WHERE ls.id = lcm.cat_id and lcm.look_id='".$look_id."' limit 0,1");
		$res = $query->result_array(); 
		return isset($res[0]['category_name']) ? $res[0]['category_name'] : '';
		
	}
	
	/* Added by saylee end */
	
	function get_share($user_id,$auth_token,$look_id,$action){
		
		$created_datetime = date('Y-m-d G:i:s'); 
		$data = array('user_id'=>$user_id,'look_id'=>$look_id,'medium'=>$action,'created_by'=>$user_id,'created_datetime'=>$created_datetime);
		$result=$this->db->insert('user_share',$data); 
		  if(!empty($result)){
           return 1;
       }else{
           return 2;
       }
        
	}

    function getStylistInfo(){
		$query = $this->db->query("select id,stylist_name,stylist_bio,CONCAT('http://www.stylecracker.com/assets/images/about/',img_name) as img_name,CONCAT('http://www.stylecracker.com/assets/images/about/',full_img) as full_img,slug,@type:='https://www.facebook.com/StyleCracker' as facebook_link,@type:='https://twitter.com/Style_Cracker' as twitter_link,@type:='https://instagram.com/stylecracker' as insta_link,style_tip,CONCAT('http://www.stylecracker.com/assets/images/about/mobile/',full_img) as stylist_cover_img from stylist where status=1");
        $res = $query->result();
        return $res;
       /* $query = $this->db->get_where('stylist', array('status' => 1));
        $result = $query->result();
        if(!empty($result)){

            return $result;
        }*/
    }

    function getSingleStylistInfo($stylist_id){
		//$query = $this->db->query("select id,stylist_name,stylist_bio,CONCAT('http://www.stylecracker.com/assets/images/about/',img_name) as img_name,CONCAT('http://www.stylecracker.com/assets/images/about/',full_img) as full_img,slug,facebook_link,twitter_link,insta_link,style_tip from stylist where status=1 AND id='$stylist_id'");
        //$res['stylist_info'] = $query->result();
		
		$query = $this->db->query("select a.id as id,a.name as name,CONCAT('http://www.stylecracker.com/sc_admin/assets/looks/',a.image) as url,a.created_datetime as timestamp,@type:='look' as type,(select count(look_id) from look_fav where `look_id`=a.id) as look_fav,(select count(look_id) from user_share where `look_id`=a.id) as look_share,(select ls.category_name  from look_category_mapping lcm, look_sub_category ls   WHERE a.stylist_id=$stylist_id AND a.id = lcm.look_id and lcm.cat_id=ls.id LIMIT 0,1) as look_type_txt from look as a  where a.stylist_id=$stylist_id");
        $res = $query->result();
		
        return $res;
        /*
		(select a.id as id,a.name as name,CONCAT('http://www.stylecracker.com/sc_admin/assets/looks/',a.image) as url,a.created_datetime as timestamp,@type:='look' as type,(select count(look_id) from look_fav where `look_id`=a.id) as look_fav,(select count(look_id) from user_share where `look_id`=a.id) as look_share,(select ls.category_name  from look_category_mapping lcm, look_sub_category ls   WHERE a.stylist_id=$stylist_id AND a.id = lcm.look_id and lcm.cat_id=ls.id) as look_type_txt from look as a  where a.stylist_id=$stylist_id) as stylist_looks
		
		
		$query = $this->db->get_where('stylist', array('status' => 1,'slug'=>$stylist_name));
        $res = $query->result_array();
        return $res[0];*/
    }

    function change_password($user_id,$oldPassword,$newPassword){
        $query = $this->db->get_where('user_login', array('id' => $user_id,'status'=>1));
        $result = $query->result();
        if(!empty($result)){
            $salt = substr($result[0]->password, 0, 10);
            $db_password =  $salt . substr(sha1($salt . $oldPassword), 0, -10); 
            if($db_password === $result[0]->password){
                //update password field 
                $salt = substr(md5(uniqid(rand(), true)), 0, 10);
                $password   =$salt . substr(sha1($salt . $newPassword), 0, -10);
                $data = array('password'=>$password );
                $this->db->update('user_login', $data, array('id' => $user_id ));  
                return 1;
            }else{
                return 2;
            }
        }
    }
	
	function get_brands($limit,$offset){
		$query = $this->db->query("select * from brand_info $limit $offset ");
        $res = $query->result();
        return $res;
		
	}
	
	function get_brand_all_names(){
        //select b.user_id as brand_id,b.company_name,b.short_description,b.long_description,IF( b.email IS NULL ,'NA', b.email ) AS email,IF( b.overall_rating IS NULL , 0, 1 ) AS overall_rating,CONCAT('http://www.stylecracker.com/sc_admin/assets/brand_logo/',b.logo) as logo_url,IF( b.contact IS NULL ,'NA', b.contact ) AS contact, (select bi.review_text from brand_review bi where bi.brand_id=b.user_id AND status=1) AS review_text  from brand_info b where b.`show_in brand_list`=1
		$query = $this->db->query("select IF(cover_image ='','http://www.stylecracker.com/assets/images/brands/brand-cover-photo.jpg',CONCAT('http://www.stylecracker.com/assets/images/brands/',cover_image)) as cover_image from brand_info where is_paid=1 AND cover_image != 'brand-cover-photo.jpg'  ");	
		$res['cover_images'] = $query->result();
		
		$query2 = $this->db->query("select user_id as brand_id,company_name,IF( short_description IS NULL ,'', short_description ) as long_description,IF( long_description IS NULL ,'', long_description ) as short_description,IF( email IS NULL ,'NA', email ) AS email,IF( overall_rating IS NULL , 0, 1 ) AS overall_rating,IF(logo IS NULL,'https://www.stylecracker.com/assets/images/brands/brand-logo-default.png',CONCAT('http://www.stylecracker.com/sc_admin/assets/brand_logo/',logo)) as logo_url,IF( contact IS NULL ,'NA', contact ) AS contact,(select COUNT(brand_review.review_text)  from brand_review where brand_review.brand_id = brand_info.user_id AND brand_review.status=1 limit 0,1) as user_review,(select COUNT(brand_review.ratings) from brand_review where brand_review.brand_id = brand_info.user_id AND brand_review.status=1 limit 0,1) as user_ratings  from brand_info where `show_in brand_list`=1");
		
		
        $res2 = $query2->result_array();
		$looks_array = array();
			
			$i = 0; 
			
			if(!empty($res2)){
				foreach($res2 as $val){
					
					$allbrandarrayobject = new stdClass(); 
					$allbrandarrayobject->brand_id = $val['brand_id'];
					$allbrandarrayobject->company_name = $val['company_name'];
					$allbrandarrayobject->short_description = $val['short_description'];
				
					$allbrandarrayobject->long_description = $val['long_description'];
					$allbrandarrayobject->email = $val['email'];
					$query_count = $this->db->query("select ratings as ratings from brand_review where brand_id = '".$val['brand_id']."' AND status=1");
        $res_count = $query_count->result_array();

        $query_rating = $this->db->query("select sum(z.ratings) as ratings from(select (count(ratings)*ratings) as ratings from brand_review where brand_id = '".$val['brand_id']."' AND status=1 group by ratings) as z");
        $res_avg = $query_rating->result_array();
					if(!empty($res_avg)){
					if(sizeof($res_count) != 0){
					$allbrandarrayobject->overall_rating = $res_avg[0]['ratings']/sizeof($res_count);
					}else{
					$allbrandarrayobject->overall_rating = 0;
					}
					}else{
					$allbrandarrayobject->overall_rating = 0;
					}
					
					$allbrandarrayobject->logo_url = $val['logo_url'];
					$allbrandarrayobject->contact = $val['contact'];
					$allbrandarrayobject->user_review = $val['user_review'];
					$allbrandarrayobject->user_ratings = $val['user_ratings'];
					$looks_array[$i] = $allbrandarrayobject;
					$i++;
				} 
		}
		
		
		
		
		$res['brand_info']= $looks_array;
		
        return $res;
	}
	
	function get_user_review($user_id,$auth_token,$brand_id){
		$query = $this->db->query("select brand_id,user_id,review_text from brand_review where brand_id=".$brand_id." and user_id=".$user_id." AND status=1 ");
        $res = $query->result();
		if($res != NULL){
			$res = $this->get_all_brand_mobile($brand_id ,$user_id);
			return $res;
        }else{
		    return 2;	
		}
        return 2; 			
        
		
	}

    function brands_review_mobile($user_id,$brand_id,$review_text,$ratings){
		
        $created_datetime = date('Y-m-d G:i:s'); 
        $data = array('user_id'=>$user_id,'brand_id'=>$brand_id,'review_text'=>$review_text, 'created_by'=>$user_id, 'ratings'=>$ratings,'created_datetime'=>$created_datetime);
        $result=$this->db->insert('brand_review',$data); 
		
        if(!empty($result)){
			//echo "select * from brand_review where  user_id = $user_id AND brand_id = $brand_id AND review_text = $review_text" ;exit;
			$query = $this->db->query("select a.*,b.first_name from brand_review as a,user_info as b where  a.user_id = $user_id AND a.brand_id = $brand_id AND  review_text LIKE '$review_text' AND b.user_id = $user_id AND a.status=1");	
			$res = $query->result();
			//print_r($res);exit;
           //$res = $this->get_all_brand_mobile($brand_id ,$user_id);
		   if($res != NULL ) return $res; else return 2;
       }else{
           return 2;
       }
    }	
    
	 function get_all_brand_mobile($brand_id ,$user_id=0){
		if($user_id != 0) $user_query = " AND b.user_id=$user_id"; else $user_query = '';
		//echo "select b.*, u.first_name, IF(u.profile_pic='', 'http://www.stylecracker.com/assets/images/profile/profile.png', CONCAT('http://www.stylecracker.com/assets/images/profile/',u.profile_pic)) as profile_pic from brand_review b INNER JOIN user_info u  ON u.user_id=b.user_id AND b.brand_id=$brand_id AND b.status=1 $user_query ORDER BY b.brand_id DESC ";
		$query = $this->db->query("select b.*, u.first_name, IF(u.profile_pic='', 'http://www.stylecracker.com/assets/images/profile/profile.png', CONCAT('http://www.stylecracker.com/assets/images/profile/',u.profile_pic)) as profile_pic from brand_review b INNER JOIN user_info u  ON u.user_id=b.user_id AND b.brand_id=$brand_id AND b.status=1 $user_query ORDER BY b.brand_id DESC ");
		//$res = $query->result();
        //$query = $this->db->get_where('brand_review', array('brand_id' => $brand_id));
        $result = $query->result_array();
        if(!empty($result)){
            return $result;
        }else{
            return NULL;
        }
    }	
	
	function get_review($brand_id ,$user_id=0){
		if($user_id != 0) $user_query = " AND b.user_id=$user_id"; else $user_query = '';
		
		$query = $this->db->query("select b.*, u.first_name, IF(u.profile_pic='', 'http://www.stylecracker.com/assets/images/profile/profile.png', CONCAT('http://www.stylecracker.com/assets/images/profile/',u.profile_pic)) as profile_pic from brand_review b INNER JOIN user_info u  ON u.user_id=b.user_id AND b.brand_id=$brand_id AND b.status=1 $user_query ORDER BY b.ratings DESC limit 2");
        $result = $query->result_array();
        if(!empty($result)){
            return $result;
        }else{
            return NULL;
        }
    }
	
    function add_contact_us($data){
		if(!empty($data)){
			$result = $this->db->insert('user_enquiry', $data);
			return $result;
		
		}
    }
	
	public function randomPassword_mobile() {
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
  }
  
  function update_profile_img($filename,$user_id){
        
        if($user_id!='' && $filename!=''){
            $data = array('profile_pic'=>$filename,'modified_by'=>$user_id);
            $this->db->update('user_info', $data, array('user_id' => $user_id ));
			
            return true;
        }
    }
	
	
	function update_cover_img($filename,$user_id){
     
        if($user_id!='' && $filename!=''){
            $data = array('profile_cover_pic'=>$filename,'modified_by'=>$user_id);
            $this->db->update('user_info', $data, array('user_id' => $user_id ));
            return true;
        }
    }
	
	function get_profile_img($user_id){
        
        if($user_id!=''){
            $query = $this->db->query("select IF(a.profile_pic='', IF(a.gender = 1,'https://www.stylecracker.com/assets/images/profile/profile-women.jpg','https://www.stylecracker.com/assets/images/profile/profile-men.jpg'), CONCAT('http://www.scnest.com/sc_admin/assets/user_profiles/',a.profile_pic)) as profile_pic from user_info as a where a.user_id = $user_id ");
			$res = $query->result_array();
			
            return $res;
        }
    }
	
	function get_cover_img($user_id){
     
        if($user_id!=''){
             $query = $this->db->query("select IF(profile_cover_pic='', 'http://www.stylecracker.com/sc_admin/assets/full_pic/', CONCAT('http://www.stylecracker.com/sc_admin/assets/full_pic/',profile_cover_pic)) as profile_cover_pic from user_info where user_id = $user_id ");
			$res = $query->result_array();
			
            return $res;
        }
    }
	
	 function upload_wordrobe_img($filename,$user_id,$tags,$product_name){
		// $explode_data = explode(',',$tags);
		 // echo "hello";
		// print_r($explode_data);exit;

        if($user_id!='' && $filename!=''){
           $data = array(
                    'is_active' => 1 ,
                    'image_name' => $filename ,
                    'created_by' => $user_id,
                    'created_datetime' => $this->config->item('sc_date'),
                    'user_id' => $user_id,
					'modified_by' => $user_id
            );
		
		
        $this->db->insert('users_wardrobe', $data);
		$wordrobe_id = $this->db->insert_id();
		//echo $wordrobe_id;exit;
		$data = array(
					'tag_user_id' => $user_id,
					'tag_ward_id' => $wordrobe_id ,
					 'tag_values' => $tags,
                    'tag_product_name' => $product_name,  
					'status' => 1 ,					
                    'created_datetime' => $this->config->item('sc_date')
                    
            );
		$this->db->insert('user_wardrobe_tags', $data);
		//echo $this->db->last_query();
		// exit;
		
		$query = $this->db->query("select a.id,CONCAT('http://www.stylecracker.com/sc_admin/assets/wardrobes/',a.image_name) as image_name,b.tag_values as tags,b.tag_product_name as product_name from users_wardrobe as a,user_wardrobe_tags as b where a.id = $wordrobe_id and a.is_active = 1 AND b.tag_user_id = $user_id AND tag_ward_id = $wordrobe_id order by id desc");
        $result = $query->result();
		// print_r($result);exit;
		return $result[0];				
        }	
    }
	
	
	function get_wardrobe_img($user_id){
		
       // $query = $this->db->query("select a.id,CONCAT('http://www.stylecracker.com/sc_admin/assets/wardrobes/',a.image_name) as image_name,b.tag_values as tags,b.tag_product_name as product_name from users_wardrobe as a,user_wardrobe_tags as b where a.user_id = $user_id and b.tag_user_id = $user_id AND  a.is_active = 1 order by id desc");
	   // $query = $this->db->query("select a.id,CONCAT('http://www.stylecracker.com/sc_admin/assets/wardrobes/',a.image_name) as image_name,b.tag_values as tags,b.tag_product_name as product_name from users_wardrobe as a INNER JOIN user_wardrobe_tags as b ON a.user_id = b.tag_user_id where b.tag_user_id = $user_id  order by id desc limit 20");
	    $query = $this->db->query("select a.id,CONCAT('http://www.stylecracker.com/sc_admin/assets/wardrobes/',a.image_name) as image_name,b.tag_values as tags,b.tag_product_name as product_name from users_wardrobe as a INNER JOIN user_wardrobe_tags as b ON a.id=b.tag_ward_id AND a.user_id = $user_id and a.is_active = 1 AND b.tag_user_id = $user_id order by id desc limit 20");	   
        //select id,CONCAT('http://www.stylecracker.com/sc_admin/assets/wardrobes/',image_name) as image_name from users_wardrobe where user_id = $user_id and is_active = 1 order by id desc
        //
        $result = $query->result_array();
		return $result;

    }
	
	function delete_wardrobe_img($user_id,$wordrobe_id){
		$data = array('is_active'=>0,'modified_by'=>$user_id);
		$result = $this->db->update('users_wardrobe', $data, array('user_id' => $user_id,'id'=>$wordrobe_id));
		$data1 = array('status'=>0);
		$result1 = $this->db->update('user_wardrobe_tags', $data1, array('tag_user_id' => $user_id,'tag_ward_id'=>$wordrobe_id)); 
		if(!empty($result)){
           return 1;
       }else{
           return 2;
       }

    }
	
	function get_all_tags(){
		
        $query = $this->db->query("SELECT * FROM taglook WHERE status=1 ");
        $result = $query->result_array();
		return $result;

    }
	
	function push_look($timestamp){
		//echo $timestamp;exit;
	$query = $this->db->query("select distinct k.id,k.slug,product_img,k.look_type,k.image,look_image,name,k.created_datetime,k.modified_datetime as timestamp,k.status,k.deflag,k.stylist_id,@type:='look' as type from
(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag,z.`stylist_id` from 
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag,a.`stylist_id` FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 and a.modified_datetime>'$timestamp') as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc) as k,look_bucket_mapping as m where k.id = m.look_id order by k.modified_datetime desc");

		 //$res = $query->result();
		 
		/* Added by saylee*/
			$res = $query->result_array();
			//print_r($res);exit;
			$looks_array = array();
			
			$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
					$alllooksarrayobject = new stdClass(); 
					$alllooksarrayobject->id = $val['id'];
					$alllooksarrayobject->name = $val['name'];
					$alllooksarrayobject->timestamp = $val['timestamp'];
					if($val['look_type'] == 1){
						$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
						//$info = getimagesize($alllooksarrayobject->url);
					}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
						$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
						//$info = getimagesize($alllooksarrayobject->url);
					}else if($val['look_type'] == 3){
						$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
					}
					// $info = getimagesize($alllooksarrayobject->url);
					// $alllooksarrayobject->height = $info[1];
					// $alllooksarrayobject->width  = $info[0];
					$alllooksarrayobject->type = $val['type'];
					$alllooksarrayobject->look_type = $val['look_type'];
					$alllooksarrayobject->user_share = $this->get_user_share($val['id']);
					$alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
					$alllooksarrayobject->share_url = 'https://www.stylecracker.com/looks/look-details/'.$val['slug'];
					$alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
					$looks_array[$i] = $alllooksarrayobject;
					$i++;
				} 
		}
			
		return $looks_array[0];
//print_r($looks_array);exit;
		
		
	
	

    }

	function search_brand_name($brand_name){
        $query = $this->db->query("select company_name from brand_info where `show_in brand_list`=1 AND company_name  LIKE  '%$brand_name%' ");
        
        $res = $query->result_array();
        return $res;
    }
	/*
	function multi_image($product_id){
		$query = $this->db->query("select CONCAT('http://www.stylecracker.com/sc_admin/assets/products/',b.image) as url,a.id,CONCAT('https://www.stylecracker.com/sc_admin/assets/products_extra/thumb/',a.product_images) as image_name from extra_images as a,product_desc as b where a.product_id = $product_id AND b.id = a.product_id");

        $result = $query->result_array();
		return $result;
	}*/
	
	/*function multi_image($product_id){
		$query = $this->db->query("select CONCAT('https://www.stylecracker.com/sc_admin/assets/products/',b.image) as url,b.id,CONCAT('https://www.stylecracker.com/sc_admin/assets/products_extra/',a.product_images) as image_name,CONCAT('https://www.stylecracker.com/sc_admin/assets/products_extra/thumb_124/',a.product_images) as thumbnail_image,CONCAT('https://www.stylecracker.com/sc_admin/assets/products_extra/thumb_546/',a.product_images) as zoom_image from extra_images as a,product_desc as b where a.product_id = $product_id AND b.id = a.product_id");
		$result = $query->result_array();
		$i=0;
		$data=array();
		if(!empty($result)){
			foreach($result as $row){
				if($i==0){
					$data[$i]['zoom_image'] = $row['url'];
					$data[$i]['image_url'] = $row['url'];
					$data[$i]['thumbnail_image'] = $row['url'];
					$i = $i+1;
					$data[$i]['zoom_image'] = $row['image_name'];
					$data[$i]['image_url'] = $row['zoom_image'];
					$data[$i]['thumbnail_image'] = $row['thumbnail_image'];
					$i++;
				}else {
					$data[$i]['zoom_image'] = $row['image_name'];
					$data[$i]['image_url'] = $row['zoom_image'];
					$data[$i]['thumbnail_image'] = $row['thumbnail_image'];
					$i++;
				}
			}
		}else{
			$query = $this->db->query('select CONCAT("https://www.stylecracker.com/sc_admin/assets/products/",a.image) as url from product_desc as a where a.id = '.$product_id.' ');
			$result = $query->result_array();
			foreach($result as $row){
				$data[$i]['zoom_image'] = $row['url'];
				$data[$i]['image_url'] = $row['url'];
				$data[$i]['thumbnail_image'] = $row['url'];
			}
		}
		
		return $data;
	}*/
	
	function multi_image($product_id){
		$query = $this->db->query("select CONCAT('https://www.stylecracker.com/sc_admin/assets/products/',b.image) as url,b.id,CONCAT('https://www.stylecracker.com/sc_admin/assets/products_extra/',a.product_images) as image_name,CONCAT('https://www.stylecracker.com/sc_admin/assets/products_extra/thumb_124/',a.product_images) as thumbnail_image,CONCAT('https://www.stylecracker.com/sc_admin/assets/products_extra/thumb_546/',a.product_images) as zoom_image,b.brand_id,b.image,a.product_images from extra_images as a,product_desc as b where a.product_id = $product_id AND b.id = a.product_id");
		$result = $query->result_array();
		$i=0;
		$data=array();
        $imagify_brandid = unserialize(IMAGIFY_BRANDID); 
		if(!empty($result)){
			foreach($result as $row){
				if($i==0){
					if(!in_array($row['brand_id'],$imagify_brandid)){

						$data[$i]['zoom_image'] = $row['url'];
						$data[$i]['image_url'] = $row['url'];
						$data[$i]['thumbnail_image'] = $row['url'];
						$i = $i+1;
						$data[$i]['zoom_image'] = $row['image_name'];
						$data[$i]['image_url'] = $row['zoom_image'];
						$data[$i]['thumbnail_image'] = $row['thumbnail_image'];
						$i++;
					}else{
						$data[$i]['zoom_image'] = 'https://www.stylecracker.com/sc_admin/assets/product_images/zoom/'.$row['image'];
						$data[$i]['image_url'] = 'https://www.stylecracker.com/sc_admin/assets/product_images/normal/'.$row['image'];
						$data[$i]['thumbnail_image'] = 'https://www.stylecracker.com/sc_admin/assets/product_images/small/'.$row['image'];
						$i = $i+1;
						$data[$i]['zoom_image'] = 'https://www.stylecracker.com/sc_admin/assets/product_images/zoom/'.$row['product_images'];
						$data[$i]['image_url'] = 'https://www.stylecracker.com/sc_admin/assets/product_images/normal/'.$row['product_images'];
						$data[$i]['thumbnail_image'] = 'https://www.stylecracker.com/sc_admin/assets/product_images/small/'.$row['product_images'];
						$i++;
					}
					
					
				}else {
					if(!in_array($row['brand_id'],$imagify_brandid)){
						$data[$i]['zoom_image'] = $row['image_name'];
						$data[$i]['image_url'] = $row['zoom_image'];
						$data[$i]['thumbnail_image'] = $row['thumbnail_image'];
					}else{
						$data[$i]['zoom_image'] = 'https://www.stylecracker.com/sc_admin/assets/product_images/zoom/'.$row['product_images'];
						$data[$i]['image_url'] = 'https://www.stylecracker.com/sc_admin/assets/product_images/normal/'.$row['product_images'];
						$data[$i]['thumbnail_image'] = 'https://www.stylecracker.com/sc_admin/assets/product_images/small/'.$row['product_images'];
					}
					$i++;
				}
			}
		}else{
			$query = $this->db->query('select CONCAT("https://www.stylecracker.com/sc_admin/assets/products/",a.image) as url,a.brand_id,a.image from product_desc as a where a.id = '.$product_id.' ');
			$result = $query->result_array();
			foreach($result as $row){
				if(!in_array($row['brand_id'],$imagify_brandid)){
					$data[$i]['zoom_image'] = $row['url'];
					$data[$i]['image_url'] = $row['url'];
					$data[$i]['thumbnail_image'] = $row['url'];
				}else{
					$data[$i]['zoom_image'] = 'https://www.stylecracker.com/sc_admin/assets/product_images/zoom/'.$row['image'];
					$data[$i]['image_url'] = 'https://www.stylecracker.com/sc_admin/assets/product_images/normal/'.$row['image'];
					$data[$i]['thumbnail_image'] = 'https://www.stylecracker.com/sc_admin/assets/product_images/small/'.$row['image'];
				}
			}
		}
		
		return $data;
	}
	
	function get_look_name(){
		$query = $this->db->query("select DISTINCT name from look where status = 1 ");
        $res = $query->result_array();
		//print_r($res);exit;
		if(!empty($res)){ return $res; }else{ return 0; }
	}
	
	// added for new homepage
	function get_options($option_name){ 
	// echo $option_name;
    	return unserialize($this->get_sc_options2($option_name));
    }
	
	function get_sc_options2($option_name){
        if($option_name!=''){
            $query = $this->db->get_where('sc_options', array('option_name' => $option_name));
            $res = $query->result_array();
            if(!empty($res)){
                return $res[0]['option_value'];
            }else{ return ''; }
        }
    }
	
	function looks_by_look_id($look_id){   
	$query = $this->db->query("select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime as timestamp,z.status,z.deflag,@type:='look' as type from 
	(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 and a.id=$look_id ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime DESC");
	$res = $query->result_array();
	// print_r($res);
	// exit;

	$looks_array = array();
			
			$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
					$alllooksarrayobject = new stdClass(); 
					$alllooksarrayobject->id = $val['id'];
					$alllooksarrayobject->name = $val['name'];
					$alllooksarrayobject->timestamp = $val['timestamp'];
					if($val['look_type'] == 1){
						$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
					}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
						$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
					}else if($val['look_type'] == 3){
						$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
					}/*
					$info = getimagesize($alllooksarrayobject->url);
					$alllooksarrayobject->height = $info[1];
					$alllooksarrayobject->width  = $info[0];*/
					$alllooksarrayobject->height = 530;
					$alllooksarrayobject->width  = 530;
					$alllooksarrayobject->type = $val['type'];
					$alllooksarrayobject->look_type = $val['look_type'];
					// $alllooksarrayobject->user_share = $this->get_user_share($val['id']);
					// $alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
					// $alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
					$alllooksarrayobject->user_share = "15";
					$alllooksarrayobject->user_likes = "5";
					$alllooksarrayobject->look_type_txt = "Weekender";
					$alllooksarrayobject->share_url = 'https://www.stylecracker.com/looks/look-details/'.$val['slug'];
					$looks_array[$i] = $alllooksarrayobject;
					$i++;
				}
			}
			
		//print_r($looks_array);exit;
		
       if(!empty($res)){
            return $looks_array[0];
        }else{
            return NULL;
        }
    }
	
	function get_sc_options($option_name){
        if($option_name!=''){
            $query = $this->db->get_where('sc_options', array('option_name' => $option_name));
            $res = $query->result_array();
            if(!empty($res)){
                return unserialize($res[0]['option_value']);
            }else{ return ''; }
        }
    }
	
	// get top 3 category
	function get_home_categories($type=NULL){
        $cat_data = array();
        $option_key_name = 'top_cat_app_'.$type;
        $cat_ids = $this->get_sc_options($option_key_name); 
        if(!empty($cat_ids)){
            foreach ($cat_ids as $cat_id) {
                $cat_data[] = $this->get_category_info($cat_id['type'],$cat_id['cat']);
            }
        }
        return $cat_data;
    }

    function get_category_info($cat_id,$preselected){
        $cat_table_name = ''; $cat_data = array();
        if($cat_id == 1){ $cat_table_name = 'product_category'; }
        else if($cat_id == 2){ $cat_table_name = 'product_sub_category'; }
        else if($cat_id == 3){ $cat_table_name = 'product_variation_type'; }
        else if($cat_id == 4){ $cat_table_name = 'product_variation_value'; }

        if($cat_table_name !=''){
             if( $cat_table_name !='product_variation_value' && $cat_table_name !='product_variation_type'){
                $query = $this->db->get_where($cat_table_name, array('status' => '1','id'=>$preselected));
            }else if( $cat_table_name =='product_variation_type'){
                $query = $this->db->query('select b.*,c.product_cat_id from product_variation_type as b,product_sub_category as c where b.status = 1 and c.id = b.prod_sub_cat_id and b.id = '.$preselected);
            }else{
                $query = $this->db->query('select a.*,c.product_cat_id from product_variation_value as a,product_variation_type as b,product_sub_category as c where a.    product_variation_type_id = b.id and a.status = 1 and c.id = b.prod_sub_cat_id and a.id = '.$preselected);
            }
            $res = $query->result_array();
			// print_r($res);;exit;
            if(!empty($res)){
                foreach ($res as $val) {
                    $cat_data['name'] = $val['name'];
                    $cat_data['cat_id'] = $val['id'];
					$cat_data['product_cat_id'] = @$val['product_cat_id'];
                    $cat_data['type'] = $cat_id;
					$cat_data['slug'] = $val['slug'];
					$cat_data['url'] = '';
					$cat_data['cat_type'] = '';
					$cat_data['share_url'] = '';
                }
            }
        }

        return $cat_data;
    }
	
	// get top 3 brands
    function get_home_brands($type=NULL){
        $brands_data = array();
        $option_key_name = 'trending_brand_'.$type;
        $brand_ids = $this->get_sc_options($option_key_name);
		$brand_data = array();
        if(!empty($brand_ids)){
            foreach ($brand_ids as $brand_id) {
                $brand_data[] = $this->get_brands_info($brand_id);
            }
        }
        return $brand_data;
    }

    function get_brands_info($brandId){
        if($brandId > 0){
			$brand_image_url = base_url().'assets/images/brands/homepage/';
			$query = $this->db->query("select a.user_id,a.company_name,IF(a.home_page_promotion_img='','nothing',CONCAT('https://www.stylecracker.com/assets/images/brands/homepage/',a.home_page_promotion_img)) as home_page_promotion_img,CONCAT('".base_url()."brands/brand-details/',a.company_name) as share_url from brand_info as a where a.user_id = $brandId ");
			$res = $query->result_array();
			return $res[0];
        }else{
            return false;
        }
    }
	
	//get the gender of user 
	function get_user_gender($user_id){
			$query = $this->db->query("select a.gender from user_info a where a.user_id = $user_id ");
			$res = $query->result_array();
			if(!empty($res)){ return $res[0]['gender']; } else return 1;
			
    }
	
	public function get_all_sliders(){
		$time = new DateTime(date('Y-m-d H:i:s'));
        $time->add(new DateInterval('PT270M'));
        $dateM = $time->format('Y-m-d H:i:s');
		$query = $this->db->query("select a.* from sc_home_slider as a where a.start_date <= '".$dateM."' AND a.end_date >= '".$dateM."' AND is_active = 1 OR a.banner_type = 0 order by a.banner_position ASC");
		// echo $this->db->last_query();exit;
		$res = $query->result_array();
		// echo "<pre>";print_r($res);exit;
		$slider_array = array();
		$last_banner_position = 0;
		$new_banner_position = 1;
		$slider_image_url = base_url().'sc_admin/assets/banners/';
		$i = 0;
		if(!empty($res)){
			foreach($res as $row){
				if($last_banner_position != $row['banner_position']){
				$slider_array[$i]['id'] = $row['id'];
				$slider_array[$i]['caption_line_1'] = $row['caption_line_1'];
				$slider_array[$i]['caption_line_2'] = $row['caption_line_2'];
				$slider_array[$i]['lable_link'] = $row['lable_link'];
				$slider_array[$i]['slide_url'] = $slider_image_url.$row['slide_url'];
				$slider_array[$i]['is_active'] = $row['is_active'];
				$slider_array[$i]['start_date'] = $row['start_date'];
				$slider_array[$i]['end_date'] = $row['end_date'];
				$slider_array[$i]['banner_url'] = $row['banner_url'];
				$slider_array[$i]['banner_position'] = $row['banner_position'];
				$slider_array[$i]['banner_type'] = $row['banner_type'];
				$slider_array[$i]['is_deleted'] = $row['is_deleted'];
				$last_banner_position = $row['banner_position']; 
				$i++;
				// echo "<pre>";print_r($slider_array);exit;
				}else{
				$i = $i-1; 
				$slider_array[$i]['id'] = $row['id'];
				$slider_array[$i]['caption_line_1'] = $row['caption_line_1'];
				$slider_array[$i]['caption_line_2'] = $row['caption_line_2'];
				$slider_array[$i]['lable_link'] = $row['lable_link'];
				$slider_array[$i]['slide_url'] = $slider_image_url.$row['slide_url'];
				$slider_array[$i]['is_active'] = $row['is_active'];
				$slider_array[$i]['start_date'] = $row['start_date'];
				$slider_array[$i]['end_date'] = $row['end_date'];
				$slider_array[$i]['banner_url'] = $row['banner_url'];
				$slider_array[$i]['banner_position'] = $row['banner_position'];
				$slider_array[$i]['banner_type'] = $row['banner_type'];
				$slider_array[$i]['is_deleted'] = $row['is_deleted'];
				$last_banner_position = $row['banner_position']; 
				// echo "<pre>";print_r($slider_array);exit;
				$i++;
				} 
				
			}
		}
		//echo "<pre>";print_r($slider_array);exit;
		return $slider_array;
	}
	
	// view all category home page
	/*function category_list($cat){
        $where_cond = '';
		// $WOMEN_CATEGORY_ID = 1;
		// $MEN_CATEGORY_ID = 12;
       if($cat == 'men'){ $where_cond = $where_cond. ' and b.`product_cat_id` = '.MEN_CATEGORY_ID.' '; }
        if($cat == 'women'){ $where_cond = $where_cond. ' and b.`product_cat_id` = '.WOMEN_CATEGORY_ID.' '; }
        $query = $this->db->query("select a.id,a.name,a.slug,@type:='' as url,@type:='' as share_url from `product_variation_type` as a,`product_sub_category` as b WHERE a.`prod_sub_cat_id` = b.`id` $where_cond and a.`status`=1 and a.`is_delete`=0");
        $res = $query->result_array();
		$i=0;
		$category_array = array();
		foreach ($res as $value) { $category_img_url = ''; $cat_type = ''; 
			if(@$value['id']>0){
				if($value['id'] >0 ) {
					$category_img_url_array = @scandir('./assets/images/products_category/variation_types/'.$value['id'],true);
					if(!empty($category_img_url_array)){
					  $category_img_url = base_url().'assets/images/products_category/variation_types/'.$value['id'].'/'.$category_img_url_array[0];
					}
					$cat_type = 'variations_type';
				  }
			}//end of if 
		
		$res[$i]['url'] = $cat_type;
		if($cat_type === 'variations_type') $cat_type = 'variation';
		// if($cat_type === 'variation') $cat_type = 'variations';
		$res[$i]['share_url'] = base_url().'all-products/'.$cat.'/'.$cat_type.'/'.$res[$i]['slug'];
		$i++;
		}//end of foreach
        return $res;
    }*/
	
	function category_list($cat){
        $where_cond = '';
		$array_categories = $this->get_sc_options('sc_view_all_cat');
		$all_array_categories = $this->get_sc_options('top_cat_all');
		$array_categories = implode(',', $array_categories['var_type']);
		
        if($cat == 'men'){ $where_cond = $where_cond. ' and b.`product_cat_id` = '.MEN_CATEGORY_ID.' '; }
        if($cat == 'women'){ $where_cond = $where_cond. ' and b.`product_cat_id` = '.WOMEN_CATEGORY_ID.' '; }
        $query = $this->db->query("select a.id,a.name,a.slug,@type:='' as url,@type:='' as share_url,@type:='' as cat_type from `product_variation_type` as a,`product_sub_category` as b WHERE a.`prod_sub_cat_id` = b.`id` $where_cond and a.`status`=1 and a.`is_delete`=0 AND a.id in($array_categories)");
        $res = $query->result_array();
		// echo $this->db->last_query();exit;
		$i=0;
		$category_array = array();
		foreach ($res as $value) { $category_img_url = ''; $cat_type = ''; 
			if(@$value['id']>0){
				if($value['id'] >0 ) {
					$category_img_url_array = @scandir('./assets/images/products_category/variation_types/'.$value['id'],true);
					$url = $this->config->item('products_variation_types_img_url').$value['id'].'/'.$value['id'].".jpg";
					$header_img = get_headers("$url");
					if(!empty($category_img_url_array)){
					  // $category_img_url = base_url().'assets/images/products_category/variation_types/'.$value['id'].'/'.$category_img_url_array[0];
					  $category_img_url = $url;
					}
					$cat_type = 'variations_type';
				  }
			}//end of if 
		
		$res[$i]['url'] = $category_img_url;
		if($cat_type === 'variations_type') $cat_type = 'variation';
		if($cat === '') $cat = 'all';
		$res[$i]['share_url'] = base_url().'all-products/'.$cat.'/'.$cat_type.'/'.$res[$i]['slug'];
		$res[$i]['cat_type'] = $cat_type;
		$i++;
		}//end of foreach
        return $res;
    }
	
	// view all brands home page
	function brand_list($cat){
        $where_cond = '';
        if($cat == 'men'){ $where_cond = $where_cond. ' and a.`is_male` = 1'; }
        if($cat == 'women'){ $where_cond = $where_cond. ' and a.`is_female` = 1'; }
        if($cat == 'all'){ $where_cond = $where_cond. ' and ( a.`is_female` = 1 || a.`is_male` = 1 )'; }
        $query = $this->db->query(" SELECT a.user_id,a.company_name,IF(a.home_page_promotion_img IS NULL,'https://www.stylecracker.com/assets/images/image-na.jpg',CONCAT('https://www.stylecracker.com/assets/images/brands/homepage/',a.home_page_promotion_img)) as home_page_promotion_img,CONCAT('".base_url()."brands/brand-details/',a.company_name) as share_url FROM `brand_info` as a WHERE 1 $where_cond and a.is_paid = 1 order by a.user_id desc");
        $res = $query->result_array();
        return $res;
    }
	
	function update_user_gender($user_id,$gender){
		$data = array();
		if($user_id>0){
            $this->db->where('user_id', $user_id);
            $this->db->delete('users_answer'); 

            $data=array('bucket'=>0,'question_comp'=>0,'question_resume_id'=>'');
            $this->db->where('id',$user_id);
            $this->db->update('user_login',$data);

            $data=array('gender'=>$gender);
            $this->db->where('user_id',$user_id);
            $this->db->update('user_info',$data);
        }
		// $data_user_info = array('gender'=>$gender);
        // $this->db->update('user_info', $data_user_info, array('user_id' => $user_id ));
		// $data_user_login = array('question_comp'=>0,'bucket'=>0);
        // $this->db->update('user_login', $data_user_login, array('id' => $user_id ));
        return true;
	}
	
	 function get_looks_via_cat($look_type,$category_slug,$category_level,$limit=NULL,$offset=NULL){
		 if($limit<1){ $limit=3; $offset=3; }
        $product_table =''; $where_cond = ''; $look_id = '';
        if($category_level!=''){
            if($category_level == 'cat'){ $cat_table_name = 'product_category'; $product_table='product_desc'; }
            else if($category_level == 'sub_cat'){ $cat_table_name = 'product_sub_category'; $product_table='product_desc'; }
            else if($category_level == 'variations_type' || $category_level == 'variation'){ $cat_table_name = 'product_variation_type'; $product_table='product_variations'; }
            else if($category_level == 'variations'){ $cat_table_name = 'product_variation_value'; $product_table='product_variations'; }
            if($category_slug!=''){
                $query = $this->db->get_where($cat_table_name, array('status' => '1','slug'=>$category_slug));
                $res = $query->result_array();
                @$category_id = $res[0]['id'];
            }

            if($category_id > 0 && $product_table != ''){
                if($look_type == 'men') { $where_cond = $where_cond.' and a.looks_for=2';   }
                else if($look_type == 'women') { $where_cond = $where_cond.' and a.looks_for=1';   }
                if($product_table == 'product_desc'){
                    if($category_level == 'cat'){ 
                        $where_cond = $where_cond.' and c.product_cat_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and c.product_sub_cat_id='.$category_id; 
                    }
                    $query = $this->db->query('select distinct a.id as id from look as a,look_products as b,product_desc as c where a.id = b.look_id and b.product_id = c.id and a.status = 1 '.$where_cond.' order by a.modified_datetime desc '.$limit.' '.$offset.' ');
                    $lookIds = $query->result_array();
                    if(!empty($lookIds)){ $i = 0;
                        foreach($lookIds as $val){
                            if($i!=0){ $look_id = $look_id.','; }
                            $look_id = $look_id.$val['id'];
                            $i++;
                        }
                    }
                }else if($product_table == 'product_variations'){
                    if($category_level == 'variations_type' || $category_level == 'variation'){ 
                        $where_cond = $where_cond.' and c.product_var_type_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and c.product_var_val_id='.$category_id; 
                    }
                    $query = $this->db->query('select distinct a.id as id from look as a,look_products as b,product_variations as c where a.id = b.look_id and b.product_id = c.product_id and a.status = 1 '.$where_cond.' order by a.modified_datetime desc LIMIT '.$limit.' OFFSET '.$offset.' ');
                    $lookIds = $query->result_array();
                    if(!empty($lookIds)){ $i = 0;
                        foreach($lookIds as $val){
                            if($i!=0){ $look_id = $look_id.','; }
                            $look_id = $look_id.$val['id'];
                            $i++;
                        }
                    }
                }
            }

            if($look_id!=''){
                $query = $this->db->query("select z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime as timestamp,@type:='look' as type,z.description,z.status,z.deflag,z.slug,z.looks_for from (SELECT a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.description,a.status,a.deflag,a.slug,a.looks_for FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 OR z.look_type = 6) WHERE 1 and z.status=1 and z.id IN($look_id) order by z.modified_datetime desc");
                $look_data = $query->result_array();
				$looks_array = $this->looks_array($look_data);
				return $looks_array;
                return $look_data;
            }
        }
    }
	
	function get_products_via_cat($limit,$offset,$look_type,$category_slug,$category_level){
        $product_table =''; $where_cond = ''; $look_id = '';
        if($category_level!=''){
            if($category_level == 'cat'){ $cat_table_name = 'product_category'; $product_table='product_desc'; }
            else if($category_level == 'sub_cat'){ $cat_table_name = 'product_sub_category'; $product_table='product_desc'; }
            else if($category_level == 'variations_type' || $category_level == 'variation'){ $cat_table_name = 'product_variation_type'; $product_table='product_variations'; }
            else if($category_level == 'variations'){ $cat_table_name = 'product_variation_value'; $product_table='product_variations'; }
            if($category_slug!=''){
                $query = $this->db->get_where($cat_table_name, array('status' => '1','slug'=>$category_slug));
                $res = $query->result_array();
                @$category_id = $res[0]['id'];
            }

            if($category_id > 0 && $product_table != ''){
               if($product_table == 'product_desc'){
                    if($category_level == 'cat'){ 
                        $where_cond = $where_cond.' and product_cat_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and product_sub_cat_id='.$category_id; 
                    }
                    if($look_type == 'men') { $where_cond = $where_cond.' and product_cat_id=12';   }
                    else if($look_type == 'women') { $where_cond = $where_cond.' and product_cat_id=1';   }
                    $query = $this->db->query("select id,name,price,image,slug from product_desc WHERE  approve_reject = 'A' and is_delete = 0 $where_cond order by id desc $limit $offset ");
                    $product_data = $query->result_array();
                    
                }else if($product_table == 'product_variations'){
                    if($category_level == 'variations_type' || $category_level == 'variation'){ 
                        $where_cond = $where_cond.' and b.product_var_type_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and b.product_var_val_id='.$category_id; 
                    }
                    if($look_type == 'men') { $where_cond = $where_cond.' and a.product_cat_id=12';   }
                    else if($look_type == 'women') { $where_cond = $where_cond.' and a.product_cat_id=1';   }
                    $query = $this->db->query("select a.id,a.name,a.price,CONCAT('http://www.stylecracker.com/sc_admin/assets/products/',a.image) as url,a.slug from product_desc as a,product_variations as b WHERE a.approve_reject = 'A' and a.is_delete = 0 and a.id = b.product_id $where_cond order by a.id desc $limit $offset ");
                    $product_data = $query->result_array();
                }
				// print_r($product_data);exit;
				$product_array = array();$i=0;
				if(!empty($product_data)){
				foreach($product_data as $val){
					$product_array[$i] = $this->single_product_data($val['id']);
					$i++;
				}
                return $product_array;
				}
			}
		}
	}
    function get_products_via_cat_total($look_type,$category_slug,$category_level){
          $product_table =''; $where_cond = ''; $look_id = '';
        if($category_level!=''){
            if($category_level == 'cat'){ $cat_table_name = 'product_category'; $product_table='product_desc'; }
            else if($category_level == 'sub_cat'){ $cat_table_name = 'product_sub_category'; $product_table='product_desc'; }
            else if($category_level == 'variations_type' || $category_level == 'variation'){ $cat_table_name = 'product_variation_type'; $product_table='product_variations'; }
            else if($category_level == 'variations'){ $cat_table_name = 'product_variation_value'; $product_table='product_variations'; }
            if($category_slug!=''){
                $query = $this->db->get_where($cat_table_name, array('status' => '1','slug'=>$category_slug));
                $res = $query->result_array();
                @$category_id = $res[0]['id'];
            }

            if($category_id > 0 && $product_table != ''){
               if($product_table == 'product_desc'){
                    if($category_level == 'cat'){ 
                        $where_cond = $where_cond.' and product_cat_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and product_sub_cat_id='.$category_id; 
                    }
                    if($look_type == 'men') { $where_cond = $where_cond.' and product_cat_id=12';   }
                    else if($look_type == 'women') { $where_cond = $where_cond.' and product_cat_id=1';   }
                    $query = $this->db->query('select count(id) as cnt  from product_desc WHERE  approve_reject = "A" and is_delete = 0 '.$where_cond.'');
                    $product_data = $query->result_array();
                    
                }else if($product_table == 'product_variations'){
                    if($category_level == 'variations_type' || $category_level == 'variation'){ 
                        $where_cond = $where_cond.' and b.product_var_type_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and b.product_var_val_id='.$category_id; 
                    }
                    if($look_type == 'men') { $where_cond = $where_cond.' and a.product_cat_id=12';   }
                    else if($look_type == 'women') { $where_cond = $where_cond.' and a.product_cat_id=1';   }
                    $query = $this->db->query('select count(a.id) as cnt from product_desc as a,product_variations as b WHERE a.approve_reject = "A" and a.is_delete = 0 and a.id = b.product_id '.$where_cond.'');
                    $product_data = $query->result_array();
                } 
                if(!empty($product_data)){
                    return $product_data[0]['cnt'];
                }else{
                    return 0;
                }
            }
        }
    }
	
	function user_favorites($user_id,$fav_for,$fav_id){          
        
        $query = $this->db->query("select id,user_id from favorites where fav_id=$fav_id AND user_id=$user_id AND fav_for = '$fav_for' ");
		$res = $query->result_array();
		// print_r($res);echo $res[0]['id'];exit;
		if(!empty($res)){
			$id = $res[0]['id'];
			$query = $this->db->query(" Delete from favorites where id=$id ");
			return 1;
		}else{
			// $created_by = date('Y-m-d G:i:s'); 
			$data = array('user_id'=>$user_id,'fav_id'=>$fav_id,'fav_for'=>$fav_for);
			$result = $this->db->insert('favorites', $data);         
			if(!empty($result)){
				return 1;
			}else{
				return 2;
			}
		}
		
    }
	
	function single_product_data($product_id){
		/* $query = $this->db->query("Select p.id as product_id ,b.brand_store_name as sold_by,ps.name as category,p.brand_id,p.name,p.url,p.description,p.slug,p.price,p.url as buy_now_url,CONCAT('http://www.stylecracker.com/sc_admin/assets/products/',p.image) as url,(select bi.company_name from brand_info bi where bi.user_id=p.brand_id) AS brand_name, (select bi.redirect_sep from brand_info bi where bi.user_id=p.brand_id) AS redirect_sep from product_desc as p,product_sub_category ps,product_store a,brand_store b where p.product_sub_cat_id = ps.id AND p.approve_reject='A' AND p.id = $product_id AND a.store_id = b.id");*/
		 
		$query = $this->db->query("Select p.id as product_id,@type:='' as category,p.brand_id,p.name,p.description,p.slug,p.price,p.url as buy_now_url,CONCAT('http://www.stylecracker.com/sc_admin/assets/products/',p.image) as url,b.company_name as brand_name,CASE 
        WHEN  p.discount_start_date <= CURRENT_TIMESTAMP() and p.discount_end_date >= CURRENT_TIMESTAMP() then p.compare_price
        ELSE 0
    END AS compare_price from product_desc as p,brand_info b where p.approve_reject='A' AND p.id = $product_id AND p.brand_id = b.user_id ");
		 
		$res = $query->result_array();
		// print_r($res);exit;
		$product_array = array();
		$i=0;
		if(!empty($res)){
			foreach($res as $val){
				
				$productobject = new stdClass(); 
				$productobject->product_id = $val['product_id'];
				$productobject->product_category = $val['category'];
				$productobject->brand_id = $val['brand_id'];
				$productobject->name = $val['name'];
				$productobject->sold_by = $val['brand_name'];
				$productobject->return_policy = 'As Per Brand';
				$productobject->description = $val['description'];
				$productobject->slug = $val['slug'];
				$productobject->price = $val['price'];
				$productobject->buy_now_url = $val['buy_now_url'];
				$productobject->is_available = $this->is_available($val['product_id']);
				$productobject->url = $val['url'];
				$productobject->brand_name = $val['brand_name'];
				//$productobject->redirect_sep = $val['redirect_sep'];
				$size_category = $this->size_category($val['product_id']);
				$productobject->size_category = $size_category['size_guide'];
				$productobject->product_size = $this->product_size($val['product_id']);
				$brand_discount = $this->brand_discount($val['brand_id']);
				$product_discount = $this->product_discount($val['product_id']);
				$discount_price = 0;
				if(!empty($brand_discount)){
					$productobject->discount_percent = $brand_discount['discount_percent'];
					$discount_price = $val['price'] - ($val['price'] * ($brand_discount['discount_percent']/100));
					$productobject->discount_price = (string)($val['price'] - ($val['price'] * ($brand_discount['discount_percent']/100)));
				}else if(!empty($product_discount)){
					$productobject->discount_percent = $product_discount['discount_percent'];
					$discount_price = $val['price'] - ($val['price'] * ($product_discount['discount_percent']/100));
					$productobject->discount_price = (string)($val['price'] - ($val['price'] * ($product_discount['discount_percent']/100)));
				}else {
					$productobject->product_discount = '0';
					$discount_price = 0;
					$productobject->discount_price = '';
				}
				
				if($discount_price < $val['price']){ $productobject->on_sale = 'ON SALE'; }
				else $productobject->on_sale = '';
				
				/*  added for new discount compare price code start*/
				
				// $val['compare_price'] = 100;$val['price']=80;$discount_percent = 10;$discount_price=72;
				if($discount_percent > 0 || $val['compare_price']>$val['price']){
					$productobject->price = (string)$val['compare_price'];
					$productobject->discount_price = (string)$discount_price;
					$new_discount_percent = $this->calculate_discount_percent($val['compare_price'],$discount_price,$val['price']);
					$productobject->discount_percent = (string)$new_discount_percent;
				}else {
					$productobject->price = (string)$val['price'];
					$productobject->discount_price = (string)$discount_price;
					$val['compare_price'] = $val['price'];
					$new_discount_percent = $this->calculate_discount_percent($val['compare_price'],$discount_price,$val['price']);
					$productobject->discount_percent = (string)$new_discount_percent;
				}
					
				
				/*  added for new discount compare price code end*/
				
				$productobject->color = '';
				$productobject->fabric = '';
				$productobject->primary_color = '';
				$productobject->share_url = base_url().'product/details/'.$val['slug'].'-'.$val['product_id'];
				$productobject->size_guide_url = $this->get_size_guide_n($size_category['size_guide'],$val['product_id'],$val['brand_id']);
				// echo $productobject->share_url;exit;
				$product_array[$i] = $productobject;
				$i++;
			}//end of for each
				
		}//end of if 
		// print_r($product_array);exit;
		if(!empty($product_array)){
			return $product_array[0]; 
		}else
			return '';
	}
	
	public function get_brands_banners(){
		$time = new DateTime(date('Y-m-d H:i:s'));
        $time->add(new DateInterval('PT270M'));
        $dateM = $time->format('Y-m-d H:i:s');
		$query = $this->db->query("select a.* from brand_banners as a where a.start_date <= '".$dateM."' AND a.end_date >= '".$dateM."' AND is_active = 1 OR a.banner_type = 0 order by a.banner_position ASC");
		$res = $query->result_array();
		$slider_array = array();
		$last_banner_position = 0;
		$new_banner_position = 1;
		$i = 0;
		$brand_banner_image_url = base_url().'sc_admin/assets/brand_banners/';
		if(!empty($res)){
			foreach($res as $row){
				if($last_banner_position != $row['banner_position']){
				$slider_array[$i]['id'] = $row['id'];
				$slider_array[$i]['caption_line_1'] = $row['caption_line_1'];
				$slider_array[$i]['caption_line_2'] = $row['caption_line_2'];
				$slider_array[$i]['lable_link'] = $row['lable_link'];
				$slider_array[$i]['slide_url'] = $brand_banner_image_url.$row['slide_url'];
				$slider_array[$i]['is_active'] = $row['is_active'];
				$slider_array[$i]['start_date'] = $row['start_date'];
				$slider_array[$i]['end_date'] = $row['end_date'];
				$slider_array[$i]['banner_url'] = $row['banner_url'];
				$slider_array[$i]['banner_position'] = $row['banner_position'];
				$slider_array[$i]['banner_type'] = $row['banner_type'];
				$slider_array[$i]['is_deleted'] = $row['is_deleted'];
				$last_banner_position = $row['banner_position']; 
				$i++;
				// echo "<pre>";print_r($slider_array);exit;
				}else{
				$i = $i-1; 
				$slider_array[$i]['id'] = $row['id'];
				$slider_array[$i]['caption_line_1'] = $row['caption_line_1']; 
				$slider_array[$i]['caption_line_2'] = $row['caption_line_2'];
				$slider_array[$i]['lable_link'] = $row['lable_link'];
				$slider_array[$i]['slide_url'] = $brand_banner_image_url.$row['slide_url'];
				$slider_array[$i]['is_active'] = $row['is_active'];
				$slider_array[$i]['start_date'] = $row['start_date'];
				$slider_array[$i]['end_date'] = $row['end_date'];
				$slider_array[$i]['banner_url'] = $row['banner_url'];
				$slider_array[$i]['banner_position'] = $row['banner_position'];
				$slider_array[$i]['banner_type'] = $row['banner_type'];
				$slider_array[$i]['is_deleted'] = $row['is_deleted'];
				$last_banner_position = $row['banner_position']; 
				// echo "<pre>";print_r($slider_array);exit;
				$i++;
				} 
				
			}
		}
		// echo "<pre>";print_r($slider_array);exit;
		return $slider_array;
	}
	
	function check_scb_product($user_id){
		$query = $this->db->query("select DISTINCT a.product_id from cart_info as a where a.user_id = $user_id AND a.order_id is null ");   
		$result = $query->result_array();		
		return $result;	
	}
	
	function insert_gcm_id($gcm_id,$user_id){
		// if($gcm_id != ''){
		// $data = array('gcm_id'=>$gcm_id,'user_id'=>$user_id);
		// $result = $this->db->insert('user_gcm_ids', $data);   
		// }
		// return 1;
		if($gcm_id != ''){
			$query = $this->db->query("select * from user_gcm_ids where (`gcm_id` = '".$gcm_id."')");
			$res = $query->result_array();
			if(empty($res)){
				$data = array('gcm_id'=>$gcm_id,'user_id'=>$user_id);
				$result = $this->db->insert('user_gcm_ids', $data);   
			}else{
				$data = array('user_id'=>$user_id);
				$result = $this->db->update('user_gcm_ids', $data, array('gcm_id' => $gcm_id ));   
			} 
			
		}
		return 1;
	}
	
	function update_gcm_id($gcm_id,$user_id){
		if($gcm_id != ''){
		$data = array('user_id'=>$user_id);
		$result = $this->db->update('user_gcm_ids', $data, array('gcm_id' => $gcm_id ));   
		}
		return 1;
	}
	
	function get_size_guide_n($sgid=NULL,$pid=NULL,$bid=NULL){
        $product_id = $pid;
        $brand_id = $bid;
        $size_guide_id = $sgid;
        $size_guide_url = '';
            if($product_id > 0 && $brand_id >0){
                if($size_guide_id == 2){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/top.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/top.jpg';
                    }
                }

                if($size_guide_id == 3){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/bottom.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/bottom.jpg';
                    }
                }

                if($size_guide_id == 4){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/footwear.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/footwear.jpg';
                    }
                }

                if($size_guide_id == 5){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/top_bottom.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/top_bottom.jpg';
                    }
                }

                if($size_guide_id == 6){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/top_men.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/top_men.jpg';
                    }
                }
                if($size_guide_id == 7){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/bottom_men.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/bottom_men.jpg';
                    }
                }

                 if($size_guide_id == 8){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/footwear_men.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/footwear_men.jpg';
                    }
                }

                if($size_guide_id == 9){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/top_bottom_men.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/top_bottom_men.jpg';
                    }
                }
            }

            if(trim($size_guide_url) == ''){
                /*Women Default Size Guide*/
                if($size_guide_id == 2){
                    if(file_exists('assets/images/size_guide/stylecracker/top.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/stylecracker/top.jpg';
                    }
                }

                else if($size_guide_id == 3){
                    if(file_exists('assets/images/size_guide/stylecracker/bottom.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/stylecracker/bottom.jpg';
                    }
                }

                else if($size_guide_id == 4){  
                    if(file_exists('assets/images/size_guide/stylecracker/footwear.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/stylecracker/footwear.jpg';
                    }

                }              
                else if($size_guide_id == 5){
                    if(file_exists('assets/images/size_guide/stylecracker/top_bottom.jpg')){
                    $size_guide_url = base_url().'assets/images/size_guide/stylecracker/top_bottom.jpg';
                    }
                 /*Mens Default Size Guide*/
                }else if($size_guide_id == 6){  
                     $size_guide_url = base_url().'assets/images/size_guide/stylecracker/top_men.jpg';
                }

                else if($size_guide_id == 7){
                    $size_guide_url = base_url().'assets/images/size_guide/stylecracker/bottom_men.jpg';
                }

                else if($size_guide_id == 8){
                    $size_guide_url = base_url().'assets/images/size_guide/stylecracker/footwear_men.jpg';
                }

                else if($size_guide_id == 9){
                    $size_guide_url = base_url().'assets/images/size_guide/stylecracker/top_bottom_men.jpg';

                }else{
                    //$size_guide_url = base_url().'assets/images/size_guide/stylecracker/sc_size_guide.jpg';

                }
            }
        
        return $size_guide_url;

    }
	
	function get_product_wishlist_id($user_id){
		$query = $this->db->query("SELECT fav_id as product_id FROM `favorites` where fav_for ='product' AND user_id = $user_id ");
		$result = $query->result_array();
		return $result;
	}
	
	function get_brand_wishlist_id($user_id){
		$query = $this->db->query("SELECT fav_id as brand_id  FROM `favorites` where fav_for ='brand' AND user_id = $user_id ");
		$result = $query->result_array();
		return $result;
	}
	
	function calculate_discount_percent($compare_price,$discount_price,$product_price=null){
		// echo "comming".$compare_price.'----'.$discount_price;exit;
		$discount_percent = 0;
		if($compare_price > 0 && $discount_price > 0){
			$discount_percent = round((1-($discount_price/$compare_price))*100);
		}else if($compare_price > 0 && $product_price > 0){
			$discount_percent = round((1-($product_price/$compare_price))*100);
		}
		
		return $discount_percent;
	}
	
	function get_gender_by_email($email){
		$query = $this->db->query(" SELECT b.gender FROM user_login as a,user_info as b WHERE a.email='$email' AND a.id = b.user_id ");
		$result = $query->result_array();
		// echo $result[0]['gender'];exit;
		if(!empty($result)){
			return $result[0]['gender'];
		}else {
			return 1;
		}
		
	}
	
	function save_appdata($id,$app_data){
		$data = array('app_info'=>$app_data);
		$result = $this->db->update('user_login', $data, array('id' => $id));  
		// echo $this->db->last_query();exit;
		return true;
	}
}
?>