<?php
class Mcart_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function update_cart($productid,$userid,$productsize,$paymod,$agent,$medium,$look_id){
		$size = $this->productsize($productsize);
        if($productid!='' && $size!=''){
		//echo $uniquecookie."_".$productid."_".$userid."_".$size;exit;
        $query = $this->db->query("select * from cart_info where (`user_id` = '".$userid."' and `product_id` = '".$productid."' and `product_size` = '".$size."' AND `order_id` is null)");
        $res = $query->result_array(); 
        if(empty($res)){
            $data = array(
               'product_id' => $productid ,
               'user_id' => $userid,
               'created_datetime' => date('Y-m-d H:i:s'),
               'product_size' => $size,
               'product_qty' => '1',
			   'agent' =>$agent,
               'platform_info'=>$medium,
			   'look_id'=>$look_id
            );
            $this->db->insert('cart_info', $data);
        }else{
            if($res[0]['product_qty']<5){
                $data = array(
                   'product_id' => $productid ,
                   'user_id' => $userid,
                   'created_datetime' => date('Y-m-d H:i:s'),
                   'product_size' => $size,
                   'product_qty' => $res[0]['product_qty']+1,
					'agent' =>$agent,
					'platform_info'=>$medium,
					'look_id'=>$look_id
                );
              
                $this->db->where(array('id'=>$res[0]['id'],'product_id' => $productid,'product_size' => $size));
                $this->db->update('cart_info', $data); 
                }   
            }
        }
		$query = $this->db->query('select a.`id`,count(a.`product_id`) as item_count,a.`product_id`,SUM(a.`product_qty`) as item_qty,a.`product_qty`,b.`name`,(a.`product_qty`*b.price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,a.SCUniqueID from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") order by a.`created_datetime` DESC');
		$res = $query->result_array();
		$cart_object = $this->cart_object($res,$userid,$paymod);
		return $cart_object[0];     
    }
	
	function productsize($productsize){	
		$query = $this->db->query("select id from product_size where `size_text`  LIKE '$productsize' ");
		$res = $query->result_array();
		return $res[0]['id'];
	}
	
    function get_cart($userid,$paymod='',$pincode='',$chkShipping='',$chkCod='',$city='',$state=''){	
		// print_r($chkShipping);print_r($chkCod);exit;
		$query = $this->db->query('select a.`id`,count(a.`product_id`) as item_count,a.`product_id`,SUM(a.`product_qty`) as item_qty,a.`product_qty`,b.`name`,(a.`product_qty`*b.price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,a.SCUniqueID from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") order by a.`created_datetime` DESC');
        $res = $query->result_array();
		$cart_object = $this->cart_object($res,$userid,$paymod,$pincode,$chkShipping,$chkCod,$city,$state);
        if(!empty($cart_object))
		{	//echo $cart_object[0]->data->item_count <= 1;exit;
			//if($cart_object[0]->data->item_count >= 1){ return $cart_object[0];} else { return NULL;}
			return $cart_object[0];
		}
		else return '';
    }
	
	function cart_object($res,$userid,$paymod='',$pincode='',$chkShipping='',$chkCod='',$city='',$state=''){
//echo $userid.'___'.$paymod.'___'.$pincode.'___'.$chkShipping.'___'.$chkCod.'___'.$city.'___'.$state;exit;
		$cart_array = array();	
			$i = 0; 
			//print_r($chkShipping);exit;
			if(!empty($res)){
				foreach($res as $val){
					$cart_object = new stdClass(); 
					$data =	new stdClass();
					$data->item_count = $val['item_count'];
					$data->item_qty = $val['item_qty'];
					$data->pincode = $pincode;
					$data->city = $city;
					$data->state = $state;
					$res = $this->product_stock($userid);
					//print_r($res);exit;
					$count = count($res);
					$count1 = count($chkShipping);
					if($count1=1 && !empty($chkShipping)){
						$notship = $this->check_ship($chkShipping);
						$data->error_message = $notship[0];
					}
					else if($count1>1){
						$data->error_message = (object)array('text' => "Remove non-shippable products from cart or try another pin code.", 'color' => '#00a65a');
					}
					if(empty($data->error_message)){
						if($count=1 && !empty($res)) {
							$data->error_message = $res[0][0];
							}
						else  if($count>1){ 
							$data->error_message = (object)array('text' => "Remove Out Of Stock products to proceed.", 'color' => '#00a65a');
						}
					}
					if(empty($data->error_message)){
						$count2 = count($chkCod);
						if($count2=1 && !empty($chkCod)){
							$notcod = $this->check_cod($chkCod);
							//$data->error_message = $notcod[0];
							$data->error_message = (object)array('text' => "COD unavailable.Try another pincode OR proceed with Online Payment.", 'color' => '#00a65a');
						}
						else  if($count2>1){
							$data->error_message = (object)array('text' => "COD unavailable.Try another pincode OR proceed with Online Payment.", 'color' => '#00a65a');
						}
					}
					// if(empty($data->error_message))
						// $data->error_message =array('text' => "sucess", 'color' => '#00a65a');
					if(!empty($chkShipping) && empty($data->error_message)) $data->is_checkout_allowed = '1'; else $data->is_checkout_allowed = '0';
					$data->payment_methods = array('online', 'COD');
					/*if($paymod == '' OR $paymod == 'online'){
						$data->payment_methods = "online";
					}else {
						$data->payment_methods = "cod";
					}*/
					$data->total_summary = $this->total_summary($userid,$paymod);
					$cart_object->data = $data;
					$cart_object->items = $this->product_object($userid);
					$cart_array[$i] = $cart_object;
					$i++;
				}
			}
		if(!empty($cart_array)) return $cart_array; else return '';
	}
	
	function check_cod($product_id){
//print_r($product_id);exit;
		if(!empty($product_id)){
		$product_id = $product_id[0]['id'];
		$query = $this->db->query("select name from product_desc where id = $product_id ");
		$res = $query->result_array();
		$name = $res[0]['name'];
		$message = array(array('text' => 'COD not available. Proceed with online payment?', 'color' => '#00a65a'));
		//print_r($message);
		return $message[0];
		}
	}
	
	function check_ship($product_id){
		if(!empty($product_id)){
		$product_id = $product_id[0]['id'];
		$query = $this->db->query("select name from product_desc where id = $product_id ");
		$res = $query->result_array();
		$name = $res[0]['name'];
		$message = array(array('text' => 'Remove non-shippable products from cart or try another pin code.', 'color' => '#00a65a'));
		//print_r($message);
		return $message[0];
		}
	}
	
	function product_stock($userid){
		$query = $this->db->query('select a.`id`,d.min_del_days,d.max_del_days,a.`product_id`,a.`product_qty`,b.`name`,d.`company_name` as seller,CONCAT("https://www.stylecracker.com/sc_admin/assets/products/",b.image) as image_url,(a.`product_qty`*b.price) as price,c.`size_text` as slelected_size,a.`product_size`,a.SCUniqueID,IF(d.cod_available=1, "cod is available", "cod not available") as cod_available,@max_allowed_qty:="5" as max_allowed_qty,b.description,b.name from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") order by a.`created_datetime` DESC');
        $res = $query->result_array();
		//print_r( $res);exit;
		$product_array = array();
		$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){	
					$stock = $this->is_available($val['product_id']);
					//print_r($stock);exit;
					//echo $val['product_id'];exit;
					if($stock == 0){
						$product_object = new stdClass(); 
						$product_object = $val['product_id'];
						$message[$i] = array(array('text' => 'Remove Out Of Stock products to proceed.', 'color' => '#00a65a'));
						//$product_array[$i] = $product_object;
						$i++;
					}
					
				}
			}
			
		if(!empty($message))return $message; else return '';
	}
	
	function product_object($userid){
		//$query = $this->db->query('select a.`id`,d.min_del_days,d.max_del_days,a.`product_id`,a.`product_qty`,b.`name`,d.`company_name` as seller,CONCAT("https://www.stylecracker.com/sc_admin/assets/products/",b.image) as image_url,(a.`product_qty`*b.price) as price,c.`size_text` as selected_size,a.`product_size`,a.SCUniqueID,IF(d.cod_available=1, "COD is available", "COD not available") as cod_available,@max_allowed_qty:="5" as max_allowed_qty,b.description from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") order by a.`created_datetime` DESC');
		$query = $this->db->query('select a.`id`,d.min_del_days,d.max_del_days,a.`product_id`,a.`product_qty`,b.`name`,d.`company_name` as seller,CONCAT("https://www.stylecracker.com/sc_admin/assets/products/",b.image) as image_url,(a.`product_qty`*b.price) as price,c.`size_text` as selected_size,a.`product_size`,a.SCUniqueID,d.cod_available as cod_available,@max_allowed_qty:="5" as max_allowed_qty,b.description from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") order by a.`created_datetime` DESC');
        $res = $query->result_array();
		
       // return $res;
	   $product_array = array();
		$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
					$deliver_message = '';
					$days = 0;
					$product_object = new stdClass(); 
					$product_object->cart_id = $val['id'];
					$product_object->product_id = $val['product_id'];
					$product_object->product_qty = $val['product_qty'];
					$product_object->name = $val['name'];
					$product_object->seller = $val['seller'];
					$product_object->image_url = $val['image_url'];
					$product_object->price = $val['price'];
					$product_object->selected_size = $val['selected_size'];
					//$product_object->SCUniqueID = $val['SCUniqueID'];
					$product_object->max_allowed_qty = $val['max_allowed_qty'];
					$product_object->description = $val['description'];
					$product_object->cod_available = $val['cod_available'];
					/*if(($val['min_del_days'] != '' && $val['max_del_days'] != '') || ($val['min_del_days'] != NULL && $val['max_del_days'] != NULL)) {
						$deliver_message = 'Delivery in '.$val['min_del_days'].'-'.$val['max_del_days'].' day(s)';
					}
					else if(($val['min_del_days'] != '') || ($val['min_del_days'] != NULL)){
						$deliver_message = 'Delivery in '.$val['min_del_days'].'  day(s)';
					}
					else if(($val['max_del_days'] != '') || ($val['max_del_days'] != NULL)){
						$deliver_message = 'Delivery in '.$val['max_del_days'].' day(s)';
					}*/
					if($val['min_del_days']>0 && $val['max_del_days']>0){
						$deliver_message = 'Delivery in '.$val['min_del_days'].'-'.$val['max_del_days'].' working days.';
					}else if(($val['min_del_days']>0 && $val['max_del_days']=='') || ($val['min_del_days']=='' && $val['max_del_days']>0)){
						$deliver_message = 'Delivery in '.$val['min_del_days'].$val['max_del_days'].' working days.';
					}
					//$product_object->delivery_days = $deliver_message;
					if($val['cod_available'] == 1){
						$cod_message = 'cod is available';
					}else  $cod_message = 'cod not available';
					$message = array(array('text' => $cod_message, 'color' => '#00a65a'),
								   array('text' => $deliver_message, 'color' => '#dd4b39') );
					$product_object->message = $message;			   
					$product_object->is_available = (string)$this->is_available($val['product_id']);
					$product_object->product_sizes = $this->product_sizes($val['product_id']);
					$product_array[$i] = $product_object;
					$i++;
				}
			}
			//print_r($product_array);exit;
			return $product_array;
		
	}
	
	function product_sizes($product_id){
		$query = $this->db->query("SELECT b.size_text FROM product_inventory AS a, product_size b WHERE a.product_id = $product_id AND a.size_id = b.id");
		$res = $query->result_array();
		return $res;
	}
	
	function is_available($product_id){
		$query = $this->db->query("SELECT a.stock_count FROM product_inventory a WHERE a.product_id = $product_id ");
		$res = $query->result_array();
		// echo $res[0]['stock_count'];
		// print_r($res);exit;
		if($res != NULL && $res[0]['stock_count']>0)
			return 1;
		else return 0;
		//return $res;
	}
	function total_summary($userid,$paymod=''){
		$query = $this->db->query('select SUM(a.`product_qty`*b.price) as total,(SUM(a.`product_qty`*b.price)+SUM(d.store_tax)+SUM(d.`cod_charges`)) as grand_total,SUM(d.store_tax) as store_tax,SUM(d.shipping_charges) as shipping_charges,SUM(d.shipping_min_values) as shipping_min_values,SUM(d.shipping_max_values) as shipping_max_values,SUM(d.`cod_charges`) as cod_charges,SUM(d.`cod_min_value`) as cod_min_value,SUM(d.`code_max_value`) as code_max_value from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") order by a.`created_datetime` DESC');
		$res = $query->result_array();
		$cart_array = array();			
		$i = 0; 
		if(!empty($res)){
			foreach($res as $val){
				$cart_object = new stdClass(); 
				$cod = $this->total_cod($userid);
				$shipping = $this->total_shipping_charges($userid);
				$tax = $this->get_tax($userid);
				$cart_object->taxes = $this->get_tax($userid);
				if($paymod == 'cod'){
				$cart_object->total_shipping_charges = $this->total_shipping_charges($userid);
				$cart_object->total_cod = $this->total_cod($userid);
				}else {
				$cart_object->total_shipping_charges = $this->total_shipping_charges($userid);
				}
				$cart_object->total = $val['total'];
				if($paymod == 'online'){
					$total_shipping_charges = $val['total'] + $shipping['shipping_charges'] + $tax['store_tax'];
					$cart_object->grand_total = (string)$total_shipping_charges;
				}else{
					$total_cod_charges = $val['total'] + $shipping['shipping_charges'] + $cod['cod_charges'] + $tax['store_tax'];
					$cart_object->grand_total = (string)$total_cod_charges;
				}
				//$cart_object->grand_total = $val['total'] + $cod['cod_charges'] + $tax['store_tax'];
				$cart_array[$i] = $cart_object;
				$i++;
			}
		}
		
        return $cart_array[0];	
	}
	
	function get_tax($userid){
		$query1 = $this->db->query('select d.`user_id` as brand_id,a.product_qty,d.store_tax as store_tax,b.price as price from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'")');
		$res1 = $query1->result_array();
		$i = 0;
		$tax_total = 0;
		$tax = 0;
		if(!empty($res1)){
		foreach($res1 as $val){
		$tax = $val['product_qty']*$val['price']*($val['store_tax']/100);
		$tax_total = $tax_total + $tax;
		$i++;
		}
		}
		$cart_object1 = new stdClass();
		if(!empty($res1)) $cart_object1->store_tax = (string)$tax_total; else $cart_object1->store_tax = 0;
		
		$query = $this->db->query('select d.`user_id` as brand_id,a.`product_id` from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") GROUP BY d.`user_id` ');
		$res = $query->result();
		$cart_array = array();			
		if(!empty($res)){
			foreach($res as $val){
				//$cart_object = new stdClass();
				$brand_info = $this->brand_tax($val->brand_id,$val->product_id);
				if(!empty($brand_info)){
				$cart_object = $brand_info;
				$cart_array[] = $cart_object;
				}
			}
		}
		$cart_object = new stdClass();
		$cart_object->brand_array = $cart_array;

		$data = array_merge((array)$cart_object1,(array)$cart_object);
		if(!empty($data)){ return $data; }else { return ''; }	
	}
	
	function brand_tax($brand_id,$product_id){
		$query = $this->db->query("SELECT a.store_tax,a.user_id as brand_id,a.company_name as seller,(b.price*(a.store_tax/100)) as brand_tax FROM `brand_info` a,`product_desc` as b WHERE a.`user_id` = $brand_id AND b.id= $product_id AND b.brand_id = $brand_id AND `store_tax` != '' AND `store_tax` != 0 ");
		$res = $query->result();
		//print_r($res);exit;
		if(!empty($res)){ return $res[0]; }else { return ''; }	
	}
	
	function total_shipping_charges($user_id){
			$productPrice = 0; $final_cod_charges =0;
        $order_list = array(); $product_total = 0;
        $shipping_charges = 0;
		$old_store_shipping_charges = 0;
		$final_shipping_charges = 0;
		$cart_data = $this->get_cart2($user_id);
		//print_r($cart_info);exit;
		if(!empty($cart_data)){
			 $new_brand = ''; $old_brand = ''; $new_store_tax = 0; $old_store_tax = 0; $new_store_cod_charges = 0; $old_store_cod_charges = 0;
            $brand_total = 0; $order_info = '';
			$i = 0;
		 foreach($cart_data as $val){
                $new_brand = $val['user_id'];
                $new_store_tax = $val['store_tax'];

                $new_store_cod = $val['is_cod'];
                $new_store_cod_charges = $val['cod_charges'];

                if($new_brand != $old_brand){ 
                    
                    if($brand_total!=0){ 
                        $tax = $brand_total*($old_store_tax/100); 
                        $order_amount = $brand_total+($brand_total*($old_store_tax/100)); 
						//$paymod == 'cod'
                      /* COD CALCULAION START */
                        if($this->getBrandExtraInfo($old_brand,'is_cod')[0]['is_cod'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0){
                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ; 
                                $order_amount = $order_amount + $final_cod_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] >= 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] > 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']<=$product_total && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']>=$product_total) {


                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                                $order_amount = $order_amount + $final_cod_charges;
                            }else { $old_store_cod_charges = 0; }
                        }else{ $old_store_cod_charges = 0; }
                        /* COD CALCULAION END */

                        /* SHIPPING CHARGES START*/
                        if($this->getBrandExtraInfo($old_brand,'is_shipping')[0]['is_shipping'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] == 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] == 0){


                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] >= 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] > 0 && $this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values']<=$product_total && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values']>=$product_total) {

                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;

                            }else { $old_store_shipping_charges = 0; }
                            }else{ $old_store_shipping_charges = 0; }
                        /* SHIPPING CHARGES END*/
						$shipping_charges = $final_shipping_charges;
                        $order_info = array(
                                'user_id' => $user_id,
                                'brand_id' => $old_brand,
                                'order_tax_amount' => $tax,
                                'order_sub_total' => $order_amount,
                                'order_total' => $order_amount,
                                // 'pay_mode' => $data['paymod']=='cod' ? '1' ,
                                // 'created_datetime' =>date('Y-m-d H:i:s'),
                                // 'first_name' => $data['uc_first_name'],
                                //'last_name' => $data['uc_last_name'],
                                // 'email_id' => $data['uc_email_id'],
                                // 'mobile_no' => $data['uc_mobile'],
                                // 'shipping_address' => $data['uc_address'],
                                // 'pincode' => $data['uc_pincode'],
                                // 'city_name' => $data['uc_city'],
                                // 'state_name' => $data['uc_state'],
                                // 'mihpayid' => $data['mihpayid'],
                                // 'txnid' => $data['txnid'],
                                'order_status' => '1',
                                'cod_amount' => $final_cod_charges,
                                'shipping_amount' => $final_shipping_charges,
                            );
                        //$order_id = $this->sc_place_order($order_info); $product_total =0;
                        //$order_list[] = $order_id; 

                    
                        $product_info = [];
                    }
                    //$brand_total = 0;
                    $old_brand = $new_brand;
                    $old_store_tax = $new_store_tax;
                    //$brand_total = $brand_total+$val['price'];

                    $product_info[$i]['product_id'] = $val['product_id'];
                    $product_info[$i]['user_id'] = $user_id;
                    $product_info[$i]['created_datetime'] = date('Y-m-d H:i:s');
                    $product_info[$i]['product_size'] = $val['product_size'];
                    $product_info[$i]['product_qty'] = $val['product_qty'];
                    $product_info[$i]['price'] = $val['price'];
                    $product_info[$i]['cart_id'] = $val['id'];
				
                    $product_total = $product_total+$val['price'];
                    $brand_total = $product_total;
					
                }else{
					
                    //$brand_total = $brand_total+$productPrice;
                    $old_brand = $new_brand;
                    $old_store_tax = $new_store_tax;

                    $product_info[$i]['product_id'] = $val['product_id'];
                    $product_info[$i]['user_id'] = $user_id;
                    $product_info[$i]['created_datetime'] = date('Y-m-d H:i:s');
                    $product_info[$i]['product_size'] = $val['product_size'];
                    $product_info[$i]['product_qty'] = $val['product_qty'];
                    $product_info[$i]['price'] = $val['price'];
                    $product_info[$i]['cart_id'] = $val['id'];
                    $product_total = $product_total+$val['price'];
                    $brand_total = $product_total;
					
                }
                $i++;
            }
			if($brand_total!=0){ 
                $tax = $brand_total*($old_store_tax/100); 
                $order_amount = $brand_total+($brand_total*($old_store_tax/100)); 

                /* COD CALCULAION START */
						
                        if($this->getBrandExtraInfo($old_brand,'is_cod')[0]['is_cod'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0){
                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ; 
                                $order_amount = $order_amount + $final_cod_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] >= 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] > 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']<=$product_total && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']>=$product_total) {


                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                                $order_amount = $order_amount + $final_cod_charges;
                            }else { $old_store_cod_charges = 0; }
                        }else{ $old_store_cod_charges = 0; }
                        /* COD CALCULAION END */

                                 /* SHIPPING CHARGES START*/
                        if($this->getBrandExtraInfo($old_brand,'is_shipping')[0]['is_shipping'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] == 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] == 0){


                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] >= 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] > 0 && $this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values']<=$product_total && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values']>=$product_total) {

                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;

                            }else { $old_store_shipping_charges = 0; }
                            }else{ $old_store_shipping_charges = 0; }
                        /* SHIPPING CHARGES END*/
						$shipping_charges = $final_shipping_charges;
                        $product_info = [];

                        

            }
		}
		$cart_object1 = new stdClass();
		if($shipping_charges != 0) $cart_object1->shipping_charges = (string)$shipping_charges; else $cart_object1->shipping_charges = '0';
		
		$query = $this->db->query('select d.`user_id` as brand_id from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$user_id.'")  GROUP BY d.`user_id` ');
		$res = $query->result();
		$cart_array = array();			
		if(!empty($res)){
			foreach($res as $val){
				$brand_info = $this->brand_shipping_charges($val->brand_id,$user_id);
				if(!empty($brand_info)){
				$cart_object = $brand_info;
				$cart_array[] = $cart_object;
				}
			}
		}
		$cart_object = new stdClass();
		$cart_object->brand_array = $cart_array;
		$data = array_merge((array)$cart_object1,(array)$cart_object);
		if(!empty($data)){ return $data; }else { return ''; }	
	}
	
	function brand_shipping_charges($brand_id,$user_id){
		//$query = $this->db->query("SELECT shipping_charges,user_id as brand_id,company_name as seller FROM `brand_info` WHERE `user_id` = $brand_id");
		$query = $this->db->query("select d.`company_name` as seller,d.`user_id` as brand_id ,d.shipping_charges as shipping_charges from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` AND d.`user_id` = $brand_id AND (a.`product_qty`*b.price) < d.shipping_max_values and (a.`user_id`=$user_id) order by b.`brand_id` ");
		$res = $query->result();
		if(!empty($res)){ return $res[0]; }else { return ''; }	
	}
	
	function total_cod($user_id){
		$productPrice = 0; $final_cod_charges =0;
        $order_list = array(); $product_total = 0;
        $shipping_charges = 0;
		$sc_cod_charges = 0;
		$old_store_shipping_charges = 0;
		$final_shipping_charges = 0;
		$cart_data = $this->get_cart2($user_id);
		//print_r($cart_info);exit;
		if(!empty($cart_data)){
			 $new_brand = ''; $old_brand = ''; $new_store_tax = 0; $old_store_tax = 0; $new_store_cod_charges = 0; $old_store_cod_charges = 0;
            $brand_total = 0; $order_info = '';
			$i = 0;
		 foreach($cart_data as $val){
                $new_brand = $val['user_id'];
                $new_store_tax = $val['store_tax'];

                $new_store_cod = $val['is_cod'];
                $new_store_cod_charges = $val['cod_charges'];

                if($new_brand != $old_brand){ 
                    
                    if($brand_total!=0){ 
                        $tax = $brand_total*($old_store_tax/100); 
                        $order_amount = $brand_total+($brand_total*($old_store_tax/100)); 
						//$paymod == 'cod'
                      /* COD CALCULAION START */
                        if($this->getBrandExtraInfo($old_brand,'is_cod')[0]['is_cod'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0){
                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ; 
                                $order_amount = $order_amount + $final_cod_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] >= 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] > 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']<=$product_total && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']>=$product_total) {


                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                                $order_amount = $order_amount + $final_cod_charges;
                            }else { $old_store_cod_charges = 0; }
                        }else{ $old_store_cod_charges = 0; }
                        /* COD CALCULAION END */
						$sc_cod_charges =  $final_cod_charges;
                        /* SHIPPING CHARGES START*/
                        if($this->getBrandExtraInfo($old_brand,'is_shipping')[0]['is_shipping'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] == 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] == 0){


                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] >= 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] > 0 && $this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values']<=$product_total && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values']>=$product_total) {

                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;

                            }else { $old_store_shipping_charges = 0; }
                            }else{ $old_store_shipping_charges = 0; }
                        /* SHIPPING CHARGES END*/
						$shipping_charges = $final_shipping_charges;
                        $order_info = array(
                                'user_id' => $user_id,
                                'brand_id' => $old_brand,
                                'order_tax_amount' => $tax,
                                'order_sub_total' => $order_amount,
                                'order_total' => $order_amount,
                                // 'pay_mode' => $data['paymod']=='cod' ? '1' ,
                                // 'created_datetime' =>date('Y-m-d H:i:s'),
                                // 'first_name' => $data['uc_first_name'],
                                //'last_name' => $data['uc_last_name'],
                                // 'email_id' => $data['uc_email_id'],
                                // 'mobile_no' => $data['uc_mobile'],
                                // 'shipping_address' => $data['uc_address'],
                                // 'pincode' => $data['uc_pincode'],
                                // 'city_name' => $data['uc_city'],
                                // 'state_name' => $data['uc_state'],
                                // 'mihpayid' => $data['mihpayid'],
                                // 'txnid' => $data['txnid'],
                                'order_status' => '1',
                                'cod_amount' => $final_cod_charges,
                                'shipping_amount' => $final_shipping_charges,
                            );
                        //$order_id = $this->sc_place_order($order_info); $product_total =0;
                        //$order_list[] = $order_id; 

                    
                        $product_info = [];
                    }
                    //$brand_total = 0;
                    $old_brand = $new_brand;
                    $old_store_tax = $new_store_tax;
                    //$brand_total = $brand_total+$val['price'];

                    $product_info[$i]['product_id'] = $val['product_id'];
                    $product_info[$i]['user_id'] = $user_id;
                    $product_info[$i]['created_datetime'] = date('Y-m-d H:i:s');
                    $product_info[$i]['product_size'] = $val['product_size'];
                    $product_info[$i]['product_qty'] = $val['product_qty'];
                    $product_info[$i]['price'] = $val['price'];
                    $product_info[$i]['cart_id'] = $val['id'];
				
                    $product_total = $product_total+$val['price'];
                    $brand_total = $product_total;
					
                }else{
					
                    //$brand_total = $brand_total+$productPrice;
                    $old_brand = $new_brand;
                    $old_store_tax = $new_store_tax;

                    $product_info[$i]['product_id'] = $val['product_id'];
                    $product_info[$i]['user_id'] = $user_id;
                    $product_info[$i]['created_datetime'] = date('Y-m-d H:i:s');
                    $product_info[$i]['product_size'] = $val['product_size'];
                    $product_info[$i]['product_qty'] = $val['product_qty'];
                    $product_info[$i]['price'] = $val['price'];
                    $product_info[$i]['cart_id'] = $val['id'];
                    $product_total = $product_total+$val['price'];
                    $brand_total = $product_total;
					
                }
                $i++;
            }
			if($brand_total!=0){ 
                $tax = $brand_total*($old_store_tax/100); 
                $order_amount = $brand_total+($brand_total*($old_store_tax/100)); 

                /* COD CALCULAION START */
						
                        if($this->getBrandExtraInfo($old_brand,'is_cod')[0]['is_cod'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0){
                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ; 
                                $order_amount = $order_amount + $final_cod_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] >= 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] > 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']<=$product_total && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']>=$product_total) {


                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                                $order_amount = $order_amount + $final_cod_charges;
                            }else { $old_store_cod_charges = 0; }
                        }else{ $old_store_cod_charges = 0; }
                        /* COD CALCULAION END */
						$sc_cod_charges =  $final_cod_charges;
                                 /* SHIPPING CHARGES START*/
                        if($this->getBrandExtraInfo($old_brand,'is_shipping')[0]['is_shipping'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] == 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] == 0){


                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] >= 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] > 0 && $this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values']<=$product_total && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values']>=$product_total) {

                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;

                            }else { $old_store_shipping_charges = 0; }
                            }else{ $old_store_shipping_charges = 0; }
                        /* SHIPPING CHARGES END*/
						$shipping_charges = $final_shipping_charges;
                        $product_info = [];

                        

            }
		}
		$cart_object1 = new stdClass();
		if($sc_cod_charges != 0) $cart_object1->cod_charges = (string)$sc_cod_charges; else $cart_object1->cod_charges = '0';
		$query = $this->db->query('select d.`user_id` as brand_id from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$user_id.'") GROUP BY d.`user_id` ');		
		$res = $query->result();
		$cart_array = array();			
		if(!empty($res)){
			foreach($res as $val){
				$brand_info = $this->brand_total_cod($val->brand_id,$user_id);
				if(!empty($brand_info)){
				$cart_object = $brand_info;
				$cart_array[] = $cart_object;
				}
			}
		}
		$cart_object = new stdClass();
		$cart_object->brand_array = $cart_array;
		//return $cart_array[0];
		$data = array_merge((array)$cart_object1,(array)$cart_object);
		if(!empty($data)){ return $data; }else { return ''; }	
	}
	
	function brand_total_cod($brand_id,$user_id){
		//$query = $this->db->query("SELECT cod_charges,user_id as brand_id,company_name as seller FROM `brand_info` WHERE `user_id` = $brand_id");
		$query = $this->db->query("select d.`company_name` as seller,d.`user_id` as brand_id ,d.cod_charges as cod_charges from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` AND d.`user_id` = $brand_id AND (a.`product_qty`*b.price) < d.code_max_value and (a.`user_id`=$user_id) order by b.`brand_id` ");
		$res = $query->result_array();
		//print_r($res);exit;
		if(!empty($res)){ return $res[0]; }else { return ''; }	
	}
	
    function deleteCartProduct($product_id,$userid,$cart_id,$paymod){
        $query = $this->db->query("delete from `cart_info` where `product_id`='".$product_id."' AND `user_id` = '".$userid."' AND `id` = '".$cart_id."'");
		
		$query = $this->db->query('select a.`id`,count(a.`product_id`) as item_count,a.`product_id`,SUM(a.`product_qty`) as item_qty,a.`product_qty`,b.`name`,(a.`product_qty`*b.price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,a.SCUniqueID from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`product_id` = "'.$product_id.'" OR a.`user_id`="'.$userid.'") order by a.`created_datetime` DESC');
		$res = $query->result_array();
		$cart_object = $this->get_cart($userid,$paymod);
		return $cart_object;       
        
    }

    function getBrandUsername($brand_id){
		//echo $brand_id;exit;
        $query = $this->db->get_where('user_login', array('id' => $brand_id));
        $res = $query->result_array();
        if(!empty($res)){
            return $res[0]['user_name'];
        }else{
            return '';
        }
    }

    function get_all_states(){
        $query = $this->db->get('states');
        $res = $query->result_array();
        return $res;
    }

    function get_city_name($pincode){
		$query = $this->db->query("select TRIM(a.`city`) as city,TRIM(b.`state_name`) as state from `city` as a,`states` as b,pincode as c where a.`state_id` = b.`id` AND a.id = c.city_id AND  c.`pincode`='".$pincode."'");
        $res = $query->result_array();
		if(!empty($res)) return $res[0]; else return '';
    }

    function productQtyUpdate($product_id,$productQty,$userid,$cart_id,$paymod){
        if($product_id!='' && $productQty!='' && $userid!=''){
           
		$data = array('product_qty' => $productQty);
		$this->db->where(array('product_id' => $product_id,'user_id'=>$userid,'id'=>$cart_id));
		$this->db->update('cart_info', $data);
        $query = $this->db->query('select a.`id`,count(a.`product_id`) as item_count,a.`product_id`,SUM(a.`product_qty`) as item_qty,a.`product_qty`,b.`name`,(a.`product_qty`*b.price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,a.SCUniqueID from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") order by a.`created_datetime` DESC');
		$res = $query->result_array();
		$cart_object = $this->get_cart($userid,$paymod);
		return $cart_object;       
        }
    }
	
	function productSizeUpdate($product_id,$size,$userid,$cart_id){
		$productsize = $this->productsize($size);
        if($product_id!='' && $productsize!='' && $userid!=''){
           
		$data = array('product_size' => $productsize);
		$this->db->where(array('product_id' => $product_id,'user_id'=>$userid,'id'=>$cart_id));
		$this->db->update('cart_info', $data);		
		$query = $this->db->query('select a.`id`,count(a.`product_id`) as item_count,a.`product_id`,SUM(a.`product_qty`) as item_qty,a.`product_qty`,b.`name`,(a.`product_qty`*b.price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,a.SCUniqueID from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`product_id` = "'.$product_id.'" OR a.`user_id`="'.$userid.'") order by a.`created_datetime` DESC');
		$res = $query->result_array();
		$cart_object = $this->get_cart($userid);
		return $cart_object;     
        
        }
    }

    function getBrandExtraInfo($brand_id){
        $query = $this->db->get_where('brand_info', array('user_id' => $brand_id));
        $res = $query->result_array();
        if(!empty($res)){
            return $res;
        }else{
            return '';
        }
    }

    function get_cod_cart_info($uniquecookie,$pincode){
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }
        $query = $this->db->query('select distinct a.id,b.`cod_exc_location` from `cart_info` as a,`brand_info` as b,`product_desc` as c where a.`product_id` = c.`id` and b.`user_id` = c.`brand_id` and a.`user_id` ="'.$user_id.'" and a.`SCUniqueID`="'.$uniquecookie.'" and a.`order_id` is null and b.`cod_exc_location` like "%'.$pincode.'%"');
        $res = $query->result_array();
        return $res;
    }

    function get_not_ship_cart_info($uniquecookie,$pincode){
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }
        $query = $this->db->query('select distinct a.id,b.`shipping_exc_location` from `cart_info` as a,`brand_info` as b,`product_desc` as c where a.`product_id` = c.`id` and b.`user_id` = c.`brand_id` and a.`user_id` ="'.$user_id.'" and a.`SCUniqueID`="'.$uniquecookie.'" and a.`order_id` is null and b.`shipping_exc_location` like "%'.$pincode.'%"');
        $res = $query->result_array();
        return $res;
    }

	 function update_order_cart_info($cart_id,$order_id,$user_id){
        if($cart_id!='' && $order_id!=''){
            $data = array(
               'order_id' => $order_id,
               'user_id' => $user_id
            );

            $this->db->where('id', $cart_id);
            $this->db->update('cart_info', $data); 
        }
    }

	  function update_order_cart_info_online_order($user_id,$order_id){

	 	$order_ids = explode('_',$order_id);
		 
		if(!empty($order_ids)){
			for($i = 1; $i<count($order_ids); $i++){

				$order_id =$order_ids[$i];

				$query = $this->db->query("SELECT cart_id FROM `order_product_info` WHERE order_id ='".$order_id."'");
       	 		$res = $query->result_array();
				for($i = 0; $i<count($res); $i++){
					 if($order_id!='' && !empty($res) && $res[$i]['cart_id']!=''){
			            $data = array(
			               'order_id' => $order_id,
			               'user_id' => $user_id,

			            );

			            $this->db->where(array('user_id'=>$user_id,'order_id'=>NULL,'id' => $res[$i]['cart_id']));

			            $this->db->update('cart_info', $data); 
			            //echo $this->db->last_query();exit;
			        }
				}
			}
		}

       
    }
	
	 function get_cart2($userid){
       $query = $this->db->query('select a.`id`,a.`product_id`,a.`product_qty`,b.`name`,(a.`product_qty`*b.price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size` from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") order by b.`brand_id`');
        $res = $query->result_array();
        return $res;
    }
	 
	 function sc_uc_order2($data){
        $this->load->model('Schome_model');
        $user_id = $data['user_id'];
		if($data['paymod']=='cod'){
			$order_status = 1;
		}
		else $order_status = 9;
        $productPrice = 0; $final_cod_charges =0;
        $order_list = array(); $product_total = 0;
        $new_store_shipping_charges = 0;
            $old_store_shipping_charges = 0;
                $final_shipping_charges = 0;
 
        $cart_data = $this->get_cart2($user_id);
        if(!empty($cart_data)){

            if($user_id == 0){
                $user_id = $this->check_user_exist_or_not($data['uc_email_id']);
            }
            $new_brand = ''; $old_brand = ''; $new_store_tax = 0; $old_store_tax = 0; $new_store_cod_charges = 0; $old_store_cod_charges = 0;
            $brand_total = 0; $order_info = '';
            $product_info = array(); $i = 0;

            if($user_id == 0 ){
                $scusername = $this->Schome_model->randomPassword();
                $scpassword = $this->Schome_model->randomPassword();
                $ip = $this->input->ip_address(); 
                $this->Schome_model->registerUser('',$scusername,$data['uc_email_id'],$scpassword,$ip,2,'');
                $user_id = $this->check_user_exist_or_not($data['uc_email_id']);
            }

            foreach($cart_data as $val){
                $new_brand = $val['user_id'];
                $new_store_tax = $val['store_tax'];

                $new_store_cod = $val['is_cod'];
                $new_store_cod_charges = $val['cod_charges'];

                if($new_brand != $old_brand){ 
                    
                    if($brand_total!=0){ 
                        $tax = $brand_total*($old_store_tax/100); 
                        $order_amount = $brand_total+($brand_total*($old_store_tax/100)); 

                      /* COD CALCULAION START */
                        if($this->getBrandExtraInfo($old_brand,'is_cod')[0]['is_cod'] == 1 && $data['paymod'] == 'cod'){
                            if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0){
                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ; 
                                $order_amount = $order_amount + $final_cod_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] >= 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] > 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']<=$product_total && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']>=$product_total) {


                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                                $order_amount = $order_amount + $final_cod_charges;
                            }else { $old_store_cod_charges = 0; }
                        }else{ $old_store_cod_charges = 0; }
                        /* COD CALCULAION END */

                        /* SHIPPING CHARGES START*/
                        if($this->getBrandExtraInfo($old_brand,'is_shipping')[0]['is_shipping'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] == 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] == 0){


                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] >= 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] > 0 && $this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values']<=$product_total && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values']>=$product_total) {

                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;

                            }else { $old_store_shipping_charges = 0; }
                            }else{ $old_store_shipping_charges = 0; }
                        /* SHIPPING CHARGES END*/

                        $order_info = array(
                                'user_id' => $user_id,
                                'brand_id' => $old_brand,
                                'order_tax_amount' => $tax,
                                'order_sub_total' => $order_amount,
                                'order_total' => $order_amount,
                                'pay_mode' => $data['paymod']=='cod' ? '1' : '2',
                                'created_datetime' =>date('Y-m-d H:i:s'),
                                'first_name' => $data['uc_first_name'],
                                'last_name' => $data['uc_last_name'],
                                'email_id' => $data['uc_email_id'],
                                'mobile_no' => $data['uc_mobile'],
                                'shipping_address' => $data['uc_address'],
                                'pincode' => $data['uc_pincode'],
                                'city_name' => $data['uc_city'],
                                'state_name' => $data['uc_state'],
                                'mihpayid' => $data['mihpayid'],
                                'txnid' => $data['txnid'],
                                'order_status' => $order_status,
                                'cod_amount' => $final_cod_charges,
                                'shipping_amount' => $final_shipping_charges,
                            );
							//echo  $final_cod_charges."coming1";
                        $order_id = $this->sc_place_order($order_info); $product_total =0;
                        $order_list[] = $order_id; 
						$final_cod_charges = 0;
                        $final_shipping_charges = 0;

                        if(!empty($product_info)){
                            foreach($product_info as $value){
                                $pro_data = array();
                                $pro_data = array('order_id'=>$order_id,'product_id'=>$value['product_id'],'user_id'=>$user_id,'created_datetime'=>date('Y-m-d H:i:s'),'product_size'=>$value['product_size'],'product_qty'=>$value['product_qty'],'product_price'=>$value['price'],'cart_id'=>$value['cart_id']);
                                $sc_o_id = $this->sc_orders_product($pro_data);
                                if($order_status == 1){$this->update_order_cart_info($value['cart_id'],$order_id,$user_id);}
                            }
                        }
                        
                        $product_info = [];
                    }
                    //$brand_total = 0;
                    $old_brand = $new_brand;
                    $old_store_tax = $new_store_tax;
                    //$brand_total = $brand_total+$val['price'];

                    $product_info[$i]['product_id'] = $val['product_id'];
                    $product_info[$i]['user_id'] = $user_id;
                    $product_info[$i]['created_datetime'] = date('Y-m-d H:i:s');
                    $product_info[$i]['product_size'] = $val['product_size'];
                    $product_info[$i]['product_qty'] = $val['product_qty'];
                    $product_info[$i]['price'] = $val['price'];
                    $product_info[$i]['cart_id'] = $val['id'];
                    $product_total = $product_total+$val['price'];
                    $brand_total = $product_total;
                }else{
                    //$brand_total = $brand_total+$productPrice;
                    $old_brand = $new_brand;
                    $old_store_tax = $new_store_tax;

                    $product_info[$i]['product_id'] = $val['product_id'];
                    $product_info[$i]['user_id'] = $user_id;
                    $product_info[$i]['created_datetime'] = date('Y-m-d H:i:s');
                    $product_info[$i]['product_size'] = $val['product_size'];
                    $product_info[$i]['product_qty'] = $val['product_qty'];
                    $product_info[$i]['price'] = $val['price'];
                    $product_info[$i]['cart_id'] = $val['id'];
                    $product_total = $product_total+$val['price'];
                    $brand_total = $product_total;

                }
                $i++;
            }
            if($brand_total!=0){ 
                $tax = $brand_total*($old_store_tax/100); 
                $order_amount = $brand_total+($brand_total*($old_store_tax/100)); 

                /* COD CALCULAION START */
                
                        if($this->getBrandExtraInfo($old_brand,'is_cod')[0]['is_cod'] == 1 && $data['paymod'] == 'cod'){
                            if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0){
                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ; 
                                $order_amount = $order_amount + $final_cod_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] >= 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] > 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']<=$product_total && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']>=$product_total) {


                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                                $order_amount = $order_amount + $final_cod_charges;
                            }else { $old_store_cod_charges = 0; }
                        }else{ $old_store_cod_charges = 0; }
                        /* COD CALCULAION END */

                                 /* SHIPPING CHARGES START*/
                        if($this->getBrandExtraInfo($old_brand,'is_shipping')[0]['is_shipping'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] == 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] == 0){


                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] >= 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] > 0 && $this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values']<=$product_total && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values']>=$product_total) {

                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;

                            }else { $old_store_shipping_charges = 0; }
                            }else{ $old_store_shipping_charges = 0; }
                        /* SHIPPING CHARGES END*/

                $order_info = array(
                                'user_id' => $user_id,
                                'brand_id' => $old_brand,
                                'order_tax_amount' => $tax,
                                'order_sub_total' => $order_amount,
                                'order_total' => $order_amount,
                                'pay_mode' => $data['paymod']=='cod' ? '1' : '2',
                                'created_datetime' =>date('Y-m-d H:i:s'),
                                'first_name' => $data['uc_first_name'],
                                'last_name' => $data['uc_last_name'],
                                'email_id' => $data['uc_email_id'],
                                'mobile_no' => $data['uc_mobile'],
                                'shipping_address' => $data['uc_address'],
                                'pincode' => $data['uc_pincode'],
                                'city_name' => $data['uc_city'],
                                'state_name' => $data['uc_state'],
                                'mihpayid' => $data['mihpayid'],
                                'txnid' => $data['txnid'],
                                'order_status' => $order_status,
                                'cod_amount' => $final_cod_charges,
                                'shipping_amount' => $final_shipping_charges,
                            );
							//echo  $final_cod_charges."coming2";
                        $order_id = $this->sc_place_order($order_info); $product_total =0;
                        $order_list[] = $order_id; 
						$final_cod_charges = 0;
                        $final_shipping_charges = 0;
                        if(!empty($product_info)){
                            foreach($product_info as $value){
                                $pro_data = array();
                                $pro_data = array('order_id'=>$order_id,'product_id'=>$value['product_id'],'user_id'=>$user_id,'created_datetime'=>date('Y-m-d H:i:s'),'product_size'=>$value['product_size'],'product_qty'=>$value['product_qty'],'product_price'=>$value['price'],'cart_id'=>$value['cart_id']);
                                $sc_o_id = $this->sc_orders_product($pro_data);
                                 if($order_status == 1){$this->update_order_cart_info($value['cart_id'],$order_id,$user_id);}
                            }
                        }
                        $product_info = [];

                        

            }
            if(!empty($order_list)){
				//$this->update_user_address($data,$user_id);
               return $this->update_order_unique_no($order_list,$user_id,$order_status);
			   
				
            }
        }else{
           // return 'Your cart is empty';
           // return '{"order" : "fail", "msg" : "Your cart is empty"}';
		   return 2;
        }

    }

    function sc_place_order($data){
        if(!empty($data)){
            $this->db->insert('order_info', $data); 
            return $this->db->insert_id();
        }
    }

    function sc_orders_product($data){
        if(!empty($data)){
            $this->db->insert('order_product_info', $data); 
            return $this->db->insert_id();
        }
    }

    function getMyOrders(){
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }

        //$query = $this->db->order_by('id','DESC')->get_where('order_info', array('user_id' => $user_id));

         $query = $this->db->query('select a.`id`, a.`user_id`, a.`brand_id`,a.`order_unique_no`, a.`order_tax_amount`, a.`cod_amount`, a.`order_sub_total`, a.`order_total`, a.`pay_mode`, a.`created_datetime`, a.`modified_datetime`, a.`order_status`, a.`first_name`, a.`last_name`, a.`email_id`, a.`mobile_no`, a.`shipping_address`, a.`pincode`, a.`city_name`, a.`state_name`,c.`state_name`,b.`company_name` from order_info as a,brand_info as b,states as c where a.`brand_id` = b.`user_id` and a.`state_name` = c.`id` and a.`user_id` = "'.$user_id.'" group by a.`order_unique_no` order by a.`id` desc');

        $result = $query->result_array();
        return $result;
    }

    function getMyProductsOrders(){
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }

        $query = $this->db->query('select a.`id`,c.`name`,c.`price`,b.product_qty,b.product_size,d.size_text as size_text from `order_info` as a,`order_product_info` as b,`product_desc` as c,`product_size` as d where 1 and a.`id` = b.`order_id` and b.`product_id` = c.`id` and a.`user_id` = "'.$user_id.'" and d.`id` = b.product_size order by a.`id` desc');

        $result = $query->result_array();
        return $result;
    }

    function update_order_unique_no($order_list,$user_id,$order_status){
        $order_info = '';
        if(!empty($order_list)){
            $i = 0;
            foreach ($order_list as $key => $value) {
                if($i!=0){ $order_info = $order_info.'_'; }
                $order_info = $order_info.$value;
                $i++;
            }
			$order_unique_no = $user_id.'_'.$order_info;
            $data = array(
                'order_unique_no' => $user_id.'_'.$order_info
            );

            $this->db->where_in('id', $order_list);
            $this->db->update('order_info', $data); 
		// $query = $this->db->query("select * from order_info where order_unique_no LIKE '$order_unique_no' ");

        // $result = $query->result_array();
		$result = $this->get_order($order_unique_no,$user_id,$order_status);
		//print_r($result);
        return $result;
			
        }

    }
	
	function get_order_info($uniquecookie){
        
        $query = $this->db->query('select order_id,user_id from `cart_info` where SCUniqueID = "'.$uniquecookie.'"');
        $order_id = $query->result_array(); 
		
        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['order_id'];
                $user_id = $val['user_id'];
             }

            $order_ids = implode(",",$order_info); 

            $query = $this->db->query('select a.`order_id`, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`, a.`product_price`,b.`name`,b.`image`,b.`brand_id`,c.size_text from order_product_info as a,product_desc as b,product_size as c where a.`product_id` = b.`id` and a.`order_id` in ('.$order_ids.') and a.`user_id`='.$user_id.' and a.product_size = c.`id`' );

            $query_res = $query->result_array();
            return $query_res;

        }
    }
	
	function get_order_price_info($uniquecookie){
        $query = $this->db->query('select order_id,user_id from `cart_info` where SCUniqueID = "'.$uniquecookie.'"');
        $order_id = $query->result_array(); 

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['order_id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info); 

        $query = $this->db->query('select sum(`order_tax_amount`) as order_tax_amount, sum(`cod_amount`) as cod_amount, sum(`order_sub_total`) as order_sub_total, sum(`order_total`) as order_total,sum(shipping_amount) as shipping_amount from order_info where id in ('.$order_ids.')' );

            $query_res = $query->result_array();
            return $query_res;
        }
    }
		// $order_id = explode(',',$order_unique_no);
		// $where_condition = implode(" or order_id = ",$order_id);
		
	function chkShipping($product_id,$pincode){
        $query = $this->db->query('select a.`id` from product_desc as a,brand_info as b where a.`brand_id`= b.`user_id` and a.`id`="'.$product_id.'" and `shipping_exc_location` like "%'.$pincode.'%"' );
        $query_res = $query->result_array();
        return $query_res;
    }
	
	function chkCod($product_id,$pincode){
        $query = $this->db->query('select a.`id` from product_desc as a,brand_info as b where a.`brand_id`= b.`user_id` and a.`id`="'.$product_id.'" and `cod_exc_location` like "%'.$pincode.'%"' );

        $query_res = $query->result_array();
        return $query_res;
    }
	
	function chkShipping2($product_id,$pincode){
		// $product_id = explode(',',$product_id);
		$count = count($product_id);
		//echo $count;echo $product_id[0]['product_id'];print_r($product_id);exit;
		$product_unavailable = array();
		for($i=0;$i<$count;$i++){
			$query = $this->db->query('select a.`id` from product_desc as a,brand_info as b where a.`brand_id`= b.`user_id` and a.`id`="'.$product_id[$i]['product_id'].'" and `shipping_exc_location` like "%'.$pincode.'%"' );
			$query_res = $query->result_array();
			if($query_res != NULL) $product_unavailable[] = $query_res[0];
		}
       //print_r($product_unavailable);exit;
        return $product_unavailable;
    }
	
	function chkCod2($product_id,$pincode){
		$count = count($product_id);
		$product_unavailable = array();
		for($i=0;$i<$count;$i++){
        $query = $this->db->query('select a.`id` from product_desc as a,brand_info as b where a.`brand_id`= b.`user_id` and a.`id`="'.$product_id[$i]['product_id'].'" and `cod_exc_location` like "%'.$pincode.'%"' );

        $query_res = $query->result_array();
		if($query_res != NULL) $product_unavailable[] = $query_res[0];
		}
		//print_r($product_unavailable);exit;
        return $product_unavailable;
    }
	function get_order($order_unique_no,$userid,$order_status = 1){	
		$order_status_query = " AND order_status = $order_status";
		$query = $this->db->query("select id,user_id,order_unique_no,brand_id,first_name,pay_mode as payment_method,created_datetime,order_status,shipping_address,order_tax_amount,cod_amount,order_total from order_info where `order_unique_no` LIKE '$order_unique_no' AND user_id = $userid ");
		$res = $query->result_array();
		//print_r($res);exit;
		$order_id = explode('_',$order_unique_no);
		$total_order = array(0);
		if(!empty($res)){
				
				foreach($res as $val){
					$orderobject = new stdClass(); 
					$orderobject->id = $val['id'];
					$orderobject->user_id = $val['user_id'];
					$orderobject->first_name = $val['first_name'];					
					$orderobject->shipping_address = $val['shipping_address'];
					$orderobject->order_tax_amount = $val['order_tax_amount'];
					$orderobject->order_no = $val['order_unique_no'];
					$orderobject->cod_amount = $val['cod_amount'];
					$total_order[] = $val['order_total'];
					if($val['payment_method'] == 1){
						$orderobject->payment_method = 'COD';
					}
					else{
						$orderobject->payment_method = 'online';
					}
					//$orderobject->payment_method = $val['payment_method'];
					$orderobject->created_datetime = $val['created_datetime'];
					$orderobject->order_status = $val['order_status'];
					$order_id = explode('_',$val['order_unique_no']);
					//$item_count = count($order_id)-1;
					$orderobject->totals = (object)$this->totals($val['order_unique_no'],$order_status);	
					$orderobject->shipping_address = (object)$this->shipping_address($val['id'],$order_status);
					$orderobject->item_qty= $this->product_quantity_count($val['order_unique_no']);
					$orderobject->brand_totals = $this->brand_totals2($val['order_unique_no']);
					// $orderobject->item_count= $item_count;
					//$orderobject->brand_details = $this->brand_details($val['brand_id']);
					
					
					//$i++;
				}
				$orderobject1[] = $this->product_details_three($val['order_unique_no'],'');
					
			}
		//echo array_sum($total_order);exit;
		
		if(!empty($total_order) && array_sum($total_order)!=0) $orderobject->order_total = array_sum($total_order); else  $orderobject->order_total = 0;
		$orderobject->item_count= (string)count($orderobject1);
		$orderobject->product_details = $orderobject1[0];
		//print_r($orderobject);exit;
		return $orderobject;
		
	}
	
	function brand_totals($order_id){
		$order_ids = explode('_',$order_id);
		$order_idhtml = '';
		if(!empty($order_ids)){
			for($i = 1; $i<count($order_ids); $i++){
					if($i != 1){ $order_idhtml = $order_idhtml.','; }
					$order_idhtml = $order_idhtml.$order_ids[$i];
			}
		}
		$query = $this->db->query("SELECT a.user_id as brand_id,a.company_name,IF(SUM(c.product_price)<a.code_max_value, a.cod_charges , 0) as cod_charges,IF(SUM(c.product_price)<a.shipping_max_values, a.shipping_charges , 0) as shipping_charges from brand_info a,product_desc b,order_product_info c WHERE c.product_id = b.id AND b.brand_id = a.user_id AND c.order_id IN ($order_idhtml) GROUP BY c.order_id");
		$res = $query->result_array();
		return $res;
	}
	
	function brand_totals2($order_id){
		
		if($order_id!=''){
			$query = $this->db->query("SELECT a.brand_id,b.company_name,a.order_tax_amount as tax_charges,a.cod_amount as cod_charges,a.shipping_amount as shipping_charges,a.`order_total` as order_total from order_info as a,brand_info as b where a.brand_id = b.user_id and a.order_unique_no ='".$order_id."' ");
			$res = $query->result_array();
		}
		return $res;
	}
	
	
	function brand_details($brand_id){
		//$query = $this->db->query("SELECT user_id as brand_id,company_name,cod_charges,shipping_charges,cod_available as cod_available FROM `brand_info` WHERE `user_id` = $brand_id");
		$query = $this->db->query("SELECT user_id as brand_id,company_name,cod_charges,shipping_charges FROM `brand_info` WHERE `user_id` = $brand_id");
		$res = $query->result_array();
		//print_r($res);exit;
		return $res[0];
		
	}
	
	function product_details_three($order_id,$brand_id){
		$order_ids = explode('_',$order_id);
		$order_idhtml = '';
		if(!empty($order_ids)){
			for($i = 1; $i<count($order_ids); $i++){
					if($i != 1){ $order_idhtml = $order_idhtml.','; }
					$order_idhtml = $order_idhtml.$order_ids[$i];
			}
		}
		//$query = $this->db->query("select op.product_id,op.product_price,op.product_qty,op.product_size,p.image,p.name,p.id,p.brand_id from order_product_info as op,product_desc as p where p.id=op.product_id and order_id IN ($order_idhtml)");
		$query = $this->db->query("select z.product_id,z.product_price,z.product_size,z.product_qty,z.image,z.name,z.id,z.brand_id,y.min_del_days,y.max_del_days from (select op.product_id,op.product_price,op.product_size,op.product_qty,p.image,p.name,p.id,p.brand_id from order_product_info as op INNER JOIN product_desc as p ON p.id=op.product_id where op.order_id IN ($order_idhtml)) as z,brand_info as y where z.brand_id = y.user_id ");
		$res = $query->result_array();
		$product_array = array();

		if(!empty($res)){ $k = 0;
			foreach($res as $val){
				$orderobject = new stdClass(); 
				$deliver_message = '';
				$orderobject->product_id = $val['product_id'];
				$orderobject->product_price = $val['product_price'];
				$orderobject->product_qty = $val['product_qty'];
				$orderobject->product_size = $this->get_size_text($val['product_size']);
				$orderobject->image = 'https://www.stylecracker.com/sc_admin/assets/products/'.$val['image'];
				$orderobject->name = $val['name'];
				/*if(($val['min_del_days'] != '' && $val['max_del_days'] != '') || ($val['min_del_days'] != NULL && $val['max_del_days'] != NULL)) {
						$deliver_message = 'Delivery in '.$val['min_del_days'].'-'.$val['max_del_days'].' day(s)';
					}
					else if(($val['min_del_days'] != '') || ($val['min_del_days'] != NULL)){
						$deliver_message = 'Delivery in '.$val['min_del_days'].'  day(s)';
					}
					else if(($val['max_del_days'] != '') || ($val['max_del_days'] != NULL)){
						$deliver_message = 'Delivery in '.$val['max_del_days'].' day(s)';
					}*/
				if($val['min_del_days']>0 && $val['max_del_days']>0){
						$deliver_message = 'Delivery in '.$val['min_del_days'].'-'.$val['max_del_days'].' working days.';
					}else if(($val['min_del_days']>0 && $val['max_del_days']=='') || ($val['min_del_days']=='' && $val['max_del_days']>0)){
						$deliver_message = 'Delivery in '.$val['min_del_days'].$val['max_del_days'].' working days.';
					}
				//$orderobject->deliver_message = $deliver_message;
				$message = array('text' => $deliver_message, 'color' => '#dd4b39') ;
				$orderobject->message = $message;	
				$orderobject->brand_details = $this->brand_details($val['brand_id']);
				//$orderobject->brand_details = $this->brand_details($val['brand_id']);
				$product_array[]= $orderobject;
				$k++;
			}
				
		}
		
		return $product_array;
		
	}
	
	function get_size_text($size_id){
		$query = $this->db->query("SELECT size_text FROM `product_size` where id = $size_id");
		$res = $query->result_array();
		return $res[0]['size_text'];
		
	}
	
	function product_details($order_id,$brand_id){
		//$order_id = explode('_',$order_unique_no);
		//$where_condition = implode(" or order_id = ",$order_id);
		$query = $this->db->query("select op.product_id,op.product_price,op.product_qty,op.product_size,p.image,p.name,p.id,p.brand_id from order_product_info as op INNER JOIN product_desc as p ON p.id=op.product_id where order_id = $order_id");
		$res = $query->result_array();
		
		$product_array = array();

		if(!empty($res)){
			foreach($res as $val){
				$orderobject = new stdClass(); 
				$orderobject->product_id = $val['product_id'];
				$orderobject->product_price = $val['product_price'];
				$orderobject->product_qty = $val['product_qty'];
				$orderobject->product_size = $val['product_size'];
				$orderobject->image = 'https://www.stylecracker.com/sc_admin/assets/products/'.$val['image'];
				$orderobject->name = $val['name'];
				
				$orderobject->brand_details = $this->brand_details($val['brand_id']);
				//$orderobject->brand_details = $this->brand_details($val['brand_id']);
				//$product_array= $orderobject;
				//$i++;
			}
				
		}
		//print_r($res);exit;
		return $orderobject;
		
	}
	
	function get_order_list($userid,$limit,$offset){
		if($limit == '') $query_limit = ''; else $query_limit = " LIMIT $limit ";
		if($offset == '') $query_offset = ''; else $query_offset = " OFFSET $offset ";	 
		$query = $this->db->query("select id,user_id,order_unique_no,brand_id,first_name,shipping_address,order_status,created_datetime,IF(pay_mode=1,'cod','online') as payment_method,order_tax_amount,cod_amount,order_total from order_info where user_id = $userid  GROUP BY order_unique_no $query_limit $query_offset ");
		$res = $query->result_array();
		//print_r($res);exit;
		if(!empty($res)){
				
				$i = 0;
				$unique_no = 0;
				$array_sum = '';
				foreach($res as $val){
					$orderobject = new stdClass();
					$orderobject->id = $val['id'];
					$orderobject->payment_method = $val['payment_method'];
					$orderobject->created_datetime = $val['created_datetime'];
					$orderobject->order_status = $val['order_status'];
					/*if($val['order_status'] == '1' || $val['order_status'] == '9'){
						$orderobject->order_status = 'Processing';
					}else if($val['order_status'] == '2') $orderobject->order_status = 'Confirmed';
					else if($val['order_status'] == '3') $orderobject->order_status = 'Dispatched';
					else if($val['order_status'] == '4') $orderobject->order_status = 'Delivered';
					else if($val['order_status'] == '5') $orderobject->order_status = 'Cancelled';
					else if($val['order_status'] == '6') $orderobject->order_status = 'Return';*/
					$order_id = explode('_',$val['order_unique_no']);
					$item_count = count($order_id)-1;
					$orderobject->item_qty= $this->product_quantity_count($val['order_unique_no']);
					$orderobject->item_count= (string)$item_count;
					$orderobject->user_id = $val['user_id'];
					$orderobject->first_name = $val['first_name'];	
					$orderobject->totals = (object)$this->totals($val['order_unique_no']);
					$orderobject->brand_totals = $this->brand_totals2($val['order_unique_no']);					
					$orderobject->shipping_address = (object)$this->shipping_address($val['id']);
					$orderobject->order_tax_amount = $val['order_tax_amount'];
					$orderobject->cod_amount = $val['cod_amount'];
					//$orderobject->order_total = $val['order_total']+$array_sum;
					
					if($unique_no == $val['order_unique_no']){
						
						$total_order[1] = $val['order_total'];
						$unique_no = 0;
						$array_sum = array_sum($total_order);
						$skip = 1;
					}else{
						$unique_no = $val['order_unique_no'];
						$total_order[0] = $val['order_total'];
						$array_sum = '';
						$skip=0;
					}
					//$orderobject->order_total = $array_sum;
					$orderobject->order_no = $val['order_unique_no'];
					
					//$orderobject->brand_details = $this->brand_details($val['brand_id']);
					if($skip ==0){
						$orderobject1 = (array) $this->product_details2($val['order_unique_no'],$val['brand_id']);
						$orderobject = (array) $orderobject;
						$final_oder[$i] = array_merge($orderobject,$orderobject1);
						$i++;
					}
					
				}
					
			}
		//echo array_sum($total_order);
		//print_r($total_order);exit;
		
		//$orderobject->order_total = array_sum($total_order);
		//$orderobject = $orderobject1;
		if(!empty($final_oder)){
			return $final_oder;
		}
		else return '';
		
	}
	
	function totals($order_unique_no, $order_status=1){
		//echo "coming".$order_unique_no;
		$order_status_query = " AND order_status = $order_status";
		$query = $this->db->query("select sum(order_sub_total) as sub_total,sum(cod_amount) as cod,sum(order_total) as grand_total,sum(order_tax_amount) as tax,sum(order_total) as grand_total,sum(shipping_amount) as shipping_charges from order_info where order_unique_no like '%$order_unique_no%' ");
		
		$res = $query->result_array();
		// print_r($res);exit;
		return $res[0];
	}
	
	function shipping_address($id, $order_status=1){
		$order_status_query = " AND order_status = $order_status";
		/*$query = $this->db->query("select user_id,first_name,last_name,mobile_no,shipping_address,pincode,city_name,state_name,email_id as email from order_info where id = $id");*/
		$query = $this->db->query("select user_id,first_name,last_name,mobile_no,shipping_address,pincode,city_name,(select b.`state_name` from `city` as a,`states` as b,pincode as c,order_info as d where a.`state_id` = b.`id` AND a.id = c.city_id AND  c.`pincode`=d.pincode AND d.id = $id limit 0,1) as state_name,email_id as email from order_info where id = $id");
		$res = $query->result_array();
		return $res[0];
	}
	
	function product_details2($order_unique_no,$brand_id){
		$order_id = explode('_',$order_unique_no);
		$where_condition = implode(" or order_id = ",$order_id);
		//echo $where_condition;exit;d.min_del_days,d.max_del_days
		$query = $this->db->query("select z.product_id,z.product_price,z.product_size,z.product_qty,z.image,z.name,z.id,z.brand_id,y.min_del_days,y.max_del_days from (select op.product_id,op.product_price,op.product_size,op.product_qty,p.image,p.name,p.id,p.brand_id from order_product_info as op INNER JOIN product_desc as p ON p.id=op.product_id where op.order_id = $where_condition) as z,brand_info as y where z.brand_id = y.user_id ");
		$res = $query->result_array();
		//echo "<pre>"; print_r($res);exit;
		$product_array = array();
		$i=0; 
		if(!empty($res)){
			foreach($res as $val){
				$orderobject = new stdClass(); 
				$orderobject->product_id = $val['product_id'];
				$orderobject->product_price = $val['product_price'];
				$orderobject->product_qty = $val['product_qty'];
				$orderobject->product_size = $this->get_size_text($val['product_size']);
				$orderobject->image = 'https://www.stylecracker.com/sc_admin/assets/products/'.$val['image'];
				$orderobject->name = $val['name'];
				$deliver_message= '';
				/*if(($val['min_del_days'] != '' && $val['max_del_days'] != '') || ($val['min_del_days'] != NULL && $val['max_del_days'] != NULL)) {
						$deliver_message = ''.$val['min_del_days'].'-'.$val['max_del_days'].' day(s)';
					}
					else if(($val['min_del_days'] != '') || ($val['min_del_days'] != NULL)){
						$deliver_message = ''.$val['min_del_days'].'  day(s)';
					}
					else if(($val['max_del_days'] != '') || ($val['max_del_days'] != NULL)){
						$deliver_message = ''.$val['max_del_days'].' day(s)';
					}*/
					if($val['min_del_days']>0 && $val['max_del_days']>0){
						$deliver_message = 'Delivery in '.$val['min_del_days'].'-'.$val['max_del_days'].' working days.';
					}else if(($val['min_del_days']>0 && $val['max_del_days']=='') || ($val['min_del_days']=='' && $val['max_del_days']>0)){
						$deliver_message = 'Delivery in '.$val['min_del_days'].$val['max_del_days'].' working days.';
					}
				$message = array('text' => $deliver_message, 'color' => '#dd4b39') ;
				$orderobject->message = $message;	
				$orderobject->brand_details = $this->brand_details($val['brand_id']);
				//$orderobject->brand_details = $this->brand_details($val['brand_id']);
				$product_array[$i] = $orderobject;
				$i++;
			}
		}
		$product_info = new stdClass();
		$product_info->product_info = $product_array;
		//print_r($product_array);exit;
		return $product_info;
	}
	
	function change_status($userid,$order_id,$status){
		$data = array(
                   'order_status' => $status  
                ); 
		$this->db->where(array('user_id'=>$userid,'order_unique_no' => $order_id));
		$this->db->update('order_info', $data);	
		$result = $this->get_order($order_id,$userid);
		if(!empty($result)){
			return $result;
		}else {
			return 2;
		}
	}
	
	function product_quantity_count($order_unique_no){
		$order_id = explode('_',$order_unique_no);
		$where_condition = implode(" or order_id = ",$order_id);
		$query = $this->db->query("select sum(product_qty) as product_qty from order_product_info where order_id = $where_condition");
		$res = $query->result_array();
		return $res[0]['product_qty'];
	}
	
	function update_user_address($data,$user_id){
		//echo $user_id;print_r($data);exit;
        $query = $this->db->get_where('user_address', array('user_id' => $user_id,'shipping_address'=>$data['uc_address'],'pincode'=>$data['uc_pincode'],'city_name'=>$data['uc_city'],'state_name'=>$data['uc_state'],'first_name'=>$data['uc_first_name'],'mobile_no'=>$data['uc_mobile']));
        $res = $query->result_array();
        if(empty($res)){
            $add_data = array(
                    'user_id'=>$user_id,
                    'shipping_address'=>$data['uc_address'],
                    'pincode'=>$data['uc_pincode'],
                    'city_name'=>$data['uc_city'],
                    'state_name'=>$data['uc_state'],
                    'first_name'=>$data['uc_first_name'],
                    'mobile_no'=>$data['uc_mobile'],
                );
            $this->db->insert('user_address', $add_data);

        }else{
            $add_data = array(
                    'user_id'=>$user_id,
                    'shipping_address'=>$data['uc_address'],
                    'pincode'=>$data['uc_pincode'],
                    'city_name'=>$data['uc_city'],
                    'state_name'=>$data['uc_state'],
                    'first_name'=>$data['uc_first_name'],
                    'mobile_no'=>$data['uc_mobile'],
                );
            $this->db->where(array('id'=>$data['existing_shipping_add'],'user_id'=>$user_id));
            $this->db->update('user_address', $add_data);
        }
    }
	
	function add_address($data){
			$add_data = array(
                    'user_id'=>$data['user_id'],
                    'shipping_address'=>$data['uc_address'],
                    'pincode'=>$data['uc_pincode'],
                    'city_name'=>$data['uc_city'],
                    'state_name'=>$data['uc_state'],
                    'first_name'=>$data['uc_first_name'],
					'email'=>$data['uc_email_id'],
                    'mobile_no'=>$data['uc_mobile'],
                    'last_name'=>$data['uc_last_name'],
                );
            $this->db->insert('user_address', $add_data);
			$insert_id = $this->db->insert_id();
		$query = $this->db->query("select * from user_address where id = $insert_id ");	
		$res = $query->result();
		if(!empty($res)) return $res[0]; else NULL;
		// return $res[0];
	}
	/*
	function getaddress($user_id){
		$query = $this->db->query("select * from user_address where user_id = $user_id ");
		$res = $query->result_array();
		return $res;
	}*/
	
	function getaddress($user_id){
		$query = $this->db->query("select * from user_address where user_id = $user_id ");
		$res = $query->result_array();
		$i=0;
		if(!empty($res)){
			foreach($res as $val){
				$addressobject = new stdClass();
				$addressobject->id = $val['id'];
				$addressobject->user_id = $val['user_id'];
				$addressobject->first_name = $val['first_name'];
				$addressobject->last_name = $val['last_name'];
				$addressobject->mobile_no = $val['mobile_no'];
				$addressobject->shipping_address = $val['shipping_address'];
				$addressobject->pincode = $val['pincode'];
				//$addressobject->city_name = $this->get_state_name($val['city_name']);
				$addressobject->city_name = $val['city_name'];
				//$addressobject->state_name = $val['state_name'];
				$state = $this->get_city_name($val['pincode']);
				if(!empty($state)){ $uc_state = trim($state['state']);
				}
				$addressobject->state_name =$uc_state;
				$addressobject->email = $val['email'];
				$addressobject->created_datetime = $val['created_datetime'];
				$address_array[$i] = $addressobject;
				$i++;
			}		
		}
		//print_r($address_array);exit;
		if(!empty($address_array))
		{
			return $address_array;
		}else return NUll;
	}
	
	function get_state_name($id){
		$query = $this->db->query("select state_name from states where id = $id ");
		$res = $query->result_array(); 
		if(!empty($res)){
			return $res[0]['state_name'];
		}else 
		return NULL;
	}
	
	function user_info($id){
		$query = $this->db->query("select * from user_address where id = $id ");
		$res = $query->result_array();
		return $res[0];
	}
	function product_info($product_id){
		$query = $this->db->query("select * from product_desc where id = $product_id ");
		$res = $query->result_array();
		return $res;
	}
	
	function notify_me($data){
			$add_data = array(
                    'user_id'=>$data['user_id'],
					'product_id'=>$data['product_id'],
                );
			// $product_click_data = array(
                    // 'user_id'=>$data['user_id'],
					// 'product_id'=>$data['product_id'],
					// 'click_count'=>1,
					// 'click_source'=>3,	
                // );
			// $this->db->insert('users_product_click_track', $product_click_data);
            $this->db->insert('notify_me', $add_data);
			$insert_id = $this->db->insert_id();
			$query = $this->db->query("select * from notify_me where id = $insert_id ");	
			$res = $query->result();
			return $res[0];
	}
	
	function get_product_id($user_id){
		$query = $this->db->query("select product_id from cart_info where user_id = $user_id and `order_id` is null");
		$res = $query->result_array();
		return $res;
	}
	
	function su_get_order_price_info($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where order_unique_no = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info);

        $query = $this->db->query('select sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no, a.shipping_address,a.pincode,a.city_name,s.state_name from order_info as a,states as s where a.`state_name` = s.`id` and a.id in ('.$order_ids.')' );

            $query_res = $query->result_array();
            return $query_res;
        }
    }
	
	function su_get_order_info($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where order_unique_no = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

            $order_ids = implode(",",$order_info);

            $query = $this->db->query('select a.`order_id`, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`, a.`product_price`,b.`name`,b.`image`,b.`brand_id`,c.size_text,d.company_name from order_product_info as a,product_desc as b,product_size as c,`brand_info` as d where a.`product_id` = b.`id` and a.`order_id` in ('.$order_ids.') and a.`user_id`='.$user_id.' and a.product_size = c.`id` and d.`user_id` = b.`brand_id`' );

            $query_res = $query->result_array();
            return $query_res;

        }
    }
	
	function br_get_order_info($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where id = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

            $order_ids = implode(",",$order_info);

            $query = $this->db->query('select a.`order_id`, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`, a.`product_price`,b.`name`,b.`image`,b.`brand_id`,c.size_text,d.company_name from order_product_info as a,product_desc as b,product_size as c,`brand_info` as d where a.`product_id` = b.`id` and a.`order_id` in ('.$order_ids.') and a.`user_id`='.$user_id.' and a.product_size = c.`id` and d.`user_id` = b.`brand_id`' );

            $query_res = $query->result_array();
            return $query_res;

        }
    }
	
	function br_get_order_price_info($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where id = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info);

        $query = $this->db->query('select sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no, a.shipping_address,a.pincode,a.city_name,s.state_name,a.brand_id from order_info as a,states as s where a.`state_name` = s.`id` and a.id in ('.$order_ids.')' );

            $query_res = $query->result_array();
            return $query_res;
        }
    }
	
	function get_user_info($user_id){
        $query = $this->db->get_where('user_login', array('id' => $user_id));
        return $query->result_array();
    }
	
	function validate_pincode($pincode){
     $query = $this->db->query("select * from pincode where pincode = $pincode");
	 $res = $query->result_array();
	 if(!empty($res))
	 {return 1;
	}
	else return 2;
    }
	
	function add_product_click($user_id,$product_id,$type){   
	$data = array('user_id'=>$user_id,'product_id' => $product_id,'click_count' => 1,'click_source'=>$type);
	$this->db->insert('users_product_click_track', $data);
    }
	
	function transaction_failed($user_id,$order_unique_no){
		if($user_id != "" && $order_unique_no != ""){
		$order_ids = explode('_',$order_unique_no);
		$order_idhtml = '';
		if(!empty($order_ids)){
			for($i = 1; $i<count($order_ids); $i++){
					if($i != 1){ $order_idhtml = $order_idhtml.','; }
					$order_idhtml = $order_idhtml.$order_ids[$i];
			}
		}
		$query = $this->db->query('update cart_info SET order_id = NULL WHERE order_id IN ('.$order_idhtml.') AND user_id = "'.$user_id.'" ');
		//echo $this->db->last_query();exit;
		$query = $this->db->query('delete from order_product_info WHERE order_id IN ('.$order_idhtml.') AND user_id = "'.$user_id.'" ');
		$query = $this->db->query('delete from order_info WHERE order_unique_no = "'.$order_unique_no.'" AND user_id = "'.$user_id.'"');
		return 1;
		}else return 2;
	}

	function get_state_id($pincode){
		$query = $this->db->query("select b.`id` as state_id from `city` as a,`states` as b,pincode as c where a.`state_id` = b.`id` AND a.id = c.city_id AND  c.`pincode`='".$pincode."'");
        $res = $query->result_array();
		if(!empty($res)){ return $res[0]['state_id']; }else{ return 0; }
	}	
	
}


?>