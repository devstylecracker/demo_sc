<?php

class Mobile_model extends MY_model {
	
	
 function registerUserMobile($firstname,$medium,$medium_id,$scusername,$scemail_id,$scpassword,$gender,$ip,$role){
        /*added by Hari for mobile_facebook and google login*/
        //$new_auth_token = substr(md5(uniqid(rand(1,6))), 0, 15);//added for generate token globally
        $insert = 0;
        $picture = '';
        $facebook_id = '';
        $google_id = '';
        if(isset($medium) && ($medium ==='mobile_facebook' || $medium === 'mobile_google' || $medium === 'mobile_ios_facebook' || $medium === 'mobile_android_facebook')){
            if($medium === 'mobile_google'){
                //$picture = $json_array['picture'];
                $google_id = $medium_id;
            }
            elseif($medium === 'mobile_facebook' || $medium === 'mobile_ios_facebook' || $medium === 'mobile_android_facebook') {
                //$picture = $json_array['picture']['data']['url'];
                $facebook_id = $medium_id;
            }
            if($this->usernameUnique($scusername) != NULL){
                 $scusername = $scusername.rand(1,1000);
            }
            if($this->emailUnique($scemail_id) != NULL){
                 $query = $this->db->query('select id from user_login where email="'.$scemail_id.'"');
                 $result = $query->result_array();
                 $insert = 1;
            }
        }
        if($insert===0){
        /***Code Ends***/
           $new_auth_token = substr(md5(uniqid(rand(1,6))), 0, 15);//added for generate token globally 
           $salt = substr(md5(uniqid(rand(), true)), 0, 10);
           $password   =$salt . substr(sha1($salt . $scpassword), 0, -10);
            $data = array(
               'user_name' => $scusername,
               'email' => $scemail_id,
               'password' => $password,
               'auth_token'=> $new_auth_token,
               'ip_address' => $ip,
               'status' => 1,
               'created_datetime' => $this->config->item('sc_date'),
               'modified_datetime' => $this->config->item('sc_date')

            );
            $this->db->insert('user_login', $data); 
            $Lst_ins_id = $this->db->insert_id();
            if($Lst_ins_id){
                $data = array('gender'=>$gender,'facebook_id'=>$facebook_id,'google_id'=>$google_id,'profile_pic'=>$picture, 'user_id' => $Lst_ins_id, 'first_name' => $firstname,  'created_datetime' => $this->config->item('sc_date'),
               'modified_datetime' => $this->config->item('sc_date'), 'registered_from'=>$medium,'created_by' => $Lst_ins_id, 'modified_by' => $Lst_ins_id);

                $this->db->insert('user_info', $data); 

                $data = array('user_id'=>$Lst_ins_id,'role_id'=>$role,'created_by'=>$Lst_ins_id,'modified_by'=>$Lst_ins_id);

                $this->db->insert('user_role_mapping', $data); 

                $res = $this->login_user_mobile($scemail_id,$scpassword);
                return $res;
                //return true;
            }
        }else{
            /*edit section written by Hari on 08 Sep 2015 */
            $query = $this->db->query("select profile_pic from user_info where user_id = ".$result[0]['id']);
            $res = $query->result();
            if(empty($res[0]->profile_pic) ){
                $data = array('facebook_id'=>$facebook_id,'google_id'=>$google_id,'profile_pic'=>$picture, 'registered_from'=>$medium,'modified_by' => $result[0]['id'],'modified_datetime' => $this->config->item('sc_date'));
            }else{
                $data = array('facebook_id'=>$facebook_id,'google_id'=>$google_id,'registered_from'=>$medium,'modified_by' => $result[0]['id'],'modified_datetime' => $this->config->item('sc_date'));
            }
            $this->db->update('user_info', $data, array('user_id' => $result[0]['id'] ));
           // $new_auth_token = substr(md5(uniqid(rand(1,6))), 0, 15);//added for generate token globally

            $query = $this->db->query("select user_role_mapping.role_id,user_info.user_id,user_info.created_datetime,user_info.first_name,user_info.last_name,user_login.bucket,user_login.question_comp,user_login.question_resume_id from user_info,user_role_mapping,user_login where user_info.user_id = ".$result[0]['id']." and user_info.user_id = user_role_mapping.user_id and user_info.user_id = user_login.id");
            $res = $query->result();
            
            if(!empty($res)){
                // $data = array('auth_token' => $new_auth_token);
                // $this->db->update('user_login', $data, array('id' =>$res[0]->user_id));//update token
                $data_attempts = array('user_id'=>$res[0]->user_id,'login_time'=>$this->config->item('sc_date'));
                $this->db->insert('user_login_attempts', $data_attempts);
                $login_attempts_id = $this->db->insert_id();

               $array_field[] =array('user_id' => $res[0]->user_id,'bucket_mobile'=>$res[0]->bucket,'question_comp_mobile'=>$res[0]->question_comp);
               return ($array_field);
            }else{
                return 2;   
            }
            
        }
    }
	
    function get_users_info_mobile($user_id){
         
         //$user_id = $this->session->userdata('user_id_mobile');
//CASE user_info.profile_cover_pic WHEN null THEN 'http://www.stylecracker.com/assets/images/profile/cover.jpg' WHEN '' THEN 'http://www.stylecracker.com/assets/images/profile/cover.jpg' ELSE 'CONCAT('http://www.stylecracker.com/sc_admin/assets/full_pic/',user_info.profile_cover_pic)' END as category_code,
         $query = $this->db->query("select user_role_mapping.role_id,user_info.user_id,user_info.created_datetime,user_info.first_name,user_info.last_name,user_login.bucket,user_login.auth_token,user_login.question_comp,user_login.question_resume_id,user_info.registered_from,user_info.facebook_id,user_info.google_id,IF(user_info.profile_pic='', IF(user_info.gender = 1,'https://www.stylecracker.com/assets/images/profile/profile-women.jpg','https://www.stylecracker.com/assets/images/profile/profile-men.jpg'), CONCAT('http://www.stylecracker.com/sc_admin/assets/user_profiles/',user_info.profile_pic)) as profile_pic , IF(user_info.profile_cover_pic IS NULL, 'http://www.stylecracker.com/assets/images/profile/cover.jpg', CONCAT('http://www.stylecracker.com/sc_admin/assets/full_pic/',user_info.profile_cover_pic)) as profile_cover_pic from user_info,user_role_mapping,user_login where user_info.user_id = ".$user_id." and user_info.user_id = user_role_mapping.user_id and user_info.user_id = user_login.id");
            $res = $query->result();

            return $res[0];
    }
	
	
	function login_user_mobile($email,$password){
        // $new_auth_token = substr(md5(uniqid(rand(1,6))), 0, 15);//added for generate token globally
        $query = $this->db->get_where('user_login', array('email' => $email,'status'=>1));
        $result = $query->result();
        if(!empty($result)){
        $salt = substr($result[0]->password, 0, 10);
        $db_password =  $salt . substr(sha1($salt . $password), 0, -10); 
        if($db_password == $result[0]->password){
            $query = $this->db->query("select user_role_mapping.role_id,user_info.user_id,user_info.created_datetime,user_info.first_name,user_info.last_name,user_login.bucket,user_login.question_comp,user_login.question_resume_id from user_info,user_role_mapping,user_login where user_info.user_id = ".$result[0]->id." and user_info.user_id = user_role_mapping.user_id and user_info.user_id = user_login.id");
            $res = $query->result();
            if(!empty($res)){
                // $data = array('auth_token' => $new_auth_token);
                // $this->db->update('user_login', $data, array('id' =>$res[0]->user_id));//update token
                $data_attempts = array('user_id'=>$res[0]->user_id,'login_time'=>$this->config->item('sc_date'));
                $this->db->insert('user_login_attempts', $data_attempts);
                $login_attempts_id = $this->db->insert_id();

                $array_field[] =array('user_id' => $res[0]->user_id,'bucket_mobile'=>$res[0]->bucket,'question_comp_mobile'=>$res[0]->question_comp);
                return ($array_field);
                //return 1;
            }else{
                return 2;   
            }
        }else{ 
                return 2;
            }
        }else{ return 2; } 
    }
	
    function get_look_id($user_id,$auth_token)
	{
        $query = $this->db->query("select look_fav.look_id from look_fav where look_fav.user_id = ".$user_id." ");
        $res = $query->result_array();
        return $res;
	}
	
	function get_wishlist_looks($user_id,$limit,$offset)
	{
		//$query = $this->db->query("select look_fav.look_id from look_fav where look_fav.user_id = ".$user_id." ");
		
		//$query = $this->db->query("select a.id,a.name,a.slug,a.description,a.look_type,CONCAT('http://www.stylecracker.com/sc_admin/assets/looks/',a.image) as url,a.created_datetime as timestamp,@type:='look' as type,(select COUNT(look_fav.look_id) from look_fav where look_fav.look_id=a.id) as user_likes,(select COUNT(look_fav.look_id) from look_fav where look_fav.look_id=a.id) as user_likes,(select COUNT(user_share.look_id) from user_share where user_share.look_id=a.id) as user_shares,(select ls.category_name  from look_category_mapping lcm, look_sub_category ls   WHERE a.id = lcm.look_id and lcm.cat_id=ls.id limit 0,1) as look_type_txt from look_fav INNER JOIN look as a where look_fav.user_id = ".$user_id." AND a.id=look_fav.look_id $limit $offset ");
		
$query = $this->db->query('select o.slug,o.id,o.product_img,o.look_type,o.image,o.look_image,name,o.created_datetime as timestamp,o.status,o.deflag,@type:="look" as type from
(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime ,z.status,z.deflag from 
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4) )as o,look_fav as h where o.`id` = h.`look_id` and h.`user_id` = '.$user_id.' order by h.id desc'.$limit.$offset);
		
        $res = $query->result_array();
		
		$looks_array = array();
			
			$i = 0; 	
			if(!empty($res)){
				foreach($res as $val){
	
					$alllooksarrayobject = new stdClass(); 
					$alllooksarrayobject->id = $val['id'];
					$alllooksarrayobject->name = $val['name'];
					$alllooksarrayobject->timestamp = $val['timestamp'];
					if($val['look_type'] == 1){
						$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
					}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
						$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
					}else if($val['look_type'] == 3){
						$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
					}
					$info = getimagesize($alllooksarrayobject->url);
					//if(isset($info) && ($info[1] == NULL || $info[1] == '') ) $info[1] = 0;
					//if(isset($info) && ($info[0] == NULL || $info[0] == '' )) $info[0] = 0;
					$alllooksarrayobject->height = $info[1];
					$alllooksarrayobject->width  = $info[0];
					$alllooksarrayobject->type = $val['type'];
					$alllooksarrayobject->look_type = $val['look_type'];
					$alllooksarrayobject->user_share = $this->get_user_share($val['id']);
					$alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
					$alllooksarrayobject->share_url = 'https://www.stylecracker.com/looks/look-details/'.$val['slug'];
					$alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
					$looks_array[$i] = $alllooksarrayobject;
					$i++;
				}
			}
			
			return $looks_array;
        // return $res;
	}
	
    function login_user_token($user_id, $auth_token){
        $query = $this->db->get_where('user_login', array('id' => $user_id,'auth_token'=>$auth_token));
        $result = $query->result();
        if(!empty($result)){

            return $result;
        }
    }
	 
	
    function forget_user_email_mobile($sc_email_id){
	$query = $this->db->get_where('user_login', array('email'=>$sc_email_id));
	$result = $query->result();
	if(sizeof($result)>0)
        {
            $scpassword =$this->randomPassword_mobile();
            $salt = substr(md5(uniqid(rand(), true)), 0, 10);
            $password   =$salt . substr(sha1($salt . $scpassword), 0, -10);
            $data=array('password'=>$password);
            $this->db->update('user_login', $data, array('email' => $sc_email_id ));

            return  $scpassword;
        }else
        {
            return false;
        } 
	/*if(!empty($result)){
	return 1;
	}else{
	return 2;
	}
	*/
    }

    function single_look_id_mobile($single_look_id){              
       //$query = $this->db->get_where('look', array('id' => $single_look_id));
       // $query = $this->db->query("Select a.id as id,a.name as name,CONCAT('http://www.stylecracker.com/sc_admin/assets/looks/',a.image) as url,a.created_datetime as timestamp,@type:='look' as type,(select count(look_fav.look_id) as likes from look INNER JOIN look_fav where look.id=$single_look_id AND look.id = look_fav.look_id) as like_count, (select count(user_share.look_id) as shares from look INNER JOIN user_share where look.id=$single_look_id AND look.id = user_share.look_id) as share_count,(select look_sub_category.category_name from look JOIN look_category_mapping ON look.id=look_category_mapping.look_id JOIN look_sub_category ON look_category_mapping.cat_id=look_sub_category.id WHERE look.id=$single_look_id) as look_type_txt from look as a where id='$single_look_id'");
       // $result = $query->result();
       // if(!empty($result)){
            // return $result[0];
        // }else{
            // return 2;
        // }
		$query = $this->db->query("select distinct k.id,k.slug,product_img,k.look_type,k.image,look_image,name,k.created_datetime,k.modified_datetime as timestamp,k.status,k.deflag,k.stylist_id,@type:='look' as type from
(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag,z.`stylist_id` from 
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag,a.`stylist_id` FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 and a.id = '".$single_look_id."' ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc) as k,look_bucket_mapping as m where k.id = m.look_id order by k.modified_datetime desc ");

		 //$res = $query->result();
		 
		/* Added by saylee*/
			$res = $query->result_array();
			
			$looks_array = array();
			
			$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
					$alllooksarrayobject = new stdClass(); 
					$alllooksarrayobject->id = $val['id'];
					$alllooksarrayobject->name = $val['name'];
					$alllooksarrayobject->timestamp = $val['timestamp'];
					if($val['look_type'] == 1){
						$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
						$info = getimagesize($alllooksarrayobject->url);
					}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
						$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
						$info = getimagesize($alllooksarrayobject->url);
					}else if($val['look_type'] == 3){
						$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
					}
					$info = getimagesize($alllooksarrayobject->url);
					//if($info[1] == NULL || $info[1] == '' ) $info[1] = 0;
					//if($info[0] == NULL || $info[0] == '' ) $info[0] = 0;
					$alllooksarrayobject->height = $info[1];
					$alllooksarrayobject->width  = $info[0];
					$alllooksarrayobject->type = $val['type'];
					$alllooksarrayobject->look_type = $val['look_type'];
					$alllooksarrayobject->user_share = $this->get_user_share($val['id']);
					$alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
					$alllooksarrayobject->share_url = 'https://www.stylecracker.com/looks/look-details/'.$val['slug'];
					$alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
					$looks_array[$i] = $alllooksarrayobject;
					$i++;
				} 
		}
			
		return $looks_array[0];
		
		
    }

    function looks_by_stylist_id_mobile($stylist_id,$offset,$limit){   
	
	//$query = $this->db->query("select a.id as id,a.look_type,a.name as name,CONCAT('http://www.stylecracker.com/sc_admin/assets/looks/',a.image) as url,a.created_datetime as timestamp,@type:='look' as type,(select count(look_id) from look_fav where `look_id`=a.id) as look_fav,(select count(look_id) from user_share where `look_id`=a.id) as look_share,(select ls.category_name  from look_category_mapping lcm, look_sub_category ls   WHERE a.stylist_id=$stylist_id AND a.id = lcm.look_id and lcm.cat_id=ls.id  LIMIT 0,1) as look_type_txt from look as a  where a.stylist_id=$stylist_id AND deflag=0 order by a.id desc ".$limit.$offset);
	
	
	$query = $this->db->query("select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime as timestamp,z.status,z.deflag,@type:='look' as type from 
	(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 and a.stylist_id=$stylist_id ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime DESC $limit $offset");
	$res = $query->result_array();
	// print_r($res);
	// exit;

	$looks_array = array();
			
			$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
					$alllooksarrayobject = new stdClass(); 
					$alllooksarrayobject->id = $val['id'];
					$alllooksarrayobject->name = $val['name'];
					$alllooksarrayobject->timestamp = $val['timestamp'];
					if($val['look_type'] == 1){
						$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
					}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
						$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
					}else if($val['look_type'] == 3){
						$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
					}
					$info = getimagesize($alllooksarrayobject->url);
					//if($info[1] == NULL || $info[1] == '' ) $info[1] = 0;
					//if($info[0] == NULL || $info[0] == '' ) $info[0] = 0;
					$alllooksarrayobject->height = $info[1];
					$alllooksarrayobject->width  = $info[0];
					$alllooksarrayobject->type = $val['type'];
					$alllooksarrayobject->look_type = $val['look_type'];
					$alllooksarrayobject->user_share = $this->get_user_share($val['id']);
					$alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
					$alllooksarrayobject->share_url = 'https://www.stylecracker.com/looks/look-details/'.$val['slug'];
					$alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
					$looks_array[$i] = $alllooksarrayobject;
					$i++;
				}
			}
			
		//print_r($looks_array);exit;
		
       if(!empty($res)){
            return $looks_array;
        }else{
            return NULL;
        }
    }
	/*
	function single_look_mobile($look_id)
	{
        //, lp.product_id
	 $query = $this->db->query("Select DISTINCT p.id as product_id ,lp.look_id, p.brand_id,p.name,p.url,p.description,p.slug,p.price,p.url as buy_now_url,CONCAT('http://www.stylecracker.com/sc_admin/assets/products/',p.image) as url,(select bi.company_name from brand_info bi where bi.user_id=p.brand_id) AS brand_name, (select bi.redirect_sep from brand_info bi where bi.user_id=p.brand_id) AS redirect_sep from look_products as lp, product_desc as p where p.id = lp.product_id AND p.approve_reject='A' AND look_id =$look_id");
	 $res = $query->result();	
	 if(!empty($res)){
	    return $res;
        }else{
            return NULL;
        }
		
	}*/
	
	function single_look_mobile($look_id)
	{
	 //$query = $this->db->query("Select DISTINCT p.id as product_id ,ps.name as category,lp.look_id, p.brand_id,p.name,p.url,p.description,p.slug,p.price,p.url as buy_now_url,CONCAT('http://www.stylecracker.com/sc_admin/assets/products/',p.image) as url,(select bi.company_name from brand_info bi where bi.user_id=p.brand_id) AS brand_name, (select bi.redirect_sep from brand_info bi where bi.user_id=p.brand_id) AS redirect_sep from look_products as lp, product_desc as p,product_sub_category ps where p.product_sub_cat_id = ps.id AND p.id = lp.product_id AND p.approve_reject='A' AND look_id =$look_id");
	 $query = $this->db->query("Select DISTINCT p.id as product_id ,b.brand_store_name as sold_by,@type:='' as category,lp.look_id, p.brand_id,p.name,p.url,p.description,p.slug,p.price,p.url as buy_now_url,CONCAT('http://www.stylecracker.com/sc_admin/assets/products/',p.image) as url,(select bi.company_name from brand_info bi where bi.user_id=p.brand_id) AS brand_name, (select bi.redirect_sep from brand_info bi where bi.user_id=p.brand_id) AS redirect_sep from look_products as lp, product_desc as p,product_store a,brand_store b where p.id = lp.product_id AND p.approve_reject='A' AND look_id =$look_id AND p.id = a.product_id AND a.store_id = b.id");
	 $res = $query->result_array();
	 
	 $product_array = array();
		$i=0;
		if(!empty($res)){
			foreach($res as $val){
				
				$productobject = new stdClass(); 
				$productobject->product_id = $val['product_id'];
				$productobject->product_category = $val['category'];
				$productobject->look_id = $val['look_id'];
				$productobject->brand_id = $val['brand_id'];
				$productobject->name = $val['name'];
				$productobject->sold_by = $val['sold_by'];
				$productobject->return_policy = 'As Per Brand';
				$productobject->description = $val['description'];
				$productobject->slug = $val['slug'];
				$productobject->price = $val['price'];
				$productobject->buy_now_url = $val['buy_now_url'];
				$productobject->is_available = $this->is_available($val['product_id']);
				$productobject->url = $val['url'];
				$productobject->brand_name = $val['brand_name'];
				//$productobject->redirect_sep = $val['redirect_sep'];
				$size_category = $this->size_category($val['product_id']);
				$productobject->size_category = $size_category['size_guide'];
				$productobject->product_size = $this->product_size($val['product_id']);
				$productobject->color = '';
				$productobject->fabric = '';
				$productobject->primary_color = '';
				$product_array[$i] = $productobject;
				$i++;
			}
				
		}
		
	//print_r($product_array);exit;
	
	 if(!empty($product_array)){
	    return $product_array;
        }else{
            return NULL;
        }
		
	}
	
	function is_available($product_id){
		$query = $this->db->query("SELECT a.stock_count FROM product_inventory a WHERE a.product_id = $product_id ");
		$res = $query->result_array();
		// echo $res[0]['stock_count'];
		// print_r($res);exit;
		if($res != NULL && $res[0]['stock_count']>0)
			return 1;
		else return 0;
		return $res;
	}
	
	function product_size($product_id){       
	$size_array =  $this->db->query("select b.size_text from product_inventory a,product_size b where a.product_id = $product_id And a.size_id = b.id AND a.stock_count>0 GROUP BY b.id");
	$res1 = $size_array->result();
	return $res1;
	}
	
	function size_category($product_id){       
	$size_array =  $this->db->query("select n.`size_guide` from `product_variations` as m,`product_variation_type` as n,product_desc c where m.`product_var_type_id` = n.`id` and m.`product_id` = c.id AND c.id = $product_id order by n.size_guide DESC ");
	$res1 = $size_array->result_array();
	
	if(!empty($res1)) return $res1[0]; else return null;
	}
	
    function user_like_mobile($user_id,$auth_token,$look_id){          
        
        $query = $this->db->query("select user_id,look_id from look_fav where look_id=$look_id AND user_id=$user_id");
		$res = $query->result();
		if($res != NULL){
			$query = $this->db->query(" Delete from look_fav where look_id=$look_id AND user_id=$user_id");
			return 1;
		}else{
			$created_by = date('Y-m-d G:i:s'); 
			$data = array('user_id'=>$user_id,'look_id'=>$look_id,'created_by'=>$created_by);
			$result = $this->db->insert('look_fav', $data);         
			if(!empty($result)){
				return 1;
			}else{
				return 2;
			}
		}
		
    }

    function user_search_mobile($user_search,$limit,$offset){
      
	  if($user_search!= null){
		$count_search = count($user_search);
		for($i=0;$i<$count_search;$i++){
			if($i == 0) $query_search = 'LOWER(a.`name`) LIKE LOWER("%'.urldecode($user_search[$i]).'%")';
			else $query_search .= ' AND LOWER(a.`name`) LIKE LOWER("%'.urldecode($user_search[$i]).'%")';
		}

        // $where_condition = '';
        // $where_condition = $where_condition.' and '.$query_search.'';
        // $query = $this->db->query('select z.id,z.slug,z.look_type,CONCAT("http://www.stylecracker.com/sc_admin/assets/looks/",z.image) as url,name,z.created_datetime as timestamp,z.description,like_count,share_count,look_type_txt from 
           // (SELECT a.slug,a.id,a.stylist_id,a.description,a.referred_count,a.look_time_limit,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,(select count(look_fav.look_id) as likes from look INNER JOIN look_fav where a.id=look_fav.look_id AND look.id = look_fav.look_id ) as like_count,(select count(user_share.look_id) as share from look INNER JOIN user_share where a.id=user_share.look_id AND look.id = user_share.look_id) as share_count,(select look_sub_category.category_name from look JOIN look_category_mapping ON look.id=look_category_mapping.look_id JOIN look_sub_category ON look_category_mapping.cat_id=look_sub_category.id WHERE a.id=look_category_mapping.look_id AND look.id = look_category_mapping.look_id LIMIT 0,1) as look_type_txt FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 AND a.status=1 '.$where_condition.') as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4) order by z.id  desc '.$limit.$offset);
		   
			
			
			$where_condition = '';
			$where_condition = $where_condition.' and '.$query_search.'';
			$query = $this->db->query('select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime as timestamp ,z.status,z.deflag from 
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 and a.status=1 '.$where_condition.') as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4) order by z.id desc '.$limit.$offset);
             $res = $query->result_array();
			   $a = count($res); 

              if($a == 0 ){
            for($i=0;$i<$count_search;$i++){
                        if($i == 0) $query_search = 'LOWER(a.`name`) LIKE LOWER("%'.urldecode($user_search[$i]).'%")';
                        else $query_search .= ' OR LOWER(a.`name`) LIKE LOWER("%'.urldecode($user_search[$i]).'%")';
                    }
            $where_condition = '';
            $where_condition = $where_condition.' and '.$query_search.'';
            $query = $this->db->query('select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime as timestamp ,z.status,z.deflag from 
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 and a.status=1 '.$where_condition.') as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4) order by z.id desc '.$limit.$offset);
             $res = $query->result_array();

              }
			 $looks_array = array();
			
			$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
					$alllooksarrayobject = new stdClass(); 
					$alllooksarrayobject->id = $val['id'];
					$alllooksarrayobject->name = $val['name'];
					$alllooksarrayobject->timestamp = $val['timestamp'];
					if($val['look_type'] == 1){
						$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
						$info = getimagesize($alllooksarrayobject->url);
					}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
						$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
						$info = getimagesize($alllooksarrayobject->url);
					}else if($val['look_type'] == 3){
						$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
					}
					$info = getimagesize($alllooksarrayobject->url);
					//if($info[1] == NULL || $info[1] == '' ) $info[1] = 0;
					//if($info[0] == NULL || $info[0] == '' ) $info[0] = 0;
					$alllooksarrayobject->height = $info[1];
					$alllooksarrayobject->width  = $info[0];
					$alllooksarrayobject->type = 'looks';//$val['type'];added by Hari
					$alllooksarrayobject->look_type = $val['look_type'];
					$alllooksarrayobject->user_share = $this->get_user_share($val['id']);
					$alllooksarrayobject->share_url = 'https://www.stylecracker.com/looks/look-details/'.$val['slug'];
					$alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
					$alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
					$looks_array[$i] = $alllooksarrayobject;
					$i++;
				} 
		}
		return $looks_array;
           //  return $res;
    }
    // else 
       // $query_search = 'LOWER(a.`name`) LIKE LOWER("%'.urldecode($user_search[$i]).'%")';

        // $query = $this->db->query('select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime as timestamp ,z.status,z.deflag from 
// (SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 and a.status=1 ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4),look_tags a where a.look_tags like "%'.$tag.'%" AND a.look_id=z.id order by z.id desc '.$limit.$offset);
        // $res = $query->result_array();
        // return $res;


    }
   
	function get_answer_id($question_id,$user_id){
		
		$query = $this->db->query("select answer_id from users_answer where user_id = $user_id AND question_id = $question_id ");
		$res = $query->result_array();
		if(!empty($res)){
			return $res[0];
		}else return null;
	}   
   
   
	function get_my_feed($user_id,$auth_token,$limit,$offset,$filter_value,$bucket_id,$timestamp,$is_up){
		//if($limit == '') $query_limit = ''; else $query_limit = " LIMIT $limit ";
		//if($offset == '') $query_offset = ''; else $query_offset = " OFFSET $offset ";
		$budget_id_query = '';
		$question_id = '7';
		$budget_id = $this->get_answer_id($question_id,$user_id);
		if(!empty($budget_id)){
			$budget_id_query = ' AND m.budget = '.$budget_id['answer_id'].' ';
		}else {
			$budget_id_query = '';
		}
		if($is_up == 1){
			
			if($timestamp === 0){
				$time_stamp_look = '';
				$time_stamp_look_new = " ";
			}
			  
		    else{ 
			 $time_stamp_look = "AND a.created_datetime>$timestamp ";
			  $time_stamp_look_new = " and a.modified_datetime>$timestamp ";
			}
		}  
		else {
			if($timestamp === 0){
			  $time_stamp_look = '';
			  $time_stamp_look_new = '';
			}
		    else{
			 $time_stamp_look = "AND a.created_datetime<$timestamp ";
			  $time_stamp_look_new = " and a.modified_datetime<$timestamp ";
			}
		}
		/*$query = $this->db->query("select distinct a.id as id,a.name as name,CONCAT('http://www.stylecracker.com/sc_admin/assets/looks/',a.image) as url,a.created_datetime as timestamp,@type:='look' as type,(select COUNT(look_fav.look_id) from look_fav where look_fav.look_id=a.id) as user_likes,(select COUNT(look_fav.look_id) from look_fav where look_fav.look_id=a.id) as user_likes,(select COUNT(user_share.look_id) from user_share where user_share.look_id=a.id) as user_shares,(select ls.category_name  from look_category_mapping lcm, look_sub_category ls   WHERE a.id = lcm.look_id and lcm.cat_id=ls.id limit 0,1) as look_type_txt from look a,look_bucket_mapping as b WHERE a.id=b.look_id AND a.look_type = ".$filter_value." AND b.bucket_id = '".$bucket_id."' $time_stamp_look ORDER BY a.created_datetime DESC $limit $offset");*/
		
	 /*$query = $this->db->query("select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime as timestamp,z.status,z.deflag,@type:='look' as type from u
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 $time_stamp_look_new and a.look_type = $filter_value  ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime DESC $limit $offset");*/
if($filter_value == 6){

        $query = $this->db->query("select z.id, z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime as timestamp,@type:='look' as type  from 
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 $time_stamp_look_new and a.id IN ((select distinct j.look_id from look_category_mapping as j,look_bucket_mapping as m where j.look_id = m.look_id and j.cat_id = 5 and m.bucket_id=$bucket_id $budget_id_query ) )) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc $limit $offset");

        $res = $query->result_array();
		
		$looks_array = $this->looks_array($res);
		return $looks_array;
       
        }else if($filter_value == 7){
//echo $bucket_id;exit;
        $query = $this->db->query("select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,@type:='look' as type,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime as timestamp from (SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 and a.id IN ((select distinct j.look_id from look_category_mapping as j,look_bucket_mapping as m where j.look_id = m.look_id and j.cat_id = 40 and m.bucket_id=$bucket_id $budget_id_query ) )) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc
 $limit $offset");

        $res = $query->result_array();
		//print_r($res);exit;
		$looks_array = $this->looks_array($res);
		return $looks_array;
       
        }//CONCAT('https://www.stylecracker.com/looks/look_details/',k.slug) as k.slug,
		else{
$query = $this->db->query("select distinct k.id, k.slug,product_img,k.look_type,k.image,look_image,name,k.created_datetime,k.modified_datetime as timestamp,k.status,k.deflag,k.stylist_id,@type:='look' as type from
(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag,z.`stylist_id` from 
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag,a.`stylist_id` FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 $time_stamp_look_new and a.look_type = '".$filter_value."') as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc) as k,look_bucket_mapping as m where k.id = m.look_id and m.bucket_id = '".$bucket_id."' $budget_id_query order by k.modified_datetime desc $limit $offset");

		 //$res = $query->result();
		 
		/* Added by saylee*/
			$res = $query->result_array();
			
			$looks_array = $this->looks_array($res);
			return $looks_array;
			
        // return $res;
		}
		
	}
	
	function get_wishlist($look_id,$user_id){
		$query = $this->db->query("select COUNT(look_fav.look_id) as wishlisted_looks from look_fav where look_fav.look_id='".$look_id."' AND look_fav.user_id='".$user_id."' ");
		$res = $query->result_array();
		return $res[0]['wishlisted_looks'];
		
	}
	
	function get_looks_by_brand($brand_id,$limit,$offset){
            //$query = $this->db->query("select product_desc.brand_id, a.id as id,a.name as name,CONCAT('http://www.stylecracker.com/sc_admin/assets/looks/',a.image) as url,a.created_datetime as timestamp,@type:='look' as type,(select COUNT(look_fav.look_id) from look_fav where look_fav.look_id=a.id) as user_likes,(select COUNT(user_share.look_id) from user_share where user_share.look_id=a.id) as user_shares,(select ls.category_name  from look_category_mapping lcm, look_sub_category ls   WHERE a.id = lcm.look_id and lcm.cat_id=ls.id) as look_type_txt from `look_products` inner join look a ON a.id=look_products.look_id JOIN product_desc  ON look_products.product_id = product_desc.id  WHERE product_desc.brand_id= ".$brand_id." AND a.status=1 order by a.modified_datetime desc ".$limit.$offset);
			$query = $this->db->query("select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime as timestamp,@type:='look' as type from 
 (SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN (select distinct u.`look_id` from `look_products` as u,`product_desc` as i where u.`product_id` = i.`id` and i.`brand_id` = $brand_id )) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4|| z.look_type = 6) order by z.modified_datetime DESC $limit $offset");

			//$query = $this->db->query("select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime as timestamp,z.status,z.deflag,@type:='look' as type from 
			//(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime DESC $limit $offset");

           $res = $query->result_array();
		   
		   $looks_array = array();
			
			$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
					$alllooksarrayobject = new stdClass(); 
					$alllooksarrayobject->id = $val['id'];
					$alllooksarrayobject->name = $val['name'];
					$alllooksarrayobject->timestamp = $val['timestamp'];
					if($val['look_type'] == 1){
						$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
					}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
						$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
					}else if($val['look_type'] == 3){
						$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
					}
					$info = getimagesize($alllooksarrayobject->url);
					//if($info[1] == NULL || $info[1] == '' ) $info[1] = 0;
					//if($info[0] == NULL || $info[0] == '' ) $info[0] = 0;
					$alllooksarrayobject->height = $info[1];
					$alllooksarrayobject->width  = $info[0];
					$alllooksarrayobject->type = $val['type'];
					$alllooksarrayobject->look_type = $val['look_type'];
					$alllooksarrayobject->user_share = $this->get_user_share($val['id']);
					$alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
					
					$alllooksarrayobject->share_url = 'https://www.stylecracker.com/looks/look-details/'.$val['slug'];
					$alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
					$looks_array[$i] = $alllooksarrayobject;
					$i++;
				}
			}
          return $looks_array;
		
		
	}
	
	
	function get_discover($filter_value,$timestamp,$limit,$offset,$is_up){
	
		//$y = 5;
		$str =  $limit;
		$a = explode(" ",$str);
		
		//$look_limit = floor(0.8*$a[2]);
		//$blog_limit = floor(0.2*$a[2]);
		$blog_limit = floor($a[2]/5);
		$look_limit = $a[2]-$blog_limit;
		//echo $look_limit.$blog_limit;exit; 
		if($is_up == 1){
			
			if($timestamp === 0){
				$time_stamp_look = 'WHERE status=1';
				$time_stamp_look_new = " ";
				$time_stamp_blog = " WHERE a.post_status = 'publish' and a.post_type = 'post' ";
			}
			  
		    else{ 
			 $time_stamp_look = " WHERE a.created_datetime>$timestamp AND status=1";
			 $time_stamp_look_new = " and a.modified_datetime>$timestamp ";
			 $time_stamp_blog = " WHERE a.post_date>$timestamp and a.post_status = 'publish' and a.post_type = 'post' ";
			}
		}  
		else {
			if($timestamp === 0){
			  $time_stamp_look = 'WHERE status=1';
			  $time_stamp_look_new = "";
			  $time_stamp_blog = " WHERE a.post_status = 'publish' and a.post_type = 'post' ";
			}
		    else{
			 $time_stamp_look = " WHERE a.created_datetime<$timestamp AND status=1";
			 $time_stamp_look_new = " and a.modified_datetime<$timestamp ";
			 $time_stamp_blog = " WHERE a.post_date<$timestamp and a.post_status = 'publish' and a.post_type = 'post' ";
			}
		}
		
		if($filter_value === '0'){
			/*All types of Looks related query*/	
			/* Added by Saylee */
			$query = $this->db->query("select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime as timestamp,z.status,z.deflag,@type:='look' as type from 
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 $time_stamp_look_new ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime DESC LIMIT $look_limit $offset");
				
			/*Blog related query*/
			$query1 = $this->db->query("select a.id as id,a.post_title as name,(select b.guid from stylecra_stylecracker_v1_blogbeta.`wp_postmeta` as a,stylecra_stylecracker_v1_blogbeta.wp_posts as b where a.`meta_key` = '_thumbnail_id' and a.`meta_value` = b.`ID` and a.`post_id`=a.id limit 0,1)as url,CONCAT('http://www.stylecracker.com/blog/',a.post_name) as blog_url,a.post_date as timestamp,@type:='blog' as type from stylecra_stylecracker_v1_blogbeta.wp_posts a $time_stamp_blog order by a.post_date desc LIMIT $blog_limit $offset ");			
			
			$data = array();
			/* Added by saylee*/
			$res = $query->result_array();
			
			$looks_array = array();
			
			$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
					$alllooksarrayobject = new stdClass(); 
					$alllooksarrayobject->id = $val['id'];
					$alllooksarrayobject->name = $val['name'];
					$alllooksarrayobject->timestamp = $val['timestamp'];
					if($val['look_type'] == 1){
						$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
					}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
						$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
					}else if($val['look_type'] == 3){
						$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
					}
					$info = getimagesize($alllooksarrayobject->url);
					//if($info[1] == NULL || $info[1] == '' ) $info[1] = 0;
					//if($info[0] == NULL || $info[0] == '' ) $info[0] = 0;
					$alllooksarrayobject->height = $info[1];
					$alllooksarrayobject->width  = $info[0];
					$alllooksarrayobject->type = $val['type'];
					$alllooksarrayobject->look_type = $val['look_type'];
					$alllooksarrayobject->user_share = $this->get_user_share($val['id']);
					$alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
					$alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
					$alllooksarrayobject->share_url = 'https://www.stylecracker.com/looks/look-details/'.$val['slug'];
					$looks_array[$i] = $alllooksarrayobject;
					$i++;
				}
			}
	
			
			$res1 = $query1->result_array();
			
			$blogs_array = array();
			
			$i = 0; 
			
			if(!empty($res1)){
				foreach($res1 as $val){
			
					$allblogsarrayobject = new stdClass(); 
					$allblogsarrayobject->id = $val['id'];
					$allblogsarrayobject->name = $val['name'];
					
					$allblogsarrayobject->url  = $val['url'];
					$allblogsarrayobject->blog_url  = $val['blog_url'];
					$allblogsarrayobject->share_url = $val['blog_url'];
					$allblogsarrayobject->timestamp = $val['timestamp'];
					//echo $allblogsarrayobject->url.'test';
					//exit;
					if(isset($allblogsarrayobject->url)){
					$info = getimagesize($allblogsarrayobject->url);
					//if($info[1] == NULL || $info[1] == '' ) $info[1] = 0;
					//if($info[0] == NULL || $info[0] == '' ) $info[0] = 0;
					$allblogsarrayobject->height = $info[1];
					$allblogsarrayobject->width  = $info[0];
					}
					$allblogsarrayobject->type = $val['type'];
					
					$blogs_array[$i] = $allblogsarrayobject;
					$i++;
				}
			}

			$a = count($looks_array);
			$b = count($blogs_array);

			if($looks_array != NULL) $lookArray = floor($a/2); else $lookArray = '';
			if($blogs_array != NULL) $blogArray = floor($b/2); else $blogArray = '';
			if($a >0){
				for($i=0;$i<$lookArray;$i++){
					$lookArraySplit[] = $looks_array[$i];
				}
			}
			if($b >0){
				for($i=0;$i<$blogArray;$i++){
					$lookArraySplit[] = $blogs_array[$i];
				}
			}
			if($a >0){
				for($i=($lookArray);$i<$a;$i++){
					$lookArraySplit[] = $looks_array[$i];
				}
			}
			if($b >0){
				for($i=($blogArray);$i<$b;$i++){
					$lookArraySplit[] = $blogs_array[$i];
				}
			}
			// $looks[0][$a-1]= $looks_array;
			// $blogs[0][$b-1]= $blogs_array;
			// print_r($looks);
			// print_r($blogs);exit;
			// echo $c.'++++++'.$d; exit;
			// $res[0][1]= $looks[0][$c-1];
			// $res[0][2] = $blogs[0][$d-1];
			// $res[0][3] = $looks[$c][$a-1];
			// $res[0][4] = $blogs[$d][$b-1];
			// print_r($res);
			//$data1 =array_merge($looks_array,$blogs_array);
			return $lookArraySplit;
			
		}
		
		else if($filter_value === '5'){
			$query = $this->db->query("select a.id as id,a.post_title as name,(select b.guid from stylecra_stylecracker_v1_blogbeta.`wp_postmeta` as a,stylecra_stylecracker_v1_blogbeta.wp_posts as b where a.`meta_key` = '_thumbnail_id' and a.`meta_value` = b.`ID` and a.`post_id`=a.id limit  0,1)as url,CONCAT('http://www.stylecracker.com/blog/',a.post_name) as blog_url,a.post_date as timestamp,@type:='blog' as type from stylecra_stylecracker_v1_blogbeta.wp_posts a $time_stamp_blog order by a.post_date desc $limit $offset ");
			$res = $query->result_array();
			
			$blogs_array = array();
			
			$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
			
					$allblogsarrayobject = new stdClass(); 
					$allblogsarrayobject->id = $val['id'];
					$allblogsarrayobject->name = $val['name'];
					
					$allblogsarrayobject->url  = $val['url'];
					$allblogsarrayobject->blog_url  = $val['blog_url'];
					$allblogsarrayobject->timestamp = $val['timestamp'];
					//$alllooksarrayobject->share_url = $val['blog_url'];
					if(isset($allblogsarrayobject->url)){
					$info = getimagesize($allblogsarrayobject->url);
					$allblogsarrayobject->height = $info[1];
					$allblogsarrayobject->width  = $info[0];
					
					}
					$allblogsarrayobject->type = $val['type'];
					
					$blogs_array[$i] = $allblogsarrayobject;
					$i++;
				}
			}
			
			return $blogs_array;
		}
		
		else if($filter_value == 6){

        $query = $this->db->query("select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,@type:='look' as type ,z.modified_datetime as timestamp from 
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN ((select distinct j.look_id from look_category_mapping as j,look_bucket_mapping as u where j.look_id = u.look_id and j.cat_id = 5) )) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc $limit $offset ");

        $res = $query->result_array();
		
		$looks_array = $this->looks_array($res);
		return $looks_array;
       
        }
		else if($filter_value == 7){

        $query = $this->db->query("select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,@type:='look' as type,z.modified_datetime  as timestamp from 
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN ((select distinct look_id from look_category_mapping where cat_id = 40)) ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4) order by z.modified_datetime desc $limit $offset ");

        $res = $query->result_array();
		$looks_array = $this->looks_array($res);
		return $looks_array;
       
        }
		
		//sample datetime = 2014-07-02 19:10:19 
		else if($filter_value != '0'){
			//echo "select a.id as id,a.name as name,CONCAT('http://www.stylecracker.com/sc_admin/assets/looks/',a.image) as url,a.created_datetime as timestamp,@type:='look' as type,(select COUNT(look_fav.look_id) from look_fav where look_fav.look_id=a.id) as user_likes,(select COUNT(user_share.look_id) from user_share where user_share.look_id=a.id) as user_shares,(select ls.category_name  from look_category_mapping lcm, look_sub_category ls   WHERE a.id = lcm.look_id and lcm.cat_id=ls.id) as look_type_txt from look a  $time_stamp_look ORDER BY a.created_datetime DESC $query_limit $query_offset";
			/*$query = $this->db->query("select a.id as id,a.name as name,CONCAT('http://www.stylecracker.com/sc_admin/assets/looks/',a.image) as url,a.created_datetime as timestamp,@type:='look' as type,(select COUNT(look_fav.look_id) from look_fav where look_fav.look_id=a.id) as user_likes,(select COUNT(user_share.look_id) from user_share where user_share.look_id=a.id) as user_shares,(select ls.category_name  from look_category_mapping lcm, look_sub_category ls   WHERE a.id = lcm.look_id and lcm.cat_id=ls.id limit 0,1) as look_type_txt from look a  $time_stamp_look ORDER BY a.created_datetime DESC $limit $offset ");*/
			
			$query = $this->db->query("select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime as timestamp,z.status,z.deflag,@type:='look' as type from 
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 $time_stamp_look_new and a.look_type = $filter_value ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime DESC $limit $offset");

			//$res = $query->result();  
			
			/* Added by saylee*/
			$res = $query->result_array();
			
			$looks_array = $this->looks_array($res);
			return $looks_array;
		}
		
		
	
	
	}
	
	function looks_array($res){
		
		$looks_array = array();
			
			$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
					$alllooksarrayobject = new stdClass(); 
					$alllooksarrayobject->id = $val['id'];
					$alllooksarrayobject->name = $val['name'];
					$alllooksarrayobject->timestamp = $val['timestamp'];
					if($val['look_type'] == 1){
						$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
					}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
						$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
					}else if($val['look_type'] == 3){
						$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
					}
					$info = getimagesize($alllooksarrayobject->url);
					$alllooksarrayobject->height = $info[1];
					$alllooksarrayobject->width  = $info[0];
					$alllooksarrayobject->type = $val['type'];
					$alllooksarrayobject->look_type = $val['look_type'];
					$alllooksarrayobject->user_share = $this->get_user_share($val['id']);
					$alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
					$alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
					$alllooksarrayobject->share_url = 'https://www.stylecracker.com/looks/look-details/'.$val['slug'];
					$looks_array[$i] = $alllooksarrayobject;
					$i++;
				}
			}
			
			return $looks_array;
		
	}
	
	/* Added by saylee start*/
	function get_user_share($look_id){
		$query = $this->db->query("select COUNT(user_share.look_id) as user_share from user_share where user_share.look_id='".$look_id."'");
		$res = $query->result_array();
		return $res[0]['user_share'];
		
	}
	
	function get_user_likes($look_id){
		$query = $this->db->query("select COUNT(look_fav.look_id) as look_fav from look_fav where look_fav.look_id='".$look_id."'");
		$res = $query->result_array();
		return $res[0]['look_fav'];
		
	}
	
	function get_sub_category($look_id){
		$query = $this->db->query("select ls.category_name from look_category_mapping lcm,look_sub_category ls WHERE ls.id = lcm.cat_id and lcm.look_id='".$look_id."' limit 0,1");
		$res = $query->result_array(); 
		return isset($res[0]['category_name']) ? $res[0]['category_name'] : '';
		
	}
	
	/* Added by saylee end */
	
	function get_share($user_id,$auth_token,$look_id,$action){
		
		$created_datetime = date('Y-m-d G:i:s'); 
		$data = array('user_id'=>$user_id,'look_id'=>$look_id,'medium'=>$action,'created_by'=>$user_id,'created_datetime'=>$created_datetime);
		$result=$this->db->insert('user_share',$data); 
		  if(!empty($result)){
           return 1;
       }else{
           return 2;
       }
        
	}

    function getStylistInfo(){
		$query = $this->db->query("select id,stylist_name,stylist_bio,CONCAT('http://www.stylecracker.com/assets/images/about/',img_name) as img_name,CONCAT('http://www.stylecracker.com/assets/images/about/',full_img) as full_img,slug,@type:='https://www.facebook.com/StyleCracker' as facebook_link,@type:='https://twitter.com/Style_Cracker' as twitter_link,@type:='https://instagram.com/stylecracker' as insta_link,style_tip,CONCAT('http://www.stylecracker.com/assets/images/about/mobile/',full_img) as stylist_cover_img from stylist where status=1");
        $res = $query->result();
        return $res;
       /* $query = $this->db->get_where('stylist', array('status' => 1));
        $result = $query->result();
        if(!empty($result)){

            return $result;
        }*/
    }

    function getSingleStylistInfo($stylist_id){
		//$query = $this->db->query("select id,stylist_name,stylist_bio,CONCAT('http://www.stylecracker.com/assets/images/about/',img_name) as img_name,CONCAT('http://www.stylecracker.com/assets/images/about/',full_img) as full_img,slug,facebook_link,twitter_link,insta_link,style_tip from stylist where status=1 AND id='$stylist_id'");
        //$res['stylist_info'] = $query->result();
		
		$query = $this->db->query("select a.id as id,a.name as name,CONCAT('http://www.stylecracker.com/sc_admin/assets/looks/',a.image) as url,a.created_datetime as timestamp,@type:='look' as type,(select count(look_id) from look_fav where `look_id`=a.id) as look_fav,(select count(look_id) from user_share where `look_id`=a.id) as look_share,(select ls.category_name  from look_category_mapping lcm, look_sub_category ls   WHERE a.stylist_id=$stylist_id AND a.id = lcm.look_id and lcm.cat_id=ls.id LIMIT 0,1) as look_type_txt from look as a  where a.stylist_id=$stylist_id");
        $res = $query->result();
		
        return $res;
        /*
		(select a.id as id,a.name as name,CONCAT('http://www.stylecracker.com/sc_admin/assets/looks/',a.image) as url,a.created_datetime as timestamp,@type:='look' as type,(select count(look_id) from look_fav where `look_id`=a.id) as look_fav,(select count(look_id) from user_share where `look_id`=a.id) as look_share,(select ls.category_name  from look_category_mapping lcm, look_sub_category ls   WHERE a.stylist_id=$stylist_id AND a.id = lcm.look_id and lcm.cat_id=ls.id) as look_type_txt from look as a  where a.stylist_id=$stylist_id) as stylist_looks
		
		
		$query = $this->db->get_where('stylist', array('status' => 1,'slug'=>$stylist_name));
        $res = $query->result_array();
        return $res[0];*/
    }

    function change_password($user_id,$oldPassword,$newPassword){
        $query = $this->db->get_where('user_login', array('id' => $user_id,'status'=>1));
        $result = $query->result();
        if(!empty($result)){
            $salt = substr($result[0]->password, 0, 10);
            $db_password =  $salt . substr(sha1($salt . $oldPassword), 0, -10); 
            if($db_password === $result[0]->password){
                //update password field 
                $salt = substr(md5(uniqid(rand(), true)), 0, 10);
                $password   =$salt . substr(sha1($salt . $newPassword), 0, -10);
                $data = array('password'=>$password );
                $this->db->update('user_login', $data, array('id' => $user_id ));  
                return 1;
            }else{
                return 2;
            }
        }
    }
	
	function get_brands($limit,$offset){
		$query = $this->db->query("select * from brand_info $limit $offset ");
        $res = $query->result();
        return $res;
		
	}
	
	function get_brand_all_names(){
        //select b.user_id as brand_id,b.company_name,b.short_description,b.long_description,IF( b.email IS NULL ,'NA', b.email ) AS email,IF( b.overall_rating IS NULL , 0, 1 ) AS overall_rating,CONCAT('http://www.stylecracker.com/sc_admin/assets/brand_logo/',b.logo) as logo_url,IF( b.contact IS NULL ,'NA', b.contact ) AS contact, (select bi.review_text from brand_review bi where bi.brand_id=b.user_id AND status=1) AS review_text  from brand_info b where b.`show_in brand_list`=1
		$query = $this->db->query("select IF(cover_image ='','http://www.stylecracker.com/assets/images/brands/brand-cover-photo.jpg',CONCAT('http://www.stylecracker.com/assets/images/brands/',cover_image)) as cover_image from brand_info where is_paid=1 AND cover_image != 'brand-cover-photo.jpg'  ");	
		$res['cover_images'] = $query->result();
		
		$query2 = $this->db->query("select user_id as brand_id,company_name,IF( short_description IS NULL ,'', short_description ) as long_description,IF( long_description IS NULL ,'', long_description ) as short_description,IF( email IS NULL ,'NA', email ) AS email,IF( overall_rating IS NULL , 0, 1 ) AS overall_rating,IF(logo IS NULL,'https://www.stylecracker.com/assets/images/brands/brand-logo-default.png',CONCAT('http://www.stylecracker.com/sc_admin/assets/brand_logo/',logo)) as logo_url,IF( contact IS NULL ,'NA', contact ) AS contact,(select COUNT(brand_review.review_text)  from brand_review where brand_review.brand_id = brand_info.user_id AND brand_review.status=1 limit 0,1) as user_review,(select COUNT(brand_review.ratings) from brand_review where brand_review.brand_id = brand_info.user_id AND brand_review.status=1 limit 0,1) as user_ratings  from brand_info where `show_in brand_list`=1");
		
		
        $res2 = $query2->result_array();
		$looks_array = array();
			
			$i = 0; 
			
			if(!empty($res2)){
				foreach($res2 as $val){
					
					$allbrandarrayobject = new stdClass(); 
					$allbrandarrayobject->brand_id = $val['brand_id'];
					$allbrandarrayobject->company_name = $val['company_name'];
					$allbrandarrayobject->short_description = $val['short_description'];
				
					$allbrandarrayobject->long_description = $val['long_description'];
					$allbrandarrayobject->email = $val['email'];
					$query_count = $this->db->query("select ratings as ratings from brand_review where brand_id = '".$val['brand_id']."' AND status=1");
        $res_count = $query_count->result_array();

        $query_rating = $this->db->query("select sum(z.ratings) as ratings from(select (count(ratings)*ratings) as ratings from brand_review where brand_id = '".$val['brand_id']."' AND status=1 group by ratings) as z");
        $res_avg = $query_rating->result_array();
					if(!empty($res_avg)){
					if(sizeof($res_count) != 0){
					$allbrandarrayobject->overall_rating = $res_avg[0]['ratings']/sizeof($res_count);
					}else{
					$allbrandarrayobject->overall_rating = 0;
					}
					}else{
					$allbrandarrayobject->overall_rating = 0;
					}
					
					$allbrandarrayobject->logo_url = $val['logo_url'];
					$allbrandarrayobject->contact = $val['contact'];
					$allbrandarrayobject->user_review = $val['user_review'];
					$allbrandarrayobject->user_ratings = $val['user_ratings'];
					$looks_array[$i] = $allbrandarrayobject;
					$i++;
				} 
		}
		
		
		
		
		$res['brand_info']= $looks_array;
		
        return $res;
	}
	
	function get_user_review($user_id,$auth_token,$brand_id){
		$query = $this->db->query("select brand_id,user_id,review_text from brand_review where brand_id=".$brand_id." and user_id=".$user_id." AND status=1 ");
        $res = $query->result();
		if($res != NULL){
			$res = $this->get_all_brand_mobile($brand_id ,$user_id);
			return $res;
        }else{
		    return 2;	
		}
        return 2; 			
        
		
	}

    function brands_review_mobile($user_id,$brand_id,$review_text,$ratings){
		
        $created_datetime = date('Y-m-d G:i:s'); 
        $data = array('user_id'=>$user_id,'brand_id'=>$brand_id,'review_text'=>$review_text, 'created_by'=>$user_id, 'ratings'=>$ratings,'created_datetime'=>$created_datetime);
        $result=$this->db->insert('brand_review',$data); 
		
        if(!empty($result)){
			//echo "select * from brand_review where  user_id = $user_id AND brand_id = $brand_id AND review_text = $review_text" ;exit;
			$query = $this->db->query("select a.*,b.first_name from brand_review as a,user_info as b where  a.user_id = $user_id AND a.brand_id = $brand_id AND  review_text LIKE '$review_text' AND b.user_id = $user_id AND a.status=1");	
			$res = $query->result();
			//print_r($res);exit;
           //$res = $this->get_all_brand_mobile($brand_id ,$user_id);
		   if($res != NULL ) return $res; else return 2;
       }else{
           return 2;
       }
    }	
    
	 function get_all_brand_mobile($brand_id ,$user_id=0){
		if($user_id != 0) $user_query = " AND b.user_id=$user_id"; else $user_query = '';
		//echo "select b.*, u.first_name, IF(u.profile_pic='', 'http://www.stylecracker.com/assets/images/profile/profile.png', CONCAT('http://www.stylecracker.com/assets/images/profile/',u.profile_pic)) as profile_pic from brand_review b INNER JOIN user_info u  ON u.user_id=b.user_id AND b.brand_id=$brand_id AND b.status=1 $user_query ORDER BY b.brand_id DESC ";
		$query = $this->db->query("select b.*, u.first_name, IF(u.profile_pic='', 'https://www.stylecracker.com/assets/images/profile/profile.png', CONCAT('https://www.stylecracker.com/sc_admin/assets/user_profiles/',u.profile_pic)) as profile_pic from brand_review b INNER JOIN user_info u  ON u.user_id=b.user_id AND b.brand_id=$brand_id AND b.status=1 $user_query ORDER BY b.brand_id DESC ");
		//$res = $query->result();
        //$query = $this->db->get_where('brand_review', array('brand_id' => $brand_id));
        $result = $query->result_array();
        if(!empty($result)){
            return $result;
        }else{
            return NULL;
        }
    }	
	
	function get_review($brand_id ,$user_id=0){
		if($user_id != 0) $user_query = " AND b.user_id=$user_id"; else $user_query = '';
		
		$query = $this->db->query("select b.*, u.first_name, IF(u.profile_pic='', 'http://www.stylecracker.com/assets/images/profile/profile.png', CONCAT('http://www.stylecracker.com/assets/images/profile/',u.profile_pic)) as profile_pic from brand_review b INNER JOIN user_info u  ON u.user_id=b.user_id AND b.brand_id=$brand_id AND b.status=1 $user_query ORDER BY b.ratings DESC limit 2");
        $result = $query->result_array();
        if(!empty($result)){
            return $result;
        }else{
            return NULL;
        }
    }
	
    function add_contact_us($data){
		if(!empty($data)){
			$result = $this->db->insert('user_enquiry', $data);
			return $result;
		
		}
    }
	
	public function randomPassword_mobile() {
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
  }
  
  function update_profile_img($filename,$user_id){
        
        if($user_id!='' && $filename!=''){
            $data = array('profile_pic'=>$filename,'modified_by'=>$user_id);
            $this->db->update('user_info', $data, array('user_id' => $user_id ));
			
            return true;
        }
    }
	
	
	function update_cover_img($filename,$user_id){
     
        if($user_id!='' && $filename!=''){
            $data = array('profile_cover_pic'=>$filename,'modified_by'=>$user_id);
            $this->db->update('user_info', $data, array('user_id' => $user_id ));
            return true;
        }
    }
	
	function get_profile_img($user_id){
        
        if($user_id!=''){
            $query = $this->db->query("select IF(profile_pic='', '".base_url()."sc_admin/assets/user_profiles/', CONCAT('".base_url()."sc_admin/assets/user_profiles/',profile_pic)) as profile_pic from user_info where user_id = $user_id ");
			$res = $query->result_array();
			
            return $res;
        }
    }
	
	function get_cover_img($user_id){
     
        if($user_id!=''){
             $query = $this->db->query("select IF(profile_cover_pic='', 'http://www.stylecracker.com/sc_admin/assets/full_pic/', CONCAT('http://www.stylecracker.com/sc_admin/assets/full_pic/',profile_cover_pic)) as profile_cover_pic from user_info where user_id = $user_id ");
			$res = $query->result_array();
			
            return $res;
        }
    }
	
	 function upload_wordrobe_img($filename,$user_id,$tags,$product_name){
		// $explode_data = explode(',',$tags);
		 // echo "hello";
		// print_r($explode_data);exit;

        if($user_id!='' && $filename!=''){
           $data = array(
                    'is_active' => 1 ,
                    'image_name' => $filename ,
                    'created_by' => $user_id,
                    'created_datetime' => $this->config->item('sc_date'),
                    'user_id' => $user_id,
					'modified_by' => $user_id
            );
		
		
        $this->db->insert('users_wardrobe', $data);
		$wordrobe_id = $this->db->insert_id();
		//echo $wordrobe_id;exit;
		$data = array(
					'tag_user_id' => $user_id,
					'tag_ward_id' => $wordrobe_id ,
					 'tag_values' => $tags,
                    'tag_product_name' => $product_name,  
					'status' => 1 ,					
                    'created_datetime' => $this->config->item('sc_date')
                    
            );
		$this->db->insert('user_wardrobe_tags', $data);
		//echo $this->db->last_query();
		// exit;
		
		$query = $this->db->query("select a.id,CONCAT('http://www.stylecracker.com/sc_admin/assets/wardrobes/',a.image_name) as image_name,b.tag_values as tags,b.tag_product_name as product_name from users_wardrobe as a,user_wardrobe_tags as b where a.id = $wordrobe_id and a.is_active = 1 AND b.tag_user_id = $user_id AND tag_ward_id = $wordrobe_id order by id desc");
        $result = $query->result();
		// print_r($result);exit;
		return $result[0];				
        }	
    }
	
	
	function get_wardrobe_img($user_id){
		
       // $query = $this->db->query("select a.id,CONCAT('http://www.stylecracker.com/sc_admin/assets/wardrobes/',a.image_name) as image_name,b.tag_values as tags,b.tag_product_name as product_name from users_wardrobe as a,user_wardrobe_tags as b where a.user_id = $user_id and b.tag_user_id = $user_id AND  a.is_active = 1 order by id desc");
	   // $query = $this->db->query("select a.id,CONCAT('http://www.stylecracker.com/sc_admin/assets/wardrobes/',a.image_name) as image_name,b.tag_values as tags,b.tag_product_name as product_name from users_wardrobe as a INNER JOIN user_wardrobe_tags as b ON a.user_id = b.tag_user_id where b.tag_user_id = $user_id  order by id desc limit 20");
	    $query = $this->db->query("select a.id,CONCAT('http://www.stylecracker.com/sc_admin/assets/wardrobes/',a.image_name) as image_name,b.tag_values as tags,b.tag_product_name as product_name from users_wardrobe as a INNER JOIN user_wardrobe_tags as b ON a.id=b.tag_ward_id AND a.user_id = $user_id and a.is_active = 1 AND b.tag_user_id = $user_id order by id desc limit 20");	   
        //select id,CONCAT('http://www.stylecracker.com/sc_admin/assets/wardrobes/',image_name) as image_name from users_wardrobe where user_id = $user_id and is_active = 1 order by id desc
        //
        $result = $query->result_array();
		return $result;

    }
	
	function delete_wardrobe_img($user_id,$wordrobe_id){
		$data = array('is_active'=>0,'modified_by'=>$user_id);
		$result = $this->db->update('users_wardrobe', $data, array('user_id' => $user_id,'id'=>$wordrobe_id));
		$data1 = array('status'=>0);
		$result1 = $this->db->update('user_wardrobe_tags', $data1, array('tag_user_id' => $user_id,'tag_ward_id'=>$wordrobe_id)); 
		if(!empty($result)){
           return 1;
       }else{
           return 2;
       }

    }
	
	function get_all_tags(){
		
        $query = $this->db->query("SELECT * FROM taglook WHERE status=1 ");
        $result = $query->result_array();
		return $result;

    }
	
	function push_look($timestamp){
		//echo $timestamp;exit;
	$query = $this->db->query("select distinct k.id,k.slug,product_img,k.look_type,k.image,look_image,name,k.created_datetime,k.modified_datetime as timestamp,k.status,k.deflag,k.stylist_id,@type:='look' as type from
(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag,z.`stylist_id` from 
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag,a.`stylist_id` FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 and a.modified_datetime>'$timestamp') as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc) as k,look_bucket_mapping as m where k.id = m.look_id order by k.modified_datetime desc");

		 //$res = $query->result();
		 
		/* Added by saylee*/
			$res = $query->result_array();
			//print_r($res);exit;
			$looks_array = array();
			
			$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
					$alllooksarrayobject = new stdClass(); 
					$alllooksarrayobject->id = $val['id'];
					$alllooksarrayobject->name = $val['name'];
					$alllooksarrayobject->timestamp = $val['timestamp'];
					if($val['look_type'] == 1){
						$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
						//$info = getimagesize($alllooksarrayobject->url);
					}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
						$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
						//$info = getimagesize($alllooksarrayobject->url);
					}else if($val['look_type'] == 3){
						$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
					}
					// $info = getimagesize($alllooksarrayobject->url);
					// $alllooksarrayobject->height = $info[1];
					// $alllooksarrayobject->width  = $info[0];
					$alllooksarrayobject->type = $val['type'];
					$alllooksarrayobject->look_type = $val['look_type'];
					$alllooksarrayobject->user_share = $this->get_user_share($val['id']);
					$alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
					$alllooksarrayobject->share_url = 'https://www.stylecracker.com/looks/look-details/'.$val['slug'];
					$alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
					$looks_array[$i] = $alllooksarrayobject;
					$i++;
				} 
		}
			
		return $looks_array[0];
//print_r($looks_array);exit;
		
		
	
	

    }

	function search_brand_name($brand_name){
        $query = $this->db->query("select company_name from brand_info where `show_in brand_list`=1 AND company_name  LIKE  '%$brand_name%' ");
        
        $res = $query->result_array();
        return $res;
    }
	/*
	function multi_image($product_id){
		$query = $this->db->query("select CONCAT('http://www.stylecracker.com/sc_admin/assets/products/',b.image) as url,a.id,CONCAT('https://www.stylecracker.com/sc_admin/assets/products_extra/thumb/',a.product_images) as image_name from extra_images as a,product_desc as b where a.product_id = $product_id AND b.id = a.product_id");

        $result = $query->result_array();
		return $result;
	}*/
	
	function multi_image($product_id){
		$query = $this->db->query("select CONCAT('http://www.stylecracker.com/sc_admin/assets/products/',b.image) as url,b.id,CONCAT('https://www.stylecracker.com/sc_admin/assets/products_extra/',a.product_images) as image_name from extra_images as a,product_desc as b where a.product_id = $product_id AND b.id = a.product_id");
		$result = $query->result_array();
		$i=0;
		$data=array();
		foreach($result as $row){
			if($i==0){
				$data[$i]['image_url'] = $row['url'];
				$i = $i+1;
				$data[$i]['image_url'] = $row['image_name'];
				$i++;
			}else {
				$data[$i]['image_url'] = $row['image_name'];
				$i++;
			}
		}
		return $data;
	}
	
	function get_products($limit,$offset){
		$query = $this->db->query("SELECT a.id AS productId, a.name AS productTitle, a.product_sku AS sku, a.price AS price, @type :=  'INR' AS currency, (

SELECT stock_count
FROM product_inventory
WHERE product_id = a.id
ORDER BY  `a`.`id` DESC 
LIMIT 1
) AS stockAvailability, CONCAT(  'https://www.stylecracker.com/sc_admin/assets/products/', a.image ) AS productPictureUrl, CONCAT( CONCAT( CONCAT(  'https://www.stylecracker.com/product/details/', a.name ) ,  '-' ) , a.id ) AS pageUrl, a.product_cat_id AS categoryIds, b.company_name AS brandName
FROM  `product_desc` AS a, brand_info AS b
WHERE a.brand_id = b.user_id
AND b.is_paid =1 $limit $offset ");
		$result = $query->result_array();
		return $result;
		
	}
	
}
?>