<?php
class Brands_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_all_brands(){

       
        // $query = $this->db->query("select a.`user_id`, a.`company_name`,b.`user_name` from `brand_info` as a,`user_login` as b,`user_info` as c where a.`user_id` = b.`id` and a.`user_id` = c.`user_id` and a.`show_in brand_list` = 1 order by a.company_name");
        // $res = $query->result();
		
		 $query = $this->db->query("select a.`user_id`, UPPER(a.`company_name`) as company_name,b.`user_name`,@type:='' as store_id from `brand_info` as a,`user_login` as b,`user_info` as c where a.`user_id` = b.`id` and a.`user_id` = c.`user_id` and a.`show_in brand_list` = 1 order by a.company_name");
        $res = $query->result();
		
		/*$query1 = $this->db->query("select a.`user_id`,  UPPER(d.`brand_store_name`) as company_name,b.`user_name`,CONCAT(CONCAT(b.`user_name`,'-'),d.id) as store_id from `brand_info` as a,`user_login` as b,`user_info` as c,brand_store as d where a.`user_id` = b.`id` and a.`user_id` = c.`user_id` and a.`show_in brand_list` = 1 AND a.user_id = d.brand_id AND a.store_type = 3 AND d.show_on_web = 1 order by a.company_name");
		$res1 = $query1->result();
		$result = (array)array_merge($res,$res1);
		$sort = array();
		usort($result, function($a, $b) { return strnatcmp($a->company_name,$b->company_name);});*/
		// echo"<pre>";print_r($result);
        $result =  $res;
        return $result;

    }

    function get_brands_categories_list(){

        //$query = $this->db->query("select a.id,a.user_name, a.bucket, a.created_datetime, a.email,TRIM(c.first_name) as first_name,c.last_name from user_login as a, user_role_mapping as b,user_info as c WHERE a.`status` =1 and a.`id` = b.`user_id` and c.`user_id` = a.`id` and b.`role_id` = 6 order by c.first_name asc");
        
        $query = $this->db->query("select category_name, category_slug from brand_category where status = 1 and is_delete = 0 and category_type = 'brand_cat'");

       // $query = $this->db->query("");

        $res = $query->result_array();
        return $res;

    }

    function get_brands_collections_list(){

        //$query = $this->db->query("select a.id,a.user_name, a.bucket, a.created_datetime, a.email,TRIM(c.first_name) as first_name,c.last_name from user_login as a, user_role_mapping as b,user_info as c WHERE a.`status` =1 and a.`id` = b.`user_id` and c.`user_id` = a.`id` and b.`role_id` = 6 order by c.first_name asc");
        
        $query = $this->db->query("select category_name, category_slug from brand_category where status = 1 and is_delete = 0 and category_type = 'brand_collection'");

       // $query = $this->db->query("");

        $res = $query->result_array();
        return $res;

    }

    function get_brands_categories_wize($category_slug){
        
        $sql = "select  bi.`user_id`, bi.`company_name`, bi.listing_page_img cover_image, ul.`user_name` from category_brand_relationship cbr
                                    left join brand_info bi on cbr.brand_id = bi.user_id
                                    left join brand_category bc on cbr.category_id = bc.id 
                                    left join `user_login` as ul on bi.user_id = ul.id 
                                    where category_slug = '$category_slug' 
                                    order by bc.`modified_datetime` DESC;";
        $query = $this->db->query($sql);
		$res = $query->result();
		return $res;
    }

    function search_brand($search,$type){
        if($type == ''){
             // $query = $this->db->query("select a.`user_id`, a.`company_name`,b.`user_name` from `brand_info` as a,`user_login` as b,`user_info` as c where a.`user_id` = b.`id` and a.`user_id` = c.`user_id` and a.`show_in brand_list` = 1 and LOWER(a.company_name) like '%".$search."%' order by a.company_name");
			$query = $this->db->query("select a.`user_id`, UPPER(a.`company_name`) as company_name,b.`user_name`,@type:='' as store_id from `brand_info` as a,`user_login` as b,`user_info` as c where a.`user_id` = b.`id` and a.`user_id` = c.`user_id` and a.`show_in brand_list` = 1 and LOWER(a.company_name) like '%".$search."%' order by a.company_name");

			$query1 = $this->db->query("select a.`user_id`,  UPPER(d.`brand_store_name`) as company_name,b.`user_name`,CONCAT(CONCAT(b.`user_name`,'-'),d.id) as store_id from `brand_info` as a,`user_login` as b,`user_info` as c,brand_store as d where a.`user_id` = b.`id` and a.`user_id` = c.`user_id` and a.`show_in brand_list` = 1 AND a.user_id = d.brand_id AND a.store_type = 3 AND d.show_on_web = 1 and LOWER(a.company_name) like '%".$search."%' order by a.company_name");
        }else{
            // $query = $this->db->query("select a.`user_id`, a.`company_name`,b.`user_name` from `brand_info` as a,`user_login` as b,`user_info` as c where a.`user_id` = b.`id` and a.`user_id` = c.`user_id` and a.`show_in brand_list` = 1 and c.is_most_popular=1 and LOWER(a.company_name) like '%".$search."%' order by a.company_name");
			$query = $this->db->query("select a.`user_id`, UPPER(a.`company_name`) as company_name,b.`user_name`,@type:='' as store_id from `brand_info` as a,`user_login` as b,`user_info` as c where a.`user_id` = b.`id` and a.`user_id` = c.`user_id` and a.`show_in brand_list` = 1 and c.is_most_popular=1 and LOWER(a.company_name) like '%".$search."%' order by a.company_name");
			
			$query1 = $this->db->query("select a.`user_id`,  UPPER(d.`brand_store_name`) as company_name,b.`user_name`,CONCAT(CONCAT(b.`user_name`,'-'),d.id) as store_id from `brand_info` as a,`user_login` as b,`user_info` as c,brand_store as d where a.`user_id` = b.`id` and a.`user_id` = c.`user_id` and a.`show_in brand_list` = 1 AND a.user_id = d.brand_id AND a.store_type = 3 AND d.show_on_web = 1 and LOWER(a.company_name) like '%".$search."%' order by a.company_name");
        }
		$res = $query->result_array();
		$res1 = $query1->result_array();
		$result = (array)array_merge($res,$res1);
		$sort = array();
		usort($result, function($a, $b) { return strnatcmp($a['company_name'],$b['company_name']);});
		// echo "<pre>";print_r($result);exit;
		return $result;
    }

    function get_brands_info($brand_slug){

        $data = array();
        if($brand_slug){
            
            $query = $this->db->query("select a.id,a.user_name, a.bucket, a.created_datetime, a.email,c.first_name,c.last_name,d.`company_name`,d.`logo` from user_login as a, user_role_mapping as b,user_info as c,brand_info as d WHERE a.`status` =1 and a.`id` = b.`user_id` and c.`user_id` = a.`id` and d.`user_id` = c.`user_id` and b.`role_id` = 6 and a.user_name='".$brand_slug."'");
            $res = $query->result_array();
            if(!empty($res)){
            $data['emailid'] =  $res[0]['email'];
            $data['first_name'] =  $res[0]['first_name'];
            $data['last_name'] =  $res[0]['last_name'];
            $data['company_name'] =  $res[0]['company_name'];
            $data['email'] =  $res[0]['email'];
           // $data['logo'] =  $res[0]['logo'];
            $data['brand_id'] =  $res[0]['id'];
            $data['user_name'] =  $res[0]['user_name'];

            if($res[0]['id']){
                
                $query = $this->db->query("select a.short_description,a.user_id,a.logo,a.cover_image,a.url,a.long_description,a.overall_rating,b.phone_one,b.phone_two,b.mobile_one,b.mobile_two from brand_info as a,brand_contact_info as b where a.user_id= b.brand_id and a.user_id='".$res[0]['id']."'");
                $res = $query->result_array();
                $data['brand_info'] =$res;
               
            } 
             $data['avg_ratings'] =$this->avgRatings($data['brand_id']);
         }
        }
        return $data;
    }

    function get_product_info($product_id){
        $this->trackProduct($product_id);
       
        $query = $this->db->query("select a.`brand_id`,a.`id`,a.name,a.url,a.price,a.image,b.company_name,b.redirect_sep from product_desc as a,`brand_info` as b where a.`brand_id` = b.`user_id` and a.`id`='".$product_id."'");
        $res = $query->result_array();
        return $res;
    }
    function post_review($post_review_text,$ratings,$brand_token){

        $user_id = $this->session->userdata('user_id');
        if($user_id!='' && $post_review_text!='' && $ratings!='' && $brand_token!=''){
            $data =array(
                    'brand_id' => $brand_token,
                    'user_id' =>  $user_id,
                    'review_text' =>$post_review_text,
                    'ratings' => $ratings,
                    'created_by' => $user_id,
                    'created_datetime'=> $this->config->item('sc_date')
                    );
            $this->db->insert('brand_review', $data); 

        }

    }

    function get_reviews($brand_id,$limit){
        if($limit>0){
            $limit_cond = "limit 0,".$limit;
        }else{
            $limit_cond ="";
        }
         $query = $this->db->query("SELECT c.`first_name`,c.`last_name`,b.`user_name`,a.`id`, a.`brand_id`, a.`user_id`, a.`review_text`, a.`created_by`, a.`modified_by`, a.`created_datetime`, a.`modified_datetime`, a.`status`, a.`ratings`,IF(c.profile_pic='', IF(c.gender = 1,'https://www.stylecracker.com/assets/images/profile/profile-women.jpg','https://www.stylecracker.com/assets/images/profile/profile-men.jpg'), CONCAT('http://www.stylecracker.com/sc_admin/assets/user_profiles/',c.profile_pic)) as profile_pic FROM `brand_review` as a,user_login as b,user_info as c WHERE 1 and a.`brand_id` = '".$brand_id."' and a.status = 1 and a.`user_id` = b.id AND b.id = c.user_id order by a.id desc ".$limit_cond);
        
        $res = $query->result_array();
        return $res;
    }

    function get_user($user_id){
        $query = $this->db->query("select first_name,last_name,profile_pic from user_info where user_id=".$user_id);
        $res = $query->result_array();
        return $res;
    }

    function avgRatings($brand_id){

        $query_count = $this->db->query("select ratings as ratings from brand_review where brand_id = '".$brand_id."'");
        $res_count = $query_count->result_array();

        $query = $this->db->query("select sum(z.ratings) as ratings from(select (count(ratings)*ratings) as ratings from brand_review where brand_id = '".$brand_id."' group by ratings) as z");
        $res = $query->result_array();

       

        if(!empty($res)){
            if(sizeof($res_count) != 0){
                return $res[0]['ratings']/sizeof($res_count);
            }else{
                return 0;
            }
        }else{
            return '';
        }
    }

     function trackProduct($product_id){
        if($product_id){
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }

       /* $query = $this->db->query("insert into users_product_click_track(user_id,product_id) values('".$user_id."','".$product_id."')");*/
       }
    }
	
	public function get_brands_banners(){
		$time = new DateTime(date('Y-m-d H:i:s'));
        $time->add(new DateInterval('PT330M'));
        $dateM = $time->format('Y-m-d H:i:s');
		// working query
		// $query = $this->db->query("select a.* from sc_home_slider as a where a.start_date <= '".date('Y-m-d h:i:s')."' AND a.end_date >= '".date('Y-m-d h:i:s')."' order by a.banner_position ASC");
		// new query
		$query = $this->db->query("select a.* from brand_banners as a where a.start_date <= '".$dateM."' AND a.end_date >= '".$dateM."' AND is_active = 1 OR a.banner_type = 0 order by a.banner_position ASC");
		// echo $this->db->last_query();exit;
		$res = $query->result_array();
		// echo "<pre>";print_r($res);exit;
		$slider_array = array();
		$last_banner_position = 0;
		$new_banner_position = 1;
		$i = 0;
		if(!empty($res)){
			foreach($res as $row){
				if($last_banner_position != $row['banner_position']){
				$slider_array[$i]['id'] = $row['id'];
				$slider_array[$i]['caption_line_1'] = $row['caption_line_1'];
				$slider_array[$i]['caption_line_2'] = $row['caption_line_2'];
				$slider_array[$i]['lable_link'] = $row['lable_link'];
				$slider_array[$i]['slide_url'] = $row['slide_url'];
				$slider_array[$i]['is_active'] = $row['is_active'];
				$slider_array[$i]['start_date'] = $row['start_date'];
				$slider_array[$i]['end_date'] = $row['end_date'];
				$slider_array[$i]['banner_url'] = $row['banner_url'];
				$slider_array[$i]['banner_position'] = $row['banner_position'];
				$slider_array[$i]['banner_type'] = $row['banner_type'];
				$slider_array[$i]['is_deleted'] = $row['is_deleted'];
				$last_banner_position = $row['banner_position']; 
				$i++;
				// echo "<pre>";print_r($slider_array);exit;
				}else{
				$i = $i-1; 
				$slider_array[$i]['id'] = $row['id'];
				$slider_array[$i]['caption_line_1'] = $row['caption_line_1'];
				$slider_array[$i]['caption_line_2'] = $row['caption_line_2'];
				$slider_array[$i]['lable_link'] = $row['lable_link'];
				$slider_array[$i]['slide_url'] = $row['slide_url'];
				$slider_array[$i]['is_active'] = $row['is_active'];
				$slider_array[$i]['start_date'] = $row['start_date'];
				$slider_array[$i]['end_date'] = $row['end_date'];
				$slider_array[$i]['banner_url'] = $row['banner_url'];
				$slider_array[$i]['banner_position'] = $row['banner_position'];
				$slider_array[$i]['banner_type'] = $row['banner_type'];
				$slider_array[$i]['is_deleted'] = $row['is_deleted'];
				$last_banner_position = $row['banner_position']; 
				// echo "<pre>";print_r($slider_array);exit;
				$i++;
				} 
				
			}
		}
		// echo "<pre>";print_r($slider_array);exit;
		return $slider_array;
	}

    function get_brands_look($brand_id){
        $look_id = '';
        $query = $this->db->query('select distinct a.id as id from look as a,look_products as b,product_desc as c where a.id = b.look_id and b.product_id = c.id and a.status = 1 and c.brand_id = "'.$brand_id.'" order by a.modified_datetime desc limit 0,3');
        $lookIds = $query->result_array();
        if(!empty($lookIds)){ $i = 0;
            foreach($lookIds as $val){
                if($i!=0){ $look_id = $look_id.','; }
                    $look_id = $look_id.$val['id'];
                $i++;
            }
        }

         if($look_id!=''){
                $query = $this->db->query('select z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.description,z.status,z.deflag,z.slug,z.looks_for from (SELECT a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.description,a.status,a.deflag,a.slug,a.looks_for FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 OR z.look_type = 6) WHERE 1 and z.status=1 and z.id IN('.$look_id.') order by z.modified_datetime desc');
                $look_data = $query->result_array();
                return $look_data;
        }
    }

    function get_brands_product($brand_id){
        $look_id = '';
        $query = $this->db->query('select id,name,price,image,slug,brand_id,product_cat_id,CASE 
        WHEN  discount_start_date <= CURRENT_TIMESTAMP() and discount_end_date >= CURRENT_TIMESTAMP() then compare_price
        ELSE 0
    END AS compare_price from product_desc WHERE  brand_id = "'.$brand_id.'" and (approve_reject = "A" || approve_reject = "D") and is_delete = 0 and status =1 order by id desc limit 0,3');
        $products = $query->result_array();
        return $products;
    }
	
	function get_store_look($store_id){
        $look_id = '';
        $query = $this->db->query('select distinct a.id as id from look as a,look_products as b,product_store as c where a.id = b.look_id and b.product_id = c.product_id and a.status = 1 and c.store_id = "'.$store_id.'" order by a.modified_datetime desc limit 0,3');
        $lookIds = $query->result_array();
        if(!empty($lookIds)){ $i = 0;
            foreach($lookIds as $val){
                if($i!=0){ $look_id = $look_id.','; }
                    $look_id = $look_id.$val['id'];
                $i++;
            }
        }

         if($look_id!=''){
                $query = $this->db->query('select z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.description,z.status,z.deflag,z.slug,z.looks_for from (SELECT a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.description,a.status,a.deflag,a.slug,a.looks_for FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 OR z.look_type = 6) WHERE 1 and z.status=1 and z.id IN('.$look_id.') order by z.modified_datetime desc');
                $look_data = $query->result_array();
                return $look_data;
        }
    }

    function get_store_product($store_id){
        $look_id = '';
        $query = $this->db->query('select a.id,a.name,a.price,a.image,a.slug,a.brand_id,a.product_cat_id,CASE 
        WHEN  a.discount_start_date <= CURRENT_TIMESTAMP() and a.discount_end_date >= CURRENT_TIMESTAMP() then a.compare_price
        ELSE 0
    END AS compare_price from product_desc as a,product_store as b WHERE  (a.approve_reject = "A" || a.approve_reject = "D") and a.is_delete = 0 and a.status =1 and b.store_id = "'.$store_id.'" AND  a.id = b.product_id order by a.id desc limit 0,3');
        $products = $query->result_array();
        return $products;
    }
    
}
?>