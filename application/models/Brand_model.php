<?php
class Brand_model extends MY_Model {
	function __construct()
    {
        // Call the Model constructor
        $this->slug = '';
        parent::__construct();
    }

	function get_brand_info($brand_id){
		
		$query = $this->db->query("select user_id as brand_id,company_name,IF(b.cover_image ='','".web_url2."assets/images/brands/brand-cover-photo.jpg',CONCAT('".web_url2."assets/images/brands/',b.cover_image)) as cover_image,IF(b.cover_image ='','".web_url2."assets/images/brands/mobile_cover_img/brand-cover-photo.jpg',CONCAT('".web_url2."assets/images/brands/mobile_cover_img/',b.cover_image)) as mobile_cover_image,IF(short_description IS NULL ,'', short_description) as short_description,IF( long_description IS NULL ,'', long_description ) as long_description from brand_info as b where b.`show_in brand_list` = 1 AND b.user_id = $brand_id");
		
        $res = $query->result_array();
		if(!empty($res)){
			return $res[0];
		}else return array();
	}
	
	function get_brand_products($brand_id,$user_id,$sort_by,$limit,$offset){
		
		$product_array = array();
		if($sort_by == '2'){
			$order_by = 'order by a.price asc';
			$prod_data['order_by'] = 'order by p.price asc';
        }else if($sort_by == '1'){
			$order_by = 'order by a.price desc';
			$prod_data['order_by'] = 'order by p.price desc';
        }else {
			$order_by = 'order by a.id desc';
		}
		
		$query = $this->db->query(" select DISTINCT a.id as product_ids from product_desc as a,product_inventory as b WHERE a.id = b.product_id AND a.brand_id = $brand_id and (a.approve_reject = 'A' OR approve_reject = 'D' OR approve_reject = 'BD') and a.is_delete = 0 AND b.stock_count > 0 $order_by ");
		$res = $query->result_array();
		
		$product_array = array();
		foreach($res as $val){
			$product_array[] = $val['product_ids'];
		}
		// echo "<pre>";print_r($product_array);exit;
		$products_ids = '';
		$products_ids = implode(',',$product_array);
		if($products_ids != ''){
			
			$prod_data['limit'] = $limit;
			$prod_data['offset'] = $offset;
			$prod_data['limit_cond'] = '';
			$prod_data['user_id'] = $user_id;
			$prod_data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_340/';
			$product_ids = explode(',',$products_ids);
			if(!empty($product_ids[0]))$product_array = $this->Mobile_look_model->get_products_list($product_ids,$prod_data);
			
			return $product_array;
			
		}else{
			return array();
		}
		
	}
	
	function top_brands_depends_on_pa($data_arr){
		
		$bucket_id = $data_arr['bucket_id'];
		if($bucket_id >0){
			$query = $this->db->query("select DISTINCT b.user_id as brand_id,b.company_name,IF(b.cover_image ='','".web_url2."assets/images/brands/brand-cover-photo.jpg',CONCAT('".web_url2."assets/images/brands/',b.cover_image)) as cover_image,IF(b.cover_image ='','".web_url2."assets/images/brands/mobile_cover_img/brand-cover-photo.jpg',CONCAT('".web_url2."assets/images/brands/mobile_cover_img/',b.cover_image)) as mobile_cover_image,IF(b.short_description IS NULL ,'', b.short_description) as short_description,IF( b.long_description IS NULL ,'', b.long_description ) as long_description,c.user_name as brand_slug,IF(b.home_page_promotion_img ='','".web_url2."assets/images/brands/homepage/21009.jpg',CONCAT('".web_url2."assets/images/brands/homepage/',b.home_page_promotion_img)) as home_page_promotion_img from pa_object_relationship as a,brand_info as b,`user_login` as c where a.object_id = b.user_id AND a.object_type='brand' and a.pa_type='bucket' and a.object_value = $bucket_id AND b.`show_in brand_list` = 1 AND b.user_id = c.id order by b.company_name ".$data_arr['limit_cond']);
		}else {
			$query = $this->db->query("select DISTINCT b.user_id as brand_id,b.company_name,IF(b.cover_image ='','".web_url2."assets/images/brands/brand-cover-photo.jpg',CONCAT('".web_url2."assets/images/brands/',b.cover_image)) as cover_image,IF(b.cover_image ='','".web_url2."assets/images/brands/mobile_cover_img/brand-cover-photo.jpg',CONCAT('".web_url2."assets/images/brands/mobile_cover_img/',b.cover_image)) as mobile_cover_image,IF(b.short_description IS NULL ,'', b.short_description) as short_description,IF( b.long_description IS NULL ,'', b.long_description ) as long_description,c.user_name as brand_slug,IF(b.home_page_promotion_img ='','".web_url2."assets/images/brands/homepage/21009.jpg',CONCAT('".web_url2."assets/images/brands/homepage/',b.home_page_promotion_img)) as home_page_promotion_img  from brand_info as b,`user_login` as c where b.`show_in brand_list` = 1 AND b.user_id = c.id order by b.company_name ".$data_arr['limit_cond']);
		}
		
        $res = $query->result_array();
		
        if(count($res) > 0){
			return $res;
		}else {
			$query = $this->db->query("select DISTINCT b.user_id as brand_id,b.company_name,IF(b.cover_image ='','".web_url2."assets/images/brands/brand-cover-photo.jpg',CONCAT('".web_url2."assets/images/brands/',b.cover_image)) as cover_image,IF(b.cover_image ='','".web_url2."assets/images/brands/mobile_cover_img/brand-cover-photo.jpg',CONCAT('".web_url2."assets/images/brands/mobile_cover_img/',b.cover_image)) as mobile_cover_image,IF(b.short_description IS NULL ,'', b.short_description) as short_description,IF( b.long_description IS NULL ,'', b.long_description ) as long_description,c.user_name as brand_slug,IF(b.home_page_promotion_img ='','".web_url2."assets/images/brands/homepage/21009.jpg',CONCAT('".web_url2."assets/images/brands/homepage/',b.home_page_promotion_img)) as home_page_promotion_img  from brand_info as b,`user_login` as c where b.`show_in brand_list` = 1 AND b.user_id = c.id order by b.company_name ".$data_arr['limit_cond']);
			$res = $query->result_array();
			return $res;
		}
    }
	
	
}