<?php
class Search_model extends MY_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

   public function autosearch($search){
        /*$query = $this->db->query("select distinct company_name as name from brand_info where is_paid=1 and company_name like '".$search."%' UNION select distinct name as name from product_desc where name like '".$search."%' UNION select distinct name as name from look where name like '".$search."%'");*/
        $query = $this->db->query("select distinct a.name as name from product_desc as a,product_inventory as b where a.name like '".$search."%' and a.is_delete=0 and a.approve_reject IN ('D','A') and a.status=1 and a.id=b.product_id and b.stock_count>0");
        $result = $query->result_array();
        $look_suggestion = '[';
        if(!empty($result)){ $i = 0;

            foreach($result as $val){
                if($i!=0){ $look_suggestion = $look_suggestion.','; }
                $look_suggestion = $look_suggestion.'{"value":"'.stripslashes($val['name']).'","data":"'.stripslashes($val['name']).'"}';

                $i++;
            }
        }
        $look_suggestion = $look_suggestion.']';
        return $look_suggestion;
    }
}
?>