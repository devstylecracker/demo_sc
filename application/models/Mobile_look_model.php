<?php

class Mobile_look_model extends MY_Model {
	
	function single_product_data($product_id,$user_id=0){
		$query = $this->db->query("Select DISTINCT p.id as product_id,(select CONCAT(cat.category_name,CONCAT(CONCAT('--',cat.id),CONCAT('--',category_slug))) as cat from category_object_relationship as co,category as cat where co.category_id = cat.id and co.object_type = 'product' and co.object_id = p.id order by co.category_id desc limit 1) category,p.brand_id,p.name,p.description,p.slug,p.price,p.url as buy_now_url,CONCAT('".$this->config->item('product_image')."',p.image) as url,p.image as p_image,b.company_name as brand_name,CASE 
        WHEN  p.discount_start_date <= CURRENT_TIMESTAMP() and p.discount_end_date >= CURRENT_TIMESTAMP() then p.compare_price
        ELSE 0
    END AS compare_price,b.short_description as brand_desc,c.user_name as brand_slug,p.slug as product_slug,p.image from product_desc as p,brand_info b,user_login as c where (p.approve_reject='A' OR p.approve_reject = 'D' OR p.approve_reject = 'BD' OR p.approve_reject = 'B') AND p.id = $product_id AND p.brand_id = b.user_id AND b.user_id = c.id AND p.status = '1' ");
		 
		$res = $query->result_array();
		$product_array = array();
		$i=0;
		if(!empty($res)){
			foreach($res as $val){
				
				$productobject = new stdClass(); 
				$productobject->product_id = $val['product_id'];
				$productobject->extra_images = $this->Mobile_model_android->multi_image($val['product_id']);
				// $productobject->product_category = $val['category'];
				if(!empty($val['category'])){ $category = explode('--',$val['category']);}
				$productobject->product_category = @$category[0];
				$productobject->product_category_id = @$category[1];
				$productobject->product_category_slug = @$category[2];
				$productobject->brand_id = $val['brand_id'];
				$productobject->name = $val['name'];
				$productobject->sold_by = $val['brand_name'];
				$productobject->return_policy = 'As Per Brand';
				$productobject->description = $val['description'];
				$productobject->slug = $val['slug'];
				$productobject->price = $val['price'];
				$productobject->buy_now_url = $val['buy_now_url'];
				$productobject->is_available = $this->is_available($val['product_id']);
				$productobject->url = $val['url'];
				$productobject->brand_name = $val['brand_name'];
				$productobject->p_image = $val['p_image'];
				$size_category = $this->size_category(@$category[1]);
				$productobject->size_category = $size_category['size_guide'];
				$productobject->product_size = $this->product_size($val['product_id']);
				$brand_discount = $this->brand_discount($val['brand_id']);
				$product_discount = $this->product_discount($val['product_id']);
				$discount_price = 0;
				$discount_percent = 0;
				if(!empty($brand_discount)){
					$productobject->discount_percent = $brand_discount['discount_percent'];
					$discount_price = $val['price'] - ($val['price'] * ($brand_discount['discount_percent']/100));
					$productobject->discount_price = (string)($val['price'] - ($val['price'] * ($brand_discount['discount_percent']/100)));
				}else if(!empty($product_discount)){
					$productobject->discount_percent = $product_discount['discount_percent'];
					$discount_price = $val['price'] - ($val['price'] * ($product_discount['discount_percent']/100));
					$productobject->discount_price = (string)($val['price'] - ($val['price'] * ($product_discount['discount_percent']/100)));
				}else {
					$productobject->product_discount = '0';
					$discount_price = 0;
					$productobject->discount_price = '';
				}
				
				if($discount_price < $val['price']){ $productobject->on_sale = 'ON SALE'; }
				else $productobject->on_sale = '';
				
				/*  added for new discount compare price code start*/
				
				// $val['compare_price'] = 100;$val['price']=80;$discount_percent = 10;$discount_price=72;
				if($discount_percent > 0 || $val['compare_price']>$val['price']){
					$productobject->price = (string)$val['compare_price'];
					$productobject->discount_price = (string)round($discount_price);
					$new_discount_percent = $this->calculate_discount_percent($val['compare_price'],$discount_price,$val['price']);
					$productobject->discount_percent = (string)$new_discount_percent;
				}else {
					$productobject->price = (string)$val['price'];
					$productobject->discount_price = (string)round($discount_price);
					$val['compare_price'] = $val['price'];
					$new_discount_percent = $this->calculate_discount_percent($val['compare_price'],$discount_price,$val['price']);
					$productobject->discount_percent = (string)$new_discount_percent;
				}
					
				
				/*  added for new discount compare price code end*/
				$productobject->is_wishlisted = $this->is_wishlisted('product',$val['product_id'],$user_id);
				//$productobject->share_url = base_url().'product/'.$val['slug'].'-'.$val['product_id'];
				$productobject->share_url = base_url().'product/details/'.$val['slug'].'-'.$val['product_id'];
				$productobject->size_guide_url = $this->get_size_guide_n($size_category['size_guide'],$val['product_id'],$val['brand_id']);
				$productobject->brand_desc = $val['brand_desc'];
				$productobject->brand_slug = $val['brand_slug'];
				$productobject->product_slug = $val['product_slug'];
				$product_array[$i] = $productobject;
				$i++;
			}//end of for each
				
		}//end of if
		if(@$_GET['test'] == 1){
			echo "<pre>";print_r($product_array);exit;
		}		
		if(!empty($product_array)) return $product_array[0]; else return null;
	}
	
	function get_products_list($product_ids,$data){
		if(@$data['limit']>0){ $data['limit_cond'] = " LIMIT ".$data['offset'].",".$data['limit']; }else { $data['limit_cond'] = "";}
		if(@$data['order_by'] == ''){ $data['order_by']=''; }
		if(@$data['search_by'] == '1'){ $product_id_in = implode(',',@$data['table_search']); $product_id_in = trim($product_id_in,","); }else{ $product_id_in = implode(',',$product_ids); $product_id_in = trim($product_id_in,","); }
		if($product_id_in != ''){
		// 	$query = $this->db->query("Select DISTINCT p.id as product_id,(select CONCAT(cat.category_name,CONCAT(CONCAT('--',cat.id),CONCAT('--',category_slug))) as cat from category_object_relationship as co,category as cat where co.category_id = cat.id and co.object_type = 'product' and co.object_id = p.id order by co.category_id desc limit 1) category,p.brand_id as brand_id,p.name,p.description,p.slug,p.price,p.url as buy_now_url,CONCAT('".$data['product_link']."',p.image) as product_img,b.company_name as brand_name,CASE 
		// 	WHEN  p.discount_start_date <= CURRENT_TIMESTAMP() and p.discount_end_date >= CURRENT_TIMESTAMP() then p.compare_price
		// 	ELSE 0
		// END AS compare_price,c.user_name as brand_slug,b.short_description as brand_desc,p.slug as product_slug,p.image from product_desc as p,brand_info b,user_login as c,product_inventory as d where (p.approve_reject='A' OR p.approve_reject = 'D' OR p.approve_reject = 'BD' OR p.approve_reject = 'B') AND p.id IN(".$product_id_in.") AND p.brand_id = b.user_id AND b.user_id = c.id AND p.id = d.product_id AND d.stock_count > 0 AND p.status = '1' ".@$data['order_by']." ".$data['limit_cond']);

		$query = $this->db->query("Select DISTINCT p.id as product_id,(select CONCAT(cat.category_name,CONCAT(CONCAT('--',cat.id),CONCAT('--',category_slug))) as cat from category_object_relationship as co,category as cat where co.category_id = cat.id and co.object_type = 'product' and co.object_id = p.id order by co.category_id desc limit 1) category,p.brand_id as brand_id,p.name,p.description,p.slug,p.price,p.url as buy_now_url,CONCAT('".$data['product_link']."',p.image) as product_img,p.image,b.company_name as brand_name,CASE 
			WHEN  p.discount_start_date <= CURRENT_TIMESTAMP() and p.discount_end_date >= CURRENT_TIMESTAMP() then p.compare_price
			ELSE 0
			END AS compare_price,p.compare_price as mrp,c.user_name as brand_slug,b.short_description as brand_desc,p.slug as product_slug,p.sku_number,p.stock_count from product_desc as p,brand_info b,user_login as c where p.id IN(".$product_id_in.") AND p.brand_id = b.user_id AND b.user_id = c.id AND p.stock_count > 0 ".@$data['order_by']." ".$data['limit_cond']);
			 
			$res = $query->result_array();
			$product_array = array();$category = array();
			$i=0;
			if(!empty($res)){
				foreach($res as $val){
					
					$productobject = new stdClass(); 
					$productobject->product_id = $val['product_id'];
					if($val['brand_id'] != 40030){
						$productobject->product_img = $val['product_img'];
					}else{
						//$productobject->product_img = 'https://www.stylecracker.com/sc_admin/assets/product_images/normal/'.$val['product_img'];
						$productobject->product_img = base_url().'sc_admin/assets/product_images/normal/'.$val['product_img'];
					}

					//$productobject->product_img_name = @$val['product_img'];
					$productobject->product_img_name = @$val['image'];
					$productobject->product_name = $val['name'];
					if(!empty($val['category'])){ $category = explode('--',$val['category']);}
					$productobject->product_category = @$category[0];
					$productobject->product_category_id = @$category[1];
					$productobject->product_category_slug = @$category[2];
					$productobject->brand_name = $val['brand_name'];
					$productobject->brand_id = $val['brand_id'];
					
					$brand_discount = $this->brand_discount($val['brand_id']);
					$product_discount = $this->product_discount($val['product_id']);
					$discount_price = 0;
					$discount_percent = 0;
					if(!empty($brand_discount)){
						$productobject->discount_percent = $brand_discount['discount_percent'];
						$discount_price = $val['price'] - ($val['price'] * ($brand_discount['discount_percent']/100));
						$productobject->discount_price = (string)($val['price'] - ($val['price'] * ($brand_discount['discount_percent']/100)));
						// number_format($number, 2, ',', '')
					}else if(!empty($product_discount)){
						$productobject->discount_percent = $product_discount['discount_percent'];
						$discount_price = $val['price'] - ($val['price'] * ($product_discount['discount_percent']/100));
						$productobject->discount_price = (string)($val['price'] - ($val['price'] * ($product_discount['discount_percent']/100)));
					}else {
						$productobject->product_discount = '0';
						$discount_price = 0;
						$productobject->discount_price = '';
					}
					
					if($discount_price < $val['price']){ $productobject->on_sale = 'ON SALE'; }
					else $productobject->on_sale = '';
					
					/*  added for new discount compare price code start*/
					
					// $val['compare_price'] = 100;$val['price']=80;$discount_percent = 10;$discount_price=72;
					if($discount_percent > 0 || $val['compare_price']>$val['price']){
						$productobject->price = (string)$val['compare_price'];
						$productobject->discount_price = (string)round($discount_price);
						$new_discount_percent = $this->calculate_discount_percent($val['compare_price'],$discount_price,$val['price']);
						$productobject->discount_percent = (string)$new_discount_percent;
					}else {
						$productobject->price = (string)$val['price'];
						$productobject->discount_price = (string)round($discount_price);
						$val['compare_price'] = $val['price'];
						$new_discount_percent = $this->calculate_discount_percent($val['compare_price'],$discount_price,$val['price']);
						$productobject->discount_percent = (string)$new_discount_percent;
					}
						
					/*  added for new discount compare price code end*/
					
					$productobject->is_wishlisted = $this->is_wishlisted('product',$val['product_id'],$data['user_id']);
					$productobject->is_available = $this->is_available($val['product_id']);
					$productobject->product_size = $this->product_size($val['product_id']);
					//$productobject->share_url = base_url().'product/'.$val['slug'].'-'.$val['product_id'];
					$productobject->share_url = base_url().'product/details/'.$val['slug'].'-'.$val['product_id'];
					$productobject->brand_slug = $val['brand_slug'];
					$productobject->brand_desc = $val['brand_desc'];
					$productobject->product_slug = $val['product_slug'];
					$productobject->sku_number = $val['sku_number'];
					$productobject->stock_count = $val['stock_count'];
					$productobject->mrp = $val['mrp'];
					$productobject->product_store_name = $this->get_product_store($val['product_id'])[0]['brand_store_name'];
					$product_array[$i] = $productobject;
					$i++;
				}//end of for each
					
			}//end of if 
		}//end of product_in if =
		
		if(@$data['order_by'] == ''){ 
			shuffle($product_array); 
		}		
		if(!empty(@$product_array)) return $product_array; else return array();
	}
	
	function is_available($product_id){
		$query = $this->db->query("SELECT a.stock_count FROM product_inventory a WHERE a.product_id = $product_id AND a.stock_count > 0");
		$res = $query->result_array();
		// echo $res[0]['stock_count'];
		// print_r($res);exit;
		if($res != NULL && $res[0]['stock_count']>0)
			return '1';
		else return '0';
	}
	
	function size_category($product_cat_id){       
		/*$size_array =  $this->db->query("select n.`size_guide` from `product_variations` as m,`product_variation_type` as n,product_desc c where m.`product_var_type_id` = n.`id` and m.`product_id` = c.id AND c.id = $product_id order by n.size_guide DESC ");*/
		$size_array =  $this->db->query("select a.`size_guide` from `category` as a where a.id = $product_cat_id");
		$res1 = $size_array->result_array();
		if(!empty($res1)) return $res1[0]; else return null;
	}
	
	function product_size($product_id){       
	$size_array =  $this->db->query("select b.size_text,a.size_id from product_inventory a,product_size b where a.product_id = $product_id And a.size_id = b.id AND a.stock_count>0 GROUP BY b.id");
	$res1 = $size_array->result();
	return $res1;
	}
	
	function brand_discount($brand_id){
		$query = $this->db->query(" select IF(a.discount_percent is null, '0', a.discount_percent) as discount_percent FROM discount_availed AS a WHERE a.brand_id = $brand_id AND a.discount_type_id = 1 AND NOW() >= a.start_date AND NOW() <= a.end_date AND a.status=1 ");
		$res = $query->result_array();
		if (!empty($res)) return $res[0]; else return null;
	}
	
	function product_discount($product_id){
		$query = $this->db->query("SELECT  IF(sum(a.discount_percent) is null, '0', sum(a.discount_percent)) as discount_percent FROM discount_availed AS a WHERE a.product_id = $product_id AND a.discount_type_id = 2 AND NOW() >= a.start_date AND NOW() <= a.end_date AND a.status=1 ");
		$res = $query->result_array();
		if (!empty($res)) return $res[0]; else return null;
	}
	
	function get_product_related_looks($product_id){
		
		$query = $this->db->query("select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime as timestamp,@type:='look' as type from 
		(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN (select distinct u.`look_id` from `look_products` as u,`product_desc` as i where u.`product_id` = i.`id` and u.`product_id` = $product_id )) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4|| z.look_type = 6) order by z.modified_datetime DESC ");
		$res = $query->result_array();
		$looks_array = $this->looks_array($res);
		return $looks_array;
		
    }
	
	/* old product query 
    function get_product_related_products($product_id){
        $query = $this->db->query("select distinct a.`id`,a.brand_id,a.name,a.url,a.price,a.image,c.company_name,c.`is_paid`,a.`description`
                from product_desc as a,brand_info as c where a.status = 1 AND a.approve_reject='A'
                and a.price>0 and c.`user_id` = a.`brand_id` and a.id in (select distinct product_id from product_variations  where 
                product_var_val_id in (SELECT product_var_val_id FROM product_variations where product_id = $product_id and product_var_val_id != 0))
                order by c.is_paid desc limit 10");
		$res = $query->result_array();
		$product_array = array();
		$i=0;
		foreach($res as $row){
		$product_array[$i]=$this->single_product_data($row['id']);	
		$i++;
		}
        return $product_array;

    }
	*/ 
	
	function get_product_related_products($product_id,$product_related,$user_id=0){
		
        $cat_id = '';
        $sql = $this->db->query("select category_id from category_object_relationship where object_id=".$product_id." and object_type='product' order by category_id desc");

        $result = $sql->result_array();
        
        if(!empty($result) && $result[0]['category_id']){
            $cat_id = ' and d.category_id IN ('.$result[0]['category_id'].')';
        }

        $sql ="select distinct group_concat(a.`id`) as product_ids,a.brand_id,a.name,a.slug,a.url,a.price,a.image,c.company_name,c.`is_paid`,a.`description`,a.compare_price  from product_desc as a,brand_info as c,category_object_relationship as d,product_inventory as b where a.status = 1 and a.is_delete = 0 and a.price>0 and c.`user_id` = a.`brand_id` and d.object_id=a.id and d.object_type='product' and a.id !='".$product_id."' ".$cat_id."  AND a.id=b.product_id AND b.stock_count>0 order by a.modified_datetime desc limit 4";

        $result = $this->db->query($sql);
		$res = $result->result_array();
		// print_r($res);exit;	
		
		if(!empty($res)){
			
			$data['user_id'] = $user_id;
			$data['offset'] = '0';
			$data['limit'] = '4';
			//$data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
			$data['product_link'] = base_url().'/sc_admin/assets/products/thumb_310/';
			$product_ids = explode(" ",@$res[0]['product_ids']);
			$product_array = $this->get_products_list($product_ids,$data);
			return $product_array; 
			
		}else { 
			return null;
		}
		
		
		/*$i=0;
		foreach($res as $row){
			$product_array[$i]=$this->single_product_data($row['id'],$user_id);	
			$i++;
		}
		
		if(!empty($product_array)){ 
			return $product_array; 
		}else { 
			return null;
		}*/		
             
        

    }
	
	function looks_array($res){
		
		$looks_array = array();
			
			$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
					$alllooksarrayobject = new stdClass(); 
					$alllooksarrayobject->id = $val['id'];
					$alllooksarrayobject->name = $val['name'];
					$alllooksarrayobject->timestamp = $val['timestamp'];
					if($val['look_type'] == 1){
						$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
					}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
						$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
					}else if($val['look_type'] == 3){
						$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
					}
					/*$info = getimagesize($alllooksarrayobject->url);
					$alllooksarrayobject->height = $info[1];
					$alllooksarrayobject->width  = $info[0];*/
					$alllooksarrayobject->height = 530;
					$alllooksarrayobject->width  = 530;
					$alllooksarrayobject->type = $val['type'];
					$alllooksarrayobject->look_type = $val['look_type'];
					// $alllooksarrayobject->user_share = $this->get_user_share($val['id']);
					// $alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
					// $alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
					$alllooksarrayobject->user_share = "0";
					$alllooksarrayobject->user_likes = "0";
					$alllooksarrayobject->look_type_txt = "Cocktail";
					$alllooksarrayobject->share_url = 'https://www.stylecracker.com/looks/look-details/'.$val['slug'];
					$alllooksarrayobject->is_discount = (string)$this->get_discount($val['id']);
					$looks_array[$i] = $alllooksarrayobject;
					$i++;
				}
			}
			
			return $looks_array;
		
	}
	
	function get_user_share($look_id){
		$query = $this->db->query("select COUNT(user_share.look_id) as user_share from user_share where user_share.look_id='".$look_id."'");
		$res = $query->result_array();
		return $res[0]['user_share'];
		
	}
	
	function get_user_likes($look_id){
		$query = $this->db->query("select COUNT(look_fav.look_id) as look_fav from look_fav where look_fav.look_id='".$look_id."'");
		$res = $query->result_array();
		return $res[0]['look_fav'];
		
	}
	
	function get_sub_category($look_id){
		$query = $this->db->query("select ls.category_name from look_category_mapping lcm,look_sub_category ls WHERE ls.id = lcm.cat_id and lcm.look_id='".$look_id."' limit 0,1");
		$res = $query->result_array(); 
		return isset($res[0]['category_name']) ? $res[0]['category_name'] : '';
		
	}
	
	function get_discount($look_id){
		$query = $this->db->query("select a.look_id,a.product_id,c.user_id as brand_id from look_products as a,product_desc as b,brand_info as c where a.look_id= $look_id AND a.product_id = b.id AND b.brand_id = c.user_id");
		$res = $query->result_array();
		$is_discount = 0;
		$i = 0;
		if(!empty($res)){
			foreach($res as $val){
				$discount = $this->get_discount_id($val['brand_id'],$val['product_id']);
				//print_r($discount);exit;
				if(!empty($discount)){
					$is_discount = 1;
				}//else if($is_discount == 1) $is_discount = 1;
				$i++;
			}
		}
		if($is_discount == 1){ return 1; } else return 0;
	}
	
	function get_discount_id($brand_id,$product_id){
          $query = $this->db->query("select id, discount_type_id from discount_availed as da where (da.brand_id = '".$brand_id."' OR da.product_id = '".$product_id."' ) AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1 ");
		  $result = $query->result_array();
		  if(!empty($result)) return $result; else return null;
	}
	
	function get_looks_by_brand($brand_id,$limit,$offset){
			$query = $this->db->query("select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime as timestamp,@type:='look' as type from 
 (SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN (select distinct u.`look_id` from `look_products` as u,`product_desc` as i where u.`product_id` = i.`id` and i.`brand_id` = $brand_id )) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4|| z.look_type = 6) order by z.modified_datetime DESC $limit $offset");

           $res = $query->result_array();
		   $looks_array = array();
			
			$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
					$alllooksarrayobject = new stdClass(); 
					$alllooksarrayobject->id = $val['id'];
					$alllooksarrayobject->name = $val['name'];
					$alllooksarrayobject->timestamp = $val['timestamp'];
					if($val['look_type'] == 1){
						$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
					}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
						$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
					}else if($val['look_type'] == 3){
						$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
					}
					$info = getimagesize($alllooksarrayobject->url);
					//if($info[1] == NULL || $info[1] == '' ) $info[1] = 0;
					//if($info[0] == NULL || $info[0] == '' ) $info[0] = 0;
					$alllooksarrayobject->height = $info[1];
					$alllooksarrayobject->width  = $info[0];
					$alllooksarrayobject->type = $val['type'];
					$alllooksarrayobject->look_type = $val['look_type'];
					$alllooksarrayobject->user_share = $this->get_user_share($val['id']);
					$alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
					$alllooksarrayobject->is_discount = (string)$this->get_discount($val['id']);
					$alllooksarrayobject->share_url = 'https://www.stylecracker.com/looks/look-details/'.$val['slug'];
					$alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
					$looks_array[$i] = $alllooksarrayobject;
					$i++;
				}
			}
          return $looks_array;
		
	}
	
	function get_brands_product($brand_id,$limit,$offset){
		
        $query = $this->db->query(" select id,name,price,image,slug,brand_id from product_desc WHERE  brand_id = $brand_id and (approve_reject='A' OR approve_reject = 'D' OR approve_reject = 'B') and is_delete = 0 order by id desc $limit $offset ");
        $product_data = $query->result_array();
		$product_array = array();$i=0;
			if(!empty($product_data)){
			foreach($product_data as $val){
				$product_array[$i] = $this->single_product_data($val['id']);
				$i++;
			}
			}
       return $product_array;
    }
	
	function get_brand_info($brand_id){
		
		$query2 = $this->db->query("select user_id as brand_id,company_name,IF(b.cover_image ='','http://www.stylecracker.com/assets/images/brands/brand-cover-photo.jpg',CONCAT('http://www.stylecracker.com/assets/images/brands/',b.cover_image)) as cover_image,IF( short_description IS NULL ,'', short_description ) as short_description,IF( long_description IS NULL ,'', long_description ) as long_description,IF( email IS NULL ,'NA', email ) AS email,IF( overall_rating IS NULL , 0, 1 ) AS overall_rating,IF(logo IS NULL,'https://www.stylecracker.com/assets/images/brands/brand-logo-default.png',CONCAT('http://www.stylecracker.com/sc_admin/assets/brand_logo/',logo)) as logo_url,IF( contact IS NULL ,'NA', contact ) AS contact,(select COUNT(brand_review.review_text)  from brand_review where brand_review.brand_id = b.user_id AND brand_review.status=1 limit 0,1) as user_review,(select COUNT(brand_review.ratings) from brand_review where brand_review.brand_id = b.user_id AND brand_review.status=1 limit 0,1) as user_ratings  from brand_info as b where b.`show_in brand_list`=1 AND b.user_id = $brand_id");
		
		
        $res2 = $query2->result_array();
		$looks_array = array();
			
			$i = 0; 
			
			if(!empty($res2)){
				foreach($res2 as $val){
					
					$allbrandarrayobject = new stdClass(); 
					$allbrandarrayobject->brand_id = $val['brand_id'];
					$allbrandarrayobject->company_name = $val['company_name'];
					$allbrandarrayobject->cover_image = $val['cover_image'];
					$allbrandarrayobject->short_description = $val['short_description'];
					$allbrandarrayobject->long_description = $val['long_description'];
					$allbrandarrayobject->email = $val['email'];
					$query_count = $this->db->query("select ratings as ratings from brand_review where brand_id = '".$val['brand_id']."' AND status=1");
        $res_count = $query_count->result_array();

        $query_rating = $this->db->query("select sum(z.ratings) as ratings from(select (count(ratings)*ratings) as ratings from brand_review where brand_id = '".$val['brand_id']."' AND status=1 group by ratings) as z");
        $res_avg = $query_rating->result_array();
					if(!empty($res_avg)){
					if(sizeof($res_count) != 0){
					$allbrandarrayobject->overall_rating = $res_avg[0]['ratings']/sizeof($res_count);
					}else{
					$allbrandarrayobject->overall_rating = 0;
					}
					}else{
					$allbrandarrayobject->overall_rating = 0;
					}
					
					$allbrandarrayobject->logo_url = $val['logo_url'];
					$allbrandarrayobject->contact = $val['contact'];
					$allbrandarrayobject->user_review = $val['user_review'];
					$allbrandarrayobject->user_ratings = $val['user_ratings'];
					$looks_array[$i] = $allbrandarrayobject;
					$i++;
				} 
		}
        return $looks_array[0];
	}
	
	function filter_products($brandIds=NULL,$colorId=NULL,$sc_offers=NULL,$price=NULL,$min_price=NULL,$max_price=NULL,$discount=NULL,$look_type=NULL,$category_slug=NULL,$category_level=NULL,$product_sort_by=NULL,$limit,$offset){
        // $offset = ($offset-1)*12;
        // if($offset<0) { $offset = 0; } 
        $product_data = array(); $where_cond = ''; $order_by = '';

        if(($category_slug==NULL && $category_level==NULL) || ($category_slug=='subcategory' && $category_level!=NULL)){

            if($discount == NULL){
                /* Main category selection from URL or From Filter selection*/
                if($look_type == 'women'){ $where_cond = $where_cond.' and product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and product_cat_id='.MEN_CATEGORY_ID; } 

                /* Sub-category selection */
                if($this->get_cat_id($category_slug,$category_level) > 0){ $where_cond = $where_cond.' and product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                /* Brands Selection*/
                if($brandIds!=''){
                    $where_cond = $where_cond.' and brand_id IN('.$brandIds.')';
                }

                /* Color Selection */
                if($colorId!=''){
                    $where_cond = $where_cond.' and sc_product_color IN('.$colorId.')';
                }

                /* Price Selection */
                if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                    if($price == 1) { $where_cond = $where_cond.' and price < 500 ';  }
                        else if($price == 2) { $where_cond = $where_cond.' and (price >= 500 ) and (price <= 999 )';  }
                        else if($price == 3) { $where_cond = $where_cond.' and (price >= 1000 ) and (price <= 1999 ) ';  }
                        else if($price == 4) { $where_cond = $where_cond.' and (price >= 2000 ) and (price <= 2999 ) ';  }
                        else if($price == 5) { $where_cond = $where_cond.' and price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                       $where_cond = $where_cond.' and (price >= '.$min_price.' ) and (price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and price >= '.$max_price;
                    }
                }

                /* Offers Selection*/
                if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                    $where_cond = $where_cond.' and id IN ('.$this->get_offer_products($sc_offers).')'; 
                }

                /* Order by*/

                if($product_sort_by > 0){
                    if($product_sort_by == 1) { $order_by = ' order by price desc'; }else{ $order_by = ' order by price asc'; }
                }else{
                    $order_by = ' order by id desc';
                }

                $query = $this->db->query('select distinct id,name,price,image,slug,brand_id from product_desc WHERE  (approve_reject="A" OR approve_reject = "D"  OR approve_reject = "B") and is_delete = 0 and price > 0 '.$where_cond.$order_by.'  '.$limit.' '.$offset.' ');
            }else { 

                if($discount != NULL){


                        /* Main category selection from URL or From Filter selection*/
                    if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                    /* Sub-category selection */
                    if($this->get_cat_id($category_slug,$category_level) > 0){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                    /* Brands Selection*/
                    if($brandIds!=''){
                        $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                    }

                    /* Color Selection */
                    if($colorId!=''){
                        $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                    }

                    /* Price Selection */
                    if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                        if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                            else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                            else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                            else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                            else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                        if($min_price > 0 && $max_price>0){
                           $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                        }else if($min_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$min_price;
                        }else if($max_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$max_price;
                        }
                    }

                    /* Offers Selection*/
                    if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                        $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                    }

                    /* Order by*/

                        if($product_sort_by > 0){
                            if($product_sort_by == 1) { $order_by = ' order by a.price desc'; }else{ $order_by = ' order by a.price asc'; }
                        }else{
                            $order_by = ' order by a.id desc';
                        }

                    if($discount == 1){
                    $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug from product_desc as a,discount_availed as b WHERE CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id = b.product_id and (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.price > 0 and a.is_delete = 0 '.$where_cond.$order_by.' '.$limit.' '.$offset.' ');
                    }else if($discount == 2){


                        $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug from product_desc as a,discount_availed as b WHERE CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id != b.product_id and (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.price > 0 and a.is_delete = 0 '.$where_cond.$order_by.' '.$limit.' '.$offset.' ');
                    }
                }
            }
            $product_data = $query->result_array();

        }else{
            if($this->get_cat_id($category_slug,$category_level) >0){
                if($category_slug == 'variation'){
                    $where_cond = $where_cond.' and b.product_var_type_id='.$this->get_cat_id($category_slug,$category_level); 
                }else if($category_slug == 'variations'){
                    $where_cond = $where_cond.' and b.product_var_val_id='.$this->get_cat_id($category_slug,$category_level); 
                }
            }
            /* Else condition started */
                if($discount == NULL){
                /* Main category selection from URL or From Filter selection*/
                if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                /* Sub-category selection */
                if($this->get_cat_id($category_slug,$category_level) > 0){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                /* Brands Selection*/
                if($brandIds!=''){
                    $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                }

                /* Color Selection */
                if($colorId!=''){
                    $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                }

                /* Price Selection */
                if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                    if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                        else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                        else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                        else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                        else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                       $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$max_price;
                    }
                }

                /* Offers Selection*/
                if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                    $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                }

                  /* Order by*/

                  if($product_sort_by > 0){
                    if($product_sort_by == 1) { $order_by = ' order by a.price desc'; }else{ $order_by = ' order by a.price asc'; }
                  }else{
                    $order_by = ' order by a.id desc';
                  }

                $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug from product_desc as a,product_variations as b WHERE (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B")  and a.price > 0 and a.is_delete = 0 and a.id = b.product_id '.$where_cond.$order_by.' '.$limit.' '.$offset.' ');
            }else { 

                if($discount > 0){


                        /* Main category selection from URL or From Filter selection*/
                    if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                    /* Sub-category selection */
                    if($this->get_cat_id($category_slug,$category_level) > 0){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                    /* Brands Selection*/
                    if($brandIds!=''){
                        $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                    }

                    /* Color Selection */
                    if($colorId!=''){
                        $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                    }

                    /* Price Selection */
                    if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                        if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                            else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                            else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                            else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                            else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                        if($min_price > 0 && $max_price>0){
                           $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                        }else if($min_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$min_price;
                        }else if($max_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$max_price;
                        }
                    }

                    /* Offers Selection*/
                    if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                        $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                    }

                      /* Order by*/

                  if($product_sort_by > 0){
                    if($product_sort_by == 1) { $order_by = ' order by a.price desc'; }else{ $order_by = ' order by a.price asc'; }
                  }else{
                    $order_by = ' order by a.id desc';
                  }

                    if($discount == 1){
                     $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug from product_desc as a,product_variations as b,`discount_availed` as c WHERE CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id = c.product_id and (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.price > 0 and a.is_delete = 0 and a.id = b.product_id '.$where_cond.$order_by.' '.$limit.' '.$offset.' ');
                    }else if($discount == 2){
                        $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug from product_desc as a,product_variations as b,`discount_availed` as c WHERE CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id != c.product_id and a.price > 0 and (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.is_delete = 0 and a.id = b.product_id '.$where_cond.$order_by.' '.$limit.' '.$offset.' ');
                    }
                }
            }
            $product_data = $query->result_array();
        }

			$product_array = array();$i=0;
			if(!empty($product_data)){
			foreach($product_data as $val){
				$product_array[$i] = $this->single_product_data($val['id']);
				$i++;
			}
			
			}
		return $product_array;

    }
	
	function filter_products_total($offset=NULL,$brandIds=NULL,$colorId=NULL,$sc_offers=NULL,$price=NULL,$min_price=NULL,$max_price=NULL,$discount=NULL,$look_type=NULL,$category_slug=NULL,$category_level=NULL,$product_sort_by=NULL){
        $product_data = array(); $where_cond = '';

        if(($category_slug==NULL && $category_level==NULL) || ($category_slug=='subcategory' && $category_level!=NULL)){

            if($discount == NULL){
                /* Main category selection from URL or From Filter selection*/
                if($look_type == 'women'){ $where_cond = $where_cond.' and product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and product_cat_id='.MEN_CATEGORY_ID; } 

                /* Sub-category selection */
                if($this->get_cat_id($category_slug,$category_level) > 0){ $where_cond = $where_cond.' and product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                /* Brands Selection*/
                if($brandIds!=''){
                    $where_cond = $where_cond.' and brand_id IN('.$brandIds.')';
                }

                /* Color Selection */
                if($colorId!=''){
                    $where_cond = $where_cond.' and sc_product_color IN('.$colorId.')';
                }

                /* Price Selection */
                if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                    if($price == 1) { $where_cond = $where_cond.' and price < 500 ';  }
                        else if($price == 2) { $where_cond = $where_cond.' and (price >= 500 ) and (price <= 999 )';  }
                        else if($price == 3) { $where_cond = $where_cond.' and (price >= 1000 ) and (price <= 1999 ) ';  }
                        else if($price == 4) { $where_cond = $where_cond.' and (price >= 2000 ) and (price <= 2999 ) ';  }
                        else if($price == 5) { $where_cond = $where_cond.' and price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                       $where_cond = $where_cond.' and (price >= '.$min_price.' ) and (price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and price >= '.$max_price;
                    }
                }

                /* Offers Selection*/
                if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                    $where_cond = $where_cond.' and id IN ('.$this->get_offer_products($sc_offers).')'; 
                }

                $query = $this->db->query('select count(distinct id) as cnt from product_desc WHERE  (approve_reject = "A" || approve_reject = "D" || a.approve_reject = "B") and is_delete = 0 '.$where_cond.' order by id desc');
            }else { 

                if($discount != NULL){


                        /* Main category selection from URL or From Filter selection*/
                    if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                    /* Sub-category selection */
                    if($this->get_cat_id($category_slug,$category_level) > 0){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                    /* Brands Selection*/
                    if($brandIds!=''){
                        $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                    }

                    /* Color Selection */
                    if($colorId!=''){
                        $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                    }

                    /* Price Selection */
                    if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                        if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                            else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                            else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                            else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                            else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                        if($min_price > 0 && $max_price>0){
                           $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                        }else if($min_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$min_price;
                        }else if($max_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$max_price;
                        }
                    }

                    /* Offers Selection*/
                    if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                        $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                    }
                    if($discount == 1){
                    $query = $this->db->query('select count(distinct a.id) as cnt from product_desc as a,discount_availed as b WHERE CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id = b.product_id and (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.is_delete = 0 '.$where_cond.' order by a.id desc');
                    }else if($discount == 2){


                        $query = $this->db->query('select count(distinct a.id) as cnt from product_desc as a,discount_availed as b WHERE CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id != b.product_id and (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.is_delete = 0 '.$where_cond.' order by a.id desc ');
                    }
                }
            }
            $product_data = $query->result_array();

        }else{
            if($this->get_cat_id($category_slug,$category_level) >0){
                if($category_slug == 'variation'){
                    $where_cond = $where_cond.' and b.product_var_type_id='.$this->get_cat_id($category_slug,$category_level); 
                }else if($category_slug == 'variations'){
                    $where_cond = $where_cond.' and b.product_var_val_id='.$this->get_cat_id($category_slug,$category_level); 
                }
            }
            /* Else condition started */
                if($discount == NULL){
                /* Main category selection from URL or From Filter selection*/
                if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                /* Sub-category selection */
                if($this->get_cat_id($category_slug,$category_level) > 0){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                /* Brands Selection*/
                if($brandIds!=''){
                    $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                }

                /* Color Selection */
                if($colorId!=''){
                    $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                }

                /* Price Selection */
                if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                    if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                        else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                        else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                        else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                        else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                       $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$max_price;
                    }
                }

                /* Offers Selection*/
                if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                    $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                }

                $query = $this->db->query('select count(distinct a.id) as cnt from product_desc as a,product_variations as b WHERE (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.is_delete = 0 and a.id = b.product_id '.$where_cond.' order by a.id desc');
            }else { 

                if($discount > 0){


                        /* Main category selection from URL or From Filter selection*/
                    if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                    /* Sub-category selection */
                    if($this->get_cat_id($category_slug,$category_level) > 0){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                    /* Brands Selection*/
                    if($brandIds!=''){
                        $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                    }

                    /* Color Selection */
                    if($colorId!=''){
                        $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                    }

                    /* Price Selection */
                    if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                        if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                            else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                            else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                            else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                            else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                        if($min_price > 0 && $max_price>0){
                           $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                        }else if($min_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$min_price;
                        }else if($max_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$max_price;
                        }
                    }

                    /* Offers Selection*/
                    if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                        $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                    }
                    if($discount == 1){
                     $query = $this->db->query('select count(distinct a.id) as cnt from product_desc as a,product_variations as b,`discount_availed` as c WHERE CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id = c.product_id and (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.is_delete = 0 and a.id = b.product_id '.$where_cond.' order by a.id desc');
                    }else if($discount == 2){
                        $query = $this->db->query('select count(distinct a.id) as cnt from product_desc as a,product_variations as b,`discount_availed` as c WHERE CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id != c.product_id and (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.is_delete = 0 and a.id = b.product_id '.$where_cond.' order by a.id desc');
                    }
                }
            }
        }
            $product_data = $query->result_array();
            if(!empty($product_data)){
                return $product_data[0]['cnt'];
            }else{
                return 0;
            }

    }
	
	function get_offer_products($sc_offers){
        if($sc_offers>0){
            $query = $this->db->query('select products from `coupon_settings` WHERE id='.$sc_offers);
            $res = $query->result_array();
            if(!empty($res)){
                return $res[0]['products'];
            }
        }
    }

    function get_cat_id($category_level=NULL,$category_slug=NULL){
        $cat_table_name = ''; $product_table='';
          if($category_level == 'cat'){ $cat_table_name = 'product_category'; $product_table='product_desc'; }
            else if($category_level == 'subcategory'){ $cat_table_name = 'product_sub_category'; $product_table='product_desc'; }
            else if($category_level == 'variation'){ $cat_table_name = 'product_variation_type'; $product_table='product_variations'; }
            else if($category_level == 'variations'){ $cat_table_name = 'product_variation_value'; $product_table='product_variations'; }
            if($product_table!=''){
                $query = $this->db->get_where($cat_table_name, array('status' => '1','slug'=>$category_slug));
                $res = $query->result_array();
                return $category_id = @$res[0]['id'];
            }else{
                return 0;
            }
    }
	
	function get_sub_category_list($param=NULL,$sub_cat_slug=NULL){
    	$where_cond = '';
    	$where_cond = $where_cond.' and product_cat_id IN('.WOMEN_CATEGORY_ID.','.MEN_CATEGORY_ID.')';
    	if($param!='' && $param!='all'){
    		$where_cond = $where_cond.' and product_cat_id ='.$param;
    	}
    	if($sub_cat_slug!=''){
    		$where_cond = $where_cond.' and slug ="'.$sub_cat_slug.'"';
    	}

    	$query = $this->db->query('SELECT id,slug,name,product_cat_id FROM `product_sub_category` WHERE status = 1 and is_delete = 0 '.$where_cond);
    	$res = $query->result_array();

    	return $res;
    }

    function get_variation_list($param=NULL,$slug=NULL,$sub_cat_id=NULL,$var_id=NULL){
    	$where_cond = '';
    	if($var_id!=''){
    		$where_cond = $where_cond.' and id ='.$var_id;
    	}
    	if($slug!=''){
    		$where_cond = $where_cond.' and slug ="'.$slug.'"';

    	}if($sub_cat_id!=''){
    		$where_cond = $where_cond.' and prod_sub_cat_id ="'.$sub_cat_id.'"';
    	}

    	$query = $this->db->query('SELECT id,slug,name FROM `product_variation_type` WHERE status = 1 and is_delete = 0 '.$where_cond);
    	$res = $query->result_array();

    	return $res;
    }

    function get_subCat_by_variation($varId){
    	if($varId>0){
    		$query = $this->db->query('SELECT b.id,b.name,b.slug FROM `product_variation_type` as a,product_sub_category as b WHERE a.status = 1 and a.is_delete = 0 and a.prod_sub_cat_id=b.id and a.id="'.$varId.'"');
    		$res = $query->result_array();
    		return $res;
    	}
    }

    function get_variations_list($varId=NULL,$slug=NULL){
    	if($varId>0){
    		$query = $this->db->query('SELECT id,name,slug,product_variation_type_id FROM product_variation_value WHERE status = 1 and is_delete = 0 and product_variation_type_id="'.$varId.'"');
    		$res = $query->result_array();

    		return $res;
    	}else if($slug!=''){
    		$query = $this->db->query('SELECT id,name,slug,product_variation_type_id FROM product_variation_value WHERE status = 1 and is_delete = 0 and slug="'.$slug.'"');
    		$res = $query->result_array();
    		return $res;
    	}

    }

    function get_paid_brands(){
		$query = $this->db->query('SELECT user_id,company_name FROM brand_info WHERE is_paid = 1 ');
        $res = $query->result_array();
		return $res;
    }
	
	function get_brand_collection(){
		$query = $this->db->query("SELECT * FROM brand_category WHERE status=1 AND is_delete=0 AND 	category_type = 'brand_collection' ");
        $res = $query->result_array();
		$brand_collection = array();$i=0;
		foreach($res as $row)
		{
		$brand_collection[$row['category_name']] = $this->get_sub_collection($row['id']);
		$i++;
		}
		return $brand_collection;
    }
	
	function get_all_category(){
		$query = $this->db->query("SELECT id,category_name,category_slug,category_type FROM brand_category WHERE status=1 AND is_delete=0 AND 	category_type = 'brand_cat' ");
        $res = $query->result_array();
		return $res;
    }
	
	function get_sub_collection($category_id){
		$query = $this->db->query("SELECT  a.brand_id,b.company_name,IF(b.listing_page_img  IS NULL,'".base_url()."assets/images/brands/list/default.jpg',CONCAT('https://www.stylecracker.com/assets/images/brands/list/',b.listing_page_img)) as listing_page_img,a.category_id from category_brand_relationship as a,brand_info as b where a.category_id = $category_id AND b.user_id = a.brand_id ");
        $res = $query->result_array();
		return $res;
    }
	
	function search_brand_via_category($category_id){
		$query = $this->db->query("SELECT DISTINCT c.user_id,c.company_name FROM category_brand_relationship as b,brand_info as c where b.category_id =$category_id AND b.brand_id = c.user_id ");
        $res = $query->result_array();
		return $res;
    }
	
	function get_brand_all_names(){
		
		$query2 = $this->db->query("select user_id as brand_id,company_name,IF( short_description IS NULL ,'', short_description ) as long_description,IF( long_description IS NULL ,'', long_description ) as short_description,IF( email IS NULL ,'NA', email ) AS email,IF( overall_rating IS NULL , 0, 1 ) AS overall_rating,IF(logo IS NULL,'https://www.stylecracker.com/assets/images/brands/brand-logo-default.png',CONCAT('http://www.stylecracker.com/sc_admin/assets/brand_logo/',logo)) as logo_url,IF( contact IS NULL ,'NA', contact ) AS contact,(select COUNT(brand_review.review_text)  from brand_review where brand_review.brand_id = brand_info.user_id AND brand_review.status=1 limit 0,1) as user_review,(select COUNT(brand_review.ratings) from brand_review where brand_review.brand_id = brand_info.user_id AND brand_review.status=1 limit 0,1) as user_ratings  from brand_info where `show_in brand_list`=1");
		
		
        $res2 = $query2->result_array();
		$brand_array = array();
			
			$i = 0; 
			
			if(!empty($res2)){
				foreach($res2 as $val){
					
					$allbrandarrayobject = new stdClass(); 
					$allbrandarrayobject->brand_id = $val['brand_id'];
					$allbrandarrayobject->company_name = $val['company_name'];
					$allbrandarrayobject->short_description = $val['short_description'];
				
					$allbrandarrayobject->long_description = $val['long_description'];
					$allbrandarrayobject->email = $val['email'];
					$query_count = $this->db->query("select ratings as ratings from brand_review where brand_id = '".$val['brand_id']."' AND status=1");
        $res_count = $query_count->result_array();

        $query_rating = $this->db->query("select sum(z.ratings) as ratings from(select (count(ratings)*ratings) as ratings from brand_review where brand_id = '".$val['brand_id']."' AND status=1 group by ratings) as z");
        $res_avg = $query_rating->result_array();
					if(!empty($res_avg)){
					if(sizeof($res_count) != 0){
					$allbrandarrayobject->overall_rating = $res_avg[0]['ratings']/sizeof($res_count);
					}else{
					$allbrandarrayobject->overall_rating = 0;
					}
					}else{
					$allbrandarrayobject->overall_rating = 0;
					}
					
					$allbrandarrayobject->logo_url = $val['logo_url'];
					$allbrandarrayobject->contact = $val['contact'];
					$allbrandarrayobject->user_review = $val['user_review'];
					$allbrandarrayobject->user_ratings = $val['user_ratings'];
					$brand_array[$i] = $allbrandarrayobject;
					$i++;
				} 
		}
		
		return $brand_array;
		
	}
	
	function get_megamenu_list($option_name)
    {
         if($option_name!=''){
           $res =  $this->db->get_where('sc_options', array('option_name' => $option_name));          
            return $res->result_array();
        }
    }
	
	function post_review($post_review_text,$ratings,$brand_token,$user_id){
        if($user_id!='' && $post_review_text!='' && $ratings!='' && $brand_token!='')
            $data =array(
                    'look_id' => $brand_token,
                    'user_id' =>  $user_id,
                    'review_text' =>$post_review_text,
                    'ratings' => $ratings,
                    'created_by' => $user_id,
                    'created_datetime'=> $this->config->item('sc_date')
                    );
          $result = $this->db->insert('look_review', $data);
		  $insert_id = $this->db->insert_id();
		  
		if(!empty($result)){
			$query = $this->db->query("select a.*,c.first_name,IF(c.profile_pic='', IF(c.gender = 1,'https://www.stylecracker.com/assets/images/profile/profile-women.jpg','https://www.stylecracker.com/assets/images/profile/profile-men.jpg'), CONCAT('http://www.stylecracker.com/sc_admin/assets/user_profiles/',c.profile_pic)) as profile_pic from look_review as a,user_info as c where  a.id = $insert_id AND a.status=1 AND c.user_id = $user_id limit 1");	
			$res = $query->result();
		   if(!empty($res)) return $res[0]; else return 2;
       }
        
    }
	
	function get_look_review($look_id,$limit,$offset){
		
         $query = $this->db->query("SELECT c.`first_name`,a.`id`, a.`look_id`, a.`user_id`, a.`review_text`, a.`created_by`, a.`modified_by`, a.`created_datetime`, a.`modified_datetime`, a.`status`, a.`ratings`,IF(c.profile_pic='', IF(c.gender = 1,'https://www.stylecracker.com/assets/images/profile/profile-women.jpg','https://www.stylecracker.com/assets/images/profile/profile-men.jpg'), CONCAT('http://www.stylecracker.com/sc_admin/assets/user_profiles/',c.profile_pic)) as profile_pic FROM `look_review` as a,user_login as b,user_info as c WHERE 1 and a.`look_id` = '".$look_id."' and a.status = 1 and a.`user_id` = b.id AND b.id = c.user_id order by a.id desc $limit $offset ");
        
        $res = $query->result_array();
        return $res;
    }
	
	function show_my_wishlist_products($user_id,$fev_for,$limit,$offset){
	    	$query = $this->db->query("select a.id,a.name,a.price,a.image,a.slug from product_desc as a,favorites as c WHERE (a.approve_reject='A' || a.approve_reject = 'D' || a.approve_reject = 'B') and a.is_delete = 0 and a.id = c.fav_id and c.`fav_for` = 'product' and c.`is_deleted` = 0 and c.`user_id` = $user_id order by a.id desc $limit $offset ");
	        $res = $query->result_array();
			$product_array = array();
			$i=0;
			foreach($res as $row){
			$product_array[$i]=$this->single_product_data($row['id']);	
			$i++;
			}
			return $product_array;		
    	
    }
	
	function show_recommended_products($bucket){
        if($bucket > 0){
            $where_condition = ' and m.bucket_id='.$bucket;
            $query = $this->db->query('select distinct k.id,k.slug,product_img,k.look_type,k.image,look_image,name,k.created_datetime,k.modified_datetime,k.status,k.deflag,k.stylist_id from
(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag,z.`stylist_id` from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag,a.`stylist_id` FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc) as k,look_bucket_mapping as m where k.id = m.look_id '.$where_condition.' order by k.modified_datetime desc limit 0,3');
            $res = $query->result_array(); 
            $look_id = ''; $i = 0;
            if(!empty($res)){
                foreach ($res as $value) {
                  
                    if($i != 0) { $look_id = $look_id.','; }
                    $look_id = $look_id.$value['id'];
                    $i++;

                }
            }

            if($look_id!=''){
                $query = $this->db->query('select a.id,a.name,a.price,a.image,a.slug from product_desc as a,look_products as c WHERE (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.is_delete = 0 and a.id = c.product_id and c.look_id in ('.$look_id.') order by a.id desc limit 0,3');
                $res = $query->result_array(); 
                $i=0;
			foreach($res as $row){
			$product_array[$i]=$this->single_product_data($row['id']);	
			$i++;
			}
			return $product_array;
            }

        }
    }
	
	function show_my_wishlist_brands($user_id,$fev_for=null,$limit=null,$offset=null){

    	if($user_id > 0){
	    	// $query = $this->db->query("SELECT a.user_id,CONCAT('".base_url()."assets/images/brands/homepage/',a.home_page_promotion_img) as home_page_promotion_img,a.`company_name` FROM `brand_info` as a,user_login as b,favorites as c WHERE 1 and a.`is_paid` = 1 and a.`user_id` = b.`id` and c.`fav_id` = a.`user_id` and c.`fav_for` = 'brand' and c.`is_deleted` = 0 and c.`user_id` = $user_id order by a.`user_id` desc");
			$query = $this->db->query("SELECT a.user_id,CONCAT('https://www.stylecracker.com/assets/images/brands/homepage/',a.home_page_promotion_img) as home_page_promotion_img,a.`company_name` FROM `brand_info` as a,user_login as b,favorites as c WHERE 1 and a.`is_paid` = 1 and a.`user_id` = b.`id` and c.`fav_id` = a.`user_id` and c.`fav_for` = 'brand' and c.`is_deleted` = 0 and c.`user_id` = $user_id order by a.`user_id` desc $limit  $offset ");
	        $res = $query->result_array(); 
			// echo $this->db->last_query();exit;
	        return $res;
    	}
    }
	
	function show_recommended_brands($bucket){
        if($bucket > 0){
            $where_condition = ' and m.bucket_id='.$bucket;
            $query = $this->db->query('select distinct k.id,k.slug,product_img,k.look_type,k.image,look_image,name,k.created_datetime,k.modified_datetime,k.status,k.deflag,k.stylist_id from
(select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag,z.`stylist_id` from
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag,a.`stylist_id` FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) order by z.modified_datetime desc) as k,look_bucket_mapping as m where k.id = m.look_id '.$where_condition.' order by k.modified_datetime desc limit 0,3');
            $res = $query->result_array(); 
            $look_id = ''; $i = 0; $brand_id=0;
            if(!empty($res)){
                foreach ($res as $value) {
                  
                    if($i != 0) { $look_id = $look_id.','; }
                    $look_id = $look_id.$value['id'];
                    $i++;

                }
            }

            if($look_id!=''){
                $query = $this->db->query('select distinct a.brand_id from product_desc as a,look_products as c WHERE (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.is_delete = 0 and a.id = c.product_id and c.look_id in ('.$look_id.') order by a.id desc');
                $res = $query->result_array(); 
                if(!empty($res)){ $k = 0; 
                    foreach($res as $val){
                        if($k != 0){ $brand_id = $brand_id .','; }
                        $brand_id = $brand_id.$val['brand_id'];
                        $k++;
                    }   
                }
            }

            if($brand_id!=''){
                // $query = $this->db->query("SELECT a.user_id,CONCAT('".base_url()."assets/images/brands/homepage/',a.home_page_promotion_img) as home_page_promotion_img,a.`company_name` FROM `brand_info` as a,user_login as b WHERE 1 and a.`is_paid` = 1 and a.`user_id` = b.`id` and a.`user_id` IN ($brand_id) order by a.`user_id` desc limit 0,3");
				$query = $this->db->query("SELECT a.user_id,CONCAT('https://www.stylecracker.com/assets/images/brands/homepage/',a.home_page_promotion_img) as home_page_promotion_img,a.`company_name` FROM `brand_info` as a,user_login as b WHERE 1 and a.`is_paid` = 1 and a.`user_id` = b.`id` and a.`user_id` IN ($brand_id) order by a.`user_id` desc limit 0,3");
                $res = $query->result_array(); 
                return $res;
            }

        }
    }
	
	function universal_search($brandIds=NULL,$products=NULL,$limit,$offset){
		$where_cond = '';
		if($brandIds!=NULL){
                $where_cond = $where_cond.' and brand_id IN ('.$brandIds.') ';
            }

            if($products!=NULL){
                $where_cond = $where_cond.' and id IN ('.$products.') ';
            }

    		$query = $this->db->query("select id,name,price,image,slug from product_desc WHERE  (approve_reject='A' || approve_reject = 'D' || a.approve_reject = 'B') and is_delete = 0  $where_cond order by id desc $limit $offset");
			$product_data = $query->result_array();
			$i=0;
			$product_array = array();
			foreach($product_data as $row){
			$product_array[$i]=$this->single_product_data($row['id']);	
			$i++;
			}
			return $product_array;
	}
	
	public function get_paid_brands_new($product_cat=NULL,$category_slug=NULL,$category_level=NULL,$brandIds=NULL,$products=NULL){
        $where_cond=''; $cat_table_name=''; $category_id='';
			// echo "product_cat".$product_cat;
			// echo "category_slug".$category_slug;
			// echo "category_level".$category_level;
			// echo "brandIds".$brandIds;
			// echo "products".$products;exit;
            if($category_level == 'cat'){ $cat_table_name = 'product_category'; $product_table='product_desc'; }
            else if($category_level == 'subcategory'){ $cat_table_name = 'product_sub_category'; $product_table='product_desc'; }
            else if($category_level == 'variation' || $category_level == 'variations_type'){ $cat_table_name = 'product_variation_type'; $product_table='product_variations'; }
            else if($category_level == 'variations'){ $cat_table_name = 'product_variation_value'; $product_table='product_variations'; }

            if($category_slug!=''){
                $query = $this->db->get_where($cat_table_name, array('status' => '1','slug'=>$category_slug));
                $res = $query->result_array();
                $category_id = @$res[0]['id'];
            }

        if($category_level!='variation' && $category_level != 'variations'){
            /* Prodcut Category condition - Level 1 main category currently consider only men and women*/
            if($product_cat!=NULL){
                $where_cond = $where_cond.' and a.product_cat_id = '.$product_cat;
            }

            if($category_id!='' && $category_level == 'subcategory'){
                $where_cond = $where_cond.' and a.product_sub_cat_id = '.$category_id;
            }

            if($brandIds!=NULL){
                $where_cond = $where_cond.' and a.brand_id IN ('.$brandIds.') ';
            }

            if($products!=NULL){
                $where_cond = $where_cond.' and a.id IN ('.$products.') ';
            }

            $query = $this->db->query('select distinct a.brand_id as user_id,b.company_name from product_desc as a,brand_info as b WHERE (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.is_delete = 0 and a.brand_id=b.user_id and b.is_paid = 1 '.$where_cond.' order by b.company_name asc');
        }else{

            /* Prodcut Category condition - Level 1 main category currently consider only men and women*/
            if($product_cat!=NULL){
                $where_cond = $where_cond.' and product_cat_id = '.$product_cat;
            }

            if($category_level == 'variation'){ 
                $where_cond = $where_cond.' and b.product_var_type_id='.$category_id; 
            }elseif($category_id){
                $where_cond = $where_cond.' and b.product_var_val_id='.$category_id; 
            }

            if($brandIds!=NULL){
                $where_cond = $where_cond.' and a.brand_id IN ('.$brandIds.') ';
            }

            if($products!=NULL){
                $where_cond = $where_cond.' and a.id IN ('.$products.') ';
            }

            $query = $this->db->query('select distinct a.brand_id as user_id,c.company_name from product_desc as a,product_variations as b,brand_info as c WHERE (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.is_delete = 0 and a.id = b.product_id and a.brand_id=c.user_id and c.is_paid = 1 '.$where_cond.' order by c.company_name asc');
        }
        $product_data = $query->result_array();
        return $product_data;

    }
	/*
	function filter_products_new($offset=NULL,$limit=NULL,$brandIds=NULL,$colorId=NULL,$sc_offers=NULL,$price=NULL,$min_price=NULL,$max_price=NULL,
	$discount=NULL,$look_type=NULL,$category_slug=NULL,$category_level=NULL,$product_sort_by=NULL,$products=NULL){
        
        // $offset = ($offset-1)*12;
        // if($offset<0) { $offset = 0; } 
        $product_data = array(); $where_cond = ''; $order_by = '';

        if(($category_slug==NULL && $category_level==NULL) || ($category_slug=='subcategory' && $category_level!=NULL)){

            if($discount == NULL){
                //  Main category selection from URL or From Filter selection
                if($look_type == 'women'){ $where_cond = $where_cond.' and product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and product_cat_id='.MEN_CATEGORY_ID; } 

                //  Sub-category selection 
                if($this->get_cat_id($category_slug,$category_level) > 0 && $category_slug == 'subcategory'){ $where_cond = $where_cond.' and product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                // Brands Selection
                if($brandIds!=''){
                    $where_cond = $where_cond.' and brand_id IN('.$brandIds.')';
                }

               // Color Selection 
                if($colorId!=''){
                    $where_cond = $where_cond.' and sc_product_color IN('.$colorId.')';
                }

                if($products!=NULL){
                $where_cond = $where_cond.' and id IN ('.$products.') ';
                }

                // Price Selection 
                if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                    if($price == 1) { $where_cond = $where_cond.' and price < 500 ';  }
                        else if($price == 2) { $where_cond = $where_cond.' and (price >= 500 ) and (price <= 999 )';  }
                        else if($price == 3) { $where_cond = $where_cond.' and (price >= 1000 ) and (price <= 1999 ) ';  }
                        else if($price == 4) { $where_cond = $where_cond.' and (price >= 2000 ) and (price <= 2999 ) ';  }
                        else if($price == 5) { $where_cond = $where_cond.' and price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                       $where_cond = $where_cond.' and (price >= '.$min_price.' ) and (price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and price >= '.$max_price;
                    }
                }

                // Offers Selection
                if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                    $where_cond = $where_cond.' and id IN ('.$this->get_offer_products($sc_offers).')'; 
                }

                // Order by

                if($product_sort_by > 0){
                    if($products!=''){
                        if($product_sort_by == 1) { $order_by = ' order by FIELD(id, '.$products.'), price desc'; }else{ $order_by = ' order by  FIELD(id, '.$products.'), price asc'; }
                    }else{
                        if($product_sort_by == 1) { $order_by = ' order by price desc'; }else{ $order_by = ' order by price asc'; }
                    }
                }else if($products){
                    if($products!=''){
                        $order_by = ' order by  FIELD(id, '.$products.')';
                    }else{
                        $order_by = ' order by id desc';
                    }
                }

                $query = $this->db->query('select distinct id,name,price,image,slug,brand_id from product_desc WHERE  approve_reject = "A" and is_delete = 0 and status=1 and price > 0 '.$where_cond.$order_by.' '.$limit.' '.$offset.' ');

            }else { 

                if($discount != NULL){


                        // Main category selection from URL or From Filter selection
                    if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                    //  Sub-category selection 
                    if($this->get_cat_id($category_slug,$category_level) > 0 && $category_slug == 'subcategory'){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                    // Brands Selection
                    if($brandIds!=''){
                        $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                    }

                    // Color Selection 
                    if($colorId!=''){
                        $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                    }

                    if($products!=NULL){
                        $where_cond = $where_cond.' and a.id IN ('.$products.') ';
                    }

                    // Price Selection 
                    if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                        if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                            else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                            else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                            else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                            else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                        if($min_price > 0 && $max_price>0){
                           $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                        }else if($min_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$min_price;
                        }else if($max_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$max_price;
                        }
                    }

                    // Offers Selection
                    if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                        $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                    }

                    // Order by

                          if($product_sort_by > 0){
                            if($products!=''){
                                if($product_sort_by == 1) { $order_by = ' order by FIELD(a.id, '.$products.'), a.price desc'; }else{ $order_by = ' order by  FIELD(a.id, '.$products.'), a.price asc'; }
                                }else{
                                    if($product_sort_by == 1) { $order_by = ' order by a.price desc'; }else{ $order_by = ' order by a.price asc'; }
                             }
                            }else if($products){
                                if($products!=''){
                                    $order_by = ' order by  FIELD(a.id, '.$products.')';
                                }else{
                                    $order_by = ' order by a.id desc';
                                }
                            }

                    if($discount == 1){
                    $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,discount_availed as b WHERE CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id = b.product_id and a.approve_reject = "A" and a.price > 0 and a.is_delete = 0 and a.status=1 '.$where_cond.$order_by.' '.$limit.' '.$offset.' ');
                    }else if($discount == 2){


                        $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,discount_availed as b WHERE CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id != b.product_id and a.approve_reject = "A" and a.price > 0 and a.is_delete = 0 and a.status=1 '.$where_cond.$order_by.' '.$limit.' '.$offset.' ');
                    }
                }
            }
            $product_data = $query->result_array();

        }else{
            if($this->get_cat_id($category_slug,$category_level) >0){
                if($category_slug == 'variation'){
                    $where_cond = $where_cond.' and b.product_var_type_id='.$this->get_cat_id($category_slug,$category_level); 
                }else if($category_slug == 'variations'){
                    $where_cond = $where_cond.' and b.product_var_val_id='.$this->get_cat_id($category_slug,$category_level); 
                }
            }
            // Else condition started 
                if($discount == NULL){
                // Main category selection from URL or From Filter selection
                if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                // Sub-category selection 
                if($this->get_cat_id($category_slug,$category_level) > 0 && $category_slug == 'subcategory'){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

               // Brands Selection
                if($brandIds!=''){
                    $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                }

                // Color Selection 
                if($colorId!=''){
                    $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                }

                if($products!=NULL){
                    $where_cond = $where_cond.' and a.id IN ('.$products.') ';
                }

                // Price Selection 
                if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                    if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                        else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                        else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                        else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                        else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                       $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$max_price;
                    }
                }

                // Offers Selection
                if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                    $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                }

               
                  // Order by

                if($product_sort_by > 0){
                    if($products!=''){
                        if($product_sort_by == 1) { $order_by = ' order by FIELD(id, '.$products.'), price desc'; }else{ $order_by = ' order by  FIELD(id, '.$products.'), price asc'; }
                    }else{
                        if($product_sort_by == 1) { $order_by = ' order by price desc'; }else{ $order_by = ' order by price asc'; }
                    }
                }else if($products){
                    if($products!=''){
                        $order_by = ' order by  FIELD(id, '.$products.')';
                    }else{
                        $order_by = ' order by id desc';
                    }
                }

                $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,product_variations as b WHERE a.approve_reject = "A" and a.price > 0 and a.is_delete = 0 and a.status=1 and a.id = b.product_id '.$where_cond.$order_by.' '.$limit.' '.$offset.' ');

            }else { 

                if($discount > 0){


                        // Main category selection from URL or From Filter selection
                    if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                    // Sub-category selection 
                    if($this->get_cat_id($category_slug,$category_level) > 0 && $category_slug == 'subcategory'){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                    // Brands Selection
                    if($brandIds!=''){
                        $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                    }

                    // Color Selection 
                    if($colorId!=''){
                        $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                    }

                    if($products!=NULL){
                        $where_cond = $where_cond.' and a.id IN ('.$products.') ';
                    }

                    // Price Selection 
                    if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                        if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                            else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                            else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                            else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                            else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                        if($min_price > 0 && $max_price>0){
                           $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                        }else if($min_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$min_price;
                        }else if($max_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$max_price;
                        }
                    }

                    // Offers Selection
                    if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                        $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                    }

                     // Order by
                   if($product_sort_by > 0){
                            if($products!=''){
                                if($product_sort_by == 1) { $order_by = ' order by FIELD(a.id, '.$products.'), a.price desc'; }else{ $order_by = ' order by  FIELD(a.id, '.$products.'), a.price asc'; }
                                }else{
                                    if($product_sort_by == 1) { $order_by = ' order by a.price desc'; }else{ $order_by = ' order by a.price asc'; }
                             }
                            }else if($products){
                                if($products!=''){
                                    $order_by = ' order by  FIELD(a.id, '.$products.')';
                                }else{
                                    $order_by = ' order by a.id desc';
                                }
                    }

                    if($discount == 1){
                     $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,product_variations as b,`discount_availed` as c WHERE CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id = c.product_id and a.approve_reject = "A" and a.price > 0 and a.is_delete = 0 and a.status=1 and a.id = b.product_id '.$where_cond.$order_by.' '.$limit.' '.$offset.' ');
                    }else if($discount == 2){
                        $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,product_variations as b,`discount_availed` as c WHERE CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id != c.product_id and a.price > 0 and a.approve_reject = "A" and a.status=1 and a.is_delete = 0 and a.id = b.product_id '.$where_cond.$order_by.' '.$limit.' '.$offset.' ');
                    }
                }
            }
            $product_data = $query->result_array();

        }

        //echo $this->db->last_query();  
        // return $product_data;
		$product_array = array();$i=0;
			if(!empty($product_data)){
			foreach($product_data as $val){
				$product_array[$i] = $this->single_product_data($val['id']);
				$i++;
			}
			
			}
		return $product_array;


    }*/
	
	function filter_products_new($offset=NULL,$limit=NULL,$brandIds=NULL,$colorId=NULL,$sc_offers=NULL,$price=NULL,$min_price=NULL,$max_price=NULL,$discount=NULL,$look_type=NULL,$category_slug=NULL,$category_level=NULL,$product_sort_by=NULL,$products=NULL){
       
        // $offset = ($offset-1)*12;
        // if($offset<0) { $offset = 0; } 
        $product_data = array(); $where_cond = ''; $order_by = '';

        if(($category_slug==NULL && $category_level==NULL) || ($category_slug=='subcategory' && $category_level!=NULL)){

            if($discount == NULL){
                /* Main category selection from URL or From Filter selection*/
                if($look_type == 'women'){ $where_cond = $where_cond.' and product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and product_cat_id='.MEN_CATEGORY_ID; } 

                /* Sub-category selection */
                if($this->get_cat_id($category_slug,$category_level) > 0 && $category_slug == 'subcategory'){ $where_cond = $where_cond.' and product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                /* Brands Selection*/
                if($brandIds!=''){
                    $where_cond = $where_cond.' and brand_id IN('.$brandIds.')';
                }

                /* Color Selection */
                if($colorId!=''){
                    $where_cond = $where_cond.' and sc_product_color IN('.$colorId.')';
                }

                if($products!=NULL){
                $where_cond = $where_cond.' and id IN ('.$products.') ';
                }

                /* Price Selection */
                if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                    if($price == 1) { $where_cond = $where_cond.' and price < 500 ';  }
                        else if($price == 2) { $where_cond = $where_cond.' and (price >= 500 ) and (price <= 999 )';  }
                        else if($price == 3) { $where_cond = $where_cond.' and (price >= 1000 ) and (price <= 1999 ) ';  }
                        else if($price == 4) { $where_cond = $where_cond.' and (price >= 2000 ) and (price <= 2999 ) ';  }
                        else if($price == 5) { $where_cond = $where_cond.' and price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                       $where_cond = $where_cond.' and (price >= '.$min_price.' ) and (price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and price >= '.$max_price;
                    }
                }

                /* Offers Selection*/
                if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                    $where_cond = $where_cond.' and id IN ('.$this->get_offer_products($sc_offers).')'; 
                }

                /* Order by*/
                $inventory_query="";
                $sql_query = 'select distinct id from product_desc WHERE 
                     (approve_reject = "A" || approve_reject = "D" || a.approve_reject = "B") and is_delete = 0 and status=1 and price > 0 '.$where_cond.' ';//limit '.$offset.',50000
                $result = $this->db->query($sql_query);
                $sql_query = $result->result_array();
                /*print_r($sql_query);
                exit;*/

                $pro_id="";
                foreach($sql_query as $pro){
                    $pro_id .= $pro['id'].", ";
                }
                $pro_id = substr($pro_id,0,-2);
                
                if($pro_id){
                    $sql_query = "select product_id id from product_inventory where stock_count > 0 and product_id in($pro_id) group by product_id";
                    
                    $result = $this->db->query($sql_query);
                    $sql_query = $result->result_array();
                    /*print_r($sql_query);
                    exit;*/
                    
                    $pro_id="";
                    foreach($sql_query as $pro){
                        $pro_id .= $pro['id'].", ";
                    }
                    $pro_id = substr($pro_id,0,-2);
                    if($pro_id)
                        $inventory_query = "id in ($pro_id) and";
                }
                
                
                //if($_GET['test'] == 1)

                if($product_sort_by > 0){
                    if($products!=''){
                        if($product_sort_by == 1) { $order_by = ' order by price desc'; }else{ $order_by = ' order by price asc'; }
                    }else{
                        if($product_sort_by == 1) { $order_by = ' order by price desc'; }else{ $order_by = ' order by price asc'; }
                    }
                }else if($products){
                    if($products!=''){
                        $order_by = ' order by  FIELD(id, '.$products.')';

                        $inventory_query = "id in (select product_id from product_inventory where stock_count > 0 and product_id in($products) group by product_id) and";
                    }else{
                        $order_by = ' order by id desc';
                    }
                }
				
                if($_GET['test']==1){
                /*'select distinct id,name,price,image,slug,brand_id from product_desc WHERE 
                    '.$inventory_query.' approve_reject = "A" and is_delete = 0 and status=1 and price > 0 '.$where_cond.$order_by.' limit '.$offset.',12';
                    exit;*/
                }

                $query = $this->db->query('select distinct id,name,price,image,slug,brand_id from product_desc WHERE '.$inventory_query.'
                     (approve_reject = "A" || approve_reject = "D" || a.approve_reject = "B") and is_delete = 0 and status=1 and price > 0 '.$where_cond.$order_by.' '.$limit.' '.$offset.' ');

            }else { 

                if($discount != NULL){


                        /* Main category selection from URL or From Filter selection*/
                    if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                    /* Sub-category selection */
                    if($this->get_cat_id($category_slug,$category_level) > 0 && $category_slug == 'subcategory'){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                    /* Brands Selection*/
                    if($brandIds!=''){
                        $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                    }

                    /* Color Selection */
                    if($colorId!=''){
                        $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                    }

                    if($products!=NULL){
                        $where_cond = $where_cond.' and a.id IN ('.$products.') ';
                    }

                    /* Price Selection */
                    if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                        if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                            else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                            else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                            else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                            else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                        if($min_price > 0 && $max_price>0){
                           $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                        }else if($min_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$min_price;
                        }else if($max_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$max_price;
                        }
                    }

                    /* Offers Selection*/
                    if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                        $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                    }

                    /* Order by*/
                    $inventory_query="";
                    
                    /*print_r($sql_query);
                    exit;*/

                    if($discount == 1){
                        $sql_query = 'select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,discount_availed as b WHERE 
                     CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id = b.product_id and (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.price > 0 and a.is_delete = 0 and a.status=1 '.$where_cond.' ';//limit '.$offset.',50000
                        $result = $this->db->query($sql_query);
                        $sql_query = $result->result_array();
                    }else if($discount == 2){
                         $sql_query = 'select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,discount_availed as b WHERE CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id != b.product_id and (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.price > 0 and a.is_delete = 0 and a.status=1 '.$where_cond.' ';//limit '.$offset.',50000
                        $result = $this->db->query($sql_query);
                        $sql_query = $result->result_array();
                    }

                    $pro_id="";
                    foreach($sql_query as $pro){
                        $pro_id .= $pro['id'].", ";
                    }
                    $pro_id = substr($pro_id,0,-2);
                    
                    if($pro_id){
                        $sql_query = "select product_id id from product_inventory where stock_count > 0 and product_id in($pro_id) group by product_id";
                        
                        $result = $this->db->query($sql_query);
                        $sql_query = $result->result_array();
                        /*print_r($sql_query);
                        exit;*/
                        
                        $pro_id="";
                        foreach($sql_query as $pro){
                            $pro_id .= $pro['id'].", ";
                        }

                        $pro_id = substr($pro_id,0,-2);
                        if($pro_id)
                        $inventory_query = "a.id in ($pro_id) and";
                    }
                    
                        
                          if($product_sort_by > 0){
                            if($products!=''){
                                if($product_sort_by == 1) { $order_by = ' order by a.price desc'; }else{ $order_by = ' order by a.price asc'; }
                                }else{
                                    if($product_sort_by == 1) { $order_by = ' order by a.price desc'; }else{ $order_by = ' order by a.price asc'; }
                             }
                            }else if($products){
                                if($products!=''){
                                    $order_by = ' order by  FIELD(a.id, '.$products.')';
                                    $inventory_query = "a.id in (select product_id from product_inventory where stock_count > 0 and product_id in($products) group by product_id) and";
                                }else{
                                    $order_by = ' order by a.id desc';
                                }
                            }

                    if($discount == 1){
                    $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,discount_availed as b WHERE '.$inventory_query.'  
                     CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id = b.product_id and (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.price > 0 and a.is_delete = 0 and a.status=1 '.$where_cond.$order_by.' '.$limit.' '.$offset.' ');
                    }else if($discount == 2){


                        $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,discount_availed as b WHERE '.$inventory_query .' CURRENT_TIMESTAMP() >= b.start_date and CURRENT_TIMESTAMP() <= b.end_date and a.id != b.product_id and (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.price > 0 and a.is_delete = 0 and a.status=1 '.$where_cond.$order_by.' '.$limit.' '.$offset.' ');
                    }
                }
            }
            $product_data = $query->result_array();

        }else{
            if($this->get_cat_id($category_slug,$category_level) >0){
                if($category_slug == 'variation'){
                    $where_cond = $where_cond.' and b.product_var_type_id='.$this->get_cat_id($category_slug,$category_level); 
                }else if($category_slug == 'variations'){
                    $where_cond = $where_cond.' and b.product_var_val_id='.$this->get_cat_id($category_slug,$category_level); 
                }
            }
            /* Else condition started */
                if($discount == NULL){
                /* Main category selection from URL or From Filter selection*/
                if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                /* Sub-category selection */ 
                if($this->get_cat_id($category_slug,$category_level) > 0 && $category_slug == 'subcategory'){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                /* Brands Selection*/
                if($brandIds!=''){
                    $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                }

                /* Color Selection */
                if($colorId!=''){
                    $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                }

                if($products!=NULL){
                    $where_cond = $where_cond.' and a.id IN ('.$products.') ';
                }

                /* Price Selection */
                if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                    if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                        else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                        else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                        else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                        else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                    if($min_price > 0 && $max_price>0){
                       $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                    }else if($min_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$min_price;
                    }else if($max_price > 0){
                        $where_cond = $where_cond.' and a.price >= '.$max_price;
                    }
                }

                /* Offers Selection*/
                if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                    $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                }



    
                   /* Order by*/
                   $inventory_query="";
                    
                    /*print_r($sql_query);
                    exit;*/

                    $sql_query = 'select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,product_variations as b WHERE  (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.price > 0 and a.is_delete = 0 and a.status=1 and a.id = b.product_id '.$where_cond.$order_by.' ';//limit '.$offset.',50000
                    $result = $this->db->query($sql_query);
                    $sql_query = $result->result_array();
                    
                    $pro_id="";
                    foreach($sql_query as $pro){
                        $pro_id .= $pro['id'].", ";
                    }
                    $pro_id = substr($pro_id,0,-2);
                    
                    if($pro_id){
                        $sql_query = "select product_id id from product_inventory where stock_count > 0 and product_id in($pro_id) group by product_id";
                        
                        $result = $this->db->query($sql_query);
                        $sql_query = $result->result_array();
                        /*print_r($sql_query);
                        exit;*/
                        
                        $pro_id="";
                        foreach($sql_query as $pro){
                            $pro_id .= $pro['id'].", ";
                        }
                        $pro_id = substr($pro_id,0,-2);
                        if($pro_id)
                            $inventory_query = "a.id in ($pro_id) and";
                    }

                if($product_sort_by > 0){
                    if($products!=''){
                        if($product_sort_by == 1) { $order_by = ' order by  price desc'; }else{ $order_by = ' order by price asc'; }
                    }else{
                        if($product_sort_by == 1) { $order_by = ' order by price desc'; }else{ $order_by = ' order by price asc'; }
                    }
                }else if($products){
                    if($products!=''){
                        $order_by = ' order by  FIELD(id, '.$products.')';
                        $inventory_query = "a.id in (select product_id from product_inventory where stock_count > 0 and product_id in($products) group by product_id) and";
                    }else{
                        $order_by = ' order by id desc';
                    }
                }

                $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,product_variations as b WHERE '.$inventory_query.'  (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.price > 0 and a.is_delete = 0 and a.status=1 and a.id = b.product_id '.$where_cond.$order_by.' '.$limit.' '.$offset.' ');

            }else { 

                if($discount > 0){


                        /* Main category selection from URL or From Filter selection*/
                    if($look_type == 'women'){ $where_cond = $where_cond.' and a.product_cat_id='.WOMEN_CATEGORY_ID; }else if($look_type == 'men'){ $where_cond = $where_cond.' and a.product_cat_id='.MEN_CATEGORY_ID; } 

                    /* Sub-category selection */
                    if($this->get_cat_id($category_slug,$category_level) > 0 && $category_slug == 'subcategory'){ $where_cond = $where_cond.' and a.product_sub_cat_id='.$this->get_cat_id($category_slug,$category_level); }

                    /* Brands Selection*/
                    if($brandIds!=''){
                        $where_cond = $where_cond.' and a.brand_id IN('.$brandIds.')';
                    }

                    /* Color Selection */
                    if($colorId!=''){
                        $where_cond = $where_cond.' and a.sc_product_color IN('.$colorId.')';
                    }

                    if($products!=NULL){
                        $where_cond = $where_cond.' and a.id IN ('.$products.') ';
                    }

                    /* Price Selection */
                    if($price != NULL || ($min_price  != NULL && $max_price != NULL)){
                        if($price == 1) { $where_cond = $where_cond.' and a.price < 500 ';  }
                            else if($price == 2) { $where_cond = $where_cond.' and (a.price >= 500 ) and (a.price <= 999 )';  }
                            else if($price == 3) { $where_cond = $where_cond.' and (a.price >= 1000 ) and (a.price <= 1999 ) ';  }
                            else if($price == 4) { $where_cond = $where_cond.' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
                            else if($price == 5) { $where_cond = $where_cond.' and a.price > 2999 ';  }

                        if($min_price > 0 && $max_price>0){
                           $where_cond = $where_cond.' and (a.price >= '.$min_price.' ) and (a.price <= '.$max_price.' )';
                        }else if($min_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$min_price;
                        }else if($max_price > 0){
                            $where_cond = $where_cond.' and a.price >= '.$max_price;
                        }
                    }

                    /* Offers Selection*/
                    if($sc_offers>0 && $this->get_offer_products($sc_offers)!=''){ 
                        $where_cond = $where_cond.' and a.id IN ('.$this->get_offer_products($sc_offers).')'; 
                    }

                      /* Order by*/

                    if($discount == 1){
                       $sql_query = 'select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,product_variations as b,`discount_availed` as c WHERE CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id = c.product_id and (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.price > 0 and a.is_delete = 0 and a.status=1 and a.id = b.product_id '.$where_cond.' ';
                    }else if($discount == 2){
                       $sql_query = 'select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,product_variations as b,`discount_availed` as c WHERE CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id != c.product_id and a.price > 0 and (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.status=1 and a.is_delete = 0 and a.id = b.product_id '.$where_cond.' ';
                    }

                    $result = $this->db->query($sql_query);
                    $sql_query = $result->result_array();
                    
                    $pro_id="";
                    foreach($sql_query as $pro){
                        $pro_id .= $pro['id'].", ";
                    }
                    $pro_id = substr($pro_id,0,-2);
                    
                    if($pro_id){
                        $sql_query = "select product_id id from product_inventory where stock_count > 0 and product_id in($pro_id) group by product_id";
                        
                        $result = $this->db->query($sql_query);
                        $sql_query = $result->result_array();
                        /*print_r($sql_query);
                        exit;*/
                        
                        $pro_id="";
                        foreach($sql_query as $pro){
                            $pro_id .= $pro['id'].", ";
                        }
                        if($pro_id)
                            $pro_id = substr($pro_id,0,-2);
                        $inventory_query = "a.id in ($pro_id) and";
                    }


                   if($product_sort_by > 0){
                            if($products!=''){
                                if($product_sort_by == 1) { $order_by = ' order by a.price desc'; }else{ $order_by = ' order by a.price asc'; }
                                }else{
                                    if($product_sort_by == 1) { $order_by = ' order by a.price desc'; }else{ $order_by = ' order by a.price asc'; }
                             }
                            }else if($products){
                                if($products!=''){
                                    $order_by = ' order by  FIELD(a.id, '.$products.')';
                                    $inventory_query = "a.id in (select product_id from product_inventory where stock_count > 0 and product_id in($products) group by product_id) and";
                                }else{
                                    $order_by = ' order by a.id desc';
                                }
                    }

                    if($discount == 1){
                     $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,product_variations as b,`discount_availed` as c WHERE '.$inventory_query.' CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id = c.product_id and (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.price > 0 and a.is_delete = 0 and a.status=1 and a.id = b.product_id '.$where_cond.$order_by.' '.$limit.' '.$offset.' ');
                    }else if($discount == 2){
                        $query = $this->db->query('select distinct a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,product_variations as b,`discount_availed` as c WHERE '.$inventory_query.' CURRENT_TIMESTAMP() >= c.start_date and CURRENT_TIMESTAMP() <= c.end_date and a.id != c.product_id and a.price > 0 and (a.approve_reject = "A" || a.approve_reject = "D" || a.approve_reject = "B") and a.status=1 and a.is_delete = 0 and a.id = b.product_id '.$where_cond.$order_by.' '.$limit.' '.$offset.' ');
                    }
                }
            }
			// echo $this->db->last_query();exit;
            $product_data = $query->result_array();

        }

        // echo $this->db->last_query();  exit;
        // return $product_data;
		$product_array = array();$i=0;
			if(!empty($product_data)){
			foreach($product_data as $val){
				$product_array[$i] = $this->single_product_data($val['id']);
				$i++;
			}
			
			}
		return $product_array;


    }
	
	function get_size_guide_n($sgid=NULL,$pid=NULL,$bid=NULL){
        $product_id = $pid;
        $brand_id = $bid;
        $size_guide_id = $sgid;
        $size_guide_url = '';
            if($product_id > 0 && $brand_id >0){
                if($size_guide_id == 2){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/top.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/top.jpg';
                    }
                }

                if($size_guide_id == 3){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/bottom.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/bottom.jpg';
                    }
                }

                if($size_guide_id == 4){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/footwear.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/footwear.jpg';
                    }
                }

                if($size_guide_id == 5){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/top_bottom.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/top_bottom.jpg';
                    }
                }

                if($size_guide_id == 6){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/top_men.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/top_men.jpg';
                    }
                }
                if($size_guide_id == 7){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/bottom_men.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/bottom_men.jpg';
                    }
                }

                 if($size_guide_id == 8){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/footwear_men.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/footwear_men.jpg';
                    }
                }

                if($size_guide_id == 9){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/top_bottom_men.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/top_bottom_men.jpg';
                    }
                }
            }

            if(trim($size_guide_url) == ''){
                /*Women Default Size Guide*/
                if($size_guide_id == 2){
                    if(file_exists('assets/images/size_guide/stylecracker/top.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/stylecracker/top.jpg';
                    }
                }

                else if($size_guide_id == 3){
                    if(file_exists('assets/images/size_guide/stylecracker/bottom.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/stylecracker/bottom.jpg';
                    }
                }

                else if($size_guide_id == 4){  
                    if(file_exists('assets/images/size_guide/stylecracker/footwear.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/stylecracker/footwear.jpg';
                    }

                }              
                else if($size_guide_id == 5){
                    if(file_exists('assets/images/size_guide/stylecracker/top_bottom.jpg')){
                    $size_guide_url = base_url().'assets/images/size_guide/stylecracker/top_bottom.jpg';
                    }
                 /*Mens Default Size Guide*/
                }else if($size_guide_id == 6){  
                     $size_guide_url = base_url().'assets/images/size_guide/stylecracker/top_men.jpg';
                }

                else if($size_guide_id == 7){
                    $size_guide_url = base_url().'assets/images/size_guide/stylecracker/bottom_men.jpg';
                }

                else if($size_guide_id == 8){
                    $size_guide_url = base_url().'assets/images/size_guide/stylecracker/footwear_men.jpg';
                }

                else if($size_guide_id == 9){
                    $size_guide_url = base_url().'assets/images/size_guide/stylecracker/top_bottom_men.jpg';

                }else{
                    //$size_guide_url = base_url().'assets/images/size_guide/stylecracker/sc_size_guide.jpg';

                }
            }
        
        return $size_guide_url;

    }
	
	function calculate_discount_percent($compare_price,$discount_price,$product_price=null){
		// echo "comming".$compare_price.'----'.$discount_price;exit;
		$discount_percent = 0;
		if($compare_price > 0 && $discount_price > 0){
			$discount_percent = round((1-($discount_price/$compare_price))*100);
		}else if($compare_price > 0 && $product_price > 0){
			$discount_percent = round((1-($product_price/$compare_price))*100);
		}
		
		return $discount_percent;
	}
	
	function getSaleProducts($offset='',$category='',$brands='',$sort='',$limit){

		$where_cond = '';

		$sale_products = $this->db->query("select product_id from (select a.product_id as product_id from discount_availed as a,product_desc as b,brand_info as c where a.product_id = b.id and b.brand_id = c.user_id and a.start_date <= CURRENT_TIMESTAMP() and a.end_date >= CURRENT_TIMESTAMP() and b.approve_reject IN ('A','D','B') and b.status = 1 and b.is_delete = 0 and a.discount_percent > 0 UNION ALL select id as product_id from product_desc where 1 and is_delete = 0 and status =1 and approve_reject IN('A','D','B') and compare_price >= price and compare_price != 0 and discount_end_date >= CURRENT_TIMESTAMP()) as z order by z.product_id desc ");
		$sale_products = $sale_products->result_array();
		$product_ids = ''; $res = array();
		
		$offset_new = $offset;
        $order_by = "RAND(".$offset_new.")";
		
		if($limit >= 0 || $offset >= 0) { $limit_cond = "limit ".$offset.",".$limit; }else { $limit_cond = ''; }
		
		if(!empty($sale_products)){ $k = 0;
			foreach($sale_products as $val){
				if($k!=0){ $product_ids = $product_ids.','; }
				$product_ids = $product_ids.$val['product_id'];
				$k++;
			}
		}

		if($category!=''){ $where_cond =$where_cond.' and c.category_id IN ('.$category.')'; }
		if($brands!=''){ $where_cond =$where_cond.' and a.brand_id IN ('.$brands.')'; }
		if($sort==2){ $order_by =' a.price asc'; }
		if($sort==1){ $order_by =' a.price desc'; }


		if($product_ids!=''){
			$get_products = $this->db->query("select distinct a.id,a.name,a.image,a.price,a.brand_id,a.slug,CASE 
			WHEN  DATE_FORMAT(a.discount_start_date,'%Y-%m-%d  %h:%i') <= DATE_FORMAT(now(),'%Y-%m-%d %h:%i') and DATE_FORMAT(a.discount_end_date,'%Y-%m-%d  %h:%i') >= DATE_FORMAT(now(),'%Y-%m-%d %h:%i') then a.compare_price
			ELSE 0
			END AS compare_price from product_desc as a,product_inventory as b,category_object_relationship as c where 1 and a.id = b.product_id and a.id = c.object_id and c.object_type= 'product' and a.status = 1 and a.is_delete=0 and a.approve_reject IN ('A','D','B') and b.stock_count > 0 and a.id IN (".$product_ids.") ".$where_cond." and a.approve_reject IN ('A','D','B') and a.status = 1 order by ".$order_by." ".$limit_cond);
			$res = $get_products->result_array();
			// echo $this->db->last_query();
		}
		$data = array();
		$data['products'] = $res;
		$data['products_ids'] = $product_ids;
		return $data;
	}
	
	function get_look_id_by_slug($slug){
	
		$query = $this->db->query("select a.id from look as a where a.slug = '".$slug."' ");
		$result = $query->result_array();
		if(!empty($result)) return $result[0]['id']; else return 0;
	}
	
	function is_wishlisted($fav_for,$fav_id,$user_id){
		if($user_id >0){
			$query = $this->db->query("select id,user_id from favorites where fav_id=$fav_id AND user_id=$user_id AND fav_for = '$fav_for' ");
			$res = $query->result_array();
			if(count($res)>0){
				return '1';
			}else return '0';
		}else return '0';
	}

	function get_product_store($product_id)
	{
		$this->db->select('bs.brand_store_name');
		$this->db->from('product_store as ps');
		$this->db->join('brand_store as bs', 'bs.id = ps.store_id','INNER');		
		$this->db->where('ps.product_id',$product_id);
		$query = $this->db->get();		
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}

	function get_products_list_data($product_ids,$data){
		//if(@$data['limit']>0){ $data['limit_cond'] = " LIMIT ".$data['offset'].",".$data['limit']; }else { $data['limit_cond'] = "";}
		if(@$data['limit']>0){ $data['offset'] = ($data['offset']>0) ? ($data['offset']*$data['limit'])+1 : $data['offset'];  $data['limit_cond'] = " LIMIT ".$data['offset'].",".$data['limit']; }else { $data['limit_cond'] = "";}
		if(@$data['order_by'] == ''){ $data['order_by']=''; }
		if(@$data['search_by'] == '1'){ $product_id_in = implode(',',@$data['table_search']); $product_id_in = trim($product_id_in,","); }else{ $product_id_in = implode(',',$product_ids); $product_id_in = trim($product_id_in,","); }
		if($product_id_in != ''){
		// 	$query = $this->db->query("Select DISTINCT p.id as product_id,(select CONCAT(cat.category_name,CONCAT(CONCAT('--',cat.id),CONCAT('--',category_slug))) as cat from category_object_relationship as co,category as cat where co.category_id = cat.id and co.object_type = 'product' and co.object_id = p.id order by co.category_id desc limit 1) category,p.brand_id as brand_id,p.name,p.description,p.slug,p.price,p.url as buy_now_url,CONCAT('".$data['product_link']."',p.image) as product_img,b.company_name as brand_name,CASE 
		// 	WHEN  p.discount_start_date <= CURRENT_TIMESTAMP() and p.discount_end_date >= CURRENT_TIMESTAMP() then p.compare_price
		// 	ELSE 0
		// END AS compare_price,c.user_name as brand_slug,b.short_description as brand_desc,p.slug as product_slug,p.image from product_desc as p,brand_info b,user_login as c,product_inventory as d where (p.approve_reject='A' OR p.approve_reject = 'D' OR p.approve_reject = 'BD' OR p.approve_reject = 'B') AND p.id IN(".$product_id_in.") AND p.brand_id = b.user_id AND b.user_id = c.id AND p.id = d.product_id AND d.stock_count > 0 AND p.status = '1' ".@$data['order_by']." ".$data['limit_cond']);

		$query = $this->db->query("Select DISTINCT p.id as product_id,(select CONCAT(cat.category_name,CONCAT(CONCAT('--',cat.id),CONCAT('--',category_slug))) as cat from category_object_relationship as co,category as cat where co.category_id = cat.id and co.object_type = 'product' and co.object_id = p.id order by co.category_id desc limit 1) category,p.brand_id as brand_id,p.name,p.description,p.slug,p.price,p.url as buy_now_url,CONCAT('".$data['product_link']."',p.image) as product_img,p.image,b.company_name as brand_name,CASE 
			WHEN  p.discount_start_date <= CURRENT_TIMESTAMP() and p.discount_end_date >= CURRENT_TIMESTAMP() then p.compare_price
			ELSE 0
			END AS compare_price,p.compare_price as mrp,c.user_name as brand_slug,b.short_description as brand_desc,p.slug as product_slug,p.sku_number,p.stock_count,p.parent_id from product_desc as p,brand_info b,user_login as c where p.id IN(".$product_id_in.") AND p.brand_id = b.user_id AND p.parent_id='0' AND b.user_id = c.id AND p.stock_count > 0 ".@$data['order_by']." ".$data['limit_cond']);
			 
			$res = $query->result_array();
			
			$product_array = array();$category = array();
			$i=0;
			if(!empty($res)){
				foreach($res as $val){
					
					$productobject = new stdClass(); 
					$productobject->product_id = $val['product_id'];
					if($val['brand_id'] != 40030){
						$productobject->product_img = $val['product_img'];
					}else{
						//$productobject->product_img = 'https://www.stylecracker.com/sc_admin/assets/product_images/normal/'.$val['product_img'];
						$productobject->product_img = base_url().'sc_admin/assets/product_images/normal/'.$val['product_img'];
					}

					//$productobject->product_img_name = @$val['product_img'];
					$productobject->product_img_name = @$val['image'];
					$productobject->product_name = $val['name'];
					if(!empty($val['category'])){ $category = explode('--',$val['category']);}
					$productobject->product_category = @$category[0];
					$productobject->product_category_id = @$category[1];
					$productobject->product_category_slug = @$category[2];
					$productobject->brand_name = $val['brand_name'];
					$productobject->brand_id = $val['brand_id'];
					
					$brand_discount = $this->brand_discount($val['brand_id']);
					$product_discount = $this->product_discount($val['product_id']);
					$discount_price = 0;
					$discount_percent = 0;
					if(!empty($brand_discount)){
						$productobject->discount_percent = $brand_discount['discount_percent'];
						$discount_price = $val['price'] - ($val['price'] * ($brand_discount['discount_percent']/100));
						$productobject->discount_price = (string)($val['price'] - ($val['price'] * ($brand_discount['discount_percent']/100)));
						// number_format($number, 2, ',', '')
					}else if(!empty($product_discount)){
						$productobject->discount_percent = $product_discount['discount_percent'];
						$discount_price = $val['price'] - ($val['price'] * ($product_discount['discount_percent']/100));
						$productobject->discount_price = (string)($val['price'] - ($val['price'] * ($product_discount['discount_percent']/100)));
					}else {
						$productobject->product_discount = '0';
						$discount_price = 0;
						$productobject->discount_price = '';
					}
					
					if($discount_price < $val['price']){ $productobject->on_sale = 'ON SALE'; }
					else $productobject->on_sale = '';
					
					/*  added for new discount compare price code start*/
					
					// $val['compare_price'] = 100;$val['price']=80;$discount_percent = 10;$discount_price=72;
					if($discount_percent > 0 || $val['compare_price']>$val['price']){
						$productobject->price = (string)$val['compare_price'];
						$productobject->discount_price = (string)round($discount_price);
						$new_discount_percent = $this->calculate_discount_percent($val['compare_price'],$discount_price,$val['price']);
						$productobject->discount_percent = (string)$new_discount_percent;
					}else {
						$productobject->price = (string)$val['price'];
						$productobject->discount_price = (string)round($discount_price);
						$val['compare_price'] = $val['price'];
						$new_discount_percent = $this->calculate_discount_percent($val['compare_price'],$discount_price,$val['price']);
						$productobject->discount_percent = (string)$new_discount_percent;
					}
						
					/*  added for new discount compare price code end*/
					
					$productobject->is_wishlisted = $this->is_wishlisted('product',$val['product_id'],$data['user_id']);
					$productobject->is_available = $this->is_available($val['product_id']);
					$productobject->product_size = $this->product_size($val['product_id']);
					$productobject->product_size = $this->product_child_sku($val['product_id'],$val['parent_id']);
					//$productobject->share_url = base_url().'product/'.$val['slug'].'-'.$val['product_id'];
					$productobject->share_url = base_url().'product/details/'.$val['slug'].'-'.$val['product_id'];
					$productobject->brand_slug = $val['brand_slug'];
					$productobject->brand_desc = $val['brand_desc'];
					$productobject->product_slug = $val['product_slug'];
					$productobject->sku_number = $val['sku_number'];
					$productobject->stock_count = $val['stock_count'];
					$productobject->mrp = $val['mrp'];
					$productobject->product_store_name = $this->get_product_store($val['product_id'])[0]['brand_store_name'];
					$product_array[$i] = $productobject;
					$i++;
				}//end of for each
					
			}//end of if 
		}//end of product_in if =
		
		if(@$data['order_by'] == ''){ 
			//shuffle($product_array); 
		}		
		if(!empty(@$product_array)) return $product_array; else return array();
	}

	function product_child_sku($product_id,$parent_sku)
	{
		if($product_id!='' && $parent_sku!='')
		{
			$this->db->select('pd.id,pd.sku_number');
			$this->db->from('product_desc as pd');		
			$this->db->where('pd.parent_id',$parent_sku);	
			$this->db->where('pd.is_delete','0');			
			$query = $this->db->get();		
			if($query->num_rows()>0)
			{
				return $query->result_array();
			}
		}		
		return false;
	}
	
}