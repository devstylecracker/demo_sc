<?php
class Cart_model extends MY_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function update_cart($uniquecookie,$productid,$userid,$size,$agent,$platform_info,$look_id){

        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }

        $time = new DateTime(date('Y-m-d H:i:s'));
        $time->add(new DateInterval('PT330M'));
        $dateM = $time->format('Y-m-d H:i:s');


        if($productid!='' && $size!=''){
        $query = $this->db->query("select * from cart_info where (`SCUniqueID` = '".$uniquecookie."' and `product_id` = '".$productid."' and `product_size` = '".$size."' and `order_id` is null )");
        $res = $query->result_array();
        if(empty($res)){
            $data = array(
               'product_id' => $productid ,
               'user_id' => $user_id,
               'created_datetime' => $dateM,
               'product_size' => $size,
               'product_qty' => '1',
               'SCUniqueID' => $uniquecookie,
               'agent' =>$agent,
               'platform_info'=>$platform_info,
               'look_id'=>$look_id
            );
            $this->db->insert('cart_info', $data);
        }else{
            if($res[0]['product_qty']<5){
                $data = array(
                   'product_id' => $productid ,
                   'user_id' => $user_id,
                   'created_datetime' => $dateM,
                   'product_size' => $size,
                   'product_qty' => $res[0]['product_qty']+1,
                   'SCUniqueID' => $uniquecookie,
                   'agent' =>$agent,
                   'platform_info'=>$platform_info
                );

                $this->db->where(array('id'=>$res[0]['id'],'product_id' => $productid,'product_size' => $size,'SCUniqueID' => $uniquecookie));
                $this->db->update('cart_info', $data);
                }
            }
        }

    }

    function get_cart($uniquecookie){
       $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = ''; }
       /*$query = $this->db->query('select a.`id`,a.`product_id`,d.`return_policy`,a.`product_qty`,b.`name`,(a.`product_qty`*b.price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size` from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`SCUniqueID` = "'.$uniquecookie.'") order by b.`brand_id`');*/
       if($user_id == '' || $user_id ==0){
        $query = $this->db->query('select a.`id`,a.`product_id`,d.`return_policy`,a.`product_qty`,b.`name`,(a.`product_qty`*b.price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,b.`id` as pid,b.`price` as product_price from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and b.status = 1 and (a.`SCUniqueID` = "'.$uniquecookie.'") order by b.`brand_id`');
      }else{
        $query = $this->db->query('select a.`id`,a.`product_id`,d.`return_policy`,a.`product_qty`,b.`name`,(a.`product_qty`*b.price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,b.`id` as pid,b.`price` as product_price from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id`  and b.status = 1 and (a.`SCUniqueID` = "'.$uniquecookie.'" || a.`user_id`="'.$user_id.'") order by b.`brand_id`');
      }
      //echo $this->db->last_query();
        //$res = $query->result_array();
        //return $res;
        $res = $query->result_array();
        $product_data = array();
            if(!empty($res)){ $i = 0;
                foreach ($res as $key => $value) {
                    $product_data[$i]['id'] = $value['id'];
                    $product_data[$i]['product_id'] = $value['product_id'];
                    $product_data[$i]['product_qty'] = $value['product_qty'];
                    $product_data[$i]['return_policy'] = $value['return_policy'];
                    $product_data[$i]['name'] =$value['name'];
                    $product_data[$i]['price'] = $value['price'];
                    $product_data[$i]['image'] =$value['image'];
                    $product_data[$i]['size_text'] =$value['size_text'];
                    $product_data[$i]['company_name'] =$value['company_name'];
                    $product_data[$i]['user_id'] =$value['user_id'];
                    $product_data[$i]['min_del_days'] =$value['min_del_days'];
                    $product_data[$i]['max_del_days'] =$value['max_del_days'];
                    $product_data[$i]['store_tax'] =$value['store_tax'];
                    $product_data[$i]['is_shipping'] =$value['is_shipping'];
                    $product_data[$i]['shipping_charges'] =$value['shipping_charges'];
                    $product_data[$i]['shipping_min_values'] =$value['shipping_min_values'];
                    $product_data[$i]['shipping_max_values'] =$value['shipping_max_values'];
                    $product_data[$i]['is_cod'] =$value['is_cod'];
                    $product_data[$i]['cod_charges'] =$value['cod_charges'];
                    $product_data[$i]['cod_min_value'] =$value['cod_min_value'];
                    $product_data[$i]['code_max_value'] =$value['code_max_value'];
                    $product_data[$i]['product_size'] =$value['product_size'];
                    $product_data[$i]['discount_percent'] = $this->get_discount_percentage($value['user_id'],$value['pid']);
                    $product_data[$i]['discount_price'] =round($value['product_qty']*($value['product_price']-($value['product_price']*$product_data[$i]['discount_percent'])/100));

                    $product_data[$i]['discount_type'] =  $this->get_discount_id($value['user_id'],$value['pid'])['discount_type_id'];

                    if($product_data[$i]['discount_type'] == 1){
                     $product_data[$i]['discount_id'] =  $this->get_discount_id($value['user_id'],0)['id'];
                    }else if($product_data[$i]['discount_type'] == 2){
                        $product_data[$i]['discount_id'] =  $this->get_discount_id(0,$value['pid'])['id'];
                    }else{
                         $product_data[$i]['discount_id'] =0;
                         $product_data[$i]['discount_type'] =0;
                    }
                    $product_data[$i]['discount_max_amount'] = $this->get_discount_maxvalue($value['user_id'],$value['pid']);
                    /*$product_data[$i]['coupon_amount'] = $this->getCouponData()*/

                    $i++;
                }
               /* $product_data['coupon_amount'] = $this->getCouponData($coupon_code)['coupon_amount'];*/
               /*echo "<pre>";print_r($product_data);*/
            }
        return $product_data;
    }

    function deleteCartProduct($uniquecookie,$cart){
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = ''; }
        $query = $this->db->query("delete from `cart_info` where id='".$cart."' and (`SCUniqueID`='".$uniquecookie."' OR `user_id` = '".$user_id."')");

    }

    function getBrandUsername($brand_id){
        $query = $this->db->get_where('user_login', array('id' => $brand_id));
        $res = $query->result_array();
        if(!empty($res)){
            return $res[0]['user_name'];
        }else{
            return '';
        }
    }

    function get_all_states(){
        $query = $this->db->get('states');
        $res = $query->result_array();
        return $res;
    }

    function get_city_name($pincode,$uniquecookie){
        $query = $this->db->query("select TRIM(a.`city`) as city,a.`state_id` from `city` as a,`pincode` as b,`states` as c where a.`state_id` = c.`id` and b.`city_id` = a.`id` and b.`pincode`='".$pincode."'");
        $res = $query->result_array();
       // $not_cod = $this->get_cod_cart_info($uniquecookie,$pincode);
       // $not_ship = $this->get_not_ship_cart_info($uniquecookie,$pincode);
        return '{"city":"'.trim(@$res[0]['city']).'","state":"'.@$res[0]['state_id'].'"}';
    }

    function productQtyUpdate($uniquecookie,$productQty,$productInfo){
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }
        if($uniquecookie!='' && $productQty!='' && $productInfo!=''){

             $data = array('product_qty' => $productQty);

             //$this->db->where(array('id' => $productInfo,'SCUniqueID' => $uniquecookie,'user_id'=>$user_id));
             $this->db->where(array('id' => $productInfo,'SCUniqueID' => $uniquecookie));
             $this->db->update('cart_info', $data);

        }
    }

    function getBrandExtraInfo($brand_id){
        $query = $this->db->get_where('brand_info', array('user_id' => $brand_id));
        $res = $query->result_array();
        if(!empty($res)){
            return $res;
        }else{
            return '';
        }
    }

    function get_cod_cart_info($uniquecookie,$pincode){
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }
        $query = $this->db->query('select distinct a.id,b.`cod_exc_location` from `cart_info` as a,`brand_info` as b,`product_desc` as c where a.`product_id` = c.`id` and b.`user_id` = c.`brand_id` and a.`user_id` ="'.$user_id.'" and a.`SCUniqueID`="'.$uniquecookie.'" and a.`order_id` is null and b.`cod_exc_location` like "%'.$pincode.'%"');
        $res = $query->result_array();
        return $res;
    }

    function get_not_ship_cart_info($uniquecookie,$pincode){
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }
        $query = $this->db->query('select distinct a.id,b.`shipping_exc_location` from `cart_info` as a,`brand_info` as b,`product_desc` as c where a.`product_id` = c.`id` and b.`user_id` = c.`brand_id` and a.`user_id` ="'.$user_id.'" and a.`SCUniqueID`="'.$uniquecookie.'" and a.`order_id` is null and b.`shipping_exc_location` like "%'.$pincode.'%"');
        $res = $query->result_array();
        return $res;
    }

    function sc_uc_order($data){
        $this->load->model('Schome_model');
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }
        $SCUniqueID = $data['SCUniqueID'];
        $productPrice = 0; $final_cod_charges =0;
        $order_list = array(); $product_total = 0;
        $new_store_shipping_charges = 0;
        $old_store_shipping_charges = 0;
        $final_shipping_charges = 0;
		    if($data['coupon_code']!='')
        {
          $coupon_code = $data['coupon_code'];
        }else
        {
          $coupon_code = '';
        }
        
        $coupon_discount = 0;
        $coupon_products = $this->getCouponData($coupon_code)['coupon_products']; 
        $coupon_min_spend = $this->getCouponData($coupon_code)['coupon_min_spend'];
        $coupon_max_spend = $this->getCouponData($coupon_code)['coupon_max_spend'];
        $coupon_discount_type = $this->getCouponData($coupon_code)['coupon_discount_type'];
		$coupon_stylecracker =  $this->getCouponData($coupon_code)['stylecrackerDiscount'];
        $individual_use_only =  $this->getCouponData($coupon_code)['individual_use_only'];
        $appliedcoupon_code = '';

          if($coupon_products!='' || $coupon_products!=0)
          {
            $coupon_products_arr =explode(',',$coupon_products);
          }

		if($data['paymod'] == 'cod'){
			$payent_method = '1';
		}else $payent_method = '2';

        $time = new DateTime(date('Y-m-d H:i:s'));
        $time->add(new DateInterval('PT330M'));
        $dateM = $time->format('Y-m-d H:i:s');

        $cart_data = $this->get_cart($SCUniqueID);
        /*echo '<pre>';print_r($cart_data);*/
        /*echo '<pre>';print_r($data);*/
        
        if(!empty($cart_data)){

            if($user_id == 0){
                $user_id = $this->check_user_exist_or_not($data['uc_email_id']);
            }
            $new_brand = ''; $old_brand = ''; $new_store_tax = 0; $old_store_tax = 0; $new_store_cod_charges = 0; $old_store_cod_charges = 0;
            $brand_total = 0; $order_info = ''; $total_discount_percent=0;
            $product_info = array(); $i = 0;$coupon_product_price = 0;

            if($user_id == 0 ){
                $scusername = $this->Schome_model->randomPassword();
                $scpassword = $this->Schome_model->randomPassword();
                $ip = $this->input->ip_address();
                $this->Schome_model->registerUser('',$scusername,$data['uc_email_id'],$scpassword,$ip,2,'');
                $user_id = $this->check_user_exist_or_not($data['uc_email_id']);
            }

            foreach($cart_data as $val){
                $new_brand = $val['user_id'];
                $new_store_tax = $val['store_tax'];

                $new_store_cod = $val['is_cod'];
                $new_store_cod_charges = $val['cod_charges'];
                $discount_id = $val['discount_id'];
                $productPrice =0;
                $coupon_brand = $this->Cart_model->getCouponData($coupon_code,$val['user_id'])['brand_id'];
                 
                if($coupon_brand==$new_brand && $coupon_brand!=0)
                {
                  if($coupon_products!=''&& $coupon_products!=0)
                  {
                      /* coupon_discount_type =3 (Product discount)*/
                    if($coupon_discount_type==3)
                    {
                        if(in_array($val['product_id'], $coupon_products_arr))
                        {
                            if($coupon_min_spend<=$val['price'] && $val['price']<=$coupon_max_spend)
                            {
                              $coupon_discount = $this->getCouponData($coupon_code)['coupon_amount'];  
                              $appliedcoupon_code = $coupon_code;    
                            }               
                        }
                    }
                     /* coupon_discount_type =4 (Product % discount)*/
                    if($coupon_discount_type==4)
                    {
                      if(in_array($val['product_id'], $coupon_products_arr))
                      {              
                        if($coupon_min_spend<=$value['price'] && $value['price']<=$coupon_max_spend)
                        {
                          $coupon_discount = $this->getCouponData($coupon_code)['coupon_amount'];  
                          $appliedcoupon_code = $coupon_code;   
                        } 
                      }
                    }        

                  }else
                  {   
                    /* coupon_discount_type =1 (cart discount)*/
                    if($coupon_discount_type==1)
                    {
                        $coupon_product_price = $coupon_product_price+$val['price'];
                        if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                        {                 
                            $coupon_discount = $this->getCouponData($coupon_code,$val['user_id'])['coupon_amount']; 
                             $appliedcoupon_code = $coupon_code;    
                        } 
                    }

                    /* coupon_discount_type =2 (cart % discount)*/
                    if($coupon_discount_type==2)
                    {
                      $coupon_product_price = $coupon_product_price+$val['price'];
                      if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                      {
                        $coupon_discount = $this->getCouponData($coupon_code,$val['user_id'])['coupon_amount']; 
                        $appliedcoupon_code = $coupon_code; 
                      }
                    }                 
                  } 

                  if($appliedcoupon_code != '')
                  {
                     $this->update_coupon_counter($appliedcoupon_code,$val['user_id']);
                  }         
                }

                /* if($coupon_brand==0 && strtolower($coupon_code)=='sccart20')
                { 
                     $coupon_product_price = $coupon_product_price+$val['price'];
                      if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                      {
                        $coupon_discount = $this->getCouponData($coupon_code,$val['user_id'])['coupon_amount']; 
                        $appliedcoupon_code = $coupon_code; 
                      }
                }*/
				
				if(($coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only!=1  )|| ( $coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only==1 && $this->session->userdata('user_id')>0 ))      
				{ 
                   /* coupon_discount_type =1 (cart discount)*/
                    if($coupon_discount_type==1)
                    {
                        $coupon_product_price = $coupon_product_price+$val['price'];
                        if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                        {                 
                            $coupon_discount = $this->getCouponData($coupon_code,$val['user_id'])['coupon_amount']; 
                             $appliedcoupon_code = $coupon_code;    
                        } 
                    }

                    /* coupon_discount_type =2 (cart % discount)*/
                    if($coupon_discount_type==2)
                    {
                      $coupon_product_price = $coupon_product_price+$val['price'];
                      if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                      {
                        $coupon_discount = $this->getCouponData($coupon_code,$val['user_id'])['coupon_amount']; 
                        $appliedcoupon_code = $coupon_code; 
                      }
                    }  
					if($appliedcoupon_code != '')
					{
						 $this->update_coupon_counter($appliedcoupon_code,'0');
					} 
				}
                
                if($new_brand != $old_brand){
				
                    if($brand_total!=0){
                        $tax = $brand_total*($old_store_tax/100);
                        $order_amount = $brand_total+($brand_total*($old_store_tax/100));

                      /* COD CALCULAION START */
                        if($this->getBrandExtraInfo($old_brand,'is_cod')[0]['is_cod'] == 1 && $data['paymod'] == 'cod'){
                            if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0 && $this->getBrandExtraInfo($old_brand,'code_max_value')[0]['code_max_value'] == 0){
                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                                $order_amount = $order_amount + $final_cod_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] >= 0 && $this->getBrandExtraInfo($old_brand,'code_max_value')[0]['code_max_value'] > 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']<=$product_total && $this->getBrandExtraInfo($old_brand,'code_max_value')[0]['code_max_value']>=$product_total) {


                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                                $order_amount = $order_amount + $final_cod_charges;
                            }else { $old_store_cod_charges = 0; }
                        }else{ $old_store_cod_charges = 0; }
                        /* COD CALCULAION END */

                        /* SHIPPING CHARGES START*/
                        if($this->getBrandExtraInfo($old_brand,'is_shipping')[0]['is_shipping'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] == 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] == 0){


                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] >= 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] > 0 && $this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values']<=$product_total && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values']>=$product_total) {

                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;

                            }else { $old_store_shipping_charges = 0; }
                            }else{ $old_store_shipping_charges = 0; }
                        /* SHIPPING CHARGES END*/
                        /* Brand Total Discount */
                        if($val['discount_type']==1 || $val['discount_type']==2 )
                        {
                            $total_discount_percent = $val['discount_percent'];
                        }else if($val['discount_type']==3)
                        {
                            $total_discount_percent = $val['discount_percent'];
                        }
						if($data['uc_state'] == 36){ $order_amount = $order_amount + SCB_CHARGES; $final_shipping_charges = SCB_CHARGES;}
                        /* Brand Total Discount End*/
                        $order_info = array(
                                'user_id' => $user_id,
                                'brand_id' => $old_brand,
                                'order_tax_amount' => $tax,
                                'order_sub_total' => $order_amount,
                                'order_total' => $order_amount,
                                'pay_mode' => $data['paymod']=='cod' ? '1' : '2',
                                'created_datetime' =>$dateM,
                                'first_name' => $data['uc_first_name'],
                                'last_name' => $data['uc_last_name'],
                                'email_id' => $data['uc_email_id'],
                                'mobile_no' => $data['uc_mobile'],
                                'shipping_address' => $data['uc_address'],
                                'pincode' => $data['uc_pincode'],
                                'city_name' => $data['uc_city'],
                                'state_name' => $data['uc_state'],
								'country_name' => '',
                                'mihpayid' => $data['mihpayid'],
                                'txnid' => $data['txnid'],
                                'order_status' => '1',
                                'cod_amount' => $final_cod_charges,
                                'shipping_amount' => $final_shipping_charges,
                                'total_discount_percent' => $total_discount_percent,
                                'coupon_code'=>$appliedcoupon_code,
                                'coupon_amount'=>$coupon_discount
                            );
                        $order_id = $this->sc_place_order($order_info); $product_total =0;
						
                        $order_list[] = $order_id;
                        $final_cod_charges = 0;
                        $final_shipping_charges = 0;
                        if(!empty($product_info)){
                            foreach($product_info as $value){
                                  $pro_data = array();
                                 $discount_type =  $this->get_discount_id($old_brand,$value['product_id'])['discount_type_id'];
                                    if( $discount_type == 1){
                                     $discount_id =  $this->get_discount_id($old_brand,0)['id'];
                                    }else if( $discount_type == 2){
                                        $discount_id =  $this->get_discount_id(0,$value['product_id'])['id'];
                                    }else{
                                         $discount_id =0;
                                         $discount_type =0;
                                    }
                                    $discount_percent = $this->get_discount_percentage($old_brand,$value['product_id']);
                                    $productPrice = round($value['price']-($value['price']*$discount_percent/100));

                                $pro_data = array('order_id'=>$order_id,'product_id'=>$value['product_id'],'user_id'=>$user_id,'created_datetime'=>$dateM,'product_size'=>$value['product_size'],'product_qty'=>$value['product_qty'],'product_price'=>$productPrice,'cart_id'=>$value['cart_id'],'discount_percent'=>$discount_percent,'discount_availed_id' =>$discount_id,'coupon_code'=>$appliedcoupon_code);
                                $sc_o_id = $this->sc_orders_product($pro_data);
                                $this->update_order_cart_info($value['cart_id'],$order_id,$user_id);

                            }
                        }

                        $product_info = [];
                    }
                    //$brand_total = 0;
                    $old_brand = $new_brand;
                    $old_store_tax = $new_store_tax;
                    //$brand_total = $brand_total+$val['price'];

                    $product_info[$i]['product_id'] = $val['product_id'];
                    $product_info[$i]['user_id'] = $user_id;
                    $product_info[$i]['created_datetime'] = $dateM;
                    $product_info[$i]['product_size'] = $val['product_size'];
                    $product_info[$i]['product_qty'] = $val['product_qty'];
                    $product_info[$i]['price'] = $val['price'];
                    $product_info[$i]['cart_id'] = $val['id'];
                    //$product_total = $product_total+$val['price'];
                    /*Added For Discount by Sudha*/
                    if($val['discount_price']!='' || $val['discount_price']!=0)
                    {
                        $product_total = $product_total+$val['discount_price'];
                    }else
                    {
                        $product_total = $product_total+$val['price'];
                    }
                    /*End Added For Discount by Sudha*/
                    $brand_total = $product_total;
                }else{
                    //$brand_total = $brand_total+$productPrice;
                    $old_brand = $new_brand;
                    $old_store_tax = $new_store_tax;

                    $product_info[$i]['product_id'] = $val['product_id'];
                    $product_info[$i]['user_id'] = $user_id;
                    $product_info[$i]['created_datetime'] = $dateM;
                    $product_info[$i]['product_size'] = $val['product_size'];
                    $product_info[$i]['product_qty'] = $val['product_qty'];
                    $product_info[$i]['price'] = $val['price'];
                    $product_info[$i]['cart_id'] = $val['id'];
                   // $product_total = $product_total+$val['price'];
                     /*Added For Discount by Sudha*/
                    if($val['discount_price']!='' || $val['discount_price']!=0)
                    {
                        $product_total = $product_total+$val['discount_price'];
                    }else
                    {
                        $product_total = $product_total+$val['price'];
                    }
                    /*End Added For Discount by Sudha*/
                    $brand_total = $product_total;

                }
                $i++;
            }
            if($brand_total!=0){
                $tax = $brand_total*($old_store_tax/100);
                $order_amount = $brand_total+($brand_total*($old_store_tax/100));

                /* COD CALCULAION START */

                        if($this->getBrandExtraInfo($old_brand,'is_cod')[0]['is_cod'] == 1 && $data['paymod'] == 'cod'){
                            if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0 && $this->getBrandExtraInfo($old_brand,'code_max_value')[0]['code_max_value'] == 0){
                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                                $order_amount = $order_amount + $final_cod_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] >= 0 && $this->getBrandExtraInfo($old_brand,'code_max_value')[0]['code_max_value'] > 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']<=$product_total && $this->getBrandExtraInfo($old_brand,'code_max_value')[0]['code_max_value']>=$product_total) {


                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                                $order_amount = $order_amount + $final_cod_charges;
                            }else { $old_store_cod_charges = 0;  }
                        }else{ $old_store_cod_charges = 0; }
                        /* COD CALCULAION END */

                                 /* SHIPPING CHARGES START*/
                        if($this->getBrandExtraInfo($old_brand,'is_shipping')[0]['is_shipping'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] == 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] == 0){


                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] >= 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] > 0 && $this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values']<=$product_total && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values']>=$product_total) {

                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;

                            }else { $old_store_shipping_charges = 0;  }
                            }else{ $old_store_shipping_charges = 0;  }
                        /* SHIPPING CHARGES END*/
				if($data['uc_state'] == 36){ $order_amount = $order_amount + SCB_CHARGES; $final_shipping_charges = SCB_CHARGES;}
                $order_info = array(
                                'user_id' => $user_id,
                                'brand_id' => $old_brand,
                                'order_tax_amount' => $tax,
                                'order_sub_total' => $order_amount,
                                'order_total' => $order_amount,
                                'pay_mode' => $data['paymod']=='cod' ? '1' : '2',
                                'created_datetime' =>$dateM,
                                'first_name' => $data['uc_first_name'],
                                'last_name' => $data['uc_last_name'],
                                'email_id' => $data['uc_email_id'],
                                'mobile_no' => $data['uc_mobile'],
                                'shipping_address' => $data['uc_address'],
                                'pincode' => $data['uc_pincode'],
                                'city_name' => $data['uc_city'],
                                'state_name' => $data['uc_state'],
								'country_name' => '',
                                'mihpayid' => $data['mihpayid'],
                                'txnid' => $data['txnid'],
                                'order_status' => '1',
                                'cod_amount' => $final_cod_charges,
                                'shipping_amount' => $final_shipping_charges,
                                'total_discount_percent' => $total_discount_percent,
                                'coupon_code'=>$appliedcoupon_code,
                                'coupon_amount'=>$coupon_discount
                            );
                        $order_id = $this->sc_place_order($order_info); $product_total =0;
                        $order_list[] = $order_id;
                        $final_cod_charges = 0;
                        $final_shipping_charges = 0;
                        if(!empty($product_info)){
                            foreach($product_info as $value){
                                $pro_data = array();
                                 $discount_type =  $this->get_discount_id($old_brand,$value['product_id'])['discount_type_id'];

                                    if( $discount_type == 1){
                                     $discount_id =  $this->get_discount_id($old_brand,0)['id'];
                                    }else if( $discount_type == 2){
                                        $discount_id =  $this->get_discount_id(0,$value['product_id'])['id'];
                                    }else{
                                         $discount_id =0;
                                         $discount_type =0;
                                    }
                                $discount_percent = $this->get_discount_percentage($old_brand,$value['product_id']);
                                $productPrice = round($value['price']-($value['price']*$discount_percent/100));
                                $pro_data = array('order_id'=>$order_id,'product_id'=>$value['product_id'],'user_id'=>$user_id,'created_datetime'=>$dateM,'product_size'=>$value['product_size'],'product_qty'=>$value['product_qty'],'product_price'=>$productPrice,'cart_id'=>$value['cart_id'],'discount_percent'=>$discount_percent,'discount_availed_id' =>$discount_id,'coupon_code'=>$appliedcoupon_code);
                                $sc_o_id = $this->sc_orders_product($pro_data);
                                $this->update_order_cart_info($value['cart_id'],$order_id,$user_id);
                            }
                        }
                        $product_info = [];



            }
            if(!empty($order_list)){
				$this->update_fail_order_status($SCUniqueID,$cart_data,$payent_method,$order_id,$user_id);
                $this->update_user_address($data,$user_id);
                return $this->update_order_unique_no($order_list,$user_id);
            }
        }else{
           // return 'Your cart is empty';
            return '{"order" : "fail", "msg" : "Your cart is empty"}';
        }

    }

    function sc_place_order($data){
        if(!empty($data)){
            $this->db->insert('order_info', $data);
			// echo $this->db->last_query();exit;
            return $this->db->insert_id();
        }
    }

    function sc_orders_product($data){
        if(!empty($data)){
            $this->db->insert('order_product_info', $data);
             $last_in_id = $this->db->insert_id();

            $product_id = $data['product_id'];
            $product_size = $data['product_size'];
            if($product_id>0 && $product_size>0 ){
            $query = $this->db->query('update product_inventory set stock_count = (stock_count-1) where product_id = "'.$product_id.'" and size_id="'.$product_size.'"');
            }
             return $last_in_id;
        }
    }

    function getMyOrders(){
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }

        //$query = $this->db->order_by('id','DESC')->get_where('order_info', array('user_id' => $user_id));

         $query = $this->db->query('select a.`shipping_amount`,a.`id`, a.`user_id`, a.`brand_id`,a.`order_unique_no`, sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total , sum(a.`order_total`) as order_total, a.`pay_mode`, a.`created_datetime`, a.`modified_datetime`, a.`order_status`, a.`first_name`, a.`last_name`, a.`email_id`, a.`mobile_no`, a.`shipping_address`, a.`pincode`, a.`city_name`, a.`state_name`,c.`state_name`,b.`company_name`,a.`total_discount_percent`,a.coupon_code,a.coupon_amount,a.order_display_no,a.billing_address from order_info as a,brand_info as b,states as c where a.`brand_id` = b.`user_id` and a.`state_name` = c.`id` and a.`user_id` = "'.$user_id.'" AND a.`order_status`=1 group by a.`order_unique_no` order by a.`id` desc');

        $result = $query->result_array();
        return $result;
    }

    function getMyProductsOrders(){
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }

        /*$query = $this->db->query('select a.`shipping_amount`,b.`order_status`,e.`company_name`,a.`order_unique_no`,a.`id`,c.`name`,c.`price`,b.product_qty,b.product_size,d.size_text as size_text,c.`image`,a.shipping_address,b.discount_percent,b.coupon_code from `order_info` as a,`order_product_info` as b,`product_desc` as c,`product_size` as d,brand_info as e where 1 and a.`id` = b.`order_id` and b.`product_id` = c.`id` and a.`user_id` = "'.$user_id.'" and d.`id` = b.product_size and e.user_id = c.`brand_id` order by a.`id` desc');*/

         $query = $this->db->query('select a.`shipping_amount`,b.`order_status`,e.`company_name`,a.`order_unique_no`,a.`id`,c.`name`,c.`price`,b.product_qty,b.product_size,d.size_text as size_text,(Select sum(i.product_price) from order_product_info as i  group by i.order_id having i.order_id = a.id ) as OrderIdProductSum,(Select sum(h.product_price) from order_info as g,order_product_info as h where g.id = h.order_id and g.order_unique_no = a.order_unique_no group by g.order_unique_no ) as OrderProductTotal,b.product_price, c.`image`,a.shipping_address,b.discount_percent,b.coupon_code,c.`id` as product_id from `order_info` as a,`order_product_info` as b,`product_desc` as c,`product_size` as d,brand_info as e where 1 and a.`id` = b.`order_id` and b.`product_id` = c.`id` and a.`user_id` = "'.$user_id.'" and d.`id` = b.product_size and e.user_id = c.`brand_id` order by a.`id` desc');

        $result = $query->result_array();

        return $result;
    }

    function update_order_unique_no($order_list,$user_id){
        $order_info = '';
        if(!empty($order_list)){
            $i = 0;
            foreach ($order_list as $key => $value) {
                if($i!=0){ $order_info = $order_info.'_'; }
                $order_info = $order_info.$value;
                $i++;
            }

            $data = array(
                'order_unique_no' => $user_id.'_'.$order_info
            );

            $this->db->where_in('id', $order_list);
            $this->db->update('order_info', $data);
            return '{"order" : "success", "msg" : "'.$user_id.'_'.$order_info.'","user_info" : "'.$user_id.'"}';
        }

    }

    function check_user_exist_or_not($email_id){
        if($email_id!=''){
            $check_email = $this->emailUnique($email_id);
                if(!empty($check_email)){
                   return $check_email[0]['id'];
                }else{
                    return 0;
                }

        }
    }

    function emailUnique($emaild){
        $query = $this->db->query('select id,email from user_login where email ="'.$emaild.'"');
        $username_unique = $query->result_array();
        return $username_unique;
    }

    function update_order_cart_info($cart_id,$order_id,$user_id){
        if($cart_id!='' && $order_id!=''){
            $data = array(
               'order_id' => $order_id,
               'user_id' => $user_id
            );

            $this->db->where('id', $cart_id);
            $this->db->update('cart_info', $data);
        }
    }

    function get_order_info($uniquecookie){

        $query = $this->db->query('select order_id,user_id from `cart_info` where SCUniqueID = "'.$uniquecookie.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['order_id'];
                $user_id = $val['user_id'];
             }

            $order_ids = implode(",",$order_info);

            $query = $this->db->query('select a.`order_id`, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`, a.`product_price`,b.`name`,b.`image`,b.`brand_id`,c.size_text from order_product_info as a,product_desc as b,product_size as c where a.`product_id` = b.`id` and a.`order_id` in ('.$order_ids.') and a.`user_id`='.$user_id.' and a.product_size = c.`id`' );

            $query_res = $query->result_array();
            return $query_res;

        }
    }

    function su_get_order_info($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where order_unique_no = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

            $order_ids = implode(",",$order_info);

            /*$query = $this->db->query('select a.`order_id`,b.price, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`, a.`product_price`,b.`name`,b.`image`,b.`brand_id`,c.size_text,d.company_name,d.min_del_days,d.max_del_days from order_product_info as a,product_desc as b,product_size as c,`brand_info` as d where a.`product_id` = b.`id` and a.`order_id` in ('.$order_ids.') and a.`user_id`='.$user_id.' and a.product_size = c.`id` and d.`user_id` = b.`brand_id`' );*/
            $query = $this->db->query('select a.`order_id`, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`, a.`product_price`,b.`name`,b.`image`,b.`brand_id`,c.size_text,d.company_name,d.min_del_days,d.max_del_days,(select a.`product_qty`*(b.price-((b.price*discount_percent)/100)) from discount_availed as da where da.brand_id = b.brand_id AND NOW() >= da.start_date AND NOW() <= da.end_date AND (da.discount_type_id=1 ) AND da.status = 1 AND da.is_delete=0 )as discount_price,a.coupon_code  from order_product_info as a,product_desc as b,product_size as c,`brand_info` as d where a.`product_id` = b.`id` and a.`order_id` in ('.$order_ids.') and a.`user_id`='.$user_id.' and a.product_size = c.`id` and d.`user_id` = b.`brand_id`' );

            $query_res = $query->result_array();
            return $query_res;

        }
    }

        function br_get_order_info($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where id = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

            $order_ids = implode(",",$order_info);

           /* $query = $this->db->query('select a.`order_id`, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`, a.`product_price`,b.`name`,b.`image`,b.`brand_id`,c.size_text,d.company_name from order_product_info as a,product_desc as b,product_size as c,`brand_info` as d where a.`product_id` = b.`id` and a.`order_id` in ('.$order_ids.') and a.`user_id`='.$user_id.' and a.product_size = c.`id` and d.`user_id` = b.`brand_id`' );*/
            $query = $this->db->query('select b.`product_sku`,a.`order_id`, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`, a.`product_price`,b.`name`,b.`image`,b.`brand_id`,c.size_text,d.company_name, (a.product_price-a.product_price*a.discount_percent/100) as discount_price, a.coupon_code from order_product_info as a,product_desc as b,product_size as c,`brand_info` as d where a.`product_id` = b.`id` and a.`order_id` in ('.$order_ids.') and a.`user_id`='.$user_id.' and a.product_size = c.`id` and d.`user_id` = b.`brand_id`');

            $query_res = $query->result_array();
            return $query_res;

        }
    }

    function su_get_order_price_info($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where order_unique_no = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info);

        $query = $this->db->query('select sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no, a.shipping_address,a.pincode,a.city_name,s.state_name,a.pay_mode,a.order_status,a.user_id,a.state_name as state_id,a.it_state,a.country_name from order_info as a,states as s where a.`state_name` = s.`id` and a.id in ('.$order_ids.')' );

            $query_res = $query->result_array();
            return $query_res;
        }
    }

       function br_get_order_price_info($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where id = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info);

        $query = $this->db->query('select a.id,a.created_datetime,sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no, a.shipping_address,a.pincode,a.city_name,a.state_name as state_id,a.it_state,a.country_name,s.state_name,a.brand_id,a.pay_mode from order_info as a,states as s where a.`state_name` = s.`id` and a.id in ('.$order_ids.')' );

            $query_res = $query->result_array();
            return $query_res;
        }
    }

    function get_order_price_info($uniquecookie){
        $query = $this->db->query('select order_id,user_id from `cart_info` where SCUniqueID = "'.$uniquecookie.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['order_id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info);

        $query = $this->db->query('select sum(`order_tax_amount`) as order_tax_amount, sum(`cod_amount`) as cod_amount, sum(`order_sub_total`) as order_sub_total, sum(`order_total`) as order_total,sum(shipping_amount) as shipping_amount from order_info where id in ('.$order_ids.')' );

            $query_res = $query->result_array();
            return $query_res;
        }
    }

    function chkShipping($product_id,$pincode){
        $query = $this->db->query('select a.`id` from product_desc as a,brand_info as b where a.`brand_id`= b.`user_id` and a.`id`="'.$product_id.'" and `shipping_exc_location` like "%'.$pincode.'%"' );

        $query_res = $query->result_array();
        return $query_res;
    }

    function chkCod($product_id,$pincode){
        $query = $this->db->query('select a.`id` from product_desc as a,brand_info as b where a.`brand_id`= b.`user_id` and a.`id`="'.$product_id.'" and `cod_exc_location` like "%'.$pincode.'%"' );

        $query_res = $query->result_array();
        return $query_res;
    }

    function scgenrate_otp($mobile,$otp_no){

        $time = new DateTime(date('Y-m-d H:i:s'));
        $time->add(new DateInterval('PT330M'));
        $dateM = $time->format('Y-m-d H:i:s');

        $data = array('mobile_no'=>$mobile,'otp_no'=>$otp_no,'created_datetime'=>$dateM);
        $this->db->insert('user_mobile_otp', $data);
        return true;
    }

    function sc_check_otp($mobile,$otp_no){
        $query = $this->db->query('select count(*) as cnt from `user_mobile_otp` where `mobile_no`="'.$mobile.'" and `otp_no`="'.$otp_no.'" and status = 0 order by id desc');
        $res = $query->result_array();
        if(!empty($res)){
            if($res['0']['cnt'] == 0){
                return 0;
            }else{
                return 1;
            }
        }
    }

    function get_user_info($user_id){
        $query = $this->db->get_where('user_login', array('id' => $user_id));
        return $query->result_array();
    }

    function get_user_existing_address(){
        $user_id = $this->session->userdata('user_id') ?  $this->session->userdata('user_id') : 0;
       // $query = $this->db->get_where('user_address', array('user_id' => $user_id));
       $query = $this->db->query('select a.`id`, a.`user_id`, a.`first_name`, a.`last_name`, a.`mobile_no`, a.`shipping_address`, a.`pincode`, a.`city_name`, b.`state_name` as states_name, a.`created_datetime`,a.is_default,b.`id` as state_id from user_address as a,states as b where a.`state_name` = b.`id` and `user_id`='.$user_id);
        return $query->result_array();
    }

    function existingAddress($id){
        $user_id = $this->session->userdata('user_id') ?  $this->session->userdata('user_id') : 0;
        //$query = $this->db->get_where('user_address', array('id' => $id,'user_id' => $user_id));
        $query = $this->db->query('select a.`id`, a.`user_id`, a.`first_name`, a.`last_name`, a.`mobile_no`, a.`shipping_address`, a.`pincode`, a.`city_name`, a.`state_name`, a.`created_datetime`,a.is_default,b.`id` as state_id from user_address as a,states as b where a.`state_name` = b.`id` and `user_id`='.$user_id.' and a.`id`='.$id);
        return $query->result_array();
    }

    function update_user_address($data,$user_id){
        //$user_id = $this->session->userdata('user_id');
        $query = $this->db->get_where('user_address', array('user_id' => $user_id,'shipping_address'=>$data['uc_address'],'pincode'=>$data['uc_pincode'],'city_name'=>$data['uc_city'],'state_name'=>$data['uc_state'],'first_name'=>$data['uc_first_name'],'last_name'=>$data['uc_last_name'],'mobile_no'=>$data['uc_mobile']));
        $res = $query->result_array();
        if(empty($res)){
            $add_data = array(
                    'user_id'=>$user_id,
                    'shipping_address'=>$data['uc_address'],
                    'pincode'=>$data['uc_pincode'],
                    'city_name'=>$data['uc_city'],
                    'state_name'=>$data['uc_state'],
                    'first_name'=>$data['uc_first_name'],
                    'last_name'=>$data['uc_last_name'],
                    'mobile_no'=>$data['uc_mobile'],
                );
            $this->db->insert('user_address', $add_data);

        }else{
            $add_data = array(
                    'user_id'=>$user_id,
                    'shipping_address'=>$data['uc_address'],
                    'pincode'=>$data['uc_pincode'],
                    'city_name'=>$data['uc_city'],
                    'state_name'=>$data['uc_state'],
                    'first_name'=>$data['uc_first_name'],
                    'last_name'=>$data['uc_last_name'],
                    'mobile_no'=>$data['uc_mobile'],
                );
            $this->db->where(array('id'=>$data['existing_shipping_add'],'user_id'=>$user_id));
            $this->db->update('user_address', $add_data);
        }
    }

    function notify_me($notify_me_id,$notify_emailid){
        $user_id = $this->session->userdata('user_id') ? $this->session->userdata('user_id') : 0;
        $data = array(
                'user_id' => $user_id,
                'product_id' => $notify_me_id,
                'emailid' => $notify_emailid,
            );
        $this->db->insert('notify_me', $data);
        return 0;
    }

	function br_get_order_price_info_mobile($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where id = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info);
		/*
        $query = $this->db->query('select sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no, a.shipping_address,a.pincode,a.city_name,s.state_name,a.brand_id,a.pay_mode from order_info as a,states as s where a.id in ('.$order_ids.')' );
		*/
		 /*$query = $this->db->query('select sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no, a.shipping_address,a.pincode,a.city_name,(select TRIM(b.`state_name`) as state_na from `city` as a,`states` as b,pincode as c where a.`state_id` = b.`id` AND a.id = c.city_id AND  c.`pincode`=a.pincode) as state_name,a.brand_id,a.pay_mode from order_info as a where a.id in ('.$order_ids.')' );*/
		  $query = $this->db->query('select sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no, a.shipping_address,a.pincode,a.city_name,s.state_name,a.brand_id,a.pay_mode from order_info as a,states as s where a.`state_name` = s.`id` and a.id in ('.$order_ids.')' );
            $query_res = $query->result_array();
            return $query_res;
        }
    }

	function su_get_order_price_info_mobile($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where order_unique_no = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info);
		/*
        $query = $this->db->query('select sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no, a.shipping_address,a.pincode,a.city_name,s.state_name,a.pay_mode from order_info as a,states as s where a.id in ('.$order_ids.')' );
		*/
		/* $query = $this->db->query('select sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no, a.shipping_address,a.pincode,a.city_name,(select TRIM(b.`state_name`) as state_na from `city` as a,`states` as b,pincode as c where a.`state_id` = b.`id` AND a.id = c.city_id AND  c.`pincode`=a.pincode) as state_name,a.pay_mode from order_info as a where a.id in ('.$order_ids.')' );*/

		$query = $this->db->query('select sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no, a.shipping_address,a.pincode,a.city_name,s.state_name,a.pay_mode from order_info as a,states as s where a.`state_name` = s.`id` and a.id in ('.$order_ids.')' );

            $query_res = $query->result_array();
			//print_r($query_res);exit;
            return $query_res;
        }
    }

	function failed_order($first_name,$last_name,$email_id,$address,$SCUniqueID,$mobile_no,$paymod,$uc_pincode,$uc_city,$country=null,$uc_state2=null){
		$query = $this->db->query("select id from cart_info where `SCUniqueID` = '".$SCUniqueID."' AND `order_id` is null");
		$result = $query->result_array();
		$var = $address.','.$uc_pincode.','.$uc_city;
		if(!empty($result))
		{
		$i= 0;
		 foreach($result as $row)
		 {	$date = date('Y-m-d H:i:s');
			 $add_data = array(
					'SCUniqueID'=>$SCUniqueID,
                    'cart_id'=>$result[$i]['id'],
                    'first_name'=>$first_name,
                    'last_name'=>$last_name,
                    'email_id'=>$email_id,
                    'address'=>$var,
					'it_state'=>$uc_state2,
					'country'=>$country,
                    'mobile_no'=>$mobile_no,
					'created_datetime'=>$date,
					'paymod'=>$paymod,
                );
            $this->db->insert('fail_order', $add_data);
			$i++;
		 }
		}
		return true;
	}

	function update_fail_order_status($SCUniqueID,$cart_data,$payent_method,$order_id=null,$user_id=null){
		$count = count($cart_data);
		$cart_id_in = '';
		  if(!empty($cart_data)){
            $i = 0;
            foreach ($cart_data as $result) {
				if($i != 0){ $cart_id_in = $cart_id_in .',';  }
                $cart_id_in = $cart_id_in . $cart_data[$i]['id'];
                $i++;

		  }}
		 $query = $this->db->query("update fail_order SET `status` = 1 where `SCUniqueID` = $SCUniqueID AND paymod = $payent_method AND `cart_Id` in ($cart_id_in)");
		 
		 $query2 = $this->db->query("select country,it_state from fail_order where status=1 and `SCUniqueID` = $SCUniqueID AND paymod = $payent_method AND `cart_Id` in ($cart_id_in) ");
		 $result = $query2->result_array();
		 $query3 = $this->db->query('update order_info SET country_name = "'.$result[0]['country'].'" where `id` = '.$order_id.' and user_id = '.$user_id.' ');
		 $query4 = $this->db->query('update order_info SET it_state = "'.$result[0]['it_state'].'" where `id` = '.$order_id.' and user_id = '.$user_id.' ');
	}

     function get_discount_percentage($brand_id,$product_id){
        if($brand_id > 0){
            $query = $this->db->query("select discount_percent from discount_availed as da where (da.brand_id = '".$brand_id."' || da.product_id = '".$product_id."') AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1");
           /* echo $this->db->last_query();*/
            $result = $query->result_array();
            if(!empty($result)){
                return $result[0]['discount_percent'];
            }else{
                return 0;
            }
        }else{ return 0; }
    }

    function get_discount_id($brand_id,$product_id)
    {
        if($brand_id > 0 && $product_id > 0){
           $query = $this->db->query("select id, discount_type_id from discount_availed as da where (da.brand_id = '".$brand_id."' OR da.product_id = '".$product_id."' ) AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1 ");
          /*   $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = '".$brand_id."' AND da.discount_type_id='".$discount_type_id."' ) ");*/
            $result = $query->result_array();

            if(!empty($result)){
                return $result[0];
            }else{
                return 0;
            }
        }else if($brand_id > 0){
            $query = $this->db->query("select id, discount_type_id from discount_availed as da where (da.brand_id = '".$brand_id."' ) AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1 ");
          /*   $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = '".$brand_id."' AND da.discount_type_id='".$discount_type_id."' ) ");*/
            $result = $query->result_array();
            if(!empty($result)){
                return $result[0];
            }else{
                return 0;
            }

        }else{

            if($product_id > 0){
                $query = $this->db->query("select da.id, da.discount_type_id from discount_availed as da where (da.brand_id = 0 AND da.product_id = '".$product_id."' ) AND da.status=1");
                 /*  $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = '".$product_id."' AND da.discount_type_id='".$discount_type_id."' ) ");*/
                $result = $query->result_array();
                if(!empty($result)){
                    return $result[0];
                }else{
                    return 0;
                }
            }else
            {
                $query = $this->db->query("select da.id, da.discount_type_id from discount_availed as da where (da.brand_id = '".$brand_id."' AND da.product_id = 0 ) AND da.status=1");
                 /*  $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = '".$product_id."' AND da.discount_type_id='".$discount_type_id."' ) ");*/
                $result = $query->result_array();
                if(!empty($result)){
                    return $result[0];
                }else{
                    return 0;
                }
            }
         }
    }

    function get_discount_maxvalue($brand_id,$product_id){
        if($brand_id > 0){
            /*$query = $this->db->query("select max_amount, from discount_availed as da where (da.brand_id = '".$brand_id."' || da.product_id = '".$product_id."') AND NOW() >= da.start_date AND NOW() <= da.end_date");*/
            $query = $this->db->query("select max_amount from discount_availed as da where (da.brand_id = '".$brand_id."') AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1");
            /*echo $this->db->last_query();exit;*/
            $result = $query->result_array();
            if(!empty($result)){
                return $result[0]['max_amount'];
            }else{
                return 0;
            }
        }else{ return 0; }
    }

    function getDiscountInfo($brand_id){
        $query = $this->db->get_where('discount_availed', array('brand_id' => $brand_id));
        /* $query = $this->db->query("select max_amount from discount_availed as da where (da.brand_id = '".$brand_id."') AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1");*/
        $res = $query->result_array();
        if(!empty($res)){
            return $res;
        }else{
            return '';
        }
    }

    function get_product_availablility($product_id){
        $query = $this->db->query("select sum(stock_count) as stock_count from `product_inventory` where product_id = '".$product_id."'");
        $res = $query->result_array();
        if(!empty($res)){
            if($res[0]['stock_count'] > 0){
                return 0;
            }else{
                return 1;
            }
        }
    }

    function get_size($product_id){
        
        $query = $this->db->query("select distinct b.`product_id`,c.`size_text`,b.stock_count,c.`id` from `product_inventory` as b,`product_size` as c where b.`size_id` = c.`id` and b.stock_count > 0 and b.`product_id`='".$product_id."'");
        $res = $query->result_array();
        return $res;

    }

    function productSizeUpdate($uniquecookie,$productsize,$productInfo){
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }
        if($uniquecookie!='' && $productsize!='' && $productInfo!=''){

            $query = $this->db->get_where('cart_info', array('SCUniqueID' => $uniquecookie,'product_size' =>$productsize));
            $res = $query->result_array();
            if(!empty($res)){
                $this->deleteCartProduct($uniquecookie,$productInfo);
                $data = array('product_qty' => $res[0]['product_qty']+1);

                $this->db->where(array('id' => $res[0]['id'],'SCUniqueID' => $uniquecookie));
                $this->db->update('cart_info', $data);
            }else{
                $data = array('product_size' => $productsize);

                $this->db->where(array('id' => $productInfo,'SCUniqueID' => $uniquecookie));
                $this->db->update('cart_info', $data);
            }
        }
    }

    /*function applyCoupon($couponCode,$cartItemarray,$cartItemarray2)
    {
        $coupon_products = '';      
        $coupon_info = array();

        if(!empty($cartItemarray))
        {
            foreach($cartItemarray as $val)
            {
                if($val!='' && $couponCode!='')
                { 
                    //$query = $this->db->query("select a.products,a.coupon_amount,a.brand_id FROM coupon_settings as a WHERE  NOW() >= a.coupon_start_datetime AND NOW() <= a.coupon_end_datetime AND a.`status`=1 AND a.`is_delete`=0 AND a.`coupon_code`='".$couponCode."'");
                  
                    $query = $this->db->query("SELECT `coupon_code`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `brand_id`, `products`, `exclude_products` FROM `coupon_settings` WHERE NOW() BETWEEN `coupon_start_datetime` AND `coupon_end_datetime` AND`status`=1 AND `is_delete`=0 AND brand_id='".$val."' AND `usage_limit_per_coupon`>`coupon_used_count` AND `coupon_code`='".$couponCode."' "); 
                    //return $this->db->last_query();
                    $result = $query->result_array();
                   
                    if(!empty($result))
                    {
                       $coupon_info['coupon_products'] =  $coupon_products.$result[0]['products'];
                       $coupon_info['coupon_code'][$result[0]['brand_id']] = $result[0]['coupon_amount']; 

                    }                    
                }                
            }           
        }    
        if(!empty($coupon_info))
        {

          return $coupon_info;
        }else
        {
            return False;
        }
          
    }*/

    /*function applyCoupon($couponCode,$cartItemarray,$cartItemarray2)
    {
        $coupon_products = '';
        $logged_in_user ='';     
        $coupon_info = array();
        $logged_in_user = $this->session->userdata('email');

        if(!empty($cartItemarray))
        {
            foreach($cartItemarray as $val)
            {
                if($val!='' && $couponCode!='')
                { 
                    //$query = $this->db->query("select a.products,a.coupon_amount,a.brand_id FROM coupon_settings as a WHERE  NOW() >= a.coupon_start_datetime AND NOW() <= a.coupon_end_datetime AND a.`status`=1 AND a.`is_delete`=0 AND a.`coupon_code`='".$couponCode."'");
                  
                    $query = $this->db->query("SELECT `individual_use_only`,`coupon_code`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `brand_id`, `products`, `exclude_products`,`coupon_email_applied`,`stylecrackerDiscount`,`individual_use_only` FROM `coupon_settings` WHERE NOW() BETWEEN `coupon_start_datetime` AND `coupon_end_datetime` AND`status`=1 AND `is_delete`=0 AND (brand_id = '".$val."' || brand_id=0 ) AND `usage_limit_per_coupon`>`coupon_used_count` AND `coupon_code`='".$couponCode."' "); 
                    
                    $result = $query->result_array();
                   
                    if(!empty($result))
                    {
                       $coupon_info['coupon_products'] =  $coupon_products.$result[0]['products'];
                       $coupon_info['coupon_code'][$result[0]['brand_id']] = $result[0]['coupon_amount']; 
                       $individual_use_only =  $result[0]['individual_use_only'];

                       $apply_email = $result[0]['coupon_email_applied'];
                       //$result[0]['brand_id']==0 && 
                        if( (strtolower($result[0]['coupon_code'])=='sccart20' && $logged_in_user=='' && $individual_use_only == 1) )
                       {
                         return 'NotLoggedIn';
                       }else
                       {                         
                         if($apply_email!="" && $individual_use_only == 1 && strtolower($result[0]['coupon_code'])=='sccart20')
                         { 
                            $apply_email_array = explode(',',$apply_email);
                           if(in_array($logged_in_user, $apply_email_array))
                           {
                               return $coupon_info;
                           }
                         }
                         
                       }

                    }                    
                }                
            }           
        }

        if(!empty($coupon_info) && strtolower($result[0]['coupon_code'])!='sccart20')
        {
          return $coupon_info;
        }else
        {
            return False;
        }
          
    }*/
	
	function applyCoupon($couponCode,$cartItemarray,$cartItemarray2)
    {
        $coupon_products = '';
        $logged_in_user ='';     
        $coupon_info = array();
        $logged_in_user = $this->session->userdata('email');

        if(!empty($cartItemarray))
        {
            foreach($cartItemarray as $val)
            {
                if($val!='' && $couponCode!='')
                { 
                    //$query = $this->db->query("select a.products,a.coupon_amount,a.brand_id FROM coupon_settings as a WHERE  NOW() >= a.coupon_start_datetime AND NOW() <= a.coupon_end_datetime AND a.`status`=1 AND a.`is_delete`=0 AND a.`coupon_code`='".$couponCode."'");
                  
                    $query = $this->db->query("SELECT `individual_use_only`, `coupon_code`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `brand_id`, `products`, `exclude_products`,`coupon_email_applied`,`stylecrackerDiscount`,`individual_use_only` FROM `coupon_settings` WHERE NOW() BETWEEN `coupon_start_datetime` AND `coupon_end_datetime` AND`status`=1 AND `is_delete`=0 AND (brand_id = '".$val."' || brand_id=0) AND `usage_limit_per_coupon`>`coupon_used_count` AND `coupon_code`='".$couponCode."' "); 
                    //return $this->db->last_query();
                    $result = $query->result_array();
                   
                    if(!empty($result))
                    {
                       $coupon_info['coupon_products'] =  $coupon_products.$result[0]['products'];
                       $coupon_info['coupon_code'][$result[0]['brand_id']] = $result[0]['coupon_amount']; 
                       $coupon_info['min_spend'][$result[0]['brand_id']] = $result[0]['min_spend']; 
                       $coupon_info['max_spend'][$result[0]['brand_id']] = $result[0]['max_spend'];

                       $individual_use_only =  $result[0]['individual_use_only']; 
                       $coupon_stylecracker =   $result[0]['stylecrackerDiscount']; 
                       $apply_email = $result[0]['coupon_email_applied'];                      
                       
                          
                        if($result[0]['brand_id']==0 && $coupon_stylecracker==1 && ($individual_use_only==1 && $logged_in_user=='' ) )
                       {
                         return 'NotLoggedIn';
                       }else
                       {
                          if($apply_email!="" &&  $individual_use_only == 1)
                         {
                            $apply_email_array = explode(',',$apply_email);
                           if(in_array($logged_in_user, $apply_email_array))
                           {
                               return $coupon_info;
                           }else
                           {
                              return False;
                           }
                         }
                       }

                    }                    
                }                
            }           
        }

        if(!empty($coupon_info))
        {          
          return $coupon_info;
        }else
        {
            return False;
        }
          
    }


   
    function getCouponData($couponCode,$CbrandId=NULL)
    {
        if($CbrandId!='')
        {
          //$brand_cond =' AND `brand_id`='.$CbrandId;
           $brand_cond =' AND (brand_id = "'.$CbrandId.'" || brand_id=0) ';
        }else
        {
          $brand_cond = '';
        }
        $query = $this->db->query("SELECT `coupon_code`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `brand_id`, `products`, `exclude_products`,`coupon_used_count`,`usage_limit_per_coupon`,`stylecrackerDiscount`,`individual_use_only` FROM `coupon_settings` WHERE NOW() BETWEEN `coupon_start_datetime` AND `coupon_end_datetime` AND`status`=1 AND `is_delete`=0 AND `usage_limit_per_coupon`>`coupon_used_count` AND `coupon_code`='".$couponCode."' ".$brand_cond." "); 
            //return $this->db->last_query();
            $result = $query->result_array();
           
            if(!empty($result))
            {
              /* $coupon_info['coupon_products'] =  $coupon_products.$result[0]['products'];*/
               $coupon_info['coupon_amount'] = $result[0]['coupon_amount']; 
               $coupon_info['brand_id'] = $result[0]['brand_id']; 
               $coupon_info['coupon_products'] = $result[0]['products'];
               $coupon_info['coupon_min_spend'] = $result[0]['min_spend'];
               $coupon_info['coupon_max_spend'] = $result[0]['max_spend'];
               $coupon_info['coupon_discount_type'] = $result[0]['discount_type'];
               $coupon_info['coupon_used_count'] = $result[0]['coupon_used_count'];
               $coupon_info['usage_limit_per_coupon'] = $result[0]['usage_limit_per_coupon'];
			   $coupon_info['stylecrackerDiscount'] = $result[0]['stylecrackerDiscount'];
			   $coupon_info['individual_use_only'] = $result[0]['individual_use_only'];
            }  

            if(!empty($coupon_info))
            {
              return $coupon_info;
            }else
            {
                return 0;
            }               
    }

    function getUserCoupon($couponCode, $CbrandId = "")
    {

      if($CbrandId!='')
      {
        //$brand_cond =' AND `brand_id`='.$CbrandId;
        $brand_cond =' AND ( `brand_id` = "'.$CbrandId.'" || brand_id=0) ';
      }else
      {
        $brand_cond = '';
      }
        $query = $this->db->query("SELECT `coupon_code`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `brand_id`, `products`, `exclude_products`,`coupon_used_count`,`usage_limit_per_coupon`,`stylecrackerDiscount`,`individual_use_only` FROM `coupon_settings` WHERE NOW() BETWEEN `coupon_start_datetime` AND `coupon_end_datetime` AND`status`=1 AND `is_delete`=0  AND `coupon_code`='".$couponCode."' ".$brand_cond."; "); 
            //return $this->db->last_query();
            $result = $query->result_array();
           
            if(!empty($result))
            {
              /* $coupon_info['coupon_products'] =  $coupon_products.$result[0]['products'];*/
               $coupon_info['coupon_amount'] = $result[0]['coupon_amount']; 
               $coupon_info['brand_id'] = $result[0]['brand_id']; 
               $coupon_info['coupon_products'] = $result[0]['products'];
               $coupon_info['coupon_min_spend'] = $result[0]['min_spend'];
               $coupon_info['coupon_max_spend'] = $result[0]['max_spend'];
               $coupon_info['coupon_discount_type'] = $result[0]['discount_type'];
               $coupon_info['coupon_used_count'] = $result[0]['coupon_used_count'];
               $coupon_info['usage_limit_per_coupon'] = $result[0]['usage_limit_per_coupon'];
			   $coupon_info['stylecrackerDiscount'] = $result[0]['stylecrackerDiscount'];
               $coupon_info['individual_use_only'] = $result[0]['individual_use_only'];
            }  

            if(!empty($coupon_info))
            {
              return $coupon_info;
            }else
            {
                return 0;
            }               
    }

    function update_coupon_counter($couponCode,$CbrandId='')
    {   
        if($couponCode!='')
        {           

          $coupon_used_count = $this->getCouponData($couponCode)['coupon_used_count'];
          $usage_limit_per_coupon = $this->getCouponData($couponCode)['usage_limit_per_coupon'];

          if($coupon_used_count<=$usage_limit_per_coupon)
          {
              $coupon_used_count++;
              $this->db->where('coupon_code',$couponCode,'brand_id',$CbrandId);
              $result = $this->db->update('coupon_settings', array('coupon_used_count'=>$coupon_used_count));         
              if($result == TRUE)
              {
                return TRUE;
              }else
              {
                return FALSE;
              }            
          }else
          {
              return FALSE;
          }
          
        }       
    }

	function get_state_id($pincode)
  { 
		$query = $this->db->query("select a.id as state_id from states a,pincode b,city as c where b.pincode = $pincode AND b.city_id = c.id AND c.state_id = a.id ");
		$result = $query->result_array();	
		// print_r($result);exit;
		if(!empty($result)) return $result[0]['state_id']; else return 0;
	}

   function get_productDiscount_price($product_id,$brand_id)
    {
        $discount_price = 0;
        $product_discount = array();
        if($product_id !='' && $brand_id !=''){
            $query = $this->db->query("select (select price from product_desc where id = '".$product_id."') as product_price,discount_percent from discount_availed as da where (da.brand_id = '".$brand_id."' OR da.product_id = '".$product_id."') AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1");
            /*echo $this->db->last_query();*/
            $result = $query->result_array();

            if(!empty($result)){
                $discount_percent = $result[0]['discount_percent'];
                $product_price = $result[0]['product_price'];
                $discount_price =($product_price*$discount_percent)/100;
                $discounted_product_price = $product_price - $discount_price;

                $product_discount['discount_percent'] = $discount_percent;
                $product_discount['discounted_product_price'] = round($discounted_product_price);
                return $product_discount;
            }else{
                return 0;
            }
        }else{ return 0; }
    }

    function getProductSku($productId,$productSize)
    {
        if($productId!="" && $productSize!="")
        {
          $query = $this->db->query("select product_sku from product_inventory where product_id='".$productId."' AND size_id='".$productSize."'; ");
           /* echo $this->db->last_query();*/
            $result = $query->result_array();           
        }   

        if($result!="")
        {
          return $result[0]['product_sku'];
        }else
        {
          return '';
        }     
          
    }

     function sc_coupon_campaign($data)
    {
         if(isset($data))
        {
           $result = $this->db->insert('couponcode_campaign_email', $data);         
        }   
       
        if($result)
        {
          return 1;
        }else
        {
          return 0;
        }     
    }

    function calculate_product_discountAmt($product_price,$totalproductprice,$discountType,$discountAmt)
    { //echo $product_price.'--'.$totalproductprice.'--'.$discountType.'--'.$discountAmt.'<br>';
      if($product_price!='' && $totalproductprice!=0 && $discountAmt!='')
      {  

        if($discountType==1)
        {
            $percent = ($product_price*100)/$totalproductprice;
            $DiscountAmt = ($discountAmt*$percent)/100;
            $productDiscountAmt = $product_price-$DiscountAmt;
            return $productDiscountAmt;

        }else if($discountType==2)   
        {     
            $percent = $discountAmt;  
            $DiscountAmt = ($product_price*$percent)/100;
            $productDiscountAmt = $product_price-$DiscountAmt;  
            return $productDiscountAmt;
        }   
       
      }else
      {
        return 0;
      }
    }

  function showEmailPopUp($couponCode)
  {
    if($couponCode!='')
    {
      $query = $this->db->query("SELECT `coupon_start_datetime`, `coupon_end_datetime`,`showEmailPopUp` FROM `coupon_settings` WHERE NOW() BETWEEN `coupon_start_datetime` AND `coupon_end_datetime` AND`status`=1 AND `is_delete`=0 AND `usage_limit_per_coupon`<=`coupon_used_count` AND `coupon_code`='".$couponCode."' "); 
            //return $this->db->last_query();
      $result = $query->result_array();     
      if($result)
      {
        if($result[0]['showEmailPopUp']==1)
        {
          return 1;
        }else
        {
          return 0;
        }
      }else
      {
        return 0;
      }
    }      
  }

    function getMyOrderCoupon($couponCode,$orderDate, $CbrandId = "")
        {
      
          if($CbrandId!='')
          {
           /* $brand_cond =' AND `brand_id`='.$CbrandId;*/
           $brand_cond =' AND (brand_id = "'.$CbrandId.'" || brand_id=0) ';
          }else
          {
            $brand_cond = '';
          }
            $query = $this->db->query("SELECT `coupon_code`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `brand_id`, `products`, `exclude_products`,`coupon_used_count`,`usage_limit_per_coupon`,`stylecrackerDiscount`,`individual_use_only`,`coupon_email_applied` FROM `coupon_settings` WHERE '".$orderDate."' BETWEEN `coupon_start_datetime` AND `coupon_end_datetime` AND`status`=1 AND `is_delete`=0  AND `coupon_code`='".$couponCode."' ".$brand_cond."; "); 
                /*echo  $this->db->last_query();*/
                $result = $query->result_array();
               
                if(!empty($result))
                {
                  /* $coupon_info['coupon_products'] =  $coupon_products.$result[0]['products'];*/
                   $coupon_info['coupon_amount'] = $result[0]['coupon_amount']; 
                   $coupon_info['brand_id'] = $result[0]['brand_id']; 
                   $coupon_info['coupon_products'] = $result[0]['products'];
                   $coupon_info['coupon_min_spend'] = $result[0]['min_spend'];
                   $coupon_info['coupon_max_spend'] = $result[0]['max_spend'];
                   $coupon_info['coupon_discount_type'] = $result[0]['discount_type'];
                   $coupon_info['coupon_used_count'] = $result[0]['coupon_used_count'];
                   $coupon_info['usage_limit_per_coupon'] = $result[0]['usage_limit_per_coupon'];
                   $coupon_info['stylecrackerDiscount'] = $result[0]['stylecrackerDiscount'];
                   $coupon_info['individual_use_only'] = $result[0]['individual_use_only'];
                   $coupon_info['coupon_email_applied'] = $result[0]['coupon_email_applied'];
                }  
      
                if(!empty($coupon_info))
                {
                  return $coupon_info;
                }else
                {
                    return 0;
                }               
        }

    function get_cartid($uniquecookie,$productid,$userid,$size,$agent,$platform_info,$look_id)
    {    
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }

        if($productid!='' && $size!='' && $user_id!=0 ){
        $query = $this->db->query("select * from cart_info where ( user_id = '".$user_id."' and `product_id` = '".$productid."' and `product_size` = '".$size."' and `order_id` is null )");
        $res = $query->result_array();
        //echo $this->db->last_query();
        //echo '<pre>';print_r($res);exit;
        return $res[0]['id'];

      }
    }

     function updateproductprice($productid,$price)
    {
      $user_id = $this->session->userdata('user_id');
      //echo $productid;echo '<br/>';echo $price;exit;
      if($productid!='' &&  $price!='' && $price>0 ){
            $data = array('price' => $price,'modified_by' => $user_id);
             //$this->db->where(array('id' => $productInfo,'SCUniqueID' => $uniquecookie,'user_id'=>$user_id));
             $this->db->where(array('id' => $productid));
             $this->db->update('product_desc', $data);
             //echo $this->db->last_query();exit;
      }
      return true;
    }
	
	function getUserAddress($id){
        $user_id = $this->session->userdata('user_id') ?  $this->session->userdata('user_id') : 0;
        //$query = $this->db->get_where('user_address', array('id' => $id,'user_id' => $user_id));
        $query = $this->db->query('select a.`id`, a.`user_id`, a.`first_name`, a.`last_name`, a.`mobile_no`, a.`shipping_address`, a.`pincode`, a.`city_name`, a.`state_name` as state_id,b.`state_name` as state_name, a.`created_datetime`,a.is_default,b.`id` as state_id from user_address as a,states as b where a.`state_name` = b.`id` and `user_id`='.$user_id.' and a.`id`='.$id);
        $res = $query->result_array();
         return json_encode($res);		 
    }

    function update_scboxcart($uniquecookie,$productid,$userid,$size,$agent,$platform_info,$quantity,$productprice=null,$sc_data=null){
       
       $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }
       $cartid = '';
       // $user_id = $this->session->userdata('user_id'); if(!$user_id){ if($userid!=''){ $user_id = $userid; }else{ $user_id = 0;} }
         
        // $time = new DateTime(date('Y-m-d H:i:s'));
        // $time->add(new DateInterval('PT330M'));
        // $dateM = $time->format('Y-m-d H:i:s');

        $dateM = date('Y-m-d H:i:s');
        $productids_str = SCBOX_PRODUCTID_STR;

        if($productid!='' && $size!=''){
          if($userid!='')
          {
            //$query = $this->db->query("select * from cart_info where (`SCUniqueID` = '".$uniquecookie."' and `user_id` = '".$userid."' and `product_id` IN (".$productids_str.") and `product_size` = '".$size."' and `order_id` is null )");
            $query = $this->db->query("select * from cart_info where ( `SCUniqueID` = '".$uniquecookie."' and `user_id` = '".$userid."' and `product_id` IN (".$productids_str.") and `product_size` = '".$size."' and `order_id` is null )");
          }else
          { 
             $query = $this->db->query("select * from cart_info where (`SCUniqueID` = '".$uniquecookie."' and `product_id` IN (".$productids_str.") and `product_size` = '".$size."' and `order_id` is null )");
          }
        //echo $this->db->last_query();exit;
        $res = $query->result_array();
    
        if(empty($res)){
            $data = array(
               'product_id' => $productid ,
               'user_id' => $user_id,
               'created_datetime' => $dateM,
               'product_size' => $size,
               'product_qty' => $quantity,
               'SCUniqueID' => $uniquecookie,
               'agent' =>$agent,
               'platform_info'=>$platform_info,
               'product_price'=>$productprice,
               'sc_source'=>@$sc_data['utm_source'],
               'sc_medium'=>@$sc_data['utm_medium'],
               'sc_campaign'=>@$sc_data['utm_campaign'],
            );
            $this->db->insert('cart_info', $data);

            $cartid = $this->db->insert_id();

        }else{

            $cartid = $res[0]['id'];

            if($res[0]['product_qty']<5){
                $data = array(
                   'product_id' => $productid ,
                   'user_id' => $user_id,
                   'created_datetime' => $dateM,
                   'product_size' => $size,
                   'product_qty' => $quantity,
                   'SCUniqueID' => $uniquecookie,
                   'agent' =>$agent,
                   'platform_info'=>$platform_info,
                   'product_price'=>$productprice,
                   'sc_source'=>@$sc_data['utm_source'],
                   'sc_medium'=>@$sc_data['utm_medium'],
                   'sc_campaign'=>@$sc_data['utm_campaign'],
                );

                //$this->db->where(array('id'=>$res[0]['id'],'product_id' => $productid,'product_size' => $size,'SCUniqueID' => $uniquecookie));
                 $this->db->where(array('id'=>$res[0]['id'],'product_size' => $size,'SCUniqueID' => $uniquecookie));      
                $this->db->update('cart_info', $data);
                }
            }
        }

        return $cartid;

    }


    function scbox_get_order_info($order_id){
        $result = array();  
        $order_array = explode('_',$order_id);    
        if(@$order_array[0]==$this->session->userdata('user_id'))
        {
           $query = $this->db->query('select id,user_id,order_total,pay_mode,order_display_no from `order_info` where order_unique_no = "'.$order_id.'"');
           $result = $query->result_array();
            return $result;
        }else
        {
          return $result;
        }
    }

   function getUserDataByEmail($emaild){
        $query = $this->db->query('select a.id,a.email,b.gender,b.platform_name,b.contact_no,b.birth_date from user_login as a, user_info as b where email ="'.$emaild.'" and a.id = b.user_id');
        $result = $query->result_array();
        return $result;
    }

    function check_order_exist($displayorderid)
    {
       $query = $this->db->query('select count(user_id) as rowcount from `order_info` where order_display_no = "'.$displayorderid.'"');
       $result = $query->result_array();      
       if($result[0]['rowcount']>0)
       {
          return 'true';
       }else
       {
          return 'false';
       }          
    }

    function update_order_displayno($order_unique_no,$new_orderno)
    {
        $data = array(
                'order_display_no' => 'SC'.$new_orderno
            );

        $this->db->where_in('order_unique_no', $order_unique_no);
        $this->db->update('order_info', $data);
    }

    function get_user_credential_by_email_password($emaild='', $userpassword='')
    {
      $this->db->select('ul.id,ul.email,ul.password,ui.first_name,ui.last_name,ui.gender,ui.platform_name,ui.contact_no,ui.birth_date');
      $this->db->from('user_login as ul');
      $this->db->join('user_info as ui', 'ul.id = ui.user_id', 'INNER');
      $this->db->where('ul.email', $emaild);
      if($userpassword != '' && !empty($userpassword))
      {
        $this->db->where('ul.password', $userpassword);
      }
      $query = $this->db->get();
      if($query->num_rows()>0)
      {
        return $query->row_array();
      }
      return false;
    }
  
  public function get_user_address($userId ='')
  {
    $this->db->select('oi.id as address_id,oi.pincode,oi.city_name,oi.state_name,oi.shipping_address,oi.mobile_no');
    $this->db->from('order_info as oi');
    $this->db->where('oi.user_id', $userId);
    $this->db->order_by('oi.id','DESC');
    $this->db->limit('1');
    $query = $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->row_array();
    }
    return false;
  }

  public function get_user_profession($userId ='', $user_profession='')
  {
    $this->db->select('om.object_meta_key,om.object_meta_value');
    $this->db->from('object_meta as om');
    $this->db->where('om.created_by', $userId);
    $this->db->where('om.object_meta_key', $user_profession);
    $this->db->order_by('om.id','DESC');
    $this->db->limit('1');
    $query = $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->row_array();
    }
    return false;
  }   
}

?>
