<?php
class Analytics_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
     function add_look_click($user_def_id,$look_id,$type){
        if($look_id){
            if($this->session->userdata('user_id')) $user_id = $this->session->userdata('user_id'); else $user_id = $user_def_id;
           /* $old_id = $this->get_look_click($look_id,$user_id);
            if(!empty($old_id)){
                $past_count = $old_id[0]['click_count'];
                $current_count = $past_count+1;
                //update section
                $data = array('click_count'=>$current_count);
                $this->db->update('users_look_click_track', $data, array('look_id' => $look_id,'user_id' => $user_id ));
            }
            else{*/
                //insert section
                $data = array('user_id'=>$user_id,'look_id' => $look_id,'click_count' => 1);
                $this->db->insert('users_look_click_track', $data);
            //}
            
             

        }
     }

     function get_look_click($look_id,$user_id){

        if($look_id){
            $query = $this->db->query('select click_count from users_look_click_track where look_id="'.$look_id.'" and user_id="'.$user_id.'"');
            $result = $query->result_array();
            return $result;
        }
    }

    function add_product_click($user_def_id,$product_id,$type,$SCUniqueID){
        if($product_id){
            if($this->session->userdata('user_id')) $user_id = $this->session->userdata('user_id'); else $user_id = $user_def_id;
           /* $old_id = $this->get_product_click($product_id,$user_id);
            if(!empty($old_id)){
                $past_count = $old_id[0]['click_count'];
                $current_count = $past_count+1;
                //update section
                $data = array('click_count'=>$current_count);
                $this->db->update('users_product_click_track', $data, array('product_id' => $product_id,'user_id' => $user_id ));
            }
            else{*/
                //insert section
                $data = array('user_id'=>$user_id,'product_id' => $product_id,'click_count' => 1,'click_source'=>$type,'unique_id'=>$SCUniqueID);
                $this->db->insert('users_product_click_track', $data);
           // }

        }
     }

     function get_product_click($product_id,$user_id){

        if($product_id){
            $query = $this->db->query('select click_count from users_product_click_track where product_id="'.$product_id.'" and user_id="'.$user_id.'"');
            $result = $query->result_array();
            return $result;
        }
    }

    function add_brand_click($user_def_id,$brand_id,$type){
        if($brand_id){
            if($this->session->userdata('user_id')) $user_id = $this->session->userdata('user_id'); else $user_id = $user_def_id;
           /* $old_id = $this->get_product_click($brand_id,$user_id);
            if(!empty($old_id)){
                $past_count = $old_id['click_count'];
                $current_count = $past_count+1;
                //update section
                $data = array('click_count'=>$current_count);
                $this->db->update('users_brand_click_track', $data, array('brand_id' => $brand_id,'user_id' => $user_id ));
            }
            else{*/
                //insert section
                $data = array('user_id'=>$user_id,'brand_id' => $brand_id,'click_count' => 1);
                $this->db->insert('users_brand_click_track', $data);
           // }

        }
     }

     function get_brand_click($brand_id,$user_id){

        if($brand_id){
            $query = $this->db->query('select click_count from users_brand_click_track where brand_id="'.$brand_id.'" and user_id="'.$user_id.'"');
	    $result = $query->result_array();
            return $result;
        }
    }

    //function for social media sharing  
    function social_media_share($user_def_id,$look_id,$source){
        if($look_id){
            if($this->session->userdata('user_id')) $user_id = $this->session->userdata('user_id'); else $user_id = $user_def_id;
            $data = array('user_id'=>$user_id,'look_id' => $look_id,'source' => $source);
            $this->db->insert('user_share', $data);
        }
    }

    //function for add geo location
    function add_geo_loc($user_def_id,$loc){
        
        if($this->session->userdata('user_id')) $user_id = $this->session->userdata('user_id'); else $user_id = $user_def_id;
      
            $data = array('user_id'=>$user_id,'user_loc' => $loc); 
            $this->db->insert('user_location_details', $data);

        
    }
    
    //function for add geo location
    function track_user_activity_model($p_visited, $p_id, $page_scroll, $time_spent, $user_def_id){
        if($this->session->userdata('user_id')) $user_id = $this->session->userdata('user_id'); else $user_id = $user_def_id;
echo $user_id;
            /*{"p_visited":18,"p_id":2,"page_scroll":0,"time_spent":715}
		*/
		//table user_page_analytics insert and update
        if($user_id != 0){
		$get_data = $this->db->query("SELECT user_id,page_id,time_spend,page_visited FROM user_page_analytics WHERE user_id='".$user_id ."' AND page_id='".$p_id."'");
		$get_data_res = $get_data->result_array();
		
		if($get_data_res== NULL){
			$data = array('user_id'=>$user_id,'page_id' => $p_id,'page_visited' => $p_visited,'time_spend'=>$time_spent);
                        $this->db->insert('user_page_analytics', $data);
		}else{
			$time_spent = $time_spent + $get_data_res[0]['time_spend'];
			$p_visited = $p_visited + $get_data_res[0]['page_visited'];
			$data=array('time_spend'=>$time_spent,'page_visited'=>$p_visited);
			$this->db->update('user_page_analytics', $data, array('user_id' => $userid, page_id => $p_id));
		}
         }else{
               $data = array('user_id'=>$user_id,'page_id' => $p_id,'page_visited' => $p_visited,'time_spend'=>$time_spent);
               $this->db->insert('user_page_analytics', $data);
         }
		
	//table user_page_scroll_history updates
	$data = array('user_id'=>$user_id,'page_id' => $p_id,'page_scroll' => $page_scroll);
        $this->db->insert('user_page_scroll_history', $data);

    }
    
    function update_user_session($session_counter, $user_id, $source, $pvisit, $fvisit, $cvisit){
    	if($this->session->userdata('user_id')) $user_id = $this->session->userdata('user_id'); else $user_id = $user_id;
        if($user_id != 0){
                $get_data = $this->db->query("SELECT * FROM user_history_info WHERE user_id='".$user_id."'");
		$get_data_res = $get_data->result_array();
		
		if($get_data_res== NULL){
			$data = array('user_id'=>$user_id,'visit_count' => $session_counter,'first_visit' => $fvisit);
			$this->db->insert('user_history_info', $data);
			
		}else{
			$data=array('visit_count'=>$session_counter);
			$this->db->update('user_history_info', $data, array('user_id' => $user_id ));
		}
                $get_data = $this->db->query("SELECT current_visit_time FROM user_visit_history WHERE user_id='".$user_id."'");
		$get_data_res = $get_data->result_array();

		if(empty($get_data_res)) $insert=1; 
		if(isset($get_data_res['current_visit_time']) && (strcmp($get_data_res['current_visit_time'], $cvisit)!=0)) $insert=1;
		if($insert == 1){
		     $add_to_table = array(`user_id`=>$userid, `current_visit_time`=>$cvisit, `last_visit_time`=>$pvisit, `campain_source`=>$source);
                     $this->db->insert('user_history_info', $add_to_table);
		}               
        }else{
          $data = array('user_id'=>$user_id,'visit_count' => $session_counter,'first_visit' => $fvisit);
	  $this->db->insert('user_history_info', $data);
        }
            
    }
 } 
?>