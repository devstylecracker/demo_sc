<?php
class Mcart_model_android extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function update_cart($productid,$userid,$productsize,$paymod,$agent,$medium,$look_id,$price){
		$size = $this->productsize($productsize);
        if($productid!='' && $size!=''){
			//echo $uniquecookie."_".$productid."_".$userid."_".$size;exit;
			$query = $this->db->query("select * from cart_info where (`user_id` = '".$userid."' and `product_id` = '".$productid."' and `product_size` = '".$size."' AND `order_id` is null)");
			$res = $query->result_array(); 
			if(empty($res)){
				$data = array(
				   'product_id' => $productid ,
				   'user_id' => $userid,
				   'created_datetime' => date('Y-m-d H:i:s'),
				   'product_size' => $size,
				   'product_qty' => '1',
				   'agent' =>$agent,
				   'platform_info'=>$medium,
				   'look_id'=>$look_id,
				   'product_price'=>$price
				);
				$this->db->insert('cart_info', $data);
			}else{
				if($res[0]['product_qty']<5){
					$data = array(
						'product_id' => $productid ,
						'user_id' => $userid,
						'created_datetime' => date('Y-m-d H:i:s'),
						'product_size' => $size,
						'product_qty' => $res[0]['product_qty']+1,
						'agent' =>$agent,
						'platform_info'=>$medium,
						'look_id'=>$look_id,
						'product_price'=>$price
					);
				  
					$this->db->where(array('id'=>$res[0]['id'],'product_id' => $productid,'product_size' => $size));
					$this->db->update('cart_info', $data); 
				}   
			}
        }
		$query = $this->db->query('select a.`id`,count(a.`product_id`) as item_count,a.`product_id`,SUM(a.`product_qty`) as item_qty,a.`product_qty`,b.`name`,(a.`product_qty`*a.product_price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,a.SCUniqueID from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") order by a.`created_datetime` DESC');
		$res = $query->result_array();
		$cart_object = $this->cart_object($res,$userid,$paymod);
		return $cart_object[0];     
    }
	
	function productsize($productsize){	
		$query = $this->db->query("select id from product_size where `size_text`  LIKE '$productsize' ");
		$res = $query->result_array();
		return $res[0]['id'];
	}
	
    function get_cart($userid,$paymod='',$pincode='',$chkShipping='',$chkCod='',$city='',$state='',$coupon_code='',$sc_credit='',$is_scbox=0){
		if($pincode != ''){
			$productid = $this->get_product_id($userid);
			$chkShipping = $this->chkShipping2($productid,$pincode);
			$chkCod = $this->chkCod2($productid,$pincode);
			$city_name = $this->get_city_name($pincode);	
			if(!empty($city_name)){
				$city = $city_name['city']; 
				$state = $city_name['state'];
			} 
		}
		 
		//added for scbox products
		if($is_scbox == '1'){
			$scbox_product_condtion = ' a.`product_id` IN ('.SCBOX_PRODUCTID_STR.') ';  
		}else{
			$scbox_product_condtion = ' a.`product_id` NOT IN ('.SCBOX_PRODUCTID_STR.') ';  
		}
		//added for scbox products
		// echo $scbox_product_condtion;exit;
		$query = $this->db->query('select a.`id`,count(a.`product_id`) as item_count,a.`product_id`,SUM(a.`product_qty`) as item_qty,a.`product_qty`,b.`name`,(a.`product_qty`*a.product_price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,a.SCUniqueID from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") AND '.$scbox_product_condtion.' order by a.`created_datetime` DESC LIMIT 0,1');
        $res = $query->result_array();
		
		
		//print_r($res);exit;
		$cart_object = $this->cart_object($res,$userid,$paymod,$pincode,$chkShipping,$chkCod,$city,$state,$coupon_code,$sc_credit,$is_scbox);
        if(!empty($cart_object))
		{	//echo $cart_object[0]->data->item_count <= 1;exit;
			//if($cart_object[0]->data->item_count >= 1){ return $cart_object[0];} else { return NULL;}
			return $cart_object[0];
		}
		else return '';
    }
	
	public function get_product_by_cart_id($cartid = '')
	{
		$this->db->select('ci.id,ci.product_id,count(ci.`product_id`) as item_count,ci.user_id,ci.SCUniqueID,ci.order_id,ci.product_size,ci.product_qty,SUM(ci.`product_qty`) as item_qty,ci.product_price,ci.created_datetime,pd.name,pd.image,ps.size_text,bi.company_name,bi.user_id,bi.min_del_days,bi.max_del_days,bi.store_tax,bi.is_shipping,bi.shipping_charges,bi.shipping_min_values,bi.shipping_max_values,bi.is_cod,bi.cod_charges,bi.cod_min_value,bi.code_max_value');
		$this->db->from('cart_info as ci');
		$this->db->join('product_desc as pd', 'ci.product_id = pd.id', 'inner');
		$this->db->join('product_size as ps', 'ci.product_size = ps.id', 'inner');
		$this->db->join('brand_info as bi', 'pd.brand_id = bi.user_id', 'inner');
		$this->db->where('ci.id', $cartid);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		return false;
	}
	
	public function get_address_by_address_id($userid='', $addressid)
	{
		$this->db->select('ua.shipping_address,ua.pincode,ua.city_name,ua.state_name,ua.address_type,s.state_name');
		$this->db->from('user_address as ua');
		$this->db->join('states as s', 's.id = ua.state_name', 'inner');
		$this->db->where('ua.user_id', $userid);
		$this->db->where('ua.id', $addressid);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		return false;
	}
	
	
  
	function cart_object($res,$userid,$paymod='',$pincode='',$chkShipping='',$chkCod='',$city='',$state='',$coupon_code='',$sc_credit='',$is_scbox=''){
//echo $userid.'___'.$paymod.'___'.$pincode.'___'.$chkShipping.'___'.$chkCod.'___'.$city.'___'.$state;exit;
		$cart_array = array();	
			$i = 0; 
			
			if(($coupon_code == 'NOCASH500' || $coupon_code == 'nocash500' || $coupon_code == 'NOCASH1000' || $coupon_code == 'nocash1000') && $paymod == 'cod')
			{
				$cod_available = 0;$coupon_code='';
			}else { 
				$cod_available = 1;
			}
			
			//print_r($res);exit;
			if(!empty($res)){
				foreach($res as $val){
					$cart_object = new stdClass(); 
					$data =	new stdClass();
					$data->item_count = $val['item_count'];
					$data->item_qty = $val['item_qty'];
					$data->pincode = $pincode;
					$data->city = $city;
					$data->state = $state;
					$res = $this->product_stock($userid);
					//print_r($res);exit;
					$count = count($res);
					$count1 = count($chkShipping);
					$count2 = '';
					
					
					
					if($count1=1 && !empty($chkShipping)){
						$notship = $this->check_ship($chkShipping);
						$data->error_message = $notship[0];
					}
					else if($count1>1){
						$data->error_message = (object)array('text' => "Remove non-shippable products from cart or try another pin code.", 'color' => '#dd4b39');
					}
					if(empty($data->error_message)){
						if($count=1 && !empty($res)) {
							$data->error_message = $res[0][0];
							}
						else  if($count>1){ 
							$data->error_message = (object)array('text' => "Remove Out Of Stock products to proceed.", 'color' => '#dd4b39');
						}
					}
					if(empty($data->error_message)){
						$count2 = count($chkCod);
						if($count2=1 && !empty($chkCod)){
							$notcod = $this->check_cod($chkCod);
							//$data->error_message = $notcod[0];
							$data->error_message = (object)array('text' => "COD unavailable.Try another pincode OR proceed with Online Payment.", 'color' => '#dd4b39');
						}
						else  if($count2>1){
							$data->error_message = (object)array('text' => "COD not available. Proceed with online payment?", 'color' => '#dd4b39');
						}
					}
					// if(empty($data->error_message))
						// $data->error_message =array('text' => "sucess", 'color' => '#00a65a');
					if((!empty($chkShipping) && empty($data->error_message)) || $count>0) $data->is_checkout_allowed = '0'; else $data->is_checkout_allowed = '1';
					if($coupon_code == 'NOCASH500' || $coupon_code == 'nocash500' || $coupon_code == 'NOCASH1000' || $coupon_code == 'nocash1000'){
						$data->error_message = (object)array('text' => "COD unavailable. Try another coupon code OR proceed with Online Payment.", 'color' => '#dd4b39');
					}
					if($count2 >= 1 && !empty($chkCod)){
						$data->payment_methods = array('online');
					}else{
						$data->payment_methods = array('online', 'COD');
					}
					
					if($pincode != '' && empty($data->error_message)){ 
						$data->error_message = (object)array('text' => "Choose a payment option and place your order.", 'color' => '#00a65a');
					}
					// $brand_array = $this->brand_discount($userid);
					// $data->brand_discount = $brand_array;
					$data->total_summary = $this->total_summary($userid,$paymod,$coupon_code,$sc_credit,$is_scbox);
					$cart_object->data = $data;
					$cart_object->items = $this->product_object($userid,$pincode,$is_scbox);
					$cart_array[$i] = $cart_object;
					$i++;
				}
			}
		if(!empty($cart_array)) return $cart_array; else return '';
	}
	/*
	function brand_discount($userid){
		// $query = $this->db->query(' select d.user_id,  from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") order by a.`created_datetime` DESC');
		$query = $this->db->query(" select d.company_name,d.user_id,b.discount_percent,b.discount_type_id  from `cart_info` as a,discount_availed as b,`product_desc` as c,`brand_info` as d where a.`order_id` is null and a.`user_id`= $userid AND c.id = a.product_id AND c.brand_id = d.user_id AND b.brand_id = d.user_id AND b.discount_type_id = 1");
		$res = $query->result_array();
		// print_r($res);exit;
		if (!empty($res)) return $res; else return 0;
	}*/
	
	function check_cod($product_id){
//print_r($product_id);exit;
		if(!empty($product_id)){
		$product_id = $product_id[0]['id'];
		$query = $this->db->query("select name from product_desc where id = $product_id ");
		$res = $query->result_array();
		$name = $res[0]['name'];
		$message = array(array('text' => 'COD unavailable.Try another pincode OR proceed with Online Payment.', 'color' => '#dd4b39'));
		//print_r($message);
		return $message[0];
		}
	}
	
	function check_ship($product_id){
		if(!empty($product_id)){
		$product_id = $product_id[0]['id'];
		$query = $this->db->query("select name from product_desc where id = $product_id ");
		$res = $query->result_array();
		$name = $res[0]['name'];
		$message = array(array('text' => 'Remove non-shippable products from cart or try another pin code.', 'color' => '#dd4b39'));
		//print_r($message);
		return $message[0];
		}
	}
	
	function product_stock($userid){
		$query = $this->db->query('select a.`id`,d.min_del_days,d.max_del_days,a.`product_id`,a.`product_qty`,b.`name`,d.`company_name` as seller,CONCAT("https://www.stylecracker.com/sc_admin/assets/products/",b.image) as image_url,(a.`product_qty`*a.product_price) as price,c.`size_text` as slelected_size,a.`product_size`,a.SCUniqueID,IF(d.cod_available=1, "cod is available", "cod not available") as cod_available,@max_allowed_qty:="5" as max_allowed_qty,b.description,b.name from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") order by a.`created_datetime` DESC');
        $res = $query->result_array();
		/* echo $this->db->last_query();
		exit(); */
		//print_r( $res);exit;
		$product_array = array();
		$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){	
					$stock = $this->is_available($val['product_id']);
					//print_r($stock);exit;
					//echo $val['product_id'];exit;
					if($stock == 0){
						$product_object = new stdClass(); 
						$product_object = $val['product_id'];
						$message[$i] = array(array('text' => 'Remove Out Of Stock products to proceed.', 'color' => '#dd4b39'));
						//$product_array[$i] = $product_object;
						$i++;
					}
					
				}
			}
			
		if(!empty($message))return $message; else return '';
	}
	
	function product_object($userid,$pincode='',$is_scbox=0){
		//$query = $this->db->query('select a.`id`,d.min_del_days,d.max_del_days,a.`product_id`,a.`product_qty`,b.`name`,d.`company_name` as seller,CONCAT("https://www.stylecracker.com/sc_admin/assets/products/",b.image) as image_url,(a.`product_qty`*a.product_price) as price,c.`size_text` as selected_size,a.`product_size`,a.SCUniqueID,IF(d.cod_available=1, "COD is available", "COD not available") as cod_available,@max_allowed_qty:="5" as max_allowed_qty,b.description from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") order by a.`created_datetime` DESC');
		 
		//added for scbox products
		if($is_scbox == '1'){
			$scbox_product_condtion = ' a.`product_id` IN ('.SCBOX_PRODUCTID_STR.') ';  
		}else{
			$scbox_product_condtion = ' a.`product_id` NOT IN ('.SCBOX_PRODUCTID_STR.') ';  
		}
		//added for scbox products
		$query = $this->db->query('select a.`id`,d.user_id as brand_id,d.min_del_days,d.max_del_days,a.`product_id`,a.`product_qty`,b.`name`,d.`company_name` as seller,CONCAT("https://www.stylecracker.com/sc_admin/assets/products/",b.image) as image_url,(a.`product_qty`*a.product_price) as price,c.`size_text` as selected_size,a.`product_size`,a.SCUniqueID,d.cod_available as cod_available,@max_allowed_qty:="5" as max_allowed_qty,b.description from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") AND '.$scbox_product_condtion.' order by a.`created_datetime` DESC');
        $res = $query->result_array();
		//print_r($res);exit;
		$product_array = array();
		$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
					$deliver_message = '';
					$days = 0;$color = '#00a65a';
					$product_object = new stdClass(); 
					$product_object->cart_id = $val['id'];
					$product_object->product_id = $val['product_id'];
					$product_object->product_qty = $val['product_qty'];
					$product_object->name = $val['name'];
					$product_object->seller = $val['seller'];
					$product_object->image_url = $val['image_url'];
					$product_object->price = $val['price'];
					$product_object->selected_size = $val['selected_size'];
					$product_object->max_allowed_qty = $val['max_allowed_qty'];
					$product_object->description = $val['description'];
					$product_object->cod_available = $val['cod_available'];
					$product_object->brand_id = $val['brand_id'];
					/*if(($val['min_del_days'] != '' && $val['max_del_days'] != '') || ($val['min_del_days'] != NULL && $val['max_del_days'] != NULL)) {
						$deliver_message = 'Delivery in '.$val['min_del_days'].'-'.$val['max_del_days'].' day(s)';
					}
					else if(($val['min_del_days'] != '') || ($val['min_del_days'] != NULL)){
						$deliver_message = 'Delivery in '.$val['min_del_days'].'  day(s)';
					}
					else if(($val['max_del_days'] != '') || ($val['max_del_days'] != NULL)){
						$deliver_message = 'Delivery in '.$val['max_del_days'].' day(s)';
					}*/
					if($val['min_del_days']>0 && $val['max_del_days']>0){
						$deliver_message = 'Delivery in '.$val['min_del_days'].'-'.$val['max_del_days'].' working days.';
					}else if(($val['min_del_days']>0 && $val['max_del_days']=='') || ($val['min_del_days']=='' && $val['max_del_days']>0)){
						$deliver_message = 'Delivery in '.$val['min_del_days'].$val['max_del_days'].' working days.';
					}
					if($pincode == ''){
						if($val['cod_available'] == 1){
							$cod_message = 'COD is available';
						}else { 
						$cod_message = 'COD not available';
						$color = '#dd4b39';
						}
					}else {
						$cod = $this->check_product_cod($val['product_id'],$pincode);
						if(!empty($cod) || $val['cod_available'] != 1)
						{
							$cod_message = 'COD unavailable for this product at '.$pincode.'.';
							$color = '#dd4b39';
						}else 
							$cod_message = 'COD is available';
					}
					
					$is_available = $this->is_available($val['product_id']);
					
					if($is_available == 0) {$cod_message = 'Out of stock'; $deliver_message = ''; $color = '#dd4b39';}
					$message = array(array('text' => $cod_message, 'color' => $color),
								   array('text' => $deliver_message, 'color' => '#00a65a') );
					$product_object->message = $message;			   
					$product_object->is_available = (string)$is_available;
					$product_object->product_sizes = $this->product_sizes($val['product_id']);
					// $discount = $this->product_discount($val['product_id']);
					// $product_object->product_discount = $discount['discount_percent'];
					// $product_object->discount_price = (string)($val['price'] - ($val['price'] * ($discount['discount_percent']/100)));
					$brand_discount = $this->brand_discount($val['brand_id']);
					$product_discount = $this->product_discount($val['product_id']);
					if(!empty($brand_discount)){
						$product_object->discount_percent = $brand_discount['discount_percent'];
						$product_object->discount_price = (string)($val['price'] - ($val['price'] * ($brand_discount['discount_percent']/100)));
					}else if(!empty($product_discount)){
						$product_object->discount_percent = $product_discount['discount_percent'];
						$product_object->discount_price = (string)($val['price'] - ($val['price'] * ($product_discount['discount_percent']/100)));
					}else {
						$product_object->product_discount = '0';
						$product_object->discount_price = $val['price'];
					}
					$return_policy = '';
					$return_policy = $this->return_policy($val['product_id']);
					if(!empty($return_policy)) $product_object->return_policy = $return_policy['return_policy']; else $product_object->return_policy = '';

					$product_array[$i] = $product_object;
					$i++;
				}
			}
			//print_r($product_array);exit;
			return $product_array;
		
	}
	
	function return_policy($product_id){
		$query = $this->db->query("SELECT  b.return_policy FROM product_desc AS a, brand_info b WHERE a.id = $product_id AND a.brand_id = b.user_id ");
		$res = $query->result_array();
		if(!empty($res)) return $res[0]; else return null;
	}
	
	function check_product_cod($product_id,$pincode){
		
		$query = $this->db->query("SELECT  a.id FROM product_desc AS a, brand_info b WHERE a.id = $product_id AND a.brand_id = b.user_id AND  `cod_exc_location` LIKE '%$pincode%'");
		$res = $query->result_array();
		
		return $res;
	}
	
	function brand_discount($brand_id){
		$query = $this->db->query(" select IF(a.discount_percent is null, '0', a.discount_percent) as discount_percent FROM discount_availed AS a WHERE a.brand_id = $brand_id AND a.discount_type_id = 1 AND NOW() >= a.start_date AND NOW() <= a.end_date AND a.status=1 ");
		$res = $query->result_array();
		if (!empty($res)) return $res[0]; else return null;
	}
	
	function product_discount($product_id){
		$query = $this->db->query("SELECT  IF(sum(a.discount_percent) is null, '0', sum(a.discount_percent)) as discount_percent FROM discount_availed AS a WHERE a.product_id = $product_id AND a.discount_type_id = 2 AND NOW() >= a.start_date AND NOW() <= a.end_date AND a.status=1 ");
		$res = $query->result_array();
		if (!empty($res)) return $res[0]; else return null;
	}
	
	/*
	function product_discount($product_id){
		$query = $this->db->query("SELECT  IF(sum(a.discount_percent) is null, '0', sum(a.discount_percent)) as discount_percent FROM discount_availed AS a WHERE a.product_id = $product_id ");
		$res = $query->result_array();
		if (!empty($res)) return $res[0]; else return 0;
	}*/
	function product_sizes($product_id){
		$query = $this->db->query("SELECT b.size_text FROM product_inventory AS a, product_size b WHERE a.product_id = $product_id AND a.size_id = b.id");
		$res = $query->result_array();
		return $res;
	}
	
	function is_available($product_id){
		$query = $this->db->query("SELECT a.stock_count FROM product_inventory a WHERE a.product_id = $product_id AND a.stock_count > 0");
		$res = $query->result_array();
		// echo $res[0]['stock_count'];
		// print_r($res);exit;
		if($res != NULL && $res[0]['stock_count']>0)
			return 1;
		else return 0;
		//return $res;
	}
	
	function product_object2($userid,$pincode='',$is_scbox=0){
		//$query = $this->db->query('select a.`id`,d.min_del_days,d.max_del_days,a.`product_id`,a.`product_qty`,b.`name`,d.`company_name` as seller,CONCAT("https://www.stylecracker.com/sc_admin/assets/products/",b.image) as image_url,(a.`product_qty`*a.product_price) as price,c.`size_text` as selected_size,a.`product_size`,a.SCUniqueID,IF(d.cod_available=1, "COD is available", "COD not available") as cod_available,@max_allowed_qty:="5" as max_allowed_qty,b.description from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") order by a.`created_datetime` DESC');
		 
		//added for scbox products
		if($is_scbox == '1'){
			$scbox_product_condtion = ' a.`product_id` IN ('.SCBOX_PRODUCTID_STR.') ';  
		}else{
			$scbox_product_condtion = ' a.`product_id` NOT IN ('.SCBOX_PRODUCTID_STR.') ';  
		}
		//added for scbox products
		$query = $this->db->query('select a.`id`,d.user_id as brand_id,d.min_del_days,d.max_del_days,a.`product_id`,a.`product_qty`,b.`name`,d.`company_name` as seller,CONCAT("https://www.stylecracker.com/sc_admin/assets/products/",b.image) as image_url,(a.`product_qty`*a.product_price) as price,c.`size_text` as selected_size,a.`product_size`,a.SCUniqueID,d.cod_available as cod_available,@max_allowed_qty:="5" as max_allowed_qty,b.description from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") AND '.$scbox_product_condtion.' order by d.user_id DESC');
        $res = $query->result_array();
		//print_r($res);exit;
		$product_array = array();
		$i = 0; 
			
			if(!empty($res)){
				foreach($res as $val){
					$deliver_message = '';
					$days = 0;
					$product_object = new stdClass(); 
					$product_object->cart_id = $val['id'];
					$product_object->product_id = $val['product_id'];
					$product_object->product_qty = $val['product_qty'];
					$product_object->name = $val['name'];
					$product_object->seller = $val['seller'];
					$product_object->image_url = $val['image_url'];
					$product_object->price = $val['price'];
					$product_object->selected_size = $val['selected_size'];
					$product_object->max_allowed_qty = $val['max_allowed_qty'];
					$product_object->description = $val['description'];
					$product_object->cod_available = $val['cod_available'];
					$product_object->brand_id = $val['brand_id'];
					if($val['min_del_days']>0 && $val['max_del_days']>0){
						$deliver_message = 'Delivery in '.$val['min_del_days'].'-'.$val['max_del_days'].' working days.';
					}else if(($val['min_del_days']>0 && $val['max_del_days']=='') || ($val['min_del_days']=='' && $val['max_del_days']>0)){
						$deliver_message = 'Delivery in '.$val['min_del_days'].$val['max_del_days'].' working days.';
					}
					if($pincode == ''){
						if($val['cod_available'] == 1){
							$cod_message = 'COD is available';
						}else { 
							$cod_message = 'COD not available';
						}
					}else {
						$cod = $this->check_product_cod($val['product_id'],$pincode);
						if(!empty($cod))
						{
							$cod_message = 'COD unavailable for this product at '.$pincode.'.';
						}else 
							$cod_message = 'COD is available';
					}
					$message = array(array('text' => $cod_message, 'color' => '#00a65a'),
								   array('text' => $deliver_message, 'color' => '#dd4b39') );
					$product_object->message = $message;			   
					$product_object->is_available = (string)$this->is_available($val['product_id']);
					$product_object->product_sizes = $this->product_sizes($val['product_id']);
					$brand_discount = $this->brand_discount($val['brand_id']);
					$product_discount = $this->product_discount($val['product_id']);
					if(!empty($brand_discount)){
						$product_object->discount_percent = $brand_discount['discount_percent'];
						$product_object->discount_price = (string)($val['price'] - ($val['price'] * ($brand_discount['discount_percent']/100)));
					}else if(!empty($product_discount)){
						$product_object->discount_percent = $product_discount['discount_percent'];
						$product_object->discount_price = (string)($val['price'] - ($val['price'] * ($product_discount['discount_percent']/100)));
					}else {
						$product_object->product_discount = '0';
						$product_object->discount_price = $val['price'];
					}
					$return_policy = '';
					$return_policy = $this->return_policy($val['product_id']);
					if(!empty($return_policy)) $product_object->return_policy = $return_policy['return_policy']; else $product_object->return_policy = '';

					$product_array[$i] = $product_object;
					$i++;
				}
			}
			//print_r($product_array);exit;
			return $product_array;
		
	}
	
	function total_summary($userid,$paymod='',$coupon_code,$sc_credit='',$is_scbox=0){
		 
		//added for scbox products
		if($is_scbox == '1'){
			$scbox_product_condtion = ' a.`product_id` IN ('.SCBOX_PRODUCTID_STR.') ';  
		}else{
			$scbox_product_condtion = ' a.`product_id` NOT IN ('.SCBOX_PRODUCTID_STR.') ';  
		}
		//added for scbox products
		$query = $this->db->query('select SUM(a.`product_qty`*a.product_price) as total,(SUM(a.`product_qty`*a.product_price)+SUM(d.store_tax)+SUM(d.`cod_charges`)) as grand_total,SUM(d.store_tax) as store_tax,SUM(d.shipping_charges) as shipping_charges,SUM(d.shipping_min_values) as shipping_min_values,SUM(d.shipping_max_values) as shipping_max_values,SUM(d.`cod_charges`) as cod_charges,SUM(d.`cod_min_value`) as cod_min_value,SUM(d.`code_max_value`) as code_max_value from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") AND '.$scbox_product_condtion.' order by a.`created_datetime` DESC LIMIT 0,1');
		$res = $query->result_array();
		/* echo "<pre>";
		print_r($res);
		echo "</pre>";
		exit(); */
		$cart_iteams = $this->product_object2($userid,$pincode=0,$is_scbox); 
		$dis_amount = 0;
		$total_product_price = 0;
		$brand_id = '';
		if(!empty($cart_iteams)){
			foreach($cart_iteams as $val){
				$total_product_price = $total_product_price + $val->price;
				$dis_amount = $dis_amount+$val->discount_price;
				$brand_id.=$val->brand_id.",";
			}
		}
		$brand_id = rtrim($brand_id, ",");
		$sc_credit_charges = 0;
		$referal_count = count($this->getuser_data($userid));
			if($dis_amount >= MIN_CART_VALUE && $referal_count > 0){
				$show_ref = 1;					
				}else $show_ref = 0;
						
		if($sc_credit == '1' && $show_ref == '1'){ $sc_credit_charges = POINT_VAlUE; } else $sc_credit_charges = 0;
		$cart_array = array();			
		$i = 0; 
		if(!empty($res)){
			foreach($res as $val){
				$cart_object = new stdClass(); 
				$cod = $this->total_cod($userid,$is_scbox);
				$shipping = $this->total_shipping_charges($userid,$is_scbox);
				$total_discount = $this->get_total_discount($cart_iteams);
				$stylecracker_discount = $this->get_stylecracker_discount($dis_amount);
				$cart_object->referal_count = (string)$referal_count;
				$cart_object->min_cart_value = (string)MIN_CART_VALUE;
				$cart_object->per_point_value = (string)POINT_VAlUE;
				$cart_object->total_discount = $total_discount;
				
				// coupon discount start 
				$get_coupon_discount = $this->get_coupon_discount($coupon_code,$cart_iteams);
				if($get_coupon_discount != 0){
				$cart_object->coupon_discount = (string)$get_coupon_discount;
				$brand_id = $this->getCouponData($coupon_code)['brand_id'];
				if($brand_id > 0){ $cart_object->coupon_brand = $this->getBrandExtraInfo($brand_id,'company_name')[0]['company_name'];} 
				else  $cart_object->coupon_brand = '';
				}else {
					$cart_object->coupon_discount = (string)$get_coupon_discount;
					$cart_object->coupon_brand = '';
				}
				// coupon discount end
				
				//$tax = $this->get_tax($userid,$get_coupon_discount,$is_scbox);
				$tax = $this->get_tax($userid,$get_coupon_discount,1);
				//$cart_object->taxes = $this->get_tax($userid,$get_coupon_discount,$is_scbox);
				$cart_object->taxes = $this->get_tax($userid,$get_coupon_discount,1);
				if($paymod == 'cod'){
				$cart_object->total_shipping_charges = $this->total_shipping_charges($userid,$is_scbox);
				$cart_object->total_cod = $this->total_cod($userid,$is_scbox);
				}else {
				$cart_object->total_shipping_charges = $this->total_shipping_charges($userid,$is_scbox);
				}
				$cart_object->stylecracker_discount_percent = (string)$stylecracker_discount;
				$cart_object->total = (string)$dis_amount;
				if($paymod == 'online'){
					$total_shipping_charges = $dis_amount + $shipping['shipping_charges'] + $tax['store_tax'];
					$cart_object->grand_total_old = (string)$total_shipping_charges;
					$discount_price = $dis_amount - ($dis_amount * ($stylecracker_discount/100));//echo $discount_price;exit;
					$cart_object->discount_price = (string)($dis_amount -$discount_price);
					$cart_object->grand_total = (string)(($dis_amount - ($dis_amount * ($stylecracker_discount/100))) + $shipping['shipping_charges'] + $tax['store_tax'] - $get_coupon_discount - $sc_credit_charges);
				}else{
					$total_cod_charges = $dis_amount + $shipping['shipping_charges'] + $cod['cod_charges'] + $tax['store_tax'];
					$discount_price = $dis_amount - ($dis_amount * ($stylecracker_discount/100));
					$cart_object->discount_price = (string)($total_cod_charges -$discount_price);
					$cart_object->grand_total_old = (string)$total_cod_charges;
					// added for discounted price 
					$cart_object->grand_total = (string)(($dis_amount - ($dis_amount * ($stylecracker_discount/100))) + $shipping['shipping_charges'] + $cod['cod_charges'] + $tax['store_tax'] - $get_coupon_discount - $sc_credit_charges);
				}
				
				$cart_array[$i] = $cart_object;
				$i++;
			}
		}
		
        return $cart_array[0];	
	}
	
	// get the value of coupon discount
	function get_coupon_discount($coupon_code,$cart_iteams){
		$coupon_brand = $this->getCouponData($coupon_code)['brand_id'];
		$coupon_min_spend = $this->getCouponData($coupon_code)['coupon_min_spend'];
		$coupon_max_spend = $this->getCouponData($coupon_code)['coupon_max_spend'];
		$coupon_products = $this->getCouponData($coupon_code)['coupon_products']; 
		$coupon_discount_type = $this->getCouponData($coupon_code)['coupon_discount_type'];
		// echo $coupon_discount_type;exit;
		$cart_iteams = (array)$cart_iteams;
		if($coupon_products!='' || $coupon_products!=0)
		{
		$coupon_products_arr = explode(',',$coupon_products);
		}
		$coupon_brandsSel = '';
		$coupon_productSel = '';
		$coupon_discount = 0;
		$coupon_charges_text = '';
		$coupon_product_price = 0;
		foreach($cart_iteams as $value){
			// print_r($value);exit;
			$new_brand_id = $value->brand_id;
			$new_brand_name = $value->seller;
			if($coupon_brand==$new_brand_id || $coupon_brand==0 )
			{        
			  if($coupon_products!=''&& $coupon_products!=0)
			  {
				/* coupon_discount_type =3 (Product discount)*/
				if($coupon_discount_type==3)
				{
				  if(in_array($value->product_id, $coupon_products_arr))
				  {              
					if($coupon_min_spend<=$value->price && $value->price<=$coupon_max_spend)
					{
					  $coupon_discount = $this->getCouponData($coupon_code,$value->brand_id)['coupon_amount'];
					  $coupon_charges_text =  $new_brand_name;
					} 
				  }
				} 
				/* coupon_discount_type =4 (Product % discount)*/
				if($coupon_discount_type==4)
				{
				  if(in_array($value->product_id, $coupon_products_arr))
				  {              
					if($coupon_min_spend<=$value->price && $value->price<=$coupon_max_spend)
					{
					  $coupon_percent = $this->getCouponData($coupon_code,$value->brand_id)['coupon_amount'];
					  $coupon_discount = $value->price*($coupon_percent/100);
					  $coupon_charges_text =  $new_brand_name;
					} 
				  }
				}                 

			  }else
			  { 
				/* coupon_discount_type =1 (cart discount)*/
				if($coupon_discount_type==1)
				{
				  $coupon_product_price = $coupon_product_price+$value->price;
				  if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
				  {
					$coupon_discount = $this->getCouponData($coupon_code,$value->brand_id)['coupon_amount'];
					$coupon_charges_text =  $new_brand_name;
				  }
				}
				/* coupon_discount_type =2 (cart % discount)*/
				if($coupon_discount_type==2)
				{
				  $coupon_product_price = $coupon_product_price+$value->price;
				  if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
				  {
					$coupon_percent = $this->getCouponData($coupon_code,$value->brand_id)['coupon_amount'];
					$coupon_discount = $coupon_product_price*($coupon_percent/100);
					$coupon_charges_text =  $new_brand_name;
				  }
				}
			  }          
			}//end of if
		}//end foreach
		return $coupon_discount;
	}
	
	function get_total_discount($cart_iteams){
		$discount_array = array();
		$dis_amount = 0;
		$product_discount = 0;
		$i = 0;
		if(!empty($cart_iteams)){
			foreach($cart_iteams as $val){
				$discount_object = new stdClass();
				$discount_object->name = $val->name;
				$discount_object->seller = $val->seller;
				$discount_object->brand_id = $val->brand_id;
				$discount_object->price = $val->price;
				$discount_object->discount_percent = $val->discount_percent;
				$discount_object->discount_price = $val->discount_price;
				$product_discount = $product_discount + $val->price - $val->discount_price;
				$dis_amount = $dis_amount+$val->discount_price;
				$discount_array['discount'][$i] = $discount_object;
				$i++;
			}
		}
		$stylecracker_discount = $this->get_stylecracker_discount($dis_amount);
		$total_discount_price = $dis_amount - ($dis_amount * ($stylecracker_discount/100));
		$discount_amount = ($dis_amount * ($stylecracker_discount/100)) + $product_discount;
		$discount_array2 = array();
		if($total_discount_price != $dis_amount){
			$discount_array2['stylecracker_discount'] = $stylecracker_discount;
			$discount_array2['discount_amount'] = (string)$discount_amount;
		}else $discount_array2['discount_amount'] = (string)$discount_amount;
		$data = array_merge($discount_array,$discount_array2);
		return $data;
	}
	
	function get_stylecracker_discount($max_amount){
		$query1 = $this->db->query("select da.discount_percent from discount_availed da where da.discount_type_id = 4 AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.max_amount <= $max_amount");
		$res = $query1->result_array();
		if(!empty($res)) return $res[0]['discount_percent']; else return 0;
	}	
	
	function get_tax($userid,$coupon_discount=0,$is_scbox=0){
		 
		//added for scbox products
		if($is_scbox == '1'){
			$scbox_product_condtion = ' a.`product_id` IN ('.SCBOX_PRODUCTID_STR.') ';  
		}else{
			$scbox_product_condtion = ' a.`product_id` NOT IN ('.SCBOX_PRODUCTID_STR.') ';  
		}
		//added for scbox products
		$query1 = $this->db->query('select b.id as product_id,d.`user_id` as brand_id,a.product_qty,d.store_tax as store_tax,a.product_price as price from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") AND '.$scbox_product_condtion.' ');
		$res1 = $query1->result_array();
		$i = 0;
		$tax_total = 0;
		$tax = 0;$product_data = array();
		if(!empty($res1)){
			foreach($res1 as $val){
				$product_data[$i]['discount_percent'] = $this->get_discount_percentage($val['brand_id'],$val['product_id']);
				$product_data[$i]['discount_price'] = (string)($val['product_qty']*($val['price']-($val['price']*$product_data[$i]['discount_percent'])/100));
				// added for scbox tax
				if($val['brand_id'] == SCBOX_BRAND_ID){
					$product_data[$i]['discount_price'] = $product_data[$i]['discount_price'] - $coupon_discount;	
				}
				// added for scbox tax
				$tax = $product_data[$i]['discount_price']*($val['store_tax']/100);
				$tax_total = $tax_total + $tax;
				$i++;
			}
		}
		$cart_object1 = new stdClass();
		if(!empty($res1)) $cart_object1->store_tax = (string)$tax_total; else $cart_object1->store_tax = 0;
		
		$query = $this->db->query('select d.`user_id` as brand_id,a.`product_id`,a.product_qty,a.product_price from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") AND '.$scbox_product_condtion.' GROUP BY d.`user_id` ');
		$res = $query->result();
		$cart_array = array();			
		if(!empty($res)){
			foreach($res as $val){
				//$cart_object = new stdClass();
				$brand_info = $this->brand_tax($val->brand_id,$val->product_id,$val->product_qty,$val->product_price,$coupon_discount);
				if(!empty($brand_info)){
				$cart_object = $brand_info;
				$cart_array[] = $cart_object;
				}
			}
		}
		$cart_object = new stdClass();
		$cart_object->brand_array = $cart_array;

		$data = array_merge((array)$cart_object1,(array)$cart_object);
		if(!empty($data)){ return $data; }else { return ''; }	
	}
	
	function brand_tax($brand_id,$product_id,$product_qty,$product_price,$coupon_discount){
		$query = $this->db->query("SELECT b.id as product_id,b.price,a.store_tax,a.user_id as brand_id,a.company_name as seller,(b.price*(a.store_tax/100)) as brand_tax FROM `brand_info` a,`product_desc` as b WHERE a.`user_id` = $brand_id AND b.id= $product_id AND b.brand_id = $brand_id AND `store_tax` != '' AND `store_tax` != 0 ");
		$res = $query->result_array();
		// print_r($res);exit;
		$product_data = array(); 
		$i = 0;
		$tax_array = array();
		
		if(!empty($res)){
		foreach($res as $val){
		$tax_object = new stdClass();
		$product_data[$i]['discount_percent'] = $this->get_discount_percentage($val['brand_id'],$val['product_id']);
		$product_data[$i]['discount_price'] = ($product_qty*$product_price)-($product_qty*$product_price*$product_data[$i]['discount_percent'])/100; 
		$tax_object->store_tax = $val['store_tax'];
		$tax_object->brand_id = $val['brand_id'];
		$tax_object->seller = $val['seller'];
		// added for scbox tax
		if($val['brand_id'] == SCBOX_BRAND_ID){
			$product_data[$i]['discount_price'] = $product_data[$i]['discount_price'] - $coupon_discount;	
		}
		// added for scbox tax
		$tax_object->brand_tax = (string)($product_data[$i]['discount_price']*($val['store_tax']/100));
		$tax_array[$i] = $tax_object; 
		$i++;
		}
		}
		//print_r($tax_array);exit;
		if(!empty($res)){ return $tax_array[0]; }else { return ''; }	
	}
	
	function total_shipping_charges($user_id,$is_scbox=0){
		
		//added for scbox products
		if($is_scbox == '1'){
			$scbox_product_condtion = ' a.`product_id` IN ('.SCBOX_PRODUCTID_STR.') ';  
		}else{
			$scbox_product_condtion = ' a.`product_id` NOT IN ('.SCBOX_PRODUCTID_STR.') ';  
		}
		//added for scbox products
		
		$productPrice = 0; $final_cod_charges =0;
        $order_list = array(); $product_total = 0;
        $shipping_charges = 0;
		$old_store_shipping_charges = 0;
		$final_shipping_charges = 0;
		$cart_data = $this->get_cart2($user_id);
		//print_r($cart_info);exit;
		if(!empty($cart_data)){
			 $new_brand = ''; $old_brand = ''; $new_store_tax = 0; $old_store_tax = 0; $new_store_cod_charges = 0; $old_store_cod_charges = 0;
            $brand_total = 0; $order_info = '';
			$i = 0;
		 foreach($cart_data as $val){
                $new_brand = $val['user_id'];
                $new_store_tax = $val['store_tax'];

                $new_store_cod = $val['is_cod'];
                $new_store_cod_charges = $val['cod_charges'];

                if($new_brand != $old_brand){ 
                    
                    if($brand_total!=0){ 
                        $tax = $brand_total*($old_store_tax/100); 
                        $order_amount = $brand_total+($brand_total*($old_store_tax/100)); 
						//$paymod == 'cod'
                      /* COD CALCULAION START */
                        if($this->getBrandExtraInfo($old_brand,'is_cod')[0]['is_cod'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0){
                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ; 
                                $order_amount = $order_amount + $final_cod_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] >= 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] > 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']<=$product_total && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']>=$product_total) {


                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                                $order_amount = $order_amount + $final_cod_charges;
                            }else { $old_store_cod_charges = 0; }
                        }else{ $old_store_cod_charges = 0; }
                        /* COD CALCULAION END */

                        /* SHIPPING CHARGES START*/
                        if($this->getBrandExtraInfo($old_brand,'is_shipping')[0]['is_shipping'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] == 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] == 0){


                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] >= 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] > 0 && $this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values']<=$product_total && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values']>=$product_total) {

                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;

                            }else { $old_store_shipping_charges = 0; }
                            }else{ $old_store_shipping_charges = 0; }
                        /* SHIPPING CHARGES END*/
						$shipping_charges = $final_shipping_charges;
                        $order_info = array(
                                'user_id' => $user_id,
                                'brand_id' => $old_brand,
                                'order_tax_amount' => $tax,
                                'order_sub_total' => $order_amount,
                                'order_total' => $order_amount,
                                // 'pay_mode' => $data['paymod']=='cod' ? '1' ,
                                // 'created_datetime' =>date('Y-m-d H:i:s'),
                                // 'first_name' => $data['uc_first_name'],
                                //'last_name' => $data['uc_last_name'],
                                // 'email_id' => $data['uc_email_id'],
                                // 'mobile_no' => $data['uc_mobile'],
                                // 'shipping_address' => $data['uc_address'],
                                // 'pincode' => $data['uc_pincode'],
                                // 'city_name' => $data['uc_city'],
                                // 'state_name' => $data['uc_state'],
                                // 'mihpayid' => $data['mihpayid'],
                                // 'txnid' => $data['txnid'],
                                'order_status' => '1',
                                'cod_amount' => $final_cod_charges,
                                'shipping_amount' => $final_shipping_charges,
                            );
                        //$order_id = $this->sc_place_order($order_info); $product_total =0;
                        //$order_list[] = $order_id; 

                    
                        $product_info = [];
                    }
                    //$brand_total = 0;
                    $old_brand = $new_brand;
                    $old_store_tax = $new_store_tax;
                    //$brand_total = $brand_total+$val['price'];

                    $product_info[$i]['product_id'] = $val['product_id'];
                    $product_info[$i]['user_id'] = $user_id;
                    $product_info[$i]['created_datetime'] = date('Y-m-d H:i:s');
                    $product_info[$i]['product_size'] = $val['product_size'];
                    $product_info[$i]['product_qty'] = $val['product_qty'];
                    $product_info[$i]['price'] = $val['price'];
                    $product_info[$i]['cart_id'] = $val['id'];
				
                    $product_total = $product_total+$val['price'];
                    $brand_total = $product_total;
					
                }else{
					
                    //$brand_total = $brand_total+$productPrice;
                    $old_brand = $new_brand;
                    $old_store_tax = $new_store_tax;

                    $product_info[$i]['product_id'] = $val['product_id'];
                    $product_info[$i]['user_id'] = $user_id;
                    $product_info[$i]['created_datetime'] = date('Y-m-d H:i:s');
                    $product_info[$i]['product_size'] = $val['product_size'];
                    $product_info[$i]['product_qty'] = $val['product_qty'];
                    $product_info[$i]['price'] = $val['price'];
                    $product_info[$i]['cart_id'] = $val['id'];
                    $product_total = $product_total+$val['price'];
                    $brand_total = $product_total;
					
                }
                $i++;
            }
			if($brand_total!=0){ 
                $tax = $brand_total*($old_store_tax/100); 
                $order_amount = $brand_total+($brand_total*($old_store_tax/100)); 

                /* COD CALCULAION START */
						
                        if($this->getBrandExtraInfo($old_brand,'is_cod')[0]['is_cod'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0){
                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ; 
                                $order_amount = $order_amount + $final_cod_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] >= 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] > 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']<=$product_total && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']>=$product_total) {


                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                                $order_amount = $order_amount + $final_cod_charges;
                            }else { $old_store_cod_charges = 0; }
                        }else{ $old_store_cod_charges = 0; }
                        /* COD CALCULAION END */

                                 /* SHIPPING CHARGES START*/
                        if($this->getBrandExtraInfo($old_brand,'is_shipping')[0]['is_shipping'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] == 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] == 0){


                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] >= 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] > 0 && $this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values']<=$product_total && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values']>=$product_total) {

                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;

                            }else { $old_store_shipping_charges = 0; }
                            }else{ $old_store_shipping_charges = 0; }
                        /* SHIPPING CHARGES END*/
						$shipping_charges = $final_shipping_charges;
                        $product_info = [];

                        

            }
		}
		$cart_object1 = new stdClass();
		//if($shipping_charges != 0) $cart_object1->shipping_charges = (string)$shipping_charges; else $cart_object1->shipping_charges = '0';
		
		$query = $this->db->query('select d.`user_id` as brand_id from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$user_id.'")  AND '.$scbox_product_condtion.' GROUP BY d.`user_id` ');
		
		$res = $query->result();
		$cart_array = array();			
		$mobile_final_shipping_charges = 0;
		if(!empty($res)){ 
			foreach($res as $val){
				$cart_object = new stdClass();
				$brand_info = $this->brand_shipping_charges($val->brand_id,$user_id); 
				if(!empty($brand_info)){
				//$cart_object = $brand_info;
				$cart_object->seller = $brand_info->seller;
				$cart_object->brand_id = $brand_info->brand_id; 
				if($brand_info->price > $brand_info->shipping_min_values && $brand_info->price < $brand_info->shipping_max_values){
					$cart_object->shipping_charges = (string)$brand_info->shipping_charges;
				}else if($brand_info->shipping_charges > 0 && $brand_info->shipping_min_values==0 && $brand_info->shipping_max_values==0){
					$cart_object->shipping_charges = (string)$brand_info->shipping_charges;
				}else{
					$cart_object->shipping_charges = (string)0;
				}
				$mobile_final_shipping_charges = $mobile_final_shipping_charges + $cart_object->shipping_charges;
				$cart_array[] = $cart_object;
				}
				
			}
		}
		if($mobile_final_shipping_charges != 0) $cart_object1->shipping_charges = (string)$mobile_final_shipping_charges; else $cart_object1->shipping_charges = '0';
		$cart_object = new stdClass();
		$cart_object->brand_array = $cart_array;
		$data = array_merge((array)$cart_object1,(array)$cart_object);
		if(!empty($data)){ return $data; }else { return ''; }	
	}
	
	function brand_shipping_charges($brand_id,$user_id){
		//$query = $this->db->query("SELECT shipping_charges,user_id as brand_id,company_name as seller FROM `brand_info` WHERE `user_id` = $brand_id");
		$query = $this->db->query("select SUM(b.price) as price,d.shipping_max_values,d.shipping_min_values,d.`company_name` as seller,d.`user_id` as brand_id ,d.shipping_charges as shipping_charges from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` AND d.`user_id` = $brand_id AND (a.`user_id`=$user_id) group by b.`brand_id` ");
		$res = $query->result();
		if(!empty($res)){ return $res[0]; }else { return ''; }	
	}
	
	function total_cod($user_id,$is_scbox=0){
		//added for scbox products
		if($is_scbox == '1'){
			$scbox_product_condtion = ' a.`product_id` IN ('.SCBOX_PRODUCTID_STR.') ';  
		}else{
			$scbox_product_condtion = ' a.`product_id` NOT IN ('.SCBOX_PRODUCTID_STR.') ';  
		}
		//added for scbox products
		
		$productPrice = 0; $final_cod_charges =0;
        $order_list = array(); $product_total = 0;
        $shipping_charges = 0;
		$sc_cod_charges = 0;
		$old_store_shipping_charges = 0;
		$final_shipping_charges = 0;
		$cart_data = $this->get_cart2($user_id);
		//print_r($cart_info);exit;
		if(!empty($cart_data)){
			 $new_brand = ''; $old_brand = ''; $new_store_tax = 0; $old_store_tax = 0; $new_store_cod_charges = 0; $old_store_cod_charges = 0;
            $brand_total = 0; $order_info = '';
			$i = 0;
		 foreach($cart_data as $val){
                $new_brand = $val['user_id'];
                $new_store_tax = $val['store_tax'];

                $new_store_cod = $val['is_cod'];
                $new_store_cod_charges = $val['cod_charges'];

                if($new_brand != $old_brand){ 
                    
                    if($brand_total!=0){ 
                        $tax = $brand_total*($old_store_tax/100); 
                        $order_amount = $brand_total+($brand_total*($old_store_tax/100)); 
						//$paymod == 'cod'
                      /* COD CALCULAION START */
                        if($this->getBrandExtraInfo($old_brand,'is_cod')[0]['is_cod'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0){
                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ; 
                                $order_amount = $order_amount + $final_cod_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] >= 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] > 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']<=$product_total && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']>=$product_total) {


                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                                $order_amount = $order_amount + $final_cod_charges;
                            }else { $old_store_cod_charges = 0; }
                        }else{ $old_store_cod_charges = 0; }
                        /* COD CALCULAION END */
						$sc_cod_charges =  $final_cod_charges;
                        /* SHIPPING CHARGES START*/
                        if($this->getBrandExtraInfo($old_brand,'is_shipping')[0]['is_shipping'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] == 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] == 0){


                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] >= 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] > 0 && $this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values']<=$product_total && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values']>=$product_total) {

                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;

                            }else { $old_store_shipping_charges = 0; }
                            }else{ $old_store_shipping_charges = 0; }
                        /* SHIPPING CHARGES END*/
						$shipping_charges = $final_shipping_charges;
                        $order_info = array(
                                'user_id' => $user_id,
                                'brand_id' => $old_brand,
                                'order_tax_amount' => $tax,
                                'order_sub_total' => $order_amount,
                                'order_total' => $order_amount,
                                // 'pay_mode' => $data['paymod']=='cod' ? '1' ,
                                // 'created_datetime' =>date('Y-m-d H:i:s'),
                                // 'first_name' => $data['uc_first_name'],
                                //'last_name' => $data['uc_last_name'],
                                // 'email_id' => $data['uc_email_id'],
                                // 'mobile_no' => $data['uc_mobile'],
                                // 'shipping_address' => $data['uc_address'],
                                // 'pincode' => $data['uc_pincode'],
                                // 'city_name' => $data['uc_city'],
                                // 'state_name' => $data['uc_state'],
                                // 'mihpayid' => $data['mihpayid'],
                                // 'txnid' => $data['txnid'],
                                'order_status' => '1',
                                'cod_amount' => $final_cod_charges,
                                'shipping_amount' => $final_shipping_charges,
                            );
                        //$order_id = $this->sc_place_order($order_info); $product_total =0;
                        //$order_list[] = $order_id; 

                    
                        $product_info = [];
                    }
                    //$brand_total = 0;
                    $old_brand = $new_brand;
                    $old_store_tax = $new_store_tax;
                    //$brand_total = $brand_total+$val['price'];

                    $product_info[$i]['product_id'] = $val['product_id'];
                    $product_info[$i]['user_id'] = $user_id;
                    $product_info[$i]['created_datetime'] = date('Y-m-d H:i:s');
                    $product_info[$i]['product_size'] = $val['product_size'];
                    $product_info[$i]['product_qty'] = $val['product_qty'];
                    $product_info[$i]['price'] = $val['price'];
                    $product_info[$i]['cart_id'] = $val['id'];
				
                    $product_total = $product_total+$val['price'];
                    $brand_total = $product_total;
					
                }else{
					
                    //$brand_total = $brand_total+$productPrice;
                    $old_brand = $new_brand;
                    $old_store_tax = $new_store_tax;

                    $product_info[$i]['product_id'] = $val['product_id'];
                    $product_info[$i]['user_id'] = $user_id;
                    $product_info[$i]['created_datetime'] = date('Y-m-d H:i:s');
                    $product_info[$i]['product_size'] = $val['product_size'];
                    $product_info[$i]['product_qty'] = $val['product_qty'];
                    $product_info[$i]['price'] = $val['price'];
                    $product_info[$i]['cart_id'] = $val['id'];
                    $product_total = $product_total+$val['price'];
                    $brand_total = $product_total;
					
                }
                $i++;
            }
			if($brand_total!=0){ 
                $tax = $brand_total*($old_store_tax/100); 
                $order_amount = $brand_total+($brand_total*($old_store_tax/100)); 

                /* COD CALCULAION START */
						
                        if($this->getBrandExtraInfo($old_brand,'is_cod')[0]['is_cod'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0){
                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ; 
                                $order_amount = $order_amount + $final_cod_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] >= 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] > 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']<=$product_total && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']>=$product_total) {


                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                                $order_amount = $order_amount + $final_cod_charges;
                            }else { $old_store_cod_charges = 0; }
                        }else{ $old_store_cod_charges = 0; }
                        /* COD CALCULAION END */
						$sc_cod_charges =  $final_cod_charges;
                                 /* SHIPPING CHARGES START*/
                        if($this->getBrandExtraInfo($old_brand,'is_shipping')[0]['is_shipping'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] == 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] == 0){


                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] >= 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] > 0 && $this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values']<=$product_total && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values']>=$product_total) {

                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;

                            }else { $old_store_shipping_charges = 0; }
                            }else{ $old_store_shipping_charges = 0; }
                        /* SHIPPING CHARGES END*/
						$shipping_charges = $final_shipping_charges;
                        $product_info = [];

                        

            }
		}
		$cart_object1 = new stdClass();
		//if($sc_cod_charges != 0) $cart_object1->cod_charges = (string)$sc_cod_charges; else $cart_object1->cod_charges = '0';
		
		$query = $this->db->query('select d.`user_id` as brand_id from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$user_id.'") AND '.$scbox_product_condtion.' GROUP BY d.`user_id` ');		
		$res = $query->result();
		$cart_array = array();	
		$mobile_final_cod_charges = 0;		
		if(!empty($res)){
			foreach($res as $val){
				$cart_object = new stdClass();
				$brand_info = $this->brand_total_cod($val->brand_id,$user_id);
				//print_r($brand_info);exit;
				if(!empty($brand_info)){
				// $cart_object = $brand_info;
				// $cart_array[] = $cart_object;
				$cart_object->seller = $brand_info->seller;
				$cart_object->brand_id = $brand_info->brand_id; 
				if($brand_info->price > $brand_info->cod_min_value && $brand_info->price < $brand_info->code_max_value){
					$cart_object->cod_charges = (string)$brand_info->cod_charges;
				}else if($brand_info->cod_charges > 0 && $brand_info->cod_min_value==0 && $brand_info->code_max_value==0){
					$cart_object->cod_charges = (string)$brand_info->cod_charges;
				}else{
					$cart_object->cod_charges = (string)0;
				}
				$mobile_final_cod_charges = $mobile_final_cod_charges + $cart_object->cod_charges;
				$cart_array[] = $cart_object;
				}
			}
		}
		if($mobile_final_cod_charges != 0) $cart_object1->cod_charges = (string)$mobile_final_cod_charges; else $cart_object1->cod_charges = '0';
		$cart_object = new stdClass();
		$cart_object->brand_array = $cart_array;
		$data = array_merge((array)$cart_object1,(array)$cart_object);
		if(!empty($data)){ return $data; }else { return ''; }	
	}
	
	function brand_total_cod($brand_id,$user_id){
		//$query = $this->db->query("SELECT a.cod_charges,a.user_id as brand_id,a.company_name as seller FROM `brand_info` a,cart_info b,product_desc c WHERE a.`user_id` = $brand_id AND (b.`product_qty`*c.price) < a.code_max_value AND b.product_id = c.id AND c.`brand_id` = a.`user_id` AND b.user_id =$user_id ");
		//$query = $this->db->query("select d.`company_name` as seller,d.`user_id` as brand_id ,d.cod_charges as cod_charges from `cart_info` as a,`product_desc` as b,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and b.`brand_id` = d.`user_id` AND d.`user_id` = $brand_id AND ((a.`product_qty`*a.product_price) < d.code_max_value) and (a.`user_id`=$user_id) order by b.`brand_id` ");
		$query = $this->db->query("select SUM(b.price) as price,d.code_max_value,d.cod_min_value,d.`company_name` as seller,d.`user_id` as brand_id ,d.cod_charges as cod_charges from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` AND d.`user_id` = $brand_id AND (a.`user_id`=$user_id) group by b.`brand_id` ");
		$res = $query->result();
		//print_r($res);exit;
		if(!empty($res)){ return $res[0]; }else { return ''; }	
	}
	
    function deleteCartProduct($product_id,$userid,$cart_id,$paymod,$pincode='',$sc_credit,$coupon_code){
        $query = $this->db->query("delete from `cart_info` where `product_id`='".$product_id."' AND `user_id` = '".$userid."' AND `id` = '".$cart_id."'");
		$query = $this->db->query('select a.`id`,count(a.`product_id`) as item_count,a.`product_id`,SUM(a.`product_qty`) as item_qty,a.`product_qty`,b.`name`,(a.`product_qty`*a.product_price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,a.SCUniqueID from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`product_id` = "'.$product_id.'" OR a.`user_id`="'.$userid.'") order by a.`created_datetime` DESC');
		$res = $query->result_array(); 
		$cart_object = $this->get_cart($userid,$paymod,$pincode,$chkShipping='',$chkCod='',$city='',$state='',$coupon_code,$sc_credit);
		return $cart_object;       
        
    }

    function getBrandUsername($brand_id){
		//echo $brand_id;exit;
        $query = $this->db->get_where('user_login', array('id' => $brand_id));
        $res = $query->result_array();
        if(!empty($res)){
            return $res[0]['user_name'];
        }else{
            return '';
        }
    }

    function get_all_states(){
        $query = $this->db->get('states');
        $res = $query->result_array();
        return $res;
    }

    function get_city_name($pincode){
		$query = $this->db->query("select TRIM(a.`city`) as city,TRIM(b.`state_name`) as state from `city` as a,`states` as b,pincode as c where a.`state_id` = b.`id` AND a.id = c.city_id AND  c.`pincode`='".$pincode."'");
        $res = $query->result_array();
		if(!empty($res)) return $res[0]; else return '';
    }

    function productQtyUpdate($product_id,$productQty,$userid,$cart_id,$paymod,$pincode,$sc_credit='',$coupon_code){
        if($product_id!='' && $productQty!='' && $userid!=''){
           
		$data = array('product_qty' => $productQty);
		$this->db->where(array('product_id' => $product_id,'user_id'=>$userid,'id'=>$cart_id));
		$this->db->update('cart_info', $data);
        $query = $this->db->query('select a.`id`,count(a.`product_id`) as item_count,a.`product_id`,SUM(a.`product_qty`) as item_qty,a.`product_qty`,b.`name`,(a.`product_qty`*a.product_price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,a.SCUniqueID from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") order by a.`created_datetime` DESC');
		$res = $query->result_array();
		$cart_object = $this->get_cart($userid,$paymod,$pincode,$chkShipping='',$chkCod='',$city='',$state='',$coupon_code,$sc_credit);
		return $cart_object;       
        }
    }
	
	function productSizeUpdate($product_id,$size,$userid,$cart_id){
		$productsize = $this->productsize($size);
        if($product_id!='' && $productsize!='' && $userid!=''){
           
		$data = array('product_size' => $productsize);
		$this->db->where(array('product_id' => $product_id,'user_id'=>$userid,'id'=>$cart_id));
		$this->db->update('cart_info', $data);		
		$query = $this->db->query('select a.`id`,count(a.`product_id`) as item_count,a.`product_id`,SUM(a.`product_qty`) as item_qty,a.`product_qty`,b.`name`,(a.`product_qty`*a.product_price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,a.SCUniqueID from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`product_id` = "'.$product_id.'" OR a.`user_id`="'.$userid.'") order by a.`created_datetime` DESC');
		$res = $query->result_array();
		$cart_object = $this->get_cart($userid);
		return $cart_object;     
        
        }
    }

    function getBrandExtraInfo($brand_id){
        $query = $this->db->get_where('brand_info', array('user_id' => $brand_id));
        $res = $query->result_array();
        if(!empty($res)){
            return $res;
        }else{
            return '';
        }
    }

    function get_cod_cart_info($uniquecookie,$pincode){
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }
        $query = $this->db->query('select distinct a.id,b.`cod_exc_location` from `cart_info` as a,`brand_info` as b,`product_desc` as c where a.`product_id` = c.`id` and b.`user_id` = c.`brand_id` and a.`user_id` ="'.$user_id.'" and a.`SCUniqueID`="'.$uniquecookie.'" and a.`order_id` is null and b.`cod_exc_location` like "%'.$pincode.'%"');
        $res = $query->result_array();
        return $res;
    }

    function get_not_ship_cart_info($uniquecookie,$pincode){
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }
        $query = $this->db->query('select distinct a.id,b.`shipping_exc_location` from `cart_info` as a,`brand_info` as b,`product_desc` as c where a.`product_id` = c.`id` and b.`user_id` = c.`brand_id` and a.`user_id` ="'.$user_id.'" and a.`SCUniqueID`="'.$uniquecookie.'" and a.`order_id` is null and b.`shipping_exc_location` like "%'.$pincode.'%"');
        $res = $query->result_array();
        return $res;
    }

	 function update_order_cart_info($cart_id,$order_id,$user_id){
        if($cart_id!='' && $order_id!=''){
            $data = array(
               'order_id' => $order_id,
               'user_id' => $user_id
            );

            $this->db->where('id', $cart_id);
            $this->db->update('cart_info', $data); 
        }
    }

	 function update_order_cart_info_online_order($user_id,$order_id){

	 	$order_ids = explode('_',$order_id);
		
		if(!empty($order_ids)){ 
			$order_id_count = count($order_ids); 
			for($i = 1; $i<$order_id_count; $i++){

				$order_id =$order_ids[$i];

				$query = $this->db->query("SELECT cart_id FROM `order_product_info` WHERE order_id ='".$order_id."'");
       	 		$res = $query->result_array();
				//echo $this->db->last_query(); exit;
				//$cart_id_count = count($res);
				if(!empty($res)){
					foreach($res as $val){
						if($val['cart_id']>0 && $order_id>0){
						$data = array(
			               'order_id' => $order_id
			            );
						$this->db->where(array('id' => $val['cart_id']));

			            $this->db->update('cart_info', $data); 
						}
					}
				}
				/*for($i = 0; $i<$cart_id_count; $i++){
					echo $res[$i]['cart_id'].'<br>';
					 if($order_id!='' && !empty($res) && $res[$i]['cart_id']!=''){
			            $data = array(
			               'order_id' => $order_id
			            );

			            $this->db->where(array('id' => $res[$i]['cart_id']));

			            $this->db->update('cart_info', $data); 
			           // echo $this->db->last_query();exit;
			        }
				}*/
			}
		}

       
    }
	/*
	function get_cart2($userid){
       $query = $this->db->query('select a.`id`,a.`product_id`,a.`product_qty`,b.`name`,(a.`product_qty`*a.product_price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size` from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") order by b.`brand_id`');
        $res = $query->result_array();
        return $res;
    }*/
	
	function get_cart2($userid,$is_scbox=0){
		 
		//added for scbox products
		if($is_scbox == '1'){
			$scbox_product_condtion = ' a.`product_id` IN ('.SCBOX_PRODUCTID_STR.') ';  
		}else{
			$scbox_product_condtion = ' a.`product_id` NOT IN ('.SCBOX_PRODUCTID_STR.') ';  
		}
		//added for scbox products
       $query = $this->db->query('select a.`id`,a.`product_id`,d.`return_policy`,a.`product_qty`,b.`name`,(a.`product_qty`*a.product_price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,b.`id` as pid,a.product_price as product_price,CASE 
        WHEN  DATE_FORMAT(b.discount_start_date,"%Y-%m-%d  %h:%i") <= DATE_FORMAT(now(),"%Y-%m-%d %h:%i") and DATE_FORMAT(b.discount_end_date,"%Y-%m-%d  %h:%i") >= DATE_FORMAT(now(),"%Y-%m-%d %h:%i") then b.compare_price
        ELSE 0
    END AS compare_price from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`user_id`="'.$userid.'") AND '.$scbox_product_condtion.' order by b.`brand_id`, a.`created_datetime` DESC LIMIT 1');

      /* $query = $this->db->query('select a.`id`,a.`product_id`,a.`product_qty`,b.`name`,(a.`product_qty`*a.product_price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,(select a.`product_qty`*(b.price-((b.price*discount_percent)/100)) from discount_availed as da where da.brand_id = b.brand_id AND NOW() >= da.start_date AND NOW() <= da.end_date AND (da.discount_type_id=1 ) AND da.status = 1 AND da.is_delete=0 )as discount_price,(select  discount_percent from discount_availed as da where da.brand_id = b.brand_id AND NOW() >= da.start_date AND NOW() <= da.end_date AND (da.discount_type_id=1 ) AND da.status = 1 AND da.is_delete=0 )as discount_percent,(select  id as discountAvailId from discount_availed as da where da.brand_id = b.brand_id AND NOW() >= da.start_date AND NOW() <= da.end_date AND (da.discount_type_id=1 ) AND da.status = 1 AND da.is_delete=0 )as discount_id from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`SCUniqueID` = "'.$uniquecookie.'") order by b.`brand_id`');
      */
       /*echo $this->db->last_query();exit;*/

        $res = $query->result_array();
        $product_data = array();
            if(!empty($res)){ $i = 0;
                foreach ($res as $key => $value) {
                    $product_data[$i]['id'] = $value['id'];
                    $product_data[$i]['product_id'] = $value['product_id'];
                    $product_data[$i]['product_qty'] = $value['product_qty'];
                    $product_data[$i]['return_policy'] = $value['return_policy'];
                    $product_data[$i]['name'] =$value['name'];
                    $product_data[$i]['price'] = $value['price']; 
                    $product_data[$i]['image'] =$value['image'];
                    $product_data[$i]['size_text'] =$value['size_text'];   
                    $product_data[$i]['company_name'] =$value['company_name'];                 
                    $product_data[$i]['user_id'] =$value['user_id'];
                    $product_data[$i]['min_del_days'] =$value['min_del_days'];
                    $product_data[$i]['max_del_days'] =$value['max_del_days'];
                    $product_data[$i]['store_tax'] =$value['store_tax'];
                    $product_data[$i]['is_shipping'] =$value['is_shipping'];
                    $product_data[$i]['shipping_charges'] =$value['shipping_charges'];
                    $product_data[$i]['shipping_min_values'] =$value['shipping_min_values'];
                    $product_data[$i]['shipping_max_values'] =$value['shipping_max_values'];
                    $product_data[$i]['is_cod'] =$value['is_cod'];
                    $product_data[$i]['cod_charges'] =$value['cod_charges'];
                    $product_data[$i]['cod_min_value'] =$value['cod_min_value'];
                    $product_data[$i]['code_max_value'] =$value['code_max_value'];
                    $product_data[$i]['product_size'] =$value['product_size'];
					$product_data[$i]['compare_price'] =$value['compare_price'];
                    $product_data[$i]['discount_percent'] = $this->get_discount_percentage($value['user_id'],$value['pid']);
                    $product_data[$i]['discount_price'] =$value['product_qty']*($value['product_price']-($value['product_price']*$product_data[$i]['discount_percent'])/100); 
                    $product_data[$i]['discount_id'] =  $this->get_discount_id($value['user_id'],$value['pid'])['id'];              
					if($this->get_discount_id('0','0')['discount_type_id'] == 4)
					{
						$product_data[$i]['sc_discount_type'] = 4;
						$product_data[$i]['sc_discount_percent'] = $this->get_discount_id('0','0')['discount_percent'];
						$product_data[$i]['sc_discount_max_amount'] = $this->get_discount_id('0','0')['max_amount'];
						$product_data[$i]['sc_discount_id'] = $this->get_discount_id('0','0')['id'];
					}else
					{
					   $product_data[$i]['sc_discount_type'] = 0;
					   $product_data[$i]['sc_discount_percent'] = 0;
					   $product_data[$i]['sc_discount_max_amount'] = 0;
					   $product_data[$i]['sc_discount_id'] = 0;
					}            
                    
                    $i++;
                }
            }
        return $product_data;
    }
	 
	 function sc_uc_order2($data){
        $this->load->model('Schome_model');
        $user_id = $data['user_id'];
		$order_display_no = 'SC'.time();
		if($data['paymod']=='cod'){
			$order_status = 1;
		}
		else $order_status = 9;
		
		if($data['paymod'] == 'cod'){
			$payent_method = '1';
		}else $payent_method = '2';
		$sc_discount_applied = '';
		$sc_discount_id = '';
        $productPrice = 0; $final_cod_charges =0;
        $order_list = array(); $product_total = 0;
		$new_store_shipping_charges = 0;
		$old_store_shipping_charges = 0;
		$final_shipping_charges = 0;
		//added for invite refer
		if($data['sc_credit'] == '1'){ $sc_credit_charges = 0; } else $sc_credit_charges = 0;
		// added for coupon code
		$coupon_code = $data['coupon_code'];
        $coupon_discount = 0;
        $coupon_products = $this->getCouponData($coupon_code)['coupon_products']; 
        $coupon_min_spend = $this->getCouponData($coupon_code)['coupon_min_spend'];
        $coupon_max_spend = $this->getCouponData($coupon_code)['coupon_max_spend'];
        $coupon_discount_type = $this->getCouponData($coupon_code)['coupon_discount_type'];
        $appliedcoupon_code = '';

		if($coupon_products!='' || $coupon_products!=0)
		{
		$coupon_products_arr =explode(',',$coupon_products);
		}
		// added for coupon code
 
        $cart_data = $this->get_cart2($user_id,$data['is_scbox']);
        if(!empty($cart_data)){

            if($user_id == 0){
                $user_id = $this->check_user_exist_or_not($data['uc_email_id']);
            }
            $new_brand = ''; $old_brand = ''; $new_store_tax = 0; $old_store_tax = 0; $new_store_cod_charges = 0; $old_store_cod_charges = 0;
            $brand_total = 0; $order_info = '';
            $product_info = array(); $i = 0;$p_price = 0;$coupon_product_price = 0;

            if($user_id == 0 ){
                $scusername = $this->Schome_model->randomPassword();
                $scpassword = $this->Schome_model->randomPassword();
                $ip = $this->input->ip_address(); 
                $this->Schome_model->registerUser('',$scusername,$data['uc_email_id'],$scpassword,$ip,2,'');
                $user_id = $this->check_user_exist_or_not($data['uc_email_id']);
            }
			/* referal code start */
			
			$ref_total_product_price = 0;$ref_dis_amount = 0;
			
			foreach($cart_data as $val){
				$ref_total_product_price = $ref_total_product_price + $val['price'];
				$ref_dis_amount = $ref_dis_amount+$val['discount_price'];
			}
			$referal_count = count($this->getuser_data($user_id));
			if($ref_dis_amount >= MIN_CART_VALUE && $referal_count > 0){
				$show_ref = 1;					
				}else $show_ref = 0;
			// sc_credit_charges is zero bcoz we are not using this constant			
			if($data['sc_credit'] == '1' && $show_ref == '1'){ 
				$sc_credit_charges = 0; 
				$data['sc_credit'] = '1';
			} else {
				$sc_credit_charges = 0;
				$data['sc_credit'] = '0';				
			}
			
			/* referal code end */
			
			
            foreach($cart_data as $val){
                $new_brand = $val['user_id'];
                $new_store_tax = $val['store_tax'];
				
                $new_store_cod = $val['is_cod'];
                $new_store_cod_charges = $val['cod_charges'];
				$p_price = $p_price+$val['price'];
				/* Discount Charges Added by Sudha */
                $discount_percent =  $val['discount_percent'];
                $discount_id = $val['discount_id'];
                /* End Discount Charges Added by Sudha */
				
				// coupon code start 
				$coupon_brand = $this->getCouponData($coupon_code,$val['user_id'])['brand_id'];
                 
                if($coupon_brand==$new_brand || $coupon_brand==0)
                {
                  if($coupon_products!=''&& $coupon_products!=0)
                  {
                      /* coupon_discount_type =3 (Product discount)*/
                    if($coupon_discount_type==3)
                    {
                        if(in_array($val['product_id'], $coupon_products_arr))
                        {
                            if($coupon_min_spend<=$val['price'] && $val['price']<=$coupon_max_spend)
                            {
                              $coupon_discount = $this->getCouponData($coupon_code)['coupon_amount'];  
                              $appliedcoupon_code = $coupon_code;    
                            }               
                        }
                    }
                     /* coupon_discount_type =4 (Product % discount)*/
                    if($coupon_discount_type==4)
                    {
                      if(in_array($val['product_id'], $coupon_products_arr))
                      {              
                        if($coupon_min_spend<=$value['price'] && $value['price']<=$coupon_max_spend)
                        {
                          $coupon_discount = $this->getCouponData($coupon_code)['coupon_amount'];  
                          $appliedcoupon_code = $coupon_code;   
                        } 
                      }
                    }        

                  }else
                  {   
                    /* coupon_discount_type =1 (cart discount)*/
                    if($coupon_discount_type==1)
                    {
                        $coupon_product_price = $coupon_product_price+$val['price'];
                        if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                        {                 
                            $coupon_discount = $this->getCouponData($coupon_code,$val['user_id'])['coupon_amount']; 
                             $appliedcoupon_code = $coupon_code;    
                        } 
                    }

                    /* coupon_discount_type =2 (cart % discount)*/
                    if($coupon_discount_type==2)
                    {
                      $coupon_product_price = $coupon_product_price+$val['price'];
                      if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                      {
                        $coupon_discount = $this->getCouponData($coupon_code,$val['user_id'])['coupon_amount']; 
                        $appliedcoupon_code = $coupon_code; 
                      }
                    }                 
                  } 
                  if($appliedcoupon_code != '')
                  {
                     $this->update_coupon_counter($appliedcoupon_code);
                  }         
                }
				// coupon code end
				$cart_iteams = $this->product_object2($user_id,$data['uc_pincode'],$data['is_scbox']);
				$coupon_discount = $this->get_coupon_discount($coupon_code,$cart_iteams);
				// echo $coupon_discount;exit;
				if($coupon_discount > 0){ $appliedcoupon_code = $coupon_code; }		
			
                if($new_brand != $old_brand){ 
                     
                    if($brand_total!=0){ 
                        $tax = $brand_total*($old_store_tax/100); 
                        $order_amount = $brand_total+($brand_total*($old_store_tax/100)); 

                      /* COD CALCULAION START */
                        if($this->getBrandExtraInfo($old_brand,'is_cod')[0]['is_cod'] == 1 && $data['paymod'] == 'cod'){
                            if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0 && $this->getBrandExtraInfo($old_brand,'code_max_value')[0]['code_max_value'] == 0){
                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ; 
                                $order_amount = $order_amount + $final_cod_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] >= 0 && $this->getBrandExtraInfo($old_brand,'code_max_value')[0]['code_max_value'] > 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']<=$product_total && $this->getBrandExtraInfo($old_brand,'code_max_value')[0]['code_max_value']>=$product_total) {


                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                                $order_amount = $order_amount + $final_cod_charges;
                            }else { $old_store_cod_charges = 0; }
                        }else{ $old_store_cod_charges = 0; }
                        /* COD CALCULAION END */

                        /* SHIPPING CHARGES START*/
                        if($this->getBrandExtraInfo($old_brand,'is_shipping')[0]['is_shipping'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] == 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] == 0){


                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] >= 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] > 0 && $this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values']<=$product_total && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values']>=$product_total) {

                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;

                            }else { $old_store_shipping_charges = 0; }
                            }else{ $old_store_shipping_charges = 0; }
                        /* SHIPPING CHARGES END*/

						/*Stylecracker Discount Condition*/
                        if($val['sc_discount_type']==4)
                        {
                            $sc_max_amount = $val['sc_discount_max_amount'];
                            $sc_discount = $val['sc_discount_percent'];  
                            if($p_price>$sc_max_amount)
                            {
                               $sc_discount_id = $val['sc_discount_id'];                            
                                $sc_discount_applied =1; 
                            }else
                            {
                                $sc_discount_id = 0;                            
                                $sc_discount_applied =0; 
                            }                         
                            
                        }
						$order_amount = $order_amount - $sc_credit_charges;
                        /*Stylecracker Discount Condition End*/
						
                        $order_info = array(
                                'user_id' => $user_id,
                                'brand_id' => $old_brand,
                                'order_tax_amount' => $tax,
                                'order_sub_total' => $order_amount,
                                'order_total' => $order_amount,
                                'pay_mode' => $data['paymod']=='cod' ? '1' : '2',
                                'created_datetime' =>date('Y-m-d H:i:s'),
                                'first_name' => $data['uc_first_name'],
                                'last_name' => $data['uc_last_name'],
                                'email_id' => $data['uc_email_id'],
                                'mobile_no' => $data['uc_mobile'],
                                'shipping_address' => $data['uc_address'],
                                'pincode' => $data['uc_pincode'],
                                'city_name' => $data['uc_city'],
                                'state_name' => $data['uc_state'],
                                'mihpayid' => $data['mihpayid'],
                                'txnid' => $data['txnid'],
                                'order_status' => $order_status,
                                'cod_amount' => $final_cod_charges,
                                'shipping_amount' => $final_shipping_charges,
								'sc_discount_applied' => $sc_discount_applied,
								'coupon_code'=>$appliedcoupon_code,
                                'coupon_amount'=>$coupon_discount,
								'confirm_mobile_no' => '1',
								'order_display_no' =>$order_display_no,
								'billing_address' => $data['billing_address']
                            );//print_r($order_info);exit;
							//echo  $final_cod_charges."coming1";
                        $order_id = $this->sc_place_order($order_info); $product_total =0;
                        $order_list[] = $order_id; 
						$final_cod_charges = 0;
                        $final_shipping_charges = 0;

                        if(!empty($product_info)){
                            foreach($product_info as $value){
                                $pro_data = array();
                                $pro_data = array('order_id'=>$order_id,'product_id'=>$value['product_id'],'user_id'=>$user_id,'created_datetime'=>date('Y-m-d H:i:s'),'product_size'=>$value['product_size'],'product_qty'=>$value['product_qty'],'product_price'=>$value['price'],'cart_id'=>$value['cart_id'],'discount_percent'=>$discount_percent,'discount_availed_id'=>$discount_id,'sc_discount'=>$sc_discount_id,'coupon_code'=>$appliedcoupon_code,'compare_price'=>$value['compare_price'] > 0 ? $value['compare_price'] : $value['price']);
                                $sc_o_id = $this->sc_orders_product($pro_data);
                                if($order_status == 1){$this->update_order_cart_info($value['cart_id'],$order_id,$user_id);}
                            }
                        }
                        
                        $product_info = [];
                    }
                    //$brand_total = 0;
                    $old_brand = $new_brand;
                    $old_store_tax = $new_store_tax;
                    //$brand_total = $brand_total+$val['price'];

                    $product_info[$i]['product_id'] = $val['product_id'];
                    $product_info[$i]['user_id'] = $user_id;
                    $product_info[$i]['created_datetime'] = date('Y-m-d H:i:s');
                    $product_info[$i]['product_size'] = $val['product_size'];
                    $product_info[$i]['product_qty'] = $val['product_qty'];
                    // $product_info[$i]['price'] = $val['price'];
					$product_info[$i]['price'] = $val['discount_price'];
                    $product_info[$i]['cart_id'] = $val['id'];
					$product_info[$i]['compare_price'] = $val['compare_price'];
                    //$product_total = $product_total+$val['price'];
					//echo $val['discount_price'];exit;
                    if($val['discount_price']!='' || $val['discount_price']!=0)
                    {
                        $product_total = $product_total+$val['discount_price'];
                    }else
                    {
                        $product_total = $product_total+$val['price'];
                    }                    
                 
                    $brand_total = $product_total;
                }else{
                    //$brand_total = $brand_total+$productPrice;
                    $old_brand = $new_brand;
                    $old_store_tax = $new_store_tax;

                    $product_info[$i]['product_id'] = $val['product_id'];
                    $product_info[$i]['user_id'] = $user_id;
                    $product_info[$i]['created_datetime'] = date('Y-m-d H:i:s');
                    $product_info[$i]['product_size'] = $val['product_size'];
                    $product_info[$i]['product_qty'] = $val['product_qty'];
                    //$product_info[$i]['price'] = $val['price'];
					$product_info[$i]['price'] = $val['discount_price'];
                    $product_info[$i]['cart_id'] = $val['id'];
					$product_info[$i]['compare_price'] = $val['compare_price'];
                    //$product_total = $product_total+$val['price'];
					
					//echo $val['discount_price'];exit;
                    if($val['discount_price']!='' || $val['discount_price']!=0)
                    {
                        $product_total = $product_total+$val['discount_price'];
                    }else
                    {
                        $product_total = $product_total+$val['price'];
                    }                    
                    
					
                    $brand_total = $product_total;

                }
                $i++;
            }
            if($brand_total!=0){ 
                $tax = $brand_total*($old_store_tax/100);
				// added for scbox tax
				if($old_brand == SCBOX_BRAND_ID){
					$tax_payble_amount = $brand_total - $coupon_discount;
					$tax = $tax_payble_amount*($old_store_tax/100);
				}
				// added for scbox tax				
                $order_amount = $brand_total + $tax; 
				
                /* COD CALCULAION START */
                
                        if($this->getBrandExtraInfo($old_brand,'is_cod')[0]['is_cod'] == 1 && $data['paymod'] == 'cod'){
                            if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0 && $this->getBrandExtraInfo($old_brand,'code_max_value')[0]['code_max_value'] == 0){
                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ; 
                                $order_amount = $order_amount + $final_cod_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] >= 0 && $this->getBrandExtraInfo($old_brand,'code_max_value')[0]['code_max_value'] > 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']<=$product_total && $this->getBrandExtraInfo($old_brand,'code_max_value')[0]['code_max_value']>=$product_total) {


                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                                $order_amount = $order_amount + $final_cod_charges;
                            }else { $old_store_cod_charges = 0; }
                        }else{ $old_store_cod_charges = 0; }
                        /* COD CALCULAION END */

                                 /* SHIPPING CHARGES START*/
                        if($this->getBrandExtraInfo($old_brand,'is_shipping')[0]['is_shipping'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] == 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] == 0){


                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] >= 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] > 0 && $this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values']<=$product_total && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values']>=$product_total) {

                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;

                            }else { $old_store_shipping_charges = 0; }
                            }else{ $old_store_shipping_charges = 0; }
                        /* SHIPPING CHARGES END*/
						
						/*Stylecracker Discount Condition*/
                        if($val['sc_discount_type']==4)
                        {
                            $sc_max_amount = $val['sc_discount_max_amount'];
                            $sc_discount = $val['sc_discount_percent'];  
                            if($p_price>$sc_max_amount)
                            {
                               $sc_discount_id = $val['sc_discount_id'];                            
                                $sc_discount_applied =1; 
                            }else
                            {
                                $sc_discount_id = 0;                            
                                $sc_discount_applied =0; 
                            }                         
                            
                        }
                        /*Stylecracker Discount Condition End*/
				$order_amount = $order_amount - $sc_credit_charges;
                $order_info = array(
                                'user_id' => $user_id,
                                'brand_id' => $old_brand,
                                'order_tax_amount' => $tax,
                                'order_sub_total' => $order_amount,
                                'order_total' => $order_amount,
                                'pay_mode' => $data['paymod']=='cod' ? '1' : '2',
                                'created_datetime' =>date('Y-m-d H:i:s'),
                                'first_name' => $data['uc_first_name'],
                                'last_name' => $data['uc_last_name'],
                                'email_id' => $data['uc_email_id'],
                                'mobile_no' => $data['uc_mobile'],
                                'shipping_address' => $data['uc_address'],
                                'pincode' => $data['uc_pincode'],
                                'city_name' => $data['uc_city'],
                                'state_name' => $data['uc_state'],
                                'mihpayid' => $data['mihpayid'],
                                'txnid' => $data['txnid'],
                                'order_status' => $order_status,
                                'cod_amount' => $final_cod_charges,
                                'shipping_amount' => $final_shipping_charges,
								'sc_discount_applied' => $sc_discount_applied,
								'coupon_code'=>$appliedcoupon_code,
                                'coupon_amount'=>$coupon_discount,
								'confirm_mobile_no' => '1',
								'order_display_no' =>$order_display_no,
								'billing_address' => $data['billing_address']
                            );//print_r($order_info);exit;
							//echo  $final_cod_charges."coming2";
                        $order_id = $this->sc_place_order($order_info); $product_total =0;
                        $order_list[] = $order_id; 
						$final_cod_charges = 0;
                        $final_shipping_charges = 0;
                        if(!empty($product_info)){
                            foreach($product_info as $value){
                                $pro_data = array();
                                $pro_data = array('order_id'=>$order_id,'product_id'=>$value['product_id'],'user_id'=>$user_id,'created_datetime'=>date('Y-m-d H:i:s'),'product_size'=>$value['product_size'],'product_qty'=>$value['product_qty'],'product_price'=>$value['price'],'cart_id'=>$value['cart_id'],'discount_percent'=>$discount_percent,'discount_availed_id'=>$discount_id,'sc_discount'=>$sc_discount_id,'coupon_code'=>$appliedcoupon_code,'compare_price'=>$value['compare_price'] > 0 ? $value['compare_price'] : $value['price']);
                                $sc_o_id = $this->sc_orders_product($pro_data);
                                 if($order_status == 1){$this->update_order_cart_info($value['cart_id'],$order_id,$user_id);}
                            }
                        }
                        $product_info = [];

                        

            }
            if(!empty($order_list)){
				$this->update_fail_order_status($user_id,$cart_data,$payent_method);
				//$this->update_user_address($data,$user_id);
               return $this->update_order_unique_no($order_list,$user_id,$order_status,$data['sc_credit']);
			   
				
            }
        }else{
           // return 'Your cart is empty';
           // return '{"order" : "fail", "msg" : "Your cart is empty"}';
		   return 2;
        }

    }

	function get_sc_discount_id($product_price){
		$query1 = $this->db->query("select da.id from discount_availed da where da.discount_type_id = 4 AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.max_amount <= $product_price");
		$res = $query1->result_array();
		if(!empty($res)) return $res[0]['id']; else return 0;
	}
	
    function sc_place_order($data){
        if(!empty($data)){
            $this->db->insert('order_info', $data); 
            return $this->db->insert_id();
        }
    }

    function sc_orders_product($data){
        if(!empty($data)){
			$this->db->insert('order_product_info', $data); 
			$last_in_id = $this->db->insert_id();

			$product_id = $data['product_id'];
			$product_size = $data['product_size'];
			if($product_id>0 && $product_size>0 ){
			$query = $this->db->query('update product_inventory set stock_count = (stock_count-1) where product_id = "'.$product_id.'" and size_id="'.$product_size.'"');
			}
			return $this->db->insert_id();
        }
    }

    function getMyOrders(){
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }

        //$query = $this->db->order_by('id','DESC')->get_where('order_info', array('user_id' => $user_id));

         $query = $this->db->query('select a.`id`, a.`user_id`, a.`brand_id`,a.`order_unique_no`, a.`order_tax_amount`, a.`cod_amount`, a.`order_sub_total`, a.`order_total`, a.`pay_mode`, a.`created_datetime`, a.`modified_datetime`, a.`order_status`, a.`first_name`, a.`last_name`, a.`email_id`, a.`mobile_no`, a.`shipping_address`, a.`pincode`, a.`city_name`, a.`state_name`,c.`state_name`,b.`company_name` from order_info as a,brand_info as b,states as c where a.`brand_id` = b.`user_id` and a.`state_name` = c.`id` and a.`user_id` = "'.$user_id.'" group by a.`order_unique_no` order by a.`id` desc');

        $result = $query->result_array();
        return $result;
    }

    function getMyProductsOrders(){
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }

        $query = $this->db->query('select a.`id`,c.`name`,c.`price`,b.product_qty,b.product_size,d.size_text as size_text from `order_info` as a,`order_product_info` as b,`product_desc` as c,`product_size` as d where 1 and a.`id` = b.`order_id` and b.`product_id` = c.`id` and a.`user_id` = "'.$user_id.'" and d.`id` = b.product_size order by a.`id` desc');

        $result = $query->result_array();
        return $result;
    }

    function update_order_unique_no($order_list,$user_id,$order_status,$sc_credit){
        $order_info = '';
        if(!empty($order_list)){
            $i = 0;
            foreach ($order_list as $key => $value) {
                if($i!=0){ $order_info = $order_info.'_'; }
                $order_info = $order_info.$value;
                $i++;
            }
			$order_unique_no = $user_id.'_'.$order_info;
            $data = array(
                'order_unique_no' => $user_id.'_'.$order_info
            );

            $this->db->where_in('id', $order_list);
            $this->db->update('order_info', $data); 
		// $query = $this->db->query("select * from order_info where order_unique_no LIKE '$order_unique_no' ");
		if($sc_credit == '1'){
			$sc_credit = $this->redeem_points($order_unique_no,$user_id);
		}
        // $result = $query->result_array();
		$result = $this->get_order($order_unique_no,$user_id,$order_status);
		
        return $result;
			
        }

    }
	
	function get_order_info($uniquecookie){
        
        $query = $this->db->query('select order_id,user_id from `cart_info` where SCUniqueID = "'.$uniquecookie.'"');
        $order_id = $query->result_array(); 
		
        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['order_id'];
                $user_id = $val['user_id'];
             }

            $order_ids = implode(",",$order_info); 

            $query = $this->db->query('select a.`order_id`, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`, a.`product_price`,b.`name`,b.`image`,b.`brand_id`,c.size_text from order_product_info as a,product_desc as b,product_size as c where a.`product_id` = b.`id` and a.`order_id` in ('.$order_ids.') and a.`user_id`='.$user_id.' and a.product_size = c.`id`' );

            $query_res = $query->result_array();
            return $query_res;

        }
    }
	
	function get_order_price_info($uniquecookie){
        $query = $this->db->query('select order_id,user_id from `cart_info` where SCUniqueID = "'.$uniquecookie.'"');
        $order_id = $query->result_array(); 

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['order_id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info); 

        $query = $this->db->query('select sum(`order_tax_amount`) as order_tax_amount, sum(`cod_amount`) as cod_amount, sum(`order_sub_total`) as order_sub_total, sum(`order_total`) as order_total,sum(shipping_amount) as shipping_amount from order_info where id in ('.$order_ids.')' );

            $query_res = $query->result_array();
            return $query_res;
        }
    }
		// $order_id = explode(',',$order_unique_no);
		// $where_condition = implode(" or order_id = ",$order_id);
		
	function chkShipping($product_id,$pincode){
        $query = $this->db->query('select a.`id` from product_desc as a,brand_info as b where a.`brand_id`= b.`user_id` and a.`id`="'.$product_id.'" and `shipping_exc_location` like "%'.$pincode.'%"' );
        $query_res = $query->result_array();
        return $query_res;
    }
	
	function chkCod($product_id,$pincode){
        $query = $this->db->query('select a.`id` from product_desc as a,brand_info as b where a.`brand_id`= b.`user_id` and a.`id`="'.$product_id.'" and `cod_exc_location` like "%'.$pincode.'%"' );

        $query_res = $query->result_array();
        return $query_res;
    }
	
	function chkShipping2($product_id,$pincode){
		// $product_id = explode(',',$product_id);
		$count = count($product_id);
		//echo $count;echo $product_id[0]['product_id'];print_r($product_id);exit;
		$product_unavailable = array();
		for($i=0;$i<$count;$i++){
			$query = $this->db->query('select a.`id` from product_desc as a,brand_info as b where a.`brand_id`= b.`user_id` and a.`id`="'.$product_id[$i]['product_id'].'" and `shipping_exc_location` like "%'.$pincode.'%"' );
			$query_res = $query->result_array();
			if($query_res != NULL) $product_unavailable[] = $query_res[0];
		}
       //print_r($product_unavailable);exit;
        return $product_unavailable;
    }
	
	// old function no use
	/*function chkCod2($product_id,$pincode){
		$count = count($product_id);
		$product_unavailable = array();
		for($i=0;$i<$count;$i++){
        $query = $this->db->query('select a.`id` from product_desc as a,brand_info as b where a.`brand_id`= b.`user_id` and a.`id`="'.$product_id[$i]['product_id'].'" and `cod_exc_location` like "%'.$pincode.'%"' );

        $query_res = $query->result_array();
		if($query_res != NULL) $product_unavailable[] = $query_res[0];
		}
		//print_r($product_unavailable);exit;
        return $product_unavailable;
    }*/
	function chkCod2($product_id,$pincode){
		$count = count($product_id);
		$product_unavailable = array();
		for($i=0;$i<$count;$i++){
		$brand_id = $this->get_brand_id($product_id[$i]['product_id']);
		if($brand_id != NULL){
			$query = $this->db->query("select a.`cod_available` from brand_info as a where a.user_id = $brand_id ");
			$res = $query->result_array();
			if(!empty($res)){ $cod_available = $res[0]['cod_available']; } else $cod_available = 0;
		}
		$cod_available = 1;
		if($cod_available>=1){
        $query = $this->db->query('select a.`id` from product_desc as a,brand_info as b where a.`brand_id`= b.`user_id` and a.`id`="'.$product_id[$i]['product_id'].'" and `cod_exc_location` like "%'.$pincode.'%"' );

        $query_res = $query->result_array();
		
		if($query_res != NULL) $product_unavailable[] = $query_res[0];
		}else { $query_res[0]['id'] = $product_id[$i]['product_id'];
				$product_unavailable[] = $query_res[0]; }
		
		}
		
        return $product_unavailable;
    }
	function get_brand_id($product_id){
		//$query = $this->db->query("select a.`brand_id` from product_desc as a where a.id = $product_id ");
		$this->db->select("a.brand_id");
		$this->db->from("product_desc as a"); 
		$this->db->where("a.id", $product_id);
		$query = $this->db->get();		
		$res = $query->result_array();
		if(!empty($res)){
			return $res[0]['brand_id'];
		}else return null;
	}

	function get_order($order_unique_no,$userid,$order_status = 1){	
		$order_status_query = " AND order_status = $order_status";
		$this->db->select('id,user_id,order_unique_no,brand_id,first_name,pay_mode as payment_method,created_datetime,order_status,shipping_address,order_tax_amount,cod_amount,order_total,order_display_no,billing_address,is_completed');
		$this->db->from('order_info');
		$this->db->where('user_id',$userid);
		$this->db->like('order_unique_no',$this->db->escape_str($order_unique_no),'both');
		$query = $this->db->get();
		$res = $query->result_array();
		$order_id = explode('_',$order_unique_no);
		$total_order = array(0);
		$orderStatusList 	= array(
									'1'=>'Processing', 
									'2'=>'Confirmed',
									'3'=>'Dispatched',
									'4'=>'Delivered',
									'5'=>'Cancelled',
									'6'=>'Return',
									'7'=>'Fake',
									'8'=>'Curated',
									'9'=>'Incomplete Online Transaction',
									);
		if(!empty($res)){
				
				$order_display_no = $res[0]['order_display_no'];				

				foreach($res as $val){
					$orderobject 					= new stdClass(); 
					$orderobject->id 				= $val['id'];
					$orderobject->user_id 			= $val['user_id'];
					$orderobject->first_name 		= $val['first_name'];					
					$orderobject->shipping_address	= $val['shipping_address'];
					$orderobject->order_tax_amount 	= $val['order_tax_amount'];
					$orderobject->order_no 			= $val['order_unique_no'];
					$orderobject->order_display_no 	= $val['order_display_no'];
					$orderobject->cod_amount 		= $val['cod_amount'];
					$total_order[] 					= $val['order_total'];
					
					if($val['payment_method'] == 1){
						$orderobject->payment_method = 'COD';
					}
					else{
						$orderobject->payment_method = 'online';
					}
					$scbox_gift_object = $this->Mcart_model_android->get_object('scbox_gift')[0]['object_id']; 
				    if($val['order_display_no']!='')
				   {
					  $gift_data = $this->get_gift_data($scbox_gift_object,$val['order_display_no']);
				    }          
     
					//$orderobject->payment_method 	= $val['payment_method'];
					$orderobject->created_datetime 	= $val['created_datetime'];
					//$orderobject->order_status_id 	= $val['order_status'];
					//$orderobject->order_status 		= $orderStatusList[$val['order_status']];				
					$orderobject->is_completed 		= $val['is_completed'];
					$order_id = explode('_',$val['order_unique_no']);
					//$item_count = count($order_id)-1;
					$totals = $this->totals($val['order_unique_no'],$order_status);
					$orderobject->totals = (object)$totals;	
					if(!empty($gift_data))
					{
						$ship_name = explode(' ', $gift_data['name']);
						$orderobject->shipping_address = array(
							'gender'				=> $gift_data['gender'],
							'first_name'			=> $ship_name[0],
							'last_name'  			=> $ship_name[1],
							'mobile_no'  			=> $gift_data['mobileno'],
							'shipping_address' 		=> $gift_data['address'],
							'pincode'  				=> $gift_data['pincode'],
							'city_name'  			=> $gift_data['city'],
							'state_name'  			=> $gift_data['state'],
							'email' 				=> $gift_data['email'],
							/*'profession' 			=> $gift_data['profession'],
							'gift_msg' 				=> $gift_data['gift_msg'],
							'dob' 					=> $gift_data['dob'],
							'stylist_call_receiver' => $gift_data['stylist_call_receiver'],*/
						);
						
						$billing_address 	= (object)$this->shipping_address($val['id'],$order_status);
						
						$orderobject->billing_address = array(
							//'user_id'				=> $billing_address->user_id,
							'billing_first_name'			=> $billing_address->first_name,
							'billing_last_name'  			=> $billing_address->last_name,
							'billing_mobile_no'  			=> $billing_address->mobile_no,
							'billing_pincode_no'  			=> $billing_address->pincode,
							'billing_address' 				=> $billing_address->shipping_address,
							'billing_email_id' 				=> $billing_address->email,
							'billing_city'  				=> $billing_address->city_name,
							'billing_state'  				=> $billing_address->state_name,
							
						);
						
						$orderobject->is_gift 			= TRUE; 
					}
					else {  
						$orderobject->shipping_address 	= (object)$this->shipping_address($val['id'],$order_status);
						$orderobject->billing_address 	= json_decode($val['billing_address']);
						$orderobject->is_gift 			= FALSE;
					 } 
					$orderobject->item_qty			= $this->product_quantity_count($val['order_unique_no']);
					$orderobject->brand_totals 		= $this->brand_totals2($val['order_unique_no']);
					// $orderobject->item_count= $item_count;
					//$orderobject->brand_details = $this->brand_details($val['brand_id']);
					$orderobject->order_total 		= (string)$totals['grand_total'];
					//$i++;
				}
				$orderobject1[] 	= $this->product_details_three($val['order_unique_no'],'');
					/*  print_r($orderobject1);
					exit; */ 
			}
		//echo array_sum($total_order);exit;
		
		if(!empty($total_order) && array_sum($total_order)!=0) 
		{
			$orderobject->order_total_old = (string)array_sum($total_order);
		}
		else  
		{
			$orderobject->order_total = (string)0;
		}
		$orderobject->item_count= (string)count($orderobject1);
		$orderobject->product_details = $orderobject1[0];
		//echo '<pre>';print_r($orderobject1);exit;
		//For Scbox Product List
		//$product_order_status = $orderobject->product_details[0]->order_status;		
		$product_order_status = $orderobject->product_details[0]->order_status_id;		
		$objectId 	= $this->Mcart_model_android->get_object('box_order_products')[0]['object_id'];
			//echo $product_order_status;
			//exit();
		$i = 0;		
			$mkey = $order_display_no;
			$metaValueData = array();
			if(@$product_order_status==4)
			{ 
				$metaValue = $this->Mcart_model_android->getSereializeProductDetail($mkey,'','',$objectId);
				
				$bjectMetaValue  = unserialize($metaValue['object_meta_value']);
				
				if(is_array($bjectMetaValue) && count($bjectMetaValue)>0)
				{
					$j=0;
					foreach($bjectMetaValue as $key => $metaId)
					{
						if(@$metaId['is_delete']==0)
						{
							$metaValueData[$j] = $this->Mcart_model_android->getMetaValueData($key);
							//echo '<pre>';print_r($metaValueData); //exit;
							$metaValueData[$j]['image'] = base_url().'sc_admin/assets/products/'.$metaValueData[$j]['image'];							
							$metaValueData[$j]['status'] = $metaId['status'];
							$j++;
						}
						
					}
				}else
				{
					$metaValueData = array();
				}				
			}
			$scprd_list = $metaValueData;				
			$i++;
		
		$orderobject->scbox_product_details = $scprd_list;
		//End of For Scbox Product List
		
		//Return & Exchange
		$return_exchange['return']['header'] = 'Oh no! We\'re sorry you didn\'t love your product. Tell us what went wrong so we can fix it when you come back: ';
		$return_exchange['return']['label'] = 'Reasons for returning ';
		$return_exchange['return']['options'][0] = 'It didn\'t fit. ';
		$return_exchange['return']['options'][1] = 'I don\'t love the quality. ';
		$return_exchange['return']['options'][2] = 'The product is damaged. ';
		$return_exchange['return']['options'][3] = 'It doesn\'t fit my style. ';
		$return_exchange['return']['anchortext'] = 'Click here to request a refund';
		$return_exchange['return']['placeholdertext'] = 'IS THERE ANYTHING WE MISSED ?';
		$return_exchange['return']['buttontext'] = 'SUBMIT ';
		
		$return_exchange['exchange']['header'] = 'Oh no! We\'re sorry you didn\'t love your product. Let us fix that for you!  ';
		
		$return_exchange['exchange']['label'] = 'Tell us what went wrong: ';
		$return_exchange['exchange']['options'][0] = 'It didn\'t fit. ';
		$return_exchange['exchange']['options'][1] = 'I don\'t love the quality. ';
		$return_exchange['exchange']['options'][2] = 'The product is damaged. ';
		$return_exchange['exchange']['options'][3] = 'It doesn\'t fit my style. ';
		$return_exchange['exchange']['anchortext'] = 'Click here to request a refund';
		$return_exchange['exchange']['placeholdertext'] = 'IS THERE ANYTHING WE MISSED ?';
		$return_exchange['exchange']['buttontext'] = 'SUBMIT ';
		$orderobject->return_exchange = $return_exchange; 
		//End Return & Exchange
		
		// print_r($orderobject);exit;
		return $orderobject;
		
	}
	
function get_gift_data($object_id,$object_meta_key=null){
    $product_array = array();
    if($object_id!='' && $object_meta_key!='')
    {
      $query = $this->db->query('select a.object_meta_key,a.object_meta_value from object_meta as a where a.object_id = "'.$object_id.'" AND a.object_meta_key = "'.$object_meta_key.'" ');
    }else if($object_id!='' && $object_meta_key=='')
    {
      $query = $this->db->query('select a.object_meta_key,a.object_meta_value from object_meta as a where a.object_id = "'.$object_id.'" ');
    }
	
    $result = $query->result_array();   
    
    if(!empty($result)){      
          $product_array = unserialize($result[0]['object_meta_value']);          
        }      
      return $product_array;
    
  }
	
	function brand_totals($order_id){
		$order_ids = explode('_',$order_id);
		$order_idhtml = '';
		if(!empty($order_ids)){
			for($i = 1; $i<count($order_ids); $i++){
					if($i != 1){ $order_idhtml = $order_idhtml.','; }
					$order_idhtml = $order_idhtml.$order_ids[$i];
			}
		}
		$query = $this->db->query("SELECT a.user_id as brand_id,a.company_name,IF(SUM(c.product_price)<a.code_max_value, a.cod_charges , 0) as cod_charges,IF(SUM(c.product_price)<a.shipping_max_values, a.shipping_charges , 0) as shipping_charges from brand_info a,product_desc b,order_product_info c WHERE c.product_id = b.id AND b.brand_id = a.user_id AND c.order_id IN ($order_idhtml) GROUP BY c.order_id");
		$res = $query->result_array();
		return $res;
	}
	
	function brand_totals2($order_id){
		
		if($order_id!=''){
			$query = $this->db->query("SELECT a.brand_id,b.company_name,a.order_tax_amount as tax_charges,a.cod_amount as cod_charges,a.shipping_amount as shipping_charges,a.`order_total` as order_total from order_info as a,brand_info as b where a.brand_id = b.user_id and a.order_unique_no ='".$order_id."' ");
			$res = $query->result_array();
		}
		return $res;
	}
	
	
	function brand_details($brand_id){
		//$query = $this->db->query("SELECT user_id as brand_id,company_name,cod_charges,shipping_charges,cod_available as cod_available FROM `brand_info` WHERE `user_id` = $brand_id");
		$query = $this->db->query("SELECT user_id as brand_id,company_name,cod_charges,shipping_charges FROM `brand_info` WHERE `user_id` = $brand_id");
		$res = $query->result_array();
		//print_r($res);exit;
		return $res[0];
		
	}
	
	function product_details_three($order_id,$brand_id){
		$order_ids = explode('_',$order_id);
		$order_idhtml = '';
		if(!empty($order_ids)){
			for($i = 1; $i<count($order_ids); $i++){
					if($i != 1){ $order_idhtml = $order_idhtml.','; }
					$order_idhtml = $order_idhtml.$order_ids[$i];
			}
		}
		//$query = $this->db->query("select op.product_id,op.product_price,op.product_qty,op.product_size,p.image,p.name,p.id,p.brand_id from order_product_info as op,product_desc as p where p.id=op.product_id and order_id IN ($order_idhtml)");
		$query = $this->db->query("select z.product_id,z.product_price,z.product_size,z.product_qty,z.image,z.name,z.id,z.brand_id,y.min_del_days,y.max_del_days,z.order_status from (select op.product_id,op.product_price,op.product_size,op.product_qty,p.image,p.name,p.id,p.brand_id,op.order_status from order_product_info as op INNER JOIN product_desc as p ON p.id=op.product_id where op.order_id IN ($order_idhtml)) as z,brand_info as y where z.brand_id = y.user_id ");
		$res = $query->result_array();
		$product_array = array();
		$product_data = array();
		if(!empty($res)){ $k = 0;
		$orderStatusList 	= array(
			'1'=>'Processing', 
			'2'=>'Confirmed',
			'3'=>'Dispatched',
			'4'=>'Delivered',
			'5'=>'Cancelled',
			'6'=>'Return',
			'7'=>'Fake',
			'8'=>'Curated',
			'9'=>'Incomplete Online Transaction',
			);
			foreach($res as $val){
				$orderobject = new stdClass(); 
				$deliver_message = '';
				$orderobject->product_id = $val['product_id'];
				//added for discount
				// $product_data[$k]['discount_percent'] = $this->get_discount_percentage($val['brand_id'],$val['product_id']);
                // $product_data[$k]['discount_price'] = $val['product_qty']*($val['product_price']-($val['product_price']*$product_data[$k]['discount_percent'])/100); 
				// added for discount 
				$orderobject->product_price = (string)$val['product_price'];
				$orderobject->product_qty = $val['product_qty'];
				$orderobject->product_size = $this->get_size_text($val['product_size']);
				$orderobject->image = base_url().'sc_admin/assets/products/'.$val['image'];
				$orderobject->name = $val['name'];
				/*if(($val['min_del_days'] != '' && $val['max_del_days'] != '') || ($val['min_del_days'] != NULL && $val['max_del_days'] != NULL)) {
						$deliver_message = 'Delivery in '.$val['min_del_days'].'-'.$val['max_del_days'].' day(s)';
					}
					else if(($val['min_del_days'] != '') || ($val['min_del_days'] != NULL)){
						$deliver_message = 'Delivery in '.$val['min_del_days'].'  day(s)';
					}
					else if(($val['max_del_days'] != '') || ($val['max_del_days'] != NULL)){
						$deliver_message = 'Delivery in '.$val['max_del_days'].' day(s)';
					}*/
				if($val['min_del_days']>0 && $val['max_del_days']>0){
						$deliver_message = 'Delivery in '.$val['min_del_days'].'-'.$val['max_del_days'].' working days.';
					}else if(($val['min_del_days']>0 && $val['max_del_days']=='') || ($val['min_del_days']=='' && $val['max_del_days']>0)){
						$deliver_message = 'Delivery in '.$val['min_del_days'].$val['max_del_days'].' working days.';
				}
				//$orderobject->deliver_message = $deliver_message;
				$message = array('text' => $deliver_message, 'color' => '#dd4b39') ;
				$orderobject->message = $message;	
				$orderobject->brand_details = $this->brand_details($val['brand_id']);
				//$orderobject->order_status = $val['order_status'];
				$orderobject->order_status_id 	= $val['order_status'];
				$orderobject->order_status 		= $orderStatusList[$val['order_status']];
				$product_array[]= $orderobject;
				$k++;
			}
				
		}
		
		return $product_array;
		
	}
	
	function get_size_text($size_id){
		$query = $this->db->query("SELECT size_text FROM `product_size` where id = $size_id");
		$res = $query->result_array();
		return $res[0]['size_text'];
		
	}
	
	function product_details($order_id,$brand_id){
		//$order_id = explode('_',$order_unique_no);
		//$where_condition = implode(" or order_id = ",$order_id);
		$query = $this->db->query("select op.product_id,op.product_price,op.product_qty,op.product_size,p.image,p.name,p.id,p.brand_id from order_product_info as op INNER JOIN product_desc as p ON p.id=op.product_id where order_id = $order_id");
		$res = $query->result_array();
		
		$product_array = array();

		if(!empty($res)){
			foreach($res as $val){
				$orderobject = new stdClass(); 
				$orderobject->product_id 	= $val['product_id'];
				$orderobject->product_price = $val['product_price'];
				$orderobject->product_qty 	= $val['product_qty'];
				$orderobject->product_size 	= $val['product_size'];
				$orderobject->image 		= base_url().'sc_admin/assets/products/'.$val['image'];
				$orderobject->name 			= $val['name'];
				
				$orderobject->brand_details = $this->brand_details($val['brand_id']);
				//$orderobject->brand_details = $this->brand_details($val['brand_id']);
				//$product_array= $orderobject;
				//$i++;
			}
				
		}
		//print_r($res);exit;
		return $orderobject;
		
	}
	
	function get_order_list($userid,$limit,$offset){
		$res = array();
		if($limit == '') $query_limit = ''; else $query_limit = " LIMIT $limit ";
		if($offset == '') $query_offset = ''; else $query_offset = " OFFSET $offset ";
		if($userid!='')
		{	 
			$query = $this->db->query("select id,user_id,order_unique_no,brand_id,first_name,shipping_address,order_status,created_datetime,IF(pay_mode=1,'cod','online') as payment_method,order_tax_amount,cod_amount,order_total,order_display_no,is_completed from order_info where order_status != 9 AND user_id = $userid  GROUP BY order_unique_no ORDER BY id desc $query_limit $query_offset ");
			$res = $query->result_array();
		}
		//print_r($res);exit;
		if(!empty($res)){
				
				$i = 0;
				$unique_no = 0;
				$array_sum = '';
				foreach($res as $val){
					$scbox_gift_object = $this->get_object('scbox_gift')[0]['object_id'];				
				    if($val['order_display_no']!='')
					{
						$gift_data = $this->get_gift_data($scbox_gift_object,$val['order_display_no']);
					}  
					$orderobject 					= new stdClass();
					$orderobject->id 				= $val['id'];
					$orderobject->payment_method 	= $val['payment_method'];
					$orderobject->created_datetime 	= $val['created_datetime'];
					$orderobject->order_status 		= $val['order_status'];
					$order_id 						= explode('_',$val['order_unique_no']);
					$item_count 					= count($order_id)-1;
					$orderobject->item_qty			= $this->product_quantity_count($val['order_unique_no']);
					$orderobject->item_count		= (string)$item_count;
					$orderobject->user_id 			= $val['user_id'];
					$orderobject->first_name 		= $val['first_name'];	
					$orderobject->totals 			= (object)$this->totals($val['order_unique_no']);
					$orderobject->brand_totals 		= $this->brand_totals2($val['order_unique_no']);
					if(!empty($gift_data))
					{
						$ship_name = explode(' ', $gift_data['name']);
						$orderobject->shipping_address = array(
							'gender'				=> $gift_data['gender'],
							'first_name'			=> $ship_name[0],
							'last_name'  			=> $ship_name[1],
							'mobile_no'  			=> $gift_data['mobileno'],
							'shipping_address' 		=> $gift_data['address'],
							'pincode'  				=> $gift_data['pincode'],
							'city_name'  			=> $gift_data['city'],
							'state_name'  			=> $gift_data['state'],
							'email' 				=> $gift_data['email'],
						);
						$orderobject->is_gift 			= TRUE; 
					}
					else{
						$orderobject->shipping_address 	= (object)$this->shipping_address($val['id']);
						$orderobject->is_gift 			= FALSE; 
					}
					$orderobject->order_tax_amount 	= $val['order_tax_amount'];
					$orderobject->cod_amount 		= $val['cod_amount'];
					//$orderobject->order_total     = $val['order_total']+$array_sum;
					
					if($unique_no == $val['order_unique_no']){
						
						$total_order[1] = $val['order_total'];
						$unique_no = 0;
						$array_sum = array_sum($total_order);
						$skip = 1;
					}else{
						$unique_no = $val['order_unique_no'];
						$total_order[0] = $val['order_total'];
						$array_sum = '';
						$skip=0;
					}
					//$orderobject->order_total = $array_sum;
					$orderobject->order_no 			= $val['order_unique_no'];
					$orderobject->order_display_no 	= $val['order_display_no'];
 					$orderobject->is_completed 		= $val['is_completed'];
					
					//$orderobject->brand_details = $this->brand_details($val['brand_id']);
					if($skip ==0){
						$orderobject1 = (array) $this->product_details2($val['order_unique_no'],$val['brand_id']);
						$orderobject = (array) $orderobject;
						$final_oder[$i] = array_merge($orderobject,$orderobject1);
						$i++;
					}
					
				}
					
			}
		//echo array_sum($total_order);
		//print_r($total_order);exit;
		
		//$orderobject->order_total = array_sum($total_order);
		//$orderobject = $orderobject1;
		if(!empty($final_oder)){
			return $final_oder;
		}
		else return '';
		
	}
	
	function totals($order_unique_no, $order_status=1){
		//echo "coming".$order_unique_no;
		$order_status_query = " AND order_status = $order_status";
		$query = $this->db->query("select sum(order_sub_total) as sub_total,sum(cod_amount) as cod,sum(order_total) as grand_total,sum(order_tax_amount) as tax,sum(order_total) as grand_total,sum(shipping_amount) as shipping_charges,coupon_amount from order_info where order_unique_no like '%$order_unique_no%' ");
		
		$res = $query->result_array();
		if(!empty($res)){
			$result = array();
			$result['sub_total'] = $res[0]['sub_total']; 
			$result['cod'] = $res[0]['cod'];
			$result['grand_total_old'] = $res[0]['grand_total'];
			$result['tax'] = $res[0]['tax'];
			$result['shipping_charges'] = $res[0]['shipping_charges'];
			$total_product_price = $res[0]['grand_total'] - $res[0]['cod'] - $res[0]['shipping_charges'] - $res[0]['tax'];
			$result['total_product_price'] = (string)$total_product_price;
			$stylecracker_discount = $this->get_stylecracker_discount($total_product_price);
			$stylecracker_discount_on_product = $total_product_price-($total_product_price*$stylecracker_discount)/100;
			$result['stylecracker_discount_percent'] = (string)$stylecracker_discount;
			$result['discount_price'] = (string)($total_product_price - $stylecracker_discount_on_product);
			$result['coupon_amount'] = $res[0]['coupon_amount'];
			$result['redeem_points'] = (string)$this->get_refre_amt($order_unique_no);
			$result['grand_total'] = (string)($stylecracker_discount_on_product + $res[0]['cod'] + $res[0]['shipping_charges'] + $res[0]['tax'] - $res[0]['coupon_amount'] - $result['redeem_points']) ;
		}
		return $result;
	}
	
	function shipping_address($id, $order_status=1){
		$order_status_query = " AND order_status = $order_status";
		/*$query = $this->db->query("select user_id,first_name,last_name,mobile_no,shipping_address,pincode,city_name,state_name,email_id as email from order_info where id = $id");*/
		$query = $this->db->query("select user_id,first_name,last_name,mobile_no,shipping_address,pincode,city_name,(select b.`state_name` from `states` as b,order_info as d where b.`id` = d.`state_name` AND d.id = $id limit 0,1) as state_name,email_id as email,billing_address from order_info where id = $id");
	
		$res = $query->result_array();
		/* echo "<pre>";
		print_r($res[0]);
		echo "</pre>"; */
		return $res[0];
	}
	
	function product_details2($order_unique_no,$brand_id){
		$order_id = explode('_',$order_unique_no);
		$where_condition = implode(" or order_id = ",$order_id);
		//echo $where_condition;exit;d.min_del_days,d.max_del_days
		$query = $this->db->query("select z.product_id,z.product_price,z.product_size,z.product_qty,z.image,z.name,z.id,z.brand_id,y.min_del_days,y.max_del_days,z.order_status,z.modified_datetime,z.order_id from (select op.product_id,op.product_price,op.product_size,op.product_qty,p.image,p.name,p.id,p.brand_id,op.order_status,op.modified_datetime,op.order_id from order_product_info as op,product_desc as p,order_info as oi where oi.`order_unique_no` = '".$order_unique_no."' AND oi.id = op.order_id AND p.id=op.product_id ) as z,brand_info as y where z.brand_id = y.user_id ");
		$res = $query->result_array();
		//echo "<pre>"; print_r($res);exit;
		$product_array = array();
		$i=0; 
		if(!empty($res)){
			$orderStatusList 	= array(
				'1'=>'Processing', 
				'2'=>'Confirmed',
				'3'=>'Dispatched',
				'4'=>'Delivered',
				'5'=>'Cancelled',
				'6'=>'Return',
				'7'=>'Fake',
				'8'=>'Curated',
				'9'=>'Incomplete Online Transaction',
			);
			foreach($res as $val){
				$orderobject = new stdClass(); 
				$orderobject->product_id 		= $val['product_id'];
				$orderobject->product_price 	= $val['product_price'];
				$orderobject->product_qty 		= $val['product_qty'];
				$orderobject->product_size 		= $this->get_size_text($val['product_size']);
				$orderobject->image 			= base_url().'sc_admin/assets/products/'.$val['image'];
				$orderobject->name 				= $val['name'];
				//$orderobject->order_status 		= $val['order_status'];
				$orderobject->order_status_id 	= $val['order_status'];
				$orderobject->order_status 		= $orderStatusList[$val['order_status']];
				$deliver_message= '';
				if($val['min_del_days']>0 && $val['max_del_days']>0){
						$deliver_message = 'Delivery in '.$val['min_del_days'].'-'.$val['max_del_days'].' working days.';
				}else if(($val['min_del_days']>0 && $val['max_del_days']=='') || ($val['min_del_days']=='' && $val['max_del_days']>0)){
						$deliver_message = 'Delivery in '.$val['min_del_days'].$val['max_del_days'].' working days.';
				}	
				$message = array('text' => $deliver_message, 'color' => '#dd4b39') ;
				$orderobject->message 				= $message;	
				$orderobject->brand_details 		= $this->brand_details($val['brand_id']);
				$orderobject->delivered_datetime 	= date("Y-m-d",strtotime($val['modified_datetime']));
				$orderobject->order_product_id 		= $val['order_id'];
				$product_array[$i] = $orderobject;
				$i++;
			}
		}
		$product_info = new stdClass();
		$product_info->product_info = $product_array;
		//print_r($product_array);exit;
		return $product_info;
	}
	
	function change_status($userid,$order_id,$status,$is_offer_applied){
		if($is_offer_applied == '0'){
			$data = array(
						'order_status' => $status,
						'coupon_code' => '0',
						'coupon_amount' => '0'
					); 
		}else{
			$data = array(
						'order_status' => $status,
						
					); 
		}
		$this->db->where(array('user_id'=>$userid,'order_unique_no' => $order_id));
		$result = $this->db->update('order_info', $data);
		//echo $result; exit;//print_r($result); exit;		
		//$result = $this->get_order($order_id,$userid);
		if($result == 1){
			return 1;
		}else {
			return 2;
		}
	}
	
	function product_quantity_count($order_unique_no){
		// $order_id = explode('_',$order_unique_no);
		// $where_condition = implode(" or order_id = ",$order_id);
		$query = $this->db->query("select sum(a.product_qty) as product_qty from order_product_info as a,order_info as b where b.`order_unique_no` = '".$order_unique_no."' AND b.id = a.order_id ");
		$res = $query->result_array();
		return $res[0]['product_qty'];
	}
	
	function update_user_address($data,$user_id){
		//echo $user_id;print_r($data);exit;
        $query = $this->db->get_where('user_address', array('user_id' => $user_id,'shipping_address'=>$data['uc_address'],'pincode'=>$data['uc_pincode'],'city_name'=>$data['uc_city'],'state_name'=>$data['uc_state'],'first_name'=>$data['uc_first_name'],'mobile_no'=>$data['uc_mobile']));
        $res = $query->result_array();
        if(empty($res)){
            $add_data = array(
                    'user_id'=>$user_id,
                    'shipping_address'=>$data['uc_address'],
                    'pincode'=>$data['uc_pincode'],
                    'city_name'=>$data['uc_city'],
                    'state_name'=>$data['uc_state'],
                    'first_name'=>$data['uc_first_name'],
                    'mobile_no'=>$data['uc_mobile'],
                );
            $this->db->insert('user_address', $add_data);

        }else{
            $add_data = array(
                    'user_id'=>$user_id,
                    'shipping_address'=>$data['uc_address'],
                    'pincode'=>$data['uc_pincode'],
                    'city_name'=>$data['uc_city'],
                    'state_name'=>$data['uc_state'],
                    'first_name'=>$data['uc_first_name'],
                    'mobile_no'=>$data['uc_mobile'],
                );
            $this->db->where(array('id'=>$data['existing_shipping_add'],'user_id'=>$user_id));
            $this->db->update('user_address', $add_data);
        }
    }
	
	function add_address($data){
			$add_data = array(
                    'user_id'=>$data['user_id'],
                    'shipping_address'=>$data['uc_address'],
                    'pincode'=>$data['uc_pincode'],
                    'city_name'=>$data['uc_city'],
                    'state_name'=>$data['uc_state'],
                    'first_name'=>$data['uc_first_name'],
					'email'=>$data['uc_email_id'],
                    'mobile_no'=>$data['uc_mobile'],
                    'last_name'=>$data['uc_last_name'],
                );
            $this->db->insert('user_address', $add_data);
			$insert_id = $this->db->insert_id();
		$query = $this->db->query("select * from user_address where id = $insert_id ");	
		$res = $query->result();
		if(!empty($res)) return $res[0]; else NULL;
		// return $res[0];
	}
	/*
	function getaddress($user_id){
		$query = $this->db->query("select * from user_address where user_id = $user_id ");
		$res = $query->result_array();
		return $res;
	}*/
	
	function getaddress($user_id){
		$query = $this->db->query("select * from user_address where user_id = $user_id ");
		$res = $query->result_array();
		$i=0;
		if(!empty($res)){
			foreach($res as $val){
				$addressobject = new stdClass();
				$addressobject->id = $val['id'];
				$addressobject->user_id = $val['user_id'];
				$addressobject->first_name = $val['first_name'];
				$addressobject->last_name = $val['last_name'];
				$addressobject->mobile_no = $val['mobile_no'];
				$addressobject->shipping_address = $val['shipping_address'];
				$addressobject->pincode = $val['pincode'];
				//$addressobject->city_name = $this->get_state_name($val['city_name']);
				$addressobject->city_name = $val['city_name'];
				//$addressobject->state_name = $val['state_name'];
				$state = $this->get_city_name($val['pincode']);
				if(!empty($state)){ $uc_state = trim($state['state']);
				}
				$addressobject->state_name =$uc_state;
				$addressobject->email = $val['email'];
				$addressobject->created_datetime = $val['created_datetime'];
				$address_array[$i] = $addressobject;
				$i++;
			}		
		}
		//print_r($address_array);exit;
		if(!empty($address_array))
		{
			return $address_array;
		}else return NUll;
	}
	
	function get_state_name($id){
		$query = $this->db->query("select state_name from states where id = $id ");
		$res = $query->result_array(); 
		if(!empty($res)){
			return $res[0]['state_name'];
		}else 
		return NULL;
	}
	
	function user_info($id){
		/* $query = $this->db->query("select * from user_address where id = $id ");
		$res = $query->result_array();
		return $res[0]; */
		
		$this->db->from('user_address');
		$this->db->where('id', $id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		return false;
	}
	function product_info($product_id){
		$query = $this->db->query("select * from product_desc where id = $product_id ");
		$res = $query->result_array();
		return $res;
	}
	
	function notify_me($data){
			$add_data = array(
                    'user_id'=>$data['user_id'],
					'product_id'=>$data['product_id'],
					'emailid'=>$data['emailid'],
                );
			// $click_count = '1';
			// $click_source = '3';
			$product_click_data = array(
                    'user_id'=>$data['user_id'],
					'product_id'=>$data['product_id'],
					'click_count'=>1,
					'click_source'=>3,	
                );
			$this->db->insert('users_product_click_track', $product_click_data);
            $this->db->insert('notify_me', $add_data);
			$insert_id = $this->db->insert_id();
			$query = $this->db->query("select * from notify_me where id = $insert_id ");	
			$res = $query->result();
			return $res[0];
	}
	
	function get_product_id($user_id){
		$query = $this->db->query("select product_id from cart_info where user_id = $user_id and `order_id` is null");
		$res = $query->result_array();
		return $res;
	}
	
	function su_get_order_price_info($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where order_unique_no = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info);

        $query = $this->db->query('select sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no, a.shipping_address,a.pincode,a.city_name,s.state_name from order_info as a,states as s where a.`state_name` = s.`id` and a.id in ('.$order_ids.')' );

            $query_res = $query->result_array();
            return $query_res;
        }
    }
	
	function su_get_order_info($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where order_unique_no = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

            $order_ids = implode(",",$order_info);

            $query = $this->db->query('select a.`order_id`, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`, a.`product_price`,b.`name`,b.`image`,b.`brand_id`,c.size_text,d.company_name from order_product_info as a,product_desc as b,product_size as c,`brand_info` as d where a.`product_id` = b.`id` and a.`order_id` in ('.$order_ids.') and a.`user_id`='.$user_id.' and a.product_size = c.`id` and d.`user_id` = b.`brand_id`' );

            $query_res = $query->result_array();
            return $query_res;

        }
    }
	
	function br_get_order_info($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where id = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

            $order_ids = implode(",",$order_info);

            $query = $this->db->query('select a.`order_id`, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`, a.`product_price`,b.`name`,b.`image`,b.`brand_id`,c.size_text,d.company_name from order_product_info as a,product_desc as b,product_size as c,`brand_info` as d where a.`product_id` = b.`id` and a.`order_id` in ('.$order_ids.') and a.`user_id`='.$user_id.' and a.product_size = c.`id` and d.`user_id` = b.`brand_id`' );

            $query_res = $query->result_array();
            return $query_res;

        }
    }
	
	function br_get_order_price_info($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where id = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info);

        $query = $this->db->query('select sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no, a.shipping_address,a.pincode,a.city_name,s.state_name,a.brand_id from order_info as a,states as s where a.`state_name` = s.`id` and a.id in ('.$order_ids.')' );

            $query_res = $query->result_array();
            return $query_res;
        }
    }
	
	function get_user_info($user_id){
        $query = $this->db->get_where('user_login', array('id' => $user_id));
        return $query->result_array();
    }
	
	function validate_pincode($pincode){
     $query = $this->db->query("select * from pincode where pincode = $pincode");
	 $res = $query->result_array();
	 if(!empty($res))
	 {return 1;
	}
	else return 2;
    }
	
	function add_product_click($user_id,$product_id,$type){   
	$data = array(
			'user_id' 		=> $user_id,
			'product_id' 	=> $product_id,
			'click_count' 	=> 1,
			'click_source'	=>$type
		);
	$this->db->insert('users_product_click_track', $data);
    }
	
	function transaction_failed($user_id,$order_unique_no){
		if($user_id != "" && $order_unique_no != ""){
		$order_ids = explode('_',$order_unique_no);
		$order_idhtml = '';
		if(!empty($order_ids)){
			for($i = 0; $i<count($order_ids); $i++){
					if($i != 0)
					{ 
						$order_idhtml = $order_idhtml.','; 
					}
					$order_idhtml = $order_idhtml.$order_ids[$i];
			}
		}
		//$query = $this->db->query('update cart_info SET order_id = NULL WHERE order_id IN ('.$order_idhtml.') AND user_id = "'.$user_id.'" ');
		$this->db->set('order_id', NULL);
		$this->db->where('user_id', $user_id);
		$this->db->where_in('order_id', $order_idhtml);
		$this->db->update('cart_info');
		
		$query = $this->db->query('delete from order_product_info WHERE order_id IN ('.$order_idhtml.') AND user_id = "'.$user_id.'" ');
		
		$query = $this->db->query('delete from order_info WHERE order_unique_no = "'.$order_unique_no.'" AND user_id = "'.$user_id.'"');
		
		return 1;
		
		}
		else return 2;
	}

	function get_state_id($pincode){
		$query = $this->db->query("select b.`id` as state_id from `city` as a,`states` as b,pincode as c where a.`state_id` = b.`id` AND a.id = c.city_id AND  c.`pincode`='".$pincode."'");
        $res = $query->result_array();
		if(!empty($res)){ return $res[0]['state_id']; }else{ return 0; }
	}
		
	function failed_order($first_name,$last_name,$email_id,$address,$user_id,$mobile_no,$paymod,$uc_pincode,$uc_city){
		$query = $this->db->query("select id from cart_info where `user_id` = '".$user_id."' AND `order_id` is null");
		$result = $query->result_array();
		//echo "comming";
		$var = $address.','.$uc_pincode.','.$uc_city;
		if(!empty($result))
		{ 
		$i= 0;
		 foreach($result as $row)
		 {	$date = date('Y-m-d H:i:s');
			 $add_data = array(
					//'SCUniqueID'=>$SCUniqueID,
                    'cart_id'=>$result[$i]['id'],
                    'first_name'=>$first_name,
                    'last_name'=>$last_name,
                    'email_id'=>$email_id,
                    'address'=>$var,
                    'mobile_no'=>$mobile_no,
					'created_datetime'=>$date,
					'paymod'=>$paymod,
                );
            $this->db->insert('fail_order', $add_data);
			$i++;
		 }
		}
		return true;
	}
	
	function update_fail_order_status($user_id,$cart_data,$payent_method){
		$count = count($cart_data);
		$cart_id_in = '';
		  if(!empty($cart_data)){
            $i = 0;
            foreach ($cart_data as $result) {
				if($i != 0){ $cart_id_in = $cart_id_in .',';  } 
                $cart_id_in = $cart_id_in . $cart_data[$i]['id'];
                $i++;
				
		  }}
		 $query = $this->db->query("update fail_order SET `status` = 1 where paymod = $payent_method AND `cart_Id` in ($cart_id_in)");
	}
	
	function update_fail_order_status_online($user_id,$order_id){
		$payent_method = '2';
	 	$order_ids = explode('_',$order_id);
		
		if(!empty($order_ids)){ 
			$order_id_count = count($order_ids); 
			for($i = 1; $i<$order_id_count; $i++){

				$order_id =$order_ids[$i];

				$query = $this->db->query("SELECT cart_id FROM `order_product_info` WHERE order_id ='".$order_id."'");
       	 		$res = $query->result_array();
				if(!empty($res)){
					foreach($res as $val){
						if($val['cart_id']>0 && $order_id>0){
						$data = array(
			               'status' => '1'
			            );
						$this->db->where(array('cart_id' => $val['cart_id'],'paymod'=>$payent_method));
			            $this->db->update('fail_order', $data); 
						}
					}
				}
			}
		}
    }
	
	function get_delivery_date($product_id){ 
		$query = $this->db->query("select b.min_del_days,b.max_del_days from product_desc a,brand_info b where a.`id` = $product_id AND a.brand_id = b.user_id");
		$result = $query->result_array();
		if(!empty($result)) return $result[0]; else return null;
	}
	
	// function get_discount_id($brand_id,$product_id)
    // { 
       // if($brand_id > 0){
           // $query = $this->db->query("select id, discount_type_id  from discount_availed as da where (da.brand_id = '".$brand_id."' ) AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1 ");
         // /*   $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = '".$brand_id."' AND da.discount_type_id='".$discount_type_id."' ) ");*/          
           // $result = $query->result_array();
           // if(!empty($result)){
               // return $result[0];
           // }else{
               // return 0;
           // }

       // }else{ 

           // if($product_id > 0){
               // $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = 0 AND da.product_id = '".$product_id."' ) AND da.status=1");
                // /*  $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = '".$product_id."' AND da.discount_type_id='".$discount_type_id."' ) ");*/
               // $result = $query->result_array();
               // if(!empty($result)){
                   // return $result[0];
               // }else{
                   // return 0;
               // }            
           // }else
           // {
               // $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = '".$brand_id."' AND da.product_id = 0 ) AND da.status=1");
                // /*  $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = '".$product_id."' AND da.discount_type_id='".$discount_type_id."' ) ");*/
               // $result = $query->result_array();
               // if(!empty($result)){
                   // return $result[0];
               // }else{
                   // return 0;
               // }          
           // }
        // }        
    // }
	
	function get_discount_id($brand_id,$product_id)
    { 
        if($brand_id == 0 && $product_id == 0){
           $query = $this->db->query("select id, discount_type_id,discount_percent,max_amount from discount_availed as da where (da.brand_id = '".$brand_id."' AND da.product_id = '".$product_id."' ) AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1 ");
                
            $result = $query->result_array();

            if(!empty($result)){
                return $result[0];
            }else{
                return 0;
            }
        }

        if($brand_id > 0 && $product_id > 0){
           $query = $this->db->query("select id, discount_type_id from discount_availed as da where (da.brand_id = '".$brand_id."' OR da.product_id = '".$product_id."' ) AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1 ");
          /*   $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = '".$brand_id."' AND da.discount_type_id='".$discount_type_id."' ) ");*/          
            $result = $query->result_array();

            if(!empty($result)){
                return $result[0];
            }else{
                return 0;
            }
        }else if($brand_id > 0){
            $query = $this->db->query("select id, discount_type_id from discount_availed as da where (da.brand_id = '".$brand_id."' ) AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1 ");
          /*   $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = '".$brand_id."' AND da.discount_type_id='".$discount_type_id."' ) ");*/          
            $result = $query->result_array();
            if(!empty($result)){
                return $result[0];
            }else{
                return 0;
            }

        }else{ 

            if($product_id > 0){
                $query = $this->db->query("select da.id, da.discount_type_id from discount_availed as da where (da.brand_id = 0 AND da.product_id = '".$product_id."' ) AND da.status=1");
                 /*  $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = '".$product_id."' AND da.discount_type_id='".$discount_type_id."' ) ");*/
                $result = $query->result_array();
                if(!empty($result)){
                    return $result[0];
                }else{
                    return 0;
                }            
            }else
            {
                $query = $this->db->query("select da.id, da.discount_type_id from discount_availed as da where (da.brand_id = '".$brand_id."' AND da.product_id = 0 ) AND da.status=1");
                 /*  $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = '".$product_id."' AND da.discount_type_id='".$discount_type_id."' ) ");*/
                $result = $query->result_array();
                if(!empty($result)){
                    return $result[0];
                }else{
                    return 0;
                }          
            }
         }  
    }
	
	function get_discount_percentage($brand_id,$product_id){
        if($brand_id > 0){
            $query = $this->db->query("select discount_percent from discount_availed as da where (da.brand_id = '".$brand_id."' || da.product_id = '".$product_id."') AND NOW() >= da.start_date AND NOW() <= da.end_date");
            $result = $query->result_array();
            if(!empty($result)){
                return $result[0]['discount_percent'];
            }else{
                return 0;
            }
        }else{ return 0; }
    }
	
	function applyCoupon($coupon_code,$brand_ids,$userid){
		$cartItemarray = explode('_',$brand_ids);
		$coupon_products = '';
        $logged_in_user ='';     
        $coupon_info = array();
		
        $logged_in_user = '';
		$res = $this->get_user_info($userid);
		if(!empty($res)){
        $logged_in_user = $res[0]['email'];
		}else{ $logged_in_user = ''; }
		
        if(!empty($cartItemarray))
        {
            foreach($cartItemarray as $val)
            {
                if($val!='' && $coupon_code!='')
                { 
                    //$query = $this->db->query("select a.products,a.coupon_amount,a.brand_id FROM coupon_settings as a WHERE  NOW() >= a.coupon_start_datetime AND NOW() <= a.coupon_end_datetime AND a.`status`=1 AND a.`is_delete`=0 AND a.`coupon_code`='".$couponCode."'");
                  
                    $query = $this->db->query("SELECT `individual_use_only`, `coupon_code`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `brand_id`, `products`, `exclude_products`,`coupon_email_applied`,`stylecrackerDiscount`,`individual_use_only` FROM `coupon_settings` WHERE NOW() BETWEEN `coupon_start_datetime` AND `coupon_end_datetime` AND`status`=1 AND `is_delete`=0 AND (brand_id = '".$val."' || brand_id=0) AND `usage_limit_per_coupon`>`coupon_used_count` AND `coupon_code`='".$coupon_code."' "); 
                    //return $this->db->last_query();
                    $result = $query->result_array();
                   
                    if(!empty($result))
                    {
                       $coupon_info['coupon_products'] =  $coupon_products.$result[0]['products'];
                       $coupon_info['coupon_code'][$result[0]['brand_id']] = $result[0]['coupon_amount']; 
                       $coupon_info['min_spend'][$result[0]['brand_id']] = $result[0]['min_spend']; 
                       $coupon_info['max_spend'][$result[0]['brand_id']] = $result[0]['max_spend'];

                       $individual_use_only =  $result[0]['individual_use_only']; 
                       $coupon_stylecracker =   $result[0]['stylecrackerDiscount']; 
                       $apply_email = $result[0]['coupon_email_applied'];                      
                       
                          
                        if($result[0]['brand_id']==0 && $coupon_stylecracker==1 && ($individual_use_only==1 && $logged_in_user=='' ) )
                       {
                         return 'NotLoggedIn';
                       }else
                       {
                          if($apply_email!="" &&  $individual_use_only == 1)
                         {
                            $apply_email_array = explode(',',$apply_email);
                           if(in_array($logged_in_user, $apply_email_array))
                           {
                               return 1;//coupon code valid
                           }else
                           {
                              return 2; //coupon code not valid
                           }
                         }
                       }

                    }                    
                }                
            }           
        }

        if(!empty($coupon_info))
        {          
          return 1; //coupon code valid
        }else
        {
            return 2; //coupon code not valid
        }
		
	}
	
	function getCouponData($couponCode,$CbrandId=NULL)
    {
		if($CbrandId!='')
        {
          //$brand_cond =' AND `brand_id`='.$CbrandId;
          $brand_cond =' AND (brand_id = "'.$CbrandId.'" || brand_id=0) ';
        }else
        {
          $brand_cond = '';
        }
		
        $query = $this->db->query("SELECT `coupon_code`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `brand_id`, `products`, `exclude_products`,`coupon_used_count`,`usage_limit_per_coupon`,	is_special_code FROM `coupon_settings` WHERE NOW() BETWEEN `coupon_start_datetime` AND `coupon_end_datetime` AND`status`=1 AND `is_delete`=0 AND `usage_limit_per_coupon`>`coupon_used_count` AND `coupon_code`='".$couponCode."' ".$brand_cond." "); 
            //return $this->db->last_query();
            $result = $query->result_array();
           
            if(!empty($result))
            {
              /* $coupon_info['coupon_products'] =  $coupon_products.$result[0]['products'];*/
               $coupon_info['coupon_amount'] = $result[0]['coupon_amount']; 
               $coupon_info['brand_id'] = $result[0]['brand_id']; 
               $coupon_info['coupon_products'] = $result[0]['products'];
               $coupon_info['coupon_min_spend'] = $result[0]['min_spend'];
               $coupon_info['coupon_max_spend'] = $result[0]['max_spend'];
               $coupon_info['coupon_discount_type'] = $result[0]['discount_type'];
               $coupon_info['coupon_used_count'] = $result[0]['coupon_used_count'];
               $coupon_info['usage_limit_per_coupon'] = $result[0]['usage_limit_per_coupon'];
			   $coupon_info['is_special_code'] = $result[0]['is_special_code'];
            }  

            if(!empty($coupon_info))
            {
              return $coupon_info;
            }else
            {
                return 0;
            }               
    }
	
	function update_coupon_counter($couponCode)
    {   
        if($couponCode!='')
        {

          $coupon_used_count = $this->getCouponData($couponCode)['coupon_used_count'];
          $usage_limit_per_coupon = $this->getCouponData($couponCode)['usage_limit_per_coupon'];

          if($coupon_used_count<=$usage_limit_per_coupon)
          {
              $coupon_used_count++;
              $this->db->where('coupon_code',$couponCode);
              $result = $this->db->update('coupon_settings', array('coupon_used_count'=>$coupon_used_count));         
              if($result == TRUE)
              {
                return TRUE;
              }else
              {
                return FALSE;
              }            
          }else
          {
              return FALSE;
          }
          
        }       
    }
	
	function getProductSku($productId,$productSize)
    {
        if($productId!="" && $productSize!="")
        {
          $query = $this->db->query("select product_sku from product_inventory where product_id='".$productId."' AND size_id='".$productSize."'; ");
           /* echo $this->db->last_query();*/
            $result = $query->result_array();           
        }   

        if(!empty($result))
        {
          return $result[0]['product_sku'];
        }else
        {
          return '';
        }     
          
    }
	
	/*Referal Start*/
	function redeem_points($unique_id,$user_id){
		
         $query2 = $this->db->query("select `id`, `user_id`, `user_email`, `type`, `description`, `points`, `date`, `source`, `source_id`, `source_email`, `status`, `invite_order_id` from referral_info where user_id=".$user_id." and type='credit' and `status`!='done' limit 0,1");
                   $res2 = $query2->result_array();
                   if(!empty($res2)){
                   $data = array();
                   $data = array(
                        'user_id' => $res2[0]['user_id'],
                        'user_email'=>$res2[0]['user_email'],
                        'type'=>'debit',
                        'description'=>$res2[0]['description'],
                        'points'=>POINT_VAlUE,
                        'source'=>'order',
                        'source_id'=>$res2[0]['source_id'],
                        'source_email'=>$res2[0]['source_email'],
                        'status'=>'done',
                        'invite_order_id'=>$unique_id
                    );
                    $this->db->insert('referral_info', $data);

                    $data1=array('status'=>'done');
                    $this->db->where('id',$res2[0]['id']);
                    $this->db->update('referral_info',$data1);
                    
                    }
       
    }
	
	 function get_refre_amt($order_id){
        $query = $this->db->query('select points from referral_info where invite_order_id ="'.$order_id.'"');
        $res = $query->result_array();
        return @$res[0]['points'];
    }
	
	function getuser_data($user_id){
		$query = $this->db->query("select a.*,b.confirm_email as confirm_email,(select c.confirm_email from user_login as c where c.email = source_email ) as source_email_confirm from referral_info as a,user_login as b where a.user_id = $user_id AND b.id = a.user_id AND a.status != 'done' ");
		$result = $query->result_array();
		return $result; 
		
	}
	
	/*Referal End*/
	
	function delete_address($user_id,$address_id){
		$query = $this->db->query('delete from user_address WHERE user_id = "'.$user_id.'" AND id = "'.$address_id.'" ');
		return true;
	}
	
	function update_useraddress($data){
		
		$add_data = array(
			'user_id'=>$data['user_id'],
			'shipping_address'=>$data['uc_address'],
			'pincode'=>$data['uc_pincode'],
			'city_name'=>$data['uc_city'],
			'state_name'=>$data['uc_state'],
			'first_name'=>$data['uc_first_name'],
			'email'=>$data['uc_email_id'],
			'mobile_no'=>$data['uc_mobile'],
			'last_name'=>$data['uc_last_name'],
		);
		$this->db->where(array('id'=>$data['address_id'],'user_id'=>$data['user_id']));
		$this->db->update('user_address', $add_data);
		$all_address = $this->user_info($data['address_id']);
		return $all_address;
	}
	
	function clear_cart($userid,$cart_ids){
		$query = $this->db->query("delete from `cart_info` where `user_id` = '".$userid."' AND `id` IN(".$cart_ids.")");
		return true;
	}
	
	function get_coupons($userid,$user_email,$page_type=null){
		 
		if($page_type=='scboxcart')
		{
			$query = $this->db->query('select a.id,a.coupon_code,a.coupon_desc,a.show_on_web,a.coupon_amount,a.min_spend,a.max_spend,a.bin_no,a.is_special_code,
			a.coupon_email_applied,a.coupon_end_datetime,a.usage_limit_to_user from coupon_settings as a where a.coupon_start_datetime < now() AND now() < a.coupon_end_datetime AND a.is_delete = 0 AND a.status = 1 AND a.brand_id = 0 AND a.usage_limit_per_coupon > a.coupon_used_count AND a.show_on_web = 1 AND a.is_special_code=1 order by a.id desc');
			$result = $query->result_array();			
			
		}else
		{
			$query = $this->db->query('select a.id,a.coupon_code,a.coupon_desc,a.show_on_web,a.coupon_amount,a.min_spend,a.max_spend,a.bin_no,
			a.coupon_email_applied,a.coupon_end_datetime from coupon_settings as a where a.coupon_start_datetime < now() AND now() < a.coupon_end_datetime AND a.is_delete = 0 AND a.status = 1 AND a.brand_id = 0 AND a.usage_limit_per_coupon > a.coupon_used_count AND a.show_on_web = 1 AND a.is_special_code != 1 order by a.id desc');
			$result = $query->result_array();
		}
		$coupon_array = array();$i=0;
		foreach($result as $val){
			// $date = explode(' ',$val['coupon_end_datetime']);
			$date = $val['coupon_end_datetime'];
			if($val['coupon_code'] == 'YESBOX500'){
				
				$coupon_array[$i]['id'] = $val['id'];
				$coupon_array[$i]['coupon_code'] = $val['coupon_code'];
				$coupon_array[$i]['coupon_desc'] = $val['coupon_desc'];
				$coupon_array[$i]['coupon_amount'] = $val['coupon_amount'];
				$coupon_array[$i]['min_spend'] = $val['min_spend'];
				$coupon_array[$i]['coupon_title'] = 'YESBANK INR 500 off on SC Box';
				$coupon_array[$i]['long_desc'] = 'long desc';
				$coupon_array[$i]['short_desc'] = $val['coupon_desc'];
				$coupon_array[$i]['coupon_type'] = '1';
				$coupon_array[$i]['place_holder'] = 'Enter BIN number';
				$coupon_array[$i]['coupon_end_datetime'] = date("d-m-Y h:i:s", strtotime(@$date));
				$i++;
			}else if($val['coupon_code'] == 'YESBOX750'){
				
				$coupon_array[$i]['id'] = $val['id'];
				$coupon_array[$i]['coupon_code'] = $val['coupon_code'];
				$coupon_array[$i]['coupon_desc'] = $val['coupon_desc'];
				$coupon_array[$i]['coupon_amount'] = $val['coupon_amount'];
				$coupon_array[$i]['min_spend'] = $val['min_spend'];
				$coupon_array[$i]['coupon_title'] = 'YESBANK INR 750 off on SC Box';
				$coupon_array[$i]['long_desc'] = 'long desc';
				$coupon_array[$i]['short_desc'] = $val['coupon_desc'];
				$coupon_array[$i]['coupon_type'] = '1';
				$coupon_array[$i]['place_holder'] = 'Enter BIN number';
				$coupon_array[$i]['coupon_end_datetime'] = date("d-m-Y h:i:s", strtotime(@$date));
				$i++;
			}else if($val['coupon_code'] == 'BMS500'){
				
				$coupon_array[$i]['id'] = $val['id'];
				$coupon_array[$i]['coupon_code'] = $val['coupon_code'];
				$coupon_array[$i]['coupon_desc'] = $val['coupon_desc'];
				$coupon_array[$i]['coupon_amount'] = $val['coupon_amount'];
				$coupon_array[$i]['min_spend'] = $val['min_spend'];
				$coupon_array[$i]['coupon_title'] = 'BookMyShow &#8377; 500 off';
				$coupon_array[$i]['long_desc'] = '<div style="text-align:left">The discount will be deducted and reflected at the payment gateway page.<br/>Click on Checkout > Online payment > Enter card details > Check offer<br/>A green notification will appear stating your debit/credit card is eligible for the discount.<br/><br/>Click on Pay now, discount will reflect and payment will be processed.</div>';
				$coupon_array[$i]['short_desc'] = '* <font color="red">This offer is applicable only if the final payment is made through BMS Virtual Visa card </font>';
				$coupon_array[$i]['coupon_type'] = '2';
				$coupon_array[$i]['place_holder'] = 'Enter BIN number';
				$coupon_array[$i]['coupon_end_datetime'] = date("d-m-Y h:i:s a", strtotime(@$date));
				$coupon_array[$i]['offer_code'] = "@2437";
				$i++;
			}else if($val['coupon_code'] == 'YES300'){
				
				$coupon_array[$i]['id'] = $val['id'];
				$coupon_array[$i]['coupon_code'] = $val['coupon_code'];
				$coupon_array[$i]['coupon_desc'] = $val['coupon_desc'];
				$coupon_array[$i]['coupon_amount'] = $val['coupon_amount'];
				$coupon_array[$i]['min_spend'] = $val['min_spend'];
				$coupon_array[$i]['coupon_title'] = 'YES BANK INR 300 off';
				$coupon_array[$i]['long_desc'] = '<div style="text-align:left">The discount will be deducted and reflected at the payment gateway page.<br/>Click on Checkout > Online payment > Enter card details > Check offer<br/>A green notification will appear stating your debit/credit card is eligible for the discount.<br/><br/>Click on Pay now, discount will reflect and payment will be processed.</div>';
				$coupon_array[$i]['short_desc'] = '* <font color="red">This offer is applicable only if the final payment is made through YES BANK Debit/Credit Card </font>';
				$coupon_array[$i]['coupon_type'] = '2';
				$coupon_array[$i]['place_holder'] = 'Enter BIN number';
				$coupon_array[$i]['coupon_end_datetime'] = date("d-m-Y h:i:s a", strtotime(@$date));
				$coupon_array[$i]['offer_code'] = "@2760";
				$i++;
				
			}else if($val['coupon_code'] == 'BMS15'){
				
				$coupon_array[$i]['id'] = $val['id'];
				$coupon_array[$i]['coupon_code'] = $val['coupon_code'];
				$coupon_array[$i]['coupon_desc'] = $val['coupon_desc'];
				$coupon_array[$i]['coupon_amount'] = $val['coupon_amount'];
				$coupon_array[$i]['min_spend'] = $val['min_spend'];
				$coupon_array[$i]['coupon_title'] = 'BookMyShow 15% off on SC Box';
				$coupon_array[$i]['long_desc'] = 'Please enter your BIN number to avail the offer. <br/>
													What is BIN number?<br/>
													A Bank Identification Number or BIN number is the initial four to six numbers that appear on your BMS Virtual card.<br/>
													About the offer<br/>
													Valid on – All products listed on the StyleCracker website and mobile app<br/>
													Valid till – '.date("d-m-Y h:i:s a", strtotime(@$date)).' <br/>
													Who can use it – All BMS Virtual card holders can use this offer to shop directly from the StyleCracker website or mobile app<br/>
													What is it about – The customer gets &#8377;.500 off on a minimum purchase of Rs. 1,000 <br/>
													How many times can it be used – The offer can be availed more than once and the transaction can take place on any day of the week <br/>';
				$coupon_array[$i]['short_desc'] = $val['coupon_desc'];
				$coupon_array[$i]['coupon_type'] = '2';
				$coupon_array[$i]['place_holder'] = 'Enter BIN number';
				$coupon_array[$i]['coupon_end_datetime'] = date("d-m-Y h:i:s a", strtotime(@$date));
				$coupon_array[$i]['offer_code'] = "@2437";
				$i++;
			}else if($val['coupon_code'] == 'YESBANK1000'){
				
				$coupon_array[$i]['id'] = $val['id'];
				$coupon_array[$i]['coupon_code'] = $val['coupon_code'];
				$coupon_array[$i]['coupon_desc'] = $val['coupon_desc'];
				$coupon_array[$i]['coupon_amount'] = $val['coupon_amount'];
				$coupon_array[$i]['min_spend'] = $val['min_spend'];
				$coupon_array[$i]['coupon_title'] = 'YES BANK 15% Discount';
				$coupon_array[$i]['long_desc'] = 'YES BANK long desc';
				$coupon_array[$i]['short_desc'] = $val['coupon_desc'];
				$coupon_array[$i]['coupon_type'] = '2';
				$coupon_array[$i]['place_holder'] = 'Enter BIN number';
				$coupon_array[$i]['coupon_end_datetime'] = date("d-m-Y h:i:s a", strtotime(@$date));
				$coupon_array[$i]['offer_code'] = "";
				$i++;
				
			}else{
				$coupon_email_applied = $val['coupon_email_applied'];
				$coupon_users = array();
				$coupon_users = explode(',',$coupon_email_applied);
				// echo $coupon_email_applied.'sfvsdvdsvsdvb              ';
				if(!empty($coupon_users) && in_array($user_email,$coupon_users) == 1 && $coupon_email_applied != '0'){
					$coupon_array[$i]['id'] = $val['id'];
					$coupon_array[$i]['coupon_code'] = $val['coupon_code'];
					$coupon_array[$i]['coupon_desc'] = $val['coupon_desc'];
					$coupon_array[$i]['coupon_amount'] = $val['coupon_amount'];
					$coupon_array[$i]['min_spend'] = $val['min_spend'];
					$coupon_array[$i]['coupon_title'] = $val['coupon_code'];
					$coupon_array[$i]['long_desc'] = 'Coupon long desc';
					$coupon_array[$i]['short_desc'] = 'Coupon short desc';
					$coupon_array[$i]['coupon_type'] = '1';
					$coupon_array[$i]['place_holder'] = '';
					$coupon_array[$i]['coupon_end_datetime'] = date("d-m-Y h:i:s a", strtotime(@$date));
					$coupon_array[$i]['offer_code'] = "";
					$i++;
				}else if($coupon_email_applied == '0'){
					$coupon_array[$i]['id'] = $val['id'];
					$coupon_array[$i]['coupon_code'] = $val['coupon_code'];
					$coupon_array[$i]['coupon_desc'] = $val['coupon_desc'];
					$coupon_array[$i]['coupon_amount'] = $val['coupon_amount'];
					$coupon_array[$i]['min_spend'] = $val['min_spend'];
					$coupon_array[$i]['coupon_title'] = $val['coupon_code'];
					$coupon_array[$i]['long_desc'] = 'Coupon long desc';
					$coupon_array[$i]['short_desc'] = 'Coupon short desc';
					$coupon_array[$i]['coupon_type'] = '1';
					$coupon_array[$i]['place_holder'] = '';
					$coupon_array[$i]['coupon_end_datetime'] = date("d-m-Y h:i:s a", strtotime(@$date));
					$coupon_array[$i]['offer_code'] = "";
					$i++;
				}
				
			}
			
		}
		
		return $coupon_array;
	}
	
	function check_coupon_exist($coupon,$user_email_id,$cart_product_cost,$bin_no){
		
        $discount_amount = 0; 
		$response = array(); 
		$cart_brand = array();
        $query = $this->db->query("SELECT `id`, `coupon_code`, `coupon_desc`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `individual_use_only`, `exclude_sale_items`, `brand_id`, `products`, `exclude_products`, `prd_category`, `exclude_prd_category`, `email_restriction`, `usage_limit_per_coupon`, `usage_limit_to_items`, `usage_limit_to_user`, `coupon_used_count`, `coupon_email_applied`, `stylecrackerDiscount`, `status`, `is_delete`, `created_datetime`, `created_by`, `modified_datetime`, `modified_by`,bin_no FROM `coupon_settings` WHERE 1 and LOWER(coupon_code) = '".$coupon."' and coupon_start_datetime <= now() and coupon_end_datetime >= now() and status = 1 and is_delete = 0 and usage_limit_per_coupon!=coupon_used_count limit 0,1");
        $res = $query->result_array();
		// print_r($res);exit;
        if(!empty($res)){
			
            $coupon_amt = 0; $coupon_per = 0; 

            $individual_use_only = $res[0]['individual_use_only'];
            $coupon_email_applied = $res[0]['coupon_email_applied'];
            $brand_id = $res[0]['brand_id'];
            $products = $res[0]['products'];
            $discount_type = $res[0]['discount_type'];
            $coupon_amount = $res[0]['coupon_amount'];
            $min_spend = $res[0]['min_spend'];
            $max_spend = $res[0]['max_spend'];
            $stylecrackerDiscount  = $res[0]['stylecrackerDiscount'];
			$db_bin_no  = $res[0]['bin_no'];
			$coupon_peruser_limit  = $res[0]['usage_limit_to_user'];

            if($discount_type == '1' || $discount_type == '3'){
                $coupon_amt = $coupon_amount;
            }else{
                $coupon_per = $coupon_amount;
            }

            $cart_users = array();
            $cart_users = explode(',',$coupon_email_applied);
			$coupon = strtoupper($coupon);
			/*
			// added for bms and yes bank
			if(($coupon == 'BMS15' || $coupon == 'YESBANK1000') && $bin_no == ''){
				$response['msg'] = 'This offer is for special users';
				$response['msg_type'] = '0';
				$response['coupon_code'] = $coupon;
				return $response;
			}else if(($coupon == 'BMS15' || $coupon == 'YESBANK1000') && $bin_no != ''){
				$bin_nos = explode(',',$db_bin_no);
				if(!in_array($bin_no,$bin_nos)){
					$response['msg'] = 'Please enter valid numbers';
					$response['msg_type'] = '0';
					$response['coupon_code'] = $coupon;
					return $response;
				}
			}
			// added for bms and yes bank
			*/
            if($individual_use_only == 1 && $coupon_email_applied!='' && $user_email_id=='' && $stylecrackerDiscount!=1){
                $response['msg'] = 'Please login to apply coupon code';
                $response['msg_type'] = '0';
                $response['coupon_code'] = '';
                return $response;
            }else if($individual_use_only == 1 && $coupon_email_applied!='' && $user_email_id!='' && in_array($user_email_id,$cart_users) == 1){
                /* Abendant Cart Discount Userwise*/
               
                if($cart_product_cost > 0 && !empty($cart_users)){
					
                    if($min_spend == 0 && $max_spend==0){
                        $cart_product_cost = $cart_product_cost;   
                    }else if($min_spend <= $cart_product_cost && $max_spend >= $cart_product_cost){
                        $cart_product_cost = $cart_product_cost;  
                    }else{
                        $cart_product_cost = 0;
                    }
                }
				
                if($cart_product_cost > 0){
                    if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                    }
                }
                if($discount_amount > 0){ 
					$response['msg'] = 'Coupon Applied'; $response['brand_id'] = 0; $response['msg_type'] = '1'; 
					$response['coupon_code'] = strtoupper($coupon);
				} else{
                    
                    $response['msg'] = 'Userwise Discount'; $response['msg_type'] = '0';
                    $response['coupon_code'] = strtoupper($coupon);
                }
                $response['coupon_discount_amount'] = $discount_amount;
                return $response;

            }else if($brand_id == 0 && $products == 0 && $stylecrackerDiscount==1){
                /* StyleCracker Discount */
                
                if($cart_product_cost > 0){
					
                    if($min_spend == 0 && $max_spend==0){
                        $cart_product_cost = $cart_product_cost;   
                    }else if($min_spend <= $cart_product_cost && $max_spend >= $cart_product_cost){
                        $cart_product_cost = $cart_product_cost;  
                    }else{
                        $cart_product_cost = 0;
                    }
                }

                if($cart_product_cost > 0){ 
                    if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $coupon_amount = $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                        $coupon_amount = $coupon_amount/100; 
                    }
                }

               /* if($coupon_peruser_limit > 0)
                {
                	if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $coupon_amount = $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                        $coupon_amount = $coupon_amount/100; 
                    }
                }*/

                if($discount_amount > 0){ 
                    
                    $response['msg'] = 'Coupon Applied';$response['brand_id'] = 0;$response['msg_type'] = '1'; 
                    $response['coupon_code'] = strtoupper($coupon);
                     } else{
                    
                    $response['msg'] = 'Coupon will apply if you spend minimum '.$min_spend.' on StyleCracker';
                    $response['msg_type'] = '0'; 
                    $response['coupon_code'] = '';
                }
                $response['coupon_discount_amount'] = $discount_amount;
                return $response;
            }else if($brand_id > 0 && $products == 0){
                /* Brands Discount */
               
                if($cart_product_cost > 0){
					
                    if($min_spend == 0 && $max_spend==0){
                        $cart_product_cost = $cart_product_cost;   
                    }else if($min_spend <= $cart_product_cost && $max_spend >= $cart_product_cost){
                        $cart_product_cost = $cart_product_cost;  
                    }else{
                        $cart_product_cost = 0;
                    }
                }

                if($cart_product_cost > 0 ){
                    if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                    }
                }
                if($discount_amount > 0){
					
					$response['msg'] = 'Coupon Applied'; $response['brand_id'] = $brand_id; $response['msg_type'] = '1'; 
					$response['coupon_code'] = strtoupper($coupon);
				} else{
					
					$response['msg'] = 'Coupon will apply if you spend minimum '.$min_spend.' on '.$this->getBrandExtraInfo($brand_id)[0]['company_name'].'s products';
					$response['msg_type'] = '0';
					$response['coupon_code'] = ''; 
                }
                $response['coupon_discount_amount'] = $discount_amount;
                return $response;
            }else if($brand_id > 0 && $products > 0){
               
                $cart_products = array();
                $cart_products = explode(',',$products);
                
                  if($cart_product_cost > 0){
					
                    if($min_spend == 0 && $max_spend==0){
                        $cart_product_cost = $cart_product_cost;   
                    }else if($min_spend <= $cart_product_cost && $max_spend >= $cart_product_cost){
                        $cart_product_cost = $cart_product_cost;  
                    }else{
                        $cart_product_cost = 0;
                    }
                }

                if($cart_product_cost > 0 ){
                    if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                    }
                } 
                if($discount_amount > 0){
                    $response['msg'] = 'Coupon Applied'; $response['brand_id'] = $brand_id; $response['msg_type'] = '1'; 
                    $response['coupon_code'] = strtoupper($coupon);
                } else{
                    $response['msg'] = 'Coupon code will be applied for specific products'; $response['msg_type'] = '0';
                    $response['coupon_code'] = '';
                }
                $response['coupon_discount_amount'] = $discount_amount;

                
                return $response;
            }

        }else{
            $response['msg'] = 'No Coupon Found or Expired';
            $response['msg_type'] = '0';
            $response['coupon_code'] = '';
            $response['coupon_discount_amount'] = '0';
            return $response;
        }
    }
	
	function check_is_special_code($coupon_code){
		//$query = $this->db->query("SELECT a.`id`, a.`coupon_code`from coupon_settings as a WHERE 1 and LOWER(a.coupon_code) = '".$coupon_code."' AND a.is_special_code != 2 AND a.is_special_code != 3 limit 0,1 ");
		$query = $this->db->query("SELECT a.`id`, a.`coupon_code`from coupon_settings as a WHERE 1 and LOWER(a.coupon_code) = '".$coupon_code."' AND a.is_special_code != 2 AND a.is_special_code != 3 AND a.usage_limit_to_user=0 limit 0,1 ");
		$result = $query->result_array();
		if(!empty($result)){
			return '1';
		}else return '0';
	}
	
	function get_coupon_info($coupon_code){
		$query = $this->db->query("SELECT a.`id`, a.`coupon_code`,a.is_special_code from coupon_settings as a WHERE 1 and LOWER(a.coupon_code) = '".$coupon_code."' ");
		$result = $query->result_array();
		return $result;
		// print_r($result);
	}
	
	function remove_scbox_product_ids($user_id){
		$query = $this->db->query("delete from `cart_info` where `product_id` IN(".SCBOX_PRODUCTID_STR.") AND `user_id` = '".$user_id."' ");
		return 1;
	}

	public function getSereializeProductDetail($objectMetaKey = '', $oid='', $uid='', $objectId= '')
	{	
		$this->db->select('object_meta_value');
		$this->db->from('object_meta');
		$this->db->where('object_meta_key', $objectMetaKey);
		//$this->db->where('created_by', $uid);
		//$this->db->where('object_id IN ('.$objectId.')');
		$this->db->where('object_id',$objectId);
		$query = $this->db->get();
		/* echo $this->db->last_query();
		exit(); */
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		return false;
	}

	 function get_object($objectslug)
    {
        $query = $this->db->get_where('object', array('object_slug' => $objectslug ));
        $result = $query->result_array();
        return $result;
    }

    public function getMetaValueData($objectId= '')
	{
		$this->db->select('id,product_cat_id,brand_id,name,description,price,image,status,approve_reject');
		$this->db->from('product_desc');
		$this->db->where('id', $objectId);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		return false;
	}

	public function updateObjectMeta($arrData = array(), $objectkey='',$selorderid='')
  	{	  
		$query = $this->db->get_where('object_meta', array('object_id' => $objectkey,'object_meta_key' => $selorderid ));
        $count = $query->num_rows(); //counting result from query		
        if($count>0){
                /*Update the PA*/
                  $this->db->where('object_id',$objectkey);
				  $this->db->where('object_meta_key',$selorderid);
				  $this->db->update('object_meta',$arrData);
				  //echo $this->db->last_query();
				  return true;
        }else{
            /*Insert the PA*/            
				  $this->db->where('object_id',$objectkey);
				  $this->db->where('object_meta_key',$selorderid);
				  $this->db->insert('object_meta',$arrData);
				  return true;
        }
	 
  	}
	
	public function get_cart_id_by_order_id($order_id='')
	{
		$this->db->select('id,product_id,product_size,product_qty,SCUniqueID,order_id,product_price');
		$this->db->from('cart_info');
		$this->db->where('order_id',$order_id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		return false;
	}
	
	
}


?>