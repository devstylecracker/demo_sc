<?php
class Brand_category_model extends MY_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_brand_product($brand_name,$category_slug,$offset=null){
		// echo $offset;exit;
		$product_table = 'product_desc';
		$where_cond = '';
		$category_id = 0;
		$offset = ($offset - 1) * 12;
		
			if($brand_name!=''){
				
				// $query = $this->db->get_where('brand_info', array('company_name'=>$brand_name));
				 $query = $this->db->query('select user_id from brand_info as a,user_login as b where b.user_name = "'.$brand_name.'" AND b.id = a.user_id ');
				$brand_info = $query->result_array();
				// echo "<pre>";print_r($brand_info);exit;
				if(!empty($brand_info)){$brand_id = @$brand_info[0]['user_id']; $where_cond = $where_cond.' and a.brand_id='.@$brand_info[0]['user_id'];
				}
				// echo $this->db->last_query();exit;
			}
			
			if($category_slug!=''){
				
				$cat_table_name = 'product_category';
				$category_level = 'cat';
				$product_table='product_desc';
				$query = $this->db->get_where($cat_table_name, array('status' => '1','slug'=>$category_slug));
				$res = $query->result_array();
				if(!empty($res)){$category_id = @$res[0]['id']; }
				
			}
			
			if($category_id <= 0){
				$cat_table_name = 'product_sub_category';
				$category_level = 'sub_cat';
				$product_table='product_desc';
				$query = $this->db->get_where($cat_table_name, array('status' => '1','slug'=>$category_slug));
				$res = $query->result_array();
				if(!empty($res)){$category_id = @$res[0]['id']; }
				
			}
			
			if($category_id <= 0){
				
				$cat_table_name = 'product_variation_type';
				$category_level = 'variations_type';
				$product_table='product_variations';
				$query = $this->db->get_where($cat_table_name, array('status' => '1','slug'=>$category_slug));
				$res = $query->result_array();
				if(!empty($res)){$category_id = @$res[0]['id']; }
				
			}
			
			if($category_id <= 0){
				
				$cat_table_name = 'product_variation_value';
				$category_level = 'variations';
				$product_table='product_variations';
				$query = $this->db->get_where($cat_table_name, array('status' => '1','slug'=>$category_slug));
				$res = $query->result_array();
				if(!empty($res)){$category_id = @$res[0]['id']; }
				
			}
			
			// echo "   cat_table_name = ".$cat_table_name."<br>category_level = ".$category_level."<br>category_id = ".$category_id."<br>category_slug = ".$category_slug."<br>product_table = ".$product_table;
			
			if($category_id > 0 && $product_table != ''){
               if($product_table == 'product_desc'){
                    if($category_level == 'cat'){ 
                        $where_cond = $where_cond.' and a.product_cat_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and a.product_sub_cat_id='.$category_id; 
                    }
                    
                    $query = $this->db->query('select a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a WHERE (a.approve_reject = "A" || a.approve_reject = "D") and a.is_delete = 0 '.$where_cond.' order by a.id desc  limit '.$offset.',12');
                    $product_data = $query->result_array();
                    
                }else if($product_table == 'product_variations'){
                    if($category_level == 'variations_type'){ 
                        $where_cond = $where_cond.' and b.product_var_type_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and b.product_var_val_id='.$category_id; 
                    }
					
                    $query = $this->db->query('select a.id,a.name,a.price,a.image,a.slug,a.brand_id from product_desc as a,product_variations as b WHERE (a.approve_reject = "A" || a.approve_reject = "D") and a.is_delete = 0 and a.id = b.product_id '.$where_cond.' order by a.id desc  limit '.$offset.',12');
                    $product_data = $query->result_array();
                } 
				
				$data['products'] = $product_data;
				return $data;				
            }
			
	}
	
}