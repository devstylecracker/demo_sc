<?php
class Favorites_model extends MY_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function add_fav($data){
        $data = array(
                       'fav_for' => $data['fav_for'] ,
                       'fav_id' => $data['fav_id'] ,
                       'user_id' => $data['user_id'],
                       'is_deleted' => 0,
                    );
        $this->db->delete('favorites', $data); 
        $deleted = $this->db->affected_rows();
        if($deleted == 0){   

            if($this->db->insert('favorites', $data))
                return 1;
            else
                return 0;
        }else{
            return 2;
        }
        exit;
        
    }
}