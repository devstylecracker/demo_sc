<?php
class Mobile_filter_model extends MY_Model {
	function __construct()
    {
        // Call the Model constructor
        $this->slug = '';
        parent::__construct();
    }

    function get_data($cat=null,$brands=null,$colors=null,$price=null,$discount=null,$sort=0,$offset=0,$lastlevelcat=null,$min_price=0,$max_price=0,$attri=null,$sizeId=null,$search=null,$is_recommended=0,$is_brand=0,$new_limit=null,$new_offset=null,$user_id=null){
        $data = array(); $cat_id=''; $product_id = '0';
        $brand = ''; $limit = 12;
        $g = []; $w_c = ''; $w_c1 = ''; $cat_id=0;
        $filters[] = '';
        $order_by = 'a.modified_datetime desc';
        //$order_by = $this->filter_product_sort();
        $offset = $offset*$limit;
    
        $cat_id = $this->get_id_by_slug($cat);

        if($cat_id>0){ $w_c1 = ' and category_id='.$cat_id; }
        if($lastlevelcat){ $w_c1 = ' and category_id IN('.trim($lastlevelcat).')';  }
        //if($is_recommended == 1){ $w_c1 = $w_c1.' and object_id IN(0'.$this->recommended_products().')'; }
        if($is_recommended == 1){ $w_c1 = $w_c1.' and object_id IN(0'.$this->recommended_products_via_attributes($user_id).')'; }

        if($attri==null){
            $get_pro_id = $this->db->query("select distinct object_id from category_object_relationship where object_type='product' ".$w_c1.' order by object_id desc');
            $result = $get_pro_id->result_array();

            if(!empty($result)){
                foreach($result as $val){
                    $product_ids[] = $val['object_id'];
                }

                $product_id = implode(',', $product_ids);
            }
        }
        if($attri!=null){
            $product_id = '0';
            $w_c2 = ''; $w_c3 = '';

             if($cat_id>0){ $w_c2 = ' and b.category_id='.$cat_id; }
             if($lastlevelcat){ $w_c2 = ' and b.category_id IN('.trim($lastlevelcat).')';  }
             //if($is_recommended == 1){ $w_c2 = $w_c2.' and b.object_id IN(0'.$this->recommended_products().')'; }
             if($is_recommended == 1){ $w_c2 = $w_c2.' and b.object_id IN(0'.$this->recommended_products_via_attributes($user_id).')'; }

             $get_pro_id1 = $this->db->query("select GROUP_CONCAT(id) as att from attribute_values_relationship where id IN (".$attri.") group by attribute_id");
             $resul1 = $get_pro_id1->result_array();
             
             $att = array();

             
             if(!empty($resul1)){ $f = 0;
                $w_c3 = ''; $w_c3 = $w_c3.' Having (';
                foreach($resul1 as $vall){ 
                    $att = explode(',', $vall['att']);
                    if($f!=0){ $w_c3 = $w_c3.') and ('; }
                    $k=0;
                    foreach($att as $val){
                        if($k!=0){ $w_c3 = $w_c3.' or '; }
                        $w_c3 = $w_c3.' Find_In_Set('.$val.',attr)';
                        $k++;
                    }
                    $f++;

                }

                $w_c3 = $w_c3.')';
                $get_pro_id = $this->db->query("select object_id,GROUP_CONCAT(attribute_id) as attr from attribute_object_relationship group by object_id".$w_c3);
                $resul = $get_pro_id->result_array();
                
                $product_id = ''; $product_ids = array();
                if(!empty($resul)){
                    foreach($resul as $val){
                        $product_ids[] = $val['object_id'];
                    }

                    $product_id = implode(',', $product_ids);
                    $w_c2 = $w_c2.' and b.object_id IN('.$product_id.')';
                }

             }

            if($product_id!=''){
               //if($is_recommended == 1){ $w_c2 = $w_c2.' and b.object_id IN(0'.$this->recommended_products().')'; }
               if($is_recommended == 1){ $w_c2 = $w_c2.' and b.object_id IN(0'.$this->recommended_products_via_attributes($user_id).')'; }
            $get_pro_id = $this->db->query("select distinct b.object_id from category_object_relationship as b where 1 ".$w_c2." order by b.object_id desc");
            $resul = $get_pro_id->result_array();

            $product_id = ''; $product_ids = array();
            if(!empty($resul)){
                foreach($resul as $val){
                    $product_ids[] = $val['object_id'];
                }

                $product_id = implode(',', $product_ids);
            }
            }
        }

        if($search!=null){
            $product_id = '0';
            $get_pro_id1 = $this->db->query("select id from product_desc where LOWER(name) like '".strtolower($search)."'");
            $resul1 = $get_pro_id1->result_array();
            $product_ids = array();

            if(!empty($resul1)){
                foreach($resul1 as $val){
                    $product_ids[] = $val['id'];
                }

                $product_id = implode(',', $product_ids);
            }
        }
        if($product_id!=''){
            $w_c .= ' and a.id IN('.$product_id.')';
        }else{
            $w_c .= ' and a.id IN(0)';
        }


        if($brands!=''){ $w_c .= ' and a.brand_id IN('.$brands.')'; }

        //if($colors!=''){ $w_c .= ' and a.sc_product_color IN('.$colors.')'; }


        if($min_price > 0 && $max_price > 0){ $w_c .= ' and a.price between '.$min_price.' and '.$max_price; }
        else if($price == 1) { $w_c .= ' and a.price < 500 ';  }
        else if($price == 2) { $w_c .= ' and (a.price >= 500 ) and (a.price <= 1999 )';  }
        else if($price == 3) { $w_c .= ' and (a.price >= 2000 ) and (a.price <= 3999 ) ';  }
        //else if($price == 4) { $w_c .= ' and (a.price >= 2000 ) and (a.price <= 2999 ) ';  }
        else if($price == 4) { $w_c .= ' and a.price > 4000 ';  }

        if($sort=='2'){
            $order_by = 'a.price asc';
        }else if($sort=='1'){
            $order_by = 'a.price desc';
        }

        if($sizeId!=null){
            $w_c .= ' and b.size_id IN('.$sizeId.')';
        }

        $get_products_qry = $this->db->query("select distinct count(a.id) as cnt from product_desc as a,product_inventory as b where 1 and a.id = b.product_id and a.status = 1 and a.is_delete=0 and a.approve_reject IN ('A','D') and b.stock_count > 0 ".$w_c." order by ".$order_by."");
        $tot = $get_products_qry->result_array();

        $data['total_row'] = $tot[0]['cnt'];
		// echo "new_limit".$new_limit.'  '.'new_offset'.$new_offset;exit;
		
        $get_products = $this->db->query("select distinct a.id,a.name ,a.image,a.price,a.brand_id,a.slug from product_desc as a,product_inventory as b where 1 and a.id = b.product_id and a.status = 1 and a.is_delete=0 and a.approve_reject IN ('A','D') and b.stock_count > 0 ".$w_c." order by ".$order_by." ".$new_limit." ".$new_offset." ");
        $res = $get_products->result_array();

      	// echo $this->db->last_query();exit;
        if($is_brand==1){
            $data['filter']['paid_brands'] = '';
        }else{
			// $data['filter']['paid_brands'] = '';
            $data['filter']['paid_brands'] = $this->get_paid_brand_list($product_id);
        }
        if($is_brand==1){
            $data['filter']['gender'] = '';
        }else{
            $data['filter']['gender'] = $this->get_gender(-1);
        }
		
        $data['filter']['category'] = $this->get_category($cat_id,$this->get_id_by_parent_id($cat),$this->get_name_by_slug($cat));
		
        if($cat_id>0){
            $data['filter']['global_attributes'] = $this->global_attributes();
            $data['filter']['category_attributes'] = $this->category_attributes($cat_id);
        }
         $data['filter']['price'] = $this->get_price_range($product_id);
        $data['filter']['size'] = $this->get_product_size($product_id);

        
        $data['cat_id'] = $cat_id;
        $data['cat_slug'] = $cat;
        $data['filter_brand_id'] = $brands;
        $data['is_recommended'] = $is_recommended;
        $data['products'] = $res;
        
        return $data;
    }

    function get_paid_brand_list($product_ids){
        if($product_ids!=''){
    	$query = $this->db->query('select distinct b.user_id,b.company_name from product_desc as a,brand_info as b,product_inventory as d where b.user_id = a.brand_id and b.is_paid = 1 and a.approve_reject IN("D","A") and a.is_delete=0 and a.status = 1 and a.id=d.product_id and d.stock_count>0 and a.id IN('.$product_ids.')');
        }else{
        $query = $this->db->query('select distinct b.user_id,b.company_name from product_desc as a,brand_info as b,product_inventory as d where b.user_id = a.brand_id and b.is_paid = 1 and a.approve_reject IN("D","A") and a.is_delete=0 and a.status = 1 and a.id=d.product_id and d.stock_count>0' );
        }
    	$res = $query->result_array();
    	return $res;
    }

    function get_gender($level=null){
        $g_html = '';   $is_recommended = '';  $i=0;
		$gender = array();
        if(isset($_GET['is_recommended'])==true && $_GET['is_recommended']==1){
            //$is_recommended = '?is_recommended=1';
            $is_recommended = '';
        }
        $query = $this->db->get_where('category', array('category_parent' => $level));
        $res = $query->result_array();
        $parent_cat = @$this->uri->segment(2);
        if(!empty($res)){
            foreach($res as $val){
                if($parent_cat == $val['category_slug']){
					// $g_html =$g_html.'<li onmouseup="" lable-for="'.$val['category_name'].'" class="active"><a title="'.$val['category_name'].'" href="'.base_url().'all-products/'.$val['category_slug'].$is_recommended.'" onclick="ga("send", "event", "ProductCategory", "clicked", "'.$val['category_name'].'");">'.$val['category_name'].'</a></li>';
					$gender[$i]['category_name'] = $val['category_name'];
					$gender[$i]['category_slug'] = $val['category_slug'];
					$i++;
                }else{
                    // $g_html =$g_html.'<li onmouseup="" lable-for="'.$val['category_name'].'"><a title="'.$val['category_name'].'" href="'.base_url().'all-products/'.$val['category_slug'].$is_recommended.'" onclick="ga("send", "event", "ProductCategory", "clicked", "'.$val['category_name'].'");">'.$val['category_name'].'</a></li>';
					$gender[$i]['category_name'] = $val['category_name'];
					$gender[$i]['category_slug'] = $val['category_slug'];
					$i++;
                }
            }
        }
        return $gender;
           
    }

    function get_category($parent_id=null,$old_cat_id=null,$cat_name=null){
		$g_html = '';    $has_child = 0; $i = 0 ; $cat_id = ''; $mi = 0; $mj = 0; $mk = 0;$parent = '';
        $is_recommended = '';
		$mobile_category = array();
		$sub_category = array();
		$last_category = array();
        // echo $parent_id.'---'.$old_cat_id.'---'.$cat_name;exit;
        if(isset($_GET['is_recommended'])==true && $_GET['is_recommended']==1){
            //$is_recommended = '?is_recommended=1';
            $is_recommended = '';
        }
        $pc_id = $this->get_parent_cat_info($parent_id);
        if($pc_id!=-'1'){
            $query = $this->db->query("SELECT `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `category` WHERE 1 and `id`='".$pc_id."'and `count` > 0");
                $res = $query->result_array();
                if(!empty($res)){
                    $g_html =$g_html.'<ul>';
                    foreach($res as $val){
                        $slug = $this->get_slug($val['id']);
                        $g_html =$g_html.'<li onmouseup="" lable-for="'.$val['category_name'].'"><a title="'.$val['category_name'].'" href="'.base_url().'all-products/'.$slug.$is_recommended.'" >'.$val['category_name'].'</a>';

						$mobile_category[$mi]['category_name'] = $val['category_name'];
						$mobile_category[$mi]['category_slug'] = $slug;
						$mi++;
                    }
                }
        }

        if($old_cat_id == '-1'){

            $query = $this->db->query("SELECT `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `category` WHERE 1 and `category_parent`='".$parent_id."'and `count` > 0");
                $res = $query->result_array();

            if(!empty($res)){
                $g_html =$g_html.'<ul>';
                foreach($res as $val){
                    $slug = $this->get_slug($val['id']);
                    $g_html =$g_html.'<li onmouseup="" lable-for="'.$val['category_name'].'"><a title="'.$val['category_name'].'" href="'.base_url().'all-products/'.$slug.$is_recommended.'" >'.$val['category_name'].'</a> </li>';
					$mobile_category[$mi]['category_name'] = $val['category_name'];
					$mobile_category[$mi]['category_slug'] = $slug;
					$mi++;
                    $this->slug = '';
                }
                $g_html =$g_html.'</ul>';
            }

        }else{

            $res1 = $this->get_list_cat($old_cat_id);

            $res =$this->get_list_cat($parent_id);

            if(!empty($res1)){
                $g_html =$g_html.'<ul>';
                foreach($res1 as $val1){
                    $slug = $this->get_slug($val1['id']);
                    if($val1['category_name'] == $cat_name){
                            $g_html =$g_html.'<li onmouseup="" lable-for="'.$val1['category_name'].'" class="active"><a title="'.$val1['category_name'].'" href="'.base_url().'all-products/'.$slug.$is_recommended.'" >'.$val1['category_name'].'</a>';
							
							$mobile_category[$mi]['category_name'] = $val1['category_name'];
							$mobile_category[$mi]['category_slug'] = $val1['category_slug'];
							$mi++;
                    }else{
                        $g_html =$g_html.'<li onmouseup="" lable-for="'.$val1['category_name'].'"><a title="'.$val1['category_name'].'" href="'.base_url().'all-products/'.$slug.$is_recommended.'">'.$val1['category_name'].'</a>';
						
						$mobile_category[$mi]['category_name'] = $val1['category_name'];
						$mobile_category[$mi]['category_slug'] = $val1['category_slug'];
						
						$mi++;
                    }
                    $this->slug = '';

                    if($val1['category_name'] == $cat_name){

                        if(!empty($res)){ 
                            foreach($res as $val2){
                                if($i!=0){ $cat_id = $cat_id.','; }
                                $cat_id = $cat_id.$val2['id'];
                                $i++;
                            }
                            }

                           $has_child = $this->has_children($cat_id);


                           if(!empty($res)){
                                $g_html =$g_html.'<ul>';

                                if($has_child > 0){
                                    foreach($res as $val){
                                        $slug = $this->get_slug($val['id']);
                                        $g_html =$g_html.'<li onmouseup="" lable-for="'.$val['category_name'].'"><a title="'.$val['category_name'].'" href="'.base_url().'all-products/'.$slug.$is_recommended.'" >'.$val['category_name'].'</a></li>';
										
										$sub_category[$mj]['category_name'] = $val['category_name'];
										$sub_category[$mj]['category_slug'] = $val['category_slug'];
										$mj++;
                                        $this->slug = '';
                                    }
                                }else{
                                    foreach($res as $val){
                                      
                                        $g_html =$g_html.'<li class="sccheckbox"><input type="checkbox" name="last_level" id="last_level'.$val['id'].'" value="'.$val['id'].'"><label for="last_level'.$val['id'].'">'.$val['category_name'].'</label></li>';
										$last_category[$mk]['category_name'] = $val['category_name'];
										$last_category[$mk]['category_id'] = $val['id'];
										$mk++;
                                    }
                                }
                                $g_html =$g_html.'</ul>';
                            }
                    }

                    $g_html =$g_html.'</li>';
					
                }
				// echo $g_html;exit;
            $g_html =$g_html.'</ul>';
            }

        }

        if($pc_id!=-'1'){
            $g_html =$g_html.'</li></ul>';
        }
		$category['mobile_category'] = $mobile_category;
		$category['sub_category'] = $sub_category;
		$category['last_category'] = $last_category;
		// echo $g_html;exit;
        return $category;
    }
	
	/*
	function get_category($parent_id=null,$old_cat_id=null,$cat_name=null){
		$category = array();
		$main_cat = array();
		$sub_category = array();
		$last_category = array();
		$parent_category = array();
		// echo 'parent_id='.$parent_id.'==old_cat_id=='.$old_cat_id.'==cat_name=='.$cat_name;exit;
		
		$pc_id = $this->get_parent_cat_info($parent_id);
		
		if($pc_id!=-'1'){
			$query = $this->db->query("SELECT `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `category` WHERE 1 and `id`='".$pc_id."'and `count` > 0");
			$parent_category = $query->result_array();     
        }
		
		$query = $this->db->query("SELECT `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `category` WHERE 1 and `category_parent`='".$parent_id."'and `count` > 0");
        $sub = $query->result_array();
		
        $query = $this->db->query("SELECT `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `category` WHERE 1 and `category_parent`='".$old_cat_id."'and `count` > 0");
        $main_cat = $query->result_array();
		$i = 0;
		foreach($sub as $val){
			
			$has_child = $this->has_children($val['id']);
			if($has_child > 0){
				$sub_category[$i] =	$val; 
			}else {
				$last_category[$i] = $val; 
			}
			$i++;
		}
		if($parent_id == 1 || $parent_id == 2 || $parent_id == 3){$main_cat = $sub_category; $sub_category = '';}
		$category['parent_category'] = $parent_category;
		$category['main_cat'] = $main_cat;
		$category['sub_category'] = $sub_category;	
		$category['last_category'] = $last_category;
		
		return $category
	}*/
	
    function get_list_cat($parent_id){
        $query = $this->db->query("SELECT `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `category` WHERE 1 and `category_parent`='".$parent_id."'and `count` > 0");
        $res = $query->result_array();
        return $res;
    }


    function get_id_by_slug($slug){
        if($slug){
            $query = $this->db->get_where('category', array('category_slug' => $slug));
            $res = $query->result_array();
            if(!empty($res)){ return $res[0]['id'];}else{ return '-1'; }

        }else{
            return '-1';
        }
    }

    function get_id_by_parent_id($slug){
        if($slug){
            $query = $this->db->get_where('category', array('category_slug' => $slug));
            $res = $query->result_array();
            if(!empty($res)){ return $res[0]['category_parent']; }else{ return '-1'; }
        }else{
            return '-1';
        }   
    }

    function get_parent_cat_info($id){
        if($id){
            $query = $this->db->query("select id,category_parent from category where id=".$id." and category_parent not in(1,3,2)");
            $res = $query->result_array();
            if(!empty($res)){ return $res[0]['category_parent']; }else{ return ''; }
        }
    }

    function get_name_by_slug($slug){
        if($slug){
            $query = $this->db->get_where('category', array('category_slug' => $slug , 'category_parent' => '-1'));
            $query = $this->db->query("SELECT `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `category` WHERE 1 and `category_parent`!='-1' and category_slug='".$slug."'");
            $res = $query->result_array();
            if(!empty($res)){ return $res[0]['category_name']; }else{ return ''; }
        }else{
            return '';
        }   
    }

    function has_children($id){
        $query = $this->db->query('select count(id) as cnt from category where category_parent IN ("'.$id.'")');
        $res = $query->result_array();

        if(!empty($res)){
            return $res[0]['cnt'];
        }else { return 0; }
    }

    function get_slug($id){
        
        /*if($id!='-1'){
        $query = $this->db->query('select category_slug,category_parent from category where id = "'.$id.'"');
        $res = $query->result_array();
        if(!empty($res)){
            foreach($res as $val){

                $this->slug = $val['category_slug'].'/'.$this->slug; 
                
                $this->get_slug($val['category_parent']);

            }
            return  $this->slug ;
        }
        }*/

        $query = $this->db->query("SELECT T2.category_slug as slug FROM (SELECT @r AS _id, (SELECT @r := category_parent FROM category WHERE id = _id) AS category_parent, @l := @l + 1 AS lvl FROM (SELECT @r := $id) vars, category m WHERE @r <> 0) T1 JOIN category T2 ON T1._id = T2.id ORDER BY T1.lvl ASC"); 
        
        $result = $query->result_array();       
        krsort ( $result);      

        $category_slug_url=""; $s=0;
        foreach($result as $row){
            if($s!=0){ $category_slug_url =$category_slug_url."/"; }
            $category_slug_url = $category_slug_url.$row['slug'];
            $s++;
        }
        return $category_slug_url;
        
    }

    function get_price_range($product_id){
        if($product_id){
        $query = $this->db->query('select min(a.price) as min_price,max(a.price) as max_price from product_desc as a where a.approve_reject IN("D","A") and a.is_delete=0 and a.status = 1 and a.id IN('.$product_id.')');
        }else{
        $query = $this->db->query('select a.sc_product_color from product_desc as a where a.approve_reject IN("D","A") and a.is_delete=0 and a.status = 1');
        }
        $res = $query->result_array();
        return $res;
    }
    
    function get_color($product_id){
        $color  = [];
        if($product_id){
        $query = $this->db->query('select a.sc_product_color from product_desc as a where a.approve_reject IN("D","A") and a.is_delete=0 and a.status = 1 and a.id IN('.$product_id.')');
        }else{
        $query = $this->db->query('select a.sc_product_color from product_desc as a where a.approve_reject IN("D","A") and a.is_delete=0 and a.status = 1');
        }
        $res = $query->result_array();

        if(!empty($res)){
            foreach ($res as $value) {
                if($value['sc_product_color']!=''){
                    $color[]  = $value['sc_product_color'];
                }
            }
        }
        return $color;
    }

    function get_cat_name($slug){
        $query = $this->db->query("SELECT `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `category` WHERE 1 and `category_slug` = '".$slug."' limit 0,1");
        $res = $query->result_array();
        return $res;
    }

    function global_attributes(){
        $query = $this->db->query("SELECT `id`, `attribute_name`, `attribute_content`, `attribute_parent`, `attribute_type`, `attribute_slug`, `show_on_web`, `order`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `attributes` WHERE 1 and `show_on_web` = 1 and `attribute_parent` = '-1' order by sort asc ");
        $res = $query->result_array();
        return $res;
    }

    function attributes_values($id,$is_recommended,$cat_id){

        $w_c = '';
        /*if($is_recommended == 1){
            if($this->top_attributes_pa($cat_id)!=''){
               $w_c = $w_c.' and id IN('.$this->top_attributes_pa($cat_id).')'; 
            }
        }*/

        $query = $this->db->query("SELECT `id`, `attribute_id`, `attr_value_name`, `attr_value_content`, `attr_value_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `attribute_values_relationship` WHERE 1 and `attribute_id`='".$id."' and `display` <> 2 and `count` >0 ".$w_c);
        $res = $query->result_array();
        return $res;   
    }

    function category_attributes($cat_id){
        
        $query = $this->db->query("SELECT `id`, `attribute_name`, `attribute_content`, `attribute_parent`, `attribute_type`, `attribute_slug`, `show_on_web`, `order`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `attributes` WHERE 1 and `show_on_web` = 1 and find_in_set('".$cat_id."',`attribute_parent`) <> 0 and `attribute_parent` != '-1' order by sort asc ");
        $res = $query->result_array();
        return $res;
    }

    function get_brand_slug($brand_slug){
        $query = $this->db->query('select a.id as id from user_login as a,brand_info as b where b.is_paid = 1 and a.user_name = '.$this->db->escape($brand_slug).'');
        $res = $query->result_array();
        if(!empty($res)){
            return $res[0]['id'];
        }else{
            return '';
        }
    }

    function get_brand_name($brand){
        $query = $this->db->query('select company_name as company_name from brand_info as b where user_id = '.$brand.'');
        $res = $query->result_array();
        if(!empty($res)){
            return $res[0]['company_name'];
        }else{
            return '';
        }
    }

    function get_product_size($product_id){
        if($product_id){
        $query = $this->db->query('select distinct b.id,b.size_text from product_inventory as a,product_size as b where a.size_id = b.id and a.stock_count >0 and a.product_id IN ('.$product_id.') order by b.order');
        $res = $query->result_array();
        return $res;
        }else{ return ''; }
    }

    function get_category_parent($cat_id){
        $query = $this->db->query("SELECT `category_parent` FROM `category` WHERE 1 and `id` = '".$cat_id."' limit 0,1");
        $res = $query->result_array();
        if(!empty($res)){
            return $res[0]['category_parent'];
        }else{
            return '0';
        }
    }

    function filter_product_sort(){

        if($this->session->userdata('user_id')>0){
        	$this->home_model->top_looks_depends_on_pa(@$this->session->userdata('user_id'));
            $bucket = $this->session->userdata('bucket_for_homepage') ;
            $budget = $this->session->userdata('budget_for_homepage');
            $age = $this->session->userdata('age_for_homepage');
            $pro_ids = '';
            if($bucket!='' && $budget!='' && $age!=''){
    
            $query = $this->db->query("select object_id from pa_object_relationship where object_type='product' and pa_type='bucket' and object_value =".$bucket." union select object_id from pa_object_relationship where object_type='product' and pa_type='age' and object_value =".$age." union select object_id from pa_object_relationship where object_type='product' and pa_type='budget' and object_value IN (".$budget.") order by object_id");

        	$res = $query->result_array($query);

        	$pro_id ='';
        	if(!empty($res)){ 
        		foreach($res as $val){
        			$pro_id = $pro_id.','.$val['object_id'];
        		}
        	
            $pro_ids = $this->top_cat_depends_on_pa($this->session->userdata('user_id'),$pro_id);
			if($pro_id!='' && $pro_ids!=''){ $pro_id = ','.$pro_ids.$pro_id; }
			else if($pro_id=='' && $pro_ids!=''){ $pro_id = $pro_id.','.$pro_ids; }
			
            return " FIELD(a.id ".$pro_id.") desc ";

        	}else{
        		return ' a.id desc';	
        	}
        	}
        }else{
            return ' a.id desc';
        }

    }

    function recommended_products(){

        if($this->session->userdata('user_id')>0){
            $this->home_model->top_looks_depends_on_pa(@$this->session->userdata('user_id'));
            $bucket = $this->session->userdata('bucket_for_homepage') ;
            $budget = $this->session->userdata('budget_for_homepage');
            $age = $this->session->userdata('age_for_homepage');
            $pro_ids = '';
            if($bucket!='' && $budget!='' && $age!=''){
    
            $query = $this->db->query("select object_id from pa_object_relationship where object_type='product' and pa_type='bucket' and object_value =".$bucket." union select object_id from pa_object_relationship where object_type='product' and pa_type='age' and object_value =".$age." union select object_id from pa_object_relationship where object_type='product' and pa_type='budget' and object_value IN (".$budget.") order by object_id");

            $res = $query->result_array($query);

            $pro_id ='';
            if(!empty($res)){ 
                foreach($res as $val){
                    $pro_id = $pro_id.','.$val['object_id'];
                }
            
            $pro_ids = $this->top_cat_depends_on_pa($this->session->userdata('user_id'),$pro_id);
            if($pro_id!='' && $pro_ids!=''){ $pro_id = ','.$pro_ids.$pro_id; }
            else if($pro_id=='' && $pro_ids!=''){ $pro_id = $pro_id.','.$pro_ids; }
            
            return $pro_id;

            }else{
                return '';    
            }
            }
        }else{
            return '';
        }

    }

    function top_cat_depends_on_pa($user_id,$products){
        $bucket = $this->session->userdata('bucket_for_homepage') ;
        $budget = $this->session->userdata('budget_for_homepage');
        $age = $this->session->userdata('age_for_homepage');

        $pro_id =''; $w_c ='';

        $query = $this->db->query("select distinct object_id from (select object_id from pa_object_relationship where object_type='category' and pa_type='bucket' and object_value =".$bucket." union select object_id from pa_object_relationship where object_type='category' and pa_type='age' and object_value =".$age." union select object_id from pa_object_relationship where object_type='category' and pa_type='budget' and object_value IN (".$budget.")) as a");
        $res = $query->result_array($query);
       // echo $this->db->last_query();
        $cat_id = '';
        if(!empty($res)){ $i=0;
            foreach($res as $val){
            	if($i!=0){ $cat_id = $cat_id.','; }
                $cat_id = $cat_id.$val['object_id'];
                $i++;
            }
            if($cat_id!=''){
            	$w_c = $w_c."and a.category_id IN (".$cat_id.")";
        	}
        }


        $query = $this->db->query("select distinct object_id from (select object_id from pa_object_relationship where object_type='brand' and pa_type='bucket' and object_value =".$bucket." union select object_id from pa_object_relationship where object_type='brand' and pa_type='age' and object_value =".$age." union select object_id from pa_object_relationship where object_type='brand' and pa_type='budget' and object_value IN (".$budget.")) as a");
        $res = $query->result_array($query);
       	$brand_id = ''; $i =0;
        if(!empty($res)){ 
            foreach($res as $val){
            	if($i!=0){ $brand_id = $brand_id.','; }
                $brand_id = $brand_id.$val['object_id'];
                $i++;
            }
            if($brand_id!=''){
            	$w_c = $w_c."and b.brand_id IN (".$brand_id.")";
            }
        }

        $query = $this->db->query("select a.object_id from category_object_relationship as a,product_desc as b where a.object_id=b.id and a.object_type='product' ".$w_c." and a.object_id NOT IN (0".$products.") order by a.object_id desc");
        $res = $query->result_array($query);

        if(!empty($res)){ $i=0;
            foreach($res as $val){
            	if($i!=0){ $pro_id = $pro_id.','; }
                $pro_id = $pro_id.$val['object_id'];
                $i++;
            }
        }

        return $pro_id;
    }

    function top_attributes_pa($cat_ids){
        $bucket = $this->session->userdata('bucket_for_homepage') ;
        $budget = $this->session->userdata('budget_for_homepage');
        $age = $this->session->userdata('age_for_homepage');

        $pro_id =''; $w_c ='';

        $query = $this->db->query("select distinct a.object_id,a.attribute_value_ids from (select object_id,attribute_value_ids from pa_object_relationship where object_type='category' and pa_type='bucket' and object_value =".$bucket." union select object_id,attribute_value_ids from pa_object_relationship where object_type='category' and pa_type='age' and object_value =".$age." union select object_id,attribute_value_ids from pa_object_relationship where object_type='category' and pa_type='budget' and object_value IN (".$budget.")) as a ");
        $res = $query->result_array($query);
        
        $cat_id = '';
        if(!empty($res)){ $i=0;
            foreach($res as $val){
                if($val['attribute_value_ids']!=''){
                    if($i!=0){ $cat_id = $cat_id.','; }
                    
                    $dt = unserialize($val['attribute_value_ids']);
                    $cat_ids = implode(',', $dt);
                    $cat_id = $cat_id.$cat_ids;
                    $i++;
                }
            }
            if($cat_id!=''){
                return $cat_id;
            }
        }
    }

    function recommended_products_via_attributes($user_id){
        $product_id = ''; $w_c3='';
        if($user_id > 0){
				$bucket = 0;
				$query = $this->db->query('select bucket from user_login where id='.$user_id." limit 0,1");
				$res = $query->result_array($query);
				if(!empty($res)){
					$bucket = $res[0]['bucket'];
				}
            $query = $this->db->query("select a.attribute_value_ids from (select object_id,attribute_value_ids from pa_object_relationship where object_type='category' and pa_type='bucket' and object_value =".$bucket." and attribute_value_ids!='') as a ");
            $res = $query->result_array($query);
			
            $cat_id = '';
            if(!empty($res)){ $i=0;$cat_id = '';
            foreach($res as $val){
                if($val['attribute_value_ids']!=''){
                    if($i!=0){ $cat_id = $cat_id.','; }

                    $dt = unserialize($val['attribute_value_ids']);
                    $cat_ids = implode(',', $dt);
                    $cat_id = $cat_id.$cat_ids;
                    $i++;
                }
            $w_c3='';
            if($cat_id==''){ $cat_id = '0'; }
             $get_pro_id1 = $this->db->query("select GROUP_CONCAT(id) as att from attribute_values_relationship where id IN (".$cat_id.") group by attribute_id");
             $resul1 = $get_pro_id1->result_array();
             $att = array();

             
             if(!empty($resul1)){ $f = 0;
                $w_c3 = ''; $w_c3 = $w_c3.' Having (';
                foreach($resul1 as $vall){ 
                    $att = explode(',', $vall['att']);
                    if($f!=0){ $w_c3 = $w_c3.') and ('; }
                    $k=0;
                    foreach($att as $val){
                        if($k!=0){ $w_c3 = $w_c3.' or '; }
                        $w_c3 = $w_c3.' Find_In_Set('.$val.',attr)';
                        $k++;
                    }
                    $f++;

                }

                $w_c3 = $w_c3.')'; 

            if($w_c3!=''){
               $query11 = $this->db->query("select object_id ,GROUP_CONCAT(attribute_id) as attr from attribute_object_relationship group by object_id".$w_c3);
                $res11 = $query11->result_array();
            //echo $this->db->last_query();
				//print_r($res11);
                if(!empty($res11)){
                    foreach($res11 as $val1){
                        //if($r!=0){ $product_id = $product_id.','; }
                        $product_id = $product_id.','.$val1['object_id'];
                       
                    }
                }
            }
        }
            }
        }
        }
		
		$product_id = $this->product_availability($product_id);
        if($product_id==''){
            $product_id1 = $this->recommended_products_via_category($user_id,$bucket);
            $product_id = $this->product_availability($product_id1);
        }
        // echo $product_id;exit;
        return $product_id;
    }
	
	function recommended_products_via_category($user_id,$bucket){

        if($user_id>0){
            $pro_ids = '';
            if($bucket!=''){
    
            $query = $this->db->query("select object_id from pa_object_relationship where object_type='category' and pa_type='bucket' and object_value =".$bucket." ");

            $res = $query->result_array();

            $cat_id =''; $product_id = '';
            if(!empty($res)){ 
                foreach($res as $val){
                    $cat_id = $cat_id.','.$val['object_id'];
                }
            

                $query1 = $this->db->query("select distinct object_id from category_object_relationship where category_id IN(-1".$cat_id.") and object_type='product'");
                $res1 = $query1->result_array();
                if(!empty($res1)){
                    foreach($res1 as $val){
                        $product_id = $product_id.','.$val['object_id'];
                    }
                }
                
                return $product_id; 

            }else{
                return '';    
            }
            }
        }else{
            return '';
        }

    }
	
	function product_availability($product_ids){
        $pro_id = '';
        if($product_ids!=''){
                $query11 = $this->db->query("select id from product_desc where status=1 and approve_reject IN('A','D') and id in(0".$product_ids.")");
                $res11 = $query11->result_array();

                if(!empty($res11)){
                    foreach($res11 as $val){
                        $pro_id = $pro_id.','.$val['id'];
                    }
                }

            return $pro_id;
        }else{
            return $pro_id;
        }
    }
	
	
}
?>