<?php
class Home_model extends MY_Model {

   function __construct()
    {
        // Call the Model constructor
        $this->slug = '';
        parent::__construct();
    }

    function get_home_look($type=NULL){
        $look_data = array();
        $option_key_name = 'top_looks_'.$type;
        if($this->session->userdata('user_id') > 0){
            $look_ids = $this->top_looks_depends_on_pa($this->session->userdata('user_id'));
        }
        
        if(empty($look_ids)){
            $look_ids = $this->get_sc_options($option_key_name);
            $this->session->set_userdata("is_home_page_personalised",0);
        }else{
            $this->session->set_userdata("is_home_page_personalised",1);
        }
        
       
        if(!empty($look_ids)){
            foreach ($look_ids as $look_id) {
                $look_data[] = $this->get_look_info($look_id);
            }
        }
        return $look_data;
    }

    function get_look_info($lookId){
        if($lookId > 0){
        $query = $this->db->query('select z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.description,z.status,z.deflag,z.slug from 
(SELECT a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.description,a.status,a.deflag,a.slug FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 and a.id = '.$lookId.') as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 OR z.look_type = 6) ');
            $res = $query->result_array();
            return $res;
        }else{
        return false;
        }
    }

    function get_home_categories($type=NULL){
        $cat_data = array();
        $option_key_name = 'top_cat_'.$type;
        

        if($this->session->userdata('user_id') > 0){
            $cat_ids = $this->top_cat_depends_on_pa($this->session->userdata('user_id'));
        }

        if(empty($cat_ids)){
            $cat_ids = $this->get_sc_options($option_key_name);
        }

        if(!empty($cat_ids)){
            foreach ($cat_ids as $cat_id) {
                $cat_data[] = $this->get_category_info($cat_id['type'],$cat_id['cat']);
            }
        }
        return $cat_data;
    }



   /* function category_list($cat){

        $where_cond = '';
        $array_categories = $this->get_sc_options('sc_view_all_cat');
        $all_array_categories = $this->get_sc_options('top_cat_all');
        //$array_categories = unserialize($array_categories);
        if($array_categories)
            $array_categories = "and a.id in(".implode(',', $array_categories['var_type']).")";
        else
            $array_categories="";

        if($cat == 'men'){ $where_cond = $where_cond. ' and b.`product_cat_id` = '.MEN_CATEGORY_ID.' '; }
        if($cat == 'women'){ $where_cond = $where_cond. ' and b.`product_cat_id` = '.WOMEN_CATEGORY_ID.' '; }
        $query = $this->db->query('select * from category');
        $res = $query->result_array(); 
        $cat_data = array();
        if(!empty($res)){
                foreach ($res as $val) {
                    $cat_data[] = array(
                            'category_name' => $val['category_name'],
                            'id' => $val['id'],
                            'category_slug' => $this->get_slug($val['id'])

                        );

                    $this->slug ='';
                }
            }
           
        return $cat_data;
    }*/

        function category_list($cat,$data){
			
        if($cat == 'men'){ 
            $query = $this->db->query("select id,category_name,category_parent,IF( category_image IS NULL ,'https://www.stylecracker.com/assets/images/products_category/187.jpg', CONCAT('https://www.stylecracker.com/assets/images/products_category/',category_image) ) as category_image from (select * from category order by category_parent, id) products_sorted,(select @pv := '3') initialisation where find_in_set(category_parent, @pv) > 0 and @pv := concat(@pv, ',', id) ".@$data['limit_cond']);
         }
        else if($cat == 'women'){  
            $query = $this->db->query("select id,category_name,category_parent,IF( category_image IS NULL ,'https://www.stylecracker.com/assets/images/products_category/187.jpg', CONCAT('https://www.stylecracker.com/assets/images/products_category/',category_image) ) as category_image from (select * from category order by category_parent, id) products_sorted,(select @pv := '1') initialisation where find_in_set(category_parent, @pv) > 0 and @pv := concat(@pv, ',', id) ".@$data['limit_cond']); }
        else{
            $query = $this->db->query("select id,category_name,category_parent,IF( category_image IS NULL ,'https://www.stylecracker.com/assets/images/products_category/187.jpg', CONCAT('https://www.stylecracker.com/assets/images/products_category/',category_image) ) as category_image from category where 1 ".@$data['limit_cond']);
        }
		
        $res = $query->result_array(); 
        $cat_data = array();
        if(!empty($res)){
                foreach ($res as $val) {

                    $cat_data[] = array(
                            'category_name' => $val['category_name'],
                            'id' => $val['id'],
							'category_image' => $val['category_image'],
                            'category_slug' => $this->get_slug1($val['id'])

                        );

                    
                }
            }
        // echo "<pre>";print_r($cat_data);exit;   
        return $cat_data;
    }

    function brand_list($cat){
        $where_cond = '';
        
        $bucket = $this->session->userdata('bucket_for_homepage') ? $this->session->userdata('bucket_for_homepage') : '0'; 
        $budget = $this->session->userdata('budget_for_homepage') ? $this->session->userdata('budget_for_homepage') : '0';
        $age = $this->session->userdata('age_for_homepage') ? $this->session->userdata('age_for_homepage') : '0'; 

        $data = array();

        $query = $this->db->query("select distinct object_id from (select object_id from pa_object_relationship where object_type='brand' and pa_type='bucket' and object_value =".$bucket." union select object_id from pa_object_relationship where object_type='brand' and pa_type='age' and object_value =".$age." union select object_id from pa_object_relationship where object_type='brand' and pa_type='budget' and object_value IN (".$budget.")) as a");
        $res = $query->result_array($query);
       
        if(!empty($res)){ 
            foreach($res as $val){
                $data[] = $val['object_id'];
            }
        }
        if(!empty($data)){
            $where_cond = ' and a.user_id IN('.implode(',', $data).')';
        }

        $query = $this->db->query('SELECT a.*,b.`user_name` FROM `brand_info` as a,user_login as b WHERE 1 '.$where_cond.' and a.is_paid = 1 and a.user_id = b.id order by a.user_id desc');
        $res = $query->result_array(); 
        return $res;
    }

    function brand_list_from_ids($ids){
        $where_cond = '';
        $query = $this->db->query('SELECT a.*,b.`user_name` FROM `brand_info` as a,user_login as b WHERE 1 and a.user_id in ('.$ids.') and a.user_id = b.id order by a.user_id desc');
        $res = $query->result_array(); 
        return $res;
    }

    function get_category_info($cat_id,$preselected){
        $cat_table_name = ''; $cat_data = array();
        if($cat_id == 1){ $cat_table_name = 'category'; }
        if($cat_table_name !=''){
            $query = $this->db->get_where($cat_table_name, array('id'=>$preselected,'status'=>1));
            $res = $query->result_array(); 
            if(!empty($res)){
                foreach ($res as $val) {
                    $cat_data['name'] = $val['category_name'];
                    $cat_data['cat_id'] = $val['id'];
                    $cat_data['product_cat_id'] = @$val['product_cat_id'];
                    $cat_data['type'] = $cat_id;
                    $cat_data['slug'] = $this->get_slug($val['id']);
                    $cat_data['product_count'] = $this->product_count($cat_id,$val['id'],'');
                    $this->slug ='';
                }
            }
        }
        return $cat_data;
    }

    function get_home_brands($type=NULL){
        $brands_data = array();
        $option_key_name = 'trending_brand_'.$type;
        $brand_ids = array();
        if($this->session->userdata('user_id') > 0){
            $brand_ids = $this->top_brands_depends_on_pa($this->session->userdata('user_id'));
        }
        if(empty($brand_ids)){
            $brand_ids = $this->get_sc_options($option_key_name);
        }

        $brand_data = array();
        if(!empty($brand_ids)){
            foreach ($brand_ids as $brand_id) {
                $brand_data[] = $this->get_brands_info($brand_id);
            }
        }
        return $brand_data;
    }

    function get_brands_info($brandId){
        if($brandId > 0){
            //$query = $this->db->get_where('brand_info', array('is_paid' => '1','user_id'=>$brandId));
             $query = $this->db->query('select a.*,b.user_name from brand_info as a,user_login as b where a.is_paid=1 and a.user_id = '.$brandId.' and a.user_id = b.id');
            $res = $query->result_array(); 
            return $res;
        }else{
            return false;
        }
    }

    function get_all_looks($looks_for){
        $where_cond = '';
        if($looks_for === 'women') { $where_cond = ' and z.looks_for=1'; }else if($looks_for === 'men') { $where_cond = ' and z.looks_for=2'; }

        $query = $this->db->query('select z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.description,z.status,z.deflag,z.slug,z.looks_for from 
(SELECT a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.description,a.status,a.deflag,a.slug,a.looks_for FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 OR z.look_type = 6) WHERE 1 and z.status=1 '.$where_cond.' order by z.modified_datetime desc limit 0,12');
        $res = $query->result_array();

        return $res;

    }

    function get_look_count($looks_for){
        $where_cond = '';
        if($looks_for === 'women') { $where_cond = ' and looks_for=1'; }else if($looks_for === 'men') { $where_cond = ' and looks_for=2'; }
        $query = $this->db->query('select count(id) as cnt from look where status = 1 '.$where_cond);
        $res = $query->result_array();
        if(!empty($res)){ return $res[0]['cnt']; }else{ return 0; }
    }

    function get_more_looks($looks_for,$offset,$filter){
        
            $offset = ($offset - 1) * 12;
        
        $where_cond = '';
        if($looks_for === 'women') { $where_cond = ' and z.looks_for=1'; }else if($looks_for === 'men') { $where_cond = ' and z.looks_for=2'; }

        if($filter > 1){
            if($filter == 2){

               $query = $this->db->query('select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime,z.looks_for from
(SELECT a.looks_for,a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN ((select distinct look_id from look_category_mapping where cat_id = 5)) ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4) WHERE 1 '.$where_cond.' order by z.modified_datetime desc limit '.$offset.',12');

            }else if($filter == 3){

                $query = $this->db->query('select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime,z.looks_for from
(SELECT a.looks_for,a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN ((select distinct look_id from look_category_mapping where cat_id = 40)) ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4) WHERE 1 '.$where_cond.' order by z.modified_datetime desc limit '.$offset.',12');

            }else if($filter == 4){

            }
        }else{

        $query = $this->db->query('select z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.description,z.status,z.deflag,z.slug,z.looks_for from 
(SELECT a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.description,a.status,a.deflag,a.slug,a.looks_for FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 OR z.look_type = 6) WHERE 1 and z.status=1 '.$where_cond.' order by z.modified_datetime desc limit '.$offset.',12');
        }
        $res = $query->result_array();
        //echo $this->db->last_query();
        return $res;

    }

    function get_filters($type){
        $cat_data = array();
        if($type != 'men') { $type = 'women'; } 
        $option_key_name = 'mega_menu_'.$type;
        $cat_ids = $this->get_sc_options($option_key_name); 
        if($type == 'men') { $type_value = MEN_CATEGORY_ID; }else{ $type_value = WOMEN_CATEGORY_ID;  }
        
        if(!empty($cat_ids[$type_value])){
            foreach ($cat_ids[$type_value] as $key => $cat_id) {
               $cat_data[$key] = $this->get_category_info(3,$key);
               if(!empty($cat_id)){
                foreach($cat_id as $val) {
                        if($val!=''){
                            $cat_data[$key]['variation_values'][] = $this->get_category_info(4,$val);
                        }
                    }
                }
            }
        }
        return $cat_data;
    }

    function product_count($cat_type,$category_id,$look_type){
        $where_cond = '';
        if($cat_type == 1){ $cat_table_name = 'product_category'; $product_table='product_desc'; }
        else if($cat_type == 2){ $cat_table_name = 'product_sub_category'; $product_table='product_desc'; }
        else if($cat_type == 3){ $cat_table_name = 'product_variation_type'; $product_table='product_variations'; }
        else if($cat_type == 4){ $cat_table_name = 'product_variation_value'; $product_table='product_variations'; }

        if($category_id > 0 && $product_table != ''){
               if($product_table == 'product_desc'){
                    if($cat_type == '1'){ 
                        $where_cond = $where_cond.' and product_cat_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and product_sub_cat_id='.$category_id; 
                    }
                    if($look_type == 'men') { $where_cond = $where_cond.' and product_cat_id=12';   }
                    else if($look_type == 'women') { $where_cond = $where_cond.' and product_cat_id=1';   }
                    $query = $this->db->query('select count(id) as cnt  from product_desc WHERE  approve_reject = "A" and is_delete = 0 '.$where_cond.'');
                    $product_data = $query->result_array();
                    
                }else if($product_table == 'product_variations'){
                    if($cat_type == '3'){ 
                        $where_cond = $where_cond.' and b.product_var_type_id='.$category_id; 
                    }else{
                        $where_cond = $where_cond.' and b.product_var_val_id='.$category_id; 
                    }
                    if($look_type == 'men') { $where_cond = $where_cond.' and a.product_cat_id=12';   }
                    else if($look_type == 'women') { $where_cond = $where_cond.' and a.product_cat_id=1';   }
                    $query = $this->db->query('select count(a.id) as cnt from product_desc as a,product_variations as b WHERE a.approve_reject = "A" and a.is_delete = 0 and a.id = b.product_id '.$where_cond.'');
                    $product_data = $query->result_array();
                } 
                if(!empty($product_data)){
                    return $product_data[0]['cnt'];
                }else{
                    return 0;
                }
        }

    }

    function get_paid_brands($type){
            $query = $this->db->get_where('brand_info', array('is_paid' => '1'));
            $res = $query->result_array();
            return $res;
    }

    function coupon_offer(){
        $query = $this->db->query('select id,coupon_code,coupon_desc from coupon_settings WHERE coupon_start_datetime<=CURRENT_TIMESTAMP() && coupon_end_datetime >= CURRENT_TIMESTAMP() and status=1 and is_delete=0');
        $res = $query->result_array();
            return $res;
    }

    function get_category_name($cat_id,$category_slug){
        $cat_table_name = ''; $cat_data = array();
        if($cat_id == 'cat'){ $cat_table_name = 'product_category'; }
        else if($cat_id == 'sub_cat'){ $cat_table_name = 'product_sub_category'; }
        else if($cat_id == 'variations_type'){ $cat_table_name = 'product_variation_type'; }
        else if($cat_id == 'variations'){ $cat_table_name = 'product_variation_value'; }
        if($category_slug!=''){
                $query = $this->db->get_where($cat_table_name, array('status' => '1','slug'=>$category_slug));
                $res = $query->result_array();
                $category_id = $res[0]['name'];
                return $category_id;
            }
    }

    function get_look_fav_or_not($look_id){
        $user_id = $this->session->userdata('user_id');
        if($look_id!='' && $user_id!=''){
           $res =  $this->db->get_where('look_fav', array('look_id' => $look_id,'user_id' => $user_id));
            return count($res->result_array());
        }
    }

     function get_fav_or_not($fav_for,$fav_id){
        $user_id = $this->session->userdata('user_id');
        if($fav_id!='' && $user_id!='' && $fav_for!=''){
           $res =  $this->db->get_where('favorites', array('fav_id' => $fav_id,'user_id' => $user_id,'fav_for'=>$fav_for));
           return count($res->result_array());
        }
    }

    function sub_category($brand_id=NULL){
          $where_cond = '';
          if($brand_id>0) { $where_cond = ' AND b.brand_id='.$brand_id; } 
          $query = $query = $this->db->query('select distinct a.id,a.name from product_sub_category as a,product_desc as b where a.`id` = b.`product_sub_cat_id` and a.status=1 and a.is_delete=0 '.$where_cond.' ');
          $res = $query->result_array();
          return $res;
    }

    function get_brands_looks($offset,$brand_usename){
        $brand_id = 0;
        $offset = $offset * 12;
        if($brand_usename!=''){ $brand_id = @$this->get_user_data('user_login','user_name',$brand_usename)[0]['id']; }
        if($brand_id > 0){

             $query = $this->db->query('select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime from
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN (select distinct u.`look_id` from `look_products` as u,`product_desc` as i where u.`product_id` = i.`id` and i.`brand_id` = '.$brand_id.' )) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4|| z.look_type = 6) order by z.modified_datetime desc limit '.$offset.',12');
            $res = $query->result_array();
            if(!empty($res)){
                return $res;
            }
        }

    }

    function get_brands_looks_total($brand_usename){
        $brand_id = 0;
        
        if($brand_usename!=''){ $brand_id = @$this->get_user_data('user_login','user_name',$brand_usename)[0]['id']; }
        if($brand_id > 0){

             $query = $this->db->query('select count(z.id) as cnt from
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN (select distinct u.`look_id` from `look_products` as u,`product_desc` as i where u.`product_id` = i.`id` and i.`brand_id` = '.$brand_id.' )) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4|| z.look_type = 6) order by z.modified_datetime desc');
            $res = $query->result_array();
            if(!empty($res)){
                return $res[0]['cnt'];
            }else{
                return 0;
            }
        }

    }

    function get_user_data($table_name,$field_name,$value){
        if($field_name!='' && $value!=''){
          $query = $query = $this->db->query('select * from '.$table_name.' where 1 and '.$field_name.' = "'.$value.'"');
          $res = $query->result_array();
          if(!empty($res)){
            return $res;
          }else{
            return 0;
          }
        }
    }

    function get_option_data($option_name)
    {
        
        //  if($option_name!=''){
        //    $res =  $this->db->get_where('sc_options', array('option_name' => $option_name));          
        //     return $res->result_array();
        // }
    }

    function get_stylist_looks($offset,$stylist_id){
        $offset = ($offset-1) * 12;
        if($offset < 0) { $offset = 0; }
        if($stylist_id > 0){ $where_cond = ' and z.stylist_id = '.$stylist_id; }
        $query = $this->db->query('select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime,z.looks_for,z.stylist_id from
(SELECT a.looks_for,a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime,a.   stylist_id FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN ((select distinct look_id from look_category_mapping where cat_id = 5)) ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4) WHERE 1 '.$where_cond.' order by z.modified_datetime desc limit '.$offset.',12');
        $res = $query->result_array();
        return $res;
    }

    function get_home_products($type=NULL){
        $brands_data = array();
        $option_key_name = 'trending_products_'.$type;
        $brand_ids = $this->get_sc_options($option_key_name);
        $brand_data = array();
        if(!empty($brand_ids)){
            foreach ($brand_ids as $brand_id) {
                $brand_data[] = $this->get_products_info($brand_id);
            }
        }
        return $brand_data;
    }

    function get_products_info($brandId){
        if($brandId > 0){
             $query = $this->db->query('SELECT `id`, `product_cat_id`, `product_sub_cat_id`, `brand_id`, `designer_id`, `name`, `url`, `description`, `slug`, `price`, `price_unit`, `price_range`, `consumer_type`, `target_market`, `rating`, `body_part`, `body_type`, `body_shape`, `personality`, `age_group`, `image`, `approve_reject`, `reason`, `hits`, `referred_count`, `status`, `is_promotional`, `is_delete`, `product_used_count`, `product_stock`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`, `product_sku`, `product_discount`, `discount_start_date`, `discount_end_date` FROM `product_desc` WHERE 1 and `image` NOT LIKE "%.png%" and `id`='.$brandId);
            $res = $query->result_array(); 
            return $res;
        }else{
            return false;
        }
    }

      function get_slug($id){
        
        if($id!='-1'){
        $query = $this->db->query('select category_slug,category_parent from category where id = "'.$id.'"');
        $res = $query->result_array();
        if(!empty($res)){
            foreach($res as $val){

                $this->slug = $val['category_slug'].'/'.$this->slug; 
                
                $this->get_slug($val['category_parent']);

            }
            return  $this->slug ;
        }
        }
        
    }

    function top_looks_depends_on_pa($user_id){
         $query = $this->db->query('select bucket from user_login where id='.$user_id." limit 0,1");
         $res = $query->result_array($query);
         if(!empty($res)){
            $bucket = $res[0]['bucket']; $age = ''; $budget = ''; $question_id = 0; $where_condition='';
            $this->session->set_userdata("bucket_for_homepage",$bucket);
            $query = $this->db->query('select question_id,answer_id from `users_answer` where user_id='.$user_id.' and question_id IN (7,8,15,18)');
            $res = $query->result_array($query);

            if(!empty($res)){
                foreach ($res as $val) {
                    if($val['question_id'] == '7' || $val['question_id'] == '18'){
                        $budget = $val['answer_id']; $question_id = $val['question_id'];
                    }
                    if($val['question_id'] == '8' || $val['question_id'] == '15'){
                        $age = $val['answer_id']; $question_id = $val['question_id'];
                    }

                }
            }
            
            if($question_id == '18'){
                if($budget == '4688')
                {
                    $where_condition = $where_condition.' and a.bucket_id='.$bucket.' AND a.budget = '.$budget;
                    $this->session->set_userdata("budget_for_homepage",$budget);
                }else if($budget == '4689'){
                    $where_condition = $where_condition.' and a.bucket_id='.$bucket.' AND a.budget in (4688,4689)';
                    $this->session->set_userdata("budget_for_homepage",'4688,4689');
                }else if($budget == '4690'){
                    $where_condition = $where_condition.' and a.bucket_id='.$bucket.' AND a.budget in (4688,4689,4690)';
                    $this->session->set_userdata("budget_for_homepage",'4688,4689,4690');
                }else if($budget == '4691'){
                    $where_condition = $where_condition.' and a.bucket_id='.$bucket.' AND a.budget in (4688,4689,4690,4691)';
                    $this->session->set_userdata("budget_for_homepage",'4688,4689,4690,4691');
                }
            }else if($question_id == '7'){
                if($budget == '36')
                {
                    $where_condition = $where_condition.' and a.bucket_id='.$bucket.' AND a.budget = '.$budget;
                    $this->session->set_userdata("budget_for_homepage",$budget);
                }else if($budget == '37'){
                    $where_condition = $where_condition.' and a.bucket_id='.$bucket.' AND a.budget in (36,37)';
                    $this->session->set_userdata("budget_for_homepage",'36,37');
                }else if($budget == '38'){
                    $where_condition = $where_condition.' and a.bucket_id='.$bucket.' AND a.budget in (36,37,38)';
                    $this->session->set_userdata("budget_for_homepage",'36,37,38');
                }else if($budget == '4670'){
                    $where_condition = $where_condition.' and a.bucket_id='.$bucket.' AND a.budget in (36,37,38,4670)';
                    $this->session->set_userdata("budget_for_homepage",'36,37,38,4670');
                }
            }else{
                $where_condition = $where_condition.' and a.bucket_id='.$bucket;
            }

            /*if($age!=''){
                $where_condition = $where_condition.' and (a.age='.$age.')';
                $this->session->set_userdata("age_for_homepage",$age);
            }*/

            $query = $this->db->query('select distinct a.look_id from look_bucket_mapping as a,look as b where 1 and a.look_id = b.id and b.deflag = 0 and b.status = 1 '.$where_condition.' order by b.id desc,b.broadcast_datetime desc limit 0,3');
            $res = $query->result_array($query);
            //echo $this->db->last_query();
            $look_ids = array();
            if(!empty($res)){
                foreach ($res as $value) {
                    $look_ids[] = $value['look_id'];
                }
            }

            return $look_ids;
         }

    }

    function top_cat_depends_on_pa($user_id){
        $bucket = $this->session->userdata('bucket_for_homepage') ? $this->session->userdata('bucket_for_homepage') : '0'; 
        $budget = $this->session->userdata('budget_for_homepage') ? $this->session->userdata('budget_for_homepage') : '0';
        $age = $this->session->userdata('age_for_homepage') ? $this->session->userdata('age_for_homepage') : '0'; 

        $data = array();

     
        $query = $this->db->query("select distinct object_id from (select object_id from pa_object_relationship where object_type='category' and pa_type='bucket' and object_value =".$bucket." union select object_id from pa_object_relationship where object_type='category' and pa_type='age' and object_value =".$age." union select object_id from pa_object_relationship where object_type='category' and pa_type='budget' and object_value IN (".$budget.")) as a,category as b where a.object_id=b.id and b.count >0 and b.status =1 order by RAND() limit 0,3");
        $res = $query->result_array($query);
        //echo $this->db->last_query();

        if(!empty($res)){ $i = 0;
            foreach($res as $val){
                $data[$i]['cat'] = $val['object_id'];
                $data[$i]['type'] = 1;
                $i++;
            }
        }


        return $data;
    }


    function top_brands_depends_on_pa($user_id){
        $bucket = $this->session->userdata('bucket_for_homepage') ? $this->session->userdata('bucket_for_homepage') : '0'; 
        $budget = $this->session->userdata('budget_for_homepage') ? $this->session->userdata('budget_for_homepage') : '0';
        $age = $this->session->userdata('age_for_homepage') ? $this->session->userdata('age_for_homepage') : '0'; 

        $data = array();


        $query = $this->db->query("select distinct object_id from (select object_id from pa_object_relationship where object_type='brand' and pa_type='bucket' and object_value =".$bucket." union select object_id from pa_object_relationship where object_type='brand' and pa_type='age' and object_value =".$age." union select object_id from pa_object_relationship where object_type='brand' and pa_type='budget' and object_value IN (".$budget.")) as a order by RAND() limit 0,3");
        $res = $query->result_array($query);
       
        if(!empty($res)){ 
            foreach($res as $val){
                $data[] = $val['object_id'];
                
            }
        }


        return $data;
    }

    function get_latest_home_page_products(){
       /* $query = $this->db->query("SELECT id,name,price,image,slug,CASE 
        WHEN  discount_start_date <= CURRENT_TIMESTAMP() and discount_end_date >= CURRENT_TIMESTAMP() then compare_price
        ELSE 0
		END AS compare_price,brand_id FROM  product_desc where brand_id in 
            (33411,34345,36066,29791,36187,20933,27689,30984,34344,27140,35679) and status = 1 and is_delete = 0 and approve_reject IN ('A','D') order by 
            RAND(), modified_datetime desc limit 10;");*/
		$query = $this->db->query("SELECT a.id,a.name,a.price,a.image,a.slug,CASE 
        WHEN  a.discount_start_date <= CURRENT_TIMESTAMP() and a.discount_end_date >= CURRENT_TIMESTAMP() then a.compare_price
        ELSE 0
		END AS compare_price,a.brand_id FROM  product_desc as a,product_inventory as b where a.brand_id in 
            (33411,34345,36066,29791,36187,20933,27689,30984,34344,27140,35679) and a.status = 1 and a.is_delete = 0 and a.approve_reject IN ('A','D') AND a.id = b.product_id AND b.stock_count > 0 order by 
            RAND(), a.modified_datetime desc limit 10;");
        return $query->result_array($query);
    }
 
     public function look_hv_discount($look_id){
        if($look_id > 0){
            $query = $this->db->query('select da.id, da.discount_type_id,lp.look_id,lp.product_id from discount_availed as da,look_products as lp,product_desc as pd where NOW() >= da.start_date AND NOW() <= da.end_date AND lp.product_id = pd.id  AND da.status=1 and (da.product_id = lp.product_id) and lp.look_id = "'.$look_id.'"');
            $res = $query->result_array();

            if(!empty($res)){
                return 1;
            }else{

            $query = $this->db->query('select da.id, da.discount_type_id,lp.look_id,lp.product_id from discount_availed as da,look_products as lp,product_desc as pd where NOW() >= da.start_date AND NOW() <= da.end_date AND lp.product_id = pd.id  AND da.status=1 and (da.brand_id =pd.brand_id) and lp.look_id = "'.$look_id.'"');
            $res = $query->result_array();
                if(!empty($res)){
                    return 1;
                }else{
                    return 0;
                }

            }


        }
    }

      function cat_img_exists($cat_id){

        $category_img_url = '';
        $query = $this->db->query("select id,category_image,category_parent from category where id=".$cat_id);
        $res = $query->result_array($query);
        if(!empty($res)){
            $category_img_url = 'assets/images/products_category/'.$res[0]['id'].".jpg";
            if(file_exists($category_img_url)){ 
                return $category_img_url;
            }else{
                return $this->cat_img_exists($res[0]['category_parent']);
            }
        }
    }

      function get_slug1($id){
        $query = $this->db->query("SELECT T2.category_slug as slug FROM (SELECT @r AS _id, (SELECT @r := category_parent FROM category WHERE id = _id) AS category_parent, @l := @l + 1 AS lvl FROM (SELECT @r := $id) vars, category m WHERE @r <> 0) T1 JOIN category T2 ON T1._id = T2.id ORDER BY T1.lvl ASC"); 
        
        $result = $query->result_array();       
        krsort ( $result);      
        $category_slug_url=""; $s=0;
        foreach($result as $row){
            if($s!=0){ $category_slug_url =$category_slug_url."/"; }
            $category_slug_url = $category_slug_url.$row['slug'];
            $s++;
        }
        return $category_slug_url;
    }
	
	function get_store_looks($offset,$brand_usename,$store_id){
        $brand_id = 0;
        $offset = $offset * 12;
        if($brand_usename!=''){ $brand_id = @$this->get_user_data('user_login','user_name',$brand_usename)[0]['id']; }
        if($brand_id > 0){

             $query = $this->db->query('select z.id,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime from
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN (select distinct u.`look_id` from `look_products` as u,`product_desc` as i,product_store as ps where u.`product_id` = i.`id` and ps.`product_id` = i.id AND ps.store_id = '.$store_id.' )) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4|| z.look_type = 6) order by z.modified_datetime desc limit '.$offset.',12');
            $res = $query->result_array();
            if(!empty($res)){
                return $res;
            }
        }

    }

    function get_store_looks_total($brand_usename,$store_id){
        $brand_id = 0;
        
        if($brand_usename!=''){ $brand_id = @$this->get_user_data('user_login','user_name',$brand_usename)[0]['id']; }
        if($brand_id > 0){

             $query = $this->db->query('select count(z.id) as cnt from
(SELECT a.id,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN (select distinct u.`look_id` from `look_products` as u,`product_desc` as i,product_store as ps where u.`product_id` = i.`id` and ps.`product_id` = i.id AND ps.store_id = '.$store_id.')) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4|| z.look_type = 6) order by z.modified_datetime desc');
            $res = $query->result_array();
            if(!empty($res)){
                return $res[0]['cnt'];
            }else{
                return 0;
            }
        }

    }

       function get_category_wise_looks($offset=null,$price_flt){
        $where_condition = ' order by z.look_price';

        if($price_flt==1){
            $where_condition = ' order by z.look_price desc';            
        }

        if($offset>=0){
            $offset = $offset*9;
            $where_condition = $where_condition.' limit '.$offset.',9';
        }
        $query = $this->db->query('select z.id,IF(z.look_price<>"",z.look_price,0) as look_price,z.slug,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.status,z.deflag,z.modified_datetime from
(SELECT a.id,(select SUM(y.price) from look_products as x,product_desc as y where x.product_id=y.id and x.look_id = a.id and y.is_delete=0 and y.status=1) as look_price,a.image,a.slug,a.look_type,b.image as new_img,a.name,a.created_datetime,a.status,a.deflag,a.modified_datetime FROM `look` as a left join `product_desc` as b on a.image = b.id  where 1 and a.deflag=0 and a.status=1 and a.id IN ((select distinct look_id from look_category_mapping where cat_id = 54)) ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4) '.$where_condition);

        $res = $query->result_array($query);
        return $res;
    }

    function get_store_specific_products($brand_id){
       $query = $this->db->query('SELECT `id`, `brand_id`, `name`, `url`, `description`, `slug`, `price`, `compare_price`, `price_range`, `target_market`, `rating`,`image`, `approve_reject`, `status`,`is_delete`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`,(select SUM(stock_count) from product_inventory where product_id = `product_desc`.id) as stock_count from product_desc where status = 1 and is_delete =0 and approve_reject IN("D","A") and `brand_id` ='.$brand_id.' order by RAND()'); 

       $res = $query->result_array();
       return $res;

    }
    function get_popup(){
        /*$query = $this->db->query("SELECT `id`, `popup_name`, `popup_content`, `target_link`, `page_link`, `popup_interval_chosen`, `start_datetime`, `end_datetime`, `status`, `desktop_img`, `mobile_img`, `created_by`, `modified_by`, `created_datetime`, `modifided_datetime` FROM `popups` WHERE 1 and `start_datetime`<= NOW() and `end_datetime`>=NOW() and status = 1 order by id desc limit 0,1");
        $res = $query->result_array($query);
        return $res;*/
    }

       function get_brands_list($brand_id=''){
        $data = array();
        $brand_array = @unserialize(FESTIVE_SALE_BRAND);
        if(!empty($brand_array)){
            if($brand_id==''){
                $brand_list = implode(',', $brand_array);
            }else{
                $brand_list = $brand_id;
            }
            $query = $this->db->query('select a.home_page_promotion_img,a.`user_id`, UPPER(a.`company_name`) as company_name,b.`user_name` from `brand_info` as a,`user_login` as b,`user_info` as c where a.`user_id` = b.`id` and a.`user_id` = c.`user_id` and a.`is_paid` = 1  AND a.`user_id` IN ('.$brand_list.') order by a.company_name');
            $data = $query->result_array();

        }
        return $data;
    }

    function get_brands_related_category(){
        $data = array(); $cat_ids = '0';$tag_id = '5558';
        $brand_array = @unserialize(FESTIVE_SALE_BRAND);
        if(!empty($brand_array)){
            $brand_list = implode(',', $brand_array);
            $query = $this->db->query('select distinct b.category_id as cat_id from product_desc as a,category_object_relationship as b,product_tag as c where a.id = b.object_id and b.object_id = c.product_id and b.object_type= "product" and a.is_delete = 0 and a.status = 1 and a.approve_reject IN ("A","D") and a.brand_id IN ('.$brand_list.') and c.tag_id ='.$tag_id);
            $res = $query->result_array();

            if(!empty($res)){ $i = 0;
                foreach($res as $val){
                    if($i!=0){ $cat_ids.= ','; }
                    $cat_ids.= $val['cat_id'];
                    $i++;
                }
            }
           $cat_ids = '77,134,117,333,5,143,366,371';
            $query = $this->db->query('SELECT `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `status`, `sort_order`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`, `old_id`, `level`, `size_guide`, `comp_cat` FROM `category` WHERE 1 and category_parent != "-1" and id IN ('.$cat_ids.') and id NOT IN (112,60,7,4,10,11,371) and status = 1 order by sort_order');
            $data = $query->result_array();
        }
        return $data;
    }

    function get_products_data($cat_id,$brand_id=0,$offset=0,$sort=0){
        $data =array();
        $tag_id = '5558';
        $product_id = '0';
        $brand = '';  $w_c = '';
        if($offset == 0){
            $limit = 12;
            $offset = $offset*$limit;
        }else{
            $limit = 6;
            $offset = ($offset+1)*$limit;
        }
        $order_by = 'a.id desc,a.modified_datetime desc';

        $get_pro_id = $this->db->query("select distinct object_id from category_object_relationship where object_type='product' and category_id = '".$cat_id."' order by object_id desc");
            $result = $get_pro_id->result_array();

            if(!empty($result)){
                foreach($result as $val){
                    $product_ids[] = $val['object_id'];
                }

                $product_id = implode(',', $product_ids);
            }
        $w_c .= ' and a.id IN ('.$product_id.')';  
        if($brand_id!=0){
            $w_c .= ' and a.brand_id IN ('.$brand_id.')';
        }else{
            $brand_array = @unserialize(FESTIVE_SALE_BRAND);
                if(!empty($brand_array)){
                $brand_list = implode(',', $brand_array);
                $w_c .= ' and a.brand_id IN ('.$brand_list.')';
            }
        }

        if($sort=='2'){
            $order_by = 'a.price asc';
        }else if($sort=='1'){
            $order_by = 'a.price desc';
        }
        $get_products = $this->db->query("select distinct a.id,a.name ,a.image,a.price,a.brand_id,a.slug,
        CASE 
        WHEN  a.discount_start_date <= CURRENT_TIMESTAMP() and a.discount_end_date >= CURRENT_TIMESTAMP() then a.compare_price
        ELSE 0
        END AS compare_price from product_desc as a,product_inventory as b,product_tag as c where 1 and a.id = b.product_id and b.product_id = c.product_id and a.status = 1 and a.is_delete=0 and a.approve_reject IN ('A','D') and b.stock_count > 0 and c.tag_id = ".$tag_id." ".$w_c." order by ".$order_by." limit ".$offset.",".$limit);
        $res = $get_products->result_array();


        $get_brands = $this->db->query("select a.brand_id as brand_id from product_desc as a,product_inventory as b,product_tag as c where 1 and a.id = b.product_id and b.product_id = c.product_id and a.status = 1 and a.is_delete=0 and a.approve_reject IN ('A','D') and b.stock_count > 0 and c.tag_id = ".$tag_id." ".$w_c."");
        $brandslist = $get_brands->result_array();

        if(!empty($brandslist)){ $i = 0;
            foreach ($brandslist as $key => $value) {
                   if($i!=0) { $brand_id = $brand_id .',';}  
                   $brand_id = $brand_id .$value['brand_id'];
                $i++;
            }
        }

        $data['filter']['paid_brands'] = $this->home_model->get_brands_list(@$brand_id);
        $data['products'] = $res;

        return $data;
    }

    function get_user_pa($user_id){
        $query = $this->db->query('SELECT a.`question_id`,b.`answer` FROM `users_answer` AS a, `answers` AS b WHERE 1 AND b.`id` = a.`answer_id` and a.`user_id`= '.$user_id);
        $res = $query->result_array();
        return $res;
    }

    function get_user_cart($user_id)
    {
        if($user_id!='')
        {
            $query = $this->db->query('SELECT  a.`id` as cartid FROM `cart_info` as a WHERE a.`user_id`!=0 and a.`order_id` is NULL and a.`user_id`= '.$user_id.' order by a.`id` desc '); 
        
            $result = $query->result_array();

            if($result)
            {
                return 1;
            }else
            {
                return 0;
            }           
        }else
        {
            return 0;
        }
        
    }

        public function senddata_onesignal()
      {
            $osdata = array();
            $encode_os = '';
            if($this->session->userdata('first_name')!=''){
            $os_fname = (@$this->session->userdata('first_name')!='') ? $this->session->userdata('first_name') : ' ';
            $os_lname = (@$this->session->userdata('last_name')!='') ? $this->session->userdata('last_name') : ' ';
            $osdata['name'] =  $os_fname.' '.$os_lname;        
            $osdata['email'] =   (@$this->session->userdata('email')!='') ? $this->session->userdata('email') : ' ';
            $osdata['gender'] =   (@$this->session->userdata('gender')!='' && @$this->session->userdata('gender')==1 ) ? 'female' : 'men ';
            $osdata['user_id'] =   (@$this->session->userdata('user_id')!='') ? $this->session->userdata('user_id') : ' ';

            $pa_data = $this->get_user_pa($this->session->userdata('user_id'));
             $question_ans = array();
                 if(!empty($pa_data)){
                    foreach($pa_data as $val){
                         $question_ans[$val['question_id']] = $val['answer'];
                    }
                 } 
            $cart = $this->get_user_cart(@$this->session->userdata('user_id'));
            $bodyType = '';
            if(@$this->session->userdata('gender')!='' && @$this->session->userdata('gender')==1)
            { /*For Female*/         

               
              $osdata['user_age'] =   (@$question_ans[8]!='') ? $question_ans[8] : ' ';
              $osdata['bucket_id'] =   (@$this->session->userdata('bucket')!='') ? $this->session->userdata('bucket') : ' ';
              $osdata['user_size'] =   (@$question_ans[9]!='') ? $question_ans[9] : ' ';
              $osdata['user_style'] =   (@$question_ans[2]!='') ? $question_ans[2] : ' ';
              $osdata['user_budget'] =   (@$question_ans[7]!='') ? $question_ans[7] : ' ';
              $osdata['user_body_shape'] = (@$question_ans[1]!='') ? $question_ans[1] : ' ';
              $osdata['abandoned_cart'] =  (@$cart==1) ? 'yes' : 'no';  
              $osdata['body_type'] =   ''; 
              $osdata['work_style'] = '';
              $osdata['body_height'] = '';
              $osdata['personal_style'] = '';

            }else if(@$this->session->userdata('gender')!='' && @$this->session->userdata('gender')==2)
            { /*For Male*/          
              $bt = strip_tags(@$question_ans[13]);
              $bodyType=substr($bt, 0, strrpos($bt, ' '));
              $osdata['user_age'] =   (@$question_ans[15]!='') ? $question_ans[15] : ' ';
              $osdata['body_type'] =   (@$bodyType!='') ? $bodyType : ' ';
              $osdata['bucket_id'] =   (@$this->session->userdata('bucket')!='') ? $this->session->userdata('bucket') : ' ';
              $osdata['work_style'] =   (@$question_ans[14]!='') ? strip_tags($question_ans[14]) : ' ';
              $osdata['body_height'] =   (@$question_ans[17]!='') ? $question_ans[17] : ' ';
              $osdata['user_budget'] =   (@$question_ans[18]!='') ? $question_ans[18] : ' ';
              $osdata['personal_style'] =   (@$question_ans[16]!='') ? $question_ans[16] : ' ';
              $osdata['abandoned_cart'] =   (@$cart==1) ? 'yes' : 'no'; 
              $osdata['user_size'] = ''; 
              $osdata['user_style'] = ''; 
              $osdata['user_body_shape'] = '';      
            }else
            {
                  $osdata['user_age'] =   '';
                  $osdata['body_type'] =   ''; 
                  $osdata['bucket_id'] =   ''; 
                  $osdata['work_style'] =   ''; 
                  $osdata['body_height'] =   ''; 
                  $osdata['user_budget'] =   ''; 
                  $osdata['personal_style'] =  ''; 
                  $osdata['abandoned_cart'] =   ''; 
                  $osdata['user_size'] = ''; 
                  $osdata['user_style'] = ''; 
                  $osdata['user_body_shape'] = '';      
            }

            $encode_os = json_encode($osdata);
               return $encode_os;
        }else{
            return '';
        }
             //$this->session->set_userdata(array('onesignaldata'=>$os_data));
         
      }

	function category_data(){
		$query = $this->db->query("select id,category_name as label,category_slug as slug from category where id IN (".FOOTER_CATEGORY.");");
		$res = $query->result_array($query);

		if(!empty($res)){
			return serialize($res);
		}
	}
	
	function footer_brand_data(){
        $query = $this->db->query("select a.`user_id`, UPPER(a.`company_name`) as company_name,b.`user_name`,@type:='' as store_id from `brand_info` as a,`user_login` as b,`user_info` as c where a.`user_id` = b.`id` and a.`user_id` = c.`user_id` and a.`show_in brand_list` = 1 AND a.store_type != 3  AND a.`user_id` IN (".FOOTER_BRAND_LIST.") order by a.company_name");
        $res = $query->result();
        if(!empty($res)){
            return serialize($res);
        }
    }


 }
 
?>
