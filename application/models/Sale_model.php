<?php
class Sale_model extends MY_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function getSaleBrands($cnt=null){
        $limit = 12; $offset = ($cnt-1) * $limit; $limit_cond ='';
        if($cnt>0){ $limit_cond = 'limit '.$offset.','.$limit; }

        $sale_products = $this->db->query("select distinct brand_id from (select distinct b.brand_id as brand_id from discount_availed as a,product_desc as b,brand_info as c where a.product_id = b.id and b.brand_id = c.user_id and a.start_date <= CURRENT_TIMESTAMP() and a.end_date >= CURRENT_TIMESTAMP() and b.approve_reject IN ('A','D') and b.status = 1 and b.is_delete = 0 and a.discount_percent > 0 UNION ALL select distinct brand_id as brand_id from product_desc where 1 and is_delete = 0 and status =1 and approve_reject IN('A','D') and compare_price >= price and compare_price != 0 and discount_end_date >= CURRENT_TIMESTAMP()) as z order by z.brand_id desc ");
        $sale_products = $sale_products->result_array();
        $product_ids = '';
        if(!empty($sale_products)){ $k = 0;
            foreach($sale_products as $val){
                if($k!=0){ $product_ids = $product_ids.','; }
                $product_ids = $product_ids.$val['brand_id'];
                $k++;
            }
        }

    	$sale_brands = $this->db->query("select a.company_name,a.user_id as brand_id,b.user_name from brand_info as a,user_login as b where a.user_id = b.id and a.user_id IN (".$product_ids.") order by a.company_name ".$limit_cond);
    	$brands = $sale_brands->result_array();
    	return $brands;
    }

    function getSaleProducts($offset='',$category='',$brands='',$sort=''){

        $where_cond = '';
        /*$sale_products = $this->db->query("select distinct z.id as product_id from (select distinct id from product_desc where 1 and is_delete = 0 and status =1 and approve_reject IN('A','D') and price >= compare_price and compare_price != 0 and discount_end_date >= NOW() UNION ALL select distinct a.product_id as id from discount_availed as a,product_desc as b,brand_info as c where a.product_id = b.id and b.brand_id = c.user_id and a.end_date >= CURDATE() and b.approve_reject IN ('A','D') and b.status = 1 and b.is_delete = 0 and a.discount_percent > 0 ) as z order by z.id desc");*/

         $sale_products = $this->db->query("select product_id from (select a.product_id as product_id from discount_availed as a,product_desc as b,brand_info as c where a.product_id = b.id and b.brand_id = c.user_id and a.start_date <= CURRENT_TIMESTAMP() and a.end_date >= CURRENT_TIMESTAMP() and b.approve_reject IN ('A','D') and b.status = 1 and b.is_delete = 0 and a.discount_percent > 0 UNION ALL select id as product_id from product_desc where 1 and is_delete = 0 and status =1 and approve_reject IN('A','D') and compare_price >= price and compare_price != 0 and discount_end_date >= CURRENT_TIMESTAMP()) as z order by z.product_id desc ");
        $sale_products = $sale_products->result_array();
        $product_ids = ''; $res = array();
        
        $limit_cond = '';
        $limit = 12; $offset_new = $offset;
        //$order_by = "RAND(".$offset_new.")";
        $order_by = "a.id desc";
        if($offset == ''){ $offset = 0; } else{ $offset = $offset*$limit; }
        
        $limit_cond = "limit ".$offset.",".$limit;
        
       
        if(!empty($sale_products)){ $k = 0;
            foreach($sale_products as $val){
                if($k!=0){ $product_ids = $product_ids.','; }
                $product_ids = $product_ids.$val['product_id'];
                $k++;
            }
        }

        if($category!=''){ $where_cond =$where_cond.' and c.category_id IN ('.$category.')'; }
        if($brands!=''){ $where_cond =$where_cond.' and a.brand_id IN ('.$brands.')'; }
        if($sort==2){ $order_by =' a.price asc'; }
        if($sort==1){ $order_by =' a.price desc'; }
        

        if($product_ids!=''){
            $get_products = $this->db->query("select distinct a.id,a.name,a.image,a.price,a.brand_id,a.slug,CASE 
        WHEN  DATE_FORMAT(a.discount_start_date,'%Y-%m-%d  %h:%i') <= DATE_FORMAT(now(),'%Y-%m-%d %h:%i') and DATE_FORMAT(a.discount_end_date,'%Y-%m-%d  %h:%i') >= DATE_FORMAT(now(),'%Y-%m-%d %h:%i') then a.compare_price
        ELSE 0
        END AS compare_price from product_desc as a,product_inventory as b,category_object_relationship as c where 1 and a.id = b.product_id and a.id = c.object_id and c.object_type= 'product' and a.status = 1 and a.is_delete=0 and a.approve_reject IN ('A','D') and b.stock_count > 0 and a.id IN (".$product_ids.") ".$where_cond." and a.approve_reject IN ('A','D') and a.status = 1 order by ".$order_by." ".$limit_cond);
            $res = $get_products->result_array();
            //echo $this->db->last_query();
        }
        return $res;
    }

    function getUsername($id){
        $get_username = $this->db->query("select user_name from user_login where id='".$id."'");
        $res = $get_username->result_array();
        if(!empty($res)){ return $res[0]['user_name']; }else{ return ''; }
    }

}
?>