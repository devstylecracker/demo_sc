<?php
class Looks_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


    function get_look_info($look_slug){
    	$data = array();
    	if($look_slug){
    		$query = $this->db->query("select z.description,z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime,z.status,z.deflag,z.looks_for from 
(SELECT a.looks_for,a.description, a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and (a.status=1 OR a.id=16249) and a.slug = '".$look_slug."' ) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6) ");
			$res = $query->result_array();
            if(empty($res)){
                return $data;
            }
			$data['look_info'] = $res;
            $data['look_info'][0]['look_tag'] = $this->get_look_tags($data['look_info'][0]['id']);

			if($data['look_info'][0]['id']!=''){

            $this->lookTrack($data['look_info'][0]['id']);
			$query = $this->db->query("select distinct a.`id`,a.sc_product_color,a.brand_id,a.name,a.product_cat_id,a.slug,a.url,a.price,a.image,CASE 
        WHEN  a.discount_start_date <= CURRENT_TIMESTAMP() and a.discount_end_date >= CURRENT_TIMESTAMP() then a.compare_price
        ELSE 0
		END AS compare_price,c.company_name,c.`is_paid`,c.`return_policy`,a.`description`,(select n.`size_guide` from `product_variations` as m,`product_variation_type` as n where m.`product_var_type_id` = n.`id` and m.`product_id` = a.`id` order by m.id desc limit 0,1) as size_guide from product_desc as a,look_products as b,brand_info as c where a.status = 1 and a.is_delete=0 and a.approve_reject = 'A' and a.status = 1 and a.`id` = b.`product_id` and a.price>0 and c.`user_id` = a.`brand_id` and b.`look_id`='".$data['look_info'][0]['id']."' order by c.is_paid desc");
			$res = $query->result_array();
			//$data['products_info'] = $res;

            $product_data = array();
            $sortOrder = array();
            $is_paidSort = array();
            if(!empty($res)){ $i = 0;
                foreach ($res as $key => $value) {
                    $product_data[$i]['id'] = $value['id'];
                    $product_data[$i]['name'] =$value['name'];
                    $product_data[$i]['slug'] =$value['slug'];
                    $product_data[$i]['url'] =$value['url'];
                    $product_data[$i]['price'] =$value['price'];
                    $product_data[$i]['image'] =$value['image'];
                    $product_data[$i]['company_name'] =$value['company_name'];
                    $product_data[$i]['is_paid'] =$value['is_paid'];
                    $product_data[$i]['description'] =$value['description'];
                    $product_data[$i]['discount_percent'] = $this->get_discount_percentage($value['brand_id'],$value['id']);
                    $product_data[$i]['discount_price'] =($value['price']*$product_data[$i]['discount_percent'])/100;
                    $product_data[$i]['size_guide'] = $this->get_size_guide($value['id']);
                    $product_data[$i]['brand_id'] = $value['brand_id'];
                    $product_data[$i]['sc_product_color'] = $value['sc_product_color'];
                    $product_data[$i]['return_policy'] = $value['return_policy'];                                 
                    $product_data[$i]['product_cat_id'] = $value['product_cat_id'];
					$product_data[$i]['compare_price'] = $value['compare_price'];
                    $sortOrder[$i] = $this->get_discount_percentage($value['brand_id'],$value['id']);
                    $is_paidSort[$i] = $value['is_paid'];
                    $i++;
                }
            }

            /*For Sorting Array by Discount and is_paid*/
            array_multisort($sortOrder,SORT_DESC,$is_paidSort,SORT_DESC,$product_data);
            /*End For Sorting Array by Discount and is_paid*/

            $data['products_info'] = $product_data;
            $query = $this->db->query("select distinct a.`product_id`,c.`size_text`,b.stock_count,c.`id` from `look_products` as a,`product_inventory` as b,`product_size` as c where a.`product_id` = b.`product_id` and b.`size_id` = c.`id` and a.`look_id`='".$data['look_info'][0]['id']."' and b.stock_count > 0 order by c.`order` asc");
            $res = $query->result_array();
             $product_inv = array();
            if(!empty($res)){
                
                foreach($res as $val){
                    $product_inv[$val['product_id']]['size_text'][$val['id']] = $val['size_text'];
                    
                }
            }
            $data['products_inventory'] =$product_inv;
            $query = $this->db->query("select c.`user_name` , d.`company_name` from `user_login` as c,brand_info as d where c.`id` = d.`user_id` and c.id IN (select distinct b.`brand_id` from `look_products` as a,`product_desc` as b where a.`product_id` = b.`id` and a.`look_id` = '".$data['look_info'][0]['id']."')");
            $res = $query->result_array();
            $data['brands_info'] = $res;

            $user_id = $this->session->userdata('user_id');
            if($user_id > 0){
                $res =  $this->db->get_where('look_fav', array('look_id' =>$data['look_info'][0]['id'],'user_id' => $user_id));
                $data['fav_count'] =  count($res->result_array());

            }

			}

		}
		return $data;
    }

    function lookTrack($look_id){
        if($look_id){
        $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = 0; }

        $query = $this->db->query("insert into users_look_click_track(user_id,look_id) values('".$user_id."','".$look_id."')");
       }
    }

    

    function get_products_extra_info($product_id){
        $res =  $this->db->get_where('product_desc', array('id' =>$product_id));
        $result = $res->result_array();
        return $result[0];
        
    }

    function get_products_extra_images($product_id){
        $res =  $this->db->get_where('extra_images', array('product_id' =>$product_id));
        $result = $res->result_array();
        return $result;
        
    }

    function get_size_guide($product_id){
        if($product_id > 0){
           
            $query = $this->db->query("select b.size_guide from category_object_relationship as a,category as b where a.category_id = b.id and a.object_type = 'product' and a.object_id ='".$product_id."' order by b.size_guide desc limit 0,1");

            $result = $query->result_array();
            if(!empty($result)){
                return $result[0]['size_guide'];
            }else{
                return 0;
            }
        }else{ return 0; }
    }
    
     function get_discount_percentage($brand_id,$product_id){
        if($brand_id > 0){
            $query = $this->db->query("select discount_percent from discount_availed as da where (da.brand_id = '".$brand_id."' || da.product_id = '".$product_id."') AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1");
           /* echo $this->db->last_query();*/
            $result = $query->result_array();
            if(!empty($result)){
                return $result[0]['discount_percent'];
            }else{
                return 0;
            }
        }else{ return 0; }
    }

      function get_discount_id($brand_id,$product_id)
    { 
        if($brand_id > 0 && $product_id > 0){
           $query = $this->db->query("select id, discount_type_id from discount_availed as da where (da.brand_id = '".$brand_id."' OR da.product_id = '".$product_id."' ) AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1 ");
          /*   $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = '".$brand_id."' AND da.discount_type_id='".$discount_type_id."' ) ");*/          
            $result = $query->result_array();

            if(!empty($result)){
                return $result[0];
            }else{
                return 0;
            }
        }else if($brand_id > 0){
            $query = $this->db->query("select id, discount_type_id from discount_availed as da where (da.brand_id = '".$brand_id."' ) AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1 ");
          /*   $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = '".$brand_id."' AND da.discount_type_id='".$discount_type_id."' ) ");*/          
            $result = $query->result_array();
            if(!empty($result)){
                return $result[0];
            }else{
                return 0;
            }

        }else{ 

            if($product_id > 0){
                $query = $this->db->query("select da.id, da.discount_type_id from discount_availed as da where (da.brand_id = 0 AND da.product_id = '".$product_id."' ) AND da.status=1");
                 /*  $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = '".$product_id."' AND da.discount_type_id='".$discount_type_id."' ) ");*/
                $result = $query->result_array();
                if(!empty($result)){
                    return $result[0];
                }else{
                    return 0;
                }            
            }else
            {
                $query = $this->db->query("select da.id, da.discount_type_id from discount_availed as da where (da.brand_id = '".$brand_id."' AND da.product_id = 0 ) AND da.status=1");
                 /*  $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = '".$product_id."' AND da.discount_type_id='".$discount_type_id."' ) ");*/
                $result = $query->result_array();
                if(!empty($result)){
                    return $result[0];
                }else{
                    return 0;
                }          
            }
         }  
    }

    function post_review($post_review_text,$ratings,$brand_token){
        $user_id = $this->session->userdata('user_id');
        if($user_id!='' && $post_review_text!='' && $ratings!='' && $brand_token!=''){
            $data =array(
                    'look_id' => $brand_token,
                    'user_id' =>  $user_id,
                    'review_text' =>$post_review_text,
                    'ratings' => $ratings,
                    'created_by' => $user_id,
                    'created_datetime'=> $this->config->item('sc_date')
                    );
            $this->db->insert('look_review', $data);            
        }
    }

    function get_reviews($look_id,$limit){
        if($limit>0){
            $limit_cond = "limit 0,".$limit;
        }else{
            $limit_cond ="";
        }
         $query = $this->db->query("SELECT b.`user_name`,a.`id`, a.`look_id`, a.`user_id`, a.`review_text`, a.`created_by`, a.`modified_by`, a.`created_datetime`, a.`modified_datetime`, a.`status`, a.`ratings` FROM `look_review` as a,user_login as b WHERE 1 and a.`look_id` = '".$look_id."' and a.status = 1 and a.`user_id` = b.id order by a.id desc ".$limit_cond);
        
        $res = $query->result_array();
        return $res;
    }

    function get_user($user_id){
        $query = $this->db->query("select first_name,last_name,profile_pic from user_info where user_id=".$user_id);
        $res = $query->result_array();
        return $res;
    }

    function get_look_tags($look_id)
    {
        $query = $this->db->query("select look_tags from look_tags where look_id=".$look_id);
        $res = $query->result_array();
        return @$res[0]['look_tags'];
    }

    function get_size_guide_n($sgid=NULL,$pid=NULL,$bid=NULL){
        $product_id = $pid;
        $brand_id = $bid;
        $size_guide_id = $sgid;
        $size_guide_url = '';
            if($product_id > 0 && $brand_id > 0){
                if($size_guide_id == 2){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/top.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/top.jpg';
                    }
                }

                if($size_guide_id == 3){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/bottom.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/bottom.jpg';
                    }
                }

                if($size_guide_id == 4){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/footwear.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/footwear.jpg';
                    }
                }

                if($size_guide_id == 5){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/top_bottom.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/top_bottom.jpg';
                    }
                }

                if($size_guide_id == 6){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/top_men.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/top_men.jpg';
                    }
                }
                if($size_guide_id == 7){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/bottom_men.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/bottom_men.jpg';
                    }
                }

                 if($size_guide_id == 8){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/footwear_men.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/footwear_men.jpg';
                    }
                }

                if($size_guide_id == 9){
                    if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/top_bottom_men.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/top_bottom_men.jpg';
                    }
                }
            }

            if(trim($size_guide_url) == ''){
                /*Women Default Size Guide*/
                if($size_guide_id == 2){
                    if(file_exists('assets/images/size_guide/stylecracker/top.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/stylecracker/top.jpg';
                    }
                }

                else if($size_guide_id == 3){
                    if(file_exists('assets/images/size_guide/stylecracker/bottom.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/stylecracker/bottom.jpg';
                    }
                }

                else if($size_guide_id == 4){  
                    if(file_exists('assets/images/size_guide/stylecracker/footwear.jpg')){
                        $size_guide_url = base_url().'assets/images/size_guide/stylecracker/footwear.jpg';
                    }

                }              
                else if($size_guide_id == 5){
                    if(file_exists('assets/images/size_guide/stylecracker/top_bottom.jpg')){
                    $size_guide_url = base_url().'assets/images/size_guide/stylecracker/top_bottom.jpg';
                    }
                 /*Mens Default Size Guide*/
                }else if($size_guide_id == 6){  
                     $size_guide_url = base_url().'assets/images/size_guide/stylecracker/top_men.jpg';
                }

                else if($size_guide_id == 7){
                    $size_guide_url = base_url().'assets/images/size_guide/stylecracker/bottom_men.jpg';
                }

                else if($size_guide_id == 8){
                    $size_guide_url = base_url().'assets/images/size_guide/stylecracker/footwear_men.jpg';
                }

                else if($size_guide_id == 9){
                    $size_guide_url = base_url().'assets/images/size_guide/stylecracker/top_bottom_men.jpg';

                }else{
                    //$size_guide_url = base_url().'assets/images/size_guide/stylecracker/sc_size_guide.jpg';

                }
            }
        
        return $size_guide_url;

    }

}

?>