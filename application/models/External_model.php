<?php
class External_model extends Schome_model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    /*
     function download_product_json(){
        //select all products for MAd street den
        $query = $this->db->query('select  id,name, slug, image FROM `product_desc` WHERE `id` >="3598" AND `is_delete` = 0 order by id desc'); 
        //SELECT name, image, slug FROM product_desc
        $result = $query->result_array();
        $posts = array();
        $image_folder = 'http://www.stylecracker.com/sc_admin/assets/products/';
        if(!empty($result)) 
        { 
            foreach($result as $row){
				$id = stripslashes($row['id']); 
                $name = stripslashes($row['name']); 
                $img = stripslashes($row['image']); 
                $slug = stripslashes($row['slug']);
				
                $posts[] = array('id'=> $id,'Name'=> $name, 'URL'=> $image_folder.$img, 'UNIQUE_SLUG'=> $slug );

            }
            return $posts;
        }
		
        else return NULL;
        
        
    }
    */
	
	
    function download_looks_json($limit,$offset,$timestamp){
	if($timestamp != '') $modify_date = "AND a.modified_datetime > $timestamp"; else  $modify_date = '';
    	$query = $this->db->query("select z.slug,z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.modified_datetime as timestamp,z.status,z.deflag,@type:='look' as type from 
(SELECT a.slug,a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.modified_datetime,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id where 1 and a.deflag=0 and a.status=1 $modify_date) as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 || z.look_type = 4 || z.look_type = 6)  order by z.modified_datetime DESC limit $limit offset $offset ");

	$res = $query->result_array();
	$looks_array = array();
	$i = 0;
     	if(!empty($res)){
		foreach($res as $val){
			$alllooksarrayobject = new stdClass(); 
			$alllooksarrayobject->id = $val['id'];
			$alllooksarrayobject->name = $val['name'];
			$alllooksarrayobject->timestamp = $val['timestamp'];
			if($val['look_type'] == 1){
				$alllooksarrayobject->url  = $this->config->item('sc_look_image_url').$val['image'];
			}else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
				$alllooksarrayobject->url = $this->config->item('sc_street_style_image_url').$val['look_image'];
			}else if($val['look_type'] == 3){
				$alllooksarrayobject->url = $this->config->item('sc_promotional_look_image').$val['product_img'];
			}
			$info = getimagesize($alllooksarrayobject->url);
			$alllooksarrayobject->height = $info[1];
			$alllooksarrayobject->width  = $info[0];
			$alllooksarrayobject->type = $val['type'];
			$alllooksarrayobject->look_type = $val['look_type'];
			$alllooksarrayobject->products = $this->single_look_mobile($val['id']);
			//$alllooksarrayobject->user_likes = $this->get_user_likes($val['id']);
			//$alllooksarrayobject->look_type_txt = $this->get_sub_category($val['id']);
			$looks_array[$i] = $alllooksarrayobject;
			$i++;
		}
	
	
	}
	//echo "<pre>";print_r($looks_array);exit;
	return $looks_array;
    }
	
	
	function single_look_mobile($look_id)
	{
        
	 $query = $this->db->query("Select CONCAT('http://www.stylecracker.com/sc_admin/assets/products/',p.image) as url,p.price,p.id,p.name,p.description,(select bi.company_name from brand_info bi where bi.user_id=p.brand_id) AS brand_name from look_products as lp, product_desc as p where p.id = lp.product_id AND p.approve_reject='A' AND look_id =$look_id");
     
	 $res = $query->result_array();	
	 return $res;
	
	}
	
	
	function download_product_json($limit,$offset,$timestamp){
	        //select all products for MAd street den
	        if($timestamp != '') $modify_date = "AND modified_datetime > $timestamp"; else  $modify_date = '';
	        $query = $this->db->query("select  modified_datetime,id,name, slug, CONCAT('http://www.stylecracker.com/sc_admin/assets/products/',image) as url FROM `product_desc` WHERE `id` >='3598' AND `is_delete` = 0 $modify_date  order by modified_datetime DESC limit $limit offset $offset"); 
			
	        //SELECT name, image, slug FROM product_desc
	        $result = $query->result_array();
		$products_array = array();
		$i = 0;
	        if(!empty($result)) 
	        { 
		    	foreach($result as $val){
				$products = new stdClass(); 
				$products->id = $val['id'];
				$products->name = $val['name']; 
				$products->timestamp = $val['modified_datetime']; 
				$products->img = $val['url']; 
				$products->slug = $val['slug'];
				$products->tag = $this->product_tag_maping($val['id']);
				$products->category = $this->product_category_maping($val['id']);
				$products->sub_category = $this->product_sub_category($val['id']);
				$products->variation_type=$this->product_variation_type($val['id']);
				$products->variation_value=$this->product_variation_value($val['id']);
		                $products_array[$i] = $products;
				$i++;
			}
	           
	        }
	        
			//echo "<pre>";print_r($products_array);exit;
		return $products_array;
    	}
	
	
	function product_tag_maping($id)
	{
  	 $query = $this->db->query("Select b.name from product_tag a INNER JOIN tag b where a.tag_id = b.id AND a.product_id = $id");     
	 $res = $query->result_array();	
	 return $res;	
	}
	
	function product_category_maping($id)
	{
  	 $query = $this->db->query("Select b.name from product_desc a INNER JOIN product_category b where a.product_cat_id = b.id AND a.id = $id");     
	 $res = $query->result_array();	
	 return $res;	
	}
	
	function product_sub_category($id)
	{
  	 $query = $this->db->query("Select b.name from product_desc a INNER JOIN product_sub_category b where a.product_sub_cat_id = b.id AND a.id = $id");     
	 $res = $query->result_array();	
	 return $res;	
	}
	function product_variation_type($id)
	{ 	 
	//$query = $this->db->query("Select b.name from product_desc a INNER JOIN product_variation_type b where
										// a.product_sub_cat_id =  b.id AND a.id = $id");
	$query = $this->db->query("SELECT a.name FROM product_variation_type a INNER JOIN product_variations b WHERE b.product_id = $id AND b.product_var_type_id = a.id ");
	$res = $query->result_array();	
	return $res;
	}
	function product_variation_value($id)
	{ 	 $query = $this->db->query("select a.name from product_variation_value a INNER JOIN product_variations b WHERE b.product_id = $id AND b.product_var_val_id = a.id");
		$res = $query->result_array();	
		return $res;
	}
	
} 
?>