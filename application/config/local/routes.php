<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//##$route['default_controller'] = 'schome';
$route['default_controller'] = 'schome_new';
$route['404_override'] = 'sc404';
$route['translate_uri_dashes'] = FALSE;
$route['sitemap\.xml'] = "sitemap/sitemap";

$route['home'] = "/";
//$route['home'] = "schome_new";
$route['signup'] = "Promo_signup";
$route['advanced-search/search-by-rs-variation'] = "advancedsearch/search_by_rs_variation";
$route['about-us'] = "schome_new/aboutus";
$route['terms-and-conditions'] = "schome_new/terms_of_use";
$route['privacy-policy'] = "schome_new/privacy_policy";
$route['return-policy'] = "schome_new/return_policy";
$route['stylist/stylist-detail/(:any)'] = "stylist/stylist_detail";
$route['faq'] = "schome_new/faq";
//$route['brands/brand-details/(:any)'] = "brands/brand_details";
// $route['brands/brand-details/(:any)'] =  "brands_web/brand_details";
$route['search/(:any)'] = "search/index";
$route['looks/look-details/(:any)'] = "sc404";
$route['sitemap'] = "sitemap/sitemap_html";
$route['sitemap-xml'] = "sitemap/sitemap_xml";
$route['Page/(:any)'] = "Page/index/";
$route['page/(:any)'] = "Page/index/";
//$route['livello'] = "Promo_signup";
$route['livello'] = "schome_new";
$route['download-app'] = "schome_new/redirect";
/* $route['all'] = "schome";
$route['men'] = "schome";
$route['women'] = "schome";
$route['ALL'] = "schome";
$route['MEN'] = "schome";
$route['WOMEN'] = "schome";
$route['All'] = "schome";
$route['Men'] = "schome";
$route['Women'] = "schome"; */
//$route['Scborough'] = "Scborough";
$route['scborough'] = "schome_new";
$route['schome/looks/(:any)'] = "sc404";
$route['looks/(:any)'] = "sc404";
$route['looks/post_review'] = "sc404";
$route['looks/get_reviews'] = "sc404";
$route['looks/get_all_reviews'] = "sc404";
$route['looks/get_products_extra_info'] = "sc404";
$route['looks/(:any)/(:any)'] = "sc404";
/* $route['categories/(:any)/(:any)/(:any)'] = "Categories";
$route['categories/(:any)/(:any)/(:any)/(:any)'] = "Categories";
$route['category-list/(:any)'] = "categories/category_list";
$route['category-list'] = "categories/category_list"; */
// $route['brand-list/(:any)'] = "categories/top_brand_list";
// $route['brand-list'] = "categories/top_brand_list";
//$route['brand-list/(:any)'] = "brands_web";
//$route['brand-list'] = "brands_web";
/*$route['all-products'] = "Allproducts";
$route['all-products/(:any)'] = "Allproducts";
$route['all-products/(:any)/(:any)'] = "Allproducts";
$route['all-products/(:any)/(:any)/(:any)'] = "Allproducts";
$route['all-products/filter_products'] = "Allproducts/filter_products";
$route['all-products/get_more_products'] = "Allproducts/get_more_products";*/
/* $route['look-wishlist'] = "wishlist/look_wishlist";
$route['product-wishlist'] = "wishlist/product_wishlist";
$route['brands-wishlist'] = "wishlist/brands_wishlist";
$route['wheel'] = "brands/show_wheel";
$route['terms-of-use-and-policies'] = "schome/privacy_policy"; */
$route['invite-and-win'] = "invite";
//$route['index.php/sale'] = "sale";
//$route['sale'] = "sale";
//$route['rakhi-special'] = "sc404";
$route['scbox'] = "scbox";
//$route['rakhi-special'] = "scbox/rakhi";


$route['all-products'] = "sc404";
$route['all-products/(:any)'] = "sc404";
$route['all-products/(:any)/(:any)'] = "sc404";
$route['all-products/(:any)/(:any)/(:any)'] = "sc404";
$route['all-products/(:any)/(:any)/(:any)/(:any)'] = "sc404";
$route['all-products/(:any)/(:any)/(:any)/(:any)/(:any)'] = "sc404";
$route['all-products/filter_products'] = "sc404";
$route['all-products/get_more_products'] = "sc404";

$route['brand/(:any)'] = "sc404"; 
$route['brand/(:any)/(:any)'] = "sc404"; 
$route['brand/(:any)/(:any)/(:any)'] = "sc404"; 
$route['brand/(:any)/(:any)/(:any)/(:any)'] = "sc404";
$route['brand/(:any)/(:any)/(:any)/(:any)/(:any)'] = "sc404";
$route['brand/filter_products'] = "sc404";
$route['brand/get_more_products'] = "sc404";
$route['brands/brand-details/(:any)'] = "sc404";
$route['looks/change_price_new'] = "sc404";

$route['kavalai-vendam'] = "sc404";

$route['sale'] = "sc404";
$route['sale/(:any)'] = "sc404";
$route['sale/(:any)/(:any)'] = "sc404";
$route['sale/(:any)/(:any)/(:any)'] = "sc404";
$route['sale/(:any)/(:any)/(:any)/(:any)'] = "sc404";
$route['sale/(:any)/(:any)/(:any)/(:any)/(:any)'] = "sc404";
$route['sale/filter_products'] = "sc404";
$route['sale/get_more_products'] = "sc404";

$route['wedding-pop-up'] = "sc404";
$route['wedding-pop-up/products/(:any)'] = "sc404";
$route['wedding-pop-up/filter_products'] = "sc404";
$route['wedding-pop-up/get_more_products'] = "sc404";
/*
$route['festive-sale'] = "Home/festive_sale";
$route['festive-sale/products/(:any)'] = "Home/festive_products_data";
$route['festive-sale/filter_products'] = "Home/more_products_data";
$route['festive-sale/get_more_products'] = "Home/more_products_data";
*/

$route['daydream'] = "sc404";
$route['daydream/(:any)'] = "sc404";
$route['daydream/(:any)/(:any)'] = "sc404";
$route['daydream/(:any)/(:any)/(:any)'] = "sc404";
$route['daydream/(:any)/(:any)/(:any)/(:any)'] = "sc404";
$route['daydream/(:any)/(:any)/(:any)/(:any)/(:any)'] = "sc404";
$route['daydream/filter_products'] = "sc404";
$route['daydream/get_more_products'] = "sc404";

$route['party-pop-up'] = "sc404";
$route['party-pop-up/products/(:any)'] = "sc404";
$route['party-pop-up/filter_products'] = "sc404";
$route['party-pop-up/get_more_products'] = "sc404";

$route['all-things-new'] = "sc404";
$route['all-things-new/products/(:any)'] = "sc404";
$route['all-things-new/filter_products'] = "sc404";
$route['all-things-new/get_more_products'] = "sc404";

/*Added for All-things-black*/
$route['all-things-black'] = "sc404";
$route['all-things-black/products/(:any)'] = "sc404";
$route['all-things-black/filter_products'] = "sc404";
$route['all-things-black/get_more_products'] = "sc404";
$route['all-black-products'] = "sc404";
$route['all-black-products/(:any)'] = "sc404";
$route['all-black-products/(:any)/(:any)'] = "sc404";
$route['all-black-products/(:any)/(:any)/(:any)'] = "sc404";
$route['all-black-products/(:any)/(:any)/(:any)/(:any)'] = "sc404";
$route['all-black-products/(:any)/(:any)/(:any)/(:any)/(:any)'] = "sc404";
$route['all-black-products/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = "sc404";
$route['all-black-products/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = "sc404";
$route['all-black-products/filter_products'] = "sc404";
$route['all-black-products/get_more_products'] = "sc404";
/*End of All-things-black*/

$route['valentine-pop-up'] = "sc404";
$route['valentine-pop-up/products/(:any)'] = "sc404";
$route['valentine-pop-up/filter_products'] = "sc404";
$route['valentine-pop-up/get_more_products'] = "sc404";

//summer popup
$route['summer-pop-up'] = "sc404";
$route['summer-pop-up/products/(:any)'] = "sc404";
$route['summer-pop-up/filter_products'] = "sc404";
$route['summer-pop-up/get_more_products'] = "sc404";

//travel popup
$route['travel-shop'] = "sc404";
$route['travel-shop/products/(:any)'] = "sc404";
$route['travel-shop/filter_products'] = "sc404";
$route['travel-shop/get_more_products'] = "sc404";

//SCbox
$route['sc-box'] = "scbox";
//$route['book-scbox'] = "scbox/bookscbox";
//$route['book-scbox/(:any)'] = "scbox/bookscbox";

$route['stylistscbox'] = "stylistscbox";
$route['stylistbookscbox'] = "stylistscbox/bookscbox";
$route['stylistbookscbox/(:any)'] = "stylistscbox/stylistbookscbox";

//new ui =====================================================================================================================

//$route['homepage_temp'] = "schome_temp";
//$route['front_temp'] = "schome_temp/home_seventeen";
//$route['home_guest'] = "schome_temp/home_guest_seventeen";

//$route['collections_temp'] = "schome_temp/collections";
//$route['categories_temp'] = "schome_temp/categories";
//$route['products_temp'] = "schome_temp/products";
//$route['cart_temp'] = "schome_temp/cart";
//$route['product-details_temp'] = "schome_temp/product_details";
//$route['sc-box_temp'] = "schome_temp/scbox";
//$route['book-my-stylist_temp'] = "schome_temp/book_my_stylist";
// $route['login'] = "schome_temp/login";
// $route['signup'] = "schome_temp/signup";
//$route['pa_temp'] = "schome_temp/pa";


/* $route['homepage'] = "schome_temp";
$route['front'] = "schome_temp/home_seventeen";
$route['home_guest'] = "schome_temp/home_guest_seventeen";
$route['collections'] = "schome_temp/collections";
$route['categories'] = "schome_temp/categories";
$route['products'] = "schome_temp/products";
$route['cart-temp'] = "schome_temp/cart";
$route['product-details'] = "schome_temp/product_details";
$route['event-details'] = "schome_temp/event_details"; */
//$route['sc-box'] = "schome_temp/scbox";
//$route['book-scbox'] = "schome_temp/book_scbox";

/* $route['book-my-stylist'] = "schome_temp/book_my_stylist";
$route['book-stylist'] = "schome_temp/book_stylist";
$route['personal-styling'] = "schome_temp/personal_styling";
$route['profile'] = "schome_temp/profile";
$route['checkout'] = "schome_temp/checkout";
$route['order-summary'] = "schome_temp/order_summary"; */
// $route['my-orders'] = "schome_temp/order_history";
//end new ui

//routes setting for new ui
$route['schome'] = "/";
$route['homepage'] = "schome_new";
$route['login'] = "schome_new/login";
$route['signup'] = "schome_new/register_user";
$route['pa'] = "schome_new/pa";
$route['pa/(:any)'] = "schome_new/pa";
$route['cart'] = "stylecart";
$route['cart/(:any)'] = "stylecart";
$route['category-list/(:any)'] = "category/category_list";
$route['category-list'] = "category/category_list";
$route['profile'] = "profiledit";
$route['feed'] = "schome_new";

/*$route['sc-box'] = "schome_new/scbox";
$route['book-scbox'] = "schome_new/bookscbox";
$route['book-scbox/(:any)'] = "schome_new/bookscbox";*/
$route['sc-box'] = "scbox";
// $route['book-scbox'] = "scbox/bookscbox";
// $route['book-scbox/(:any)'] = "scbox/bookscbox";



$route['stylist-profile'] = "schome_temp/stylist_profile";
$route['brand-details'] = "schome_temp/brand_details";

//all static pages
$route['about-us'] = "static_pages/page_about_us";
$route['contact-us'] = "static_pages/page_contact_us";
$route['terms-and-conditions'] = "static_pages/page_terms_of_use";
$route['privacy-policy'] = "static_pages/page_privacy_policy";
$route['return-policy'] = "static_pages/page_return_policy";
$route['return-policy-scbox'] = "static_pages/page_return_policy_scbox";
$route['faq'] = "static_pages/page_faq";
$route['help-feedback'] = "static_pages/page_help_feedback";
// $route['brands'] = "static_pages/page_brands";
//// all static pages

//$route['book-scbox/(:any)'] = "schome_new/bookscbox";

//end of routes setting for new ui

//$route['product/details/(:any)'] = "product_web/product_details";
//$route['product/(:any)'] = "product_web/product_details";
$route['product/details/(:any)'] = "/";
$route['product/(:any)'] = "/";
// $route['message/send_message'] = "Message/send_message";

//collection
//$route['collections'] = "collection_web";
//$route['collections/(:any)'] = "collection_web/single_collection";

//event
//$route['events'] = "event_web";
//$route['events/(:any)'] = "event_web/get_single_event";
$route['events'] = "/";
$route['events/(:any)'] = "/";


//blogs
$route['sc-live'] = "blog_web";

//product filter 
$route['category/product-filter/(:any)'] = "category/product_filter";
$route['category/product-filter?(:any)'] = "category/product_filter";
$route['products/(:any)'] = "category/product_filter";
$route['products?(:any)'] = "category/product_filter";

//home page
//$route['home-products'] = "schome_new/get_home_products";
$route['home-products'] = "/";
$route['contact-us'] = "schome_new/contact_us";
//$route['bookmarks'] = "bookmarks";
//$route['bookmarks-(:any)'] = "bookmarks/view_all_bookmark";
$route['bookmarks'] = "/";
$route['bookmarks-(:any)'] = "/";


//brand page
//$route['brands'] = "brands_web";
// $route['brands/brand-details/(:any)'] = "brands_web/brand_details";
//$route['brands/brand-products/(:any)'] = "brands_web/get_brand_products";
//$route['all-brands'] = "brands_web/view_all_brand";
//$route['brands/(:any)'] = "brands_web/brand_details";

$route['brands'] = "sc404";
$route['brands/brand-products/(:any)'] = "sc404";
$route['all-brands'] = "sc404";
$route['brands/(:any)'] = "sc404";

//order pages
$route['my-orders'] = "sc_orders/order_history";

//category list
/* $route['category-list'] = "category/categorylist";
$route['categories'] = "category/categorylist";
$route['categories/(:any)'] = "category/categorylist"; */
$route['category-list'] = "sc404";
$route['categories'] = "sc404";
$route['categories/(:any)'] = "sc404";

//profile edit
$route['profile'] = "profiledit";


//Cart and checkout
$route['cart'] = "stylecart";
$route['checkout'] = "stylecart/checkout";

//yesbank
$route['yesbank'] = "Yes_bank/yesbank_home";
$route['yesbank/scbox'] = "Yes_bank/yesbank_scbox";
$route['yesbank/personal-stylists'] = "Yes_bank/yesbank_personal_stylists";
$route['yesbank/terms-and-conditions'] = "Yes_bank/terms_and_conditions";

$route['Feedback/getcustomUsers'] = "Feedback/getcustomUsers";
$route['feedback/getFeedbackOrders'] = "Feedback/getFeedbackOrders";
$route['feedback/feedbackOrdersResend'] = "Feedback/feedbackOrdersResend";
$route['feedback/getOrdersCustom'] = "Feedback/getOrdersCustom";
$route['feedback/(:any)'] = "Feedback/order";
$route['feedback-data'] = "Feedback/getUsersFeedback";


$route['book-scbox/step1'] = "Scbox_new/step1";
$route['book-scbox/step2'] = "scbox_new/step2";
$route['book-scbox/step2a'] = "scbox_new/step2a";
$route['book-scbox/step3'] = "scbox_new/step3";
$route['book-scbox/step4'] = "scbox_new/step4";
$route['book-scbox/step5'] = "scbox_new/step5";
$route['book-scbox/step6'] = "scbox_new/step6";
$route['book-scbox/step2a'] = "scbox_new/step2a";
$route['scbox-thankyou'] = "scbox_new";