<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/* PAyment gatway Test Paramaters*/

// define('MERCHANT_KEY','gtKFFx');
// define('PAYUSALT','eCwWELxi');
// define('SURL','http://localhost/stylecracker_v2018/stylecart/online_order_success');
// define('PAYMENT_URL','https://test.payu.in/_payment');
// define('SCBOXSURL','http://localhost/stylecracker_v2018/scbox_new/online_order_success');
$site_url = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on' ? 'https' : 'http';
$site_url .= '://'. $_SERVER['HTTP_HOST'].'/';
if($_SERVER['HTTP_HOST']=='localhost')
{
	define('MERCHANT_KEY','gtKFFx');
	define('PAYUSALT','eCwWELxi');
	define('SURL',$site_url.'stylecracker_v2018/stylecart/online_order_success');
	define('PAYMENT_URL','https://test.payu.in/_payment');
	define('SCBOXSURL',$site_url.'stylecracker_v2018/scbox_new/online_order_success');
}else
{
	define('MERCHANT_KEY','gtKFFx');
	define('PAYUSALT','eCwWELxi');
	define('SURL',$site_url.'stylecart/online_order_success');
	define('PAYMENT_URL','https://test.payu.in/_payment');
	define('SCBOXSURL',$site_url.'scbox_new/online_order_success');
}

/*sms gatway*/
define('SMS_USERNAME','stylecracker_com');
// define('SMS_PASSWORD','Style123*');
define('SMS_PASSWORD','SC$123*');//password changed by rajesh on 03-07-2017

define('WOMEN_CATEGORY_ID',1);
define('MEN_CATEGORY_ID',12);
define('KID_CATEGORY_ID',9);
define('COLOR_VARIATION_TYPE',42);

define('BOROUGH_BRANDS',"'22184','22183','22178','21792','20936','20835','21025','20963'");

define('SCB_CHARGES',4500);

define('FILTER_COLOR',serialize(array('1-#FBCEB1;'=>'Apricot','2-#F5F5DC;'=>'Beige','3-#000000;'=>'Black','4-#0000FF;'=>'Blue','5-#964B00;'=>'Brown','6-#FF7F50;'=>'Coral','7-#D4AF37;'=>'Gold','8-#00FF00;'=>'Green','10-#808080;'=>'Grey','11-#C3B091;'=>'Khaki','12-#800000;'=>'Maroon','13-#F0F8FF;'=>'Monochrome','14-#F0F8FF;'=>'Multi','15-#000080;'=>'Navy','16-#F0F8FF;'=>'Nude','17-#FF7F00;'=>'Orange','18-#FFC0CB;'=>'Pink','19-#9F00C5;'=>'Purple','20-#FF0000;'=>'Red','21-#C0C0C0;'=>'Silver','22-#D2B48C;'=>'Tan','23-#FFFFFF;'=>'white','24-#FFFF00;'=>'Yellow')));


define('ALL_FILTER_COLOR',serialize(array('1'=>'Apricot','2'=>'Beige','3'=>'Black','4'=>'Blue','5'=>'Brown','6'=>'Coral','7'=>'Gold','8'=>'Green','10'=>'Grey','11'=>'Khaki','12'=>'Maroon','13'=>'Monochrome','14'=>'Multi','15'=>'Navy','16'=>'Nude','17'=>'Orange','18'=>'Pink','19'=>'Purple','20'=>'Red','21'=>'Silver','22'=>'Tan','23'=>'white','24'=>'Yellow')));

define('FILTER_COLOR_MOBILE',serialize(array('1'=>array('Apricot','#FBCEB1;'),'2'=>array('Beige','#F5F5DC;'),'3'=>array('Black','#000000;'),'4'=>array('Blue','#0000FF;'),'5'=>array('Brown','#964B00;'),'6'=>array('Coral','#FF7F50;'),'7'=>array('Gold','#D4AF37;'),'8'=>array('Green','#00FF00;'),'10'=>array('Grey','#808080;'),'11'=>array('Khaki','#C3B091;'),'12'=>array('Maroon','#800000;'),'13'=>array('Monochrome','#F0F8FF;'),'14'=>array('Multi','#F0F8FF;'),'15'=>array('Navy','#000080;'),'16'=>array('Nude','#F0F8FF;'),'17'=>array('Orange','#FF7F00;'),'18'=>array('Pink','#FFC0CB;'),'19'=>array('Purple','#9F00C5;'),'20'=>array('Red','#FF0000;'),'21'=>array('Silver','#C0C0C0;') ,'22'=>array('Tan','#D2B48C;'),'23'=>array('white','#FFFFFF;'),'24'=>array('Yellow','#FFFF00;'))));

define('POINT_VAlUE',500);
define('MIN_CART_VALUE',1000);
define('REDEEM_POINTS_CART_TEXT','Referral Amount');

define('BACKENDURL','https://www.stylecracker.com/sc_admin');

define('FESTIVE_SALE_BRAND','a:19:{i:0;i:43183;i:1;i:25202;i:2;i:33412;i:3;i:23744;i:4;i:30424;i:5;i:28524;i:6;i:23407;i:7;i:20839;i:8;i:33598;i:9;i:33592;i:10;i:27689;i:11;i:28363;i:12;i:28349;i:13;i:28793;i:14;i:40554;i:15;i:41615;i:16;i:41546;i:17;i:36125;i:18;i:22183;}'); 
define('SINGLE_BRANDS',43162);
define('SINGLE_BRANDS_PAGE_LAYOUT',4); // 3 Column and filter bar ,  4 Column without filter bar
define('PARTY_SALE_BRAND','a:20:{i:0;i:34006;i:1;i:43586;i:2;i:43319;i:3;i:43400;i:4;i:40554;i:5;i:43514;i:6;i:43546;i:7;i:43372;i:8;i:43509;i:9;i:43595;i:10;i:43484;i:11;i:43559;i:12;i:43578;i:13;i:41307;i:14;i:40282;i:15;i:41532;i:16;i:39113;i:17;i:43183;i:18;i:38634;i:19;i:43334;}'); 

define('NEWYEAR_SALE_BRAND','a:35:{i:0;i:27994;i:1;i:29784;i:2;i:27949;i:4;i:40729;i:5;i:35258;i:6;i:30049;i:7;i:36454;i:8;i:35236;i:9;i:31465;i:10;i:33412;i:11;i:40030;i:12;i:34344;i:13;i:34345;i:14;i:41624;i:15;i:25032;i:16;i:27992;i:17;i:39492;i:18;i:28188;i:19;i:36125;i:20;i:23347;i:21;i:31004;i:22;i:40722;i:23;i:28363;i:24;i:29791;i:25;i:25169;i:26;i:29304;i:27;i:32516;i:28;i:25456;i:29;i:36066;i:30;i:37859;i:31;i:25314;i:32;i:20512;i:33;i:27339;i:34;i:25454;i:35;i:29107;}');


define('ALL_THINGS_BLACK','a:103:{i:0;s:5:"20512";i:1;s:5:"20936";i:2;s:5:"22111";i:3;s:5:"23347";i:4;s:5:"23348";i:5;s:5:"23406";i:6;s:5:"23407";i:7;s:5:"23861";i:8;s:5:"23867";i:9;s:5:"24156";i:10;s:5:"24700";i:11;s:5:"24775";i:12;s:5:"24929";i:13;s:5:"25032";i:14;s:5:"25168";i:15;s:5:"25169";i:16;s:5:"25221";i:17;s:5:"25280";i:18;s:5:"25314";i:19;s:5:"25455";i:20;s:5:"25456";i:21;s:5:"26364";i:22;s:5:"26985";i:23;s:5:"27160";i:24;s:5:"27339";i:25;s:5:"27689";i:26;s:5:"27947";i:27;s:5:"27949";i:28;s:5:"27992";i:29;s:5:"27994";i:30;s:5:"28188";i:31;s:5:"28349";i:32;s:5:"28354";i:33;s:5:"28363";i:34;s:5:"28374";i:35;s:5:"28523";i:36;s:5:"28524";i:37;s:5:"28545";i:38;s:5:"28552";i:39;s:5:"28792";i:40;s:5:"29107";i:41;s:5:"29131";i:42;s:5:"29200";i:43;s:5:"29434";i:44;s:5:"29717";i:45;s:5:"29775";i:46;s:5:"29784";i:47;s:5:"29791";i:48;s:5:"29915";i:49;s:5:"30049";i:50;s:5:"30124";i:51;s:5:"30358";i:52;s:5:"30424";i:53;s:5:"30437";i:54;s:5:"30822";i:55;s:5:"30868";i:56;s:5:"30883";i:57;s:5:"30912";i:58;s:5:"30984";i:59;s:5:"31462";i:60;s:5:"31465";i:61;s:5:"31764";i:62;s:5:"32176";i:63;s:5:"33411";i:64;s:5:"33412";i:65;s:5:"32516";i:66;s:5:"33592";i:67;s:5:"33943";i:68;s:5:"34006";i:69;s:5:"34917";i:70;s:5:"35236";i:71;s:5:"35258";i:72;s:5:"35676";i:73;s:5:"35678";i:74;s:5:"35679";i:75;s:5:"35820";i:76;s:5:"36066";i:77;s:5:"36125";i:78;s:5:"36188";i:79;s:5:"36454";i:80;s:5:"36580";i:81;s:5:"38839";i:82;s:5:"39113";i:83;s:5:"39492";i:84;s:5:"39646";i:85;s:5:"39907";i:86;s:5:"40030";i:87;s:5:"40282";i:88;s:5:"40554";i:89;s:5:"40722";i:90;s:5:"40729";i:91;s:5:"41532";i:92;s:5:"43319";i:93;s:5:"43334";i:94;s:5:"43372";i:95;s:5:"43400";i:96;s:5:"43484";i:97;s:5:"43509";i:98;s:5:"43514";i:99;s:5:"43546";i:100;s:5:"43559";i:101;s:5:"43578";i:102;s:5:"43595";}');

define('VALENTINE_SALE','a:22:{i:0;i:45153;i:1;i:45010;i:2;i:44948;i:3;i:44953;i:4;i:44998;i:5;i:44866;i:6;i:44373;i:7;i:32166;i:8;i:33618;i:9;i:44568;i:10;i:44491;i:11;i:20936;i:12;i:43400;i:13;i:28296;i:14;i:43334;i:15;i:38634;i:16;i:22111;i:17;i:41532;i:18;i:30822;i:19;i:35679;i:20;i:31764;i:21;i:44996;}');

//SUMMER SALE
define('SUMMER_SALE','a:18:{i:0;i:45805;i:1;i:45967;i:2;i:45789;i:3;i:41729;i:4;i:45941;i:5;i:45500;i:6;i:45670;i:7;i:45766;i:8;i:45630;i:9;i:45668;i:10;i:40729;i:11;i:41532;i:12;i:36415;i:13;i:40282;i:14;i:43334;i:15;i:25032;i:16;i:36125;i:17;i:40722;}');

//SUMMER SALE
define('TRAVEL_SALE','a:17:{i:0;i:45647;i:1;i:29131;i:2;i:45804;i:3;i:20936;i:4;i:45830;i:5;i:45781;i:6;i:45643;i:7;i:29107;i:8;i:40020;i:9;i:33411;i:10;i:32166;i:11;i:25454;i:12;i:25456;i:13;i:39495;i:14;i:40030;i:15;i:30822;i:16;i:39492;}');

/* Constants New Site */

define('web_url', 'https://www.stylecracker.com/');
define('web_url2', 'https://www.stylecracker.com/');
/*For PA Images*/
define('PA_FEMALE', web_url.'assets/seventeen/images/pa/female/shapes/');
define('PA_MALE', web_url.'assets/seventeen/images/pa/male/shapes/');

define('PA_FEMALE_STYLE',web_url.'assets/seventeen/images/pa/female/style/');
define('PA_MALE_STYLE', web_url.'assets/seventeen/images/pa/male/style/');

/*For PA Images*/
define('PA_BRAND_LIST',serialize(array('1'=>(object) ['brand_id' => '18136','brand_name' => 'Zara'],'2'=>(object) ['brand_id' => '18139','brand_name' => 'Adidas'],'3'=>(object) ['brand_id' => '18148','brand_name' => 'Converse'],'4'=>(object) ['brand_id' => '20835','brand_name' => 'Zooomberg'],'5'=>(object) ['brand_id' => '28296','brand_name' => 'Color Buckket'],'6'=>(object) ['brand_id' => '28188','brand_name' => 'London Bee  Clothing'],'7'=>(object) ['brand_id' => '35820','brand_name' => 'Tuna London'],'8'=>(object) ['brand_id' => '21841','brand_name' => 'Jaded London'],'9'=>(object) ['brand_id' => '18182','brand_name' => 'Reebok'],'10'=>(object) ['brand_id' => '18258','brand_name' => 'Tresmode'])));

/*Added for SCBOX */

define('SCBOX_PACKAGE',serialize(array(
'0'=>(object) ['id' => '1','name' => '3 items','desc' =>  'Apparel<br/>
				Bag <br/>
				Jewellery','price'=>'2999','productid' => '190481','packname' => 'package1','notice'=>'<p></p><i class="fa fa-inr"  ></i> 2399 (-20%)','type' =>'women','show'=>'1','numofproducts'=>'3'],
'1'=>(object) ['id' => '2','name' => '4 items','desc' =>  'Apparel<br/>
				Bag OR Footwear<br/>
				Beauty Product<br/>
				Jewellery','price'=>'4999','productid' => '190482','packname' => 'package2','notice'=>'<p></p><i class="fa fa-inr"  ></i> 3749 (-25%)','type' =>'women','show'=>'1','numofproducts'=>'4'],
'2'=>(object) ['id' => '3','name' => '5 items','desc' =>  'Apparel<br/>
				Bag <br/>
				Beauty Product <br/>
				Footwear <br/>
				Jewellery <br/>','price'=>'6999','productid' => '190483','packname' => 'package3','notice'=>'<p></p><i class="fa fa-inr"  ></i> 4899 (-30%)','type' =>'women','show'=>'1','numofproducts'=>'5'],
'4'=>(object) ['id' => '1','name' => '3 items','desc' =>  'Apparel<br/>
				Bag <br/>
				Jewellery','price'=>'2999','productid' => '190481','packname' => 'package1','notice'=>'<p></p><i class="fa fa-inr"  ></i> 2399 (-20%)','type' =>'men','show'=>'1','numofproducts'=>'3'],
'5'=>(object) ['id' => '2','name' => '4 items','desc' =>  'Apparel<br/>
				Bag OR Footwear<br/>
				Beauty Product<br/>
				Jewellery','price'=>'4999','productid' => '190482','packname' => 'package2','notice'=>'<p></p><i class="fa fa-inr"  ></i> 3749 (-25%)','type' =>'men','show'=>'1','numofproducts'=>'4'],
'6'=>(object) ['id' => '3','name' => '5 items','desc' =>  'Apparel<br/>
				Bag <br/>
				Beauty Product <br/>
				Footwear <br/>
				Jewellery <br/>','price'=>'6999','productid' => '190483','packname' => 'package3','notice'=>'<p></p><i class="fa fa-inr"  ></i> 4899 (-30%)','type' =>'men','show'=>'1','numofproducts'=>'5'],
'3'=>(object) ['id' => '4','name' => 'Unlimited items','desc' => 'Customized based on the type and number of products that you are looking for.<br><br><br>','price'=>'','productid' => '190484','packname' => 'package4','notice'=>'<p style="padding-bottom:1px;"></p>','type' =>'a','show'=>'1','numofproducts'=>''],
'7'=>(object) ['id' => '7','name' => '4 items rakhi','desc' => 'Sling Bag<br/> Scarf<br/> Tassel<br/> Earrings <br/>Snackible Snack.','price'=>'999','productid' => '192378','packname' => 'package5','notice'=>'','show'=>'0','numofproducts'=>''],
'8'=>(object) ['id' => '8','name' => '6 items rakhi','desc' => 'Necklace<br/>Pouch<br/>Tote Bag<br/>Dupatta<br/>Beauty Product<br/> Snackible Snack.','price'=>'1999','productid' => '192379','packname' => 'package6','notice'=>'','show'=>'0','numofproducts'=>''],

)));


define('SCBOX_ID',"'1','2','3','4','5','6'");
define('SCBOX_PRODUCTID',serialize(array('0'=>'190481','1'=>'190482','2'=>'190483','3'=>'190484','4'=>'192378','5'=>'192379')));
define('SCBOX_PRODUCTID_STR',"'190481','190482','190483','190484','192378','192379'");
define('SCBOX_CUSTOM_ID','190484');
define('SCBOX_BRAND_ID','46352');
define('SCBOX_CUSTOM_OBJID','4');

define('SCBOX_SKINTONE_LIST',serialize(array(	
	'0'=>(object) ['id' => '#f5d6c1','colorid' => '3','name' => 'Pale'],
	'1'=>(object) ['id' => '#ebbca7','colorid' => '4','name' => 'Rosy Pale'],
	'2'=>(object) ['id' => '#e3ac8a','colorid' => '5','name' => 'Light'],
	'3'=>(object) ['id' => '#cf986d','colorid' => '6','name' => 'Wheatish'],
	'4'=>(object) ['id' => '#ab7343','colorid' => '7','name' => 'Tan'],
	'5'=>(object) ['id' => '#825937','colorid' => '8','name' => 'Medium'],
	'6'=>(object) ['id' => '#6b4f31','colorid' => '10','name' => 'Dark'])));	

define('SCBOX_COLOR_LIST',serialize(array(
'0'=>(object) ['id' => '5','name' => 'Pastel','imgpath' =>  web_url.'assets/seventeen/images/qa/pastel.png'],
'1'=>(object) ['id' => '2','name' => 'Neutral','imgpath' =>  web_url.'assets/seventeen/images/qa/neutral.png'],
'2'=>(object) ['id' => '6','name' => 'METALLIC','imgpath' =>  web_url.'assets/seventeen/images/qa/metallic.jpg'],
'3'=>(object) ['id' => '1','name' => 'Earthy','imgpath' =>  web_url.'assets/seventeen/images/qa/earthly.png'],
'4'=>(object) ['id' => '3','name' => 'Bright','imgpath' =>  web_url.'assets/seventeen/images/qa/bright.png'],
'5'=>(object) ['id' => '4','name' => 'Black & White','imgpath' =>  web_url.'assets/seventeen/images/qa/black-white.png'])));

define('SCBOX_PRINT_LIST',serialize(array(
'0'=>(object) ['id' => '9','name' => 'Geometric','imgpath' =>  web_url.'assets/seventeen/images/qa/geometric.png'],
'1'=>(object) ['id' => '8','name' => 'Floral','imgpath' =>  web_url.'assets/seventeen/images/qa/floral.png'],
'2'=>(object) ['id' => '10','name' => 'Abstract','imgpath' =>  web_url.'assets/seventeen/images/qa/abstract.png'],
'3'=>(object) ['id' => '11','name' => 'Animal','imgpath' =>  web_url.'assets/seventeen/images/qa/animal.png'],
'4'=>(object) ['id' => '12','name' => 'No Prints','imgpath' =>  web_url.'assets/seventeen/images/qa/no_prints.jpg']
)));


define('SCBOX_BUDGET_LIST',serialize(array('0'=>(object) ['id' => '1','name' => '2000'],'1'=>(object) ['id' => '2','name' => '8000'],'2'=>(object) ['id' => '3','name' => '15000'])));

define('SCBOX_MENTOP_LIST',"'XS','S','M','L','XL'");
define('SCBOX_MENBOTTOM_LIST',"'24','26','28','30','32','34','36','38','40','42','44','46','48'");
define('SCBOX_MENFOOT_LIST',"'6','6.5','7','7.5','8','8.5','9','9.5','10','10.5','11','11.5','12'");

define('SCBOX_WOMENTOP_LIST',"'XS','S','M','L','XL','XXL'");
define('SCBOX_WOMENBOTTOM_LIST',"'24','26','27','28','30','32','34','36','38','40'");
define('SCBOX_WOMENFOOT_LIST',"'3','3.5','4','4.5','5','5.5','6','6.5','7','7.5','8'");
define('SCBOX_BANDSIZE_LIST',"'30','32','34','36','38','40','42'");
define('SCBOX_CUPSIZE_LIST',"'A','B','C','D','DD','E','F'");
define('SCBOX_SIZE',serialize(array('0'=>(object) ['id' => '2','name' => 'FREE'])));

/* SCBOX */

/*added for bookmyshow ======================================*/
define('bms_card_no','a:5:{i:0;i:111111;i:1;i:222222;i:2;i:333333;i:3;i:444444;i:4;i:555555;}'); 
define('bms_discount',500);
define('bms_amt_limit',1000);
define('bms_popup_title','To get the flat &#8377; '.bms_discount.' Off on purchase of &#8377; '.bms_amt_limit.' Please enter your BookMyShow BIN number (First 6 digits of your BMS card number) to apply.');
define('bms_error_message','Please enter your BookMyShow BIN number to get &#8377; '.bms_discount.' off on purchase of &#8377; '.bms_amt_limit.'.');

/*added for bookmyshow*/

define('ATTRIBUTE_SIZE_ID', '17');
define('CATEGORY_BASE_URL', 'all-products/');



/*added for Intercom*/
define('INTERCOM_APPID', 'oz2nj7x5');
define('INTERCOM_KEY', 'dG9rOjFhZjVlNjY4XzQxNThfNDViMF84OTU5XzZkOTEzNGVjMTZlNzoxOjA=');

//product category url
define('SC_URL', 'https://www.stylecracker.com/');
define('SCNEST_URL', 'https://www.stylecracker.com/');
define('PRODUCT_CAT_URL',SCNEST_URL.'assets/images/products_category/');

//Pa image constant
define('WOMEN_DEFAULT_IMG',SCNEST_URL.'assets/seventeen/images/pa/female/profile-default-female.png');
define('MEN_DEFAULT_IMG',SCNEST_URL.'assets/seventeen/images/pa/male/profile-default-male.png');


//Footer brand list
define('FOOTER_BRAND_LIST','47530,47506,47355,47376,47279,47406,41307,47374,47370,47378,47280,47508,47406');

//Footer category list
define('FOOTER_CATEGORY','52,47,55,49,7,20,65,6,57,295');
//category URL
define('CAT_URL',web_url2.'products/');

//brand URL
define('BRNAD_URL',web_url2.'brands/brand-details/');
define('BRAND_URL',web_url2.'brands/');

//single product url message
define('PRODUCT_URL','product/');

//constant message
define('LOGIN_MESSAGE','Please login first');
define('REVIEW_MESSAGE','Please log in to rate and review a product');

//COLLECTION AND EVENT COVER IMAGE PATH-------------
define('COLLECTION_AND_EVENT_URL',web_url.'sc_admin/assets/event_img/');

//profile img url
define('PROFILE_IMG_URL',SCNEST_URL.'sc_admin/assets/user_profiles/');

//profile img url
define('COVER_IMG_URL',SCNEST_URL.'sc_admin/assets/full_pic/');

// pa brands list
define('PA_BRAND_LIST_MEN',serialize(array(
'1'=>(object) ['brand_name' => 'Reliance Trends','budget_id' => '4706','is_show' => '1','brand_key'=>'1'],
'2'=>(object) ['brand_name' => 'Westside','budget_id' => '4706','is_show' => '1','brand_key'=>'2'],
'3'=>(object) ['brand_name' => 'Maxx','budget_id' => '4706','is_show' => '1','brand_key'=>'3'],
'4'=>(object) ['brand_name' => 'Zara','budget_id' => '4707','is_show' => '1','brand_key'=>'4'],
'5'=>(object) ['brand_name' => 'Jack & Jones','budget_id' => '4707','is_show' => '1','brand_key'=>'5'],
'6'=>(object) ['brand_name' => 'Kenneth Cole','budget_id' => '4708','is_show' => '1','brand_key'=>'6'],
'7'=>(object) ['brand_name' => 'Diesel','budget_id' => '4708','is_show' => '1','brand_key'=>'7'],
'8'=>(object) ['brand_name' => 'Manyavar','budget_id' => '4708','is_show' => '1','brand_key'=>'8'],
'9'=>(object) ['brand_name' => 'Burberry','budget_id' => '4709','is_show' => '1','brand_key'=>'9'],
'10'=>(object) ['brand_name' => 'Rohit Bal','budget_id' => '4709','is_show' => '1','brand_key'=>'10'])));

define('PA_BRAND_LIST_WOMEN',serialize(array(
'1'=>(object) ['brand_name' => 'Pantaloons','budget_id' => '4706','is_show' => '1','brand_key'=>'1'],
'2'=>(object) ['brand_name' => 'Westside','budget_id' => '4706','is_show' => '1','brand_key'=>'2'],
'3'=>(object) ['brand_name' => 'Lifestyle','budget_id' => '4706','is_show' => '1','brand_key'=>'3'],
'4'=>(object) ['brand_name' => 'Zara','budget_id' => '4707','is_show' => '1','brand_key'=>'4'],
'5'=>(object) ['brand_name' => 'Forever21','budget_id' => '4707','is_show' => '1','brand_key'=>'5'],
'6'=>(object) ['brand_name' => 'Global Desi','budget_id' => '4707','is_show' => '1','brand_key'=>'6'],
'7'=>(object) ['brand_name' => 'Anita Dongre','budget_id' => '4708','is_show' => '1','brand_key'=>'7'],
'8'=>(object) ['brand_name' => 'BCBG','budget_id' => '4708','is_show' => '1','brand_key'=>'8'],
'9'=>(object) ['brand_name' => 'Michael Kors','budget_id' => '4708','is_show' => '1','brand_key'=>'9'],
'10'=>(object) ['brand_name' => 'Gucci','budget_id' => '4709','is_show' => '1','brand_key'=>'10'],
'11'=>(object) ['brand_name' => 'Fendi','budget_id' => '4709','is_show' => '1','brand_key'=>'11'],
'12'=>(object) ['brand_name' => 'Dolce & Gabbana','budget_id' => '4709','is_show' => '1','brand_key'=>'12'])));


/*YESBANK AND BMS Offer Codes*/
define('BMSCODE','BMS15');
define('OFFER_CODES',serialize(array('0'=>'BMS500','1'=>'BMS15','2'=>'YESBOX1000','3'=>'YESBOX750','4'=>'YES300','5'=>'YESBOX500','6'=>'YESBOX1000')));
define('SCBOXOFFER_CODES',serialize(array('0'=>'BMS15','1'=>'YESBOX750','2'=>'YESBOX500','3'=>'YESBOX1000')));

//define('PAYUOFFER_KEY','@2437');
define('BMS500_OFFER_KEY','@2437');
define('YES300_OFFER_KEY','@2760');
//Scbox Offer Keys
define('BMS15_OFFER_KEY','@2756');
define('YESBOX500_OFFER_KEY','@2762');
define('YESBOX750_OFFER_KEY','@2784');
define('YESBOX1000_OFFER_KEY','@2786');

//PAYUOFFER KEYS
define('BMS_OFFER_CODE','@2437');

/*YESBANK AND BMS Offer Codes*/

// 2factor otp 
define('API_KEY_2FACTOR','f069430d-3934-11e7-8473-00163ef91450');
define('OTP_FROM','STYCKR');

define('IMAGIFY_BRANDID',serialize(array(
'0'=>'40030',
'1'=>'40282',
'2'=>'47530',
'3'=>'47355',
'4'=>'45967',
'5'=>'25032',
'6'=>'28296',
'7'=>'47508',
'8'=>'23347',
'9'=>'40895',
'10'=>'47506',
'11'=>'27992',
'12'=>'43484',
'13'=>'45670',
'14'=>'44948',
'15'=>'30822',
'16'=>'38634',
'17'=>'28792',
'18'=>'45668',
'19'=>'25454',
'20'=>'39492',
'21'=>'43334',
'22'=>'47400',
'23'=>'41307',
'24'=>'31465',
'25'=>'39495',
'26'=>'45643',
'27'=>'41532',
'28'=>'47378',
'29'=>'40020',
'30'=>'25169',
'31'=>'28545',
'32'=>'47535',
'33'=>'35258',
'34'=>'27949',
'35'=>'36454',
'36'=>'44491',
'37'=>'20936',
'38'=>'41624',
'39'=>'45781',
'40'=>'36415',
'41'=>'45647',
'42'=>'32166',
'43'=>'39646',
'44'=>'47279',
'45'=>'47239',
'46'=>'24156')));

define('FEEDBACK_QUESTIONS',serialize(array(
'1'=>(object) ['name' => 'bop','question' => 'How was your StyleCracker Box Experience from start to finish? ','is_show' => '1'],
'2'=>(object) ['name' => 'scs','question' => 'Stylist communication skills','is_show' => '1'],
'3'=>(object) ['name' => 'sa','question' => 'Stylists ability to understand your likes and dislikes','is_show' => '1'],
'4'=>(object) ['name' => 'time','question' => 'Timeliness of box delivery','is_show' => '1'],
'5'=>(object) ['name' => 'package','question' => 'Packaging of the delivered box','is_show' => '1'],
'6'=>(object) ['name' => 'quality','question' => 'Quality of products','is_show' => '1'],
'7'=>(object) ['name' => 'unique','question' => 'Uniqueness of products','is_show' => '1'],
'8'=>(object) ['name' => 'money','question' => 'Value for money','is_show' => '1'],
'9'=>(object) ['name' => 'communicate','question' => 'Communication with StyleCracker team in case of any queries relating to order status and delivery','is_show' => '1'],
'10'=>(object) ['name' => 'returnp','question' => 'Return process (if applicable)','is_show' => '0'],
'11'=>(object) ['name' => 'refundp','question' => 'Refund process (if applicable)','is_show' => '0'],
'12'=>(object) ['name' => 'recommend','question' => 'Would you recommend the SC Box to your friends and family?','is_show' => '0'],
'13'=>(object) ['name' => 'stylistexperience','question' => 'Did the stylist go out of the way to make your experience a little extra special? If yes, please let us know.','is_show' => '0'],
'14'=>(object) ['name' => 'processexperience','question' => 'Is there anything we could do in the entire process to further enhance your experience?.','is_show' => '0'],
'15'=>(object) ['name' => 'anyfeedback','question' => 'Any other feedback','is_show' => '0'],
'16'=>(object) ['name' => 'orderagain','question' => 'Would you order an SC Box again?','is_show' => '0'],
)));

define('DEFAULT_IMAGE_PATH',"'https://i.imgur.com/Qu4zXlp.jpg'");

define('ONLINE_PAY_DISCOUNT','5');