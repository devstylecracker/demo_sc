<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
class Blog extends TT_REST_Controller {

	function __construct(){

       parent::__construct();
	   $this->load->model('Blog_model');
	   $this->load->library('curl'); 
	   $this->load->model('Pa_model');
   	}
	
	public function get_all_blog_get(){
	
		$data['user_id'] = @$this->get('user_id');
		$data['auth_token'] = @$this->get('auth_token');
		$data['search_by'] = @$this->get('search_by');
		$data['table_search'] = @$this->get('table_search');
		$data['limit'] = @$this->get('limit');
		$data['offset'] = @$this->get('offset');
		$data['limit_cond'] = " LIMIT ".$data['offset'].",".$data['limit'];
		$result = $this->Blog_model->get_blog_data($data);
		$this->success_response($result);
	}
	
	public function add_to_wishlist_post(){
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		$fav_for = $this->post('fav_for');
		$fav_id = $this->post('fav_id');
		$res = $this->Blog_model->user_wishlist($user_id,$fav_for,$fav_id);
		if($res === 1){ 
			$data = new stdClass();
			$data->message = 'success';
			$this->success_response($data);
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}		
	}
}