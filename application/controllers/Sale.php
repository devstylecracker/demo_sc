<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sale extends MY_Controller {

	function __construct(){
       parent::__construct();
       $this->load->model('Sale_model');
       $this->load->model('Allproducts_model');
       $this->load->model('Product_desc_model');
       
   	}

   	function index(){

   		$data = array();

      $this->seo_title = 'Sale - Buy Online Now on StyleCracker';
      $this->seo_desc = '';
      $this->seo_keyword = '';

      $data['brands'] = $this->Sale_model->getSaleBrands(1);
      $data['all_data']['filter']['paid_brands'] = $this->Sale_model->getSaleBrands();
      $data['left_filter'] = $this->load->view('sale/sale_product_filter', $data, true);
      $data['all_data']['products'] = $this->Sale_model->getSaleProducts(0,'','',0);
		if($_GET['test'] == 1){
			echo "<pre>";print_r($data['all_data']['filter']['paid_brands']);exit;
		}
   		$this->load->view('common/header_view');
		  $this->load->view('sale/sale',$data);
		  $this->load->view('common/footer_view');

   	}

    function load_more_sale_products(){
      $group_no = $this->input->post('group_no');
      $category = $this->input->post('category');
      $brands = $this->input->post('brands');
      $product_sort_by_sale = $this->input->post('product_sort_by_sale');
      $data['all_data']['products'] = $this->Sale_model->getSaleProducts($group_no,$category,$brands,$product_sort_by_sale);
      $this->load->view('sale/more_sale',$data);
    }

    function getSaleBrands(){
      $cnt = $this->input->post('cnt');
      $data['brands'] = $this->Sale_model->getSaleBrands($cnt+1);
      $brandhtmlslider = '';
      if(!empty($data['brands'])){
        foreach($data['brands'] as $val){

          if (file_exists('./assets/images/brands/logos/'.$val['brand_id'].'.jpg')) {
            $brandhtmlslider = $brandhtmlslider.'<div class="swiper-slide"><a href="'.base_url().'brands/brand-details/'.$val['user_name'].'" target="_blank"><img alt="'.$val['company_name'].'" src="https://www.stylecracker.com/assets/images/brands/logos/'.$val['brand_id'].'.jpg"></a></div>';
          }
        }
      }
      echo $brandhtmlslider;
    }

    function filtersProducts(){
      $category = $this->input->post('category');
      $brands = $this->input->post('brands');
      $product_sort_by_sale = $this->input->post('product_sort_by_sale');
      
      $data['all_data']['products'] = $this->Sale_model->getSaleProducts(0,$category,$brands,$product_sort_by_sale);
      $this->load->view('sale/more_sale',$data);
    }
}

?>