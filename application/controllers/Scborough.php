<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scborough extends MY_Controller {


	function __construct(){
       parent::__construct();
       $this->load->model('Scborough_model');
       $this->load->model('Pa_model');
       $this->load->library('bitly');
       $this->load->library('email');
       $this->load->model('Brands_model');
       $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));

   	}

	public function index()
	{
    $this->seo_title = 'SCBorough | Stylecracker ';
    $this->seo_desc = 'An Exclusive Online Shoppable Preview of the StyleCracker Borough';
    $this->seo_keyword = '';

    $offset = $this->uri->segment(2)!='' && $this->uri->segment(2)>0 ? $this->uri->segment(2) : 1;
	// added for auto login
	$id = $this->input->get('id');
	$user_id = base64_decode($id);
	$get_user_info = $this->Scborough_model->get_user_info($user_id);
	if(!empty($get_user_info)){
		$res = $this->login($get_user_info[0]['email'],$get_user_info[0]['password']);
	}
	// added for auto login
    $homehtml = '';
    $filter_value = '';
    $pagination = '';
    $adjacents = 3;

    $stylist_offset = 0;
    $street_offset = 0;
    $blog_offset = 0;
    $filter = 0;
    $prev = 0;
    $next = 1;

    $filter = $this->input->get('filter');
    $filter1 = $this->input->get('filter1');
    $filter2 = $this->input->get('filter2');

   if ( $this->cache->get('filter_type') !== $filter )
   {
      $looks = $this->Scborough_model->get_homepage_looks($stylist_offset,$street_offset,$blog_offset,5, $filter,$filter1,$filter2);     
      $filter_type1 = $filter;
      $this->cache->save('scboroughLooks', $looks, 500000000000);
      $this->cache->save('filter_type', $filter_type1, 500000000000);
   }

    if($this->cache->get('scboroughLooks'))
    {
        $looks = $this->cache->get('scboroughLooks');
    }
 
      if(!empty($looks['product'])){

        if(isset($looks['product'])){

            if(!empty($looks['product'])){
            foreach ($looks['product'] as $val) {

            $new_price = $val['price']-$val['discount_price'];
            $strikeClass = 'text-strike';
            $MRP = $val['price'];
            $price_text = '';
            $inventory_text = '';

            $homehtml = $homehtml.' <div class="col-md-3 col-sm-3 col-xs-6 scb-item grid-item" brand="'.$val['company_name'].'" data-price="'.$MRP.'" category="'.$val['product_sub_cat_id'].'">
                <div class="grid-sizer"></div>
                <div class="item item-look-product111 item-product">';
            $user_link = 0;
            if($this->session->userdata('user_id'))
              {
               $user_link = $this->session->userdata('user_id')!='' ? $this->session->userdata('user_id') : 0 ;
              }


            if($val['is_paid']!=1 || empty($looks['products_inventory'][$val['id']]['size_text']))
            {
              $sold_text = '<div class="btn-out-of-stock">Sold Out</div>';
            }else
            {
              $sold_text = '';
            }



            if($val['discount_percent']!='')
            {
             $price_text =' <span class="mrp-price" style="font-size:12px;"> <i class="fa fa-inr"></i><span class="text-strike"> '. $MRP.'</span></span>
             <span class="price"><i class="fa fa-inr"></i> '.round($new_price).'</span>
            <span class="discount-price">('.$val['discount_percent'].'% OFF)</span>';
            }
            else {
              $price_text = '<span class="price"><i class="fa fa-inr"></i> '. $MRP.'</span>';
            }

            if($val['is_paid']==1)
            {
               $data_content = "<div class='popup-pincode'><span class='picode-label'>Enter pincode</span><input type='text' name='avil_pincode_".$val['id']."'  id='avil_pincode_".$val['id']."'  class='form-control input-sm' maxlength='6' /><button class='btn btn-primary btn-xs'>Check</button><div class='message'></div></div>";
               $shipping_text='<div class="col-md-5">
                  <div class="shipping-availibility">
                    <span class="sc-popover-btn" title="" data-toggle="sc-popover" data-content="'.$data_content.'" ><i class="ic sc-ship" title="Check Availability"></i></span>
                  </div>
                    </div>';
            }else
            {
               $shipping_text='';
            }

            if($val['is_paid']==1)
            {
              $inventory_text = '<div class="product-sizes-wrp">
              <div class="product-sizes-list">
              <ul>';
              $cnt = '';

              if(!empty($looks['products_inventory'][$val['id']]['size_text'])) {
                foreach($looks['products_inventory'][$val['id']]['size_text'] as $key=>$value){
                  if(count($looks['products_inventory'][$val['id']]['size_text']) == 1) { $cnt =  'selected'; $chk ='checked="true"'; }else { $cnt = ''; $chk =''; }
                 $inventory_text = $inventory_text.' <li><label class="'.$cnt.'"><input type="radio" name="size_'.$val['id'].'" value="'.$key.'" '.$chk.' class="product-avail-size">'.$value.'</label></li>';
              } }
              $inventory_text = $inventory_text.'</ul>
              <div class="product-sizes-error hide" id="productSizeSelect'.$val['id'].'"> Select size </div>';
              if($val['size_guide'] != 1){
              $inventory_text = $inventory_text.'<div class="product-size-chart" data-toggle="modal" data-target="#mysize'.$val['size_guide'].'" size-guide-id="'.$val['size_guide'].'">
                Size Guide <i class="ic sc-size-chart"></i>
              </div>';
                    }
              $inventory_text = $inventory_text.'</div>
              </div>';
            }

            $user_link = 0;
            if($this->session->userdata('user_id')){ $user_link = $this->session->userdata('user_id')!='' ? $this->session->userdata('user_id') : 0 ; }

             if($val['is_paid']!=1 || empty($looks['products_inventory'][$val['id']]['size_text']))
             {

                $buynow_text = '<div class="col-md-4">
                    <a href="javascript:void(0);" class="btn btn-secondary1 btn-block only-cart btn-add-to-cart disabled">
                    <i class="ic-add-to-cart"></i> Cart</a>
                    </div>
                    <div class="col-md-8">
                      <a class="btn btn-primary btn-block " href="javascript:void(0);" onclick="notify('.base64_encode($val['id']).');">Notify Me</a>
                    </div>';

              }else{

                $buynow_text = '<div class="col-md-4">
                    <a href="javascript:void(0);" id="add_cart'.$val['id'].'" data-attr="'.$val['id'].'" class="btn btn-secondary1 btn-block btn-add-to-cart" look-attr="'.$val['id'].'" onclick="scborough_addtocart(this);">
                    <i class="ic-add-to-cart"></i> Cart</a>
                    </div>
                    <div class="col-md-8" style="padding: 0px;">
                      <a class="btn btn-primary btn-block" href="javascript:void(0);" id="buy_now'.$val['id'].'" data-attr="'.$val['id'].'" look-attr="'.$val['id'].'" onclick="scborough_buynow(this);">Buy Now</a>
                    </div>';
             }

             $homehtml = $homehtml.'<div class="item-img">'.$sold_text.'
                    <div class="item-img-inner">
                      <img class="btn-img-quick-view" data-toggle="modal" onclick="product_quick_view('.$val['id'].')" src="'.$this->config->item('product_image_thumb').@$val['image'].'" alt="'.$val['name'].'- StyleCracker" title="'.$val['name'].' - StyleCracker"/>
                    </div>
                    <div class="item-look-hover">
                        <a title="'.$val['name'].'" href="'.base_url().'product/details/'.$val['slug'].'-'.$val['id'].'"><span class="btn btn-primary">Get this</span>
                        </a>
                    </div>
                    </div>                    
                    <div class="item-desc">
                      <div class="item-title" title="'.stripslashes($val['name']).'">
                        '.stripslashes($val['name']).'
                      </div>
                      <div class="item-brand" title="Brand">
                        <span class="item-brand-label">by</span> <span>'.$val['company_name'].'</span>
                      </div>
                      <div class="item-price-wrp">
                        <div class="row">
                          <div class="col-md-7">
                        <div class="item-price">
                        '.$price_text.'
                      </div>
                        </div>
                        </div>
                      </div>
                    </div>

            </div></div>';
            } }
        }

  }


    $homehtml = $homehtml.'<input type="hidden" name="blog_count" id="blog_count" value="'.$this->get_product_count().'">';

    /*if( isset($looks['offset']['product']) && ($looks['offset']['product'] * $this->config->item('product_look')) < $this->get_product_count()){
      $homehtml = $homehtml.'<input type="hidden" name="blog_offset" id="blog_offset" value="'.$looks['offset']['product'].'">';
    }

        $blog_count =  $this->get_product_count();

        $blog_count_ = ($blog_count/12);

        $perpage =12;
        $total =  $blog_count;
        $lastpage = ceil($total/$perpage);
        $lpm1 = $lastpage - 1;

      if($lastpage > 1)
        {
          $pagination .= "<div class='sc-pagination-wrp'><ul class=\"pagination sc-pagination\">";
          //previous button

          if ($offset != 1)
            $pagination.= "<li><a href=".base_url()."page/".'1'.">First</a></li>";
          //else
            //$pagination.= "<li><span class=\"disabled\">First</span></li>";

          if ($offset > 1)
            $pagination.= "<li><a href=".base_url()."page/".$prev.">previous</a></li>";
          //else
            //$pagination.= "<li><span class=\"disabled\">previous</span></li>";


          $limit = $offset + 5;
          if($offset != $lastpage && $offset< $lastpage-5  ){
          for ($counter =  $offset; $counter <= $limit; $counter++)
            {
              if ($counter == $offset)
                $pagination.= "<li><span class=\"current\">$counter</span></li>";
              else
                $pagination.= "<li><a href=".base_url()."page/".$counter.">$counter</a></li>";
            }
          }else{
            for ($counter = $lastpage-5; $counter <= $lastpage; $counter++)
            {
              if ($counter == $offset)
                $pagination.= "<li><span class=\"current\">$counter</span></li>";
              else
                $pagination.= "<li><a href=".base_url()."page/".$counter.">$counter</a></li>";
            }

          }

          //next button
          if ($offset < $counter - 1)
            $pagination.= "<li><a href=".base_url()."page/".$next.">next </a></li>";

          if ($offset != $lastpage)
            $pagination.= "<li><a href=".base_url()."page/".$lastpage.">Last </a></li>";

          $pagination.= "</ul></div>\n";
        }

       $data['pagination'] =  $pagination;*/      
       $data['schomeview'] =  $homehtml;
      
	$this->load->view('common/header_view');
    $this->load->view('common/scb_slider_view');
	$this->load->view('scb_view',$data);
	$this->load->view('common/footer_view');
    /*$this->load->view('common/header_view_schome');
    $this->load->view('common/scb_slider_view');
    $this->load->view('scb_view',$data);
    $this->load->view('common/footer_view_schome');*/
	}

	public function get_looks(){
		//if ($this->input->is_ajax_request()) {
		$homehtml = '';
		$filter_value = '';

		$filter = $this->input->get('filter');
    $filter1 = $this->input->get('filter1');
    $filter2 = $this->input->get('filter2');
    $stylist_offset =0;
    $street_offset = 0;
    $blog_offset = 0;

		$looks = $this->Scborough_model->get_homepage_looks($stylist_offset,$street_offset,$blog_offset,5, $filter,$filter1,$filter2);

    $target_array['stylist_look'][] = @$looks['stylist_look'];

  	if(!empty($looks['product'])){

           if(isset($looks['product'])){

          if(!empty($looks['product'])){
              foreach ($looks['product'] as $val) {

              $new_price = $val['price']-$val['discount_price'];
              $strikeClass = 'text-strike';
              $MRP = $val['price'];
              $price_text = '';
              $inventory_text = '';

              $homehtml = $homehtml.' <div class="col-md-3 col-sm-6 col-xs-6 scb-item grid-item" brand="'.$val['company_name'].'" price="'.$MRP.'" category="" >
                  <div class="grid-sizer"></div>
                  <div class="item item-look-product111 item-product">';
              $user_link = 0;
              if($this->session->userdata('user_id'))
              {
               $user_link = $this->session->userdata('user_id')!='' ? $this->session->userdata('user_id') : 0 ;
              }

              if($val['is_paid']!=1 || empty($looks['products_inventory'][$val['id']]['size_text']))
              {
                $sold_text = '<div class="btn-out-of-stock">Sold Out</div>';
              }else
              {
                $sold_text = '';
              }

              if($val['discount_percent']!='')
              {
                 $price_text =' <span class="mrp-price" style="font-size:12px;"> <i class="fa fa-inr"></i><span class="text-strike"> '. $MRP.'</span></span>
                 <span class="price"><i class="fa fa-inr"></i> '.round($new_price).'</span>
                <span class="discount-price">('.$val['discount_percent'].'% OFF)</span>';
              }
              else
              {
                $price_text = '<span class="price"><i class="fa fa-inr"></i> '. $MRP.'</span>';
              }

              if($val['is_paid']==1)
              {
                $data_content = "<div class='popup-pincode'><span class='picode-label'>Enter pincode</span><input type='text' name='avil_pincode_".$val['id']."'  id='avil_pincode_".$val['id']."'  class='form-control input-sm' maxlength='6' /><button class='btn btn-primary btn-xs'>Check</button><div class='message'></div></div>";
                 $shipping_text='<div class="col-md-5">
                    <div class="shipping-availibility">
                      <span class="sc-popover-btn" title="" data-toggle="sc-popover" data-content="'.$data_content.'" ><i class="ic sc-ship" title="Check Availability"></i></span>
                    </div>
                      </div>';

              }else
              {
                 $shipping_text='';
              }

              if($val['is_paid']==1)
              {
                $inventory_text = '<div class="product-sizes-wrp">
                <div class="product-sizes-list">
                <ul>';
                if(!empty($looks['products_inventory'][$val['id']]['size_text']))
                {
                  foreach($looks['products_inventory'][$val['id']]['size_text'] as $key=>$value)
                  {

                   $inventory_text = $inventory_text.' <li><label class=""><input type="radio" name="size_'.$val['id'].'" value="'.$key.'" class="product-avail-size">'.$value.'</label></li>';
                  }
                }
                $inventory_text = $inventory_text.'</ul>
                <div class="product-sizes-error hide" id="productSizeSelect'.$val['id'].'"> Select size </div>';
                if($val['size_guide'] != 1)
                {
                  $inventory_text = $inventory_text.'<div class="product-size-chart" data-toggle="modal" data-target="#mysize'.$val['size_guide'].'" size-guide-id="'.$val['size_guide'].'">
                    Size Guide <i class="ic sc-size-chart"></i>
                  </div>';
                }
                $inventory_text = $inventory_text.'</div>
                </div>';
              }

              $user_link = 0;
              if($this->session->userdata('user_id')){ $user_link = $this->session->userdata('user_id')!='' ? $this->session->userdata('user_id') : 0 ; }

               if($val['is_paid']!=1 || empty($looks['products_inventory'][$val['id']]['size_text']))
               {
                  $buynow_text = '<div class="col-md-3">
                      <a href="javascript:void(0);" class="btn btn-secondary1 btn-block only-cart btn-add-to-cart disabled">
                      <i class="ic-add-to-cart"></i> Cart</a>
                      </div>
                      <div class="col-md-9">
                        <a class="btn btn-primary btn-block " href="javascript:void(0);" onclick="notify('.base64_encode($val['id']).');">Notify Me</a>
                      </div>';
                }else{
                  $buynow_text = '<div class="col-md-3">
                      <a href="javascript:void(0);" id="add_cart'.$val['id'].'" data-attr="'.$val['id'].'" class="btn btn-secondary1 btn-block btn-add-to-cart" look-attr="'.$val['id'].'" onclick="scborough_addtocart(this);">
                      <i class="ic-add-to-cart"></i> Cart</a>
                      </div>
                      <div class="col-md-9" style="padding: 0px;">
                        <a class="btn btn-primary btn-block" href="javascript:void(0);" id="buy_now'.$val['id'].'" data-attr="'.$val['id'].'" look-attr="'.$val['id'].'" onclick="scborough_buynow(this);">Buy Now</a>
                      </div>';
               }

               $homehtml = $homehtml.'<div class="item-img">'.$sold_text.'
                      <div class="item-img-inner">
                        <img class="btn-img-quick-view" data-toggle="modal" onclick="product_quick_view('.$val['id'].')" src="'.$this->config->item('product_image_thumb').@$val['image'].'" alt="'.$val['name'].'- StyleCracker" title="'.$val['name'].' - StyleCracker"/>
                      </div></div>
                      <div class="item-desc">
                        <div class="item-title" title="'.stripslashes($val['name']).'">
                          '.stripslashes($val['name']).'
                        </div>
                        <div class="item-brand" title="Brand">
                          <span class="item-brand-label">by</span> '.$val['company_name'].'
                        </div>
                        <div class="item-price-wrp">
                          <div class="row">
                            <div class="col-md-7">
                          <div class="item-price">
                          '.$price_text.'
                        </div>
                          </div>

                          </div>
                        </div>
                      </div>

              </div></div>';
            }
          }
  	     }
       }

    $homehtml = $homehtml.'<input type="hidden" name="blog_count" id="blog_count" value="'.$this->get_product_count().'">';

	  if( isset($looks['offset']['product']) && ($looks['offset']['product'] * $this->config->item('product_look')) < $this->get_product_count()){
    	$homehtml = $homehtml.'<input type="hidden" name="blog_offset" id="blog_offset" value="'.$looks['offset']['product'].'">';
	  }

		echo $homehtml;

	}


  function get_product_count(){
    $stylist_look = $this->Scborough_model->get_product_count();
    return count($stylist_look);
  }

  public function redirect(){
    $this->load->view('redirecttoapp');
  }

  public function showList()
  {
    $filter = $this->input->post('filter');
    $filter1 = $this->input->post('filter1');
    $filter2 = $this->input->post('filter2');
    $option_html = '<select name="select_type1" id="select_type1" class="selectpicker1" onchange="showFilterResult();">
            <option value="0">All</option>';

    if($filter=='brand')
    {
      $list_data = $this->Scborough_model->get_all_brands();
      if(!empty($list_data))
      {
        foreach($list_data as $val)
        {
          $option_html = $option_html.'<option value="'.$val->user_id.'">'.$val->company_name.'</option>';
        }

      }
    }else if($filter=='category')
    {
      $list_data = $this->Scborough_model->category_list();
      //echo '<pre>';print_r($list_data);
      if(!empty($list_data))
      {
        foreach($list_data as $val)
        {
          $option_html = $option_html.'<option value="'.$val->id.'">'.$val->name.'</option>';
        }
      }
    }
    $option_html = $option_html.'</select>';
    echo $option_html;
  }

  public function login($logEmailid,$logPassword){

         if(trim($logEmailid) == ''){
            echo "Please enter your email id";

          }else if(trim($logPassword) == ''){
            echo "Please enter the Password";

          }else{
           $result = $this->Scborough_model->login_user($logEmailid,$logPassword);


          }
  }

}
