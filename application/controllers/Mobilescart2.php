<?php                                                                                                                                                                                                                                                ?><?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';

class Mobilescart2 extends TT_REST_Controller 
{
  function __construct()
	{
		parent::__construct();
		$this->load->model('Schome_model');
		$this->load->model('Mobile_model');
		$this->load->model('Cart_model');
		$this->load->model('Mcart_model2');
		$this->load->library('user_agent');
		$this->open_methods = array('scgenrate_hashes_post'); 
	}
	
	
	public function update_cart_post(){
		
		//$uniquecookie = $this->post('SCUniqueID');
		$productid = $this->post('productid');
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$productsize = $this->post('productsize');
		$paymod = $this->post('paymod');
		$type = $this->post('type');
		$medium = $this->post('medium');
		if($type == ''){
			$type	= '5';
		}
		if($paymod == ''){
			$paymod	= 'online';
		}
		if ($this->agent->is_browser())
		{
			$agent = 'Computer '.$this->agent->browser().' '.$this->agent->version();
		}
		elseif ($this->agent->is_robot())
		{
			$agent = 'Robot '.$this->agent->robot();
		}
		elseif ($this->agent->is_mobile())
		{
			$agent = 'Mobile '.$this->agent->mobile();
		}
		else
		{
			$agent = 'Unidentified';
		} 
		$product_click = $this->Mcart_model2->add_product_click($userid,$productid,$type);
		$data = (object)$this->Mcart_model2->update_cart($productid,$userid,$productsize,$paymod,$agent,$medium);
	
		if($data !=NULL){
	      	 $this->success_response($data);
		    }else{
	      	   $this->success_response($data);
		       //$this->failed_response(1001, "No Record(s) Found");
		}
	}
	
	public function deleteCartProduct_post(){
			
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$productid = $this->input->post('product_id');
		$cart_id = $this->input->post('cart_id');
		$paymod = $this->post('paymod');
		if($paymod == ''){
			$paymod	= 'online';
		}
		$pincode = $this->post('pincode');
		if($this->checkpincode($pincode) != 2 && $pincode != ''){
        $this->failed_response(1010, "Pincode does not exist");
		}
		$data = (object)$this->Mcart_model2->deleteCartProduct($productid,$userid,$cart_id,$paymod,$pincode);
		
		if($data !=NULL){
		 $this->success_response($data);
		}else{
			$res = array();
		   $this->success_response($data);
		   //$this->failed_response(1001, "No Record(s) Found");
		}
	}
	
	public function getBrandUsername_get(){
		$brand_id = $this->get('brand_id');
		$data = $this->Mcart_model2->getBrandUsername($brand_id);
		
		if($data !=NULL){
		 $this->success_response($data);
		}else{
			$res = array();
		   $this->success_response($data);
		   //$this->failed_response(1001, "No Record(s) Found");
		}
	}
	
	public function productQtyUpdate_post(){
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$product_id = $this->input->post('product_id');
		$productQty = $this->input->post('productQty');
		$cart_id = $this->input->post('cart_id');
		$paymod = $this->post('paymod');
		$pincode = $this->post('pincode');
		if($this->checkpincode($pincode) != 2 && $pincode != ''){
        $this->failed_response(1010, "Pincode does not exist");
		}
                if($productQty>5){
                $this->failed_response(1001, "product quantity should be less than 5");
                 }
		if($paymod == ''){
			$paymod	= 'online';
		}
		//$productInfo = base64_decode(str_replace("product_cart_info_","",$this->input->post('productInfo')));
		if($productQty>0 && $productQty<=5){
			$data = (object)$this->Mcart_model2->productQtyUpdate($product_id,$productQty,$userid,$cart_id,$paymod,$pincode);
		
		
		if($data !=NULL){
		 $this->success_response($data);
		}else{
	           $res = array();
		   $this->success_response($res);
		   //$this->failed_response(1001, "No Record(s) Found");
		}
                }
	}
	
	public function get_cart_post(){
		
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$paymod = $this->post('paymod');
		if($paymod == ''){
			$paymod	= 'online';
		}
		$pincode = $this->post('pincode');
		if($this->checkpincode($pincode) != 2 && $pincode != ''){
        $this->failed_response(1010, "Pincode does not exist");
		}
		$data = (object)$this->Mcart_model2->get_cart($userid,$paymod,$pincode);
		if($data !=NULL && !empty($data)){
		 $this->success_response($data);
		}else{
			$res = new stdclass();
			$res->item = '0';
			$this->success_response($res);
		}
	}
		
	public function sc_uc_order_post(){
		
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$address_id = $this->input->post('address_id');
		$user_info = $this->Mcart_model2->user_info($address_id);
		$paymod = $this->input->post('paymod');
		//$SCUniqueID = $this->input->post('SCUniqueID');
		$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		if($paymod == 'cod'){
			$paymod_v = 1;
		}else {
			$paymod_v = 2;
		}
		$data = array();
		
		$data['user_id'] = $userid;
		$data['uc_first_name'] = $user_info['first_name'];
		$data['uc_last_name'] = $user_info['last_name'];
		$data['uc_email_id'] = $user_info['email'];
		$data['uc_mobile'] = $user_info['mobile_no'];
		$data['uc_address'] = $user_info['shipping_address'];
		$data['uc_pincode'] = $user_info['pincode'];
		$data['uc_city'] = $user_info['city_name'];
		$data['uc_state'] = $user_info['state_name'];
		$data['paymod'] = $paymod;
		$data['mihpayid'] = '';
		$data['txnid'] = '';
		
		if($paymod == 'cod' or $paymod == 'online'){
		  $res = $this->Mcart_model2->sc_uc_order2($data); 
		  if($res !=NULL && $res != '2'){
		  //print_r($res);exit;
			$order_no = $res->order_no; 
			//echo $order_no;exit;
			if($paymod == 'cod'){
		  // $this->orderPlaceMessage($userid,$order_no,$data['uc_first_name'],$data['uc_last_name']);
		  // $this->orderPlaceMessagetoBrand($userid,$order_no,$data['uc_first_name'],$data['uc_last_name']);
			}
		  return $this->success_response($res);
		}else{
		   return $this->failed_response(1201, "Your cart is empty");
		}
		 
		}

	}
	
	function orderPlaceMessage($user_id,$order_id,$first_name,$last_last){
    if($this->getEmailAddress($user_id)!=''){
      $config['protocol'] = 'smtp';

//      $config['mailpath'] = '/usr/sbin/sendmail';
      $config['charset'] = 'iso-8859-1';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');

      $this->email->initialize($config);
      $res = $this->Cart_model->su_get_order_info($order_id);
      $brand_price = $this->Cart_model->su_get_order_price_info_mobile($order_id);
   //print_r($brand_price);exit; 
      $productInfohtml = '';
      $pay_mode = '';
                $i = 0;
                $productInfohtml = '';

                  if(!empty($res)){
                  foreach($res as $val){
                    $i++;

                   $productInfohtml =$productInfohtml.' <tr>
                      <td><img src="'. $this->config->item('sc_promotional_look_image').$val['image'].'" width="50" style="border:1px solid #cccccc; padding:2px; width:auto; height:auto; max-width:50px; max-height:50px;"></td>
                      <td>'. $val['name'].' </td>
                      <td>'. $val['product_qty'].' </td>
                      <td>'. $val['size_text'].'</td>
                      <td>'. $val['company_name'].' </td>
                      <td style="text-align:right; width:60px;">&#8377; '. number_format((float)$val['product_price'], 2, '.', '').'</td></tr>';

                  }
                }
                  $cod_amount = $brand_price[0]['cod_amount']>0 ? $brand_price[0]['cod_amount'] : 0 ;
                $shipping_amount = $brand_price[0]['shipping_amount']>0 ? $brand_price[0]['shipping_amount'] : 0 ;
                 
                 if($brand_price[0]['order_tax_amount'] > 0) {
                   $productInfohtml =$productInfohtml.' <tr>
                   <td colspan="5" style="padding:2px 8px; text-align:right;">Tax :</td>
                    <td style="padding:2px 8px; text-align:right;">
                      &#8377;  '.number_format((float)$brand_price[0]['order_tax_amount'], 2, '.', '').'
                    </td>
                  </tr>';
                  }
                  if($cod_amount > 0) {
                  $productInfohtml =$productInfohtml.'<tr>

                    <td colspan="5" style="padding:2px 8px; text-align:right;">COD Charges:</td>
                    
                    <td style="padding:2px 8px; text-align:right;">
                      &#8377;  '.$cod_amount.'
                    </td>
                  </tr>';
                }
                if($shipping_amount > 0){
                  $productInfohtml =$productInfohtml.'<tr>

                    <td colspan="5" style="padding:2px 8px; text-align:right;">Shipping Charges:
                    </td>
                    <td style="padding:2px 8px; text-align:right;">
                      &#8377;  '.number_format((float)$shipping_amount, 2, '.', '').'
                    </td>
                  </tr>';
                }
                if($brand_price[0]['pay_mode'] == 1){
                $pay_mode = '<p style="margin-bottom:20px;">
                  Payment Mode : <img src="https://www.stylecracker.com/assets/images/icons/ic-cod-1.png" style="vertical-align:middle;"> Cash on Delivery
                </p>';
                }

                if($brand_price[0]['pay_mode'] == 2){
                $pay_mode = '<p style="margin-bottom:20px;">
                  Payment Mode : <img src="https://www.stylecracker.com/assets/images/icons/ic-online-payment-1.png" style="vertical-align:middle;"> Online Payment
                </p>';
                }
                /* if($brand_price[0]['order_total'] > 0) {
                  $productInfohtml =$productInfohtml.'<tr>
                    <td>
                      <strong>Order Total :<strong> </td>
                      <td>
                      &#8377;  '.$brand_price[0]['order_total'].'
                      </td>
                    </tr>
                  ';
              }*/
$productInfohtml =$productInfohtml.'</tbody>
      </table>
      <div style="text-align:right; background:#f3f3f3; padding:5px 30px; font-size:18px; text-transform:uppercase; margin-top:20px; border-bottom:2px solid #cccccc;">
        Total Amount  : &#8377; '.number_format((float)$brand_price[0]['order_total'], 2, '.', '').'
      </div>
    </td>
  </tr>';
           $productInfohtml =$productInfohtml.' <tr>
    <td style="padding:0px 30px;">
        <div style=" border-bottom:1px solid #ccc; padding-bottom:30px;">
        Thank you for shopping with <strong>StyleCracker</strong>.<br/>
For any query or assistance, feel free to <a href="https://www.stylecracker.com/schome/aboutus" style="color:#00b19c; text-decoration:none;">Contact Us</a>
      </div>
    </td>
  </tr>
  <tr>
    <td style="vertical-align:middle; padding:20px 30px;">
      <span style="color: #888; font-weight: bold; line-height: 1.3; position: relative;top: -4px; vertical-align:top;">  Follow Us:  </span>

      <a target="_blank" href="https://www.facebook.com/StyleCracker" target="_blank"  title="Facebook"><img src="https://www.stylecracker.com/assets/images/social-media/facebook.png" alt="Facebook" style="width:24px;"></a>
      <a target="_blank" href="https://twitter.com/Style_Cracker" target="_blank"  title="Twitter"><img src="https://www.stylecracker.com/assets/images/social-media/twitter.png" alt="Twitter" style="width:24px;"></a>
      <a target="_blank" href="https://www.youtube.com/user/StyleCrackerTV" target="_blank"  title="Youtube"><img src="https://www.stylecracker.com/assets/images/social-media/youtube.png"  alt="Youtube" style="width:24px;"></a>
      <a href="https://instagram.com/stylecracker" target="_blank" title="Instagram"><img src="https://www.stylecracker.com/assets/images/social-media/instagram.png"  alt="Instagram" style="width:24px;"></a>
      <a target="_blank" href="https://www.pinterest.com/stylecracker" target="_blank"  title="Pintrest"><img src="https://www.stylecracker.com/assets/images/social-media/pintrest.png" alt="Pintrest" style="width:24px;"></a>

          </td>
        </tr>
      </table>


          <table width="600" cellpadding="0" cellspacing="0" align="center" border="0" style="width:600px; background:#fff; border:1px solid #cccccc; margin-bottom:20px;">
        <tr>
          <td align="left" style="padding:20px 0 0 20px; width:160px;">
              <img src="https://www.stylecracker.com/assets/images/logo.png" style="width:150px; margin-bottom:5px;">
          <div>
            <a href="https://www.stylecracker.com" style="color:#00b19c; text-decoration:none;">www.stylecracker.com</a>
          </div>
          </td>
            <td align="left"  style="padding:20px 0 0; vertical-align:bottom;">
              <img src="https://www.stylecracker.com/assets/images/icons/ic-download-app-1.png">
            <td>

            </td>
          <td align="right"  style="padding:20px 20px  0 0; text-align:center; ">
            <div style="font-size:18px; margin-bottom:10px;">
              StyleCracker App is India\'s only
personalised styling platform.
            </div>

             <a href="https://play.google.com/store/apps/details?id=com.stylecracker.android" target="_blank" style="color:#333; text-decoration:none;">
                  <img src="https://www.stylecracker.com/assets/images/icons/en_generic_rgb_wo_45.png" style="height:30px;" target="_blank"></a>

            <a href="https://itunes.apple.com/us/app/stylecracker/id1061463812?mt=8&uo=4" target="_blank" style="color:#333; text-decoration:none; ">
              <img src="https://www.stylecracker.com/assets/images/icons/apple_store.png" style="height:30px;" target="_blank"></a>

               
          </td>
        </tr>
      </table>

    </body>
    </html>';
$this->email->from($this->config->item('from_email'), 'StyleCracker: Order Processing');
      $this->email->to($this->getEmailAddress($user_id));
      $this->email->cc('dev@stylecracker.com');
      $this->email->subject('StyleCracker: Order Processing');

      $this->email->message('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Order Summary</title>

</head>
<body style="background:#f3f3f3; color:#333333; text-align:left; font-family:Arial,Helvetica,sans-serif;
font-weight: normal;
line-height: 1.5;
font-size: 14px; padding:0; margin:0 auto;">
<table width="600" cellpadding="0" cellspacing="0" align="center" border="0" style="width:600px; background:#fff; margin-bottom:20px;">
  <tr>
          <td style="text-align:left; border-bottom:3px solid #00b19c; padding:20px;">

            <table cellpadding="0" cellspacing="0" align="center" border="0" style="width:100%; ">
              <tr>
                      <td style="text-align:left;">

            <img src="https://www.stylecracker.com/assets/images/logo.png" style="height:34px;">
  </td>
  <td style="text-align:right; vertical-align:middle; font-size:14px; color:#00b19c;">

  <a href="https://www.stylecracker.com/myOrders" style="color:#00b19c; text-decoration:none;">My Orders</a> 


          </td>
        </tr>
      </table>
      </td>
    </tr>

  <tr><td style="padding:30px;">

      <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
        <tr>
          <td align="center" style="padding:15px 0 30px; vertical-align:middle;">
             <span style="padding:8px 0; border-bottom:1px solid #cccccc; font-size:18px; text-transform:uppercase;">
               <img src="https://www.stylecracker.com/assets/images/icons/ic-order-processing-1.png" style="vertical-align:middle;"> Order Processing</span>
          </td>
        </tr>
        <tr>
          <td style="font-size:15px; line-height:1.7; ">
            <p style="margin-bottom:20px;">
            Hi <strong>'.$first_name.' '.$last_last.'</strong>,<br/>
            Thank you for your order! Your Order No <strong style="color:#00B19C">'.$order_id.'</strong> is being processed. <br>
            Please find below, the summary of your order.
            </p>
            '.$pay_mode.'
            <div style="background:#f3f3f3; border:1px solid #cccccc; padding:20px 20px 15px; display:inline-block;">
                <div style="text-transform:uppercase; font-weight:bold; line-height:1;">
                  Shipping Address
                </div>
                <div>
                  '. $brand_price[0]['shipping_address'].'<br>
                    '.  $brand_price[0]['state_name'].' ,'.  $brand_price[0]['city_name'].'  - '.  $brand_price[0]['pincode'].' 
                </div>

            </div>
          </td>
        </tr>
      </table>


      <br/>
      <br/>
      <div style="text-transform:uppercase; font-weight:bold; line-height:1; font-size:16px; margin-bottom:20px;">
        Order Details:
      </div>

      <table width="100%" cellpadding="8" cellspacing="0" align="center" border="0" style="font-size:12px;">
        <tbody>
          <tr>
            <th style="background-color:#f3f3f3; font-size:14px;">Image</th>
            <th style="background-color:#f3f3f3; font-size:14px;">Product Details</th>
            <th style="background-color:#f3f3f3; font-size:14px;">Qty</th>
            <th style="background-color:#f3f3f3; font-size:14px;">Size</th>
            <th style="background-color:#f3f3f3; font-size:14px;">Seller</th>
            <th style="background-color:#f3f3f3; font-size:14px; width:60px; text-align:right;">Price</th>
          </tr>'.$productInfohtml);


      $this->email->send();
    }
  }

     function orderPlaceMessagetoBrand($user_id,$orders_id,$first_name,$last_last){
    if($this->getEmailAddress($user_id)!=''){

      $order_id = explode('_', $orders_id);
      unset($order_id[0]);
      if(!empty($order_id)){
      foreach($order_id as $order_noval){
      $config['protocol'] = 'smtp';

//      $config['mailpath'] = '/usr/sbin/sendmail';
      $config['charset'] = 'iso-8859-1';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');

      $this->email->initialize($config);
      $res = $this->Cart_model->br_get_order_info($order_noval);
      $brand_price = $this->Cart_model->br_get_order_price_info_mobile($order_noval);
	  //print_r($brand_price);exit;
      $companyName = $res[0]['company_name'];

      $productInfohtml = '';

                $i = 0;
             $productInfohtml = '';

                  if(!empty($res)){
                  foreach($res as $val){
                    $i++;

                   $productInfohtml =$productInfohtml.' <tr>
                      <td><img src="'. $this->config->item('sc_promotional_look_image').$val['image'].'" width="50" style="border:1px solid #cccccc; padding:2px; width:auto; height:auto; max-width:50px; max-height:50px;"></td>
                      <td>'. $val['name'].' </td>
                      <td>'. $val['product_qty'].' </td>
                      <td>'. $val['size_text'].'</td>
                      <td>'. $val['company_name'].' </td>
                      <td style="text-align:right; width:60px;">&#8377; '. $val['product_price'].'</td></tr>';

                  }
                }
                  $cod_amount = $brand_price[0]['cod_amount']>0 ? $brand_price[0]['cod_amount'] : 0 ;
                $shipping_amount = $brand_price[0]['shipping_amount']>0 ? $brand_price[0]['shipping_amount'] : 0 ;
                 
                 if($brand_price[0]['order_tax_amount'] > 0) {
                   $productInfohtml =$productInfohtml.' <tr>
                   <td colspan="5" style="padding:2px 8px; text-align:right;">Tax :</td>
                    <td style="padding:2px 8px; text-align:right;">
                      &#8377;  '.$brand_price[0]['order_tax_amount'].'
                    </td>
                  </tr>';
                  }
                  if($cod_amount > 0) {
                  $productInfohtml =$productInfohtml.'<tr>

                    <td colspan="5" style="padding:2px 8px; text-align:right;">COD Charges:</td>
                    
                    <td style="padding:2px 8px; text-align:right;">
                      &#8377;  '.$cod_amount.'
                    </td>
                  </tr>';
                }
                if($shipping_amount > 0){
                  $productInfohtml =$productInfohtml.'<tr>

                    <td colspan="5" style="padding:2px 8px; text-align:right;">Shipping Charges:
                    </td>
                    <td style="padding:2px 8px; text-align:right;">
                      &#8377;  '.$shipping_amount.'
                    </td>
                  </tr>';
                }
                if($brand_price[0]['pay_mode'] == 1){
                $pay_mode = '<p style="margin-bottom:20px;">
                  Payment Mode : <img src="https://www.stylecracker.com/assets/images/icons/ic-cod-1.png" style="vertical-align:middle;"> Cash on Delivery
                </p>';
                }

                if($brand_price[0]['pay_mode'] == 2){
                $pay_mode = '<p style="margin-bottom:20px;">
                  Payment Mode : <img src="https://www.stylecracker.com/assets/images/icons/ic-online-payment-1.png" style="vertical-align:middle;"> Online Payment
                </p>';
                }
                /* if($brand_price[0]['order_total'] > 0) {
                  $productInfohtml =$productInfohtml.'<tr>
                    <td>
                      <strong>Order Total :<strong> </td>
                      <td>
                      &#8377;  '.$brand_price[0]['order_total'].'
                      </td>
                    </tr>
                  ';
              }*/

 $productInfohtml =$productInfohtml.'</tbody>
      </table>
      <div style="text-align:right; background:#f3f3f3; padding:5px 30px; font-size:18px; text-transform:uppercase; margin-top:20px; border-bottom:2px solid #cccccc;">
        Total Amount  : &#8377; '.$brand_price[0]['order_total'].'
      </div>
    </td>
  </tr>';
           $productInfohtml =$productInfohtml.' <tr>
    <td style="padding:0px 30px;">
        <div style=" border-bottom:1px solid #ccc; padding-bottom:30px;">
        Thank you for shopping with <strong>StyleCracker</strong>.<br/>
For any query or assistance, feel free to <a href="https://www.stylecracker.com/schome/aboutus" style="color:#00b19c; text-decoration:none;">Contact Us</a>
      </div>
    </td>
  </tr>
  <tr>
    <td style="vertical-align:middle; padding:20px 30px;">
      <span style="color: #888; font-weight: bold; line-height: 1.3; position: relative;top: -4px; vertical-align:top;">  Follow Us:  </span>

      <a target="_blank" href="https://www.facebook.com/StyleCracker" target="_blank"  title="Facebook"><img src="https://www.stylecracker.com/assets/images/social-media/facebook.png" alt="Facebook" style="width:24px;"></a>
      <a target="_blank" href="https://twitter.com/Style_Cracker" target="_blank"  title="Twitter"><img src="https://www.stylecracker.com/assets/images/social-media/twitter.png" alt="Twitter" style="width:24px;"></a>
      <a target="_blank" href="https://www.youtube.com/user/StyleCrackerTV" target="_blank"  title="Youtube"><img src="https://www.stylecracker.com/assets/images/social-media/youtube.png"  alt="Youtube" style="width:24px;"></a>
      <a href="https://instagram.com/stylecracker" target="_blank" title="Instagram"><img src="https://www.stylecracker.com/assets/images/social-media/instagram.png"  alt="Instagram" style="width:24px;"></a>
      <a target="_blank" href="https://www.pinterest.com/stylecracker" target="_blank"  title="Pintrest"><img src="https://www.stylecracker.com/assets/images/social-media/pintrest.png" alt="Pintrest" style="width:24px;"></a>

          </td>
        </tr>
      </table>


          <table width="600" cellpadding="0" cellspacing="0" align="center" border="0" style="width:600px; background:#fff; border:1px solid #cccccc; margin-bottom:20px;">
        <tr>
          <td align="left" style="padding:20px 0 0 20px; width:160px;">
              <img src="https://www.stylecracker.com/assets/images/logo.png" style="width:150px; margin-bottom:5px;">
          <div>
            <a href="https://www.stylecracker.com" style="color:#00b19c; text-decoration:none;">www.stylecracker.com</a>
          </div>
          </td>
            <td align="left"  style="padding:20px 0 0; vertical-align:bottom;">
              <img src="https://www.stylecracker.com/assets/images/icons/ic-download-app-1.png">
            <td>

            </td>
          <td align="right"  style="padding:20px 20px  0 0; text-align:center; ">
            <div style="font-size:18px; margin-bottom:10px;">
              StyleCracker App is India\'s only
personalised styling platform.
            </div>

             <a href="https://play.google.com/store/apps/details?id=com.stylecracker.android"  target="_blank" style="color:#333; text-decoration:none;">
                  <img src="https://www.stylecracker.com/assets/images/icons/en_generic_rgb_wo_45.png" style="height:30px;" target="_blank"></a>

            <a href="https://itunes.apple.com/us/app/stylecracker/id1061463812?mt=8&uo=4" target="_blank" style="color:#333; text-decoration:none; ">
              <img src="https://www.stylecracker.com/assets/images/icons/apple_store.png" style="height:30px;" target="_blank"></a>

               
          </td>
        </tr>
      </table>

    </body>
    </html>';

      $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Processing');
      //$this->email->to($this->getEmailAddress($user_id));
      $this->email->to($this->getEmailAddress($brand_price[0]['brand_id']));
      //$this->email->cc('dev@stylecracker.com');
	  $this->email->cc('rajesh@stylecracker.com');
      $this->email->subject('StyleCracker: Order Processing');


       $this->email->message('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Order Summary</title>

</head>
<body style="background:#f3f3f3; color:#333333; text-align:left; font-family:Arial,Helvetica,sans-serif;
font-weight: normal;
line-height: 1.5;
font-size: 14px; padding:0; margin:0 auto;">
<table width="600" cellpadding="0" cellspacing="0" align="center" border="0" style="width:600px; background:#fff; margin-bottom:20px;">
  <tr>
          <td style="text-align:left; border-bottom:3px solid #00b19c; padding:20px;">

            <table cellpadding="0" cellspacing="0" align="center" border="0" style="width:100%; ">
              <tr>
                      <td style="text-align:left;">

            <img src="https://www.stylecracker.com/assets/images/logo.png" style="height:34px;">
  </td>
  <td style="text-align:right; vertical-align:middle; font-size:14px; color:#00b19c;">

  <a href="https://www.stylecracker.com/sc_admin" style="color:#00b19c; text-decoration:none;">My Orders</a> 


          </td>
        </tr>
      </table>
      </td>
    </tr>

  <tr><td style="padding:30px;">

      <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
        <tr>
          <td align="center" style="padding:15px 0 30px; vertical-align:middle;">
             <span style="padding:8px 0; border-bottom:1px solid #cccccc; font-size:18px; text-transform:uppercase;">
               <img src="https://www.stylecracker.com/assets/images/icons/ic-order-processing-1.png" style="vertical-align:middle;"> Order Processing</span>
          </td>
        </tr>
        <tr>
          <td style="font-size:15px; line-height:1.7; ">
            <p style="margin-bottom:20px;">
            Hi team '.$companyName.' <br/>
            Congrats!, We have recevied new order for you. Your Order No is <strong style="color:#00B19C">'.$order_noval.'</strong> , further details are as below. <br>
            Please log on to your dashboard at <a href="https://www.stylecracker.com/sc_admin" target="_blank" >https://www.stylecracker.com/sc_admin</a> and update the status accordingly.
            </p>
            '.$pay_mode.'
            <div style="background:#f3f3f3; border:1px solid #cccccc; padding:20px 20px 15px; display:inline-block;">
                <div style="text-transform:uppercase; font-weight:bold; line-height:1;">
                  Shipping Address
                </div>
                <div>
                '.$first_name.' '.$last_last.'<br>
                  '. $brand_price[0]['shipping_address'].'<br>
                    '.  $brand_price[0]['state_name'].' ,'.  $brand_price[0]['city_name'].'  - '.  $brand_price[0]['pincode'].' <br>
                    Email:- '.$brand_price[0]['email_id'].' <br>
                    Mobile No:- '.$brand_price[0]['mobile_no'].'
                </div>

            </div>
          </td>
        </tr>
      </table>


      <br/>
      <br/>
      <div style="text-transform:uppercase; font-weight:bold; line-height:1; font-size:16px; margin-bottom:20px;">
        Order Details:
      </div>

      <table width="100%" cellpadding="8" cellspacing="0" align="center" border="0" style="font-size:12px;">
        <tbody>
          <tr>
            <th style="background-color:#f3f3f3; font-size:14px;">Image</th>
            <th style="background-color:#f3f3f3; font-size:14px;">Product Details</th>
            <th style="background-color:#f3f3f3; font-size:14px;">Qty</th>
            <th style="background-color:#f3f3f3; font-size:14px;">Size</th>
            <th style="background-color:#f3f3f3; font-size:14px;">Seller</th>
            <th style="background-color:#f3f3f3; font-size:14px; width:60px; text-align:right;">Price</th>
          </tr>'.$productInfohtml);

     $this->email->send();
        }
      }

    }
  }
  
    function getEmailAddress($user_id){
      $res = $this->Cart_model->get_user_info($user_id);
      if(!empty($res)){
        return $res[0]['email'];
      }else{ return ''; }
	}
	public function get_city_name_get(){
		
		$pincode = $this->input->get('pincode');
		$validate_pincode = $this->Mcart_model2->validate_pincode($pincode);
		if(strlen($pincode) != 6){
        $this->failed_response(1012, "pincode should contain 6 digits only ");
		}else if($validate_pincode == 2){
        $this->failed_response(1013, "please enter valid pincode");
		}
		$data = (object)$this->Mcart_model2->get_city_name($pincode);
		if($data !=NULL){
		 $this->success_response($data);
		}else{
		   $this->success_response($data);
		   //$this->failed_response(1001, "No Record(s) Found");
		}
	}
	
	public function get_order_info_get(){
      $uniquecookie = $this->get('SCUniqueID');
      $res['order_info']= $this->Mcart_model2->get_order_info($uniquecookie);
      $res['brand_price'] = $this->Mcart_model2->get_order_price_info($uniquecookie);
	  print_r($res);exit;
	}
	
	public function getPinCode_get(){
        $pincode = $this->get('pincode');
        $product_id = $this->get('productid');
		$validate_pincode = $this->Mcart_model2->validate_pincode($pincode);
        $chkShipping = $this->Mcart_model2->chkShipping($product_id,$pincode);
        $chkCod = $this->Mcart_model2->chkCod($product_id,$pincode);
		if(strlen($pincode) != 6){
        $this->failed_response(1012, "pincode should contain 6 digits only ");
		}else if($validate_pincode == 2){
        $this->failed_response(1013, "please enter valid pincode");
		}
		/*
        if(!empty($chkShipping) && !empty($chkCod)){
          $text =  'Shipping to pincode '.$pincode.' is not-available';
        }else if(!empty($chkShipping) && empty($chkCod)){
          $text = 'Shipping to pincode '.$pincode.' is not-available';
        }else if(empty($chkShipping) && !empty($chkCod)){
          $text = 'COD to pincode '.$pincode.' is not-available';
        }else{
          $text = 'Shipping to pincode '.$pincode.' is available';
        }
		*/
		
		 if(!empty($chkShipping) && !empty($chkCod)){
          $this->success_response('Shipping to pincode '.$pincode.' is not-available');
        }else if(!empty($chkShipping) && empty($chkCod)){
          $this->success_response('Shipping to pincode '.$pincode.' is not-available');
        }else if(empty($chkShipping) && !empty($chkCod)){
          $this->success_response('COD unavailable for this product at '.$pincode.'.');
        }else{
          $this->success_response('Shipping to pincode '.$pincode.' is available');
        }
		// $return_array = array($text);
		// $this->success_response(json_encode($return_array));
	}
	
	public function get_order_post(){
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$order_unique_no = $this->post('order_unique_no');
		$data = $this->Mcart_model2->get_order($order_unique_no,$userid);
		if($data !=NULL){
		 $this->success_response($data);
		}else{
			$res = array();
		   $this->success_response($data);
		   //$this->failed_response(1001, "No Record(s) Found");
		}
	}
	
	public function get_order_list_post(){
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$limit = $this->post('limit');
		$offset = $this->post('offset');
		$data = $this->Mcart_model2->get_order_list($userid,$limit,$offset);
		if($data !=NULL){
		 $this->success_response($data);
		}else{
			$data = array();
			$this->success_response($data);
		   //$this->failed_response(1001, "No Orders(s) Found please order something");
		}
	}
	
	public function change_status_post(){
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$order_id = $this->post('order_id');
		$status = $this->post('status');
		$res = $this->Mcart_model2->change_status($userid,$order_id,$status);
		if($status == 1){
			$res1 = $this->Mcart_model2->update_order_cart_info_online_order($userid,$order_id);
		// $this->orderPlaceMessage($userid,$order_id,$data['uc_first_name'],$data['uc_last_name']);
		// $this->orderPlaceMessagetoBrand($userid,$order_id,$data['uc_first_name'],$data['uc_last_name']);
		}else{
			$res1 = (string)$this->Mcart_model2->transaction_failed($userid,$order_id);
		}
		if(!empty($res)){
			$this->success_response($res);
		}
		else{
			$this->failed_response(1300, "Wrong credentials!");
		}	
	}
	
	//productQtyUpdate
	public function productSizeUpdate_post(){
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$product_id = $this->input->post('product_id');
		$productsize = $this->input->post('productsize');
		$cart_id = $this->post('cart_id');
		//$productInfo = base64_decode(str_replace("product_cart_info_","",$this->input->post('productInfo')));
		$data = (object)$this->Mcart_model2->productSizeUpdate($product_id,$productsize,$userid,$cart_id);
		
		
		if($data !=NULL){
		 $this->success_response($data);
		}else{
			$res = array();
		   $this->success_response($data);
		   //$this->failed_response(1001, "No Record(s) Found");
		}
	}
	/*
	function multiPinCode_get(){
        $pincode = $this->get('pincode');
		// $userid = $this->post('user_id');
		// $auth_token = $this->post('auth_token');
        $product_id = $this->get('productid');
		$payment_method = $this->get('payment_method');
		
		if($payment_method == 'online'){
			$i=0;
			$text_array = array();
			$chkShipping = $this->Mcart_model2->chkShipping2($product_id,$pincode);
			if(!empty($chkShipping)){
				foreach($chkShipping as $val){
					//echo $val['id'];exit;
					$product_object = new stdClass();
					$product_info = $this->Mcart_model2->product_info($val['id']);
					$product_object->text =  'Shipping not-available for '.$product_info[0]['name'].' please remove this item from shopping cart and continue to place order';
					$product_object->image_url = "https://www.stylecracker.com/sc_admin/assets/products/".$product_info[0]['image'];
					$text_array[$i] = $product_object;
					$i++; 
				}
			}
		}
		else if($payment_method == 'cod'){
			$j=0;
			$text_array = array();
			$chkCod = $this->Mcart_model2->chkCod2($product_id,$pincode);
			if(!empty($chkCod)){
				foreach($chkCod as $val){
					//echo $val['id'];exit;
					$product_object = new stdClass(); 
					$product_info = $this->Mcart_model2->product_info($val['id']);
					$product_object->text =  'COD not-available for '.$product_info[0]['name'].' please remove this item from shopping cart and continue to place order';
					$product_object->image_url = "https://www.stylecracker.com/sc_admin/assets/products/".$product_info[0]['image'];
					$text_array[$j] = $product_object;
					$j++; 
				}
			}
		}
		$this->success_response($text_array);
	}
	*/
	
	public function multiPinCode_post(){
        $pincode = $this->post('pincode');
		$user_id = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		if(trim($pincode) == ''){
		$this->failed_response(1007, "Please enter the pincode");

		}else if(strlen($pincode) != 6){
        $this->failed_response(1013, "pincode should contain 6 digit only");
		}
		$product_id = $this->Mcart_model2->get_product_id($user_id);
		$chkShipping = $this->Mcart_model2->chkShipping2($product_id,$pincode);
		$chkCod = $this->Mcart_model2->chkCod2($product_id,$pincode);
		$city_name = $this->Mcart_model2->get_city_name($pincode);
        $paymod = $this->post('paymod');
		if($paymod == ''){
			$paymod	= 'online';
		}
		if(!empty($city_name)){
			$city = $city_name['city']; 
			$state = $city_name['state'];
		
		
		$data = (object)$this->Mcart_model2->get_cart($user_id,$paymod,$pincode,$chkShipping,$chkCod,$city,$state);
		if($data !=NULL){
		 $this->success_response($data);
		}else{
			$res = array();
		   $this->success_response($data);
		   //$this->failed_response(1001, "No Record(s) Found");
		}
		
		}
		else $this->failed_response(1001, "Please enter valid Pincode");
		
	}
	public function getaddress_post(){
		$user_id = $this->post('user_id');
		$auth_token = $this->post('auth_token');
        $data = $this->Mcart_model2->getaddress($user_id);		
		if($data !=NULL){
		 $this->success_response($data);
		}else{
		   $res = array();
		   $this->success_response($res);
		}
		
	}
	
	public function add_address_post(){
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$uc_first_name = $this->input->post('uc_first_name');
		$uc_email_id = $this->input->post('uc_email_id');
		$uc_mobile = $this->input->post('uc_mobile');
		$uc_address = $this->input->post('uc_address');
		$uc_pincode = $this->input->post('uc_pincode');
		$uc_city = $this->input->post('uc_city');
		$state = $this->Mcart_model2->get_city_name($uc_pincode);
		//$uc_state = trim($state['state']);
		$uc_state = $this->Mcart_model2->get_state_id($uc_pincode);
		$name =  explode(' ',$uc_first_name);
		$count = count($name);
		if(!empty($name) && $count >= 2){
			$first_name = $name[0];
			$last_name = $name[1].' '.@$name[2];
		}else {
			$first_name = $uc_first_name;
			$last_name = $uc_first_name;
		}
		if(trim($uc_first_name) == ''){
		$this->failed_response(1005, "Please enter the uc_first_name");

		}else if(trim($uc_email_id) == ''){
		$this->failed_response(1006, "Please enter the uc_email_id");

		}else if(trim($uc_mobile) == ''){
		$this->failed_response(1007, "Please enter the uc_mobile");

		}else if(filter_var($uc_email_id, FILTER_VALIDATE_EMAIL) === false){
		$this->failed_response(1008, "Please enter valid Email-id");

		}else if(strlen($uc_mobile) != 10){
		$this->failed_response(1005, "Please enter valid mobile number");

		}else if(trim($uc_address) == ''){
		$this->failed_response(1006, "Please enter the uc_address");

		}else if(trim($uc_pincode) == ''){
		$this->failed_response(1007, "Please enter the uc_pincode");

		}else if(trim($uc_city) == ''){
		$this->failed_response(1006, "Please enter the uc_city");

		}else if(trim($uc_state) == ''){
		$this->failed_response(1007, "Please enter the uc_state");

		}else if(strlen($uc_pincode) != 6){
        $this->failed_response(1013, "pincode should contain 6 number");
		}else if($this->checkpincode($uc_pincode) != 2){
        $this->failed_response(1010, "Pincode does not exist");
		}
		$data['user_id'] = $userid;
		$data['uc_first_name'] = $first_name;
		$data['uc_last_name'] = $last_name;
		$data['uc_email_id'] = $uc_email_id;
		$data['uc_mobile'] = $uc_mobile;
		$data['uc_address'] = $uc_address;
		$data['uc_pincode'] = $uc_pincode;
		$data['uc_city'] = $uc_city;
		$data['uc_state'] = $uc_state;
		
        $res = (object)$this->Mcart_model2->add_address($data);
		
		if($res !=NULL){
		 $this->success_response($res);
		}else{
		   $this->success_response($res);
		}
		
	}
	
	public function notify_me_post()
	{
		$user_id = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$product_id = $this->post('product_id');
		$data['user_id'] = $user_id;
		$data['product_id'] = $product_id;
		$res = (object)$this->Mcart_model2->notify_me($data);
		if($res !=NULL){
		//$text = "You will be informed when product will be in stock";
                $text = "We will let you know once this product is back in stock.";
		//$return_array = array($text);
		//$message = $this->success_response(json_encode($return_array));
             //   $message = $this->success_response(json_encode($text));	
		$this->success_response($text);
		}else{
		   $this->failed_response(1001, "No records found");
		}
	}
	
	public function checkpincode($pincode){
      $res = $this->Mcart_model2->get_city_name($pincode);
	  //echo count($res);exit;
      return count($res);
	}
	
	public function sc_check_otp_post(){
		$user_id = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$uc_mobile_no = trim($this->post('mobile_no'));
		$data['otp_number'] = trim($this->post('otp_number'));
		$data['mobile_no'] = '91'.$uc_mobile_no;
		$res = $this->Cart_model->sc_check_otp($data['mobile_no'],$data['otp_number']);
		//echo $res;exit;
		if($res!=0){
			$data = new stdclass();
			$data->message = 'mobile number validated successfully';
			$this->success_response($data);
		}else{
			 $this->failed_response(1001, "wrong OTP please try again");
		}
	}

	public function scgenrate_otp_post(){
		$user_id = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$uc_mobile_no = trim($this->post('mobile_no'));
		$data['mobile_no'] = '91'.$uc_mobile_no;
		if(strlen($uc_mobile_no) != 10){
        $this->failed_response(1013, "please enter 10 digit mobile number");
		}
		$otp_no = rand(5,100000);
		$digits = 5;
		$otp_no =rand(pow(10, $digits-1), pow(10, $digits)-1);
		//$otp_no = 99999;
		$res = $this->Cart_model->scgenrate_otp($data['mobile_no'],$otp_no);
		/* Call SMS send API */
		$message = 'Thank+you+for+choosing+STYCKR.+Your+OTP+for+cash+on+delivery+is+'.$otp_no.'.+Please+use+this+to+confirm+your+order.+Stay+Stylish!';
		$ch = curl_init('https://www.txtguru.in/imobile/api.php?');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'username=stylecracker_com&password=99903979&source=STYCKR&dmobile='.$data['mobile_no'].'&message='.$message.'');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		$data = curl_exec($ch);
		/* end of code Call SMS send API */
		if($res = 1){
		$data = new stdclass();
			$data->message = 'success';
			$this->success_response($data);
		}else{
			 $this->failed_response(1001, "OTP generated successfully");
		}
	}
	
	public function online_payment_post(){
		
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$address_id = $this->input->post('address_id');
		$user_info = $this->Mcart_model2->user_info($address_id);
		$paymod = $this->input->post('paymod');
		$user_credentials = $this->input->post('user_credentials');
		$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		$data = array();
		
		$data['user_id'] = $userid;
		$data['uc_first_name'] = $user_info['first_name'];
		$data['uc_last_name'] = $user_info['last_name'];
		$data['uc_email_id'] = $user_info['email'];
		$data['uc_mobile'] = $user_info['mobile_no'];
		$data['uc_address'] = $user_info['shipping_address'];
		$data['uc_pincode'] = $user_info['pincode'];
		$data['uc_city'] = $user_info['city_name'];
		$data['uc_state'] = $user_info['state_name'];
		$data['paymod'] = $paymod;
		//$data['SCUniqueID'] = $SCUniqueID;
		$data['mihpayid'] = '';
		$data['txnid'] = '';

		if($paymod == 'cod'){
		  $res = $this->Mcart_model2->sc_uc_order2($data);
		  if($res !=NULL && $res != '2'){
		  $this->success_response($res);
		  }
		}else if($paymod == 'online'){
		  $res = (array)$this->Mcart_model2->sc_uc_order2($data);
		  if($res !=NULL && $res != '2'){
			$amount = $res['order_total'];
			$productinfo = $res['product_details'][0][0]->name;
			$firstname = $res['first_name'];
			$email = $res['first_name'];
			 $data = $this->getHashes($txnid, $amount, $productinfo, $firstname, $email, $user_credentials);
			// print_r($data);exit;
			   if($data !=NULL && $res != '2'){
				  $this->success_response($data);
			   }
		  }
		}
		else {
		$this->failed_response(1201, "Your cart is empty");
		}

		

	}
	
	public function getHashes($txnid, $amount, $productinfo, $firstname, $email, $user_credentials=NULL)
	{
      // $firstname, $email can be "", i.e empty string if needed. Same should be sent to PayU server (in request params) also.
      $key = 'Y2tBLg';
      $salt = 'rzMxzsyT';
  
      $payhash_str = $key . '|' . $txnid . '|' .$amount  . '|' .$productinfo  . '|' . $firstname . '|' . $email . '|' . $salt;
      $paymentHash = strtolower(hash('sha512', $payhash_str));
      $arr['payment_hash'] = $paymentHash;

      $cmnNameMerchantCodes = 'stylecracker pay U';
      $merchantCodesHash_str = $key . '|' . $cmnNameMerchantCodes . '|default|' . $salt ;
      $merchantCodesHash = strtolower(hash('sha512', $merchantCodesHash_str));
      $arr['get_merchant_stylecracker_codes_hash'] = $merchantCodesHash;

      $cmnMobileSdk = 'vas_for_mobile_sdk';
      $mobileSdk_str = $key . '|' . $cmnMobileSdk . '|default|' . $salt;
      $mobileSdk = strtolower(hash('sha512', $mobileSdk_str));
      $arr['vas_for_mobile_sdk_hash'] = $mobileSdk;

      $cmnPaymentRelatedDetailsForMobileSdk1 = 'payment_related_details_for_mobile_sdk';
      $detailsForMobileSdk_str1 = $key  . '|' . $cmnPaymentRelatedDetailsForMobileSdk1 . '|default|' . $salt ;
      $detailsForMobileSdk1 = strtolower(hash('sha512', $detailsForMobileSdk_str1));
      $arr['payment_related_details_for_mobile_sdk_hash'] = $detailsForMobileSdk1;

      if($user_credentials != NULL && $user_credentials != '')
      {
            $cmnNameDeleteCard = 'delete_user_card';
            $deleteHash_str = $key  . '|' . $cmnNameDeleteCard . '|' . $user_credentials . '|' . $salt ;
            $deleteHash = strtolower(hash('sha512', $deleteHash_str));
            $arr['delete_user_card_hash'] = $deleteHash;
            
            $cmnNameGetUserCard = 'get_user_cards';
            $getUserCardHash_str = $key  . '|' . $cmnNameGetUserCard . '|' . $user_credentials . '|' . $salt ;
            $getUserCardHash = strtolower(hash('sha512', $getUserCardHash_str));
            $arr['get_user_cards_hash'] = $getUserCardHash;
            
            $cmnNameEditUserCard = 'edit_user_card';
            $editUserCardHash_str = $key  . '|' . $cmnNameEditUserCard . '|' . $user_credentials . '|' . $salt ;
            $editUserCardHash = strtolower(hash('sha512', $editUserCardHash_str));
            $arr['edit_user_card_hash'] = $editUserCardHash;
            
            $cmnNameSaveUserCard = 'save_user_card';
            $saveUserCardHash_str = $key  . '|' . $cmnNameSaveUserCard . '|' . $user_credentials . '|' . $salt ;
            $saveUserCardHash = strtolower(hash('sha512', $saveUserCardHash_str));
            $arr['save_user_card_hash'] = $saveUserCardHash;
            
            $cmnPaymentRelatedDetailsForMobileSdk = 'payment_related_details_for_mobile_sdk';
            $detailsForMobileSdk_str = $key  . '|' . $cmnPaymentRelatedDetailsForMobileSdk . '|' . $user_credentials . '|' . $salt ;
            $detailsForMobileSdk = strtolower(hash('sha512', $detailsForMobileSdk_str));
            $arr['payment_related_details_for_mobile_sdk_hash'] = $detailsForMobileSdk;
      }
	   //print_r($arr);exit;
		return array('result'=>$arr);
		
	}

	public function scgenrate_hashes_post(){

	 	$amount =  $this->post('amount');
	    $email =  $this->post('email');
	    $firstname =  $this->post('firstname');
	    $phone =  $this->post('phone');
	    $productinfo =  $this->post('productinfo');
	    $txnid =  $this->post('txnid');
		$surl = $this->post('surl');
		$furl = $this->post('furl');
	    //$furl =  "https://payu.herokuapp.com/ios_failure";
	    $key =  'd9WSht';
	    $offer_key =  "test123@6622";
	    //$surl = "http://www.scflash.com/Sccart/successOrder";
	    $udf1 = '';
	    $udf2 = '';
	    $udf3 = '';
	    $udf4 = '';
	   	$udf5 = '';
	    $user_credentials =  'default';
	    $result = $this->mobileHashTestWs($txnid, $amount, $productinfo, $firstname, $email, $user_credentials, $udf1, $udf2, $udf3, $udf4, $udf5);
	    if(!empty($result)){
	    	$this->success_response($result);
	    }else return NULL; 

	}


	public function mobileHashTestWs($txnid, $amount, $productinfo, $firstname, $email, $user_credentials=NULL, $udf1, $udf2, $udf3, $udf4, $udf5)
	{
      // $firstname, $email can be "", i.e empty string if needed. Same should be sent to PayU server (in request params) also.
      $key = 'd9WSht';
      $salt = 'It1JXKPL';
      if($udf1 == NULL) {
            $udf1 = '';
      }
      if($udf2 == NULL) {
            $udf2 = '';
      }
      if($udf3 == NULL) {
            $udf3 = '';
      }
      if($udf4 == NULL) {
            $udf4 = '';
      }
      if($udf5 == NULL) {
            $udf5 = '';
      }
      // $payhash_str = $key . '|' . $txnid . '|' . $amount . '|' . $productinfo . '|' . $firstname . '|' . $email . '|' . $udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '||||||' . $salt;
      // $paymentHash = strtolower(hash('sha512', $payhash_str));
      // $arr['paymentHash'] = $paymentHash;
	  $payhash_str = $key . '|' . $txnid . '|' . $amount . '|' . $productinfo . '|' . $firstname . '|' . $email . '|' . $udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '||||||' . $salt;
      $paymentHash = strtolower(hash('sha512', $payhash_str));
      $arr['paymentHash'] = $paymentHash;
      $cmnNameMerchantCodes = 'get_merchant_ibibo_codes';
      $merchantCodesHash_str = $key . '|' . $cmnNameMerchantCodes . '|default|' . $salt ;
      $merchantCodesHash = strtolower(hash('sha512', $merchantCodesHash_str));
      $arr['merchantCodesHash'] = $merchantCodesHash;

      $cmnMobileSdk = 'vas_for_mobile_sdk';
      $mobileSdk_str = $key . '|' . $cmnMobileSdk . '|default|' . $salt;
      $mobileSdk = strtolower(hash('sha512', $mobileSdk_str));
      $arr['mobileSdk'] = $mobileSdk;

      $cmnPaymentRelatedDetailsForMobileSdk1 = 'payment_related_details_for_mobile_sdk';
      $detailsForMobileSdk_str1 = $key  . '|' . $cmnPaymentRelatedDetailsForMobileSdk1 . '|default|' . $salt ;
      $detailsForMobileSdk1 = strtolower(hash('sha512', $detailsForMobileSdk_str1));
      $arr['detailsForMobileSdk'] = $detailsForMobileSdk1;

      if($user_credentials != NULL && $user_credentials != '')
      {
            $cmnNameDeleteCard = 'delete_user_card';
            $deleteHash_str = $key  . '|' . $cmnNameDeleteCard . '|' . $user_credentials . '|' . $salt ;
            $deleteHash = strtolower(hash('sha512', $deleteHash_str));
            $arr['deleteHash'] = $deleteHash;
            
            $cmnNameGetUserCard = 'get_user_cards';
            $getUserCardHash_str = $key  . '|' . $cmnNameGetUserCard . '|' . $user_credentials . '|' . $salt ;
            $getUserCardHash = strtolower(hash('sha512', $getUserCardHash_str));
            $arr['getUserCardHash'] = $getUserCardHash;
            
            $cmnNameEditUserCard = 'edit_user_card';
            $editUserCardHash_str = $key  . '|' . $cmnNameEditUserCard . '|' . $user_credentials . '|' . $salt ;
            $editUserCardHash = strtolower(hash('sha512', $editUserCardHash_str));
            $arr['editUserCardHash'] = $editUserCardHash;
            
            $cmnNameSaveUserCard = 'save_user_card';
            $saveUserCardHash_str = $key  . '|' . $cmnNameSaveUserCard . '|' . $user_credentials . '|' . $salt ;
            $saveUserCardHash = strtolower(hash('sha512', $saveUserCardHash_str));
            $arr['saveUserCardHash'] = $saveUserCardHash;
            
            $cmnPaymentRelatedDetailsForMobileSdk = 'payment_related_details_for_mobile_sdk';
            $detailsForMobileSdk_str = $key  . '|' . $cmnPaymentRelatedDetailsForMobileSdk . '|' . $user_credentials . '|' . $salt ;
            $detailsForMobileSdk = strtolower(hash('sha512', $detailsForMobileSdk_str));
            $arr['detailsForMobileSdk'] = $detailsForMobileSdk;
      }
    	return array('result'=>$arr);
	}
	/*
	public function transaction_failed_post(){
		$user_id = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$order_id = $this->post('order_id');
		//$status = $this->post('status');
		$res = (string)$this->Mcart_model2->transaction_failed($user_id,$order_id);
		if($res == 1){
			$response = array();
			$response['text'] = "Your transaction is failed";
			$this->success_response((object)$response);
		}
		else{
			$this->failed_response(1300, "Wrong credentials!");
		}	
		
	}*/
	
	public function transaction_failed_post(){
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$order_id = $this->post('order_id');
		$status = $this->post('status');
		$res = $this->Mcart_model2->change_status($userid,$order_id,$status);
		if($status == 1){
			$res1 = $this->Mcart_model2->update_order_cart_info_online_order($userid,$order_id);
		// $this->orderPlaceMessage($userid,$order_id,$data['uc_first_name'],$data['uc_last_name']);
		// $this->orderPlaceMessagetoBrand($userid,$order_id,$data['uc_first_name'],$data['uc_last_name']);
		}else{
			$res1 = (string)$this->Mcart_model2->transaction_failed($userid,$order_id);
		}
		if(!empty($res)){
			$this->success_response($res);
		}
		else{
			$this->failed_response(1300, "Wrong credentials!");
		}
	}
	
	public function successOrder_get(){
		 
	}
	
	public function failureOrder_get(){
		
	}
}
	
?>