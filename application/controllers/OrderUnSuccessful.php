<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class OrderUnSuccessful extends MY_Controller {

	function __construct(){

       parent::__construct();

   	}

	public function index()
	{

   // if($this->session->userdata('user_id')){

      $this->load->view('common/header_view');
      $this->load->view('cart/order-unsuccessful');
      $this->load->view('common/footer_view');
   // }
	}

}
