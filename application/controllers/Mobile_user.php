<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
class Mobile_user extends TT_REST_Controller 
{
  function __construct()
	{
		parent::__construct();
		$this->load->model('Schome_model');
		$this->load->model('User_info_model');
		$this->load->model('Pa_model');
		$this->load->library('bitly');
		$this->load->library('email');
		$this->load->library('user_agent');
		$this->load->library('curl');
		$this->load->model('Collection_model');
		$this->load->model('Event_model');
		$this->load->model('Mobile_look_model');
		$this->load->model('Blog_model');
		$this->load->model('Mobile_model_android');
		$this->load->model('Brand_model');
		$this->open_methods = array('register_user_post','login_post','scgenrate_otp_post');    
	}
    
	public function register_user_post(){
		
		$scusername = $this->post('sc_username_name');
		$scemail_id = $this->post('sc_email_id');
		$scpassword = $this->post('sc_password');
		$medium 	= $this->post('medium');
		$medium_id 	= $this->post('medium_id');
		$gender 	= $this->post('gender');
		$contact_no = $this->post('contact_no');
		$birth_date = $this->post('birth_date');
		$version 	= $this->post('version');
		$age_range = $this->post('age_range'); 
		
		$json_array = array();
		
		if($gender == 'male' || $gender == 'men'){ 
			$gender = 2; 
			$json_array['question_id'] = 15;
		}else if($gender == 'female' || $gender == 'women'){ 
			$gender = 1;
			$json_array['question_id'] = 8;
		}else $gender = 1;
		
		$skip = 0;
		if($medium == 'mobile_facebook' || $medium == 'mobile_ios_facebook' || $medium == 'mobile_android_facebook' || $medium == 'mobile_ios_google' || $medium == 'mobile_android_google'){ $skip  = 1;}
		
		if(trim($scemail_id) == ''){
			$this->failed_response(1006, "Please enter the Email-id");

		}else if(trim($scpassword) == ''){
			$this->failed_response(1007, "Please enter the Password");

		}else if(filter_var($scemail_id, FILTER_VALIDATE_EMAIL) === false){
			$this->failed_response(1008, "Please enter valid Email-id");

		}else if($this->emailUnique($scemail_id) > 0 && $skip == 0){
			$this->failed_response(1010, "Email ID already exists.");
		}
		else if(strlen($scpassword) < 8 && $skip == 0){
			$this->failed_response(1013, "Password should have atleast 8 character");
		}
		else if(strlen($scpassword) > 20 && $skip == 0){
			$this->failed_response(1014, "Password should not greater than 20 character");
		}
		else{

			$name = str_replace(' ', '', $scusername);
			$random = md5(uniqid(rand(0,2), true));
			$random = substr($random, 0, 5);
			$firstname_split = explode(' ',$scusername);
			$firstname = $firstname_split[0];
			$last_name = @$firstname_split[1];
			$scusername = $name.$random;
			if($this->emailUnique($scemail_id) > 0){
				$gender = $this->User_info_model->get_gender_by_email($scemail_id); 
			}else{
				$email_sent = $this->send_welcome_email($scemail_id);
			}
		
			$ip = $this->input->ip_address();
			$json_array['firstname'] = $firstname;
			$json_array['lastname'] = $last_name;
			$json_array['scmobile'] = $contact_no;
			$json_array['event_code'] = '';
			$json_array['id'] = $medium_id;
			$json_array['birthday'] = $birth_date;
			$json_array['age_range'] = $age_range;

			$res = $this->User_info_model->registerUser($medium,$scusername,$scemail_id,$scpassword,$ip,2,$gender,$json_array);
			if(!empty($res)) { 
				$data = new stdClass();
				$data->user_info= $this->getuserinfo_get($res['user_id']);
				$gender = $this->Pa_model->get_gender($res['user_id']);
				//$data->pa= $this->pa_get($res['user_id'],$gender);
				$this->success_response($data);
			}
			else{ 
				$this->failed_response(1011, "Registration failed!"); 
			}
		}					
	}
	
	public function forget_user_email_get(){
	$sc_email_id = $this->get('sc_email_id');
	$res = $this->Schome_model->forgot_password($sc_email_id);
	
	if($res != false){
		if($this->send_forgotpwd_email($sc_email_id,$res)) 
		{
			   $mobileNumber = $this->Schome_model->send_forgot_password_to_mobile($sc_email_id);
				file_get_contents('https://2factor.in/API/R1/?module=TRANS_SMS&apikey='.API_KEY_2FACTOR.'&to='.$mobileNumber['contact_no'].'&from='.OTP_FROM.'&templatename=forgot_password&var1='.$res.'');
			   $data = new stdClass();
		       $data->message = 'New Password has been Send to your email-id '.$sc_email_id;
			   $this->success_response($data);
		}
		else{
		  $this->failed_response(1017, "Invalid Email-id ");
		}	
	}else{
		$this->failed_response(1012, "Authorization failed!");
	}
	
  }
  
  public function send_forgotpwd_email($email_id,$new_password){

      $config['protocol'] = 'smtp';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');

      $this->email->initialize($config);

      $this->email->from($this->config->item('from_email'), 'Stylecracker');
        if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){              
            $this->email->to($this->config->item('sc_test_emaild'));                    
        }else{
      		$this->email->to($email_id);
      	}
      $this->email->subject('StyleCracker: New Password');
      $this->email->message("Your login details are <br> Email : ".$email_id."<br><br>Your new password is:".$new_password."<br><br>You can change your password at any time by logging into your account. <br><br>Warm Regards,<br>StyleCracker");

      if($this->email->send())
      {
        return "Email Sent";
      }else
      {
        return "Email Error";
      }
      
  }
	
	public function getuserinfo_get($user_id)
	{
		if($user_id){
			$user_info = (object)$this->User_info_model->get_users_info_mobile($user_id);
			return $user_info;
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
	}
	
	public function pa_get($user_id,$gender='')
	{
		// $gender=2;
		if($user_id){
			if($gender == '1'){
				$data = new stdClass();
				//$data -> body_shape = $this->Pa_model->get_question_img_answers(1,$gender);
				//$data -> style = $this->Pa_model->get_question_img_answers(2,$gender);
				$data -> brand_list = unserialize(PA_BRAND_LIST_WOMEN);
				
			} // end of women pa
			else {
				$data = new stdClass();
				//$data->body_shape = $this->Pa_model->get_question_img_answers(24,$gender);
				//$data->style = $this->Pa_model->get_question_img_answers(25,$gender);
				$data -> brand_list = unserialize(PA_BRAND_LIST_MEN);

			}// end men pa
			
			$pa_image_sets = array("set1/", "set2/", "set3/");
			$rand_set = $pa_image_sets[array_rand($pa_image_sets)];
			
			//$data -> user_body_shape = @$this->Pa_model->get_users_answer_obj($user_id,'user_body_shape',$gender)[0];
			//$data -> user_style_p1 = @$this->Pa_model->get_users_answer_obj($user_id,'user_style_p1',$gender,$rand_set)[0];
			//$data -> user_style_p2 = @$this->Pa_model->get_users_answer_obj($user_id,'user_style_p2',$gender,$rand_set)[0];
			//$data -> user_style_p3 = @$this->Pa_model->get_users_answer_obj($user_id,'user_style_p3',$gender,$rand_set)[0];
			$data -> user_brand_list = @$this->Pa_model->get_users_brand_list($user_id,'user_brand_list',$type='',$gender);
			$data -> question_resume_id = $this->Pa_model->get_question_resume_id($user_id);
			$data -> edited_data ='';
			return $data;
			
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}

	}
	
	public function send_welcome_email($email){

      $config['protocol'] = 'smtp';
      $config['charset'] = 'iso-8859-1';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');
	 
	  // added for confiem email
	  $email_en = base64_encode($email);
	  $user_id_en = base64_encode($this->session->userdata('user_id'));
	  // $link = base_url()."schome/confirm_email/".$email_en."";
	  // $email = 'rajesh@stylecracker.com';
	  
      $this->email->initialize($config);

      $this->email->from($this->config->item('from_email'), 'Stylecracker');
        if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){              
                $this->email->to($this->config->item('sc_test_emaild')); 
                $this->email->cc($this->config->item('sc_testcc_emaild'));      
        }else{
      		$this->email->to($email);
      	}
      $this->email->subject('Welcome to StyleCracker');
      $this->email->message('Hi there,<br><br>Welcome to StyleCracker! <br><br> StyleCracker is India\'s first online styling platform and we are here to make you the best version of youself! <br><br> When we started StyleCracker our aim was to make personalized styling available to everyone. We have a dedicated team of stylists who are online 24/7 waiting to answer ANY style query you may have. Never again will you have to worry about \'what do I wear\'- be it day-to-day or for special occasions, we will be there! <br><br> So, please explore the website and let us know how we can help. It would be great if you could also share your feedback with us so that we can help choose the best products for you. <br><br> Love, <br>Archana Walavalkar & Dhimaan Shah <br>Founders<br> <a href="'.base_url().'">www.stylecracker.com</a>');

      $this->email->send();

  }
  
	public function emailUnique($email){
		$res = $this->Schome_model->emailUnique($email);
		return count($res);
	}
	
	public function login_post(){
		$gcm_id = '';
		$logEmailid = $this->post('sc_email_id');
		$logPassword = $this->post('sc_password');
		$type = $this->post('type');

		if(trim($logEmailid) == ''){
			$this->failed_response(1006, "Please enter the Email-id");

		}else if($this->emailUnique($logEmailid) > 0){
			$result = $this->User_info_model->login_user($logEmailid,$logPassword,'mobile',$type);

			if($result != NULL && $result != 2){
					
					if(isset($result['question_comp'])) $question_comp = $result['question_comp']; else $question_comp = '';

					//$question_resume_id = $this->session->userdata('question_resume_id');
					$data = new stdClass();
					if($result['bucket'] == '' || $question_comp!=1 ){
						$data->redirect_url = 'pa';
					}else{
						$data->redirect_url = 'profile';
					}
					$gender = $this->User_info_model->get_user_gender($result['user_id']);
					$data->user_info = $this->getuserinfo_get($result['user_id']);//return value of get_users_info_mobile function
					//$extra_info = $this->User_info_model->get_user_info_get($result['user_id']);
				/* 	$stateName	= $this->User_info_model->get_state_name_by_id($extra_info['user_state']);
					$data->user_info->pincode		= $extra_info['user_pincode'];
					$data->user_info->city			= $extra_info['user_city'];
					$data->user_info->state			= ($stateName)?$stateName:'';
					$data->user_info->profession	= $extra_info['user_profession'];
					$data->user_info->shipping_address	= $extra_info['user_shipaddress']; */
					//echo $this->db->last_query();
					/* echo "<pre>";
					print_r($data->extra_info);
					echo "</pre>";
					exit(); */
					// $update_gcm_id = $this->Mobile_model_android->update_gcm_id($gcm_id,$result[0]['user_id']);
					//$data->pa = $this->pa_get($result['user_id'],$gender);
					$this->success_response($data);
			}else{
				$this->failed_response(1012, "Email id or password is incorrect");
			}
		}else {
			if($type == 'facebook' || $type == 'google'){
				$data = new stdClass();
				$data->success = 'Email id or password is incorrect';
				$this->success_response($data);
			}else{
				$this->failed_response(1006, "Email id or password is incorrect");
			}
			
		}
	}
	
	public function fetch_user_info_post()
	{
		$user_id 	= $this->input->post('user_id');
		$auth_token = $this->input->post('auth_token');
		if(empty($user_id)) { $this->failed_response(1006, "Please Check User Id"); }
		if(empty($auth_token)) { $this->failed_response(1006, "Please Check Auth Token"); }
		$extra_info = $this->User_info_model->get_user_info_get($user_id);
		if(empty($extra_info))
		{
			$this->failed_response(1006, "No Data Found");
		} 
		$stateName	= $this->User_info_model->get_state_name_by_id($extra_info['user_state']);
		$data['user_info']['dob']			= ($extra_info['user_dob'])?$extra_info['user_dob']:'';
		$data['user_info']['mobile']		= ($extra_info['user_mobile'])?$extra_info['user_mobile']:'';
		$data['user_info']['pincode']		= ($extra_info['user_pincode'])?$extra_info['user_pincode']:'';
		$data['user_info']['city']			= ($extra_info['user_city'])?$extra_info['user_city']:'';
		$data['user_info']['state']			= ($stateName)?$stateName:'';
		$data['user_info']['profession']	= ($extra_info['user_profession'])?$extra_info['user_profession']:'';
		$data['user_info']['shipping_address']	= ($extra_info['user_shipaddress'])?$extra_info['user_shipaddress']:'';
		$this->success_response($data);	
	}
	
	public function update_pa_post()
	{
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');		
		$json_string = $this->post('json_string');
		$gender = $this->post('gender');
		
		if($gender == '1'){
			$data_answers = array('user_body_shape'=>1,'user_style_p1'=>2,'user_style_p2'=>2,'user_style_p3'=>2,'user_brand_list'=>30);
		}else{
			$data_answers = array('user_body_shape'=>24,'user_style_p1'=>25,'user_style_p2'=>25,'user_style_p3'=>25,'user_brand_list'=>30);
		}
		
		$jsondecode = json_decode($json_string, true);
		$body_shape_ans = 0;$user_style_p1 = 0;
		foreach($jsondecode as $key=>$json){
			$question = @$json['question'];
			$question_id = $data_answers[$question];
			$answer_id = @$json['answer_id'];
			if($question_id == '1' || $question_id == '24'){ $body_shape_ans = $answer_id; }
			if(($question_id == '2' || $question_id == '25') && $question == 'user_style_p1'){ $user_style_p1 = $answer_id; }
			$this->Pa_model->save_user_answer($question_id,$answer_id,$user_id,$gender,$question);
		}
		$bucket = $this->Pa_model->assign_buckets($body_shape_ans,$user_style_p1,$user_id);
		$data = new stdClass();
		$data->user_info = $this->getuserinfo_get($user_id);
		$data->pa = $this->pa_get($user_id,$gender);
		$this->success_response($data);
	}
	
	public function scgenrate_otp_post(){
		
		$uc_mobile_no 	= trim($this->post('mobile_no'));
		$type 			= $this->post('type');
		
		$data['mobile_no'] = '91'.$uc_mobile_no;
		$data['type'] = $type;
		if(strlen($uc_mobile_no) != 10){
			$this->failed_response(1013, "please enter 10 digit mobile number");
		}
		
		$otp_no = $this->User_info_model->scgenrate_otp($data);
		if($otp_no > 0){
			/* Call SMS send API */
			if($type == 'register'){
				// $message = 'Thank+you+for+choosing+STYCKR.+Your+OTP+for+login+is+'.$otp_no.'.+Please+use+this+to+confirm+your+mobile+number.+Stay+Stylish!';
			file_get_contents('https://2factor.in/API/R1/?module=TRANS_SMS&apikey='.API_KEY_2FACTOR.'&to='.$uc_mobile_no.'&from='.OTP_FROM.'&templatename=mobile_verification&var1='.$otp_no.'');
			
				//$message = 'Welecome+to+StyleCracker.+Your+OTP+for+mobile+verification+is+'.$otp_no.'+Stay+Stylish!';
				//file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=91'.$uc_mobile_no.'&message='.$message);
				
			}
			else if($type == 'edit_account'){
				//$message = 'Your+OTP+for+updating+mobile+number+is+'.$otp_no.'.';
				//file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile='.$data['mobile_no'].'&message='.$message);
				
				file_get_contents('https://2factor.in/API/R1/?module=TRANS_SMS&apikey='.API_KEY_2FACTOR.'&to='.$uc_mobile_no.'&from='.OTP_FROM.'&templatename=mobile_number_updated&var1='.$otp_no.'');
				//$res_msg = 'https://2factor.in/API/R1/?module=TRANS_SMS&apikey='.API_KEY_2FACTOR.'&to='.$uc_mobile_no.'&from='.OTP_FROM.'&templatename=mobile_number_updated&var1='.$otp_no.'';
			}
			else if($type == 'order'){
				$message = 'Thank+you+for+choosing+STYCKR.+Your+OTP+for+cash+on+delivery+is+'.$otp_no.'.+Please+use+this+to+confirm+your+order.+Stay+Stylish!';
				file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile='.$data['mobile_no'].'&message='.$message);
			}else{
				$message = 'Please+enter+the+OTP+'.$otp_no.'+to+confirm+your+new+mobile+number.';
				file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile='.$data['mobile_no'].'&message='.$message);
			}
			/* end of code Call SMS send API */
			$data = new stdclass();
			$data->otp_no = $otp_no;
			//$data->msg = $res_msg;
			$this->success_response($data);
		}else{
			 $this->failed_response(1001, "Please try again");
		}
	}
	
	public function get_age_answer_get(){
		//$data['women'] = $this->Pa_model->get_question_img_answers(8);
		$data['men'] = $this->Pa_model->get_question_answers(17);
		$this->success_response($data);
	}
	
	function validate_email_get(){
		$email_id = $this->get('email_id');
		if($this->emailUnique($email_id) > 0 && $email_id != ''){
			$data['type'] = '1';
			$data['response'] = 'Email ID already exists.';
			$this->success_response($data);
		}else {
			$data['type'] = '0';
			$data['response'] = 'Email ID not exists.';
			$this->success_response($data);
		}
	}
	
	public function get_constants_get(){
		$platform = $this->get('platform');
		
		if($platform == 'android'){
			$data['force_update_version'] = '4.0.0';
			$data['current_version'] = '4.0.1';
			$data['force_update_message'] = 'Please update to new version of StyleCracker app.';
			$data['later_update_message'] = 'A newer version of StyleCracker app is available.';
		}else if($platform == 'ios'){
			$data['force_update_version'] = '4.0';
			$data['current_version'] = '4.1';
			$data['force_update_message'] = 'Please update to new version of StyleCracker app.';
			$data['later_update_message'] = 'A newer version of StyleCracker app is available.';
		}
		$data['referal_point_value'] = POINT_VAlUE;
		$data['referal_min_cart_value'] = MIN_CART_VALUE;
		$data['bms_discount'] = bms_discount;
		$data['bms_amt_limit'] = bms_amt_limit;
		$data['bms_popup_title'] = bms_popup_title;
		$data['bms_error_message'] = bms_error_message;
		$this->success_response($data);
	}
	
	public function sc_home_post(){
		
		$user_data['user_id'] = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		//$user_data['body_shape'] = @$this->Pa_model->get_users_answer_obj($user_data['user_id'],'user_body_shape')[0]['id'];
		//$user_data['pa_pref1'] = @$this->Pa_model->get_users_answer_obj($user_data['user_id'],'user_style_p1')[0]['id'];
		$user_data['gender'] = $this->post('gender');
		$user_data['type'] = '';
		$user_data['limit_cond'] = " LIMIT 0,3";
		$data['top_collections'] = $this->Collection_model->get_all_collection($user_data);
		$data['top_blogs'] = $this->Blog_model->get_blog_data($user_data);
		$data['top_events'] = $this->Event_model->get_all_event($user_data);
		$user_data['bucket_id'] = $this->post('bucket_id');
		$top_brands = $this->Brand_model->top_brands_depends_on_pa($user_data);
		$data['top_brands'] = $top_brands;
		$scbox['image'] = 'http://stg.stylecracker.com/assets/images/scbox/sc-box-cover.jpg';
		$scbox['position'] = '1';
		$scbox['text'] = 'Get your own personal SC Box';
		$id = base64_encode($user_data['user_id']);
		$scbox['redirect_link'] = 'https://www.stylecracker.com/sc-box?is_app=1&id='.$id.'&g='.$user_data['gender'].'';
		$data['scbox'] = $scbox;
		$yesbank['redirect_link'] = 'https://www.stylecracker.com/yesbank?is_app=1&id='.$id.'';
		$data['yesbank'] = $yesbank;
		// $user_data['search_by'] = "1";
		// $user_data['table_search'] = '136,135';
		// $data['top_collections2'] = $this->Collection_model->get_all_collection($user_data);
		// $data['top_collections'] = array_merge($data['top_collections2'] , $data['top_collections']);
		
		$this->success_response($data);
	}
	public function add_to_wishlist_post(){
		
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		$fav_for = $this->post('fav_for');
		$fav_id = $this->post('fav_id');
		$res = $this->Collection_model->user_wishlist($user_id,$fav_for,$fav_id);
		if($res === 1){ 
			$data = new stdClass();
			$data->message = 'success';
			$this->success_response($data);
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
		
	}
	
	public function get_all_wishlist_post(){
		$data['user_id'] = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		$data['body_shape'] = $this->post('body_shape');
		$data['pa_pref1'] = $this->post('pa_pref1');
		$data['limit'] = $this->post('limit');
		$data['offset'] = $this->post('offset');
		$fav_for = $this->post('fav_for');
		$data['limit_cond'] = " LIMIT ".$data['offset'].",".$data['limit'];
		
		$object_ids = $this->User_info_model->get_wishlisted_ids($data,$fav_for);
		
		if($fav_for == 'product' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = explode(",",$object_ids);
			$data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
			$object_data['object_data'] = $this->Mobile_look_model->get_products_list($object_ids,$data);
			
		}else if($fav_for == 'collection' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['object_data'] = $this->Collection_model->get_all_collection($data);
			
		}else if($fav_for == 'event' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['object_data'] = $this->Event_model->get_all_event($data);
			
		}else if($fav_for == 'blog' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['object_data'] = $this->Blog_model->get_blog_data($data);
			
		}else{
			
			// $this->failed_response(1001, "No Record(s) Found");
		}
		
		if(!empty($object_data) && $object_data['object_data'] != null){
			
			$this->success_response($object_data);
		}else {
			$data2['message'] = 'success';
			$this->success_response($data2);
		}
	}
	
	public function get_wishlist_ids_post(){
		$user_data['user_id'] = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		
		$user_data['limit_cond'] = " ";
		$empty_array = array();
		$product_ids = $this->User_info_model->get_wishlisted_ids($user_data,'product');
		if(!empty($product_ids)){ $data['product_ids'] = explode(',',$product_ids); }else { $data['product_ids'] = $empty_array; }
		
		$collection_ids = $this->User_info_model->get_wishlisted_ids($user_data,'collection');
		if(!empty($collection_ids)){ $data['collection_ids'] = explode(',',$collection_ids); }else { $data['collection_ids'] = $empty_array; }
		
		$event_ids = $this->User_info_model->get_wishlisted_ids($user_data,'event');
		if(!empty($event_ids)){ $data['event_ids'] = explode(',',$event_ids); }else { $data['event_ids'] = $empty_array; }
		
		$blog_ids = $this->User_info_model->get_wishlisted_ids($user_data,'blog');
		if(!empty($blog_ids)){ $data['blog_ids'] = explode(',',$blog_ids); }else { $data['blog_ids'] = $empty_array; }
		
		if($data != NULL){
			$this->success_response($data);
		}else{
			$this->success_response($empty_array);
		}	
	}
	/*
	public function top_wishlist_post(){
		$data['user_id'] = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$data['body_shape'] = $this->post('body_shape');
		$data['pa_pref1'] = $this->post('pa_pref1');
		$data['bucket'] = $this->post('bucket');
		// $data['bucket'] = 36;
		$data['limit'] = 3;
		$data['offset'] = 0;
		$data['limit_cond'] = " LIMIT 0,3 ";
		$data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
		
		$fav_for = 'product';
		$object_ids = $this->User_info_model->get_wishlisted_ids($data,$fav_for);
		// product data
		if($fav_for == 'product' && !empty($object_ids)){
			$data['search_by'] = '1';$data['table_search'] = explode(",",$object_ids);
			$object_data['product_data'] = $this->Mobile_look_model->get_products_list($object_ids,$data);
			
		}else {
			$object_ids = $this->User_info_model->recommended_products_via_attributes($data['bucket']);
			$data['search_by'] = '1'; $data['table_search'] = explode(",",$object_ids);
			if(!empty($object_ids)){ 
				$data['table_search'] = explode(",",$object_ids);
			}else{ 
				$object_ids = '6365,6552,18193';
				$data['table_search'] = explode(",",$object_ids);
			}
			$object_data['product_data'] = $this->Mobile_look_model->get_products_list($object_ids,$data);
			$object_data['product_message'] = "You haven't added any products to your wishlist, here are three we think you might like";
		}
		
		$fav_for = 'collection';
		$object_ids = $this->User_info_model->get_wishlisted_ids($data,$fav_for);
		// collection data
		if($fav_for == 'collection' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['collection_data'] = $this->Collection_model->get_all_collection($data);
			
		}else{
			$data['search_by'] = '0';
			$object_data['collection_data'] = $this->Collection_model->get_all_collection($data);
			$object_data['collection_message'] = "You haven't added any collection to your wishlist, here are three we think you might like";
		}
		
		$fav_for = 'event';
		$object_ids = $this->User_info_model->get_wishlisted_ids($data,$fav_for);
		// event data
		if($fav_for == 'event' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['event_data'] = $this->Event_model->get_all_event($data);
			
		}else{
			$data['search_by'] = '0';
			$object_data['event_data'] = $this->Event_model->get_all_event($data);
			$object_data['event_message'] = "You haven't added any event to your wishlist, here are three we think you might like";
		}
		
		$fav_for = 'blog';
		$object_ids = $this->User_info_model->get_wishlisted_ids($data,$fav_for);
		// blog data
		if($fav_for == 'blog' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['blog_data'] = $this->Blog_model->get_blog_data($data);
			
		}else{
			$data['search_by'] = '0';
			$object_data['blog_data'] = $this->Blog_model->get_blog_data($data);
			$object_data['blog_message'] = "You haven't added any blog to your wishlist, here are three we think you might like";
			
		}
		
		$this->success_response($object_data);
	}*/
	
	public function top_wishlist_post(){
		$data['user_id'] = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$data['body_shape'] = $this->post('body_shape');
		$data['pa_pref1'] = $this->post('pa_pref1');
		$data['bucket'] = $this->post('bucket');
		$data['gender'] = $this->post('gender');
		$data['limit'] = 5;
		$data['offset'] = 0;
		$data['limit_cond'] = " LIMIT 0,5 ";
		$data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
		
		$fav_for = 'product';
		$object_ids = $this->User_info_model->get_wishlisted_ids($data,$fav_for);
		// product data
		if($fav_for == 'product' && !empty($object_ids)){
			$data['search_by'] = '1';$data['table_search'] = explode(",",$object_ids);
			$object_data['product_data'] = $this->Mobile_look_model->get_products_list($object_ids,$data);
			
		}else {
			$object_data['product_data'] = array();
			$object_data['product_message1'] = "You haven't bookmarked any products";
			$object_data['product_message2'] = "Tap on a products heart icon to add it to your bookmarks";
		}
		
		$fav_for = 'collection';
		$object_ids = $this->User_info_model->get_wishlisted_ids($data,$fav_for);
		// collection data
		if($fav_for == 'collection' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['collection_data'] = $this->Collection_model->get_all_collection($data);
			
		}else{
			$data['search_by'] = '0';
			$object_data['collection_data'] = array();
			$object_data['collection_message1'] = "You haven't bookmarked any collections";
			$object_data['collection_message2'] = "Tap on a collections heart icon to add it to your bookmarks";
		}
		
		$fav_for = 'event';
		$object_ids = $this->User_info_model->get_wishlisted_ids($data,$fav_for);
		// event data
		if($fav_for == 'event' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['event_data'] = $this->Event_model->get_all_event($data);
			
		}else{
			$data['search_by'] = '0';
			$object_data['event_data'] = array();
			$object_data['event_message1'] = "You haven't bookmarked any events";
			$object_data['event_message2'] = "Tap on a events heart icon to add it to your bookmarks";
			
		}
		
		$fav_for = 'blog';
		$object_ids = $this->User_info_model->get_wishlisted_ids($data,$fav_for);
		// blog data
		if($fav_for == 'blog' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['blog_data'] = $this->Blog_model->get_blog_data($data);
			
		}else{
			$data['search_by'] = '0';
			$object_data['blog_data'] = array();
			$object_data['blog_message1'] = "You haven't bookmarked any blogs";
			$object_data['blog_message2'] = "Tap on a blogs heart icon to add it to your bookmarks";
			
		}
		$object_data['show_event'] = '0';
		$this->success_response($object_data);
	}
	
	public function update_account_details_post(){
		
		$auth_token 		= $this->input->post('auth_token');
		$user_id 			= $this->input->post('user_id');
		$gender				= $this->input->post('gender');
		$date_birth			= $this->input->post('birth_date');
		$dob 				= explode('-', $date_birth);
		$birth_date 		= $dob[2].'-'.$dob[1].'-'.$dob[0];
		$mobile_no 			= $this->input->post('mobile_no');
		$is_gender_change 	= $this->input->post('is_gender_change');
		$user_name 			= $this->input->post('user_name');
		
		if(trim($user_id) == ''){ $this->failed_response(1006, "Please check user id"); }
		else if(trim($auth_token) 	== ''){$this->failed_response(1006, "Please check auth token");}
		else if(trim($gender) 		== ''){$this->failed_response(1006, "Please check gender");}
		else if(trim($date_birth) 	== ''){$this->failed_response(1006, "Please check birth date");}
		else if(trim($mobile_no) 	== ''){$this->failed_response(1006, "Please check mobile number");}
		else if(trim($user_name) 	== ''){$this->failed_response(1006, "Please Enter full name");}
		
		$name =  explode(' ',$user_name);
		$count = count($name);
		if(!empty($name) && $count >= 2){
			$first_name = $name[0];
			$last_name 	= $name[1].' '.@$name[2];
		}else {
			
			$this->failed_response(1010, "Please Enter full name.");			
		}
		
		if($gender == 'male' || $gender == 'men'){ 
			$gender 		= 2; 
			$question_id 	= 15;
		}
		else if($gender == 'female' || $gender == 'women'){ 
			$gender 		= 1;
			$question_id 	= 8;
		}
		
		$userLoginUpdate = array();
		$userLoginUpdate['user_name'] 			= $first_name.''.$last_name;
		$userLoginUpdate['modified_datetime'] 	= date('Y-m-d H:i:s');
		$update_user_info = $this->User_info_model->update_username($userLoginUpdate, $user_id);
		//echo $this->db->last_query();
		
		$userInfoUpdate = array();
		$userInfoUpdate['first_name'] 	= $first_name;
		$userInfoUpdate['last_name'] 	= $last_name;
		$userInfoUpdate['gender'] 		= $gender;
		$userInfoUpdate['contact_no'] 	= $mobile_no;
		$userInfoUpdate['birth_date'] 	= $birth_date;
		$update_user_info = $this->User_info_model->update_user_info($userInfoUpdate, $user_id);
		//echo $this->db->last_query();
		
		$userObjectMeta = array();
		$userObjectMeta[0]['object_meta_key']	= 'user_name';
		$userObjectMeta[0]['object_meta_value']	= $user_name;
		$userObjectMeta[1]['object_meta_key']	= 'user_gender';
		$userObjectMeta[1]['object_meta_value']	= $gender;
		$userObjectMeta[2]['object_meta_key']	= 'user_dob';
		$userObjectMeta[2]['object_meta_value']	= $birth_date;
		$userObjectMeta[3]['object_meta_key']	= 'user_mobile';
		$userObjectMeta[3]['object_meta_value']	= $mobile_no;
		
		if(is_array($userObjectMeta) && count($userObjectMeta)>0)
		{
			foreach($userObjectMeta as $key=>$objectValue){
              $objectValue['modified_datetime'] 	= date('Y-m-d H:i:s');
			  $update_user_object = $this->User_info_model->update_user_object($objectValue, $user_id);
			  //echo $this->db->last_query();
            }
		}
		//$result 	= $this->User_info_model->update_account_details($user_data);
		//echo $this->db->last_query(); exit();
		$data 				= new stdClass();
		$data->user_info	= $this->getuserinfo_get($user_id);
		$gender 			= $this->Pa_model->get_gender($user_id);
		//$data->pa			= $this->pa_get($user_data['user_id'],$gender);
		$this->success_response($data);
	}
	
	public function get_all_notification_post()
	{
		$user_data['user_id'] 	= $this->post('user_id');
		$auth_token 			= $this->post('auth_token');
		
		$data = $this->User_info_model->get_all_notification($user_data);
		//echo $this->db->last_query();
		//exit();
		if(empty($data)){
			$data2 = array();
			$this->success_response($data2);
		}else {
			$this->success_response($data);
		}
		
	}
	
	public function update_notification_post(){
		$user_data['user_id'] 					= $this->input->post('user_id');
		$auth_token 							= $this->input->post('auth_token');
		$user_data['notification_id'] 			= $this->input->post('notification_id');
		$user_data['order_notification_read'] 	= $this->input->post('order_notification_read');
		$data = $this->User_info_model->update_notification($user_data);
		$data2['success'] = array();
		$this->success_response($data2);
		
	}
	
	public function home_page_brands_get(){
		$user_data['user_id'] 		= $this->get('user_id');
		$user_data['bucket_id'] 	= $this->get('bucket_id');
		$user_data['limit'] 		= $this->get('limit');
		$user_data['offset'] 		= $this->get('offset');
		$user_data['limit_cond'] 	= ' LIMIT '.$user_data['offset'].','.$user_data['limit'].' ';
		$top_brands = $this->Brand_model->top_brands_depends_on_pa($user_data);
		$this->success_response($top_brands);
	}

	public function send_otp_get($mobile_no,$TemplateName,$variable){
		$YourAPIKey = API_KEY_2FACTOR;
		$From = OTP_FROM;
		$To=$mobile_no;
		$TemplateName=$TemplateName;
		$VAR1=$variable;



		### DO NOT Change anything below this line
		$agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
		$url = "https://2factor.in/API/V1/$YourAPIKey/ADDON_SERVICES/SEND/TSMS"; 
		$ch = curl_init(); 
		curl_setopt($ch,CURLOPT_URL,$url); 
		curl_setopt($ch,CURLOPT_POST,true); 
		curl_setopt($ch,CURLOPT_POSTFIELDS,"TemplateName=$TemplateName&From=$From&To=$To&VAR1=$VAR1"); 
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		curl_exec($ch); 
		curl_close($ch);
	}
	
	public function test_api_get(){
		$email = 'rajesh@stylecracker.com';
		echo $this->send_welcome_email($email);
		echo "comming";
	}

	public function web_urls_post(){
		
		$data = new stdClass();
		$user_data = array();
		$user_data['user_id'] = $this->post('user_id');
		$auth_token = $this->post('auth_token');		
		$user_data['gender'] = $this->post('gender');		
		$id = base64_encode($user_data['user_id']);
		$scbox['redirect_link'] = 'https://www.stylecracker.com/sc-box?is_app=1&id='.$id.'&g='.$user_data['gender'].'';
		$data->scbox = $scbox;		
		
		$this->success_response($data);
	}
	
public function fb_photo_access_post()
	{
		$user_id 			= $this->input->post('user_id');
		$auth_token 		= $this->input->post('auth_token');
		$fb_access_token 	= $this->input->post('fb_access_token');
		
		if(trim($user_id) == ''){ $this->failed_response(1006, "Please check user id"); }
		else if(trim($auth_token) 	== ''){$this->failed_response(1006, "Please check auth token");}
		else if(trim($fb_access_token) 		== ''){$this->failed_response(1006, "Please check fb access token");}
		$fbAccessData = 'https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=1650358255198436&client_secret=403b4cbf601b9a67b6594c7be781e8b9&fb_exchange_token='.$fb_access_token;
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $fbAccessData,
			CURLOPT_USERAGENT => 'Codular Sample cURL Request'
		));
		$resp = curl_exec($curl);
		curl_close($curl);
		$fbData = serialize(json_decode($resp, true));
		$data = new stdclass();
		$data->result  = $this->User_info_model->saveupdate_fb_token($fbData,$user_id);
		$data->message = 'thank you for giving access to your facebook photos.'; 
		$this->success_response($data);
	}
	
}