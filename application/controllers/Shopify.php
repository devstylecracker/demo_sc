<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';

class Shopify extends TT_REST_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Schome_model');		
		$this->load->model('Shopify_model');
		$this->open_methods = array('shopify_name_get','shopify_view_product_get');    
	}	



	public function shopify_name_get(){	
		$referring_site = $this->get('referring_site');
		$landing_site = $this->get('landing_site');
		/*echo $landing_site;
		exit;*/
		$sc_product_id = '0';
		if(trim($landing_site) != '' && (strpos($landing_site,'sc_productid') !== false)) {	
		//if(trim($landing_site) != '' ){		
			$landing = trim($this->get('landing_site'));			
	    		$getproduct_id = explode('=' , $landing);
	    		$getproduct_id = explode('sc_productid=' , $landing);	    			    			    		
	    	  	$sc_product_id = $getproduct_id[1];	    	  	
	    	  	$sc_user_id = '0';    	 	    	  	 		    	  		    	  	
			//$countArray = count($getID);
			//$sc_product_id = $getID[$countArray-1];
			//$sc_user_id = $getID[$countArray-2];
			//$sc_product_id = $this->get('sc_product_id');
		}else{
			$sc_product_id = '0';
			$sc_user_id = '0';
		}
		//if($this->session->userdata('user_id')) $user_id = $this->session->userdata('user_id'); else $user_id = 0;
		//$landing_site = $this->get('landing_site');
		//$landing_site = 'http://www.stylecracker.com/Brands/redirect/6959';

		$brand_name = $this->get('brand_name');
		$sc_currency = $this->get('sc_currency');		
		$sc_product_price = $this->get('sc_product_price');	
		$order_id = $this->get('order_id');
		$payment_check_out_id = $this->get('payment_check_out_id');		
		$email = $this->get('email');		
		$landing_site = $this->get('landing_site');		
		$subtotal_price = $this->get('subtotal_price');		
		$total_price_p = $this->get('total_price');
		$name = $this->get('name');		
		$phone = $this->get('phone');
		if($sc_product_id != '0'){		
		$sc_product_id = $this->encryptOrDecrypt($sc_product_id ,'');							
	  $res = $this->Shopify_model->get_shop_name($sc_user_id,$sc_product_id ,$brand_name,$sc_currency,$sc_product_price,$order_id,$payment_check_out_id,$email,$landing_site,$referring_site,$subtotal_price,$total_price_p,$name,$phone);	
			if($res == 1){
				$data = new stdClass();
				$data->message = 'success';
				$this->success_response($data);
			}else{
				$this->failed_response(1001, "API / Database Call Failed");
			} 
	    	}else{
	    	  echo "you are out";
	    	  exit;
	    	  }
	    	}		
		//$sc_product_id_array = explode('/',$referring_site);
		//$sc_product_id =  substr($referring_site, -4);
		//$referring_site = ' http://www.stylecracker.com/Brands/redirect/7035';
		 
                /*if (strpos($referring_site,'stylecracker') !== false) {
			$res = $this->Shopify_model->get_shop_name($sc_user_id,$sc_product_id,$brand_name,$sc_currency,$sc_product_price,$order_id,$payment_check_out_id,$email,$landing_site,$referring_site,$subtotal_price,$total_price_p,$name,$phone);		
			if($res === 1){
				$data = new stdClass();
				$data->message = 'success';
				$this->success_response($data);
			}else{
				$this->failed_response(1001, "API / Database Call Failed");
			} 
                 }else echo "not coming";
						
	}*/

	function encryptOrDecrypt($mprhase, $crypt) {
	     $MASTERKEY = "STYLECRACKERAPI";
	     $td = mcrypt_module_open('tripledes', '', 'ecb', '');
	     $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
	     mcrypt_generic_init($td, $MASTERKEY, $iv);
	     if ($crypt == 'encrypt')
	     {
	         $return_value = base64_encode(mcrypt_generic($td, $mprhase));
	     }
	     else
	     {
	         $return_value = mdecrypt_generic($td, base64_decode($mprhase));
	     }
	     mcrypt_generic_deinit($td);
	     mcrypt_module_close($td);
	     return $return_value;
	} 


 }