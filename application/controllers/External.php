<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';
class External extends TT_REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
       parent::__construct();
       $this->load->model('Schome_model'); 
       $this->load->model('Pa_model'); 
       $this->load->model('External_model'); 
      // $this->load->library('upload');
   	} 
   
	public function index()
	{
		
	}


	public function download_product_json_get(){
		$limit= $this->get('limit');
		$offset = $this->get('offset');
		$timestamp = urldecode($this->get('timestamp'));
		if($offset =='') $offset = 0;
		if($timestamp == '') $timestamp = '';
		$products = $this->External_model->download_product_json($limit,$offset,$timestamp);  
		$response['product_desc'] = $products;
		 if($response != NULL || $response != ''){
			  
			$this->success_response($response);
		}
		else{
			$response = array();	
			$this->success_response($response);
	
	  }
		//$this->load->view('external',$response);
	}
	
	public function download_looks_json_get(){
		$limit= $this->get('limit');
        $offset = $this->get('offset');
		$timestamp = urldecode($this->get('timestamp'));
		if($offset =='') $offset = 0;
		if($timestamp == '') $timestamp = '';
		$looks = $this->External_model->download_looks_json($limit,$offset,$timestamp);  
		$response['look_desc'] = $looks;
		if($response != NULL || $response != ''){
			  
			$this->success_response($response);
		}
		else{
			$response = array();	
			$this->success_response($response);
	
	  }
		//echo "<pre>";print_r($response);exit;
		//$this->load->view('external',$response);
	}

}