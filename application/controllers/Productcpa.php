<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';

class Productcpa extends TT_REST_Controller 
{
	function __construct()
	{
		parent::__construct();	
		$this->load->model('Shopify_model');
		//$this->open_methods = array('product_cpa_get');    
	}	
	public function product_cpa_get(){		
		//if(($this->get('brand_name')) != 'labellife'){
			$referring_site = $this->get('referring_site');	        
			if($referring_site != '' && (strpos($referring_site ,'stylecracker.com'))) {                	
	         	$referring_site = trim($this->get('referring_site'));//referring_site                 	
	         	/*echo $referring_site;
	        		exit;*/
	    			$getID = explode( "_", $referring_site);	    			
	    		if(is_array($getID) && (count($getID) > 1)){	    			
				$countArray = count($getID);
				$sc_product_id = $getID[$countArray-1];
				$sc_user_id = $getID[$countArray-2];
				}else{
					$sc_product_id = 0;
					$sc_user_id = 0;
				}
			//$sc_product_id = $this->get('sc_product_id');
			}else{
				$sc_product_id = 0;
				$sc_user_id = 0;
			}
			$sc_product_id = trim($this->get('product_id'));					
			$brand_name = trim($this->get('brand_name'));
			$sc_currency = trim($this->get('sc_currency'));		
			$sc_product_price = trim($this->get('sc_product_price'));		
			$order_id = trim($this->get('order_id'));
			$payment_check_out_id = trim($this->get('payment_check_out_id'));		
			$email = trim($this->get('email'));		
			$landing_site = trim($this->get('landing_site'));
			//$referring_site = trim($this->get('referring_site'));		
			$subtotal_price = trim($this->get('subtotal_price'));		
			$total_price_p = trim($this->get('total_price'));
			$name = trim($this->get('name'));		
			$platform = trim($this->get('platform'));
			//if(isset($this->get('platform')) $platform = trim($this->get('platform'));	else $platform = '';
			$phone = trim($this->get('phone'));	
			$encode_true = trim($this->get('encode_true'));
			if($encode_true == 1 || $platform == "magento"){
				$sc_product_id = $this->encryptOrDecrypt($sc_product_id,'');			
			}
			if($platform == "zepo"){
				$digits = 5;
				$order_id = rand(pow(10, $digits-1), pow(10, $digits)-1);				
			}
			//$sc_product_id = $product_id;
			//$sc_product_id =  substr($referring_site, -4);
			$res = $this->Shopify_model->get_shop_name($sc_user_id,$sc_product_id,$brand_name,$sc_currency,$sc_product_price,$order_id,$payment_check_out_id,$email,$landing_site,$referring_site,$subtotal_price,$total_price_p,$name,$phone);							
			if($res == 1){
				$data = new stdClass();
				$data->message = 'success';
				$this->success_response($data);
			}else{
				//echo "asdfasdfsadf";
				//exit;
				$this->failed_response(1001, "API / Database Call Failed");
			}
							
		//}
	}
	
	function encryptOrDecrypt($mprhase, $crypt) {
	     $MASTERKEY = "STYLECRACKERAPI";
	     $td = mcrypt_module_open('tripledes', '', 'ecb', '');
	     $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
	     mcrypt_generic_init($td, $MASTERKEY, $iv);
	     if ($crypt == 'encrypt')
	     {
	         $return_value = base64_encode(mcrypt_generic($td, $mprhase));
	     }
	     else
	     {
	         $return_value = mdecrypt_generic($td, base64_decode($mprhase));
	     }
	     mcrypt_generic_deinit($td);
	     mcrypt_module_close($td);
	     return $return_value;
	} 
	
 }