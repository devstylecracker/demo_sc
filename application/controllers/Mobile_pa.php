<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
class Mobile_pa extends TT_REST_Controller 
{
  function __construct()
	{
		parent::__construct();
		$this->load->library('bitly');
		$this->load->library('email');
		$this->load->library('user_agent');
		$this->load->library('curl');
		$this->load->model('Pa_model');	
		$this->load->model('user_info_model');	
		$this->load->model('Mcart_model_android');		
		$this->open_methods = array('register_user_post','scgenrate_otp_post','save_user_info_post','update_order_data_post');    
	}
   
	public function sc_box_home_get()
	{
		$data = new stdClass();
		
		$data->banner['title'] 			= '<b>A BOX FULL OF FASHION.</b><br /> <b>THAT YOU ARE GUARANTEED TO LOVE.</b><br />PUT TOGETHER BY OUR CELEBRITY STYLISTS.<br /> DELIVERED STRAIGHT TO YOUR DOORSTEP.';
		$data->banner['subtitle'] 		= 'SIMPLE. STYLISH. STRESS FREE.';
		$data->banner['button_text'] 		= 'Order Now';
		$data->banner['discount'] 		= 'Upto 30% off on your first box.';
		$data->banner['image'] 			= 'https://www.stylecracker.com/assets/images/scbox/box-banner.jpg';
		
		$data->curated_for_you['youtube_video'] 			= 'https://www.youtube.com/embed/VPrI2rk7FU4';
		$data->curated_for_you['button_text'] 				= 'Get Started';
		$data->curated_for_you['steps'][0]['title'] 		= 'Your Style';
		$data->curated_for_you['steps'][0]['desc'] 		= ' One size does not fit all and we get that. At StyleCracker we curate fashion for you keeping in mind your personality, likes, dislikes, body shape, <b>budget</b> and other important details. Yes, we'."'".'ve got you covered every step of the way.';
		$data->curated_for_you['steps'][1]['title']		= 'Your Budget';
		$data->curated_for_you['steps'][1]['desc']		= ' StyleCracker is for everyone! Once we'."'".'ve understood your <b>style</b>, our stylists will curate a box for you within your budget. While we do have pre-set amounts and quantities to get you started on your selection, you could also customize your order.';
		$data->curated_for_you['steps'][2]['title']	= 'Your Convenience';
		$data->curated_for_you['steps'][2]['desc']	= 'StyleCracker is designed to make life easier and more convenient for you. All you need to do is fill out a form, speak to our stylists and get styled. It'."'".'s that simple! Leave the fashion to the experts and enjoy the stress free shopping experience!';
		

		//	HOW IT WORKS	
		$data->how_it_works['title'] 		= 'How it Works';
		
		$data->how_it_works['steps'][0]['sub_title'] 	= 'We get to know you';
		$data->how_it_works['steps'][0]['image'] 		= 'https://www.stylecracker.com/assets/images/scbox/how-it-works-1.png';
		$data->how_it_works['steps'][0]['desc'] 		= 'To get started, you'."'".'ll need to fill out a simple online form. This helps us decode your style preferences. Next, a StyleCracker celebrity stylist will call you to understand your requirements. Once this is done, the stylist will get to work to curate your box.';
		
		$data->how_it_works['steps'][1]['sub_title'] 	= 'We send you the box';
		$data->how_it_works['steps'][1]['image'] 		= 'https://www.stylecracker.com/assets/images/scbox/how-it-works-2.png';
		$data->how_it_works['steps'][1]['desc'] 		= 'Once our stylist has curated the box keeping in mind your likes, dislikes, body shape and budget we put it all together and ship you, your box of style. Shipping is absolutely free and it gets delivered straight to your doorstep.';
		
		$data->how_it_works['steps'][2]['sub_title'] 	= 'Keep only what you like';
		$data->how_it_works['steps'][2]['image'] 		= 'https://www.stylecracker.com/assets/images/scbox/how-it-works-3.png';
		$data->how_it_works['steps'][2]['desc']  		= 'When you receive the box, check out the selection we'."'".'ve sent you. Try it on for size and to see how it works for you. Keep everything you like. If there'."'".'s something that doesn'."'".'t suit you, send it back to us. No questions asked.';
		
		$data->how_it_works['steps'][3]['sub_title'] 	= 'Give us your feedback';
		$data->how_it_works['steps'][3]['image'] 		= 'https://www.stylecracker.com/assets/images/scbox/how-it-works-4.png';
		$data->how_it_works['steps'][3]['desc'] 		= 'Our goal is to ensure that you, and every other <b>user</b>, loves every item curated personally for you. Your constant feedback is most valuable to make sure we get even better. So please take a couple of minutes to tell us what you liked and what you didn'."'".'t.';
		$data->how_it_works['button_text'] 			= 'Order Now';
		
		//TESTIMONIALS
		$data->testimonial['title'] 			= 'TESTIMONIALS';
		$data->testimonial['desc'] 				= 'We asked a few of our customers about their first experience with the StyleCracker Box';
		$data->testimonial['video_link'] 		= 'https://www.youtube.com/embed/6k5JAHuM_TY';
		$data->testimonial['button_text'] 		= 'Order Now';
		
		$data->testimonial['customers'][0]['name'] 				= 'Priyanka Talreja';
		$data->testimonial['customers'][0]['image'] 			= 'https://www.stylecracker.com/assets/images/home/banner/priyanka1.jpg';
		$data->testimonial['customers'][0]['desc'] 				= 'I received my surprise box of goodies from StyleCracker and it just couldn'."'".'t get better! In this day and age of online shopping, a box of goodies suited to my style and preference was something very outstanding. Not to mention how flattering it is to have a box personalized to my taste and style. Way to go StyleCracker!';
		$data->testimonial['customers'][1]['name'] 				= 'Glenn Gonsalves';
		$data->testimonial['customers'][1]['image'] 			= 'https://www.stylecracker.com/assets/images/home/banner/glen.jpg';
		$data->testimonial['customers'][1]['desc'] 				= 'Since I was looking for a personal stylist for myself, having one who'."'".'s worked with A-listers and the biggest names in the movie business made StyleCracker my absolute choice! From being style guides to helping me with even '."'".'what not to wear'."'".',
              they'."'".'ve been extremely helpful in solving all my fashion queries.';
		
		//OUR COMMITMENT
		
		$data->our_commitment['title'] 			= 'OUR COMMITMENT';
		
		$data->our_commitment['steps'][0]['sub_title'] 		= 'It'."'".'s All About You';
		$data->our_commitment['steps'][0]['image'] 			= 'https://www.stylecracker.com/assets/images/scbox/our-comm-1.png';
		$data->our_commitment['steps'][0]['desc'] 			= 'The StyleCracker box is about you. While our stylists put together and curate each item in the box, it'."'".'s always done keeping your style in mind.';
		
		$data->our_commitment['steps'][1]['sub_title'] 		= 'Experienced Stylists';
		$data->our_commitment['steps'][1]['image'] 			= 'https://www.stylecracker.com/assets/images/scbox/our-comm-2.png';
		$data->our_commitment['steps'][1]['desc'] 			= ' All stylists part of the StyleCracker family come with years of experience. From styling celebrities, editorials and films to individuals for weddings and day-to-day requirements, we do it all. So worry not! You are in great hands.';
		
		$data->our_commitment['steps'][2]['sub_title'] 		= 'Transparent Pricing';
		$data->our_commitment['steps'][2]['image'] 			= 'https://www.stylecracker.com/assets/images/scbox/our-comm-3.png';
		$data->our_commitment['steps'][2]['desc'] 			= 'Every product you get comes with the official label and price tag, so you'."'".'ll get what you paid for. More often than not, you'."'".'ll get stuff worth a lot more.';
		
		$data->our_commitment['steps'][3]['sub_title'] 		= 'Returns, Replacements and Refunds';
		$data->our_commitment['steps'][3]['image'] 			= 'https://www.stylecracker.com/assets/images/scbox/our-comm-4.png';
		$data->our_commitment['steps'][3]['desc'] 			= ' If you don'."'".'t like something we send in the StyleCracker box, just <b>return it</b>. No questions asked.';
		$data->our_commitment['button_text'] 			= 'Order Now';
		
		//Pricing
		$data->package['title']				= 'Pricing';
		$data->package['pricing_title']		= 'Upto 30% off on your first order.';
		//$scbox_pack = unserialize(SCBOX_PACKAGE);
		$taxonomy_id_female	= 1;
		//$scbox_pack_female 	= $this->frontend_model->scxObject($taxonomy_id_female);
		
			$data->package['women'][0]['object_id']	= "1";
			$data->package['women'][0]['name']			= "3 Items";
			$data->package['women'][0]['price']		= "2999";
			$data->package['women'][0]['packname']		= "3 Items";
			//$data->package['women'][0]['desc']		= $val->desc;
			$data->package['women'][0]['image']		= "https://www.stylecracker.com/assets/images/scbox/women/package1.jpg";

			$data->package['women'][1]['object_id']	= "2";
			$data->package['women'][1]['name']			= "4 Items";
			$data->package['women'][1]['price']		= "4999";
			$data->package['women'][1]['packname']		= "4 Items";
			//$data->package['women'][1]['desc']		= $val->desc;
			$data->package['women'][1]['image']		= "https://www.stylecracker.com/assets/images/scbox/women/package2.jpg";

			$data->package['women'][2]['object_id']	= "3";
			$data->package['women'][2]['name']			= "5 Items";
			$data->package['women'][2]['price']		= "6999";
			$data->package['women'][2]['packname']		= "5 Items";
			//$data->package['women'][1]['desc']		= $val->desc;
			$data->package['women'][2]['image']		= "https://www.stylecracker.com/assets/images/scbox/women/package3.jpg";

			$data->package['women'][3]['object_id']	= "4";
			$data->package['women'][3]['name']			= "Unlimited items";
			$data->package['women'][3]['price']		= "8000";
			$data->package['women'][3]['packname']		= "Unlimited items";
			//$data->package['women'][3]['desc']		= $val->desc;
			$data->package['women'][3]['image']		= "https://www.stylecracker.com/assets/images/scbox/women/package4.jpg";
			
		$taxonomy_id_male	= 2;
		//$scbox_pack_male 	= $this->frontend_model->scxObject($taxonomy_id_male);
		
			$data->package['men'][0]['object_id']	= "1";
			$data->package['men'][0]['name']			= "3 Items";
			$data->package['men'][0]['price']		= "2999";
			$data->package['men'][0]['packname']		= "3 Items";
			//$data->package['men'][0]['desc']		= $val->desc;
			$data->package['men'][0]['image']		= "https://www.stylecracker.com/assets/images/scbox/men/package1.jpg";

			$data->package['men'][1]['object_id']	= "2";
			$data->package['men'][1]['name']			= "4 Items";
			$data->package['men'][1]['price']		= "4999";
			$data->package['men'][1]['packname']		= "4 Items";
			//$data->package['men'][1]['desc']		= $val->desc;
			$data->package['men'][1]['image']		= "https://www.stylecracker.com/assets/images/scbox/men/package2.jpg";

			$data->package['men'][2]['object_id']	= "3";
			$data->package['men'][2]['name']			= "5 Items";
			$data->package['men'][2]['price']		= "6999";
			$data->package['men'][2]['packname']		= "5 Items";
			//$data->package['men'][2]['desc']		= $val->desc;
			$data->package['men'][2]['image']		= "https://www.stylecracker.com/assets/images/scbox/men/package3.jpg";

			$data->package['men'][3]['object_id']	= "4";
			$data->package['men'][3]['name']			= "Unlimited items";
			$data->package['men'][3]['price']		= "8000";
			$data->package['men'][3]['packname']		= "Unlimited items";
			//$data->package['men'][3]['desc']		= $val->desc;
			$data->package['men'][3]['image']		= "https://www.stylecracker.com/assets/images/scbox/men/package4.jpg";

		$data->package['button_text'] 			= 'Order Now';
		$this->success_response($data);
	}
	
	public function save_user_info_post()
	{
		$user_id			=	$this->input->post('user_id');
		$auth_token			=	$this->input->post('auth_token');
		$for				=	$this->input->post('for');
		$gender				=	$this->input->post('gender');
		$full_name			=	$this->input->post('full_name');
		$email				=	$this->input->post('email');
		$mobile				=	$this->input->post('mobile');
		$pincode			=	$this->input->post('pincode');
		$city				=	$this->input->post('city');
		$state				=	$this->input->post('state');
		$profession			=	$this->input->post('profession');
		$shipping_address   =	$this->input->post('address');
		//$product_id   		=	$this->input->post('product_id');
		$price   			=	$this->input->post('price');
		$dob   				=	$this->input->post('dob');
		$platform_name   	=	$this->input->post('platform_name');
		$objectid   		=	$this->input->post('object_id');
		
		$mdob = explode('-', $dob);
		$scusername 		= preg_replace('/\s+/', ' ', trim($full_name));
		$username = explode(' ',trim($scusername));       
        $json_array['firstname'] = ucwords(strtolower($username[0]));
        $json_array['lastname'] = ucwords(strtolower($username[1]));
		
		$giftId 	= '';
		$cartId 	= '';
		$addressId 	= '';
		$shippingId = '';
		if(trim($for) == ''){ $this->failed_response(1006, "Please check for me / this is gift"); }
		else if(trim($pincode) 			== ''){$this->failed_response(1006, "Please select pincode");}
		else if(trim($email) 			== ''){$this->failed_response(1006, "Please enter email id");}
		else if(trim($json_array['firstname']) 	== ''){$this->failed_response(1006, "Please select first name");}
		else if(trim($json_array['lastname']) 	== ''){$this->failed_response(1006, "Please enter last name");}
		else if(trim($city) 			== ''){$this->failed_response(1006, "Please select city");}
		else if(trim($state) 			== ''){$this->failed_response(1006, "Please select state");}
		else if(trim($profession) 		== ''){$this->failed_response(1006, "Please select profession");}
		else if(trim($shipping_address) == ''){$this->failed_response(1006, "Please Enter shipping address");}
		//else if(trim($product_id) 		== ''){$this->failed_response(1006, "Please Enter Product");}
		else if(trim($price) 			== ''){$this->failed_response(1006, "Please Enter price");}
		else if(trim($objectid) 		== ''){$this->failed_response(1006, "Please Enter Object id");}
		else if(trim($platform_name) 	== ''){$this->failed_response(1006, "Please Enter platform info");}
		//else if($username[0] 			== ''){$this->failed_response(1006, "Please Enter first name");}
		//else if($username[1] 			== ''){$this->failed_response(1006, "Please Enter last name");}
		
		
		$giftObjectId = $this->Mcart_model_android->get_object('scbox_gift');
		
		$product_id = $this->Pa_model->get_scboxdata($objectid,'id')->productid;
		$birthDate = $mdob[2].'-'.$mdob[1].'-'.$mdob[0];
		$user_add  = $this->user_info_model->update_birth_date($birthDate, $user_id); 
		$stateid   = $this->user_info_model->get_state_name(trim($state));
		if(empty($stateid))
		{
			$this->failed_response(1006, "Please Enter Valid State Name");
		}
		
		$objectIddb = $this->user_info_model->get_object_id('scbox_gift');
		
		$scboxSize = unserialize(SCBOX_SIZE);
		$cartInfo = array();
		$cartInfo['product_id']			= $product_id;
		$cartInfo['user_id']			= $user_id;
		$cartInfo['created_datetime']	= date('Y-m-d H:i:s');
		$cartInfo['product_size'] 		= $scboxSize[0]->id;
		$cartInfo['product_qty']		= '1';
		$cartInfo['product_price']		= $price;
		$cartInfo['platform_info']		= $platform_name;
		$cartId = $this->user_info_model->add_cart_info($cartInfo, $user_id);
		/*  echo $this->db->last_query();
		echo $cartId;
		exit();  */
		
		
		$userAddress = array();
		$userAddress['user_id'] 			= $user_id;
		$userAddress['first_name'] 			= $username[0];
		$userAddress['last_name'] 			= $username[1];
		$userAddress['email'] 				= $email;
		$userAddress['mobile_no'] 			= $mobile;
		$userAddress['shipping_address'] 	= $shipping_address;
		$userAddress['pincode'] 			= $pincode;
		$userAddress['city_name'] 			= $city;
		$userAddress['state_name'] 			= $stateid['id'];
		$userAddress['created_datetime'] 	= date('Y-m-d H:i:s');
		$userAddress['address_type'] 		= ($for == 2)?'B':'S';
		$userAddress['is_default'] 			= 0;
		$userAddress['cart_id'] 			= $cartId;
		$addressId.= $this->user_info_model->add_user_address($userAddress, $cartId, $user_id,$userAddress['address_type']);
		//echo $this->db->last_query();
		
		
          $arr_answers[1]['question'] 	=  'user_name';
          $arr_answers[1]['answer_id'] 	=  $full_name;
          $arr_answers[2]['question'] 	=  'user_gender';
          $arr_answers[2]['answer_id'] 	=  $gender;
          $arr_answers[3]['question'] 	=  'user_emailid';
          $arr_answers[3]['answer_id'] 	=  $email;
          $arr_answers[4]['question'] 	=  'user_dob';
          $arr_answers[4]['answer_id'] 	=  $dob;
          $arr_answers[5]['question'] 	=  'user_mobile';
          $arr_answers[5]['answer_id'] 	=  $mobile;
          $arr_answers[6]['question'] 	=  'user_shipaddress';
          $arr_answers[6]['answer_id'] 	=  $shipping_address;
          $arr_answers[7]['question'] 	=  'user_pincode';
          $arr_answers[7]['answer_id'] 	=  $pincode;
          $arr_answers[8]['question'] 	=  'user_city';
          $arr_answers[8]['answer_id'] 	=  $city;
          $arr_answers[9]['question'] 	=  'user_state';
          $arr_answers[9]['answer_id'] 	=  $stateid['id'];
          $arr_answers[10]['question'] 	=  'user_id';
          $arr_answers[10]['answer_id'] =  $user_id;
          $arr_answers[11]['question'] 	=  'scbox_pack';
          $arr_answers[11]['answer_id'] =  '';
          $arr_answers[12]['question'] 	=  'scbox_price';
          $arr_answers[12]['answer_id'] =  $price;
          $arr_answers[13]['question'] 	=  'scbox_productid';
          $arr_answers[13]['answer_id'] =  $product_id;
          $arr_answers[14]['question'] 	=  'scbox_objectid';
          $arr_answers[14]['answer_id'] =  $objectid;
          $arr_answers[15]['question'] 	=  'user_profession';
          $arr_answers[15]['answer_id'] =  $profession;
		
			foreach($arr_answers as $key=>$json){
              $question 	= $json['question'];
              $question_id 	= @$data_answers[$question];
              $answer_id 	= $json['answer_id'];
              $this->Pa_model->save_scboxuser_answer($question_id,$answer_id,$user_id,$objectid,$question);
			  
            }
			
		// for gift
		if($for == '2')
		{	
			$friend_gender			=	$this->input->post('friend_gender');
			$friend_full_name		=	$this->input->post('friend_full_name');
			$friend_day				=	$this->input->post('friend_day');
			$friend_month			=	$this->input->post('friend_month');
			$friend_year			=	$this->input->post('friend_year');
			$friend_email			=	$this->input->post('friend_email');
			$friend_mobile			=	$this->input->post('friend_mobile');
			$friend_pincode			=	$this->input->post('friend_pincode');
			$friend_city			=	$this->input->post('friend_city');
			$friend_state			=	$this->input->post('friend_state');
			$friend_profession		=	$this->input->post('friend_profession');
			$friend_billing_address =	$this->input->post('friend_address');
			$friend_dob   			=	$this->input->post('friend_dob');
			$stylist_call_receiver  =	$this->input->post('stylist_call_receiver');
			
			$fdob = explode('-', $friend_dob);
			$friend_scusername 		= preg_replace('/\s+/', ' ', trim($friend_full_name));
			$friend_username = explode(' ',trim($friend_scusername));       
			$friend_json_array['firstname'] = ucwords(strtolower($friend_username[0]));
			$friend_json_array['lastname'] = ucwords(strtolower($friend_username[1]));
			
			if(trim($friend_gender) == ''){$this->failed_response(1006, "Please select your friend gender");}
			else if(trim($friend_full_name)	== ''){$this->failed_response(1006, "Please enter your friend full name");}
			else if(trim($dob) 		== ''){$this->failed_response(1006, "Please enter  date of birth");}
			/*else if(trim($friend_month) 	== ''){$this->failed_response(1006, "Please enter month");}
			else if(trim($friend_year) 		== ''){$this->failed_response(1006, "Please enter year");}*/
			else if(trim($friend_email) 	== ''){$this->failed_response(1006, "Please enter your friend email");}
			else if(trim($friend_mobile) 	== ''){$this->failed_response(1006, "Please enter your friend mobile");}
			else if(trim($friend_pincode) 	== ''){$this->failed_response(1006, "Please enter your friend pincode");}
			else if(trim($friend_city) 		== ''){$this->failed_response(1006, "Please select your friend city");}
			else if(trim($friend_state) 	== ''){$this->failed_response(1006, "Please select your friend state");}
			else if(trim($friend_profession) == ''){$this->failed_response(1006, "Please enter your friend profession");}
			else if(trim($friend_billing_address) == ''){$this->failed_response(1006, "Please enter billing address");}
			
			$gifter_details								= array();
			$gifter_details['gender'] 					= $friend_gender;
            $gifter_details['name'] 					= $friend_full_name;
            $gifter_details['email'] 					= $friend_email;
            $gifter_details['mobileno']					= $friend_mobile;
            $gifter_details['pincode'] 					= $friend_pincode;
            $gifter_details['city'] 					= $friend_city;
            $gifter_details['state'] 					= $friend_state;
            $gifter_details['address'] 					= $friend_billing_address;
            $gifter_details['profession'] 				= $friend_profession; 
            $gifter_details['gift_msg'] 				= ''; 
            $gifter_details['stylist_call_receiver'] 	= ($stylist_call_receiver)?$stylist_call_receiver:'no'; 
            $gifter_details['dob'] 						= $fdob[2].'-'.$fdob[1].'-'.$fdob[0];  
            $gifter_details_json 						= serialize($gifter_details);
			
			$arrgifterDetail = array();
			$arrgifterDetail['object_id'] 			= $giftObjectId[0]['object_id']; //$cartId;
			$arrgifterDetail['object_meta_key'] 	= $cartId; //'gifter_details';
			$arrgifterDetail['object_meta_value'] 	= $gifter_details_json;
			$arrgifterDetail['created_datetime'] 	= date('Y-m-d H:i:s');
			$arrgifterDetail['created_by'] 			= $user_id;
			$arrgifterDetail['modified_by'] 		= $user_id;
			$arrgifterDetail['object_status'] 		= '1';
			$giftId.= $this->user_info_model->add_gifter_detail($arrgifterDetail,$giftObjectId[0]['object_id'],$cartId);
			//echo $this->db->last_query();
			$friend_stateid   = $this->user_info_model->get_state_name(trim($friend_state));
			if(empty($friend_stateid))
			{
				$this->failed_response(1006, "Please Enter Valid State Name");
			}
			$userFriendAddress = array();
			$userFriendAddress['user_id'] 			= $user_id  ;
			$userFriendAddress['first_name'] 		= ($friend_username[0])? $friend_username[0]:$friend_full_name;
			$userFriendAddress['last_name'] 		= ($friend_username[1])? $friend_username[1]:'';
			$userFriendAddress['email'] 			= $friend_email;
			$userFriendAddress['mobile_no'] 		= $friend_mobile;
			$userFriendAddress['shipping_address'] 	= $friend_billing_address;
			$userFriendAddress['pincode'] 			= $friend_pincode;
			$userFriendAddress['city_name'] 		= $friend_city;
			$userFriendAddress['state_name'] 		= $friend_stateid['id'];
			$userFriendAddress['created_datetime'] 	= date('Y-m-d H:i:s');
			$userFriendAddress['address_type'] 		= 'S';
			$userFriendAddress['is_default'] 		= 0;
			$userFriendAddress['cart_id'] 			= $cartId;
			$shippingId.=  $this->user_info_model->add_user_address($userFriendAddress, $cartId, $user_id,$userFriendAddress['address_type']);
		}
		$data = new stdClass();
		$data->message = 'Data Send successfully.';
		$data->user_id = $user_id;
		$data->gift_id = $giftId;
		$data->cart_id = $cartId;
		$data->billing_address_id 	= $addressId;
		$data->shipping_address_id 	= $shippingId;
		$this->success_response($data);
		
	}
	
	function update_order_data_post()
	{
		$user_id		=	$this->input->post('user_id');
		$auth_token		=	$this->input->post('auth_token');
		$objectid		=	$this->input->post('object_id');
		$step			=	$this->input->post('step');
		$cart_id		=	$this->input->post('cart_id');
		$pa_data		=	$this->input->post('pa_data');
		$categories		=	$this->input->post('category_answer_id');
		$attributes		=	$this->input->post('attribute_answer_id');
		
		if($cart_id == ''){ $this->failed_response(1006, "Please enter Cart Id"); }
		if($step == ''){ $this->failed_response(1006, "Please check step"); }
		if($objectid == ''){ $this->failed_response(1006, "Please check object id"); }
		$objectIddb = '';
		if($step == 'category_selection')
		{
			$objectIddb = $this->user_info_model->get_object_id('category_selection');
			if($pa_data == ''){ $this->failed_response(1006, "Please check category_selection"); }
			//if($categories == ''){ $this->failed_response(1006, "Please check category answer id"); }
			//if($attributes == ''){ $this->failed_response(1006, "Please check attribute answer id"); }
			$arr_answers[0]['question'] =  'category_selection';
			$arr_answers[0]['answer_id'] =  $pa_data; 
			if(!empty($categories) && !empty($attributes))
			{
				$this->user_order_pa_save($user_id, $cart_id, $categories, $attributes);
			}
		}
		else if($step == 'pa_selection_before')
		{
			$objectIddb = $this->user_info_model->get_object_id('pa_selection_before');
			if($pa_data == ''){ $this->failed_response(1006, "Please check pa_selection_before"); }
			//if($categories == ''){ $this->failed_response(1006, "Please check category answer id"); }
			//if($attributes == ''){ $this->failed_response(1006, "Please check attribute answer id"); }
			$arr_answers[0]['question'] =  'pa_selection_before';
			$arr_answers[0]['answer_id'] =  $pa_data ; 
			if(!empty($categories) && !empty($attributes))
			{
				$this->user_order_pa_save($user_id, $cart_id, $categories, $attributes);
			}
		}
		else if($step == 3)
        { 
		  $objectIddb = $this->user_info_model->get_object_id('pa_selection_before');
          if($pa_data == ''){ $this->failed_response(1006, "Please check pa_data");}
		 // if($categories == ''){ $this->failed_response(1006, "Please check category answer id");}
		 // if($attributes == ''){ $this->failed_response(1006, "Please check attribute answer id");}
          $arr_answers[0]['question'] =  'pa_selection_before';
          $arr_answers[0]['answer_id'] =  $pa_data ;  
		  if(!empty($categories) && !empty($attributes))
			{
				$this->user_order_pa_save($user_id, $cart_id, $categories, $attributes);
			}	  

        }
		else if($step == 'pa_selection_after')
        { 
		  $objectIddb = $this->user_info_model->get_object_id('pa_selection_after');
		  if($pa_data == ''){ $this->failed_response(1006, "Please check pa_selection_after");}
		  //if($categories == ''){ $this->failed_response(1006, "Please check category answer id");}
		 // if($attributes == ''){ $this->failed_response(1006, "Please check attribute answer id");}
          $arr_answers[0]['question'] 	=  'pa_selection_after';
          $arr_answers[0]['answer_id'] 	=  $pa_data ;
          $order_unique_no = $this->input->post('order_no');
			if(trim($order_unique_no) == ''){$this->failed_response(1006, "Please enter order no");}
            //$pa_selection = $pa_selection_optional;
            $pa_selection = $pa_data;
            $objectid = $this->Pa_model->get_object('pa_selection_after')[0]['object_id'];
			
            $order_dispno = $this->Pa_model->get_orderdisplayno($order_unique_no)[0]['order_display_no'];
            $this->Pa_model->save_objectmeta_data($pa_selection,$user_id,$objectid,$order_dispno);
            $this->Pa_model->update_cartid_to_order_unique_no($order_dispno,$user_id,$cart_id);
			if(!empty($categories) && !empty($attributes))
			{
				$this->user_order_pa_save($user_id, $cart_id, $categories, $attributes);
			}
        }
		
		else if($step == 'extra_info')
        {
			$objectIddb = $this->user_info_model->get_object_id('scbox_user_comments');
			//$arr_answers[0]['question'] =  'user_comment';
			$arr_answers[0]['question'] =  'scbox_user_comments';
            $arr_answers[0]['answer_id'] =  ($pa_data)?$pa_data:'';
            /*$arr_answers[1]['question'] =  'user_wardrobe';
            $arr_answers[1]['answer_id'] =  ($img_name) ? $img_name:'';*/
        }  
		
		 foreach($arr_answers as $key=>$json){
            $question = $json['question'];
            $question_id = @$data_answers[$question];
            $answer_id = $json['answer_id'];                 
            /* $this->Pa_model->save_scboxuser_answer($question_id,$answer_id,$user_id,$objectid,$question,$cart_id,$objectIddb); */
			$this->Pa_model->save_scboxuser_answer($question_id,$answer_id,$user_id,$objectid,$question);
			$this->Pa_model->save_scboxuser_answer($question_id,$answer_id,$user_id,$objectIddb,$cart_id);
			
          }
		$data = new stdClass();
		$data->message = 'Data Send successfully.';
		$this->success_response($data);
	}
	
	public function user_order_pa_save($user_id, $cart_id, $categories, $attributes)
	{
		
		if(!empty($categories))
		  {
			$category_array = explode(',', $categories);     
			$i=0;
			foreach($category_array as $val)
			{
			  if($val!='' && $val!='0')
			  {
				$category_pa_data[$i]['object_id'] = $cart_id;
				$category_pa_data[$i]['category_id'] = trim($val);
				$category_pa_data[$i]['object_type'] = 'order';
				$category_pa_data[$i]['modified_by'] = $user_id;
			  }
			  $i++;
			}
		  }

		  if(!empty($attributes))
		  {
			   $attributes_array = explode(',', $attributes);
				$j=0;
				foreach($attributes_array as $val)
				{
				  if($val!='' && $val!='0')
				  {
					$attribute_pa_data[$j]['object_id'] = $cart_id;
					$attribute_pa_data[$j]['attribute_id'] = trim($val);
					$attribute_pa_data[$j]['object_type'] = 'order';
					$attribute_pa_data[$j]['modified_by'] = $user_id;
				  }       
				  
				  $j++;
				}
		  }
		
		if(!empty($category_pa_data) && !empty($attribute_pa_data))
		  {
			$this->Pa_model->user_pa_data('order',$category_pa_data,$attribute_pa_data,$cart_id);
		  }
	}
	
	
	public function image_upload_post()
	{
		$user_id		=	$this->input->post('user_id');
		$auth_token		=	$this->input->post('auth_token');
		$objectid		=	$this->input->post('object_id');
		
		if($user_id == ''){ $this->failed_response(1006, "Please check user id"); }
		if($auth_token == ''){ $this->failed_response(1006, "Please check auth token"); }
		if($objectid == ''){ $this->failed_response(1006, "Please check object id"); }
		$files = $_FILES;
		$count = count($_FILES['image']['name']);
			$output = [];
			for($i=0; $i<$count; $i++)
			{ 		
				$_FILES['image']['name']= $files['image']['name'][$i];
				$_FILES['image']['type']= $files['image']['type'][$i];
				$_FILES['image']['tmp_name']= $files['image']['tmp_name'][$i];
				$_FILES['image']['error']= $files['image']['error'][$i];
				$_FILES['image']['size']= $files['image']['size'][$i];    
				$ext = pathinfo($files['image']['name'][$i], PATHINFO_EXTENSION);
				$img_name = time().mt_rand(0,10000).'.'.$ext;
				$pathAndName =  $this->config->item('wordrobe_path').$img_name;
				$moveResult = move_uploaded_file($files['image']['tmp_name'][$i], $pathAndName);
				$config['image_library'] 	= 'gd2';
				$config['source_image'] 	= $this->config->item('wordrobe_path').$img_name;
				$config['new_image'] 		= $this->config->item('wordrobe_path').'thumb/';
				$config['create_thumb'] 	= TRUE;
				$config['thumb_marker'] 	= '';
				$config['maintain_ratio'] 	= TRUE;
				$config['width'] 			= 200;
				$config['height'] 			= 200;
				$this->image_lib->initialize($config);
				$this->image_lib->clear();
				if (!$this->image_lib->resize())
				{
					$this->image_lib->display_errors();
					$ok =0;
				}else{
					$ok =1;
				}
				$arrAddImage = array();
				$arrAddImage['is_active']			= 1;
				$arrAddImage['image_name']			= $img_name;
				$arrAddImage['user_id']				= $user_id;
				$arrAddImage['created_by']			= $user_id;
				$arrAddImage['created_datetime']	= date('Y-m-d H:i:s');
				$wardrobe = $this->user_info_model->addUserWardrobe($arrAddImage);
				
		   }
		   
			$arr_answers[1]['question'] =  'user_wardrobe';
			$arr_answers[1]['answer_id'] =  ($img_name) ? $img_name:'';
		
			foreach($arr_answers as $key=>$json){
            $question = $json['question'];
            $question_id = @$data_answers[$question];
            $answer_id = $json['answer_id'];                 
            $this->Pa_model->save_scboxuser_answer($question_id,$answer_id,$user_id,$objectid,$question);
          }
		$data = new stdClass();
		$data->message = 'Data Send successfully.';
		$this->success_response($data);		   
		
	}
	
	
	public function category_combo_get()
	{		
		$data = new stdClass();
		$gender 		= $this->input->get('gender');
		$package 		= $this->input->get('package');
		if(trim($gender) == '' && trim($package) == ''){
			$this->failed_response(1006, "Please select gender and package");
		}
		
		$women_packages = array(1,2,3,4);
		$men_packages = array(1,2,3,4);
		/* //if(trim($gender) == 1 && (trim($package) != 1 || trim($package) != 2 || trim($package) != 3 || trim($package) != 4 ))
		if(trim($gender) == 1 && !(in_array(trim($package),$women_packages)))
		{
			$this->failed_response(1006, "Package your are requesting is not available for this gender");
		}
		
		//if(trim($gender) == 1 && (trim($package) != 5 || trim($package) != 6 || trim($package) != 7 || trim($package) != 8 ))
		if(trim($gender) == 1 && !(in_array(trim($package),$men_packages)))
		{
			$this->failed_response(1006, "Package your are requesting is not available for this gender");
		} */
		if(trim($gender) == 1 && (in_array(trim($package),$women_packages)))
		{ 
			//$getCategory	= $this->frontend_model->get_package_category($gender,$package);
			if($package==1)
			{
				$getCategory	= array( array("id"=>"3","name"=>"Apparel"),
									array("id"=>"4","name"=>"Bags"),									
									array("id"=>"6","name"=>"Accessories"),
									array("id"=>"8","name"=>"Jewellery"),
									array("id"=>"7","name"=>"Beauty"),
									);
			}else
			{
				$getCategory	= array( array("id"=>"3","name"=>"Apparel"),
									 array("id"=>"4","name"=>"Bags"),
									 array("id"=>"5","name"=>"Footwear"),
									 array("id"=>"6","name"=>"Accessories"),
									 array("id"=>"8","name"=>"Jewellery"),
									 array("id"=>"7","name"=>"Beauty"),
									);
			}
			
		}else if(trim($gender) == 2 && (in_array(trim($package),$men_packages)))
		{
			//$getCategory	= $this->frontend_model->get_package_category($gender,$package);
			if($package==1)
			{
				$getCategory	= array( array("id"=>"40","name"=>"Apparel"),
									 array("id"=>"63","name"=>"Bags"),									
									 array("id"=>"74","name"=>"Accessories"),									
									 array("id"=>"83","name"=>"Grooming"),
									);
			}else
			{
				$getCategory	= array( array("id"=>"40","name"=>"Apparel"),
									 array("id"=>"63","name"=>"Bags"),
									 array("id"=>"68","name"=>"Footwear"),
									 array("id"=>"74","name"=>"Accessories"),									
									 array("id"=>"83","name"=>"Grooming"),
									);
			}
		}else{
			$this->failed_response(1006, "Package your are requesting is not available for this gender");
		}
		//$category['label']	= strtoupper($this->pa_questions_model->get_termtaxanomy_data(14)[0]['taxonomy_content']);	
		$category['label']	= 'WHAT I WANT IN MY BOX?';
		//echo '<pre>';print_r($category['label']);exit;
		//$category['instruction_text']	= $this->frontend_model->get_object_property($package,'instruction_key');
		if($package==1) {$category['instruction_text']	= '(Choose 3)';}
		else if($package==2) {$category['instruction_text']	= '(Choose 4)';}
		else if($package==3) {$category['instruction_text']	= '(Choose 5)';}
		else if($package==4) {$category['instruction_text']	= '(Choose Any)';}
		//$category['instruction_text']	= '(Choose 4)';		
		if($gender == 1 && $gender != '' && $package!='')
		{
			$category['categories']	= $getCategory;	
			
			if($package==1)
			{
				//$category['category_combos']['pkg_price'] = $this->frontend_model->get_object_property(1,'price');	
				$category['category_combos']['pkg_price'] = '2999';		
				$category['category_combos']['pkg_combo'] = array(		
						array("Apparel", "Bags", "Accessories"),
						array("Apparel", "Bags", "Jewellery"),
						array("Apparel", "Bags", "Beauty"),					
						
						array("Bags", "Accessories", "Jewellery"),
						array("Bags", "Accessories", "Beauty"),
						
						array("Bags", "Jewellery", "Beauty"),				
									
				);
			}else if($package==2)
			{			
				//$category['category_combos']['pkg_price'] = $this->frontend_model->get_object_property(2,'price');
				$category['category_combos']['pkg_price'] = '4999';
				$category['category_combos']['pkg_combo'] = array(		
						array("Apparel", "Bags", "Accessories", "Jewellery"),
						array("Apparel", "Bags", "Accessories", "Beauty"),
						array("Apparel", "Bags", "Jewellery", "Beauty"),
						
						array("Apparel", "Footwear", "Accessories", "Jewellery"),
						array("Apparel", "Footwear", "Accessories", "Beauty"),
						array("Apparel", "Footwear", "Jewellery", "Beauty"),
						
						array("Bags", "Footwear", "Accessories", "Jewellery"),
						array("Bags", "Footwear", "Accessories", "Beauty"),
						array("Bags", "Footwear", "Jewellery", "Beauty"),				
				);
			}else if($package==3)
			{			
				//$category['category_combos']['pkg_price'] = $this->frontend_model->get_object_property(3,'price');
				$category['category_combos']['pkg_price'] = '6999';
				$category['category_combos']['pkg_combo'] = array(		
						array("Apparel", "Bags", "Footwear", "Accessories", "Jewellery"),
						array("Apparel", "Bags", "Footwear", "Jewellery", "Beauty"),
						array("Apparel", "Bags", "Footwear", "Accessories", "Beauty"),
						array("Apparel", "Bags", "Accessories", "Jewellery", "Beauty"),
						
						array("Apparel", "Footwear", "Accessories", "Jewellery", "Beauty"),
						array("Bags", "Footwear", "Accessories", "Jewellery", "Beauty"),				
				);	
			}else if($package==4)
			{			
				//$category['category_combos']['pkg_price'] = $this->frontend_model->get_object_property(4,'price');
				$category['category_combos']['pkg_price'] = '';
				$category['category_combos']['pkg_combo'] = array(		
						array("Apparel", "Bags", "Footwear", "Accessories", "Jewellery", "Beauty"),								
				);	
			}
			
		}
		else if($gender == 2 && $gender != '')
		{
			$category['categories']	= $getCategory;
			if($package==1)
			{
				//$category['category_combos']['pkg_price'] = $this->frontend_model->get_object_property(5,'price');
				$category['category_combos']['pkg_price'] = '2999';	
				$category['category_combos']['pkg_combo'] = array(		
						array("Apparel", "Bags", "Accessories"),
						array("Apparel", "Bags", "Grooming"),
						array("Apparel", "Accessories", "Grooming"),
						array("Bags", "Accessories", "Grooming"),
						
				);
			}else if($package==2)
			{
				//$category['category_combos']['pkg_price'] = $this->frontend_model->get_object_property(6,'price');
				$category['category_combos']['pkg_price'] = '4999';	
				$category['category_combos']['pkg_combo'] = array(		
						array("Apparel", "Bags", "Accessories", "Grooming"),
						array("Apparel", "Footwear", "Accessories", "Grooming"),
						array("Bags", "Footwear", "Accessories", "Grooming")	
				);
			}else if($package==3)
			{
				//$category['category_combos']['pkg_price'] = $this->frontend_model->get_object_property(7,'price');
				$category['category_combos']['pkg_price'] = '6999';					
				$category['category_combos']['pkg_combo'] =		
			array("Apparel", "Bags", "Footwear", "Accessories", "Grooming");
			}else if($package==4)
			{			
				//$category['category_combos']['pkg_price'] = $this->frontend_model->get_object_property(4,'price');
				$category['category_combos']['pkg_price'] = '';		
				$category['category_combos']['pkg_combo'] = array(		
						array("Apparel", "Bags", "Footwear", "Accessories", "Grooming"));
			}
			 
		}		
		$data 	= (object)$category;		
		$this->success_response($data);
	}

	public function render_pa_questions_get()
	{
         $data = array();
		$render_pa_questions	= '[{"gender":"women","categories_que_id":"bag_type,footwear_type,accessories_types,beauty_type,accessories_sunglasses_type,","attributes_que_id":"pa_bodshape,apparel_top_size,apparel_band_size,apparel_dress_size,apparel_trouser_size,apparel_jeans_size,apparel_cup_size,apparel_top_fit_like_this,apparel_dress_fit_like_this,apparel_bottom_fit_like_this,apparel_skirt_and_length_to_be_like_this,apparel_dos_and_donts,apparel_style_preference,apparel_color_avoid,apparel_print_avoid,bag_color_avoid,bag_print_avoid,footwear_width,footwear_size,jewellery_type,beauty_shade,apparel_height_feet,apparel_height_inches,bag_i_want,bag_size,footwear_heel_type,footwear_heel_height,accessories_sunglasses_refelector_type,accessories_belts_type,jewellery_tone,beauty_finishing_type,","include_all_que_id":"apparel_stuff_dont_like","pre_payment":{"categories":[{"category_id":"3","name":"Apparel","questions":[{"question_id":"pa_bodshape","label":"THIS IS WHAT MY BODY LOOKS LIKE","instruction_text":"(Pick One)","answer_key":"21","template_type":"1","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"#13D792","collection":[{"collection_id":"289","label":"Hourglass","description":"Medium bust<br>Slim waist<br>Medium hips","image_default":"http://www.scnest.in/assets/images_api/pa/women/body_shape_hourglass-s.png","image_selected":"http://www.scnest.in/assets/images_api/pa/women/body_shape_hourglass-select.png","answer_value":"289"},{"collection_id":"290","label":"Apple","description":"Heavy bust<br>Wide waist<br>Narrow hips","image_default":"http://www.scnest.in/assets/images_api/pa/women/body_shape_apple-s.png","image_selected":"http://www.scnest.in/assets/images_api/pa/women/body_shape_apple-select.png","answer_value":"290"},{"collection_id":"291","label":"Column","description":"Small bust<br>Slender waist<br>Narrow hips","image_default":"http://www.scnest.in/assets/images_api/pa/women/body_shape_column-s.png","image_selected":"http://www.scnest.in/assets/images_api/pa/women/body_shape_column-select.png","answer_value":"291"},{"collection_id":"292","label":"Pear","description":"Small bust<br>Medium waist<br>Wider hips and<br>Upper thighs","image_default":"http://www.scnest.in/assets/images_api/pa/women/body_shape_pear-s.png","image_selected":"http://www.scnest.in/assets/images_api/pa/women/body_shape_pear-select.png","answer_value":"292"},{"collection_id":"293","label":"Inverted Triangle","description":"Heavy bust<br>Slender waist<br>Narrow hips<br>Slim legs","image_default":"http://www.scnest.in/assets/images_api/pa/women/body_shape_inverted-s.png","image_selected":"http://www.scnest.in/assets/images_api/pa/women/body_shape_inverted-select.png","answer_value":"293"},{"collection_id":"294","label":"Goblet","description":"Broad shoulders<br>Heavy bust<br>Medium waist<br>Narrow hips<br>Slim legs","image_default":"http://www.scnest.in/assets/images_api/pa/women/body_shape_goblet-s.png","image_selected":"http://www.scnest.in/assets/images_api/pa/women/body_shape_goblet-select.png","answer_value":"294"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_stuff_dont_like","label":"STUFF I DON\'T NEED","instruction_text":"(Pick Upto 4)","answer_key":"","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"4","selection_color":"#ED1B2E","collection":[{"collection_id":"-9","label":"Tops","description":"","image_default":"http://www.scnest.in/assets/images_api/pa/women/stuff_dont_like_tops.jpg","image_selected":"","answer_value":"-9"},{"collection_id":"-10","label":"Dresses","description":"","image_default":"http://www.scnest.in/assets/images_api/pa/women/stuff_dont_like_dress.jpg","image_selected":"","answer_value":"-10"},{"collection_id":"-11","label":"Shorts and Skirts","description":"","image_default":"http://www.scnest.in/assets/images_api/pa/women/stuff_dont_like_shorts.jpg","image_selected":"","answer_value":"-11"},{"collection_id":"-13","label":"Jeans and Trousers","description":"","image_default":"http://www.scnest.in/assets/images_api/pa/women/stuff_dont_like_jeans.jpg","image_selected":"","answer_value":"-13"},{"collection_id":"-14","label":"Outerwear","description":"","image_default":"http://www.scnest.in/assets/images_api/pa/women/stuff_dont_like_outerwear.jpg","image_selected":"","answer_value":"-14"},{"collection_id":"-12","label":"Jumpsuits","description":"","image_default":"http://www.scnest.in/assets/images_api/pa/women/stuff_dont_like_jumpsuit.jpg","image_selected":"","answer_value":"-12"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_top_size","label":"SIZE AND FIT","instruction_text":"(Answer All)","answer_key":"24","template_type":"3","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"567","label":"Select Top size","description":"Select Top size","image_default":"","image_selected":"","answer_value":"567"},{"collection_id":"306","label":"xs/uk 4/us 2/eu 32","description":"xs/uk 4/us 2/eu 32","image_default":"","image_selected":"","answer_value":"306"},{"collection_id":"307","label":"s/uk 6/us 4/eu 34","description":"s/uk 6/us 4/eu 34","image_default":"","image_selected":"","answer_value":"307"},{"collection_id":"308","label":"s/uk 8/us 6/eu 36","description":"s/uk 8/us 6/eu 36","image_default":"","image_selected":"","answer_value":"308"},{"collection_id":"309","label":"m/uk 10 /us 8/eu 38","description":"m/uk 10 /us 8/eu 38","image_default":"","image_selected":"","answer_value":"309"},{"collection_id":"310","label":"m/uk 12/us 10/eu 40","description":"m/uk 12/us 10/eu 40","image_default":"","image_selected":"","answer_value":"310"},{"collection_id":"311","label":"l/uk 14/us 12/eu 42","description":"l/uk 14/us 12/eu 42","image_default":"","image_selected":"","answer_value":"311"},{"collection_id":"312","label":"l/uk 16/us 14/eu 44","description":"l/uk 16/us 14/eu 44","image_default":"","image_selected":"","answer_value":"312"},{"collection_id":"313","label":"xl/uk 18/us 16/eu 46","description":"xl/uk 18/us 16/eu 46","image_default":"","image_selected":"","answer_value":"313"},{"collection_id":"314","label":"xxl/uk 20/us 18/eu 48","description":"xxl/uk 20/us 18/eu 48","image_default":"","image_selected":"","answer_value":"314"},{"collection_id":"315","label":"xxxl/uk 22/us 20/eu 50","description":"xxxl/uk 22/us 20/eu 50","image_default":"","image_selected":"","answer_value":"315"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_dress_size","label":"","instruction_text":"","answer_key":"26","template_type":"3","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"569","label":"select dress size","description":"select dress size","image_default":"","image_selected":"","answer_value":"569"},{"collection_id":"325","label":"xs/uk 4/us 2/eu 32","description":"xs/uk 4/us 2/eu 32","image_default":"","image_selected":"","answer_value":"325"},{"collection_id":"326","label":"s/uk 6/us 4/eu 34","description":"s/uk 6/us 4/eu 34","image_default":"","image_selected":"","answer_value":"326"},{"collection_id":"327","label":"s/uk 8/us 6/eu 36","description":"s/uk 8/us 6/eu 36","image_default":"","image_selected":"","answer_value":"327"},{"collection_id":"328","label":"m/uk 10 /us 8/eu 38","description":"m/uk 10 /us 8/eu 38","image_default":"","image_selected":"","answer_value":"328"},{"collection_id":"329","label":"m/uk 12/us 10/eu 40","description":"m/uk 12/us 10/eu 40","image_default":"","image_selected":"","answer_value":"329"},{"collection_id":"330","label":"l/uk 14/us 12/eu 42","description":"l/uk 14/us 12/eu 42","image_default":"","image_selected":"","answer_value":"330"},{"collection_id":"331","label":"l/uk 16/us 14/eu 44","description":"l/uk 16/us 14/eu 44","image_default":"","image_selected":"","answer_value":"331"},{"collection_id":"332","label":"xl/uk 18/us 16/eu 46","description":"xl/uk 18/us 16/eu 46","image_default":"","image_selected":"","answer_value":"332"},{"collection_id":"333","label":"xxl/uk 20/us 18/eu 48","description":"xxl/uk 20/us 18/eu 48","image_default":"","image_selected":"","answer_value":"333"},{"collection_id":"334","label":"xxxl/uk 22/us 20/eu 50","description":"xxxl/uk 22/us 20/eu 50","image_default":"","image_selected":"","answer_value":"334"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_trouser_size","label":"","instruction_text":"","answer_key":"28","template_type":"3","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"570","label":"select trouser size","description":"select trouser size","image_default":"","image_selected":"","answer_value":"570"},{"collection_id":"340","label":"xs/25","description":"xs/25","image_default":"","image_selected":"","answer_value":"340"},{"collection_id":"341","label":"s/26","description":"s/26","image_default":"","image_selected":"","answer_value":"341"},{"collection_id":"342","label":"m/28","description":"m/28","image_default":"","image_selected":"","answer_value":"342"},{"collection_id":"343","label":"l/30","description":"l/30","image_default":"","image_selected":"","answer_value":"343"},{"collection_id":"344","label":"xl/32","description":"xl/32","image_default":"","image_selected":"","answer_value":"344"},{"collection_id":"345","label":"xxl/34","description":"xxl/34","image_default":"","image_selected":"","answer_value":"345"},{"collection_id":"346","label":"xxxl/36","description":"xxxl/36","image_default":"","image_selected":"","answer_value":"346"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_jeans_size","label":"","instruction_text":"","answer_key":"29","template_type":"3","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"571","label":"select jeans size","description":"select jeans size","image_default":"","image_selected":"","answer_value":"571"},{"collection_id":"347","label":"xs/25","description":"xs/25","image_default":"","image_selected":"","answer_value":"347"},{"collection_id":"348","label":"s/26","description":"s/26","image_default":"","image_selected":"","answer_value":"348"},{"collection_id":"349","label":"m/28","description":"m/28","image_default":"","image_selected":"","answer_value":"349"},{"collection_id":"350","label":"l/30","description":"l/30","image_default":"","image_selected":"","answer_value":"350"},{"collection_id":"351","label":"xl/32","description":"xl/32","image_default":"","image_selected":"","answer_value":"351"},{"collection_id":"352","label":"xxl/34","description":"xxl/34","image_default":"","image_selected":"","answer_value":"352"},{"collection_id":"353","label":"xxxl/36","description":"xxxl/36","image_default":"","image_selected":"","answer_value":"353"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_band_size","label":"","instruction_text":"","answer_key":"25","template_type":"3","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"572","label":"select bra: band size","description":"select bra: band size","image_default":"","image_selected":"","answer_value":"572"},{"collection_id":"316","label":"30","description":"30","image_default":"","image_selected":"","answer_value":"316"},{"collection_id":"317","label":"32","description":"32","image_default":"","image_selected":"","answer_value":"317"},{"collection_id":"318","label":"34","description":"34","image_default":"","image_selected":"","answer_value":"318"},{"collection_id":"319","label":"36","description":"36","image_default":"","image_selected":"","answer_value":"319"},{"collection_id":"320","label":"38","description":"38","image_default":"","image_selected":"","answer_value":"320"},{"collection_id":"321","label":"40","description":"40","image_default":"","image_selected":"","answer_value":"321"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_cup_size","label":"","instruction_text":"","answer_key":"18","template_type":"3","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"573","label":"select bra: cup size","description":"select bra: cup size","image_default":"","image_selected":"","answer_value":"573"},{"collection_id":"436","label":"a","description":"a","image_default":"","image_selected":"","answer_value":"436"},{"collection_id":"437","label":"b","description":"b","image_default":"","image_selected":"","answer_value":"437"},{"collection_id":"438","label":"c","description":"c","image_default":"","image_selected":"","answer_value":"438"},{"collection_id":"439","label":"d","description":"d","image_default":"","image_selected":"","answer_value":"439"},{"collection_id":"440","label":"dd","description":"dd","image_default":"","image_selected":"","answer_value":"440"},{"collection_id":"441","label":"f","description":"f","image_default":"","image_selected":"","answer_value":"441"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_top_fit_like_this","label":"MY TOP SHOULD FIT LIKE THIS","instruction_text":"(Pick 1 Or More)","answer_key":"23","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#13D792","collection":[{"collection_id":"322","label":"Fitted","description":"Fitted","image_default":"http://www.scnest.in/assets/images_api/pa/women/top_fit_like_this_fitted.jpg","image_selected":"","answer_value":"322"},{"collection_id":"323","label":"Relaxed","description":"Relaxed","image_default":"http://www.scnest.in/assets/images_api/pa/women/top_fit_like_this_relaxed.jpg","image_selected":"","answer_value":"323"},{"collection_id":"324","label":"Flowy","description":"Flowy","image_default":"http://www.scnest.in/assets/images_api/pa/women/top_fit_like_this_flowy.jpg","image_selected":"","answer_value":"324"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_dress_fit_like_this","label":"MY DRESS SHOULD FIT LIKE THIS","instruction_text":"(Pick 1 Or More)","answer_key":"27","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#13D792","collection":[{"collection_id":"335","label":"Bodycon","description":"Bodycon","image_default":"http://www.scnest.in/assets/images_api/pa/women/dress_fit_like_this_bodycon.jpg","image_selected":"","answer_value":"335"},{"collection_id":"336","label":"Shift","description":"Shift","image_default":"http://www.scnest.in/assets/images_api/pa/women/dress_fit_like_this_shift.jpg","image_selected":"","answer_value":"336"},{"collection_id":"337","label":"A line","description":"A line","image_default":"http://www.scnest.in/assets/images_api/pa/women/dress_fit_like_this_aline.jpg","image_selected":"","answer_value":"337"},{"collection_id":"338","label":"Skater","description":"Skater","image_default":"http://www.scnest.in/assets/images_api/pa/women/dress_fit_like_this_skater.jpg","image_selected":"","answer_value":"338"},{"collection_id":"339","label":"Flowy","description":"Flowy","image_default":"http://www.scnest.in/assets/images_api/pa/women/dress_fit_like_this_flowy.jpg","image_selected":"","answer_value":"339"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_bottom_fit_like_this","label":"MY BOTTOMS SHOULD FIT LIKE THIS","instruction_text":"(Pick 1 Or More)","answer_key":"30","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#13D792","collection":[{"collection_id":"354","label":"Skinny","description":"Skinny","image_default":"http://www.scnest.in/assets/images_api/pa/women/bottom_fit_like_this_skinny.jpg","image_selected":"","answer_value":"354"},{"collection_id":"355","label":"Slim","description":"Slim","image_default":"http://www.scnest.in/assets/images_api/pa/women/bottom_fit_like_this_slim.jpg","image_selected":"","answer_value":"355"},{"collection_id":"356","label":"Straight","description":"Straight","image_default":"http://www.scnest.in/assets/images_api/pa/women/bottom_fit_like_this_straight.jpg","image_selected":"","answer_value":"356"},{"collection_id":"357","label":"Flared","description":"Flared","image_default":"http://www.scnest.in/assets/images_api/pa/women/bottom_fit_like_this_flared.jpg","image_selected":"","answer_value":"357"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_skirt_and_length_to_be_like_this","label":"I LIKE THE LENGTH OF MY SKIRTS AND DRESSES TO BE LIKE THIS","instruction_text":"(Pick 1 Or More)","answer_key":"31","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#13D792","collection":[{"collection_id":"358","label":"Mini","description":"Mini","image_default":"http://www.scnest.in/assets/images_api/pa/women/skirt_fit_like_this_mini.jpg","image_selected":"","answer_value":"358"},{"collection_id":"359","label":"Above the knee","description":"Above the knee","image_default":"http://www.scnest.in/assets/images_api/pa/women/skirt_fit_like_this_above.jpg","image_selected":"","answer_value":"359"},{"collection_id":"360","label":"Below the knee","description":"Below the knee","image_default":"http://www.scnest.in/assets/images_api/pa/women/skirt_fit_like_this_below.jpg","image_selected":"","answer_value":"360"},{"collection_id":"361","label":"Midi","description":"Midi","image_default":"http://www.scnest.in/assets/images_api/pa/women/skirt_fit_like_this_midi.jpg","image_selected":"","answer_value":"361"},{"collection_id":"362","label":"Maxi","description":"Maxi","image_default":"http://www.scnest.in/assets/images_api/pa/women/skirt_fit_like_this_maxi.jpg","image_selected":"","answer_value":"362"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_dos_and_donts","label":"MY DO\'S AND DON\'TS","instruction_text":"(Pick Yes Or No For Each Style)","answer_key":"58","template_type":"1","template_sub_type":"3","min_selection":"8","max_selection":"","selection_color":"","collection":[{"collection_id":"494","label":"Sleeveless","description":"Sleeveless","image_default":"http://www.scnest.in/assets/images_api/pa/women/dos_and_donts_sleeveless.jpg","image_selected":"","answer_value":"494"},{"collection_id":"495","label":"Halter","description":"Halter","image_default":"http://www.scnest.in/assets/images_api/pa/women/dos_and_donts_halter.jpg","image_selected":"","answer_value":"495"},{"collection_id":"496","label":"Strappy","description":"Strappy","image_default":"http://www.scnest.in/assets/images_api/pa/women/dos_and_donts_strappy.jpg","image_selected":"","answer_value":"496"},{"collection_id":"497","label":"Off Shoulder","description":"Off Shoulder","image_default":"http://www.scnest.in/assets/images_api/pa/women/dos_and_donts_offshoulder.jpg","image_selected":"","answer_value":"497"},{"collection_id":"498","label":"Tube","description":"","image_default":"http://www.scnest.in/assets/images_api/pa/women/dos_and_donts_tube.jpg","image_selected":"","answer_value":"498"},{"collection_id":"499","label":"Crop Top","description":"Crop Top","image_default":"http://www.scnest.in/assets/images_api/pa/women/dos_and_donts_crop.jpg","image_selected":"","answer_value":"499"},{"collection_id":"500","label":"Backless","description":"Backless","image_default":"http://www.scnest.in/assets/images_api/pa/women/dos_and_donts_backless.jpg","image_selected":"","answer_value":"500"},{"collection_id":"501","label":"Deep Neck","description":"Deep Neck","image_default":"http://www.scnest.in/assets/images_api/pa/women/dos_and_donts_deepneck.jpg","image_selected":"","answer_value":"501"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_style_preference","label":"MY STYLE PREFERENCE","instruction_text":"(Pick Love, Like Or Dislike For Each Style)","answer_key":"22","template_type":"1","template_sub_type":"4","min_selection":"6","max_selection":"","selection_color":"","collection":[{"collection_id":"295","label":"Classicist","description":"Classicist","image_default":"http://www.scnest.in/assets/images_api/pa/women/style_preference_classic.jpg","image_selected":"","answer_value":"295"},{"collection_id":"296","label":"Romantic-Feminine","description":"Romantic-Feminine","image_default":"http://www.scnest.in/assets/images_api/pa/women/style_preference_romantic.jpg","image_selected":"","answer_value":"296"},{"collection_id":"297","label":"Free Sprit","description":"Free Sprit","image_default":"http://www.scnest.in/assets/images_api/pa/women/style_preference_bohemian.jpg","image_selected":"","answer_value":"297"},{"collection_id":"298","label":"Bombshell","description":"Bombshell","image_default":"http://www.scnest.in/assets/images_api/pa/women/style_preference_bombshell.jpg","image_selected":"","answer_value":"298"},{"collection_id":"299","label":"Bold and Edgy","description":"Bold and Edgy","image_default":"http://www.scnest.in/assets/images_api/pa/women/style_preference_bold.jpg","image_selected":"","answer_value":"299"},{"collection_id":"300","label":"Athleisure","description":"Athleisure","image_default":"http://www.scnest.in/assets/images_api/pa/women/style_preference_atleisure.jpg","image_selected":"","answer_value":"300"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_color_avoid","label":"COLORS I AVOID","instruction_text":"(Pick 1 Or More)","answer_key":"2","template_type":"2","template_sub_type":"1","min_selection":"1","max_selection":"","selection_color":"#ED1B2E","collection":[{"collection_id":"-419","label":"#90140c","description":"","image_default":"","image_selected":"","answer_value":"-419"},{"collection_id":"-424","label":"#b8bebe","description":"","image_default":"","image_selected":"","answer_value":"-424"},{"collection_id":"-403","label":"#be914a","description":null,"image_default":"","image_selected":"","answer_value":"-403"},{"collection_id":"-404","label":"#ff0080","description":null,"image_default":"","image_selected":"","answer_value":"-404"},{"collection_id":"-405","label":"#734021","description":null,"image_default":"","image_selected":"","answer_value":"-405"},{"collection_id":"-406","label":"#fec4c3","description":null,"image_default":"","image_selected":"","answer_value":"-406"},{"collection_id":"-407","label":"#0b0706","description":null,"image_default":"","image_selected":"","answer_value":"-407"},{"collection_id":"-408","label":"#98012e","description":null,"image_default":"","image_selected":"","answer_value":"-408"},{"collection_id":"-409","label":"#ffffff","description":null,"image_default":"","image_selected":"","answer_value":"-409"},{"collection_id":"-410","label":"#f57d31","description":null,"image_default":"","image_selected":"","answer_value":"-410"},{"collection_id":"-411","label":"#87ceeb","description":null,"image_default":"","image_selected":"","answer_value":"-411"},{"collection_id":"-412","label":"#cdd1d1","description":null,"image_default":"","image_selected":"","answer_value":"-412"},{"collection_id":"-413","label":"#d5011a","description":null,"image_default":"","image_selected":"","answer_value":"-413"},{"collection_id":"-414","label":"#800080","description":null,"image_default":"","image_selected":"","answer_value":"-414"},{"collection_id":"-415","label":"#bcd2ee","description":null,"image_default":"","image_selected":"","answer_value":"-415"},{"collection_id":"-416","label":"#576331","description":null,"image_default":"","image_selected":"","answer_value":"-416"},{"collection_id":"-417","label":"#041531","description":null,"image_default":"","image_selected":"","answer_value":"-417"},{"collection_id":"-418","label":"#f7b719","description":null,"image_default":"","image_selected":"","answer_value":"-418"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_color_avoid","label":"","instruction_text":"","answer_key":"","template_type":"4","template_sub_type":"2","min_selection":"0","max_selection":"0","selection_color":"","collection":[{"collection_id":"516","label":"I\'m a rainbow, I like all colors","description":null,"image_default":"","image_selected":"","answer_value":"516"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_print_avoid","label":"PRINTS AND PATTERNS I AVOID","instruction_text":"(Pick 1 Or More)","answer_key":"4","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#ED1B2E","collection":[{"collection_id":"-198","label":"Polka Dots","description":"Polka Dots","image_default":"http://www.scnest.in/assets/images_api/pa/women/print_avoid_polka.jpg","image_selected":"","answer_value":"-198"},{"collection_id":"-184","label":"Floral","description":"Floral","image_default":"http://www.scnest.in/assets/images_api/pa/women/print_avoid_floral.jpg","image_selected":"","answer_value":"-184"},{"collection_id":"-199","label":"Stripes","description":"Stripes","image_default":"http://www.scnest.in/assets/images_api/pa/women/print_avoid_stripes.jpg","image_selected":"","answer_value":"-199"},{"collection_id":"-251","label":"Animal","description":"Animal","image_default":"http://www.scnest.in/assets/images_api/pa/women/print_avoid_animal.jpg","image_selected":"","answer_value":"-251"},{"collection_id":"-253","label":"Checks","description":"Checks","image_default":"http://www.scnest.in/assets/images_api/pa/women/print_avoid_check.jpg","image_selected":"","answer_value":"-253"},{"collection_id":"-255","label":"Solid","description":"Solid","image_default":"http://www.scnest.in/assets/images_api/pa/women/print_avoid_solid.jpg","image_selected":"","answer_value":"-255"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"women_commentbox","label":"COMMENT BOX","instruction_text":"","answer_key":"","template_type":"5","template_sub_type":"1","min_selection":"0","max_selection":"0","selection_color":"","collection":[{"collection_id":"378","label":"IS THERE ANYTHING WE MISSED?","description":"","image_default":"","image_selected":"","answer_value":"378"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]},{"category_id":"4","name":"Bags","questions":[{"question_id":"bag_type","label":"THE KIND OF BAG I AM LOOKING FOR","instruction_text":"(Pick 1 Or More)","answer_key":"","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#13D792","collection":[{"collection_id":"15","label":"Sling","description":"Sling","image_default":"http://www.scnest.in/assets/images_api/pa/women/bag_type_sling.jpg","image_selected":"","answer_value":"15"},{"collection_id":"16","label":"Clutch","description":"Clutch","image_default":"http://www.scnest.in/assets/images_api/pa/women/bag_type_clutch.jpg","image_selected":"","answer_value":"16"},{"collection_id":"17","label":"Handbag","description":"Handbag","image_default":"http://www.scnest.in/assets/images_api/pa/women/bag_type_handbag.jpg","image_selected":"","answer_value":"17"},{"collection_id":"18","label":"Tote","description":"Tote","image_default":"http://www.scnest.in/assets/images_api/pa/women/bag_type_tote.jpg","image_selected":"","answer_value":"18"},{"collection_id":"19","label":"Backpack","description":"Backpack","image_default":"http://www.scnest.in/assets/images_api/pa/women/bag_type_backpack.jpg","image_selected":"","answer_value":"19"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]},{"category_id":"5","name":"Footwear","questions":[{"question_id":"footwear_width","label":"FOOTWEAR WIDTH","instruction_text":"(Pick One)","answer_key":"39","template_type":"4","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"387","label":"I Have Broad Feet","description":"I Have Broad Feet","image_default":"","image_selected":"","answer_value":"387"},{"collection_id":"388","label":"I Have Narrow Feet","description":"I Have Narrow Feet","image_default":"","image_selected":"","answer_value":"388"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"footwear_type","label":"THE TYPE OF SHOES I AM LOOKING FOR ","instruction_text":"(Pick 1 Or More)","answer_key":"","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#13D792","collection":[{"collection_id":"20","label":"Flats","description":"Flats","image_default":"http://www.scnest.in/assets/images_api/pa/women/footwear_type_flats.jpg","image_selected":"","answer_value":"20"},{"collection_id":"21","label":"Sneakers","description":"Sneakers","image_default":"http://www.scnest.in/assets/images_api/pa/women/footwear_type_sneakers.jpg","image_selected":"","answer_value":"21"},{"collection_id":"22","label":"Heels","description":"Heels","image_default":"http://www.scnest.in/assets/images_api/pa/women/footwear_type_heels.jpg","image_selected":"","answer_value":"22"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"footwear_size","label":"FOOTWEAR SIZE","instruction_text":"(Pick One)","answer_key":"38","template_type":"3","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"564","label":"select footwear size","description":"select footwear size","image_default":"","image_selected":"","answer_value":"564"},{"collection_id":"380","label":"uk 3/us 5/eu 36","description":"uk 3/us 5/eu 36","image_default":"","image_selected":"","answer_value":"380"},{"collection_id":"381","label":"uk 4/us 6/eu 37","description":"uk 4/us 6/eu 37","image_default":"","image_selected":"","answer_value":"381"},{"collection_id":"382","label":"uk 5/us 7/eu 38","description":"uk 5/us 7/eu 38","image_default":"","image_selected":"","answer_value":"382"},{"collection_id":"383","label":"uk 6/us 8/eu 39","description":"uk 6/us 8/eu 39","image_default":"","image_selected":"","answer_value":"383"},{"collection_id":"581","label":"uk 6.5/us 7/eu 40","description":"uk 6.5/us 7/eu 40","image_default":"","image_selected":"","answer_value":"581"},{"collection_id":"384","label":"uk 7/us 9/eu 41","description":"uk 7/us 9/eu 41","image_default":"","image_selected":"","answer_value":"384"},{"collection_id":"385","label":"uk 8/us 10/eu 42","description":"uk 8/us 10/eu 42","image_default":"","image_selected":"","answer_value":"385"},{"collection_id":"386","label":"uk 9/us 11/eu 43","description":"uk 9/us 11/eu 43","image_default":"","image_selected":"","answer_value":"386"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]},{"category_id":"6","name":"Accessories","questions":[{"question_id":"accessories_types","label":"ACCESSORIES","instruction_text":"(Pick One)","answer_key":"","template_type":"4","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"23","label":"Sunglasses","description":"Sunglasses","image_default":"","image_selected":"","answer_value":"23"},{"collection_id":"29","label":"Belts","description":"Belts","image_default":"","image_selected":"","answer_value":"29"},{"collection_id":"30","label":"Stationery","description":"Stationery","image_default":"","image_selected":"","answer_value":"30"},{"collection_id":"31","label":"Caps","description":"Caps","image_default":"","image_selected":"","answer_value":"31"},{"collection_id":"32","label":"Scarves","description":"Scarves","image_default":"","image_selected":"","answer_value":"32"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]},{"category_id":"8","name":"Jewellery","questions":[{"question_id":"jewellery_type","label":"THE KIND OF JEWELLERY I AM LOOKING FOR","instruction_text":"(Pick 1 Or More)","answer_key":"42","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#13D792","collection":[{"collection_id":"395","label":"Statement","description":"Statement","image_default":"http://www.scnest.in/assets/images_api/pa/women/jewellery_type_statement.jpg","image_selected":"","answer_value":"395"},{"collection_id":"396","label":"Minimal","description":"Minimal","image_default":"http://www.scnest.in/assets/images_api/pa/women/jewellery_type_minimal.jpg","image_selected":"","answer_value":"396"},{"collection_id":"397","label":"Bold/Chunky","description":"Bold/Chunky","image_default":"http://www.scnest.in/assets/images_api/pa/women/jewellery_type_chunky.jpg","image_selected":"","answer_value":"397"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]},{"category_id":"7","name":"Beauty","questions":[{"question_id":"beauty_type","label":"THE BEAUTY PRODUCT I\'M LOOKING FOR","instruction_text":"(Pick One)","answer_key":"","template_type":"4","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"35","label":"Eye shadow","description":"Eye shadow","image_default":"","image_selected":"","answer_value":"35"},{"collection_id":"34","label":"Liner","description":"Liner","image_default":"","image_selected":"","answer_value":"34"},{"collection_id":"33","label":"Lipstick","description":"Lipstick","image_default":"","image_selected":"","answer_value":"33"},{"collection_id":"36","label":"Nail paint","description":"Nail paint","image_default":"","image_selected":"","answer_value":"36"},{"collection_id":"39","label":"Mascara","description":"Mascara","image_default":"","image_selected":"","answer_value":"39"},{"collection_id":"38","label":"Hand creme","description":"Hand creme","image_default":"","image_selected":"","answer_value":"38"},{"collection_id":"37","label":"Dry shampoo","description":"Dry shampoo","image_default":"","image_selected":"","answer_value":"37"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"beauty_shade","label":"SHADE","instruction_text":"(Pick One)","answer_key":"36","template_type":"4","template_sub_type":"3","min_selection":"0","max_selection":"1","selection_color":"","collection":[{"collection_id":"374","label":"Red","description":"#ff1a1a,#ff0000,#e60000,#cc0000","image_default":"","image_selected":"","answer_value":"374"},{"collection_id":"375","label":"Nude","description":"#ffc0cb,#cc99a2,#d29494,#ae3f3f","image_default":"","image_selected":"","answer_value":"375"},{"collection_id":"376","label":"Pink","description":"#ff77aa,#ec3a8b,#ff3377,#ff0065","image_default":"","image_selected":"","answer_value":"376"},{"collection_id":"377","label":"Deep","description":"#660000,#330033,#7b3f00,#b30000","image_default":"","image_selected":"","answer_value":"377"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]},{"category_id":"","name":"Comments","questions":[{"question_id":"women_commentbox","label":"COMMENT BOX","instruction_text":"","answer_key":"","template_type":"5","template_sub_type":"1","min_selection":"1","max_selection":"","selection_color":"","collection":[{"collection_id":" ","label":"IS THERE ANYTHING WE MISSED?","description":"","image_default":"","image_selected":"","answer_value":" "}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]}   ]},"post_payment":{"categories":[{"category_id":"3","name":"Apparel","questions":[{"question_id":"apparel_height_feet","label":"How Tall Are You?","instruction_text":"(Pick One)","answer_key":"61","template_type":"3","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"574","label":"Select height in feet","description":"Select height in feet","image_default":"","image_selected":"","answer_value":"574"},{"collection_id":"517","label":"3 ft","description":"3 ft","image_default":"","image_selected":"","answer_value":"517"},{"collection_id":"518","label":"4 ft","description":"4 ft","image_default":"","image_selected":"","answer_value":"518"},{"collection_id":"519","label":"5 ft","description":"5 ft","image_default":"","image_selected":"","answer_value":"519"},{"collection_id":"520","label":"6 ft","description":"6 ft","image_default":"","image_selected":"","answer_value":"520"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_height_inches","label":"","instruction_text":"","answer_key":"62","template_type":"3","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"575","label":"Select height in inches","description":"Select height in inches","image_default":"","image_selected":"","answer_value":"575"},{"collection_id":"521","label":"0 inch","description":"0 inch","image_default":"","image_selected":"","answer_value":"521"},{"collection_id":"522","label":"1 inch","description":"1 inch","image_default":"","image_selected":"","answer_value":"522"},{"collection_id":"523","label":"2 inch","description":"2 inch","image_default":"","image_selected":"","answer_value":"523"},{"collection_id":"524","label":"3 inch","description":"3 inch","image_default":"","image_selected":"","answer_value":"524"},{"collection_id":"525","label":"4 inch","description":"4 inch","image_default":"","image_selected":"","answer_value":"525"},{"collection_id":"526","label":"5 inch","description":"5 inch","image_default":"","image_selected":"","answer_value":"526"},{"collection_id":"527","label":"6 inch","description":"6 inch","image_default":"","image_selected":"","answer_value":"527"},{"collection_id":"528","label":"7 inch","description":"7 inch","image_default":"","image_selected":"","answer_value":"528"},{"collection_id":"529","label":"8 inch","description":"8 inch","image_default":"","image_selected":"","answer_value":"529"},{"collection_id":"530","label":"9 inch","description":"9 inch","image_default":"","image_selected":"","answer_value":"530"},{"collection_id":"531","label":"10 inch","description":"10 inch","image_default":"","image_selected":"","answer_value":"531"},{"collection_id":"532","label":"11 inch","description":"11 inch","image_default":"","image_selected":"","answer_value":"532"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]},{"category_id":"4","name":"Bags","questions":[{"question_id":"bag_i_want","label":"YOU SELECTED A BAG! COULD YOU TELL US A LITTLE MORE ABOUT THE KIND OF BAG YOU WANT? I WANT IT FOR","instruction_text":"(Pick One)","answer_key":"32","template_type":"4","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"363","label":"Everyday","description":"Everyday","image_default":"","image_selected":"","answer_value":"363"},{"collection_id":"364","label":"Work","description":"Work","image_default":"","image_selected":"","answer_value":"364"},{"collection_id":"365","label":"Party","description":"Party","image_default":"","image_selected":"","answer_value":"365"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"bag_size","label":"I WANT THE SIZE OF MY BAG TO BE","instruction_text":"(Pick One)","answer_key":"33","template_type":"3","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"","label":"Select Bag Size","description":"Select Bag Size","image_default":"","image_selected":"","answer_value":""},{"collection_id":"366","label":"small","description":"small","image_default":"","image_selected":"","answer_value":"366"},{"collection_id":"367","label":"medium","description":"medium","image_default":"","image_selected":"","answer_value":"367"},{"collection_id":"368","label":"large","description":"large","image_default":"","image_selected":"","answer_value":"368"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"bag_color_avoid","label":"COLORS I AVOID","instruction_text":"(Pick 1 Or More)","answer_key":"2","template_type":"2","template_sub_type":"1","min_selection":"1","max_selection":"","selection_color":"#ED1B2E","collection":[{"collection_id":"-419","label":"#90140c","description":"","image_default":"","image_selected":"","answer_value":"-419"},{"collection_id":"-424","label":"#b8bebe","description":"","image_default":"","image_selected":"","answer_value":"-424"},{"collection_id":"-403","label":"#be914a","description":null,"image_default":"","image_selected":"","answer_value":"-403"},{"collection_id":"-404","label":"#ff0080","description":null,"image_default":"","image_selected":"","answer_value":"-404"},{"collection_id":"-405","label":"#734021","description":null,"image_default":"","image_selected":"","answer_value":"-405"},{"collection_id":"-406","label":"#fec4c3","description":null,"image_default":"","image_selected":"","answer_value":"-406"},{"collection_id":"-407","label":"#0b0706","description":null,"image_default":"","image_selected":"","answer_value":"-407"},{"collection_id":"-408","label":"#98012e","description":null,"image_default":"","image_selected":"","answer_value":"-408"},{"collection_id":"-409","label":"#ffffff","description":null,"image_default":"","image_selected":"","answer_value":"-409"},{"collection_id":"-410","label":"#f57d31","description":null,"image_default":"","image_selected":"","answer_value":"-410"},{"collection_id":"-411","label":"#87ceeb","description":null,"image_default":"","image_selected":"","answer_value":"-411"},{"collection_id":"-412","label":"#cdd1d1","description":null,"image_default":"","image_selected":"","answer_value":"-412"},{"collection_id":"-413","label":"#d5011a","description":null,"image_default":"","image_selected":"","answer_value":"-413"},{"collection_id":"-414","label":"#800080","description":null,"image_default":"","image_selected":"","answer_value":"-414"},{"collection_id":"-415","label":"#bcd2ee","description":null,"image_default":"","image_selected":"","answer_value":"-415"},{"collection_id":"-416","label":"#576331","description":null,"image_default":"","image_selected":"","answer_value":"-416"},{"collection_id":"-417","label":"#041531","description":null,"image_default":"","image_selected":"","answer_value":"-417"},{"collection_id":"-418","label":"#f7b719","description":null,"image_default":"","image_selected":"","answer_value":"-418"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"bag_color_avoid","label":"","instruction_text":"","answer_key":"","template_type":"4","template_sub_type":"2","min_selection":"0","max_selection":"0","selection_color":"","collection":[{"collection_id":"516","label":"I\'m a rainbow, I like all colors","description":null,"image_default":"","image_selected":"","answer_value":"516"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"bag_print_avoid","label":"PRINTS AND PATTERNS I AVOID","instruction_text":"(Pick 1 Or More)","answer_key":"4","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#ED1B2E","collection":[{"collection_id":"198","label":"Polka Dots","description":"Polka Dots","image_default":"http://www.scnest.in/assets/images_api/pa/women/print_avoid_polka.jpg","image_selected":"","answer_value":"198"},{"collection_id":"184","label":"Floral","description":"Floral","image_default":"http://www.scnest.in/assets/images_api/pa/women/print_avoid_floral.jpg","image_selected":"","answer_value":"184"},{"collection_id":"199","label":"Stripes","description":"Stripes","image_default":"http://www.scnest.in/assets/images_api/pa/women/print_avoid_stripes.jpg","image_selected":"","answer_value":"199"},{"collection_id":"251","label":"Animal","description":"Animal","image_default":"http://www.scnest.in/assets/images_api/pa/women/print_avoid_animal.jpg","image_selected":"","answer_value":"251"},{"collection_id":"253","label":"Checks","description":"Checks","image_default":"http://www.scnest.in/assets/images_api/pa/women/print_avoid_check.jpg","image_selected":"","answer_value":"253"},{"collection_id":"255","label":"Solid","description":"Solid","image_default":"http://www.scnest.in/assets/images_api/pa/women/print_avoid_solid.jpg","image_selected":"","answer_value":"255"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]},{"category_id":"5","name":"Footwear","questions":[{"question_id":"footwear_heel_type","label":"YOU\'RE LOOKING FOR A PAIR OF SHOES OF HEELS! COULD YOU TELL US WHAT KIND OF HEELS YOU\'D THIS? THE TYPE OF HEELS I AM LOOKING FOR","instruction_text":"(Pick 1 Or More)","answer_key":"40","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#13D792","collection":[{"collection_id":"389","label":"Wedge","description":"Wedge","image_default":"http://www.scnest.in/assets/images_api/pa/women/heel_type_wedge.jpg","image_selected":"","answer_value":"389"},{"collection_id":"390","label":"Block","description":"Block","image_default":"http://www.scnest.in/assets/images_api/pa/women/heel_type_block.jpg","image_selected":"","answer_value":"390"},{"collection_id":"391","label":"Stilletos","description":"Stilletos","image_default":"http://www.scnest.in/assets/images_api/pa/women/heel_type_stilettoes.jpg","image_selected":"","answer_value":"391"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"footwear_heel_height","label":"HOW TALL WOULD YOU LIKE YOUR HEELS TO BE?","instruction_text":"(Pick One)","answer_key":"41","template_type":"3","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"576","label":"Select heel height","description":"","image_default":"","image_selected":"","answer_value":"576"},{"collection_id":"392","label":"low:1.5\"-2.5\"","description":"low:1.5\"-2.5\"","image_default":"","image_selected":"","answer_value":"392"},{"collection_id":"393","label":"medium:2.5-3.5\"","description":"medium:2.5-3.5\"","image_default":"","image_selected":"","answer_value":"393"},{"collection_id":"394","label":"high:3.5\" and above","description":"high:3.5\" and above","image_default":"","image_selected":"","answer_value":"394"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]},{"category_id":"6","name":"Accessories","questions":[{"question_id":"accessories_sunglasses_type","label":"YOU\'RE LOOKING FOR SUNGLASSES! TELL US WHAT KIND? SUNGLASSES","instruction_text":"(Pick 1 Or More)","answer_key":"","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#13D792","collection":[{"collection_id":"24","label":"Aviators","description":"Aviators","image_default":"http://www.scnest.in/assets/images_api/pa/women/sunglasses_type_aviator.jpg","image_selected":"","answer_value":"24"},{"collection_id":"25","label":"Cat eyes","description":"Cat eyes","image_default":"http://www.scnest.in/assets/images_api/pa/women/sunglasses_type_cat.jpg","image_selected":"","answer_value":"25"},{"collection_id":"26","label":"Round","description":"Round","image_default":"http://www.scnest.in/assets/images_api/pa/women/sunglasses_type_round.jpg","image_selected":"","answer_value":"26"},{"collection_id":"27","label":"Square","description":"Square","image_default":"http://www.scnest.in/assets/images_api/pa/women/sunglasses_type_square.jpg","image_selected":"","answer_value":"27"},{"collection_id":"28","label":"Wayfarers","description":"Wayfarers","image_default":"http://www.scnest.in/assets/images_api/pa/women/sunglasses_type_wayfarer.jpg","image_selected":"","answer_value":"28"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"accessories_sunglasses_refelector_type","label":"REFELECTORS","instruction_text":"(Pick Yes Or No)","answer_key":"56","template_type":"4","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"533","label":"Yes","description":"Yes","image_default":"","image_selected":"","answer_value":"533"},{"collection_id":"534","label":"No","description":"No","image_default":"","image_selected":"","answer_value":"534"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"accessories_belts_type","label":"YOU\'RE LOOKING FOR A BELT! COULD YOU TELL US WHAT KIND OF BELT? BELTS","instruction_text":"(Pick 1 Or More)","answer_key":"35","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#13D792","collection":[{"collection_id":"371","label":"Slim","description":"Slim","image_default":"http://www.scnest.in/assets/images_api/pa/women/belt_type_slim.jpg","image_selected":"","answer_value":"371"},{"collection_id":"372","label":"Medium","description":"Medium","image_default":"http://www.scnest.in/assets/images_api/pa/women/belt_type_medium.jpg","image_selected":"","answer_value":"372"},{"collection_id":"373","label":"Chunky","description":"Chunky","image_default":"http://www.scnest.in/assets/images_api/pa/women/belt_type_chunky.jpg","image_selected":"","answer_value":"373"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]},{"category_id":"7","name":"Jewellery","questions":[{"question_id":"jewellery_tone","label":"YOU\'RE LOOKING FOR JEWELLERY! COULD YOU TELL US THE TONE OF JEWELLERY YOU ARE LOOKING FOR. THE TONE OF JEWELLERY I AM LOOKING FOR.","instruction_text":"(Pick 1 Or More)","answer_key":"43","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#13D792","collection":[{"collection_id":"230","label":"Gold","description":"Gold","image_default":"http://www.scnest.in/assets/images_api/pa/women/jewellery_tone_gold.jpg","image_selected":"","answer_value":"398"},{"collection_id":"399","label":"Silver","description":"Silver","image_default":"http://www.scnest.in/assets/images_api/pa/women/jewellery_tone_silver.jpg","image_selected":"","answer_value":"399"},{"collection_id":"400","label":"Oxidised","description":"Oxidised","image_default":"http://www.scnest.in/assets/images_api/pa/women/jewellery_tone_oxidized.jpg","image_selected":"","answer_value":"400"},{"collection_id":"401","label":"Multicolored","description":"Multicolored","image_default":"http://www.scnest.in/assets/images_api/pa/women/jewellery_tone_multicolored.jpg","image_selected":"","answer_value":"401"},{"collection_id":"402","label":"Tonal","description":"Tonal","image_default":"http://www.scnest.in/assets/images_api/pa/women/jewellery_tone_tonal.jpg","image_selected":"","answer_value":"402"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]},{"category_id":"8","name":"Beauty","questions":[{"question_id":"beauty_finishing_type","label":"YOU\'RE LOOKING FOR A BEAUTY PRODUCT! COULD YOU TELL US MORE? THE FINISH OF THE BEAUTY PRODUCT I\'M LOOKING FOR","instruction_text":"(Pick One)","answer_key":"37","template_type":"4","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"378","label":"Matte","description":"Matte","image_default":"","image_selected":"","answer_value":"378"},{"collection_id":"379","label":"Shiny","description":"Shiny","image_default":"","image_selected":"","answer_value":"379"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]}]}},{"gender":"men","categories_que_id":"bag_type,footwear_type,accessories_types,accessories_sunglasses_type","attributes_que_id":"pa_bodshape,apparel_shirt_size,apparel_tshirt_size,apparel_trouser_size,apparel_jeans_size,apparel_tshirt_fit_like_this,apparel_shirt_fit_like_this,apparel_trouser_fit_like_this,apparel_jeans_fit_like_this,apparel_dos_and_donts,apparel_style_preference,apparel_color_avoid,apparel_print_avoid,bag_color_avoid,bag_print_avoid,footwear_size,apparel_height_feet,apparel_height_inches,accessories_sunglasses_refelector_type","include_all_que_id":"apparel_stuff_dont_like","include_all_que_id":"apparel_stuff_dont_like","pre_payment":{"categories":[{"category_id":"40","name":"Apparel","questions":[{"question_id":"pa_bodshape","label":"This Is What My Body Looks Like","instruction_text":"(Pick One)","answer_key":"60","template_type":"1","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"#13D792","collection":[{"collection_id":"506","label":"Slim","description":"Lean Shoulders<br>Medium chest<br>Thin waist","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/shapes/slim.png","image_selected":"https://www.stylecracker.com/assets/seventeen/images/pa/male/shapes/slim-selected.png","answer_value":"506"},{"collection_id":"507","label":"Average","description":"Broad shoulders<br>Wide chest<br>Narrow waist","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/shapes/average.png","image_selected":"https://www.stylecracker.com/assets/seventeen/images/pa/male/shapes/average-selected.png","answer_value":"507"},{"collection_id":"508","label":"Athletic","description":"Bulky shoulders<br>Broad chest<br>Medium waist","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/shapes/athletic.png","image_selected":"https://www.stylecracker.com/assets/seventeen/images/pa/male/shapes/athletic-selected.png","answer_value":"508"},{"collection_id":"509","label":"Triangular","description":"Wide shoulders and chest<br>Bulky arms<br>Wide waist","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/shapes/husky.png","image_selected":"https://www.stylecracker.com/assets/seventeen/images/pa/male/shapes/husky-selected.png","answer_value":"509"},{"collection_id":"510","label":"Heavy","description":"Hefty shoulders and arms<br>Large chest<br>Wide waist","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/shapes/heavy.png","image_selected":"https://www.stylecracker.com/assets/seventeen/images/pa/male/shapes/heavy-selected.png","answer_value":"510"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_stuff_dont_like","label":"What I Don’t Want","instruction_text":"(Pick Upto 7)","answer_key":"","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"7","selection_color":"#ED1B2E","collection":[{"collection_id":"-41","label":"T-Shirts","description":"","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/dwant/1tshirt.jpg","image_selected":"","answer_value":"-41"},{"collection_id":"-95","label":"Casual Shirts","description":"","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/dwant/1casual.jpg","image_selected":"","answer_value":"-95"},{"collection_id":"-96","label":"Formal Shirts","description":"","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/dwant/1formal.jpg","image_selected":"","answer_value":"-96"},{"collection_id":"-54","label":"Shorts","description":"","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/dwant/1short.jpg","image_selected":"","answer_value":"-54"},{"collection_id":"-55","label":"Joggers","description":"","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/dwant/1jogger.jpg","image_selected":"","answer_value":"-55"},{"collection_id":"-56","label":"Jeans","description":"","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/dwant/1jeans.jpg","image_selected":"","answer_value":"-56"},{"collection_id":"-60","label":"Chinos","description":"","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/dwant/1chinos.jpg","image_selected":"","answer_value":"-60"},{"collection_id":"-61","label":"Trousers","description":"","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/dwant/1trouser.jpg","image_selected":"","answer_value":"-61"},{"collection_id":"-62","label":"Outerwear","description":"","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/dwant/1outerwear.jpg","image_selected":"","answer_value":"-62"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_shirt_size","label":"SIZE AND FIT","instruction_text":"(Answer All)","answer_key":"46","template_type":"3","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"352","label":"select shirt size","description":"select shirt size","image_default":"","image_selected":"","answer_value":"352"},{"collection_id":"449","label":"xs/chest 34-36 / neck 15","description":"xs/chest 34-36 / neck 15","image_default":"","image_selected":"","answer_value":"449"},{"collection_id":"450","label":"s/chest 36.5-38 / neck 15.5","description":"s/chest 36.5-38 / neck 15.5","image_default":"","image_selected":"","answer_value":"450"},{"collection_id":"451","label":"m/chest 38-40/ neck 16","description":"m/chest 38-40/ neck 16","image_default":"","image_selected":"","answer_value":"451"},{"collection_id":"452","label":"l/chest 40-42/ neck 17","description":"l/chest 40-42/ neck 17","image_default":"","image_selected":"","answer_value":"452"},{"collection_id":"453","label":"xl/chest 42-44/ neck 17.5","description":"xl/chest 42-44/ neck 17.5","image_default":"","image_selected":"","answer_value":"453"},{"collection_id":"454","label":"xxl/chest 44-46/ neck 18.5","description":"xxl/chest 44-46/ neck 18.5","image_default":"","image_selected":"","answer_value":"454"},{"collection_id":"455","label":"xxxl/chest 46-48/ neck 19.5","description":"xxxl/chest 46-48/ neck 19.5","image_default":"","image_selected":"","answer_value":"455"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_tshirt_size","label":"","instruction_text":"","answer_key":"45","template_type":"3","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"373","label":"select t-shirt size","description":"select t-shirt size","image_default":"","image_selected":"","answer_value":"373"},{"collection_id":"442","label":"xs/34-36","description":"xs/34-36","image_default":"","image_selected":"","answer_value":"442"},{"collection_id":"83","label":"s/36.5-38","description":"s/36.5-38","image_default":"","image_selected":"","answer_value":"443"},{"collection_id":"444","label":"m/38-40","description":"m/38-40","image_default":"","image_selected":"","answer_value":"444"},{"collection_id":"445","label":"l/40-42","description":"l/40-42","image_default":"","image_selected":"","answer_value":"445"},{"collection_id":"446","label":"xl/42-44","description":"xl/42-44","image_default":"","image_selected":"","answer_value":"446"},{"collection_id":"447","label":"xxl/44-46","description":"xxl/44-46","image_default":"","image_selected":"","answer_value":"447"},{"collection_id":"448","label":"xxxl/46-48","description":"xxxl/46-48","image_default":"","image_selected":"","answer_value":"448"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_trouser_size","label":"","instruction_text":"","answer_key":"47","template_type":"3","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"374","label":"select trouser size","description":"select trouser size","image_default":"","image_selected":"","answer_value":"374"},{"collection_id":"456","label":"xs/ waist inches - 28","description":"xs/ waist inches - 28","image_default":"","image_selected":"","answer_value":"456"},{"collection_id":"457","label":"s/ waist inches - 30","description":"s/ waist inches - 30","image_default":"","image_selected":"","answer_value":"457"},{"collection_id":"458","label":"m/ waist inches - 32","description":"m/ waist inches - 32","image_default":"","image_selected":"","answer_value":"458"},{"collection_id":"459","label":"l/ waist inches - 34","description":"l/ waist inches - 34","image_default":"","image_selected":"","answer_value":"459"},{"collection_id":"460","label":"xl/ waist inches - 36","description":"xl/ waist inches - 36","image_default":"","image_selected":"","answer_value":"460"},{"collection_id":"461","label":"xxl/ waist inches - 38","description":"xxl/ waist inches - 38","image_default":"","image_selected":"","answer_value":"461"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_jeans_size","label":"","instruction_text":"","answer_key":"48","template_type":"3","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"375","label":"select jeans size","description":"select jeans size","image_default":"","image_selected":"","answer_value":"375"},{"collection_id":"462","label":"xs/ waist inches - 28","description":"xs/ waist inches - 28","image_default":"","image_selected":"","answer_value":"462"},{"collection_id":"463","label":"s/ waist inches - 30","description":"s/ waist inches - 30","image_default":"","image_selected":"","answer_value":"463"},{"collection_id":"464","label":"m/ waist inches - 32","description":"m/ waist inches - 32","image_default":"","image_selected":"","answer_value":"464"},{"collection_id":"465","label":"l/ waist inches - 34","description":"l/ waist inches - 34","image_default":"","image_selected":"","answer_value":"465"},{"collection_id":"466","label":"xl/ waist inches - 36","description":"xl/ waist inches - 36","image_default":"","image_selected":"","answer_value":"466"},{"collection_id":"467","label":"xxl/ waist inches - 38","description":"xxl/ waist inches - 38","image_default":"","image_selected":"","answer_value":"467"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_tshirt_fit_like_this","label":"i like my t-shirt to be fit like this","instruction_text":"(Pick 1 Or More)","answer_key":"49","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#13D792","collection":[{"collection_id":"468","label":"slim","description":"slim","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/tshirts/1slim.jpg","image_selected":"","answer_value":"468"},{"collection_id":"469","label":"regular","description":"regular","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/tshirts/1regular.jpg","image_selected":"","answer_value":"469"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_shirt_fit_like_this","label":"I Like My Shirt To Fit Like This","instruction_text":"(Pick 1 Or More)","answer_key":"50","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#13D792","collection":[{"collection_id":"470","label":"slim","description":"slim","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/shirts/1slim.jpg","image_selected":"","answer_value":"470"},{"collection_id":"471","label":"regular","description":"regular","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/shirts/1regular.jpg","image_selected":"","answer_value":"471"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_trouser_fit_like_this","label":"I Like My Trouser To Fit Like This","instruction_text":"(Pick 1 Or More)","answer_key":"51","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#13D792","collection":[{"collection_id":"126","label":"skinny","description":"skinny","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/trouser/1skinny.jpg","image_selected":"","answer_value":"472"},{"collection_id":"473","label":"slim","description":"slim","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/trouser/1slim.jpg","image_selected":"","answer_value":"473"},{"collection_id":"474","label":"regular","description":"regular","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/trouser/1regular.jpg","image_selected":"","answer_value":"474"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_jeans_fit_like_this","label":"I Like My Jeans To Fit Like This","instruction_text":"(Pick 1 Or More)","answer_key":"52","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#13D792","collection":[{"collection_id":"475","label":"skinny","description":"skinny","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/jeans/1skinny.jpg","image_selected":"","answer_value":"475"},{"collection_id":"476","label":"slim","description":"slim","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/jeans/1slim.jpg","image_selected":"","answer_value":"476"},{"collection_id":"477","label":"regular","description":"regular","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/jeans/1straight.jpg","image_selected":"","answer_value":"477"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_dos_and_donts","label":"MY DO\'S AND DON\'TS","instruction_text":"(Pick Yes Or No For Each Style)","answer_key":"64","template_type":"1","template_sub_type":"3","min_selection":"7","max_selection":"","selection_color":"","collection":[{"collection_id":"536","label":"round neck","description":"round neck","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/dos/1round.jpg","image_selected":"","answer_value":"536"},{"collection_id":"537","label":"v neck","description":"v neck","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/dos/1vneck.jpg","image_selected":"","answer_value":"537"},{"collection_id":"538","label":"henleys","description":"henleys","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/dos/1henley.jpg","image_selected":"","answer_value":"538"},{"collection_id":"539","label":"collared","description":"collared","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/dos/1collar.jpg","image_selected":"","answer_value":"539"},{"collection_id":"540","label":"half sleeves","description":"half sleeves","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/dos/1half.jpg","image_selected":"","answer_value":"540"},{"collection_id":"541","label":"full sleeves","description":"full sleeves","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/dos/1full.jpg","image_selected":"","answer_value":"541"},{"collection_id":"542","label":"jackets","description":"jackets","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/dos/1jacket.jpg","image_selected":"","answer_value":"542"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_style_preference","label":"MY STYLE PREFERENCE","instruction_text":"(Pick Love, Like Or Dislike For Each Style)","answer_key":"53","template_type":"1","template_sub_type":"4","min_selection":"4","max_selection":"","selection_color":"","collection":[{"collection_id":"512","label":"casual","description":"casual","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/preference/casual.jpg","image_selected":"","answer_value":"512"},{"collection_id":"513","label":"bold","description":"bold","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/preference/1bold.jpg","image_selected":"","answer_value":"513"},{"collection_id":"514","label":"athleisure","description":"athleisure","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/preference/1bold.jpg","image_selected":"","answer_value":"514"},{"collection_id":"515","label":"well groomed","description":"well groomed","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/preference/2well.jpg","image_selected":"","answer_value":"515"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_color_avoid","label":"COLORS I AVOID","instruction_text":"(Pick 1 Or More)","answer_key":"2","template_type":"2","template_sub_type":"1","min_selection":"1","max_selection":"","selection_color":"#ED1B2E","collection":[{"collection_id":"-421","label":"#8bf5cd","description":null,"image_default":"","image_selected":"","answer_value":"-421"},{"collection_id":"-422","label":"#89aad5","description":null,"image_default":"","image_selected":"","answer_value":"-422"},{"collection_id":"-423","label":"#bf94e4","description":null,"image_default":"","image_selected":"","answer_value":"-423"},{"collection_id":"-424","label":"#aeb0ae","description":null,"image_default":"","image_selected":"","answer_value":"-424"},{"collection_id":"-405","label":"#734021","description":null,"image_default":"","image_selected":"","answer_value":"-405"},{"collection_id":"-425","label":"#2082ef","description":null,"image_default":"","image_selected":"","answer_value":"-425"},{"collection_id":"-407","label":"#0b0706","description":null,"image_default":"","image_selected":"","answer_value":"-407"},{"collection_id":"-426","label":"#68dbd6","description":null,"image_default":"","image_selected":"","answer_value":"-426"},{"collection_id":"-472","label":"#f5dc05","description":null,"image_default":"","image_selected":"","answer_value":"-472"},{"collection_id":"364","label":"#fffefe","description":null,"image_default":"","image_selected":"","answer_value":"-428"},{"collection_id":"-429","label":"#0c4c8a","description":null,"image_default":"","image_selected":"","answer_value":"-429"},{"collection_id":"-430","label":"#576331","description":null,"image_default":"","image_selected":"","answer_value":"-430"},{"collection_id":"-431","label":"#041531","description":null,"image_default":"","image_selected":"","answer_value":"-431"},{"collection_id":"-432","label":"#de443a","description":null,"image_default":"","image_selected":"","answer_value":"-432"},{"collection_id":"-433","label":"#bde4f5","description":null,"image_default":"","image_selected":"","answer_value":"-433"},{"collection_id":"-434","label":"#ffbbc7","description":null,"image_default":"","image_selected":"","answer_value":"-434"},{"collection_id":"-435","label":"#0a6148","description":null,"image_default":"","image_selected":"","answer_value":"-435"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_color_avoid","label":"","instruction_text":"","answer_key":"","template_type":"4","template_sub_type":"2","min_selection":"0","max_selection":"0","selection_color":"","collection":[{"collection_id":"516","label":"I like all colors","description":null,"image_default":"","image_selected":"","answer_value":"516"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_print_avoid","label":"PRINTS AND PATTERNS I AVOID","instruction_text":"(Pick 1 Or More)","answer_key":"4","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#ED1B2E","collection":[{"collection_id":"-198","label":"polka dots","description":"polka dots","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/female/style/prints/polka.jpg","image_selected":"","answer_value":"-198"},{"collection_id":"-184","label":"floral","description":"floral","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/prints/1floral.jpg","image_selected":"","answer_value":"-184"},{"collection_id":"-199","label":"stripes","description":"stripes","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/female/style/prints/stripes.jpg","image_selected":"","answer_value":"-199"},{"collection_id":"-535","label":"quirky","description":"quirky","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/prints/1quirky.jpg","image_selected":"","answer_value":"-535"},{"collection_id":"-253","label":"checks","description":"checks","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/prints/1check.jpg","image_selected":"","answer_value":"-253"},{"collection_id":"-190","label":"houndstooth","description":"houndstooth","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/prints/1hound.jpg","image_selected":"","answer_value":"-190"},{"collection_id":"-255","label":"solid","description":"solid","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/female/style/prints/solid.jpg","image_selected":"","answer_value":"-255"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"men_commentbox","label":"COMMENT BOX","instruction_text":"","answer_key":"","template_type":"5","template_sub_type":"1","min_selection":"0","max_selection":"0","selection_color":"","collection":[{"collection_id":"378","label":"IS THERE ANYTHING WE MISSED?","description":"","image_default":"","image_selected":"","answer_value":"378"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]},{"category_id":"63","name":"Bags","questions":[{"question_id":"bag_type","label":"THE KIND OF BAG I AM LOOKING FOR","instruction_text":"(Pick 1 Or More)","answer_key":"","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#13D792","collection":[{"collection_id":"64","label":"wallets","description":"wallets","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/bags/1wallet.jpg","image_selected":"","answer_value":"64"},{"collection_id":"65","label":"messenger bags","description":"messenger bags","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/bags/1messenger.jpg","image_selected":"","answer_value":"65"},{"collection_id":"66","label":"laptop bags","description":"laptop bags","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/bags/1laptop.jpg","image_selected":"","answer_value":"66"},{"collection_id":"67","label":"backpacks","description":"backpacks","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/bags/1backpack.jpg","image_selected":"","answer_value":"67"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]},{"category_id":"68","name":"Footwear","questions":[{"question_id":"footwear_type","label":"THE TYPE OF SHOES I AM LOOKING FOR ","instruction_text":"(Pick 1 Or More)","answer_key":"","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#13D792","collection":[{"collection_id":"69","label":"flip flops & sandals","description":"flip flops & sandals","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/shoe/1flip.jpg","image_selected":"","answer_value":"69"},{"collection_id":"70","label":"sneakers","description":"sneakers","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/shoe/1sneaker.jpg","image_selected":"","answer_value":"70"},{"collection_id":"71","label":"casual shoes","description":"casual shoes","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/shoe/1casual.jpg","image_selected":"","answer_value":"71"},{"collection_id":"72","label":"loafers","description":"loafers","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/shoe/1loafer.jpg","image_selected":"","answer_value":"72"},{"collection_id":"73","label":"formal shoes","description":"formal shoes","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/shoe/1formal.jpg","image_selected":"","answer_value":"73"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"footwear_size","label":"FOOTWEAR SIZE","instruction_text":"(Pick One)","answer_key":"54","template_type":"3","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"161","label":"select footwear size","description":"select footwear size","image_default":"","image_selected":"","answer_value":"161"},{"collection_id":"478","label":"eu 38/uk 5/us 6","description":"eu 38/uk 5/us 6","image_default":"","image_selected":"","answer_value":"478"},{"collection_id":"479","label":"eu 40/uk 6/us 7","description":"eu 40/uk 6/us 7","image_default":"","image_selected":"","answer_value":"479"},{"collection_id":"480","label":"eu 41/uk 7/us 8","description":"eu 41/uk 7/us 8","image_default":"","image_selected":"","answer_value":"480"},{"collection_id":"481","label":"42.5/uk 8/us 9","description":"42.5/uk 8/us 9","image_default":"","image_selected":"","answer_value":"481"},{"collection_id":"482","label":"44/uk 9/us 10","description":"44/uk 9/us 10","image_default":"","image_selected":"","answer_value":"482"},{"collection_id":"483","label":"45/uk 10/us 11","description":"45/uk 10/us 11","image_default":"","image_selected":"","answer_value":"483"},{"collection_id":"484","label":"46/uk 11/us 12","description":"46/uk 11/us 12","image_default":"","image_selected":"","answer_value":"484"},{"collection_id":"485","label":"47.5/uk 12/us 13","description":"47.5/uk 12/us 13","image_default":"","image_selected":"","answer_value":"485"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]},{"category_id":"74","name":"Accessories","questions":[{"question_id":"accessories_types","label":"ACCESSORIES","instruction_text":"(Pick One)","answer_key":"","template_type":"4","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"75","label":"sunglasses","description":"sunglasses","image_default":"","image_selected":"","answer_value":"75"},{"collection_id":"76","label":"belts","description":"belts","image_default":"","image_selected":"","answer_value":"76"},{"collection_id":"77","label":"caps","description":"caps","image_default":"","image_selected":"","answer_value":"77"},{"collection_id":"176","label":"pocket square","description":"pocket square","image_default":"","image_selected":"","answer_value":"78"},{"collection_id":"79","label":"ties and bow ties","description":"ties and bow ties","image_default":"","image_selected":"","answer_value":"79"},{"collection_id":"80","label":"lapel and collar pins","description":"lapel and collar pins","image_default":"","image_selected":"","answer_value":"80"},{"collection_id":"81","label":"cuff links","description":"cuff links","image_default":"","image_selected":"","answer_value":"81"},{"collection_id":"82","label":"jewellery","description":"jewellery","image_default":"","image_selected":"","answer_value":"82"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]},{"category_id":"","name":"Comments","questions":[{"question_id":"men_commentbox","label":"COMMENT BOX","instruction_text":"","answer_key":"","template_type":"5","template_sub_type":"1","min_selection":"1","max_selection":"","selection_color":"","collection":[{"collection_id":" ","label":"IS THERE ANYTHING WE MISSED?","description":"","image_default":"","image_selected":"","answer_value":" "}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]}]},"post_payment":{"categories":[{"category_id":"40","name":"Apparel","questions":[{"question_id":"apparel_height_feet","label":"How Tall Are You?","instruction_text":"(Pick One)","answer_key":"61","template_type":"3","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"392","label":"Select height in feet","description":"Select height in feet","image_default":"","image_selected":"","answer_value":"392"},{"collection_id":"517","label":"3 ft","description":"3 ft","image_default":"","image_selected":"","answer_value":"517"},{"collection_id":"518","label":"4 ft","description":"4 ft","image_default":"","image_selected":"","answer_value":"518"},{"collection_id":"519","label":"5 ft","description":"5 ft","image_default":"","image_selected":"","answer_value":"519"},{"collection_id":"195","label":"6 ft","description":"6 ft","image_default":"","image_selected":"","answer_value":"520"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"apparel_height_inches","label":"","instruction_text":"","answer_key":"62","template_type":"3","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"393","label":"Select height in inches","description":"Select height in inches","image_default":"","image_selected":"","answer_value":"393"},{"collection_id":"521","label":"0 inch","description":"0 inch","image_default":"","image_selected":"","answer_value":"521"},{"collection_id":"522","label":"1 inch","description":"1 inch","image_default":"","image_selected":"","answer_value":"522"},{"collection_id":"523","label":"2 inch","description":"2 inch","image_default":"","image_selected":"","answer_value":"523"},{"collection_id":"524","label":"3 inch","description":"3 inch","image_default":"","image_selected":"","answer_value":"524"},{"collection_id":"525","label":"4 inch","description":"4 inch","image_default":"","image_selected":"","answer_value":"525"},{"collection_id":"526","label":"5 inch","description":"5 inch","image_default":"","image_selected":"","answer_value":"526"},{"collection_id":"527","label":"6 inch","description":"6 inch","image_default":"","image_selected":"","answer_value":"527"},{"collection_id":"528","label":"7 inch","description":"7 inch","image_default":"","image_selected":"","answer_value":"528"},{"collection_id":"529","label":"8 inch","description":"8 inch","image_default":"","image_selected":"","answer_value":"529"},{"collection_id":"530","label":"9 inch","description":"9 inch","image_default":"","image_selected":"","answer_value":"530"},{"collection_id":"531","label":"10 inch","description":"10 inch","image_default":"","image_selected":"","answer_value":"531"},{"collection_id":"531","label":"11 inch","description":"11 inch","image_default":"","image_selected":"","answer_value":"531"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]},{"category_id":"63","name":"Bags","questions":[{"question_id":"bag_color_avoid","label":"COLORS I AVOID","instruction_text":"(Pick 1 Or More)","answer_key":"2","template_type":"2","template_sub_type":"1","min_selection":"1","max_selection":"","selection_color":"#ED1B2E","collection":[{"collection_id":"-421","label":"#8bf5cd","description":null,"image_default":"","image_selected":"","answer_value":"-421"},{"collection_id":"-422","label":"#89aad5","description":null,"image_default":"","image_selected":"","answer_value":"-422"},{"collection_id":"-423","label":"#bf94e4","description":null,"image_default":"","image_selected":"","answer_value":"-423"},{"collection_id":"-424","label":"#aeb0ae","description":null,"image_default":"","image_selected":"","answer_value":"-424"},{"collection_id":"-405","label":"#734021","description":null,"image_default":"","image_selected":"","answer_value":"-405"},{"collection_id":"-425","label":"#2082ef","description":null,"image_default":"","image_selected":"","answer_value":"-425"},{"collection_id":"-407","label":"#0b0706","description":null,"image_default":"","image_selected":"","answer_value":"-407"},{"collection_id":"-426","label":"#68dbd6","description":null,"image_default":"","image_selected":"","answer_value":"-426"},{"collection_id":"-427","label":"#f5dc05","description":null,"image_default":"","image_selected":"","answer_value":"-427"},{"collection_id":"-428","label":"#fffefe","description":null,"image_default":"","image_selected":"","answer_value":"-428"},{"collection_id":"-429","label":"#0c4c8a","description":null,"image_default":"","image_selected":"","answer_value":"-429"},{"collection_id":"-430","label":"#576331","description":null,"image_default":"","image_selected":"","answer_value":"-430"},{"collection_id":"-431","label":"#041531","description":null,"image_default":"","image_selected":"","answer_value":"-431"},{"collection_id":"-432","label":"#de443a","description":null,"image_default":"","image_selected":"","answer_value":"-432"},{"collection_id":"-433","label":"#bde4f5","description":null,"image_default":"","image_selected":"","answer_value":"-433"},{"collection_id":"-434","label":"#ffbbc7","description":null,"image_default":"","image_selected":"","answer_value":"-434"},{"collection_id":"-435","label":"#0a6148","description":null,"image_default":"","image_selected":"","answer_value":"-435"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"bag_color_avoid","label":"","instruction_text":"","answer_key":"","template_type":"4","template_sub_type":"2","min_selection":"0","max_selection":"0","selection_color":"","collection":[{"collection_id":"516","label":"I like all colors","description":null,"image_default":"","image_selected":"","answer_value":"516"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"bag_print_avoid","label":"PRINTS AND PATTERNS I AVOID","instruction_text":"(Pick 1 Or More)","answer_key":"4","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#ED1B2E","collection":[{"collection_id":"-198","label":"polka dots","description":"polka dots","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/female/style/prints/polka.jpg","image_selected":"","answer_value":"-198"},{"collection_id":"-184","label":"floral","description":"floral","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/prints/1floral.jpg","image_selected":"","answer_value":"-184"},{"collection_id":"-199","label":"stripes","description":"stripes","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/female/style/prints/stripes.jpg","image_selected":"","answer_value":"-199"},{"collection_id":"-535","label":"quirky","description":"quirky","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/prints/1quirky.jpg","image_selected":"","answer_value":"-535"},{"collection_id":"-253","label":"checks","description":"checks","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/prints/1check.jpg","image_selected":"","answer_value":"-253"},{"collection_id":"-190","label":"houndstooth","description":"houndstooth","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/male/prints/1hound.jpg","image_selected":"","answer_value":"-190"},{"collection_id":"-255","label":"solid","description":"solid","image_default":"https://www.stylecracker.com/assets/seventeen/images/pa/female/style/prints/solid.jpg","image_selected":"","answer_value":"-255"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]},{"category_id":"74","name":"Accessories","questions":[{"question_id":"accessories_sunglasses_type","label":"YOU\'RE LOOKING FOR SUNGLASSES! TELL US WHAT KIND? SUNGLASSES","instruction_text":"(Pick 1 Or More)","answer_key":"","template_type":"1","template_sub_type":"2","min_selection":"1","max_selection":"","selection_color":"#13D792","collection":[{"collection_id":"97","label":"aviators","description":"aviators","image_default":"http://www.scnest.com/assets/seventeen/images/pa/female/style/sunglass/aviator.jpg","image_selected":"","answer_value":"97"},{"collection_id":"99","label":"round","description":"round","image_default":"http://www.scnest.com/assets/seventeen/images/pa/female/style/sunglass/round.jpg","image_selected":"","answer_value":"99"},{"collection_id":"100","label":"square","description":"square","image_default":"http://www.scnest.com/assets/seventeen/images/pa/female/style/sunglass/square.jpg","image_selected":"","answer_value":"100"},{"collection_id":"98","label":"wayfarers","description":"wayfarers","image_default":"http://www.scnest.com/assets/seventeen/images/pa/female/style/sunglass/wayfarer.jpg","image_selected":"","answer_value":"98"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}},{"question_id":"accessories_sunglasses_refelector_type","label":"REFELECTORS","instruction_text":"(Pick Yes Or No)","answer_key":"56","template_type":"4","template_sub_type":"1","min_selection":"1","max_selection":"1","selection_color":"","collection":[{"collection_id":"225","label":"Yes","description":"Yes","image_default":"","image_selected":"","answer_value":"486"},{"collection_id":"487","label":"No","description":"No","image_default":"","image_selected":"","answer_value":"487"}],"conditional_logic":{"operator":"","rules":{"field":"","operator":"","value":""}}}]}]}}]';	
		$data = json_decode($render_pa_questions, true);	
        $this->success_response($data);                    

	}

	
	
}