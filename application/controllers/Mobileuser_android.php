<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';

class Mobileuser_android extends TT_REST_Controller 
{
  function __construct()
	{
		parent::__construct();
		$this->load->model('Schome_model');
		$this->load->model('Mobile_model_android');
		$this->load->model('Pa_model');
		$this->load->model('Mobile_pa_model');
		$this->load->library('email');
		$this->load->library('curl');
		$this->open_methods = array('register_user_post','login_post','contact_us_post','upload_profile_post');    
		//$this->open_methods = array('getuserinfo_get');
		 //$this->open_methods = array('pa');
		//$this->load->library('../controllers/Schome');
		//$this->open_methods = array('login_post');
	}
    
  public function register_user_post(){
    // if ($this->input->is_ajax_request()) {

      $scusername = $this->post('sc_username_name');
      $scemail_id = $this->post('sc_email_id');
      $scpassword = $this->post('sc_password');
       // $firstname = $this->post('firstname');
       // $lastname = $this->post('lastname');
	  $medium = $this->post('medium');
      if($medium == '') $medium = 'mobile';
      $medium_id =  $this->post('medium_id');
	  $gender =  $this->post('gender');
	  $contact_no = $this->post('contact_no');
	  $birth_date = $this->post('birth_date');
	  $version = $this->post('version');
	  if($version == ''){ $version = 0; }
	  $version =2;
	  $gcm_id = '';
	  $gcm_id = $this->post('gcm_id');
	  if($gender == 'male' || $gender == 'men'){ 
		$gender = 2; 
	  }else if($gender == 'female' || $gender == 'women'){ 
		$gender = 1;
	  }else $gender = 1;
	  if($gender == '') $gender = 1;
	  $skip = 0;
		/*
		$scusername=""; 
        $scusername.=$firstname; 
		$scusername.=$lastname; 
      
      $scusername.= date('Y/m/dH:i:s');
		*/
	  if($medium == 'mobile_facebook' || $medium == 'mobile_ios_facebook' || $medium == 'mobile_android_facebook'){ $skip  = 1;}
	   /* added for treating old user as women*/
		if($this->emailUnique($scemail_id) > 0){
			$gender = $this->Mobile_model_android->get_gender_by_email($scemail_id); 
		}else{
			// $email_sent = $this->send_welcome_email($scemail_id);
			$this->Schome_model->update_couponcode($scemail_id,'Welcome10');
		}
	  /* added for treating old user as women*/
      if(trim($scusername) == ''){
        $this->failed_response(1005, "Please enter the username");

      }else if(trim($scemail_id) == ''){
        $this->failed_response(1006, "Please enter the Email-id");

      }else if(trim($scpassword) == ''){
        $this->failed_response(1007, "Please enter the Password");

      }else if(filter_var($scemail_id, FILTER_VALIDATE_EMAIL) === false){
        $this->failed_response(1008, "Please enter valid Email-id");

      }else if($this->emailUnique($scemail_id) > 0 && $skip == 0){
        $this->failed_response(1010, "Email ID already exists.");
		
      }else if(strlen($scpassword) < 8 && $skip == 0){
        $this->failed_response(1013, "Password should have atleast 8 character");
		
	  }else if(strlen($scpassword) > 20 && $skip == 0){
        $this->failed_response(1014, "Password should not greater than 20 character");
		
	  }else if($version < 1){
        $this->failed_response(1010, "Please update your app");
		
      }else{
      
    	$name = str_replace(' ', '', $scusername);
    	$random = md5(uniqid(rand(0,2), true));
    	$random = substr($random, 0, 5);
		$firstname_split = explode(' ',$scusername);
		$firstname = $firstname_split[0];
		$last_name = @$firstname_split[1];
    	$scusername = $name.$random;
    	
      	$ip = $this->input->ip_address();
        $res = $this->Mobile_model_android->registerUserMobile($firstname,$medium,$medium_id,$scusername,$scemail_id,$scpassword,$gender,$ip,2,$contact_no,$birth_date,$last_name);
		if(($res != NULL)) { 
			$data = new stdClass();
			$data->user_info= $this->getuserinfo_get($res[0]['user_id']);//return value of get_users_info_mobile function
			$update_gcm_id = $this->Mobile_model_android->update_gcm_id($gcm_id,$res[0]['user_id']);
			$gender = $this->Pa_model->get_gender($res[0]['user_id']);
			if($res[0]['user_id'] > 0)
			{  
				if($this->Schome_model->get_nonscdomain($scemail_id))
				{ 
					$result = $this->curl->simple_post(BACKENDURL.'/Mailchimp/newUserImport/'.$res[0]['user_id'], false, array(CURLOPT_USERAGENT => true)); 
				}       
			}
			$data->pa= $this->pa_get($res[0]['user_id'],$gender);
			$this->success_response($data);
		}
        else{ 
          $this->failed_response(1011, "Registration failed!");
          // return '0'; 
        }
    //mobile_google, mobile, mobile_facebook
      }


    // }else{
      // return 'Somethings went wrong';
    // }

  }
	
	public function user_details_post()
	{
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		//$data = new stdClass();
		
		$data = new stdClass();
    	$data->user_info = $this->getuserinfo_get($user_id);
		$data->pa = $this->pa_get($user_id);
	   if($data != NULL){
		$this->success_response($data);
	   }
	   else
			$this->failed_response(1001, "No Record(s) Found");
		
	}
	
	public function wishlist_ids_post()
	{
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		$data = $this->Mobile_model_android->get_look_id($user_id,$auth_token);
		if($data != NULL){
			//$data['user_id'] = $user_id;
			//$data['token'] = $auth_token;
			$this->success_response($data);
		}else
			$this->failed_response(1001, "No Record(s) Found");
			
	}
	
	public function wishlist_looks_post()
	{
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');		
		$limit= $this->post('limit');
        $offset = $this->post('offset');
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);
		$data = $this->Mobile_model_android->get_wishlist_looks($user_id,$limit,$offset);
		
		if($data != NULL){
			//$data->user_id = $user_id;
			//$data->token = $auth_token;
			$this->success_response($data);
		}
		else
			$data = array();	
			$this->success_response($data);
			//$this->failed_response(1001, "No Record(s) Found");
			
	}
	
	
	public function wishlist_looks_new_post()
	{
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');		
		$limit= $this->post('limit');
        $offset = $this->post('offset');
		$bucket_id = $this->post('bucket_id');
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);
		$data['looks'] = $this->Mobile_model_android->get_wishlist_looks($user_id,$limit,$offset);
		if(!empty($data['looks'])){
			$data['message'] = '';
			$this->success_response($data);
		}else{
			$data['message'] = " You haven't added any looks to your wishlist, here are three we think you might like ";
			$data['looks'] = $this->Mobile_model_android->show_recommended_looks($bucket_id);	
			$this->success_response($data);
			//$this->failed_response(1001, "No Record(s) Found");
		}
	}
	
	public function update_mobile_pa_post()
	{
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');		
		$json_string = $this->post('json_string');
		$gender = '1';
		//extract the json string and send success response+
		$data_answers = array('user_body_shape'=>1,'user_style'=>2,'user_age'=>8,'user_budget'=>7,'user_size'=>9);
		$jsondecode = json_decode($json_string, true);
		foreach($jsondecode as $key=>$json){
			$question = $json['question'];
			$question_id = $data_answers[$question];
			$anser_id = $json['anser_id'];
			$this->Mobile_pa_model->update_mobile_pa($question_id,$anser_id,$user_id,$gender);
		}
		$data = new stdClass();
		$data->user_info = $this->getuserinfo_get($user_id);
		$data->pa = $this->pa_get($user_id,$gender);
		//$data->user_id = $user_id;
		//$data->auth_token = $auth_token;
		$this->success_response($data);
	}
	
	
	public function getuserinfo_get($user_id)
  {
    if($user_id){
      $user_info = (object)$this->Mobile_model_android->get_users_info_mobile($user_id);
      return $user_info;
    }else{
      $this->failed_response(1001, "No Record(s) Found");
    }
  }
    
  public function pa_get($user_id,$gender='')
  {
    
    if($user_id){
		if($gender == '1'){
			$data = new stdClass();
			$data -> body_shape = $this->Mobile_pa_model->get_question_img_answers_mobile(1,$user_id);
			$data -> style = $this->Mobile_pa_model->get_question_img_answers_mobile(2,$user_id);
			$data -> age = $this->Mobile_pa_model->get_question_img_answers_mobile(8,$user_id);
			$data -> budget = $this->Mobile_pa_model->get_question_img_answers_mobile(7,$user_id);
			$data -> size = $this->Mobile_pa_model->get_question_img_answers_mobile(9,$user_id);
			
			$data -> user_body_shape = $this->Mobile_pa_model->get_users_answer_mobile(1,$user_id);
			$data -> user_style = $this->Mobile_pa_model->get_users_answer_mobile(2,$user_id);
			$data -> user_age = $this->Mobile_pa_model->get_users_answer_mobile(8,$user_id);
			$data -> user_budget = $this->Mobile_pa_model->get_users_answer_mobile(7,$user_id);
			$data -> user_size = $this->Mobile_pa_model->get_users_answer_mobile(9,$user_id);
			$data -> question_resume_id = $this->Mobile_pa_model->get_question_resume_id_mobile($user_id);
			$data -> edited_data ='';
			return $data;
		} // end of women pa
		else {
			$data = new stdClass();
			$data->body_shape = $this->Mobile_pa_model->get_question_answers_mobile(13,$user_id);
			$data->style = $this->Mobile_pa_model->get_question_answers_mobile(14,$user_id);
			$data->age = $this->Mobile_pa_model->get_question_img_answers_mobile_men(15,$user_id);
			$data->personal_style = $this->Mobile_pa_model->get_question_img_answers_mobile_men(16,$user_id);
			$data->budget = $this->Mobile_pa_model->get_question_answers_mobile(18,$user_id);
			$data->size = $this->Mobile_pa_model->get_question_answers_mobile(17,$user_id);
			
			$data->shirts = $this->Mobile_pa_model->get_question_answers_mobile(19,$user_id);
			$data->jeans = $this->Mobile_pa_model->get_question_answers_mobile(20,$user_id);
			$data->polos = $this->Mobile_pa_model->get_question_answers_mobile(21,$user_id);
			$data->jackets = $this->Mobile_pa_model->get_question_answers_mobile(22,$user_id);
			$data->t_shirt = $this->Mobile_pa_model->get_question_answers_mobile(23,$user_id);
			
			// $data->quotient1 = $this->Mobile_pa_model->get_questions(19);
			// $data->quotient2 = $this->Mobile_pa_model->get_questions(20);
			// $data->quotient3 = $this->Mobile_pa_model->get_questions(21);
			// $data->quotient4 = $this->Mobile_pa_model->get_questions(22);
			
			$data->user_body_shape = $this->Mobile_pa_model->get_users_answer_mobile(13,$user_id);
			$data->user_style = $this->Mobile_pa_model->get_users_answer_mobile(14,$user_id);
			$data->user_age = $this->Mobile_pa_model->get_users_answer_mobile(15,$user_id);
			$data->user_personal_style = $this->Mobile_pa_model->get_users_answer_mobile(16,$user_id);
			$data->user_budget = $this->Mobile_pa_model->get_users_answer_mobile(18,$user_id);
			$data->user_size = $this->Mobile_pa_model->get_users_answer_mobile(17,$user_id);
			
			$data->user_shirts = $this->Mobile_pa_model->get_users_answer_mobile(19,$user_id);
			$data->user_jeans = $this->Mobile_pa_model->get_users_answer_mobile(20,$user_id);
			$data->user_polos = $this->Mobile_pa_model->get_users_answer_mobile(21,$user_id);
			$data->user_jackets = $this->Mobile_pa_model->get_users_answer_mobile(22,$user_id);
			$data->user_t_shirt = $this->Mobile_pa_model->get_users_answer_mobile(23,$user_id);
			$data -> question_resume_id = $this->Mobile_pa_model->get_question_resume_id_mobile($user_id);
			$data -> edited_data ='';
			return $data;
			
		}

     }
	else
	{
	$this->failed_response(1001, "No Record(s) Found");
	}

  }
	//update men pa
	public function update_men_pa_post()
	{
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');		
		$json_string = $this->post('json_string');
		$gender = '2';
		//extract the json string and send success response+
		$data_answers = array('user_body_shape'=>13,'user_style'=>14,'user_age'=>15,'user_personal_style'=>16,'user_budget'=>18,'user_size'=>17,'shirts'=>19,'jeans'=>20,'polos'=>21,'jackets'=>22,'t_shirt'=>23);
		$jsondecode = json_decode($json_string, true);
		foreach($jsondecode as $key=>$json){
			$question = $json['question'];
			$question_id = $data_answers[$question];
			$anser_id = $json['anser_id'];
			$this->Mobile_pa_model->update_mobile_pa($question_id,$anser_id,$user_id,$gender);
		}
		$data = new stdClass();
		$data->user_info = $this->getuserinfo_get($user_id);
		$data->pa = $this->pa_get($user_id,$gender);
		//$data->user_id = $user_id;
		//$data->auth_token = $auth_token;
		$this->success_response($data);
	}
	
  public function usernameUnique($username){
      $res = $this->Schome_model->usernameUnique($username);
      return count($res);
  }

  public function emailUnique($email){
      $res = $this->Schome_model->emailUnique($email);
      return count($res);
  }

  public function signup($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$json_array = array()){
      $res = $this->Schome_model->registerUser($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$json_array);
      if($res){ return $res; }else{ return '0'; }
  }

  
   public function login_post(){
		$gcm_id = '';
        $logEmailid = $this->post('sc_email_id');
        $logPassword = $this->post('sc_password');
		$gcm_id = $this->post('gcm_id');
		
		$version = $this->post('version');
		if($version == ''){ $version = 0; }
		$version = 2;
		if($version < 1){
			$this->failed_response(1010, "Please update your app.");
		}
		
         if(trim($logEmailid) == ''){
            echo "Please enter your email id";

          }else if(trim($logPassword) == ''){
            echo "Please enter the Password";

          }else{
           $result = $this->Mobile_model_android->login_user_mobile($logEmailid,$logPassword);

            if($result != NULL && $result != 2){
					
      				if(isset($result[0]['question_comp_mobile'])) $question_comp = $result[0]['question_comp_mobile']; else $question_comp = '';

      				//$question_resume_id = $this->session->userdata('question_resume_id');
      				$data = new stdClass();
      				if($result[0]['bucket_mobile'] == '' || $question_comp!=1 ){
      					$data->redirect_url = 'pa';
      				}else{
      					$data->redirect_url = 'profile';
      				}
					$gender = $this->Mobile_model_android->get_user_gender($result[0]['user_id']);
      				$data->user_info = $this->getuserinfo_get($result[0]['user_id']);//return value of get_users_info_mobile function
					$update_gcm_id = $this->Mobile_model_android->update_gcm_id($gcm_id,$result[0]['user_id']);
      				$data->pa = $this->pa_get($result[0]['user_id'],$gender);
					
					if($result[0]['user_id'] > 0)
					{  
						if($this->Schome_model->get_nonscdomain($logEmailid))
						{ 
							$result = $this->curl->simple_post(BACKENDURL.'/Mailchimp/newUserImport/'.$result[0]['user_id'], false, array(CURLOPT_USERAGENT => true)); 
						}       
					}
					
      				$this->success_response($data);
            }else{
                $this->failed_response(1012, "Please enter a valid email address/password");

            }

          }
  }
	
  /*public function forget_user_email_post(){
    $sc_email_id = $this->post('sc_email_id');
    $sc_user_id = $this->post('user_id');
    $auth_token = $this->post('auth_token');
    $res = $this->Mobile_model_android->forget_user_email_mobile($sc_email_id,$sc_user_id);     
    if($res === 1){
      $data = new stdClass();
      $data->user_id = $sc_user_id;
      $data->auth_token = $auth_token;
      $this->success_response($data);
    }else{
       $this->failed_response(1012, "Authorization failed!");
    }

  }*/

  public function change_password_post(){
    $sc_user_id = $this->post('user_id');
    $auth_token = $this->post('auth_token');
    $oldPassword = $this->input->post('oldPassword');
    $newPassword = $this->input->post('newPassword');
	if(trim($newPassword) == ''){
        $this->failed_response(1007, "Please enter the Password");

    }
	else if(strlen($newPassword) < 8 ){
        $this->failed_response(1013, "Password should have atleast 8 character");
	}else if(strlen($newPassword) > 20){
        $this->failed_response(1013, "Password should not greater than 20 character");
	}
    $res = $this->Mobile_model_android->change_password($sc_user_id,$oldPassword,$newPassword); 
    if($res === 1){

      $data = new stdClass();
      $data->message = 'success';
      $this->success_response($data);
    }else{
      $this->failed_response(1300, "Wrong credentials!");
    }

  }
  
  public function forget_user_email_get(){
	$sc_email_id = $this->get('sc_email_id');
	$res = $this->Mobile_model_android->forget_user_email_mobile($sc_email_id);
	if($res != false){
		if($this->send_forgotpwd_email_mobile($sc_email_id,$res)) 
		{
			   $data = new stdClass();
		       $data->message = 'New Password has been Send to your email-id '.$sc_email_id;
			   $this->success_response($data);
		}
		else{
		  $this->failed_response(1017, "Invalid Email-id ");
		}	
	}else{
		$this->failed_response(1012, "Authorization failed!");
	}
	
  }
  
   public function contact_us_post(){

	$data = array();
	$contact_name = $this->post('contact_name');
	$email_id = $this->post('email_id');
	$subject = $this->post('subject');
	$message = $this->post('message');
	
	if($contact_name!='' && $email_id!='' && $subject!='' && $message!=''){
		$data = array('name' => $contact_name,'email'=>$email_id,'message' => $subject.' '.$message,'enquiry_date_time'=>$this->config->item('sc_date'));
		$res = $this->Mobile_model_android->add_contact_us($data);
		
		if($res == 1)
		{
		
		// $query->message = 'success';
			$data = new stdClass();
			$data->message = 'success';
			$this->success_response($data);
		
		}
		else
		{
			$this->failed_response(1012, "Authorization failed!");
		}
	}
  }

	function checkLimit($limit){
		if($limit == '') $query_limit = ''; else $query_limit = " LIMIT $limit ";
		return $query_limit;
	}
	
	function checkOffset($offset){
		if($offset == '') $query_offset = ''; else $query_offset = " OFFSET $offset ";
		return $query_offset;
	}  
	
	public function send_forgotpwd_email_mobile($email_id,$new_password){

      $config['protocol'] = 'smtp';
//      $config['mailpath'] = '/usr/sbin/sendmail';
 //     $config['charset'] = 'iso-8859-1';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');

      $this->email->initialize($config);

      $this->email->from($this->config->item('from_email'), 'Stylecracker');
      if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){        
	      $this->email->to($this->config->item('sc_test_emaild'));
	  }else{
     	 $this->email->to($email_id);
      }
      $this->email->subject('StyleCracker: New Password');
      $this->email->message("Your login details are <br> Email : ".$email_id."<br><br>Your new password is:".$new_password."<br><br>You can change your password at any time by logging into your account. <br><br>Warm Regards,<br>StyleCracker");

      if($this->email->send())
      {
        return "Email Sent";
      }else
      {
        return "Email Error";
      }
      
  }
  
  public function upload_profile_post(){

		$status = "";
    	$msg = "";
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
    	$file_element_name = 'profile_img';
		
		//$post = file_get_contents('php://input');
		//print_r($post); exit;
		if ($status != "error")
    	{
			$config['upload_path'] = $this->config->item('profile_image_path');
			//$config['upload_path']= 'D:\xampp\htdocs\stylecracker\assets\profileimages';
	        $config['allowed_types'] = 'JPG|jpg|png|jpge|PNG';
	        $config['max_size'] = 1024 * 8;
	        $config['file_name'] = time();
			//echo $config['upload_path']; exit; 

	        $this->load->library('upload', $config);
	        if (!$this->upload->do_upload($file_element_name))
	        {
	            $status = 'error';
	            $msg = $this->upload->display_errors('', '');
				//echo $msg."image is not uploading";exit;
				
	        }
	        else
	        {
	            $data = $this->upload->data();
	            $result = $this->Mobile_model_android->update_profile_img(($data['file_name']),$user_id);
	            if($this->Mobile_model_android->update_profile_img(($data['file_name']),$user_id))
	            {
	                $status = "success";
	                $msg = "File successfully uploaded";
	            }
	            else
	            {
	                $status = "error";
	                $msg = "Something went wrong when saving the file, please try again.";
	            }

	        }
    	}
			$data = new stdClass();
			$data = $this->Mobile_model_android->get_profile_img($user_id);
			$data = (object)($data[0]);
			//$data->pa = $this->pa_get($result[0]['user_id']);
			
			$this->success_response($data);
		
	}
	
	
	public function cover_image_post(){
		
		$status = "";
    	$msg = "";
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
    	$file_element_name = 'cover_img';

		if ($status != "error")
    	{
	        $config['upload_path'] = $this->config->item('cover_image_path');
			//$config['upload_path']= 'D:\xampp\htdocs\stylecracker\assets\coverimages';
	        $config['allowed_types'] = 'JPG|jpg|png|jpge|PNG';
	       // $config['max_size'] = 1024 * 8;
	        $config['file_name'] = time();


	        $this->load->library('upload', $config);

	        if (!$this->upload->do_upload($file_element_name))
	        {
	            $status = 'error';
	            $msg = $this->upload->display_errors('', '');
	             $response = array(
				"status" => 'error',
				"message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
				);
	        }
	        else
	        {
	            $data = $this->upload->data();
	            $width = $data['image_width'];
	            $height = $data['image_height'];
	            $fileext = $data['file_ext'];
	            //$file_id = $this->Mobile_model_android->update_profile_img(($data['file_name']),$user_id);
	            if($this->Mobile_model_android->update_cover_img(($data['file_name']),$user_id))
	            {
	                 $response = array(
					"status" => 'success',
					"url" => $this->config->item('cover_image_path').$config['file_name'].$fileext,
					"width" => $width,
					"height" => $height
				    );
	            }
	            else
	            {
	                 $response = array(
					"status" => 'error',
					"message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
					);
	            }

	        }
    	}
			
			$data = new stdClass();
			$data = $this->Mobile_model_android->get_cover_img($user_id);
			$data = (object)($data[0]);
			//$data->pa = $this->pa_get($result[0]['user_id']);
			
			$this->success_response($data);

		//echo json_encode($response);
	}

	
	public function upload_wordrobe_post(){
		$status = "";
    	$msg = "";
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		$file_element_name = 'upload_photoes[]';
		$product_name = $this->post('product_name');
		// code for tag
		//$tags = $this->post('tag');
		$tags = urldecode($this->post('tag'));
		$this->load->library('image_lib');		
		if ($status != "error")
    	{
	        $files = $_FILES;
			
			if(empty($files)){
					$this->failed_response(1201, "Please Upload Photo and try again!");
			}
			else{
    			$ext = pathinfo($files['upload_photoes']['name'], PATHINFO_EXTENSION);
		       		if($ext == 'JPG' || $ext == 'jpg' || $ext == 'png' || $ext == 'jpge' || $ext == 'PNG' ){
						$img_name = time().mt_rand(0,10000).'.'.$ext;
						$pathAndName = $this->config->item('wordrobe_path').$img_name;
						$moveResult = move_uploaded_file($files['upload_photoes']['tmp_name'], $pathAndName);
						//$this->createThumbs( $pathAndName, $this->config->item('wordrobe_thumb_path'), 100 ,$img_name);
						$config['image_library'] = 'gd2';
						$config['source_image'] = $pathAndName;
						$config['new_image'] =  $this->config->item('wordrobe_thumb_path');
						$config['create_thumb'] = TRUE;
						$config['thumb_marker'] = '';
						$config['width'] = 180;
						$config['height'] = 180;

						//$this->load->library('image_lib', $config);
						$this->image_lib->initialize($config);

						if ( ! $this->image_lib->resize())
						{
    						echo $this->image_lib->display_errors();
						}
						$data = new stdClass();
						$data = (object)$this->Mobile_model_android->upload_wordrobe_img($img_name,$user_id,$tags,$product_name);
					}
				}
    	}
			$this->success_response($data);

	}
	
	public function get_wordrobe_post(){
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		$data = $this->Mobile_model_android->get_wardrobe_img($user_id);
		$this->success_response($data);
	}
	
	public function delete_wordrobe_post(){
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		$wordrobe_id = $this->post('wordrobe_id');
		$res = $this->Mobile_model_android->delete_wardrobe_img($user_id,$wordrobe_id);
		//echo $res;exit;
		if($res == '1'){
		$data = new stdClass();
		$data->message = 'success';
		$this->success_response($data);
		}
		else{
		$this->failed_response(1300, "Wrong credentials!");
   		}
	}
	
	public function tags_get(){
		$data = $this->Mobile_model_android->get_all_tags();
		
		if($data != NULL){
			  
			$this->success_response($data);
		}
		else	
		
			$this->failed_response(1001, "No Record(s) Found");	
	}
	
	public function user_gender_post(){
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		$gender = $this->post('gender');
		if($gender == 'men') $gender = 2;else $gender = 1;
		$res = $this->Mobile_model_android->update_user_gender($user_id,$gender);
		$data = new stdClass();
		$data->user_info = $this->getuserinfo_get($user_id);//return value of get_users_info_mobile function
		$data->pa = $this->pa_get($user_id,$gender);
		$this->success_response($data);
	}  
	
	public function insert_gcm_id_get(){
		$gcm_id = $this->get('gcm_id');
		$user_id = $this->get('user_id');
		$res = $this->Mobile_model_android->insert_gcm_id($gcm_id,$user_id);
		$data = new stdClass();
		$data->message = 'success';
		$this->success_response($data);
	}
	
	public function send_welcome_email($email){

      $config['protocol'] = 'smtp';
//      $config['mailpath'] = '/usr/sbin/sendmail';
      $config['charset'] = 'iso-8859-1';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');
	 
	  // added for confiem email
	  $email_en = base64_encode($email);
	  $user_id_en = base64_encode($this->session->userdata('user_id'));
	  // $link = base_url()."schome/confirm_email/".$email_en."";
	  // $email = 'rajesh@stylecracker.com';
	  
      $this->email->initialize($config);

      $this->email->from($this->config->item('from_email'), 'Stylecracker');
      if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){        
	      $this->email->to($this->config->item('sc_test_emaild'));
	  }else{
      	  $this->email->to($email);
      }
      $this->email->subject('Welcome to StyleCracker');
      $this->email->message('Hi,<br>Welcome to StyleCracker! <br><br> StyleCracker is India\'s first online styling platform and we are here to make you the best version of you! <br><br> When we started StyleCracker our aim was to make personalized styling available to all. We have a dedicated team of stylists who are online 24/7 waiting to answer ANY style query you may have. Never again will you have to worry about \'what do I wear\'- be it day-to-day or for special occasions, we will be there! <br><br> So, please explore the website and let us know how we can help. It would be great if you could also share your feedback with us, it will help present the best product for you. <br><br> Love, <br>Archana Walavalkar & Dhimaan Shah <br>Founders<br> <a href="'.base_url().'">www.stylecracker.com</a>');

      $this->email->send();

  }
  
	public function wishlist_all_ids_get(){
		
		$user_id = $this->get('user_id');
		$auth_token = 1234;
		$data['look_ids'] = $this->Mobile_model_android->get_look_id($user_id,$auth_token);
		$data['product_ids'] = $this->Mobile_model_android->get_product_wishlist_id($user_id);
		$data['brand_ids'] = $this->Mobile_model_android->get_brand_wishlist_id($user_id);
		if($data != NULL){
			$this->success_response($data);
		}else
			$this->failed_response(1001, "No Record(s) Found");
			
	}
	
	function send_email_to_aliance_get(){
		
		$get_brand_info = $this->Schome_model->get_brand_info();
		// print_r($get_brand_info);exit;
		if(!empty($get_brand_info)){
			foreach($get_brand_info as $val){
				$config['protocol'] = 'smtp';
				$config['charset'] = 'iso-8859-1';
				$config['mailtype'] = 'html';
				$config['wordwrap'] = TRUE;
				$config['smtp_host'] = $this->config->item('smtp_host');
				$config['smtp_user'] = $this->config->item('smtp_user');
				$config['smtp_pass'] = $this->config->item('smtp_pass');
				$config['smtp_port'] = $this->config->item('smtp_port');
				// $email = 'rajesh@stylecracker.com';
				$this->email->initialize($config);
				$email_to = 'alliances@stylecracker.com';
				$this->email->from($this->config->item('from_email'), 'Stylecracker');
				 if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){        
			      $this->email->to($this->config->item('sc_test_emaild'));
			      $this->email->bcc($this->config->item('sc_testcc_emaild'));
			    }else{
					$this->email->to($email_to);
				}
				$this->email->subject('Contract Renewal');
				$this->email->message('Dear all,<br><br>Please note that the contract for "'.$val['company_name'].'" is ending on '.$val['contract_end_date'].'. Please look into it incase we have to renew it.');

				$this->email->send();	
				
			}
		}else {
				$config['protocol'] = 'smtp';
				$config['charset'] = 'iso-8859-1';
				$config['mailtype'] = 'html';
				$config['wordwrap'] = TRUE;
				$config['smtp_host'] = $this->config->item('smtp_host');
				$config['smtp_user'] = $this->config->item('smtp_user');
				$config['smtp_pass'] = $this->config->item('smtp_pass');
				$config['smtp_port'] = $this->config->item('smtp_port');
				$email = 'arun@stylecracker.com';
				$this->email->initialize($config);

				$this->email->from($this->config->item('from_email'), 'Stylecracker');
				 if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){        
			      $this->email->to($this->config->item('sc_test_emaild'));
			    }else{
					$this->email->to($email);
				}
				$this->email->subject('Contract Renewal');
				$this->email->message('Cron running.');
				$this->email->send();	
		}
		
			$data = new stdClass();
			$data->message = 'success';
			$this->success_response($data);
		
	}
	
	public function check_version_get(){
		
		$platform = $this->get('platform');
		
		if($platform == 'android'){
			$data['force_update_version'] = '4.0.0';
			$data['current_version'] = '4.0.0';
			$data['force_update_message'] = 'Please update to new version of StyleCracker app.';
			$data['later_update_message'] = 'A newer version of StyleCracker app is available.';
		}else if($platform == 'ios'){
			$data['force_update_version'] = '4.0';
			$data['current_version'] = '4.1';
			$data['force_update_message'] = 'Please update to new version of StyleCracker app.';
			$data['later_update_message'] = 'A newer version of StyleCracker app is available.';
		}else{
			$data = array();
		}
		$this->success_response($data);
	}
	
	public function get_constants_get(){
		$platform = $this->get('platform');
		
		if($platform == 'android'){
			$data['force_update_version'] = '4.0.0';
			$data['current_version'] = '4.0.0';
			$data['force_update_message'] = 'Please update to new version of StyleCracker app.';
			$data['later_update_message'] = 'A newer version of StyleCracker app is available.';
		}else if($platform == 'ios'){
			$data['force_update_version'] = '4.0';
			$data['current_version'] = '4.1';
			$data['force_update_message'] = 'Please update to new version of StyleCracker app.';
			$data['later_update_message'] = 'A newer version of StyleCracker app is available.';
		}
		$data['referal_point_value'] = POINT_VAlUE;
		$data['referal_min_cart_value'] = MIN_CART_VALUE;
		$data['bms_discount'] = bms_discount;
		$data['bms_amt_limit'] = bms_amt_limit;
		$data['bms_popup_title'] = bms_popup_title;
		$data['bms_error_message'] = bms_error_message;
		$this->success_response($data);
	}
  
}

?>