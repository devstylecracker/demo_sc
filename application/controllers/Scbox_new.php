<?php
	//ini_set('max_execution_time', 0);
	//ini_set('memory_limit', '-1');
	
	set_time_limit(0);
defined('BASEPATH') OR exit('No direct script access allowed');
class Scbox_new extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('Schome_model');
		$this->load->model('Pa_model');		
		$this->load->library('email');
		$this->load->driver('cache');
		$this->load->library('user_agent');
		$this->load->library('curl');	
		$this->load->model('Cart_model');
		$this->load->model('cartnew_model');
		$this->load->model('product_desc_model');
		$this->load->model('User_info_model');
		$this->load->model('Scborough_model');
		//$this->config->load('instagram');
		$this->load->library('form_validation');
		$this->load->library('upload');
		//$this->load->library('UploadHandler');
  }

	public function index(){   
    $this->seo_title = 'StyleCracker Box - Buy Online Now on StyleCracker';
    /* added for auto login */
      $user_id = $this->input->get('id');     
      $user_id = base64_decode($user_id);
      $get_user_info = $this->Scborough_model->get_user_info($user_id);
      if(!empty($get_user_info)){
        $res = $this->login($get_user_info[0]['email'],$get_user_info[0]['password']);
      }
    /* added for auto login */
    if(@$_GET['success']==1){
      $this->load->view('seventeen/common/header_view');
      $this->load->view('seventeen/scbox_thankyou');
      $this->load->view('seventeen/common/footer_view');
    }else{
      $this->load->view('seventeen/common/header_view');
      $this->load->view('seventeen/scbox');
      $this->load->view('seventeen/common/footer_view');
    }
  }

  public function step1(){
    $data = array(); 
    $objectid = @$_COOKIE['scbox_objectid'];
    $user_id = $this->session->userdata('user_id');
    $user_profile_data = array();
    if($objectid!='' && $user_id!='' )
    {
      $scbox_data = $this->Pa_model->get_scbox_userdata($objectid,$user_id);
          if(!empty($scbox_data))
          {
            foreach($scbox_data as $val)
            {
              $data['userdata'][$val['object_meta_key']] = $val['object_meta_value'];
            }
          }
        $user_profile_data      =  $this->Pa_model->get_user_profile_pic($user_id);
        $data['user_profile_data']  = $user_profile_data; 
           //echo '<pre>';print_r($data);exit;
    }
    $this->session->set_userdata('scform',$_SERVER['REDIRECT_URL']);
    $this->load->view('seventeen/common/header_view');

    $this->load->view('seventeen/scbox/book-scboxform',$data);
    $this->load->view('seventeen/common/footer_view');
  }

  public function step2(){
    if(@$_COOKIE['scbox_isgift']=='true')
    {
        if(@$_COOKIE['scbox-billgender']=='women'){
          $this->load->view('seventeen/common/header_view');
          $this->load->view('seventeen/scbox/book-scboxwomen');
          $this->load->view('seventeen/common/footer_view');
        }else if(@$_COOKIE['scbox-billgender']=='men'){
          $this->load->view('seventeen/common/header_view');
          $this->load->view('seventeen/scbox/book-scboxmen');
          $this->load->view('seventeen/common/footer_view');
        }    
    }else
    {
        if(@$_COOKIE['scbox-gender']=='women'){
          $this->load->view('seventeen/common/header_view');
          $this->load->view('seventeen/scbox/book-scboxwomen');
          $this->load->view('seventeen/common/footer_view');
        }else if(@$_COOKIE['scbox-gender']=='men'){
          $this->load->view('seventeen/common/header_view');
          $this->load->view('seventeen/scbox/book-scboxmen');
          $this->load->view('seventeen/common/footer_view');
        }    
    }
      
  }

  public function step2a(){    
      $this->load->view('seventeen/common/header_view');
      $this->load->view('seventeen/scbox/book-scboxmen');
      $this->load->view('seventeen/common/footer_view');      
  }
//===For Cart===
  public function step3(){// For cart page
    $this->load->view('seventeen/common/header_view');
    $this->load->view('seventeen/scbox/');
    $this->load->view('seventeen/common/footer_view');
  }

  public function step4(){
	  
	if(@$_COOKIE['scbox_isgift']=='true')
    {
        if(@$_COOKIE['scbox-billgender']=='women'){
           $this->load->view('seventeen/common/header_view');
		  $this->load->view('seventeen/scbox/book-scbox-afterpaymentwomen');
		  $this->load->view('seventeen/common/footer_view');
        }else if(@$_COOKIE['scbox-billgender']=='men'){
          $this->load->view('seventeen/common/header_view');
		  $this->load->view('seventeen/scbox/book-scbox-afterpaymentmen');
		  $this->load->view('seventeen/common/footer_view');
        }    
    }else
    {
		if(@$_COOKIE['scbox-gender']=='women'){  
		  $this->load->view('seventeen/common/header_view');
		  $this->load->view('seventeen/scbox/book-scbox-afterpaymentwomen');
		  $this->load->view('seventeen/common/footer_view');
		}else if(@$_COOKIE['scbox-gender']=='men'){ 
		  $this->load->view('seventeen/common/header_view');
		  $this->load->view('seventeen/scbox/book-scbox-afterpaymentmen');
		  $this->load->view('seventeen/common/footer_view');
		}
	}
  }

  public function step5(){ 
	  $data = array();
	  //$this->load->library('UploadHandler');
	 // $upload_handler = new UploadHandler();
	  /* $validation_config = array(
		 array(
			'field' => 'scbox_missed',
			'label' => 'Comment Box',
			'rules' => 'required',
        ),
	  );*/
  	  //$this->form_validation->set_rules($validation_config);
  	 /*if ($this->form_validation->run() == FALSE)
  	 { 
  		$this->load->view('seventeen/common/header_view');
  		$this->load->view('seventeen/scbox/book-scbox_optional');
  		$this->load->view('seventeen/common/footer_view');
  	 }else
	  {		*/
      if(!empty($this->input->post()))
      {
				$files = $_FILES;	

					 
          $count = count($_FILES['image']['name']);
          
          if($_FILES['image']['name'][0]!='')
          {
    				
						$output = [];
    					for($i=0; $i<$count; $i++)
    					{ 					
    						
    						$_FILES['image']['name']= $files['image']['name'][$i];
    						$_FILES['image']['type']= $files['image']['type'][$i];
    						$_FILES['image']['tmp_name']= $files['image']['tmp_name'][$i];
    						$_FILES['image']['error']= $files['image']['error'][$i];
    						$_FILES['image']['size']= $files['image']['size'][$i];    
    						$ext = pathinfo($files['image']['name'][$i], PATHINFO_EXTENSION);
    						$img_name = time().mt_rand(0,10000).'.'.$ext;
    						$pathAndName =  $this->config->item('wordrobe_path').$img_name;
    						// Run the move_uploaded_file() function here
    						$moveResult = move_uploaded_file($files['image']['tmp_name'][$i], $pathAndName);

    						//$image_name= $this->upload->data('file_name');

    						$config['image_library'] = 'gd2';
    						$config['source_image'] = $this->config->item('wordrobe_path').$img_name;
    						$config['new_image'] = $this->config->item('wordrobe_path').'thumb/';
    						$config['create_thumb'] = TRUE;
    						$config['thumb_marker'] = '';
    						$config['maintain_ratio'] = TRUE;
    						$config['width'] = 200;
    						$config['height'] = 200;

    						$this->image_lib->initialize($config);

    						if (!$this->image_lib->resize())
    						{
    							$this->image_lib->display_errors();
    							$ok =0;

    						}else{

    							$ok =1;
    						}
    						$data_post = array(
    							'user_id' => $this->session->userdata('user_id'),
    							'image_name' => $img_name,												
    							'is_active ' => '1',												
    							'created_by' => $this->session->userdata('user_id'),
    							'created_datetime'=> date("Y-m-d H:i:s")   
    						);
    						 
    					$resultimg = $this->User_info_model->addUserWardrobe($data_post); 
               
    	           }	
    			
    			//exit();
        		$arrAddComment 							= array();
        		$arrAddComment['object_id']				= $_COOKIE['scbox_objectid'];
    			 $arrAddComment['object_meta_key'] 	 	= 'scbox_user_comments';
        		$arrAddComment['object_meta_value']		= $this->input->post('scbox_missed');
        		$arrAddComment['created_by']			= $this->session->userdata('user_id');
        		$arrAddComment['modified_by']			= $this->session->userdata('user_id');
        		$arrAddComment['created_datetime']		= date('Y-m-d H:i:s');
        		$comment = $this->User_info_model->addUserInfo($arrAddComment);
    			 if(!empty($file_name))
    			{
    				$arrAddImage = array();
    				$arrAddImage['image_name']			= $file_name;
    				$arrAddImage['user_id']				= $this->session->userdata('user_id');
    				$arrAddImage['created_by']			= $this->session->userdata('user_id');
    				$arrAddImage['created_datetime']	= date('Y-m-d H:i:s');
    				$wardrobe = $this->User_info_model->addUserWardrobe($arrAddImage);
    			} 
				/* echo "1";
				exit(); */
        		$this->session->set_flashdata('scbox_message', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Profile Uploaded Successfully</div>');
        		redirect('scbox-thankyou?success=1');
        }else
        {

           $this->session->set_flashdata('scbox_message', '<div class="alert-error alert-dismissable message error">Please Upload Profile picture</div>');

          // $this->load->view('seventeen/common/header_view');
          // $this->load->view('seventeen/scbox/book-scbox_optional');
          // $this->load->view('seventeen/common/footer_view');
        }

      }
      $this->load->view('seventeen/common/header_view');
      $this->load->view('seventeen/scbox/book-scbox_optional');
      $this->load->view('seventeen/common/footer_view');
		
	 // } 
	  
   
  }

  public function step6(){
    $this->load->view('seventeen/common/header_view');
    $this->load->view('seventeen/book-scbox');
    $this->load->view('seventeen/common/footer_view');
  }

  public function signup($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$sc_gender, $json_array = array()){
    //echo $medium.'--'.$scusername.'--'.$scemail_id.'---'.$scpassword.'--'.$ip.'--'.$role.'--'.$sc_gender;exit;
      $scusername = $scusername.date('Y-m-d h:i:s');
      $res = $this->User_info_model->registerUser($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$sc_gender,$json_array);
      if($scemail_id!=''){
        $this->Schome_model->update_couponcode($scemail_id,'Welcome10');
      }
      if($res){ return $res; }else{ return '0'; }
  }

  public function login($logEmailid,$logPassword){
         if(trim($logEmailid) == ''){
            echo "Please enter your email id";
          }else if(trim($logPassword) == ''){
            echo "Please enter the Password";
          }else{
           $result = $this->Scborough_model->login_user($logEmailid,$logPassword,$medium='website',$type='');
          }
  }

  public function getpincodedata(){
      $pincode = $this->input->post('scbox_pincode');
      $data = $this->Schome_model->getpincodedata($pincode);      
      if(!empty($data)){
         echo '{"id" : "'.(int)trim(@$data[0]['id']).'", "statename" : "'.trim(strip_tags(@$data[0]['state_name'])).'", "city" : "'.trim(@$data[0]['city']).'"}';exit;
       }else{
          echo 0;exit;
       }     
  }

  function escapeJsonString($value){ # list from www.json.org: (\b backspace, \f formfeed)
      $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
      $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
      $result = str_replace($escapers, $replacements, $value);
      return $result;
  }

  function checkuserdata(){
    $gender = '';
    $useremail = $this->input->post('useremail');
    $user_data =  $this->Cart_model->getUserDataByEmail($useremail);

    if(!empty($user_data))
    {
      $gender = $user_data[0]['gender']!='' ? $user_data[0]['gender'] : 0;
    }else
    {
      $gender = 0;
    }
    echo $gender;exit;
  }

  public function send_scbox_email($email_id,$user_email_id,$sc_boxmobile,$first_name,$last_name,$order_no=null){
    /* For SMS*/
    $mobile_no = '91'.$sc_boxmobile;
    $stylist_names = array("Mahek","Aishwarya", "Tanvi");
    $stylist_name = $stylist_names[array_rand($stylist_names)];
    //$order_no = @$_COOKIE['sc_order_id'];
    $message = 'Thank+you+for+booking+SCBOX+with+STYCKR.+Stay+Stylish!';
    /*$message = 'Thank+you+for+booking+your+personalised+SCBOX+with+StyleCracker.+Your+personal+stylist+will+get+in+touch+with+you+at+the+earliest.';
        file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile='.$mobile_no.'&message='.$message);*/
     /* For SMS*/   
    /* sms for sat7 to mon 10*/
    $message2 = '';
    date_default_timezone_set('Asia/Kolkata');
    $day = date('D');
    if($day == 'Sat' && date('H') > 19){
      // sat send after 7pm
      $message2 = 'Thank+you+for+registering.+Your+Order+No.+is+'.$order_no.'.+Your+celebrity+stylist+will+get+in+touch+with+you+shortly.+For+further+queries,+please+contact+us+on+07045135438';
    }else if($day == 'Sun'){
      // sunday 
      $message2 = 'Thank+you+for+registering.+Your+Order+No.+is+'.$order_no.'.+Your+celebrity+stylist+will+get+in+touch+with+you+shortly.+For+further+queries,+please+contact+us+on+07045135438';
    }else if($day == 'Mon' && date('H') < 10){
      // mon send before 10am
      $message2 = 'Thank+you+for+registering.+Your+Order+No.+is+'.$order_no.'.+Your+celebrity+stylist+will+get+in+touch+with+you+shortly.+For+further+queries,+please+contact+us+on+07045135438';
    }
    if($message2 != ''){
      file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile='.$mobile_no.'&message='.$message2); 
    }
    /* sms for sat7 to mon 10*/
    $password_message = '';
    $tdyloggedIn = $this->Pa_model->get_todayloggedin_user();
    if($tdyloggedIn){
      $password_message = '<br/><br/>The following are your login credentials: <br/>
                        Email ID: '.$user_email_id.' <br/>
                        Password: scbox-'.$first_name.' ';
    }

    $config['protocol'] = 'smtp';
    //$config['mailpath'] = '/usr/sbin/sendmail';
    //$config['charset'] = 'iso-8859-1';
    $config['mailtype'] = 'html';
    $config['wordwrap'] = TRUE;
    $config['smtp_host'] = $this->config->item('smtp_host');
    $config['smtp_user'] = $this->config->item('smtp_user');
    $config['smtp_pass'] = $this->config->item('smtp_pass');
    $config['smtp_port'] = $this->config->item('smtp_port');
    $this->email->initialize($config);
    if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){
       $email_id = $this->config->item('sc_test_emaild');
        //$email_id2 = "arun@stylecracker.com";
    }else{
       $email_id = "styling@stylecracker.com";
    }
   
   
    $this->email->from($this->config->item('from_email'), 'StyleCracker');
    $this->email->to($user_email_id);
    $this->email->cc($email_id);
    // $this->email->bcc($email_id);
    $this->email->subject('StyleCracker Personalized Box Request');
    $this->email->message('Dear '.$first_name.',<br/><br/>
          Hope all is well. Congratulations, we have just assigned you your very own personal stylist. We have just received your email regarding your interest in the StyleCracker Box with Order No. <b style="color:#13d792;">'.$order_no.'</b>, and can\'t wait to work with you on curating the perfect box to make you look great. <br/><br/>           
          Please do share a convenient date and time to connect and your stylist will be sure to call you then. <br/><br/>
          In case of any queries, please feel free to contact us on our customer care number 02240007762 (10am to 7pm, Monday to Saturday) or email us on customercare@stylecracker.com and we will be more than happy to assist you with anything that you might need.<br/><br/>           
          PS: We\'re here to ensure you love what you get in your box so please feel free to share as many details as possible when we speak.<br/><br/>
          '.$password_message.'
          <br/><br/>Kind Regards<br/>Your Stylist<br/><img src="https://www.stylecracker.com/assets/images/logo.png" ><br/>
          Facebook: <a href="https://www.facebook.com/StyleCracker/" target="_blank" >https://www.facebook.com/StyleCracker/</a><br/>
          Instagram: @stylecracker<br/>
          Twitter: Style_Cracker<br/>
          SCBOX Web: <a href="https://www.stylecracker.com/sc-box" target="_blank" >https://www.stylecracker.com/sc-box</a><br/>');
    if(@$this->email->send()){
      return "Email Sent";
    }else{
      return "Email Error";
    }
  }

  function getgenderSizeGuide(){
    $gender = $this->input->post('scbox_gender');
    $size_type = $this->input->post('size_type');
    $sizeguide_url = '';
    if($gender==1){
        if($size_type=='top')
        {
          $sizeguide_url = base_url().'assets/images/size_guide/stylecracker/top.jpg';;
        }else if($size_type=='bottom')
        {
          $sizeguide_url = base_url().'assets/images/size_guide/stylecracker/bottom.jpg';
        }if($size_type=='footwear')
        {
          $sizeguide_url = base_url().'assets/images/size_guide/stylecracker/footwear.jpg';
        }
    }else if($gender==2){
        if($size_type=='top')
        {
          $sizeguide_url =  base_url().'assets/images/size_guide/stylecracker/top_men.jpg';
        }else if($size_type=='bottom')
        {
          $sizeguide_url = base_url().'assets/images/size_guide/stylecracker/bottom_men.jpg';
        }if($size_type=='footwear')
        {
          $sizeguide_url = base_url().'assets/images/size_guide/stylecracker/footwear_men.jpg';
        }
    }
    echo $sizeguide_url;exit;
  }

  function add_scboxordertocart($objectid,$quantity,$scboxprice,$sc_source=null,$sc_medium=null,$sc_campaign=null){   
        $orderdata = array(); $scbox_constDetails = array();         
        //$objectid = $this->input->post('scbox_objectid');
        //$quantity = $this->input->post('scbox_quantity');

       // $resultcart = $this->cartnew_model->deleteCart();
        if(!isset($_COOKIE['SCUniqueID'])) {
          $uniquecookie = '';
        }else{       
          $uniquecookie = $_COOKIE['SCUniqueID'];
        }

        if ($this->agent->is_browser()){
            $agent = 'Computer '.$this->agent->browser().' '.$this->agent->version();
        }elseif ($this->agent->is_robot()){
            $agent = 'Robot '.$this->agent->robot();
        }elseif ($this->agent->is_mobile()){
            $agent = 'Mobile '.$this->agent->mobile();
        }else{
            $agent = 'Unidentified';
        }

        $platform_info = $this->agent->platform();       
        $scbox_data = unserialize(SCBOX_PACKAGE);       
        $productsize_arr = unserialize(SCBOX_SIZE);
        $productsize = $productsize_arr[0]->id!=''?$productsize_arr[0]->id:'2';       
        $userid = $this->session->userdata('user_id');
        $scbox_constDetails = $this->Pa_model->get_scboxdata($objectid,'id');         
        //$productid = $this->Pa_model->get_scboxdata($objectid,'id')->productid;  
        $productid = $scbox_constDetails->productid; 
        
        $lookid = 0;  
        //$packageid = $this->Pa_model->get_scboxdata($objectid,'id')->packname;          
        $packageid = $scbox_constDetails->packname; 
        if($packageid == 'package4'){
          $scboxprice = $scboxprice;
          //$this->Cart_model->updateproductprice($productid,$scboxprice);
        }else{
          //$scboxprice = $this->Pa_model->get_scboxdata($objectid,'id')->price;
          $scboxprice = $scbox_constDetails->price;
        }
        $sc_data = array();   
        $sc_data['utm_source'] = @$_COOKIE['sc_source'];
        $sc_data['utm_medium'] = @$_COOKIE['sc_medium'];
        $sc_data['utm_campaign'] = @$_COOKIE['sc_campaign'];
        
        //echo '<pre>';print_r($productsize);exit;
        //echo $uniquecookie.'==,=='.$productid.'=,='.$userid.'=,='.$productsize.'=,='.$agent.'=,='.$platform_info.'=,='.$scboxprice;exit;
        //$res = $this->Cart_model->update_cart($uniquecookie,$productid,$userid,$productsize,$agent,$platform_info,$lookid);    
        $res = $this->Cart_model->update_scboxcart($uniquecookie,$productid,$userid,$productsize,$agent,$platform_info,$quantity,$scboxprice,$sc_data);   
        return $res;
  }
  
  function updateCouponInc($order_unique_no){
    $order_unique_no = $this->cartnew_model->updateCouponInc($order_unique_no);
  }  

  function ordersuccessOrder($order_no,$objectid){
     // $order_no = $this->uri->segment(3);
   
      setcookie('cart_email','', -1, "/", "",  0);
      if($order_no!=''){ 

            $totalProductPrice =0;
            $referral_point = 0;
             /* MailChimp Remove From Cart*/
            $mc_user = explode('_',$order_no);       
            /*if(@$mc_user[0]!='')
            {
              $this->curl->simple_post(BACKENDURL.'/Mailchimp/delete_mccart/'.$mc_user[0].'/'.$mc_user[0], false, array(CURLOPT_USERAGENT => true));
            }*/
            /* MailChimp Remove From Cart End*/
            /* MailChimp Add Order */
            /* if(@$order_no!='')
            {
              $this->curl->simple_post(BACKENDURL.'/Mailchimp/send_orderinfo/'.$order_no, false, array(CURLOPT_USERAGENT => true));
            }*/
            if(@$mc_user[0]!='')
            {
               $userEmail = $this->cartnew_model->getUserEmail($mc_user[0]);
               if($userEmail!='')
               {
                $this->cartnew_model->update_couponcode($userEmail,'SCSHOPMORE10');
               }
            }
            /* MailChimp Add Order End*/
            
            $referral_point = $this->cartnew_model->get_refre_amt($order_no);
            $data['res'] = $this->cartnew_model->su_get_order_info($order_no);
            $data['brand_price'] = $this->cartnew_model->su_get_order_price_info($order_no);
            $data['order_display_no'] = @$data['brand_price'][0]['order_display_no'];
            foreach($data['res'] as $val){
               $totalProductPrice = $totalProductPrice+$val['product_price'];         
            }

            //$coupon_result = $this->calculateCouponAmount($data['res'],$totalProductPrice);
            
            $data['coupon_discount']= @$coupon_result['coupon_discount'];
            $data['coupon_code'] = @$coupon_result['coupon_code'];

            if($data['brand_price'][0]['confirm_mobile_no']==0 && $data['brand_price'][0]['pay_mode'] == 1){
              $mobile_no = '91'.$data['brand_price'][0]['mobile_no'];

              $string = '0123456789';
              $string_shuffled = str_shuffle($string);
              $otp_no = substr($string_shuffled, 1, 5);

            /* $otp_no = rand(5,100000);*/
            //$otp_no = 99999;
            $this->cartnew_model->scgenrate_otp($mobile_no,$otp_no);
            /*$message = 'Thank+you+for+choosing+STYCKR.+Your+OTP+for+cash+on+delivery+is+'.$otp_no.'.+Please+use+this+to+confirm+your+order.+Stay+Stylish!';
            file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile='.$mobile_no.'&message='.$message);*/

            }

            /*$this->load->view('seventeen/common/header_view');
            $this->load->view('./cart/orderSuccesfull',$data);
            $this->load->view('seventeen/common/footer_view');*/
            redirect('book-scbox/'.$objectid);
            //redirect('sc-box?success=1');

      }else{ 
        redirect('sc-box');
      }
    }
    

    function getEmailAddress($user_id){
        $res = $this->cartnew_model->get_user_info($user_id);
        if(!empty($res)){
          return $res[0]['email'];
        }else{ return ''; }
    }

    function savescbox()
    {
       $brand_list = '';$arr_answers = array();$question_id='';  $i=0; 
       $objectid = ($this->input->post('scbox_objectid')!='') ? $this->input->post('scbox_objectid') : $_COOKIE['scbox_objectid']; 
       $user_id = ($this->session->userdata('user_id')!='') ? $this->session->userdata('user_id') : 0;
       $gender = ($this->session->userdata('gender')!='') ? $this->session->userdata('gender') : $this->input->post('scbox_gender');  
       $scboxprice = $this->input->post('scbox_price'); 
       $utm_source = $this->input->post('utm_source');
       $utm_medium = $this->input->post('utm_medium');
       $scbox_page = $this->input->post('scbox_page');
       //$isGift = $this->input->post('scbox_isgift');
       $isGift = $_COOKIE['scbox_isgift'];

       $utm_campaign = $this->input->post('sc');
       $scbox_style_new = array();
       //$user_id_gifted = @$this->cartnew_model->emailUnique(trim($_COOKIE['scbox_gifted_email']))[0]['id'];
       //echo 'scbox_gifted_email=='.$_COOKIE['scbox_gifted_email'];exit;
       
       //setcookie('scbox_giftedid',$user_id_gifter,time()+ (86400 * 30), "/");

      if(!empty(@$this->input->post('scbox-confirm')) && $this->input->post('slide')==1)
      {
        setcookie('billing_address','', -1, "/", "",  0);
        $gifter_details = array();$gifter_details_json='';
        //echo '<pre>'; print_r($this->input->post());exit;   

        if($isGift=='true'){               
           
            $gifter_details['gender'] = trim($this->input->post('scbox_billgender'));
            $gifter_details['name'] = trim($this->input->post('scbox_billname'));
            $gifter_details['email'] = trim($this->input->post('scbox_billemailid'));
            $gifter_details['mobileno'] = trim($this->input->post('scbox_billmobile'));
            $gifter_details['pincode'] = trim($this->input->post('scbox_billpincode'));
            $gifter_details['city'] = trim($this->input->post('scbox_billcity'));
            $gifter_details['state'] = trim($this->input->post('scbox_billstate'));
            $gifter_details['address'] = trim($this->input->post('scbox_billaddress'));
            $gifter_details['profession'] = trim($this->input->post('scbox_billprofession'));
            $gifter_details['gift_msg'] = trim($this->input->post('scbox_giftmsg')); 
            $gifter_details['stylist_call_receiver'] = @$_COOKIE['stylist_call'];
            $gifter_details['dob'] = trim($this->input->post('scbox_billage'));
            $gifter_details_json = serialize($gifter_details);  
            $scusername = $this->input->post('scbox_billname');       
            $scusername = preg_replace('/\s+/', ' ', trim($scusername));
            $username = explode(' ',trim($scusername));       
            $json_array['firstname'] = ucwords(strtolower(@$username[0]));
            $json_array['lastname'] = ucwords(strtolower(@$username[1]));
            $scemail_id = $this->input->post('scbox_billemailid');
            $scbox_mobile =  $this->input->post('scbox_billmobile');       
            //$scpassword = 'scbox-'.$username[0];
            $scpassword = $this->input->post('scbox_password');
            $sc_gender = $this->input->post('scbox_billgender');        
            $ip = $this->input->ip_address();
            $birthdate = $this->input->post('scbox_billage');   
            $json_array['age_range'] = $this->User_info_model->get_userage_range($birthdate,$sc_gender);
            $json_array['birthday'] = $birthdate;
            $json_array['scmobile'] = $this->input->post('scbox_billmobile');
            $json_array['question_id'] = $sc_gender==1 ? 8:17;      

             if($scusername!='' && $scemail_id!='' && $scpassword!='' && $sc_gender!=''){
                $webres = $this->signup('website',$scusername,$scemail_id,$scpassword,$ip,2,$sc_gender,$json_array);
            }        
          }
          

        $scusername = $this->input->post('scbox_name');       
        $scusername = preg_replace('/\s+/', ' ', trim($scusername));
        $username = explode(' ',trim($scusername));       
        $json_array['firstname'] = ucwords(strtolower(@$username[0]));
        $json_array['lastname'] = ucwords(strtolower(@$username[1]));
        $scemail_id = $this->input->post('scbox_emailid');
        $scbox_mobile =  $this->input->post('scbox_mobile');       
        //$scpassword = 'scbox-'.$username[0];
        $scpassword = $this->input->post('scbox_password');
        $sc_gender = $this->input->post('scbox_gender');        
        $ip = $this->input->ip_address();
        $birthdate = $this->input->post('scbox_age');      
        $json_array['age_range'] = $this->User_info_model->get_userage_range($birthdate,$sc_gender);
        $json_array['birthday'] = $birthdate;
        $json_array['scmobile'] = $this->input->post('scbox_mobile');
        $json_array['question_id'] = $sc_gender==1 ? 8:17;
       // $scbox_brandpref = $this->input->post('brand_pref');

        
          //if(!empty($scbox_brandpref)){ $brand_list = $scbox_brandpref; }else $brand_list = ''; 

          if($scusername!='' && $scemail_id!='' && $scpassword!='' && $sc_gender!='')
          {
              $webres = $this->signup('website',$scusername,$scemail_id,$scpassword,$ip,2,$sc_gender,$json_array);
          }
          //$user_id_gifted = @$this->cartnew_model->emailUnique(trim($_COOKIE['scbox_gifted_email']))[0]['id'];
           /*if($isGift=='true'){
              $user_id = (@$_COOKIE['scbox_giftedid']!='') ? $_COOKIE['scbox_giftedid'] : 0; 
           }else{
               $user_id = ($this->session->userdata('user_id')!='') ? $this->session->userdata('user_id') : 0; 
           }*/

          $user_id = ($this->session->userdata('user_id')!='') ? $this->session->userdata('user_id') : 0; 

          $user_id = @$this->cartnew_model->emailUnique(trim($scemail_id))[0]['id'];

          // $arr_answers[0]['question'] =  'scbox_brand';
          // $arr_answers[0]['answer_id'] =  $brand_list;

          $arr_answers[1]['question'] =  'user_name';
          $arr_answers[1]['answer_id'] =  (@$this->input->post('scbox_name')!='') ? $this->input->post('scbox_name') : '';
         
          $arr_answers[2]['question'] =  'user_gender';
          $arr_answers[2]['answer_id'] =  (@$this->input->post('scbox_gender')!='') ? $this->input->post('scbox_gender') : '';
         
          $arr_answers[3]['question'] =  'user_emailid';
          $arr_answers[3]['answer_id'] =  (@$this->input->post('scbox_emailid')!='') ? $this->input->post('scbox_emailid') : '';

          $arr_answers[4]['question'] =  'user_dob';
          $arr_answers[4]['answer_id'] =  (@$this->input->post('scbox_age')!='') ? $this->input->post('scbox_age') : '';
        
          $arr_answers[5]['question'] =  'user_mobile';
          $arr_answers[5]['answer_id'] =  (@$this->input->post('scbox_mobile')!='') ? $this->input->post('scbox_mobile') : '';
        
          $arr_answers[6]['question'] =  'user_shipaddress';
          $arr_answers[6]['answer_id'] =  (@$this->input->post('scbox_shipaddress')!='') ? $this->input->post('scbox_shipaddress') : '';
        
          $arr_answers[7]['question'] =  'user_pincode';
          $arr_answers[7]['answer_id'] =  (@$this->input->post('scbox_pincode')!='') ? $this->input->post('scbox_pincode') : '';
         
          $arr_answers[8]['question'] =  'user_city';
          $arr_answers[8]['answer_id'] =  (@$this->input->post('scbox_city')!='') ? $this->input->post('scbox_city') : '';
        
          $arr_answers[9]['question'] =  'user_state';
          $arr_answers[9]['answer_id'] =  (@$this->input->post('scbox_state')!='') ? $this->input->post('scbox_state') : '';
        
       /*   $arr_answers[10]['question'] =  'user_likes';
          $arr_answers[10]['answer_id'] =  (@$this->input->post('scbox_likes')!='') ? $this->input->post('scbox_likes') : '';
         
          $arr_answers[11]['question'] =  'user_dislikes';
          $arr_answers[11]['answer_id'] =  (@$this->input->post('scbox_dislikes')!='') ? $this->input->post('scbox_dislikes') : '';*/

          $arr_answers[12]['question'] =  'user_id';
          $arr_answers[12]['answer_id'] =  (@$this->input->post('scbox_userid')!='') ? $this->input->post('scbox_userid') : '';
          
         /* $arr_answers[13]['question'] =  'scbox_pack';
          $arr_answers[13]['answer_id'] =  (@$this->input->post('scbox_package')!='') ? $this->input->post('scbox_package') : '';*/
          
          $arr_answers[14]['question'] =  'scbox_price';
          $arr_answers[14]['answer_id'] =  (@$this->input->post('scbox_price')!='') ? $this->input->post('scbox_price') : '';
         
         /* $arr_answers[15]['question'] =  'scbox_productid';
          $arr_answers[15]['answer_id'] =  (@$this->input->post('scbox_productid')!='') ? $this->input->post('scbox_productid') : '';*/
        
          $arr_answers[16]['question'] =  'scbox_objectid';
          $arr_answers[16]['answer_id'] =  (@$this->input->post('scbox_objectid')!='') ? $this->input->post('scbox_objectid') : '';

          $arr_answers[17]['question'] =  'utm_source';
          $arr_answers[17]['answer_id'] =  (@$this->input->post('utm_source')!='') ? $this->input->post('utm_source') : '';

          $arr_answers[18]['question'] =  'utm_medium';
          $arr_answers[18]['answer_id'] =  (@$this->input->post('utm_medium')!='') ? $this->input->post('utm_medium') : '';

          $arr_answers[19]['question'] =  'utm_campaign';
          $arr_answers[19]['answer_id'] =  (@$this->input->post('utm_campaign')!='') ? $this->input->post('utm_campaign') : '';

          $arr_answers[20]['question'] =  'user_profession';
          $arr_answers[20]['answer_id'] =  (@$this->input->post('scbox_profession')!='') ? $this->input->post('scbox_profession') : '';
          //echo 'txtprofession--'.$this->input->post('scbox_profession');exit;

          if($isGift=='true'){
            $arr_answers[21]['question'] =  'gifter_details';
            $arr_answers[21]['answer_id'] =  $gifter_details_json;
            setcookie('giftedto_details',$gifter_details_json,time() + 86400, "/"); 
            //$user_id = $user_id_gifted; 
          }

       /*  foreach($arr_answers as $key=>$json){
              $question = $json['question'];
             // $question_id = @$data_answers[$question];
              $answer_id = $json['answer_id'];
              $this->Pa_model->save_scboxuser_answer($question_id,$answer_id,$user_id,$objectid,$question);
            }*/

            // $arr_answers_serialised = serialize($arr_answers);
            // $this->Pa_model->save_scboxuser_answer($question_id,$arr_answers_serialised,$user_id,'860','scbox_user_details');

            $i = 0;$scbox_user_data = array();
            foreach($arr_answers as $key=>$json){
              $question = $json['question'];
             // $question_id = @$data_answers[$question];
              $answer_id = $json['answer_id'];              
              $scbox_user_data[$i]['object_id'] = $objectid;
              $scbox_user_data[$i]['object_meta_key'] = $json['question'];
              $scbox_user_data[$i]['object_meta_value'] = $json['answer_id'];
              $scbox_user_data[$i]['created_datetime'] = date('Y-m-d H:i:s');
              $scbox_user_data[$i]['created_by'] = $user_id;
              $scbox_user_data[$i]['modified_by'] = $user_id;              
             $i++; 
            }
            $this->Pa_model->save_scboxuser_answer_batch($question_id,$scbox_user_data,$user_id,$objectid,'user_id');



          setcookie('billing_address',$this->input->post('scbox_shipaddress'),time() + 86400, "/"); 
          //echo 'objectid--'.$objectid.',1,scboxprice--'.$scboxprice.'--,--'.$utm_source.'--,--'.$utm_medium.'--,--'.$utm_campaign;exit;
          // Add the scbox product to cart
          $cartid = $this->add_scboxordertocart($objectid,1,$scboxprice,$utm_source,$utm_medium,$utm_campaign);
          setcookie('sc_cart', $cartid, time() + (86400 * 30), "/");
          // Add the scbox product to cart
          
          // Add for first purchase
          if(!empty(@$user_id) && $objectid!=SCBOX_CUSTOM_OBJID)
          {
            $ordercount_result = $this->cartnew_model->get_ordercount(@$user_id);
            if($ordercount_result==0)
            {
              $defaultcoupon = $this->cartnew_model->get_defaultcoupons($user_id,'','scboxcart',$scboxprice);
              setcookie('discountcoupon_stylecracker', $defaultcoupon, time() + (86400 * 30), "/");
            }
          }
          //End- Add for first purchase


          if( ($utm_source!='' && $utm_source=='fb') || (@$scbox_page=='rakhi'))
          {
             echo 'utm';exit;
          }else
          {
            if($this->session->userdata('bucket') > 0)
            {
              echo 'success1';exit;
            }else
            {
               echo 'success';exit;
            }
          }   

        }
		    else if($this->input->post('slide')=='2a')
        {
          $category_selection = $this->input->post('category_selection');
          $arr_answers[0]['question'] =  'category_selection';
          $arr_answers[0]['answer_id'] =  $category_selection ;     
          $objectid = $this->Pa_model->get_object('pa_selection_before')[0]['object_id'];

          // Save Users PA category-attribute data
            $this->user_order_pa_save($user_id);
          // End Save Users PA category-attribute data          
        
        }else if($this->input->post('slide')==2)
        {
          $scbox_bodyshape = $this->input->post('body_shape_ans');
         // $arr_answers[0]['question'] =  'user_body_shape';
          //$arr_answers[0]['answer_id'] =  $scbox_bodyshape ;

          $pa_selection = $this->input->post('pa_data');
          $pa_selection_jsonencode = json_encode($pa_selection);
          //$pa_selection_jsondecode = json_decode($pa_selection);
          $arr_answers[0]['question'] =  'pa_selection_before';
          $arr_answers[0]['answer_id'] =  $pa_selection ;
          //echo $this->input->post('pa_data');
          //echo '<pre>';print_r($pa_selection_jsondecode);
          $objectid = $this->Pa_model->get_object('pa_selection_before')[0]['object_id'];

         
          // Save Users PA category-attribute data
            $this->user_order_pa_save($user_id);
          // End Save Users PA category-attribute data
           
        
        }else if($this->input->post('slide')==3)
        { 
           $pa_selection = $this->input->post('pa_data');
          $pa_selection_jsonencode = json_encode($pa_selection);
          //$pa_selection_jsondecode = json_decode($pa_selection);
          $arr_answers[0]['question'] =  'pa_selection_before';
          $arr_answers[0]['answer_id'] =  $pa_selection ;  
          $objectid = $this->Pa_model->get_object('pa_selection_before')[0]['object_id'];

          // Save Users PA category-attribute data
            $this->user_order_pa_save($user_id);
          // End Save Users PA category-attribute data        

        }else if($this->input->post('slide')==5)
        { 
          $pa_selection_optional = $this->input->post('pa_data_optional');
          $pa_selection_jsonencode = json_encode($pa_selection_optional);
          //$pa_selection_jsondecode = json_decode($pa_selection);
          $arr_answers[0]['question'] =  'pa_selection_after';
          $arr_answers[0]['answer_id'] =  $pa_selection_optional ;
          $order_unique_no = $this->input->post('order_no');

            $pa_selection = $_COOKIE['pa_selection_optional'];
            $objectid = $this->Pa_model->get_object('pa_selection_after')[0]['object_id'];
            $order_dispno = $this->Pa_model->get_orderdisplayno($order_unique_no)[0]['order_display_no'];
            $this->Pa_model->save_objectmeta_data($pa_selection,$user_id,$objectid,$order_dispno);
          //echo $this->input->post('pa_data');
          //echo '<pre>';print_r($pa_selection_jsondecode);

          // Save Users PA category-attribute data
            $this->user_order_pa_save($user_id);
          // End Save Users PA category-attribute data
          

        }else if($this->input->post('slide')==6)
        {
            // $scbox_brandpref = $this->input->post('brand_pref');
            //  if(!empty($scbox_brandpref)){ $brand_list = $scbox_brandpref; }else $brand_list = '';
            // $arr_answers[0]['question'] =  'scbox_brand_list';
            // $arr_answers[0]['answer_id'] =  $scbox_brandpref;
            // $arr_answers[1]['question'] =  'user_profession';
            // $arr_answers[1]['answer_id'] =  ($this->input->post('scbox_profession')!='') ? $this->input->post('scbox_profession') : '';
            $arr_answers[2]['question'] =  'user_comment';
            $arr_answers[2]['answer_id'] =  ($this->input->post('scbox_occassion')!='') ? $this->input->post('scbox_occassion') : '';
            $arr_answers[3]['question'] =  'user_wardrobe';
            $arr_answers[3]['answer_id'] =  ($this->input->post('scbox_wardrobe')!='') ? $this->input->post('scbox_wardrobe') : '';
            $arr_answers[4]['question'] =  'user_willtoexpmnt';
            $arr_answers[4]['answer_id'] =  ($this->input->post('experiment')!='') ? $this->input->post('experiment') : '';
        }  
                
        if($gender == '1'){
          $data_answers = array('user_body_shape'=>1,'user_style_p1'=>2,'user_style_p2'=>2,'user_style_p3'=>2,'user_brand_list'=>30);
        }else{
          $data_answers = array('user_body_shape'=>24,'user_style_p1'=>25,'user_style_p2'=>25,'user_style_p3'=>25,'user_brand_list'=>30);
        }

        if($isGift=='true'){
            $user_id = $user_id_gifted;
         }else
         {
           $user_id = $this->session->userdata('user_id');
         }

         if(!empty($arr_answers))
         {          
             foreach($arr_answers as $key=>$json){
              $question = $json['question'];
              $question_id = @$data_answers[$question];
              $answer_id = $json['answer_id'];                 
              $this->Pa_model->save_scboxuser_answer($question_id,$answer_id,$user_id,$objectid,$question);
            }
         }
       

          echo 'success';exit;      
    }
	
	public function online_order_success()
    {        
        $data = array();
        $scbox_pack = unserialize(SCBOX_PACKAGE);
        $productid = $this->Schome_model->scbox_get_orderproduct_info($_POST['udf5'])[0]['product_id'];     
        $scboxpack_key = $this->Schome_model->get_array_keyvalue($scbox_pack,'productid',$productid);  
        $objectid =   $scbox_pack[$scboxpack_key]->id;
        
        if(@$_POST['status'] == 'failure'){
          if($this->cartnew_model->delete_unsuccessfully_order($_POST['udf1'])){
              //redirect('checkout?error='.$_POST['field9']);
                redirect('cart/'.$objectid.'?type=scbox&error='.$_POST['field9'].'&step=3');   
              //redirect('book-scbox/'.$objectid.'?error='.$_POST['field9'].'&step=5');
            }
        }

        if(@$_POST['status'] == 'success'){
          $this->cartnew_model->change_order_status($_POST);
            $p_price = 0;
            $points = $this->cartnew_model->get_refer_point(); 
            $p_price = $this->cartnew_model->get_order_price_ref($_POST['udf5']);
            $discountsd = ''; $discountsdamt = 0;
            $discountsd = @$_COOKIE['discountcoupon_stylecracker'];

            $data['brand_price'] = $this->cartnew_model->su_get_order_price_info($_POST['udf5']);
            //$user_id = $data['brand_price'][0]['5'];
            $user_id = $data['brand_price'][0]['user_id'];
            $discountsdamt = $this->cartnew_model->check_coupon_after_order_place($discountsd,trim($this->getEmailAddress($user_id)),$_POST['udf5']);
            $p_price = $p_price - $discountsdamt['coupon_discount_amount'];
            if($points == 1 && $p_price>=MIN_CART_VALUE){
                if(isset($_COOKIE['is_redeem_point']) == true && $_COOKIE['is_redeem_point'] == 1){
                    $this->cartnew_model->redeem_points($_POST['udf5']);
                }
            } 

          //setcookie('discountcoupon_stylecracker','', -1, "/", "",  0);
          //setcookie('SCUniqueID','', -1, "/", "",  0);
          //$user_id = $this->session->userdata('user_id');
          
         // $this->updateCouponInc($_POST['udf5']);
          //$this->orderPlaceMessage($user_id,$_POST['udf5'],$data['brand_price'][0]['first_name'],$data['brand_price'][0]['last_name']);
          //  $this->orderPlaceMessagetoBrand($user_id,$_POST['udf5'],$data['brand_price'][0]['first_name'],$data['brand_price'][0]['last_name']);


          //redirect('stylecart/ordersuccessOrder/'.$_POST['udf5']);
        //  echo '<pre>';print_r($_POST);exit;
          //  redirect('book-scbox/?order='.$_POST['udf5']);
           //redirect('book-scbox/'.$objectid.'?order='.$_POST['udf5']);
         //redirect('sc-box/ordersuccessOrder/'.$_POST['udf5']);
          $order_data = $this->Cart_model->scbox_get_order_info(trim($_POST['udf5']));  
          setcookie('sc_order_id',@$order_data[0]['order_display_no'], time() + (86400 * 30), "/");
          
          //***$email_id = "sudha@stylecracker.com";
          if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){
             $email_id = $this->config->item('sc_test_emaild');
              //$email_id2 = "arun@stylecracker.com";
          }else{
              $email_id = "styling@stylecracker.com";
          }
          $user_email_id = $this->session->userdata('email');
          $scbox_mobile = $this->session->userdata('contact_no');
          $firstname = $this->session->userdata('first_name');
          $lastname = $this->session->userdata('last_name');
          $this->send_scbox_email($email_id,$user_email_id,$scbox_mobile,$firstname,$lastname,@$order_data[0]['order_display_no']);

          unset($_COOKIE['pa_selection']);
          unset($_COOKIE['pa_selection_optional']);
          unset($_COOKIE['scbox_cartotal']);
          unset($_COOKIE['category_selection']);
          unset($_COOKIE['giftedto_details']);
          unset($_COOKIE['jsonObjBeforePayment']);
          unset($_COOKIE['jsonObjAfterPayment']);
          
          //redirect(base_url().'book-scbox/'.$objectid.'?order='.$_POST['udf5'].'&step=6'); 
		      redirect(base_url().'book-scbox/step4?order='.$_POST['udf5']); 	  

        }else{
            if($this->cartnew_model->delete_unsuccessfully_order($_POST['udf1'])){
              redirect('cart/'.$objectid.'?type=scbox&error='.$_POST['field9'].'&step=3');
              //redirect('book-scbox/'.$objectid.'?error='.$_POST['field9'].'&step=5');
            }
          }
    }

     function place_order()
    {
          $orderdata = array();
          $packageid = ''; $productid = '';$order_dispno='';
          //$objectid = $this->uri->segment(2);
          $objectid = $this->input->post('scbox_objectid')!='' ? $this->input->post('scbox_objectid') : $_COOKIE['scbox_objectid'];
          $quantity = $this->input->post('scbox_quantity');

          //$resultcart = $this->cartnew_model->deleteCart();
          if(!isset($_COOKIE['SCUniqueID'])) {
            $uniquecookie = '';
          }else{       
            $uniquecookie = $_COOKIE['SCUniqueID'];
          }

          if ($this->agent->is_browser())
          {
              $agent = 'Computer '.$this->agent->browser().' '.$this->agent->version();
          }
          elseif ($this->agent->is_robot())
          {
              $agent = 'Robot '.$this->agent->robot();
          }
          elseif ($this->agent->is_mobile())
          {
              $agent = 'Mobile '.$this->agent->mobile();
          }
          else
          {
              $agent = 'Unidentified';
          }

          $platform_info = $this->agent->platform();

          //echo '<pre>';print_r($data);
          $scbox_data = unserialize(SCBOX_PACKAGE);
          //$pid = $this->uri->segment(2);

          //$scboxprice = $data['userdata']['scbox_price'];
          $scboxprice = $this->input->post('scbox_price');
          $productsize_arr = unserialize(SCBOX_SIZE);
          $productsize = $productsize_arr[0]->id!=''?$productsize_arr[0]->id:'2';
          //$userid = $data['userid'];
          $userid = $this->session->userdata('user_id');

          $productid = $this->Pa_model->get_scboxdata($objectid,'scbox_productid')->productid; 
          $lookid = 0;  
          $packageid = $this->Pa_model->get_scboxdata($objectid,'id')->packname;
          //$packageid = @$data['userdata']['scbox_packid'];
          if($packageid == 'package4')
          {
            $this->Cart_model->updateproductprice($productid,$scboxprice);
          }
          
          $scbox_data = $this->Pa_model->get_scbox_userdata($objectid,$userid);
          if(!empty($scbox_data))
          {
            foreach($scbox_data as $val)
            {
              $data['userdata'][$val['object_meta_key']] = $val['object_meta_value'];
            }
          }
          //echo '<pre>';print_r($productsize);exit;
          //echo $uniquecookie.'==,=='.$productid.'=,='.$userid.'=,='.$productsize.'=,='.$agent.'=,='.$platform_info.'=,='.$lookid;
          //$res = $this->Cart_model->update_cart($uniquecookie,$productid,$userid,$productsize,$agent,$platform_info,$lookid);    
          //$res = $this->Cart_model->update_scboxcart($uniquecookie,$productid,$userid,$productsize,$agent,$platform_info,$quantity);   
          //COD:1  
          /*$username = preg_replace('!\s+!', ' ', $data['userdata']['user_name']);  
           $username = explode(' ',$data['userdata']['user_name']);       
              $firstname = @$username[0];
              $lastname = @$username[1];

           $billing_first_name = $firstname;
           $billing_last_name = $lastname;
           $billing_mobile_no = $data['userdata']['user_mobile'];
           $billing_pincode_no = $data['userdata']['user_pincode'];
           $billing_address = $data['userdata']['user_shipaddress'];
           $billing_city = $data['userdata']['user_city'];
           $billing_state = $data['userdata']['user_state'];
           $shipping_first_name = $firstname;
           $shipping_last_name = $lastname;
           $shipping_mobile_no = $data['userdata']['user_mobile'];
           $shipping_pincode_no = $data['userdata']['user_pincode'];
           $shipping_address = $data['userdata']['user_shipaddress'];
           $shipping_city = $data['userdata']['user_city'];
           $shipping_state = $data['userdata']['user_state'];
           $cart_email = $data['userdata']['user_emailid'];*/

            $billing_first_name=$this->input->post('billing_first_name'); 
            $billing_last_name=$this->input->post('billing_last_name'); 
            $billing_mobile_no=$this->input->post('billing_mobile_no'); 
            $billing_pincode_no=$this->input->post('billing_pincode_no'); 
            $billing_address_n = @$data['userdata']['user_shipaddress'];
            if(@$this->input->post('billing_address')!=''){
              $billing_address=$this->escapeJsonString($billing_address_n); 
            }else{
              $billing_address='';
            }
            if($billing_address!=''){
                  $billing_address = @stripslashes(strip_tags($billing_address));
                }
            $billing_city=$this->input->post('billing_city'); 
            $billing_state=$this->input->post('billing_state'); 
            $shipping_first_name=$billing_first_name; 
            $shipping_last_name=$billing_last_name; 
            $shipping_mobile_no=$billing_mobile_no; 
            $shipping_pincode_no=$billing_pincode_no; 
            $shipping_address=$billing_address; 
            $shipping_city= $billing_city; 
            $shipping_state=$billing_state; 
            $cart_email=$this->input->post('cart_email'); 
            $pay_mod=$this->input->post('pay_mod'); 
            $cart_total=$this->input->post('cart_total');
            $pay_mod = 'cod';

            //$cookie_value = '';

           // $orderdata['coupon_code_msg'] = $this->cartnew_model->check_coupon_exist(strtolower($_COOKIE['discountcoupon_stylecracker']),'','scboxcart');

            // if(empty(@$_COOKIE['discountcoupon_stylecracker'])) {
            //   //$orderdata['coupon_code_msg'] = $this->cartnew_model->check_coupon_exist('','','scboxcart');
            //   setcookie('discountcoupon_stylecracker', $cookie_value, time() + (86400 * 30), "/");
            // }else{                
            // }

            if($billing_first_name!='' && $billing_last_name!='' && $billing_mobile_no!='' && $billing_pincode_no!='' && $billing_address!='' && $billing_city!='' && $billing_state!='' && $shipping_first_name!='' && $shipping_last_name!='' && $shipping_mobile_no!='' && $shipping_pincode_no!='' && $shipping_address!='' && $shipping_city!='' && $shipping_state!='' && $cart_email!='' && $pay_mod!=''){
              
              $orderdata['user_info'] = array('billing_first_name'=>$billing_first_name,'billing_last_name'=>$billing_last_name,'billing_mobile_no'=>$billing_mobile_no,'billing_pincode_no'=>$billing_pincode_no,'billing_address'=>$billing_address,'billing_city'=>$billing_city,'billing_state'=>$billing_state,'shipping_first_name'=>$shipping_first_name,'shipping_last_name'=>$shipping_last_name,'shipping_mobile_no'=>$shipping_mobile_no,'shipping_pincode_no'=>$shipping_pincode_no,'shipping_address'=>$shipping_address,'shipping_city'=>$shipping_city,'shipping_state'=>$shipping_state,'cart_email'=>$cart_email,'pay_mod'=>$pay_mod,'existing_shipping_add'=>0);

              $orderdata['cart_info'] = $this->cartnew_model->get_scboxcart();
             
              $orderdata['coupon_code'] = @$_COOKIE['discountcoupon_stylecracker'];
              //echo '<pre>';print_r($orderdata['cart_info']);exit;
              $p_price = 0;  $orderdata['referral_point']=0;
            if(!empty($orderdata['cart_info'])){
              foreach($orderdata['cart_info'] as $val){
                  /*if($val['product_id']==SCBOX_CUSTOM_ID)
                  {
                     $val['discount_price']=$scboxprice;
                     $p_price = $p_price + $val['discount_price'];
                  }else
                  {
                    $p_price = $p_price + $val['discount_price'];
                  }*/
                  $p_price = $p_price + $val['discount_price'];
              }
            }

              if(isset($_COOKIE['discountcoupon_stylecracker']) && $_COOKIE['discountcoupon_stylecracker']!=''){ 
                $orderdata['coupon_code_msg'] = $this->cartnew_model->check_coupon_exist(strtolower($_COOKIE['discountcoupon_stylecracker']),'','scboxcart');
                $p_price = $p_price-@$data['coupon_code_msg']['coupon_discount_amount'];
              }    
              

              $order_unique_no = $this->cartnew_model->stylecracker_place_order($orderdata);
              
              if($order_unique_no > 0){
                //$user_id = $this->session->userdata('user_id');

                $points = $this->cartnew_model->get_refer_point(); 
                                if($points == 1 && $p_price >=MIN_CART_VALUE){
                                    if(isset($_COOKIE['is_redeem_point']) == true && $_COOKIE['is_redeem_point'] == 1){
                                        $this->cartnew_model->redeem_points($order_unique_no);
                                    }
                                }

                $this->cartnew_model->update_order_id_in_cart($order_unique_no);
                $orderdata['brand_price'] = $this->cartnew_model->su_get_order_price_info($order_unique_no);
                  $user_id = $orderdata['brand_price'][0]['user_id'];
                $this->updateCouponInc($order_unique_no);
                $this->orderPlaceMessage_sc($user_id,$order_unique_no,$billing_first_name,$billing_last_name);

                $order_dispno = $this->Pa_model->get_orderdisplayno($order_unique_no)[0]['order_display_no'];

                setcookie('sc_order_id',@$order_dispno, time() + (86400 * 30), "/");

                if(@$_COOKIE['pa_selection']!='')
                {
                  $pa_selection = @$_COOKIE['pa_selection'];
                }else
                {
                  $pa_selection = ' ';
                }
                
                $objectid_ = $this->Pa_model->get_object('pa_selection_before')[0]['object_id'];                
                $this->Pa_model->save_objectmeta_data($pa_selection,$user_id,$objectid_,$order_dispno);
                if(@$_COOKIE['category_selection']!='')
                {
                  $category_selection = $_COOKIE['category_selection'];
                }else
                {
                  $category_selection = ' ';
                }
                
                $objectid_ = $this->Pa_model->get_object('category_selection')[0]['object_id'];
                $this->Pa_model->save_objectmeta_data($category_selection,$user_id,$objectid_,$order_dispno);
                if(@$_COOKIE['scbox_isgift'] == 'true')
                {
                  $giftedto_details = $_COOKIE['giftedto_details'];
                  $objectid_ = $this->Pa_model->get_object('scbox_gift')[0]['object_id'];
                  $this->Pa_model->save_objectmeta_data($giftedto_details,$user_id,$objectid_,$order_dispno);
                }

                if(@$_COOKIE['scbox_isgift'] == 'true' && @$_COOKIE['stylist_call'] == 'yes')
               {
                    $pa_selection = '';
                    $objectid_ = $this->Pa_model->get_object('pa_selection_before')[0]['object_id'];                   
                    $this->Pa_model->save_objectmeta_data($pa_selection,$userid,$objectid_,$order_dispno);
                    $category_selection = '';
                    $objectid_ = $this->Pa_model->get_object('category_selection')[0]['object_id'];
                    $this->Pa_model->save_objectmeta_data($category_selection,$userid,$objectid_,$order_dispno);

               }
       
                
                  //$this->orderPlaceMessage($user_id,$order_unique_no,$billing_first_name,$billing_last_name);
                    //$this->orderPlaceMessagetoBrand($user_id,$order_unique_no,$billing_first_name,$billing_last_name);
                //echo $order_unique_no;
                //$this->ordersuccessOrder($order_unique_no,$data['userdata']['scbox_objectid']);
                //$this->online_order_success();
               //$response = base_url().'book-scbox/'.$objectid.'?order='.$order_unique_no.'&step=6';
                //***$email_id = "sudha@stylecracker.com";
                if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){
                   $email_id = $this->config->item('sc_test_emaild');
                    //$email_id2 = "arun@stylecracker.com";
                }else{
                  $email_id = "styling@stylecracker.com";
                }
                $user_email_id = $this->session->userdata('email');
                $scbox_mobile = $this->session->userdata('contact_no');
                $firstname = $this->session->userdata('first_name');
                $lastname = $this->session->userdata('last_name');
                $this->send_scbox_email($email_id,$user_email_id,$scbox_mobile,$firstname,$lastname,$order_dispno);

               echo trim($order_unique_no);exit;
               
                // redirect(base_url().'book-scbox/277?order='.$order_unique_no.'&step=6'); 
              }

            }else
            {
              
            }
    }

    function place_online_order()
    {
        if(!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }else{
          $data = array();
          $packageid = '';$productid = ''; $offerkey = ''; $o_price = 0; $o_cart_total = 0;
          $offer_codes = unserialize(OFFER_CODES);
          //$objectid = $this->uri->segment(2);
           //$resultcart = $this->cartnew_model->deleteCart();
           $objectid = $this->input->post('scbox_objectid')!='' ? $this->input->post('scbox_objectid') : $_COOKIE['scbox_objectid'];
           $quantity = $this->input->post('scbox_quantity');

          if(!isset($_COOKIE['SCUniqueID'])) {
            $uniquecookie = '';
          }else{       
            $uniquecookie = $_COOKIE['SCUniqueID'];
          }

          if ($this->agent->is_browser())
          {
              $agent = 'Computer '.$this->agent->browser().' '.$this->agent->version();
          }elseif ($this->agent->is_robot())
          {
              $agent = 'Robot '.$this->agent->robot();
          }
          elseif ($this->agent->is_mobile())
          {
              $agent = 'Mobile '.$this->agent->mobile();
          }
          else
          {
              $agent = 'Unidentified';
          }

          $platform_info = $this->agent->platform();

          //echo '<pre>';print_r($data);
          $scbox_data = unserialize(SCBOX_PACKAGE);
          $pid = $this->uri->segment(2);
         // echo '<pre>';print_r($scbox_data);exit;
          $scboxprice = $this->input->post('scbox_price');
          $productsize_arr = unserialize(SCBOX_SIZE);
          $productsize = $productsize_arr[0]->id!=''?$productsize_arr[0]->id:'2';
          //$userid = $data['userid'];
          $userid = $this->session->userdata('user_id');
        
          $productid = $this->Pa_model->get_scboxdata($objectid,'scbox_productid')->productid; 
          $lookid = 0;  
          $packageid = $this->Pa_model->get_scboxdata($objectid,'id')->packname;
          
          if($packageid == 'package4')
          {
            $this->Cart_model->updateproductprice($productid,$scboxprice);
          }
          //echo $scboxprice;exit;
          //echo '<pre>';print_r($productsize);exit;
        //echo $uniquecookie.'==,=='.$productid.'=,='.$userid.'=,='.$productsize.'=,='.$agent.'=,='.$platform_info.'=,='.$lookid;exit;
        
          $sc_data = array();   
          $sc_data['utm_source'] = @$_COOKIE['sc_source'];
          $sc_data['utm_medium'] = @$_COOKIE['sc_medium'];
          $sc_data['utm_campaign'] = @$_COOKIE['sc_campaign'];
        
         // $res = $this->Cart_model->update_cart($uniquecookie,$productid,$userid,$productsize,$agent,$platform_info,$lookid); 
         $res = $this->Cart_model->update_scboxcart($uniquecookie,$productid,$userid,$productsize,$agent,$platform_info,$quantity,$scboxprice,$sc_data);   
                 
         // echo $res;exit; 
          $billing_first_name=$this->input->post('billing_first_name'); 
          $billing_last_name=$this->input->post('billing_last_name'); 
          $billing_mobile_no=$this->input->post('billing_mobile_no'); 
          $billing_pincode_no=$this->input->post('billing_pincode_no'); 
          if(@$this->input->post('billing_address')!=''){
            $billing_address=$this->escapeJsonString($this->input->post('billing_address')); 
          }else{
            $billing_address='';
          }
          if($billing_address!=''){
                $billing_address = @stripslashes(strip_tags($billing_address));
              }
          $billing_city=$this->input->post('billing_city'); 
          $billing_state=$this->input->post('billing_state'); 
          $shipping_first_name=$billing_first_name; 
          $shipping_last_name=$billing_last_name; 
          $shipping_mobile_no=$billing_mobile_no; 
          $shipping_pincode_no=$billing_pincode_no; 
          $shipping_address=$billing_address; 
          $shipping_city= $billing_city; 
          $shipping_state=$billing_state; 
          $cart_email=$this->input->post('cart_email'); 
          $pay_mod=$this->input->post('pay_mod'); 
          $cart_total=$this->input->post('cart_total');
          $page_type=$this->input->post('page_type');

          if(isset($_COOKIE['SCUniqueID'])){
                $SCUniqueID =  $_COOKIE['SCUniqueID'];
              }

          if($billing_first_name!='' && $billing_last_name!='' && $billing_mobile_no!='' && $billing_pincode_no!='' && $billing_address!='' && $billing_city!='' && $billing_state!='' && $shipping_first_name!='' && $shipping_last_name!='' && $shipping_mobile_no!='' && $shipping_pincode_no!='' && $shipping_address!='' && $shipping_city!='' && $shipping_state!='' && $cart_email!='' && $pay_mod!='' && $cart_total!=''){           

            $this->cartnew_model->failed_order($shipping_first_name,$shipping_last_name,$cart_email,$shipping_address,$SCUniqueID,$shipping_mobile_no,$pay_mod,$shipping_pincode_no,$shipping_city,$country=null,$uc_state2=null);

            $data['user_info'] = array('billing_first_name'=>$billing_first_name,'billing_last_name'=>$billing_last_name,'billing_mobile_no'=>$billing_mobile_no,'billing_pincode_no'=>$billing_pincode_no,'billing_address'=>$billing_address,'billing_city'=>$billing_city,'billing_state'=>$billing_state,'shipping_first_name'=>$shipping_first_name,'shipping_last_name'=>$shipping_last_name,'shipping_mobile_no'=>$shipping_mobile_no,'shipping_pincode_no'=>$shipping_pincode_no,'shipping_address'=>$shipping_address,'shipping_city'=>$shipping_city,'shipping_state'=>$shipping_state,'cart_email'=>$cart_email,'pay_mod'=>$pay_mod,'existing_shipping_add'=>0);

            $data['cart_info'] = $this->cartnew_model->get_scboxcart();
            $data['coupon_code'] = @$_COOKIE['discountcoupon_stylecracker'];
            //echo '<pre>';print_r($data['cart_info']);exit;
              $p_price = 0;  $data['referral_point']=0;
          if(!empty($data['cart_info'])){
            foreach($data['cart_info'] as $val){

              if($val['product_id']==SCBOX_CUSTOM_ID)
              {
                 $val['discount_price']=$scboxprice;
                 $p_price = $p_price + $val['discount_price'];
              }else
              {
                $p_price = $p_price + $val['discount_price'];
              }
              //$p_price = $p_price + $val['discount_price'];
            }
          }
         // echo $p_price;exit;
          //echo '<pre>';print_r($data);exit;
            $online_payment_discount = 0;       
            if(ONLINE_PAY_DISCOUNT>0)
            {
              $online_payment_discount = ($p_price*ONLINE_PAY_DISCOUNT)/100;
            }else
            {
              $online_payment_discount = 0;
            }
            
            if(isset($_COOKIE['discountcoupon_stylecracker']) && $_COOKIE['discountcoupon_stylecracker']!=''){ 
              $data['coupon_code_msg'] = $this->cartnew_model->check_coupon_exist(strtolower($_COOKIE['discountcoupon_stylecracker']),'','scboxcart');
              $p_price = $p_price-$data['coupon_code_msg']['coupon_discount_amount'];
               $cart_total =  $cart_total - $data['coupon_code_msg']['coupon_discount_amount'];
            }  
           
          $order_unique_no = $this->cartnew_model->stylecracker_place_order($data);
          $cart_total = $this->cartnew_model->cart_total($order_unique_no,$page_type);

          $pa_selection = (@$_COOKIE['pa_selection']!='') ? @$_COOKIE['pa_selection'] : '';
          $objectid_ = $this->Pa_model->get_object('pa_selection_before')[0]['object_id'];
          $order_dispno = $this->Pa_model->get_orderdisplayno($order_unique_no)[0]['order_display_no'];
          $this->Pa_model->save_objectmeta_data($pa_selection,$userid,$objectid_,$order_dispno);
          $category_selection = (@$_COOKIE['category_selection']!='') ? @$_COOKIE['category_selection'] : '';
          $objectid_ = $this->Pa_model->get_object('category_selection')[0]['object_id'];
          $this->Pa_model->save_objectmeta_data($category_selection,$userid,$objectid_,$order_dispno);

          if(@$_COOKIE['scbox_isgift'] == 'true')
          {
            $giftedto_details = $_COOKIE['giftedto_details'];
            $objectid_ = $this->Pa_model->get_object('scbox_gift')[0]['object_id'];
            $this->Pa_model->save_objectmeta_data($giftedto_details,$userid,$objectid_,$order_dispno);
          }

           if(@$_COOKIE['scbox_isgift'] == 'true' && @$_COOKIE['stylist_call'] == 'yes')
           {
                $pa_selection = '';
                $objectid_ = $this->Pa_model->get_object('pa_selection_before')[0]['object_id'];                   
                $this->Pa_model->save_objectmeta_data($pa_selection,$userid,$objectid_,$order_dispno);
                $category_selection = '';
                $objectid_ = $this->Pa_model->get_object('category_selection')[0]['object_id'];
                $this->Pa_model->save_objectmeta_data($category_selection,$userid,$objectid_,$order_dispno);

           }
       
       
          if(isset($_COOKIE['discountcoupon_stylecracker']) && $_COOKIE['discountcoupon_stylecracker']!='' && in_array(@$_COOKIE['discountcoupon_stylecracker'], $offer_codes)){ 
              //$data['coupon_code_msg'] = $this->cartnew_model->check_coupon_exist(strtolower($_COOKIE['discountcoupon_stylecracker']),'','scboxcart');
              //$p_price = $p_price-$data['coupon_code_msg']['coupon_discount_amount'];
               $cart_total =  $_COOKIE['scbox_cartotal'];
            }  
           
           

          //$cart_total = $this->cartnew_model->cart_total($order_unique_no);

          
              /*$redeem_points = 0; 
                  $points = $this->cartnew_model->get_refer_point(); 
                              if($points == 1 && $p_price>=MIN_CART_VALUE){
                                  if(isset($_COOKIE['is_redeem_point']) == true && $_COOKIE['is_redeem_point'] == 1){
                                      $redeem_points = POINT_VAlUE;
                                  }
                              }
                  $cart_total = $this->cartnew_model->cart_total($order_unique_no)-$redeem_points;*/
                   
                    //echo $cart_total;exit;
                $MERCHANT_KEY = MERCHANT_KEY;
                $SALT = PAYUSALT;
                $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);

                // Added for Payu Offer
                if(isset($_COOKIE['discountcoupon_stylecracker']) && in_array(@$_COOKIE['discountcoupon_stylecracker'], $offer_codes)){ 
                  //$offerkey = PAYUOFFER_KEY;
                    $offerkey_couponcode = strtoupper(@$_COOKIE['discountcoupon_stylecracker']);
                    $offerkeyconst = $offerkey_couponcode.'_OFFER_KEY'; 
                    if($offerkeyconst=='BMS15_OFFER_KEY')
                    {
                      $offerkey = BMS15_OFFER_KEY; 
                    }else if($offerkeyconst=='YESBOX500_OFFER_KEY')
                    {
                      $offerkey = YESBOX500_OFFER_KEY; 
                    }else if($offerkeyconst=='YESBOX750_OFFER_KEY')
                    {
                      $offerkey = YESBOX750_OFFER_KEY; 
                    }else if($offerkeyconst=='YESBOX1000_OFFER_KEY')
                    {
                      $offerkey = YESBOX1000_OFFER_KEY; 
                    }
                  $cart_total = $o_cart_total-$online_payment_discount; 
                }else
                {
                  $cart_total =  $_COOKIE['scbox_cartotal'];
                }      
                      
              //End of Added for Payu Offer
                
                //Pay u biz hash key
               $hash = $MERCHANT_KEY.'|'.$txnid.'|'.$cart_total.'|{"paymentParts":[{"name":"splitID1","value":"6","merchantId":"396446","description":"test description","commission":"2"}]}|'.$billing_first_name.''.$billing_last_name.'|'.$cart_email.'|'.$SCUniqueID.'|'.$billing_address.'|'.$billing_pincode_no.'|'.$billing_city.'|'.$order_unique_no.'||||||'.$SALT;
         
            
                $hash = strtolower(hash('sha512', $hash));
                
                echo '{"order" : "online","chk_pay_mode":"card_pay","payu_hash":"'.$hash.'","txnid":"'.$txnid.'","amount":"'.$cart_total.'","email":"'.$cart_email.'","firstname":"'.$billing_first_name.$billing_last_name.'","phoneno":"'.$billing_mobile_no.'","address_sc":"'.$billing_address.'","state":"'.$order_unique_no.'","offerkey":"'.$offerkey.'"}';exit;

          }
        }
    }

    function orderPlaceMessage_sc($user_id,$order_id,$first_name,$last_last){
   /* function orderPlaceMessage_scnew()
    {
      $user_id='10668';
      //$order_id='10668_514_515';//product discount -scb100
      $order_id='10668_493_494_495';//sc1000 -stylecracker discount
      //$order_id='10668_496';//normal order
      //$order_id='10668_512_513';//brand discount
      $first_name='sudha';
      $last_last='S';*/

      if($this->getEmailAddress($user_id)!=''){
        $config['protocol'] = 'smtp';
        $totalProductPrice=0;
        $referral_point = 0;
        $referral_point = $this->cartnew_model->get_refre_amt($order_id);


      //  $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;
        $config['smtp_host'] = $this->config->item('smtp_host');
        $config['smtp_user'] = $this->config->item('smtp_user');
        $config['smtp_pass'] = $this->config->item('smtp_pass');
        $config['smtp_port'] = $this->config->item('smtp_port');
      
        $this->email->initialize($config);

        $data['orderUniqueNo'] = $order_id;
        $data['Username'] = $first_name.' '.$last_last;
        $data['order_product_info'] = $this->cartnew_model->su_get_order_info($order_id);    
          $data['order_info'] = $this->cartnew_model->su_get_order_price_info_res($order_id);
          $data['shipping_address'] = $data['order_info'][0]['shipping_address'];
          $data['city_name'] = $data['order_info'][0]['city_name'];
          $data['state_name'] = $data['order_info'][0]['state_name'];
          $data['pincode'] = $data['order_info'][0]['pincode'];
          $data['country_name'] = $data['order_info'][0]['country_name'];
          $data['paymentmode'] = $data['order_info'][0]['pay_mode'];
          $data['referral_point'] = $referral_point;
          $data['order_display_no'] = $data['order_info'][0]['order_display_no'];

          if($data['order_info'][0]['state_id'] == '36') { $data['state_name'] =  $data['order_info'][0]['it_state']; }

          //$data['res'] =  $this->cartnew_model->su_get_order_info($order_id); 

        //$this->load->view('emails/order_successful_user_copy',$data);

          $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Processing'); 
          $this->email->to('sudha@stylecracker.com');          
          //$this->email->cc('order@stylecracker.com');    
          //$this->email->bcc('sudha@stylecracker.com');      
             
          $this->email->subject('StyleCracker: Order Processing');
          //$this->load->view('emails/order_successful_user_copy',$data);
          $message = $this->load->view('emails/order_successful_user_copy',$data,true);
          $this->email->message($message);
          //$this->email->send();

    }
  } 

  function user_order_pa_save($user_id)
  {
      $category_pa_data = array();
      $attribute_pa_data = array();
      $attributes = array();
      $attributes1 = array();
      $attributes2 = array();
      $categories = array();
      // $user_category_pa_data = array();
      // $user_attribute_pa_data = array();

      $categories1 = @$_COOKIE['arrUserCategorySelected'];
      $categories2 = @$_COOKIE['arrUserAnswerCategory'];
      $categories3 = @$_COOKIE['arrUserAnswerAfterCategory'];
      //$categories = array_merge($categories1,$categories2);
      $categories = $categories1.','.$categories2;

      if(!empty($categories1) || !empty($categories2) || !empty($categories3))
      {
        $categories = $categories1.','.$categories2.','.$categories3;
      }
      $categories = trim($categories,",");    
      //$attributes = @$_COOKIE['arrUserAnswerSelected']; 
      $attributes1 = @$_COOKIE['arrUserAnswerSelected']; 
      $attributes2 = @$_COOKIE['arrUserAnswerAfterSelected'];
      $cartid = @$_COOKIE['sc_cart'];

      if(!empty($attributes1) || !empty($attributes2))
      {
        $attributes = $attributes1.','.$attributes2;
      }
     // echo '<pre>';print_r($attributes);exit;
      if(!empty($categories))
      {
        $category_array = explode(',', $categories);     
        $i=0;
        foreach($category_array as $val)
        {
          if($val!='' && $val!='0')
          {
            $category_pa_data[$i]['object_id'] = $cartid;
            $category_pa_data[$i]['category_id'] = $val;
            $category_pa_data[$i]['object_type'] = 'order';
            $category_pa_data[$i]['modified_by'] = $user_id;
          }
          $i++;
        }
      }

      if(!empty($attributes))
      {
           $attributes_array = explode(',', $attributes);
            $j=0;
            foreach($attributes_array as $val)
            {
              if($val!='' && $val!='0')
              {
                $attribute_pa_data[$j]['object_id'] = $cartid;
                $attribute_pa_data[$j]['attribute_id'] = $val;
                $attribute_pa_data[$j]['object_type'] = 'order';
                $attribute_pa_data[$j]['modified_by'] = $user_id;
              }       
              
              $j++;
            }
      }

      //echo '<pre>';print_r($attribute_pa_data);exit;
     
       if(!empty($category_pa_data) && !empty($attribute_pa_data))
      {
        $this->Pa_model->user_pa_data('order',$category_pa_data,$attribute_pa_data,$cartid);
      }
     
  }

  public function smtp_email_validate()
  {
    $email  = $this->input->post('useremail');
    $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => "http://apilayer.net/api/check?access_key=824b8ca79de8de901a98e85e064f4f1f&email=".$email."&smtp=1&format=1",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
    ));

    $response   = curl_exec($curl);
    $err    = curl_error($curl);
    curl_close($curl);
    $responseData = json_decode($response, true);
    echo json_encode($responseData);exit;
  }
  
  public function check_user_credential()
  {
    $email      = $this->input->post('useremail');
    $scpassword   = $this->input->post('userpassword');
    $user_password  = $this->Cart_model->get_user_credential_by_email_password($email);
    if(!empty($user_password) && is_array($user_password))
    {
      $salt       = substr($user_password['password'], 0, 10);
      $db_password  = $salt . substr(sha1($salt . $scpassword), 0, -10);
      $user_data    = $this->Cart_model->get_user_credential_by_email_password($email,$db_password);
      if(is_array($user_data) && !empty($user_data))
      {
        $get_user_address = $this->Cart_model->get_user_address($user_data['id']);
        //echo $this->db->last_query();
        /* if(!empty($get_user_address) && count($get_user_address)>0)
        {
          //echo "From Order Info";
          $user_address['pincode']      = $get_user_address['pincode']; 
          $user_address['shipping_address'] = $get_user_address['shipping_address'];
          $user_address['mobile_no']      = $get_user_address['mobile_no'];
        }
        else{ */
          $user_pincode     = $this->Cart_model->get_user_profession($user_data['id'],'user_pincode');
          $user_shipping    = $this->Cart_model->get_user_profession($user_data['id'],'user_shipaddress');
          $user_mobile    = $this->Cart_model->get_user_profession($user_data['id'],'user_mobile');
          $user_dob       = $this->Cart_model->get_user_profession($user_data['id'],'user_dob');
          //echo $this->db->last_query();
          //echo "From Object Meta";
          $user_address['pincode']      = $user_pincode['object_meta_value'];
          $user_address['shipping_address'] = $user_shipping['object_meta_value'];  
          $user_address['mobile_no']      = $user_mobile['object_meta_value'];
          $user_address['user_dob']     = $user_dob['object_meta_value'];
        /* } */
        $user_profession  = $this->Cart_model->get_user_profession($user_data['id'],'user_profession');
        $user_address['user_profession']  = $user_profession['object_meta_value'];  
        $data['userData'] = ($user_data + $user_address);
      }
      else{
        $data['userData'] = 'false';
      }
    }
    else{
      $data['newUser'] = 'true';
    }
    /* echo "<pre>";
    print_r($data);
    echo "</pre>"; */
    echo json_encode($data, true);exit;
  }
	
  
}
