<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sccart extends MY_Controller {

	function __construct(){

       parent::__construct();
       $this->load->model('Cart_model');
       $this->load->model('Analytics_model');
       $this->load->library('email');
       $this->load->library('user_agent');
       $this->load->library('curl');  
   	}

	public function index()
	{
    $data = array();
    if(isset($_COOKIE['SCUniqueID'])){
      $uniquecookie = $_COOKIE['SCUniqueID'];
      $result = $this->Cart_model->get_cart($uniquecookie);
      $data['cart_info'] = $result; 

    }else{
      echo "Something Wrong";
    }

		$this->load->view('common/header_view');
		$this->load->view('cart/sccart',$data);
		$this->load->view('common/footer_view');
	}

	public function update_cart(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$uniquecookie = $this->input->post('uniquecookie');
			$productid = $this->input->post('productid');
			$userid = $this->input->post('userid');
			$productsize = $this->input->post('productsize');
      $lookid = $this->input->post('lookid');

      if ($this->agent->is_browser())
      {
          $agent = 'Computer '.$this->agent->browser().' '.$this->agent->version();
      }
      elseif ($this->agent->is_robot())
      {
          $agent = 'Robot '.$this->agent->robot();
      }
      elseif ($this->agent->is_mobile())
      {
          $agent = 'Mobile '.$this->agent->mobile();
      }
      else
      {
          $agent = 'Unidentified';
      }

      $platform_info = $this->agent->platform();

			$this->Cart_model->update_cart($uniquecookie,$productid,$userid,$productsize,$agent,$platform_info,$lookid);

      /*Mailchimp call For AddToCart*/
      /*if(!empty($this->session->userdata('user_id')))
      {  
        $mc_cartid = $this->Cart_model->get_cartid($uniquecookie,$productid,$userid,$productsize,$agent,$platform_info,$lookid);        
         $this->curl->simple_post(BACKENDURL.'/Mailchimp/mailchimp_update_cart/'.$mc_cartid.'/'.$this->session->userdata('user_id'), false, array(CURLOPT_USERAGENT => true));     
      }*/
     /*End Mailchimp call For AddToCart*/
		}
	}

	public function get_cart(){
    /*if (!$this->input->is_ajax_request()) {
        exit('No direct script access allowed');
    }else{*/
      $uniquecookie = $this->input->post('uniquecookie');
      $uc_pincode_ = $this->input->post('uc_pincode') ? $this->input->post('uc_pincode') : '';
	  $state_id = '';
	  if($uc_pincode_!=''){ $state_id = $this->Cart_model->get_state_id($uc_pincode_);}
	  // echo $state_id;exit;
      $paymode = $this->input->post('paymode') ? $this->input->post('paymode') : '';
	    $country = $this->input->post('country')!='' ? $this->input->post('country') : '';
      $uc_error = 0;
      $coupon_code = $this->input->post('SCCouponcookie'); 
	  $coupon_info = $this->Cart_model->getCouponData($coupon_code);
      /*$coupon_brand = $this->Cart_model->getCouponData($coupon_code)['brand_id'];
      $coupon_min_spend = $this->Cart_model->getCouponData($coupon_code)['coupon_min_spend'];
      $coupon_max_spend = $this->Cart_model->getCouponData($coupon_code)['coupon_max_spend'];
      $coupon_products = $this->Cart_model->getCouponData($coupon_code)['coupon_products']; 
      $coupon_discount_type = $this->Cart_model->getCouponData($coupon_code)['coupon_discount_type'];
	  $coupon_stylecracker =  $this->Cart_model->getCouponData($coupon_code)['stylecrackerDiscount'];
      $individual_use_only =  $this->Cart_model->getCouponData($coupon_code)['individual_use_only'];*/	
	  $coupon_brand = $coupon_info['brand_id'];
      $coupon_min_spend = $coupon_info['coupon_min_spend'];
      $coupon_max_spend = $coupon_info['coupon_max_spend'];
      $coupon_products = $coupon_info['coupon_products']; 
      $coupon_discount_type = $coupon_info['coupon_discount_type'];
      $coupon_stylecracker =  $coupon_info['stylecrackerDiscount'];
      $individual_use_only =  $coupon_info['individual_use_only'];
      $coupon_total = 0;
	  if(BOROUGH_PRODUCTS!='')
      {
          $scb_products = BOROUGH_PRODUCTS;
      }else
      {
          $scb_products = '';
      }
      if($coupon_products!='' || $coupon_products!=0)
      {
        $coupon_products_arr =explode(',',$coupon_products);
      }

      $result = $this->Cart_model->get_cart($uniquecookie);
     /* echo "<pre>";print_r($result);exit;*/
	  // echo "<pre>";echo $result[0]['product_id'];exit;
	  $show_cod = array();$i=0;
	  
	  if(BOROUGH_PRODUCTS!='')
        {
            $scb_products = BOROUGH_PRODUCTS;
        }else
        {
            $scb_products = '';
        }
	  $show_cod = array();$i=0;
	  foreach($result as $row){
		$product_id = $row['product_id'];
		$product_id = $product_id;
		$findme = "'$product_id'";
		$pos = strpos($scb_products, $findme);
		if ($pos === false) {
			$show_cod[$i] = "0";
		} else {
			$show_cod[$i]= "1";
		}
		$i++;	
	  }
	  
	  if (in_array("0", $show_cod,TRUE))
	  {
		$scb_cod = 0;
	  }else $scb_cod = 1;
	  
	  foreach($result as $row){
		$product_id = $row['product_id'];
		$product_id = $product_id;
		$findme = "'$product_id'";
		$pos = strpos($scb_products, $findme);
		if ($pos === false) {
			// $show_cod[$i] = 0;
			$show_cod[$i] = "0";
		} else {
			// $show_cod[$i]= 1;
			$show_cod[$i]= "1";
		}
		$i++;	
	  }
	   // echo "<pre>";print_r($show_cod);exit;
	  if (in_array("0", $show_cod,TRUE))
	  {
		$scb_cod = 0;
	  }else $scb_cod = 1;
      $carthtml = '';
      $scOrderTotal = 0;
      $total_qty = 0;
      $delivery_status = '';
      $new_store_tax = 0;
      $old_store_tax = 0;
      $new_store_cod = 0;
      $old_store_cod = 0;
      $scWithTax = 0;
      $new_store_shipping = 0;
      $old_store_shipping = 0;
      $new_store_shipping_charges = 0;
      $old_store_shipping_charges = 0;
      $new_store_cod_charges = 0;
      $old_store_cod_charges = 0;
      $scWithShipping = 0;
      $qty_array = array(1,2,3,4,5);

      $final_tax = 0;
      $final_shipping_charges = 0;
      $final_cod_charges = 0;
      $final_sub_total = 0;

      $final_tax_text = '';
      $final_shipping_charges_text = '';
      $final_cod_charges_text = '';
      $final_sub_total_text = '';

      $maxDiscountAmt = 0;    
      $maxDiscountFlag = 0;   
      $brandprdPrice = array();   
      $totalDiscountAmt = 0 ;
      $coupon_brandsSel = '';
      $coupon_productSel = '';
      $coupon_discount = 0;
      /*print_r($coupon_discount);*/
      $coupon_charges_text = '';
      /*$coupon_product_price = 0;*/

      /*if($coupon_discount > 0){
        $couponhtml = '<div class="coupon-charges order-coupon-charges">
                        <span class="text-label">Coupon Discount:</span>
              <span class="cart-price">  <span class="fa fa-inr"></span> '.$coupon_discount.'</span>
            <span class="icon-info popover-cart-price"tabindex="0" data-toggle="popover" data-popover-content="#order-coupon-discount-details"><i class="fa fa-info-circle"></i></span>  </div>
            <div id="order-coupon-discount-details" class="hidden"><div class="popover-cart-price-content">'.$coupon_charges_text.'</div></div>';
         }*/

      if(!empty($result)){

        $carthtml = $carthtml.'<ul class="cart-items">';
        $new_brand_id = ''; $old_brand_id = ''; $f = 0; $product_total = 0; $scOrderTotal = 0;
        $new_brand_name = ''; $old_brand_name = '';

      foreach ($result as $key => $value) {
        $new_brand_id = $value['user_id'];
        $new_brand_name = $value['company_name'];
        $new_store_shipping = $value['is_shipping'];
        $new_store_shipping_charges = $value['shipping_charges'];
        $new_store_cod = $value['is_cod'];
        $new_store_cod_charges = $value['cod_charges'];
        $total_qty = $total_qty + $value['product_qty'];
        $new_store_tax = $value['store_tax'];
        $coupon_brand = $this->Cart_model->getCouponData($coupon_code,$new_brand_id)['brand_id'];

        if($value['discount_percent']!='' || $value['discount_percent']!=0)
        {
           $price = $value['discount_price'];
           $dptag = '<span class="cart-item-price cart-mrp">
                                <span class="fa fa-inr"></span> '.$value['price'].'
                              </span>';

        }else
        {
           $price = $value['price'];
           $dptag = '';
        }

        //echo $coupon_brand.'<br/>';
        if($coupon_brand==$new_brand_id && $coupon_brand!=0)
        {        
          if($coupon_products!=''&& $coupon_products!=0)
          {
            /* coupon_discount_type =3 (Product discount)*/
            if($coupon_discount_type==3)
            {
              if(in_array($value['product_id'], $coupon_products_arr))
              {              
                if($coupon_min_spend<=$price &&  $price<=$coupon_max_spend)
                {
                  $coupon_discount = $this->Cart_model->getCouponData($coupon_code,$value['user_id'])['coupon_amount'];
                  $coupon_charges_text =  $new_brand_name;
                } 
              }
            } 
            /* coupon_discount_type =4 (Product % discount)*/
            if($coupon_discount_type==4)
            {
              if(in_array($value['product_id'], $coupon_products_arr))
              {              
                if($coupon_min_spend<=$price &&  $price<=$coupon_max_spend)
                {
                  $coupon_percent = $this->Cart_model->getCouponData($coupon_code,$value['user_id'])['coupon_amount'];
                  $coupon_discount =  $price*($coupon_percent/100);
                  $coupon_charges_text =  $new_brand_name;
                } 
              }
            }                 

          }else
          { 
            /* coupon_discount_type =1 (cart discount)*/
            if($coupon_discount_type==1)
            {
              $coupon_product_price = $coupon_product_price+$price;
              if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
              {
                $coupon_discount = $this->Cart_model->getCouponData($coupon_code,$value['user_id'])['coupon_amount'];
                $coupon_charges_text =  $new_brand_name;
              }
            }
            /* coupon_discount_type =2 (cart % discount)*/
            if($coupon_discount_type==2)
            {
              $coupon_product_price = $coupon_product_price+$price;
              if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
              {
                $coupon_percent = $this->Cart_model->getCouponData($coupon_code,$value['user_id'])['coupon_amount'];
                $coupon_discount = $coupon_product_price*($coupon_percent/100);
                $coupon_charges_text =  $new_brand_name;
              }
            }
          }          
        }

        /* if($coupon_brand==0 && strtolower($coupon_code)=='sccart20')    
        {     
            $coupon_product_price = $coupon_product_price+$price;   
            if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)    
            {   
              $coupon_percent = $this->Cart_model->getCouponData($coupon_code,$coupon_brand)['coupon_amount'];    
              $coupon_discount = $coupon_product_price*($coupon_percent/100);   
              $coupon_charges_text =  $new_brand_name;    
            }   
        }*/
		
		if(($coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only!=1  )|| ( $coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only==1 && $this->session->userdata('user_id')>0 ))
        {
            /* coupon_discount_type =1 (cart discount)*/
            if($coupon_discount_type==1)
            {  
              $coupon_product_price = $coupon_product_price+$price;
              if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
              {
                $coupon_discount = $this->Cart_model->getCouponData($coupon_code,$coupon_brand)['coupon_amount'];
                $coupon_charges_text =  '';
              }
            }
            /* coupon_discount_type =2 (cart % discount)*/
            if($coupon_discount_type==2)
            {  
              $coupon_product_price = $coupon_product_price+$price;
              if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
              {
                $coupon_percent = $this->Cart_model->getCouponData($coupon_code,$coupon_brand)['coupon_amount'];
                $coupon_discount = $coupon_product_price*($coupon_percent/100);
                $coupon_charges_text =  '';
              }
            }
        }

        $coupon_total = $coupon_product_price;       
       
        if(!empty($value['user_id']))
        {
          $coupon_brandsSel = $coupon_brandsSel.$value['user_id'].',';
        }
        
        if(!empty($value['product_id']))
        {
          $coupon_productSel = $coupon_productSel.$value['product_id'].',';
        }
       
        
        if($value['min_del_days']>0 && $value['max_del_days']>0){
          $delivery_status = 'Delivery in '.$value['min_del_days'].'-'.$value['max_del_days'].' working days.';
        }else if(($value['min_del_days']>0 && $value['max_del_days']=='') || ($value['min_del_days']=='' && $value['max_del_days']>0)){
          $delivery_status = 'Delivery in '.$value['min_del_days'].$value['max_del_days'].' working days.';
        }
      if(($new_brand_id == $old_brand_id) || $f == 0){

                  $carthtml = $carthtml.'<li id="product_cart_info_'.base64_encode($value['id']).'">
                                    <div class="row">
                                      <div class="col-md-2 col-sm-2 col-xs-2 ">
                                      <div class="item-img">
                                        <img src="'.$this->config->item('sc_promotional_look_image_url').$value['image'].'">
                                        </div>
                                      </div>
                                      <div class="col-md-10 col-sm-10 col-xs-10">
                                        <div class="cart-item-inner">
                                          <div class="row">
                                            <div class="col-md-8">
                                              <span class="cart-item-name">'.$value['name'].'</span>
                                              </div>
                                                    <div class="col-md-4 cart-item-right">
                                                    '.$dptag.'
                                              <span class="cart-item-price cart-price">
                                                <span class="fa fa-inr"></span> '.$price.'
                                              </span>
                                              <span class="cart-delete" onclick="deleteProductFromCart(\''.base64_encode($value['id']).'\');RemoveCartPrdGA(\''.addslashes($value['company_name']).' - '.addslashes($value['name']).'\');"><i class="fa fa-times-circle-o"></i> </span>
                                            </div>

  <div class="col-md-12">
                                              <div class="cart-item-meta">
                                              <span class="cart-item-seller">
                                                    <span class="text-label">Seller:</span> <a target="_blank" href="'.base_url().'brands/brand_details/'.$this->getBrandUsername($value['user_id']).'"> '.$value['company_name'].' </a>
                                                    </span>

                                                <span class="qty-wrp">
                                                  <span class="text-label">Size:</span> ';
                                                $carthtml = $carthtml.'<select name="productQty" class="form-control input-sm product-qty" onchange="updateProductSize(this);">';
                                                $size_array = $this->getProductSize($value['product_id'],$value['size_text']);
                                                  if(!empty($size_array)){
                                                    foreach($size_array as $qtyval){
                                                      if($qtyval['size_text'] == $value['size_text']){
                                                        $carthtml = $carthtml.'<option value="'.$qtyval['id'].'" selected>'.$qtyval['size_text'].'</option>';
                                                      }else{
                                                        $carthtml = $carthtml.'<option value="'.$qtyval['id'].'">'.$qtyval['size_text'].'</option>';
                                                      }
                                                    }
                                                  }
                                                $carthtml = $carthtml.'</select>';
                                               
                                                $carthtml = $carthtml.'</span>
                                                <span class="qty-wrp">
                                                  <span class="text-label">Qty:</span> ';
                                                $carthtml = $carthtml.'<select name="productQty" class="form-control input-sm product-qty" onchange="updateProductQty(this,'.$value["product_id"].');">';
                                                  if(!empty($qty_array)){
                                                    foreach($qty_array as $qtyval){
                                                      if($qtyval == $value['product_qty']){
                                                        $carthtml = $carthtml.'<option value="'.$qtyval.'" selected>'.$qtyval.'</option>';
                                                      }else{
                                                        $carthtml = $carthtml.'<option value="'.$qtyval.'">'.$qtyval.'</option>';
                                                      }
                                                    }
                                                  }
                                                $carthtml = $carthtml.'</select>';


                                                $carthtml = $carthtml.'</span>
                                              </div>
                                              <div>
                                              </div>
                                            </div>

                                            <div class="col-md-12">
                                              <div class="row">
                                                <div class="col-md-12">

                                                    <span class="sc-popover-btn" title="" data-toggle="sc-popover" data-content="<div class=\'return-policy-box\'>
                                                  <p>
                                                  '.$value['return_policy'].'
                                                  </p>
                                                      </div>">'.(($value['return_policy']) ? 'Return Policy': '').  ' </span> '
                                                      .(($value['return_policy'] && $delivery_status) ? '|': '').'
                                                    <span class="delivery-status">
                                                      '.$delivery_status.'
                                                    </span>

                                                  </div>
                                                  <div class="col-md-12">';
                                                    if((($uc_pincode_!='' && strchr($this->getBrandExtraInfo($value['user_id'],'cod_exc_location'),$uc_pincode_)!='') || ($this->getBrandExtraInfo($value['user_id'],'cod_available') != 1)) && $paymode != 'card'){
                                                      $uc_error = $uc_error+1;
                                                      $carthtml = $carthtml.'<div class="delivery-status cod-status error" id="cod-status-'.$value['id'].'">COD is not avaliable

                                                    </div>';
                                                  }
                                                  if($this->get_product_availablility($value['product_id']) == 1){
                                                    $uc_error = $uc_error+1;
                                                      $carthtml = $carthtml.'<div class="delivery-status cod-status error">Out of stock

                                                    </div>';
                                                  }
                                                  if($uc_pincode_!='' && strchr($this->getBrandExtraInfo($value['user_id'],'shipping_exc_location'),$uc_pincode_)!=''){
                                                    $uc_error = $uc_error+1;
                                                      $carthtml = $carthtml.'<div class="delivery-status cod-status error" id="not_ship-'.$value['id'].'">Shipping is not avaliable

                                                    </div>';
                                                  }

                                                 $carthtml = $carthtml.' </div>
                                                </div>
                                              </div>

                                            </div>
                                          </div>
                                        </div>

                                      </div>
                                    </li>'; $product_total = $product_total+$price;
                                    $final_sub_total = $final_sub_total+$price;
                                    $scOrderTotal =  $scOrderTotal+$product_total;

                                    $final_sub_total_text = $final_sub_total_text.$value['company_name'].$final_sub_total;

      }else{
        $carthtml = $carthtml.'<li class="seller-amount">';
        /* Shipping charges start*/
          if($this->getBrandExtraInfo($old_brand_id,'is_shipping') == 1){
          if($this->getBrandExtraInfo($old_brand_id,'shipping_min_values') == 0 && $this->getBrandExtraInfo($old_brand_id,'shipping_max_values') == 0){

                  /*$carthtml = $carthtml.'<div class="seller-tax">
                    <span class="text-label">'.$old_brand_name.' Shipping Charges:</span>
                    <span class="cart-price">  <span class="fa fa-inr"></span> '.$this->getBrandExtraInfo($old_brand_id,'shipping_charges').'</span>
                    </div>';*/
                    $final_shipping_charges_text =  $final_shipping_charges_text.'<div class="order-charges-details"><span class="text-label">'.$old_brand_name.' Shipping Charges:</span><span class="cart-price"><span class="fa fa-inr"></span> '.$this->getBrandExtraInfo($old_brand_id,'shipping_charges').'</span></div>';

                      $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand_id,'shipping_charges');
                      $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                  }else if($this->getBrandExtraInfo($old_brand_id,'shipping_min_values') >= 0 && $this->getBrandExtraInfo($old_brand_id,'shipping_max_values') > 0 && $this->getBrandExtraInfo($old_brand_id,'shipping_min_values')<=$product_total && $this->getBrandExtraInfo($old_brand_id,'shipping_max_values')>=$product_total) {

                  /*$carthtml = $carthtml.'<div class="seller-tax">
                    <span class="text-label">'.$old_brand_name.' Shipping Charges:</span>
                    <span class="cart-price">  <span class="fa fa-inr"></span> '.$this->getBrandExtraInfo($old_brand_id,'shipping_charges').'</span>
                    </div>';*/
                      $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand_id,'shipping_charges');
                      $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;

                       $final_shipping_charges_text =  $final_shipping_charges_text.'<div class="order-charges-details"><span class="text-label">'.$old_brand_name.' Shipping Charges:</span><span class="cart-price"><span class="fa fa-inr"></span> '.$this->getBrandExtraInfo($old_brand_id,'shipping_charges').'</span></div>';

                  }else { $old_store_shipping_charges = 0; }
                }else{ $old_store_shipping_charges = 0; }
                /* Shipping charges end*/

                /* COD charges start*/
          if($this->getBrandExtraInfo($old_brand_id,'is_cod') == 1 && $paymode == 'cod'){
          if($this->getBrandExtraInfo($old_brand_id,'cod_min_value') == 0 && $this->getBrandExtraInfo($old_brand_id,'code_max_value') == 0){

                  /*$carthtml = $carthtml.'<div class="seller-tax">
                    <span class="text-label">'.$old_brand_name.' Cod Charges:</span>
                    <span class="cart-price">  <span class="fa fa-inr"></span> '.$this->getBrandExtraInfo($old_brand_id,'cod_charges').'</span>
                    </div>';*/
                      $old_store_cod_charges = $this->getBrandExtraInfo($old_brand_id,'cod_charges');
                      $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                      $final_cod_charges_text =  $final_cod_charges_text.'<div class="order-charges-details"><span class="text-label">'.$old_brand_name.' COD Charges:</span><span class="cart-price"><span class="fa fa-inr"></span> '.$this->getBrandExtraInfo($old_brand_id,'cod_charges').'</span></div>';


                  }else if($this->getBrandExtraInfo($old_brand_id,'cod_min_value') >= 0 && $this->getBrandExtraInfo($old_brand_id,'code_max_value') > 0 && $this->getBrandExtraInfo($old_brand_id,'cod_min_value')<=$product_total && $this->getBrandExtraInfo($old_brand_id,'code_max_value')>=$product_total) {

                  /*$carthtml = $carthtml.'<div class="seller-tax">
                    <span class="text-label">'.$old_brand_name.' Cod Charges:</span>
                    <span class="cart-price">  <span class="fa fa-inr"></span> '.$this->getBrandExtraInfo($old_brand_id,'cod_charges').'</span>
                    </div>';*/
                      $old_store_cod_charges = $this->getBrandExtraInfo($old_brand_id,'cod_charges');
                      $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;

                      $final_cod_charges_text =  $final_cod_charges_text.'<div class="order-charges-details"><span class="text-label">'.$old_brand_name.' COD Charges:</span><span class="cart-price"><span class="fa fa-inr"></span> '.$this->getBrandExtraInfo($old_brand_id,'cod_charges').'</span></div>';

                  }else { $old_store_cod_charges = 0; }
                }else{ $old_store_cod_charges = 0; }
                /* COD charges end*/


                if(($product_total*($old_store_tax/100)) !=0){
                  $final_tax = $final_tax + ($product_total*($old_store_tax/100));
                  $final_tax_text =  $final_tax_text.'<div class="order-charges-details"><span class="text-label">'.$old_brand_name.' Tax:</span><span class="cart-price"><span class="fa fa-inr"></span> '.($product_total*($old_store_tax/100)).'</span></div>';
                /*$carthtml = $carthtml.'<div class="seller-tax">
                    <span class="text-label">'.$old_brand_name.' Tax:</span>
                    <span class="cart-price">  <span class="fa fa-inr"></span> '.($product_total*($old_store_tax/100)).'</span>
                  </div>';*/
                }
                 /*$carthtml = $carthtml.'<div class="seller-subtotal"><span class="text-label">'.$old_brand_name.' Sub Total:</span> <span class="cart-price"><span class="fa fa-inr"></span> '.($product_total+($product_total*($old_store_tax/100))+$old_store_shipping_charges+$old_store_cod_charges).'</span></div>
                  </li>';*/
                  $carthtml = $carthtml.'</li>';
                  $scWithTax = $scWithTax + ($product_total+($product_total*($old_store_tax/100))) +$old_store_shipping_charges+$old_store_cod_charges;

                  $product_total = 0;
      $carthtml = $carthtml.'<li id="product_cart_info_'.base64_encode($value['id']).'">
                                    <div class="row">
                                      <div class="col-md-2 col-sm-2 col-xs-2">
                                        <div class="item-img">
                                        <img src="'.$this->config->item('sc_promotional_look_image_url').$value['image'].'">
                                        </div>
                                      </div>
                                      <div class="col-md-10 col-sm-10 col-xs-10">
                                        <div class="cart-item-inner">
                                          <div class="row">
                                            <div class="col-md-8">
                                              <span class="cart-item-name">'.$value['name'].'</span>
                                              </div>
                                                <div class="col-md-4 cart-item-right">
                                                '.$dptag.'
                                              <span class="cart-item-price cart-price">
                                                <span class="fa fa-inr"></span> '.$price.'
                                              </span>

                                              <span class="cart-delete" onclick="deleteProductFromCart(\''.base64_encode($value['id']).'\');RemoveCartPrdGA(\''.addslashes($value['company_name']).' - '.addslashes($value['name']).'\');"><i class="fa fa-times-circle-o"></i> </span>
                                            </div>
                                              <div class="col-md-12">
                                              <div class="cart-item-meta">
                                                <span class="cart-item-seller">
                                                    <span class="text-label">Seller:</span> <a target="_blank" href="'.base_url().'brands/brand_details/'.$this->getBrandUsername($value['user_id']).'"> '.$value['company_name'].' </a></span>


                                                <span class="qty-wrp">
                                                  <span class="text-label">Size:</span> ';
                                                       $carthtml = $carthtml.'<select name="productQty" class="form-control input-sm product-qty" onchange="updateProductSize(this);">';
                                                $size_array = $this->getProductSize($value['product_id'],$value['size_text']);
                                                  if(!empty($size_array)){
                                                    foreach($size_array as $qtyval){
                                                      if($qtyval['size_text'] == $value['size_text']){
                                                        $carthtml = $carthtml.'<option value="'.$qtyval['id'].'" selected>'.$qtyval['size_text'].'</option>';
                                                      }else{
                                                        $carthtml = $carthtml.'<option value="'.$qtyval['id'].'">'.$qtyval['size_text'].'</option>';
                                                      }
                                                    }
                                                  }
                                                $carthtml = $carthtml.'</select>';
                                                $carthtml = $carthtml.'</span>
                                                <span class="qty-wrp">
                                                  <span class="text-label">Qty:</span> ';

                                                   $carthtml = $carthtml.'<select name="productQty" class="form-control input-sm product-qty" onchange="updateProductQty(this,'.$value["product_id"].');">';
                                                  if(!empty($qty_array)){
                                                    foreach($qty_array as $qtyval){
                                                      if($qtyval == $value['product_qty']){
                                                        $carthtml = $carthtml.'<option value="'.$qtyval.'" selected>'.$qtyval.'</option>';
                                                      }else{
                                                        $carthtml = $carthtml.'<option value="'.$qtyval.'">'.$qtyval.'</option>';
                                                      }
                                                    }
                                                  }
                                                $carthtml = $carthtml.'</select>';

                                               $carthtml = $carthtml.'</span>
                                              </div>
                                              <div>
                                              </div>
                                            </div>

                                            <div class="col-md-12">
                                              <div class="row">
                                                  <div class="col-md-12">
                                                  <span class="sc-popover-btn" title="" data-toggle="sc-popover" data-content="<div class=\'return-policy-box\'>
                                                  <p> '.$value['return_policy'].'
                                                  </p>
                                                      </div>">'.(($value['return_policy']) ? 'Return Policy': '').  ' </span> '
                                                      .(($value['return_policy'] && $delivery_status) ? '|': '').'
                                                    <span class="delivery-status">
                                                      '.$delivery_status.'
                                                    </span>
                                                  </div>
                                                  <div class="col-md-12">';
                                                    if((($uc_pincode_!='' && strchr($this->getBrandExtraInfo($value['user_id'],'cod_exc_location'),$uc_pincode_)!='' ) || ($this->getBrandExtraInfo($value['user_id'],'cod_available') != 1)) && $paymode != 'card'){
                                                      $uc_error = $uc_error+1;
                                                      $carthtml = $carthtml.'<div class="delivery-status cod-status error" id="cod-status-'.$value['id'].'">COD is not avaliable

                                                    </div>';
                                                  }
                                                  if($this->get_product_availablility($value['product_id']) == 1){
                                                    $uc_error = $uc_error+1;
                                                      $carthtml = $carthtml.'<div class="delivery-status cod-status error">Out of stock
                                                    </div>';
                                                  }
                                                  if($uc_pincode_!='' && strchr($this->getBrandExtraInfo($value['user_id'],'shipping_exc_location'),$uc_pincode_)!=''){
                                                    $uc_error = $uc_error+1;
                                                      $carthtml = $carthtml.'<div class="delivery-status cod-status error" id="not_ship-'.$value['id'].'">Shipping is not avaliable
                                                    </div>';
                                                  }

                                                $carthtml = $carthtml.'</div>
                                              </div>

                                            </div>
                                          </div>
                                        </div>
                                        </div>
                                      </div>
                                    </li>'; $product_total = $product_total+$price;
                                     $final_sub_total = $final_sub_total+$price;
                                     $final_sub_total_text = $final_sub_total_text.$value['company_name'].$final_sub_total;
                                    $scOrderTotal =  $scOrderTotal+$product_total+($product_total*($old_store_tax/100))+$old_store_shipping_charges;
                                }
                                $old_brand_id = $new_brand_id; $f++;
                                $old_brand_name = $value['company_name'];
                                $old_store_tax = $value['store_tax'];
                                $old_store_shipping = $value['is_shipping'];
                                $old_store_shipping_charges = $value['shipping_charges'];
                                $old_store_cod = $value['is_cod'];
                                $old_store_cod_charges = $value['cod_charges'];
                                $delivery_status = '';

                }
            $carthtml = $carthtml.'<li class="seller-amount">';
            /* Shipping charges start*/
            if($this->getBrandExtraInfo($old_brand_id,'is_shipping') == 1){
          if($this->getBrandExtraInfo($old_brand_id,'shipping_min_values') == 0 && $this->getBrandExtraInfo($old_brand_id,'shipping_max_values') == 0){

                  /*$carthtml = $carthtml.'<div class="seller-tax">
                    <span class="text-label">'.$old_brand_name.' Shipping Charges:</span>
                    <span class="cart-price">  <span class="fa fa-inr"></span> '.$this->getBrandExtraInfo($old_brand_id,'shipping_charges').'</span>
                    </div>';*/
                      $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand_id,'shipping_charges');
                      $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;

                    $final_shipping_charges_text =  $final_shipping_charges_text.'<div class="order-charges-details"><span class="text-label">'.$old_brand_name.' Shipping Charges:</span><span class="cart-price"><span class="fa fa-inr"></span> '.$this->getBrandExtraInfo($old_brand_id,'shipping_charges').'</span></div>';



                  }else if($this->getBrandExtraInfo($old_brand_id,'shipping_min_values') >= 0 && $this->getBrandExtraInfo($old_brand_id,'shipping_max_values') > 0 && $this->getBrandExtraInfo($old_brand_id,'shipping_min_values')<=$product_total && $this->getBrandExtraInfo($old_brand_id,'shipping_max_values')>=$product_total) {

                  /*$carthtml = $carthtml.'<div class="seller-tax">
                    <span class="text-label">'.$old_brand_name.' Shipping Charges:</span>
                    <span class="cart-price">  <span class="fa fa-inr"></span> '.$this->getBrandExtraInfo($old_brand_id,'shipping_charges').'</span>
                    </div>';*/
                      $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand_id,'shipping_charges');
                      $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;

                      $final_shipping_charges_text =  $final_shipping_charges_text.'<div class="order-charges-details"><span class="text-label">'.$old_brand_name.' Shipping Charges:</span><span class="cart-price"><span class="fa fa-inr"></span> '.$this->getBrandExtraInfo($old_brand_id,'shipping_charges').'</span></div>';

                  }else { $old_store_shipping_charges = 0; }
                }else{ $old_store_shipping_charges = 0; }
                /* Shipping charges end*/

                /* cod charges start*/
            if($this->getBrandExtraInfo($old_brand_id,'is_cod') == 1 && $paymode == 'cod'){
          if($this->getBrandExtraInfo($old_brand_id,'cod_min_value') == 0 && $this->getBrandExtraInfo($old_brand_id,'code_max_value') == 0){

                  /*$carthtml = $carthtml.'<div class="seller-tax">
                    <span class="text-label">'.$old_brand_name.' Cod Charges:</span>
                    <span class="cart-price">  <span class="fa fa-inr"></span> '.$this->getBrandExtraInfo($old_brand_id,'cod_charges').'</span>
                    </div>';*/
                      $old_store_cod_charges = $this->getBrandExtraInfo($old_brand_id,'cod_charges');
                      $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;

                      $final_cod_charges_text =  $final_cod_charges_text.'<div class="order-charges-details"><span class="text-label">'.$old_brand_name.' COD Charges:</span><span class="cart-price"><span class="fa fa-inr"></span> '.$this->getBrandExtraInfo($old_brand_id,'cod_charges').'</span></div>';

                  }else if($this->getBrandExtraInfo($old_brand_id,'cod_min_value') >= 0 && $this->getBrandExtraInfo($old_brand_id,'code_max_value') > 0 && $this->getBrandExtraInfo($old_brand_id,'cod_min_value')<=$product_total && $this->getBrandExtraInfo($old_brand_id,'code_max_value')>=$product_total) {

                  /*$carthtml = $carthtml.'<div class="seller-tax">
                    <span class="text-label">'.$old_brand_name.' Cod Charges:</span>
                    <span class="cart-price">  <span class="fa fa-inr"></span> '.$this->getBrandExtraInfo($old_brand_id,'cod_charges').'</span>
                    </div>';*/

                   $final_cod_charges_text =  $final_cod_charges_text.'<div class="order-charges-details"><span class="text-label">'.$old_brand_name.' COD Charges:</span><span class="cart-price"><span class="fa fa-inr"></span> '.$this->getBrandExtraInfo($old_brand_id,'cod_charges').'</span></div>';

                      $old_store_cod_charges = $this->getBrandExtraInfo($old_brand_id,'cod_charges');
                      $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                  }else { $old_store_cod_charges = 0; }
                }else{ $old_store_cod_charges = 0; }
                /* cod charges end*/

            if(($product_total*($old_store_tax/100)) !=0){
              $final_tax = $final_tax + ($product_total*($old_store_tax/100));
              $final_tax_text =  $final_tax_text.'<div class="order-charges-details"><span class="text-label">'.$old_brand_name.' Tax:</span><span class="cart-price"><span class="fa fa-inr"></span> '.($product_total*($old_store_tax/100)).'</span></div>';
            /*$carthtml = $carthtml.'<div class="seller-tax">
                    <span class="text-label">'.$old_brand_name.' Tax:</span>
                    <span class="cart-price">  <span class="fa fa-inr"></span> '.($product_total*($old_store_tax/100)).'</span>
                  </div>';*/
            }
            /*$carthtml = $carthtml.'<div class="seller-subtotal"><span class="text-label">'.$old_brand_name.' Sub Total:</span> <span class="cart-price"><span class="fa fa-inr"></span> '.($product_total+($product_total*($old_store_tax/100))+$old_store_shipping_charges+$old_store_cod_charges).'</span></div>
                  </li>';*/
            $carthtml = $carthtml.'</li>';
                   $scWithTax = $scWithTax + ($product_total+($product_total*($old_store_tax/100)))+$old_store_shipping_charges+$old_store_cod_charges;

      $carthtml = $carthtml.'</ul>';
    }else{
      $carthtml = $carthtml.'<div class="cart-empty-wrp">
    <div class="text-label">
    Your Shopping Cart Is Empty
    </div>
      <div>
                                        <span class="btn btn-primary close111" data-dismiss="modal">Start Purchasing now</span>
                                      </div></div>';
    }    
    $scWithTax = $scWithTax - $coupon_discount;
	if($scb_cod == 1 && strtolower($country) != 'india'){
		$carthtml = $carthtml.'<input type="hidden" name="OrderTotal" id="OrderTotal" value="'.round(round($scWithTax,2)+SCB_CHARGES).'">';
	}else{
		$carthtml = $carthtml.'<input type="hidden" name="OrderTotal" id="OrderTotal" value="'.round($scWithTax,2).'">';
	}
    $carthtml = $carthtml.'<input type="hidden" name="totalQty" id="totalQty" value="'.$total_qty.'">';
    $carthtml = $carthtml.'<input type="hidden" name="paymod" id="paymod" value="cod">';
    $carthtml = $carthtml.'<input type="hidden" name="ucsecsue" id="ucsecsue" value="'.$uc_error.'">';
    $carthtml = $carthtml.'<input type="hidden" name="ucCoupon" id="ucCoupon" value="">';
	$first_pro = @$result[0]['product_id'] > 0 ? $result[0]['product_id'] : 0;
	
	if($scb_cod == 1){
		$carthtml = $carthtml.'<input type="hidden" name="boroughprod" id="boroughprod" value="yes">';	
	}

  $coupon_total = $final_sub_total;
    $starthtml = '<h4 class="mycart-title">Review your Order</h4><div class="cart-items-wrp" id="cart-items-wrp12111"><div class="cart-items-inner">';
    /*<button class="btn btn-secondary btn-sm"  onclick="redeemCoupon(\''.base64_encode($coupon_brandsSel).'\');"  >Apply</button>*/
    /*onclick="redeemCoupon(\''.base64_encode($coupon_brandsSel).'\');"*/
    //$coupon_brandsSel = rtrim($coupon_brandsSel,',');
    $endhtml = '</div></div>
    <div class="order-amount"><div class="row"><div class="col-md-5">    
    <div class="form-group coupon-box"><div class="coupon-label">Enter Coupon Code</div><input type="text" name ="Coupon" id="Coupon" class="form-control input-sm" value="" placeholder="Coupon Code">
    <span class="btn btn-secondary btn-sm"  onclick="redeemCoupon(\''.base64_encode($coupon_brandsSel).'\',\''.base64_encode($coupon_productSel).'\',\''.base64_encode($coupon_total).'\');"  >Apply</span>
    <div><span class="coupon-status" id="coupon-status" ></span></div>
    </div>
    </div><div class="col-md-7"><div class="order-charges-wrp">';
        if($final_sub_total > 0){
        $endhtml = $endhtml.'<div class="order-charges order-subtotal">
                        <span class="text-label">Total:</span>
              <span class="cart-price">  <span class="fa fa-inr"></span> '.round($final_sub_total,2).'</span>
              </div>';
            }

        if($final_tax > 0){
        $endhtml = $endhtml.'<div class="order-charges order-tax">
                        <span class="text-label">Tax:</span>
              <span class="cart-price">  <span class="fa fa-inr"></span> '.round($final_tax,2).'</span><span class="icon-info popover-cart-price" tabindex="0" data-toggle="popover" data-popover-content="#order-tax-charges-details"><i class="fa fa-info-circle"></i></span></div>
              <div id="order-tax-charges-details" class="hidden"><div class="popover-cart-price-content">'.$final_tax_text.'</div></div>';
            }

            //</div><a data-toggle="popover" data-placement="right" data-content="'.$final_tax_text.'"><i class="fa fa-eye"></i></a>';

    if($final_cod_charges > 0){
        $endhtml = $endhtml.'<div class="order-charges order-cod-charges">
                        <span class="text-label">COD charges:</span>
              <span class="cart-price">  <span class="fa fa-inr"></span> '.round($final_cod_charges,2).'</span>
                <span class="icon-info popover-cart-price"tabindex="0" data-toggle="popover" data-popover-content="#order-cod-charges-details"><i class="fa fa-info-circle"></i></span> </div>
<div id="order-cod-charges-details" class="hidden"><div class="popover-cart-price-content">'.$final_cod_charges_text.'</div></div>';
            }

    if($final_shipping_charges > 0){
        $endhtml = $endhtml.'<div class="order-charges order-shipping-charges">
                        <span class="text-label">Shipping charges:</span>
              <span class="cart-price">  <span class="fa fa-inr"></span> '.$final_shipping_charges.'</span>
            <span class="icon-info popover-cart-price"tabindex="0" data-toggle="popover" data-popover-content="#order-shipping-charges-details"><i class="fa fa-info-circle"></i></span>  </div>
<div id="order-shipping-charges-details" class="hidden"><div class="popover-cart-price-content">'.$final_shipping_charges_text.'</div></div>';
            }

        if($coupon_charges_text!='')
        {
          $brand_coupon_text_visible = '';
        }else
        {
          $brand_coupon_text_visible = 'hide';
        }

       if($coupon_discount > 0){
        $endhtml = $endhtml.'<div class="order-charges coupon-charges order-coupon-charges">
                        <span class="text-label">Coupon Discount:</span>
              <span class="cart-price">  <span class="fa fa-inr"></span> '.$coupon_discount.'</span>
            <span class="icon-info popover-cart-price '.$brand_coupon_text_visible.'"tabindex="0" data-toggle="popover" data-popover-content="#order-coupon-discount-details"><i class="fa fa-info-circle"></i></span>  </div>
<div id="order-coupon-discount-details" class="hidden"><div class="popover-cart-price-content">'.$coupon_charges_text.'</div></div>';
            }
			
		if($scb_cod == 1 && strtolower($country) != 'india'){
			$endhtml = $endhtml.'<div class="order-charges coupon-charges order-coupon-charges">
                        <span class="text-label">International Shipping Charges</span>
              <span class="cart-price">  <span class="fa fa-inr"></span>'.SCB_CHARGES.'</span>
            </div>';		
			$carthtml = $carthtml.'<input type="hidden" name="scb_charges" id="scb_charges" value="'.SCB_CHARGES.'">';
		}

        $endhtml = $endhtml.'</div></div></div><div class="order-total-amount">Order Total : <span class="cart-price" id="scOrderTotal"><span class="fa fa-inr"></span> 0</span></div>
                </div>';


    echo $starthtml.$carthtml.$endhtml;
 /* } */

}
  public function get_product_availablility($product_id){
    $result = $this->Cart_model->get_product_availablility($product_id);
    return $result;
  }
	public function deleteCartProduct(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$uniquecookie = $this->input->post('uniquecookie');
			$cart = base64_decode($this->input->post('cart'));
			$this->Cart_model->deleteCartProduct($uniquecookie,$cart);
		}
	}

	public function getBrandUsername($brand_id){
		return $this->Cart_model->getBrandUsername($brand_id);
	}

	public function get_city_name(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$pincode = $this->input->post('pincode');
			$SCUniqueID = $this->input->post('SCUniqueID');
			echo $this->Cart_model->get_city_name($pincode,$SCUniqueID);
		}
	}

	public function productQtyUpdate(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$uniquecookie = $this->input->post('uniquecookie');
			$productQty = $this->input->post('productQty');
			$productInfo = base64_decode(str_replace("product_cart_info_","",$this->input->post('productInfo')));
			if($productQty>0 && $productQty<=5){
				$this->Cart_model->productQtyUpdate($uniquecookie,$productQty,$productInfo);
			}
			echo 'done';
		}
	}

	public function getBrandExtraInfo($brand_id,$field_name){
		$result = $this->Cart_model->getBrandExtraInfo($brand_id);
		return @$result[0][$field_name];
	}

	public function sc_uc_order(){
   /* echo '<pre>'; print_r($this->input->post());*/
		$uc_first_name = $this->input->post('uc_first_name');
		$uc_last_name = $this->input->post('uc_last_name');
		$uc_email_id = $this->input->post('uc_email_id');
		$uc_mobile = $this->input->post('uc_mobile');
		$uc_address = str_replace("\n", '', $this->input->post('uc_address'));
		$uc_pincode = $this->input->post('uc_pincode');
		$uc_city = $this->input->post('uc_city');
		$uc_state = $this->input->post('uc_state');
		$uc_state2 = $this->input->post('uc_state2');
		//if($uc_state2!='') { $uc_state = '36'; }
		// if($uc_state == '' && $uc_state2 != ''){ $uc_state = $uc_state2; }
		$country = $this->input->post('country');
		// echo $uc_state."-----------------".$country;exit;
		$paymod = $this->input->post('paymod');
		$SCUniqueID = $this->input->post('SCUniqueID');
		$orderval = $this->input->post('orderval');
		$ucsecsue =$this->input->post('ucsecsue');
		$existing_shipping_add =$this->input->post('existing_shipping_add');
		$ucCoupon = $this->input->post('uc_coupon');
    //$service_provider =$this->input->post('service_provider');
    $_SESSION['SCCouponCode'] = $ucCoupon;
    
    $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		$data = array();

		$data['uc_first_name'] = $uc_first_name;
		$data['uc_last_name'] = $uc_last_name;
		$data['uc_email_id'] = $uc_email_id;
		$data['uc_mobile'] = $uc_mobile;
		$data['uc_address'] = $uc_address;
		$data['uc_pincode'] = $uc_pincode;
		$data['uc_city'] = $uc_city;
		$data['uc_state'] = $uc_state;
		$data['country'] = $country;
		$data['paymod'] = $paymod;
		$data['SCUniqueID'] = $SCUniqueID;
		$data['orderval'] = $orderval;
		$data['ucsecsue'] = $ucsecsue;
		$data['existing_shipping_add'] = $existing_shipping_add;
		$data['mihpayid'] = '';
		$data['txnid'] = '';
		$data['coupon_code'] = $ucCoupon;
		
		// echo "<pre>";print_r($data);exit;
    if($paymod == 'cod'){
      $res = $this->Cart_model->sc_uc_order($data);
      $obj = json_decode($res);
      $this->orderPlaceMessage($obj->user_info,$obj->msg,$uc_first_name,$uc_last_name);
      $this->orderPlaceMessagetoBrand($obj->user_info,$obj->msg,$data['uc_first_name'],$data['uc_last_name']);
		
      echo $res;

    }else{
		$paymod_online = '2';
		if($existing_shipping_add != 0 && $existing_shipping_add > 0){
			$existingAddress = array();
			$existingAddress = $this->Cart_model->existingAddress($existing_shipping_add); 
			if($uc_state!=36) {
				//$uc_pincode = $existingAddress[0]['pincode'];
				//$uc_city = $existingAddress[0]['city_name'];
			}
			if($uc_state2!='') { $uc_state = '36'; }
		}
		$result = $this->Cart_model->failed_order($uc_first_name,$uc_last_name,$uc_email_id,$uc_address,$SCUniqueID,$uc_mobile,$paymod_online,$uc_pincode,$uc_city,$country,$uc_state2);
      $MERCHANT_KEY = MERCHANT_KEY;
      $SALT = PAYUSALT;
      $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
      /*$hash = $MERCHANT_KEY.'|'.$txnid.'|'.$orderval.'|{"paymentParts":[{"name":"splitID1","value":"6","merchantId":"396446","description":"test description","commission":"2"}]}|'.$uc_email_id.'|||||||||||'.$SALT;*/

      //Pay u biz hash key
      $hash = $MERCHANT_KEY.'|'.$txnid.'|'.$orderval.'|{"paymentParts":[{"name":"splitID1","value":"6","merchantId":"396446","description":"test description","commission":"2"}]}|'.$uc_first_name.''.$uc_last_name.'|'.$uc_email_id.'|'.$SCUniqueID.'|'.$uc_address.'|'.$uc_pincode.'|'.$uc_city.'|'.$uc_state.'||||||'.$SALT;
     
		// echo $MERCHANT_KEY.'|'.$txnid.'|'.$orderval.'|{"paymentParts":[{"name":"splitID1","value":"6","merchantId":"396446","description":"test description","commission":"2"}]}|'.$uc_first_name.''.$uc_last_name.'|'.$uc_email_id.'|'.$SCUniqueID.'|'.$uc_address.'|'.$uc_pincode.'|'.$uc_city.'|'.$uc_state.'||||||'.$SALT;
      $hash = strtolower(hash('sha512', $hash));
      echo '{"order" : "online","chk_pay_mode":"card_pay","payu_hash":"'.$hash.'","txnid":"'.$txnid.'","amount":"'.$orderval.'","email":"'.$uc_email_id.'","firstname":"'.$uc_first_name.'","phoneno":"'.$uc_mobile.'","address_sc":"'.$uc_address.'"}';

    }

	}

  function orderPlaceMessage($user_id,$order_id,$first_name,$last_last){
	  
    if($this->getEmailAddress($user_id)!=''){
      $config['protocol'] = 'smtp';
       $totalProductPrice=0;

//      $config['mailpath'] = '/usr/sbin/sendmail';
      $config['charset'] = 'iso-8859-1';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');
		
      $this->email->initialize($config);
      $res = $this->Cart_model->su_get_order_info($order_id);    

      $brand_price = $this->Cart_model->su_get_order_price_info($order_id);
	  // echo "<pre>";print_r($brand_price);exit;
	  if($brand_price[0]['state_id'] == '36') { $brand_price[0]['state_name'] = $brand_price[0]['it_state']; }
      /*Coupon Discount */
      foreach($res as $val){
         $totalProductPrice = $totalProductPrice+$val['product_price'];         
      }
     $coupon_result = $this->calculateCouponAmount($res,$totalProductPrice);
      /*echo '<pre>';print_r($coupon_result);*/
      $coupon_discount_type= @$coupon_result['coupon_discount_type'];
      $coupon_discount= @$coupon_result['coupon_discount'];
      $sc_coupon_discount = @$coupon_result['coupon_amount'];
      $coupon_code = strtoupper($coupon_result['coupon_code']);
      /*Coupon Discount End */
      $productInfohtml = '';
      $pay_mode = '';
                $i = 0;
      $productInfohtml = '';     
      $coupDiscountPrdPrice = 0;
      $coupDiscountPrdPricetxt = '';      

                  if(!empty($res)){
                  foreach($res as $val){
                    $i++;
                    $delivery_status="";
                    if($val['min_del_days']>0 && $val['max_del_days']>0){
                      $delivery_status = '<span style="font-size: 11px;color: #2db143;">Delivery in '.$val['min_del_days'].'-'.$val['max_del_days'].' working days.</span>';
                    }else if(($val['min_del_days']>0 && $val['max_del_days']=='') || ($val['min_del_days']=='' && $val['max_del_days']>0)){
                      $delivery_status = '<span style="font-size: 11px;color: #2db143;">Delivery in '.$val['min_del_days'].$val['max_del_days'].' working days.</span>';
                    }

                     if($coupon_code!='')
                    { 
                      if($coupon_discount_type==1)
                      {                        
                        $coupDiscountPrdPrice = $this->cart_model->calculate_product_discountAmt($val['product_price'],$totalProductPrice,$coupon_discount_type,$coupon_discount);
                        $coupDiscountPrdPricetxt = '( Discounted Amt: &#8377; '.round($coupDiscountPrdPrice).')';
                      }else if($coupon_discount_type==2)
                      {                                          
                        $coupDiscountPrdPrice = $this->cart_model->calculate_product_discountAmt($val['product_price'],$totalProductPrice,$coupon_discount_type,$sc_coupon_discount);
                        $coupDiscountPrdPricetxt = '<br/>( Discounted Amt: &#8377; '.round($coupDiscountPrdPrice).')';
                      }
                    }

                   $productInfohtml =$productInfohtml.' <tr>
                      <td><img src="'. $this->config->item('sc_promotional_look_image').$val['image'].'" width="50" style="border:1px solid #cccccc; padding:2px; width:auto; height:auto; max-width:50px; max-height:50px;"></td>
                      <td>'. $val['name'].'<br/> '.$delivery_status.' </td>
                      <td>'. $val['product_qty'].' </td>
                      <td>'. $val['size_text'].'</td>
                      <td>'. $val['company_name'].' </td>
                      <td style="text-align:right; width:60px;">&#8377; '. $val['product_price'].' '.$coupDiscountPrdPricetxt.'</td></tr>';

                  }
                }
                  $cod_amount = $brand_price[0]['cod_amount']>0 ? $brand_price[0]['cod_amount'] : 0 ;
                $shipping_amount = $brand_price[0]['shipping_amount']>0 ? $brand_price[0]['shipping_amount'] : 0 ;
                 
                 if($brand_price[0]['order_tax_amount'] > 0) {
                   $productInfohtml =$productInfohtml.' <tr>
                   <td colspan="5" style="padding:2px 8px; text-align:right;">Tax :</td>
                    <td style="padding:2px 8px; text-align:right;">
                      &#8377;  '.$brand_price[0]['order_tax_amount'].'
                    </td>
                  </tr>';
                  }
                  if($cod_amount > 0) {
                  $productInfohtml =$productInfohtml.'<tr>

                    <td colspan="5" style="padding:2px 8px; text-align:right;">COD Charges:</td>
                    
                    <td style="padding:2px 8px; text-align:right;">
                      &#8377;  '.$cod_amount.'
                    </td>
                  </tr>';
                }
                if($shipping_amount > 0){
                  $productInfohtml =$productInfohtml.'<tr>
                    <td colspan="5" style="padding:2px 8px; text-align:right;">Shipping Charges:
                    </td>
                    <td style="padding:2px 8px; text-align:right;">
                      &#8377;  '.$shipping_amount.'
                    </td>
                  </tr>';
                }

                if($coupon_discount>0)
                { 
                  $productInfohtml =$productInfohtml.'<tr>
                    <td colspan="5" style="padding:2px 8px; text-align:right;">Coupon Code <span color="#f87106">'.$coupon_code.'</span>  Discount:
                    </td>
                    <td style="padding:2px 8px; text-align:right;">
                      &#8377;  '.round($coupon_discount,2).'
                    </td>
                  </tr>';
                }
                if($brand_price[0]['pay_mode'] == 1){
                $pay_mode = '<p style="margin-bottom:20px;">
                  Payment Mode : <img src="https://www.stylecracker.com/assets/images/icons/ic-cod-1.png" style="vertical-align:middle;"> Cash on Delivery
                </p>';
                }

                if($brand_price[0]['pay_mode'] == 2){
                $pay_mode = '<p style="margin-bottom:20px;">
                  Payment Mode : <img src="https://www.stylecracker.com/assets/images/icons/ic-online-payment-1.png" style="vertical-align:middle;"> Online Payment
                </p>';
                }
                /* if($brand_price[0]['order_total'] > 0) {
                  $productInfohtml =$productInfohtml.'<tr>
                    <td>
                      <strong>Order Total :<strong> </td>
                      <td>
                      &#8377;  '.$brand_price[0]['order_total'].'
                      </td>
                    </tr>
                  ';
              }*/

$mailOrderTotal = round(($brand_price[0]['order_total']-$coupon_discount),2);
 $productInfohtml =$productInfohtml.'</tbody>
      </table>
      <div style="text-align:right; background:#f3f3f3; padding:5px 30px; font-size:18px; text-transform:uppercase; margin-top:20px; border-bottom:2px solid #cccccc;">
        Total Amount  : &#8377; '.$mailOrderTotal.'
      </div>
    </td>
  </tr>';
           $productInfohtml =$productInfohtml.' <tr>
    <td style="padding:0px 30px;">
        <div style=" border-bottom:1px solid #ccc; padding-bottom:30px;">
        Thank you for shopping with <strong>StyleCracker</strong>.<br/>
For any query or assistance, feel free to <a href="https://www.stylecracker.com/schome/aboutus" style="color:#00b19c; text-decoration:none;">Contact Us</a>
      </div>
    </td>
  </tr>
  <tr>
    <td style="vertical-align:middle; padding:20px 30px;">
      <span style="color: #888; font-weight: bold; line-height: 1.3; position: relative;top: -4px; vertical-align:top;">  Follow Us:  </span>

      <a target="_blank" href="https://www.facebook.com/StyleCracker" target="_blank"  title="Facebook"><img src="https://www.stylecracker.com/assets/images/social-media/facebook.png" alt="Facebook" style="width:24px;"></a>
      <a target="_blank" href="https://twitter.com/Style_Cracker" target="_blank"  title="Twitter"><img src="https://www.stylecracker.com/assets/images/social-media/twitter.png" alt="Twitter" style="width:24px;"></a>
      <a target="_blank" href="https://www.youtube.com/user/StyleCrackerTV" target="_blank"  title="Youtube"><img src="https://www.stylecracker.com/assets/images/social-media/youtube.png"  alt="Youtube" style="width:24px;"></a>
      <a href="https://instagram.com/stylecracker" target="_blank" title="Instagram"><img src="https://www.stylecracker.com/assets/images/social-media/instagram.png"  alt="Instagram" style="width:24px;"></a>
      <a target="_blank" href="https://www.pinterest.com/stylecracker" target="_blank"  title="Pintrest"><img src="https://www.stylecracker.com/assets/images/social-media/pintrest.png" alt="Pintrest" style="width:24px;"></a>

          </td>
        </tr>
      </table>


          <table width="600" cellpadding="0" cellspacing="0" align="center" border="0" style="width:600px; background:#fff; border:1px solid #cccccc; margin-bottom:20px;">
        <tr>
          <td align="left" style="padding:20px 0 0 20px; width:160px;">
              <img src="https://www.stylecracker.com/assets/images/logo.png" style="width:150px; margin-bottom:5px;">
          <div>
            <a href="https://www.stylecracker.com" style="color:#00b19c; text-decoration:none;">www.stylecracker.com</a>
          </div>
          </td>
            <td align="left"  style="padding:20px 0 0; vertical-align:bottom;">
              <img src="https://www.stylecracker.com/assets/images/icons/ic-download-app-1.png">
            <td>

            </td>
          <td align="right"  style="padding:20px 20px  0 0; text-align:center; ">
            <div style="font-size:18px; margin-bottom:10px;">
              StyleCracker App is India\'s only
personalised styling platform.
            </div>

             <a href="https://play.google.com/store/apps/details?id=com.stylecracker.android" target="_blank" style="color:#333; text-decoration:none;">
                  <img src="https://www.stylecracker.com/assets/images/icons/en_generic_rgb_wo_45.png" style="height:30px;" target="_blank"></a>

            <a href="https://itunes.apple.com/us/app/stylecracker/id1061463812?mt=8&uo=4" target="_blank" style="color:#333; text-decoration:none; ">
              <img src="https://www.stylecracker.com/assets/images/icons/apple_store.png" style="height:30px;" target="_blank"></a>

               
          </td>
        </tr>
      </table>

    </body>
    </html>';


      $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Processing');
      $this->email->to($this->getEmailAddress($user_id));    
      $this->email->cc('order@stylecracker.com');     
      $this->email->subject('StyleCracker: Order Processing');

       $banner_image = '';

      if($banner_image!='')
      {
         $email_banner ='<div> <img src="https://www.stylecracker.com/assets/images/email/'.$banner_image.'" ></div>';
      }else
      {
        $email_banner ='';
      }

      $this->email->message('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Order Summary</title>

</head>
<body style="background:#f3f3f3; color:#333333; text-align:left; font-family:Arial,Helvetica,sans-serif;
font-weight: normal;
line-height: 1.5;
font-size: 14px; padding:0; margin:0 auto;">
<table width="600" cellpadding="0" cellspacing="0" align="center" border="0" style="width:600px; background:#fff; margin-bottom:20px;">
  <tr>
          <td style="text-align:left; border-bottom:3px solid #00b19c; padding:20px;">

            <table cellpadding="0" cellspacing="0" align="center" border="0" style="width:100%; ">
              <tr>
                      <td style="text-align:left;">

            <img src="https://www.stylecracker.com/assets/images/logo.png" style="height:34px;">
  </td>
  <td style="text-align:right; vertical-align:middle; font-size:14px; color:#00b19c;">

  <a href="https://www.stylecracker.com/myOrders" style="color:#00b19c; text-decoration:none;">My Orders</a> 


          </td>
        </tr>
      </table>
      </td>
    </tr>

  <tr><td style="padding:30px;">

      <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
        <tr>
          <td align="center" style="padding:15px 0 30px; vertical-align:middle;">
             <span style="padding:8px 0; border-bottom:1px solid #cccccc; font-size:18px; text-transform:uppercase;">
               <img src="https://www.stylecracker.com/assets/images/icons/ic-order-processing-1.png" style="vertical-align:middle;"> Order Processing</span>
          </td>
        </tr>
        <tr>
          <td style="font-size:15px; line-height:1.7; ">
            <p style="margin-bottom:20px;">
            Hi <strong>'.$first_name.' '.$last_last.'</strong>,<br/>
            Thank you for your order! Your Order No <strong style="color:#00B19C">'.$order_id.'</strong> is being processed. </p>
          '.$email_banner.'
           <p style="margin-bottom:20px;">                                       
            Please find below, the summary of your order.
            </p>
            '.$pay_mode.'
            <div style="background:#f3f3f3; border:1px solid #cccccc; padding:20px 20px 15px; display:inline-block;">
                <div style="text-transform:uppercase; font-weight:bold; line-height:1;">
                  Shipping Address
                </div>
                <div>
                  '. $brand_price[0]['shipping_address'].'<br>
                    '.  $brand_price[0]['state_name'].'  '.  $brand_price[0]['country_name'].' ,'.  $brand_price[0]['city_name'].'  - '.  $brand_price[0]['pincode'].' 
                </div>

            </div>
          </td>
        </tr>
      </table>


      <br/>
      <br/>
      <div style="text-transform:uppercase; font-weight:bold; line-height:1; font-size:16px; margin-bottom:20px;">
        Order Details:
      </div>

      <table width="100%" cellpadding="8" cellspacing="0" align="center" border="0" style="font-size:12px;">
        <tbody>
          <tr>
            <th style="background-color:#f3f3f3; font-size:14px;">Image</th>
            <th style="background-color:#f3f3f3; font-size:14px;">Product Details</th>
            <th style="background-color:#f3f3f3; font-size:14px;">Qty</th>
            <th style="background-color:#f3f3f3; font-size:14px;">Size</th>
            <th style="background-color:#f3f3f3; font-size:14px;">Seller</th>
            <th style="background-color:#f3f3f3; font-size:14px; width:60px; text-align:right;">Price</th>
          </tr>'.$productInfohtml);


      $this->email->send();
    }
  }

    function orderPlaceMessagetoBrand($user_id,$orders_id,$first_name,$last_last){
    if($this->getEmailAddress($user_id)!=''){

      $totalProductPrice = 0; $coupon_discount_ = 0;
      $res1 = $this->Cart_model->su_get_order_info($orders_id); 
      foreach($res1 as $val11){
         $totalProductPrice = $totalProductPrice+$val11['product_price'];         
      }
	
      $order_id = explode('_', $orders_id);
      unset($order_id[0]);
      if(!empty($order_id)){
      foreach($order_id as $order_noval){
      $config['protocol'] = 'smtp';

//      $config['mailpath'] = '/usr/sbin/sendmail';
      $config['charset'] = 'iso-8859-1';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');

      $this->email->initialize($config);
      $res = $this->Cart_model->br_get_order_info($order_noval);
      $brand_price = $this->Cart_model->br_get_order_price_info($order_noval);
      $companyName = $res[0]['company_name'];
	// echo "<pre>";print_r($brand_price);exit;
if($brand_price[0]['state_id'] == '36') { $brand_price[0]['state_name'] = $brand_price[0]['it_state']; }
      /*Coupon Discount */
      $coupon_result = $this->calculateCouponAmount($res,$totalProductPrice);
      $coupon_discount_type= @$coupon_result['coupon_discount_type'];
      $coupon_discount= @$coupon_result['coupon_discount'];
      $coupon_code = strtoupper($coupon_result['coupon_code']);   
      $sc_coupon_discount = @$coupon_result['coupon_amount'];     
      /*Coupon Discount End */

      $productInfohtml = '';
      $i = 0;
      $productInfohtml = '';
      $coupDiscountPrdPrice = 0;
      $coupDiscountPrdPricetxt = '';
      $stylecrackercoupondisc = 0; // only for sc1000 and sc10
      $stylecrackercoupondisc_array = array();
      
                $i = 0;
             $productInfohtml = '';

                  if(!empty($res)){
                  foreach($res as $val){
                    $i++;

                    $productSku = $this->getProductSku($val['product_id'],$val['product_size']);
                    if($productSku!='')
                    {
                      $productSkuStr = '('.$productSku.')';
                    }else
                    {
                      $productSkuStr = '';
                    }

                    if($coupon_code!='')
                    {
                      if($coupon_discount_type==1)
                      {                        
                        $coupDiscountPrdPrice = $this->cart_model->calculate_product_discountAmt($val['product_price'],$totalProductPrice,$coupon_discount_type,$coupon_discount);
                        $coupDiscountPrdPricetxt = '( Discounted Amt: &#8377; '.round($coupDiscountPrdPrice).')';
                        $stylecrackercoupondisc = $coupDiscountPrdPrice;
                        $stylecrackercoupondisc_array[$val['brand_id']][] = ($val['product_price']-$stylecrackercoupondisc);
                        $coupon_discount_ = @round(array_sum($stylecrackercoupondisc_array[$val['brand_id']]));
                      }else if($coupon_discount_type==2)
                      {                                          
                        $coupDiscountPrdPrice = $this->cart_model->calculate_product_discountAmt($val['product_price'],$totalProductPrice,$coupon_discount_type,$sc_coupon_discount);
                        $coupDiscountPrdPricetxt = '<br/>( Discounted Amt: &#8377; '.round($coupDiscountPrdPrice).')';

                        $stylecrackercoupondisc_array[$val['brand_id']][] = ($val['product_price']-$coupDiscountPrdPrice);
                        $coupon_discount_ = @round(array_sum($stylecrackercoupondisc_array[$val['brand_id']]));

                      }
                    }

                   $productInfohtml =$productInfohtml.' <tr>
                      <td><img src="'. $this->config->item('sc_promotional_look_image').$val['image'].'" width="50" style="border:1px solid #cccccc; padding:2px; width:auto; height:auto; max-width:50px; max-height:50px;"></td>
                      <td>'. $val['name'].' '.$productSkuStr.' </td>
                      <td>'. $val['product_qty'].' </td>
                      <td>'. $val['size_text'].'</td>
                      <td>'. $val['company_name'].' </td>
                      <td style="text-align:right; width:60px;">&#8377; '. $val['product_price'].' '.$coupDiscountPrdPricetxt.'</td></tr>';

                  }
                }
                  $cod_amount = $brand_price[0]['cod_amount']>0 ? $brand_price[0]['cod_amount'] : 0 ;
                $shipping_amount = $brand_price[0]['shipping_amount']>0 ? $brand_price[0]['shipping_amount'] : 0 ;
                 
                 if($brand_price[0]['order_tax_amount'] > 0) {
                   $productInfohtml =$productInfohtml.' <tr>
                   <td colspan="5" style="padding:2px 8px; text-align:right;">Tax :</td>
                    <td style="padding:2px 8px; text-align:right;">
                      &#8377;  '.round($brand_price[0]['order_tax_amount']).'
                    </td>
                  </tr>';
                  }
                  if($cod_amount > 0) {
                  $productInfohtml =$productInfohtml.'<tr>

                    <td colspan="5" style="padding:2px 8px; text-align:right;">COD Charges:</td>
                    
                    <td style="padding:2px 8px; text-align:right;">
                      &#8377;  '.round($cod_amount).'
                    </td>
                  </tr>';
                }
                if($shipping_amount > 0){
                  $productInfohtml =$productInfohtml.'<tr>

                    <td colspan="5" style="padding:2px 8px; text-align:right;">Shipping Charges:
                    </td>
                    <td style="padding:2px 8px; text-align:right;">
                      &#8377;  '.round($shipping_amount).'
                    </td>
                  </tr>';
                }                
                if($coupon_code!='')
                {
                  $productInfohtml =$productInfohtml.'<tr>
                    <td colspan="5" style="padding:2px 8px; text-align:right;">Coupon Code '.$coupon_code.' Discount
                    </td>
                    <td style="padding:2px 8px; text-align:right;">
                      &#8377;  '.round($coupon_discount_).'
                    </td>
                  </tr>';
                }
               
                if($brand_price[0]['pay_mode'] == 1){
                $pay_mode = '<p style="margin-bottom:20px;">
                  Payment Mode : <img src="https://www.stylecracker.com/assets/images/icons/ic-cod-1.png" style="vertical-align:middle;"> Cash on Delivery
                </p>';
                }

                if($brand_price[0]['pay_mode'] == 2){
                $pay_mode = '<p style="margin-bottom:20px;">
                  Payment Mode : <img src="https://www.stylecracker.com/assets/images/icons/ic-online-payment-1.png" style="vertical-align:middle;"> Online Payment
                </p>';
                }
                /* if($brand_price[0]['order_total'] > 0) {
                  $productInfohtml =$productInfohtml.'<tr>
                    <td>
                      <strong>Order Total :<strong> </td>
                      <td>
                      &#8377;  '.$brand_price[0]['order_total'].'
                      </td>
                    </tr>
                  ';
              }*/
  if($coupon_discount_type==1 || $coupon_discount_type==2 )
    {
      $cprice = 0; $cprice = array_sum($stylecrackercoupondisc_array[$brand_price[0]['brand_id']]);
      $mailOrderTotal = round(($brand_price[0]['order_total']-$cprice));

    }else{
      $mailOrderTotal = round(($brand_price[0]['order_total']-$coupon_discount_));
    }
 $productInfohtml =$productInfohtml.'</tbody>
      </table>
      <div style="text-align:right; background:#f3f3f3; padding:5px 30px; font-size:18px; text-transform:uppercase; margin-top:20px; border-bottom:2px solid #cccccc;">
        Total Amount  : &#8377; '.$mailOrderTotal.'
      </div>
    </td>
  </tr>';
           $productInfohtml =$productInfohtml.' <tr>
    <td style="padding:0px 30px;">
        <div style=" border-bottom:1px solid #ccc; padding-bottom:30px;">
        Thank you for shopping with <strong>StyleCracker</strong>.<br/>
For any query or assistance, feel free to <a href="https://www.stylecracker.com/schome/aboutus" style="color:#00b19c; text-decoration:none;">Contact Us</a>
      </div>
    </td>
  </tr>
  <tr>
    <td style="vertical-align:middle; padding:20px 30px;">
      <span style="color: #888; font-weight: bold; line-height: 1.3; position: relative;top: -4px; vertical-align:top;">  Follow Us:  </span>

      <a target="_blank" href="https://www.facebook.com/StyleCracker" target="_blank"  title="Facebook"><img src="https://www.stylecracker.com/assets/images/social-media/facebook.png" alt="Facebook" style="width:24px;"></a>
      <a target="_blank" href="https://twitter.com/Style_Cracker" target="_blank"  title="Twitter"><img src="https://www.stylecracker.com/assets/images/social-media/twitter.png" alt="Twitter" style="width:24px;"></a>
      <a target="_blank" href="https://www.youtube.com/user/StyleCrackerTV" target="_blank"  title="Youtube"><img src="https://www.stylecracker.com/assets/images/social-media/youtube.png"  alt="Youtube" style="width:24px;"></a>
      <a href="https://instagram.com/stylecracker" target="_blank" title="Instagram"><img src="https://www.stylecracker.com/assets/images/social-media/instagram.png"  alt="Instagram" style="width:24px;"></a>
      <a target="_blank" href="https://www.pinterest.com/stylecracker" target="_blank"  title="Pintrest"><img src="https://www.stylecracker.com/assets/images/social-media/pintrest.png" alt="Pintrest" style="width:24px;"></a>

          </td>
        </tr>
      </table>


          <table width="600" cellpadding="0" cellspacing="0" align="center" border="0" style="width:600px; background:#fff; border:1px solid #cccccc; margin-bottom:20px;">
        <tr>
          <td align="left" style="padding:20px 0 0 20px; width:160px;">
              <img src="https://www.stylecracker.com/assets/images/logo.png" style="width:150px; margin-bottom:5px;">
          <div>
            <a href="https://www.stylecracker.com" style="color:#00b19c; text-decoration:none;">www.stylecracker.com</a>
          </div>
          </td>
            <td align="left"  style="padding:20px 0 0; vertical-align:bottom;">
              <img src="https://www.stylecracker.com/assets/images/icons/ic-download-app-1.png">
            <td>

            </td>
          <td align="right"  style="padding:20px 20px  0 0; text-align:center; ">
            <div style="font-size:18px; margin-bottom:10px;">
              StyleCracker App is India\'s only
personalised styling platform.
            </div>

             <a href="https://play.google.com/store/apps/details?id=com.stylecracker.android"  target="_blank" style="color:#333; text-decoration:none;">
                  <img src="https://www.stylecracker.com/assets/images/icons/en_generic_rgb_wo_45.png" style="height:30px;" target="_blank"></a>

            <a href="https://itunes.apple.com/us/app/stylecracker/id1061463812?mt=8&uo=4" target="_blank" style="color:#333; text-decoration:none; ">
              <img src="https://www.stylecracker.com/assets/images/icons/apple_store.png" style="height:30px;" target="_blank"></a>

               
          </td>
        </tr>
      </table>

    </body>
    </html>';
	
	    $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Processing');      
      $this->email->to($this->getEmailAddress($brand_price[0]['brand_id']));
      $this->email->cc('order@stylecracker.com');    
      $this->email->subject('StyleCracker: Order Processing');
/*
      $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Processing');
      //$this->email->to($this->getEmailAddress($user_id));
		$this->email->to('rajesh@stylecracker.com');
      $this->email->cc('rajeshkharatmol94@gmail.com');
      $this->email->subject('StyleCracker: Order Processing');
*/

       $this->email->message('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Order Summary</title>

</head>
<body style="background:#f3f3f3; color:#333333; text-align:left; font-family:Arial,Helvetica,sans-serif;
font-weight: normal;
line-height: 1.5;
font-size: 14px; padding:0; margin:0 auto;">
<table width="600" cellpadding="0" cellspacing="0" align="center" border="0" style="width:600px; background:#fff; margin-bottom:20px;">
  <tr>
          <td style="text-align:left; border-bottom:3px solid #00b19c; padding:20px;">

            <table cellpadding="0" cellspacing="0" align="center" border="0" style="width:100%; ">
              <tr>
                      <td style="text-align:left;">

            <img src="https://www.stylecracker.com/assets/images/logo.png" style="height:34px;">
  </td>
  <td style="text-align:right; vertical-align:middle; font-size:14px; color:#00b19c;">

  <a href="https://www.stylecracker.com/sc_admin" style="color:#00b19c; text-decoration:none;">My Orders</a> 


          </td>
        </tr>
      </table>
      </td>
    </tr>

  <tr><td style="padding:30px;">

      <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
        <tr>
          <td align="center" style="padding:15px 0 30px; vertical-align:middle;">
             <span style="padding:8px 0; border-bottom:1px solid #cccccc; font-size:18px; text-transform:uppercase;">
               <img src="https://www.stylecracker.com/assets/images/icons/ic-order-processing-1.png" style="vertical-align:middle;"> Order Processing</span>
          </td>
        </tr>
        <tr>
          <td style="font-size:15px; line-height:1.7; ">
            <p style="margin-bottom:20px;">
            Hi team '.$companyName.' <br/>
            Congrats!, We have recevied new order for you. Your Order No is <strong style="color:#00B19C">'.$order_noval.'</strong> , further details are as below. <br>
            Please log on to your dashboard at <a href="https://www.stylecracker.com/sc_admin" target="_blank" >https://www.stylecracker.com/sc_admin</a> and update the status accordingly.
            </p>
            '.$pay_mode.'
            <div style="background:#f3f3f3; border:1px solid #cccccc; padding:20px 20px 15px; display:inline-block;">
                <div style="text-transform:uppercase; font-weight:bold; line-height:1;">
                  Shipping Address
                </div>
                <div>
                '.$first_name.' '.$last_last.'<br>
                  '. $brand_price[0]['shipping_address'].'<br>
                    '.  $brand_price[0]['state_name'].' '.  $brand_price[0]['country_name'].','.  $brand_price[0]['city_name'].'  - '.  $brand_price[0]['pincode'].' <br>
                    Email:- '.$brand_price[0]['email_id'].' <br>
                    Mobile No:- '.$brand_price[0]['mobile_no'].'
                </div>

            </div>
          </td>
        </tr>
      </table>


      <br/>
      <br/>
      <div style="text-transform:uppercase; font-weight:bold; line-height:1; font-size:16px; margin-bottom:20px;">
        Order Details:
      </div>

      <table width="100%" cellpadding="8" cellspacing="0" align="center" border="0" style="font-size:12px;">
        <tbody>
          <tr>
            <th style="background-color:#f3f3f3; font-size:14px;">Image</th>
            <th style="background-color:#f3f3f3; font-size:14px;">Product Details</th>
            <th style="background-color:#f3f3f3; font-size:14px;">Qty</th>
            <th style="background-color:#f3f3f3; font-size:14px;">Size</th>
            <th style="background-color:#f3f3f3; font-size:14px;">Seller</th>
            <th style="background-color:#f3f3f3; font-size:14px; width:60px; text-align:right;">Price</th>
          </tr>'.$productInfohtml);

     $this->email->send();
        }
      }

    }
  }

  function getEmailAddress($user_id){
      $res = $this->Cart_model->get_user_info($user_id);
      if(!empty($res)){
        return $res[0]['email'];
      }else{ return ''; }
  }

  function get_order_info(){
    if (!$this->input->is_ajax_request()) {
        exit('No direct script access allowed');
    }else{
      $uniquecookie = $this->input->post('uniquecookie');
      $res = $this->Cart_model->get_order_info($uniquecookie);
      $brand_price = $this->Cart_model->get_order_price_info($uniquecookie);

      $coupon_result = $this->calculateCouponAmount($res,'');
      $coupon_discount= $coupon_result['coupon_discount'];
      $coupon_code = strtoupper($coupon_result['coupon_code']);
      if($coupon_code!='')
      {
        $coupon_text = '<div class="seller-subtotal"><span class="text-label">Coupon Code '.$coupon_code.' Discount:</span>
                      <span class="cart-price"><span class="fa fa-inr"></span> '.$coupon_discount.'</span></div>';
      }else
      {
        $coupon_text = '';
      }

      $orderhtml = '';

      $orderhtml = $orderhtml.'<div class="modal-dialog modal-md">
                  <div class="modal-content">
                    <div class="modal-header modal-header-success">
                      <button type="button" class="close" data-dismiss="modal">×</button>
                      <h4 class="modal-title">Order Successfull</h4>
                    </div>';

      $orderhtml = $orderhtml.'<div class="modal-body mycart-body">
                  <div class="order-summary-info">
                    Your order has been successfully placed. Please check your email for order detail.
                  </div> <div class="cart-items-wrp"> <ul class="cart-items">';
      if(!empty($res)){
        foreach($res as $val){
        $orderhtml = $orderhtml.'<li>
              <div class="row">
                <div class="col-md-2">
								  <div class="item-img">
                  <img src="'.$this->config->item('sc_promotional_look_image').$val['image'].'">
									</div>
                </div>
                <div class="col-md-10">
                  <div class="cart-item-inner">
                    <div class="row">
                      <div class="col-md-8">
                        <a class="cart-item-name" target="_blank" href="javascript:void(0);">'.$val['name'].'</a>
                        <div class="cart-item-meta">
                          <span class="size-wrp">
                            <span class="text-label">Size:</span> '.$val['size_text'].'
                          </span>
                          <span class="qty-wrp">
                            <span class="text-label">Qty:</span>
                            '.$val['product_qty'].'
                          </span>
                        </div>
                        <div>
                        </div>
                      </div>
                      <div class="col-md-4 cart-item-right">
                        <span class="cart-item-price cart-price">
                          <span class="fa fa-inr"></span> '.$val['product_price'].'
                        </span>

                      </div>
                      <div class="col-md-12">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="cart-item-seller">
                              <span class="text-label">Seller:</span> <a target="_blank" href="'.base_url().'brands/brand_details/'.$this->getBrandUsername($val['brand_id']).'"> '.$this->getBrandExtraInfo($val['brand_id'],'company_name').' </a></div>
                            </div>
                            <div class="col-md-6 cart-item-right">
                              <div class="delivery-status">

                              </div>
                            </div>                      
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>

                </div>
              </li>';
            }
      }
      $cod_amount = $brand_price[0]['cod_amount']>0 ? $brand_price[0]['cod_amount'] : 0 ;
      $shipping_amount = $brand_price[0]['cod_amount']>0 ? $brand_price[0]['cod_amount'] : 0 ;
      $orderhtml = $orderhtml.'<li class="seller-amount">
                            <div class="seller-tax"><span class="text-label">Tax :</span>
                              <span class="cart-price"><span class="fa fa-inr"></span> '.$brand_price[0]['order_tax_amount'].'</span></div>
                              <div class="seller-subtotal"><span class="text-label">COD Charges:</span>
                              <span class="cart-price"><span class="fa fa-inr"></span> '.$cod_amount.'</span></div>
                              <div class="seller-subtotal"><span class="text-label">Shipping Charges:</span>
                              <span class="cart-price"><span class="fa fa-inr"></span> '.$shipping_amount.'</span></div>
                              '.$coupon_text.'
                            </li>';


      $orderhtml = $orderhtml.'</ul></div>';
      $mailOrderTotal = round(($brand_price[0]['order_total']-$coupon_discount),2);
      $orderhtml = $orderhtml.'<div class="order-amount">
                               <div class="order-total">Order Total :
                               <span class="cart-price">
                               <span class="fa fa-inr"></span> '.$mailOrderTotal.'</span></div>
                        </div>';

      $orderhtml = $orderhtml.'</div></div></div>';
      echo $orderhtml;
    }
  }

  function getPinCode(){
    if (!$this->input->is_ajax_request()) {
        exit('No direct script access allowed');
    }else{
        $pincode = $this->input->post('pincode');
        $product_id = $this->input->post('id');

        $chkShipping = $this->Cart_model->chkShipping($product_id,$pincode);
        $chkCod = $this->Cart_model->chkCod($product_id,$pincode);


        if(!empty($chkShipping) && !empty($chkCod)){
          echo "<div class='error'>Shipping to pincode ".$pincode." is not-available</div>";
        }else if(!empty($chkShipping) && empty($chkCod)){
          echo "<div class='error'>Shipping to pincode ".$pincode." is not-available</div>";
        }else if(empty($chkShipping) && !empty($chkCod)){
          echo "<div class='error'>COD to pincode ".$pincode." is not-available</div>";
        }else{
          echo "<div class='success'>Shipping to pincode ".$pincode." is available</div>";
        }
    }
  }

  function successOrder(){
    //echo "<pre>"; print_r( $_POST );  exit;
    $data = array();
    if($_POST['status'] == 'failure'){
      redirect('OrderUnSuccessful/');
    }

    $data['uc_first_name'] = $this->input->post('firstname');
    $data['uc_last_name'] = $this->input->post('lastname');
    $data['uc_email_id'] = $this->input->post('email');
    $data['uc_mobile'] = $this->input->post('phone');
    $data['uc_address'] = $this->input->post('udf2');
    $data['uc_pincode'] = $this->input->post('udf3');
    $data['uc_city'] = $this->input->post('udf4');
    $data['uc_state'] = $this->input->post('udf5');
    $data['paymod'] = 'card';
    $data['SCUniqueID'] = $this->input->post('udf1');
    $data['orderval'] = $this->input->post('net_amount_debit');
    $data['coupon_code'] = $_SESSION['SCCouponCode'];
    $data['ucsecsue'] = 0;

    if($data['paymod'] == 'card' && $_POST['status'] == 'success'){
      $data['mihpayid'] = $this->input->post('mihpayid');
      $data['txnid'] = $this->input->post('txnid');
      $res = $this->Cart_model->sc_uc_order($data);
      $obj = json_decode($res);
      if($obj->order=='success'){
		// echo "comming in sucess";exit;
        $this->orderPlaceMessage($obj->user_info,$obj->msg,$data['uc_first_name'],$data['uc_last_name']);
        $this->orderPlaceMessagetoBrand($obj->user_info,$obj->msg,$data['uc_first_name'],$data['uc_last_name']);

        redirect('Sccart/ordersuccessOrder/'.$obj->msg);
      }else{

        redirect('OrderUnSuccessful/');
      }
    }
  }

    /*function successOrder_manually(){
    
    $data = array();
    

    $data['uc_first_name'] = 'Shweta';
    $data['uc_last_name'] = 'mehra';
    $data['uc_email_id'] = 'mehrashweta88@gmail.com';
    $data['uc_mobile'] = '9819482987';
    $data['uc_address'] = '18 B1 Great Eastern Royale, 333 Belasis Road, Tardeo Mumbai - 34';
    $data['uc_pincode'] = '400034';
    $data['uc_city'] = 'Mumbai';
    $data['uc_state'] = '21';
    $data['paymod'] = 'card';
    $data['SCUniqueID'] = '14551818737216308';
    $data['orderval'] = '16535.20';
    $data['ucsecsue'] = 0;
    
    if($data['paymod'] == 'card'){
      $data['mihpayid'] = '526453337';
      $data['txnid'] = '709606d88cd40c62906d';
      $res = $this->Cart_model->sc_uc_order($data);
      $obj = json_decode($res);
      if($obj->order=='success'){
        $this->orderPlaceMessage($obj->user_info,$obj->msg,$data['uc_first_name'],$data['uc_last_name']);
        $this->orderPlaceMessagetoBrand($obj->user_info,$obj->msg,$data['uc_first_name'],$data['uc_last_name']);

        redirect('Sccart/ordersuccessOrder/'.$obj->msg, 'refresh');
      }else{

        redirect('OrderUnSuccessful/', 'refresh');
      }
    }
  }*/ 

  function sc_check_otp(){
    $data['mobile_no'] = '91'.$this->input->post('mobile_no');
    $data['otp_number'] = $this->input->post('otp_number');
    echo $this->Cart_model->sc_check_otp($data['mobile_no'],$data['otp_number']);

  }

  function scgenrate_otp(){
	$uc_first_name = $this->input->post('uc_first_name');
	$uc_last_name = $this->input->post('uc_last_name');
	$email_id = $this->input->post('uc_email_id');
	$uc_address = $this->input->post('uc_address');
	$SCUniqueID = $this->input->post('SCUniqueID');
	$uc_pincode = $this->input->post('uc_pincode');
	$uc_city = $this->input->post('uc_city');
	$existing_shipping_add =$this->input->post('existing_shipping_add');
	$paymod_cod = '1';  
    $data['mobile_no'] = '91'.$this->input->post('mobile_no');
    $otp_no = rand(5,100000);
   /* $otp_no = 99999;*/
	if($existing_shipping_add != 0 && $existing_shipping_add > 0){
		$existingAddress = array();
		$existingAddress = $this->Cart_model->existingAddress($existing_shipping_add); 
		$uc_pincode = $existingAddress[0]['pincode'];
		$uc_city = $existingAddress[0]['city_name'];
	}
    $this->Cart_model->scgenrate_otp($data['mobile_no'],$otp_no);
	$result = $this->Cart_model->failed_order($uc_first_name,$uc_last_name,$email_id,$uc_address,$SCUniqueID,$data['mobile_no'],$paymod_cod,$uc_pincode,$uc_city);
    $message = 'Thank+you+for+choosing+STYCKR.+Your+OTP+for+cash+on+delivery+is+'.$otp_no.'.+Please+use+this+to+confirm+your+order.+Stay+Stylish!';
    file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile='.$data['mobile_no'].'&message='.$message);
    /* Call SMS send API */

  }

  function ordersuccessOrder(){
    $order_no = $this->uri->segment(3);
    if($order_no!=''){
      $totalProductPrice =0;
      $data['res'] = $this->Cart_model->su_get_order_info($order_no);
      $data['brand_price'] = $this->Cart_model->su_get_order_price_info($order_no);
	 
      foreach($data['res'] as $val){
         $totalProductPrice = $totalProductPrice+$val['product_price'];         
      }

      $coupon_result = $this->calculateCouponAmount($data['res'],$totalProductPrice);
      /*echo '<pre>';print_r($coupon_result);*/
      $data['coupon_discount']= $coupon_result['coupon_discount'];
      $data['coupon_code'] = $coupon_result['coupon_code'];
      
      $this->load->view('common/header_view');
      $this->load->view('cart/orderSuccesfull',$data);
      $this->load->view('common/footer_view');
    }else{
      redirect('/');
    }
  }

  function existingAddress(){
     $address_id = $this->input->post('address');
     if($address_id!=''){
        $res = $this->Cart_model->existingAddress($address_id);
     }
     if(!empty($res)){ echo json_encode($res); }else{
      echo '';
     }
  }

  function notify_me(){
    $notify_me_id = base64_decode($this->input->post('notify_me_id'));
    $notify_emailid = $this->input->post('notify_emailid');
    $SCUniqueID = $this->input->post('SCUniqueID');

    if($notify_me_id!='' && $notify_emailid!=''){
        if (filter_var($notify_emailid, FILTER_VALIDATE_EMAIL) === false) {
          echo '{"message":"1","text":"Please enter valid email-id","prod_id":'.$notify_me_id.'}';
        }else{
          $res = $this->Cart_model->notify_me($notify_me_id,$notify_emailid);
          if($this->session->userdata('user_id')) $user_id = $this->session->userdata('user_id'); else $user_id = 0;
          $this->Analytics_model->add_product_click($user_id,$notify_me_id,3,$SCUniqueID);
          echo '{"message":"0","text":"We\'ll inform you once the product is back in stock!","prod_id":'.$notify_me_id.'}';
        }
    }
  }

  function getProductSize($product_id,$selected_id){
    $res = $this->Cart_model->get_size($product_id);
    return $res;
  }

  public function productSizeUpdate(){
    if (!$this->input->is_ajax_request()) {
        exit('No direct script access allowed');
    }else{
      $uniquecookie = $this->input->post('uniquecookie');
      $productSize = $this->input->post('productSize');
      $productInfo = base64_decode(str_replace("product_cart_info_","",$this->input->post('productInfo')));
      
        $this->Cart_model->productSizeUpdate($uniquecookie,$productSize,$productInfo);
      
      echo 'done';
    }
  }

  function applyCoupon()
  {  
    if (!$this->input->is_ajax_request())
    {
        exit('No direct script access allowed');
    }else
    {
      $couponData = trim($this->input->post('couponData'));
      $cartItem = $this->input->post('cartItem');
      $cartItem2 = $this->input->post('cartItem2');
      $coupon_total = $this->input->post('coupon_total');
      
      $cartItemdecode = base64_decode($cartItem);     
      $cartItemarray = explode(',', $cartItemdecode);    
      
      $cartItemdecode2 = base64_decode($cartItem2);     
      $cartItemarray2 = explode(',', $cartItemdecode2); 
      $couponTotaldecode = base64_decode($coupon_total); 

      $result = $this->Cart_model->applyCoupon($couponData,$cartItemarray,$cartItemarray2);
     
      if(!empty($result))
      {
        //echo 'Coupon Applied';
        if($result=='NotLoggedIn')
        {
          echo 'Log In To Apply Coupon';
        }else
        {
          /*echo 'Coupon Applied';*/
          
          if($result['min_spend'][0]>$couponTotaldecode || $couponTotaldecode>$result['max_spend'][0])
          {
            echo 'Coupon <b>'.$couponData.'</b> Applicable for Amount > '.$result['min_spend'][0];
          }else
          {
            if(!empty($result))
            { 
              echo 'Coupon Applied';
            }else
            {
              if($this->Cart_model->showEmailPopUp($couponData))  
              {   
                echo 'Enter Email';   
              }else   
              {   
                echo 'No Coupon Found';   
              }
            }
          }
        }
      }else
      {
        if($this->Cart_model->showEmailPopUp($couponData))   
        {   
          echo 'Enter Email';   
        }else   
        {   
          echo 'No Coupon Found';   
        }
      }
    }
  }

  /*function calculateCouponAmount($cartproduct=array())
  {    
      if(!empty($cartproduct))
      {
        
        foreach($cartproduct as $val)
        {

          if(@$val['discount_percent']!='' || @$val['discount_percent']!=0)
          {
             $price = $val['discount_price'];            
          }else
          {
             $price = $val['product_price'];            
          }

          if(isset($val['coupon_code']) && $val['coupon_code']!='')
          {
            $coupon_info = $this->Cart_model->getUserCoupon($val['coupon_code'],$val['brand_id']);
            /*$data['coupon_discount'] = $coupon_info['coupon_amount'];
            $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];*/
        /*    $coupon_brand = $coupon_info['brand_id'];
            $coupon_products = $coupon_info['coupon_products'];
            if($coupon_products!='' || $coupon_products!=0)
            {
              $coupon_products_arr =explode(',',$coupon_products);
            }
            $coupon_min_spend = $coupon_info['coupon_min_spend'];
            $coupon_max_spend = $coupon_info['coupon_max_spend'];
            $coupon_discount_type = $coupon_info['coupon_discount_type']; 
			$coupon_stylecracker =  $coupon_info['stylecrackerDiscount'];
            $individual_use_only =  $coupon_info['individual_use_only']; 
            $data['coupon_code'] = $val['coupon_code'];
            $coupon_product_price = 0;

              if($coupon_brand==$val['brand_id'] && $coupon_brand!=0)
              {        
                if($coupon_products!=''&& $coupon_products!=0)
                {
                   //coupon_discount_type =3 (Product discount)
                  if($coupon_discount_type==3)
                  {
                    if(in_array($val['product_id'], $coupon_products_arr))
                    {              
                      if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                      {                        
                        $data['coupon_discount'] = $coupon_info['coupon_amount'];  
                        $data['coupon_code'] = $val['coupon_code'];                   
                      } 
                    }
                  } 
                   //coupon_discount_type =4 (Product % discount)
                  if($coupon_discount_type==4)
                  {
                    if(in_array($val['product_id'], $coupon_products_arr))
                    {              
                      if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                      {
                        $coupon_percent = $coupon_info['coupon_amount'];
                        $data['coupon_discount'] = $price*($coupon_percent/100); 
                        $data['coupon_code'] = $val['coupon_code'];                      
                      } 
                    }
                  }                 

                }else
                { 
                   //coupon_discount_type =1 (cart discount)
                  if($coupon_discount_type==1)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $data['coupon_discount'] = $coupon_info['coupon_amount']; 
                      $data['coupon_code'] = $val['coupon_code'];                   
                    }
                  }
                   //coupon_discount_type =2 (cart % discount)
                  if($coupon_discount_type==2)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $coupon_percent = $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];                      
                    }
                  }
                }          
              }

             /*  if($coupon_brand==0 && strtolower($val['coupon_code'])=='sccart20')
              { 
                   $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $coupon_percent = $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];                      
                    }
              }*/
				
		/*	 if(($coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only!=1  )|| ( $coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only==1 && $this->session->userdata('user_id')>0 ))        
              { 
                     //coupon_discount_type =1 (cart discount)
                  if($coupon_discount_type==1)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {					  
                      $data['coupon_discount'] = $coupon_info['coupon_amount']; 
                      $data['coupon_code'] = $val['coupon_code'];                   
                    }
                  }
                   //coupon_discount_type =2 (cart % discount)
                  if($coupon_discount_type==2)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $coupon_percent = $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];                      
                    }
                  }
              }
              

          }else
          {
            $data['coupon_discount']=0;
            $data['coupon_code'] = '';
          }         
        }      
      } 
      return $data;      
  }*/

   function calculateCouponAmount($cartproduct=array(),$totalProductPrice=null)
  {    
      if(!empty($cartproduct))
      {
        $total_product_price = 0; $coupon_product_price = 0;
        foreach($cartproduct as $val)
        {
            if(@$val['discount_percent']!='' || @$val['discount_percent']!=0)
            {
               $price = $val['discount_price'];            
            }else
            {
               $price = $val['product_price'];            
            }
            $total_product_price = $total_product_price+$price;
        }
        foreach($cartproduct as $val)
        {

          if(@$val['discount_percent']!='' || @$val['discount_percent']!=0)
          {
             $price = $val['discount_price'];            
          }else
          {
             $price = $val['product_price'];            
          }

           $coupon_info = $this->Cart_model->getUserCoupon($val['coupon_code'],$val['brand_id']);
            $coupon_brand = $coupon_info['brand_id'];
            $coupon_products = $coupon_info['coupon_products'];
            if($coupon_products!='' || $coupon_products!=0)
            {
              $coupon_products_arr =explode(',',$coupon_products);
            }
            $coupon_min_spend = $coupon_info['coupon_min_spend'];
            $coupon_max_spend = $coupon_info['coupon_max_spend'];
            $coupon_discount_type = $coupon_info['coupon_discount_type']; 
            $coupon_stylecracker =  $coupon_info['stylecrackerDiscount'];
            $individual_use_only =  $coupon_info['individual_use_only']; 

          if(isset($val['coupon_code']) && $val['coupon_code']!='')
          {
           
            /*$data['coupon_discount'] = $coupon_info['coupon_amount'];
            $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];*/
          
            $data['coupon_code'] = $val['coupon_code'];
            $coupon_product_price = 0;

              if($coupon_brand==$val['brand_id'] && $coupon_brand!=0)
              {        
                if($coupon_products!=''&& $coupon_products!=0)
                {
                   //coupon_discount_type =3 (Product discount)
                  if($coupon_discount_type==3)
                  {
                    if(in_array($val['product_id'], $coupon_products_arr))
                    {              
                      if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                      {
                        $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                        $data['coupon_amount'] =  $coupon_info['coupon_amount'];                      
                        $data['coupon_discount'] = $coupon_info['coupon_amount'];  
                        $data['coupon_code'] = $val['coupon_code'];                   
                      } 
                    }
                  } 
                   //coupon_discount_type =4 (Product % discount)
                  if($coupon_discount_type==4)
                  {
                    if(in_array($val['product_id'], $coupon_products_arr))
                    {              
                      if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                      {
                        $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                        $data['coupon_amount'] =  $coupon_info['coupon_amount'];  
                        $coupon_percent = $coupon_info['coupon_amount'];
                        $data['coupon_discount'] = $price*($coupon_percent/100); 
                        $data['coupon_code'] = $val['coupon_code'];                      
                      } 
                    }
                  }                 

                }else
                { 
                   //coupon_discount_type =1 (cart discount)
                  if($coupon_discount_type==1)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_info['coupon_amount']; 
                      $data['coupon_code'] = $val['coupon_code'];                   
                    }
                  }
                   //coupon_discount_type =2 (cart % discount)
                  if($coupon_discount_type==2)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];  
                      $coupon_percent = $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];                      
                    }
                  }
                }          
              }

             /*  if($coupon_brand==0 && strtolower($val['coupon_code'])=='sccart20')
              { 
                   $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $coupon_percent = $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];                      
                    }
              }*/
        
       if(($coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only!=1  )|| ( $coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only==1 && $this->session->userdata('user_id')>0 ))        
              { 
                $coupon_product_price = $totalProductPrice;

                     //coupon_discount_type =1 (cart discount)
                  if($coupon_discount_type==1)
                  {
                    
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    { 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];            
                      $data['coupon_discount'] = $coupon_info['coupon_amount']; 
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                      $data['coupon_code'] = $val['coupon_code'];                   
                    }
                  }
                   //coupon_discount_type =2 (cart % discount)
                  if($coupon_discount_type==2)
                  {                   
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    { 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];                       
                      $coupon_percent = $coupon_info['coupon_amount']; 
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];  
                      $data['coupon_discount'] = $totalProductPrice*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];                      
                    }
                  }
              }
              

          }else
          {
            $data['coupon_discount']=0;
            $data['coupon_code'] = '';
          }         
        }      
      } 
      return $data;      
  }

  function getProductSku($productId,$productSize)
  {
    $product_sku = $this->Cart_model->getProductSku($productId,$productSize);
    return $product_sku;
  }
  
  function sc_coupon_campaign()
  {
      $data = array();
      if (!$this->input->is_ajax_request()) {
        exit('No direct script access allowed');
      }else{
        $couponEmail = $this->input->post('couponEmail');
        $couponCode = $this->input->post('couponCode');
        $ip = $this->input->ip_address();

        $data = array( 'coupon_code' => $couponCode,
                        'email_id' =>$couponEmail,
                        'ip_address' =>$ip );

        $result = $this->Cart_model->sc_coupon_campaign($data);
       
        if($result)
        {
          echo '1';
        }else
        {
          echo '0';
        }
      }
  }

}
