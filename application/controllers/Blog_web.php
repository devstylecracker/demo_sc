<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog_web extends MY_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('Blog_model');
		$this->load->model('Favorites_model');
		$this->load->model('Pa_model');
	}

	public function index(){
		
		$user_data['user_id'] = $this->session->userdata('user_id');
		$user_data['limit_cond'] = " LIMIT 0,12";
		$data['top_blogs'] = $this->Blog_model->get_blog_data($user_data);
		//$data['pa'] = $this->Blog_model->get_user_pa();
		// $user_data['limit_cond'] = " ";
		// $data['top_blogs'] = count($this->Blog_model->get_blog_data($user_data));
		$this->seo_title = 'Fashion, style, beauty, health, travel & lifestyle  tips & trends';
		$this->seo_desc = '';
		$this->load->view('seventeen/common/header_view');
		$this->load->view('seventeen/allblog_view',$data);
		$this->load->view('seventeen/common/footer_view');
	}
	
	function get_more_blogs(){
		$user_id = 0;
		$user_data['user_id'] = $this->session->userdata('user_id');
		$user_data['offset'] = $this->input->post('offset')*12;
		$user_data['limit_cond'] = " LIMIT ".$user_data['offset'].",12 ";
		$object_data['top_blogs'] = $this->Blog_model->get_blog_data($user_data);
		if($user_data['user_id'] < 0){ $user_id=0; }else{ $user_id = $user_data['user_id']; }
		$object_data['user_id'] = $user_id;
		$this->load->view('seventeen/more_blog',$object_data);
		/*$blog_html = '';
				if(!empty($top_blogs)){ foreach($top_blogs as $val){
				if(@$val["is_wishlisted"] == "1") $active = "active"; else $active = "";
				$blog_html = $blog_html.'<div class="col-md-4 col-sm-6 col-xs-12 grid-col grid-col">
				  <div class="item-wrp">
					<div class="item">
					<a href="'.$val["blog_url"].'">
					  <div class="item-img-wrp">
						<img src="'.$val["image"].'" alt="">
					  </div>
					   </a>
					  <div class="item-desc-wrp">
						<div class="title"><a href="'.$val["blog_url"].'">'.$val["post_title"].' </a></div>
						<div class="desc">'.$val["post_desc"].'...</div>
						<div class="category">'.$val['post_type'].'</div>
					  </div>
						<div class="item-btns-wrp">
						  <a href="'.$val["blog_url"].'" class="scbtn scbtn-primary"><span>Read More</span></a>
						</div>
						<span class="ic-heart '.$active.' " id="wishlist_blog'.$val["id"].'" onclick="add_to_fav(\'blog\','.$val["id"].','.$user_id.');">
					  <i class="icon-heart"></i>
					</span>
					
					</div>
				  </div>
				</div>';
				}//foreach
			}//if
		echo $blog_html;*/
	}
	
}