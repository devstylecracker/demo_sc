<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
class Mobile_pop_up extends TT_REST_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Schome_model');
		$this->load->model('Mobile_model_android');
		$this->open_methods = array('single_look_post');    
		$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
	}
	
	public function pop_up_get(){
		$id = $this->get('id');
		$app_data['app_platform'] = $this->get('app_platform');
		$app_data['app_version'] = $this->get('app_version');
		$app_data['platform_version'] = $this->get('platform_version');
		$app_data['model_name'] = $this->get('model_name');
		$app_data['latitude'] = $this->get('latitude');
		$app_data['longitude'] = $this->get('longitude');
		
		if($app_data['app_platform'] == 'android'){
			$data['icon_link'] = 'https://www.stylecracker.com/assets/images/app_icons/popup_android.png';
			$popup[0]['pop_up_name'] = 'Travel Shop';
			$popup[1]['pop_up_name'] = 'Summer Store';
		}else{ 
			$data['icon_link'] = 'https://www.stylecracker.com/assets/images/app_icons/popup_ios.png';
			$popup[0]['pop_up_name'] = ' ';
			$popup[1]['pop_up_name'] = ' ';
		}
		
		$app_data = serialize($app_data);
		$result = $this->Mobile_model_android->save_appdata($id,$app_data);
		$id = base64_encode($id);
		$data['show_on_app'] = '1';
		$data['popup_title'] = 'POP-UP';
		
		$popup[0]['image_link'] = 'https://www.stylecracker.com/assets/images/promo/travel_shop_app.jpg';
		$popup[0]['pop_up_url'] = 'https://www.stylecracker.com/travel-shop?id='.$id;
		$popup[1]['image_link'] = 'https://www.stylecracker.com/assets/images/promo/summer_ready_app.jpg';
		$popup[1]['pop_up_url'] = 'https://www.stylecracker.com/summer-pop-up?id='.$id;
		
		$data['pop_ups' ] = $popup;
		$this->success_response($data);
	}
	
}