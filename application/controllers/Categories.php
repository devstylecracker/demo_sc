<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Categories extends MY_Controller {


	function __construct(){

       parent::__construct();
       $this->load->model('category_model');
       $this->load->model('home_model');
       $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));

   	}

	public function index()
	{
		$data = array();
		$look_type=$this->uri->segment(2);
		$category_slug=$this->uri->segment(3);
		$category_level=$this->uri->segment(4);
    $offset=$this->uri->segment(5);
    if($offset <=1 ){ $offset = 1; }
		//$look_type=$this->uri->segment();
		$data['page_filter_name'] =$look_type;
		$data['category_slug'] =$category_slug;
		$data['category_level'] =$category_level;
    $data['nav_cat_name'] = $this->home_model->get_category_name($category_level,$category_slug); 
		$data['paid_brands'] = $this->home_model->get_paid_brands($look_type);
		$data['color'] = unserialize(FILTER_COLOR);
		$data['filter_data'] = $this->home_model->get_filters($look_type);
		$data['coupon_offer'] = $this->home_model->coupon_offer();
		$data['left_filter'] = $this->load->view('common/left_product_filter', $data, true);
		$data['looks_data'] = $this->category_model->get_looks_via_cat($look_type,$category_slug,$category_level);
		$data['products_data'] = $this->category_model->get_products_via_cat($offset,$look_type,$category_slug,$category_level);
		$data['products_total'] = $this->category_model->get_products_via_cat_total($look_type,$category_slug,$category_level);

    $pagination = '';
    $adjacents = 3;
    $prev = 0;
    $next = 1;  
    $perpage = 12;
    $lastpage = ceil($data['products_total']/$perpage);
    $lpm1 = $lastpage - 1;
    if($lastpage > 1)
        { 
          $pagination .= "<div class='sc-pagination-wrp'><ul class=\"pagination sc-pagination\">";
          //previous button

          if ($offset != 1) 
            $pagination.= "<li><a href=".base_url()."categories/".$data['page_filter_name']."/".$data['category_slug']."/".$data['category_level']."/".'1'.">First</a></li>";

          if ($offset > 1) 
            $pagination.= "<li><a href=".base_url()."categories/".$data['page_filter_name']."/".$data['category_slug']."/".$data['category_level']."/".$prev.">previous</a></li>";
          
          
          $limit = $offset + 5;
          if($offset != $lastpage && $offset< $lastpage-5  ){
          for ($counter =  $offset; $counter <= $limit; $counter++)
            {
              if ($counter == $offset)
                $pagination.= "<li><span class=\"current\">$counter</span></li>";
              else
                $pagination.= "<li><a href=".base_url()."categories/".$data['page_filter_name']."/".$data['category_slug']."/".$data['category_level']."/".$counter.">$counter</a></li>";          
            }
          }else{ 
            for ($counter = $lastpage-5; $counter <= $lastpage; $counter++)
            {
              if ($counter == $offset)
                $pagination.= "<li><span class=\"current\">$counter</span></li>";
              else
                $pagination.= "<li><a href=".base_url()."categories/".$data['page_filter_name']."/".$data['category_slug']."/".$data['category_level']."/".$counter.">$counter</a></li>";          
            }

          }
          
          //next button
          if ($offset < $counter - 1) 
            $pagination.= "<li><a href=".base_url()."categories/".$data['page_filter_name']."/".$data['category_slug']."/".$data['category_level']."/".$next.">next </a></li>";


          if ($offset != $lastpage) 
            $pagination.= "<li><a href=".base_url()."categories/".$data['page_filter_name']."/".$data['category_slug']."/".$data['category_level']."/".$lastpage.">Last </a></li>";

          $pagination.= "</ul></div>\n";    
      }

    $data['pagination'] =  $pagination;
		$data['offset'] = $offset;
		
		$this->load->view('common/header_view');
		$this->load->view('categories',$data);
		$this->load->view('common/footer_view');
	}

	function get_more_products(){

	  $type = $this->input->post('type');
   	  $id = $this->input->post('id');
   	  $offset = $this->input->post('offset');
   	  $look_type=$this->input->post('look_type');
	  $category_slug=$this->input->post('category_slug');
	  $category_level=$this->input->post('category_level');
	  $type = $this->input->post('type');
   	  $id = $this->input->post('id');
   	  $price_filter = $this->input->post('price_filter');
   	  $min_price = $this->input->post('min_price');
   	  $max_price = $this->input->post('max_price');
   	  $color = $this->input->post('color');
   	  $discount = $this->input->post('discount');
   	  $sc_offers = $this->input->post('sc_offers');
      
      //$data['products_data'] = $this->category_model->get_products_via_cat($offset,$look_type,$category_slug,$category_level);
       $data['products_data'] = $this->category_model->filter_products($offset,$look_type,$category_slug,$category_level,$type,$id,$price_filter,$min_price,$max_price,$color,$discount,$sc_offers);
      $this->load->view('more_products',$data);

   	}

   	function filter_products(){
   		$look_type = $this->input->post('look_type');
   		$category_slug = $this->input->post('category_slug');
   		$category_level = $this->input->post('category_level');
   		$type = $this->input->post('type');
   		$id = $this->input->post('id');
   		$price_filter = $this->input->post('price_filter');
   		$offset = 1;
   		$min_price = $this->input->post('min_price');
   		$max_price = $this->input->post('max_price');
   		$color = $this->input->post('color');
   		$discount = $this->input->post('discount');
   		$sc_offers = $this->input->post('sc_offers');

   		$data['page_filter_name'] =$look_type;
		  $data['category_slug'] =$category_slug;
		  $data['category_level'] =$category_level;
		  $data['id'] =$id;
		  $data['type'] =$type;
		  $data['color_data'] =$color;
   		$data['total_products'] = $this->category_model->filter_products_total($offset,$look_type,$category_slug,$category_level,$type,$id,$price_filter,$min_price,$max_price,$color,$discount,$sc_offers);
   		$data['products_data'] = $this->category_model->filter_products($offset,$look_type,$category_slug,$category_level,$type,$id,$price_filter,$min_price,$max_price,$color,$discount,$sc_offers);
   		$this->load->view('filter_products',$data);
   	}

    /*function category_list(){
      $data = array();

      $filter_type = strtolower(trim($this->uri->segment(2)));
      if($filter_type == '' || $filter_type == 'all'){ $filter_selection = 'all'; }
      else if($filter_type == 'men'){ $filter_selection = 'men'; }
      else if($filter_type == 'women'){ $filter_selection = 'women'; }
      $data['page_filter_name'] = $filter_type;

       if ( $this->cache->file->get('category_filter_data_'.$filter_selection)  == FALSE ) {
          $data['top_categories'] = $this->home_model->category_list($filter_type);
          $this->cache->file->save('category_filter_data_'.$filter_selection, $data, 500000000000000000);
          
       }else{
          //echo 'cache';
          $data = $this->cache->file->get('category_filter_data_'.$filter_selection);
       }

      $this->load->view('common/header_view');
      $this->load->view('category_list',$data);
      $this->load->view('common/footer_view');
    }*/

    function category_list(){
      $data = array();

      $filter_type = strtolower(trim($this->uri->segment(2)));
      if($filter_type == '' || $filter_type == 'all'){ $filter_selection = 'all'; }
      else if($filter_type == 'men'){ $filter_selection = 'men'; }
      else if($filter_type == 'women'){ $filter_selection = 'women'; }
      $data['page_filter_name'] = $filter_type;

      
      if ($this->cache->file->get('category_filter_data_'.$filter_selection)  != FALSE ) {
          $data['top_categories'] = $this->home_model->category_list($filter_selection);
          $this->cache->file->save('category_filter_data_'.$filter_selection, $data, 500000000000000000);
          
       }else{
          //echo 'cache';
          $data = $this->cache->file->get('category_filter_data_'.$filter_selection);
       }

       $data['top_categories'] = $this->home_model->category_list($filter_selection);
      $this->load->view('common/header_view');
      $this->load->view('category_list',$data);
      $this->load->view('common/footer_view');
    }

    function top_brand_list(){
     $data = array();
      $data['top_brands'] = $this->home_model->brand_list();
      
      $this->load->view('common/header_view');
      $this->load->view('brand_list',$data);
      $this->load->view('common/footer_view');
    }
}
