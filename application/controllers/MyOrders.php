<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MyOrders extends MY_Controller {

	function __construct(){

       parent::__construct();
       $this->load->model('Cart_model');
       $this->load->model('cartnew_model');
   	}

	public function index()
	{		
    if($this->session->userdata('user_id')){
      
      $data['myOrders'] = $this->Cart_model->getMyOrders();
      $data['myProductOrders'] = $this->Cart_model->getMyProductsOrders();

      $this->load->view('common/header_view');
      $this->load->view('cart/myOrders',$data);
      $this->load->view('common/footer_view');
    }
	}

}
