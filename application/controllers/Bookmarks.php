<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// require APPPATH.'libraries/IntercomClient.php';
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
class Bookmarks extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Schome_model');
		$this->load->model('home_model');
		$this->load->model('Pa_model');
		$this->load->model('Product_desc_model');
		$this->load->library('bitly');
		$this->load->driver('cache');
		$this->load->library('user_agent');
		$this->load->library('curl');
		$this->load->model('Collection_model');
		$this->load->model('Event_model');
		$this->load->model('Mobile_look_model');
		$this->load->model('Favorites_model');
		$this->load->model('User_info_model');
		$this->load->model('Blog_model');

	}


	public function index(){
	  
	  $object_data = array();
		
		$data['user_id'] = $this->session->userdata('user_id');
		$data['body_shape'] = '';
		$data['pa_pref1'] = '';
		$data['bucket'] = '';
		$data['gender'] = '';
		$data['limit'] = 4;
		$data['offset'] = 0;
		$data['limit_cond'] = " LIMIT 0,4";
		$data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
		
		$fav_for = 'product';
		$object_ids = $this->User_info_model->get_wishlisted_ids($data,$fav_for);
		// product data
		if($fav_for == 'product' && !empty($object_ids)){
			$data['limit'] = 4;
			$data['offset'] = 0;
			$data['limit_cond'] = " LIMIT 0,4";
			$data['search_by'] = '1';$data['table_search'] = explode(",",$object_ids);
			$object_data['products'] = $this->Mobile_look_model->get_products_list($object_ids,$data);
			$object_data['total_products'] = count($data['table_search']);
			
		}else {
			$object_data['products'] = array();
			$object_data['product_message1'] = "You haven't bookmarked any products";
			$object_data['product_message2'] = "Tap on a products heart icon to add it to your bookmarks";
		}
		
		$fav_for = 'collection';
		$object_ids = $this->User_info_model->get_wishlisted_ids($data,$fav_for);
		
		// collection data
		if($fav_for == 'collection' && !empty($object_ids)){
			$data['limit'] = 2;
			$data['offset'] = 0;
			$data['limit_cond'] = " LIMIT 0,2";
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['collection_data'] = $this->Collection_model->get_all_collection($data);
			$object_data['total_collections'] = count(explode(",",$object_ids));
			
		}else{
			$data['search_by'] = '0';
			$object_data['collection_data'] = array();
			$object_data['collection_message1'] = "You haven't bookmarked any collections";
			$object_data['collection_message2'] = "Tap on a collections heart icon to add it to your bookmarks";
		}
		
		$fav_for = 'event';
		$object_ids = $this->User_info_model->get_wishlisted_ids($data,$fav_for);
		// event data
		if($fav_for == 'event' && !empty($object_ids)){
			$data['limit'] = 2;
			$data['offset'] = 0;
			$data['limit_cond'] = " LIMIT 0,2";
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['event_data'] = $this->Event_model->get_all_event($data);
			$object_data['total_events'] = count(explode(",",$object_ids));
			
		}else{
			$data['search_by'] = '0';
			$object_data['event_data'] = array();
			$object_data['event_message1'] = "You haven't bookmarked any events";
			$object_data['event_message2'] = "Tap on a events heart icon to add it to your bookmarks";
			
		}
		
		$fav_for = 'blog';
		$object_ids = $this->User_info_model->get_wishlisted_ids($data,$fav_for);
		// blog data
		if($fav_for == 'blog' && !empty($object_ids)){
			$data['limit'] = 3;
			$data['offset'] = 0;
			$data['limit_cond'] = " LIMIT 0,3";
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['blog_data'] = $this->Blog_model->get_blog_data($data);
			$object_data['total_blogs'] = count(explode(",",$object_ids));
			
		}else{
			$data['search_by'] = '0';
			$object_data['blog_data'] = array();
			$object_data['blog_message1'] = "You haven't bookmarked any blogs";
			$object_data['blog_message2'] = "Tap on a blogs heart icon to add it to your bookmarks";
			
		}
		$object_data['user_id'] = $this->session->userdata('user_id');
		// echo "<pre>";print_r($object_data);exit;
		$this->load->view('seventeen/common/header_view');
		$this->load->view('seventeen/user_wishlist',$object_data);
		$this->load->view('seventeen/common/footer_view');
	}
	
	function view_all_bookmark(){
		$data['user_id'] = $this->session->userdata('user_id');
		$data['body_shape'] = '';
		$data['pa_pref1'] = '';
		$data['bucket'] = '';
		$data['gender'] = '';
		$data['limit'] = 12;
		$data['offset'] = 0;
		$data['limit_cond'] = " LIMIT 0,12";
		$data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
		$segment = $this->uri->segment(1);
		$fav_for = '';$object_ids = '';
		$object_data['user_id'] = $data['user_id'];
		
		if($segment == 'bookmarks-products'){
			
			$fav_for = 'product';
		}else if($segment == 'bookmarks-collections'){
			
			$fav_for = 'collection';
		}else if($segment == 'bookmarks-events'){
			
			$fav_for = 'event';
		}else if($segment == 'bookmarks-blogs'){
			
			$fav_for = 'blog';
		}else {
			echo "something went wrong please try again";
		}
		
		if($fav_for != '')$object_ids = $this->User_info_model->get_wishlisted_ids($data,$fav_for);
		
		if($fav_for == 'product' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = explode(",",$object_ids);
			$data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
			$object_data['products'] = $this->Mobile_look_model->get_products_list($object_ids,$data);
			$object_data['total_products'] = count($data['table_search']);
			$this->load->view('seventeen/common/header_view');
			$this->load->view('seventeen/wishlist_product',$object_data);
			$this->load->view('seventeen/common/footer_view');
			
		}else if($fav_for == 'collection' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['top_collection'] = $this->Collection_model->get_all_collection($data);
			$object_data['total_collection'] = count(explode(",",$object_ids));
			
			$this->load->view('seventeen/common/header_view');
			$this->load->view('seventeen/wishlist_collection',$object_data);
			$this->load->view('seventeen/common/footer_view');
			
		}else if($fav_for == 'event' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['top_events'] = $this->Event_model->get_all_event($data);
			$object_data['total_events'] = count(explode(",",$object_ids));
			
			$this->load->view('seventeen/common/header_view');
			$this->load->view('seventeen/wishlist_event',$object_data);
			$this->load->view('seventeen/common/footer_view');
			
		}else if($fav_for == 'blog' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['top_blogs'] = $this->Blog_model->get_blog_data($data);
			$object_data['total_blogs'] = count(explode(",",$object_ids));
			
			$this->load->view('seventeen/common/header_view');
			$this->load->view('seventeen/wishlist_blog',$object_data);
			$this->load->view('seventeen/common/footer_view');
			
		}else{
			
			echo "something went wrong please try again";
		}
	}
	
	function get_more_bookmark(){
		$data['user_id'] = $this->session->userdata('user_id');
		$data['body_shape'] = '';
		$data['pa_pref1'] = '';
		$data['bucket'] = '';
		$data['gender'] = '';
		$data['limit'] = 12;
		$data['offset'] = $this->input->post('offset')*12;
		$fav_for = $this->input->post('fav_for');
		$data['limit_cond'] = ' ';
		$data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
		$object_ids = '';
		$object_data['user_id'] = $data['user_id'];
		
		if($fav_for != '')$object_ids = $this->User_info_model->get_wishlisted_ids($data,$fav_for);
		
		$data['limit_cond'] = ' LIMIT '.$data['offset'].',12 ';
		if($fav_for == 'product' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = explode(",",$object_ids);
			$data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
			$object_data['products'] = $this->Mobile_look_model->get_products_list($object_ids,$data);
			
			$this->load->view('seventeen/more_product',$object_data);
			
		}else if($fav_for == 'collection' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['top_collection'] = $this->Collection_model->get_all_collection($data);
			$object_data['total_collection'] = count(explode(",",$object_ids));
			
			$this->load->view('seventeen/more_collection',$object_data);
			
		}else if($fav_for == 'event' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['top_events'] = $this->Event_model->get_all_event($data);
			$object_data['total_events'] = count(explode(",",$object_ids));
			
			$this->load->view('seventeen/more_event',$object_data);
			
		}else if($fav_for == 'blog' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['top_blogs'] = $this->Blog_model->get_blog_data($data);
			$object_data['total_blogs'] = count(explode(",",$object_ids));
			
			$this->load->view('seventeen/more_blog',$object_data);
			
		}else{
			
			echo "something went wrong please try again";
		}
	}
  
}