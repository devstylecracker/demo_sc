<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo_signup2 extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
       parent::__construct();
      // $this->load->model('Brands_model');
   	}

	public function index()
	{
		$this->load->view('common/landing_header_view');
		$this->load->view('promo_signup2');
		$this->load->view('common/footer_view');
	}
}