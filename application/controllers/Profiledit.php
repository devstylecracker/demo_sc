<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profiledit extends MY_Controller {
	

	function __construct(){
       parent::__construct();
       $this->load->model('Schome_model');
       $this->load->model('Pa_model');
       $this->load->model('User_info_model');
      // $this->load->library('upload');
       $this->load->library('bitly');
   	}
	/*
	public function index()
	{	
		if($this->session->userdata('user_id')){

			//$user_info = $this->Schome_model->get_users_info();
			$data['profiledata'] = $this->User_info_model->get_users_info_mobile($this->session->userdata('user_id'));

			if($data['profiledata']->gender == 'women'){
	    		
				@$data['profiledata']->user_body_shape = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(1)[0]['answer_id']);
	        	@$data['profiledata']->user_style = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(2)[0]['answer_id']);
	         	@$data['profiledata']->user_age = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(8)[0]['answer_id']);
	         	@$data['profiledata']->userageid = $this->Pa_model->get_users_answer(8)[0]['answer_id'];
	        	@$data['agerange'] = $this->Pa_model->get_question_answers(8);
	        	$bodyshapeid = @$this->Pa_model->get_users_answer_obj($this->session->userdata('user_id'),'user_body_shape')[0]['id']; 
	        		
	        	$data['profiledata']->bodyshapeimg = $this->Pa_model->get_question_img_answers(1,1,$bodyshapeid)[0]['image_name'];
	        	//print_r($data['profiledata']);exit;

	        	$data['user_pa_data'] = $this->pa_get($this->session->userdata('user_id'),1);	        	

	        	if(!empty($this->input->post()))
	        	{
	        		$this->change_account_details();
	        	}
				$this->load->view('seventeen/common/header_view');
				$this->load->view('seventeen/profile',$data);
				$this->load->view('seventeen/common/footer_view');
				
	        }else{
	        	//echo "profile Edit male";
	        	@$data['profiledata']->user_body_shape = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(13)[0]['answer_id']);
	       	 	@$data['profiledata']->user_style = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(14)[0]['answer_id']);	       	 	
	        	@$data['profiledata']->user_age = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(17)[0]['answer_id']);
	        	@$data['profiledata']->userageid = $this->Pa_model->get_users_answer(17)[0]['answer_id'];
	        	@$data['agerange'] = $this->Pa_model->get_question_answers(17);	        	

	        	$data['user_pa_data'] = $this->pa_get($this->session->userdata('user_id'),2);

	        	if(!empty($this->input->post()))
	        	{
	        		$this->change_account_details();
	        	}
				$this->load->view('seventeen/common/header_view');
				$this->load->view('seventeen/profile',$data);
				$this->load->view('seventeen/common/footer_view');				
	        }

		}else{
			redirect('/');
		}
	}*/
	
	public function index()
	{	
		
		if($this->session->userdata('user_id')){
			$user_id = $this->session->userdata('user_id');
			$img_set = $this->Pa_model->get_user_img_set($this->session->userdata('user_id'));
			$data['profiledata'] = $this->User_info_model->get_users_info_mobile($this->session->userdata('user_id'));
			$data['profiledata']->name = $data['profiledata']->first_name.' '.$data['profiledata']->last_name;

			if($data['profiledata']->gender == 'women'){
	    		
				@$data['profiledata']->user_body_shape = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(1)[0]['answer_id']);
	        	@$data['profiledata']->user_style = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(2)[0]['answer_id']);
	         	@$data['profiledata']->user_age = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(8)[0]['answer_id']);
	         	
	         	@$data['profiledata']->userageid = $data['profiledata']->age_answer_id!='' ? $data['profiledata']->age_answer_id : $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(8)[0]['answer_id']);

	        	@$data['agerange'] = $this->Pa_model->get_question_answers(8);
	        	$bodyshapeid = @$this->Pa_model->get_users_answer_obj($this->session->userdata('user_id'),'user_body_shape')[0]['id']; 
	        		
	        	$data['profiledata']->bodyshapeimg = $this->Pa_model->get_question_img_answers(1,1,$bodyshapeid,$img_set)[0]['image_name'];
				$gender = 1;
	        }else{
	        	
	        	@$data['profiledata']->user_body_shape = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(13)[0]['answer_id']);
	       	 	@$data['profiledata']->user_style = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(14)[0]['answer_id']);	       	 	
	        	@$data['profiledata']->user_age = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(17)[0]['answer_id']);

	        	@$data['profiledata']->userageid = $data['profiledata']->age_answer_id!='' ? $data['profiledata']->age_answer_id : $this->Pa_model->get_users_answer(17)[0]['answer_id'];
	        	@$data['agerange'] = $this->Pa_model->get_question_answers(17);	        	
				$gender = 2;				
	        }

			$data['user_pa_data'] = $this->pa_get($this->session->userdata('user_id'),$gender);
			$data['user_info'] = $this->getuserinfo_get($this->session->userdata('user_id'));
			if(!empty($this->input->post())){

				$result = $this->change_account_details();				

				$data['profiledata']->gender = $this->input->post('sc_gender1')==1 ? 'women' : 'men';
				$data['profiledata']->name = $this->input->post('sc_name1');
				$data['profiledata']->userageid = $this->input->post('sc_agerange1');
				$data['profiledata']->contact_no = $this->input->post('sc_mobile1');
				//redirect('profile', 'refresh'); 
			}
			
			$this->load->view('seventeen/common/header_view');
			$this->load->view('seventeen/profile',$data);
			$this->load->view('seventeen/common/footer_view');		
		}else{
			redirect('/');
		}
	}

	public function change_account_details(){		
		
		$user_id = $this->session->userdata('user_id');
		$savedgender = $this->session->userdata('gender');
		$mobile = $this->session->userdata('contact_no');		
		// echo '<pre>';print_r($this->session->userdata());

		if(!empty($user_id))
		{	
			$birth_day 						= $this->input->post('day_start');
			$birth_month 					= $this->input->post('month_start');
			$birth_year 					= $this->input->post('year_start');
			$user_data['user_id'] 			= $user_id;
			$user_data['gender'] 			= $this->input->post('sc_gender1');
			$user_data['age_range'] 		= $this->input->post('sc_agerange1');
			$user_data['birth_date'] 		= $birth_year.'-'.$birth_month.'-'.$birth_day;
			$user_data['mobile_no'] 		= $this->input->post('sc_mobile1');
			$user_data['is_gender_change'] 	= ($savedgender!=$user_data['gender']) ? 1 : 0;
			$user_name 						= $this->input->post('sc_name1');
			$user_name = preg_replace('/\s+/', ' ', $user_name);
			$name =  explode(' ',$user_name);
			$count = count($name);
			if(!empty($name) && $count >= 2){
				$user_data['first_name'] = $name[0];
				$user_data['last_name'] = $name[1].' '.@$name[2];				
			}else{
				
				$user_data['error'] =  "Please enter your first and last name.";				
			}
			
			if($user_data['gender'] == '2' ){ 
				$user_data['gender'] = 2; 
				$user_data['question_id'] = 15;
			}else if($user_data['gender'] == '1' ){ 
				$user_data['gender'] = 1;
				$user_data['question_id'] = 8;
			}			
			//echo '<pre>';print_r($user_data);exit();
			//echo '<pre>';print_r( $this->session->userdata());
			$result = $this->User_info_model->update_account_details($user_data);
			if($result==2)
			{
				  $user_data['confirm_mobile'] = $this->User_info_model->getOtpStatus($this->session->userdata('user_id'));		                 
		          $scmobile = $user_data['mobile_no'];
		          $otp = $this->scgenrate_otp($scmobile);
		           redirect('pa/10');		        
			}
			$data = new stdClass();
			$data->user_info = (object)$this->User_info_model->get_users_info_mobile($user_id);
			//echo '<pre>';print_r($data->user_info);exit;			
			$gender = $this->Pa_model->get_gender($user_data['user_id']);
			$data->pa= $this->pa_get($user_data['user_id'],$user_data['gender']);
			//echo '<pre>';print_r($data->pa);exit;
			if($savedgender!=$gender) // remove on 18-01-2018
			{				
				$this->load->view('seventeen/common/header_view');    			
    			$this->load->view('seventeen/common/footer_view');
    			redirect('pa?type=add');
			}

			if($result==1)
			{
				return $result;				
			}else
			{
				return $user_data['error'];
			}

		}else
		{
			redirect('/');
		}
		//$this->success_response($data);
	}

	/*Reset Password Function*/

	public function resetPassword()
    {
	    $oldpwd = $this->input->post('oldpwd');
	    $newpwd = $this->input->post('newpwd');

	    if(trim($oldpwd) == ''){
          echo "Please enter your old password";

          }if(trim($newpwd) == ''){
            echo "Please enter your new password";

          }else{
            $result = $this->Schome_model->reset_password($oldpwd,$newpwd);
            if($result){      
             echo "Your Password has been reset";
            }else{
              if($result == false){               
                echo "Old Password is not correct";
              }else
              {
                echo $result;
              }
            }
          }    
	  }
	/*End Reset Password Function*/
	
	public function generate_bitly(){
		if($this->session->userdata('user_id')){
			$loc = $this->input->post('loc');
			echo $this->bitly->shorten($loc);
		}
	}
	

	public function wardrobe(){
		$this->load->view('common/header_view');
		$this->load->view('wardrobe');
		$this->load->view('common/footer_view');
	}

	public function pa_get($user_id,$gender='')
	{
		// $gender=2;
		if($user_id){
			$img_set = $this->Pa_model->get_user_img_set($user_id);
			if($gender == '1'){
				$data = new stdClass();
				$data -> body_shape = $this->Pa_model->get_question_img_answers(1,$gender,'',$img_set);
				$data -> style = $this->Pa_model->get_question_img_answers(2,$gender,'',$img_set);
				$data -> brand_list = unserialize(PA_BRAND_LIST_WOMEN);
				
			} // end of women pa
			else {
				$data = new stdClass();
				$data->body_shape = $this->Pa_model->get_question_answers(24,$gender,'',$img_set);
				$data->style = $this->Pa_model->get_question_img_answers(25,$gender,'',$img_set);
				$data -> brand_list = unserialize(PA_BRAND_LIST_MEN);

			}// end men pa
			
			$data -> user_body_shape = @$this->Pa_model->get_users_answer_obj($user_id,'user_body_shape',$gender)[0];
			$data -> user_style_p1 = @$this->Pa_model->get_users_answer_obj($user_id,'user_style_p1',$gender,$img_set)[0];
			$data -> user_style_p2 = @$this->Pa_model->get_users_answer_obj($user_id,'user_style_p2',$gender,$img_set)[0];
			$data -> user_style_p3 = @$this->Pa_model->get_users_answer_obj($user_id,'user_style_p3',$gender,$img_set)[0];
			$data -> user_brand_list = @$this->Pa_model->get_users_brand_list($user_id,'user_brand_list','',$gender);
			$data -> question_resume_id = $this->Pa_model->get_question_resume_id($user_id);
			$data -> edited_data ='';
			return $data;
			
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}

	}
	
	public function getuserinfo_get($user_id)
	{
		if($user_id){
			$user_info = (object)$this->User_info_model->get_users_info_mobile($user_id);
			return $user_info;
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
	}

	public function scgenrate_otp($uc_mobile_no){
    if($uc_mobile_no!='')
    {
	      $data['mobile_no'] = '91'.$uc_mobile_no;
		  $data['type'] = 'register';
	      $otp_no = $this->User_info_model->scgenrate_otp($data);
	      if($otp_no > 0){
	        /* Call SMS send API */
	        $message = 'Welcome+to+StyleCracker.+Your+OTP+for+mobile+verification+is+'.$otp_no.'.+Stay+Stylish!';
	        file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile='.$data['mobile_no'].'&message='.$message);       
	        /* end of code Call SMS send API */
	        return $otp_no;
	      }else{
	        return false;
	      }
	    }
  	}		


}
