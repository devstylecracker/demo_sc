<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Product extends MY_Controller {


	function __construct(){

       parent::__construct();

       $this->load->model('Product_desc_model');
       $this->load->model('home_model');
       $this->load->model('Wishlist_model');
       $this->load->model('Looks_model');
       

   	}

   	public function product_click(){
		if(isset($_POST['product_id']) && $_POST['product_id'] != "" && is_numeric($_POST['product_id']) && $_POST['product_id'] >0 ){
	   		$click_data['product_id'] = $_POST['product_id'];
	   		$this->Product_desc_model->add_users_product_click_track($click_data);
	   		$response['success'] = 1;
	   		echo json_encode($response);
	   	}else{
	   		$response['success'] = 0;
	   		echo json_encode($response);
	   	}
   	}

	public function details()
	{	
		$product_id = $this->uri->segment(count($this->uri->segment_array()));
		
		if(strrpos($product_id,'-'))
			$product_id = substr($product_id,strrpos($product_id,'-',-1)+1);
		
		if(isset($product_id) && $product_id && is_numeric($product_id) && $product_id >0){
			$click_data['product_id'] = $product_id;
			//$this->Product_desc_model->add_users_product_click_track($click_data);
			
			/*$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "http://166.62.89.246:8120/fetch_prod?prod_id=$product_id");
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 0);
			$product_related = curl_exec($ch);
			$product_respose = json_decode($product_related,1);
			if(!isset($product_respose['error']))
				$product_related = substr($product_related, 1,-1);
			else
			
			curl_close($ch);*/
			$product_related = 0;
			
			$data = $this->Product_desc_model->get_product($product_id);
			$this->seo_title = $data['products_details']['product_name'].' - '.$data['products_details']['brand_name'].' - '.'Buy Online Now on StyleCracker';
			$this->seo_desc = strip_tags($data['products_details']['product_description']);
			$this->seo_image = $this->config->item('product_image').$data['products_details']['image'];
			// echo "<pre>";print_r($data);exit;
			if(isset($data['products_details']) && $data['products_details']){
				/*$data['sub_category_info'] = $this->Product_desc_model->get_sub_category_info(@$data['products_details']['product_sub_cat_id']);*/
				$data['related_looks'] = $this->Product_desc_model->get_product_related_looks($product_id);
				$data['related_product'] = $this->Product_desc_model->get_product_related_products($product_id,$product_related);
				
				
				$this->seo_canonical = base_url().'product/details/'.$data['products_details']['slug'].'-'.$product_id;
				$this->load->view('common/header_view');
				$this->load->view('product',$data);
				$this->load->view('common/footer_view');
			}else{
				$this->load->view('common/header_view');
				$this->load->view('errors/error_404');
				$this->load->view('common/footer_view');

			}
		}else{
				$this->load->view('common/header_view');
				$this->load->view('errors/error_404');
				$this->load->view('common/footer_view');

		}
	}

	public function pg()
	{
		$this->load->view('common/header_view');
		$this->load->view('product');
		$this->load->view('common/footer_view');
	}

	function get_size_guide(){
		$product_id = $this->input->post('product_id');
		$brand_id = $this->input->post('brand_id');
		$size_guide_id = $this->input->post('size_guide_id');
		$size_guide_url = '';
			if($product_id > 0 && $brand_id >0){
				if($size_guide_id == 2){
					if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/top.jpg')){
						$size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/top.jpg';
					}
				}

				if($size_guide_id == 3){
					if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/bottom.jpg')){
						$size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/bottom.jpg';
					}
				}

				if($size_guide_id == 4){
					if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/footwear.jpg')){
						$size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/footwear.jpg';
					}
				}

				if($size_guide_id == 5){
					if(file_exists('assets/images/size_guide/brands/'.$brand_id.'/top_bottom.jpg')){
						$size_guide_url = base_url().'assets/images/size_guide/brands/'.$brand_id.'/top_bottom.jpg';
					}
				}
			}

			if(trim($size_guide_url) == ''){
				if($size_guide_id == 2){
					if(file_exists('assets/images/size_guide/stylecracker/top.jpg')){
						$size_guide_url = base_url().'assets/images/size_guide/stylecracker/top.jpg';
					}
				}

				else if($size_guide_id == 3){
					if(file_exists('assets/images/size_guide/size_guide/stylecracker/bottom.jpg')){
						$size_guide_url = base_url().'assets/images/size_guide/stylecracker/bottom.jpg';
					}
				}

				else if($size_guide_id == 4){
					if(file_exists('assets/images/size_guide/size_guide/stylecracker/footwear.jpg')){
						$size_guide_url = base_url().'assets/images/size_guide/stylecracker/footwear.jpg';
					}
				}

				else if($size_guide_id == 5){
					if(file_exists('assets/images/size_guide/size_guide/stylecracker/top_bottom.jpg')){
						$size_guide_url = base_url().'assets/images/size_guide/stylecracker/top_bottom.jpg';
					}
				}else{
				$size_guide_url = base_url().'assets/images/size_guide/stylecracker/sc_size_guide.jpg';
				}
			}
		
		echo $size_guide_url;

	}
}
