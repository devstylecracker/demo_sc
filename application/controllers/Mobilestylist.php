<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';

class Mobilestylist extends TT_REST_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Schome_model');
		$this->load->model('Mobile_model');
		$this->load->model('Pa_model');
		$this->open_methods = array('stylistinfo_post');    
	}

	public function getallstylistinfo_get(){
		$res = $this->Mobile_model->getStylistInfo();
		if(($res != NULL)) { 
			$data = new stdClass();
			$data= $res;//return value of get_users_info_mobile function
			$this->success_response($data);
		}
		else{ 
			$this->failed_response(1200, "No Record(s) Found");
			// return '0'; 
		}
	}

	public function stylistinfo_get(){
		$stylistid = $this->get('sc_stylist_id');
		$res = $this->Mobile_model->getSingleStylistInfo($stylistid);
		if(($res != NULL)) { 
			//$data = new stdClass();
			$data = $res;//return value of get_users_info_mobile function
			$this->success_response($data);
		}
		else{ 
			$this->failed_response(1200, "No Record(s) Found");
			// return '0'; 
		}
	}

 }