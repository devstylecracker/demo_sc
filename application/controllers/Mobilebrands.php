<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';

class Mobilebrands extends TT_REST_Controller 
{
 function __construct()
	{
		parent::__construct();
		$this->load->model('Schome_model');
		$this->load->model('Mobile_model');
		$this->load->model('Pa_model');
		$this->load->model('Brands_model');
		$this->load->model('Mobile_pa_model');
		$this->load->model('Mobile_model_android');
		$this->load->model('Mobile_look_model');
		$this->open_methods = array('get_all_brand_review_post');      
	}
	
	public function brands_get(){
		$limit = $this->get('limit');
		$offset = $this->get('offset');
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);		
		$data = (object)$this->Mobile_model->get_brands($limit,$offset);
		if((array)$data != NULL){
			  
			$this->success_response($data);
		}
		else	
			$data = array();	
			$this->success_response($data);
			//$this->failed_response(1001, "No Record(s) Found");
	
	}
	
	public function brand_all_names_get(){
		
		$data = (object)$this->Mobile_model->get_brand_all_names();
		//$data['featured review'] = $this->Mobile_model->get_review($brand_id); 
		if((array)$data != NULL){
			  
			$this->success_response($data);
		}
		else	
			//$data = array();	
			//$this->success_response($data);
			$this->failed_response(1001, "No Record(s) Found");
	
	}
	
	public function get_user_review_post(){
	    $user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
	    $brand_id = $this->post('brand_id');
	    $data = $this->Mobile_model->get_user_review($user_id,$auth_token,$brand_id);
            //print_r($data);
	     if($data != 2){
			  
			$this->success_response($data);
		}
		else	
			$this->failed_response(1001, "No Record(s) Found");
	
	
	}

	public function get_all_brand_review_get(){
		$brand_id = $this->get('brand_id');		
		//$data['featured review'] = $this->Mobile_model->get_review($brand_id); 		
		//$data['allreview'] = $this->Mobile_model->get_all_brand_mobile($brand_id);
		$data = $this->Mobile_model->get_all_brand_mobile($brand_id);			
		if($data !=NULL){
      	 // $data = new stdClass();
	    //  $data['avg_rating'] = $this->Brands_model->avgRatings($brand_id);
		 
	      $this->success_response($data);
	    }else{
			$data = array();
	       $this->success_response($data);
	    }
	}

	public function brands_review_post(){
		$user_id = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$brand_id = $this->post('brand_id');				
		$review_text = $this->post('review_text');
		//$status = $this->post('status');
		$ratings = $this->post('ratings');				
		$res = $this->Mobile_model->brands_review_mobile($user_id,$brand_id,$review_text,$ratings);		
		if($res != 2){
			$data = new stdClass();
			$data = (object)($res[0]);
			$data->avg_rating = $this->Brands_model->avgRatings($brand_id);
			$this->success_response($data);
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}		
	}
	function checkLimit($limit){
		if($limit == '') $query_limit = ''; else $query_limit = " LIMIT $limit ";
		return $query_limit;
	}
	function checkOffset($offset){
		if($offset == '') $query_offset = ''; else $query_offset = " OFFSET $offset ";
		return $query_offset;
	}
	/* old search 
	public function search_brand_name_get(){
		$brand_name = $this->get('brand_name');	
		$data = $this->Mobile_model->search_brand_name($brand_name);			
		if($data !=NULL){
      	 // $data = new stdClass();
	    //  $data['avg_rating'] = $this->Brands_model->avgRatings($brand_id);
		 
	      $this->success_response($data);
	    }else{
			$data = array();
	       $this->success_response($data);
	    }
	}
	*/
	
	public function search_brand_name_get(){
		$brand_name = $this->get('brand_name');
		$category_id = $this->get('category_id');
		if($category_id != ''){
			$data = $this->Mobile_look_model->search_brand_via_category($category_id);	
		}else{ 
			$data = $this->Mobile_model->search_brand_name($brand_name);
		}
		if($data !=NULL){
	      $this->success_response($data);
	    }else{
			$data = array();
			$this->success_response($data);
	    }
	}
	
	public function brand_slider_get(){
		$brands_banners = $this->Mobile_model_android->get_brands_banners();
		if(!empty($brands_banners)){
			$this->success_response($brands_banners);
	    }else{
		   $data = array();
	       $this->success_response($brands_banners);
	    }
	}
	
	public function single_brand_get(){
		$brand_id = $this->get('brand_id');
		$limit = $this->get('limit');
		$offset = $this->get('offset');
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);
		
		// $data['brands_banners'] = $this->Mobile_model_android->get_brands_banners();
		$data['brand_info'] = $this->Mobile_look_model->get_brand_info($brand_id);
		$data['brand_looks'] = $this->Mobile_look_model->get_looks_by_brand($brand_id,$limit,$offset);
		$data['products_data'] = $this->Mobile_look_model->get_brands_product($brand_id,$limit,$offset);
		if(!empty($data)){ 
			$this->success_response($data);
		}else{
		   $data = array();
	       $this->success_response($data);
	    }
	}
	
	public function brand_collection_get(){
		$limit = $this->get('limit');
		$offset = $this->get('offset');
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);
		$data['brands_banners'] = $this->Mobile_model_android->get_brands_banners();
		$data['brand_collection'] = $this->Mobile_look_model->get_brand_collection();
		$brand_collection_keys = array_keys($data['brand_collection']);
		$data['brand_collection']['brand_collection_keys'] = $brand_collection_keys;
		$data['brand_directory']['list'] = $this->Mobile_look_model->get_brand_all_names();
		$data['brand_directory']['category'] = $this->Mobile_look_model->get_all_category();
		if(!empty($data)){ 
			$this->success_response($data);
		}else{
		   $data = array();
	       $this->success_response($data);
	    }
	}
	
	function get_brand_wishlist_post(){
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		$bucket_id = $this->post('bucket_id');
		$fev_for = 'brand';
		$limit = $this->post('limit');
		$offset = $this->post('offset');
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);
		$data['brands'] = $this->Mobile_look_model->show_my_wishlist_brands($user_id,$fev_for,$limit,$offset);
		$data['message'] = '';
		if(!empty($data['brands'])){
			$this->success_response($data);
		}else{
			$data['brands'] = $this->Mobile_look_model->show_recommended_brands($bucket_id);
			$data['message'] = "You haven't added any brands to your wishlist, here are three we think you might like";
			$this->success_response($data);
		}
	} 
	
	function product_get(){
		$limit = $this->get('limit');
		$offset = $this->get('offset');
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);
		$res = $this->Mobile_model->get_products($limit,$offset);
		if(!empty($res)){
		      $this->success_response($res);
		    }else{
			   $res = array();
			   $this->success_response($res);
		    }  
	
	}
}
