<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Category extends MY_Controller {

	function __construct(){

		parent::__construct();
		$this->load->model('Category_filter_model');
		$this->load->model('Mobile_look_model');
		$this->load->model('category_model');
		$this->load->model('home_model');
		$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));

   	}

	function categorylist(){
		$data = array(); $cat_id=''; $data['category_breadcrumps'] = '';
		$gender = $this->session->userdata('gender');
		$category_slug=$this->uri->segment(2);
		if($category_slug == 'women'){ $gender = 1; }else if($category_slug == 'men'){ $gender = 2; }
		if($gender == 1){
			$cat_id = 1;
			$data['category_breadcrumps'] = '<li><a onclick="get_child_cat('.$cat_id.',\'Women\');">Women</a></li>';
			$data['gender'] = $gender.'-Women';
		}else if($gender == 2){
			$cat_id = 3;
			$data['category_breadcrumps'] = '<li><a onclick="get_child_cat('.$cat_id.',\'Men\');">Men</a></li>';
			$data['gender'] = $gender.'-Men';
		}
		// $cat_id = 47;
		if($cat_id == ''){
			$data['category_list'] = $this->Category_filter_model->get_gender(-1);
		}else{
			$data['category_list'] = $this->Category_filter_model->get_child_list($cat_id);
		}

		// echo "<pre>";print_r($data);exit;
		$this->load->view('seventeen/common/header_view');
		$this->load->view('seventeen/categories',$data);
		$this->load->view('seventeen/common/footer_view');

	}

	function get_child_cat(){
		$category_html = '';$breadcrumb_html_start= '';$breadcrumb_html_end= ''; $cat_name='';
		$cat_id = $this->input->post('cat_id');
		$arr_cat_breadcrump = $this->input->post('arr_cat_breadcrump');
		$category_list = $this->Category_filter_model->get_child_list($cat_id);

		$category_breadcrumps = '';
		$category_breadcrumps = $this->Category_filter_model->get_breadcrump($cat_id,$page_type='cat_list');

		$breadcrumb_html_start = $breadcrumb_html_start.'<ol class="breadcrumb">'.$category_breadcrumps.'</ol><div class="grid grid-items categories"><div class="row">';
		$breadcrumb_html_end = $breadcrumb_html_end.'</div></div>';
		if(!empty($category_list)) {
			foreach ($category_list as $value) {
				if($value['is_last'] == 0){
					$cat_name = $value['category_id'].'-'.$value['category_name'];
					$href = ' href="javascript:void(0)" onclick="get_child_cat('.$value['category_id'].',\''.$cat_name.'\');"';
				}else{
					$cat_slug = $value['category_slug'].'-'.$value['category_id'];
					$href = ' href="'.CAT_URL.''.$cat_slug.'" ';
				}

				$category_html = $category_html.'<div class="col-md-3 col-sm-6 col-xs-6 grid-col">
											  <div class="item-wrp">
												<div class="item">
												  <div class="item-img-wrp">
													<a id="category_image" value="'.$value['category_name'].'" '.$href.' >
														<img title="'.$value['category_name'].' - StyleCracker" alt="'.$value['category_name'].'- StyleCracker" src="'.$value['category_image'].'" onerror="this.onerror=null; this.src=\'https://i.imgur.com/Qu4zXlp.jpg\';"/>
													</a>
												  </div>
												  <div class="item-desc-wrp">
													<div class="title">
													<a id="category_name" value="'.$value['category_name'].'" title="'.$value['category_name'].'" '.$href.' >'.$value['category_name'].'</a>
													</div>
												  </div>
												</div>
											  </div>
											</div>';
          }//foreach
      }//if
		$categorylist_html = $breadcrumb_html_start.$category_html.$breadcrumb_html_end;
	  echo $categorylist_html;

	}

	function product_filter(){
		$user_id = $this->session->userdata('user_id');
		$bucket = $this->session->userdata('bucket');
		$cat_id = $this->uri->segment(count($this->uri->segment_array()));
		if(strrpos($cat_id,'-')) $cat_id = substr($cat_id,strrpos($cat_id,'-',-1)+1);
		if($user_id >0 || $bucket > 0){ $is_recommended = 1; }else{ $is_recommended = 0; }
		$limit = '20';
		$search = '';
		if(isset($_GET['search']) && $_GET['search']!=''){
			$search = $_GET['search'];
			$cat_id = 0;
		}
		$data = $this->Category_filter_model->get_products($user_id,$bucket,$cat_id,$lastlevelcat=null,$brandId=null,$priceId=null,$min_price=0,$max_price=0,$sort_by=0,$attributesId=null,$sizeId=null,$search,$limit,$offset=0,$is_recommended,$is_all=0);

		// price_list
		$data['filter']['price_list'] = array('0'=>'Below 999','1'=>'1000 - 1999','2'=>'2000 - 3499','3'=>'3500 - 4999','4'=>'5000 +');

		// sort by
		$data['filter']['sort_by'] = array('0'=>'latest','1'=>'Price high to low','2'=>'Price low to high');

		// global_attributes
		if(!empty($data['filter']['global_attributes'])){
            foreach($data['filter']['global_attributes'] as $val){
				$i=0;
				foreach($this->Category_filter_model->attributes_values($val['id']) as $values){
					$data['filter'][$val['attribute_slug']][$i]['id'] = $values['id'];
					$data['filter'][$val['attribute_slug']][$i]['attr_value_name'] = $values['attr_value_name'];
					@$data['filter'][$val['attribute_slug']][$i]['attr_hashtag'] = $values['attr_hashtag'];
					$i++;
				}
			}
		}

		// category_attributes
		if(!empty($data['filter']['category_attributes'])){
            foreach($data['filter']['category_attributes'] as $val){
				$i=0;
				foreach($this->Category_filter_model->attributes_values($val['id']) as $values){
					$data['filter'][$val['attribute_slug']][$i]['id'] = $values['id'];
					$data['filter'][$val['attribute_slug']][$i]['attr_value_name'] = $values['attr_value_name'];
					$i++;
				}
			}
		}


		$data['user_id'] = $user_id;
		$data['bucket'] = $bucket;
		$data['cat_id'] = $cat_id;
		$data['cat_name'] = $this->Category_filter_model->get_cat_name($cat_id);
		$data['category_breadcrumps'] = $this->Category_filter_model->get_breadcrump($cat_id);
		$data['selected_search'] = $search;
		$this->seo_title = ' '.@$data['cat_name'][0]['category_name'].' - Buy Online Now on StyleCracker';
		$this->seo_desc = '';
		// echo "<pre>";print_r($data);exit;
		$this->load->view('seventeen/common/header_view');
		$this->load->view('seventeen/product_filterpage',$data);
		$this->load->view('seventeen/common/footer_view');
	}

	function search_product(){
		$category_list = $this->Category_filter_model->search_product();

	}
	function filter_products(){

        $data = array();$product_html = '';
		$user_id = $this->session->userdata('user_id');
		$bucket = $this->session->userdata('bucket');
		$cat_id = $this->input->post('cat_id');
        $lastlevelcat = $this->input->post('lastlevelcat');
        $brandId = $this->input->post('brandId');
        $priceId = $this->input->post('priceId');
        $min_price = $this->input->post('min_price');
        $max_price = $this->input->post('max_price');
        $sort_by = $this->input->post('sort_by');
        $attributesId = $this->input->post('attributesId');
        $sizeId = $this->input->post('sizeId');
        $search = $this->input->post('search');
		$offset = $this->input->post('offset');
		$limit = '20';
		$offset = '0';
		if($user_id >0 || $bucket > 0){ $is_recommended = 1; }else{ $is_recommended = 0; }
        if($priceId!=''){ $min_price =0; $max_price=0; }
		// echo "<pre>";print_r($_POST);exit;

		if(!empty($lastlevelcat))$lastlevelcat = implode(",",$lastlevelcat);
		if(!empty($brandId))$brandId = implode(",",$brandId);
		if(!empty($attributesId))$attributesId = implode(",",$attributesId);
		if(!empty($sizeId))$sizeId = implode(",",$sizeId);

        $data = $this->Category_filter_model->get_products($user_id,$bucket,$cat_id,$lastlevelcat,$brandId,$priceId,$min_price=0,$max_price=0,$sort_by,$attributesId,$sizeId,$search,$limit,$offset,$is_recommended,$is_all=0);

		// price_list
		$data['filter']['price_list'] = array('0'=>'Below 999','1'=>'1000 - 1999','2'=>'2000 - 3499','3'=>'3500 - 4999','4'=>'5000 +');

		// sort by
		$data['filter']['sort_by'] = array('0'=>'latest','1'=>'Price high to low','2'=>'Price low to high');

		// global_attributes
		if(!empty($data['filter']['global_attributes'])){
            foreach($data['filter']['global_attributes'] as $val){
				$i=0;
				foreach($this->Category_filter_model->attributes_values($val['id']) as $values){
					$data['filter'][$val['attribute_slug']][$i]['id'] = $values['id'];
					$data['filter'][$val['attribute_slug']][$i]['attr_value_name'] = $values['attr_value_name'];
					@$data['filter'][$val['attribute_slug']][$i]['attr_hashtag'] = $values['attr_hashtag'];
					$i++;
				}
			}
		}

		// category_attributes
		if(!empty($data['filter']['category_attributes'])){
            foreach($data['filter']['category_attributes'] as $val){
				$i=0;
				foreach($this->Category_filter_model->attributes_values($val['id']) as $values){
					$data['filter'][$val['attribute_slug']][$i]['id'] = $values['id'];
					$data['filter'][$val['attribute_slug']][$i]['attr_value_name'] = $values['attr_value_name'];
					$i++;
				}
			}
		}

		$data['user_id'] = $user_id;
		$data['bucket'] = $bucket;
		$data['cat_id'] = $cat_id;
		$data['cat_name'] = $this->Category_filter_model->get_cat_name($cat_id);
		$data['category_breadcrumps'] = $this->Category_filter_model->get_breadcrump($cat_id);

		$data['selected_lastlevelcat'] = explode(",",$lastlevelcat);
		$data['selected_brandId'] = explode(",",$brandId);
		$data['selected_attributesId'] = explode(",",$attributesId);
		$data['selected_sizeId'] = explode(",",$sizeId);
		$data['selected_sort_by'] = $sort_by;
		$data['selected_priceId'] = $priceId;
		$data['selected_search'] = $search;

        $this->load->view('seventeen/filter_product',$data);
    }

	function get_more_filter_products(){

        $data = array();$product_html = '';
		$user_id = $this->session->userdata('user_id');
		$bucket = $this->session->userdata('bucket');
		$cat_id = $this->input->post('cat_id');
        $lastlevelcat = $this->input->post('lastlevelcat');
        $brandId = $this->input->post('brandId');
        $priceId = $this->input->post('priceId');
        $min_price = $this->input->post('min_price');
        $max_price = $this->input->post('max_price');
        $sort_by = $this->input->post('sort_by');
        $attributesId = $this->input->post('attributesId');
        $sizeId = $this->input->post('sizeId');
        $search = $this->input->post('search');
		$offset = $this->input->post('offset');
		$limit = '20';
		$offset = $offset*20;
		if($user_id >0 || $bucket > 0){ $is_recommended = 1; }else{ $is_recommended = 0; }
        if($priceId!=''){ $min_price =0; $max_price=0; }
		// echo "<pre>";print_r($_POST);exit;

		// if(!empty($lastlevelcat))$lastlevelcat = implode(",",$lastlevelcat);
		// if(!empty($brandId))$brandId = implode(",",$brandId);
		// if(!empty($attributesId))$attributesId = implode(",",$attributesId);
		// if(!empty($sizeId))$sizeId = implode(",",$sizeId);

        $data = $this->Category_filter_model->get_products($user_id,$bucket,$cat_id,$lastlevelcat,$brandId,$priceId,$min_price=0,$max_price=0,$sort_by,$attributesId,$sizeId,$search,$limit,$offset,$is_recommended,$is_all=0);

		// price_list
		$data['filter']['price_list'] = array('0'=>'Below 999','1'=>'1000 - 1999','2'=>'2000 - 3499','3'=>'3500 - 4999','4'=>'5000 +');

		// sort by
		$data['filter']['sort_by'] = array('0'=>'latest','1'=>'Price high to low','2'=>'Price low to high');

		// global_attributes
		if(!empty($data['filter']['global_attributes'])){
            foreach($data['filter']['global_attributes'] as $val){
				$i=0;
				foreach($this->Category_filter_model->attributes_values($val['id']) as $values){
					$data['filter'][$val['attribute_slug']][$i]['id'] = $values['id'];
					$data['filter'][$val['attribute_slug']][$i]['attr_value_name'] = $values['attr_value_name'];
					@$data['filter'][$val['attribute_slug']][$i]['attr_hashtag'] = $values['attr_hashtag'];
					$i++;
				}
			}
		}

		// category_attributes
		if(!empty($data['filter']['category_attributes'])){
            foreach($data['filter']['category_attributes'] as $val){
				$i=0;
				foreach($this->Category_filter_model->attributes_values($val['id']) as $values){
					$data['filter'][$val['attribute_slug']][$i]['id'] = $values['id'];
					$data['filter'][$val['attribute_slug']][$i]['attr_value_name'] = $values['attr_value_name'];
					$i++;
				}
			}
		}

		// echo "<pre>";print_r($data);exit;
		$data['user_id'] = $user_id;
		$data['bucket'] = $bucket;
		$data['cat_id'] = $cat_id;

        $this->load->view('seventeen/more_filter_product',$data);
    }

	function get_size_html(){
		$intProdID = $this->input->post('intProdID');
        $strProdSize = $this->input->post('strProdSize');
        $strProdLabel = $this->input->post('strProdLabel');
		$arrProdSize = explode(',',$strProdSize);
		$arrProdLabel = explode(',',$strProdLabel);
		$i=0;$size_html = '';
		// echo "<pre>";print_r($arrProdSize);exit;
		if(@$arrProdSize[0]!=''){
			foreach($arrProdSize as $val){
				if(strlen($arrProdLabel[$i]) < 3){
					$size_html = $size_html.'<span class="btn-diamond btn-size productsize'.$intProdID.'" onclick="add_active_class_size('.$intProdID.','.$arrProdSize[$i].');" id="product_size'.$arrProdSize[$i].'" >'.$arrProdLabel[$i].'</span>';
				}else{
					$size_html = $size_html.'<span class="btn-diamond-secondary btn-size productsize'.$intProdID.'" attr-size="'.$val.'" onclick="add_active_class_size('.$intProdID.','.$arrProdSize[$i].');" id="product_size'.$arrProdSize[$i].'" ><span>'.$arrProdLabel[$i].'</span></span>';
				}
				$i++;
			}
		}else{
			$size_html = 'Out Of Stock';
		}

		echo $size_html;
	}
	
	function search_products(){
		// $search = 'Kultprit Navy & Grey Slim Fit Sweatshirt';//product
		// $search = 'kultprit';//brand
		$search = 'blue red';//tag
		$data = $this->Category_filter_model->search_products($search);
		echo "<pre>";print_r($data);
	}
	
	function get_popup_products(){
		$product_list = ''; 
		$lastlevelcat = $this->input->post('cat');
		$brandId = $this->input->post('brand');
		$offset = $this->input->post('offset');
		$search = $this->input->post('popup_tags');
		$cart_id = $this->input->post('cart_id');
		$limit = 1000;
		$product_data = $this->Category_filter_model->get_products($user_id=0,$bucket=0,$cat_id=0,$lastlevelcat,$brandId,$priceId=0,$min_price=0,$max_price=0,$sort_by=0,$attributesId=0,$sizeId=0,$search,$limit,$offset,$is_recommended=0,$is_all=0);
		// echo "<pre>";print_r($product_data);
		$imagify_brandid = unserialize(IMAGIFY_BRANDID); 
		if(!empty($product_data)){ 
			foreach($product_data['products'] as $val){ 
				// echo "<pre>";print_r($val);
				 $productUrl = '';
				 $productzoomImage = '';
		          if(in_array($val->brand_id,$imagify_brandid))
		          {
		            $productUrl = $this->config->item('products_thumbnail').@$val->product_img_name;
		          }else
		          {
		           	//$productUrl = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_124/'.$val->product_img_name;
		          	$productUrl = base_url('sc_admin/assets/products/thumb_124/').@$val->product_img_name;
		          }
		          $productZoomImage = base_url().'sc_admin/assets/products/'.@$val->product_img_name;

				  $product_list = $product_list.'<li class="item">
						<div id ="product_img'.$val->product_id.'" class="product-img" onclick="show_product_details('.$val->product_id.');"  data-toggle="tooltip" data-placement="top" data-html="true">
								<img data-img-link="'.$val->product_img.'" class="lazy" id="'.$val->product_id.'" src="'.$val->product_img.'" width="80px" height="100px" onerror="this.onerror=null; this.src=\'https://i.imgur.com/Qu4zXlp.jpg\';">					  
																		
							  </div>
								<div class="desc"  style="text-align:left;">
							    <div>CTC: <i class="fa fa-inr"></i> '.$val->price.' </div>
							    <div>MRP: <i class="fa fa-inr"></i> '.$val->mrp.' </div>
								<div>Seller: '.$val->brand_name.'</div>								
								<div><button class="btn btn-box-tool btn-1" style="left:0 !important;" onclick="get_product_extraimage('.$val->product_id.',\''.$productZoomImage.'\')" >VIEW</button></div>
								</div>
							</li>';

				// Product Image modal
				//<button class="btn btn-box-tool" style="left:0 !important;" data-toggle="modal" data-target="#modal-gallery"><i class="fa fa-search-plus fa-lg align" "=""></i></button>
			}
			
			if($product_list != ''){
				//$product_list = $product_list.'<li id="load_more_'.$offset.'" class="item show-more"><div class="btn btn-info" onclick=more_product('.$lastlevelcat.') >Load more </div></li>';	
				$product_list = $product_list.'<li id="load_more_'.$offset.'" class="show-more" style="text-align: center;margin: 10px;"><div class="btn btn-info" onclick=more_product('.$lastlevelcat.') >Load more </div></li>';	
			}else{
				$product_list = $product_list.'<li class="item">No record found </li>';
			}
		}else{
			
			$product_list = $product_list.'<li class="item">No record found</li>';
		}
		
		echo $product_list;
	}

	function get_order_recommendation()
	{

		//echo 'get_order_recommendation';exit;
		$display_data = array();
		$lastlevel_catdata = $this->Category_filter_model->getLastLevelCategory();
		$last_level_category = array_column($lastlevel_catdata, 'id');	

		$firstlevel_catdata = $this->Category_filter_model->get_category_level2();
		$first_level_category = array_column($firstlevel_catdata, 'category_id');	
		//echo '<pre>';print_r($second_level_category);exit;		
		//$offset = $this->input->post('offset');		
		$category_arr = array();
		$attribute_arr = array();
		$category_name_arr = array();
		$attribute_name_arr = array();
		$negative_category_arr = array();
		$positive_category_arr = array();
		$user_category_selection = array();	
		$user_attribute_selection = array();
		$category_product_list = array();
		$user_firstlevelcategory_selection = array();	
		$category_product_list_data = array();	

		$category_str = '';
		$attribute_str = '';
		$negative_category_str = '';
		$negative_attribute_str = '';
		$positive_category_str = '';
		$positive_attribute_str = '';
		$category_name_str = '';
		$attribute_name_str = '';
		$numofproducts = '';
		$cart_id = $this->input->post('cartid');
		$recom_page_type = $this->input->post('type');
		$cart_product_id = $this->Category_filter_model->get_cart_data($cart_id)[0]['product_id'];
		$numofproducts = $this->Category_filter_model->get_scboxdata_byproduct($cart_product_id)->numofproducts;		
		
		if($cart_id!='')
		{
			$category_data = $this->Category_filter_model->get_order_category($cart_id);
			//echo '<pre>';print_r($category_data);exit;
			$attribute_data = $this->Category_filter_model->get_order_attributes($cart_id);
			//echo '<pre>';print_r($attribute_data);
		}
		
		if(!empty($category_data) || !empty($attribute_data))
		{
			foreach($category_data as $val)
			{
				if($val['category_id']!='')
				{
					$category_arr[] = $val['category_id'];
					$category_name_arr[] = $val['category_name'];
					$category_name_str = $category_name_str.'<br/>'.$val['category_id'].' '.$val['category_name'];
					$user_category_selection[$val['category_id']] = $val['category_name'];
					if(in_array($val['category_id'], $first_level_category))
					{
						$user_firstlevelcategory_selection[$val['category_id']] = $firstlevel_catdata[$val['category_id']];
					}
					if($val['category_id']>0){
						$positive_category_arr[] = $val['category_id'];					
					}else{
						$negative_category_arr[] = abs($val['category_id']);					
					}
				}			
			}

			$category_str = implode(',', $category_arr);
			$lastlevel_catp = array_intersect($last_level_category, $positive_category_arr);
			$lastlevel_catn = array_intersect($last_level_category, $negative_category_arr);		
			$positive_category_str = implode(',', $lastlevel_catp);
			$negative_category_str = implode(',', $lastlevel_catn);

			foreach($attribute_data as $val)
			{
				if($val['attribute_id']!='')
				{
					$attribute_arr[] = $val['attribute_id'];
					$attribute_name_arr[] = $val['attr_value_name'];
					$attribute_name_str = $attribute_name_str.'<br/>'.$val['attribute_id'].' '.$val['attr_value_name'];
					$user_attribute_selection[$val['attribute_id']] = $val['attr_value_name'];
					if($val['attribute_id']>0)
					{
						$positive_attribute_str = $positive_attribute_str.','.$val['attribute_id'];
					}else
					{
						$negative_attribute_str = $negative_attribute_str.','.abs($val['attribute_id']);
					}
				}
				
			}
			$positive_attribute_str = trim($positive_attribute_str,',');
			$negative_attribute_str = trim($negative_attribute_str,',');		

			$category_str = implode(',', $category_arr);
			$attribute_str = implode(',', $attribute_arr);
			
			$product_result = $this->Category_filter_model->get_product_recommendation($positive_category_str,$positive_attribute_str,$negative_category_str,$negative_attribute_str);
			//echo '<pre>';print_r($product_result);		
			$productids_array = array_map(function ($product_result) {return $product_result->product_id;}, $product_result);
			//echo '<pre>';print_r($productids_array);exit;
			$productids_str = implode(',', $productids_array);	
			//echo 'productids_str==='.$productids_str;
			//echo '<br/>';print_r($product_result);exit;	
			if($productids_str!='')
			{
				$category_product_list = $this->Category_filter_model->get_product_category_level2($productids_str,$product_result);			
			}

			if(!empty($user_firstlevelcategory_selection))
			{
				foreach($user_firstlevelcategory_selection as $key => $val)
				{
					$category_product_list_data[$key]['cat_id'] = $key;
					$category_product_list_data[$key]['cat_name'] = @$val['category_name'];
					$category_product_list_data[$key]['cat_data'] = @$category_product_list[@$key];
				}
				
				//echo '<pre>';print_r($user_firstlevelcategory_selection);exit;
			}
			
			//echo '<pre>';print_r($category_product_list);exit;
			$display_data['category_product_list'] = $category_product_list;
			// $display_data['user_category_selection'] = $user_category_selection;
			// $display_data['user_attribute_selection'] = $user_attribute_selection;
			$display_data['user_category_selection'] = $user_category_selection;
			$display_data['user_attribute_selection'] = $user_attribute_selection;
			$display_data['user_firstlevelcategory_selection'] = $user_firstlevelcategory_selection;
			$display_data['category_product_list_data'] = $category_product_list_data;
			$display_data['numofproducts'] = $numofproducts;
			$display_data['cart_id'] = $cart_id;
			$display_data['success'] = true;
		}else
		{
			$display_data['success'] = false;
			$display_data['error_msg'] = 'No Data available for Curation';
		}		


		//set transient for display_data
		//echo $this->load->view('seventeen/curation/recom_popup_view',$display_data,true);	
		if($recom_page_type=='scbox-order-products-panel')
		{
			//echo $this->load->view('seventeen/curation/recom_panel_view',$display_data,true);	
			echo  json_encode($display_data);	

		}else
		{
			echo $this->load->view('seventeen/curation/recom_popup_view',$display_data,true);	
		}	

	}


	

}
