<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Brand_category extends MY_Controller {


	function __construct(){

       parent::__construct();
       $this->load->model('Allproducts_model');
       $this->load->model('Brand_category_model');
	   $this->load->model('Product_desc_model');
   	}

    function index(){
		echo "comming";
		$brand_name = $this->uri->segment(3);
		
	}
	
	function brand_product(){
		
		$brand_name = $this->uri->segment(3);
		$category_slug = $this->uri->segment(4);
		
		$data['all_data'] = $this->Brand_category_model->get_brand_product($brand_name,$category_slug,1);
		
		$title = array('bowties-286'=>'Bow Ties','bracelets-3314'=>'Bracelets','cuff-links-3318'=>'Cuff Links','grooming-345'=>'Grooming','frames'=>'Frames','pocket-squares-289'=>'Pocket Squares','sunglasses-3326'=>'Sunglasses','ties-18'=>'Ties-18');
		$this->seo_title = $title[$category_slug].' - Buy Online Now on StyleCracker ';
		
		$this->load->view('common/header_view');
		$this->load->view('Brand_category_view',$data);
		$this->load->view('common/footer_view');
	}
	
	function get_more_cat_product(){
		 $offset = $this->input->post('offset');
		 $category_slug = $this->input->post('category_slug');
		 $brand_name = $this->input->post('brand_name');
		 $data['all_data'] = $this->Brand_category_model->get_brand_product($brand_name,$category_slug,$offset);
		 $this->load->view('more_products2',$data);
	}
	
	
	
	
}