<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stylecart extends MY_Controller {

	function __construct(){

       parent::__construct();
       $this->load->helper('cookie');
       $this->load->model('cartnew_model');
       $this->load->model('Schome_model');
       $this->load->model('product_desc_model');

       $this->load->model('Mcart_model_android');

       $this->load->library('curl');
   	}


	public function index()
	{
		$objectid = '';
		$parameterStr = $this->uri->segment(2);
		$parameterArr = explode('?',$parameterStr);		
		$objectid = @$parameterArr[0];
		$data = array();
		if($objectid!='')
		{			
				$data['cart_info'] = $this->cartnew_model->get_scboxcart();
				
				$p_price = 0;  $data['referral_point']=0;
				if(!empty($data['cart_info'])){
					foreach($data['cart_info'] as $val){
						$p_price = $p_price + $val['discount_price'];
					}
				}
				
				$data['product_total_for_msg'] = $p_price;

				$data['pincode'] = $this->input->post('pincode')!='' ? $this->input->post('pincode') : @$_COOKIE['stylecracker_shipping_pincode'];
				$data['sc_cart_pay_mode'] = 'cod';
				
				// Add for first purchase
				if(!empty(@$data['cart_info'][0]['user_id']))
				{
					$ordercount_result = $this->cartnew_model->get_ordercount(@$this->session->userdata('user_id'));
									
					if($ordercount_result==0)
					{
						$defaultcoupon = $this->cartnew_model->get_defaultcoupons(@$this->session->userdata('user_id'),'','scboxcart',$data['cart_info'][0]['price']);
						setcookie('discountcoupon_stylecracker', $defaultcoupon, time() + (86400 * 30), "/");
					}else
					{

						if(isset($_COOKIE["discountcoupon_stylecracker"]) && (@$_COOKIE["discountcoupon_stylecracker"]=='FST2999' || @$_COOKIE["discountcoupon_stylecracker"]=='FST4999' || @$_COOKIE["discountcoupon_stylecracker"]=='FST6999') ) { 
							unset($_COOKIE["discountcoupon_stylecracker"]);
							setcookie("discountcoupon_stylecracker", "", time() - 3600, "/");
						}
					}
				}
				//End- Add for first purchase
				//setcookie('discountcoupon_stylecracker', 'SCBOX50', time() + (86400 * 30), "/");
				if(isset($_COOKIE['discountcoupon_stylecracker']) && $_COOKIE['discountcoupon_stylecracker']!=''){ 
					$data['coupon_code_msg'] = $this->cartnew_model->check_coupon_exist(strtolower($_COOKIE['discountcoupon_stylecracker']),'','scboxcart');
					$p_price = $p_price-$data['coupon_code_msg']['coupon_discount_amount'];
				}
				// echo 'coupon--'.$_COOKIE['discountcoupon_stylecracker'];
				//echo '<pre>';print_r($data['coupon_code_msg']);exit;
				setcookie('stylecracker_shipping_pincode', $data['pincode'], time() + ((86400 * 2) * 30), "/", "",  0);

					/*Referral Code*/ 
				if($this->session->userdata('user_id')!=''){
					$point = 0;
					$point = $this->cartnew_model->get_refer_point();
					if($point == 1 && $p_price >= MIN_CART_VALUE){ 
						// setcookie('is_redeem_point', '1', time() + ((86400 * 2) * 30), "/", "",  0);
						// $data['referral_point'] = POINT_VAlUE;
						setcookie('is_redeem_point', '0', time() + ((86400 * 2) * 30), "/", "",  0);
					}else{
						setcookie('is_redeem_point', '0', time() + ((86400 * 2) * 30), "/", "",  0);
					}
				}else{
						setcookie('is_redeem_point', '0', time() + ((86400 * 2) * 30), "/", "",  0);
					}

				//echo '<pre>';print_r($data);exit;
				$this->load->view('seventeen/common/header_view');
				$this->load->view('seventeen/cart/scboxcart',$data);
				$this->load->view('seventeen/common/footer_view');
		}else
		{
				$data['cart_info'] = $this->cartnew_model->get_cart();

				$p_price = 0;  $data['referral_point']=0;
				if(!empty($data['cart_info'])){
					foreach($data['cart_info'] as $val){
						$p_price = $p_price + $val['discount_price'];
					}
				}
				
				$data['product_total_for_msg'] = $p_price;
				$data['pincode'] = $this->input->post('pincode')!='' ? $this->input->post('pincode') : @$_COOKIE['stylecracker_shipping_pincode'];
				$data['sc_cart_pay_mode'] = 'cod';
				if(isset($_COOKIE['discountcoupon_stylecracker']) && $_COOKIE['discountcoupon_stylecracker']!=''){ 
					$data['coupon_code_msg'] = $this->cartnew_model->check_coupon_exist(strtolower($_COOKIE['discountcoupon_stylecracker']));
					$p_price = $p_price-$data['coupon_code_msg']['coupon_discount_amount'];
				}
				setcookie('stylecracker_shipping_pincode', $data['pincode'], time() + ((86400 * 2) * 30), "/", "",  0);

					/*Referral Code*/ 
				if($this->session->userdata('user_id')!=''){
					$point = 0;
					$point = $this->cartnew_model->get_refer_point();
					if($point == 1 && $p_price >= MIN_CART_VALUE){ 
						setcookie('is_redeem_point', '1', time() + ((86400 * 2) * 30), "/", "",  0);
						$data['referral_point'] = POINT_VAlUE;
					}else{
						setcookie('is_redeem_point', '0', time() + ((86400 * 2) * 30), "/", "",  0);
					}
				}else{
						setcookie('is_redeem_point', '0', time() + ((86400 * 2) * 30), "/", "",  0);
					}


				$this->load->view('seventeen/common/header_view');
				$this->load->view('seventeen/cart/cart',$data);
				$this->load->view('seventeen/common/footer_view');
		}
		
				/*$this->load->view('seventeen/common/header_view');
				$this->load->view('seventeen/cart/cart',$data);
				$this->load->view('seventeen/common/footer_view');*/
		
		

	}

	public function scremovecart(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$id = $this->input->post('id');
			$this->cartnew_model->scremovecart($id);

			/* MailChimp Cart Remove*/
			/*if(!empty($this->session->userdata('user_id')))
	      	{ 
         		$this->curl->simple_post(BACKENDURL.'/Mailchimp/deleteCartLine/'.$this->session->userdata('user_id').'/'.$this->session->userdata('user_id').'/'.$id, false, array(CURLOPT_USERAGENT => true));   
         	}*/
         	/* End MailChimp Cart Remove*/

			$data['referral_point']=0;
			$data['cart_info'] = $this->cartnew_model->get_cart();
			$data['pincode'] = @$_COOKIE['stylecracker_shipping_pincode'];

			$p_price = 0;  $data['referral_point']=0;
			if(!empty($data['cart_info'])){
				foreach($data['cart_info'] as $val){
					$p_price = $p_price + $val['discount_price'];
				}
			}

			if(isset($_COOKIE['discountcoupon_stylecracker']) && $_COOKIE['discountcoupon_stylecracker']!=''){ 
				$data['coupon_code_msg'] = $this->cartnew_model->check_coupon_exist(strtolower($_COOKIE['discountcoupon_stylecracker']));
				$p_price = $p_price-$data['coupon_code_msg']['coupon_discount_amount'];
			}
			/*Referral Code*/

			if($this->session->userdata('user_id')!=''){ 
				$point = 0;
				$point = $this->cartnew_model->get_refer_point();
				if($point == 1 && $p_price>=MIN_CART_VALUE){ 
					setcookie('is_redeem_point', '1', time() + ((86400 * 2) * 30), "/", "",  0);
					$data['referral_point'] = POINT_VAlUE;
				}else{
					setcookie('is_redeem_point', '0', time() + ((86400 * 2) * 30), "/", "",  0);
				}
			}else{
				setcookie('is_redeem_point', '0', time() + ((86400 * 2) * 30), "/", "",  0);
				}

			if($this->input->post('page-type')=='cart')
			{	
				$this->load->view('seventeen/cart/stylecart_productview',$data);
			}else if($this->input->post('page-type')=='scboxcart')
			{	
				$this->load->view('seventeen/cart/scboxcart_productview',$data);
			}else
			{ 
				$this->load->view('seventeen/cart/cartproduct_view',$data);
			}

		}
	}

	public function remore_redeem(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			
			$data['referral_point']=0;
			$data['cart_info'] = $this->cartnew_model->get_cart();
			$data['pincode'] = '';
			$page_type = $this->input->post('page_type');
			
					$p_price = 0;  $data['referral_point']=0;
			if(!empty($data['cart_info'])){
				foreach($data['cart_info'] as $val){
					$p_price = $p_price + $val['discount_price'];
				}
			}

			if(isset($_COOKIE['discountcoupon_stylecracker']) && $_COOKIE['discountcoupon_stylecracker']!=''){ 
				$data['coupon_code_msg'] = $this->cartnew_model->check_coupon_exist(strtolower($_COOKIE['discountcoupon_stylecracker']));
				$p_price = $p_price-$data['coupon_code_msg']['coupon_discount_amount'];
			}
					/*Referral Code*/
			

		if($this->session->userdata('user_id')!=''){ 
			$point = 0;
			$point = $this->cartnew_model->get_refer_point();
			if($point == 1 && $p_price>=MIN_CART_VALUE){ 
				//setcookie('is_redeem_point', '1', time() + ((86400 * 2) * 30), "/", "",  0);
				$data['referral_point'] = POINT_VAlUE;
			}
			}
			if($page_type=='scboxcart')
			{
				$this->load->view('seventeen/cart/scboxcart_productview',$data);
			}else
			{
				//$this->load->view('seventeen/cart/cartproduct_view',$data);
				$this->load->view('seventeen/cart/stylecart_productview',$data);
			}
			

		}
	}

	public function updatecart(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$item_count = $this->input->post('item_count');
			$sccoupon_code = trim($this->input->post('sccoupon_code'));
			$data['referral_point']=0;
			if($item_count > 0){ 
				for($i = 1; $i <= $item_count; $i++){
					$scitem = $this->input->post('scitem'.$i);
					if($scitem > 0){
						$quantity = $this->input->post('quantity'.$scitem);
						$scsize = $this->input->post('scsize'.$scitem);

						$this->cartnew_model->updatecart($scitem,$quantity,$scsize);

						/*Mailchimp call For AddToCart*/
						 /*if(!empty($this->session->userdata('user_id')))
					      {  
					        //$mc_cartid = $this->Cart_model->get_cartid($uniquecookie,$productid,$userid,$productsize,$agent,$platform_info,$lookid);  

					         $this->curl->simple_post(BACKENDURL.'/Mailchimp/mailchimp_update_cart/'.$scitem.'/'.$this->session->userdata('user_id'), false, array(CURLOPT_USERAGENT => true));     
					      }*/
					    /*End Mailchimp call For AddToCart*/
					}
				}
				
				if($sccoupon_code!=''){ 
					$data['coupon_code_msg'] = $this->cartnew_model->check_coupon_exist(strtolower($sccoupon_code));
				}						
			}
			$data['pincode'] = $this->input->post('pincode');

			$data['cart_info'] = $this->cartnew_model->get_cart();

			/*Referral Code*/

			$p_price = 0;  $data['referral_point']=0;
			if(!empty($data['cart_info'])){
				foreach($data['cart_info'] as $val){
					$p_price = $p_price + $val['discount_price'];
				}
			}

			if(isset($_COOKIE['discountcoupon_stylecracker']) && $_COOKIE['discountcoupon_stylecracker']!=''){ 
				$data['coupon_code_msg'] = $this->cartnew_model->check_coupon_exist(strtolower($_COOKIE['discountcoupon_stylecracker']));
				$p_price = $p_price-$data['coupon_code_msg']['coupon_discount_amount'];
			}
			
			if($this->session->userdata('user_id')!=''){ 
				$point = 0;
				$point = $this->cartnew_model->get_refer_point();
				if($point == 1 && $p_price>=MIN_CART_VALUE){ 
					setcookie('is_redeem_point', '1', time() + ((86400 * 2) * 30), "/", "",  0);
					$data['referral_point'] = POINT_VAlUE;
				}else{
					setcookie('is_redeem_point', '0', time() + ((86400 * 2) * 30), "/", "",  0);
				}
			}else{
				setcookie('is_redeem_point', '0', time() + ((86400 * 2) * 30), "/", "",  0);
				}
			//echo '<pre>';print_r($data);exit;				
			if($this->input->post('page-type')=='cart')
			{	
				$this->load->view('seventeen/cart/stylecart_productview',$data);
			}else
			{ 
				$this->load->view('seventeen/cart/cartproduct_view',$data);
			}
		}
	}

	public function checkout()
	{

		$data = array(); $p_price = 0;
		$data['pincode'] = '';$data['referral_point']=0;
		$data['cart_info'] = $this->cartnew_model->get_cart();
		if(!empty($data['cart_info'])){
			foreach($data['cart_info'] as $val){
				$p_price = $p_price + $val['discount_price'];
			}
		}
		$data['product_total_for_msg'] = $p_price;
		if(empty($data['cart_info'])){ redirect('stylecart'); }
		if(!$this->session->userdata('user_id')){
			setcookie('current_url', base_url().'checkout', time() + ((86400 * 2) * 30), "/", "",  0);
			setcookie('is_redirect', 1, time() + ((86400 * 2) * 30), "/", "",  0);
		}else{
			setcookie('current_url', '', time() + ((86400 * 2) * 30), "/", "",  0);
			setcookie('is_redirect', 1, time() + ((86400 * 2) * 30), "/", "",  0);
		}
		if(isset($_COOKIE['discountcoupon_stylecracker']) && $_COOKIE['discountcoupon_stylecracker']!=''){ 
			$data['coupon_code_msg'] = $this->cartnew_model->check_coupon_exist(strtolower($_COOKIE['discountcoupon_stylecracker']));
			$p_price = $p_price-$data['coupon_code_msg']['coupon_discount_amount'];
		}

		/*Referral Code*/
		if($this->session->userdata('user_id')!=''){ 
			$point = 0;
			$point = $this->cartnew_model->get_refer_point();
			if($point == 1 && $p_price >= MIN_CART_VALUE){ 
				setcookie('is_redeem_point', '1', time() + ((86400 * 2) * 30), "/", "",  0);
				$data['referral_point'] = POINT_VAlUE;
			}else{
				setcookie('is_redeem_point', '0', time() + ((86400 * 2) * 30), "/", "",  0);
			}
		}else{
			setcookie('is_redeem_point', '0', time() + ((86400 * 2) * 30), "/", "",  0);
			}
		//echo '<pre>';print_r($data);exit;
		$this->load->view('seventeen/common/header_view');
		$this->load->view('seventeen/cart/checkout',$data);
		$this->load->view('seventeen/common/footer_view');
	}

	public function check_email_exist(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$cartemail = $this->input->post('cartemail');
			if (!filter_var($cartemail, FILTER_VALIDATE_EMAIL) === false) {
				$res = $this->cartnew_model->check_email_exist($cartemail);
				/* 1 - Exist 0 - Not Exist */
				if($res == 1){
					echo 1;exit;
				}else{
					echo 0;exit;
				}
				
			}else{
				echo 'Enter valid email address';exit;
			}
		}
	}

	function get_pincode_info(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$billing_pincode_no = $this->input->post('billing_pincode_no');
			echo $this->cartnew_model->get_pincode_info($billing_pincode_no);
		}
	}

	function apply_coupon_code(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$data = array();			
			$sccoupon_code = trim($this->input->post('sccoupon_code'));
			$sc_binno = trim($this->input->post('sc_binno'));
			$page_type = trim($this->input->post('page_type'));
			$is_input = trim($this->input->post('type'));
			if($is_input == 'apply'){
				$check_is_special_code = $this->Mcart_model_android->check_is_special_code($sccoupon_code);
				if($check_is_special_code == '0'){
					//setcookie('discountcoupon_stylecracker', '', -1, "/");  
		           	$data['coupon_code_msg']['msg'] = 'Enter valid coupon code';
		            $data['coupon_code_msg']['msg_type'] = '2';
		            $data['coupon_code_msg']['coupon_code'] = '';
		            $data['coupon_code_msg']['coupon_discount_amount'] = 0;
				}else
				{
					$data['coupon_code_msg'] = $this->cartnew_model->check_coupon_exist(strtolower($sccoupon_code),$sc_binno,$page_type);
				}
			}else
			{
				$data['coupon_code_msg'] = $this->cartnew_model->check_coupon_exist(strtolower($sccoupon_code),$sc_binno,$page_type);
			}
			
			echo json_encode($data['coupon_code_msg']);exit;
		}
	}

	function order_summary(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$data = array(); $data['referral_point']=0;	$bin_no = '';$online_payment_discount=0;
			$p_price = 0;  $data['referral_point']=0;		
			$page_type = trim($this->input->post('page_type'));

			if($page_type=='scboxcart')
			{
				$data['cart_info'] = $this->cartnew_model->get_scboxcart();
				$data['object_id'] = trim($this->input->post('object_id'));
			}else
			{
				$data['cart_info'] = $this->cartnew_model->get_cart();
			}
			$data['pincode'] = @$this->input->post('pincode');
			$data['sc_cart_pay_mode'] = @$this->input->post('sc_cart_pay_mode');
			$sccoupon_code = trim(@$this->input->post('sccoupon_code'));
			

					$p_price = 0;  $data['referral_point']=0;
			if(!empty($data['cart_info'])){
				foreach($data['cart_info'] as $val){
					$p_price = $p_price + $val['discount_price'];
				}
			}

			if(isset($_COOKIE['discountcoupon_stylecracker']) && $_COOKIE['discountcoupon_stylecracker']!=''){ 
				$data['coupon_code_msg'] = $this->cartnew_model->check_coupon_exist(strtolower($_COOKIE['discountcoupon_stylecracker']),$bin_no,$page_type);
				$p_price = $p_price-$data['coupon_code_msg']['coupon_discount_amount'];
			}
					/*Referral Code*/			
					if($page_type!='scboxcart')
					{
						if($this->session->userdata('user_id')!=''){ 
							$point = 0;
							$point = $this->cartnew_model->get_refer_point();
							if($point == 1 && $p_price>=MIN_CART_VALUE){ 
								setcookie('is_redeem_point', '1', time() + ((86400 * 2) * 30), "/", "",  0);
								$data['referral_point'] = POINT_VAlUE;
							}
						}
					}

					if(trim($data['sc_cart_pay_mode'])=='card')
					{
						$online_payment_discount = ($p_price*ONLINE_PAY_DISCOUNT)/100;
						$data['online_payment_discount'] = $online_payment_discount;
					}else
					{
						$data['online_payment_discount'] = 0;
					}
			setcookie('stylecracker_shipping_pincode', $data['pincode'], time() + ((86400 * 2) * 30), "/", "",  0);
			
			if($page_type == 'cart')
			{				
				$this->load->view('seventeen/cart/stylecart_productview',$data);				
				
			}else if($page_type=='scboxcart')
			{				
				$this->load->view('seventeen/cart/scboxcart_productview',$data);
			}else
			{
				$this->load->view('seventeen/cart/desktop_order_summary',$data);
			}			
			
		}
	}

	/*function order_summary_mobile(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$data = array(); $data['referral_point']=0;
			$data['cart_info'] = $this->cartnew_model->get_cart();
			$data['pincode'] = @$this->input->post('pincode');
			$data['sc_cart_pay_mode'] = @$this->input->post('sc_cart_pay_mode');
			$sccoupon_code = trim($this->input->post('sccoupon_code'));

			$p_price = 0;  $data['referral_point']=0;
			if(!empty($data['cart_info'])){
				foreach($data['cart_info'] as $val){
					$p_price = $p_price + $val['discount_price'];
				}
			}

			if(isset($_COOKIE['discountcoupon_stylecracker']) && $_COOKIE['discountcoupon_stylecracker']!=''){ 
				$data['coupon_code_msg'] = $this->cartnew_model->check_coupon_exist(strtolower($_COOKIE['discountcoupon_stylecracker']));
				$p_price = $p_price-$data['coupon_code_msg']['coupon_discount_amount'];
			}
			//Referral Code

			

						if($this->session->userdata('user_id')!=''){ 
							$point = 0;
							$point = $this->cartnew_model->get_refer_point();
							if($point == 1 && $p_price >= MIN_CART_VALUE){ 
								setcookie('is_redeem_point', '1', time() + ((86400 * 2) * 30), "/", "",  0);
								$data['referral_point'] = POINT_VAlUE;
							}
						}
			setcookie('stylecracker_shipping_pincode', $data['pincode'], time() + ((86400 * 2) * 30), "/", "",  0);
			$this->load->view('cart/mobile_order_summary',$data);
		}
	}*/

	function remore_redeem_checkout(){

	}

	function place_order(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$data = array();
			$billing_first_name=$this->input->post('billing_first_name'); 
			$billing_last_name=$this->input->post('billing_last_name'); 
			$billing_mobile_no=$this->input->post('billing_mobile_no'); 
			$billing_pincode_no=$this->input->post('billing_pincode_no'); 
			$billing_address=$this->input->post('billing_address'); 
			$billing_city=$this->input->post('billing_city'); 
			$billing_state=$this->input->post('billing_state'); 
			$shipping_first_name=$this->input->post('shipping_first_name'); 
			$shipping_last_name=$this->input->post('shipping_last_name'); 
			$shipping_mobile_no=$this->input->post('shipping_mobile_no'); 
			$shipping_pincode_no=$this->input->post('shipping_pincode_no'); 
			$shipping_address=$this->input->post('shipping_address'); 
			$shipping_city=$this->input->post('shipping_city'); 
			$shipping_state=$this->input->post('shipping_state'); 
			$cart_email=$this->input->post('cart_email'); 
			$pay_mod=$this->input->post('pay_mod'); 

			if($billing_first_name!='' && $billing_last_name!='' && $billing_mobile_no!='' && $billing_pincode_no!='' && $billing_address!='' && $billing_city!='' && $billing_state!='' && $shipping_first_name!='' && $shipping_last_name!='' && $shipping_mobile_no!='' && $shipping_pincode_no!='' && $shipping_address!='' && $shipping_city!='' && $shipping_state!='' && $cart_email!='' && $pay_mod!=''){

				$data['user_info'] = array('billing_first_name'=>$billing_first_name,'billing_last_name'=>$billing_last_name,'billing_mobile_no'=>$billing_mobile_no,'billing_pincode_no'=>$billing_pincode_no,'billing_address'=>$billing_address,'billing_city'=>$billing_city,'billing_state'=>$billing_state,'shipping_first_name'=>$shipping_first_name,'shipping_last_name'=>$shipping_last_name,'shipping_mobile_no'=>$shipping_mobile_no,'shipping_pincode_no'=>$shipping_pincode_no,'shipping_address'=>$shipping_address,'shipping_city'=>$shipping_city,'shipping_state'=>$shipping_state,'cart_email'=>$cart_email,'pay_mod'=>$pay_mod,'existing_shipping_add'=>0);

				$data['cart_info'] = $this->cartnew_model->get_cart();
				$data['coupon_code'] = @$_COOKIE['discountcoupon_stylecracker'];

				$p_price = 0;  $data['referral_point']=0;
			if(!empty($data['cart_info'])){
				foreach($data['cart_info'] as $val){
					$p_price = $p_price + $val['discount_price'];
				}
			}

				if(isset($_COOKIE['discountcoupon_stylecracker']) && $_COOKIE['discountcoupon_stylecracker']!=''){ 
					$data['coupon_code_msg'] = $this->cartnew_model->check_coupon_exist(strtolower($_COOKIE['discountcoupon_stylecracker']));
					$p_price = $p_price-$data['coupon_code_msg']['coupon_discount_amount'];
				}

				

				$order_unique_no = $this->cartnew_model->stylecracker_place_order($data);
				if($order_unique_no > 0){
					//$user_id = $this->session->userdata('user_id');

					$points = $this->cartnew_model->get_refer_point(); 
			                    if($points == 1 && $p_price >=MIN_CART_VALUE){
			                        if(isset($_COOKIE['is_redeem_point']) == true && $_COOKIE['is_redeem_point'] == 1){
			                            $this->cartnew_model->redeem_points($order_unique_no);
			                        }
			                    }

					$this->cartnew_model->update_order_id_in_cart($order_unique_no);
					$data['brand_price'] = $this->cartnew_model->su_get_order_price_info($order_unique_no);
	    			$user_id = $data['brand_price'][0]['user_id'];
	    			$this->updateCouponInc($order_unique_no);
					//$this->orderPlaceMessage_sc($user_id,$order_unique_no,$billing_first_name,$billing_last_name);
	    			//$this->orderPlaceMessage($user_id,$order_unique_no,$billing_first_name,$billing_last_name);
        			//$this->orderPlaceMessagetoBrand($user_id,$order_unique_no,$billing_first_name,$billing_last_name);
					echo $order_unique_no;exit;
				}

			}
		}
	}

	function updateCouponInc($order_unique_no){
		$order_unique_no = $this->cartnew_model->updateCouponInc($order_unique_no);
	}

	function escapeJsonString($value) { # list from www.json.org: (\b backspace, \f formfeed)
	    $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
	    $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
	    $result = str_replace($escapers, $replacements, $value);
	    return $result;
	}

	function place_online_order(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$data = array(); $offerkey = ''; $o_price = 0; $o_cart_total = 0;
			$offer_codes = unserialize(OFFER_CODES);
			$scboxoffer_codes = unserialize(SCBOXOFFER_CODES);

			$billing_first_name=$this->input->post('billing_first_name'); 
			$billing_last_name=$this->input->post('billing_last_name'); 
			$billing_mobile_no=$this->input->post('billing_mobile_no'); 
			$billing_pincode_no=$this->input->post('billing_pincode_no'); 
			if(@$this->input->post('billing_address')!=''){
				$billing_address=$this->escapeJsonString($this->input->post('billing_address')); 
			}else{
				$billing_address='';
			}
			if($billing_address!=''){
     				$billing_address = @stripslashes(strip_tags($billing_address));
     			}
			$billing_city=$this->input->post('billing_city'); 
			$billing_state=$this->input->post('billing_state'); 
			$shipping_first_name=$this->input->post('shipping_first_name'); 
			$shipping_last_name=$this->input->post('shipping_last_name'); 
			$shipping_mobile_no=$this->input->post('shipping_mobile_no'); 
			$shipping_pincode_no=$this->input->post('shipping_pincode_no'); 
			$shipping_address=$this->input->post('shipping_address'); 
			$shipping_city=$this->input->post('shipping_city'); 
			$shipping_state=$this->input->post('shipping_state'); 
			$cart_email=$this->input->post('cart_email'); 
			$pay_mod=$this->input->post('pay_mod'); 
			$cart_total=$this->input->post('cart_total'); 
			if(isset($_COOKIE['SCUniqueID'])){
      			$SCUniqueID =  $_COOKIE['SCUniqueID'];
      		}

			if($billing_first_name!='' && $billing_last_name!='' && $billing_mobile_no!='' && $billing_pincode_no!='' && $billing_address!='' && $billing_city!='' && $billing_state!='' && $shipping_first_name!='' && $shipping_last_name!='' && $shipping_mobile_no!='' && $shipping_pincode_no!='' && $shipping_address!='' && $shipping_city!='' && $shipping_state!='' && $cart_email!='' && $pay_mod!='' && $cart_total!=''){


				$this->cartnew_model->failed_order($shipping_first_name,$shipping_last_name,$cart_email,$shipping_address,$SCUniqueID,$shipping_mobile_no,$pay_mod,$shipping_pincode_no,$shipping_city,$country=null,$uc_state2=null);

				$data['user_info'] = array('billing_first_name'=>$billing_first_name,'billing_last_name'=>$billing_last_name,'billing_mobile_no'=>$billing_mobile_no,'billing_pincode_no'=>$billing_pincode_no,'billing_address'=>$billing_address,'billing_city'=>$billing_city,'billing_state'=>$billing_state,'shipping_first_name'=>$shipping_first_name,'shipping_last_name'=>$shipping_last_name,'shipping_mobile_no'=>$shipping_mobile_no,'shipping_pincode_no'=>$shipping_pincode_no,'shipping_address'=>$shipping_address,'shipping_city'=>$shipping_city,'shipping_state'=>$shipping_state,'cart_email'=>$cart_email,'pay_mod'=>$pay_mod,'existing_shipping_add'=>0);

				$data['cart_info'] = $this->cartnew_model->get_cart();
				$data['coupon_code'] = @$_COOKIE['discountcoupon_stylecracker'];

					$p_price = 0;  $data['referral_point']=0;
			if(!empty($data['cart_info'])){
				foreach($data['cart_info'] as $val){
					$p_price = $p_price + $val['discount_price'];

				}
			}
			$o_price = $p_price;

				if(isset($_COOKIE['discountcoupon_stylecracker']) && $_COOKIE['discountcoupon_stylecracker']!=''){ 
					$data['coupon_code_msg'] = $this->cartnew_model->check_coupon_exist(strtolower($_COOKIE['discountcoupon_stylecracker']));
					$p_price = $p_price-$data['coupon_code_msg']['coupon_discount_amount'];
				}
				
				$order_unique_no = $this->cartnew_model->stylecracker_place_order($data);

				//$cart_total = $this->cartnew_model->cart_total($order_unique_no);

			
					$redeem_points = 0; 
							$points = $this->cartnew_model->get_refer_point(); 
			                    if($points == 1 && $p_price>=MIN_CART_VALUE){
			                        if(isset($_COOKIE['is_redeem_point']) == true && $_COOKIE['is_redeem_point'] == 1){
			                            $redeem_points = POINT_VAlUE;
			                        }
			                    }
			        $o_cart_total = $this->cartnew_model->cart_total($order_unique_no);
			      	$cart_total = $this->cartnew_model->cart_total($order_unique_no)-$redeem_points;
				$MERCHANT_KEY = MERCHANT_KEY;
      			$SALT = PAYUSALT;
      			$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);

      			// Added for Payu Offer
      			if(isset($_COOKIE['discountcoupon_stylecracker']) && in_array(@$_COOKIE['discountcoupon_stylecracker'], $offer_codes)){ 
      				//$offerkey = PAYUOFFER_KEY;
      				
      				if(in_array(@$_COOKIE['discountcoupon_stylecracker'], $scboxoffer_codes))
      				{
      					setcookie('discountcoupon_stylecracker', '', -1, "/"); 
      					$offerkey = '';
      				}else
      				{
      					$offerkey_couponcode = strtoupper(@$_COOKIE['discountcoupon_stylecracker']);
      					$offerkeyconst = $offerkey_couponcode.'_OFFER_KEY'; 
      					if($offerkeyconst=='BMS500_OFFER_KEY')
      					{
      						$offerkey = BMS500_OFFER_KEY; 
      					}else if($offerkeyconst=='YES300_OFFER_KEY')
      					{
      						$offerkey = YES300_OFFER_KEY; 
      					}
      					$cart_total = $o_cart_total; 
      				}
      				
      			}   

      			//End of Added for Payu Offer
      			
      			//Pay u biz hash key
     			 $hash = $MERCHANT_KEY.'|'.$txnid.'|'.$cart_total.'|{"paymentParts":[{"name":"splitID1","value":"6","merchantId":"396446","description":"test description","commission":"2"}]}|'.$billing_first_name.''.$billing_last_name.'|'.$cart_email.'|'.$SCUniqueID.'|'.$billing_address.'|'.$billing_pincode_no.'|'.$billing_city.'|'.$order_unique_no.'||||||'.$SALT;
     
				
      			$hash = strtolower(hash('sha512', $hash));
      			
      			echo '{"order" : "online","chk_pay_mode":"card_pay","payu_hash":"'.$hash.'","txnid":"'.$txnid.'","amount":"'.$cart_total.'","email":"'.$cart_email.'","firstname":"'.$billing_first_name.$billing_last_name.'","phoneno":"'.$billing_mobile_no.'","address_sc":"'.$billing_address.'","state":"'.$order_unique_no.'","offerkey":"'.$offerkey.'"}';exit;

			}
		}
	}

	function online_order_success(){
		$data = array();
	    if(@$_POST['status'] == 'failure'){
	    	if($this->cartnew_model->delete_unsuccessfully_order($_POST['udf1'])){
	      		redirect('checkout?error='.$_POST['field9']);
	      	}
	    }

	    if(@$_POST['status'] == 'success'){
	    	$this->cartnew_model->change_order_status($_POST);
	    		$p_price = 0;
	    		$points = $this->cartnew_model->get_refer_point(); 
	    		$p_price = $this->cartnew_model->get_order_price_ref($_POST['udf5']);
	    		$discountsd = ''; $discountsdamt = 0;
	    		$discountsd = @$_COOKIE['discountcoupon_stylecracker'];

	    		$data['brand_price'] = $this->cartnew_model->su_get_order_price_info($_POST['udf5']);
	    		$user_id = $data['brand_price'][0]['user_id'];

	    		$discountsdamt = $this->cartnew_model->check_coupon_after_order_place($discountsd,trim($this->getEmailAddress($user_id)),$_POST['udf5']);
	    		$p_price = $p_price - $discountsdamt['coupon_discount_amount'];
			    if($points == 1 && $p_price>=MIN_CART_VALUE){
			        if(isset($_COOKIE['is_redeem_point']) == true && $_COOKIE['is_redeem_point'] == 1){
			            $this->cartnew_model->redeem_points($_POST['udf5']);
			        }
			    } 

	    	setcookie('discountcoupon_stylecracker','', -1, "/", "",  0);
	    	setcookie('SCUniqueID','', -1, "/", "",  0);
	    	//$user_id = $this->session->userdata('user_id');
	    	
	    	$this->updateCouponInc($_POST['udf5']);
	    	$this->orderPlaceMessage($user_id,$_POST['udf5'],$data['brand_price'][0]['first_name'],$data['brand_price'][0]['last_name']);
        	$this->orderPlaceMessagetoBrand($user_id,$_POST['udf5'],$data['brand_price'][0]['first_name'],$data['brand_price'][0]['last_name']);


	    	redirect('stylecart/ordersuccessOrder/'.$_POST['udf5']);

	    }else{
			if($this->cartnew_model->delete_unsuccessfully_order($_POST['udf1'])){
	      		redirect('checkout?error='.$_POST['field9']);
	      	}
      	}
	}

	function ordersuccessOrder(){
	    $order_no = $this->uri->segment(3);
	    setcookie('cart_email','', -1, "/", "",  0);
	    if($order_no!=''){
	      $totalProductPrice =0;
	      $referral_point = 0;

	       /* MailChimp Remove From Cart*/
	      $mc_user = explode('_',$order_no);	     
	      if(@$mc_user[0]!='')
	      {
	      	$this->curl->simple_post(BACKENDURL.'/Mailchimp/delete_mccart/'.$mc_user[0].'/'.$mc_user[0], false, array(CURLOPT_USERAGENT => true));
	      }
	      /* MailChimp Remove From Cart End*/
	      /* MailChimp Add Order */
	       if(@$order_no!='')
	      {
	      	$this->curl->simple_post(BACKENDURL.'/Mailchimp/send_orderinfo/'.$order_no, false, array(CURLOPT_USERAGENT => true));
	      }
	      if(@$mc_user[0]!='')
	      {
	      	 $userEmail = $this->cartnew_model->getUserEmail($mc_user[0]);
	      	 if($userEmail!='')
	      	 {
	      	 	$this->cartnew_model->update_couponcode($userEmail,'SCSHOPMORE10');
	      	 }
	      }
	      /* MailChimp Add Order End*/
	      
		  $referral_point = $this->cartnew_model->get_refre_amt($order_no);
	      $data['res'] = $this->cartnew_model->su_get_order_info($order_no);
	      $data['brand_price'] = $this->cartnew_model->su_get_order_price_info($order_no);
	 	
	      $data['order_display_no'] = @$data['brand_price'][0]['order_display_no'];
	      foreach($data['res'] as $val){
	         $totalProductPrice = $totalProductPrice+$val['product_price'];         
	      }

	      $coupon_result = $this->calculateCouponAmount($data['res'],$totalProductPrice);
		    
	      $data['coupon_discount']= @$coupon_result['coupon_discount'];
	      $data['coupon_code'] = @$coupon_result['coupon_code'];

	      if($data['brand_price'][0]['confirm_mobile_no']==0 && $data['brand_price'][0]['pay_mode'] == 1){
	      	$mobile_no = '91'.$data['brand_price'][0]['mobile_no'];


	      	/*$string = '0123456789';
       		$string_shuffled = str_shuffle($string);
       		$otp_no = substr($string_shuffled, 1, 5);
			echo $otp_no;*/
   			/* $otp_no = rand(5,100000);*/
    		//$otp_no = 99999;
    		//$this->cartnew_model->scgenrate_otp($mobile_no,$otp_no);    		
			
	      }

	      /*$this->load->view('common/header_view');
	      $this->load->view('cart/orderSuccesfull',$data);
	      $this->load->view('common/footer_view');*/

	      $data['mobile_no'] = @$mobile_no;
	      $data['type'] = 'check_otp';
	      $data['mc_user'] = $mc_user[0];
	      
	     
	     if(@$data['brand_price'][0]['pay_mode']==1)
	     {
	     	$otp_result = $this->cartnew_model->check_mobile($data);
			
		      if($otp_result=='registered' || @$_GET['success']==1)
		      {
		      	$this->load->view('seventeen/common/header_view');
		      	$this->load->view('seventeen/cart/orderSuccessfull',$data);
		     	$this->load->view('seventeen/common/footer_view');	      	
		      }else
		      { 
				$otp_no = $otp_result;
				$message = 'Thank+you+for+choosing+STYCKR.+Your+OTP+for+cash+on+delivery+is+'.$otp_no.'.+Please+use+this+to+confirm+your+order.+Stay+Stylish!';
				file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile='.$mobile_no.'&message='.$message);
		     	$this->load->view('seventeen/common/header_view');
		      	$this->load->view('seventeen/cart/check_cartotp',$data);
		      	$this->load->view('seventeen/common/footer_view');
		      }
		 }else
		 {
		 	$this->load->view('seventeen/common/header_view');
	      	$this->load->view('seventeen/cart/orderSuccessfull',$data);
	     	$this->load->view('seventeen/common/footer_view');	
		 }	      

	    }else{
	      redirect('/');
	    }
  	}

	
  	function getEmailAddress($user_id){
	    $res = $this->cartnew_model->get_user_info($user_id);
     	if(!empty($res)){
        	return $res[0]['email'];
      	}else{ return ''; }
  	}

    function calculateCouponAmount($cartproduct=array(),$totalProductPrice=null)
  	{    
      if(!empty($cartproduct))
      {
        $total_product_price = 0; $coupon_product_price = 0;
        foreach($cartproduct as $val)
        {
            if(@$val['discount_percent']!='' || @$val['discount_percent']!=0)
            {
               $price = $val['discount_price'];            
            }else
            {
               $price = $val['product_price'];            
            }
            $total_product_price = $total_product_price+$price;
        }
        foreach($cartproduct as $val)
        {

          if(@$val['discount_percent']!='' || @$val['discount_percent']!=0)
          {
             $price = $val['discount_price'];            
          }else
          {
             $price = $val['product_price'];            
          }

           $coupon_info = $this->cartnew_model->getUserCoupon($val['coupon_code'],$val['brand_id']);
            $coupon_brand = $coupon_info['brand_id'];
            $coupon_products = $coupon_info['coupon_products'];
            if($coupon_products!='' || $coupon_products!=0)
            {
              $coupon_products_arr =explode(',',$coupon_products);
            }
            $coupon_min_spend = $coupon_info['coupon_min_spend'];
            $coupon_max_spend = $coupon_info['coupon_max_spend'];
            $coupon_discount_type = $coupon_info['coupon_discount_type']; 
            $coupon_stylecracker =  $coupon_info['stylecrackerDiscount'];
            $individual_use_only =  $coupon_info['individual_use_only']; 

          if(isset($val['coupon_code']) && $val['coupon_code']!='')
          {
           
            /*$data['coupon_discount'] = $coupon_info['coupon_amount'];
            $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];*/
          
            $data['coupon_code'] = $val['coupon_code'];
            //$coupon_product_price = 0;

              if($coupon_brand==$val['brand_id'] && $coupon_brand!=0)
              {        
                if($coupon_products!=''&& $coupon_products!=0)
                {
                   //coupon_discount_type =3 (Product discount)
                  if($coupon_discount_type==3)
                  {
                    if(in_array($val['product_id'], $coupon_products_arr))
                    {              
                      if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                      {
                        $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                        $data['coupon_amount'] =  $coupon_info['coupon_amount'];                      
                        $data['coupon_discount'] = $coupon_info['coupon_amount'];  
                        $data['coupon_code'] = $val['coupon_code'];                   
                      } 
                    }
                  } 
                   //coupon_discount_type =4 (Product % discount)
                  if($coupon_discount_type==4)
                  {
                    if(in_array($val['product_id'], $coupon_products_arr))
                    {              
                      if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                      {
                        $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                        $data['coupon_amount'] =  $coupon_info['coupon_amount'];  
                        $coupon_percent = $coupon_info['coupon_amount'];
                        $data['coupon_discount'] = $price*($coupon_percent/100); 
                        $data['coupon_code'] = $val['coupon_code'];                      
                      } 
                    }
                  }                 

                }else
                { 
                   //coupon_discount_type =1 (cart discount)
                  if($coupon_discount_type==1)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_info['coupon_amount']; 
                      $data['coupon_code'] = $val['coupon_code'];                   
                    }
                  }
                   //coupon_discount_type =2 (cart % discount)
                  if($coupon_discount_type==2)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];  
                      $coupon_percent = $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];                      
                    }
                  }
                }          
              }

             /*  if($coupon_brand==0 && strtolower($val['coupon_code'])=='sccart20')
              { 
                   $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $coupon_percent = $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];                      
                    }
              }*/
        
       if(($coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only!=1  )|| ( $coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only==1 ))        
              { 
                $coupon_product_price = $totalProductPrice;

                     //coupon_discount_type =1 (cart discount)
                  if($coupon_discount_type==1)
                  {
                    
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    { 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];            
                      $data['coupon_discount'] = $coupon_info['coupon_amount']; 
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                      $data['coupon_code'] = $val['coupon_code'];                   
                    }
                  }
                   //coupon_discount_type =2 (cart % discount)
                  if($coupon_discount_type==2)
                  {                   
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    { 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];                       
                      $coupon_percent = $coupon_info['coupon_amount']; 
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];  
                      $data['coupon_discount'] = $totalProductPrice*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];                      
                    }
                  }
              }
              

          }else
          {
          	if(!isset($data['coupon_code']))
          	{
	            $data['coupon_discount']=0;
	            $data['coupon_code'] = '';
	        }
          }         
        }      
      } 
      return $data;      
  }

  function getProductSku($productId,$productSize)
  {
    $product_sku = $this->cartnew_model->getProductSku($productId,$productSize);
    return $product_sku;
  }

  function delete_address(){
  	$id = $this->input->post('id');
  	$this->cartnew_model->delete_address($id);

  }

  function get_address(){
  	$id = $this->input->post('id');
  	echo $this->cartnew_model->get_address($id);exit;
  }

  function check_cart_availability(){
  		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$data = array();  $data['referral_point']=0;
			$data['cart_info'] = $this->cartnew_model->get_cart();
			$data['pincode'] = $this->input->post('pincode'); 
			$data['sc_cart_pay_mode'] = 'cod';

			$p_price = 0;  $data['referral_point']=0;
			if(!empty($data['cart_info'])){
				foreach($data['cart_info'] as $val){
					$p_price = $p_price + $val['discount_price'];
				}
			}

			if(isset($_COOKIE['discountcoupon_stylecracker']) && $_COOKIE['discountcoupon_stylecracker']!=''){ 
				$data['coupon_code_msg'] = $this->cartnew_model->check_coupon_exist(strtolower($_COOKIE['discountcoupon_stylecracker']));
				$p_price = $p_price-$data['coupon_code_msg']['coupon_discount_amount'];
			}
			setcookie('stylecracker_shipping_pincode', $data['pincode'], time() + ((86400 * 2) * 30), "/", "",  0);

				/*Referral Code*/

			
		if($this->session->userdata('user_id')!=''){ 
			$point = 0;
			$point = $this->cartnew_model->get_refer_point();
			if($point == 1 && $p_price>=MIN_CART_VALUE){ 
				setcookie('is_redeem_point', '1', time() + ((86400 * 2) * 30), "/", "",  0);
				$data['referral_point'] = POINT_VAlUE;
			}else{
				setcookie('is_redeem_point', '0', time() + ((86400 * 2) * 30), "/", "",  0);
			}
		}else{
			setcookie('is_redeem_point', '0', time() + ((86400 * 2) * 30), "/", "",  0);
			}

			$this->load->view('seventeen/cart/cartproduct_view',$data);
		}
  }

  function remove_products_otherthan_nonremoval(){
  		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$this->cartnew_model->deleteEntireCart();
		}
  }

  function sc_check_otp(){
  		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
		    $data['mobile_no'] = '91'.$this->input->post('mobile_no');
		    $data['otp_number'] = trim($this->input->post('otp_number'));
		    $data['oid'] = $this->input->post('oid');
		    $data['type'] = 'check_otp';
		    //$res = $this->cartnew_model->sc_check_otp($data['mobile_no'],$data['otp_number'],$data['oid']);
		  
		    if($data['mobile_no']!='' && $data['otp_number']!='')
		    {	
			    $res = $this->cartnew_model->check_mobile($data);
			   
			    if($res == 'registered' || ($data['otp_number']==$res) ){		    	

			    	$data['res'] = $this->cartnew_model->su_get_order_info($data['oid']);
		      		$data['brand_price'] = $this->cartnew_model->su_get_order_price_info($data['oid']);
		      		$user_id = $data['brand_price'][0]['user_id'];
			    	$this->orderPlaceMessage($user_id,$data['oid'],$data['brand_price'][0]['first_name'],$data['brand_price'][0]['last_name']);
	        		$this->orderPlaceMessagetoBrand($user_id,$data['oid'],$data['brand_price'][0]['first_name'],$data['brand_price'][0]['last_name']);
	        		echo 'success';exit;
			    }else
			    {
			    	echo 'error';exit;
			    }
			 }
		    //echo $res;
		}
  	}

  	function deleteEntireCart(){
  		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{

		}
  	}

  	function edit_users_address(){
  		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			
			$billing_address = $this->input->post('billing_address');
			$billing_city = $this->input->post('billing_city');
			$billing_first_name = $this->input->post('billing_first_name');
			$billing_last_name = $this->input->post('billing_last_name');
			$billing_mobile_no = $this->input->post('billing_mobile_no');
			$billing_pincode_no = $this->input->post('billing_pincode_no');
			$billing_state = $this->input->post('billing_state');
			$edit_address_id = $this->input->post('edit_address_id');

			$data = array( 'first_name' =>$billing_first_name,
					'last_name' =>$billing_last_name,
					'mobile_no' =>$billing_mobile_no,
					'shipping_address' => $billing_address,
					'pincode' =>$billing_pincode_no,
					'city_name' =>$billing_city,
					'state_name' =>$billing_state,
				);
			$this->cartnew_model->edit_users_address($data,$edit_address_id);

		}
  	}  
   

  	function orderPlaceMessage($user_id,$order_id,$first_name,$last_last){   

  		if($this->getEmailAddress($user_id)!=''){
	      $config['protocol'] = 'smtp';
	      $totalProductPrice=0;
	      $referral_point = 0;
		  $referral_point = $this->cartnew_model->get_refre_amt($order_id);

	//      $config['mailpath'] = '/usr/sbin/sendmail';
	      $config['charset'] = 'iso-8859-1';
	      $config['mailtype'] = 'html';
	      $config['wordwrap'] = TRUE;
	      $config['smtp_host'] = $this->config->item('smtp_host');
	      $config['smtp_user'] = $this->config->item('smtp_user');
	      $config['smtp_pass'] = $this->config->item('smtp_pass');
	      $config['smtp_port'] = $this->config->item('smtp_port');
			
	      $this->email->initialize($config);

		  	$data['orderUniqueNo'] = $order_id;
		    $data['Username'] = $first_name.' '.$last_last;
		  	$data['order_product_info'] = $this->cartnew_model->su_get_order_info($order_id);    
	      	$data['order_info'] = $this->cartnew_model->su_get_order_price_info_res($order_id);
	      	$data['shipping_address'] = $data['order_info'][0]['shipping_address'];
	      	$data['city_name'] = $data['order_info'][0]['city_name'];
	      	$data['state_name'] = $data['order_info'][0]['state_name'];
	      	$data['pincode'] = $data['order_info'][0]['pincode'];
	      	$data['country_name'] = $data['order_info'][0]['country_name'];
	      	$data['paymentmode'] = $data['order_info'][0]['pay_mode'];
	      	$data['referral_point'] = $referral_point;
	      	$data['order_display_no'] = $data['order_info'][0]['order_display_no'];
	      	$data['mobile_no'] = $data['order_info'][0]['mobile_no'];

	      	if($data['order_info'][0]['state_id'] == '36') { $data['state_name'] =  $data['order_info'][0]['it_state']; }

	      	$data['res'] =  $this->cartnew_model->su_get_order_info($order_id); 

	  		//$this->load->view('emails/order_successful_user_copy',$data);

		      $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Processing');

		       if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){        
			      $this->email->to($this->config->item('sc_test_emaild'));
			      $this->email->bcc($this->config->item('sc_testcc_emaild'));
			    }else{
			      $this->email->to($this->getEmailAddress($user_id));    
			      $this->email->cc('order@stylecracker.com'); 
			    }   
		     
		      $this->email->subject('StyleCracker: Order Processing');
		      //$this->load->view('emails/order_successful_user_copy',$data);
		      $message = $this->load->view('emails/order_successful_user_copy',$data,true);
		  	  $this->email->message($message);

		  	  /* Order Processing sms to the user */
		  	   $message = 'Thanks+for+choosing+StyleCracker!+We\'re+processing+your+order+number+'.$data['order_info'][0]['order_display_no'].'.+We\'ll+keep+you+updated. :)';
    		   file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=91'.$data['order_info'][0]['mobile_no'].'&message='.$message);

		  	  $this->email->send();

	  }
	  }

	  //function orderPlaceMessage_sc($user_id,$order_id,$first_name,$last_last){
	  function orderPlaceMessage_sc()
	  {
	  	$user_id='10668';
	  	//$order_id='10668_514_515';//product discount -scb100
  		$order_id='10668_1745';//sc1000 -stylecracker discount
  		//$order_id='10668_496';//normal order
  		//$order_id='10668_512_513';//brand discount
  		$first_name='sudha';
  		$last_last='S';

  		if($this->getEmailAddress($user_id)!=''){
	      $config['protocol'] = 'smtp';
	      $totalProductPrice=0;
	      $referral_point = 0;
		  $referral_point = $this->cartnew_model->get_refre_amt($order_id);


			//  $config['mailpath'] = '/usr/sbin/sendmail';
	      $config['charset'] = 'iso-8859-1';
	      $config['mailtype'] = 'html';
	      $config['wordwrap'] = TRUE;
	      $config['smtp_host'] = $this->config->item('smtp_host');
	      $config['smtp_user'] = $this->config->item('smtp_user');
	      $config['smtp_pass'] = $this->config->item('smtp_pass');
	      $config['smtp_port'] = $this->config->item('smtp_port');
			
	      $this->email->initialize($config);

		  	$data['orderUniqueNo'] = $order_id;
		    $data['Username'] = $first_name.' '.$last_last;
		  	$data['order_product_info'] = $this->cartnew_model->su_get_order_info($order_id);    
	      	$data['order_info'] = $this->cartnew_model->su_get_order_price_info_res($order_id);
	      	$data['shipping_address'] = $data['order_info'][0]['shipping_address'];
	      	$data['city_name'] = $data['order_info'][0]['city_name'];
	      	$data['state_name'] = $data['order_info'][0]['state_name'];
	      	$data['pincode'] = $data['order_info'][0]['pincode'];
	      	$data['country_name'] = $data['order_info'][0]['country_name'];
	      	$data['paymentmode'] = $data['order_info'][0]['pay_mode'];
	      	$data['referral_point'] = $referral_point;
	      	$data['order_display_no'] = $data['order_info'][0]['order_display_no'];
	      	$data['mobile_no'] = $data['order_info'][0]['mobile_no'];

	      	if($data['order_info'][0]['state_id'] == '36') { $data['state_name'] =  $data['order_info'][0]['it_state']; }

	      	//$data['res'] =  $this->cartnew_model->su_get_order_info($order_id); 

	  		//$this->load->view('emails/order_successful_user_copy',$data);

		      $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Processing');		     
		      /*$this->email->to('order@stylecracker.com');    */
		      $this->email->to('sudha@stylecracker.com');  	   
		         
		      $this->email->subject('StyleCracker: Order Processing');
		      $this->load->view('emails/order_successful_user_copy',$data);
		      //$message = $this->load->view('emails/order_successful_user_copy',$data,true);

		  	  $this->email->message($message);
		  	  //$this->email->send();

	  }
	}

    function orderPlaceMessagetoBrand($user_id,$orders_id,$first_name,$last_last){
	/*function orderPlaceMessagetoBrand_new()
	{
		$user_id='10668';
  		//$orders_id='10668_537_538_539_540_541';	
		//$user_id='24224';
		//$orders_id='24224_529_530_531_532';
  		$orders_id='10668_514_515';//product discount scb100
  		//$orders_id='10668_493_494_495';//sc1000 -stylecracker discount
  		//$orders_id='10668_496';//normal order 
  		$orders_id='10668_512_513';// brand discount -civilwar10
  		$first_name='sudha';
  		$last_last='S';*/
    
	    if($this->getEmailAddress($user_id)!=''){

	      $totalProductPrice = 0; $coupon_discount_ = 0;
	      $referral_point = 0;
		  $referral_point = $this->cartnew_model->get_refre_amt($orders_id);
	      $res1 = $this->cartnew_model->su_get_order_info($orders_id);      
		  $data['res1']  = $res1;
	      $order_id = explode('_', $orders_id);
	      unset($order_id[0]);
	      if(!empty($order_id)){
	      foreach($order_id as $order_noval){
	      	  $config['protocol'] = 'smtp';
	     
		//    $config['mailpath'] = '/usr/sbin/sendmail';
		      $config['charset'] = 'iso-8859-1';
		      $config['mailtype'] = 'html';
		      $config['wordwrap'] = TRUE;
		      $config['smtp_host'] = $this->config->item('smtp_host');
		      $config['smtp_user'] = $this->config->item('smtp_user');
		      $config['smtp_pass'] = $this->config->item('smtp_pass');
		      $config['smtp_port'] = $this->config->item('smtp_port');

		      $this->email->initialize($config);
		      $data['orderUniqueNo'] = $order_noval;
		     
		      $data['order_info'] = $this->cartnew_model->br_get_order_price_info($order_noval);
		    /* echo '<pre>';print_r($data['order_info']);*/
		      $data['order_product_info'] = $this->cartnew_model->br_get_order_info($order_noval);
		      //echo '<pre>';print_r($data['order_product_info']);
		      $data['Username'] = $first_name.' '.$last_last;
		      $data['shipping_address'] = $data['order_info'][0]['shipping_address'];
		      $data['city_name'] = $data['order_info'][0]['city_name'];
		      $data['state_name'] = $data['order_info'][0]['state_name'];
		      $data['pincode'] = $data['order_info'][0]['pincode'];
		      $data['country_name'] = $data['order_info'][0]['country_name'];
		      $data['paymentmode'] = $data['order_info'][0]['pay_mode'];
		      $data['email'] = $data['order_info'][0]['email_id'];
		      $data['mobile_no'] = $data['order_info'][0]['mobile_no'];
		      $data['company_name'] = $data['order_product_info'][0]['company_name'];
		      $data['referral_point'] = $referral_point;
		      $data['order_display_no'] = $data['order_info'][0]['order_display_no'];

		      $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Processing');      
		      $this->email->to($this->getEmailAddress($data['order_info'][0]['brand_id']));
		      /*$this->email->cc('order@stylecracker.com');  */
		     $this->email->to('sudha@stylecracker.com');		      
		      //$this->email->cc('saylee@stylecracker.com');
		      $this->email->subject('StyleCracker: Order Processing');
		      //$this->load->view('emails/order_successful_brand_copy',$data);
		      $message = $this->load->view('emails/order_successful_brand_copy',$data,true);
		  	  $this->email->message($message);
		  	  $this->email->send();
		     
			}	
			}
		}
	}

	function orderPlaceMessage_test(){   
		/*$user_id ='21655';
		$order_id='21655_1310';
		$first_name='abc';
		$last_last='abc';*/
		/*$user_id ='31335';
		$order_id='31335_1324';
		$first_name='Rohith';  
		$last_last='Chevuru';*/
		$user_id ='34907';
		$order_id='34907_1331_1332';
		$first_name='Saylee';   
		$last_last='Dakare';
  		if($this->getEmailAddress($user_id)!=''){
	      $config['protocol'] = 'smtp';
	      $totalProductPrice=0;
	      $referral_point = 0;
		  $referral_point = $this->cartnew_model->get_refre_amt($order_id);

	//      $config['mailpath'] = '/usr/sbin/sendmail';
	      $config['charset'] = 'iso-8859-1';
	      $config['mailtype'] = 'html';
	      $config['wordwrap'] = TRUE;
	      $config['smtp_host'] = $this->config->item('smtp_host');
	      $config['smtp_user'] = $this->config->item('smtp_user');
	      $config['smtp_pass'] = $this->config->item('smtp_pass');
	      $config['smtp_port'] = $this->config->item('smtp_port');
			
	      $this->email->initialize($config);

		  	$data['orderUniqueNo'] = $order_id;
		    $data['Username'] = $first_name.' '.$last_last;
		  	$data['order_product_info'] = $this->cartnew_model->su_get_order_info($order_id);    
	      	$data['order_info'] = $this->cartnew_model->su_get_order_price_info_res($order_id);
	      	$data['shipping_address'] = $data['order_info'][0]['shipping_address'];
	      	$data['city_name'] = $data['order_info'][0]['city_name'];
	      	$data['state_name'] = $data['order_info'][0]['state_name'];
	      	$data['pincode'] = $data['order_info'][0]['pincode'];
	      	$data['country_name'] = $data['order_info'][0]['country_name'];
	      	$data['paymentmode'] = $data['order_info'][0]['pay_mode'];
	      	$data['referral_point'] = $referral_point;

	      	if($data['order_info'][0]['state_id'] == '36') { $data['state_name'] =  $data['order_info'][0]['it_state']; }

	      	$data['res'] =  $this->cartnew_model->su_get_order_info($order_id); 

	  		//$this->load->view('emails/order_successful_user_copy',$data);

		      $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Processing');
		     /* $this->email->to($this->getEmailAddress($user_id));    
		      $this->email->cc('order@stylecracker.com');    */
		      /*$this->email->to('sudha@stylecracker.com');		           */
		      //$this->email->cc('saylee@stylecracker.com');    
		      $this->email->subject('StyleCracker: Order Processing');
		      $this->load->view('emails/order_successful_user_copy',$data);
		     /* $message = $this->load->view('emails/order_successful_user_copy',$data,true);*/
		  	  /*$this->email->message($message);*/
		  	  /*$this->email->send();*/

	  }
	  }

	   function orderPlaceMessagetoBrand_test(){
	  /* 	$user_id ='31335';
		$orders_id='31335_1324'; 
		$first_name='Rohith';  
		$last_last='Chevuru';*/

		/*$user_id ='34907';
		$orders_id='34907_1331_1332';
		$first_name='Saylee';   
		$last_last='Dakare';*/
		$user_id ='38211';
		$orders_id='38211_1336_1337';
		$first_name='Ganesh';   
		$last_last='Sonar';
		
	    
	    if($this->getEmailAddress($user_id)!=''){

	      $totalProductPrice = 0; $coupon_discount_ = 0;
	      $referral_point = 0;
		  $referral_point = $this->cartnew_model->get_refre_amt($orders_id);
	      $res1 = $this->cartnew_model->su_get_order_info($orders_id);      
		  $data['res1']  = $res1;
	      $order_id = explode('_', $orders_id);
	      unset($order_id[0]);
	      if(!empty($order_id)){
	      foreach($order_id as $order_noval){
	      	  $config['protocol'] = 'smtp';
	     
		//    $config['mailpath'] = '/usr/sbin/sendmail';
		      $config['charset'] = 'iso-8859-1';
		      $config['mailtype'] = 'html';
		      $config['wordwrap'] = TRUE;
		      $config['smtp_host'] = $this->config->item('smtp_host');
		      $config['smtp_user'] = $this->config->item('smtp_user');
		      $config['smtp_pass'] = $this->config->item('smtp_pass');
		      $config['smtp_port'] = $this->config->item('smtp_port');

		      $this->email->initialize($config);
		      $data['orderUniqueNo'] = $order_noval;
		     
		      $data['order_info'] = $this->cartnew_model->br_get_order_price_info($order_noval);
		     //echo '<pre>';print_r($data['order_info']);
		      $data['order_product_info'] = $this->cartnew_model->br_get_order_info($order_noval);
		      //echo '<pre>';print_r($data['order_product_info']);
		      $data['Username'] = $first_name.' '.$last_last;
		      $data['shipping_address'] = $data['order_info'][0]['shipping_address'];
		      $data['city_name'] = $data['order_info'][0]['city_name'];
		      $data['state_name'] = $data['order_info'][0]['state_name'];
		      $data['pincode'] = $data['order_info'][0]['pincode'];
		      $data['country_name'] = $data['order_info'][0]['country_name'];
		      $data['paymentmode'] = $data['order_info'][0]['pay_mode'];
		      $data['email'] = $data['order_info'][0]['email_id'];
		      $data['mobile_no'] = $data['order_info'][0]['mobile_no'];
		      $data['company_name'] = $data['order_product_info'][0]['company_name'];
		      $data['referral_point'] = $referral_point;

		      $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Processing');      
		      //$this->email->to($this->getEmailAddress($data['order_info'][0]['brand_id']));
		      //echo $data['order_info'][0]['brand_id'];
		     // $this->email->cc('order@stylecracker.com');  
		      /*$this->email->to('sudha@stylecracker.com');	*/	      
		      //$this->email->cc('saylee@stylecracker.com');
		      $this->email->subject('StyleCracker: Order Processing');
		     $this->load->view('emails/order_successful_brand_copy',$data);
		      /*$message = $this->load->view('emails/order_successful_brand_copy',$data,true);
		  	  $this->email->message($message);
		  	  $this->email->send();*/
		     
			}	
			}
		}
	}

	 function getUserAddress(){
	  	$id = $this->input->post('id');
	  	//echo '<pre>'; print_r($this->cart_model->existingAddress($id));
	  	 echo $this->cart_model->getUserAddress($id);exit;
  	}

  	 function getpincodedata()
    {
      $pincode = $this->input->post('pincode');
      $data = $this->Schome_model->getpincodedata($pincode);
      if(!empty($data))
      {
         echo '{"id" : "'.(int)trim(@$data[0]['id']).'", "statename" : "'.trim(strip_tags(@$data[0]['state_name'])).'", "city" : "'.trim(@$data[0]['city']).'"}';exit;
       }else
       {
          echo 0;exit;
       }  
     
    }

    function updateUserAddress()
    {
    	$data =array(); $htmlResult ='';
    	$userid = ($this->session->userdata('user_id')!='') ? $this->session->userdata('user_id') : 0 ;
    	$shipping_name = trim($this->input->post('shipping_name'));

    	$data['existing_shipping_add'] = trim($this->input->post('user_address_id'));
    	$data['user_first_name'] = trim($this->input->post('user_first_name'));
    	$data['user_last_name'] = trim($this->input->post('user_last_name'));    	
    	$data['user_mobile_no'] = trim($this->input->post('user_mobile_no'));
    	$data['user_address'] = trim($this->input->post('user_address'));
    	$data['user_pincode_no'] = trim($this->input->post('user_pincode_no'));
    	$data['user_city'] = trim($this->input->post('user_city'));
    	$data['user_state'] = trim($this->input->post('user_state')); 

    	$type =  trim($this->input->post('type'));   
    	$setdefault = trim($this->input->post('setdefault'));	
    	if($type=='add')
    	{
    		$response = $this->cartnew_model->updateUserAddress($data,$userid);
    		
    	}else
    	{    		
    		$response = $this->cartnew_model->updateUserAddress($data,$userid);
    		//echo $response;
    	}

    	$result = $this->cart_model->get_user_existing_address(0);    	
    		
    		foreach($result as $val)
    		{
    			$activeShip = ( (isset($_COOKIE['shipping_id']) && ($_COOKIE['shipping_id']==$val['id'])) || ($setdefault==1 && $response==$val['id']) || ($setdefault==3  && $response==$val['id']))? 'active' : '';
    			$activeBill = ( (isset($_COOKIE['billing_id']) && ($_COOKIE['billing_id']==$val['id'])) || ($setdefault==2  && $response==$val['id']) || ($setdefault==3  && $response==$val['id'])) ? 'active' : '';

	    		$htmlResult = $htmlResult.'<li class="" id="user_address_'.$val['id'].'" attr-state="'.(int)$val['state_id'].'" ><div class="title-3">
	                       '.trim($val['first_name']).' '.trim($val['last_name']).'
	                      </div>
	                      <div class="desc-address">
	                          '.trim($val['shipping_address']).' 
	                      </div>
	                      <div class="desc">
	                          '.trim($val['city_name']).', '.trim($val['states_name']).', '.trim($val['pincode']).', '.trim($val['mobile_no']).' 
	                      </div>
	                      <a class="close111 btn-remove"  onclick="delete_user_address(\''.$val['id'].'\')" ><i class="icon-close" ></i></a>
	                      <a class="btn-edit" href="#modal-address" data-toggle="modal" onclick="editAddress(this,\''.$val['id'].'\')" attr-value="'.$val['id'].'" ><i class="icon-edit"></i></a>
	                      <div class="btns-wrp">
	                        <label class="sc-checkbox ship-checkbox '.$activeShip.'"  attr-value="'.$val['id'].'"><i class="icon icon-diamond"></i><span>Ship Here</span></label>
	                        <label class="sc-checkbox bill-checkbox '.$activeBill.'" attr-value="'.$val['id'].'"><i class="icon icon-diamond"></i><span>Bill Here</span></label>
	                      </div>
	                    </li>';
            }
    		echo $htmlResult;exit;
    	
    }

    function get_cartQty(){
    
      $total_qty = '0';
      /*$uniquecookie = $this->input->post('uniquecookie');
      $uc_pincode_ = $this->input->post('uc_pincode') ? $this->input->post('uc_pincode') : '';
      $result = $this->cartnew_model->getCartQty($uniquecookie);
      
        if(!empty($result)){               
        foreach ($result as $key => $value) {         
          $total_qty = $total_qty + $value['product_qty'];             
          }
        }  */

    	echo $total_qty;exit;	
	}


	public function get_coupons(){
		// $userid = $this->session->userdata('user_id');		
		// $user_email = $this->session->userdata('email');
	    $userid = '';		
		$user_email = '';
		$page_type = $this->input->post('page_type');
		$data['couponcodes'] = $this->Mcart_model_android->get_coupons($userid,$user_email,$page_type);	
		// if(!empty($data['couponcodes'])){	//echo '<pre>';print_r($data['couponcodes']);	

			 $this->load->view('seventeen/cart/modal_offers_popup',$data);
			//$this->load->view('seventeen/cart/cartproduct_view',$data);
		// }else{
		// 	$res = array();
		// 	// $this->success_response($res);
		// 	//echo '<pre>';print_r($couponcodes);
		// 	echo false;
		// }
	}
	
	public function validate_coupon(){
		
		$userid = $this->session->userdata('user_id');	
		//$auth_token = $this->post('auth_token');
		$total_product_price = $this->input->post('total_product_price');
		$email = $this->session->userdata('email');
		$coupon_code = $this->input->post('coupon_code');
		$bin_no = $this->input->post('bin_no');
		$response = $this->Mcart_model_android->check_coupon_exist($coupon,$user_email_id,$cart_product_cost);
		$this->success_response($response);
		
	}
	
}


