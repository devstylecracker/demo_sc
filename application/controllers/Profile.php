<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
       parent::__construct();
       $this->load->model('Schome_model');
       $this->load->model('Pa_model');
      // $this->load->library('upload');
       $this->load->library('bitly');
   	}

	public function index()
	{
		if($this->session->userdata('user_id')){

			$user_info = $this->Schome_model->get_users_info();

			if($user_info[0]->bucket <= 0 || $user_info[0]->question_comp!=1){
				redirect('schome/pa');
			}

			$gender = $this->Pa_model->get_gender($this->session->userdata('user_id'));
			if($gender == 1){
	    		
				//@$data['user_body_shape'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(1)[0]['answer_id']);
	       	 	//@$data['user_style'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(2)[0]['answer_id']);
	        	@$data['user_age'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(8)[0]['answer_id']);
	        	//@$data['user_budget'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(7)[0]['answer_id']);
	        	//@$data['user_size'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(9)[0]['answer_id']);
	        	@$data['user_info'] = $user_info;
	        	@$data['user_wishlist_count']= $this->Pa_model->get_wishlist_count(); 

				$this->load->view('common/header_view');
				$this->load->view('profile',$data);
				$this->load->view('common/footer_view');
				$this->load->view('profile_js');
	        }else{
	        	//@$data['user_body_shape'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(13)[0]['answer_id']);
	       	 	//@$data['user_style'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(14)[0]['answer_id']);
	       	 	//@$data['work_style'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(15)[0]['answer_id']);
	       	 	//@$data['personal_style'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(16)[0]['answer_id']);
	        	@$data['user_age'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(17)[0]['answer_id']);
	        	//@$data['user_budget'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(18)[0]['answer_id']);
	        	//@$data['user_size'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(19)[0]['answer_id']);
	        	@$data['user_info'] = $user_info;
	        	@$data['user_wishlist_count']= $this->Pa_model->get_wishlist_count(); 

				$this->load->view('common/header_view');
				$this->load->view('profile_male',$data);
				$this->load->view('common/footer_view');
				$this->load->view('profile_js');

	        }

		}else{
			redirect('/');
		}
	}

	public function upload_profile(){
		$status = "";
    	$msg = "";
    	$file_element_name = 'profile_img';
		if ($status != "error")
    	{
	        $config['upload_path'] = $this->config->item('profile_image_path');
	        $config['allowed_types'] = 'JPG|jpg|png|jpge|PNG';
	        $config['max_size'] = 1024 * 8;
	        $config['file_name'] = time();


	        $this->load->library('upload', $config);

	        if (!$this->upload->do_upload($file_element_name))
	        {
	            $status = 'error';
	           $msg = $this->upload->display_errors('', '');
	        }
	        else
	        {
	            $data = $this->upload->data();
	            $file_id = $this->Pa_model->update_profile_img($data['file_name']);
	            if($this->Pa_model->update_profile_img($data['file_name']))
	            {
	                $status = "success";
	                $msg = "File successfully uploaded";
	            }
	            else
	            {
	                $status = "error";
	                $msg = "Something went wrong when saving the file, please try again.";
	            }

	        }
    	}

		echo json_encode(array('status' => $status, 'msg' => $msg));
	}

	public function cover_image(){
		$status = "";
    	$msg = "";
    	$file_element_name = 'img';

		if ($status != "error")
    	{
	        $config['upload_path'] = $this->config->item('cover_image_path');
	        $config['allowed_types'] = 'JPG|jpg|png|jpge|PNG';
	       // $config['max_size'] = 1024 * 8;
	        $config['file_name'] = time();


	        $this->load->library('upload', $config);

	        if (!$this->upload->do_upload($file_element_name))
	        {
	            $status = 'error';
	            $msg = $this->upload->display_errors('', '');
	             $response = array(
				"status" => 'error',
				"message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
				);
	        }
	        else
	        {
	            $data = $this->upload->data();
	            $width = $data['image_width'];
	            $height = $data['image_height'];
	            $fileext = $data['file_ext'];
	            //$file_id = $this->Pa_model->update_profile_img($data['file_name']);
	            if($this->Pa_model->update_cover_img($data['file_name']))
	            {
	                 $response = array(
					"status" => 'success',
					"url" => $this->config->item('cover_image_path').$config['file_name'].$fileext,
					"width" => $width,
					"height" => $height
				    );
	            }
	            else
	            {
	                 $response = array(
					"status" => 'error',
					"message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
					);
	            }

	        }
    	}

		echo json_encode($response);
	}

	function crop_img(){
		$imgUrl = $this->input->post('imgUrl');
		// original sizes
		$imgInitW =  $this->input->post('imgInitW');
		$imgInitH =  $this->input->post('imgInitH');
		// resized sizes
		$imgW =  $this->input->post('imgW');
		$imgH =  $this->input->post('imgH');
		// offsets
		$imgY1 =  $this->input->post('imgY1');
		$imgX1 =  $this->input->post('imgX1');
		// crop box
		$cropW =  $this->input->post('cropW');
		$cropH =  $this->input->post('cropH');
		// rotation angle
		$angle = $this->input->post['rotation'];

		$jpeg_quality = 100;
		$crop_img_temp_name = explode('/', $imgUrl);
		$crop_img_temp_name = explode('.', end($crop_img_temp_name));
		$crop_img_name = $crop_img_temp_name[0];
		$ext_new = explode('.', $crop_img_name);

		$output_filename = $this->config->item('cover_image_path').$crop_img_name;

// uncomment line below to save the cropped image in the same location as the original image.
//$output_filename = dirname($imgUrl). "/croppedImg_".rand();

$what = getimagesize($imgUrl);

switch(strtolower($what['mime']))
{
    case 'image/png':
        $img_r = imagecreatefrompng($imgUrl);
		$source_image = imagecreatefrompng($imgUrl);
		$type = '.png';
        break;
    case 'image/jpeg':
        $img_r = imagecreatefromjpeg($imgUrl);
		$source_image = imagecreatefromjpeg($imgUrl);
		error_log("jpg");
		$type = '.jpeg';
        break;
    case 'image/gif':
        $img_r = imagecreatefromgif($imgUrl);
		$source_image = imagecreatefromgif($imgUrl);
		$type = '.gif';
        break;
    default: die('image type not supported');
}


//Check write Access to Directory

if(!is_writable(dirname($output_filename))){
	$response = Array(
	    "status" => 'error',
	    "message" => 'Can`t write cropped File'
    );
}else{

    // resize the original image to size of editor
    $resizedImage = imagecreatetruecolor($imgW, $imgH);
	imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
    // rotate the rezized image
    $rotated_image = imagerotate($resizedImage, -$angle, 0);
    // find new width & height of rotated image
    $rotated_width = imagesx($rotated_image);
    $rotated_height = imagesy($rotated_image);
    // diff between rotated & original sizes
    $dx = $rotated_width - $imgW;
    $dy = $rotated_height - $imgH;
    // crop rotated image to fit into original rezized rectangle
	$cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
	imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
	imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
	// crop image into selected area
	$final_image = imagecreatetruecolor($cropW, $cropH);
	imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
	imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
	// finally output png image
	//imagepng($final_image, $output_filename.$type, $png_quality);
	imagejpeg($final_image, $output_filename.$type, $jpeg_quality);
	$response = Array(
	    "status" => 'success',
	    "url" => $this->config->item('cover_image_path').$ext_new[0].$type
    );
    $this->Pa_model->update_cover_img($ext_new[0].$type);
}
echo json_encode($response);

	}

	public function upload_wordrobe(){
		$status = "";
    	$msg = "";
    	$file_element_name = 'upload_photoes[]';
    	$this->load->library('image_lib');
		if ($status != "error")
    	{
	        $files = $_FILES;
    		$cpt = count($_FILES['upload_photoes']['name']);
    		$k  = 0;
		    for($i=0; $i<$cpt; $i++)
		    {

		       		$ext = pathinfo($files['upload_photoes']['name'][$i], PATHINFO_EXTENSION);
		       		if($ext == 'JPG' || $ext == 'jpg' || $ext == 'png' || $ext == 'jpeg' || $ext == 'PNG' ){
						$img_name = time().mt_rand(0,10000).'.'.$ext;
						$pathAndName = $this->config->item('wordrobe_path').$img_name;
						$moveResult = move_uploaded_file($files['upload_photoes']['tmp_name'][$i], $pathAndName);
						//$this->createThumbs( $pathAndName, $this->config->item('wordrobe_thumb_path'), 100 ,$img_name);
						$config['image_library'] = 'gd2';
						$config['source_image'] = $pathAndName;
						$config['new_image'] =  $this->config->item('wordrobe_thumb_path');
						$config['create_thumb'] = TRUE;
						$config['thumb_marker'] = '';
						$config['width'] = 180;
						$config['height'] = 180;

						//$this->load->library('image_lib', $config);
						$this->image_lib->initialize($config);

						if ( ! $this->image_lib->resize())
						{
    						echo $this->image_lib->display_errors();
						}

						/*
						$this->load->library('image_lib');
						$config['image_library'] = 'gd2';
						$config['source_image'] = $this->config->item('wordrobe_thumb_path');
						$config['create_thumb'] = TRUE;
						$config['maintain_ratio'] = TRUE;
						$config['width'] = 100;
						$config['height'] = 100;

						$this->load->library('image_lib', $config);

						if($this->image_lib->resize()){
							echo $this->image_lib->display_errors(); exit;
						}*/

						$this->Pa_model->upload_wordrobe_img($img_name);
						$k++;
					}
		    }


		    $status = "success";
	        $msg = $k." File successfully uploaded";
    	}

		echo json_encode(array('status' => $status, 'msg' => $msg));
	}

	public function set_upload_options(){

		$config = array();

			$config['upload_path'] = $this->config->item('wordrobe_path');
	        $config['allowed_types'] = 'JPG|jpg|png|jpge|PNG';
	        //$config['max_size'] = 1024 * 8;
	        $config['file_name'] = time().mt_rand(0,100);
	        return $config;
	}

	public function display_wardrobe(){
		if($this->session->userdata('user_id')){
			$this->Pa_model->display_wardrobe();
		}
	}

	public function delete_wardrobe_img(){
		if($this->session->userdata('user_id')){
			$image_id = $this->input->post('id');
			$this->Pa_model->delete_wardrobe_img($image_id);
			return true;
		}
	}

	public function add_to_wishlist(){
		if($this->session->userdata('user_id')){
			$look_id = $this->input->post('look_id');
			$this->Schome_model->add_to_wishlist($look_id);
			return true;
		}
	}

	public function remove_from_wishlist(){
		if($this->session->userdata('user_id')){
			$look_id = $this->input->post('look_id');
			$this->Schome_model->remove_from_wishlist($look_id);
			return true;
		}
	}

	public function generate_bitly(){
		if($this->session->userdata('user_id')){
			$loc = $this->input->post('loc');
			echo $this->bitly->shorten($loc);
		}
	}

	public function createThumbnail($filename){

	    $path_to_image_directory =$this->config->item('wordrobe_thumb_path');
	    if(preg_match('/[.](jpg)$/', $filename)) {
	        $im = imagecreatefromjpeg($path_to_image_directory . $filename);
	    } else if (preg_match('/[.](gif)$/', $filename)) {
	        $im = imagecreatefromgif($path_to_image_directory . $filename);
	    } else if (preg_match('/[.](png)$/', $filename)) {
	        $im = imagecreatefrompng($path_to_image_directory . $filename);
	    }

	    $ox = imagesx($im);
	    $oy = imagesy($im);

	    $nx = $final_width_of_image;
	    $ny = floor($oy * ($final_width_of_image / $ox));

	    $nm = imagecreatetruecolor($nx, $ny);

	    imagecopyresized($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);

	    if(!file_exists($path_to_thumbs_directory)) {
	      if(!mkdir($path_to_thumbs_directory)) {
	           die("There was a problem. Please try again!");
	      }
	       }

	    imagejpeg($nm, $path_to_thumbs_directory . $filename);

	}
	public function wardrobe(){
		$this->load->view('common/header_view');
		$this->load->view('wardrobe');
		$this->load->view('common/footer_view');
	}


}
