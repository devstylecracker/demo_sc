<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';

class Mobile_brand extends TT_REST_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Mobile_look_model');
		$this->load->model('Brand_model');
	}
	
	public function single_brand_get(){
		$brand_id = $this->get('brand_id');
		$user_id = $this->get('user_id');
		$sort_by = $this->get('sort_by');
		$limit = $this->get('limit');
		$offset = $this->get('offset');	
		$data['brand_info'] = $this->Brand_model->get_brand_info($brand_id);
		$data['brand_products'] = $this->Brand_model->get_brand_products($brand_id,$user_id,$sort_by,$limit,$offset);
		if(!empty($data)){ 
			$this->success_response($data);
		}else{
		   $data = array();
	       $this->success_response($data);
	    }
	}
	
}