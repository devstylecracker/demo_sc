<?php
ini_set('max_execution_time', 0);
ini_set('memory_limit','512M');
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class SCProductinventorycron extends MY_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('image_lib');
        $this->load->model('Scproductinventorycron_model');  
        date_default_timezone_set("Asia/Calcutta");
       /* $config = Array(
					    'protocol' => 'smtp',
					    'smtp_host' => 'ssl://smtp.googlemail.com',
					    'smtp_port' => 465,
					    'smtp_user' => 'styleemailphp@gmail.com',
					    'smtp_pass' => 'Style123*',
					    'mailtype'  => 'html', 
					    'charset'   => 'iso-8859-1'
					);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");*/
    }

    function index(){
    	echo 'Index SCProductinventorycron';
    	exit;
    }
    
    function status(){
    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);

		print_r($current);
		exit;
    }

    //--------------Forever Data Processed and add
    function forever_master_xml_json_get(){
    	// Set your email information
		$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron Forever Master start';		 
		$message = 'START|forever(master)|'. date("Y-m-d-h:i:sa").'|';
		$message = "";//---------------------------------------------------------- Added by amol
    	//$this->email($from, $to, $subject, $message);
    	$file_path_log = "resource/log.txt";
		$myfile = fopen($file_path_log, "w") or die("Unable to open file!");
		//fwrite($myfile, $message."<br/>");
		fwrite($myfile, $message);//---------------------------------------------------------- Added by amol
		fclose($myfile);
exit;//---------------------------------------------------------- Stoped for 15 days Amol

    	$ftp_server = "182.72.235.75";
		$ftp_user = "f21_affliate";
		$ftp_pass = "f21_affliate";
		// set up a connection or die
		$conn_id = ftp_connect($ftp_server) or die("Couldn't connect to $ftp_server");

		// try to login
		if (@ftp_login($conn_id, $ftp_user, $ftp_pass)) {
		     "Connected as $ftp_user@$ftp_server\n";
		} else {
		     "Couldn't connect as $ftp_user\n";
		}
		$contents = ftp_nlist($conn_id, ".");

		$master="";
		foreach($contents as $row){
			if (strpos($row, 'MASTER') !== false) { 
			    $master = $row;
			}
			/*if (strpos($row, 'DELTA') !== false) {
			    $delta = $row;
			}*/
		}
		$filename = "ftp://".$ftp_user.":".$ftp_pass."@".$ftp_server."/f21_affliate/".$master;//AmazonFeed_MASTER_.date('Y_m_d').".xml";


		$local_file = "resource/forever_json/forever_master.xml";
		// try to download $server_file and save to $local_file
		if (ftp_get($conn_id, $local_file, $master, FTP_BINARY)) {
		    echo "Successfully written to $local_file\n";
		}
		else {
		    echo "There was a problem\n";
		}
		// close the connection

		// close the connection
		ftp_close($conn_id); 

		$myXMLData = file_get_contents('resource/forever_json/forever_master.xml');
		$xml=simplexml_load_string($myXMLData) or die("Error: Cannot create object");
		$x=0;
		$z=1;
		//echo count($xml);exit;
		if(!empty($xml->item)){
			$product_json = '[{';
			$product_json1 = '[{';

			$i = 0;	
			foreach($xml->item as $val){ 		
				if($i!=0){
					$product_json = $product_json.'},{';
				}
				if($x!=0){
					$product_json1 = $product_json1.'},{';
				}
					$product_json = $product_json.'"product_sku":"'.strip_tags($val->SKU).'",';
					$product_json = $product_json.'"product_category":"'.strip_tags($val->Category).'",';
					$product_json = $product_json.'"product_subcategory":"'.strip_tags($val->SubCategory).'",';						
					$arr =  $val->Title;
					$text_one = str_replace('<![CDATA[', '', $arr);
					$text_two = str_replace(']]>', '', $text_one);
					$product_json = $product_json.'"product_name":"'.strip_tags(str_replace('"','',(trim($text_two)))).'",';						
					$product_json = $product_json.'"product_url":"'.strip_tags($val->Link).'",';
					$description_arr =  $val->Description;
					$description_one = str_replace('<![CDATA[', '', $description_arr);
					$description_two = str_replace(']]>', '', $description_one);			
					$product_json = $product_json.'"product_description":"'.strip_tags(str_replace('"','',(trim($description_two)))).'",';			
					$product_json = $product_json.'"price":"'.strip_tags($val->Price).'",';
					$product_json = $product_json.'"age_group":"",';
					if(strip_tags(@$val->OtherImageUrl1)){ $otherimageurl1 = strip_tags(@$val->OtherImageUrl1);  }else{$otherimageurl1 = ''; }
					if(strip_tags(@$val->Image)){ $image = strip_tags(@$val->Image);  }else{$image = ''; }						
					$product_json = $product_json.'"product_img":["'.strip_tags($otherimageurl1).'","'.strip_tags($image).'"],';			
					if($val->Availability == 'TRUE'){$availabil=1;} else{$availabil=0;}						
					$product_json = $product_json.'"product_stock":[{"'.strip_tags($val->SizeMap).'":"'.strip_tags($availabil=1).'"}],';
					$product_json = $product_json.'"product_color":"'.strip_tags($val->ColorName).'",';
					$product_json = $product_json.'"product_discount":"",';
					$product_json = $product_json.'"discount_start_date":"",';
					$product_json = $product_json.'"discount_end_date":"",';			
					if(strip_tags(@$val->Keyword1)){ $keyword1 = strip_tags(@$val->Keyword1);  }else{$keyword1 = ''; }
					if(strip_tags(@$val->Keyword2)){ $keyword2 = strip_tags(@$val->Keyword2);  }else{$keyword2 = ''; }
					if(strip_tags(@$val->Keyword3)){ $keyword3 = strip_tags(@$val->Keyword3);  }else{$keyword3 = ''; }
					if(strip_tags(@$val->Keyword4)){ $keyword4 = strip_tags(@$val->Keyword4);  }else{$keyword4 = ''; }
					if(strip_tags(@$val->ColorName)){ $colorname = strip_tags(@$val->ColorName);  }else{$colorname = ''; }
					if(strip_tags(@$val->Material)){ $material = strip_tags(@$val->Material);  }else{$material = ''; }
					if(strip_tags(@$val->TargetGender)){ $targetgender = strip_tags(@$val->TargetGender);  }else{$targetgender = ''; }
					$product_json = $product_json.'"product_tags":["'.strip_tags($keyword1).'","'.strip_tags($keyword2).'","'.strip_tags($keyword3).'","'.strip_tags($keyword4).'","'.strip_tags($colorname).'","'.strip_tags($material).'","'.strip_tags($targetgender).'"]';
				////------ Json 1
				/*$product_json1 = $product_json1.'"product_sku":"'.strip_tags($val->SKU).'",';
				$product_json1 = $product_json1.'"product_category":"'.strip_tags($val->Category).'",';
				$product_json1 = $product_json1.'"product_subcategory":"'.strip_tags($val->SubCategory).'",';						
				$arr =  $val->Title;
				$text_one = str_replace('<![CDATA[', '', $arr);
				$text_two = str_replace(']]>', '', $text_one);
				$product_json1 = $product_json1.'"product_name":"'.strip_tags($text_two).'",';						
				$product_json1 = $product_json1.'"product_url":"'.strip_tags($val->Link).'",';
				$description_arr =  $val->Description;
				$description_one = str_replace('<![CDATA[', '', $description_arr);
				$description_two = str_replace(']]>', '', $description_one);			
				$product_json1 = $product_json1.'"product_description":"'.strip_tags(str_replace('"','',(trim($description_two)))).'",';			
				$product_json1 = $product_json1.'"price":"'.strip_tags($val->Price).'",';
				$product_json1 = $product_json1.'"age_group":"",';
				if(strip_tags(@$val->OtherImageUrl1)){ $otherimageurl1 = strip_tags(@$val->OtherImageUrl1);  }else{$otherimageurl1 = ''; }
				if(strip_tags(@$val->Image)){ $image = strip_tags(@$val->Image);  }else{$image = ''; }						
				$product_json1 = $product_json1.'"product_img":["'.strip_tags($otherimageurl1).'","'.strip_tags($image).'"],';			
				if($val->Availability == 'TRUE'){$availabil=1;} else{$availabil=0;}						
				$product_json1 = $product_json1.'"product_stock":[{"'.strip_tags($val->SizeMap).'":"'.strip_tags($availabil=1).'"}],';
				$product_json1 = $product_json1.'"product_discount":"",';
				$product_json1 = $product_json1.'"discount_start_date":"",';
				$product_json1 = $product_json1.'"discount_end_date":"",';			
				if(strip_tags(@$val->Keyword1)){ $keyword1 = strip_tags(@$val->Keyword1);  }else{$keyword1 = ''; }
				if(strip_tags(@$val->Keyword2)){ $keyword2 = strip_tags(@$val->Keyword2);  }else{$keyword2 = ''; }
				if(strip_tags(@$val->Keyword3)){ $keyword3 = strip_tags(@$val->Keyword3);  }else{$keyword3 = ''; }
				if(strip_tags(@$val->Keyword4)){ $keyword4 = strip_tags(@$val->Keyword4);  }else{$keyword4 = ''; }
				if(strip_tags(@$val->ColorName)){ $colorname = strip_tags(@$val->ColorName);  }else{$colorname = ''; }
				if(strip_tags(@$val->Material)){ $material = strip_tags(@$val->Material);  }else{$material = ''; }
				if(strip_tags(@$val->TargetGender)){ $targetgender = strip_tags(@$val->TargetGender);  }else{$targetgender = ''; }
				$product_json1 = $product_json1.'"product_tags":["'.strip_tags($keyword1).'","'.strip_tags($keyword2).'","'.strip_tags($keyword3).'","'.strip_tags($keyword4).'","'.strip_tags($colorname).'","'.strip_tags($material).'","'.strip_tags($targetgender).'"]';
				*/
				$i++;
				$x++;
				if($x == 2000){
					$x=0;

					/*$product_json1 = $product_json1.'}]'; 
					//echo $product_json;
					$url = "resource/forever_json/master_list/forever_master_$z.json";
					$myfile = fopen($url, "w") or die("Unable to open file!");
					$txt = $product_json1;
					if(fwrite($myfile, $txt)){
						$file = "resource/forever_json/logs.txt";
						// Open the file to get existing content
						$current = file_get_contents($file);
						// Append a new person to the file
						$current .= date("dd/mm/Y H:i:s")."Done - $z - $master\n";
						// Write the contents back to the file
						file_put_contents($file, $current);
					}else{
						$file = 'resource/forever_json/logs.txt';
						// Open the file to get existing content
						$current = file_get_contents($file);
						// Append a new person to the file
						$current .= date("dd/mm/Y H:i:s")."Failed - $z - $master \n";
						// Write the contents back to the file
						file_put_contents($file, $current);
					}
					fclose($myfile);
					$z++;
					$product_json1 = '[{';*/
				}
			}
			$product_json = $product_json.'}]';
		//	$product_json1 = $product_json1.'}]';
		}else{
			echo "Data not found";
			exit;
		}
		$z++;
		///---- json 1
		/*$url = "resource/forever_json/master_list/forever_master_$z.json";
		$myfile = fopen($url, "w") or die("Unable to open file!");
		$txt = $product_json1;
		if(fwrite($myfile, $txt)){
			$file = "resource/forever_json/logs.txt";
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Done - $x - $master\n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}else{
			$file = 'resource/forever_json/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Failed - $x - $master \n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}
		fclose($myfile);*/
		///---- json 1
		$file_path = "resource/forever_json/forever_master.json";
		$myfile = fopen($file_path, "w") or die("Unable to open file!");
		$txt = "";
		fwrite($myfile, $txt);
		fclose($myfile);		
		if(file_put_contents($file_path, $product_json)){
			$inputs['url'] = $file_path;
	        $inputs['api_key'] = "GgZ6cu5cQZbDd2ZmPsRLa5po6tt0TqNc"; //Live //'rbVarYQtOCbtoHtrLuW75CgBjnuWL1xLlA884n0sm0A='; // amol pc
	        $inputs['secret_key'] = "D6jK8JdKm8MGjUWfGz2YD7RZi70tmTWSMJEBf1c9qXlsi2pE7FNmBg=="; //live // 'nsXjOxzh0Pgo6jFQmShFpNslbSxihnUT43tC7vFDD0ymATS1hUuvBg==';//amol pc
	        $inputs['products'] = ''; // "D6jK8JdKm8MGjUWfGz2YD7RZi70tmTWSMJEBf1c9qXlsi2pE7FNmBg=="; //live
	        $inputs['delete_inventory'] = true;
	        $this->bulk_uploadproducts_get($inputs);
			$file = 'resource/forever_json/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Done - $master\n";
			// Write the contents back to the file
			file_put_contents($file, $current);
			/*file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=END|forever|master|'. date("Y-m-d-h:i:sa"));*/
			// Set your email information
			$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
			$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
			$subject = 'Cron Forever Master End';			 
			$message = 'END|forever(master)|'. date("Y-m-d-h:i:sa");
	    	//$this->email($from, $to, $subject, $message);

	    	$file_path_log = "resource/log.txt";
			// Open the file to get existing content
			$current = file_get_contents($file_path_log);
			// Append a new person to the file
			$current .= $message;
			// Write the contents back to the file
			file_put_contents($file_path_log, $current."|");

		}else{
			$file = 'resource/forever_json/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Failed - $master \n";
			// Write the contents back to the file
			file_put_contents($file, $current);
			/*file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=FAILED|forever|master|'. date("Y-m-d-h:i:sa"));*/
			$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
			$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
			$subject = 'Cron Forever Master FAILED';
			 
			$message = 'FAILED|forever(master)|'. date("Y-m-d-h:i:sa");
	    	//$this->email($from, $to, $subject, $message);
		}
		
    }

    function forever_delta_xml_json_get(){    	
    	exit;
    	//file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=START|forever|delta|'. date("Y-m-d-h:i:sa"));
    	$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron Forever Delta Start';
		 
		$message = 'START|forever|delta|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");

    	$ftp_server = "182.72.235.75";
		$ftp_user = "f21_affliate";
		$ftp_pass = "f21_affliate";

		// set up a connection or die
		$conn_id = ftp_connect($ftp_server) or die("Couldn't connect to $ftp_server");

		// try to login
		if (@ftp_login($conn_id, $ftp_user, $ftp_pass)) {
		     "Connected as $ftp_user@$ftp_server\n";
		} else {
		     "Couldn't connect as $ftp_user\n";
		}
		$contents = ftp_nlist($conn_id, ".");

		$master="";
		foreach($contents as $row){
			/*if (strpos($row, 'MASTER') !== false) { 
			    $master = $row;
			}*/
			if (strpos($row, 'DELTA') !== false) {
			   $delta = $row;

			}
		}

		//echo "<pre>"; print_r($contents); exit;
		$filename = "ftp://".$ftp_user.":".$ftp_pass."@".$ftp_server."/f21_affliate/".$delta;//AmazonFeed_MASTER_.date('Y_m_d').".xml";

		//$tempHandle = fopen('php://temp', 'r+');
		//$handle = fopen($filename, "r");
		//$contents = fread($handle, filesize($filename));
		$data = file_get_contents($filename);
		//ftp_fget($conn_id, $tempHandle, $filename, FTP_ASCII, 0);
		//$data = $this->ftp_get_contents($conn_id, $filename);
		
		$myfile = fopen("resource/forever_json/forever_delta.xml", "w") or die("Unable to open file!");
		fwrite($myfile, $data);
		fclose($myfile);
		
		// close the connection
		ftp_close($conn_id); 

		$myXMLData = file_get_contents('resource/forever_json/forever_delta.xml');
		$xml=simplexml_load_string($myXMLData) or die("Error: Cannot create object");
		if(!empty($xml->item)){
			$product_json = '[{';
			$i = 0;	
			foreach($xml->item as $val){ 		
				if($i!=0){
					$product_json = $product_json.'},{';
				}
					$product_json = $product_json.'"product_sku":"'.strip_tags($val->SKU).'",';
					$product_json = $product_json.'"product_category":"'.strip_tags($val->Category).'",';
					$product_json = $product_json.'"product_subcategory":"'.strip_tags($val->SubCategory).'",';						
					$arr =  $val->Title;
					$text_one = str_replace('<![CDATA[', '', $arr);
					$text_two = str_replace(']]>', '', $text_one);
					$product_json = $product_json.'"product_name":"'.strip_tags(str_replace('"','',(trim($text_two)))).'",';						
					$product_json = $product_json.'"product_url":"'.strip_tags($val->Link).'",';
					$description_arr =  $val->Description;
					$description_one = str_replace('<![CDATA[', '', $description_arr);
					$description_two = str_replace(']]>', '', $description_one);			
					$product_json = $product_json.'"product_description":"'.strip_tags(str_replace('"','',(trim($description_two)))).'",';			
					$product_json = $product_json.'"price":"'.strip_tags($val->Price).'",';
					$product_json = $product_json.'"age_group":"",';
					if(strip_tags(@$val->OtherImageUrl1)){ $otherimageurl1 = strip_tags(@$val->OtherImageUrl1);  }else{$otherimageurl1 = ''; }
					if(strip_tags(@$val->Image)){ $image = strip_tags(@$val->Image);  }else{$image = ''; }						
					$product_json = $product_json.'"product_img":["'.strip_tags($otherimageurl1).'","'.strip_tags($image).'"],';			
					if($val->Availability == 'TRUE'){$availabil=1;} else{$availabil=0;}						
					$product_json = $product_json.'"product_stock":[{"'.strip_tags($val->SizeMap).'":"'.strip_tags($availabil=1).'"}],';
					$product_json = $product_json.'"product_color":"'.strip_tags($val->ColorName).'",';
					$product_json = $product_json.'"product_discount":"",';
					$product_json = $product_json.'"discount_start_date":"",';
					$product_json = $product_json.'"discount_end_date":"",';			
					if(strip_tags(@$val->Keyword1)){ $keyword1 = strip_tags(@$val->Keyword1);  }else{$keyword1 = ''; }
					if(strip_tags(@$val->Keyword2)){ $keyword2 = strip_tags(@$val->Keyword2);  }else{$keyword2 = ''; }
					if(strip_tags(@$val->Keyword3)){ $keyword3 = strip_tags(@$val->Keyword3);  }else{$keyword3 = ''; }
					if(strip_tags(@$val->Keyword4)){ $keyword4 = strip_tags(@$val->Keyword4);  }else{$keyword4 = ''; }
					if(strip_tags(@$val->ColorName)){ $colorname = strip_tags(@$val->ColorName);  }else{$colorname = ''; }
					if(strip_tags(@$val->Material)){ $material = strip_tags(@$val->Material);  }else{$material = ''; }
					if(strip_tags(@$val->TargetGender)){ $targetgender = strip_tags(@$val->TargetGender);  }else{$targetgender = ''; }

					$product_json = $product_json.'"product_tags":["'.strip_tags($keyword1).'","'.strip_tags($keyword2).'","'.strip_tags($keyword3).'","'.strip_tags($keyword4).'","'.strip_tags($colorname).'","'.strip_tags($material).'","'.strip_tags($targetgender).'"]';

				$i++;
				//if($i == 2000){  $product_json = $product_json.'}]'; break;echo $product_json; exit; }
			}
			$product_json = $product_json.'}]';
		}else{
			echo "Data not found";
			exit;
		}
		$file_path = "resource/forever_json/forever_delta.json";
		$myfile = fopen($file_path, "w") or die("Unable to open file!");
		$txt = "";
		fwrite($myfile, $txt);
		fclose($myfile);
		if(file_put_contents($file_path,$product_json)){
			$file = 'resource/forever_json/logs.txt';
			
			$inputs['url'] = $file_path;
	        $inputs['api_key'] = "GgZ6cu5cQZbDd2ZmPsRLa5po6tt0TqNc"; //Live //'rbVarYQtOCbtoHtrLuW75CgBjnuWL1xLlA884n0sm0A='; // amol pc
	        $inputs['secret_key'] = "D6jK8JdKm8MGjUWfGz2YD7RZi70tmTWSMJEBf1c9qXlsi2pE7FNmBg=="; //live // 'nsXjOxzh0Pgo6jFQmShFpNslbSxihnUT43tC7vFDD0ymATS1hUuvBg==';//amol pc
	        $inputs['products'] = ''; // "D6jK8JdKm8MGjUWfGz2YD7RZi70tmTWSMJEBf1c9qXlsi2pE7FNmBg=="; //live
	        $this->bulk_uploadproducts_get($inputs);


			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Done - $delta \n";
			// Write the contents back to the file
			file_put_contents($file, $current);
			/*file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=END|forever|delta|'. date("Y-m-d-h:i:sa"));*/
			$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
			$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
			$subject = 'Cron Forever Delta End';
			 
			$message = 'END|forever|delta|'. date("Y-m-d-h:i:sa");
	    	//$this->email($from, $to, $subject, $message);

		}else{
			$file = 'resource/forever_json/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Failed - $delta \n";
			// Write the contents back to the file
			file_put_contents($file, $current);
			//file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=FAILED|forever|delta|'. date("Y-m-d-h:i:sa"));
			$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
			$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
			$subject = 'Cron Forever Delta Failed';
			 
			$message = 'FAILED|forever|delta|'. date("Y-m-d-h:i:sa");
	    	//$this->email($from, $to, $subject, $message);
		}
		
    }
	
	//--------------Forever Data Processed and add
    function colorbuckket_json_get(){  
    	/*echo date("Y-m-d-h:i:sa");  	
    	exit;*/
		$inputs['url'] = '';
        $inputs['api_key'] = $_REQUEST['scapi_key'];        
        $inputs['secret_key'] = $_REQUEST['scsecret_key'];        
        $inputs['products'] = $_REQUEST['products'];        
        $this->bulk_uploadproducts_get($inputs);
    }

    //--------------brown_boy Data Processed and add
    function brown_boy_update_get(){   
		//Total count product in shopify
		/*file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=START|BrownBoy|'. date("Y-m-d-h:i:sa"));*/
		$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron BrownBoy Start';
		 
		$message = 'START|BrownBoy|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");
		//Total count product in shopify
		@$product_count_url = file_get_contents('https://444b687cbafcb3af3eafa522c191fffe:865817ecd4ac7b1e11c0af01149d62c7@brown-boy-india.myshopify.com/admin/products/count.json?published_status=published');
		$json_product = json_decode($product_count_url, true);
		$total_product_count = $json_product['count'];
		//$total_product_count = 10;
		$length = 100;
		$offset = 0;
		$pro_count = -1;
		while($length*$offset <= $total_product_count){
			$page = $offset + 1;
		  	@$str = file_get_contents('https://444b687cbafcb3af3eafa522c191fffe:865817ecd4ac7b1e11c0af01149d62c7@brown-boy-india.myshopify.com/admin/products.json?limit=250&page='.$page.'&published_status=published');    	
			$json = json_decode($str, true);
			/*echo '<pre>';
			print_r($json);	*/			
			if(!empty($json)){			
			foreach ($json as $field => $value) {		
				foreach ($value as $key) {							
					foreach ($key['variants'] as $row) {
				  	   $pro_count++;			   
					   $sku = strip_tags($key['variants'][0]['sku']);			   
					   if($sku != ""){
					   		$data[$pro_count]['product_sku'] = strip_tags($row['sku']);
					   }else{
					   		$data[$pro_count]['product_sku'] = strip_tags($row['id']);
					   }					   
					   $data[$pro_count]['brand_product_id'] = strip_tags($key['id']);
					   $data[$pro_count]['product_category'] = strip_tags($key['product_type']);
					   $data[$pro_count]['product_subcategory'] = "";
					   $data[$pro_count]['product_name'] = strip_tags($key['title']);
					   $add_url =  strip_tags($key['handle']);
					   $data[$pro_count]['product_url'] = strip_tags("https://www.brownboy.in/products/".$add_url);					   
					   $remove[] = ",";
							$remove[] = "&amp;amp;";
							$remove[] = '&lt;/p&gt;';
							$remove[] = 'br /&gt;';
							$remove[] = '&lt;/p&gt;';
							$remove[] = "/n";
							$remove[] = "/r";
							$remove[] = "quot;";
							$remove[] = "quot;quot;";
							$remove[] = "quot;quot;";
							$str="";					
						$data[$pro_count]['product_description'] = html_entity_decode(strip_tags(trim(strip_tags($key['body_html']))), ENT_QUOTES);	
						/*$price = strip_tags($key['variants'][0]['compare_at_price']);		
					   $data[$pro_count]['price'] = $price;*/
					   $compare_at_price = strip_tags($key['variants'][0]['compare_at_price']);		
						if($compare_at_price == '')
							$data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);					   		
						else
							$data[$pro_count]['price'] = strip_tags($key['variants'][0]['compare_at_price']);
					   $data[$pro_count]['age_group'] = "";					   
					   @$id = trim($row['id']);							   					   					   
					   @$images_variant_ids = strip_tags($key['images'][0]['variant_ids'][0]);													   
						if(!isset($data[$pro_count]['product_img']))
							$data[$pro_count]['product_img']="";
						$imgs = $key['images'];
						foreach ($imgs as $img_row) {
							if(isset($img_row['variant_ids'][0]))			    						
								@$images_ids= trim($img_row['variant_ids'][0]);
							if($id == @$images_ids){								  
								$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }
							 if($row['option1']=="Default Title"){
							 	$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }					 
							 if(@$img_row['variant_ids'][0] == "" && @$id != @$images_ids && $row['option1']!="Default Title"){
							 	$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }						         
						}					   
					   //$size = strip_tags($row['option1']);
						$get_size = strip_tags($row['option1']);
							if($get_size =='Default Title'){
								$size = 'One Size';
							}else{
								$size = strip_tags($row['option1']);
							}		
					   $inventory_quantity = strip_tags($row['inventory_quantity']);						   
					   $stock = array($size => strip_tags($row['inventory_quantity']));					   
					   $data[$pro_count]['product_stock'][] = $stock;
					   $data[$pro_count]['product_color'] = "";
					   /*$data[$pro_count]['product_color'] = "";		
					   $data[$pro_count]['product_discount'] = "";		
					   $data[$pro_count]['discount_start_date'] = "";		
					   $data[$pro_count]['discount_end_date'] = "";*/
					   /*$discount_amount = strip_tags($key['variants'][0]['price']);					   
					   $data[$pro_count]['product_discount'] = $discount_amount;
					   $data[$pro_count]['discount_start_date'] = date("Y/m/d",strtotime("0 day")).'T00:00:00+00:00';		
					   $data[$pro_count]['discount_end_date'] = date("Y/m/d",strtotime("0 day")).'T23:59:59+23:59';
					   $discount = $this->discount_calculation($discount_amount,$price);
					   $data[$pro_count]['discount_percentage'] = $discount;*/
					   
					   if ($compare_at_price == ''):
							$data[$pro_count]['product_discount'] = "";		
							$data[$pro_count]['discount_start_date'] = "";		
							$data[$pro_count]['discount_end_date'] = "";								   
							$data[$pro_count]['discount_percentage'] = "";
						else:
							$price = strip_tags($key['variants'][0]['compare_at_price']);		
							$discount_amount = strip_tags($key['variants'][0]['price']);					   
							$data[$pro_count]['product_discount'] = $discount_amount;
							$data[$pro_count]['discount_start_date'] = date("Y/m/d",strtotime("0 day")).'T00:00:00+00:00';		
							$data[$pro_count]['discount_end_date'] = date("Y/m/d",strtotime("0 day")).'T23:59:59+23:59';
							$discount = $this->discount_calculation($discount_amount,$price);
							$data[$pro_count]['discount_percentage'] = $discount;	
						endif;
					   $tags =  strip_tags($key['tags']);							   
					   $tags_exp = explode(",",$tags);											   			   
						   foreach($tags_exp as $tag_value){
						   	$data[$pro_count]['product_tags'][] = $tag_value;					   
						   }
					}		
				}
			}
		}else{
				echo "Data not found";
			 }	 
			$offset++;
		}				
		//exit;
		$product_json1 = json_encode($data);
		/*echo '<pre>';
		print_r($product_json1);
		exit;		*/
		$url = "resource/brown_boy/brown_boy.json";
		$myfile = fopen($url, "w") or die("Unable to open file!");
		$txt = $product_json1;
		if(fwrite(@$myfile, @$txt)){
			$file = "resource/brown_boy/logs.txt";
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Done\n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}else{
			$file = 'resource/brown_boy/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Failed\n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}
		fclose($myfile);
		
		$inputs['url'] = "";
        $inputs['api_key'] = trim("JFDV7a+NeGzNwHoQ20eMpJ/LMbeSeJM5");
        $inputs['secret_key'] = trim("sffVVgCUVs7GR7MHmHe+n51O49cP9sitKXi73+XpDNjyrsr2hOUWng==");
        $inputs['products'] = $product_json1; 
        $inputs['delete_inventory'] = true;
        $this->bulk_uploadproducts_get($inputs);

		/*file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=END|BrownBoy|'. date("Y-m-d-h:i:sa"));*/
		$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron BrownBoy End';
		 
		$message = 'END|BrownBoy|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");
	}
//--------------brown_boy Data Processed and end


//--------------be bajrang Data Processed and add
    function be_bajrang_update_get(){  
    	/*file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=START|beBajrang|'. date("Y-m-d-h:i:sa")); */
    	$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron beBajrang Start';
		 
		$message = 'START|beBajrang|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");

    	//Total count product in shopify
		@$product_count_url = file_get_contents('https://43652801ed3124cb1325e9071ad8fe18:023ca9d575355ab7598ff83fd0d11623@be-bajrang.myshopify.com/admin/products/count.json?published_status=published');
		$json_product = json_decode($product_count_url, true);
		$total_product_count = $json_product['count'];
		//$total_product_count = 10;
		$length = 100;
		$offset = 0;
		$pro_count = -1;
		while($length*$offset <= $total_product_count){
			$page = $offset + 1;
		  	@$str = file_get_contents('https://43652801ed3124cb1325e9071ad8fe18:023ca9d575355ab7598ff83fd0d11623@be-bajrang.myshopify.com/admin/products.json?limit=250&page='.$page.'&published_status=published');    	
			$json = json_decode($str, true);				
			/*echo '<pre>';
			print_r($json);*/
			if(!empty($json)){			
			foreach ($json as $field => $value) {		
				foreach ($value as $key) {							
					foreach ($key['variants'] as $row) {
				  	   $pro_count++;			   
					   $sku = strip_tags($key['variants'][0]['sku']);			   
					   if($sku != ""){
					   		$data[$pro_count]['product_sku'] = strip_tags($row['sku']);
					   }else{
					   		$data[$pro_count]['product_sku'] = strip_tags($row['id']);
					   }
					   $data[$pro_count]['brand_product_id'] = strip_tags($key['id']);
					   $data[$pro_count]['product_category'] = strip_tags($key['product_type']);
					   $data[$pro_count]['product_subcategory'] = "";
					   $data[$pro_count]['product_name'] = strip_tags($key['title']);
					   $add_url =  strip_tags($key['handle']);
					   $data[$pro_count]['product_url'] = strip_tags("http://bebajrang.com/products/".$add_url);					   
					   $remove[] = ",";
							$remove[] = "&amp;amp;";
							$remove[] = '&lt;/p&gt;';
							$remove[] = 'br /&gt;';
							$remove[] = '&lt;/p&gt;';
							$remove[] = "/n";
							$remove[] = "/r";
							$remove[] = "quot;";
							$remove[] = "quot;quot;";
							$remove[] = "quot;quot;";
							$str="";					
					   $data[$pro_count]['product_description'] = html_entity_decode(strip_tags(trim(strip_tags($key['body_html']))), ENT_QUOTES);	
					   $compare_at_price = strip_tags($key['variants'][0]['compare_at_price']);		
					   if($compare_at_price == '')
							$data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);					   		
					   else
					   		$data[$pro_count]['price'] = strip_tags($key['variants'][0]['compare_at_price']);		
					   $data[$pro_count]['age_group'] = "";					   
					   @$id = trim($row['id']);							   					   					   
					   @$images_variant_ids = strip_tags($key['images'][0]['variant_ids'][0]);													   
						if(!isset($data[$pro_count]['product_img']))
							$data[$pro_count]['product_img']="";
						$imgs = $key['images'];
						foreach ($imgs as $img_row) {
							if(isset($img_row['variant_ids'][0]))			    						
								@$images_ids= trim($img_row['variant_ids'][0]);
							if($id == @$images_ids){								  
								$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }
							 if($row['option1']=="Default Title"){
							 	$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }					 
							 if(@$img_row['variant_ids'][0] == "" && @$id != @$images_ids && $row['option1']!="Default Title"){
							 	$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }						         
						}					   
					   //$size = strip_tags($row['option1']);
						$get_size = strip_tags($row['option1']);
						   if($get_size =='Default Title'){
						   		$size = 'One Size';
						   }else{
						   		$size = strip_tags($row['option1']);
						   }	
					   $inventory_quantity = strip_tags($row['inventory_quantity']);						   
					   $stock = array($size => strip_tags($row['inventory_quantity']));					   
					   $data[$pro_count]['product_stock'][] = $stock;
					   $data[$pro_count]['product_color'] = strip_tags($row['option1']);		
					   if ($compare_at_price == ''):
							$data[$pro_count]['product_discount'] = "";		
							$data[$pro_count]['discount_start_date'] = "";		
							$data[$pro_count]['discount_end_date'] = "";								   
							$data[$pro_count]['discount_percentage'] = "";
					   else:
							$price = strip_tags($key['variants'][0]['compare_at_price']);		
							$discount_amount = strip_tags($key['variants'][0]['price']);					   
							$data[$pro_count]['product_discount'] = $discount_amount;
							$data[$pro_count]['discount_start_date'] = date("Y/m/d",strtotime("0 day")).'T00:00:00+00:00';		
							$data[$pro_count]['discount_end_date'] = date("Y/m/d",strtotime("0 day")).'T23:59:59+23:59';
							$discount = $this->discount_calculation($discount_amount,$price);
							$data[$pro_count]['discount_percentage'] = $discount;	
						endif;	
					   $tags =  strip_tags($key['tags']);							   
					   $tags_exp = explode(",",$tags);											   			   
						   foreach($tags_exp as $tag_value){
						   	$data[$pro_count]['product_tags'][] = $tag_value;					   
						   }
					}		
				}
			}
		}else{
				echo "Data not found";
			 }	 
			$offset++;
		}
		$product_json1 = json_encode($data);				
		/*echo '<pre>';
		print_r($product_json1);	
		exit;*/
		$url = "resource/be_bajrang/be_bajrang.json";
		$myfile = fopen($url, "w") or die("Unable to open file!");
		$txt = $product_json1;
		if(fwrite($myfile, $txt)){
			$file = "resource/be_bajrang/logs.txt";
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			//$current .= date("dd/mm/Y H:i:s")."Done -  - $master\n";
			$current .= date("dd/mm/Y H:i:s")."Done\n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}else{
			$file = 'resource/be_bajrang/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Failed\n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}
		fclose($myfile);

		$inputs['url'] = "";
        $inputs['api_key'] = trim("80IWmfa6/RTal11zhadcXYQTQ2m8gIbq");
        $inputs['secret_key'] = trim("W9MCyrj9988HJt1bD0F1COgIpwXjQzhvZsTx+x+/UpjhrJ+/S/Rlbg==");
        $inputs['products'] = $product_json1; 
        $inputs['delete_inventory'] = true;
        $this->bulk_uploadproducts_get($inputs);
		/*file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=END|beBajrang|'. date("Y-m-d-h:i:sa"));*/
		$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron beBajrang End';
		 
		$message = 'END|beBajrang|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");
	}
//--------------be_bajrang Data Processed and end

//--------------LeCalla Data Processed and add
    function LeCalla_update_get(){   
    	/*file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=START|lecalla|'. date("Y-m-d-h:i:sa"));*/
    	$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron lecalla Start';		 
		$message = 'START|lecalla|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);
    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");
		//Total count product in shopify
			@$product_count_url = file_get_contents('https://a6239e2494377e1e1afead3fa75b3f49:c48a64eb35b76ec7704412d0af2972ce@myfj-2.myshopify.com/admin/products/count.json?published_status=published');
			$json_product = json_decode($product_count_url, true);
			$total_product_count = $json_product['count'];
			//$total_product_count = 10;
			$length = 100;
			$offset = 0;
			$pro_count = -1;
			while($length*$offset <= $total_product_count){
				$page = $offset + 1;
			  	@$str = file_get_contents('https://a6239e2494377e1e1afead3fa75b3f49:c48a64eb35b76ec7704412d0af2972ce@myfj-2.myshopify.com/admin/products.json?limit='.$length.'&page='.$page.'&published_status=published');    	
				$json = json_decode($str, true);					
				/*echo '<pre>';
				print_r($json);*/
				if(!empty($json)){			
				foreach ($json as $field => $value) {		
					foreach ($value as $key) {							
						foreach ($key['variants'] as $row) {
					  	   $pro_count++;			   
							   $sku = strip_tags($key['variants'][0]['sku']);			   
						   if($sku != ""){
						   		$data[$pro_count]['product_sku'] = strip_tags($row['sku']);
						   }else{
						   		$data[$pro_count]['product_sku'] = strip_tags($row['id']);
						   }
						   $data[$pro_count]['brand_product_id'] = strip_tags($key['id']);
						   $data[$pro_count]['product_category'] = strip_tags($key['product_type']);
						   $data[$pro_count]['product_subcategory'] = "";
						   $data[$pro_count]['product_name'] = strip_tags($key['title']);
						   $add_url =  strip_tags($key['handle']);
						   $data[$pro_count]['product_url'] = strip_tags("http://www.lecalla.in/products/".$add_url);					   
						   $remove[] = ",";
								$remove[] = "&amp;amp;";
								$remove[] = '&lt;/p&gt;';
								$remove[] = 'br /&gt;';
								$remove[] = '&lt;/p&gt;';
								$remove[] = "/n";
								$remove[] = "/r";
								$remove[] = "quot;";
								$remove[] = "quot;quot;";
								$remove[] = "quot;quot;";
								$str="";					
							$data[$pro_count]['product_description'] = html_entity_decode(strip_tags(trim(strip_tags($key['body_html']))), ENT_QUOTES);	
						   //$data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);
							$compare_at_price = strip_tags($key['variants'][0]['compare_at_price']);		
							   if($compare_at_price == '')
									$data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);					   		
							   else
							   		$data[$pro_count]['price'] = strip_tags($key['variants'][0]['compare_at_price']);		
						   $data[$pro_count]['age_group'] = "";					   
						   @$id = trim($row['id']);							   					   					   
						   @$images_variant_ids = strip_tags($key['images'][0]['variant_ids'][0]);													   
							if(!isset($data[$pro_count]['product_img']))
								$data[$pro_count]['product_img']="";
							$imgs = $key['images'];
							foreach ($imgs as $img_row) {
								if(isset($img_row['variant_ids'][0]))			    						
									@$images_ids= trim($img_row['variant_ids'][0]);
								if($id == @$images_ids){								  
									$images_url = strip_tags($img_row['src']);
								    $images_url_one = explode("?",$images_url);							   
								    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
								    unset($images_url_one);
								 }					 					 
								 if(@$img_row['variant_ids'][0] == "" && @$id != @$images_ids){
								 	$images_url = strip_tags($img_row['src']);
								    $images_url_one = explode("?",$images_url);							   
								    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
								    unset($images_url_one);
								 }						         
							}					   
						   //$size = strip_tags($row['option1']);
						   $get_size = strip_tags($row['option1']);
							   if($get_size =='Default Title'){
							   		$size = 'One Size';
							   }else{
							   		$size = strip_tags($row['option1']);
							   }		
						   $inventory_quantity = strip_tags($row['inventory_quantity']);						   
						   $stock = array($size => strip_tags($row['inventory_quantity']));					   
						   $data[$pro_count]['product_stock'][] = $stock;
						   $data[$pro_count]['product_color'] = strip_tags($row['option1']);		
						   /*$data[$pro_count]['product_discount'] = "";		
						   $data[$pro_count]['discount_start_date'] = "";		
						   $data[$pro_count]['discount_end_date'] = "";*/
							if ($compare_at_price == ''):
								$data[$pro_count]['product_discount'] = "";		
								$data[$pro_count]['discount_start_date'] = "";		
								$data[$pro_count]['discount_end_date'] = "";								   
								$data[$pro_count]['discount_percentage'] = "";
							else:
								$price = strip_tags($key['variants'][0]['compare_at_price']);		
								$discount_amount = strip_tags($key['variants'][0]['price']);					   
								$data[$pro_count]['product_discount'] = $discount_amount;
								$data[$pro_count]['discount_start_date'] = date("Y/m/d",strtotime("0 day")).'T00:00:00+00:00';		
								$data[$pro_count]['discount_end_date'] = date("Y/m/d",strtotime("0 day")).'T23:59:59+23:59';
								$discount = $this->discount_calculation($discount_amount,$price);
								$data[$pro_count]['discount_percentage'] = $discount;	
							endif;									   
						   $tags =  strip_tags($key['tags']);							   
						   $tags_exp = explode(",",$tags);											   			   
							   foreach($tags_exp as $tag_value){
							   	$data[$pro_count]['product_tags'][] = $tag_value;					   
							   }
						}		
					}
				}
			}else{
					echo "Data not found";
				 }	 
				$offset++;
			}
		//exit;
		$product_json1 = json_encode($data);	
		/*echo '<pre>';
		print_r($product_json1);
		exit;*/
		$url = "resource/LeCalla/LeCalla.json";
		$myfile = fopen($url, "w") or die("Unable to open file!");
		$txt = $product_json1;
		if(fwrite($myfile, $txt)){
			$file = "resource/LeCalla/logs.txt";
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Done\n";
			//$current .= date("dd/mm/Y H:i:s")."Done";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}else{
			$file = 'resource/LeCalla/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Failed\n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}
		fclose($myfile);

		$inputs['url'] = "";
        $inputs['api_key'] = trim("tAVapAO3Kgl2yQZ4no/VNg==");
        $inputs['secret_key'] = trim("tZR22rLfVP+KOqzk949BjXKFPBvBZsUVkIUyOqfPR2uhwjhWbZQADg==");
        $inputs['products'] = $product_json1; 
        $inputs['delete_inventory'] = true;
        $this->bulk_uploadproducts_get($inputs);


		/*file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=END|Lecalla|'. date("Y-m-d-h:i:sa"));*/
		$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron lecalla End';
		 
		$message = 'END|lecalla|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");

	}
//--------------LeCalla Data Processed and end	

//--------------srstore Data Processed and add
	function srstore_update_get(){
		/*file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=START|srstores|'. date("Y-m-d-h:i:sa"));*/
		$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron srstores Start';		 
		$message = 'START|srstores|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);
    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");
		//Total count product in shopify
		@$product_count_url = file_get_contents('https://0872aa04e4e0c35a3c73d1fc3bd7ea26:86ff9453811c63e3200b122220a1523e@srstore09.myshopify.com/admin/products/count.json?published_status=published');
		$json_product = json_decode($product_count_url, true);
		$total_product_count = $json_product['count'];
		//$total_product_count = 10;
		$length = 100;
		$offset = 0;
		$pro_count = -1;
		while($length*$offset <= $total_product_count){
			$page = $offset + 1;
		  	@$str = file_get_contents('https://0872aa04e4e0c35a3c73d1fc3bd7ea26:86ff9453811c63e3200b122220a1523e@srstore09.myshopify.com/admin/products.json?limit=250&page='.$page.'&published_status=published');    	
			$json = json_decode($str, true);		
			if(!empty($json)){			
			foreach ($json as $field => $value) {		
				foreach ($value as $key) {							
					foreach ($key['variants'] as $row) {
						$sku = strip_tags($row['sku']);
						$sku_arr = explode(" ",$sku);
						$sku_arr_one = explode("-",$sku);
						$sku_value_one = $sku_arr[0];
						$sku_value_two = $sku_arr_one[0];
					   if($sku_value_one == "KRZ" || $sku_value_two == "KRZ"){	
					   		$pro_count++;
						   $sku = strip_tags($key['variants'][0]['sku']);	
							   if($sku != ""){
							   		$data[$pro_count]['product_sku'] = strip_tags($row['sku']);
							   }else{
							   		$data[$pro_count]['product_sku'] = strip_tags($row['id']);
							   }
						   $data[$pro_count]['brand_product_id'] = strip_tags($key['id']);						   
						   $data[$pro_count]['product_category'] = strip_tags($key['product_type']);
						   $data[$pro_count]['product_subcategory'] = "";
						   $data[$pro_count]['product_name'] = strip_tags($key['title']);
						   $add_url =  strip_tags($key['handle']);
						   $data[$pro_count]['product_url'] = strip_tags("https://srstore.in/products/".$add_url);		
						   $string = strip_tags($key['body_html']);
						   $description_two = str_replace("'", "", $string);	
						   $products = strip_tags(str_replace('"','',(trim($description_two))));
						   $data[$pro_count]['product_description'] = strip_tags($products);
						   $data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);
						   $data[$pro_count]['age_group'] = "";					   
						   @$id = trim($row['id']);							   					   					   
						   @$images_variant_ids = strip_tags($key['images'][0]['variant_ids'][0]);													   
							if(!isset($data[$pro_count]['product_img']))
								$data[$pro_count]['product_img']="";
							$imgs = $key['images'];
							$data[$pro_count]['product_img'] = array();							
							//---------------Get images dynamic
							foreach ($imgs as $img_row) {
								//if(isset($img_row['variant_ids'][0]))			    						
								foreach($img_row['variant_ids'] as $img_data_row){
									@$images_ids= trim($img_data_row);
									if($id == $images_ids){
									   	 //$data[$pro_count]['product_img'] .= strip_tags($img_row['src']) .',';
										$images_url = strip_tags($img_row['src']);
									    $images_url_one = explode("?",$images_url);							   
									    $data[$pro_count]['product_img'][] = $images_url_one[0];
									    unset($images_url_one);
									 }
								}						         
							}
							//---------------If images not found then add all images to product														
							if(!isset($data[$pro_count]['product_img']) || count($data[$pro_count]['product_img']) == 0){								
								foreach ($imgs as $img_row) {
									//if(isset($img_row['variant_ids'][0]))			    						
										   	 //$data[$pro_count]['product_img'] .= strip_tags($img_row['src']) .',';
									if(!isset($img_row['variant_ids'][0])){
										$images_url = strip_tags($img_row['src']);
									    $images_url_one = explode("?",$images_url);							   
									    $data[$pro_count]['product_img'][] = $images_url_one[0];
									    unset($images_url_one);
									}
								}
							}
						   $size = strip_tags($row['option1']);
						   $inventory_quantity = strip_tags($row['inventory_quantity']);
						   //$stock = array();
						   $stock = array($size => strip_tags($row['inventory_quantity']));					   
						   //$c = array_combine($size, $inventory_quantity);					   
						   /*echo '<pre>';
						   print_r($stock);*/
						   //$data[$pro_count][$size] = strip_tags($row['inventory_quantity']);
						   $data[$pro_count]['product_stock'][] = $stock;
						   //$data[$pro_count]['product_stock'] = array( strip_tags($row['inventory_quantity']);					   
						   $data[$pro_count]['product_color'] = strip_tags($row['option2']);;		
						   $data[$pro_count]['product_discount'] = "";		
						   $data[$pro_count]['discount_start_date'] = "";		
						   $data[$pro_count]['discount_end_date'] = "";		
						   //$data[$pro_count]['product_tags'] = "";
						   $tags =  strip_tags($key['tags']);							   
						   $tags_exp = explode(",",$tags);											   
						   //if (is_array($tags_exp) || is_object($tags_exp)){
							   foreach($tags_exp as $tag_value){
							   	$data[$pro_count]['product_tags'][] = $tag_value;					   
							   }
							//}					   			    					   
						   //@$data[$pro_count]['product_color'] = strip_tags($row['option2']);					   
						}			
					}		
				}
			}
		}else{
				echo "Data not found";
			 }	 
			$offset++;
		}
		$product_json1 = json_encode($data);						
		$url = "resource/srstore/srstore.json";
		$myfile = fopen($url, "w") or die("Unable to open file!");
		$txt = $product_json1;
		if(fwrite($myfile, $txt)){
			$file = "resource/srstore/logs.txt";
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Done\n";
			//$current .= date("dd/mm/Y H:i:s")."Done";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}else{
			$file = 'resource/srstore/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Failed\n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}
		fclose($myfile);
		$inputs['url'] = "";
        $inputs['api_key'] = trim("Tt2eszJ48Phsc+PGCYVtCnSX6F/94dSy");
        $inputs['secret_key'] = trim("DqTD+W8SI+pybjorGYO/x2RRPsz0H43btM5Qy1wrgcJvTWpIQWuoXQ==");
        $inputs['products'] = $product_json1;         
        $inputs['delete_inventory'] = true;
        $this->bulk_uploadproducts_get($inputs);
		/*file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=END|srstore|'. date("Y-m-d-h:i:sa"));*/
		$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron srstores End';		 
		$message = 'END|srstores|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);
    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");
	}
//--------------srstore Data Processed and end

//--------------Upbeatz Data Processed and add
    function Upbeatz_update_get(){   
    	/*file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=START|Upbeatz|'. date("Y-m-d-h:i:sa"));*/
    	$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron Upbeatz Start';
		 
		$message = 'START|Upbeatz|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");

    	//Total count product in shopify
		@$product_count_url = file_get_contents('https://3261768638da7cc69812a937cc861beb:42ee084e99d30e08ead3908b2f9cecad@upbeatz.myshopify.com/admin/products/count.json?published_status=published&handle=aquamarine-smoke-classic-premiums');
		$json_product = json_decode($product_count_url, true);
		$total_product_count = $json_product['count'];
		//$total_product_count = 10;
		$length = 100;
		$offset = 0;
		$pro_count = -1;
		while($length*$offset <= $total_product_count){
			$page = $offset + 1;
		  	//@$str = file_get_contents('https://3261768638da7cc69812a937cc861beb:42ee084e99d30e08ead3908b2f9cecad@upbeatz.myshopify.com/admin/products.json?limit=250&page='.$page.'&published_status=published');    	
		  	@$str = file_get_contents('https://3261768638da7cc69812a937cc861beb:42ee084e99d30e08ead3908b2f9cecad@upbeatz.myshopify.com/admin/products.json?handle=aquamarine-smoke-classic-premiums');    	
			$json = json_decode($str, true);				
			/*echo '<pre>';
			print_r($json);*/
			if(!empty($json)){			
			foreach ($json as $field => $value) {		
				foreach ($value as $key) {							
					foreach ($key['variants'] as $row) {
				  	   $pro_count++;			   					   
					   $data[$pro_count]['product_sku'] = strip_tags($row['sku']);
					   $data[$pro_count]['brand_product_id'] = strip_tags($key['id']);
					   $data[$pro_count]['product_category'] = strip_tags($key['product_type']);
					   $data[$pro_count]['product_subcategory'] = "";
					   $data[$pro_count]['product_name'] = strip_tags($key['title']);
					   $add_url =  strip_tags($key['handle']);
					   $data[$pro_count]['product_url'] = strip_tags("https://www.upbeatz.com/products/".$add_url);					   
					   $remove[] = ",";
							$remove[] = "&amp;amp;";
							$remove[] = '&lt;/p&gt;';
							$remove[] = 'br /&gt;';
							$remove[] = '&lt;/p&gt;';
							$remove[] = "/n";
							$remove[] = "/r";
							$remove[] = "quot;";
							$remove[] = "quot;quot;";
							$remove[] = "quot;quot;";
							$str="";					
						$data[$pro_count]['product_description'] = html_entity_decode(strip_tags(trim(strip_tags($key['body_html']))), ENT_QUOTES);	
					   //$data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);
						$compare_at_price = strip_tags($key['variants'][0]['compare_at_price']);		
						   if($compare_at_price == '')
								$data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);					   		
						   else
						   		$data[$pro_count]['price'] = strip_tags($key['variants'][0]['compare_at_price']);	
					   $data[$pro_count]['age_group'] = "";					   
					   @$id = trim($row['id']);							   					   					   
					   @$images_variant_ids = strip_tags($key['images'][0]['variant_ids'][0]);													   
						if(!isset($data[$pro_count]['product_img']))
							$data[$pro_count]['product_img']="";
						$imgs = $key['images'];
						foreach ($imgs as $img_row) {
							if(isset($img_row['variant_ids'][0]))			    						
								@$images_ids= trim($img_row['variant_ids'][0]);
							if($id == @$images_ids){								  
								$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }
							 if($row['option1']=="Default Title"){
							 	$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }					 
							 if(@$img_row['variant_ids'][0] == "" && @$id != @$images_ids && $row['option1']!="Default Title"){
							 	$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }						         
						}					   
					   //$size = strip_tags($row['option1']);
						$get_size = strip_tags($row['option1']);
							if($get_size =='Default Title'){
								$size = 'One Size';
							}else{
								$size = strip_tags($row['option1']);
							}
					   $inventory_quantity = strip_tags($row['inventory_quantity']);						   
					   $stock = array($size => strip_tags($row['inventory_quantity']));					   
					   $data[$pro_count]['product_stock'][] = $stock;					   
					   $data[$pro_count]['product_color'] = "";		
					   /*$data[$pro_count]['product_discount'] = "";		
					   $data[$pro_count]['discount_start_date'] = "";		
					   $data[$pro_count]['discount_end_date'] = "";*/
					   if ($compare_at_price == ''):
							$data[$pro_count]['product_discount'] = "";		
							$data[$pro_count]['discount_start_date'] = "";		
							$data[$pro_count]['discount_end_date'] = "";								   
							$data[$pro_count]['discount_percentage'] = "";
					   else:
							$price = strip_tags($key['variants'][0]['compare_at_price']);		
							$discount_amount = strip_tags($key['variants'][0]['price']);					   
							$data[$pro_count]['product_discount'] = $discount_amount;
							$data[$pro_count]['discount_start_date'] = date("Y/m/d",strtotime("0 day")).'T00:00:00+00:00';		
							$data[$pro_count]['discount_end_date'] = date("Y/m/d",strtotime("0 day")).'T23:59:59+23:59';
							$discount = $this->discount_calculation($discount_amount,$price);
							$data[$pro_count]['discount_percentage'] = $discount;	
						endif;									   
					   $tags =  strip_tags($key['tags']);							   
					   $tags_exp = explode(",",$tags);											   			   
						   foreach($tags_exp as $tag_value){
						   	$data[$pro_count]['product_tags'][] = $tag_value;					   
						   }
					}		
				}
			}
		}else{
				echo "Data not found";
			 }	 
			$offset++;
		}		
		$product_json1 = json_encode($data);			
		/*echo '<pre>';
		print_r($product_json1);
		exit;*/
		$url = "resource/Upbeatz/Upbeatz.json";
		$myfile = fopen($url, "w") or die("Unable to open file!");
		$txt = $product_json1;
		if(fwrite($myfile, $txt)){
			$file = "resource/Upbeatz/logs.txt";
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Done\n";
			//$current .= date("dd/mm/Y H:i:s")."Done";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}else{
			$file = 'resource/Upbeatz/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Failed";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}
		fclose($myfile);
		$inputs['url'] = "";
        $inputs['api_key'] = trim("MYS6LiO8e19IR90jj5UGeg==");
        $inputs['secret_key'] = trim("DqTD+W8SI+pybjorGYO/x2RRPsz0H43btM5Qy1wrgcJvTWpIQWuoXQ==");
        $inputs['products'] = $product_json1; 
        $inputs['delete_inventory'] = true;
        $this->bulk_uploadproducts_get($inputs);
		/*file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=END|Upbeatz|'. date("Y-m-d-h:i:sa"));*/
		$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron Kyonio End';
		 
		$message = 'END|Upbeatz|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");

	}
//--------------Upbeatz Data Processed and end		
    


//--------------zooomberg Data Processed and add		
    function zooomberg_json_update_get(){
    	///file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=START|zooomberg|'. date("Y-m-d-h:i:sa"));
    	$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron zooomberg Start';
		 
		$message = 'START|zooomberg|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");

		//Total count product in shopify
		@$product_count_url = file_get_contents('https://394a698dd829cdebeb1c869066138402:e737461b50fca6c47b0be0b75da9e7e5@zooomberg.myshopify.com/admin/products/count.json?published_status=published');
		$json_product = json_decode($product_count_url, true);
		$total_product_count = $json_product['count'];
		//$total_product_count = 10;
		$length = 100;
		$offset = 0;
		$pro_count = -1;
		while($length*$offset <= $total_product_count){
			$page = $offset + 1;
		  	@$str = file_get_contents('https://394a698dd829cdebeb1c869066138402:e737461b50fca6c47b0be0b75da9e7e5@zooomberg.myshopify.com/admin/products.json?limit=250&page='.$page.'&published_status=published');    	
			$json = json_decode($str, true);			
			$last_product_id=0;
			if(!empty($json)){			
			foreach ($json as $field => $value) {		
				foreach ($value as $key) {							
						foreach ($key['variants'] as $row) {
								$product_type =  strip_tags($key['product_type']);		
								$product_type = strip_tags($key['product_type']);
								$product_type_array = explode(" ",$product_type);
								   if(@$product_type_array[2] == 'Sale'){								
							   	   $pro_count++;
							   	   $sku = strip_tags($key['variants'][0]['sku']);	
									   if($sku != ""){
									   		$data[$pro_count]['product_sku'] = strip_tags($row['sku']);
									   }else{
									   		$data[$pro_count]['product_sku'] = strip_tags($row['id']);
									   }
								   $data[$pro_count]['brand_product_id'] = strip_tags($key['id']);
								   $data[$pro_count]['product_category'] = strip_tags($key['product_type']);
								   $data[$pro_count]['product_subcategory'] = "";
								   $data[$pro_count]['product_name'] = strip_tags($key['title']);
								   $add_url =  strip_tags($key['handle']);
								   $data[$pro_count]['product_url'] = strip_tags("http://www.zooomberg.com/products/".$add_url);	
			/*					   echo "https://srstore.in/products/".$add_url .'<br/>';*/
								   $string = strip_tags($key['body_html']);
								   $description_two = str_replace("'", "", $string);	
								   $products = strip_tags(str_replace('"','',(trim($description_two))));
								   $data[$pro_count]['product_description'] = strip_tags($products);
								   $data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);
								   $data[$pro_count]['age_group'] = "";					   
								   @$id = trim($row['id']);	
								   					   					   
								   @$images_variant_ids = strip_tags($key['images'][0]['variant_ids'][0]);													   
									if(!isset($data[$pro_count]['product_img']))
										$data[$pro_count]['product_img']="";
									$imgs = $key['images'];

									$data[$pro_count]['product_img'] = array();
									
									//---------------Get images dynamic
									foreach ($imgs as $img_row) {
										//if(isset($img_row['variant_ids'][0]))			    						
										foreach($img_row['variant_ids'] as $img_data_row){
											@$images_ids= trim($img_data_row);
											if($id == $images_ids){
											   	 //$data[$pro_count]['product_img'] .= strip_tags($img_row['src']) .',';
												$images_url = strip_tags($img_row['src']);
											    $images_url_one = explode("?",$images_url);							   
											    $data[$pro_count]['product_img'][] = $images_url_one[0];
											    unset($images_url_one);
											 }
										}
								         
									}

									//---------------If images not found then add all images to product														
									if(!isset($data[$pro_count]['product_img']) || count($data[$pro_count]['product_img']) == 0){								
										foreach ($imgs as $img_row) {
											//if(isset($img_row['variant_ids'][0]))			    						
												   	 //$data[$pro_count]['product_img'] .= strip_tags($img_row['src']) .',';
											if(!isset($img_row['variant_ids'][0])){
												$images_url = strip_tags($img_row['src']);
											    $images_url_one = explode("?",$images_url);							   
											    $data[$pro_count]['product_img'][] = $images_url_one[0];
											    unset($images_url_one);
											}
										}
									}

								   $size = strip_tags($row['option1']);
								   $inventory_quantity = strip_tags($row['inventory_quantity']);
								   //$stock = array();
								   $stock = array($size => strip_tags($row['inventory_quantity']));					   								   
								   $data[$pro_count]['product_stock'][] = $stock;
								   if($last_product_id != $row['product_id']){
								   		$last_product_id = $row['product_id'];
								   		$count_product=0;
								   }
								   foreach($key['options'] as $col_data){
									   if(isset($col_data['values']) && $col_data['name'] == "Color"){
										   if(isset($row['option2']) && $row['option2'] != "")
										   		$data[$pro_count]['product_color'] = strip_tags($row['option2']);
										   else if(isset($row['option1']) && $row['option1'] != "")
										   		$data[$pro_count]['product_color'] = strip_tags($row['option1']);
										}
									}
								   $count_product++;
								   $data[$pro_count]['product_discount'] = "";		
								   $data[$pro_count]['discount_start_date'] = "";		
								   $data[$pro_count]['discount_end_date'] = "";		
								   //$data[$pro_count]['product_tags'] = "";
								   $tags =  strip_tags($key['tags']);							   
								   $tags_exp = explode(",",$tags);											   
								   //if (is_array($tags_exp) || is_object($tags_exp)){
									   foreach($tags_exp as $tag_value){
									   	$data[$pro_count]['product_tags'][] = $tag_value;					   
									   }
									//}					   			    					   
								   //@$data[$pro_count]['product_color'] = strip_tags($row['option2']);					   
							}			

						}		
					}

				}
			}else{
				echo "Data not found";
			 }	 
			$offset++;
		}		
		$product_json1 = json_encode($data);						
		$url = "resource/zooomberg/zooomberg.json";
		$myfile = fopen($url, "w") or die("Unable to open file!");
		$txt = $product_json1;
		if(fwrite($myfile, $txt)){
			$file = "resource/zooomberg/logs.txt";
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Done\n";
			//$current .= date("dd/mm/Y H:i:s")."Done";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}else{
			$file = 'resource/zooomberg/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Failed\n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}
		fclose($myfile);
		$inputs['url'] = "";
        $inputs['api_key'] = trim("WPVoOm/lgEnNQqm9pGUKIxHdy2J5GTyw");
        $inputs['secret_key'] = trim("9XwFgYe8A2ibs4Qabke9WLr31m51Ww2ZmVd/XLgE4wbsyNa7eWWP/w==");
        $inputs['products'] = $product_json1; 
        $inputs['delete_inventory'] = true;
        $this->bulk_uploadproducts_get($inputs);
		///file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=END-Zooomberg-'. date("Y-m-d-h:i:sa"));
		$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron zooomberg End';		 
		$message = 'END|zooomberg|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");

    }
//--------------zooomberg Data Processed and end



//--------------Kyonio Data Processed and add		
    function kyonio_json_update_get(){
    	///file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=START|zooomberg|'. date("Y-m-d-h:i:sa"));
    	$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron Kyonio Start';		 
		$message = 'START|Kyonio|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);
    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");
		//Total count product in shopify
			//@$product_count_url = file_get_contents('https://c401cbf05f2ce6996b88464aceeb6af6:63f3ce31fbf58731d889b84c0326f7de@city-chique.myshopify.com/admin/products/count.json?published_status=published&handle=statement-earrings');
			@$product_count_url = file_get_contents('https://c401cbf05f2ce6996b88464aceeb6af6:63f3ce31fbf58731d889b84c0326f7de@city-chique.myshopify.com/admin/products/count.json?published_status=published');
			$json_product = json_decode($product_count_url, true);
			$total_product_count = $json_product['count'];
			//$total_product_count = 10;
			$length = 100;
			$offset = 0;
			$pro_count = -1;
			while($length*$offset <= $total_product_count){
				$page = $offset + 1;
			  	//@$str = file_get_contents('https://c401cbf05f2ce6996b88464aceeb6af6:63f3ce31fbf58731d889b84c0326f7de@city-chique.myshopify.com/admin/products.json?limit='.$length.'&page='.$page.'&published_status=published&handle=statement-earrings');    	
			  	@$str = file_get_contents('https://c401cbf05f2ce6996b88464aceeb6af6:63f3ce31fbf58731d889b84c0326f7de@city-chique.myshopify.com/admin/products.json?limit='.$length.'&page='.$page.'&published_status=published');    	
				$json = json_decode($str, true);					
				if(!empty($json)){			
				foreach ($json as $field => $value) {		
					foreach ($value as $key) {							
						foreach ($key['variants'] as $row) {
					  	   $pro_count++;			   
						   $sku = strip_tags($key['variants'][0]['sku']);			   
						   if($sku != ""){
						   		$data[$pro_count]['product_sku'] = strip_tags($row['sku']);
						   }else{
						   		$data[$pro_count]['product_sku'] = strip_tags($row['id']);
						   }
						   $data[$pro_count]['brand_product_id'] = strip_tags($key['id']);
						   $data[$pro_count]['product_category'] = strip_tags($key['product_type']);
						   $data[$pro_count]['product_subcategory'] = "";
						   $data[$pro_count]['product_name'] = strip_tags($key['title']);
						   $add_url =  strip_tags($key['handle']);
						   $data[$pro_count]['product_url'] = strip_tags("https://www.kyonio.com/products/".$add_url);					   
						   $remove[] = ",";
								$remove[] = "&amp;amp;";
								$remove[] = '&lt;/p&gt;';
								$remove[] = 'br /&gt;';
								$remove[] = '&lt;/p&gt;';
								$remove[] = "/n";
								$remove[] = "/r";
								$remove[] = "quot;";
								$remove[] = "quot;quot;";
								$remove[] = "quot;quot;";
								$str="";					
							$data[$pro_count]['product_description'] = html_entity_decode(strip_tags(trim(strip_tags($key['body_html']))), ENT_QUOTES);	
						   $data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);
						   $data[$pro_count]['age_group'] = "";					   
						   @$id = trim($row['id']);						   					   					   
						   @$images_variant_ids = strip_tags($key['images'][0]['variant_ids'][0]);													   
							if(!isset($data[$pro_count]['product_img']))
								$data[$pro_count]['product_img']="";
							$imgs = $key['images'];
							$data[$pro_count]['product_img'] = array();							
							//---------------Get images dynamic
							foreach ($imgs as $img_row) {
								//if(isset($img_row['variant_ids'][0]))			    						
								foreach($img_row['variant_ids'] as $img_data_row){
									@$images_ids= trim($img_data_row);
									if($id == $images_ids){									   	 
										$images_url = strip_tags($img_row['src']);
									    $images_url_one = explode("?",$images_url);							   
									    $data[$pro_count]['product_img'][] = $images_url_one[0];
									    unset($images_url_one);
									 }
								}
						         
							}
							//---------------If images not found then add all images to product														
							if(!isset($data[$pro_count]['product_img']) || count($data[$pro_count]['product_img']) == 0){								
								foreach ($imgs as $img_row) {								
									if(!isset($img_row['variant_ids'][0])){
										$images_url = strip_tags($img_row['src']);
									    $images_url_one = explode("?",$images_url);							   
									    $data[$pro_count]['product_img'][] = $images_url_one[0];
									    unset($images_url_one);
									}
								}
							}						   
						   //$size = strip_tags($row['option1']);
							$get_size = strip_tags($row['option1']);
								if($get_size =='Default Title'){
									$size = 'One Size';
								}else{
									$size = strip_tags($row['option1']);
								}			
						   $inventory_quantity = strip_tags($row['inventory_quantity']);						   
						   $stock = array($size => strip_tags($row['inventory_quantity']));					   
						   $data[$pro_count]['product_stock'][] = $stock;
						   $data[$pro_count]['product_color'] = strip_tags($row['option1']);		
						   $data[$pro_count]['product_discount'] = "";		
						   $data[$pro_count]['discount_start_date'] = "";		
						   $data[$pro_count]['discount_end_date'] = "";								   
						   $tags =  strip_tags($key['tags']);							   
						   $tags_exp = explode(",",$tags);											   			   
							   foreach($tags_exp as $tag_value){
							   	$data[$pro_count]['product_tags'][] = $tag_value;					   
							   }
						}		
					}
				}
			}else{
					echo "Data not found";
				 }	 
				$offset++;
			}
		//exit;
		$product_json1 = json_encode($data);			
		/*echo '<pre>';
		print_r($product_json1);
		exit;*/
		$url = "resource/Kyonio/Kyonio.json";
		$myfile = fopen($url, "w") or die("Unable to open file!");
		$txt = $product_json1;
		if(fwrite($myfile, $txt)){
			$file = "resource/Kyonio/logs.txt";
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Done\n";
			//$current .= date("dd/mm/Y H:i:s")."Done";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}else{
			$file = 'resource/Kyonio/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Failed\n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}
		fclose($myfile);

		$inputs['url'] = "";
        $inputs['api_key'] = trim("65U2DWakoMI5CMLCHYXFwhhz5fu1+G9c");
        $inputs['secret_key'] = trim("O21mZWDpUbb1C7D24lhmJEJr5NMyejpBjnnarBqp8vTo06gE5lFGcA==");
        $inputs['products'] = $product_json1;         
        $inputs['delete_inventory'] = true;
        
        $this->bulk_uploadproducts_get($inputs);
		///file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=END-Zooomberg-'. date("Y-m-d-h:i:sa"));
		$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron Kyonio End';
		 
		$message = 'END|Kyonio|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");

    }
//--------------Kyonio Data Processed and end



//--------------loco_en_cabeza Data Processed and add		
    function loco_en_cabeza_json_update_get(){
    	///file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=START|zooomberg|'. date("Y-m-d-h:i:sa"));
    	$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron loco en cabeza Start';
		 
		$message = 'START|loco en cabeza|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");

		//Total count product in shopify
		@$product_count_url = file_get_contents('https://3a972675806cc209565a920d731d2a8f:a5faac53135a0c90f024c723783240bf@locoencabeza.myshopify.com/admin/products/count.json?published_status=published');
		$json_product = json_decode($product_count_url, true);
		$total_product_count = $json_product['count'];
		//$total_product_count = 10;
		$length = 100;
		$offset = 0;
		$pro_count = -1;
		while($length*$offset <= $total_product_count){
			$page = $offset + 1;
		  	@$str = file_get_contents('https://3a972675806cc209565a920d731d2a8f:a5faac53135a0c90f024c723783240bf@locoencabeza.myshopify.com/admin/products.json?limit=250&page='.$page.'&published_status=published');    	
			$json = json_decode($str, true);				
			/*echo '<pre>';
			print_r($json);*/
			if(!empty($json)){			
			foreach ($json as $field => $value) {		
				foreach ($value as $key) {							
					foreach ($key['variants'] as $row) {
				  	   $pro_count++;			   
					   $sku = strip_tags($key['variants'][0]['sku']);			   
					   if($sku != ""){
					   		$data[$pro_count]['product_sku'] = strip_tags($row['sku']);
					   }else{
					   		$data[$pro_count]['product_sku'] = strip_tags($row['id']);
					   }
					   $data[$pro_count]['brand_product_id'] = strip_tags($key['id']);
					   $data[$pro_count]['product_category'] = strip_tags($key['product_type']);
					   $data[$pro_count]['product_subcategory'] = "";
					   $data[$pro_count]['product_name'] = strip_tags($key['title']);
					   $add_url =  strip_tags($key['handle']);
					   $data[$pro_count]['product_url'] = strip_tags("https://locoencabeza.com/products/".$add_url);					   			  
						$remove[] = ",";
							$remove[] = "&amp;amp;";
							$remove[] = '&lt;/p&gt;';
							$remove[] = 'br /&gt;';
							$remove[] = '&lt;/p&gt;';
							$remove[] = "/n";
							$remove[] = "/r";
							$remove[] = "quot;";
							$remove[] = "quot;quot;";
							$remove[] = "quot;quot;";
							$str="";
							$products = html_entity_decode(strip_tags(trim(strip_tags($key['body_html']))), ENT_QUOTES);
					   //$data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);
					   $compare_at_price = strip_tags($key['variants'][0]['compare_at_price']);		
						   if($compare_at_price == '')
								$data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);					   		
						   else
						   		$data[$pro_count]['price'] = strip_tags($key['variants'][0]['compare_at_price']);		
					   $data[$pro_count]['age_group'] = "";					   
					   @$id = trim($row['id']);							   					   					   
					   @$images_variant_ids = strip_tags($key['images'][0]['variant_ids'][0]);													   
						if(!isset($data[$pro_count]['product_img']))
							$data[$pro_count]['product_img']="";
						$imgs = $key['images'];
						foreach ($imgs as $img_row) {
							if(isset($img_row['variant_ids'][0]))			    						
								@$images_ids= trim($img_row['variant_ids'][0]);
							if($id == @$images_ids){								  
								$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }
							 if($row['option1']=="Default Title"){
							 	$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }					 
							 if(@$img_row['variant_ids'][0] == "" && @$id != @$images_ids && $row['option1']!="Default Title"){
							 	$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }						         
						}					   
					   $size = strip_tags($row['option1']);
					   $inventory_quantity = strip_tags($row['inventory_quantity']);						   
					   $stock = array($size => strip_tags($row['inventory_quantity']));					   
					   $data[$pro_count]['product_stock'][] = $stock;
					   //$data[$pro_count]['product_color'] = "";					   
					   /*$data[$pro_count]['product_discount'] = "";		
					   $data[$pro_count]['discount_start_date'] = "";		
					   $data[$pro_count]['discount_end_date'] = "";*/								   
					   if ($compare_at_price == ''):
							$data[$pro_count]['product_discount'] = "";		
							$data[$pro_count]['discount_start_date'] = "";		
							$data[$pro_count]['discount_end_date'] = "";								   
							$data[$pro_count]['discount_percentage'] = "";
					   else:
							$price = strip_tags($key['variants'][0]['compare_at_price']);		
							$discount_amount = strip_tags($key['variants'][0]['price']);					   
							$data[$pro_count]['product_discount'] = $discount_amount;
							$data[$pro_count]['discount_start_date'] = date("Y/m/d",strtotime("0 day")).'T00:00:00+00:00';		
							$data[$pro_count]['discount_end_date'] = date("Y/m/d",strtotime("0 day")).'T23:59:59+23:59';
							$discount = $this->discount_calculation($discount_amount,$price);
							$data[$pro_count]['discount_percentage'] = $discount;	
						endif;	
					   $tags =  strip_tags($key['tags']);							   
					   $tags_exp = explode(",",$tags);											   			   
						   foreach($tags_exp as $tag_value){
						   	$data[$pro_count]['product_tags'][] = $tag_value;					   
						   }
					}		
				}
			}
		}else{
				echo "Data not found";
			 }	 
			$offset++;
		}		
		/*exit;*/
		$product_json1 = json_encode($data);				
		/*echo '<pre>';
		print_r($product_json1);
		exit;*/		
		$url = "resource/loco_en_cabeza/loco_en_cabeza.json";
		$myfile = fopen($url, "w") or die("Unable to open file!");
		$txt = $product_json1;
		if(fwrite($myfile, $txt)){
			$file = "resource/loco_en_cabeza/logs.txt";
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Done\n";
			//$current .= date("dd/mm/Y H:i:s")."Done";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}else{
			$file = 'resource/loco_en_cabeza/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Failed\n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}
		fclose($myfile);

		$inputs['url'] = "";
        $inputs['api_key'] = trim("gwBIxHUTYgryIZMSNLKgeXyNI33YgVRt");
        $inputs['secret_key'] = trim("1JeQ7kOs98PHkANR/egvDNTc1GvpZr+rP98BZrRttxVKeSJAyun/Iw==");
        $inputs['products'] = $product_json1;         
        $inputs['delete_inventory'] = true;
        $this->bulk_uploadproducts_get($inputs);
		///file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=END-Zooomberg-'. date("Y-m-d-h:i:sa"));
		$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron loco en cabeza End';
		 
		$message = 'END|loco en cabeza|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");


    }
//--------------loco en cabeza Data Processed and end


//--------------london_bee Data Processed and add		
    function london_bee_json_update_get(){
    	///file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=START|zooomberg|'. date("Y-m-d-h:i:sa"));
    	$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron london bee Start';			 
		$message = 'START|london bee|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);
    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");
    	//Total count product in shopify
		@$product_count_url = file_get_contents('https://360239092bc94dd82f52dd05743ba41d:02dfbfce6ae75bf28c3a560fc74f17f1@london-bee.myshopify.com/admin/products/count.json?published_status=published');//&handle=London-Bee-Mens-Cotton-Leaf-Print-Boxer-MLB0070
		$json_product = json_decode($product_count_url, true);
		$total_product_count = $json_product['count'];
		$total_product_count = 10;
		$length = 100;
		$offset = 0;
		$pro_count = -1;
		while($length*$offset <= $total_product_count){
			$page = $offset + 1;
		  	@$str = file_get_contents('https://360239092bc94dd82f52dd05743ba41d:02dfbfce6ae75bf28c3a560fc74f17f1@london-bee.myshopify.com/admin/products.json?limit=250&id=20256415686&page='.$page.'&published_status=published');    	//&handle=London-Bee-Mens-Cotton-Leaf-Print-Boxer-MLB0070
			$json = json_decode($str, true);				
			if(!empty($json)){			
			foreach ($json as $field => $value) {		
				foreach ($value as $key) {							
					foreach ($key['variants'] as $row) {
				  	   $pro_count++;			   
					   $sku = strip_tags($key['variants'][0]['sku']);			   
					   if($sku != ""){
					   		$data[$pro_count]['product_sku'] = strip_tags($row['sku']);
					   }else{
					   		$data[$pro_count]['product_sku'] = strip_tags($row['id']);
					   }
					   $data[$pro_count]['brand_product_id'] = strip_tags($key['id']);
					   $data[$pro_count]['product_category'] = strip_tags($key['product_type']);
					   $data[$pro_count]['product_subcategory'] = "";
					   $data[$pro_count]['product_name'] = strip_tags($key['title']);
					   $add_url =  strip_tags($key['handle']);
					   $data[$pro_count]['product_url'] = strip_tags("https://www.londonbee.in/products/".$add_url);					   			  
						$remove[] = ",";
							$remove[] = "&amp;amp;";
							$remove[] = '&lt;/p&gt;';
							$remove[] = 'br /&gt;';
							$remove[] = '&lt;/p&gt;';
							$remove[] = "/n";
							$remove[] = "/r";
							$remove[] = "quot;";
							$remove[] = "quot;quot;";
							$remove[] = "quot;quot;";
							$str="";
							$products = html_entity_decode(strip_tags(trim(strip_tags($key['body_html']))), ENT_QUOTES);
					   
					   /*$price = strip_tags($key['variants'][0]['compare_at_price']);		
					   $data[$pro_count]['price'] = $price;*/
					   $compare_at_price = trim(strip_tags($key['variants'][0]['compare_at_price']));		
						   if(!isset($compare_at_price) || (string)$compare_at_price == ''){
								$data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);
							}
						   else{
						   		$data[$pro_count]['price'] = strip_tags($key['variants'][0]['compare_at_price']);
						   	}

					   $data[$pro_count]['age_group'] = "";					   
					   @$id = trim($row['id']);							   					   					   
					   @$images_variant_ids = strip_tags($key['images'][0]['variant_ids'][0]);													   
						if(!isset($data[$pro_count]['product_img']))
							$data[$pro_count]['product_img']="";
						$imgs = $key['images'];
						foreach ($imgs as $img_row) {
							if(isset($img_row['variant_ids'][0]))			    						
								@$images_ids= trim($img_row['variant_ids'][0]);
							if($id == @$images_ids){								  
								$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }
							 if($row['option1']=="Default Title"){
							 	$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }					 
							 if(@$img_row['variant_ids'][0] == "" && @$id != @$images_ids && $row['option1']!="Default Title"){
							 	$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }						         
						}					   
					   $size = strip_tags($row['option1']);
					   $inventory_quantity = strip_tags($row['inventory_quantity']);						   
					   $stock = array($size => strip_tags($row['inventory_quantity']));					   
					   $data[$pro_count]['product_stock'][] = $stock;
					   $data[$pro_count]['product_color'] = "";					   
					   /*$discount_amount = strip_tags($key['variants'][0]['price']);					   
					   $data[$pro_count]['product_discount'] = $discount_amount;
					   $data[$pro_count]['discount_start_date'] = date("Y/m/d",strtotime("0 day")).'T00:00:00+00:00';		
					   $data[$pro_count]['discount_end_date'] = date("Y/m/d",strtotime("0 day")).'T23:59:59+23:59';
					   $discount = $this->discount_calculation($discount_amount,$price);
					   $data[$pro_count]['discount_percentage'] = $discount;*/
					   /*$data[$pro_count]['product_discount'] = "";		
					   $data[$pro_count]['discount_start_date'] = "";		
					   $data[$pro_count]['discount_end_date'] = "";*/
					   if ($compare_at_price == ''):
							$data[$pro_count]['product_discount'] = "";		
							$data[$pro_count]['discount_start_date'] = "";		
							$data[$pro_count]['discount_end_date'] = "";								   
							$data[$pro_count]['discount_percentage'] = "";
					   else:
							$price = strip_tags($key['variants'][0]['compare_at_price']);		
							$discount_amount = strip_tags($key['variants'][0]['price']);					   
							$data[$pro_count]['product_discount'] = $discount_amount;
							$data[$pro_count]['discount_start_date'] = date("Y/m/d",strtotime("0 day")).'T00:00:00+00:00';		
							$data[$pro_count]['discount_end_date'] = date("Y/m/d",strtotime("0 day")).'T23:59:59+23:59';
							$discount = $this->discount_calculation($discount_amount,$price);
							$data[$pro_count]['discount_percentage'] = $discount;	
						endif;								   
					   $tags =  strip_tags($key['tags']);							   
					   $tags_exp = explode(",",$tags);											   			   
						   foreach($tags_exp as $tag_value){
						   	$data[$pro_count]['product_tags'][] = $tag_value;					   
						   }
					}	

				}
			}
		}else{
				echo "Data not found";
			 }	 
			$offset++;
		}
		$product_json1 = json_encode($data);	

		/*echo '<pre>';
		print_r($product_json1);
		exit;			*/
		$url = "resource/london_bee/london_bee.json";
		$myfile = fopen($url, "w") or die("Unable to open file!");
		$txt = $product_json1;
		if(fwrite($myfile, $txt)){
			$file = "resource/london_bee/logs.txt";
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Done\n";
			//$current .= date("dd/mm/Y H:i:s")."Done";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}else{
			$file = 'resource/london_bee/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Failed\n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}
		fclose($myfile);

		$inputs['url'] = "";
        $inputs['api_key'] = trim("gwBIxHUTYgrgfUqDCum4PZ/LMbeSeJM5");
        $inputs['secret_key'] = trim("MEWX5Jxq7N3mG0rim5Loj/X2toi3Ab0REU8y7oB95Y3uCLdGdrDoJQ==");
        $inputs['products'] = $product_json1;         
        $inputs['delete_inventory'] = true;
        $this->bulk_uploadproducts_get($inputs);
		///file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=END-Zooomberg-'. date("Y-m-d-h:i:sa"));
		$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron london bee End';
		 
		$message = 'END|london bee|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");

    }
//--------------london bee Data Processed and end



//--------------Fighting Fame Data Processed and add		
    function fightingfame_json_update_get(){
    	///file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=START|zooomberg|'. date("Y-m-d-h:i:sa"));
    	$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron fighting fame Start';		 
		$message = 'START|fightingfame|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");

	    //Total count product in shopify
		@$product_count_url = file_get_contents('https://decb5aa11085c42be2bb504e6cc02fc0:188a38d9d1474a15d79c2bdd0f2ae371@fighting-fame.myshopify.com/admin/products/count.json?published_status=published');
		$json_product = json_decode($product_count_url, true);
		$total_product_count = $json_product['count'];
		//$total_product_count = 10;
		$length = 100;
		$offset = 0;
		$pro_count = -1;
		while($length*$offset <= $total_product_count){
			$page = $offset + 1;
		  	@$str = file_get_contents('https://decb5aa11085c42be2bb504e6cc02fc0:188a38d9d1474a15d79c2bdd0f2ae371@fighting-fame.myshopify.com/admin/products.json?limit=250&page='.$page.'&published_status=published');    	
			$json = json_decode($str, true);				
			if(!empty($json)){			
			foreach ($json as $field => $value) {		
				foreach ($value as $key) {							
					foreach ($key['variants'] as $row) {
				  	   $pro_count++;			   
					   $sku = strip_tags($key['variants'][0]['sku']);			   
					   if($sku != ""){
					   		$data[$pro_count]['product_sku'] = strip_tags($row['sku']);
					   }else{
					   		$data[$pro_count]['product_sku'] = strip_tags($row['id']);
					   }
					   $data[$pro_count]['brand_product_id'] = strip_tags($key['id']);
					   $data[$pro_count]['product_category'] = strip_tags($key['product_type']);
					   $data[$pro_count]['product_subcategory'] = "";
					   $data[$pro_count]['product_name'] = strip_tags($key['title']);
					   $add_url =  strip_tags($key['handle']);
					   $data[$pro_count]['product_url'] = strip_tags("https://www.fightingfame.com/products/".$add_url);					   			  
						$remove[] = ",";
							$remove[] = "&amp;amp;";
							$remove[] = '&lt;/p&gt;';
							$remove[] = 'br /&gt;';
							$remove[] = '&lt;/p&gt;';
							$remove[] = "/n";
							$remove[] = "/r";
							$remove[] = "quot;";
							$remove[] = "quot;quot;";
							$remove[] = "quot;quot;";
							$str="";
							$products = html_entity_decode(strip_tags(trim(strip_tags($key['body_html']))), ENT_QUOTES);
					   $data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);
					   $data[$pro_count]['age_group'] = "";					   
					   @$id = trim($row['id']);							   					   					   
					   @$images_variant_ids = strip_tags($key['images'][0]['variant_ids'][0]);													   
						if(!isset($data[$pro_count]['product_img']))
							$data[$pro_count]['product_img']="";
						$imgs = $key['images'];
						foreach ($imgs as $img_row) {
							if(isset($img_row['variant_ids'][0]))			    						
								@$images_ids= trim($img_row['variant_ids'][0]);
							if($id == @$images_ids){								  
								$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }
							 if($row['option1']=="Default Title"){
							 	$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }					 
							 if(@$img_row['variant_ids'][0] == "" && @$id != @$images_ids && $row['option1']!="Default Title"){
							 	$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }						         
						}					   
					   $size = strip_tags($row['option1']);
					   $inventory_quantity = strip_tags($row['inventory_quantity']);						   
					   $stock = array($size => strip_tags($row['inventory_quantity']));					   
					   $data[$pro_count]['product_stock'][] = $stock;		
					   $data[$pro_count]['product_discount'] = "";		
					   $data[$pro_count]['discount_start_date'] = "";		
					   $data[$pro_count]['discount_end_date'] = "";								   
					   $tags =  strip_tags($key['tags']);							   
					   $tags_exp = explode(",",$tags);											   			   
						   foreach($tags_exp as $tag_value){
						   	$data[$pro_count]['product_tags'][] = $tag_value;					   
						   }
					}		
				}
			}
		}else{
				echo "Data not found";
			 }	 
			$offset++;
		}		
		$product_json1 = json_encode($data);		
		/*echo '<pre>';
		print_r($product_json1);
		exit;		*/
		$url = "resource/fightingfame/fightingfame.json";
		$myfile = fopen($url, "w") or die("Unable to open file!");
		$txt = $product_json1;
		if(fwrite($myfile, $txt)){
			$file = "resource/fightingfame/logs.txt";
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Done\n";
			//$current .= date("dd/mm/Y H:i:s")."Done";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}else{
			$file = 'resource/fightingfame/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Failed\n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}
		fclose($myfile);

		$inputs['url'] = "";
        $inputs['api_key'] = trim("DxRzvwTueacesAYmSLQsO6Ae61YNMjwI");
        $inputs['secret_key'] = trim("6ukMaYD4GHiy5atM6hIdT9ZR020XUaxyraTfYLKBk89pAaVWf2pbHg==");
        $inputs['delete_inventory'] = true;
        $inputs['products'] = $product_json1;         
        $this->bulk_uploadproducts_get($inputs);
		///file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=END-Zooomberg-'. date("Y-m-d-h:i:sa"));
		$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron fighting fame End';
		 
		$message = 'END|fightingfame|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");

    }
//--------------fightingfame Data Processed and end


//--------------Mae Studio Data Processed and add		
    function mae_studio_json_update_get(){
    	///file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=START|zooomberg|'. date("Y-m-d-h:i:sa"));
    	$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron Mae Studio Start';		 
		$message = 'START|Mae Studio|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");

		
		//Total count product in shopify
		@$product_count_url = file_get_contents('https://9c6e314bcc1e44d7863b1e1eda039abb:6573c25731bb4ae3c86369837b429667@mae-clothing.myshopify.com/admin/products/count.json?published_status=published');
		$json_product = json_decode($product_count_url, true);
		$total_product_count = $json_product['count'];
		//$total_product_count = 10;
		$length = 100;
		$offset = 0;
		$pro_count = -1;
		while($length*$offset <= $total_product_count){
			$page = $offset + 1;
		  	@$str = file_get_contents('https://9c6e314bcc1e44d7863b1e1eda039abb:6573c25731bb4ae3c86369837b429667@mae-clothing.myshopify.com/admin/products.json?limit=100&page='.$page.'&published_status=published');    	
			$json = json_decode($str, true);				
			if(!empty($json)){			
			foreach ($json as $field => $value) {		
				foreach ($value as $key) {							
					foreach ($key['variants'] as $row) {
				  	   $pro_count++;			   
					   $sku = strip_tags($key['variants'][0]['sku']);			   
					   if($sku != ""){
					   		$data[$pro_count]['product_sku'] = strip_tags($row['sku']);
					   }else{
					   		$data[$pro_count]['product_sku'] = strip_tags($row['id']);
					   }
					   $data[$pro_count]['brand_product_id'] = strip_tags($key['id']);
					   $data[$pro_count]['product_category'] = strip_tags($key['product_type']);
					   $data[$pro_count]['product_subcategory'] = "";
					   $data[$pro_count]['product_name'] = strip_tags($key['title']);
					   $add_url =  strip_tags($key['handle']);
					   $data[$pro_count]['product_url'] = strip_tags("http://mae.co.in/products/".$add_url);					   			  
						$remove[] = ",";
							$remove[] = "&amp;amp;";
							$remove[] = '&lt;/p&gt;';
							$remove[] = 'br /&gt;';
							$remove[] = '&lt;/p&gt;';
							$remove[] = "/n";
							$remove[] = "/r";
							$remove[] = "quot;";
							$remove[] = "quot;quot;";
							$remove[] = "quot;quot;";
							$str="";
							$products = html_entity_decode(strip_tags(trim(strip_tags($key['body_html']))), ENT_QUOTES);
					   $data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);
					   $data[$pro_count]['age_group'] = "";					   
					   @$id = trim($row['id']);	
						   					   					   
						   @$images_variant_ids = strip_tags($key['images'][0]['variant_ids'][0]);													   
							if(!isset($data[$pro_count]['product_img']))
								$data[$pro_count]['product_img']="";
							$imgs = $key['images'];

							$data[$pro_count]['product_img'] = array();
							
							//---------------Get images dynamic
							foreach ($imgs as $img_row) {
								//if(isset($img_row['variant_ids'][0]))			    						
								foreach($img_row['variant_ids'] as $img_data_row){
									@$images_ids= trim($img_data_row);
									if($id == $images_ids){
									   	 //$data[$pro_count]['product_img'] .= strip_tags($img_row['src']) .',';
										$images_url = strip_tags($img_row['src']);
									    $images_url_one = explode("?",$images_url);							   
									    $data[$pro_count]['product_img'][] = $images_url_one[0];
									    unset($images_url_one);
									 }
								}
						         
							}

							//---------------If images not found then add all images to product														
							if(!isset($data[$pro_count]['product_img']) || count($data[$pro_count]['product_img']) == 0){								
								foreach ($imgs as $img_row) {
									//if(isset($img_row['variant_ids'][0]))			    						
										   	 //$data[$pro_count]['product_img'] .= strip_tags($img_row['src']) .',';
									if(!isset($img_row['variant_ids'][0])){
										$images_url = strip_tags($img_row['src']);
									    $images_url_one = explode("?",$images_url);							   
									    $data[$pro_count]['product_img'][] = $images_url_one[0];
									    unset($images_url_one);
									}
								}
							}	
					   $size = strip_tags($row['option1']);
					   $inventory_quantity = strip_tags($row['inventory_quantity']);						   
					   $stock = array($size => strip_tags($row['inventory_quantity']));					   
					   $data[$pro_count]['product_stock'][] = $stock;
					   $data[$pro_count]['product_color'] = strip_tags($row['option2']);
					   //$data[$pro_count]['product_color'] = "";					   
					   $data[$pro_count]['product_discount'] = "";		
					   $data[$pro_count]['discount_start_date'] = "";		
					   $data[$pro_count]['discount_end_date'] = "";								   
					   $tags =  strip_tags($key['tags']);							   
					   $tags_exp = explode(",",$tags);											   			   
						   foreach($tags_exp as $tag_value){
						   	$data[$pro_count]['product_tags'][] = $tag_value;					   
						   }
					}		
				}
			}
		}else{
				echo "Data not found";
			 }	 
			$offset++;
		}
		/*echo '<pre>';
		print_r($data);*/
		//echo json_encode($data);
		$product_json1 = json_encode($data);		
		$url = "resource/mae_studio/mae_studio.json";
		$myfile = fopen($url, "w") or die("Unable to open file!");
		$txt = $product_json1;
		if(fwrite($myfile, $txt)){
			$file = "resource/mae_studio/logs.txt";
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Done\n";
			//$current .= date("dd/mm/Y H:i:s")."Done";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}else{
			$file = 'resource/mae_studio/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Failed\n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}
		fclose($myfile);

		$inputs['url'] = "";
        $inputs['api_key'] = trim("DJxU+rttF95SgJo1OZUFZ5SFTaYNVZJxlA884n0sm0A=");
        $inputs['secret_key'] = trim("PM/yPGXjsHCRQbEJVL1agK22ntXjYIFLP26bZ7cD23XpAhMMc7EQSA==");
        $inputs['products'] = $product_json1;         
        $inputs['delete_inventory'] = true;
        $this->bulk_uploadproducts_get($inputs);
		///file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=END-Zooomberg-'. date("Y-m-d-h:i:sa"));
		$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron Mae Studio End';
		 
		$message = 'END|Mae Studio|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");

    }
//--------------mae_studio Data Processed and end



//--------------plants_superheroes Data Processed and add		
    function plant_superheroes_json_update_get(){
    	///file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=START|zooomberg|'. date("Y-m-d-h:i:sa"));
    	$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron Plants Superheroes Start';		 
		$message = 'START|Plants Superheroes|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);
    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");
		//Total count product in shopify
		@$product_count_url = file_get_contents('https://c0967c37df012507ca69612a7e548a5b:e739b7b9d2676428db300f7b99cc60c2@superherostore.myshopify.com/admin/products/count.json?published_status=published');
		$json_product = json_decode($product_count_url, true);
		$total_product_count = $json_product['count'];		
		$length = 100;
		$offset = 0;
		$pro_count = -1;
		while($length*$offset <= $total_product_count){
		    $page = $offset + 1;
		  	@$str = file_get_contents('https://c0967c37df012507ca69612a7e548a5b:e739b7b9d2676428db300f7b99cc60c2@superherostore.myshopify.com/admin/products.json?limit=250&page='.$page.'&published_status=published');    	
			$json = json_decode($str, true);			
			if(!empty($json)){			
			foreach ($json as $field => $value) {		
				foreach ($value as $key) {													
						foreach ($key['variants'] as $row) {
								$product_type = $key['product_type'];
								$vendor = $key['vendor'];
							   if($product_type == "T-Shirt" || $product_type == "Full%20Sleeves" || $product_type == "Vest" || $product_type == "Hoodie"||$product_type == "Sweat%20Shirt" || $product_type == "Jacket" || $product_type == "PJs" || $product_type == "Tee"|| $product_type == "Skirts" || $product_type == "Leggings" || $product_type == "Nightwear" || $vendor == "Macmerise" || $product_type == "Cap" || $product_type == "Wallet"|| $product_type == "Flip%20Flops" || $product_type == "Socks"|| $product_type == "Money%20Clip" || $product_type == "Card%20Holder"|| $product_type == "Keychains" || $product_type == "Pop%20Vinyl%20Figurine"|| $product_type == "Calender" || $product_type == "Messenger%20Bag" || $product_type == "Coffee%20Mug"|| $product_type == "Cushion%20Cover"|| $product_type == "Towel" || $product_type == "posters" || $product_type == "Coasters" || $product_type == "Ring" || $product_type == "Dog%20Tag"|| $product_type == "Pendant" || $product_type == "Earrings" || 0==0){
							   		$pro_count++;
								   $data[$pro_count]['product_sku'] = strip_tags($row['sku']);					   
								   $data[$pro_count]['brand_product_id'] = strip_tags($key['id']);
								   $data[$pro_count]['product_category'] = strip_tags($key['product_type']);
								   $data[$pro_count]['product_subcategory'] = "";
								   $data[$pro_count]['product_name'] = strip_tags($key['title']);
								   $add_url =  strip_tags($key['handle']);
								   $data[$pro_count]['product_url'] = strip_tags("https://www.planetsuperheroes.com/products/".$add_url);				
								   $string = strip_tags($key['body_html']);
								   $description_two = str_replace("'", "", $string);	
								   $products = strip_tags(str_replace('"','',(trim($description_two))));
								   $data[$pro_count]['product_description'] = strip_tags($products);
								   $data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);
								   $data[$pro_count]['age_group'] = "";					   
								  /* @$id = trim($row['id']);							   					   					   
								   @$images_variant_ids = strip_tags($key['images'][0]['variant_ids'][0]);													   
									if(!isset($data[$pro_count]['product_img']))
										$data[$pro_count]['product_img']="";
									@$imgs = $key['images'];
									foreach ($imgs as $img_row) {
										if(isset($img_row['variant_ids'][0]))			    						
											@$images_ids= trim($img_row['variant_ids'][0]);
										if(@$id == @$images_ids){
										   	 //$data[$pro_count]['product_img'] .= strip_tags($img_row['src']) .',';
											$images_url = strip_tags($img_row['src']);
										    $images_url_one = explode("?",$images_url);							   
										    $data[$pro_count]['product_img'][] = $images_url_one[0];
										    unset($images_url_one);
										 }
								         
									}	*/

									 @$id = trim($row['id']);	
								   					   					   
								   @$images_variant_ids = strip_tags($key['images'][0]['variant_ids'][0]);													   
									if(!isset($data[$pro_count]['product_img']))
										$data[$pro_count]['product_img']="";
									$imgs = $key['images'];

									$data[$pro_count]['product_img'] = array();
									
									//---------------Get images dynamic
									foreach ($imgs as $img_row) {
										//if(isset($img_row['variant_ids'][0]))			    						
										foreach($img_row['variant_ids'] as $img_data_row){
											@$images_ids= trim($img_data_row);
											if($id == $images_ids){
											   	 //$data[$pro_count]['product_img'] .= strip_tags($img_row['src']) .',';
												$images_url = strip_tags($img_row['src']);
											    $images_url_one = explode("?",$images_url);							   
											    $data[$pro_count]['product_img'][] = $images_url_one[0];
											    unset($images_url_one);
											 }
										}
								         
									}

									//---------------If images not found then add all images to product														
									if(!isset($data[$pro_count]['product_img']) || count($data[$pro_count]['product_img']) == 0){								
										foreach ($imgs as $img_row) {
											//if(isset($img_row['variant_ids'][0]))			    						
												   	 //$data[$pro_count]['product_img'] .= strip_tags($img_row['src']) .',';
											if(!isset($img_row['variant_ids'][0])){
												$images_url = strip_tags($img_row['src']);
											    $images_url_one = explode("?",$images_url);							   
											    $data[$pro_count]['product_img'][] = $images_url_one[0];
											    unset($images_url_one);
											}
										}
									}				   
								   $size = strip_tags($row['option2']);
								   $inventory_quantity = strip_tags($row['inventory_quantity']);
								   //$stock = array();
								   $stock = array($size => strip_tags($row['inventory_quantity']));					   
								   //$c = array_combine($size, $inventory_quantity);					   
								   /*echo '<pre>';
								   print_r($stock);*/
								   //$data[$pro_count][$size] = strip_tags($row['inventory_quantity']);
								   $data[$pro_count]['product_stock'][] = $stock;
								   //$data[$pro_count]['product_stock'] = array( strip_tags($row['inventory_quantity']);					   
								   $data[$pro_count]['product_color'] = strip_tags($row['option1']);		
								   $data[$pro_count]['product_discount'] = "";		
								   $data[$pro_count]['discount_start_date'] = "";		
								   $data[$pro_count]['discount_end_date'] = "";		
								   //$data[$pro_count]['product_tags'] = "";
								   $tags =  strip_tags($key['tags']);							   
								   $tags_exp = explode(",",$tags);											   
								   //if (is_array($tags_exp) || is_object($tags_exp)){
									   foreach($tags_exp as $tag_value){
									   	$data[$pro_count]['product_tags'][] = $tag_value;					   
									   }
									//}					   			    					   
								   //@$data[$pro_count]['product_color'] = strip_tags($row['option2']);					   
							}			

						}		
					}

				}
			}else{
				echo "Data not found";
			 }	 
			$offset++;
		}
		$product_json1 = json_encode($data);
		$url = "resource/plant_superheroes/plant_superheroes.json";
		$myfile = fopen($url, "w") or die("Unable to open file!");
		$txt = $product_json1;
		if(fwrite($myfile, $txt)){
			$file = "resource/plant_superheroes/logs.txt";
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Done\n";
			//$current .= date("dd/mm/Y H:i:s")."Done";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}else{
			$file = 'resource/plant_superheroes/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Failed\n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}
		fclose($myfile);
		$inputs['url'] = "";
        $inputs['api_key'] = trim("InRXu40/kzOYqbpvblnBoDXKgQYGd6maIItZPOCfENQ=");
        $inputs['secret_key'] = trim("+tDjJniiUKgPT3V4WgrZqKl8rZhL12Fn6NhFRMg+pOV95tYeduE9Yg==");
        $inputs['products'] = $product_json1;         
        $inputs['delete_inventory'] = true;
        $this->bulk_uploadproducts_get($inputs);
		///file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=END-Zooomberg-'. date("Y-m-d-h:i:sa"));
		$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron Plant Superheroes End';
		 
		$message = 'END|Plant Superheroes|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");

    }
//--------------Plant Superheroes Data Processed and end






//--------------FIRANJI Data Processed and add		
    function firanji_json_update_get(){    	
    	$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron Plants Firanji Start';		 
		$message = 'START|FIRANJI|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");

		ini_set('memory_limit', '-1');
		//Total count product in shopify
		@$product_count_url = file_get_contents('https://31777924276d77ba5e2ed6a8ec8bc8b1:39526a9090739f6b9af85e0a7becbc41@firanji.myshopify.com/admin/products/count.json?published_status=published');
		$json_product = json_decode($product_count_url, true);
		$total_product_count = $json_product['count'];
		//$total_product_count = 10;
		$length = 100;
		$offset = 0;
		$pro_count = -1;
		while($length*$offset <= $total_product_count){
			$page = $offset + 1;
		  	@$str = file_get_contents('https://31777924276d77ba5e2ed6a8ec8bc8b1:39526a9090739f6b9af85e0a7becbc41@firanji.myshopify.com/admin/products.json?limit='.$length.'&page='.$page.'&published_status=published');    	
			$json = json_decode($str, true);			
			/*echo '<pre>';
			print_r($json);*/
			if(!empty($json)){			
			foreach ($json as $field => $value) {				
				foreach ($value as $key) {								
					foreach ($key['variants'] as $row) {				
					   $pro_count++;
					   $data[$pro_count]['product_sku'] = strip_tags($row['sku']);					   
					   $data[$pro_count]['brand_product_id'] = strip_tags($key['id']);
					   $data[$pro_count]['product_category'] = strip_tags($key['product_type']);
					   $data[$pro_count]['product_subcategory'] = "";
					   $data[$pro_count]['product_name'] = strip_tags($key['title']);
					   $add_url =  strip_tags($key['handle']);
					   $data[$pro_count]['product_url'] = strip_tags("https://www.firanji.com/products/".$add_url);	
					   $string = strip_tags($key['body_html']);
					   $description_two = str_replace("'", "", $string);	
					   $products = strip_tags(str_replace('"','',(trim($description_two))));
					   $data[$pro_count]['product_description'] = strip_tags($products);
					   //$data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);
					   $compare_at_price = strip_tags($key['variants'][0]['compare_at_price']);		
					   if($compare_at_price == ''){
							$data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);					   		
						}elseif($compare_at_price == 0.00){
							$data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);					   		
						}else{
					   		$data[$pro_count]['price'] = strip_tags($key['variants'][0]['compare_at_price']);
					   	}	
					   $data[$pro_count]['age_group'] = "";					   
					   @$id = trim($row['id']);				   					   					   
					   @$images_variant_ids = strip_tags($key['images'][0]['variant_ids'][0]);													   
						if(!isset($data[$pro_count]['product_img']))
							$data[$pro_count]['product_img']="";
						$imgs = $key['images'];
						foreach ($imgs as $img_row) {
							if(isset($img_row['variant_ids'][0]))			    						
								@$images_ids= trim($img_row['variant_ids'][0]);
								$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];
							    unset($images_url_one);
						}					   
					   //$size = strip_tags($row['option1']);
						$get_size = strip_tags($row['option1']);
							if($get_size =='Default Title'){
								$size = 'One Size';
							}else{
								$size = strip_tags($row['option1']);
							}
					   $inventory_quantity = strip_tags($row['inventory_quantity']);					
					   $stock = array($size => strip_tags($row['inventory_quantity']));					   					
					   $data[$pro_count]['product_stock'][] = $stock;					
					   $data[$pro_count]['product_color'] = strip_tags($row['option2']);		
					   if ($compare_at_price == ''){
							$data[$pro_count]['product_discount'] = "";		
							$data[$pro_count]['discount_start_date'] = "";		
							$data[$pro_count]['discount_end_date'] = "";								   
							$data[$pro_count]['discount_percentage'] = "";
					   }elseif($compare_at_price == 0.00){
						   	$data[$pro_count]['product_discount'] = "";		
							$data[$pro_count]['discount_start_date'] = "";		
							$data[$pro_count]['discount_end_date'] = "";								   
							$data[$pro_count]['discount_percentage'] = "";
					   }else{
							$price = strip_tags($key['variants'][0]['compare_at_price']);		
							$discount_amount = strip_tags($key['variants'][0]['price']);					   
							$data[$pro_count]['product_discount'] = $discount_amount;
							$data[$pro_count]['discount_start_date'] = date("Y/m/d",strtotime("0 day")).'T00:00:00+00:00';		
							$data[$pro_count]['discount_end_date'] = date("Y/m/d",strtotime("0 day")).'T23:59:59+23:59';
							$discount = $this->discount_calculation($discount_amount,$price);
							$data[$pro_count]['discount_percentage'] = $discount;	
						}
					   /*$data[$pro_count]['product_discount'] = "";		
					   $data[$pro_count]['discount_start_date'] = "";		
					   $data[$pro_count]['discount_end_date'] = "";	*/
					   $tags =  strip_tags($key['tags']);							   
					   $tags_exp = explode(",",$tags);				
						   foreach($tags_exp as $tag_value){
						   	$data[$pro_count]['product_tags'][] = $tag_value;					   
						   }
						}		
					}

				}
		}else{
				echo "Data not found";
			 }	 
			$offset++;
		}	
		//exit;
		$product_json1 = json_encode($data);			
		/*echo '<pre>';
		print_r($product_json1);
		exit;*/
		$url = "resource/FIRANJI/FIRANJI.json";
		$myfile = fopen($url, "w") or die("Unable to open file!");
		$txt = $product_json1;
		if(fwrite($myfile, $txt)){
			$file = "resource/FIRANJI/logs.txt";
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Done\n";
			//$current .= date("dd/mm/Y H:i:s")."Done";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}else{
			$file = 'resource/FIRANJI/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Failed\n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}
		fclose($myfile);

		$inputs['url'] = "";
        $inputs['api_key'] = trim("0qFig/8F9L2ojcqQndWo/w==");
        $inputs['secret_key'] = trim("USwMR0ZY9B83Inv1dkTOKhwx1K0k3aGuB/OHldRU6yl52ZfXeMseXA==");
        $inputs['products'] = $product_json1;         
        $inputs['delete_inventory'] = true;
        $this->bulk_uploadproducts_get($inputs);		
		$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron FIRANJI End';
		 
		$message = 'END|FIRANJI|'. date("Y-m-d-h:i:sa");
		
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");

    }
//--------------FIRANJI Data Processed and end



//--------------be bajrang Data Processed and add
    function felomhe_update_get(){  
    	/*file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=START|beBajrang|'. date("Y-m-d-h:i:sa")); */
    	$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron Felomhe Start';
		 
		$message = 'START|Felomhe|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");

		//Total count product in shopify
		@$product_count_url = file_get_contents('https://bef7cc5505592f8ac3767075108ecae6:6bc4abe9584c6cab0fcca14dd29d098a@jinubaby.myshopify.com/admin/products/count.json?published_status=published');
		$json_product = json_decode($product_count_url, true);
		$total_product_count = $json_product['count'];
		//$total_product_count = 10;
		$length = 100;
		$offset = 0;
		$pro_count = -1;
		
		while($length*$offset <= $total_product_count){
			$page = $offset + 1;
		  	@$str = file_get_contents('https://bef7cc5505592f8ac3767075108ecae6:6bc4abe9584c6cab0fcca14dd29d098a@jinubaby.myshopify.com/admin/products.json?limit=250&page='.$page.'&published_status=published');    	
			$json = json_decode($str, true);				
			/*echo '<pre>';
			print_r($json);*/
			if(!empty($json)){			
			foreach ($json as $field => $value) {		
				foreach ($value as $key) {							
					foreach ($key['variants'] as $row) {
				  	   $pro_count++;			   
					   $sku = strip_tags($key['variants'][0]['sku']);			   
					   if($sku != ""){
					   		$data[$pro_count]['product_sku'] = strip_tags($row['sku']);
					   }else{
					   		$data[$pro_count]['product_sku'] = strip_tags($row['id']);
					   }
					   $data[$pro_count]['brand_product_id'] = strip_tags($key['id']);
					   $data[$pro_count]['product_category'] = strip_tags($key['product_type']);
					   $data[$pro_count]['product_subcategory'] = "";
					   $data[$pro_count]['product_name'] = strip_tags($key['title']);
					   $add_url =  strip_tags($key['handle']);
					   $data[$pro_count]['product_url'] = strip_tags("https://www.felomhe.com/products/".$add_url);					   			  
						$remove[] = ",";
							$remove[] = "&amp;amp;";
							$remove[] = '&lt;/p&gt;';
							$remove[] = 'br /&gt;';
							$remove[] = '&lt;/p&gt;';
							$remove[] = "/n";
							$remove[] = "/r";
							$remove[] = "quot;";
							$remove[] = "quot;quot;";
							$remove[] = "quot;quot;";
							$str="";
							$products = html_entity_decode(strip_tags(trim(strip_tags($key['body_html']))), ENT_QUOTES);
					   //$data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);
						$compare_at_price = strip_tags($key['variants'][0]['compare_at_price']);		
						$price = strip_tags($key['variants'][0]['price']);
						   if($compare_at_price == ''){
								$data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);					   		
							}elseif($compare_at_price == 0.00){
								$data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);					   		
							}else{
						   		$data[$pro_count]['price'] = strip_tags($key['variants'][0]['compare_at_price']);
						   	}
					   $data[$pro_count]['age_group'] = "";					   
					   @$id = trim($row['id']);							   					   					   
					   @$images_variant_ids = strip_tags($key['images'][0]['variant_ids'][0]);													   
						if(!isset($data[$pro_count]['product_img']))
							$data[$pro_count]['product_img']="";
						$imgs = $key['images'];
						foreach ($imgs as $img_row) {
							if(isset($img_row['variant_ids'][0]))			    						
								@$images_ids= trim($img_row['variant_ids'][0]);
							if($id == @$images_ids){								  
								$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }
							 if($row['option1']=="Default Title"){
							 	$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }					 
							 if(@$img_row['variant_ids'][0] == "" && @$id != @$images_ids && $row['option1']!="Default Title"){
							 	$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }						         
						}					   
					   //$size = strip_tags($row['option1']);
						$get_size = strip_tags($row['option1']);
							if($get_size =='Default Title'){
								$size = 'One Size';
							}else{
								$size = strip_tags($row['option1']);
							}		
					   $inventory_quantity = strip_tags($row['inventory_quantity']);						   
					   $stock = array($size => strip_tags($row['inventory_quantity']));					   
					   $data[$pro_count]['product_stock'][] = $stock;
					   //var_dump(strip_tags($row['option2']));
					   $data[$pro_count]['product_color'] = strip_tags($row['option2']);		
					   //$data[$pro_count]['product_color'] = "";		
					   /*$data[$pro_count]['product_discount'] = "";		
					   $data[$pro_count]['discount_start_date'] = "";		
					   $data[$pro_count]['discount_end_date'] = "";*/
					   if ($compare_at_price == ''){
							$data[$pro_count]['product_discount'] = "";		
							$data[$pro_count]['discount_start_date'] = "";		
							$data[$pro_count]['discount_end_date'] = "";								   
							$data[$pro_count]['discount_percentage'] = "";
					   }elseif($compare_at_price == 0.00){
						   	$data[$pro_count]['product_discount'] = "";		
							$data[$pro_count]['discount_start_date'] = "";		
							$data[$pro_count]['discount_end_date'] = "";								   
							$data[$pro_count]['discount_percentage'] = "";
					   }else{
							$price = strip_tags($key['variants'][0]['compare_at_price']);		
							$discount_amount = strip_tags($key['variants'][0]['price']);					   
							$data[$pro_count]['product_discount'] = $discount_amount;
							$data[$pro_count]['discount_start_date'] = date("Y/m/d",strtotime("0 day")).'T00:00:00+00:00';		
							$data[$pro_count]['discount_end_date'] = date("Y/m/d",strtotime("0 day")).'T23:59:59+23:59';
							$discount = $this->discount_calculation($discount_amount,$price);
							$data[$pro_count]['discount_percentage'] = $discount;	
						}	

					   $tags =  strip_tags($key['tags']);							   
					   $tags_exp = explode(",",$tags);											   			   
						   foreach($tags_exp as $tag_value){
						   	$data[$pro_count]['product_tags'][] = $tag_value;					   
						   }
					}		
				}
			}
			}else{
					echo "Data not found";
				 }	 
				$offset++;
			}
			//exit;			
			$product_json1 = json_encode($data);			
			/*echo '<pre>';
			print_r($product_json1);
			exit;*/
		$url = "resource/felomhe/felomhe.json";
		$myfile = fopen($url, "w") or die("Unable to open file!");
		$txt = $product_json1;
		if(fwrite($myfile, $txt)){
			$file = "resource/felomhe/logs.txt";
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			//$current .= date("dd/mm/Y H:i:s")."Done -  - $master\n";
			$current .= date("dd/mm/Y H:i:s")."Done\n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}else{
			$file = 'resource/felomhe/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Failed\n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}
		fclose($myfile);

		$inputs['url'] = "";
        $inputs['api_key'] = trim("VihspXrtZ8ZbN4jNMg6aUoQTQ2m8gIbq");
        $inputs['secret_key'] = trim("33e43ZHTH6LgSpo7iYNtcANXXmzKsde05mXxiArhuSyO8oFsvn99NA==");
        $inputs['products'] = $product_json1; 
        $inputs['delete_inventory'] = true;
        $this->bulk_uploadproducts_get($inputs);


		/*file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=END|beBajrang|'. date("Y-m-d-h:i:sa"));*/
		$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron Felomhe End';
		 
		$message = 'END|Felomhe|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");
	}
//--------------be_bajrang Data Processed and end

//--------------faballey Data Processed and open		    
function faballey_xml_json_get()
{
	exit;
	// Set your email information
		$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron Faballey Master start';
		 
		$message = 'START|faballey|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");


    	$myXMLData = file_get_contents("http://admin.faballey.com/uploadxml/stylecrackerfeed.xml"); // your file is in the string "$xml" now.
		file_put_contents("resource/faballey/faballey.xml", $myXMLData); // now your xml file is saved.

		//$myXMLData = file_get_contents('http://admin.faballey.com/uploadxml/stylecrackerfeed.xml');
		$xml=simplexml_load_string($myXMLData) or die("Error: Cannot create object");			
		if(!empty($xml->item))
		{
			$product_json = '[{'; $i = 0;	
			foreach($xml->item as $val){ 
				if($i!=0){
					$product_json = $product_json.'},{';
				}
				$product_json = $product_json.'"product_sku":"'.strip_tags($val->product_sku).'",';
				$product_json = $product_json.'"product_category":"'.strip_tags($val->product_category).'",';
				$product_json = $product_json.'"product_subcategory":"'.strip_tags($val->product_subcategory).'",';
				$product_json = $product_json.'"product_name":"'.strip_tags($val->product_name).'",';
				$product_json = $product_json.'"product_url":"'.strip_tags($val->product_url).'",';			
				$remove[] = ",";
				$remove[] = "&amp;amp;";
				$remove[] = '&lt;/p&gt;';
				$remove[] = 'br /&gt;';
				$remove[] = '&lt;/p&gt;';
				$remove[] = "/n";
				$remove[] = "/r";
				$remove[] = "quot;";
				$remove[] = "quot;quot;";
				$remove[] = substr("\ ", 0,1);

				$str="";
				$descrip = html_entity_decode(strip_tags(trim($val->product_description)), ENT_QUOTES);
				$title_remove = str_replace($remove, $str, $descrip);
				$title_remove = preg_replace( "/\r|\n|\"/", "", $title_remove );
				$product_json = $product_json.'"product_description":"'.strip_tags($title_remove).'",';			
				$product_json = $product_json.'"price":"'.strip_tags($val->Price).'",';
				$product_json = $product_json.'"age_group":"",';			
				$image_extra = $val->product_img;
				$image_extra_exp = explode(",", $image_extra);			
				$product_json = $product_json.'"product_img":["'.strip_tags(@$image_extra_exp[0]).'","'.strip_tags(@$image_extra_exp[1]).'","'.strip_tags(@$image_extra_exp[2]).'","'.strip_tags(@$image_extra_exp[3]).'","'.strip_tags(@$keyword_ex[4]).'"],';
				//$product_json = $product_json.'"product_img":"'.strip_tags($val->product_img).'",';
				/*if($val->size == "One Size"){*/
				$stock = array();
				$stock[] = array(strip_tags(strip_tags($val->size)) => strip_tags(strip_tags($val->product_stock)));				
				$product_json = $product_json.'"product_stock":'.json_encode($stock).',';
				/*}else{
					$stock = array();
					$stock[] = array(strip_tags(strip_tags($val->size)) => strip_tags(strip_tags($val->product_stock)));				
					$product_json = $product_json.'"product_stock":'.strip_tags(json_encode($stock)).',';
				}*/ 			
				$product_json = $product_json.'"product_discount":"",';
				$product_json = $product_json.'"discount_start_date":"",';
				$product_json = $product_json.'"discount_end_date":"",';

				if(strip_tags(@$val->product_tags)){ $keyword1 = strip_tags(@$val->product_tags);  }else{$keyword1 = ''; }
				$keyword_ex = explode(",", $keyword1);					    
			    
				/*if(strip_tags(@$val->Keyword2)){ $keyword2 = strip_tags(@$val->Keyword2);  }else{$keyword2 = ''; }
				if(strip_tags(@$val->Keyword3)){ $keyword3 = strip_tags(@$val->Keyword3);  }else{$keyword3 = ''; }
				if(strip_tags(@$val->Keyword4)){ $keyword4 = strip_tags(@$val->Keyword4);  }else{$keyword4 = ''; }*/
				$product_json = $product_json.'"product_tags":["'.strip_tags(@$keyword_ex[0]).'","'.strip_tags(@$keyword_ex[1]).'","'.strip_tags(@$keyword_ex[2]).'","'.strip_tags(@$keyword_ex[3]).'","'.strip_tags(@$keyword_ex[4]).'","'.strip_tags(@$keyword_ex[5]).'","'.strip_tags(@$keyword_ex[6]).'","'.strip_tags(@$keyword_ex[7]).'"]';
				$i++;
				//if($i == 1500){ $product_json = $product_json.'}]'; echo $product_json; exit;}
			}
			$product_json = $product_json.'}]';			
			//$product_json1 = $product_json;
			/*$url = "faballey.json";
			$myfile = fopen($url, "w") or die("Unable to open file!");
			$txt = $product_json;
			$myfile = fopen("faballey.json", "w") or die("Unable to open file!");
			fwrite($myfile, $txt);
			fclose($myfile);*/
			echo '<pre>';
			print_r($product_json);
			exit;

			$file_path = "resource/faballey/faballey.json";
			/*$myfile = fopen($file_path, "w") or die("Unable to open file!");
			$txt = "";
			fwrite($myfile, $txt);
			fclose($myfile);*/
			if(file_put_contents($file_path, $product_json)){
				$inputs['url'] = $file_path;
		        $inputs['api_key'] = "688wACxu7zF6AKnt8OPoyAvL3jSfMJ+2aFTGm+fvDQA="; 
		        $inputs['secret_key'] = "1iF3NWIxmlnYrbD7BD7s3JcvFCkp2VAylHczDso27YsipZ/dmzxgjA=="; 
		        $inputs['products'] = ''; 
		        /*Delete inventory*/
		        $inputs['delete_inventory'] = true;
		        //---------Update/add in database
		        $this->bulk_uploadproducts_get($inputs);
				
				$file = 'resource/faballey/logs.txt';
				// Open the file to get existing content
				$current = file_get_contents($file);
				// Append a new person to the file
				$current .= date("dd/mm/Y H:i:s")."Done - $master\n";
				// Write the contents back to the file
				file_put_contents($file, $current);
				/*file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=END|forever|master|'. date("Y-m-d-h:i:sa"));*/
				// Set your email information
				$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
				$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
				$subject = 'Cron faballey End';
				 
				$message = 'END|faballey|'. date("Y-m-d-h:i:sa");
		    	//$this->email($from, $to, $subject, $message);

		    	$file_path_log = "resource/log.txt";
				// Open the file to get existing content
				$current = file_get_contents($file_path_log);
				// Append a new person to the file
				$current .= $message."\n";
				// Write the contents back to the file
				file_put_contents($file_path_log, $current."|");
		    	/*file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=END-faballey-'. date("Y-m-d-h:i:sa"));
		    	file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919821443562&message=END-faballey-'. date("Y-m-d-h:i:sa"));
*/
			}else{
				$file = 'resource/faballey/logs.txt';
				// Open the file to get existing content
				$current = file_get_contents($file);
				// Append a new person to the file
				$current .= date("dd/mm/Y H:i:s")."Failed - $master \n";
				// Write the contents back to the file
				file_put_contents($file, $current);
				/*file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=FAILED|forever|master|'. date("Y-m-d-h:i:sa"));*/
				$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
				$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
				$subject = 'Cron faballey FAILED';
				 
				$message = 'FAILED|faballey|'. date("Y-m-d-h:i:sa");
		    	//$this->email($from, $to, $subject, $message);

		    	$file_path_log = "resource/log.txt";
				// Open the file to get existing content
				$current = file_get_contents($file_path_log);
				// Append a new person to the file
				$current .= $message."\n";
				// Write the contents back to the file
				file_put_contents($file_path_log, $current."|");
			}    
		}	
	}
//--------------faballey Data Processed and end
//--------------Myrah Data Processed and add
    function myrah_update_get(){   
    	/*file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=START|Upbeatz|'. date("Y-m-d-h:i:sa"));*/
    	$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron Myrah Start';
		 
		$message = 'START|Myrah|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");
		//Total count product in shopify
		@$product_count_url = file_get_contents('https://dedd70a0c03fb0bfd2bb07df88bd8fc8:1a5ebe0a0a2e90f0d2499c398dce224a@myrahtheboutique.myshopify.com/admin/products/count.json?published_status=published&handle=top-1');
		$json_product = json_decode($product_count_url, true);
		$total_product_count = $json_product['count'];
		//$total_product_count = 100;
		$length = 100;
		$offset = 0;
		$pro_count = -1;
		while($length*$offset <= $total_product_count){
			$page = $offset + 1;
		  	@$str = file_get_contents('https://dedd70a0c03fb0bfd2bb07df88bd8fc8:1a5ebe0a0a2e90f0d2499c398dce224a@myrahtheboutique.myshopify.com/admin/products.json?limit=100&page='.$page.'&published_status=published');    	
			$json = json_decode($str, true);				
			/*echo '<pre>';
			print_r($json);*/
			if(!empty($json)){			
			foreach ($json as $field => $value) {		
				foreach ($value as $key) {							
					foreach ($key['variants'] as $row) {
				  	   $pro_count++;			   
				  	   $data[$pro_count]['product_sku'] = strip_tags($row['id']);					   
				  	   $data[$pro_count]['brand_product_id'] = strip_tags($key['id']);
					   $data[$pro_count]['product_category'] = strip_tags($key['product_type']);
					   $data[$pro_count]['product_subcategory'] = "";
					   $data[$pro_count]['product_name'] = strip_tags($key['title']);
					   $add_url =  strip_tags($key['handle']);
					   $data[$pro_count]['product_url'] = strip_tags("https://www.myrah.co.in/products/".$add_url);					   
					   $remove[] = ",";
							$remove[] = "&amp;amp;";
							$remove[] = '&lt;/p&gt;';
							$remove[] = 'br /&gt;';
							$remove[] = '&lt;/p&gt;';
							$remove[] = "/n";
							$remove[] = "/r";
							$remove[] = "quot;";
							$remove[] = "quot;quot;";
							$remove[] = "quot;quot;";
							$str="";					
						$data[$pro_count]['product_description'] = html_entity_decode(strip_tags(trim(strip_tags($key['body_html']))), ENT_QUOTES);	
					   //$data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);
						$compare_at_price = strip_tags($key['variants'][0]['compare_at_price']);		
							if($compare_at_price == '')
								$data[$pro_count]['price'] = strip_tags($key['variants'][0]['price']);					   		
							else
									$data[$pro_count]['price'] = strip_tags($key['variants'][0]['compare_at_price']);		
					   $data[$pro_count]['age_group'] = "";					   
					   @$id = trim($row['id']);							   					   					   
					   @$images_variant_ids = strip_tags($key['images'][0]['variant_ids'][0]);													   
						if(!isset($data[$pro_count]['product_img']))
							$data[$pro_count]['product_img']="";
						$imgs = $key['images'];
						foreach ($imgs as $img_row) {
							if(isset($img_row['variant_ids'][0]))			    						
								@$images_ids= trim($img_row['variant_ids'][0]);
							if($id == @$images_ids){								  
								$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }
							 if($row['option1']=="Default Title"){
							 	$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }					 
							 if(@$img_row['variant_ids'][0] == "" && @$id != @$images_ids && $row['option1']!="Default Title"){
							 	$images_url = strip_tags($img_row['src']);
							    $images_url_one = explode("?",$images_url);							   
							    $data[$pro_count]['product_img'][] = $images_url_one[0];					    
							    unset($images_url_one);
							 }						         
						}					   
					   //$size = strip_tags($row['option1']);
						$get_size = strip_tags($row['option1']);
							if($get_size =='Default Title'){
								$size = 'One Size';
							}else{
								$size = strip_tags($row['option1']);
							}			
					   $inventory_quantity = strip_tags($row['inventory_quantity']);						   
					   $stock = array($size => strip_tags($row['inventory_quantity']));					   
					   $data[$pro_count]['product_stock'][] = $stock;
					   //$data[$pro_count]['product_color'] = strip_tags($row['option1']);		
					   /*$data[$pro_count]['product_color'] = "";		
					   $data[$pro_count]['product_discount'] = "";		
					   $data[$pro_count]['discount_start_date'] = "";		
					   $data[$pro_count]['discount_end_date'] = "";*/
						if ($compare_at_price == ''):
							$data[$pro_count]['product_discount'] = "";		
							$data[$pro_count]['discount_start_date'] = "";		
							$data[$pro_count]['discount_end_date'] = "";								   
							$data[$pro_count]['discount_percentage'] = "";
						else:
							$price = strip_tags($key['variants'][0]['compare_at_price']);		
							$discount_amount = strip_tags($key['variants'][0]['price']);					   
							$data[$pro_count]['product_discount'] = $discount_amount;
							$data[$pro_count]['discount_start_date'] = date("Y/m/d",strtotime("0 day")).'T00:00:00+00:00';		
							$data[$pro_count]['discount_end_date'] = date("Y/m/d",strtotime("0 day")).'T23:59:59+23:59';
							$discount = $this->discount_calculation($discount_amount,$price);
							$data[$pro_count]['discount_percentage'] = $discount;	
						endif;								   
					   $tags =  strip_tags($key['tags']);							   
					   $tags_exp = explode(",",$tags);											   			   
						   foreach($tags_exp as $tag_value){
						   	$data[$pro_count]['product_tags'][] = $tag_value;					   
						   }
					}		
				}
			}
		}else{
				echo "Data not found";
			 }	 
			$offset++;
		}		
		/*exit;*/
		$product_json1 = json_encode($data);
		/*echo '<pre>';
		print_r($product_json1);
		exit;*/
		$url = "resource/myrah/myrah.json";
		$myfile = fopen($url, "w") or die("Unable to open file!");
		$txt = $product_json1;
		if(fwrite($myfile, $txt)){
			$file = "resource/myrah/logs.txt";
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Done\n";
			//$current .= date("dd/mm/Y H:i:s")."Done";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}else{
			$file = 'resource/myrah/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Failed";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}
		fclose($myfile);
		$inputs['url'] = "";
        $inputs['api_key'] = trim("1liaRRplisGdFb4tw04wg2IjcV7y25Ld");
        $inputs['secret_key'] = trim("nuBJh+VZsUZRz4Ue05gs1uhIjeLlr1YhwtUtayyqlWVkmTAoU906qw==");
        $inputs['products'] = $product_json1; 
        $inputs['delete_inventory'] = true;
        $this->bulk_uploadproducts_get($inputs);

		/*file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=END|Upbeatz|'. date("Y-m-d-h:i:sa"));*/
		$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron Myrah End';
		 
		$message = 'END|Myrah|'. date("Y-m-d-h:i:sa");
    	//$this->email($from, $to, $subject, $message);

    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");
	}
//--------------Myrah Data Processed and end		



//--------------Miss Chase Data Processed and add		
    function miss_chase_json_update_get(){
    	///file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=START|zooomberg|'. date("Y-m-d-h:i:sa"));
    	/*$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron Miss Chase Start';*/		 
		$message = 'START|Miss Chase|'. date("Y-m-d-h:i:sa");
    	/*$this->email($from, $to, $subject, $message);*/
    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");
		//Total count product in shopify	  	
		//new file
		$myXMLData = file_get_contents('http://www.misschase.com/products/stylecracker_productinfo');
		$xml=simplexml_load_string($myXMLData) or die("Error: Cannot create object");	
		$pro_count = -1;
		if(!empty($xml)){			
		foreach ($xml as $field => $value) {		
			foreach ($value as $key) {							
				$pro_count++;			
				$data[$pro_count]['product_sku'] = strip_tags($key->sku);
				$data[$pro_count]['product_category'] = strip_tags($key->product_category);
				$data[$pro_count]['product_subcategory'] = strip_tags($key->product_sub_category);
				$data[$pro_count]['product_name'] = strip_tags($key->name);
				$data[$pro_count]['product_url'] = strip_tags($key->link);
				$description_arr =  $key->description;
				$description_one = str_replace('<![CDATA[', '', $description_arr);
				$description_two = str_replace(']]>', '', $description_one);	
				$description_three = str_replace('/n', '', $description_two);	
				 $remove[] = ",";
						$remove[] = "&amp;amp;";
						$remove[] = '&lt;/p&gt;';
						$remove[] = 'br /&gt;';
						$remove[] = '&lt;/p&gt;';
						$remove[] = "/n";
						$remove[] = "/r";
						$remove[] = "quot;";
						$remove[] = "quot;quot;";
						$remove[] = "quot;quot;";
						$remove[] = 'dublequot;';
						$remove[] = '"';
						$str="";
				$description_json = str_replace( $remove, "", $description_three);		
				$data[$pro_count]['product_description'] = html_entity_decode(strip_tags(trim(strip_tags($description_json))), ENT_QUOTES);			
				$data[$pro_count]['price'] = strip_tags($key->price);
				$data[$pro_count]['age_group'] = "";
				$filesdata = (array)$key->additional_image_link;
				$data[$pro_count]['product_img'] = $filesdata;			
				$size_value = strip_tags($key->size);			
				$inventory_quantity = strip_tags($key->Instock);						   
				if($inventory_quantity=='true'){
					$availabil=2;
				}else{
					$availabil=0;
				}
				$stock = array($size_value => strip_tags($availabil));					   
				$data[$pro_count]['product_stock'][] = $stock;
				$data[$pro_count]['product_color'] = "";
				$data[$pro_count]['product_discount'] = "";			
				$data[$pro_count]['discount_start_date'] = "";
				$data[$pro_count]['discount_end_date'] = "";			
				$data[$pro_count]['product_tags'] = "";			
			}
		}
	}else{
			echo "Data not found";
		 }	 		
		$product_json1 = json_encode($data);						
		$url = "resource/miss_chase/miss_chase.json";
		$myfile = fopen($url, "w") or die("Unable to open file!");
		$txt = $product_json1;
		if(fwrite($myfile, $txt)){
			$file = "resource/miss_chase/logs.txt";
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Done\n";
			//$current .= date("dd/mm/Y H:i:s")."Done";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}else{
			$file = 'resource/miss_chase/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Failed\n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}
		fclose($myfile);
		$inputs['url'] = "";
        $inputs['api_key'] = trim("x/58/aLTwT39BdaDbzmE79ufD8bYsJFM");
        $inputs['secret_key'] = trim("XYuwk68psrP9uLCgPI2EgNu5bp+bdT8Y4H9G6Yt5Ls6DwkWwABH62g==");
        $inputs['products'] = $product_json1; 
        $inputs['delete_inventory'] = true;
        $this->bulk_uploadproducts_get($inputs);
		//file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=END-Zooomberg-'. date("Y-m-d-h:i:sa"));
		/*$from = array('email' => 'styleemailphp@gmail.com', 'name' => 'StyleCracker Cron');
		$to = array('arun.arya007@gmail.com', 'amol@stylecracker.com');
		$subject = 'Cron Miss Chase End';*/
		$message = 'END|Miss Chase|'. date("Y-m-d-h:i:sa");
    	/*$this->email($from, $to, $subject, $message);*/
    	$file_path_log = "resource/log.txt";
		// Open the file to get existing content
		$current = file_get_contents($file_path_log);
		// Append a new person to the file
		$current .= $message."\n";
		// Write the contents back to the file
		file_put_contents($file_path_log, $current."|");
    }
//--------------miss_chase Data Processed and end
/*
    function forever_uploadproducts_get(){
	    file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919820440455&message=cronrunstart2000');	
		$product_json =file_get_contents('resource/forever_json/forever_master.json')or die("Error reading file $path");
		
		$error['msg'] = '200 OK';
        $a = 0; $u = 0; $e = 0;
        $allowed_ext =  array('gif','png' ,'jpg','jpeg','GIF','PNG' ,'JPG','JPEG');
        $products = array();
    
        $api_key = "GgZ6cu5cQZbDd2ZmPsRLa5po6tt0TqNc";
		$secret_key = "D6jK8JdKm8MGjUWfGz2YD7RZi70tmTWSMJEBf1c9qXlsi2pE7FNmBg==";
        if($product_json){
        	$allproducts = trim($product_json); 
            $products = json_decode($product_json); //print_r($products);exit;
        }
              
        
        if($api_key==''){
            $error['msg'] = 'API key is missing';
        }else if($secret_key==''){
            $error['msg'] = 'Secret Key is missing';
        }else if(empty($products)){
            $error['msg'] = 'Products does not exist';
        }else if($api_key!='' && $secret_key!=''){
           	$brand_id = $this->Scproductinventorycron_model->get_brand_id($this->encryptOrDecrypt($api_key,''),$this->encryptOrDecrypt($secret_key,''));
	            if($brand_id == 0){
	            	$error['msg'] = 'Invalid API Key and Secret Key';
	            }
        }
       
        if(!empty($products) && $brand_id>0){  
        	$brand_code = $this->Scproductinventorycron_model->getbrand_code($brand_id);
            foreach($products as $value){
                $data = array();
                if (isset($value->product_sku)) { $product_sku = $brand_code.strip_tags($value->product_sku); } else{ $product_sku =''; }
                if (isset($value->product_category)) { $product_category = strip_tags($value->product_category); } else{ $product_category =''; }
                if (isset($value->product_subcategory)) { $product_subcategory = strip_tags($value->product_subcategory); } else{ $product_subcategory =''; }
               	if (isset($value->product_name)) { $product_name = strip_tags($value->product_name); } else{ $product_name =''; }
                if (isset($value->product_url)) { $product_url = strip_tags($value->product_url); } else{ $product_url =''; }
                if (isset($value->product_description)) { $product_description = addslashes(strip_tags($value->product_description)); } else{ $product_description =''; }
                if (isset($value->price)) { $price = strip_tags($value->price); } else{ $price =''; }
                if (isset($value->age_group)) { $age_group = strip_tags($value->age_group); } else{ $age_group =''; }
                if (isset($value->product_discount)) { $product_discount = strip_tags($value->product_discount); } else{ $product_discount =''; }
                if (isset($value->discount_start_date)) { $discount_start_date = strip_tags($value->discount_start_date); } else{ $discount_start_date =''; }
                if (isset($value->discount_end_date)) { $discount_end_date = strip_tags($value->discount_end_date); } else{ $discount_end_date =''; }  
                if(!empty($value->product_img)) { $product_imges = $value->product_img; }else{ $product_imges = array(); }
                if(!empty($value->product_stock)) { $product_stock = $value->product_stock; }else{ $product_stock = array(); }
                if(!empty($value->product_tags)) { $product_tags = $value->product_tags; }else{ $product_tags = array(); }
                
                if($product_name!='' && $price>0 && $product_sku!='' && $product_url!='' && !empty($product_imges)){

	                $product_id = $this->Scproductinventorycron_model->get_product_id($product_sku,$product_url);

	                $data['product_desc']['product_sku'] = $product_sku;
	                $data['product_desc']['product_cat_id'] = $this->get_categoryID($product_category);
	                $data['product_desc']['product_sub_cat_id'] = $this->get_subcategoryID($product_subcategory);
	                $data['product_desc']['brand_id'] = $brand_id;
	                $data['product_desc']['name'] = $product_name;
	                $data['product_desc']['url'] = $product_url;
	                $data['product_desc']['description'] = $product_description;
	                
	                $data['product_desc']['price'] = $price;
	                $data['product_desc']['price_range'] = $this->get_pricerangeID($price);
	                $data['product_desc']['created_by'] = $brand_id;
	                $data['product_desc']['created_datetime'] = date('Y-m-d H:i:s');
	                $data['product_desc']['product_discount'] = $product_discount;
	                $data['product_desc']['discount_start_date'] = $discount_start_date;
	                $data['product_desc']['discount_end_date'] = $discount_end_date;
	                $data['product_desc']['status'] = 1;
	                $data['product_desc']['is_promotional'] = 0;
	                $data['product_desc']['approve_reject'] = 'P';
	                $data['product_desc']['is_delete'] = 0;

	                // Update Product information

	                $data['product_udesc']['price'] = $price;
	                $data['product_udesc']['price_range'] = $this->get_pricerangeID($price);
	                $data['product_udesc']['product_discount'] = $product_discount;
	                $data['product_udesc']['discount_start_date'] = $discount_start_date;
	                $data['product_udesc']['discount_end_date'] = $discount_end_date;
					$data['product_udesc']['created_by'] = $brand_id;

	                if(!empty($product_imges)) { $img_inc = 0;
	                	foreach($product_imges as $pimg){
	                		
	                		
	                		$imagename = basename($pimg);  
	                                                     
	                        $ext = pathinfo($imagename, PATHINFO_EXTENSION);
							
							if(!in_array($ext,$allowed_ext)){ $ok = 0;  }else{
	                                $newimagename = time().mt_rand(0,10000).'.'.$ext; 
	                                if($img_inc == 0 &&  $product_id==0){ 
	                                	$data['product_desc']['image'] =  $newimagename; 
	                                	$this->saveProductImage($pimg,$newimagename);

	                                }else{
	                					$data['product_extra_images'][] =  $newimagename; 
	                					$this->saveProductExtraImages($pimg,$newimagename);
	                				}
	                               	//$data['product_desc']['image'] =$newimagename;
	                               
	                        }


	                		$img_inc++;
	                	}
	                }

	                if(!empty($product_stock) && !empty($product_stock[0])) { $s = 0;
	                	foreach($product_stock[0] as $size => $stock){
	                			
	                			//$data['product_stock']['size'][] = $size; 
	                			$data['product_stock']['size'][$s] = $size; 
	                			$data['product_stock']['stock'][$s] = $stock; 
	                			$s++;
	                		}
	                }
	                
	                if(!empty($product_tags)) {
	                	foreach($product_tags as $tags){
	                			$data['product_tags'][] = $tags; 
	                		}
	                }

	                
	                if($product_id == 0 ){
	                    // Insert the product
	                    $data['product_desc']['slug'] = str_replace(' ','-',strtolower($product_name)).'-'.time().mt_rand();
	                	$this->Scproductinventorycron_model->add_product($data);
	                	$a++;
	                }else{
	                    // update the product
	                    $data['product_id'] = $product_id;
	                    $this->Scproductinventorycron_model->update_product($data);
	                    $u++;
	                }

	            }else{
	            	
	            	$e++;
	            }

            }
        }
      
        $error['success'] = "Successfully added ".$a;
        $error['updated'] = "Successfully updated ".$u;
        if($e>0){ $error['error'] = "Errors found ".$e; }
        file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=+919819645032&message=cronrunend');	
        echo json_encode($error);
    }*/
    //--------------Forever Data Processed and add



    

    public function get_categoryID($category_id){
    	if($category_id!=''){
    		return $this->Scproductinventorycron_model->get_categoryID($category_id);
    	}else{
    		return 1;
    	}
    }

    public function get_subcategoryID($category_id){
    	if($category_id!=''){
    		return $this->Scproductinventorycron_model->get_subcategoryID($category_id);
    	}else{
    		return 1;
    	}
    }

    public function get_pricerangeID($price){
    	if($price > 50000){ return 4670; }
    	else if($price < 50000 && $price > 15000){ return 38; }
    	else if($price < 15000 && $price > 5000){ return 37; }
    	else { return 36; }

    }


    public function saveProductImage($pimg,$newimagename){
    	 if(file_exists($pimg))
		        {
		        	$ok =0;
		        }else{//
		            file_put_contents('./sc_admin/assets/products/'.$newimagename, file_get_contents($pimg));
		            $config['image_library'] = 'gd2';
		            $config['source_image'] = './sc_admin/assets/products/'.$newimagename;
		            $config['new_image'] = './sc_admin/assets/products/thumb/';
		            $config['create_thumb'] = TRUE;
		            $config['thumb_marker'] = '';
		            $config['maintain_ratio'] = TRUE;
		            $config['width'] = 200;
		            $config['height'] = 200;

		            $this->image_lib->initialize($config);

		                if (!$this->image_lib->resize()){
		                        $this->image_lib->display_errors();
		                        $ok =0;
							}else{
								$ok =1;
		                        }
		        }

		           /* 310 thumb creation*/
		            $config['image_library'] = 'gd2';
		            $config['source_image'] = './sc_admin/assets/products/'.$newimagename;
		            $config['new_image'] = './sc_admin/assets/products/thumb_310/';
		            $config['create_thumb'] = TRUE;
		            $config['thumb_marker'] = '';
		            $config['maintain_ratio'] = TRUE;
		            $config['width'] = 310;
		            //$config['height'] = 200;

		            $this->image_lib->initialize($config);

		                if (!$this->image_lib->resize()){
		                        $this->image_lib->display_errors();
		                        $ok =0;
							}else{
								$ok =1;
		            }

		               /* 310 thumb creation*/
		            $config['image_library'] = 'gd2';
		            $config['source_image'] = './sc_admin/assets/products/'.$newimagename;
		            $config['new_image'] = './sc_admin/assets/products/thumb_160/';
		            $config['create_thumb'] = TRUE;
		            $config['thumb_marker'] = '';
		            $config['maintain_ratio'] = TRUE;
		            $config['width'] = 160;
		            //$config['height'] = 200;

		            $this->image_lib->initialize($config);

		                if (!$this->image_lib->resize()){
		                        $this->image_lib->display_errors();
		                        $ok =0;
							}else{
								$ok =1;
		            }

		            /* Thumb for 274x340  */
		            $size = getimagesize('./sc_admin/assets/products/'.$newimagename);
		            if(!empty($size))
		            {
		                if($size[1]<=340)
		                {
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = './sc_admin/assets/products/'.$newimagename;
                            $config['new_image'] = './sc_admin/assets/products/thumb_340/';
                            $config['create_thumb'] = TRUE;
                            $config['thumb_marker'] = '';
                            $config['maintain_ratio'] = TRUE;
                            //$config['width'] = 310;
                            $config['height'] = $size[1];

                            $this->image_lib->initialize($config);

                            if (!$this->image_lib->resize())
                            {
                                $this->image_lib->display_errors();
                                $ok =0;

                            }else{

                                $ok =1;
                            }
                        }else{
                        	$config['image_library'] = 'gd2';
                            $config['source_image'] = './sc_admin/assets/products/'.$newimagename;
                            $config['new_image'] = './sc_admin/assets/products/thumb_340/';
                            $config['create_thumb'] = TRUE;
                            $config['thumb_marker'] = '';
                            $config['maintain_ratio'] = TRUE;
                            //$config['width'] = 310;
                            $config['height'] = 340;

                            $this->image_lib->initialize($config);

                            if (!$this->image_lib->resize())
                            {
                                $this->image_lib->display_errors();
                                $ok =0;

                            }else{

                                $ok =1;
                            }
                        }
                    }
                     /* Thumb for 440x546   */
		            if(!empty($size))
		            {
		                if($size[1]<=546)
		                {
                            /* Thumb for 440x546   */
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = './sc_admin/assets/products/'.$newimagename;
                            $config['new_image'] = './sc_admin/assets/products/thumb_546/';
                            $config['create_thumb'] = TRUE;
                            $config['thumb_marker'] = '';
                            $config['maintain_ratio'] = TRUE;
                            //$config['width'] = 310;
                            $config['height'] = $size[1];

                            $this->image_lib->initialize($config);

                            if (!$this->image_lib->resize())
                            {
                                $this->image_lib->display_errors();
                                $ok =0;

                            }else{

                                $ok =1;
                            }
	                    }else{
	                    	 /* Thumb for 440x546   */
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = './sc_admin/assets/products/'.$newimagename;
                            $config['new_image'] = './sc_admin/assets/products/thumb_546/';
                            $config['create_thumb'] = TRUE;
                            $config['thumb_marker'] = '';
                            $config['maintain_ratio'] = TRUE;
                            //$config['width'] = 310;
                            $config['height'] = 546;

                            $this->image_lib->initialize($config);

                            if (!$this->image_lib->resize())
                            {
                                $this->image_lib->display_errors();
                                $ok =0;

                            }else{

                                $ok =1;
                            }
	                    }
	                }

	                /* Thumb for 100x124    */
		            if(!empty($size))
		            {
		                if($size[1]<=124)
		                {
                            /* Thumb for 100x124    */
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = './sc_admin/assets/products/'.$newimagename;
                            $config['new_image'] = './sc_admin/assets/products/thumb_124/';
                            $config['create_thumb'] = TRUE;
                            $config['thumb_marker'] = '';
                            $config['maintain_ratio'] = TRUE;
                            //$config['width'] = 310;
                            $config['height'] = $size[1];

                            $this->image_lib->initialize($config);

                            if (!$this->image_lib->resize())
                            {
                                $this->image_lib->display_errors();
                                $ok =0;

                            }else{

                                $ok =1;
                            }
                        }else{
                        	/* Thumb for 100x124    */
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = './sc_admin/assets/products/'.$newimagename;
                            $config['new_image'] = './sc_admin/assets/products/thumb_124/';
                            $config['create_thumb'] = TRUE;
                            $config['thumb_marker'] = '';
                            $config['maintain_ratio'] = TRUE;
                            //$config['width'] = 310;
                            $config['height'] = 124;

                            $this->image_lib->initialize($config);

                            if (!$this->image_lib->resize())
                            {
                                $this->image_lib->display_errors();
                                $ok =0;

                            }else{

                                $ok =1;
                            }
                        }
                    }
    }

    public function saveProductExtraImages($pimg,$newimagename){
    	 if(file_exists($pimg))
		        {
		            $ok =0;
		        }else{
		            
		            file_put_contents('./sc_admin/assets/products_extra/'.$newimagename, file_get_contents($pimg));
		            $config['image_library'] = 'gd2';
		            $config['source_image'] = './sc_admin/assets/products_extra/'.$newimagename;
		            $config['new_image'] = './sc_admin/assets/products_extra/thumb/';
		            $config['create_thumb'] = TRUE;
		            $config['thumb_marker'] = '';
		            $config['maintain_ratio'] = TRUE;
		            $config['width'] = 200;
		            $config['height'] = 200;

		            $this->image_lib->initialize($config);

		                if (!$this->image_lib->resize()){
		                        $this->image_lib->display_errors();
		                        $ok =0;
							}else{
								$ok =1;
		                        }

		            /* 160 thumb creation*/

		            $config['image_library'] = 'gd2';
		            $config['source_image'] = './sc_admin/assets/products_extra/'.$newimagename;
		            $config['new_image'] = './sc_admin/assets/products_extra/thumb_160/';
		            $config['create_thumb'] = TRUE;
		            $config['thumb_marker'] = '';
		            $config['maintain_ratio'] = TRUE;
		            $config['width'] = 160;
		            //$config['height'] = 200;

		            $this->image_lib->initialize($config);

		                if (!$this->image_lib->resize()){
		                        $this->image_lib->display_errors();
		                        $ok =0;
							}else{
								$ok =1;
		            }

		            /* 310 thumb creation*/
		            $config['image_library'] = 'gd2';
		            $config['source_image'] = './sc_admin/assets/products_extra/'.$newimagename;
		            $config['new_image'] = './sc_admin/assets/products_extra/thumb_310/';
		            $config['create_thumb'] = TRUE;
		            $config['thumb_marker'] = '';
		            $config['maintain_ratio'] = TRUE;
		            $config['width'] = 310;
		            //$config['height'] = 200;

		            $this->image_lib->initialize($config);

		                if (!$this->image_lib->resize()){
		                        $this->image_lib->display_errors();
		                        $ok =0;
							}else{
								$ok =1;
		            		}

		            //--------------New Code
		            /* Thumb for 274x340  */
		            $size = getimagesize('./sc_admin/assets/products_extra/'.$newimagename);
		            if(!empty($size))
		            {
		                if($size[1]<=340)
		                {
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = './sc_admin/assets/products_extra/'.$newimagename;
                            $config['new_image'] = './sc_admin/assets/products_extra/thumb_340/';
                            $config['create_thumb'] = TRUE;
                            $config['thumb_marker'] = '';
                            $config['maintain_ratio'] = TRUE;
                            //$config['width'] = 310;
                            $config['height'] = $size[1];

                            $this->image_lib->initialize($config);

                            if (!$this->image_lib->resize())
                            {
                                $this->image_lib->display_errors();
                                $ok =0;

                            }else{

                                $ok =1;
                            }
                        }else{
                        	$config['image_library'] = 'gd2';
                            $config['source_image'] = './sc_admin/assets/products_extra/'.$newimagename;
                            $config['new_image'] = './sc_admin/assets/products_extra/thumb_340/';
                            $config['create_thumb'] = TRUE;
                            $config['thumb_marker'] = '';
                            $config['maintain_ratio'] = TRUE;
                            //$config['width'] = 310;
                            $config['height'] = 340;

                            $this->image_lib->initialize($config);

                            if (!$this->image_lib->resize())
                            {
                                $this->image_lib->display_errors();
                                $ok =0;

                            }else{

                                $ok =1;
                            }
                        }
                    }
                     /* Thumb for 440x546   */
		            if(!empty($size))
		            {
		                if($size[1]<=546)
		                {
                            /* Thumb for 440x546   */
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = './sc_admin/assets/products_extra/'.$newimagename;
                            $config['new_image'] = './sc_admin/assets/products_extra/thumb_546/';
                            $config['create_thumb'] = TRUE;
                            $config['thumb_marker'] = '';
                            $config['maintain_ratio'] = TRUE;
                            //$config['width'] = 310;
                            $config['height'] = $size[1];

                            $this->image_lib->initialize($config);

                            if (!$this->image_lib->resize())
                            {
                                $this->image_lib->display_errors();
                                $ok =0;

                            }else{

                                $ok =1;
                            }
	                    }else{
	                    	 /* Thumb for 440x546   */
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = './sc_admin/assets/products_extra/'.$newimagename;
                            $config['new_image'] = './sc_admin/assets/products_extra/thumb_546/';
                            $config['create_thumb'] = TRUE;
                            $config['thumb_marker'] = '';
                            $config['maintain_ratio'] = TRUE;
                            //$config['width'] = 310;
                            $config['height'] = 546;

                            $this->image_lib->initialize($config);

                            if (!$this->image_lib->resize())
                            {
                                $this->image_lib->display_errors();
                                $ok =0;

                            }else{

                                $ok =1;
                            }
	                    }
	                }

	                /* Thumb for 100x124    */
		            if(!empty($size))
		            {
		                if($size[1]<=124)
		                {
                            /* Thumb for 100x124    */
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = './sc_admin/assets/products_extra/'.$newimagename;
                            $config['new_image'] = './sc_admin/assets/products_extra/thumb_124/';
                            $config['create_thumb'] = TRUE;
                            $config['thumb_marker'] = '';
                            $config['maintain_ratio'] = TRUE;
                            //$config['width'] = 310;
                            $config['height'] = $size[1];

                            $this->image_lib->initialize($config);

                            if (!$this->image_lib->resize())
                            {
                                $this->image_lib->display_errors();
                                $ok =0;

                            }else{

                                $ok =1;
                            }
                        }else{
                        	/* Thumb for 100x124    */
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = './sc_admin/assets/products_extra/'.$newimagename;
                            $config['new_image'] = './sc_admin/assets/products_extra/thumb_124/';
                            $config['create_thumb'] = TRUE;
                            $config['thumb_marker'] = '';
                            $config['maintain_ratio'] = TRUE;
                            //$config['width'] = 310;
                            $config['height'] = 124;

                            $this->image_lib->initialize($config);

                            if (!$this->image_lib->resize())
                            {
                                $this->image_lib->display_errors();
                                $ok =0;

                            }else{

                                $ok =1;
                            }
                        }
                    }
		        }
    }

    public function encryptOrDecrypt($mprhase, $crypt) {
         $MASTERKEY = "STYLECRACKERAPI";
         $td = @mcrypt_module_open('tripledes', '', 'ecb', '');
         $iv = @mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
         @mcrypt_generic_init($td, $MASTERKEY, $iv);
         if ($crypt == 'encrypt')
         {
             $return_value = @base64_encode(mcrypt_generic($td, $mprhase));
         }
         else
         {
             $return_value = @mdecrypt_generic($td, base64_decode($mprhase));
         }
         @mcrypt_generic_deinit($td);
         @mcrypt_module_close($td);
         return $return_value;
    }

    function products_upload_in(){
    	$data = $_REQUEST;
    	$data['api_key'] = $_REQUEST['scapi_key'];
    	$data['secret_key'] = $_REQUEST['scsecret_key'];
    	$this->bulk_uploadproducts_get($data);
    }

    function bulk_uploadproducts_get($inputs){
    	//echo "------------------------started";
    	if(isset($inputs['url']) && $inputs['url'] != "")
    		$product_json =file_get_contents($inputs['url'])or die("Error reading file $path");
    	else if(isset($inputs['products']))
    		$product_json = $inputs['products'];
        $api_key = $inputs['api_key'];
		$secret_key = $inputs['secret_key'];

		$error['msg'] = '200 OK';
        $a = 0; $u = 0; $e = 0;
        $allowed_ext =  array('gif','png' ,'jpg','jpeg','GIF','PNG' ,'JPG','JPEG');
        $products = array();
       /* if (array_key_exists('scapi_key', $str)) {
            $api_key = trim($str['scapi_key']);
        }
        
        if (array_key_exists('scsecret_key', $str)) {
            $secret_key = trim($str['scsecret_key']);
        }

        if (array_key_exists('products', $str)) { 
            $allproducts = trim($str['products']); 
            $products = json_decode($allproducts); 
        }*/

        if(isset($product_json)){
        	$allproducts = trim($product_json); 
            $products = json_decode($product_json); //print_r($products);exit;
        }

        if($api_key==''){
            $error['msg'] = 'API key is missing';
        }else if($secret_key==''){
            $error['msg'] = 'Secret Key is missing';
        }else if(empty($products)){
            $error['msg'] = 'Products does not exist';
        }else if($api_key!='' && $secret_key!=''){
           	$brand_id = $this->Scproductinventorycron_model->get_brand_id($this->encryptOrDecrypt($api_key,''),$this->encryptOrDecrypt($secret_key,''));
	            if($brand_id == 0){
	            	$error['msg'] = 'Invalid API Key and Secret Key';
	            }
        }
        
        $data = array();
        if(!empty($products) && $brand_id>0){  
        	$brand_code = $this->Scproductinventorycron_model->getbrand_code($brand_id);
       	
        	//------ Condition for delete inventory if running full records intry for perticuller product to remove all out of stock products
        	if(isset($inputs['delete_inventory']) && $inputs['delete_inventory'] == true){
            	//$this->db->query("delete from product_inventory where product_id in (select id from product_desc where brand_id = $brand_id)");
            	$this->db->query("update product_inventory set stock_count = 0 where product_id in (select id from product_desc where brand_id = $brand_id)");
            	echo "UI";
            }

        	$cnt_prod=0;
        	$cnt_tags=0;
        	$cnt_prod_update=0;
        	$last_product_img=(string)"NO";
            foreach($products as $value){
                if (isset($value->product_sku)) { $product_sku = strip_tags($value->product_sku); } else{ $product_sku =''; }
                if (isset($value->product_color) && strlen($value->product_color) > 0) { $product_color = strtolower(strip_tags($value->product_color)); } else{ $product_color ='default'; }
                if (isset($value->product_category)) { $product_category = strip_tags($value->product_category); } else{ $product_category =''; }
                if (isset($value->product_subcategory)) { $product_subcategory = strip_tags($value->product_subcategory); } else{ $product_subcategory =''; }
               	if (isset($value->product_name)) { $product_name = strip_tags($value->product_name); } else{ $product_name =''; }
                if (isset($value->product_url)) { $product_url = strip_tags($value->product_url); } else{ $product_url =''; }
                if (isset($value->product_description)) { $product_description = strip_tags($value->product_description); } else{ $product_description =''; }
                if (isset($value->price)) { $price = (float)strip_tags($value->price); } else{ $price =''; }
                if (isset($value->age_group)) { $age_group = strip_tags($value->age_group); } else{ $age_group =''; }
                if (isset($value->product_discount)) { $product_discount = strip_tags($value->product_discount); } else{ $product_discount =''; }
                if (isset($value->discount_start_date)) { $discount_start_date = strip_tags($value->discount_start_date); } else{ $discount_start_date =''; }
                if (isset($value->discount_end_date)) { $discount_end_date = strip_tags($value->discount_end_date); } else{ $discount_end_date =''; }  
                if (isset($value->discount_percentage)) { $discount_percentage = strip_tags($value->discount_percentage); } else{ $discount_percentage =''; }  
                if(!empty($value->product_img)) { $product_imges = $value->product_img; }else{ $product_imges = array(); }
                if(!empty($value->product_stock)) { $product_stock = $value->product_stock; }else{ $product_stock = array(); }
                if(!empty($value->product_tags)) { $product_tags = $value->product_tags; }else{ $product_tags = array(); }
                @$brand_product_id = $value->brand_product_id;
                //print_r($product_tags);
                /*$this->db->query("update product_desc set product_color='$product_color' where url = '$product_url' and product_color='default'");
                echo $this->db->last_query();
                continue;*/
                echo ". ";
                $product_imges = array_filter($product_imges);
                if($product_url!='')
                	$prod_exist = $this->Scproductinventorycron_model->get_product_id_color($product_url, $product_color);//get_product_id_sku($product_sku,$product_url);//
				//echo $this->db->last_query();
                if($product_name!='' && $price>0 && $product_sku!='' && $product_url!='' && !empty($product_imges) && isset($prod_exist[0]['id'])){
	                $product_id = $prod_exist[0]['id'];
                	$price = ceil($price);
                	//$this->db->query("update product_desc set product_color='$product_color' where id = $product_id");

	                
	                //$product_sku_exi = $prod_exist[0]['product_sku'];
	                
	                /*echo "<br/>product id : $product_id,  product name : $product_name color : $product_color<br/>";
	                continue;*/

	                $stk['product_id'] = addslashes($product_id);
	                $pro['product_sku'] = addslashes($product_sku);
	                $pro['product_cat_id'] = $this->get_categoryID(addslashes($product_category));
	                $pro['product_sub_cat_id'] = $this->get_subcategoryID(addslashes($product_subcategory));
	                $pro['brand_id'] = addslashes($brand_id);
	                $pro['name'] = addslashes($product_name);
	                $pro['url'] = addslashes($product_url);
	                $pro['description'] = addslashes($product_description);
	                $pro['product_color'] = addslashes($product_color);
	                
	                $pro['price'] = addslashes($price);
	                $pro['price_range'] = addslashes($this->get_pricerangeID($price));
	                $pro['created_by'] = addslashes($brand_id);
	                $pro['created_datetime'] = date('Y-m-d H:i:s');
	                $pro['product_discount'] = addslashes($product_discount);
	                $pro['discount_start_date'] = addslashes($discount_start_date);
	                $pro['discount_end_date'] = addslashes($discount_end_date);
	                //$desc['discount_percentage'] = addslashes($discount_percentage);
	                $pro['status'] = 1;
	                $pro['is_promotional'] = 0;
	                $pro['approve_reject'] = 'P';
	                $pro['is_delete'] = 0;

	                /* Update Product information*/

	                $desc['id'] = addslashes($product_id);
	                $desc['price'] = addslashes($price);
	             	$desc['product_sku'] = $product_sku;
	             	//$desc['product_color'] = $product_color;
	            	$desc['price_range'] = addslashes($this->get_pricerangeID($price));
	                $desc['product_discount'] = addslashes($product_discount);
	                $desc['discount_start_date'] = addslashes($discount_start_date);
	                $desc['discount_end_date'] = addslashes($discount_end_date);
	                //$desc['discount_percentage'] = addslashes($discount_percentage);
					$desc['created_by'] = addslashes($brand_id);
					

					if($product_id == 0){
						$cnt_prod++;
					}else{
						$cnt_prod_update++;
					}
					$new_product_img = (string)$product_url.$product_color;
					if(!empty($product_imges) && $product_id == 0 && $last_product_img !== $new_product_img ) {
	                 	$img_inc = 0;
	                 	$last_product_img = (string)$new_product_img;
	                 	
	                	foreach($product_imges as $pimg){
							$imagename = basename($pimg);  
	                                                     
	                        $ext = pathinfo($imagename, PATHINFO_EXTENSION);
							
							if(!in_array($ext,$allowed_ext)){ $ok = 0;  }else{
	                                $newimagename = addslashes(time().mt_rand(0,10000).'.'.$ext); 
	                                if($img_inc == 0 &&  $product_id==0){ 
	                                	$pro['image'] =  $newimagename; 
	                                	$this->saveProductImage($pimg,$newimagename);

	                                }else{
	                                	if(!isset($data_add['sql_extra_img'])){
	                                		$data_add['sql_extra_img'] = "INSERT INTO `extra_images` (`created_datetime`, `product_id`, `product_images`) VALUES ";
	                                	}
	                					$image_info =  $newimagename;
	                					$data_add['sql_extra_img'] .= "('".date('Y-m-d H:i:s')."', (select id from product_desc where url = '$product_url' and product_color='$product_color' limit 1), '$image_info'), ";
	                					$this->saveProductExtraImages($pimg,$newimagename);
	                				}
	                               
	                        }


	                		$img_inc++;
	                	}
	                }
	                

	                if(!empty($product_stock) && !empty($product_stock[0])) { 
	                	$s = 0;
	                	$product_stock[0] = (array)$product_stock[0];
	                	$product_stock[0] = array_filter($product_stock[0]);
	                	
	                	foreach($product_stock[0] as $size => $stock){
	                		$size = addslashes($size);
	                		$stock = addslashes($stock);
	                		if($product_id != 0){
                				$stk['product_id'] = $product_id; 
                				if(!isset($data['sql_stock_inventory_update']) || $data['sql_stock_inventory_update'] == ""){
                					$data['sql_stock_inventory_update']="select ";
                				}
                				/*if(!isset($data['sql_stock_inventory_update'])){
                            		$data['sql_stock_inventory_update'] = "INSERT INTO `product_inventory` (`product_id`, `product_sku`, `size_id`, `stock_count`, `brand_product_id`) VALUES ";
                            	}
            					$data['sql_stock_inventory_update'] .= "($product_id,'$product_sku',add_size('$size'),'$stock','$brand_product_id'), ";*/
            					@$data['sql_stock_inventory_update'] .= "inventory_update('$product_id', '$product_sku', '$size', '$stock', '$brand_product_id'),";

            				}
                			else{
                				$subquery = $stk['product_id'] = "(select id from product_desc where url = '$product_url' and product_color='$product_color' limit 1)";
                				if(!isset($data_add['sql_stock_inventory_add'])){
                        			$data_add['sql_stock_inventory_add'] = "INSERT INTO `product_inventory` (`product_id`, `product_sku`, `size_id`, `stock_count`, `brand_product_id`) VALUES ";
                            	}
            					$data_add['sql_stock_inventory_add'] .= "($subquery,'$product_sku',add_size('$size'),'$stock','$brand_product_id'), ";

							}
							$delete['product_id'] = $product_id;
                			
                			$stk['size_id'] = $size; 
                			$stk['stock_count'] = $stock; 
                			$stk['product_sku'] = $product_sku;
                			$stock_data['delete'][] = $product_id;

                			$s++;
	                	}//echo "stock";
	                }
	                
	                if(!empty($product_tags)) {
	                	$product_tags = (array)$product_tags;
	                	$product_tags = array_filter($product_tags);
	                	
	                	foreach($product_tags as $tags){
	                			$tags = addslashes($tags);
	                			$subquery =  "(select id from product_desc where url = '$product_url' and product_color='$product_color'  limit 1)";
								$create_date = date('Y-m-d H:i:s');
								$tag_id = "add_tags('".strtolower(trim($tags))."', '".date('Y-m-d H:i:s')."', '".str_replace(' ','-',strtolower($tags))."-".time().mt_rand()."')";
								
								
								if($product_id != 0){
	                				if(!isset($data['sql_product_tag_update'])){
	                            		$data['sql_product_tag_update'] = "INSERT INTO `product_tag` (`created_by`, `created_datetime`, `product_id`, `status`, `tag_id`) VALUES ";
	                            	}
	            					$data['sql_product_tag_update'] .= "('$brand_id','$create_date',$subquery,1,$tag_id), ";
	            					
								}
	                			else{
	                				if(!isset($sql_product_tag_add)){
	                            		$sql_product_tag_add = "INSERT INTO `product_tag` (`created_by`, `created_datetime`, `product_id`, `status`, `tag_id`) VALUES ";
	                            	}
	            					$sql_product_tag_add .= "('$brand_id','$create_date',$subquery,1,$tag_id), ";

	            					$cnt_tags++;
	            					if($cnt_tags==200){
	            						$cnt_tags=0;
	            						$data_add['sql_product_tag_add'][] = $sql_product_tag_add;
	            						unset($sql_product_tag_add);
	            					}

	                			}
	                		}//echo "tag";
	                }




	                if(!empty($discount_percentage)) {
	                	
	                	$subquery =  "(select id from product_desc where url = '$product_url' and product_color='$product_color' limit 1)";

	                	if($product_id > 0)
	                	{
	        				/*if(!isset($data['sql_product_discount_update']))
	        				{
	                    		$data['sql_product_discount_update'] = "INSERT INTO `discount_availed` (`product_id`,`discount_percent`,`discount_price`,`start_date`,`end_date`,`created_by`, `created_datetime`) VALUES ";
	                    	}*/
	    					//$data['sql_product_discount_update'] .= "('$subquery','$discount_percentage','$product_discount','$discount_start_date','$discount_end_date','$brand_id','$create_date'),";
	    					if((!isset($last_pid)  || $last_pid != $product_id)){
	    						
	    						$create_date = date("Y-m-d H:i:s"); 
		    					@$data['sql_product_discount_update'] .= "UPDATE `discount_availed` SET discount_percent='$discount_percentage', discount_price='$product_discount',
		    					start_date='$discount_start_date', end_date='$discount_end_date', created_by='$brand_id',modified_datetime='$create_date' WHERE product_id='$product_id';";

								$data['sql_product_discount_update'] .= "INSERT INTO `discount_availed` (`product_id`,`discount_percent`,`discount_price`,`start_date`,`end_date`,`created_by`, `created_datetime`,status) select *from (select $product_id,'$discount_percentage','$product_discount','$discount_start_date','$discount_end_date','$brand_id','$create_date',1) as tmp WHERE NOT EXISTS ( SELECT product_id FROM discount_availed WHERE product_id = '$product_id');";	            	
								$data['sql_product_discount_update'];
								//exit;

								$last_pid=	$product_id;
/*
								if(!isset($data['sql_product_discount_update'])){
                        			$data['sql_product_discount_update'] = "INSERT INTO `discount_availed` (`product_id`,`discount_percent`,`discount_price`,`start_date`,`end_date`,`created_by`, `created_datetime`) VALUES ";
                        		}
    						
    							$data['sql_product_discount_update'] .= "($subquery,'$discount_percentage','$product_discount','$discount_start_date','$discount_end_date','$brand_id','$create_date');";*/
							}
							
						}
            			else{
            					if(!isset($sql_product_discount_add)){
            						$create_date = date("Y-m-d H:i:s"); 
                        			$sql_product_discount_add = "INSERT INTO `discount_availed` (`product_id`,`discount_percent`,`discount_price`,`start_date`,`end_date`,`created_by`, `created_datetime`,status) VALUES ";
                        		}
    						
    							$sql_product_discount_add .= "($subquery,'$discount_percentage','$product_discount','$discount_start_date','$discount_end_date','$brand_id','$create_date'),1";
	                		}	                		
	                	$cnt_tags++;
    					if($cnt_tags==200){
    						$cnt_tags=0;
    						$data_add['sql_product_discount_add'][] = $sql_product_discount_add;
    						unset($sql_product_discount_add);
    					}    				 
	            	}

	                
	                if($product_id == 0 ){
	                	
	                    $pro['slug'] = str_replace(' ','-',strtolower($product_name)).'-'.time().mt_rand();
	                	$data_add['product_desc_add']['data'][$product_url.$product_color] = $pro;
	                    
	                	if($cnt_prod >= 600){
	                		$cnt_prod=0;
	                		if(isset($sql_product_tag_add)){
	                			$data_add['sql_product_tag_add'][] = $sql_product_tag_add;
	                			unset($sql_product_tag_add);
	                		}
	                		$this->Scproductinventorycron_model->add_product_bulk($data_add);
							unset($data_add);echo "added";
	                	}
	                	//	echo "id -0";
	                	$a++;
	                }else{
	                	$data['product_desc_update']['product_udesc'][] = $desc;
	                	//foreach($stock_data['delete'] as $row){
		                	//$data['product_desc_update']['product_stock']['delete'][] = "";//$row;
		                //}
	                	if($cnt_prod_update >= 600){
        					$this->Scproductinventorycron_model->update_product_bulk($data);
        					unset($data);
        					$cnt_prod_update=0;echo "updated";
        				}
	        			
        			//	echo "id exist";

	                    $u++;
	                }
	                unset($stock_data);
	            }else{
	            	$e++;
	            }
            }
        }
//echo "end";
        if(isset($sql_product_tag_add))
        	$data_add['sql_product_tag_add'][] = $sql_product_tag_add;
        
        if(isset($data_add))
        	$this->Scproductinventorycron_model->add_product_bulk($data_add);

        if(isset($data))
        	$this->Scproductinventorycron_model->update_product_bulk($data);
        //print_r($data);
        $error['success'] = "Successfully added ".$a;
        $error['updated'] = "Successfully updated ".$u;
        if($e>0){ $error['error'] = "Errors found ".$e; }
        echo json_encode($error);
    }

    function get_brand_new_all_category($brand_id){
    	$sql = "SELECT 
				    category_name, brand_id, sc_category_id, category_parent_id
				FROM
				    brand_new_categories_subcategories
				where brand_id = '$brand_id'
				GROUP BY category_name , brand_id, category_parent_id;";
		$result = $this->db->query($sql);
    	return $result = $result->result_array();
    }

    function get_category_id_from_array($sub_category_name,$brand_id,$data,$category_id){
		//print_r($data);
    	foreach($data as $row){
  //  		    	echo "<pre>";
//print_r($data);

    		/*echo $row['category_name']." == $sub_category_name && ".$row['brand_id']." == $brand_id && $category_id == ".$row['category_parent_id'];

    		echo "<br/>";

    		echo $row['sc_category_id'];

    		echo "<br/>";
    		continue;		*/
    		if($row['category_name'] == $sub_category_name && $row['brand_id'] == $brand_id && $category_id == $row['category_parent_id']){
    			if((int)$row['sc_category_id'] > 0){
    				return $row['sc_category_id'];
    			}
    			return 0;
    		}else{
    			$return = "";
    		}
    		$return = "";
    	}

    	return $return;

    }

    function get_category_ids($category_id, $sub_category_name, $brand_id, $product_id, $sc_category_id,$latest_category_name){//
		
		//echo $sub_category_name;
    	$category_name = $sub_category_name; 
    	$category_parent = $category_id;
    	
    	if((string)$sc_category_id!= "" && (int)$sc_category_id > 0 ){
		    $sql = "SELECT GROUP_CONCAT(T2.id) parent FROM (SELECT
						        @r AS _id,
						        (SELECT @r := category_parent FROM category WHERE id = _id) AS category_parent,
						        @l := @l + 1 AS lvl
						    FROM
						        (SELECT @r := ".$sc_category_id.") vars,
						        category m
						    WHERE @r <> 0) T1
						JOIN category T2
						ON T1._id = T2.id
						ORDER BY T1.lvl DESC limit 1";
			/*) parent from category ct
						where id = ".$row['id']." limit 1;*/
			$result = $this->db->query($sql);
    		$result = $result->result_array();
			$parents = explode(',', $result[0]['parent']);
			return $result[0]['parent'];
		}

		if((string)$sc_category_id == (string)'0' ){
    		//echo $sub_category_name;
    		return "";
    	}
    	
    	$sql = "select id from category ct where category_name = '$category_name';";
		$result = $this->db->query($sql);
    	$result = $result->result_array();
    	
    	if($result){
	    	foreach($result as $row){
	    		//select @iddd:= ct.id id, (
	    		$sql = "SELECT GROUP_CONCAT(T2.id) parent FROM (SELECT
							        @r AS _id,
							        (SELECT @r := category_parent FROM category WHERE id = _id) AS category_parent,
							        @l := @l + 1 AS lvl
							    FROM
							        (SELECT @r := ".$row['id'].") vars,
							        category m
							    WHERE @r <> 0) T1
							JOIN category T2
							ON T1._id = T2.id
							ORDER BY T1.lvl DESC limit 1";
				/*) parent from category ct
							where id = ".$row['id']." limit 1;*/
				$result = $this->db->query($sql);
	    		$result = $result->result_array();
				
					
				$parents = explode(',', $result[0]['parent']);
				if(in_array($category_parent, $parents)){
					return $result[0]['parent'];
				}else{
					if($sc_category_id == ""){
						$sql = "INSERT INTO `brand_new_categories_subcategories`
									(`product_id`,
									`category_name`,
									`created_by`,
									`brand_id`,
									`category_parent_id`)
									VALUES
									('$product_id',
									'$category_name',
									'$brand_id',
									'$brand_id',
									'$category_parent');";
						$this->db->query($sql);
					}
					return "";
				}
	    	}
	    }else{
	    	//echo $sc_category_id."--------------".in_array($category_name, $latest_category_name) .'<br/>';exit;	    	
	    	//echo '--------------------'; 
	    	//var_dump($latest_category_name);
	    	//var_dump(in_array($category_name, $latest_category_name)) .'<br/>';
	    	//$sc_category_id = "";
	    	//$category_name = "Accessories";	    	
	    	//$category_name = "Apparel";	    	
	    	

	    	/*if(in_array($category_name,$latest_category_name) && in_array($category_name,$latest_category_name) > 0){
	    		echo "</br/>In array";//.in_array($category_name,$latest_category_name);	
	    		print_r($category_name);
	    	}else{
			    echo "</br/>Not in array".in_array($category_name,$latest_category_name);
	    		//echo "Value insert";
	    		print_r($category_name);*/
	    	$is_exist = 0;
	    	foreach ($latest_category_name as $value) {
	    		if((string)$value['parent'] ==  (string)$category_parent && (string)$value['child_name'] == (string)$sub_category_name){
				    $is_exist = 1;
				    continue;
				}
			}
			if((int)$is_exist == (int)0){
				//echo $is_exist;
				$sql = "INSERT INTO `brand_new_categories_subcategories`
											(`product_id`,
											`category_name`,
											`created_by`,
											`brand_id`,
											`category_parent_id`)
											VALUES
											('$product_id',
											'$category_name',
											'$brand_id',
											'$brand_id',
											'$category_parent');";
				$this->db->query($sql);
			}
/*			}
*/

			
			return "";
	    }
    }

    function email($from, $to, $subject, $message){
    	// Set SMTP Configuration
		$emailConfig = array(
		   'protocol' => 'smtp',
		    'smtp_host' => 'ssl://smtp.googlemail.com',
		    'smtp_port' => 465,
		    'smtp_user' => 'styleemailphp@gmail.com',
		    'smtp_pass' => 'Style123*',
		    'mailtype'  => 'html',
		    'charset'   => 'iso-8859-1'
		);
		 
		// Load CodeIgniter Email library
		$this->load->library('email', $emailConfig);
		 
		// Sometimes you have to set the new line character for better result
		$this->email->set_newline("\r\n");
		// Set email preferences
		$this->email->from($from['email'], $from['name']);
		$this->email->to($to);
		 
		$this->email->subject($subject);
		$this->email->message($message);
		// Ready to send email and check whether the email was successfully sent
		 
		if (!$this->email->send()) {
		    // Raise error message
		    //show_error($this->email->print_debugger());
		}
		else {
		    // Show success notification or other things here
		    echo 'Success to send email';
		}

    }
    function discount_calculation($discount_amount, $price){    	
		if($price != '' && $price > 0 && $discount_amount != '' && $discount_amount > 0){
		$discount_persentage = ($price-$discount_amount)/$price*100;
		return round($discount_persentage);
		}else{
			return '';
		}
    }
}

?>