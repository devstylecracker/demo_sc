<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
class Mobile_look_android extends TT_REST_Controller 
{
  function __construct()
	{
		parent::__construct();
		$this->load->model('Schome_model');
		$this->load->model('Mobile_model_android');
		$this->load->model('Pa_model');
		$this->load->model('Mobile_pa_model');
		$this->load->model('Mobile_look_model');
		$this->load->model('Allproducts_model');
		$this->load->model('Product_desc_model');
		$this->load->model('Sale_model');
		$this->load->model('Megasale_model');
		$this->load->model('home_model');
		$this->load->model('Mcart_model_android');
		$this->open_methods = array('single_look_post','looks_by_stylist_post','textbox_search_post','looks_by_brand_post','discover_post');    
		$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
	}
	 public function my_feed_post(){
		 
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		$limit = $this->post('limit');
		$offset = $this->post('offset');
        $filter = $this->post('filter');
		$is_up = $this->post('is_up');
		$bucket_id = $this->post('user_bucket_id');	
		$timestamp = $this->post('timestamp');	
		$version = $this->post('version');
		if($version == ''){ $version = 0; }
		$version =2;		
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);		
		$filter_value = '';
		
		if(trim($bucket_id) == ''){
			$this->failed_response(1015, "Please enter the bucket_id");
		}else if($version < 1){
			$this->failed_response(1010, "Please update your app");
		}
		
		if($timestamp == ''){ $timestamp = 0; }
		if($is_up == ''){ $is_up = 1; }
		if($filter == 'hot-right-now'){ $filter_value = '3'; }
		else if($filter == 'looks'){ $filter_value = '1'; }
		else if($filter == 'street-style'){ $filter_value = '2'; }
		else if($filter == 'sc-celeb-style'){ $filter_value = '4'; }
        else if($filter == 'sc-live-posts'){ $filter_value = '5'; }
		else if($filter == 'festive'){ $filter_value = '6'; }
		else if($filter == 'bridal'){ $filter_value = '7'; }
		
		$data = $this->Mobile_model_android->get_my_feed($user_id,$auth_token,$limit,$offset,$filter_value,$bucket_id,$timestamp,$is_up);
		 
		if($data !=NULL){
			$this->success_response($data);
		}else{
			$res = array();
			$this->success_response($data);
		}
	 }
	 
	  public function view_all_looks_get(){
		 
        $filter = $this->get('filter');
		$is_up = $this->get('is_up');
		$timestamp = $this->get('timestamp');
		$look_for = $this->get('look_for');
		  if($look_for == 'men') $look_for = 2;else $look_for = 1;
		$limit = $this->get('limit');
		$offset = $this->get('offset');		  
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);
		
		$filter_value = '1';
		if($timestamp == ''){ $timestamp = 0; }
		if($is_up == ''){ $is_up = 1; }
		if($filter == 'hot-right-now'){ $filter_value = '3'; }
		else if($filter == 'looks'){ $filter_value = '1'; }
		else if($filter == 'street-style'){ $filter_value = '2'; }
		else if($filter == 'sc-celeb-style'){ $filter_value = '4'; }
        else if($filter == 'sc-live-posts'){ $filter_value = '5'; }
		else if($filter == 'festive'){ $filter_value = '6'; }
		else if($filter == 'bridal'){ $filter_value = '7'; }
		else if($filter == 'sale'){ $filter_value = '8'; }
		$data = $this->Mobile_model_android->view_all_looks($limit,$offset,$filter_value,$timestamp,$is_up,$look_for);
		
		 if($data !=NULL){
	      	 $this->success_response($data);
		    }else{
				$res = array();
	      	   $this->success_response($data);
		    }
	 }
	 
	 public function looks_by_brand_get(){
		  
	      $brand_id = $this->get('brand_id');
		  $limit = $this->get('limit');
		  $offset = $this->get('offset');
		  $limit = $this->checkLimit($limit);
		  $offset = $this->checkOffset($offset);		  
		  $data = $this->Mobile_model_android->get_looks_by_brand($brand_id,$limit,$offset);

		  if($data != NULL){
			$this->success_response($data);
		}
		else	
			//$this->failed_response(1001, "No Record(s) Found");
			$data = array();	
			$this->success_response($data);
	  }
	
	public function discover_get(){
		  
		  //echo ini_get("memory_limit")."\n";
          ini_set("memory_limit","-1");
		//echo ini_get("memory_limit")."\n";
			
	      $filter = $this->get('filter');
		  $timestamp = $this->get('timestamp');
		  $limit = $this->get('limit');
		  $offset = $this->get('offset');
		  $limit = $this->checkLimit($limit);
		  $offset = $this->checkOffset($offset);
		  $is_up = $this->get('is_up');
		  if($is_up == '') $is_up = 1;
		  $look_for = $this->get('look_for');
		  if($look_for == '') $look_for = 1;
		  
		  if($timestamp == ''){ $timestamp = 0; }
		  if($filter == 'hot-right-now'){ $filter_value = '3'; }
		  else if($filter == 'looks'){ $filter_value = '1'; }
		  else if($filter == 'street-style'){ $filter_value = '2'; }
		  else if($filter == 'sc-celeb-style'){ $filter_value = '4'; }
          else if($filter == 'sc-live-posts'){ $filter_value = '5'; }
		  else if($filter == 'festive'){ $filter_value = '6'; }
		  else if($filter == 'bridal'){ $filter_value = '7'; }
		  else if($filter == ''){ $filter_value = '0'; }
		  $data = $this->Mobile_model_android->get_discover($filter_value,$timestamp,$limit,$offset,$is_up,$look_for);
		  
		  if($data != NULL || $data != ''){
			  
			$this->success_response($data);
		}
		else
			$data = array();	
			$this->success_response($data);
			//$this->failed_response(1001, "No Record(s) Found");
	
	  }
	  

	  
	  public function share_post(){
	    $user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
	    $look_id = $this->post('look_id');
        $action = $this->post('medium');
		$data = $this->Mobile_model_android->get_share($user_id,$auth_token,$look_id,$action);
		if($data === 1){
		$data = new stdClass();
		$data->message = 'success';
		$this->success_response($data);
		}else{
		$this->failed_response(1001, "No Record(s) Found");
		}		
	  
	  }
	 public function single_look_get(){
	  	$user_id = $this->get('user_id'); // added by Hari for Product redirection
		$single_look_id = $this->get('look_id');
		$data['look'] = (object)$this->Mobile_model_android->single_look_id_mobile($single_look_id);
		$data['products'] = $this->Mobile_model_android->single_look_mobile($single_look_id);
		
		$dataresponse = (object)$data;
		
		if(($dataresponse != NULL)) { 
			$this->success_response($dataresponse);
		}
		else{ 
			$this->failed_response(1001, "No Record(s) Found");
		}
	 }
	 
	public function sing_look_get($look_id,$user_id)
	{
		if($look_id){
			$user_info = $this->Mobile_model_android->single_look_mobile($look_id);
			/*added for adding parameter in redirection start*/
			/*
			$count_products = count($user_info);
			for($i=0;$i<$count_products;$i++){
				$encode_product_id = $this->encryptOrDecryptMobile($user_info[$i]->product_id,'encrypt');
				if($user_info[$i]->redirect_sep != '' && $user_info[$i]->brand_id == '20424'){ //closet37
					$user_info[$i]->buy_now_url = $user_info[$i]->buy_now_url.$user_info[$i]->redirect_sep.'product_id='.$encode_product_id .'&u_id=' .$user_id.'&tracking=56274b42acf75&referer=true';
				}
				
				else if($user_info[$i]->redirect_sep != '' && $user_info[$i]->brand_id == '20936'){ //style fiesta
					$user_info[$i]->buy_now_url = $user_info[$i]->buy_now_url.$user_info[$i]->redirect_sep.'product_id='.$encode_product_id .'&u_id=' .$user_id.'&tracking=563c7d3dd40ce&referer=true';
				}   
				else if($user_info[$i]->redirect_sep != '' && $user_info[$i]->brand_id == '20933'){ 
					$user_info[$i]->buy_now_url = $user_info[$i]->buy_now_url.$user_info[$i]->redirect_sep.'product_id='.$encode_product_id.'&u_id='.$user_id;
				}
				else if($user_info[$i]->redirect_sep != '' && $user_info[$i]->brand_id == '22183'){ //The Muslin Bag this is code brand_id of 22183
					$user_info[$i]->buy_now_url = $user_info[$i]->buy_now_url.$user_info[$i]->redirect_sep.'scproductid='.$encode_product_id.'&u_id='.$user_id;
				}
				else if($user_info[$i]->redirect_sep != ''){ 
					$user_info[$i]->buy_now_url = $user_info[$i]->buy_now_url.$user_info[$i]->redirect_sep.'sc_productid='.$encode_product_id.'&u_id='.$user_id;
				}
				else{ 
					$user_info[$i]->buy_now_url = $user_info[$i]->buy_now_url;
				}
			}		
			/*code ends*/
			
			return $user_info;
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
		
	}
	public function looks_by_stylist_get(){
		$stylist_id = $this->get('stylist_id');
		$offset = $this->get('offset');
		$limit = $this->get('limit');
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);
		$res = $this->Mobile_model_android->looks_by_stylist_id_mobile($stylist_id,$offset,$limit);
		if($res !=NULL){
	      	 $this->success_response($res);
		    }else{
				$res = array();
			 //$res = new stdClass();
			// $res['message'] = 'success';
	      	   $this->success_response($res);
		       //$this->failed_response(1001, "No Record(s) Found");
		    }
			
			/*if($res == NULL){
			
		    }else{
				$res->message = 'success';
				//$res = array();
				$this->success_response($res);
			}*/
	}
	public function user_like_post(){
		$user_id = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$look_id = $this->post('look_id');		
		$res = $this->Mobile_model_android->user_like_mobile($user_id,$auth_token,$look_id);
		if($res === 1){
			
			$data = new stdClass();
			$data->message = 'success';
			$this->success_response($data);
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
	}
	public function textbox_search_get(){
		$user_search = urldecode($this->get('user_search'));
		//$user_search = explode(' ',$user_search);
		$tag = urldecode($this->get('user_tag'));
		//echo $tag;exit;
		//$tags = urldecode($this->post('tag'));
		if($tag != NULL)
		{
			// $user_search = explode(',',$tag);
			$user_search = $tag;
		}
		
		// echo "<pre>";
		// print_r($user_search);
		// exit;
		$limit = $this->get('limit');
		$offset = $this->get('offset');		
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);
		$res = $this->Mobile_model_android->user_search_mobile($user_search,$limit,$offset);
		if($res !=NULL){
	      	//   $data = new stdClass();
		      // $data = (object)$res;
		      $this->success_response($res);
		    }else{
		       //$this->failed_response(1001, "No Record(s) Found");
			   $res = array();
			   $this->success_response($res);
		    }
	}
	
	public function multi_image_get(){
		$product_id = $this->get('product_id');
		$res = $this->Mobile_model_android->multi_image($product_id);
		if($res !=NULL){
			 $this->success_response($res);
		    }else{
			   $res = array();
			   $this->success_response($res);
			}
	}
	
	function checkLimit($limit){
		if($limit == '') $query_limit = ''; else $query_limit = " LIMIT $limit ";
		return $query_limit;
	}
	function checkOffset($offset){
		if($offset == '') $query_offset = ''; else $query_offset = " OFFSET $offset ";
		return $query_offset;
	}
	
	public function push_notification_get(){
		//print_r($_SERVER);exit;
		$myfile = @fopen("timestamp.txt", "r") or die("Unable to open file!");
		$timestamp = fread($myfile,filesize("timestamp.txt"));
		$res = $this->Mobile_model_android->push_look($timestamp);
		$timestamp = $res->timestamp;
		$file = fopen("timestamp.txt","w");
		$write =  fwrite($file,"$timestamp");
		if($res !=NULL){
	      	$ch = curl_init("https://www.stylecracker.com/silentpn.php");
			curl_exec($ch);
		    }else{		       
			   $res = array();
			   $this->success_response($res);
		    }
	 
	  }
	  
	  /*Added by Hari for product redirect encryption*/
	function encryptOrDecryptMobile($mprhase, $crypt) {
	       $MASTERKEY = "STYLECRACKERAPI";
	       $td = mcrypt_module_open('tripledes', '', 'ecb', '');
	       $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
	       mcrypt_generic_init($td, $MASTERKEY, $iv);
	       if ($crypt == 'encrypt')
	       {
	           $return_value = base64_encode(mcrypt_generic($td, $mprhase));
	       }
	       else
	       {
	           $return_value = mdecrypt_generic($td, base64_decode($mprhase));
	       }
	       mcrypt_generic_deinit($td);
	       mcrypt_module_close($td);
	       return $return_value;
	}
	  
	function get_look_name_get(){
		$res = $this->Mobile_model_android->get_look_name();
		if(!empty($res)){
			$this->success_response($res);
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
	}

	function scb_product_post(){	
	$user_id = $this->post('user_id');
	$auth_token = $this->post('auth_token');
	$result = $this->Mobile_model_android->check_scb_product($user_id);
	$show_cod = array();$i=0;
	$id = base64_encode($user_id);
	if(BOROUGH_PRODUCTS!=''){            
	$scb_products = BOROUGH_PRODUCTS;     
	}else        {   
	$scb_products = '';    
    }	  
	$show_cod = array();
	$i=0;
	$count = count($result);
	foreach($result as $row){
		$product_id = $row['product_id'];
		$product_id = $product_id;
		$findme = "'$product_id'";
		$pos = strpos($scb_products, $findme);
		if ($pos === false) {
		$show_cod[$i] = "0";		
		} else {
		$show_cod[$i]= "1";
		}
		$i++;
	}
	// print_r($show_cod);exit;
	if (in_array("0", $show_cod,TRUE) || $count == 0)	  {
		$scb_cod = '0';
		}else $scb_cod = '1';
		$data['scb_product'] = '2';
		// $data['link'] = 'https://www.stylecracker.com/scborough?id='.$id;
		$data['link'] = 'https://www.stylecracker.com/Scshop';
		// $data['link'] = '';
		$data['show_icon'] = 'false';
		if(!empty($data)){
			$this->success_response($data);
		}else{
			$this->failed_response(1001, "No Record(s) Found");
			}
	}

	function top_looks_get(){
		$filter_selection = $this->get('gender');
		$version = $this->get('version');
		if($version == ''){ $version = 0; }
		$version = 2;
		if($version < 1){
			$this->failed_response(1010, "Please update your app");
		}
		
		$res['slider_data'] = $this->Mobile_model_android->get_all_sliders();
		
		if($filter_selection == 'men'){
			$data = $this->Mobile_model_android->get_options('top_looks_men');
			$res['top_categories'] = $this->Mobile_model_android->get_home_categories($filter_selection);
			$res['top_brands'] = $this->Mobile_model_android->get_home_brands($filter_selection);
			
		}else if($filter_selection == 'women'){
			$data = $this->Mobile_model_android->get_options('top_looks_women');
			$res['top_categories'] = $this->Mobile_model_android->get_home_categories($filter_selection);
			$res['top_brands'] = $this->Mobile_model_android->get_home_brands($filter_selection);
			
		}else {
			$data = $this->Mobile_model_android->get_options('top_looks_all');
			$res['top_categories'] = $this->Mobile_model_android->get_home_categories($filter_selection);
			$res['top_brands'] = $this->Mobile_model_android->get_home_brands($filter_selection);
		}
		
		$i=0;
		foreach($res['top_categories'] as $value){
		$category_img_url = '';
		if($value['type'] == 1) { 
                $category_img_url_array = @scandir('./assets/images/products_category/category/'.$value['cat_id'],true);
                $header_img = get_headers($this->config->item('products_category_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg");
                if(!empty($category_img_url_array)){
                  $category_img_url = $this->config->item('products_category_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg";
                }
                $cat_type = 'cat';
              }else if($value['type'] == 2) { 
                if($value['product_cat_id'] == WOMEN_CATEGORY_ID ){ $page_filter_name = 'women'; }else{ $page_filter_name ='men'; }
                $category_img_url_array = @scandir('./assets/images/products_category/sub_category/'.$value['cat_id'],true);
                $header_img = get_headers($this->config->item('products_sub_category_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg");
                if(!empty($category_img_url_array)){

                  $category_img_url = $this->config->item('products_sub_category_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg";
                }
                $cat_type = 'subcategory';
              }else if($value['type'] == 3) { 
			  if($value['product_cat_id'] == WOMEN_CATEGORY_ID ){ $page_filter_name = 'women'; }else{ $page_filter_name ='men'; }
                $category_img_url_array = @scandir('./assets/images/products_category/variation_types/'.$value['cat_id'],true);
                $header_img = get_headers($this->config->item('products_variation_types_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg");
                if(!empty($category_img_url_array)){
               
                  $category_img_url = $this->config->item('products_variation_types_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg";
                }
				$cat_type = 'variation';
              }else if($value['type'] == 4) { 
               if($value['product_cat_id'] == WOMEN_CATEGORY_ID ){ $page_filter_name = 'women'; }else{ $page_filter_name ='men'; }
                $category_img_url_array = @scandir('./assets/images/products_category/variation_values/'.$value['cat_id'],true);
                if(!empty($category_img_url_array)){
                $header_img = get_headers($this->config->item('products_variation_values_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg");
              
                  $category_img_url = $this->config->item('products_variation_values_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg";
                }
                $cat_type = 'variations';
              }
			  
			$res['top_categories'][$i]['url'] = $category_img_url;
			$res['top_categories'][$i]['cat_type'] = $cat_type;
			$res['top_categories'][$i]['share_url'] = base_url().'all-products/'.$filter_selection.'/'.$cat_type.'/'.$res['top_categories'][$i]['slug'];
			$i++;
		}	  
		$look_array = array(); $i = 0;
		foreach($data as $val){
			$look_array[$i] = $this->Mobile_model_android->looks_by_look_id($val);
			$i++;
		}
		
		$res['top_looks']= $look_array;
		if(!empty($res)){
			$this->success_response($res);
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
	}
	
	function view_all_category_get(){
      $data = array();

      $filter_type = $this->get('filter_type');//men or women
	  
      if($filter_type == ''){ $filter_selection = 'all'; }
      else if($filter_type == 'men'){ $filter_selection = 'men'; }
      else if($filter_type == 'women'){ $filter_selection = 'women'; }
      $data['page_filter_name'] = $filter_type;
		
      if ( $this->cache->get('category_filter_type') !== $filter_type ) {
          $data['top_categories'] = $this->Mobile_model_android->category_list($filter_type);
          $this->cache->save('category_filter_data', $data, 10);
          $this->cache->save('category_filter_type', $filter_type, 10);
       }
      if($this->cache->get('category_filter_data')){
        $data = $this->cache->get('category_filter_data');
      }
	 
	  $data['top_categories'] = $this->Mobile_model_android->category_list($filter_type);
     $this->success_response($data); 
	  
    }
	
	function view_all_brand_get(){
      $data = array();
      $filter_type = $this->get('filter_type');//men or women
      if($filter_type == ''){ $filter_selection = 'all'; }
      else if($filter_type == 'men'){ $filter_selection = 'men'; }
      else if($filter_type == 'women'){ $filter_selection = 'women'; }
		
      $data['page_filter_name'] = $filter_type;

      if ( $this->cache->get('brand_filter_type') !== $filter_type ) {
        $data['top_brands'] = $this->Mobile_model_android->brand_list($filter_type);
        $this->cache->save('brand_filter_data', $data, 500);
        $this->cache->save('brand_filter_type', $filter_type, 500);
      }
      if($this->cache->get('brand_filter_data')){
        $data = $this->cache->get('brand_filter_data');
      }
	  // $data['top_brands'] = $this->Mobile_model_android->brand_list($filter_type);
	   
	  $this->success_response($data); 
    }
	
	function single_category_get(){
		$data = array();
		$look_type = $this->get('page_filter_name');
		$category_slug = $this->get('slug');
		$category_level = $this->get('cat_type');
		$product_cat_id = $this->get('product_cat_id');
		$limit = $this->get('limit');
		$offset = $this->get('offset');
		if($product_cat_id == WOMEN_CATEGORY_ID ){ $look_type = 'women'; }else{ $look_type ='men'; }
		// echo $look_type;exit;
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);		
		$data['looks_data'] = $this->Mobile_model_android->get_looks_via_cat($look_type,$category_slug,$category_level);
		$data['products_data'] = $this->Mobile_model_android->get_products_via_cat($limit,$offset,$look_type,$category_slug,$category_level);
		$data['products_total'] = $this->Mobile_model_android->get_products_via_cat_total($look_type,$category_slug,$category_level);
		$this->success_response($data);
	}
	
	function favorites_post(){
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		$fav_for = $this->post('fav_for');
		$fav_id = $this->post('fav_id');
		$res = $this->Mobile_model_android->user_favorites($user_id,$fav_for,$fav_id);
		if($res === 1){
			$data = new stdClass();
			$data->message = 'success';
			$this->success_response($data);
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
		
	}
	
	function single_product_get(){
		$product_id = $this->get('product_id');
		$user_id = $this->get('user_id');
		
		// added for product click tracking 
		$type = $this->get('type');
		$source = '';
		if($type == 'ios'){ $source = '7'; }else $source = '5';
		$product_click = $this->Mcart_model_android->add_product_click($user_id,$product_id,$source);
		// added for product click tracking 
		
		$res['product_data'] = $this->Mobile_look_model->single_product_data($product_id,$user_id);
		$product_related = 0;
		$res['related_product'] = (array)$this->Mobile_look_model->get_product_related_products($product_id,$product_related,$user_id);
		if(!empty($res)){
			$this->success_response($res);
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
	}
	
	function view_all_category_product_get(){
		$data = array();
		$look_type = $this->get('page_filter_name');
		$category_slug = $this->get('slug');
		$category_level = $this->get('cat_type');	
		$limit = $this->get('limit');
		$offset = $this->get('offset');
		$type = $this->get('type');
		$limit2 = $this->checkLimit($limit);
		$offset2 = $this->checkOffset($offset);	
		if($type == 'look'){
		$data['looks_data'] = $this->Mobile_model_android->get_looks_via_cat($look_type,$category_slug,$category_level,$limit,$offset);
		}else if($type == 'product'){
		$data['products_data'] = $this->Mobile_model_android->get_products_via_cat($limit2,$offset2,$look_type,$category_slug,$category_level);
		}
		$this->success_response($data);
	}
	
	function filter_products_get(){

   		$brandIds = $this->get('brandIds');//brand ids 
   		$colorIds = $this->get('colorIds');//color ids 
   		$sc_offers = $this->get('sc_offers');
   		$price_filter = $this->get('price_filter');//price ids 
   		$min_price = $this->get('min_price2');//min price 
   		$max_price = $this->get('max_price');//max price 
   		$discount = $this->get('discount');//1=discounted products 2= non discount product
   		$look_type = $this->get('gender'); 
		$category_slug = $this->get('sccategory_level');//sccategory_level = variation 
		$category_level= $this->get('sccategory_slug'); //category_level = skirts 
		$product_sort_by = $this->get('product_sort_by');  
		$limit = $this->get('limit');
		$offset = $this->get('offset');
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);
		
		$search = $this->get('search');
		if($search != ''){
            $search_text = strip_tags($_GET['search']);

            /* check if it's brand or not*/
            if($search_text!=''){ 
               if($this->Allproducts_model->check_brand($search_text)) {
                  $brandIds =   $this->Allproducts_model->check_brand($search_text);
               }else if(!empty($this->Allproducts_model->check_look($search_text))) { 
                  $looks_product_array =$this->Allproducts_model->check_look($search_text);
               }else if(!empty($this->Allproducts_model->check_product($search_text))) { 
                  $looks_product_array =$this->Allproducts_model->check_product($search_text);
               }
            }

         }
		 
   		$data['total_products'] = $this->Mobile_look_model->filter_products_total($offset,$brandIds,$colorIds,$sc_offers,$price_filter,$min_price,$max_price,$discount,$look_type,$category_slug,$category_level,$product_sort_by);
		$data['products_data'] = $this->Mobile_look_model->filter_products_new($offset,$limit,$brandIds,$colorIds,$sc_offers,$price_filter,$min_price,$max_price,$discount,$look_type,$category_slug,$category_level,$product_sort_by,@$looks_product_array['products']);
   		// $data['products_data'] = $this->Mobile_look_model->filter_products($brandIds,$colorIds,$sc_offers,$price_filter,$min_price,$max_price,$discount,$look_type,$category_slug,$category_level,$product_sort_by,$limit,$offset);
        if(!empty($data)){
			$this->success_response($data);
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
       
   	}
	
	function filter_list_get(){
		$gender = $this->get('gender'); //either gender or brand will be come on this place
   		$category_type = $this->get('category_type'); // either subcategory or variation or variations
   		$category_slug = $this->get('category_slug'); // slug name of subcategory or variation or variations
		$limit = $this->get('limit');
		$offset = $this->get('offset');
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);
		$brandIds = 0;$looks_product_array = array();
		$subcategory_slug ='';
		if($gender == 'women'){
   			$gender = WOMEN_CATEGORY_ID;
   			$looks_for = '1';
   		}else if($gender == 'men'){
   			$gender = MEN_CATEGORY_ID;
   			$looks_for = '2';
   		}
		
		$search = $this->get('search');
		if($search !=''){
			$search_text = strip_tags($search);

            /* check if it's brand or not*/
            if($search_text!=''){ 
               if($this->Allproducts_model->check_brand($search_text)) {
                  $brandIds =   $this->Allproducts_model->check_brand($search_text);
               }else if(!empty($this->Allproducts_model->check_look($search_text))) { 
                  $looks_product_array =$this->Allproducts_model->check_look($search_text);
               }else if(!empty($this->Allproducts_model->check_product($search_text))) { 
                  $looks_product_array =$this->Allproducts_model->check_product($search_text);
               }
            }
		}
		
		$data['gender'] =array('men','women');
		
		//category and variation list 
		if($category_type == 'subcategory'){
   			$subcategory_id = @$this->Mobile_look_model->get_sub_category_list($gender,$category_slug)[0]['id'];
   			$subcategory_slug = $category_slug ;
   			$data['variation'] = $this->Mobile_look_model->get_variation_list(NULL,NULL,$subcategory_id);
   		}else if($category_type == 'variation' || $category_type == 'variations_type'){
			$category_type = 'variation';
   			$variation_id = @$this->Mobile_look_model->get_variation_list(NULL,$category_slug)[0]['id'];
   			$data['variation'] = $this->Mobile_look_model->get_variation_list(NULL,NULL,NULL,$variation_id);
   			$subcategory_slug = $this->Mobile_look_model->get_subCat_by_variation($variation_id)[0]['slug'];
   			$data['variations'] = $this->Mobile_look_model->get_variations_list($variation_id);
   		}else if($category_type == 'variations'){
   			$data['variations'] = $this->Mobile_look_model->get_variations_list('',$category_slug);
   			$data['variation'] = $this->Mobile_look_model->get_variation_list(NULL,NULL,NULL,$data['variations'][0]['product_variation_type_id']);
   			$subcategory_slug = @$this->Mobile_look_model->get_subCat_by_variation($data['variation'][0]['id'])[0]['slug'];
   		}
		//brand_list 
		$data['paid_brands'] = $this->Mobile_look_model->get_paid_brands_new($gender,$category_slug,$category_type,$brandIds,@$looks_product_array['products']);
		$data['sub_category'] = $this->Allproducts_model->get_sub_category_list($gender,$subcategory_slug);
		//color list
		// $data['color'] = unserialize(FILTER_COLOR);
		$data['color'] = unserialize(FILTER_COLOR_MOBILE);
		$data['price_list'] = array('Below 500','500 - 999','1000 - 1999','2000 - 2999','Above 2999');
		$data['Discounts'] = array('Discount','Non-Discount');
			$megamenu_keys = array_keys($data);
		$data['keys'] = $megamenu_keys;
		
		  if(!empty($data)){
			$this->success_response($data);
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}

	}
	
	function megamenu_list_get(){
		$gender = $this->get('gender');
		if($gender == 'men'){
		$megamenu_list = $this->Mobile_look_model->get_megamenu_list('mega_menu_men_app');
		$data['list'] = unserialize(@$megamenu_list[0]['option_value']);
		}else {
		$megamenu_list = $this->Mobile_look_model->get_megamenu_list('mega_menu_women_app');
		$data['list'] = unserialize(@$megamenu_list[0]['option_value']);		
		}
		// print_r($data['list']);exit;
		$count=count($data); 
		$key_array = array();
		// echo key($data).$count;exit;
		$i=0;
		foreach($data['list'] as $value)
		{
		$key_array[$i] = $value['label'];
		$i++;
		}
		$mm_menu = array();
		if($gender=='men')
		{    
		$mm_image = base_url().'assets/images/menu/megamenu_img_men.jpeg'; 
		}else if($gender=='women')
		{   
		$mm_image = base_url().'assets/images/menu/megamenu_img_women.jpeg';
		}
		// print_r(array_keys($data['list']));exit;
		$megamenu_keys = array_keys($data['list']);
		$data['list'] = $data['list'];
		$data['keys'] = (object)$megamenu_keys;
		$data['image'] = $mm_image;
		if(!empty($data)){
			$this->success_response($data);
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
	}
	
	function add_look_review_post(){
		
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		$post_review = $this->post('review');
		$ratings = $this->post('ratings');
		$look_id = $this->post('look_id');    

		if($post_review!='' && $ratings!='' && $look_id!=''){
			$res = (object)$this->Mobile_look_model->post_review($post_review,$ratings,$look_id,$user_id);
		if(!empty($res)){
			$this->success_response($res);
		}else{
			$this->failed_response(1001, "please enter review and ratings");
		}

		}
	}
	
	function get_look_review_get(){
		
		$look_id = $this->get('look_id');
		$limit = $this->get('limit');
		$offset = $this->get('offset');
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);
		$data = $this->Mobile_look_model->get_look_review($look_id,$limit,$offset);
		if(!empty($data)){
			$this->success_response($data);
		}else{
			$data = array();
			$this->success_response($data);
		}
	}
	
	function get_product_wishlist_post(){
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		$bucket_id = $this->post('bucket_id');
		$fev_for = 'product';
		$limit = $this->post('limit');
		$offset = $this->post('offset');
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);
		$data['products'] = $this->Mobile_look_model->show_my_wishlist_products($user_id,$fev_for,$limit,$offset);
		// print_r($data['products']);exit;
		$data['message'] = '';
		if(!empty($data['products'])){
			$this->success_response($data);
		}else{
			$data['products'] = $this->Mobile_look_model->show_recommended_products($bucket_id);
			$data['message'] = "You haven't added any products to your wishlist, here are three we think you might like";
			$this->success_response($data);
		}
	}
	
	function universal_search_get(){
		$universal_search = $this->get('universal_search');
		$limit = $this->post('limit');
		$offset = $this->post('offset');
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);
		if($universal_search!=''){
		$search_text = strip_tags($universal_search);
		$brandIds = '';
		$looks_product_array = '';
		/* check if it's brand or not*/
		if($search_text!=''){ 
		   if($this->Allproducts_model->check_brand($search_text)) {
			  $brandIds =   $this->Allproducts_model->check_brand($search_text);
		   }else if(!empty($this->Allproducts_model->check_look($search_text))) { 
			  $looks_product_array =$this->Allproducts_model->check_look($search_text);
		   }else if(!empty($this->Allproducts_model->check_product($search_text))) { 
			  $looks_product_array =$this->Allproducts_model->check_product($search_text);
		   }
		}
		}
		$data['products'] = $this->Mobile_look_model->universal_search($brandIds,$looks_product_array['products'],$limit,$offset);
		
		if(!empty($data['products'])){
			$this->success_response($data);
		}else{
			$data['products'] = array();
			$this->success_response($data);
		} 
	}
	
	function new_filter_list_get(){
   		$data = array();
   		$gender_or_brand = 'all'; $subcategory_id =''; $subcategory_slug =''; $looks_for = ''; $offset = 1; $show_pagination = 1; $brandIds = 0; $looks_product_array = array(); $search_text=''; $pagination_url = '';
   		// $contoller_name = $this->uri->segment(1); //all-products
   		$gender_or_brand = $this->get('gender'); //either gender or brand will be come on this place
        // $gender_or_brand_ = $this->get('gender'); //either gender or brand will be come on this place
   	 	$category_type = $this->get('category_type'); // either subcategory or variation or variations
   		$category_slug = $this->get('category_slug'); // slug name of subcategory or variation or variations
		$search = $this->get('search');
		$limit = $this->get('limit');
		$offset = $this->get('offset');
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);
		$product_sort_by = $this->get('product_sort_by');
		$brand = $this->get('brand');
		
		// $product_cat_id = $this->get('product_cat_id');
		// if($product_cat_id == WOMEN_CATEGORY_ID ){ $gender_or_brand = 'women'; }else{ $gender_or_brand ='men'; }
		
   		if($gender_or_brand == 'women'){
   			$gender_or_brand = WOMEN_CATEGORY_ID;
   			$looks_for = '1';
   		}else if($gender_or_brand == 'men'){
   			$gender_or_brand = MEN_CATEGORY_ID;
   			$looks_for = '2';
   		}else if($gender_or_brand == 'brand'){
   			$brands_slug = $category_type;
   		}else if($gender_or_brand == 'page'){
   			$offset = $category_type;
   			$show_pagination = 1;
   			$gender_or_brand ='';
   		}
		// echo $gender_or_brand;exit;
		$data['gender'] =array('men','women');
		
		if($category_type == 'variations_type'){ $category_type = 'variation'; }
		
		if($category_type == 'subcategory' && $gender_or_brand != 'brand'){
   			$subcategory_id = @$this->Mobile_look_model->get_sub_category_list($gender_or_brand,$category_slug)[0]['id'];
   			$subcategory_slug = $category_slug ;
   			$data['variation'] = $this->Mobile_look_model->get_variation_list(NULL,NULL,$subcategory_id);
   		}else if($category_type == 'variation' || $category_type == 'variations_type' && $gender_or_brand != 'brand'){
   			$variation_id = @$this->Mobile_look_model->get_variation_list(NULL,$category_slug)[0]['id'];
   			$data['variation'] = $this->Mobile_look_model->get_variation_list(NULL,NULL,NULL,$variation_id);
   			$subcategory_slug = @$this->Mobile_look_model->get_subCat_by_variation($variation_id)[0]['slug'];
   			$data['variations'] = $this->Mobile_look_model->get_variations_list($variation_id);
   		}else if($category_type == 'variations' && $gender_or_brand != 'brand'){
   			$data['variations'] = $this->Mobile_look_model->get_variations_list('',$category_slug);
   			$data['variation'] = @$this->Mobile_look_model->get_variation_list(NULL,NULL,NULL,$data['variations'][0]['product_variation_type_id']);
   			$subcategory_slug = @$this->Mobile_look_model->get_subCat_by_variation($data['variation'][0]['id'])[0]['slug'];
   		}
         if(isset($brand)){
            $brandIds = $this->get_brand_id($brand);
         }

         if($search != ''){
            $search_text = strip_tags($_GET['search']);

            /* check if it's brand or not*/
            if($search_text!=''){ 
               if($this->Allproducts_model->check_brand($search_text)) {
                  $brandIds =   $this->Allproducts_model->check_brand($search_text);
               }else if(!empty($this->Allproducts_model->check_look($search_text))) { 
                  $looks_product_array =$this->Allproducts_model->check_look($search_text);
               }else if(!empty($this->Allproducts_model->check_product($search_text))) { 
                  $looks_product_array =$this->Allproducts_model->check_product($search_text);
               }
            }

         }
$data['paid_brands'] = $this->Mobile_look_model->get_paid_brands_new($gender_or_brand,$category_slug,$category_type,$brandIds,@$looks_product_array['products']);
         // $data['search_text'] = $search_text;
         // $data['brandIds'] = $brandIds;
   		$data['looks_data'] = $this->Mobile_model_android->get_looks_via_cat($looks_for,$category_slug,$category_type);
   		// $data['paid_brands'] = $this->Mobile_look_model->get_paid_brands_new($gender_or_brand,$category_slug,$category_type,$brandIds,@$looks_product_array['products']);
   		$data['sub_category'] = $this->Allproducts_model->get_sub_category_list($gender_or_brand,$subcategory_slug);
   		// $data['coupon_offer'] = $this->home_model->coupon_offer();
		$data['coupon_offer'] = array();
   		$data['color'] = unserialize(FILTER_COLOR_MOBILE);
         $data['products_total'] =$this->Allproducts_model->filter_products_total($offset,$brandIds,'','','','','','',$gender_or_brand,$category_type,$category_slug,'',@$looks_product_array['products']);
         
         $data['products_data'] = $this->Mobile_look_model->filter_products_new($offset,$limit,$brandIds,'','','','','','',$gender_or_brand,$category_type,$category_slug,$product_sort_by,@$looks_product_array['products']);
		 $data['price_list'] = array('Below 500','500 - 999','1000 - 1999','2000 - 2999','Above 2999');
		$data['Discounts'] = array('Discount','Non-Discount');
		 $megamenu_keys = array_keys($data);
		$data['keys'] = $megamenu_keys;
		if(!empty($data)){
			$this->success_response($data); 
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
   	}
	
	function get_brand_id($slug){ 
      	return $this->Allproducts_model->get_brand_id($slug); 
    }
	
	function test_get(){ 
      	echo "comming";exit;
    }
	
	function new_sale_get(){
		
		$data = array();
		$category = '';
		$brands = $this->get('brands');
		$sort_by = $this->get('sort_by');
		$limit = $this->get('limit');
		$offset = $this->get('offset');
		$products = $this->Mobile_look_model->getSaleProducts($offset,$category,$brands,$sort_by,$limit);
		// $product_ids = $this->Mobile_look_model->getSaleProducts(0,$category,$brands,$sort_by,0);
		$data['all_data']['show_icon'] = '0';
		$data['all_data']['title'] = 'Sale';
		$data['all_data']['link'] = 'https://www.stylecracker.com/assets/images/app_icons/sale_icon.png';
		$data['all_data']['paid_brands'] = $this->Megasale_model->get_paid_brand_list($products['products_ids']);
		// print_r($products);exit;
		$product_array = array();
		$i=0;
		foreach($products['products'] as $row){
			$product_array[$i]=$this->Mobile_look_model->single_product_data($row['id']);	
			$i++;
		}
		$data['all_data']['products'] = $product_array;
		
		$j=0;
		foreach($data['all_data']['paid_brands'] as $val){
			$data['all_data']['paid_brands'][$j]['brand_id'] = $val['user_id'];
			$j++;
		}
		
		if($data !=NULL){
			$this->success_response($data);
		}else{
			$res = array();
			$this->success_response($data);
		}
		
	}
	
	function get_look_by_slug_get(){
		$slug = $this->get('slug');
		$look_id = $this->Mobile_look_model->get_look_id_by_slug($slug);
		
		if($look_id > 0){
			$data['look'] = (object)$this->Mobile_model_android->single_look_id_mobile($look_id);
			$data['products'] = $this->sing_look_get($look_id,$user_id=0);	
			$dataresponse = (object)$data;
		}
		
		if(!empty($dataresponse)){ 
			$this->success_response($dataresponse);
		}else{ 
			$this->failed_response(1001, "No Record(s) Found");
		}
	}
	
	function get_all_cat_get(){
		$gender = $this->get('gender');
		$limit = $this->get('limit');
		$offset = $this->get('offset');
		$data['limit_cond'] = " LIMIT ".$offset.",".$limit;
		if($gender == 1){ $gender = 'women'; }else { $gender = 'men'; }
		$categories = $this->home_model->category_list($gender,$data);
		if(!empty($categories)){ 
			$this->success_response($categories);
		}else{ 
			$this->failed_response(1001, "No Record(s) Found");
		}
	}
	 
}
?>