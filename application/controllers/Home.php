<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
class Home extends MY_Controller {

	function __construct(){
       parent::__construct();
       $this->load->model('home_model');
       $this->load->model('Product_desc_model');
       $this->load->driver('cache');
       $this->load->model('Allproducts_model');
       $this->load->model('Scborough_model');
       
   	}

   	public function index(){

   	  $offset = $this->uri->segment(3)!='' && $this->uri->segment(3)>0 ? $this->uri->segment(3) : 1;
   	  $filter_type = strtolower(trim($this->uri->segment(2)));
   	  $pagination = '';
      $adjacents = 3;
      $prev = 0;
      $next = 1;  

      if($filter_type == '' || $filter_type == 'all'){ $filter_selection = 'all'; }
      else if($filter_type == 'men'){ $filter_selection = 'men'; }
      else if($filter_type == 'women'){ $filter_selection = 'women'; }
	  
		if($filter_type== 'all' || $filter_type== ''){
			$this->seo_title =  "Personalized Looks by Bollywood Stylists - StyleCracker.com";
		$this->seo_desc = '';
		$this->seo_keyword = '';
		} 
     if($this->uri->segment(3) > 0){
        $this->cache->file->delete('look_'.$filter_selection);
      }
		if ( $this->cache->file->get('look_'.$filter_selection) == FALSE ) {
      $total = $this->home_model->get_look_count($filter_type);
      $data['looks'] = $this->home_model->get_more_looks($filter_type,$offset,'0');
      $data['looks_total'] = $total;
	    $perpage = 12;

        $lastpage = ceil($total/$perpage);
        $lpm1 = $lastpage - 1;


        if($lastpage > 1)
        { 
          $pagination .= "<div class='sc-pagination-wrp'><ul class=\"pagination sc-pagination\">";
          //previous button

          if ($offset != 1) 
            $pagination.= "<li><a href=".base_url()."looks/all/".'1'.">First</a></li>";

          if ($offset > 1) {
            @$prev = $offset-1;
            $pagination.= "<li><a href=".base_url()."looks/all/".$prev.">previous</a></li>";
            $this->seo_prev = base_url()."looks/all/".$prev;
          }
          
          
          $limit = $offset + 5;
          if($offset != $lastpage && $offset< $lastpage-5  ){
          for ($counter =  $offset; $counter <= $limit; $counter++)
            {
              if ($counter == $offset)
              {
                $pagination.= "<li><span class=\"current\">$counter</span></li>";
                $this->seo_canonical = base_url()."looks/all/".$counter;
              }
              else
                $pagination.= "<li><a href=".base_url()."looks/all/".$counter.">$counter</a></li>";          
            }
          }else{ 
            for ($counter = $lastpage-5; $counter <= $lastpage; $counter++)
            {
              if ($counter == $offset){
                $pagination.= "<li><span class=\"current\">$counter</span></li>";
                $this->seo_canonical = base_url()."looks/all/".$counter;
              }
              else
                $pagination.= "<li><a href=".base_url()."looks/all/".$counter.">$counter</a></li>";
            }

          }
          
          //next button
          if ($offset < $counter - 1) {
            @$next = $offset+1;
            $pagination.= "<li><a href=".base_url()."looks/all/".$next.">next </a></li>";
             $this->seo_next = base_url()."looks/all/".$next;
          }


          if ($offset != $lastpage) 
            $pagination.= "<li><a href=".base_url()."looks/all/".$lastpage.">Last </a></li>";

          $pagination.= "</ul></div>\n";    
      }
      $data['pagination'] =  $pagination;
      if($this->uri->segment(3) == ''){
        $this->cache->file->save('look_'.$filter_selection, $data,500000000000000000);
      }
    }else{
      //echo 'cache'.$filter_selection;
      $data = $this->cache->file->get('look_'.$filter_selection);
    }

   	  $this->load->view('common/header_view');
      $this->load->view('looks_view',$data);
      $this->load->view('common/footer_view');

   	}

   	function get_more_looks(){

   	  $filter_type = $this->input->post('look_type');
   	  $offset = $this->input->post('offset');
      $look_filter_type = $this->input->post('look_filter_type');

   	  if($filter_type == ''){ $filter_selection = 'all'; }
      else if($filter_type == 'men'){ $filter_selection = 'men'; }
      else if($filter_type == 'women'){ $filter_selection = 'women'; }

      
      $data['looks'] = $this->home_model->get_more_looks($filter_type,$offset,$look_filter_type);
      $this->load->view('more_looks',$data);

   	}

      function category_wise_looks(){
      $this->seo_title =  "Kavalai Vendam Looks by Bollywood Stylists - Stylecracker.com";
      $this->seo_desc = '';
      $this->seo_keyword = '';
	  
	  /* added for auto login */
		$user_id = $this->input->get('id');
		if($user_id >0) $array_field=array('is_app' => 1);
		$user_id = base64_decode($user_id);
		$get_user_info = $this->Scborough_model->get_user_info($user_id);
		if(!empty($get_user_info)){
			$res = $this->login($get_user_info[0]['email'],$get_user_info[0]['password']);
		}
	  /* added for auto login */
	  
      $data = array();
      $data['total_look'] = count($this->home_model->get_category_wise_looks(-1,0));
      $data['looks'] = $this->home_model->get_category_wise_looks(0,0);
      //$data['products'] = $this->home_model->get_store_specific_products('41302');
      $this->load->view('common/header_view');
      $this->load->view('promo/category_wise_looks',$data);
      $this->load->view('common/footer_view');
    }

    function get_all_category_wise_looks(){
      $data = array();
      $offset = $this->input->post('offset');
      $price_flt = $this->input->post('price_flt');
      $data['looks'] = $this->home_model->get_category_wise_looks($offset,$price_flt);
      $this->load->view('more_looks',$data);
    }

      function festive_sale(){
      $data = array();
      $this->seo_title =  "Wedding Sale on StyleCracker";
      $this->seo_desc = '';
      $this->seo_keyword = '';
      $data['brands'] = $this->home_model->get_brands_list();
      $data['categories'] = $this->home_model->get_brands_related_category();
      $this->load->view('common/header_view');
      $this->load->view('promo/festive_sale',$data);
      $this->load->view('common/footer_view');
    }

    function festive_products_data(){
      $data = array();
      $this->seo_title =  "Wedding Sale on StyleCracker";
      $this->seo_desc = '';
      $this->seo_keyword = '';
      $cat_id = $this->uri->segment(3);
      $cat_id = @end(@explode('-',$cat_id));
      $data['all_data'] = $this->home_model->get_products_data($cat_id,0,0,0);
      $data['left_filter'] = $this->load->view('promo/brand_filter', $data, true);
      $data['cat_id'] = $cat_id;

      $this->load->view('common/header_view');
      $this->load->view('promo/brand_products',$data);
      $this->load->view('common/footer_view');
      
    }

    function more_products_data(){
      $data = array();
      $group_no = $this->input->post('group_no');
      $brands = $this->input->post('brandId')!='' ? $this->input->post('brandId') : 0;
      $cat_id = $this->input->post('cat_id');
      $product_sort_by_sale = $this->input->post('product_sort_by_sale');
      $data['all_data'] = $this->home_model->get_products_data($cat_id,$brands,$group_no,$product_sort_by_sale);
      $this->load->view('promo/more_sale',$data);
    }
	
	/* added by rajesh for party pop-up*/
	
	function party_sale(){
		
		$data = array();
		$this->seo_title =  "Party Sale on StyleCracker";
		$this->seo_desc = '';
		$this->seo_keyword = '';
		
		/* added for auto login */
		$user_id = $this->input->get('id');
		$user_id = (int)base64_decode($user_id);
		if($user_id >0) { 
			$array_field = array('is_app' => 1);
			$this->session->set_userdata($array_field);
		} 
		$get_user_info = $this->Scborough_model->get_user_info($user_id);
		if(!empty($get_user_info)){
			$res = $this->login($get_user_info[0]['email'],$get_user_info[0]['password']);
		}
		/* added for auto login */
		
		$data['brands'] = $this->home_model->get_party_brands_list();
		$data['categories'] = $this->home_model->get_party_brands_related_category();

		$this->load->view('common/header_view');
		$this->load->view('promo/party_sale',$data);
		$this->load->view('common/footer_view');
		
	}
	
	function party_products_data(){
		
		$data = array();
		$this->seo_title =  "Party Sale on StyleCracker";
		$this->seo_desc = '';
		$this->seo_keyword = '';
		$cat_id = $this->uri->segment(3);
		$cat_id = @end(@explode('-',$cat_id));
		$data['all_data'] = $this->home_model->get_party_products_data($cat_id,0,0,0);
		$data['left_filter'] = $this->load->view('promo/brand_filter', $data, true);
		$data['cat_id'] = $cat_id;    

		$this->load->view('common/header_view');
		$this->load->view('promo/party_products',$data);
		$this->load->view('common/footer_view');

	}
	
	function more_party_products_data(){
      $data = array();
      $group_no = $this->input->post('group_no');
      $brands = $this->input->post('brandId')!='' ? $this->input->post('brandId') : 0;
      $cat_id = $this->input->post('cat_id');
      $product_sort_by_sale = $this->input->post('product_sort_by_sale');
      $data['all_data'] = $this->home_model->get_party_products_data($cat_id,$brands,$group_no,$product_sort_by_sale);
      $this->load->view('promo/more_sale',$data);
    }
	
	public function login($logEmailid,$logPassword){

         if(trim($logEmailid) == ''){
            echo "Please enter your email id";

          }else if(trim($logPassword) == ''){
            echo "Please enter the Password";

          }else{
           $result = $this->Scborough_model->login_user($logEmailid,$logPassword);


          }
	}
	
	/* added by rajesh for new year pop-up*/
	
	function newyear_sale(){
		
		$data = array();
		$this->seo_title =  "All Things New on StyleCracker";
		$this->seo_desc = '';
		$this->seo_keyword = '';
		
		/* added for auto login */
		$user_id = $this->input->get('id');
		if($user_id >0) $array_field=array('is_app' => 1);
		$user_id = base64_decode($user_id);
		$get_user_info = $this->Scborough_model->get_user_info($user_id);
		if(!empty($get_user_info)){
			$res = $this->login($get_user_info[0]['email'],$get_user_info[0]['password']);
		}
		/* added for auto login */
		
		$data['brands'] = $this->home_model->get_newyear_brands_list();
		// $data['categories'] = $this->home_model->get_newyear_brands_related_category(); 
		$women_cat = array('52','47','49','48','50','53','70','320','7','55','13','62'); 
		$men_cat = array('20','65','30','18','31','66','9','27','12','62');
		$data['women_categories'] = $this->home_model->get_newyear_brands_related_category($women_cat);
		$data['men_categories'] = $this->home_model->get_newyear_brands_related_category($men_cat);
		// if($this->session->userdata('is_app') == '')
		$this->load->view('common/header_view',false);
		$this->load->view('promo/newyear_sale',$data);
		$this->load->view('common/footer_view');
		
	}
	
	function newyear_products_data(){
		
		$data = array();
		$this->seo_title =  "All Things New Sale on StyleCracker";
		$this->seo_desc = '';
		$this->seo_keyword = '';
		$cat_id = $this->uri->segment(3);
		$cat_id = @end(@explode('-',$cat_id));
		$data['all_data'] = $this->home_model->get_newyear_products_data($cat_id,0,0,0);
		$data['left_filter'] = $this->load->view('promo/brand_filter', $data, true);
		$data['cat_id'] = $cat_id;

		$this->load->view('common/header_view');
		$this->load->view('promo/newyear_products',$data);
		$this->load->view('common/footer_view');

	}
	
	function more_newyear_products_data(){
      $data = array();
      $group_no = $this->input->post('group_no');
      $brands = $this->input->post('brandId')!='' ? $this->input->post('brandId') : 0;
      $cat_id = $this->input->post('cat_id');
      $product_sort_by_sale = $this->input->post('product_sort_by_sale');
      $data['all_data'] = $this->home_model->get_newyear_products_data($cat_id,$brands,$group_no,$product_sort_by_sale);
      $this->load->view('promo/more_sale',$data);
    }

  /* added by sudha for All-things-black pop-up*/
  
  function popup_sale(){
    
    $data = array();
    $this->seo_title =  "All Things Black on StyleCracker";
    $this->seo_desc = '';
    $this->seo_keyword = '';
    
     // added for auto login 
    $user_id = $this->input->get('id');
    $user_id = (int)base64_decode($user_id);
    if($user_id >0) { 
      $array_field = array('is_app' => 1);
      $this->session->set_userdata($array_field);
    } 
    $get_user_info = $this->Scborough_model->get_user_info($user_id);
    if(!empty($get_user_info)){
      $res = $this->login($get_user_info[0]['email'],$get_user_info[0]['password']);
    }
     // added for auto login 
    
    $data['brands'] = $this->home_model->get_popup_brands_list();
    $data['categories'] = $this->home_model->get_popup_brands_related_category();

    $this->load->view('common/header_view');
    $this->load->view('promo/popup_sale',$data);
    $this->load->view('common/footer_view');
    
  }
  
  function popup_products_data(){
    
    $data = array();
    $this->seo_title =  "All Things Black on StyleCracker";
    $this->seo_desc = '';
    $this->seo_keyword = '';
    $cat_id = $this->uri->segment(3);
    $cat_id = @end(@explode('-',$cat_id));
    $data['all_data'] = $this->home_model->get_popup_products_data($cat_id,0,0,0);
    $data['left_filter'] = $this->load->view('promo/brand_filter', $data, true);
    $data['cat_id'] = $cat_id;    

    $this->load->view('common/header_view');
    $this->load->view('promo/popup_products',$data);
    $this->load->view('common/footer_view');

  }
  
  function more_popup_products_data(){
      $data = array();
      $group_no = $this->input->post('group_no');
      $brands = $this->input->post('brandId')!='' ? $this->input->post('brandId') : 0;
      $cat_id = $this->input->post('cat_id');
      $product_sort_by_sale = $this->input->post('product_sort_by_sale');
      $data['all_data'] = $this->home_model->get_popup_products_data($cat_id,$brands,$group_no,$product_sort_by_sale);
      $this->load->view('promo/more_sale',$data);
    }

/* End of All-things-black pop-up*/

/* Added by Sudha for valentine-pop-up*/
  function valentine_sale(){
    
    $data = array();
    $this->seo_title =  "Valentine’s popup on StyleCracker";
    $this->seo_desc = '';
    $this->seo_keyword = '';
    
    /* added for auto login */
    $user_id = $this->input->get('id');
    if($user_id >0) $array_field=array('is_app' => 1);
    $user_id = base64_decode($user_id);
    $get_user_info = $this->Scborough_model->get_user_info($user_id);
    if(!empty($get_user_info)){
      $res = $this->login($get_user_info[0]['email'],$get_user_info[0]['password']);
    }
    /* added for auto login */    
    $brand_array = @unserialize(VALENTINE_SALE);
    $data['brands'] = $this->home_model->getPopup_brands_list($brand_array);
    // $data['categories'] = $this->home_model->get_newyear_brands_related_category(); 
    $women_cat = array('52','47','48','49','146','50','320','55','382','7','68'); 
    //,'62'
    $men_cat = array('65','20','66','37','9','12');
    $data['women_categories'] = $this->home_model->getPopup_brands_related_category($women_cat,$brand_array);
    $data['men_categories'] = $this->home_model->getPopup_brands_related_category($men_cat,$brand_array);
    // if($this->session->userdata('is_app') == '')
    $this->load->view('common/header_view',false);
    $this->load->view('promo/valentine_sale',$data);
    $this->load->view('common/footer_view');
    
  }
  
  function valentine_products_data(){
    
    $data = array();
    $this->seo_title =  "Valentine’s popup on StyleCracker";
    $this->seo_desc = '';
    $this->seo_keyword = '';
    $cat_id = $this->uri->segment(3);
    $cat_id = @end(@explode('-',$cat_id));
	$brand_array = @unserialize(VALENTINE_SALE);
    $data['all_data'] = $this->home_model->getPopup_products_data($cat_id,0,0,0,$brand_array);
    $data['left_filter'] = $this->load->view('promo/brand_filter', $data, true);
    $data['cat_id'] = $cat_id;

    $this->load->view('common/header_view');
    $this->load->view('promo/valentine_products',$data);
    $this->load->view('common/footer_view');

  }
  
  function more_valentine_products_data(){
      $data = array();
      $group_no = $this->input->post('group_no');
      $brands = $this->input->post('brandId')!='' ? $this->input->post('brandId') : 0;
      $cat_id = $this->input->post('cat_id');
      $product_sort_by_sale = $this->input->post('product_sort_by_sale');
	  $brand_array = @unserialize(VALENTINE_SALE);
      $data['all_data'] = $this->home_model->getPopup_products_data($cat_id,$brands,$group_no,$product_sort_by_sale,$brand_array);
      $this->load->view('promo/more_sale',$data);
    }

/* End of valentine-pop-up*/

/* Added by Rajesh for summer-pop-up*/
  function summer_sale(){
    
    $data = array();
    $this->seo_title =  "Summer Ready popup on StyleCracker";
    $this->seo_desc = '';
    $this->seo_keyword = '';
    
    /* added for auto login */
    $user_id = $this->input->get('id');
    if($user_id >0) $array_field=array('is_app' => 1);
    $user_id = base64_decode($user_id);
    $get_user_info = $this->Scborough_model->get_user_info($user_id);
    if(!empty($get_user_info)){
      $res = $this->login($get_user_info[0]['email'],$get_user_info[0]['password']);
    }
    /* added for auto login */    
    $brand_array = @unserialize(SUMMER_SALE);
    $data['brands'] = $this->home_model->getPopup_brands_list($brand_array);
    // $data['categories'] = $this->home_model->get_newyear_brands_related_category(); 
    $women_cat = array('47','52','49','50','48','53','320','55','7');
    //,'62'
	$men_cat = array('65','20','30','27','9');
    $data['women_categories'] = $this->home_model->getPopup_brands_related_category($women_cat,$brand_array);
    $data['men_categories'] = $this->home_model->getPopup_brands_related_category($men_cat,$brand_array);
	// echo "<pre>";print_r($data['men_categories']);exit;
    // if($this->session->userdata('is_app') == '')
    $this->load->view('common/header_view',false);
    $this->load->view('promo/summer_sale',$data);
    $this->load->view('common/footer_view');
    
  }
  
  function summer_products_data(){
    
    $data = array();
    $this->seo_title =  "Summer Ready popup on StyleCracker";
    $this->seo_desc = '';
    $this->seo_keyword = '';
    $cat_id = $this->uri->segment(3);
    $cat_id = @end(@explode('-',$cat_id));
	$brand_array = @unserialize(SUMMER_SALE);
    $data['all_data'] = $this->home_model->getPopup_products_data($cat_id,0,0,0,$brand_array);
    $data['left_filter'] = $this->load->view('promo/brand_filter', $data, true);
    $data['cat_id'] = $cat_id;

    $this->load->view('common/header_view');
    $this->load->view('promo/summer_products',$data);
    $this->load->view('common/footer_view');

  }
  
  function more_summer_products_data(){
      $data = array();
      $group_no = $this->input->post('group_no');
      $brands = $this->input->post('brandId')!='' ? $this->input->post('brandId') : 0;
      $cat_id = $this->input->post('cat_id');
      $product_sort_by_sale = $this->input->post('product_sort_by_sale');
	  $brand_array = @unserialize(SUMMER_SALE);
      $data['all_data'] = $this->home_model->getPopup_products_data($cat_id,$brands,$group_no,$product_sort_by_sale,$brand_array);
      $this->load->view('promo/more_sale',$data);
    }

/* End of summer-pop-up*/

/* Added by Rajesh for travel-pop-up*/
  function travel_sale(){
    
    $data = array();
    $this->seo_title =  "Travel Shop on StyleCracker";
    $this->seo_desc = '';
    $this->seo_keyword = '';
    
    /* added for auto login */
    $user_id = $this->input->get('id');
    if($user_id >0) $array_field=array('is_app' => 1);
    $user_id = base64_decode($user_id);
    $get_user_info = $this->Scborough_model->get_user_info($user_id);
    if(!empty($get_user_info)){
      $res = $this->login($get_user_info[0]['email'],$get_user_info[0]['password']);
    }
    /* added for auto login */    
    $brand_array = @unserialize(TRAVEL_SALE);
    $data['brands'] = $this->home_model->getPopup_brands_list($brand_array);
    // $data['categories'] = $this->home_model->get_newyear_brands_related_category(); 
    $women_cat = array('47','48','52','49','50','64','70','320','55','7','13','62','326');
    //,'62'
	$men_cat = array('65','32','30');
    $data['women_categories'] = $this->home_model->getPopup_brands_related_category($women_cat,$brand_array);
    $data['men_categories'] = $this->home_model->getPopup_brands_related_category($men_cat,$brand_array);
	// echo "<pre>";print_r($data['men_categories']);exit;
    // if($this->session->userdata('is_app') == '')
    $this->load->view('common/header_view',false);
    $this->load->view('promo/travel_sale',$data);
    $this->load->view('common/footer_view');
    
  }
  
  function travel_products_data(){
    
    $data = array();
    $this->seo_title =  "Travel Shop on StyleCracker";
    $this->seo_desc = '';
    $this->seo_keyword = '';
    $cat_id = $this->uri->segment(3);
    $cat_id = @end(@explode('-',$cat_id));
	$brand_array = @unserialize(TRAVEL_SALE);
    $data['all_data'] = $this->home_model->getPopup_products_data($cat_id,0,0,0,$brand_array);
    $data['left_filter'] = $this->load->view('promo/brand_filter', $data, true);
    $data['cat_id'] = $cat_id;

    $this->load->view('common/header_view');
    $this->load->view('promo/travel_products',$data);
    $this->load->view('common/footer_view');

  }
  
  function more_travel_products_data(){
      $data = array();
      $group_no = $this->input->post('group_no');
      $brands = $this->input->post('brandId')!='' ? $this->input->post('brandId') : 0;
      $cat_id = $this->input->post('cat_id');
      $product_sort_by_sale = $this->input->post('product_sort_by_sale');
	  $brand_array = @unserialize(TRAVEL_SALE);
      $data['all_data'] = $this->home_model->getPopup_products_data($cat_id,$brands,$group_no,$product_sort_by_sale,$brand_array);
      $this->load->view('promo/more_sale',$data);
    }

/* End of travel-pop-up*/

}