<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// require APPPATH.'libraries/IntercomClient.php';
class Yes_bank extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->model('User_info_model');
		$this->load->model('Scborough_model');
	}


// all static pages

public function yesbank_home(){

	/* added for auto login */
	$user_id = $this->input->get('id');     
	$user_id = base64_decode($user_id);
	$get_user_info = $this->Scborough_model->get_user_info($user_id);
	if(!empty($get_user_info)){
		$res = $this->login($get_user_info[0]['email'],$get_user_info[0]['password']);
	}
	/* added for auto login */
	$this->load->view('seventeen/common/header_view');
	$this->load->view('seventeen/microsites/yesbank/home');
	$this->load->view('seventeen/common/footer_view');
}

public function yesbank_scbox(){
	$this->load->view('seventeen/common/header_view');
	$this->load->view('seventeen/microsites/yesbank/scbox');
	$this->load->view('seventeen/common/footer_view');
}

public function yesbank_personal_stylists(){
		$this->load->view('seventeen/common/header_view');
	$this->load->view('seventeen/microsites/yesbank/personal_stylists');
		$this->load->view('seventeen/common/footer_view');
}

public function terms_and_conditions(){
		$this->load->view('seventeen/common/header_view');
	$this->load->view('seventeen/microsites/yesbank/terms_and_conditions');
		$this->load->view('seventeen/common/footer_view');
}

//

	public function login($logEmailid,$logPassword){

		if(trim($logEmailid) == ''){
			echo "Please enter your email id";

		}else if(trim($logPassword) == ''){
			echo "Please enter the Password";

		}else{
			$result = $this->Scborough_model->login_user($logEmailid,$logPassword,$medium='website',$type='');
		}
    }

}
