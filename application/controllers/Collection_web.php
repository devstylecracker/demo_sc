<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
class Collection_web extends MY_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('Pa_model');
		$this->load->model('Product_desc_model');
		$this->load->model('Collection_model');
		$this->load->model('Event_model');
		$this->load->model('Mobile_look_model');
		$this->load->model('Favorites_model');
		$this->load->model('Category_filter_model');
	}

	public function index(){
		
		$user_data['user_id'] = $this->session->userdata('user_id');
		$user_data['gender'] = $this->session->userdata('gender');
		$user_data['limit'] = 12;
		$user_data['offset'] = 0;
		if($user_data['user_id'] > 0){
			$user_data['body_shape'] = @$this->Pa_model->get_users_answer_obj($user_data['user_id'],'user_body_shape')[0]['id'];
			$user_data['pa_pref1'] = @$this->Pa_model->get_users_answer_obj($user_data['user_id'],'user_style_p1')[0]['id'];
		}else {
			$user_data['body_shape'] = 0;
			$user_data['pa_pref1'] = 0;
		}
		
		$user_data['type'] = '';
		$user_data['limit_cond'] = " LIMIT 0,12";
		$data['top_collection'] = $this->Collection_model->get_all_collection($user_data);
		$user_data['limit_cond'] = " ";
		$data['total_collection'] = count($this->Collection_model->get_all_collection($user_data));
		$data['user_id'] = $this->session->userdata('user_id');
		
		$this->seo_title = 'Personalized Collections by Bollywood Stylists - StyleCracker.com';
		$this->seo_desc = '';
		
		$this->load->view('seventeen/common/header_view');
		$this->load->view('seventeen/allcollection_view',$data);
		$this->load->view('seventeen/common/footer_view');
	}
	
	function get_more_collection(){
		$user_id = 0;
		// echo "<pre>";print_r($_SESSION);exit;
		
		$user_data['user_id'] = $this->session->userdata('user_id');
		$user_data['gender'] = $this->session->userdata('gender');
		$user_data['offset'] = $this->input->post('offset')*12;
		if($user_data['user_id'] > 0){
			$user_data['body_shape'] = @$this->Pa_model->get_users_answer_obj($user_data['user_id'],'user_body_shape')[0]['id'];
			$user_data['pa_pref1'] = @$this->Pa_model->get_users_answer_obj($user_data['user_id'],'user_style_p1')[0]['id'];
		}else {
			$user_data['body_shape'] = 0;
			$user_data['pa_pref1'] = 0;
		}
		$user_data['type'] = '';
		$user_data['limit_cond'] = " LIMIT ".$user_data['offset'].",12 ";
		$object_data['top_collection'] = $this->Collection_model->get_all_collection($user_data);
		if($user_data['user_id'] < 0){ $user_id=0; }else{ $user_id = $user_data['user_id']; }
		$object_data['user_id'] = $user_id;
		$this->load->view('seventeen/more_collection',$object_data);
		/*$collection_html = '';
			if(!empty($top_collection)){ foreach($top_collection as $val){
				if(@$val["is_wishlisted"] == "1") $active = "active"; else $active = "";
				$collection_html = $collection_html.'<div class="col-md-6 col-sm-12 col-xs-12 grid-col">
					  <div class="item-wrp overlay">
						<div class="item">
						  <div class="item-img-wrp">
							<img src="'.$val["collection_image"].'" alt="">
						  </div>
						  <div class="item-desc-wrp">
							<div class="inner">
							<h2 class="title">'.$val["collection_name"].'</h2>
							<div class="desc">'.$val["collection_short_desc"].'</div>
							<div class="btns-wrp">
							  <a href="'.base_url().'collections/'.@$val['object_slug'].'-'.$val["collection_id"].'" class="scbtn scbtn-secondary white"><span>View Collection</span></a>
							</div>
						  
						  </div>
							</div>
							<span class="ic-heart '.$active.' " id="wishlist_c'.$val["collection_id"].'" onclick="add_to_fav(\'collection\','.$val["collection_id"].','.$user_id.');" >
							  <i class="icon-heart"></i>
							</span>
						</div>
					  </div>
					</div>';
				}//foreach
			}//if	
		echo $collection_html;*/
	}
	/*
	function single_collection(){
		
		$object_id = $this->uri->segment(count($this->uri->segment_array()));
		
		if(strrpos($object_id,'-')) $object_id = substr($object_id,strrpos($object_id,'-',-1)+1);
		
		if(isset($object_id) && $object_id && is_numeric($object_id) && $object_id >0){
			
			$data['object_id'] = $object_id;
			$data['user_id'] = $this->session->userdata('user_id');
			$data['gender'] = $this->session->userdata('gender');
			if($data['user_id'] > 0){
				$data['body_shape'] = @$this->Pa_model->get_users_answer_obj($data['user_id'],'user_body_shape')[0]['id'];
				$data['pa_pref1'] = @$this->Pa_model->get_users_answer_obj($data['user_id'],'user_style_p1')[0]['id'];
			}else {
				$data['body_shape'] = 0;
				$data['pa_pref1'] = 0;
			}
			$data['type'] = 'single';
			$data['offset'] = '0';
			$data['limit'] = '12';
			$data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
			$data['product_data'] = $this->Collection_model->get_single_collection($data['object_id'],$data);
			$data['limit'] = '0';
			// $data['total_products'] = count($data['product_data']['collection_products_array']);
			$data['limit_cond'] = " LIMIT 0,4";
			$data['recommended_collection'] = $this->Collection_model->get_all_collection($data);
			if($data['gender'] == 1){
				$gender_cat_id = 1;
			}else if($data['gender'] == 2){
				$gender_cat_id = 2;
			}
			// added for total 
			$count_data = $this->Collection_model->filter_collection_products($data['user_id'],$object_id,@$lastlevelcat,@$brandId,@$priceId,@$sort_by,@$attributesId,@$sizeId,$limit=12,$offset=0);
			$data['total_products'] = $count_data['total_products'];
			// if($_GET['test'] == 1){
				// echo "<pre>";echo $data['total_products'];exit;
			// }
			// added for filter
			$product_ids = implode(',',$data['product_data']['collection_products_array']);
			$data['filter']['gender'] = array('0'=>'Women','1'=>'Men');
			$data['filter']['category'] = $this->Category_filter_model->get_all_product_category($product_ids); //category list
			$data['filter']['sort_by'] = array('0'=>'latest','1'=>'Price high to low','2'=>'Price low to high'); // sort by
			$data['filter']['paid_brands'] = $this->Category_filter_model->get_paid_brand_list($product_ids); //brand list
			$data['filter']['price_list'] = array('0'=>'Below 999','1'=>'1000 - 1999','2'=>'2000 - 3499','3'=>'3500 - 4999','4'=>'5000 +'); //price list 
			$data['filter']['size'] =  $this->Category_filter_model->get_product_size($product_ids);
			$data['filter']['global_attributes'] = $this->Category_filter_model->global_attributes();
			
			if(!empty($data['filter']['global_attributes'])){
				foreach($data['filter']['global_attributes'] as $val){
					$i=0;
					foreach($this->Category_filter_model->attributes_values($val['id']) as $values){
						$data['filter'][$val['attribute_slug']][$i]['id'] = $values['id'];
						$data['filter'][$val['attribute_slug']][$i]['attr_value_name'] = $values['attr_value_name'];
						@$data['filter'][$val['attribute_slug']][$i]['attr_hashtag'] = $values['attr_hashtag'];
						$i++;
					}
				}
			}
			
			// added for filter
			
			echo "<pre>";print_r($data);exit;
			$this->seo_title = ' '.$data['product_data']['collection_name'].' - Buy Online Now on StyleCracker';
			$this->seo_desc = '';
			$this->load->view('seventeen/common/header_view');
			$this->load->view('seventeen/collection_product',$data);
			$this->load->view('seventeen/common/footer_view');
			
		}else{
			
			$this->load->view('common/header_view');
			$this->load->view('errors/error_404');
			$this->load->view('common/footer_view');

		}
	}*/
	
	function single_collection(){
		
		$object_id = $this->uri->segment(count($this->uri->segment_array()));
		
		if(strrpos($object_id,'-')) $object_id = substr($object_id,strrpos($object_id,'-',-1)+1);
		
		if(isset($object_id) && $object_id && is_numeric($object_id) && $object_id >0){
			
			$user_id = $this->session->userdata('user_id');
			$gender = $this->session->userdata('gender');
			$lastlevelcat = $this->input->get('lastlevelcat');
			$brandId = $this->input->get('brandId');
			$priceId = $this->input->get('priceId');
			$sizeId = $this->input->get('sizeId');
			$attributesId = $this->input->get('attributesId');
			$sort_by = $this->input->get('sort_by');
			$limit = 20;
			$offset = 0;
			$product_id = '';
			$collection_data = $this->Collection_model->get_collection_info($object_id);
			$product_id = @implode(',',unserialize(@$collection_data['collection_products']));
			
			if($gender == '2'){
				$gender_cat_id = 3;
			}else {
				$gender_cat_id = 1;
			}
			
			$data = $this->Collection_model->filter_collection_products($user_id,$object_id,$lastlevelcat,$brandId,$priceId,$sort_by,$attributesId,$sizeId,$limit,$offset,$gender_cat_id,$source='web');
			// $data['filter']['gender'] = array('1'=>'WOMEN','3'=>'MEN');
			if(@$collection_data['gender'] == 3){
				$data['filter']['gender'] = array('1'=>'WOMEN','3'=>'MEN');
			}else if(@$collection_data['gender'] == 2){
				$data['filter']['gender'] = array('3'=>'MEN');
			}else{
				$data['filter']['gender'] = array('1'=>'WOMEN');
			}
			// $data['filter']['category'] = $this->Category_filter_model->get_all_product_category($product_id); //category list
			if($data['web_product_ids'] != ''){
				$data['filter']['category'] = $this->Category_filter_model->get_all_product_category($data['web_product_ids']); //category list
			}else{
				$data['filter']['category'] = array();
			}
			
			$data['product_data']['total_row'] = $data['total_products'];
			$data['filter']['sort_by'] = array('0'=>'latest','1'=>'Price high to low','2'=>'Price low to high'); // sort by
			$data['filter']['price_list'] = array('0'=>'Below 999','1'=>'1000 - 1999','2'=>'2000 - 3499','3'=>'3500 - 4999','4'=>'5000 +'); //price list 
			
			$data['product_data']['collection_data'] = $collection_data;
			$data['product_data']['collection_data']['collection_id'] = $object_id;
			$data['user_id'] = $user_id;
			$data['object_id'] = $object_id;
			if($data['user_id'] > 0){
				$data['body_shape'] = @$this->Pa_model->get_users_answer_obj($data['user_id'],'user_body_shape')[0]['id'];
				$data['pa_pref1'] = @$this->Pa_model->get_users_answer_obj($data['user_id'],'user_style_p1')[0]['id'];
			}else {
				$data['body_shape'] = 0;
				$data['pa_pref1'] = 0;
			}
			$data['type'] = 'single';
			$data['gender'] = $this->session->userdata('gender');
			$data['limit_cond'] = " LIMIT 0,5";
			$data['recommended_collection'] = $this->Collection_model->get_all_collection($data);
			$data['gender_cat_id'] = $gender_cat_id;
			// echo "<pre>";print_r($data);exit;
			
			$this->seo_title = ' '.@$data['product_data']['collection_data']['collection_name'].' - Buy Online Now on StyleCracker';
			$this->seo_desc = '';
			$this->load->view('seventeen/common/header_view');
			$this->load->view('seventeen/collection_product',$data);
			$this->load->view('seventeen/common/footer_view');
			
		}else{
			
			$this->load->view('common/header_view');
			$this->load->view('errors/error_404');
			$this->load->view('common/footer_view');

		}
	}
	
	function add_size_data(){
		$data['object_id'] = $this->input->post('object_id');
		$data['user_id'] = $this->session->userdata('user_id');
		$data['type'] = 'single';
		$data['offset'] = $this->input->post('offset')*12;
		$data['limit'] = '12';
		$data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
		$product_data = $this->Collection_model->get_single_collection($data['object_id'],$data);
		$product_html = '';$price_html = '';$active = '';$select_sizehtml = '';
		if($data['user_id'] > 0){ $user_id = $data['user_id']; }else { $user_id = '0'; }
		
		if(!empty($product_data['collection_products'])){ 
			foreach($product_data['collection_products'] as $val){
				if(!empty(@$val->product_size)){
					foreach($val->product_size as $value){
						$select_sizehtml = $select_sizehtml.'<span class="btn-diamond btn-size">'.$value->size_text.'</span>';	
					}
				}
				//select size html
			$product_html = $product_html.'<div id="modal-select-size'.$val->product_id.'" class="modal fade modal-select-size" role="dialog">
											<div class="modal-dialog  modal-md">
												<div class="modal-content">
											<button type="button" class="close" data-dismiss="modal"><i class="icon-close"></i></button>
										  <div class="title-2">
											Select Size
										  </div>
											<div class="product-item">
													<div class="item-sizes-wrp">
													'.$select_sizehtml.'
												   </div>
												</div>
										</div>
											</div>
										</div>';
			}
		}
		
		echo $product_html;
	}
	/*
	function filter_collection_products(){
		$object_id = $this->input->post('object_id');
		$sort_by = $this->input->post('intSortBy');
		if($sort_by == '2'){
			$data['order_by'] = 'order by p.price asc';
        }else if($sort_by == '1'){
			$data['order_by'] = 'order by p.price desc';
        }
		if($object_id >0){
			$data['object_id'] = $object_id;
			$data['user_id'] = $this->session->userdata('user_id');
			$data['gender'] = $this->session->userdata('gender');
			if($data['user_id'] > 0){
				$data['body_shape'] = @$this->Pa_model->get_users_answer_obj($data['user_id'],'user_body_shape')[0]['id'];
				$data['pa_pref1'] = @$this->Pa_model->get_users_answer_obj($data['user_id'],'user_style_p1')[0]['id'];
			}else {
				$data['body_shape'] = 0;
				$data['pa_pref1'] = 0;
			}
			$data['type'] = 'single';
			$data['offset'] = '0';
			$data['limit'] = '12';
			$data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
			$data['product_data'] = $this->Collection_model->get_single_collection($data['object_id'],$data);
			$this->load->view('seventeen/more_product',$data);
		}else{
			echo "Something went wrong please try again";
		}
		
	}*/
	
	function filter_collection_products(){
		$data = array();
		$user_id = $this->session->userdata('user_id');
		$object_id = $this->input->post('object_id');
        $lastlevelcat = $this->input->post('lastlevelcat');
        $brandId = $this->input->post('brandId');
        $priceId = $this->input->post('priceId');
        $sizeId = $this->input->post('sizeId');
        $attributesId = $this->input->post('attributesId');
        $sort_by = $this->input->post('sort_by');
		$offset = $this->input->post('offset');
		$gender_cat_id = $this->input->post('gender_cat_id');
		$limit = '20';
		$offset = '0';
		
		if(!empty($lastlevelcat))$lastlevelcat = implode(",",$lastlevelcat);
		if(!empty($brandId))$brandId = implode(",",$brandId);
		if(!empty($attributesId))$attributesId = implode(",",$attributesId);
		if(!empty($sizeId))$sizeId = implode(",",$sizeId);
		
		$product_id = '';
		$collection_data = $this->Collection_model->get_collection_info($object_id);
		$product_id = implode(',',unserialize($collection_data['collection_products']));
		
		$data = $this->Collection_model->filter_collection_products($user_id,$object_id,$lastlevelcat,$brandId,$priceId,$sort_by,$attributesId,$sizeId,$limit,$offset,$gender_cat_id,$source='web');
		$data['product_data']['collection_products'] = $data['products'];
		$data['product_data']['total_row'] = $data['total_products'];
		// $data['filter']['gender'] = array('1'=>'WOMEN','3'=>'MEN');
		if($collection_data['gender'] == 3){
			$data['filter']['gender'] = array('1'=>'WOMEN','3'=>'MEN');
		}else if($collection_data['gender'] == 2){
			$data['filter']['gender'] = array('3'=>'MEN');
		}else{
			$data['filter']['gender'] = array('1'=>'WOMEN');
		}
		// $data['filter']['category'] = $this->Category_filter_model->get_all_product_category($product_id);
		if($data['web_product_ids'] != ''){
			$data['filter']['category'] = $this->Category_filter_model->get_all_product_category($data['web_product_ids']); //category list
		}else{
			$data['filter']['category'] = array();
		}
		
		$data['filter']['sort_by'] = array('0'=>'latest','1'=>'Price high to low','2'=>'Price low to high'); // sort by
		$data['filter']['price_list'] = array('0'=>'Below 999','1'=>'1000 - 1999','2'=>'2000 - 3499','3'=>'3500 - 4999','4'=>'5000 +'); //price list 
		
		$data['filter']['global_attributes'] = $this->Category_filter_model->global_attributes();
		
		if(!empty($data['filter']['global_attributes'])){
			foreach($data['filter']['global_attributes'] as $val){
				$i=0;
				foreach($this->Category_filter_model->attributes_values($val['id']) as $values){
					$data['filter'][$val['attribute_slug']][$i]['id'] = $values['id'];
					$data['filter'][$val['attribute_slug']][$i]['attr_value_name'] = $values['attr_value_name'];
					@$data['filter'][$val['attribute_slug']][$i]['attr_hashtag'] = $values['attr_hashtag'];
					$i++;
				}
			}
		}
		$data['product_data']['collection_data'] = $collection_data;
		$data['user_id'] = $user_id;
		$data['selected_gender'] = $gender_cat_id;
		$data['selected_lastlevelcat'] = explode(",",$lastlevelcat);
		$data['selected_brandId'] = explode(",",$brandId);
		$data['selected_attributesId'] = explode(",",$attributesId);
		$data['selected_sizeId'] = explode(",",$sizeId);
		$data['selected_sort_by'] = $sort_by;
		$data['selected_priceId'] = $priceId;
		
		$this->load->view('seventeen/filter_collection_product',$data);
	}
	
	function get_more_products(){

        $data = array();$product_html = '';
		$user_id = $this->session->userdata('user_id');
		$object_id = $this->input->post('object_id');
        $lastlevelcat = $this->input->post('lastlevelcat');
        $brandId = $this->input->post('brandId');
        $priceId = $this->input->post('priceId');
        $sizeId = $this->input->post('sizeId');
        $attributesId = $this->input->post('attributesId');
        $sort_by = $this->input->post('sort_by');
		$offset = $this->input->post('offset');
		$gender_cat_id = $this->input->post('gender_cat_id');
		$limit = '20';
		$offset = $offset*20;

        $data = $this->Collection_model->filter_collection_products($user_id,$object_id,$lastlevelcat,$brandId,$priceId,$sort_by,$attributesId,$sizeId,$limit,$offset,$gender_cat_id,$source='web');

		// echo "<pre>";print_r($data);exit;
		$data['user_id'] = $user_id;

        $this->load->view('seventeen/more_filter_product',$data);
    }
  
}