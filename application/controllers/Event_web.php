<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event_web extends MY_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('Pa_model');
		$this->load->model('Product_desc_model');
		$this->load->model('Collection_model');
		$this->load->model('Event_model');
		$this->load->model('Mobile_look_model');
		$this->load->model('Favorites_model');
	}

	public function index(){
		$user_data['user_id'] = $this->session->userdata('user_id');
		$user_data['gender'] = $this->session->userdata('gender');
		$user_data['limit'] = 12;
		$user_data['offset'] = 0;
		if($user_data['user_id'] > 0){
			$user_data['body_shape'] = @$this->Pa_model->get_users_answer_obj($user_data['user_id'],'user_body_shape')[0]['id'];
			$user_data['pa_pref1'] = @$this->Pa_model->get_users_answer_obj($user_data['user_id'],'user_style_p1')[0]['id'];
		}else {
			$user_data['body_shape'] = 0;
			$user_data['pa_pref1'] = 0;
		}
		
		$user_data['type'] = '';
		$user_data['limit_cond'] = " LIMIT 0,12 ";
		$data['top_events'] = $this->Event_model->get_all_event($user_data);
		$user_data['limit_cond'] = " ";
		$data['total_events'] = count($this->Event_model->get_all_event($user_data));
		$data['user_id'] = $this->session->userdata('user_id');
		// echo "<pre>";print_r($data);exit;
		$this->seo_title = ' Upcoming Events For Fashion, Shopping, etc';
		$this->seo_desc = '';
		$this->load->view('seventeen/common/header_view');
		$this->load->view('seventeen/allevent_view',$data);
		$this->load->view('seventeen/common/footer_view');
	}
	
	function get_more_events(){
		$user_id = 0;
		// echo "<pre>";print_r($_SESSION);exit;
		
		$user_data['user_id'] = $this->session->userdata('user_id');
		$user_data['gender'] = $this->session->userdata('gender');
		$user_data['offset'] = $this->input->post('offset')*12;
		if($user_data['user_id'] > 0){
			$user_data['body_shape'] = @$this->Pa_model->get_users_answer_obj($user_data['user_id'],'user_body_shape')[0]['id'];
			$user_data['pa_pref1'] = @$this->Pa_model->get_users_answer_obj($user_data['user_id'],'user_style_p1')[0]['id'];
		}else {
			$user_data['body_shape'] = 0;
			$user_data['pa_pref1'] = 0;
		}
		$user_data['type'] = '';
		$user_data['limit_cond'] = " LIMIT ".$user_data['offset'].",12 ";
		$object_data['top_events'] = $this->Event_model->get_all_event($user_data);
		if($user_data['user_id'] < 0){ $user_id=0; }else{ $user_id = $user_data['user_id']; }
		$object_data['user_id'] = $user_id;
		$this->load->view('seventeen/more_event',$object_data);
		/*
		$event_html = '';
			if(!empty($top_events)){ foreach($top_events as $val){
				if(@$val["is_wishlisted"] == "1") $active = "active"; else $active = "";
				$event_html = $event_html.'<div class="col-md-6 col-sm-12 col-xs-12 grid-col">
					  <div class="item-wrp overlay">
						<div class="item">
						  <div class="item-img-wrp">
							<img src="'.$val["event_image"].'" alt="">
						  </div>
						  <div class="item-desc-wrp">
							<div class="inner">
							<h2 class="title">'.$val["event_name"].'</h2>
							<div class="desc">'.$val["event_short_desc"].'</div>
							<div class="btns-wrp">
							  <a href="'.base_url().'events/'.$val["object_slug"].'-'.$val["event_id"].'" class="scbtn scbtn-secondary white"><span>Read More</span></a>
							</div>
						  
						  </div>
							</div>
							<span class="ic-heart '.$active.' " id="wishlist_e'.$val["event_id"].'" onclick="add_to_fav(\'event\','.$val["event_id"].','.$user_id.');" >
							   <i class="icon-heart"></i>
							</span>
						</div>
					  </div>
					</div>';
				}//foreach
			}//if	
		echo $event_html;*/
	}
	
	function get_single_event(){
		$object_id = $this->uri->segment(count($this->uri->segment_array()));
		
		if(strrpos($object_id,'-')) $object_id = substr($object_id,strrpos($object_id,'-',-1)+1);
		
		if(isset($object_id) && $object_id && is_numeric($object_id) && $object_id >0){
			
			$data['object_id'] = $object_id;
			$data['user_id'] = $this->session->userdata('user_id');
			$data['type'] = 'single';
			$data['offset'] = '0';
			$data['limit'] = '12';
			$data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
			$data['product_data'] = $this->Event_model->get_single_event($data['object_id'],$data);
			$data['limit'] = '0';
			$data['total_products'] = count($this->Event_model->get_single_event($data['object_id'],$data)['event_products']);
			// echo "<pre>";print_r($data['product_data']['event_name']);exit;
			$this->seo_title = ' '.$data['product_data']['event_name'].' - Buy Online Now on StyleCracker ';
			$this->seo_desc = '';
			$this->load->view('seventeen/common/header_view');
			$this->load->view('seventeen/event_products',$data);
			$this->load->view('seventeen/common/footer_view');
			
		}else{
			
			$this->load->view('common/header_view');
			$this->load->view('errors/error_404');
			$this->load->view('common/footer_view');

		}
	}
	
	function get_event_products(){
		$data['object_id'] = $this->uri->segment(3);
		$data['user_id'] = $this->session->userdata('user_id');
		$data['type'] = 'single';
		$data['offset'] = '0';
		$data['limit'] = '12';
		$data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
		$data['product_data'] = $this->Event_model->get_single_event($data['object_id'],$data);
		$data['limit'] = '0';
		$data['total_products'] = count($this->Event_model->get_single_event($data['object_id'],$data)['event_products']);
		$this->load->view('seventeen/common/header_view');
		$this->load->view('seventeen/event_all_products',$data);
		$this->load->view('seventeen/common/footer_view');
	}
	
	function get_more_products_event(){
		$data['object_id'] = $this->input->post('object_id');
		$data['user_id'] = $this->session->userdata('user_id');
		$data['type'] = 'single';
		$data['offset'] = $this->input->post('offset')*12;
		$data['limit'] = '12';
		$data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
		$product_data = $this->Event_model->get_single_event($data['object_id'],$data);
		$product_html = '';$price_html = '';$active = '';
		if($data['user_id'] > 0){ $user_id = $data['user_id']; }else { $user_id = '0'; }
		
		if(!empty($product_data['event_products'])){ 
			foreach($product_data['event_products'] as $val){
				
				/*size html*/
				$size_ids='';$size_texts='';$size_id= array();$size_text= array();
				if(!empty($val->product_size)){
					foreach($val->product_size as $size){
						$size_id[] = $size->size_id;
						$size_text[] = $size->size_text;
					}// foreach
				}//if
				if(!empty($size_id))$size_ids = implode(',',$size_id);
				if(!empty($size_text))$size_texts = implode(',',$size_text);
				/*size html*/
				
				if($val->price > $val->discount_price){
					$price_html = '<div class="mrp"><i class="fa fa-inr"></i>'.$val->price.'</div>
						<div class="price"><i class="fa fa-inr"></i>'.$val->price.'</div>
						<div class="discount">'.$val->discount_percent.'% OFF</div>';
				}else {
					$price_html = '<div class="price"><i class="fa fa-inr"></i>'.$val->price.'</div>';
				}
				
				if(@$val->is_wishlisted == '1'){ $active = 'active'; }else { $active = '';}
				
			$product_html = $product_html.'<div class="col-md-3 col-sm-6 col-xs-6 grid-col">
					  <div class="item-wrp">
						<div class="item">
						  <div class="item-img-wrp">
							<a href="'.base_url().PRODUCT_URL.$val->product_slug.'-'.$val->product_id.'"><img src="'.$val->product_img.'" alt=""></a>
						  </div>
						  <div class="item-desc-wrp">
							<a href="'.CAT_URL.$val->product_category_slug.'-'.$val->product_category_id.'"><div class="category">'.$val->product_category.'</div></a>
							<a href="'.base_url().PRODUCT_URL.$val->product_slug.'-'.$val->product_id.'"><div class="title">'.$val->product_name.'</div></a>
							<div class="brand">By <a href="'.base_url().'brands/brand-details/'.$val->brand_slug.'-'.$val->brand_id.'">'.$val->brand_name.'</a></div>
							<div class="item-price-wrp">
								'.$price_html.'
							</div>
						  </div>
						  <div class="item-media-wrp">
						<span class="ic ic-plus" data-toggle="modal"  id="'.$val->product_id.'" value="'.$val->product_id.'" name="prod_size" data-sizes="'.$size_ids.'" data-label="'.$size_texts.'" onclick="GetProductSize(this);">
						<i class="icon-plus"></i>
						</span>
						<span class="ic ic-share" onclick="send_message(\'product\',\''.$val->product_id.'\',\''.$val->product_name.'\',\''.$val->product_img.'\',\''.base_url().PRODUCT_URL.$val->product_slug.'-'.$val->product_id.'\',\''.$user_id.'\');">
						  <i class="icon-send"></i>
						</span>
						  </div>
						<span class="ic-heart '.$active.' " id="wishlist_product'.$val->product_id.'" onclick="add_to_fav(\'product\','.$val->product_id.','.$user_id.');">
							  <i class="icon-heart"></i>
						</span>
						</div>
					  </div>
					</div>';
					
			}//foreach
		}//if
		echo $product_html;
	}
	
	function test_time(){
		echo  date("F j, Y, g:i a"); 
		echo 'date_default_timezone_set: ' . date_default_timezone_get() . '<br />';
	}
  
}