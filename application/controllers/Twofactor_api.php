<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
class Twofactor_api extends TT_REST_Controller 
{
  function __construct()
	{
		parent::__construct();		
		$this->open_methods = array('sms_post');    
	}
    

	public function sms_post(){		
		$data = array();
		$uc_mobile_no = trim($this->post('mobile_no'));
		$type = $this->post('type');
		$type__ = 'inbound_connected_caller,inbound_missed_call,outbound_connected_caller,outbound_missed_call';
		$data['mobile_no'] = '91'.$uc_mobile_no;
		$data['type'] = $type;
		if(strlen($uc_mobile_no) != 10){
			$this->failed_response(1013, "please enter 10 digit mobile number");
		}
				
		if(strlen($uc_mobile_no) == 10){ 
			/* Call SMS send API */
			if($type == 'inbound_connected_caller'){
				
				/*file_get_contents('https://2factor.in/API/R1/?module=TRANS_SMS&apikey='.API_KEY_2FACTOR.'&to='.$uc_mobile_no.'&from='.OTP_FROM.'&templatename=mobile_verification&var1='.$otp_no.'');*/
				$message = 'Thank+you+for+calling+us.+We+have+received+your+call+on+918048130591';
				file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile='.$data['mobile_no'].'&message='.$message);

			}else if($type == 'inbound_missed_call'){

				/*file_get_contents('https://2factor.in/API/R1/?module=TRANS_SMS&apikey='.API_KEY_2FACTOR.'&to='.$uc_mobile_no.'&from='.OTP_FROM.'&templatename=mobile_verification&var1='.$otp_no.'');*/
				$message = 'Thank+you+for+calling+us.+We+apologize+for+missing+your+call.+We+have+noted+your+number+and+shall+contact+you+shortly.';
				file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile='.$data['mobile_no'].'&message='.$message);


			}else if($type == 'outbound_connected_caller'){

				/*	file_get_contents('https://2factor.in/API/R1/?module=TRANS_SMS&apikey='.API_KEY_2FACTOR.'&to='.$uc_mobile_no.'&from='.OTP_FROM.'&templatename=mobile_verification&var1='.$otp_no.'');*/
				/*$message = 'Thank+you+for+calling+us.';
					file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile='.$data['mobile_no'].'&message='.$message);*/

			}else if($type == 'outbound_missed_call'){

				/*file_get_contents('https://2factor.in/API/R1/?module=TRANS_SMS&apikey='.API_KEY_2FACTOR.'&to='.$uc_mobile_no.'&from='.OTP_FROM.'&templatename=mobile_verification&var1='.$otp_no.'');*/
				/*$message = 'Thank+you+for+calling+us.+We+apologize+for+missing+your+call.+We+have+noted+your+number+and+shall+contact+you+shortly.';
				file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile='.$data['mobile_no'].'&message='.$message);*/

			}
			
			 
			/* end of code Call SMS send API */
			$data = new stdclass();
			//$data->otp_no = $otp_no;
			$this->success_response($data);
		}else{
			 $this->failed_response(1001, "Please try again");
		}
	}

	
}