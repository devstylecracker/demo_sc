<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// require APPPATH.'libraries/IntercomClient.php';
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
class Schome_new extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
		parent::__construct();
		$this->load->model('Schome_model');
		$this->load->model('home_model');
		$this->load->model('Pa_model');
		$this->load->model('Product_desc_model');
		$this->load->library('bitly');
		$this->load->library('email');
		$this->load->driver('cache');
		$this->load->library('user_agent');
		$this->load->library('curl');
		$this->load->model('Collection_model');
		$this->load->model('Event_model');
		$this->load->model('Mobile_look_model');
		$this->load->model('Favorites_model');
		$this->load->model('User_info_model');
		$this->load->model('Blog_model');
		$this->load->model('Brand_model');

  }


 public function index(){
    
      if(!$this->session->userdata('user_id')){

        if($this->input->cookie('34b07cba21adf4cd95d35c1c0bd47ff0') && $this->input->cookie('1c0253b55e82e53890985a837274811b')){
            $logEmailid = $this->encryptOrDecrypt($this->input->cookie('34b07cba21adf4cd95d35c1c0bd47ff0'),'');
            $logPassword = $this->encryptOrDecrypt($this->input->cookie('1c0253b55e82e53890985a837274811b'),'');
            $result = $this->User_info_model->login_user(trim($logEmailid),trim($logPassword),'website','');
          }
        if(!$this->session->userdata('user_id')){
              $data = array();
              $user_data = array();
              $this->seo_title = $this->lang->line('seo_title');
              $this->seo_desc = $this->lang->line('seo_desc');
              $this->seo_keyword = $this->lang->line('seo_keyword');

              if(isset($_REQUEST['utm_source'])){
                $utm_source = substr($_REQUEST['utm_source'],strpos($_REQUEST['utm_source'],"utmcsr="));
                $array_field = array('utm_source' => $utm_source,'utm_source1' => trim($utm_source));
                $this->session->set_userdata($array_field);
              }             

              // if( $this->cache->file->get('filter_type_scbox') == FALSE)
              // {
                $this->load->view('seventeen/common/header_view');
                $this->load->view('seventeen/scbox',$data);
                $this->load->view('seventeen/common/footer_view');
              //   $this->cache->file->save('filter_type_scbox', $data,500000000000000000);
              // }else
              // {                
              //    $data = $this->cache->file->get('filter_type_scbox');
              // }
        }else{
          header("Location: ".base_url());
          exit;
        }

      }else{

          $data = array();
          $this->seo_title = $this->lang->line('seo_title');
          $this->seo_desc = $this->lang->line('seo_desc');
          $this->seo_keyword = $this->lang->line('seo_keyword');
          $this->load->helper('cookie');
          if(isset($_REQUEST['utm_source'])){
             $utm_source = substr($_REQUEST['utm_source'],strpos($_REQUEST['utm_source'],"utmcsr="));
             $array_field = array('utm_source' => $utm_source,'utm_source1' => trim($utm_source));
             $this->session->set_userdata($array_field);
           }

           if(($this->uri->segment(1)) != "" && ($this->uri->segment(1)) != "all"){
            setcookie('SCGender', $this->uri->segment(1), time() + ((86400 * 2) * 30), "/", "",  0);
            $filter_selection = $this->uri->segment(1);
            $filter_type = strtolower($this->uri->segment(1));
            $this->seo_canonical = base_url().$this->uri->segment(1);
          }else if($this->uri->segment(1) == "all"){
            $this->seo_canonical = base_url();
            setcookie('SCGender', $this->uri->segment(1), time()-3600, "/", "",  0);
          }else if(isset($_COOKIE['SCGender'])){

            $filter_selection = $_COOKIE['SCGender'];
            $filter_type = strtolower($_COOKIE['SCGender']);
          }

          if(isset($_COOKIE['SCGender'])){
            $uri = $this->uri->segment(1);
            if($uri != $filter_type && $uri==''){
              redirect($filter_type);
            }
          }


          $user_data['user_id'] = $this->session->userdata('user_id');
          $user_data['limit'] = 4;
          $user_data['offset'] = 0;
          $user_data['body_shape'] = @$this->Pa_model->get_users_answer_obj($user_data['user_id'],'user_body_shape')[0]['id'];
          $user_data['pa_pref1'] = @$this->Pa_model->get_users_answer_obj($user_data['user_id'],'user_style_p1')[0]['id'];
          $user_data['type'] = '';
          $user_data['gender'] = $this->session->userdata('gender'); 
          $data['user_id'] = $this->session->userdata('user_id');
          $user_data['bucket_id'] = $this->session->userdata('bucket');       
        
        // if( $this->cache->file->get('filter_type_scbox') == FALSE)
        // {
          $this->load->view('seventeen/common/header_view');
          $this->load->view('seventeen/scbox',$data);
          $this->load->view('seventeen/common/footer_view');
        //   $this->cache->file->save('filter_type_scbox', $data,500000000000000000);
        // }else
        // {                
        //    $data = $this->cache->file->get('filter_type_scbox');
        // }
    }
} 

public function register_user(){
    $data = array();
    $data['agerange'] = '';
    $data['token'] = $this->create_token($token_type='signup');
    if(empty($this->session->userdata('user_id')))
    {
        if(!empty($this->input->post())){
          $sc_name = preg_replace('/\s+/', ' ',trim($this->input->post('sc_name')));
          $sc_name = explode(' ',$sc_name);

          @$sc_firstname 	= trim($sc_name[0]);
          @$sc_lastname 	= trim($sc_name[1]);

          $ageRange 						= $this->input->post('sc_agerange');
          $scmobile 						= $this->input->post('sc_mobile');
          @$json_array['firstname'] 		= $sc_firstname;
          @$json_array['lastname'] 			= $sc_lastname;
          @$scusername 						= $this->input->post('sc_username_name');
          @$sc_gender 						= $this->input->post('sc_gender');
          @$json_array['scmobile'] 			= $scmobile;
          @$json_array['question_id'] 		= $sc_gender==1 ? 8:17;
          @$json_array['age_range'] 		= $this->input->post('scbox_age');
          $scemail_id 						= $this->input->post('sc_emailid');
          $scpassword 						= $this->input->post('sc_password');
          $promo 							= $this->input->post('registered_from') !='' ? $this->input->post('registered_from') : '';
          /* Added By Sudha For Event*/
          $event = $this->input->post('sc_event') !='' ? $this->input->post('sc_event') : '';
          $data['inputData'] = '';
          if($sc_gender==1)
          {
            $data['agerange'] = $this->Pa_model->get_question_answers(8);
          }
		  else if($sc_gender==2)
          {
            $data['agerange'] =  $this->Pa_model->get_question_answers(17);
          }

		  if(trim($sc_firstname) == '' || trim($sc_lastname) == ''){
            $data['error'] = "Please enter the Firstname Lastname";
          }
		 /*  else if(trim($json_array['age_range']) == ''){
             $data['error'] = "Please select Age range";
          } */
		 /* else if(trim($json_array['age_range']) == ''){
             $data['error'] = "Please select date of birth";
          }*/
		  else  if(trim($scemail_id) == ''){
            $data['error'] = "Please enter the Email-id";
          }
		  else if(trim($sc_gender) == '')
		  {
            $data['error'] = "Please select Gender";
          }
		  else if(trim($scpassword) == '')
		  {
            $data['error'] = "Please enter the Password";
          }
		  else if(trim($scmobile) == '')
          {
           $data['error'] = "Please enter the Mobile No.";
          }
		  else if(filter_var($scemail_id, FILTER_VALIDATE_EMAIL) === false)
		  {
            $data['error'] = "Please enter valid Email-id";
          }
		  else if($this->emailUnique($scemail_id) > 0){
            $data['error'] = "Email id already exists";

          }
		  else if($event!='')
		  {
            $ip = $this->input->ip_address();
            $scevent = strip_tags(trim($event));
            $eventcode = $scevent;
            $json_array['event_code'] = $eventcode;
            $this->signup('event',$scusername,$scemail_id,$scpassword,$ip,2,$sc_gender,$json_array);
    			//echo $this->db->last_query();
    			//exit(); 
          }
		  else
		  {
            $ip = $this->input->ip_address();
            if($this->input->post('sc_signup')=='edit')        
			{
                $this->signupedit('website',$scusername,$scemail_id,$scpassword,$ip,2,$sc_gender,$json_array);
            }
			else
			{
				$webres = $this->signup('website',$scusername,$scemail_id,$scpassword,$ip,2,$sc_gender,$json_array);
			}
            if($promo=='comp')
			{
                $this->session->set_userdata('campaign','yes');
            }
            if($webres)
            {
                /*Mailchimp call For New User*/
                 if(!empty($this->session->userdata('user_id')))
                {
                  if($this->Schome_model->get_nonscdomain($scemail_id))
                  {
                    $result = $this->curl->simple_post(BACKENDURL.'/Mailchimp/newUserImport/'.$this->session->userdata('user_id'), false, array(CURLOPT_USERAGENT => true));
                  }
                }
                /*End Mailchimp call For New User*/
                if($scemail_id!='')
                {
                  $this->Schome_model->update_couponcode($scemail_id,'Welcome10');
                }
                $otp = $this->scgenrate_otp($scmobile);
               $this->send_welcome_email($scemail_id);
              redirect(base_url().'pa/1');
               //redirect('/');
            }
			else
				{
				  redirect(base_url().'signup');
				}
          }
          $inputData = array('sc_name' => $sc_name, 'sc_gender' => $sc_gender, 'scmobile' => $scmobile, 'scemail_id' => $scemail_id, 'agerange' => $ageRange);
          $data['inputData'] = json_encode($inputData);
		  $this->seo_title = ' Stylecracker.com - Signup';
          $this->load->view('seventeen/common/header_view');
          $this->load->view('seventeen/signup',$data);
          $this->load->view('seventeen/common/footer_view');
        }
		else
			{
				$this->seo_title = ' Stylecracker.com - Signup';
				$this->load->view('seventeen/common/header_view');
				$this->load->view('seventeen/signup',$data);
				$this->load->view('seventeen/common/footer_view');
			}
    }
	else
		{
		   redirect('/');
		}
  }
  
  public function usernameUnique($username){
      $res = $this->Schome_model->usernameUnique($username);
      return count($res);
  }

  public function emailUnique($email){
      $res = $this->Schome_model->emailUnique($email);
      return count($res);
  }

  /*public function signup($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$sc_gender=1, $json_array = array()){
	
      // $res = $this->Schome_model->registerUser($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$sc_gender,$json_array);
      $res = $this->User_info_model->registerUser($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$sc_gender,$json_array);
	  
      if($scemail_id!='')
      {
        $this->Schome_model->update_couponcode($scemail_id,'Welcome10');
      }
      if($res){ return $res; }else{ return '0'; }

  }*/

  public function signup($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$sc_gender, $json_array = array()){
    //echo $medium.'--'.$scusername.'--'.$scemail_id.'---'.$scpassword.'--'.$ip.'--'.$role.'--'.$sc_gender;exit;
      $scusername = $scusername.date('Y-m-d h:i:s');
      $res = $this->User_info_model->registerUser($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$sc_gender,$json_array);
   /*  echo $this->db->last_query();
      echo "<pre>";
    print_r($res);
    echo "</pre>";
    exit();  */
      if($scemail_id!=''){
        $this->Schome_model->update_couponcode($scemail_id,'Welcome10');
      }
      if($res){ return $res; }else{ return '0'; }
  }

  public function pa($edit_id=NULL){



    if($this->session->userdata('user_id')){
        $data = array();
        $user_id = $this->session->userdata('user_id');
		    $gender = $this->session->userdata('gender');
        $data['mobileno'] = $this->User_info_model->get_users_info_mobile($user_id)->contact_no;
        $data['confirm_mobile'] = $this->User_info_model->getOtpStatus($this->session->userdata('user_id'));
        $data['registered_from'] = $this->User_info_model->get_users_info_mobile($user_id)->registered_from;
        if($this->uri->segment(2)){
          $edit_id = $this->uri->segment(2);
        }else{
          $edit_id = '';
        }
        
        if(@$_GET['type']=='add' && $data['confirm_mobile']!=1 && $data['mobileno']!='' && $data['registered_from']!='google' && $data['registered_from']!='facebook' && $edit_id=='')
        {          
          $scmobile = $data['mobileno'];
          $otp = $this->scgenrate_otp($scmobile);
         
        }else if($edit_id!=2 && $edit_id!=1)
        {
        
          redirect('/');
        }

        
		    $img_set = $this->Pa_model->get_user_img_set($user_id);

        if($this->session->userdata('gender')==1){

            $data['body_shape'] = $this->Pa_model->get_question_img_answers(1,$gender,'',$img_set);
            $data['style'] = $this->Pa_model->get_question_img_answers(2,$gender,'',$img_set);
			      $data['brand_list'] = unserialize(PA_BRAND_LIST_WOMEN);
        }else{

			      $data['body_shape'] = $this->Pa_model->get_question_img_answers(24,$gender,'',$img_set);
            $data['style'] = $this->Pa_model->get_question_img_answers(25,$gender,'',$img_set);
			      $data['brand_list'] = unserialize(PA_BRAND_LIST_MEN);
        }
        	
			$pa_image_sets = array("set1/", "set2/", "set3/");
			$rand_set = $pa_image_sets[array_rand($pa_image_sets)];
			
    		$data['user_body_shape'] = @$this->Pa_model->get_users_answer_obj($user_id,'user_body_shape',$rand_set)[0];
    		$data['user_style_p1'] = @$this->Pa_model->get_users_answer_obj($user_id,'user_style_p1',$rand_set)[0];
    		$data['user_style_p2'] = @$this->Pa_model->get_users_answer_obj($user_id,'user_style_p2',$rand_set)[0];
    		$data['user_style_p3'] = @$this->Pa_model->get_users_answer_obj($user_id,'user_style_p3',$rand_set)[0];
    		$data['user_brand_list'] = @$this->Pa_model->get_users_brand_list($user_id,'user_brand_list','',$gender);
        $data['slide'] = 'new';
        //echo '<pre>';print_r($data['user_brand_list']);
        $edit_id = $this->uri->segment(2);
        if($edit_id!=''){

          $data['user_pa_data'] = $this->pa_get($user_id,$gender);
          //echo '<pre>';print_r($data['user_pa_data']);

          $data['question_resume_id'] = $edit_id;
          $data['edited_data'] ='edit';
        }else{

          $data['question_resume_id'] = $this->Pa_model->get_question_resume_id();
          $data['edited_data'] ='';
        }
    		// echo "<pre>";print_r($data);exit;
    		$this->load->view('seventeen/common/header_view');
    		$this->load->view('seventeen/pa',$data);
    		$this->load->view('seventeen/common/footer_view');
        // $this->load->view('seventeen/profile_js');



    }else{
      redirect('/');
    }

  }

  public function logout(){
      $result = $this->Schome_model->track_logout();
      unset($_COOKIE['billing_first_name']);
      unset($_COOKIE['billing_last_name']);
      unset($_COOKIE['billing_mobile_no']);
      unset($_COOKIE['billing_address']);
      unset($_COOKIE['billing_city']);
      unset($_COOKIE['billing_state']);

      unset($_COOKIE['shipping_first_name']);
      unset($_COOKIE['shipping_last_name']);
      unset($_COOKIE['shipping_mobile_no']);
      unset($_COOKIE['shipping_address']);
      unset($_COOKIE['shipping_city']);
      unset($_COOKIE['shipping_state']);
      unset($_COOKIE['SCUniqueID']);

      setcookie('billing_first_name', '', time() - 3600, '/');
      setcookie('billing_last_name', '', time() - 3600, '/');
      setcookie('billing_mobile_no', '', time() - 3600, '/');
      setcookie('billing_address', '', time() - 3600, '/');
      setcookie('billing_city', '', time() - 3600, '/');
      setcookie('billing_state', '', time() - 3600, '/');

      setcookie('shipping_first_name', '', time() - 3600, '/');
      setcookie('shipping_last_name', '', time() - 3600, '/');
      setcookie('shipping_mobile_no', '', time() - 3600, '/');
      setcookie('shipping_address', '', time() - 3600, '/');
      setcookie('shipping_city', '', time() - 3600, '/');
      setcookie('shipping_state', '', time() - 3600, '/');
      setcookie('SCUniqueID', '', time() - 3600, '/');

      session_destroy();
      //md5(STYLELOGINID)   34b07cba21adf4cd95d35c1c0bd47ff0
      //md5(STYLELOGINPWD) 1c0253b55e82e53890985a837274811b
      delete_cookie("34b07cba21adf4cd95d35c1c0bd47ff0");
      delete_cookie("1c0253b55e82e53890985a837274811b");
      redirect('/');
  }

   public function login(){
    $data = array();
  //$data['token'] = $this->create_token($token_type='login');
  if(empty($this->session->userdata('user_id')))
    {
        if(!empty($this->input->post())){
        //$token = $this->input->post('token');
        // $validate_token = $this->validate_token($token,$token_type='login');
        // if($validate_token != 1){
        //   redirect(base_url().'404_override');  
        // }
        $logEmailid = $this->input->post('logEmailid');
        $logPassword = $this->input->post('logPassword');
        $logRemember = $this->input->post('logRemember');
		$scform = $this->input->post('scform_data');

         if(trim($logEmailid) == ''){

           $data['error'] = 'Please enter your email id';
            $this->load->view('seventeen/common/header_view');
            $this->load->view('seventeen/login',$data);
            $this->load->view('seventeen/common/footer_view');

          }else if(trim($logPassword) == ''){

          $data['error'] = 'Please enter the Password';
            $this->load->view('seventeen/common/header_view');
            $this->load->view('seventeen/login',$data);
            $this->load->view('seventeen/common/footer_view');

          }else{
           $result = $this->User_info_model->login_user($logEmailid,$logPassword,'website',' ');
          if($result == 1){
            //$delete_token = $this->delete_old_token($token,$token_type='login');
           if($logRemember == 1){
            //md5(STYLELOGINID)   34b07cba21adf4cd95d35c1c0bd47ff0
            //md5(STYLELOGINPWD) 1c0253b55e82e53890985a837274811b
            setcookie('34b07cba21adf4cd95d35c1c0bd47ff0',$this->encryptOrDecrypt($logEmailid,'encrypt'), time() + (86400 * 30), "/");
            setcookie('1c0253b55e82e53890985a837274811b',$this->encryptOrDecrypt($logPassword,'encrypt'), time() + (86400 * 30), "/");

           }

            $question_comp = $this->session->userdata('question_comp');
            $question_resume_id = $this->session->userdata('question_resume_id');

           if(!empty($scform))
			{
				redirect(base_url().'book-scbox/step1/');
				$this->session->unset_userdata('scform');
			}
			else{
				if($this->session->userdata('bucket') == '' || $question_comp!=1 ){
					if(@$this->session->userdata('url') != ''){
					  header("Location: ".$this->session->userdata('url'));
					}else{
					   redirect(base_url().'my-orders');
					}
				}
				else{
					setcookie('is_redirect', 1, time() + (86400 * 30), "/");
					 redirect(base_url().'my-orders');
					if(@$this->session->userdata('url') != ''){
					  header("Location: ".$this->session->userdata('url'));
					}else{
					   redirect(base_url().'my-orders');
					}
				}
		  }

            /*Mailchimp call For UserSignUp*/
             if(!empty($this->session->userdata('user_id')))
            {
              if($this->Schome_model->get_nonscdomain($logEmailid))
              {
              $result = $this->curl->simple_post(BACKENDURL.'/Mailchimp/newUserImport/'.$this->session->userdata('user_id'), false, array(CURLOPT_USERAGENT => true));
              }
            }
            /*End Mailchimp call For UserSignUp*/

            /*OneSignal Notification*/
            //$os_data = $this->senddata_onesignal();
            //$this->session->set_userdata(array('onesignaldata'=>$os_data));
            /*OneSignal Notification End*/

          }else{

            $data['error'] = 'Invalid Email and Password';
            $this->load->view('seventeen/common/header_view');
            $this->load->view('seventeen/login',$data);
            $this->load->view('seventeen/common/footer_view');
          }

          }
       }else
       {
        
        $this->seo_title = 'Stylecracker.com - Login';
        $this->load->view('seventeen/common/header_view');
        $this->load->view('seventeen/login',$data);
        $this->load->view('seventeen/common/footer_view');
       }
  }else
    {
       redirect('/');
    }
  }

/*Forgot Password Function*/
  public function forgotPassword()
  {
    $Emailid = $this->input->post('Emailid');
    if(trim($Emailid) == ''){
            echo "Please enter your email id";

          }else{
            $result = $this->Schome_model->forgot_password($Emailid);
            if($result != false){

               if($this->send_forgotpwd_email($Emailid,$result))
               {
                  
				  $mobileNumber = $this->Schome_model->send_forgot_password_to_mobile($Emailid);
				file_get_contents('https://2factor.in/API/R1/?module=TRANS_SMS&apikey='.API_KEY_2FACTOR.'&to='.$mobileNumber['contact_no'].'&from='.OTP_FROM.'&templatename=forgot_password&var1='.$result.'');
				  echo '<p style="color:#13d792;">New password has been sent to your email-id '.$Emailid.'</p>';
               }else
               {
                  echo "Email Error";exit;
               }

            }else{
              echo 'Invalid Email-id ';exit;

            }

          }
  }


/*Reset Password Function*/

  public function resetPassword()
  {
    $oldpwd = $this->input->post('oldpwd');
    $newpwd = $this->input->post('newpwd');

    if(trim($oldpwd) == ''){
        echo "Please enter your Old Password";

          }if(trim($newpwd) == ''){
            echo "Please enter your New Password";

          }else{
            $result = $this->Schome_model->reset_password($oldpwd,$newpwd);
            if($result){

             echo "Your Password has been reset";

            }else{

              if($result == false){
                echo "Old Password is not correct";exit;
              }else
              {
                echo $result;exit;
              }
            }
          }
  }


  public function aboutus(){

      $data = array();
      if ( $this->cache->file->get('sc_about_us') == FALSE ) {
        $data['stylist_data'] = $this->Schome_model->get_all_stylist();

        $this->cache->file->save('sc_about_us', $data, 500000000000000000);
      }else{

        $data = $this->cache->file->get('sc_about_us');
      }

      $this->seo_title = "About StyleCracker - India's personalized styling platform";
      $this->seo_desc = 'Get customised, shoppable looks & products to suit your body type, personal style & budget';
      $this->seo_keyword = $this->lang->line('seo_keyword');

      $this->load->view('common/header_view');
      $this->load->view('about_us',$data);
      $this->load->view('common/footer_view');
  }


  function get_look_fav($look_id){
    if($this->session->userdata('user_id') && $look_id){
      $res = $this->Schome_model->get_look_fav_or_not($look_id);
      return $res;
    }
  }

  public function faq(){
    $this->seo_title = 'FAQ - StyleCracker';
    $this->seo_desc = $this->lang->line('seo_desc');
    $this->seo_keyword = $this->lang->line('seo_keyword');

      $this->load->view('common/header_view');
      $this->load->view('faq');
      $this->load->view('common/footer_view');
  }

  public function privacy_policy(){

    $this->seo_title = 'Privacy Policy - StyleCracker';
    $this->seo_desc = $this->lang->line('seo_desc');
    $this->seo_keyword = $this->lang->line('seo_keyword');

      $this->load->view('common/header_view');
      $this->load->view('privacy_policy');
      $this->load->view('common/footer_view');
  }

   public function return_policy(){

    $this->seo_title = 'Return Policy - StyleCracker';
    $this->seo_desc = $this->lang->line('seo_desc');
    $this->seo_keyword = $this->lang->line('seo_keyword');

      $this->load->view('common/header_view');
      $this->load->view('return_policy');
      $this->load->view('common/footer_view');
  }

	public function terms_of_use(){
    $this->seo_title = 'Terms and Conditions - StyleCracker';
    $this->seo_desc = $this->lang->line('seo_desc');
    $this->seo_keyword = $this->lang->line('seo_keyword');

	  $this->load->view('common/header_view');
	  $this->load->view('terms_of_use');
	  $this->load->view('common/footer_view');
	}

	public function contact_us(){
		
		$this->load->view('seventeen/common/header_view');
		$this->load->view('seventeen/static/contact_us');
		$this->load->view('seventeen/common/footer_view');
		
	}

   public function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
  }


  public function register_user_google_signin(){

    if (isset($_GET['code'])) {

      $this->googleplus->getAuthenticate();
      $user_profile = $this->googleplus->getUserInfo();
      //$this->session->set_userdata('login',true);
      //$user_profile = $this->session->set_userdata('user_profile',$this->googleplus->getUserInfo());

      $scpassword = $this->randomPassword();
      $ip = $this->input->ip_address();
      if(isset($user_profile['given_name'])) $user_profile['firstname'] = $firstName = $user_profile['given_name']; else $firstName = '';
      if(isset($user_profile['family_name'])) $user_profile['lastname'] = $lastName = $user_profile['family_name']; else $lastName = '';
      $scusername = $firstName.$lastName;
      $scemail_id = $user_profile['email'];


      $gender = 1;
      if($user_profile['gender'] == 'male')
        $gender = 2;

      $result = $this->signup('google',$scusername,$scemail_id,$scpassword,$ip,2,$gender,$user_profile);
      /*Mailchimp call For UserSignIn*/
         if(!empty($this->session->userdata('user_id')))
        {
            $result = $this->curl->simple_post(BACKENDURL.'/Mailchimp/newUserImport/'.$this->session->userdata('user_id'), false, array(CURLOPT_USERAGENT => true));
        }
      /*End Mailchimp call For UserSignIn*/
      if($result == 'pa')
      {
          redirect('/');
         //redirect('schome_new/pa?ga=ga');
      }else redirect('schome_new');

    }

    $this->data['google_login_url'] = $this->googleplus->loginURL();
  }

  public function send_welcome_email($email){

      $config['protocol'] = 'smtp';
//      $config['mailpath'] = '/usr/sbin/sendmail';
      $config['charset'] = 'iso-8859-1';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');
      $data = array();
      $this->email->initialize($config);

      $username = $this->session->userdata('first_name');

      $this->email->from($this->config->item('from_email'), 'Stylecracker');
      if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){
         $email = $this->config->item('sc_test_emaild');
          //$email_id2 = "arun@stylecracker.com";
      }else{
        $this->email->to($email);
      }
      $this->email->subject('Welcome to StyleCracker');
      $this->email->message('Hi '.$username.',<br>Welcome to StyleCracker! <br><br> StyleCracker is India\'s first online styling platform and we are here to make you the best version of youself! <br><br> When we started StyleCracker our aim was to make personalized styling available to all. We have a dedicated team of stylists who are online 24/7 waiting to answer ANY style query you may have. Never again will you have to worry about \'what do I wear\'- be it day-to-day or for special occasions, we will be there! <br><br> So, please explore the website and let us know how we can help. It would be great if you could also share your feedback with us, it will help us present the best product for you. <br><br> Love, <br>Archana Walavalkar & Dhimaan Shah <br>Founders<br> <a href="'.base_url().'">www.stylecracker.com</a>');
      $this->email->send();
      //$this->load->view('emailer/welcome-to-sc',$data);

      // $this->email->subject('StyleCracker has a new friend');
      // $message = $this->load->view('emailer/welcome-to-sc',$data,true);
      // $this->email->message($message);
     // $this->email->send();

  }


  public function send_forgotpwd_email($email_id,$new_password){

      $config['protocol'] = 'smtp';
//      $config['mailpath'] = '/usr/sbin/sendmail';
 //     $config['charset'] = 'iso-8859-1';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');

      $this->email->initialize($config);

      $this->email->from($this->config->item('from_email_forgetpwd'), 'Stylecracker');
      if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){
       $email_id = $this->config->item('sc_test_emaild');
        //$email_id2 = "arun@stylecracker.com";
      }else{
        $this->email->to($email_id);
      }
      $this->email->subject('StyleCracker: New Password');
      $this->email->message("Your login details are <br> Email : ".$email_id."<br><br>Your new password is:".$new_password."<br><br>You can change your password at any time by logging into your account. <br><br>Warm Regards,<br>StyleCracker");

      if($this->email->send())
      {
        return "Email Sent";
      }else
      {
        return "Email Error";
      }

  }

  public function redirect_thankyou_page(){
     $this->load->view('redirect_thankyou_page');
  }

   public function look_hv_discount($look_id){
      return $this->Schome_model->look_hv_discount($look_id);
  }

  public function redirect(){
    $this->load->view('redirecttoapp');
  }

  function fav_add(){
        $data['fav_id']="";
        $data['fav_for']="";

        $data = array_merge($data, $this->input->post());

        if($this->session->userdata('user_id'))
          $data['user_id']=$this->session->userdata('user_id');

        if(($data['fav_for'] == "product" || $data['fav_for'] == "brand"|| $data['fav_for'] == "collection" || $data['fav_for'] == "event" || $data['fav_for'] == "blog") && is_numeric($data['user_id']) && is_numeric($data['fav_id'])){
          $this->load->model("Favorites_model");
          $response = $this->Favorites_model->add_fav($data);
          if($response == 1)
          {
            $result['success'] = "true";
            $result['msg'] = "Successful Added";
            print_r(json_encode($result));
          }else if($response == 2)
          {
            $result['success'] = "true";
            $result['msg'] = "Successful deleted";
            print_r(json_encode($result));
          }else{
            $result['success'] = "false";
            $result['msg'] = "Unable to add.";
            print_r(json_encode($result));
          }

        }else{
          $result['success'] = "false";
          $result['msg'] = "invalid request";
          print_r(json_encode($result));
        }
    }

	function confirm_email(){
		$email = base64_decode($this->uri->segment(3));
		$confirm_email = $this->Schome_model->confirm_email($email);
		$referee_id = $this->Schome_model->getreferee_id($email);
		if($confirm_email == '1'){

			// if($referee_id > 0){
				// $bid = '11257';
				// $secretKey = '72BB9BBBC9FDCA3165355D2F0985A983';
				// $campaignID = '10607';
				// $orderID = $referee_id; //set order id
				// $status = '1'; //status of the order
				// $event = 'register';
				// $http_val = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://';
				// $result = file_get_contents($http_val.'www.ref-r.com/campaign/t1/confirmConversion?secretKey='.$secretKey.'&bid='.$bid.'&campaignID='.$campaignID.'&orderID='.$orderID.'&status='.$status.'&event='.$event);
			// }

			$this->load->view('common/header_view');
			$this->load->view('login/login');
			$this->load->view('common/footer_view');
		}else {
			 header("Location: https://www.stylecracker.com/");
		}

	}

  public function home_new(){
    if($this->session->userdata('user_id') > 0){
      header("Location: ".base_url());
      exit;
    }

    if(isset($_REQUEST['utm_source'])){
      $utm_source = substr($_REQUEST['utm_source'],strpos($_REQUEST['utm_source'],"utmcsr="));
      $array_field = array('utm_source' => trim($utm_source),'utm_source1' => trim($utm_source));
      $this->session->set_userdata($array_field);

   }

   $this->seo_title = $this->lang->line('seo_title');
   $this->seo_desc = $this->lang->line('seo_desc');
   $this->seo_keyword = $this->lang->line('seo_keyword');

    if($this->cache->file->get('home_new') == FALSE){
      $data['products'] = $this->home_model->get_latest_home_page_products();
      $this->cache->file->save('home_new', $data['products'],500000000000000000);
    }else{
       $data['products'] = $this->cache->file->get('home_new');
    }
    $this->load->view('common/header_view');
    $this->load->view('home/home_new',$data);
    $this->load->view('common/footer_view');
  }

	function sc_change_gender(){
	  $type = $this->input->post('type');
	  $gender = $this->input->post('gender');
	  if($this->session->userdata('gender')!=$gender){
		echo $this->Pa_model->sc_change_gender($gender);exit;
	  }else{
		echo '2';exit;
	  }
	}

	public function senddata_onesignal(){
		$osdata = array();
		$os_fname = (@$this->session->userdata('first_name')!='') ? $this->session->userdata('first_name') : ' ';
		$os_lname = (@$this->session->userdata('last_name')!='') ? $this->session->userdata('last_name') : ' ';
		$osdata['name'] =  $os_fname.' '.$os_lname;
		$osdata['email'] =   (@$this->session->userdata('email')!='') ? $this->session->userdata('email') : ' ';
		$osdata['gender'] =   (@$this->session->userdata('gender')!='' && @$this->session->userdata('gender')==1 ) ? 'female' : 'men ';
		$osdata['user_id'] =   (@$this->session->userdata('user_id')!='') ? $this->session->userdata('user_id') : ' ';

		$pa_data = $this->home_model->get_user_pa($this->session->userdata('user_id'));
		 $question_ans = array();
			 if(!empty($pa_data)){
				foreach($pa_data as $val){
					 $question_ans[$val['question_id']] = $val['answer'];
				}
			 }
		$cart = $this->home_model->get_user_cart(@$this->session->userdata('user_id'));
		if(@$this->session->userdata('gender')!='' && @$this->session->userdata('gender')==1)
		{ /*For Female*/


		  $osdata['user_age'] =   (@$question_ans[8]!='') ? $question_ans[8] : ' ';
		  $osdata['bucket_id'] =   (@$this->session->userdata('bucket')!='') ? $this->session->userdata('bucket') : ' ';
		  $osdata['user_size'] =   (@$question_ans[9]!='') ? $question_ans[9] : ' ';
		  $osdata['user_style'] =   (@$question_ans[2]!='') ? $question_ans[2] : ' ';
		  $osdata['user_budget'] =   (@$question_ans[7]!='') ? $question_ans[7] : ' ';
		  $osdata['user_body_shape'] = (@$question_ans[1]!='') ? $question_ans[1] : ' ';
		  $osdata['abandoned_cart'] =  (@$cart==1) ? 'yes' : 'no';

		}else
		{ /*For Male*/
		  $bt = strip_tags($question_ans[13]);
		  $bodyType=substr($bt, 0, strrpos($bt, ' '));
		  $osdata['user_age'] =   (@$question_ans[15]!='') ? $question_ans[15] : ' ';
		  $osdata['body_type'] =   (@$bodyType!='') ? $bodyType : ' ';
		  $osdata['bucket_id'] =   (@$this->session->userdata('bucket')!='') ? $this->session->userdata('bucket') : ' ';
		  $osdata['work_style'] =   (@$question_ans[14]!='') ? strip_tags($question_ans[14]) : ' ';
		  $osdata['body_height'] =   (@$question_ans[17]!='') ? $question_ans[17] : ' ';
		  $osdata['user_budget'] =   (@$question_ans[18]!='') ? $question_ans[18] : ' ';
		  $osdata['personal_style'] =   (@$question_ans[16]!='') ? $question_ans[16] : ' ';
		  $osdata['abandoned_cart'] =   (@$cart==1) ? 'yes' : 'no';
		}

		$encode_os = json_encode($osdata);
		 //$this->session->set_userdata(array('onesignaldata'=>$os_data));
		return $encode_os;
	}

  function getAgerange()
  {
    $type = $this->input->post('type');
    if($type=='getage')
    {
      $agerange = array();
      $gender = $this->input->post('sc_gender');
      $html_ag = '';
      if($gender=='female')
      { $agerange = $this->Pa_model->get_question_answers(8);
      }else if($gender=='male')
      { $agerange = $this->Pa_model->get_question_answers(17); }

      if(!empty($agerange)){
          $html_ag = $html_ag.'<option value="">AGE RANGE</option>';
          foreach($agerange as $val){
              $html_ag = $html_ag.'<option value="'.$val['id'].'" >'.$val['answer'].'</option>';
            }
          }
      echo $html_ag;exit;
    }
  }

  /*public function scgenrate_otp($uc_mobile_no){
    if($uc_mobile_no!='')
    {
      $data['mobile_no'] = '91'.$uc_mobile_no;
	  $data['type'] = 'register';
      $otp_no = $this->User_info_model->scgenrate_otp($data);
      if($otp_no > 0){
         // Call SMS send API 
        $message = 'Welcome+to+StyleCracker.+Your+OTP+for+mobile+verification+is+'.$otp_no.'.+Stay+Stylish!';
        file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile='.$data['mobile_no'].'&message='.$message);       
         // end of code Call SMS send API 
        return $otp_no;
      }else{
        return false;
      }
    }
  }*/

 public function scgenrate_otp($uc_mobile_no){
    if($uc_mobile_no!='')
    {
      $data['mobile_no'] = '91'.$uc_mobile_no;
    $data['type'] = 'register';
      $otp_no = $this->User_info_model->scgenrate_otp($data);
      if($otp_no > 0){
        /* Call SMS send API */
        // $message = 'Welcome+to+StyleCracker.+Your+OTP+for+mobile+verification+is+'.$otp_no.'.+Stay+Stylish!';
        // file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile='.$data['mobile_no'].'&message='.$message);      
    file_get_contents('https://2factor.in/API/R1/?module=TRANS_SMS&apikey='.API_KEY_2FACTOR.'&to='.$uc_mobile_no.'&from='.OTP_FROM.'&templatename=mobile_verification&var1='.$otp_no.'');   
        /* end of code Call SMS send API */
        return $otp_no;
      }else{
        return false;
      }
    }
  }
  public function sc_check_otp()
  {
    if (!$this->input->is_ajax_request()) {
        exit('No direct script access allowed');
    }else{

      $data['mobile_no'] = '91'.$this->input->post('mobile_no');
      $data['userid'] = $this->input->post('userid');
	  $data['type'] = 'register';
      if($this->input->post('type')=='check_otp')
      {
        $data['otp_number'] = $this->input->post('otp_number');
        $return_otp = $this->User_info_model->scgenrate_otp($data);
        if($data['otp_number']==$return_otp)
        {
          $this->User_info_model->updateOtpStatus($data['mobile_no'],$return_otp,$data['userid']);
          echo true;exit;
        }else{
          echo false;exit;
        }
      }else if($this->input->post('type')=='resend_otp')
      {
        $data['mobile_no'] = $this->input->post('mobile_no');
		$data['userid'] = $this->input->post('userid');
		$data['type'] = 'register';
        $return_otp = $this->scgenrate_otp($data['mobile_no']);
        if($return_otp)
        {
           echo true;exit;
        }else
        {
           echo false;exit;
        }

      }

    }
  }

	function update_pa(){

    $type = $this->input->post('type');
    $answer = $this->input->post('answer');
    $user_id = $this->session->userdata('user_id');
    $gender = $this->session->userdata('gender');    
    $brand_list = '';

    $data['user_pa_data'] = $this->pa_get($user_id,$gender);  
    //echo '<pre>';print_r($data['user_pa_data']);exit;
    if(@$data['user_pa_data']->user_body_shape!='' && @$data['user_pa_data']->user_style_p1)
    {
      $user_bodyshape_ans = @$data['user_pa_data']->user_body_shape;
      $user_stylep1_ans = @$data['user_pa_data']->user_style_p1;
    }else
    {
      $user_bodyshape_ans = 0;
      $user_stylep1_ans = 0;
    }
   
   
    if($gender == '1'){
      $data_answers = array('user_body_shape'=>1,'user_style_p1'=>2,'user_style_p2'=>2,'user_style_p3'=>2,'user_brand_list'=>30);
    }else{
      $data_answers = array('user_body_shape'=>24,'user_style_p1'=>25,'user_style_p2'=>25,'user_style_p3'=>25,'user_brand_list'=>30);
    }
    if($type=='body_shape')
    {
      $this->Pa_model->save_user_answer($data_answers['user_body_shape'],$answer,$user_id,$gender,'user_body_shape');     
      //$this->Pa_model->assign_buckets($answer,$user_stylep1_ans,$user_id);
    }else if($type=='body_style')
    {
      $this->Pa_model->save_user_answer($data_answers['user_style_p1'],$answer[0],$user_id,$gender,'user_style_p1');
      $this->Pa_model->save_user_answer($data_answers['user_style_p2'],$answer[1],$user_id,$gender,'user_style_p2');
      $this->Pa_model->save_user_answer($data_answers['user_style_p3'],$answer[2],$user_id,$gender,'user_style_p3');
     // $this->Pa_model->assign_buckets($user_bodyshape_ans,$answer[0],$user_id);
    }else if($type =='brandlist')
    {
       if(!empty($answer)){ $brand_list = implode(',',$answer);}else $brand_list = '';
       $this->Pa_model->save_user_answer($data_answers['user_brand_list'],$brand_list,$user_id,$gender,'user_brand_list');
    }
	
	}

	function save_pa(){

		$user_body_shape = $this->input->post('user_body_shape');
		$arr_user_style_pref = $this->input->post('arr_user_style_pref');
		$arr_user_brand_list = $this->input->post('arr_user_brand_list');
		$gender = $this->session->userdata('gender');
		$user_id = $this->session->userdata('user_id');

		$brand_list = '';$arr_answers = array();
		if(!empty($arr_user_brand_list)){ $brand_list = implode(',',$arr_user_brand_list);}else $brand_list = '';

		$arr_answers[0]['question'] =  'user_body_shape';
		$arr_answers[0]['answer_id'] =  $user_body_shape ;
		$arr_answers[1]['question'] =  'user_style_p1';
		$arr_answers[1]['answer_id'] =  $arr_user_style_pref[0];
		$arr_answers[2]['question'] =  'user_style_p2';
		$arr_answers[2]['answer_id'] =  $arr_user_style_pref[1];
		$arr_answers[3]['question'] =  'user_style_p3';
		$arr_answers[3]['answer_id'] =  $arr_user_style_pref[2];
		$arr_answers[4]['question'] =  'user_brand_list';
		$arr_answers[4]['answer_id'] =  $brand_list;
		// echo "<pre>";print_r($arr_answers);exit;

		if($gender == '1'){
			$data_answers = array('user_body_shape'=>1,'user_style_p1'=>2,'user_style_p2'=>2,'user_style_p3'=>2,'user_brand_list'=>30);
		}else{
			$data_answers = array('user_body_shape'=>24,'user_style_p1'=>25,'user_style_p2'=>25,'user_style_p3'=>25,'user_brand_list'=>30);
		}

		$body_shape_ans = 0;$user_style_p1 = 0;

		foreach($arr_answers as $key=>$json){
			$question = $json['question'];
			$question_id = $data_answers[$question];
			$answer_id = $json['answer_id'];
			if($question_id == '1' || $question_id == '24'){ $body_shape_ans = $answer_id; }
			if(($question_id == '2' || $question_id == '25') && $question == 'user_style_p1'){ $user_style_p1 = $answer_id; }
			$this->Pa_model->save_user_answer($question_id,$answer_id,$user_id,$gender,$question);
		}
		$bucket = $this->Pa_model->assign_buckets($body_shape_ans,$user_style_p1,$user_id);
		return true;

	}

	function save_profile_img(){
		$base64_string = $this->input->post('imageData');
		$user_id = $this->session->userdata('user_id');
		if($base64_string != ''){
			$pos = strpos($base64_string, ';');
			$type = explode('/', substr($base64_string, 0, $pos))[1];
			// $file_name = $user_id.'.'.$type;
			$file_name = time();
			$output_file = $this->config->item('profile_image_path').$file_name;
			$ifp = fopen($output_file, "wb");
			$data = explode(',', $base64_string);
			// echo $output_file;
			$file_id = $this->Pa_model->update_profile_img($file_name);
			fwrite($ifp, base64_decode($data[1]));
			fclose($ifp);
			echo "Image has been Saved.";exit;
		}else{
			echo "Please select image to upload";exit;
		}
		
	}

  public function pa_get($user_id,$gender='')
  {
    // $gender=2;
    if($user_id){
		$img_set = $this->Pa_model->get_user_img_set($user_id);
      if($gender == '1'){
        $data = new stdClass();
        $data -> body_shape = $this->Pa_model->get_question_img_answers(1,$gender,'',$img_set);
        $data -> style = $this->Pa_model->get_question_img_answers(2,$gender,'',$img_set);

      } // end of women pa
      else {
        $data = new stdClass();
        $data->body_shape = $this->Pa_model->get_question_answers(24,$gender,'',$img_set);
        $data->style = $this->Pa_model->get_question_img_answers(25,$gender,'',$img_set);

      }// end men pa
	  
		$pa_image_sets = array("set1/", "set2/", "set3/");
		$rand_set = $pa_image_sets[array_rand($pa_image_sets)];
		
      $data -> brand_list = unserialize(PA_BRAND_LIST);
      $data -> user_body_shape = @$this->Pa_model->get_users_answer_obj($user_id,'user_body_shape',$gender,$rand_set)[0];
      $data -> user_style_p1 = @$this->Pa_model->get_users_answer_obj($user_id,'user_style_p1',$gender,$rand_set)[0];
      $data -> user_style_p2 = @$this->Pa_model->get_users_answer_obj($user_id,'user_style_p2',$gender,$rand_set)[0];
      $data -> user_style_p3 = @$this->Pa_model->get_users_answer_obj($user_id,'user_style_p3',$gender,$rand_set)[0];
      $data -> user_brand_list = @$this->Pa_model->get_users_brand_list($user_id,'user_brand_list','',$gender);
      $data -> question_resume_id = $this->Pa_model->get_question_resume_id($user_id);
      $data -> edited_data ='';
      return $data;

    }else{
      $this->failed_response(1001, "No Record(s) Found");
    }

  }
  
	function get_home_products(){
		
		$user_data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
		$user_data['limit_cond'] = " ";
		$data['products'] = $this->Schome_model->get_home_page_products($key='home_page_products',$user_data);
		
		$this->load->view('seventeen/common/header_view');
		$this->load->view('seventeen/home_products',$data);
		$this->load->view('seventeen/common/footer_view');
	}
	
	function save_contact_us_data(){
		$data = array();
		$contact_name = $this->input->post('contact_name');
		$email_id = $this->input->post('email_id');
		$subject = $this->input->post('subject');
		$message = $this->input->post('message');
		if($contact_name!='' && $email_id!='' && $subject!='' && $message!=''){
			$data = array('name' => $contact_name,'email'=>$email_id,'message' => $message,'enquiry_date_time'=>$this->config->item('sc_date'),'subject' => $subject);

			$config['protocol'] = 'smtp';
			$config['charset'] = 'iso-8859-1';
			$config['mailtype'] = 'html';
			$config['wordwrap'] = TRUE;
			$config['smtp_host'] = $this->config->item('smtp_host');
			$config['smtp_user'] = $this->config->item('smtp_user');
			$config['smtp_pass'] = $this->config->item('smtp_pass');
			$config['smtp_port'] = $this->config->item('smtp_port');

			$this->email->initialize($config);

			$this->email->from($this->config->item('from_email'), 'Stylecracker');
      if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){
       $email_id = $this->config->item('sc_test_emaild');
        //$email_id2 = "arun@stylecracker.com";
      }else{
			 $this->email->to('customercare@stylecracker.com');
      }
			$this->email->subject('User Enquiry-'.$subject);
			$new_msg = 'Name : '.$contact_name.'<br/>Email ID - '.$email_id.'<br/>Subject - '.$subject.'<br/>Message - '.$message;
			$this->email->message($new_msg);

			$this->email->send();
			return $this->Schome_model->add_contact_us($data);
		}
	}
	
	function user_wishlist(){
		
		$object_data = array();
		
		$data['user_id'] = $this->session->userdata('user_id');
		$data['body_shape'] = '';
		$data['pa_pref1'] = '';
		$data['bucket'] = '';
		$data['gender'] = '';
		$data['limit'] = 5;
		$data['offset'] = 0;
		$data['limit_cond'] = " LIMIT 0,5";
		$data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
		
		$fav_for = 'product';
		$object_ids = $this->User_info_model->get_wishlisted_ids($data,$fav_for);
		// product data
		if($fav_for == 'product' && !empty($object_ids)){
			$data['search_by'] = '1';$data['table_search'] = explode(",",$object_ids);
			$object_data['products'] = $this->Mobile_look_model->get_products_list($object_ids,$data);
			
		}else {
			$object_data['products'] = array();
			$object_data['product_message1'] = "You haven't bookmarked any products";
			$object_data['product_message2'] = "Tap on a products heart icon to add it to your bookmarks";
		}
		
		$fav_for = 'collection';
		$object_ids = $this->User_info_model->get_wishlisted_ids($data,$fav_for);
		
		// collection data
		if($fav_for == 'collection' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['collection_data'] = $this->Collection_model->get_all_collection($data);
			
		}else{
			$data['search_by'] = '0';
			$object_data['collection_data'] = array();
			$object_data['collection_message1'] = "You haven't bookmarked any collections";
			$object_data['collection_message2'] = "Tap on a collections heart icon to add it to your bookmarks";
		}
		
		$fav_for = 'event';
		$object_ids = $this->User_info_model->get_wishlisted_ids($data,$fav_for);
		// event data
		if($fav_for == 'event' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['event_data'] = $this->Event_model->get_all_event($data);
			
		}else{
			$data['search_by'] = '0';
			$object_data['event_data'] = array();
			$object_data['event_message1'] = "You haven't bookmarked any events";
			$object_data['event_message2'] = "Tap on a events heart icon to add it to your bookmarks";
			
		}
		
		$fav_for = 'blog';
		$object_ids = $this->User_info_model->get_wishlisted_ids($data,$fav_for);
		// blog data
		if($fav_for == 'blog' && !empty($object_ids)){
			
			$data['search_by'] = '1';$data['table_search'] = $object_ids;
			$object_data['blog_data'] = $this->Blog_model->get_blog_data($data);
			
		}else{
			$data['search_by'] = '0';
			$object_data['blog_data'] = array();
			$object_data['blog_message1'] = "You haven't bookmarked any blogs";
			$object_data['blog_message2'] = "Tap on a blogs heart icon to add it to your bookmarks";
			
		}
		$object_data['user_id'] = $this->session->userdata('user_id');
		// echo "<pre>";print_r($object_data);exit;
		$this->load->view('seventeen/common/header_view');
		$this->load->view('seventeen/user_wishlist',$object_data);
		$this->load->view('seventeen/common/footer_view');
	}
	
	function get_all_notification(){
		$user_data['user_id'] = $this->session->userdata('user_id');
		$data = $this->User_info_model->get_all_notification($user_data);
		$li_html = '';$count_html = '';$notification_count = 0;
		if(!empty($data)){
			foreach($data as $val){
				$active = '';if($val['order_notification_read'] == 1){ $active = 'active'; }else{ $notification_count++; }
				$li_html = $li_html.'<li onclick="ga("send", "event", "Notification", "clicked", "'.$val['notification_id'].'");" class="'.$active.'" id="notification'.$val['notification_id'].'" onclick="notification_read('.$val['notification_id'].');"><i class="icon-bag ic"></i> '.$val['message'].' </li>';
			}
		}else {
			$li_html = 'No notification found';
		}
		if($notification_count>0){$count_html = '<span class="ic-count shape-diamond solid">'.$notification_count.'</span>';}
		$notification_html = '<a class="dropdown-toggle" data-toggle="dropdown"  href="#"><i class="icon-notification"></i>
             '.$count_html.'
              </a>
              <div class="dropdown-menu dropdown-menu-notifications">
                  <div class="scrollbar">
                <ul>
                  '.$li_html.'
                </ul>
                  </div>
              </div>';
		
		// echo "<pre>";print_r($data);exit;
		echo $notification_html;exit;
	}
	
	function update_notification(){
		$user_data['notification_id'] = $this->input->post('notification_id');
		$user_data['order_notification_read'] = '1';
		$data = $this->User_info_model->update_notification($user_data);
		return true;
	}

   public function cart_login(){
    $data = array();
      if(!empty($this->input->post())){

        $logEmailid = $this->input->post('logEmailid');
        $logPassword = $this->input->post('logPassword');       

         if(trim($logEmailid) == ''){

             $data['error'] = 'Please enter your email id';
             /* $this->load->view('seventeen/common/header_view');
              $this->load->view('seventeen/login',$data);
              $this->load->view('seventeen/common/footer_view');*/

          }else if(trim($logPassword) == ''){

            $data['error'] = 'Please enter the Password';
             /* $this->load->view('seventeen/common/header_view');
              $this->load->view('seventeen/login',$data);
              $this->load->view('seventeen/common/footer_view');*/

          }else{

           $result = $this->User_info_model->login_user($logEmailid,$logPassword,'website',' ');

            if($result == 1){

                echo 'Loggedin';exit;

             }
           }
         }
       } 
	
	function fb_test(){
		$user_data = $this->input->post('response');
		$test = $this->input->post('test');
		if($test == 'test'){ echo "<pre>";print_r($user_data);exit; }
		if(!empty($user_data)){ echo "<pre>";print_r($user_data);exit; }
		$this->load->view('seventeen/common/header_view');
		$this->load->view('seventeen/test',$object_data);
		$this->load->view('seventeen/common/footer_view');
	}

	function save_fb_token(){
		$user_id = $this->session->userdata('user_id');
		$data = serialize($this->input->post('data'));
		$result = $this->User_info_model->saveupdate_fb_token($data,$user_id);
		return true;
	}

	
}
