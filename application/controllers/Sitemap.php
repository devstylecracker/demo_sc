<?php

Class Sitemap extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('Sitemap_model');
    }

    function sitemap_xml()
    {
		$data['urlslist'] = $this->Sitemap_model->getURLS();
		// $data['ur'] = $this->Sitemap_model->getPosts();
		$data['prod'] = $this->Sitemap_model->getProd();
		$data['sub'] = $this->Sitemap_model->getSubCategory();
		$data['var'] = $this->Sitemap_model->getvarition();
		$data['vars'] = $this->Sitemap_model->getvaritions();
		$data['all_categories'] = $this->Sitemap_model->getallcategories();
		$data['ur'] = array();
		$this->load->view("Sitemap_view",$data);
    }

    function sitemap_html(){
		
		$data['brands'] = $this->Sitemap_model->getURLS();
		$data['products'] = $this->Sitemap_model->getProd();
		// $data['looks'] = $this->Sitemap_model->getPosts();

		$this->seo_title = 'Sitemap - StyleCracker';
		$this->seo_desc = $this->lang->line('seo_desc');
		$this->seo_keyword = $this->lang->line('seo_keyword');

		$this->load->view('seventeen/common/header_view');
		$this->load->view("Sitemap_html_view",$data);
		$this->load->view('seventeen/common/footer_view');

    }
}

?>
