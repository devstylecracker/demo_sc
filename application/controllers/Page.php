<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
       parent::__construct();
       $this->load->model('Schome_model');
   	}
	public function index($offset=NULL)
	{

		$offset = $this->uri->segment(2)!='' && $this->uri->segment(2)>0 ? $this->uri->segment(2) : 1;
		
		$this->seo_title = 'Online Personal Stylists – Your Own 24x7 Fashion Stylist & Wardrobe Consultant | Stylecracker ';
    	$this->seo_desc = 'Stylecracker is India’s 1st Online Personal Styling Platform. Our famous fashion stylists advises an individual on new fashion trends and clothing styles according to your body type. Stylists are available for fashion & styling advice round the clock. You can also shop for curated looks directly from Stylecracker.com. Signup for free!';
    	$this->seo_keyword = 'personal stylist, personal stylist online, fashion stylist, online personal stylist, wardrobe consultant, fashion websites';

    	$homehtml = '';
    	$filter_value = '';
    	$pagination = '';
    	$adjacents = 3;

    	$stylist_offset = ($offset-1);
    	$street_offset = '-1';
    	$blog_offset = ($offset-1);
    	$filter = 0;
    	$prev = ($offset) - 1;
    	$next = ($offset) + 1;	

	    if($filter === 'hot-right-now'){ $filter_value = '3'; }
	    else if($filter === 'looks'){ $filter_value = '1'; }
	    else if($filter === 'street-style'){ $filter_value = '2'; }
	    else if($filter === 'sc-celeb-style'){ $filter_value = '4'; }
	    else if($filter === 'sc-live-posts'){ $filter_value = '5'; }
	    else if($filter === 'festive'){ $filter_value = '6'; }
	    else if($filter === 'bridal'){ $filter_value = '7'; }

    	$looks = $this->Schome_model->get_homepage_looks($stylist_offset,$street_offset,$blog_offset,$filter_value);

    	$length = array(4,8);

    $target_array = array();
    if(!empty($looks['stylist_look'])){
      foreach($length as $i) {
        $target_array['stylist_look'][] = array_splice($looks['stylist_look'], 0, $i);
    }}


     if(!empty($target_array['stylist_look'][0])){ $i = 0;
            foreach($target_array['stylist_look'][0] as $val){
                //$short_url = $this->bitly->shorten(base_url().'looks/look_details/'.$val['slug']);
                $short_url = base_url().'looks/look-details/'.$val['slug'];
        $img_title_alt = str_replace('#','',$val['name']);

                    $homehtml = $homehtml.'<div class="col-md-4 col-sm-6 grid-item">';

                $street_style_class = $val['look_type'] == 2 ? "item-street-style" : "";
                $product_style_class = $val['look_type'] == 3 ? "item-product" : "";
                $celeb_style_class = $val['look_type'] == 4 ? "item-celeb-style" : "";

                $homehtml = $homehtml.'<div class="item '.$street_style_class.' '.$product_style_class.' '.$celeb_style_class.'">';
                $homehtml = $homehtml.'<div class="item-img item-hover">';
                  if($val['look_type'] == 1){
                $homehtml = $homehtml.'<a onclick="_targetClick('.$val['id'].',\'stylist\')" href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_look_image_url').$val['image'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker"  /></a>';
                  $img_url = $this->config->item('sc_look_image_url').$val['image'];
                  }else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
                $homehtml = $homehtml.'<a onclick="_targetClick('.$val['id'].',\'stylist\')" href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_street_style_image_url').$val['look_image'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker"  /></a>';
                  $img_url = $this->config->item('sc_street_style_image_url').$val['look_image'];
                }else if($val['look_type'] == 3){
                $homehtml = $homehtml.'<a onclick="_targetClick('.$val['id'].',\'stylist\')" href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_promotional_look_image').$val['product_img'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker" /></a>';
                  $img_url = $this->config->item('sc_promotional_look_image').$val['product_img'];
                }
                $homehtml = $homehtml.'<div class="item-look-hover">';
                $homehtml = $homehtml.'<a target="_blank" href="'.base_url().'looks/look-details/'.$val['slug'].'" onclick="_targetClick('.$val['id'].',\'stylist\')"><span class="btn btn-primary"> Get This </span></a>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'<div class="item-desc">';
                $homehtml = $homehtml.'<div class="item-title">';
                $homehtml = $homehtml.'<a target="_blank" href="'.base_url().'looks/look-details/'.$val['slug'].'" onclick="_targetClick('.$val['id'].',\'stylist\')" title="'.$val['name'].'">'.$val['name'].'</a>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'<div class="item-media">';
                $homehtml = $homehtml.'<ul class="social-buttons">';
                $homehtml = $homehtml.'<li><a onClick="_targetFacebookShare(\''.$val['name'].'\',\''.base_url().'looks/look-details/'.$val['slug'].'\',\''.$this->config->item('sc_look_image_url').$val['image'].'\',\''.$val['id'].'\');"><i class="fa fa-facebook"></i></a></li>';
                  $homehtml = $homehtml.'<li><a onclick="_targetTweet(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\');"><i class="fa fa-twitter"></i></a></li>';
                  $homehtml = $homehtml.'<li><a onclick="_targetPinterest(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\',\''.$img_url.'\');"><i class="fa fa-pinterest"></i></a></li>';
               
                $homehtml = $homehtml.'</ul>';
                $homehtml = $homehtml.'<div class="user-likes">';
                if($this->session->userdata('user_id')){
                  if($this->get_look_fav($val['id']) == 0){
                    $homehtml = $homehtml.'<a target="_blank" title="Add to Wish List" id="whislist'.$val['id'].'" onclick="add_to_wishlist('.$val['id'].');"><i class="ic sc-love"></i></a>';
                  }else{
                    $homehtml = $homehtml.'<a target="_blank" title="Remove from Wish List" id="whislist'.$val['id'].'" onclick="remove_from_wishlist('.$val['id'].');"><i class="ic sc-love active"></i></a>';
                  }
                }else{
                  $homehtml = $homehtml.'<a target="_blank" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a>';
                }
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';


                $homehtml = $homehtml.'</div>';

                $i++; } }

                if(!empty($looks['blog'])){

                  if(isset($looks['blog']['name'][0])){

                      $blog_date = date_create($looks['blog']['post_date'][0]);
            $blog_posted_date = date_format($blog_date, 'd F,Y');

                      $homehtml = $homehtml.'<div class="col-md-4 col-sm-6 grid-item">';
                    $homehtml = $homehtml.'<div class="item item-blog">';
                      $homehtml = $homehtml.'<div class="item-img item-hover">';
                      $homehtml = $homehtml.'<a href="'.$this->config->item('blog_url').$looks['blog']['post_name'][0].'" target="_blank">';
                        $homehtml = $homehtml.'<img src="'.$looks['blog']['guid'][0].'" title="'.$looks['blog']['name'][0].' - StyleCracker" alt="'.$looks['blog']['name'][0].' - StyleCracker"/></a>';

                       $homehtml = $homehtml.'<div class="item-look-hover">';
                $homehtml = $homehtml.'<a href="'.$this->config->item('blog_url').$looks['blog']['post_name'][0].'" target="_blank"><span class="btn btn-primary"> Read More </span></a>';
                $homehtml = $homehtml.'</div>';
                        $homehtml = $homehtml.'</div>';
                      $homehtml = $homehtml.'<div class="item-desc">';
                      $homehtml = $homehtml.'<div class="item-title">';
                        $homehtml = $homehtml.'<span>SC Live Feature:</span> <a href="'.$this->config->item('blog_url').$looks['blog']['post_name'][0].'" target="_blank">'.$looks['blog']['name'][0].'</a>';
                      $homehtml = $homehtml.'</div>';
                      $homehtml = $homehtml.'<div class="item-media">';
                        $homehtml = $homehtml.'<div class="post-date"><span>Posted: </span>'.$blog_posted_date.'</div>';
                        $homehtml = $homehtml.'<ul class="social-buttons hide">';
                        $homehtml = $homehtml.'<li><a href="#"><i class="fa fa-facebook"></i></a></li>';
                        $homehtml = $homehtml.'<li><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></li>';
                        $homehtml = $homehtml.'<li><a target="_blank" href="#"><i class="fa fa-pinterest"></i></a></li>';
                      //  $homehtml = $homehtml.'<li><a target="_blank" href="#"><i class="fa fa-google-plus"></i></a></li>';
                        $homehtml = $homehtml.'</ul>';
                        $homehtml = $homehtml.'<div class="user-likes">';
                        //$homehtml = $homehtml.'<a target="_blank" href="#"><i class="ic sc-love"></i></a>';
                        $homehtml = $homehtml.'</div>';
                      $homehtml = $homehtml.'</div>';
                      $homehtml = $homehtml.'</div>';
                    $homehtml = $homehtml.'</div>';
                    $homehtml = $homehtml.'</div>';
                   // $homehtml = $homehtml.'</div>';
                    //$homehtml = $homehtml.'</div>';
                  }

                  if(isset($looks['blog']['name'][1])){

                      $blog_date = date_create($looks['blog']['post_date'][1]);
            $blog_posted_date = date_format($blog_date, 'd F,Y');

                      $homehtml = $homehtml.'<div class="col-md-4 col-sm-6 grid-item">';
                    $homehtml = $homehtml.'<div class="item item-blog">';
                      $homehtml = $homehtml.'<div class="item-img item-hover">';
                      $homehtml = $homehtml.'<a href="'.$this->config->item('blog_url').$looks['blog']['post_name'][1].'" target="_blank">';
                        $homehtml = $homehtml.'<img src="'.$looks['blog']['guid'][1].'" title="'.$looks['blog']['name'][1].'- StyleCracker" alt="'.$looks['blog']['name'][1].'- StyleCracker"/></a>';

                       $homehtml = $homehtml.'<div class="item-look-hover">';
                $homehtml = $homehtml.'<a href="'.$this->config->item('blog_url').$looks['blog']['post_name'][1].'" target="_blank"><span class="btn btn-primary"> Read More </span></a>';
                $homehtml = $homehtml.'</div>';
                        $homehtml = $homehtml.'</div>';
                      $homehtml = $homehtml.'<div class="item-desc">';
                      $homehtml = $homehtml.'<div class="item-title">';
                        $homehtml = $homehtml.'<span>SC Live Feature:</span> <a href="'.$this->config->item('blog_url').$looks['blog']['post_name'][1].'" target="_blank">'.$looks['blog']['name'][1].'</a>';
                      $homehtml = $homehtml.'</div>';
                      $homehtml = $homehtml.'<div class="item-media">';
                        $homehtml = $homehtml.'<div class="post-date"><span>Posted: </span>'.$blog_posted_date.'</div>';
                        $homehtml = $homehtml.'<ul class="social-buttons hide">';
                        $homehtml = $homehtml.'<li><a href="#"><i class="fa fa-facebook"></i></a></li>';
                        $homehtml = $homehtml.'<li><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></li>';
                        $homehtml = $homehtml.'<li><a target="_blank" href="#"><i class="fa fa-pinterest"></i></a></li>';
                      //  $homehtml = $homehtml.'<li><a target="_blank" href="#"><i class="fa fa-google-plus"></i></a></li>';
                        $homehtml = $homehtml.'</ul>';
                        $homehtml = $homehtml.'<div class="user-likes">';
                        //$homehtml = $homehtml.'<a target="_blank" href="#"><i class="ic sc-love"></i></a>';
                        $homehtml = $homehtml.'</div>';
                      $homehtml = $homehtml.'</div>'; 
                      $homehtml = $homehtml.'</div>';
                    $homehtml = $homehtml.'</div>';
                    $homehtml = $homehtml.'</div>';
                  //  $homehtml = $homehtml.'</div>';
                    //$homehtml = $homehtml.'</div>';
                  }

                   if(!$this->session->userdata('user_id')){
                      $homehtml = $homehtml.'<div class="col-md-4 col-sm-6 grid-item">';
                      $homehtml = $homehtml.'<div class="item item-blog">';
                      $homehtml = $homehtml.'<div class="item-img item-hover">';
                      $homehtml = $homehtml.'<a onclick="CheckURL();" href="javascript:void(0);" >';
                      $homehtml = $homehtml.'<img src="'.base_url().'assets/SignUpGIF.gif" title="Sign up" alt="Sign Up"/></a>';

                      $homehtml = $homehtml.'<div class="item-look-hover">';
                      $homehtml = $homehtml.'<a href="https://play.google.com/store/apps/details?id=com.stylecracker.android&hl=en" target="_blank"></a>';
                      $homehtml = $homehtml.'</div>';
                      $homehtml = $homehtml.'</div>';
                      $homehtml = $homehtml.'</div>';
                      $homehtml = $homehtml.'</div>';
                     // $homehtml = $homehtml.'</div>';
                      //$homehtml = $homehtml.'</div>';
                    }
                    
              }



              if(!empty($target_array['stylist_look'][1])){ $i = 0;
            foreach($target_array['stylist_look'][1] as $val){
                //$short_url = $this->bitly->shorten(base_url().'looks/look_details/'.$val['slug']);
                $short_url = base_url().'looks/look-details/'.$val['slug'];
        $img_title_alt = str_replace('#','',$val['name']);

                    $homehtml = $homehtml.'<div class="col-md-4 col-sm-6 grid-item">';

                $street_style_class = $val['look_type'] == 2 ? "item-street-style" : "";
                $product_style_class = $val['look_type'] == 3 ? "item-product" : "";
                $celeb_style_class = $val['look_type'] == 4 ? "item-celeb-style" : "";

                $homehtml = $homehtml.'<div class="item '.$street_style_class.' '.$product_style_class.' '.$celeb_style_class.'">';
                $homehtml = $homehtml.'<div class="item-img item-hover">';
                  if($val['look_type'] == 1){
                $homehtml = $homehtml.'<a onclick="_targetClick('.$val['id'].',\'stylist\')" href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_look_image_url').$val['image'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker"  /></a>';
                  $img_url = $this->config->item('sc_look_image_url').$val['image'];
                  }else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
                $homehtml = $homehtml.'<a onclick="_targetClick('.$val['id'].',\'stylist\')" href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_street_style_image_url').$val['look_image'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker"  /></a>';
                  $img_url = $this->config->item('sc_street_style_image_url').$val['look_image'];
                }else if($val['look_type'] == 3){
                $homehtml = $homehtml.'<a onclick="_targetClick('.$val['id'].',\'stylist\')" href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_promotional_look_image').$val['product_img'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker" /></a>';
                  $img_url = $this->config->item('sc_promotional_look_image').$val['product_img'];
                }
                $homehtml = $homehtml.'<div class="item-look-hover">';
                $homehtml = $homehtml.'<a target="_blank" href="'.base_url().'looks/look-details/'.$val['slug'].'" onclick="_targetClick('.$val['id'].',\'stylist\')"><span class="btn btn-primary"> Get This </span></a>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'<div class="item-desc">';
                $homehtml = $homehtml.'<div class="item-title">';
                $homehtml = $homehtml.'<a target="_blank" href="'.base_url().'looks/look-details/'.$val['slug'].'" onclick="_targetClick('.$val['id'].',\'stylist\')" title="'.$val['name'].'">'.$val['name'].'</a>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'<div class="item-media">';
                $homehtml = $homehtml.'<ul class="social-buttons">';
                $homehtml = $homehtml.'<li><a onClick="_targetFacebookShare(\''.$val['name'].'\',\''.base_url().'looks/look-details/'.$val['slug'].'\',\''.$this->config->item('sc_look_image_url').$val['image'].'\',\''.$val['id'].'\');"><i class="fa fa-facebook"></i></a></li>';
                  $homehtml = $homehtml.'<li><a onclick="_targetTweet(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\');"><i class="fa fa-twitter"></i></a></li>';
                  $homehtml = $homehtml.'<li><a onclick="_targetPinterest(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\',\''.$img_url.'\');"><i class="fa fa-pinterest"></i></a></li>';
            
                $homehtml = $homehtml.'</ul>';
                $homehtml = $homehtml.'<div class="user-likes">';
                if($this->session->userdata('user_id')){
                  if($this->get_look_fav($val['id']) == 0){
                    $homehtml = $homehtml.'<a target="_blank" title="Add to Wish List" id="whislist'.$val['id'].'" onclick="add_to_wishlist('.$val['id'].');"><i class="ic sc-love"></i></a>';
                  }else{
                    $homehtml = $homehtml.'<a target="_blank" title="Remove from Wish List" id="whislist'.$val['id'].'" onclick="remove_from_wishlist('.$val['id'].');"><i class="ic sc-love active"></i></a>';
                  }
                }else{
                  $homehtml = $homehtml.'<a target="_blank" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a>';
                }
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';


                $homehtml = $homehtml.'</div>';

                $i++; } }

              $homehtml = $homehtml.'<input type="hidden" name="stylist_count" id="stylist_count" value="'.$this->get_stylist_look_count().'">';
              $homehtml = $homehtml.'<input type="hidden" name="blog_count" id="blog_count" value="'.$this->get_blog_count().'">';
              $homehtml = $homehtml.'<input type="hidden" name="street_count" id="street_count" value="'.$this->get_street_look_count().'">';

              if( isset($looks['offset']['stylist']) && ($looks['offset']['stylist'] * $this->config->item('stylist_plus_promo_look')) < $this->get_stylist_look_count()){
                $homehtml = $homehtml.'<input type="hidden" name="stylist_offset" id="stylist_offset" value="'.$looks['offset']['stylist'].'">';
              }

              if( isset($looks['offset']['blog']) && ($looks['offset']['blog'] * $this->config->item('blog_look')) < $this->get_blog_count()){
                $homehtml = $homehtml.'<input type="hidden" name="blog_offset" id="blog_offset" value="'.$looks['offset']['blog'].'">';
              }

              if( isset($looks['offset']['street']) && ($looks['offset']['street'] * $this->config->item('street_look')) < $this->get_street_look_count()){
                $homehtml = $homehtml.'<input type="hidden" name="street_offset" id="street_offset" value="'.$looks['offset']['street'].'">';
              }


    	$stylist_look_count =  $this->get_stylist_look_count();
    	$blog_count =  $this->get_blog_count();
    	
    	$stylist_count_ = ($stylist_look_count/12);
    	$blog_count_ = ($blog_count/2);

    	$total = $stylist_count_ > $blog_count_ ? $stylist_look_count : $blog_count;

    	$perpage = $stylist_count_ > $blog_count_ ? 12 : 2;

    	$lastpage = ceil($total/$perpage);
    	$lpm1 = $lastpage - 1;

    	//$totalPages = ceil($total / $perpage);

		if($lastpage > 1)
			{	
				$pagination .= "<div class='sc-pagination-wrp'><ul class=\"pagination sc-pagination\">";
				//previous button

				if ($offset != 1) 
					$pagination.= "<li><a href=".base_url()."page/".'1'.">First</a></li>";
				//else
					//$pagination.= "<li><span class=\"disabled\">First</span></li>";	

				if ($offset > 1) 
					$pagination.= "<li><a href=".base_url()."page/".$prev.">previous</a></li>";
				//else
					//$pagination.= "<li><span class=\"disabled\">previous</span></li>";	
				
				
				$limit = $offset + 5;
				if($offset != $lastpage && $offset< $lastpage-5  ){
				for ($counter =  $offset; $counter <= $limit; $counter++)
					{
						if ($counter == $offset)
							$pagination.= "<li><span class=\"current\">$counter</span></li>";
						else
							$pagination.= "<li><a href=".base_url()."page/".$counter.">$counter</a></li>";					
					}
				}else{ 
					for ($counter = $lastpage-5; $counter <= $lastpage; $counter++)
					{
						if ($counter == $offset)
							$pagination.= "<li><span class=\"current\">$counter</span></li>";
						else
							$pagination.= "<li><a href=".base_url()."page/".$counter.">$counter</a></li>";					
					}

				}
				
				//next button
				if ($offset < $counter - 1) 
					$pagination.= "<li><a href=".base_url()."page/".$next.">next </a></li>";
				//else
					//$pagination.= "<li><span class=\"disabled\">next </span></li>";


				if ($offset != $lastpage) 
					$pagination.= "<li><a href=".base_url()."page/".$lastpage.">Last </a></li>";
				//else
					//$pagination.= "<li><span class=\"disabled\">Last </span></li>";

				$pagination.= "</ul></div>\n";		
			}

		$data['schomeview'] =  $homehtml;
		$data['pagination'] =  $pagination;
    	$this->load->view('common/header_view');
    	$this->load->view('common/slider_view');
		$this->load->view('schome_view',$data);
		$this->load->view('common/footer_view');
	}

	function get_stylist_look_count(){
		$stylist_look = $this->Schome_model->get_stylist_look_count();
		return count($stylist_look);
	}

	function get_promotional_look_count(){
		$stylist_look = $this->Schome_model->get_promotional_look_count();
		return count($stylist_look);
	}

	function get_street_look_count(){
		$stylist_look = $this->Schome_model->get_street_look_count();
		return count($stylist_look);
	}

	function get_blog_count(){
		$stylist_look = $this->Schome_model->get_blog_count();
		return count($stylist_look);
	}
}
