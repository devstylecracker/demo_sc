<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sc404 extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
			$this->load->view('seventeen/common/header_view');
			$this->load->view('seventeen/errors/error_404');
			$this->load->view('seventeen/common/footer_view');

    }
}
?>
