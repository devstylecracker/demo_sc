<?php
ini_set('max_execution_time', 30000000000);
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('memory_limit','512M');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';


/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class SCProductinventory extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('image_lib');
        $this->load->model('SCProductinventory_model');  
        $this->load->model('Scproductinventorycron_model');         
    }

    function index(){
    	echo 'Index SCProductinventory';
    	exit;
    }

    function uploadproducts_post(){
        
        $str = $this->post(); 
        $str['api_key'] = $str['scapi_key'];
        $str['secret_key'] = $str['scsecret_key'];

		$this->bulk_uploadproducts_get($str);
        exit;
        $error['msg'] = '200 OK';
        $a = 0; $u = 0; $e = 0;
        $allowed_ext =  array('gif','png' ,'jpg','jpeg','GIF','PNG' ,'JPG','JPEG');
        $products = array();
        if (array_key_exists('scapi_key', $str)) {
            $api_key = trim($str['scapi_key']);
        }
        
        if (array_key_exists('scsecret_key', $str)) {
            $secret_key = trim($str['scsecret_key']);
        }

        if (array_key_exists('products', $str)) { 
            $allproducts = trim($str['products']); 
            $products = json_decode($allproducts); 
        }
              
        
        if($api_key==''){
            $error['msg'] = 'API key is missing';
        }else if($secret_key==''){
            $error['msg'] = 'Secret Key is missing';
        }else if(empty($products)){
            $error['msg'] = 'Products does not exist';
        }else if($api_key!='' && $secret_key!=''){
           	$brand_id = $this->SCProductinventory_model->get_brand_id($this->encryptOrDecrypt($api_key,''),$this->encryptOrDecrypt($secret_key,''));
	            if($brand_id == 0){
	            	$error['msg'] = 'Invalid API Key and Secret Key';
	            }
        }
       
        if(!empty($products) && $brand_id>0){  
        	$brand_code = $this->SCProductinventory_model->getbrand_code($brand_id);
            foreach($products as $value){
                $data = array();
                if (isset($value->product_sku)) { $product_sku = $brand_code.strip_tags($value->product_sku); } else{ $product_sku =''; }
                if (isset($value->product_category)) { $product_category = strip_tags($value->product_category); } else{ $product_category =''; }
                if (isset($value->product_subcategory)) { $product_subcategory = strip_tags($value->product_subcategory); } else{ $product_subcategory =''; }
               	if (isset($value->product_name)) { $product_name = strip_tags($value->product_name); } else{ $product_name =''; }
                if (isset($value->product_url)) { $product_url = strip_tags($value->product_url); } else{ $product_url =''; }
                if (isset($value->product_description)) { $product_description = strip_tags($value->product_description); } else{ $product_description =''; }
                if (isset($value->price)) { $price = strip_tags($value->price); } else{ $price =''; }
                if (isset($value->age_group)) { $age_group = strip_tags($value->age_group); } else{ $age_group =''; }
                if (isset($value->product_discount)) { $product_discount = strip_tags($value->product_discount); } else{ $product_discount =''; }
                if (isset($value->discount_start_date)) { $discount_start_date = strip_tags($value->discount_start_date); } else{ $discount_start_date =''; }
                if (isset($value->discount_end_date)) { $discount_end_date = strip_tags($value->discount_end_date); } else{ $discount_end_date =''; }  
                if(!empty($value->product_img)) { $product_imges = $value->product_img; }else{ $product_imges = array(); }
                if(!empty($value->product_stock)) { $product_stock = $value->product_stock; }else{ $product_stock = array(); }
                if(!empty($value->product_tags)) { $product_tags = $value->product_tags; }else{ $product_tags = array(); }
                
                if($product_name!='' && $price>0 && $product_sku!='' && $product_url!='' && !empty($product_imges)){

	                $product_id = $this->SCProductinventory_model->get_product_id($product_sku,$product_url);

	                $data['product_desc']['product_sku'] = $product_sku;
	                $data['product_desc']['product_cat_id'] = $this->get_categoryID($product_category);
	                $data['product_desc']['product_sub_cat_id'] = $this->get_subcategoryID($product_subcategory);
	                $data['product_desc']['brand_id'] = $brand_id;
	                $data['product_desc']['name'] = $product_name;
	                $data['product_desc']['url'] = $product_url;
	                $data['product_desc']['description'] = $product_description;
	                
	                $data['product_desc']['price'] = $price;
	                $data['product_desc']['price_range'] = $this->get_pricerangeID($price);
	                $data['product_desc']['created_by'] = $brand_id;
	                $data['product_desc']['created_datetime'] = date('Y-m-d H:i:s');
	                $data['product_desc']['product_discount'] = $product_discount;
	                $data['product_desc']['discount_start_date'] = $discount_start_date;
	                $data['product_desc']['discount_end_date'] = $discount_end_date;
	                $data['product_desc']['status'] = 1;
	                $data['product_desc']['is_promotional'] = 0;
	                $data['product_desc']['approve_reject'] = 'A';
	                $data['product_desc']['is_delete'] = 0;

	                /* Update Product information*/

	                $data['product_udesc']['price'] = $price;
	                $data['product_udesc']['price_range'] = $this->get_pricerangeID($price);
	                $data['product_udesc']['product_discount'] = $product_discount;
	                $data['product_udesc']['discount_start_date'] = $discount_start_date;
	                $data['product_udesc']['discount_end_date'] = $discount_end_date;

	                if(!empty($product_imges)) { $img_inc = 0;
	                	foreach($product_imges as $pimg){
	                		
	                		
	                		$imagename = basename($pimg);  
	                                                     
	                        $ext = pathinfo($imagename, PATHINFO_EXTENSION);
							
							if(!in_array($ext,$allowed_ext)){ $ok = 0;  }else{
	                                $newimagename = time().mt_rand(0,10000).'.'.$ext; 
	                                if($img_inc == 0 &&  $product_id==0){ 
	                                	$data['product_desc']['image'] =  $newimagename; 
	                                	$this->saveProductImage($pimg,$newimagename);

	                                }else{
	                					$data['product_extra_images'][] =  $newimagename; 
	                					$this->saveProductExtraImages($pimg,$newimagename);
	                				}
	                               	//$data['product_desc']['image'] =$newimagename;
	                               
	                        }


	                		$img_inc++;
	                	}
	                }

	                if(!empty($product_stock) && !empty($product_stock[0])) { $s = 0;
	                	foreach($product_stock[0] as $size => $stock){
	                			
	                			//$data['product_stock']['size'][] = $size; 
	                			$data['product_stock']['size'][$s] = $size; 
	                			$data['product_stock']['stock'][$s] = $stock; 
	                			$s++;
	                		}
	                }
	                
	                if(!empty($product_tags)) {
	                	foreach($product_tags as $tags){
	                			$data['product_tags'][] = $tags; 
	                		}
	                }

	                
	                if($product_id == 0 ){
	                    // Insert the product
	                    $data['product_desc']['slug'] = str_replace(' ','-',strtolower($product_name)).'-'.time().mt_rand();
	                	$this->SCProductinventory_model->add_product($data);
	                	$a++;
	                }else{
	                    // update the product
	                    $data['product_id'] = $product_id;
	                    $this->SCProductinventory_model->update_product($data);
	                    $u++;
	                }

	            }else{
	            	
	            	$e++;
	            }

            }
        }
      
        $error['success'] = "Successfully added ".$a;
        $error['updated'] = "Successfully updated ".$u;
        if($e>0){ $error['error'] = "Errors found ".$e; }
        echo json_encode($error);
    }

    public function get_categoryID($category_id){
    	if($category_id!=''){
    		return $this->SCProductinventory_model->get_categoryID($category_id);
    	}else{
    		return 1;
    	}
    }

    public function get_subcategoryID($category_id){
    	if($category_id!=''){
    		return $this->SCProductinventory_model->get_subcategoryID($category_id);
    	}else{
    		return 1;
    	}
    }

    public function get_pricerangeID($price){
    	if($price > 50000){ return 4670; }
    	else if($price < 50000 && $price > 15000){ return 38; }
    	else if($price < 15000 && $price > 5000){ return 37; }
    	else { return 36; }
    }


    public function saveProductImage($pimg,$newimagename){
    	 if(file_exists($pimg))
		        {
		            $ok =0;
		        }else{
		            
		            file_put_contents('./sc_admin/assets/products/'.$newimagename, file_get_contents($pimg));
		            $config['image_library'] = 'gd2';
		            $config['source_image'] = './sc_admin/assets/products/'.$newimagename;
		            $config['new_image'] = './sc_admin/assets/products/thumb/';
		            $config['create_thumb'] = TRUE;
		            $config['thumb_marker'] = '';
		            $config['maintain_ratio'] = TRUE;
		            $config['width'] = 200;
		            $config['height'] = 200;

		            $this->image_lib->initialize($config);

		                if (!$this->image_lib->resize()){
		                        $this->image_lib->display_errors();
		                        $ok =0;
							}else{
								$ok =1;
		                        }
		        }

		           /* 310 thumb creation*/
		            $config['image_library'] = 'gd2';
		            $config['source_image'] = './sc_admin/assets/products/'.$newimagename;
		            $config['new_image'] = './sc_admin/assets/products/thumb_310/';
		            $config['create_thumb'] = TRUE;
		            $config['thumb_marker'] = '';
		            $config['maintain_ratio'] = TRUE;
		            $config['width'] = 310;
		            //$config['height'] = 200;

		            $this->image_lib->initialize($config);

		                if (!$this->image_lib->resize()){
		                        $this->image_lib->display_errors();
		                        $ok =0;
							}else{
								$ok =1;
		            }

		               /* 310 thumb creation*/
		            $config['image_library'] = 'gd2';
		            $config['source_image'] = './sc_admin/assets/products/'.$newimagename;
		            $config['new_image'] = './sc_admin/assets/products/thumb_160/';
		            $config['create_thumb'] = TRUE;
		            $config['thumb_marker'] = '';
		            $config['maintain_ratio'] = TRUE;
		            $config['width'] = 310;
		            //$config['height'] = 200;

		            $this->image_lib->initialize($config);

		                if (!$this->image_lib->resize()){
		                        $this->image_lib->display_errors();
		                        $ok =0;
							}else{
								$ok =1;
		            }
    }

    public function saveProductExtraImages($pimg,$newimagename){
    	 if(file_exists($pimg))
		        {
		            $ok =0;
		        }else{
		            
		            file_put_contents('./sc_admin/assets/products_extra/'.$newimagename, file_get_contents($pimg));
		            $config['image_library'] = 'gd2';
		            $config['source_image'] = './sc_admin/assets/products_extra/'.$newimagename;
		            $config['new_image'] = './sc_admin/assets/products_extra/thumb/';
		            $config['create_thumb'] = TRUE;
		            $config['thumb_marker'] = '';
		            $config['maintain_ratio'] = TRUE;
		            $config['width'] = 200;
		            $config['height'] = 200;

		            $this->image_lib->initialize($config);

		                if (!$this->image_lib->resize()){
		                        $this->image_lib->display_errors();
		                        $ok =0;
							}else{
								$ok =1;
		                        }

		            /* 160 thumb creation*/

		            $config['image_library'] = 'gd2';
		            $config['source_image'] = './sc_admin/assets/products_extra/'.$newimagename;
		            $config['new_image'] = './sc_admin/assets/products_extra/thumb_160/';
		            $config['create_thumb'] = TRUE;
		            $config['thumb_marker'] = '';
		            $config['maintain_ratio'] = TRUE;
		            $config['width'] = 160;
		            //$config['height'] = 200;

		            $this->image_lib->initialize($config);

		                if (!$this->image_lib->resize()){
		                        $this->image_lib->display_errors();
		                        $ok =0;
							}else{
								$ok =1;
		            }

		            /* 310 thumb creation*/
		            $config['image_library'] = 'gd2';
		            $config['source_image'] = './sc_admin/assets/products_extra/'.$newimagename;
		            $config['new_image'] = './sc_admin/assets/products_extra/thumb_310/';
		            $config['create_thumb'] = TRUE;
		            $config['thumb_marker'] = '';
		            $config['maintain_ratio'] = TRUE;
		            $config['width'] = 310;
		            //$config['height'] = 200;

		            $this->image_lib->initialize($config);

		                if (!$this->image_lib->resize()){
		                        $this->image_lib->display_errors();
		                        $ok =0;
							}else{
								$ok =1;
		            }
		        }
    }

    public function encryptOrDecrypt($mprhase, $crypt) {
         $MASTERKEY = "STYLECRACKERAPI";
         $td = @mcrypt_module_open('tripledes', '', 'ecb', '');
         $iv = @mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
         @mcrypt_generic_init($td, $MASTERKEY, $iv);
         if ($crypt == 'encrypt')
         {
             $return_value = @base64_encode(mcrypt_generic($td, $mprhase));
         }
         else
         {
             $return_value = @mdecrypt_generic($td, base64_decode($mprhase));
         }
         @mcrypt_generic_deinit($td);
         @mcrypt_module_close($td);
         return $return_value;
    } 

    function bulk_uploadproducts_get($inputs){
    	//echo "------------------------start";
    	if(isset($inputs['url']) && $inputs['url'] != "")
    		$product_json =file_get_contents($inputs['url'])or die("Error reading file $path");
    	else if(isset($inputs['products']))
    		$product_json = $inputs['products'];
        $api_key = $inputs['api_key'];
		$secret_key = $inputs['secret_key'];

		$error['msg'] = '200 OK';
        $a = 0; $u = 0; $e = 0;
        $allowed_ext =  array('gif','png' ,'jpg','jpeg','GIF','PNG' ,'JPG','JPEG');
        $products = array();
       /* if (array_key_exists('scapi_key', $str)) {
            $api_key = trim($str['scapi_key']);
        }
        
        if (array_key_exists('scsecret_key', $str)) {
            $secret_key = trim($str['scsecret_key']);
        }

        if (array_key_exists('products', $str)) { 
            $allproducts = trim($str['products']); 
            $products = json_decode($allproducts); 
        }*/

        if(isset($product_json)){
        	$allproducts = trim($product_json); 
            $products = json_decode($product_json); //print_r($products);exit;
        }

        if($api_key==''){
            $error['msg'] = 'API key is missing';
        }else if($secret_key==''){
            $error['msg'] = 'Secret Key is missing';
        }else if(empty($products)){
            $error['msg'] = 'Products does not exist';
        }else if($api_key!='' && $secret_key!=''){
           	$brand_id = $this->Scproductinventorycron_model->get_brand_id($this->encryptOrDecrypt($api_key,''),$this->encryptOrDecrypt($secret_key,''));
	            if($brand_id == 0){
	            	$error['msg'] = 'Invalid API Key and Secret Key';
	            }
        }
        
       	$data = array();
        if(!empty($products) && $brand_id>0){  
        	$brand_code = $this->Scproductinventorycron_model->getbrand_code($brand_id);
        	$cnt_prod=0;
        	$cnt_tags=0;
        	$cnt_prod_update=0;
        	$last_product_img=(string)"NO";
            foreach($products as $value){
                if (isset($value->product_sku)) { $product_sku = strip_tags($value->product_sku); } else{ $product_sku =''; }
                if (isset($value->product_color)) { $product_color = strip_tags($value->product_color); } else{ $product_color ='default'; }
                if (isset($value->product_category)) { $product_category = strip_tags($value->product_category); } else{ $product_category =''; }
                if (isset($value->product_subcategory)) { $product_subcategory = strip_tags($value->product_subcategory); } else{ $product_subcategory =''; }
               	if (isset($value->product_name)) { $product_name = strip_tags($value->product_name); } else{ $product_name =''; }
                if (isset($value->product_url)) { $product_url = strip_tags($value->product_url); } else{ $product_url =''; }
                if (isset($value->product_description)) { $product_description = strip_tags($value->product_description); } else{ $product_description =''; }
                if (isset($value->price)) { $price = (float)strip_tags($value->price); } else{ $price =''; }
                if (isset($value->age_group)) { $age_group = strip_tags($value->age_group); } else{ $age_group =''; }
                if (isset($value->product_discount)) { $product_discount = strip_tags($value->product_discount); } else{ $product_discount =''; }
                if (isset($value->discount_start_date)) { $discount_start_date = strip_tags($value->discount_start_date); } else{ $discount_start_date =''; }
                if (isset($value->discount_end_date)) { $discount_end_date = strip_tags($value->discount_end_date); } else{ $discount_end_date =''; }  
                if(!empty($value->product_img)) { $product_imges = $value->product_img; }else{ $product_imges = array(); }
                if(!empty($value->product_stock)) { $product_stock = $value->product_stock; }else{ $product_stock = array(); }
                if(!empty($value->product_tags)) { $product_tags = $value->product_tags; }else{ $product_tags = array(); }
                $product_imges = array_filter($product_imges);

                if($product_name!='' && $price>0 && $product_sku!='' && $product_url!='' && !empty($product_imges)){
                	$price = ceil($price);
                	$this->db->query("update product_desc set product_color='$product_color' where (product_sku = '$product_sku' or substring(product_sku,6) = '$product_sku') and product_sku != '';");

	                $prod_exist = $this->Scproductinventorycron_model->get_product_id_color($product_url, $product_color);//get_product_id_sku($product_sku,$product_url);//
	                echo ". ";
	                $product_sku_exi = $prod_exist[0]['product_sku'];
	                $product_id = $prod_exist[0]['id'];
	                
	                
	                $stk['product_id'] = addslashes($product_id);
	                $pro['product_sku'] = addslashes($product_sku);
	                $pro['product_cat_id'] = $this->get_categoryID(addslashes($product_category));
	                $pro['product_sub_cat_id'] = $this->get_subcategoryID(addslashes($product_subcategory));
	                $pro['brand_id'] = addslashes($brand_id);
	                $pro['name'] = addslashes($product_name);
	                $pro['url'] = addslashes($product_url);
	                $pro['description'] = addslashes($product_description);
	                $pro['product_color'] = addslashes($product_color);
	                
	                $pro['price'] = addslashes($price);
	                $pro['price_range'] = addslashes($this->get_pricerangeID($price));
	                $pro['created_by'] = addslashes($brand_id);
	                $pro['created_datetime'] = date('Y-m-d H:i:s');
	                $pro['product_discount'] = addslashes($product_discount);
	                $pro['discount_start_date'] = addslashes($discount_start_date);
	                $pro['discount_end_date'] = addslashes($discount_end_date);
	                $pro['status'] = 1;
	                $pro['is_promotional'] = 0;
	                $pro['approve_reject'] = 'P';
	                $pro['is_delete'] = 0;

	                /* Update Product information*/

	                $desc['id'] = addslashes($product_id);
	                $desc['price'] = addslashes($price);
	             	$desc['product_sku'] = $product_sku;
	             	//$desc['product_color'] = $product_color;
	            	$desc['price_range'] = addslashes($this->get_pricerangeID($price));
	                $desc['product_discount'] = addslashes($product_discount);
	                $desc['discount_start_date'] = addslashes($discount_start_date);
	                $desc['discount_end_date'] = addslashes($discount_end_date);
					$desc['created_by'] = addslashes($brand_id);

					if($product_id == 0){
						$cnt_prod++;
					}else{
						$cnt_prod_update++;
					}
					$new_product_img = (string)$product_url.$product_color;
					if(!empty($product_imges) && $product_id == 0 && $last_product_img !== $new_product_img ) {
	                 	$img_inc = 0;
	                 	$last_product_img = (string)$new_product_img;
	                 	$product_imges = array_filter($product_imges);
	                	foreach($product_imges as $pimg){
							$imagename = basename($pimg);  
	                                                     
	                        $ext = pathinfo($imagename, PATHINFO_EXTENSION);
							
							if(!in_array($ext,$allowed_ext)){ $ok = 0;  }else{
	                                $newimagename = time().mt_rand(0,10000).'.'.$ext; 
	                                if($img_inc == 0 &&  $product_id==0){ 
	                                	$pro['image'] =  $newimagename; 
	                                	$this->saveProductImage($pimg,$newimagename);

	                                }else{
	                                	if(!isset($data_add['sql_extra_img'])){
	                                		$data_add['sql_extra_img'] = "INSERT INTO `extra_images` (`created_datetime`, `product_id`, `product_images`) VALUES ";
	                                	}
	                					$image_info =  $newimagename; 
	                					$data_add['sql_extra_img'] .= "('".date('Y-m-d H:i:s')."', (select id from product_desc where url = '$product_url' and product_color='$product_color' limit 1), '$image_info'), ";
	                					$this->saveProductExtraImages($pimg,$newimagename);
	                				}
	                               
	                        }


	                		$img_inc++;
	                	}
	                }
	                

	                if(!empty($product_stock) && !empty($product_stock[0])) { 
	                	$s = 0;
	                	$product_stock[0] = (array)$product_stock[0];
	                	$product_stock[0] = array_filter($product_stock[0]);
	                	foreach($product_stock[0] as $size => $stock){
	                			$size = addslashes($size);
	                			$stock = addslashes($stock);
	                			if($product_id != 0){
	                				$stk['product_id'] = $product_id; 
	                				if(!isset($data['sql_stock_inventory_update'])){
	                            		$data['sql_stock_inventory_update'] = "INSERT INTO `product_inventory` (`product_id`, `product_sku`, `size_id`, `stock_count`) VALUES ";
	                            	}
	            					$data['sql_stock_inventory_update'] .= "($product_id,'$product_sku',add_size('$size'),'$stock'), ";
	            				}
	                			else{
	                				$subquery = $stk['product_id'] = "(select id from product_desc where url = '$product_url' and product_color='$product_color' limit 1)";
	                				if(!isset($data_add['sql_stock_inventory_add'])){
                            			$data_add['sql_stock_inventory_add'] = "INSERT INTO `product_inventory` (`product_id`, `product_sku`, `size_id`, `stock_count`) VALUES ";
	                            	}
	            					$data_add['sql_stock_inventory_add'] .= "($subquery,'$product_sku',add_size('$size'),'$stock'), ";

								}
								$delete['product_id'] = $product_id;
	                			
	                			$stk['size_id'] = $size; 
	                			$stk['stock_count'] = $stock; 
	                			$stk['product_sku'] = $product_sku;
	                			$stock_data['delete'][] = $product_id;

	                			$s++;
	                		}
	                }
	                
	                if(!empty($product_tags)) {
	                	$product_tags = array_filter($product_tags);

	                	foreach($product_tags as $tags){
	                			$tags = addslashes($tags);
	                			$subquery =  "(select id from product_desc where url = '$product_url' and product_color='$product_color'  limit 1)";
								$create_date = date('Y-m-d H:i:s');
								$tag_id = "add_tags('".strtolower(trim($tags))."', '".date('Y-m-d H:i:s')."', '".str_replace(' ','-',strtolower($tags))."-".time().mt_rand()."')";
								
								
								if($product_id != 0){
	                				if(!isset($data['sql_product_tag_update'])){
	                            		$data['sql_product_tag_update'] = "INSERT INTO `product_tag` (`created_by`, `created_datetime`, `product_id`, `status`, `tag_id`) VALUES ";
	                            	}
	            					$data['sql_product_tag_update'] .= "('$brand_id','$create_date',$subquery,1,$tag_id), ";
	            					
								}
	                			else{
	                				if(!isset($sql_product_tag_add)){
	                            		$sql_product_tag_add = "INSERT INTO `product_tag` (`created_by`, `created_datetime`, `product_id`, `status`, `tag_id`) VALUES ";
	                            	}
	            					$sql_product_tag_add .= "('$brand_id','$create_date',$subquery,1,$tag_id), ";

	            					$cnt_tags++;
	            					if($cnt_tags==200){
	            						$cnt_tags=0;
	            						$data_add['sql_product_tag_add'][] = $sql_product_tag_add;
	            						unset($sql_product_tag_add);
	            					}

	                			}
	                		}
	                }
	                
	                if($product_id == 0 ){
	                	
	                    $pro['slug'] = str_replace(' ','-',strtolower($product_name)).'-'.time().mt_rand();
	                	$data_add['product_desc_add']['data'][$product_url.$product_color] = $pro;
	                    
	                	if($cnt_prod >= 600){
	                		$cnt_prod=0;
	                		if(isset($sql_product_tag_add)){
	                			$data_add['sql_product_tag_add'][] = $sql_product_tag_add;
	                			unset($sql_product_tag_add);
	                		}
	                		$this->Scproductinventorycron_model->add_product_bulk($data_add);
							unset($data_add);
	                	}
	                		
	                	$a++;
	                }else{
	                	$data['product_desc_update']['product_udesc'][] = $desc;
	                	foreach($stock_data['delete'] as $row){
		                	$data['product_desc_update']['product_stock']['delete'][] = $row;
		                }
	                	if($cnt_prod_update >= 600){
        					$this->Scproductinventorycron_model->update_product_bulk($data);
        					unset($data);
        					$cnt_prod_update=0;
        				}
	        			


	                    $u++;
	                }
	                unset($stock_data);
	            }else{
	            	$e++;
	            }
            }
        }

        if(isset($sql_product_tag_add))
        	$data_add['sql_product_tag_add'][] = $sql_product_tag_add;
	    
        if(isset($data_add))
        	$this->Scproductinventorycron_model->add_product_bulk($data_add);

        if(isset($data))
        	$this->Scproductinventorycron_model->update_product_bulk($data);


        $error['success'] = "Successfully added ".$a;
        $error['updated'] = "Successfully updated ".$u;
        if($e>0){ $error['error'] = "Errors found ".$e; }
        echo json_encode($error);
    }

}
?>