<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scshop extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Schome_model');		
		$this->load->model('home_model');		
		//$this->open_methods = array('shopify_name_get','shopify_view_product_get'); 
		$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));   
	}	


	function index(){
      $data = array();
      $filter_type = strtolower(trim($this->uri->segment(2)));
      if($filter_type == ''){ $filter_selection = 'all'; }
      else if($filter_type == 'men'){ $filter_selection = 'men'; }
      else if($filter_type == 'women'){ $filter_selection = 'women'; }

      $ids = "29062,29673,25314,25032,25280,27992,29200,30267,25455,30049,25454,25202,23396,23744,23867,25221,31465,30358,29107,30124,29775,21025,28545,25456,29717,27949,28856,24700,22183,25168,30868,24156,27947,23861,28347,23407,30733,20963,28793,28363,24929,28188,20512,30984";
      $data['page_filter_name'] = $filter_type;

      if ( $this->cache->get('brand_filter_type') !== $filter_type ) {
        $data['top_brands'] = $this->home_model->brand_list_from_ids($ids);//brand_list($filter_type);
        $this->cache->save('brand_filter_data', $data, 500);
        $this->cache->save('brand_filter_type', $filter_type, 500);
      }
      if($this->cache->get('brand_filter_data')){
        $data = $this->cache->get('brand_filter_data');
      }
      $this->load->view('common/header_view');
      $this->load->view('brand_list_scshop',$data);
      $this->load->view('common/footer_view');
    }
	
 }