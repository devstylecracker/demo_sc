<?php
if (!defined('BASEPATH'))
exit('No direct script access allowed');
 
//include the facebook.php from libraries directory
require_once APPPATH . 'libraries/facebook.php';
require_once APPPATH . 'controllers/Schome_new.php';
class Fb extends Schome_new {
 
public function __construct() {
parent::__construct();
$this->config->load('facebook');
//$this->library->load('facebook');
}
 
public function index() {
  
}
 
 public function via_facebook_signin(){
  $response = $this->input->post('response');
  $json_array = json_decode($response, true);
  $responseDecode = json_decode($response);
  $scpassword = $this->randomPassword();
  $ip = $this->input->ip_address();
  if(isset($responseDecode->first_name)) $firstName = $responseDecode->first_name; else $firstName = '';
  if(isset($responseDecode->last_name)) $lastName = $responseDecode->last_name; else $lastName = '';
  if(isset($responseDecode->birthday)) $birthday = $responseDecode->birthday; else $birthday = '';
  $scusername = $firstName.$lastName;
  @$json_array['firstname'] = $json_array['first_name'];
  @$json_array['lastname'] = $json_array['last_name'];
  @$json_array['birthday'] = $json_array['birthday'];
  @$json_array['profile_pic']   = $json_array['picture']['data']['url'];
  if(isset($responseDecode->email)) $scemail_id = $responseDecode->email; else $scemail_id = '';
  if($scemail_id == '') $result = 'Email not verified in Facebook';
  else {
    $gender=1;
    if($responseDecode->gender == 'male') 
      $gender=2;
    $result = $this->signup('facebook',$scusername,$scemail_id,$scpassword,$ip,2,$gender,$json_array);
  }
  echo $result ;
  
 }



/*nOT USING FUNCTIONS*/ 
public function logout() {
$signed_request_cookie = 'fbsr_' . $this->config->item('appID');
setcookie($signed_request_cookie, '', time() - 3600, "/");
$this->session->sess_destroy();  //session destroy
//redirect('/fb/index', 'refresh');  //redirect to the home page
}
 
public function fblogin() {
 
$facebook = new Facebook(array(
'appId' => $this->config->item('appID'),
'secret' => $this->config->item('appSecret'),
));


// We may or may not have this data based on whether the user is logged in.
// If we have a $user id here, it means we know the user is logged into
// Facebook, but we don't know if the access token is valid. An access
// token is invalid if the user logged out of Facebook.
$user = $facebook->getUser(); // Get the facebook user id
$profile = NULL;
$logout = NULL;
 
if ($user) {
try {
$profile = $facebook->api('/me');  //Get the facebook user profile data
$access_token = $facebook->getAccessToken();
$params = array('next' => base_url('fb/logout/'), 'access_token' => $access_token);
$logout = $facebook->getLogoutUrl($params);
 
} catch (FacebookApiException $e) {
error_log($e);
$user = NULL;
}
 
$data['user_id'] = $user;
$data['name'] = $profile['name'];
$data['logout'] = $logout;
$this->session->set_userdata($data);
redirect('/fb/test');
}
}
 
public function test() {
$this->load->view('test');
}
 
}
 
/* End of file fb.php */
/* Location: ./application/controllers/fb.php */