<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
       parent::__construct();
       $this->load->model('Schome_model');
       $this->load->model('Search_model');
   	}

	public function index($offset=null,$offset2=null)
	{
		$data = array();
		$data['search_data'] = urldecode($offset);
		if($offset2){$data['search_data'] .= '/'.urldecode($offset2);}
		$data['search_count'] =  $looks = $this->Schome_model->get_search($data['search_data']);

		$this->load->view('common/header_view');
		$this->load->view('search',$data);
		$this->load->view('common/footer_view');
	}

  	
	public function autosearch(){
		$search = $this->input->get('query');
		$res =  $this->Search_model->autosearch($search);
		print $res;

	}
}
