<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cpatesting extends MY_Controller {

	function __construct(){
       parent::__construct();
       $this->load->model('Cpa_model');       
   	}

public function shopify_view_product(){
		$sc_product_id = "7029";
		$data['product_desc'] = $this->Cpa_model->get_product_details($sc_product_id);
		$this->load->view('cpa_view', $data);		
	}
}