<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sc_orders extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Favorites_model');
		$this->load->model('Mcart_model_android');
		$this->load->model('cartnew_model');
		$this->load->library('email');
	}
	public function order_history(){
		if(empty($this->session->userdata('user_id')))
		{
			$this->session->set_flashdata('order_login_msg', 'Please login to access your Order History.');
			redirect('login');
			exit();
		}
		$user_id = $this->session->userdata('user_id');
		$limit = '';$offset = '';$metaValueData = array();
		$order_list = $this->Mcart_model_android->get_order_list($user_id,$limit,$offset);
		$data['order_list'] = $order_list;
		$objectId 		= $this->Mcart_model_android->get_object('box_order_products')[0]['object_id'];
		//echo '<pre>';print_r($data['order_list']);echo '</pre>';;
		
		$i = 0;
		if(!empty($data['order_list']))
		{
			foreach($data['order_list'] as $value)
			{
				$metaValueData = array();
				$mkey = $value['order_display_no'];
				//echo '<pre>';print_r($value);//exit;
				
				if(@$value['product_info'][0]->order_status_id>=4 && @$value['product_info'][0]->order_status_id!=8)
				{
					$metaValue = $this->Mcart_model_android->getSereializeProductDetail($mkey,'','',$objectId);					
					$bjectMetaValue  = unserialize($metaValue['object_meta_value']);					
					if(is_array($bjectMetaValue) && $bjectMetaValue!='' && count($bjectMetaValue)>0)
					{
						$j=0;
						/* echo "<pre>";
						print_r($bjectMetaValue);
						echo "</pre>"; */
						foreach($bjectMetaValue as $key => $metaId)
						{
							if(@$metaId['is_delete']==0)
							{
								$metaValueData[$j] = $this->Mcart_model_android->getMetaValueData($key);
								$metaValueData[$j]['status'] = $metaId['status'];
								$j++;
							}
							
							
						}
					}			
				}
				$data['order_list'][$i]['scbox_prd_data'] 	= $metaValueData;			
				$i++;
			}					
		}		
		
		//exit();
		$data['scbox_prd_list'] = '';		
		$this->load->view('seventeen/common/header_view');
		$this->load->view('seventeen/order_history',$data);
		$this->load->view('seventeen/common/footer_view');
	}

	function updateUserResponse()
	{
		$save_data = array();
	    $product_id = $this->input->post('product_id');
	    $order_id = $this->input->post('order_id');
	    $response_type = $this->input->post('response_type');
	    $user_response = $this->input->post('user_response');
        $order_unique_no = $this->input->post('order_unique_no');
        $user_recomment = $this->input->post('user_recomment');
        
	    if($product_id!='' && $order_id!='' && $response_type!='' && $user_response!='' && $order_unique_no!='')
	    {
		    $objectId = $this->Mcart_model_android->get_object('box_order_products')[0]['object_id'];
		    $metaValue = $this->Mcart_model_android->getSereializeProductDetail($order_id,'','',$objectId);
			$ObjectMetaValue  = unserialize($metaValue['object_meta_value']);
			$ObjectMetaValue[$product_id]['status'] = $response_type;	
			$ObjectMetaValue[$product_id]['user_response'] = $user_response;
			$ObjectMetaValue[$product_id]['user_recomment'] = $user_recomment;
			
		
			$save_data['object_meta_value'] = serialize($ObjectMetaValue);			
			$res = $this->Mcart_model_android->updateObjectMeta($save_data,$objectId,$order_id);
			if($res)
			{
				$email_response = $this->orderReturnExchangeEmail($order_id,$order_unique_no,$product_id);
				echo 'success';exit;
			}
		}
	}

	function orderReturnExchangeEmail($order_display_no,$order_unique_no,$product_id){   
	/*function orderReturnExchangeEmail(){   	
		$order_display_no = 'SC1508133359';
		$order_unique_no = '24958_1905';
		$product_id = '90638';*/
		$emailid = 'dev@stylecracker.com';
		$objectId = $this->Mcart_model_android->get_object('box_order_products')[0]['object_id'];
		$metaValue = $this->Mcart_model_android->getSereializeProductDetail($order_display_no,'','',$objectId);
		$ObjectMetaValue  = unserialize($metaValue['object_meta_value']);
		$scbox['product_data']['size'] = $ObjectMetaValue[$product_id]['size'];
		$scbox['product_data']['user_status'] = $ObjectMetaValue[$product_id]['status'];
		$scbox['product_data']['is_delete'] = $ObjectMetaValue[$product_id]['is_delete'];
		$scbox['product_data']['user_response'] = $ObjectMetaValue[$product_id]['user_response'];
		$scbox['product_data']['sku'] = $ObjectMetaValue[$product_id]['sku'];
		$scbox['product_data']['details'] = $this->Mcart_model_android->getMetaValueData($product_id);
		
		$message = '';
  		if($emailid!=''){
	      $config['protocol'] = 'smtp';
	      $totalProductPrice=0;
	      $referral_point = 0;
		  $referral_point = $this->cartnew_model->get_refre_amt($order_unique_no);	  
	
	      $config['charset'] = 'iso-8859-1';
	      $config['mailtype'] = 'html';
	      $config['wordwrap'] = TRUE;
	      $config['smtp_host'] = $this->config->item('smtp_host');
	      $config['smtp_user'] = $this->config->item('smtp_user');
	      $config['smtp_pass'] = $this->config->item('smtp_pass');
	      $config['smtp_port'] = $this->config->item('smtp_port');
			
	      $this->email->initialize($config);

		  	$data['orderUniqueNo'] = $order_unique_no;
		  	$data['order_info'] = $this->cartnew_model->su_get_order_price_info_res($order_unique_no);
		    $data['Username'] = @$data['order_info'][0]['first_name'].' '.@$data['order_info'][0]['last_name'];
		    $data['first_name'] = @$data['order_info'][0]['first_name'];
		    $data['last_last'] = @$data['order_info'][0]['last_name'];
		  	$data['order_product_info'] = $this->cartnew_model->su_get_order_info($order_unique_no); 
	      	$data['email_id'] = $data['order_info'][0]['email_id'];
	      	$data['shipping_address'] = $data['order_info'][0]['shipping_address'];
	      	$data['city_name'] = $data['order_info'][0]['city_name'];
	      	$data['state_name'] = $data['order_info'][0]['state_name'];
	      	$data['pincode'] = $data['order_info'][0]['pincode'];
	      	$data['country_name'] = $data['order_info'][0]['country_name'];
	      	$data['paymentmode'] = $data['order_info'][0]['pay_mode'];
	      	$data['referral_point'] = $referral_point;
	      	$data['order_display_no'] = $data['order_info'][0]['order_display_no'];
	      	$data['mobile_no'] = $data['order_info'][0]['mobile_no'];
	      	$data['order_date'] = date('d-m-Y',strtotime($data['order_product_info'][0]['created_datetime']));
	      	$data['order_deliverydate'] = date('d-m-Y',strtotime($data['order_product_info'][0]['modified_datetime']));
	      	$data['scbox'] = $scbox;

	      	if($data['order_info'][0]['state_id'] == '36') { $data['state_name'] =  $data['order_info'][0]['it_state']; }

	      	  $data['res'] =  $this->cartnew_model->su_get_order_info($order_unique_no); 
		      //$this->email->from($this->config->item('from_email'), 'StyleCracker: Order Return/Exchange Request');
		      $user_product_status = ucfirst($scbox['product_data']['user_status']);
		      $this->email->from($this->config->item('from_email'), 'StyleCracker: Order '.$user_product_status.' Request');
		      
		      //$this->email->to($emailid); 
		       if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){        
			      $this->email->to($this->config->item('sc_test_emaild'));
			      $this->email->bcc($this->config->item('sc_testcc_emaild'));
			    }else{
				      $this->email->to($data['email_id']);		     
				      if(trim($user_product_status)=='Return')
				      {
				      	$this->email->cc(array('refund@stylecracker.com'));      
				      }else if(trim($user_product_status)=='Exchange')
				      {
				      	$this->email->cc(array('exchange@stylecracker.com'));      
				      }
				    }
		      	  
		      $this->email->subject('Order '.$user_product_status.' Request');    
		      //$this->email->subject('Order Return/Exchange Request');
		      //echo '<pre>';print_r($data);
		      $this->load->view('emails/order_returnexchange_email',$data);   

		      $message = $this->load->view('emails/order_returnexchange_email',$data,true);
		     
		  	  $this->email->message($message);

		  	  $sentmsg = $this->email->send();
		  	  if($sentmsg)
		  	  {
		  	  	return 'sent';
		  	  }else
		  	  {
		  	  	return 'notsent';
		  	  }

	  }
	  }
	
}