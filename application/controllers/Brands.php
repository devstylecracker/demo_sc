<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Brands extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
       parent::__construct();
       $this->load->model('Brands_model');
       $this->load->model('Home_model');
       $this->load->model('Product_desc_model');
       $this->load->driver('cache');
   	}

	public function index()
  {
    $data = array();
	
    if ( $this->cache->file->get('brand_listing_page') == FALSE ) {
    $data['brands_list'] = $this->Brands_model->get_all_brands();
    $data['brands_banners'] = $this->Brands_model->get_brands_banners();
    $data['brand_categories']['name'] = $this->Brands_model->get_brands_categories_list();
    $data['brand_collections']['name'] = $this->Brands_model->get_brands_collections_list();

    foreach($data['brand_collections']['name'] as $row){
      $data['brand_collections']['brands'][$row['category_slug']] = $this->Brands_model->get_brands_categories_wize($row['category_slug']);
    }
      $this->cache->file->save('brand_listing_page', $data, 500000000000000000);
    }else{
      $data = $this->cache->file->get('brand_listing_page');
    }
	  $data['brands_banners'] = $this->Brands_model->get_brands_banners();

      $this->seo_title = "India's Top Fashion Brands - Buy Online Now on StyleCracker";
      $this->seo_desc = $this->lang->line('seo_desc');
      $this->seo_keyword = $this->lang->line('seo_keyword');
	
    $this->load->view('common/header_view');
    $this->load->view('brands',$data);
    $this->load->view('common/footer_view');
  }

  public function search_brand(){

   $intCount = '-1'; $o=1;
    $search = $this->input->post('search');
    $type = $this->input->post('type');
    $brands_list = $this->Brands_model->search_brand(strtolower($search),$type);
    $number_array = array("0","1","2","3","4","5","6","7","8","9","!","@");
    $brand_count = count($brands_list); 
    if($brand_count > 0){
       $intCount = $brand_count / 3 ;
    }
    if($intCount < 1){ $intCount = 1; }
    $i = 1;
  
    $brandhtml = '';
   

    if(!empty($brands_list)){ $k = 1; $new_name = ''; $old_name = '';
    foreach($brands_list as $val){  
    if(round($intCount) >= $k){

    if($k == 1){ 
        $brandhtml = $brandhtml.'<div class="col-md-4">';
     } 
      $new_name = strtoupper(substr($val['company_name'], 0,1)); 
      if($new_name != $old_name && !in_array($new_name, $number_array)){ 
      if($i != 1){ 
        $brandhtml=$brandhtml.'</ul>';
      } 
       $brandhtml = $brandhtml.' <ul class="brand-listing"> ';
        $brandhtml = $brandhtml.'<li id="'.strtoupper($new_name).'" class="alpha">'.strtoupper($new_name);
        $brandhtml = $brandhtml.'<script type="text/Javascript">$('.strtoupper($new_name).').attr("href","#'.strtoupper($new_name).'");</script>';
        $brandhtml =$brandhtml.'</li>';
        $brandhtml = $brandhtml.'<li><a href="'.base_url().'brands/brand-details/'.$val['user_name'].'/'.$val['store_id'].'">'.$val['company_name'].'</a></li>';
        $old_name = $new_name; 
       }else{ 
       if(in_array($new_name, $number_array) && $o == 1){ 
          $brandhtml = $brandhtml.'<li id="0-9" class="alpha">0-9</li>';
        }
        $brandhtml =$brandhtml.'<li><a href="'.base_url().'brands/brand-details/'.$val['user_name'].'/'.$val['store_id'].'">'.$val['company_name'].'</a></li>';
       } 
    }else{  
      $brandhtml =$brandhtml.' </div>';
      $k = 0; 
     } 
 $k++; $i++; $o++;}
  }

      echo $brandhtml;
  }


  function brand_details(){
    $data = array();
    $brand_slug = strip_tags($this->uri->segment(3));
	$store_slug = strip_tags($this->uri->segment(4));
    if($brand_slug != '' && $store_slug == ''){
		$data = $this->Brands_model->get_brands_info($brand_slug);
		if(@$data['user_name']==''){
		  redirect('/brands');
		}
		$data['looks_data'] = $this->Brands_model->get_brands_look($data['brand_id']);
		$data['products_data'] = $this->Brands_model->get_brands_product($data['brand_id']);
		$data['nav_cat_name'] = $this->home_model->sub_category($data['brand_id']); 
		$data['brands_review'] = $this->Brands_model->get_reviews($data['brand_id'],0);
	   
		$this->seo_title = $data['company_name']." - Buy Online Now on StyleCracker";
		$this->seo_desc = $data['brand_info'][0]['short_description'];
		$this->seo_canonical = base_url().'brands/brand-details/'.$data['user_name'];
		redirect('/brands/'.$data['user_name'].'-'.$data['brand_id'].'');
		$this->load->view('common/header_view');
		$this->load->view('brands_details',$data);
		$this->load->view('common/footer_view');
	  }else if($store_slug != ''){
		   $data = $this->Brands_model->get_brands_info($brand_slug);
		   $store_ids = explode('-',$store_slug);
		   // $data = $this->Brands_model->get_store_info($store_slug[1]);
		if(@$store_ids[1] < 0){
		  redirect('/brands');
		}
		$data['looks_data'] = $this->Brands_model->get_store_look(@$store_ids[1]);
		$data['products_data'] = $this->Brands_model->get_store_product(@$store_ids[1]);
		$data['nav_cat_name'] = $this->home_model->sub_category(@$store_ids[1]); 
		$data['brands_review'] = $this->Brands_model->get_reviews(@$data['brand_id'],0);
		// echo "<pre>";print_r($data['looks_data']);exit;
		$this->seo_title = $data['company_name']." - Buy Online Now on StyleCracker";
		$this->seo_desc = $data['brand_info'][0]['short_description'];
		$this->seo_canonical = base_url().'brands/brand-details/'.$data['user_name'];
		$data['user_name'] = $data['user_name'].'/'.$store_slug;
		$this->load->view('common/header_view');
		$this->load->view('brands_details',$data);
		$this->load->view('common/footer_view');
	  }else{
		redirect('/schome');
	  }
  }

  function redirect(){
    $product_id = $this->uri->segment(4);
    $user_id = $this->uri->segment(3);
    if($product_id){
      $data['product'] = $this->Brands_model->get_product_info($product_id);
      $data['encode_product_id'] = $this->encryptOrDecrypt($product_id,'encrypt');
      $data['user_info'] = $user_id;
      //$this->load->view('common/header_view');
      $this->load->view('redirect',$data);
     // $this->load->view('common/footer_view');
    }
  }

  function post_review(){
    $post_review = $this->input->post('post_review');
    $ratings = $this->input->post('ratings');
    $brand_token = $this->input->post('brand_token');
    

    if($post_review!='' && $ratings!='' && $brand_token!=''){
        if($this->Brands_model->post_review($post_review,$ratings,$brand_token)){
          return true;
        }else{
          return false;
        }
    }

  }

  function get_reviews(){
      $htmldata = '';
      $brand_token = $this->input->post('brand_token');
      $review = $this->Brands_model->get_reviews($brand_token,2);
      if(!empty($review)){
        foreach($review as $val){
          $to_time = strtotime($this->config->item('sc_date'));
          $from_time = strtotime($val['created_datetime']);
          /*$htmldata = $htmldata.'<h3 class="title">Reviews:</h3>';
          $htmldata = $htmldata.'<div>'.$this->get_user($val['user_id']).'</div>';
          $htmldata = $htmldata.'<div>'.$val['review_text'].'</div>';
          $htmldata = $htmldata.'<div>Rated'.$val['ratings'].'</div>';*/

             $htmldata = $htmldata.'<div class="user-review">';
                $htmldata = $htmldata.'<div class="row">';
                  $htmldata = $htmldata.'<div class="col-md-3">';
                $htmldata = $htmldata.'<div class="user-info">';
                $htmldata = $htmldata.'<div class="user-photo"><img src="'.$val['profile_pic'].'"></div>';

                $htmldata = $htmldata.'<div class="user-name-wrp">';
                  $htmldata = $htmldata.'<div class="user-ratings">';
                    $htmldata = $htmldata.'<div class="ratings-wrp">';
                      $htmldata = $htmldata.'<ul class="ratings rating-'.$val['ratings'].'">';
                        $htmldata = $htmldata.'<li class="star1"></li>';
                        $htmldata = $htmldata.'<li class="star2"></li>';
                        $htmldata = $htmldata.'<li class="star3"></li>';
                        $htmldata = $htmldata.'<li class="star4"></li>';
                        $htmldata = $htmldata.'<li class="star5"></li>';
                     $htmldata = $htmldata.'</ul>';
                    $htmldata = $htmldata.'</div>';
                  $htmldata = $htmldata.'</div>';

                $htmldata = $htmldata.'<div class="user-name">';
                $htmldata = $htmldata.@$val['first_name'].' '.@$val['last_name'];
                $htmldata = $htmldata.'</div>';
                $htmldata = $htmldata.'<div class="user-activity">';
                $htmldata = $htmldata.$val['created_datetime'];
                  $htmldata = $htmldata.'</div>';
                $htmldata = $htmldata.'</div>';
                    $htmldata = $htmldata.'<div class="clear">';
                  $htmldata = $htmldata.'</div>';
                 $htmldata = $htmldata.'</div>';
                 $htmldata = $htmldata.'</div>';
                 $htmldata = $htmldata.'<div class="col-md-9">';
                  $htmldata = $htmldata.strip_tags($val['review_text']);
                  $htmldata = $htmldata.'</div>';
                  $htmldata = $htmldata.'</div>';
                $htmldata = $htmldata.'</div>';
          
          //$htmldata = $htmldata.'<div>'.round(abs($to_time - $from_time) / 60,2). " minute".'</div>';
         
        }
        $htmldata = $htmldata.'<div class="load-more-wrp" id="load_more" onclick="load_more_reviews();"><div class="load-more">Load More</div></div>';
      }
      
      echo $htmldata;
  }

   function get_all_reviews(){
      $htmldata = '';
      $brand_token = $this->input->post('brand_token');
      $review = $this->Brands_model->get_reviews($brand_token,0);
      if(!empty($review)){
        foreach($review as $val){
          $to_time = strtotime($this->config->item('sc_date'));
          $from_time = strtotime($val['created_datetime']);
          
         /* $htmldata = $htmldata.'<div>'.$this->get_user($val['user_id']).'</div>';
          $htmldata = $htmldata.'<div>'.$val['review_text'].'</div>';
          $htmldata = $htmldata.'<div>Rated'.$val['ratings'].'</div>';
           $htmldata = $htmldata.'<div>'.round(abs($to_time - $from_time) / 60,2). " minute".'</div>';*/

                 $htmldata = $htmldata.'<div class="user-review">';
                $htmldata = $htmldata.'<div class="row">';
                  $htmldata = $htmldata.'<div class="col-md-3">';
                $htmldata = $htmldata.'<div class="user-info">';
                $htmldata = $htmldata.'<div class="user-photo"><img src="'.$val['profile_pic'].'"></div>';

                $htmldata = $htmldata.'<div class="user-name-wrp">';
                  $htmldata = $htmldata.'<div class="user-ratings">';
                    $htmldata = $htmldata.'<div class="ratings-wrp">';
                      $htmldata = $htmldata.'<ul class="ratings rating-'.$val['ratings'].'">';
                        $htmldata = $htmldata.'<li class="star1"></li>';
                        $htmldata = $htmldata.'<li class="star2"></li>';
                        $htmldata = $htmldata.'<li class="star3"></li>';
                        $htmldata = $htmldata.'<li class="star4"></li>';
                        $htmldata = $htmldata.'<li class="star5"></li>';
                     $htmldata = $htmldata.'</ul>';
                    $htmldata = $htmldata.'</div>';
                  $htmldata = $htmldata.'</div>';

                $htmldata = $htmldata.'<div class="user-name">';
                $htmldata = $htmldata.@$val['first_name'].' '.@$val['last_name'];
                $htmldata = $htmldata.'</div>';
                $htmldata = $htmldata.'<div class="user-activity">';
                $htmldata = $htmldata.$val['created_datetime'];
                  $htmldata = $htmldata.'</div>';
                $htmldata = $htmldata.'</div>';
                    $htmldata = $htmldata.'<div class="clear">';
                  $htmldata = $htmldata.'</div>';
                 $htmldata = $htmldata.'</div>';
                 $htmldata = $htmldata.'</div>';
                 $htmldata = $htmldata.'<div class="col-md-9">';
                  $htmldata = $htmldata.strip_tags($val['review_text']);
                  $htmldata = $htmldata.'</div>';
                  $htmldata = $htmldata.'</div>';
                $htmldata = $htmldata.'</div>';
          
        }
      }
      echo $htmldata;
  }

  function get_user($user_id){
       $get_user_info = $this->Brands_model->get_user($user_id);
       if($get_user_info){
       return $get_user_info[0]['first_name'].' '.$get_user_info[0]['last_name'];
      }else{
        return '';
      }
  }
  function view_profile($user_id){
       $get_user_info = $this->Brands_model->get_user($user_id);
       if($get_user_info){
          return $get_user_info[0]['profile_pic'];
        }else{
          return '';
      }
  }
  function encryptOrDecrypt($mprhase, $crypt) {
       $MASTERKEY = "STYLECRACKERAPI";
       $td = mcrypt_module_open('tripledes', '', 'ecb', '');
       $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
       mcrypt_generic_init($td, $MASTERKEY, $iv);
       if ($crypt == 'encrypt')
       {
           $return_value = base64_encode(mcrypt_generic($td, $mprhase));
       }
       else
       {
           $return_value = mdecrypt_generic($td, base64_decode($mprhase));
       }
       mcrypt_generic_deinit($td);
       mcrypt_module_close($td);
       return $return_value;
  } 

  function looks(){
    $brand_username = $this->uri->segment(3);
	$store_slug = $this->uri->segment(4);
	$store_ids = explode('-',$store_slug);
	
    if(trim($brand_username) != '' && $store_slug == ''){
       $data['looks'] = $this->Home_model->get_brands_looks('',$brand_username);
       $data['total_looks'] = $this->Home_model->get_brands_looks_total($brand_username);
       $data['brand_id'] = $this->home_model->get_user_data('user_login','user_name',$brand_username)[0]['id'];
       $data['brand_name'] = $this->home_model->get_user_data('brand_info','user_id',$data['brand_id'])[0]['company_name'];
       $data['brand_username'] = $brand_username;

      $this->load->view('common/header_view');
      $this->load->view('brands_looks_view',$data);
      $this->load->view('common/footer_view');

    }else if(trim($store_slug) != ''){
       $data['looks'] = $this->Home_model->get_store_looks('',$brand_username,@$store_ids[1]);
       $data['total_looks'] = $this->Home_model->get_store_looks_total($brand_username,@$store_ids[1]);
       $data['brand_id'] = $this->home_model->get_user_data('user_login','user_name',$brand_username)[0]['id'];
       $data['brand_name'] = $this->home_model->get_user_data('brand_info','user_id',$data['brand_id'])[0]['company_name'];
       $data['brand_username'] = $brand_username;
	   $data['store_id'] = $store_ids[1];
	   
      $this->load->view('common/header_view');
      $this->load->view('brands_looks_view',$data);
      $this->load->view('common/footer_view');

    }else{
      redirect('/');
    }

  }

  function get_more_looks(){

    $brand_username = $this->input->post('brand_username');
    $offset = $this->input->post('offset');
    if($brand_username!='' && $offset>0){
      $data['looks'] = $this->Home_model->get_brands_looks($offset,$brand_username);
    }
    $this->load->view('more_looks',$data);

  }

  function show_wheel(){
      $this->load->view('common/header_view');
      $this->load->view('show_wheel');
      $this->load->view('common/footer_view');
  }
}
