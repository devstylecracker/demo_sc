<?php 
ini_set('max_execution_time',600);
defined('BASEPATH') OR exit('No direct script access allowed');
class Feedback extends MY_Controller {
	function __construct(){
       parent::__construct();
       $this->load->model('cartnew_model');
       $this->load->model('feedback_model');
       $this->load->library('email');
       $this->load->library('bitly');
   	} 
	public function index()
	{		
		echo 'Feedback';exit;
	}
	public function order()
	{	
		$order_id=$this->uri->segment(2);
		$data = array();$d_result = '';$u_data = '';
		$data['success'] = '';
		$data['error'] = '';
		
		if($order_id!='')
		{
			$d_result = $this->cartnew_model->get_deliveredOrders($order_id);
			$u_data = $this->cartnew_model->get_deliveredOrdersData($order_id);
			$data['first_name'] = $u_data[0]['first_name'];
			
		}
		
		if($order_id!='' && !empty($u_data))
    	{
	    	//$user_id = $this->cartnew_model->get_deliveredOrders($order_id)[0]['user_id'];
			$user_id = $u_data[0]['user_id'];
	    	$objectid = $this->feedback_model->get_object('order_feedback')[0]['object_id'];
	    	$data['user_response'] = unserialize(@$this->feedback_model->get_objectmeta($objectid,$order_id,$user_id)[0]['object_meta_value']);	
	    	 
	    	if(!empty($data['user_response']))
	    	{
	    		$data['success'] = 1;
	    	}    	
	    }else
	    {
	    	$data['error'] = 'Order Id not delivered (Order Id does not exist)';
	    }
	    
	    
		if(!empty($this->input->post()) && !empty($u_data))
	    { 
	    	if($order_id!='')
	    	{
		    	$answer = serialize($this->input->post());
		    	//$user_id = $this->cartnew_model->get_deliveredOrders($order_id)[0]['user_id'];
				$user_id = $u_data[0]['user_id'];
		    	$objectid = $this->feedback_model->get_object('order_feedback')[0]['object_id'];

		    	if(!empty($this->input->post()))
		    	{
		    		$result = $this->feedback_model->save_feedback_answer($order_id,$answer,$user_id,$objectid);

		    		if($result)
		    		{
		    			$data['success'] = 1;
		    			$this->orderFeedbackEmailResponseStylist($order_id,$answer);
		    			$this->orderFeedbackEmailResponse($order_id,$answer,$user_id);
		    		}else
		    		{
		    			$data['success'] = 0;
		    		}
		    	}
		    }
			
		}
		
		$this->load->view('seventeen/common/header_view');
		$this->load->view('seventeen/feedback',$data);
		$this->load->view('seventeen/common/footer_view');	
	}

	function orderFeedbackEmail($user_id,$order_id,$order_display_no,$first_name,$last_last,$emailid){   

		$message = '';
  		if($emailid!=''){
	      $config['protocol'] = 'smtp';
	      $totalProductPrice=0;
	      $referral_point = 0;
		  $referral_point = $this->cartnew_model->get_refre_amt($order_id);

	//      $config['mailpath'] = '/usr/sbin/sendmail';
	      $config['charset'] = 'iso-8859-1';
	      $config['mailtype'] = 'html';
	      $config['wordwrap'] = TRUE;
	      $config['smtp_host'] = $this->config->item('smtp_host');
	      $config['smtp_user'] = $this->config->item('smtp_user');
	      $config['smtp_pass'] = $this->config->item('smtp_pass');
	      $config['smtp_port'] = $this->config->item('smtp_port');
			
	      $this->email->initialize($config);

		  	$data['orderUniqueNo'] = $order_id;
		    $data['Username'] = $first_name.' '.$last_last;
		    $data['first_name'] = $first_name;
		    $data['last_last'] = $last_last;
		  	$data['order_product_info'] = $this->cartnew_model->su_get_order_info($order_id);    
	      	$data['order_info'] = $this->cartnew_model->su_get_order_price_info_res($order_id);
	      	$data['email_id'] = $data['order_info'][0]['email_id'];
	      	$data['shipping_address'] = $data['order_info'][0]['shipping_address'];
	      	$data['city_name'] = $data['order_info'][0]['city_name'];
	      	$data['state_name'] = $data['order_info'][0]['state_name'];
	      	$data['pincode'] = $data['order_info'][0]['pincode'];
	      	$data['country_name'] = $data['order_info'][0]['country_name'];
	      	$data['paymentmode'] = $data['order_info'][0]['pay_mode'];
	      	$data['referral_point'] = $referral_point;
	      	$data['order_display_no'] = $data['order_info'][0]['order_display_no'];
	      	$data['mobile_no'] = $data['order_info'][0]['mobile_no'];
	      	$data['order_date'] = date('d-m-Y',strtotime($data['order_product_info'][0]['created_datetime']));
	      	$data['order_deliverydate'] = date('d-m-Y',strtotime($data['order_product_info'][0]['modified_datetime']));


	      	if($data['order_info'][0]['state_id'] == '36') { $data['state_name'] =  $data['order_info'][0]['it_state']; }

	      	$data['res'] =  $this->cartnew_model->su_get_order_info($order_id); 

	  		//$this->load->view('emails/order_successful_user_copy',$data);

		      $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Feedback');
		      $this->email->to($emailid);    
		     /* $this->email->cc('order@stylecracker.com');    */
		      	  
		    /*  $this->email->to('sudha@stylecracker.com');     */  
		      $this->email->subject('Did you love your #SCBox? We want to hear from you!');
		      //echo '<pre>';print_r($data);
		      $this->load->view('emails/order_feedback_email',$data);   

		      $message = $this->load->view('emails/order_feedback_email',$data,true);
		     
		  	  $this->email->message($message);

		  	  $sentmsg = $this->email->send();
		  	  if($sentmsg)
		  	  {
		  	  	return 'sent';
		  	  }else
		  	  {
		  	  	return 'notsent';
		  	  }

	 		 }
	  }

	  public function getFeedbackOrders()
	  { 
	  	$result = $this->cartnew_model->get_deliveredOrders();
		
	  	$feeddata = array();
	  	$orderids = '';
	  	//echo '<pre>';print_r($result);
	  	//echo'count=='; echo $count = sizeof($result);exit;
	  	$count = sizeof($result);
	  	$feeddata['count'] = $count;	  	
	  	$i=0;
		foreach($result as $val)
		{
			/*if($val['order_display_no']=='SC1499326143')
			{*/
				$r = $this->orderFeedbackEmail($val['user_id'],$val['order_unique_no'],$val['order_display_no'],$val['first_name'],$val['last_name'],$val['email_id']);
			 //}
			$orderids = $orderids.','.$val['order_display_no'];
			//echo '<pre>';print_r($r);
			//exit;
			$i++;
		}
		$feeddata['orderids'] = $orderids;
		$this->orderfeedbackCron($feeddata); 
	  }

	  function orderFeedbackEmailResponseStylist($order_display_no,$answer){  
	  	  $message = '';
	  	  $config['protocol'] = 'smtp';
	      $totalProductPrice=0;
	      $referral_point = 0;
		  //$referral_point = $this->cartnew_model->get_refre_amt($order_display_no);
		  $config['charset'] = 'iso-8859-1';
	      $config['mailtype'] = 'html';
	      $config['wordwrap'] = TRUE;
	      $config['smtp_host'] = $this->config->item('smtp_host');
	      $config['smtp_user'] = $this->config->item('smtp_user');
	      $config['smtp_pass'] = $this->config->item('smtp_pass');
	      $config['smtp_port'] = $this->config->item('smtp_port');
	      $this->email->initialize($config);
	      $result = $this->cartnew_model->get_deliveredOrdersData($order_display_no);
	      $data['Username'] = $result[0]['first_name'].' '. $result[0]['last_name'];
	      $objectid = $this->feedback_model->get_object('order_feedback')[0]['object_id'];
	      //$data['user_response'] = unserialize(@$this->feedback_model->get_objectmeta($objectid,$order_id,$user_id)[0]['object_meta_value']);
	      $data['user_response'] =unserialize($answer);
	      $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Feedback');
	      $this->email->to('lavin@stylecracker.com');    
	      $this->email->cc('arun@stylecracker.com');    	  
	      //$this->email->to('sudha@stylecracker.com');       
	      $this->email->subject('StyleCracker: Order Feedback Response -'.$order_display_no);
	      $data['emailer'] = 1;
	      $message = $this->load->view('emails/order_feedbackresponsestylist_email',$data,true);
	      $this->email->message($message);
	  	  $sentmsg = $this->email->send();
	  	  if($sentmsg)
	  	  {
	  	  	return 'sent';
	  	  }else
	  	  {
	  	  	return 'notsent';
	  	  }
	  }

	   function orderFeedbackEmailResponse($order_display_no,$answer,$user_id){  
	  	  $message = '';
	  	  $config['protocol'] = 'smtp';
	      $totalProductPrice=0;
	      $referral_point = 0;
		  //$referral_point = $this->cartnew_model->get_refre_amt($order_display_no);
		  $config['charset'] = 'iso-8859-1';
	      $config['mailtype'] = 'html';
	      $config['wordwrap'] = TRUE;
	      $config['smtp_host'] = $this->config->item('smtp_host');
	      $config['smtp_user'] = $this->config->item('smtp_user');
	      $config['smtp_pass'] = $this->config->item('smtp_pass');
	      $config['smtp_port'] = $this->config->item('smtp_port');
	      $this->email->initialize($config);
	      $result = $this->cartnew_model->get_deliveredOrdersData($order_display_no);
	      $data['Username'] = $result[0]['first_name'].' '. $result[0]['last_name'];
	      $data['first_name'] = $result[0]['first_name'];
	      $data['last_name'] = $result[0]['last_name'];
	      $data['email'] =  $result[0]['email_id'];
	      $objectid = $this->feedback_model->get_object('order_feedback')[0]['object_id'];
	      //$data['user_response'] = unserialize(@$this->feedback_model->get_objectmeta($objectid,$order_id,$user_id)[0]['object_meta_value']);
	      $data['user_response'] =unserialize($answer);
	      $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Feedback Response');
	      $this->email->to($data['email']);    
	      /*$this->email->cc('order@stylecracker.com');    */		      	  
	      /*$this->email->to('sudha@stylecracker.com'); */      
	      $this->email->subject('We\'re Saying Thank You With 20% Off Your Next #SCBox!');
	      $data['emailer'] = 1;
	      $message = $this->load->view('emails/order_feedbackresponse_email',$data,true);
	      $this->email->message($message);
	  	  $sentmsg = $this->email->send();
	  	  if($sentmsg)
	  	  {
	  	  	return 'sent';
	  	  }else
	  	  {
	  	  	return 'notsent';
	  	  }
	  }

	  function orderfeedbackCron($feeddata){  
	  	  $message = '';
	  	  $config['protocol'] = 'smtp';
	      $totalProductPrice=0;
	      $referral_point = 0;
		 // $referral_point = $this->cartnew_model->get_refre_amt($order_display_no);
		  $config['charset'] = 'iso-8859-1';
	      $config['mailtype'] = 'html';
	      $config['wordwrap'] = TRUE;
	      $config['smtp_host'] = $this->config->item('smtp_host');
	      $config['smtp_user'] = $this->config->item('smtp_user');
	      $config['smtp_pass'] = $this->config->item('smtp_pass');
	      $config['smtp_port'] = $this->config->item('smtp_port');
	      $this->email->initialize($config);
	    
	      $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Feedback Cron');
	      // $this->email->to('sudha@stylecracker.com');    
	      // $this->email->cc('arun@stylecracker.com');    		      	  
	      $this->email->to('sudha@stylecracker.com');  
	      $this->email->cc(array('arun@stylecracker.com','lavin@stylecracker.com'));         
	      $this->email->subject('StyleCracker: Order Feedback Cron -');	     
	      $message = 'Count = '.$feeddata['count'].'<br/>Order Ids email sent- <br/>'.$feeddata['orderids'];
	      $this->email->message($message);
	      $object_key = 'order_feedback_sent_'.date('Y-m-d');
	      $object_value = trim($feeddata['orderids'],',');
	      $useridval = 0;
	      $objectid = $this->feedback_model->get_object('order_feedback')[0]['object_id'];
	      if($object_value!='')
	      {
	      	$this->feedback_model->save_feedback_answer($object_key,$object_value,$useridval,$objectid);
	      }
	  	  $sentmsg = $this->email->send();
	  	  if($sentmsg)
	  	  {
	  	  	return 'sent';
	  	  }else
	  	  {
	  	  	return 'notsent';
	  	  }
	  }
	  
	  function displayUserResponse($order_display_no,$user_id)
	  {
		$result = $this->cartnew_model->get_deliveredOrders($order_display_no);
	    $data['Username'] = $result[0]['first_name'].' '. $result[0]['last_name'];
		$data['first_name'] = $result[0]['first_name'];
		$data['last_name'] = $result[0]['last_name'];
		$data['email'] =  $result[0]['email_id'];
		$objectid = $this->feedback_model->get_object('order_feedback')[0]['object_id'];
	    $data['user_response'] = unserialize(@$this->feedback_model->get_objectmeta($objectid,$order_display_no,$user_id)[0]['object_meta_value']);
		$message = $this->load->view('emails/order_feedbackresponsestylist_email',$data,true); 
		echo $message;
	  }
	  
	  function getUsersFeedback()
	  {
		  //echo 'getUsersFeedback';exit;
		  $i=0;$username = '';
		  $datahtml = '<table style="border:1;"><tr><td>Sr.No</td><td>Order Id</td><td>Name</td><td>EmailId</td><td>Datetime</td><td>Feedback</td></tr>';
		  $userdata = $this->feedback_model->get_userfeedbackdata();
		  foreach($userdata as $val)
		  {  $i++;
			  
			  $data['user_response'] = unserialize($val['object_meta_value']);
			  $datahtml = $datahtml.'<tr><td>'.$i.'<td>'.$val['object_meta_key'].'</td><td>'.$val['first_name'].' '.$val['last_name'].'</td><td>'.$val['email_id'].'</td><td>'.$val['created_datetime'].'</td><td>'.'<a href="'.base_url().'feedback/displayUserResponse/'.$val['object_meta_key'].'/'.$val['user_id'].'" >Feedback</a></td></tr>';
		  }
		  $datahtml = $datahtml.'</table>';
		  echo $datahtml;
	  }

	  public function feedbackOrdersResend()
	  { 
	  	$result_obj = $this->cartnew_model->get_deliveredOrders_recall();
	  	//$orderids = explode(',',$result_obj[0]['object_meta_value']);
	  	//echo '<pre>';print_r($result_obj);exit;
	  	$feeddata = array();
	  	$orderids = '';
	  	//echo '<pre>';print_r($result);
	  	$count = sizeof($result_obj);
	  	$feeddata['count'] = $count;	  	
	  	//$r = $this->orderFeedbackEmail(24795,'24795_1793','SC1501130154','Sudha','Scss','sudhasc1234@style.com');exit;
	  	$i=0;
	  	if(!empty($result_obj))
	  	{
	  		foreach($result_obj as $val)
			{
				// if($val['order_display_no']=='SC1500616640')
				// {
					$r = $this->orderFeedbackEmail($val['user_id'],$val['order_unique_no'],$val['order_display_no'],$val['first_name'],$val['last_name'],$val['email_id']);
				 //}
				$orderids = $orderids.','.$val['order_display_no'];
				//echo '<pre>';print_r($r);
				//exit;
				$i++;
			}
				
	  	}
		$feeddata['orderids'] = $orderids;
		$this->orderfeedbackCron($feeddata); 
		exit;
	  }

	   public function getOrdersCustom()
	  { echo 'getOrdersCustom';exit; 
	  	$result = $this->cartnew_model->get_customOrdersbyrange();
	  	$feeddata = array();
	  	$orderids = '';
	  	//echo '<pre>';print_r($result);
	  	$count = sizeof($result);
	  	$feeddata['count'] = $count;	  	
	  	//$r = $this->orderFeedbackEmail(24795,'24795_1793','SC1501130154','Sudha','Scss','sudhasc1234@style.com');exit;
	  	$i=0;
		foreach($result as $val)
		{ 
			if($i==0)
			{
				$r = $this->orderCustomEmail($val['order_display_no'],'',$val['user_id']);
			 }
			$orderids = $orderids.','.$val['order_display_no'];
			//echo '<pre>';print_r($r);
			//exit;
			$i++;
		}
		$feeddata['orderids'] = $orderids;
		//$this->customEmailSentData($feeddata); 		
	  }

	  function orderCustomEmail($order_display_no,$answer,$user_id){ 
	  	  $message = '';
	  	  $config['protocol'] = 'smtp';
	      $totalProductPrice=0;
	      $referral_point = 0;
		  //$referral_point = $this->cartnew_model->get_refre_amt($order_display_no);
		  $config['charset'] = 'iso-8859-1';
	      $config['mailtype'] = 'html';
	      $config['wordwrap'] = TRUE;
	      $config['smtp_host'] = $this->config->item('smtp_host');
	      $config['smtp_user'] = $this->config->item('smtp_user');
	      $config['smtp_pass'] = $this->config->item('smtp_pass');
	      $config['smtp_port'] = $this->config->item('smtp_port');
	      $this->email->initialize($config);
	      $result = $this->cartnew_model->get_customOrdersbyrange($order_display_no);	      
	      $data['Username'] = $result[0]['first_name'].' '.$result[0]['last_name'];
	     $data['first_name'] = $result[0]['first_name'];
	      //$data['first_name'] = 'Sudha';
	      $data['last_name'] = $result[0]['last_name'];
	      $data['email'] =  $result[0]['email_id'];
	      //$data['email'] =  'sudha@stylecracker.com';
	      $objectid = $this->feedback_model->get_object('order_feedback')[0]['object_id'];
	      //$data['user_response'] = unserialize(@$this->feedback_model->get_objectmeta($objectid,$order_id,$user_id)[0]['object_meta_value']);
	      //$data['user_response'] =unserialize($answer);
	      $this->email->from($this->config->item('from_email'), 'StyleCracker');
	      $this->email->to($data['email']);    
	      /*$this->email->cc('order@stylecracker.com');    */		      	  
	     /* $this->email->to('sudha@stylecracker.com'); 
	      $this->email->cc(array('rahul@stylecracker.com','arun@stylecracker.com'));    */   
	      $this->email->subject('You\'re Going To Love This');
	      $data['emailer'] = 1;
	      echo $message = $this->load->view('emails/order_custom_email',$data,true);
	      $this->email->message($message);
	  	  $sentmsg = $this->email->send();
	  	  if($sentmsg)
	  	  {
	  	  	return 'sent';
	  	  }else
	  	  {
	  	  	return 'notsent';
	  	  }
	  }

	  function customEmailSentData($feeddata){  
	  	  $message = '';
	  	  $config['protocol'] = 'smtp';
	      $totalProductPrice=0;
	      $referral_point = 0;
		  $referral_point = $this->cartnew_model->get_refre_amt($order_display_no);
		  $config['charset'] = 'iso-8859-1';
	      $config['mailtype'] = 'html';
	      $config['wordwrap'] = TRUE;
	      $config['smtp_host'] = $this->config->item('smtp_host');
	      $config['smtp_user'] = $this->config->item('smtp_user');
	      $config['smtp_pass'] = $this->config->item('smtp_pass');
	      $config['smtp_port'] = $this->config->item('smtp_port');
	      $this->email->initialize($config);
	    
	      $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Custom Email');
	      // $this->email->to('sudha@stylecracker.com');    
	      // $this->email->cc('arun@stylecracker.com');    		      	  
	      $this->email->to('sudha@stylecracker.com');  
	      $this->email->cc(array('arun@stylecracker.com','lavin@stylecracker.com'));         
	      $this->email->subject('StyleCracker: Order Custom Email -');	     
	      $message = 'Count = '.$feeddata['count'].'<br/>Order Ids email sent- <br/>'.$feeddata['orderids'];
	      $this->email->message($message);
	      $object_key = 'order_custom_marketing_email_sent_'.date('Y-m-d');
	      $object_value = trim($feeddata['orderids'],',');
	      $useridval = 0;
	      $objectid = $this->feedback_model->get_object('order_feedback')[0]['object_id'];
	      if($object_value!='')
	      {
	      	$this->feedback_model->save_feedback_answer($object_key,$object_value,$useridval,$objectid);
	      }
	  	  $sentmsg = $this->email->send();
	  	  if($sentmsg)
	  	  {
	  	  	return 'sent';
	  	  }else
	  	  {
	  	  	return 'notsent';
	  	  }
	  }

	  function getcustomUsers($type=null)
	{ exit;
		$i = 0;$scbox_users_count = '';$nonscbox_users_count ='';$email_ids = '';
		if($type=='scbox_users')
		{
			/*$result_scbox =  $this->feedback_model->getscboxusers();	
	        $scbox_users_count = sizeof($result_scbox);
	        $email_ids = 'Count == '.$scbox_users_count.'<br/><br/>';exit;
	        foreach($result_scbox as $val)
	        {
	        	$email = trim($val['email_id']);
	      	  	$email_ids = $email.','.$email;
	      	  	//if($i<1)
	      	  	//{
	      	  		//$email = 'sudha@stylecracker.com';
	      	  		$this->orderCustomEmail_scboxUserstemp($email);   
	      	  	//}	        	
	        	$i++;
	        }*/
	        
		}else if($type=='nonscbox_users')
		{
			/*$result_nonscbox =  $this->feedback_model->getnonscboxusers();	
	        $nonscbox_users_count = sizeof($result_nonscbox); 
	        echo $email_ids = 'Count == '.$nonscbox_users_count.'<br/><br/>';exit; 
	        
	        foreach($result_nonscbox as $val)
	        {
	        	$email = trim($val['email']);
	      	  	$email_ids = $email.','.$email;
	      	  	if($i<1)
	      	  	{
	        		$this->orderCustomEmail_nonscboxUserstemp($email); 
	        	}  
	        	$i++;
	        }*/
		}else if($type=='scboxconfirm_users')
		{
			$result_confirmscbox =  $this->feedback_model->getscboxusersConfirm();	
	        $scbox_users_count = sizeof($result_confirmscbox); 
	        $email_ids = ''; 
	       // echo 'count--'.$scbox_users_count.'<br/>';
	        //echo '<pre>';print_r($result_confirmscbox);exit;
	        foreach($result_confirmscbox as $val)
	        {
	        	$username = trim($val['first_name']);
	        	//$email = 'arun@stylecracker.com';
	        	$email = trim($val['email_id']);
	      	  	$email_ids = $email.','.$email;
	      	  	// if($i<1)
	      	  	// {
	        		$this->orderCustomEmail_scboxConfirmtemp($email,$username); 
	        	//}exit;  
	        	$i++;
	        }
		}

		$this->emailDataSent($email_ids);

	}

 	function orderCustomEmail_scboxUserstemp($email){  
	  	  $message = '';
	  	  $config['protocol'] = 'smtp';
	      $totalProductPrice=0;
	      $referral_point = 0;
		 
		  $config['charset'] = 'iso-8859-1';
	      $config['mailtype'] = 'html';
	      $config['wordwrap'] = TRUE;
	      $config['smtp_host'] = $this->config->item('smtp_host');
	      $config['smtp_user'] = $this->config->item('smtp_user');
	      $config['smtp_pass'] = $this->config->item('smtp_pass');
	      $config['smtp_port'] = $this->config->item('smtp_port');
	      $this->email->initialize($config);	      
	      $this->email->from($this->config->item('from_email'), 'StyleCracker');
	      
	      	if(!empty($email))
	      	{
	      	  //$this->email->to('sudha@stylecracker.com'); 
	      	  $this->email->to($email);        
		      $this->email->subject('Get stylist approved fashion at 50% off!');
		      $data['emailer'] = 1;
		     
		      echo $message = $this->load->view('emails/scboxusers_email',@$data,true);
		      $this->email->message($message);
		  	  $sentmsg = $this->email->send();
		  	  if($sentmsg)
		  	  {
		  	  	return 'sent';
		  	  }else
		  	  {
		  	  	return 'notsent';
		  	  }
		  	}     
		     
	  }

	  function orderCustomEmail_nonscboxUserstemp($email){  
	  	  $message = '';
	  	  $config['protocol'] = 'smtp';
	      $totalProductPrice=0;
	      $referral_point = 0;
		 
		  $config['charset'] = 'iso-8859-1';
	      $config['mailtype'] = 'html';
	      $config['wordwrap'] = TRUE;
	      $config['smtp_host'] = $this->config->item('smtp_host');
	      $config['smtp_user'] = $this->config->item('smtp_user');
	      $config['smtp_pass'] = $this->config->item('smtp_pass');
	      $config['smtp_port'] = $this->config->item('smtp_port');
	      $this->email->initialize($config);
	     
	     if(!empty($email))
      	{
	      $this->email->from($this->config->item('from_email'), 'StyleCracker');	     
	      $this->email->to('sudha@stylecracker.com');
	      //$this->email->to($email);         
	      $this->email->subject('Get stylist approved fashion at 50% off!');
	      $data['emailer'] = 1;	    
	     echo $message = $this->load->view('emails/nonscboxusers_email',@$data,true);
	      $this->email->message($message);
	  	  $sentmsg = $this->email->send();
	  	  if($sentmsg)
	  	  {
	  	  	return 'sent';
	  	  }else
	  	  {
	  	  	return 'notsent';
	  	  }
	  	}
	  }

	  function emailDataSent($message)
	  {
	  		$message = '';
	  	  $config['protocol'] = 'smtp';	     
		  $config['charset'] = 'iso-8859-1';
	      $config['mailtype'] = 'html';
	      $config['wordwrap'] = TRUE;
	      $config['smtp_host'] = $this->config->item('smtp_host');
	      $config['smtp_user'] = $this->config->item('smtp_user');
	      $config['smtp_pass'] = $this->config->item('smtp_pass');
	      $config['smtp_port'] = $this->config->item('smtp_port');
	      $this->email->initialize($config);
	    
	      $this->email->from($this->config->item('from_email'), 'StyleCracker');
	      // $this->email->to('sudha@stylecracker.com');    
	      // $this->email->cc('arun@stylecracker.com');    		      	  
	      $this->email->to('sudha@stylecracker.com');  
	      $this->email->cc(array('arun@stylecracker.com'));         
	      $this->email->subject('StyleCracker:  Scbox Users Custom 50 % offer Email');	     
	      //$message = 'Count = '.$scbox_users_count.'<br/>Emailds Ids email sent- <br/>'.$email_ids;
	      $this->email->message($message);      
	      
	  	  $sentmsg = $this->email->send();
	  	  if($sentmsg)
	  	  {
	  	  	return 'sent';
	  	  }else
	  	  {
	  	  	return 'notsent';
	  	  }
	  }

	   function orderCustomEmail_scboxConfirmtemp($email,$username){  
	  	  $message = '';
	  	  $config['protocol'] = 'smtp';
	      $totalProductPrice=0;
	      $referral_point = 0;
		 
		  $config['charset'] = 'iso-8859-1';
	      $config['mailtype'] = 'html';
	      $config['wordwrap'] = TRUE;
	      $config['smtp_host'] = $this->config->item('smtp_host');
	      $config['smtp_user'] = $this->config->item('smtp_user');
	      $config['smtp_pass'] = $this->config->item('smtp_pass');
	      $config['smtp_port'] = $this->config->item('smtp_port');
	      $this->email->initialize($config);	      
	     if(!empty($email))
      	{
      	  $data['username'] = $username;
	      $this->email->from($this->config->item('from_email'), 'StyleCracker');	     
	      $this->email->to($email);       
	      $this->email->subject('[Urgent] Last day to confirm your SC Box order at Flat 50%');
	      $data['emailer'] = 1;	    
	      $message = $this->load->view('emails/scboxconfirm_email',@$data,true);
	      $this->email->message($message);
	  	  $sentmsg = $this->email->send();
	  	  if($sentmsg)
	  	  {
	  	  	return 'sent';
	  	  }else
	  	  {
	  	  	return 'notsent';
	  	  }
	  	}
	  }



}

