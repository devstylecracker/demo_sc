<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';

class Mobile_home extends TT_REST_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Schome_model');
		$this->load->model('Mobile_model_android');
		$this->load->model('Mobile_home_model');
		$this->load->model('Mobile_filter_model');
		$this->load->model('Mobile_look_model');
		$this->load->model('Mobile_filterbrand_model');
		$this->open_methods = array('product_page_post','filter_products_post','brand_page_filter_post'); 	

	}
	public function home_page_get(){
		$user_id = $this->get('user_id');
		$auth_token = $this->get('auth_token');
		$filter_selection = $this->get('gender');
		$res = array();
		// $res['slider_data'] = $this->Mobile_model_android->get_all_sliders();
		if($filter_selection == 'men'){
			$data_look = $this->Mobile_home_model->get_home_look('top_looks_men',$user_id);
			$res['top_categories'] = $this->Mobile_home_model->get_home_categories($filter_selection,$user_id);
			$res['top_brands'] = $this->Mobile_home_model->get_home_brands($filter_selection,$user_id);
			
		}else if($filter_selection == 'women'){
			$data_look = $this->Mobile_home_model->get_home_look('top_looks_women',$user_id);
			$res['top_categories'] = $this->Mobile_home_model->get_home_categories($filter_selection,$user_id);
			$res['top_brands'] = $this->Mobile_home_model->get_home_brands($filter_selection,$user_id);
			
		}else {
			$data_look = $this->Mobile_home_model->get_home_look('top_looks_all',$user_id);
			$res['top_categories'] = $this->Mobile_home_model->get_home_categories($filter_selection,$user_id);
			$res['top_brands'] = $this->Mobile_home_model->get_home_brands($filter_selection,$user_id);
		}
		$i=0;
		
		/*foreach($res['top_categories'] as $value){
		$category_img_url = '';
		if($value['type'] == 1) { 
                $category_img_url_array = @scandir('./assets/images/products_category/category/'.$value['cat_id'],true);
                $header_img = get_headers($this->config->item('products_category_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg");
                if(!empty($category_img_url_array)){
               
                  $category_img_url = $this->config->item('products_category_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg";
                }
                $cat_type = 'cat';
              }else if($value['type'] == 2) { 
                if($value['product_cat_id'] == WOMEN_CATEGORY_ID ){ $page_filter_name = 'women'; }else{ $page_filter_name ='men'; }
                $category_img_url_array = @scandir('./assets/images/products_category/sub_category/'.$value['cat_id'],true);
                $header_img = get_headers($this->config->item('products_sub_category_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg");
                if(!empty($category_img_url_array)){
               

                  $category_img_url = $this->config->item('products_sub_category_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg";
                }
                $cat_type = 'subcategory';
              }else if($value['type'] == 3) { 
			  if($value['product_cat_id'] == WOMEN_CATEGORY_ID ){ $page_filter_name = 'women'; }else{ $page_filter_name ='men'; }
                $category_img_url_array = @scandir('./assets/images/products_category/variation_types/'.$value['cat_id'],true);
                $header_img = get_headers($this->config->item('products_variation_types_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg");
                if(!empty($category_img_url_array)){
               
                  $category_img_url = $this->config->item('products_variation_types_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg";
                }
				$cat_type = 'variation';
              }else if($value['type'] == 4) { 
               if($value['product_cat_id'] == WOMEN_CATEGORY_ID ){ $page_filter_name = 'women'; }else{ $page_filter_name ='men'; }
                $category_img_url_array = @scandir('./assets/images/products_category/variation_values/'.$value['cat_id'],true);
                if(!empty($category_img_url_array)){
                $header_img = get_headers($this->config->item('products_variation_values_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg");
               
                  $category_img_url = $this->config->item('products_variation_values_img_url').$value['cat_id'].'/'.$value['cat_id'].".jpg";
                }
                $cat_type = 'variations';
              }
			  
			$res['top_categories'][$i]['url'] = $category_img_url;
			$res['top_categories'][$i]['cat_type'] = $cat_type;
			$res['top_categories'][$i]['share_url'] = base_url().'all-products/'.$filter_selection.'/'.$cat_type.'/'.$res['top_categories'][$i]['slug'];
			$i++;
		}*/	  
		$look_array = array(); $i = 0;
		// print_r($data_look);
		foreach($data_look as $val){
			$look_array[$i] = $this->Mobile_model_android->looks_by_look_id($val);
			$i++;
		}
		
		$res['top_looks']= $look_array;
		
		if(!empty($res)){
			$this->success_response($res);
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
		
	}	 
	
	public function product_page_get(){
		/*
		$data = array();
		$brand = ''; $color = ''; $cat = ''; $offset = 0; $search = ''; $is_brand=0; $is_recommended = 0;
		
		$search = $this->post('search');
		$is_recommended = $this->post('is_recommended');
		$cat = $this->post('cat');
		$new_limit = $this->post('limit');
		$new_offset = $this->post('offset');
		
		$new_limit = $this->checkLimit($new_limit);
		$new_offset = $this->checkOffset($new_offset);
		// echo "new_limit".$new_limit.'  '.'new_offset'.$new_offset;exit;
		$data['all_data'] = $this->Mobile_filter_model->get_data($cat,$brand,$color,'','','',$offset,'','','','','',$search,$is_recommended,$is_brand,$new_limit,$new_offset);
		*/
		
		$data = array();
		
		$user_id = $this->get('user_id');
		$auth_token = $this->get('auth_token');
		
        $cat = $this->input->get('cat');
        $brandId = $this->input->get('brandId');
        $colorId = $this->input->get('colorId');
        $lastCatId = $this->input->get('lastCatId');
        $priceId = $this->input->get('priceId');
        $min_price = $this->input->get('min_price');
        $max_price = $this->input->get('max_price');
        $isButton = $this->input->get('isButton');
        $sort_by = $this->input->get('sort_by');
        $attributesId = $this->input->get('attributesId');
        $sizeId = $this->input->get('sizeId');
        $search = $this->input->get('search');
        $is_recommended = $this->input->get('is_recommended');
		$new_limit = $this->get('limit');
		$new_offset = $this->get('offset');
		
		$new_limit = $this->checkLimit($new_limit);
		$new_offset = $this->checkOffset($new_offset);
		// echo "new_limit".$new_limit.'  '.'new_offset'.$new_offset;exit;
        if($isButton == 0){$min_price =0; $max_price=0; }
         $data['all_data'] = $this->Mobile_filter_model->get_data($cat,$brandId,$colorId,$priceId,'',$sort_by,0,$lastCatId,$min_price,$max_price,$attributesId,$sizeId,$search,$is_recommended,0,$new_limit,$new_offset,$user_id);
		
		$product_array = array();
			$i=0;
			foreach($data['all_data']['products'] as $row){
				$product_array[$i]=$this->Mobile_look_model->single_product_data($row['id']);	
				$i++;
			}
		$data['all_data']['products'] = $product_array;
		
		$data['all_data']['filter']['price_list'] = array('0','Below 500','500 - 1999','2000 - 3999','4000 +');
		if(!empty($data['all_data']['filter']['global_attributes'])){
            foreach($data['all_data']['filter']['global_attributes'] as $val){
				$i=0;
				foreach($this->Mobile_filter_model->attributes_values($val['id'],0,0) as $values){
					$data['all_data']['filter'][$val['attribute_slug']][$i]['id'] = $values['id'];
					$data['all_data']['filter'][$val['attribute_slug']][$i]['attr_value_name'] = $values['attr_value_name'];
					$i++;
				}
			}
		}
		
		if(!empty($data['all_data']['filter']['category_attributes'])){
            foreach($data['all_data']['filter']['category_attributes'] as $val){
				$i=0;
				foreach($this->Mobile_filter_model->attributes_values($val['id'],$data['all_data']['is_recommended'],$val['attribute_parent']) as $values){
					$data['all_data']['filter'][$val['attribute_slug']][$i]['id'] = $values['id'];
					$data['all_data']['filter'][$val['attribute_slug']][$i]['attr_value_name'] = $values['attr_value_name'];
					$i++;
				}
			}
		}
		
		// print_r($data);exit;
		if(!empty($data)){
			$this->success_response($data);
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
		
		
	}
	
	public function brand_page_filter_get(){
		/*
		$data = array();
		$brand = ''; $color = ''; $cat = ''; $offset = 0; $search = ''; $is_brand=0; $is_recommended = 0;
		
		$search = $this->post('search');
		$is_recommended = $this->post('is_recommended');
		$cat = $this->post('cat');
		$new_limit = $this->post('limit');
		$new_offset = $this->post('offset');
		
		$new_limit = $this->checkLimit($new_limit);
		$new_offset = $this->checkOffset($new_offset);
		// echo "new_limit".$new_limit.'  '.'new_offset'.$new_offset;exit;
		$data['all_data'] = $this->Mobile_filter_model->get_data($cat,$brand,$color,'','','',$offset,'','','','','',$search,$is_recommended,$is_brand,$new_limit,$new_offset);
		*/
		
		$data = array();
		
		$user_id = $this->get('user_id');
		$auth_token = $this->get('auth_token');
		
        $cat = $this->input->get('cat');
        $brandId = $this->input->get('brandId');
        $colorId = $this->input->get('colorId');
        $lastCatId = $this->input->get('lastCatId');
        $priceId = $this->input->get('priceId');
        $min_price = $this->input->get('min_price');
        $max_price = $this->input->get('max_price');
        $isButton = $this->input->get('isButton');
        $sort_by = $this->input->get('sort_by');
        $attributesId = $this->input->get('attributesId');
        $sizeId = $this->input->get('sizeId');
        $search = $this->input->get('search');
        $is_recommended = $this->input->get('is_recommended');
		$new_limit = $this->get('limit');
		$new_offset = $this->get('offset');
		
		$new_limit = $this->checkLimit($new_limit);
		$new_offset = $this->checkOffset($new_offset);
		// echo "new_limit".$new_limit.'  '.'new_offset'.$new_offset;exit;
        if($isButton == 0){$min_price =0; $max_price=0; }
		
		// $brand = $this->Mobile_filterbrand_model->get_brand_slug($brandId);
		$is_brand = 1;
		
		$data['all_data'] = $this->Mobile_filterbrand_model->get_data($cat,$brandId,$colorId,$priceId,'',$sort_by,0,$lastCatId,$min_price,$max_price,$attributesId,$sizeId,$search,$is_recommended,$is_brand,$new_limit,$new_offset,$user_id);

		$product_array = array();
			$i=0;
			foreach($data['all_data']['products'] as $row){
				$product_array[$i]=$this->Mobile_look_model->single_product_data($row['id']);	
				$i++;
			}
		$data['all_data']['products'] = $product_array;
		
		$data['all_data']['filter']['price_list'] = array('0','Below 500','500 - 1999','2000 - 3999','4000 +');
		if(!empty($data['all_data']['filter']['global_attributes'])){
            foreach($data['all_data']['filter']['global_attributes'] as $val){
				$i=0;
				foreach($this->Mobile_filterbrand_model->attributes_values($val['id'],0,0) as $values){
					$data['all_data']['filter'][$val['attribute_slug']][$i]['id'] = $values['id'];
					$data['all_data']['filter'][$val['attribute_slug']][$i]['attr_value_name'] = $values['attr_value_name'];
					$i++;
				}
			}
		}
		
		if(!empty($data['all_data']['filter']['category_attributes'])){
            foreach($data['all_data']['filter']['category_attributes'] as $val){
				$i=0;
				foreach($this->Mobile_filterbrand_model->attributes_values($val['id'],$data['all_data']['is_recommended'],$val['attribute_parent']) as $values){
					$data['all_data']['filter'][$val['attribute_slug']][$i]['id'] = $values['id'];
					$data['all_data']['filter'][$val['attribute_slug']][$i]['attr_value_name'] = $values['attr_value_name'];
					$i++;
				}
			}
		}
		
		// print_r($data);exit;
		if(!empty($data)){
			$this->success_response($data);
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
		
		
	}
	
	
	function checkLimit($limit){
		if($limit == '') $query_limit = ''; else $query_limit = " LIMIT $limit ";
		return $query_limit;
	}
	
	function checkOffset($offset){
		if($offset == '') $query_offset = ''; else $query_offset = " OFFSET $offset ";
		return $query_offset;
	}
	

}