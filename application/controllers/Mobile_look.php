<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';

class Mobile_look extends TT_REST_Controller 
{
  function __construct()
	{
		parent::__construct();
		$this->load->model('Schome_model');
		$this->load->model('Mobile_model');
		$this->load->model('Pa_model');
		$this->load->model('Mobile_pa_model');
		$this->open_methods = array('single_look_post','looks_by_stylist_post','textbox_search_post','looks_by_brand_post','discover_post');    
		//$this->open_methods = array('getuserinfo_get');
		 //$this->open_methods = array('pa');
		//$this->load->library('../controllers/Schome');
		//$this->open_methods = array('login_post');
	}
	 public function my_feed_post(){
		 
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		$limit = $this->post('limit');
		$offset = $this->post('offset');
        $filter = $this->post('filter');
		$is_up = $this->post('is_up');
		$bucket_id = $this->post('user_bucket_id');	
		$timestamp = $this->post('timestamp');	
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);		
		$filter_value = '1';
		 if(trim($bucket_id) == ''){
        $this->failed_response(1015, "Please enter the bucket_id");

      }
		if($timestamp == ''){ $timestamp = 0; }
		if($is_up == ''){ $is_up = 1; }
		if($filter == 'hot-right-now'){ $filter_value = '3'; }
		else if($filter == 'looks'){ $filter_value = '1'; }
		else if($filter == 'street-style'){ $filter_value = '2'; }
		else if($filter == 'sc-celeb-style'){ $filter_value = '4'; }
        else if($filter == 'sc-live-posts'){ $filter_value = '5'; }
		else if($filter == 'festive'){ $filter_value = '6'; }
		else if($filter == 'bridal'){ $filter_value = '7'; }
		$data = $this->Mobile_model->get_my_feed($user_id,$auth_token,$limit,$offset,$filter_value,$bucket_id,$timestamp,$is_up);
		/*
		if($data != NULL){
			//$data->user_id = $user_id;
			//$data->token = $auth_token;
			$this->success_response($data);
		}
		else	
			$this->failed_response(1001, "No Record(s) Found");
		 */
		 
		 if($data !=NULL){
	      	 $this->success_response($data);
		    }else{
				$res = array();
			 //$res = new stdClass();
			// $res['message'] = 'success';
	      	   $this->success_response($data);
		       //$this->failed_response(1001, "No Record(s) Found");
		    }
	 }
	 public function looks_by_brand_get(){
		  
	      $brand_id = $this->get('brand_id');
		  $limit = $this->get('limit');
		  $offset = $this->get('offset');
		  $limit = $this->checkLimit($limit);
		  $offset = $this->checkOffset($offset);		  
		  $data = $this->Mobile_model->get_looks_by_brand($brand_id,$limit,$offset);

		  if($data != NULL){
			$this->success_response($data);
		}
		else	
			//$this->failed_response(1001, "No Record(s) Found");
			$data = array();	
			$this->success_response($data);
	  }
	
	public function discover_get(){
		  
		  //echo ini_get("memory_limit")."\n";
          ini_set("memory_limit","-1");
		//echo ini_get("memory_limit")."\n";
			
	      $filter = $this->get('filter');
		  $timestamp = $this->get('timestamp');
		  $limit = $this->get('limit');
		  $offset = $this->get('offset');
		  $limit = $this->checkLimit($limit);
		  $offset = $this->checkOffset($offset);
		  $is_up = $this->get('is_up');
		  if($is_up == '') $is_up = 1;
		  
		  if($timestamp == ''){ $timestamp = 0; }
		  if($filter == 'hot-right-now'){ $filter_value = '3'; }
		  else if($filter == 'looks'){ $filter_value = '1'; }
		  else if($filter == 'street-style'){ $filter_value = '2'; }
		  else if($filter == 'sc-celeb-style'){ $filter_value = '4'; }
          else if($filter == 'sc-live-posts'){ $filter_value = '5'; }
		  else if($filter == 'festive'){ $filter_value = '6'; }
		  else if($filter == 'bridal'){ $filter_value = '7'; }
		  else if($filter == ''){ $filter_value = '0'; }
		  $data = $this->Mobile_model->get_discover($filter_value,$timestamp,$limit,$offset,$is_up);
		  
		  if($data != NULL || $data != ''){
			  
			$this->success_response($data);
		}
		else
			$data = array();	
			$this->success_response($data);
			//$this->failed_response(1001, "No Record(s) Found");
	
	  }
	  

	  
	  public function share_post(){
	    $user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
	    $look_id = $this->post('look_id');
        $action = $this->post('medium');
		$data = $this->Mobile_model->get_share($user_id,$auth_token,$look_id,$action);
		if($data === 1){
		$data = new stdClass();
		$data->message = 'success';
		$this->success_response($data);
		}else{
		$this->failed_response(1001, "No Record(s) Found");
		}		
	  
	  }
	 public function single_look_get(){
	  	$user_id = $this->get('user_id'); // added by Hari for Product redirection
		$single_look_id = $this->get('look_id');
		$data['look'] = (object)$this->Mobile_model->single_look_id_mobile($single_look_id);
         	$data['products'] = $this->sing_look_get($single_look_id,$user_id);		 
		 //$data = array_merge($data_arr,$data2);
		 //$dataresponse = new stdClass();
		// $dataresponse = (object)$data2;
		 $dataresponse = (object)$data;
		 if(($dataresponse != NULL)) { 
    		$this->success_response($dataresponse);
			}
			else{ 
				$this->failed_response(1001, "No Record(s) Found");
				// return '0'; 
			}
	 }
	 
	public function sing_look_get($look_id,$user_id)
	{
		if($look_id){
			$user_info = $this->Mobile_model->single_look_mobile($look_id);
			/*added for adding parameter in redirection start*/
			/*
			$count_products = count($user_info);
			for($i=0;$i<$count_products;$i++){
				$encode_product_id = $this->encryptOrDecryptMobile($user_info[$i]->product_id,'encrypt');
				if($user_info[$i]->redirect_sep != '' && $user_info[$i]->brand_id == '20424'){ //closet37
					$user_info[$i]->buy_now_url = $user_info[$i]->buy_now_url.$user_info[$i]->redirect_sep.'product_id='.$encode_product_id .'&u_id=' .$user_id.'&tracking=56274b42acf75&referer=true';
				}
				
				else if($user_info[$i]->redirect_sep != '' && $user_info[$i]->brand_id == '20936'){ //style fiesta
					$user_info[$i]->buy_now_url = $user_info[$i]->buy_now_url.$user_info[$i]->redirect_sep.'product_id='.$encode_product_id .'&u_id=' .$user_id.'&tracking=563c7d3dd40ce&referer=true';
				}   
				else if($user_info[$i]->redirect_sep != '' && $user_info[$i]->brand_id == '20933'){ 
					$user_info[$i]->buy_now_url = $user_info[$i]->buy_now_url.$user_info[$i]->redirect_sep.'product_id='.$encode_product_id.'&u_id='.$user_id;
				}
				else if($user_info[$i]->redirect_sep != '' && $user_info[$i]->brand_id == '22183'){ //The Muslin Bag this is code brand_id of 22183
					$user_info[$i]->buy_now_url = $user_info[$i]->buy_now_url.$user_info[$i]->redirect_sep.'scproductid='.$encode_product_id.'&u_id='.$user_id;
				}
				else if($user_info[$i]->redirect_sep != ''){ 
					$user_info[$i]->buy_now_url = $user_info[$i]->buy_now_url.$user_info[$i]->redirect_sep.'sc_productid='.$encode_product_id.'&u_id='.$user_id;
				}
				else{ 
					$user_info[$i]->buy_now_url = $user_info[$i]->buy_now_url;
				}
			}		
			/*code ends*/
			
			return $user_info;
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
		
	}
	public function looks_by_stylist_get(){
		$stylist_id = $this->get('stylist_id');
		$offset = $this->get('offset');
		$limit = $this->get('limit');
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);
		$res = $this->Mobile_model->looks_by_stylist_id_mobile($stylist_id,$offset,$limit);
		if($res !=NULL){
	      	 $this->success_response($res);
		    }else{
				$res = array();
			 //$res = new stdClass();
			// $res['message'] = 'success';
	      	   $this->success_response($res);
		       //$this->failed_response(1001, "No Record(s) Found");
		    }
			
			/*if($res == NULL){
			
		    }else{
				$res->message = 'success';
				//$res = array();
				$this->success_response($res);
			}*/
	}
	public function user_like_post(){
		$user_id = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$look_id = $this->post('look_id');		
		$res = $this->Mobile_model->user_like_mobile($user_id,$auth_token,$look_id);
		if($res === 1){
			
			$data = new stdClass();
			$data->message = 'success';
			$this->success_response($data);
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
	}
	public function textbox_search_get(){
		$user_search = urldecode($this->get('user_search'));
		$user_search = explode(' ',$user_search);
		$tag = urldecode($this->get('user_tag'));
		//echo $tag;exit;
		//$tags = urldecode($this->post('tag'));
		if($tag != NULL)
		{
			$user_search = explode(',',$tag);
		}
		
		// echo "<pre>";
		// print_r($user_search);
		// exit;
		$limit = $this->get('limit');
		$offset = $this->get('offset');		
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);
		$res = $this->Mobile_model->user_search_mobile($user_search,$limit,$offset);
		if($res !=NULL){
	      	//   $data = new stdClass();
		      // $data = (object)$res;
		      $this->success_response($res);
		    }else{
		       //$this->failed_response(1001, "No Record(s) Found");
			   $res = array();
			   $this->success_response($res);
		    }
	}
	
	public function multi_image_get(){
		$product_id = $this->get('product_id');
		$res = $this->Mobile_model->multi_image($product_id);
		if($res !=NULL){
			 $this->success_response($res);
		    }else{
			   $res = array();
			   $this->success_response($res);
			}
	}
	
	function checkLimit($limit){
		if($limit == '') $query_limit = ''; else $query_limit = " LIMIT $limit ";
		return $query_limit;
	}
	function checkOffset($offset){
		if($offset == '') $query_offset = ''; else $query_offset = " OFFSET $offset ";
		return $query_offset;
	}
	
	public function push_notification_get(){
		//print_r($_SERVER);exit;
		$myfile = @fopen("timestamp.txt", "r") or die("Unable to open file!");
		$timestamp = fread($myfile,filesize("timestamp.txt"));
		$res = $this->Mobile_model->push_look($timestamp);
		$timestamp = $res->timestamp;
		$file = fopen("timestamp.txt","w");
		$write =  fwrite($file,"$timestamp");
		if($res !=NULL){
	      	$ch = curl_init("https://www.stylecracker.com/silentpn.php");
			curl_exec($ch);
		    }else{		       
			   $res = array();
			   $this->success_response($res);
		    }
	 
	  }
	  
	  /*Added by Hari for product redirect encryption*/
	   function encryptOrDecryptMobile($mprhase, $crypt) {
	       $MASTERKEY = "STYLECRACKERAPI";
	       $td = mcrypt_module_open('tripledes', '', 'ecb', '');
	       $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
	       mcrypt_generic_init($td, $MASTERKEY, $iv);
	       if ($crypt == 'encrypt')
	       {
	           $return_value = base64_encode(mcrypt_generic($td, $mprhase));
	       }
	       else
	       {
	           $return_value = mdecrypt_generic($td, base64_decode($mprhase));
	       }
	       mcrypt_generic_deinit($td);
	       mcrypt_module_close($td);
	       return $return_value;
	  } 
}
?>