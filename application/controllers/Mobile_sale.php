<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';

class Mobile_sale extends TT_REST_Controller 
{
  function __construct()
	{
		parent::__construct();
		$this->load->model('Sale_model');
		$this->load->model('Mobile_look_model');
		$this->open_methods = array('');    
	}
	
	function sale_get(){
		$category = array();
		$category[0]['title'] = array('women');
		$category[1]['title'] = array('men');
		$category[0]['value'] = array('1');
		$category[1]['value'] = array('12');
		$data['all_data']['filter']['category'] = $category;
		$data['all_data']['filter']['paid_brands'] = $this->Sale_model->getSaleBrands();
		$products = $this->Sale_model->getSaleProducts(0,'','',0);
		
		$product_array = array();
		$i=0;
			foreach($products as $row){
			$product_array[$i]=$this->Mobile_look_model->single_product_data($row['id']);	
			$i++;
			}
		$data['all_data']['products'] = $product_array;	
			if($data !=NULL){
				$this->success_response($data);
			}else{
				$res = array();
				$this->success_response($data);
			}
		
	}
	
	function new_sale_post(){
		echo "comming";exit;
		$data = array();
		$category = '';
		$brands = $this->get('brands');
		$sort_by = $this->get('sort_by');
		$limit = $this->get('limit');
		$offset = $this->get('offset');
		$products = $this->Mobile_look_model->getSaleProducts($offset,$category,$brands,$sort_by,$limit);
		$data['all_data']['paid_brands'] = $this->Sale_model->getSaleBrands();
		
		$product_array = array();
		$i=0;
		foreach($products as $row){
			$product_array[$i]=$this->Mobile_look_model->single_product_data($row['id']);	
			$i++;
		}
		$data['all_data']['products'] = $product_array;
		
		if($data !=NULL){
			$this->success_response($data);
		}else{
			$res = array();
			$this->success_response($data);
		}
		
	}
	
	function load_more_sale_products_get(){
      $group_no = $this->get('group_no');
      $category = $this->get('category');
      $brands = $this->get('brands');
      $product_sort_by_sale = $this->get('product_sort_by_sale');
      $products = $this->Sale_model->getSaleProducts($group_no,$category,$brands,$product_sort_by_sale);
	  
	  $product_array = array();
		$i=0;
			foreach($products as $row){
			$product_array[$i]=$this->Mobile_look_model->single_product_data($row['id']);	
			$i++;
			}
		$data['all_data']['products'] = $product_array;	
		
		
		if($data !=NULL){
			$this->success_response($data);
		}else{
			$res = array();
			$this->success_response($data);
		}
			
    }
	
	function filtersProducts_get(){
      $category = $this->get('category');
      $brands = $this->get('brands');
      $product_sort_by_sale = $this->get('product_sort_by_sale');
	  $group_no = $this->get('group_no');
	  
      $products = $this->Sale_model->getSaleProducts($group_no,$category,$brands,$product_sort_by_sale);
	  
      $product_array = array();
		$i=0;
			foreach($products as $row){
			$product_array[$i]=$this->Mobile_look_model->single_product_data($row['id']);	
			$i++;
			}
		$data['all_data']['products'] = $product_array;	
		
		
		if($data !=NULL){
			$this->success_response($data);
		}else{
			$res = array();
			$this->success_response($data);
		}
		
    }
	 
}
?>