<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* code for checking error */
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
class Scbox_place_order extends MY_Controller {
	

	function __construct(){
		parent::__construct();
		$this->load->model('Schome_model');
		$this->load->model('Pa_model');		
		$this->load->library('email');
		$this->load->driver('cache');
		$this->load->library('user_agent');
		$this->load->library('curl');	
    $this->load->model('Cart_model');
    $this->load->model('cartnew_model');
    $this->load->model('product_desc_model');
    $this->load->model('User_info_model');
    $this->load->model('Scborough_model');
    $this->config->load('instagram');
  }


  public function index($data=null){
      $userdata = array();
      echo 'Scbox_place_order<br/>';
      $fp = fopen('php://input', 'r');
      $rawData = stream_get_contents($fp);
      $decode_rawData = json_decode($rawData,true);
       // echo "<pre>";
       // print_r($decode_rawData);

      $scbox_data = unserialize(SCBOX_PACKAGE);
      
      $userdata = $decode_rawData;

      foreach($scbox_data as $val)
      {
          if($val->price==$userdata['scbox_price'])
          {
            $userdata['scbox_objectid'] = $val->id;
            $userdata['scbox_productid'] = $val->productid;
            $userdata['scbox_pack'] = $val->packname;           
          }
      }
      
      //echo $userdata['user_pincode'];exit;
     
     
      //$userdata = json_decode($data);
      //echo '<pre>';print_r($userdata);exit;
      $pincode_data = $this->getpincodeinfo($userdata['user_pincode']);
      $userdata['user_stateid'] = $pincode_data['id'];
      //echo '<pre>';print_r($pincode_data);exit;
      $resultscbox = $this->savescbox($userdata);

      $data['userdata'] = $userdata;    

      if($resultscbox=='success')
      {
        $userid = $this->cartnew_model->emailUnique($userdata['user_emailid'])[0]['id'];
        $userdata['user_id'] = $userid;
        $data['userdata']['user_id'] = $userid; 
        if($this->Cart_model->check_order_exist('SC'.$data['userdata']['wp_order_id'])=='false')
        {          
           $result = $this->place_scbox_order($data);
        }     
       
        echo $result;exit;
      }

     // echo '<pre>';print_r($userdata);exit;
  }

  public function signup($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$sc_gender, $json_array = array()){
    //echo $medium.'--'.$scusername.'--'.$scemail_id.'---'.$scpassword.'--'.$ip.'--'.$role.'--'.$sc_gender;exit;  
      $scusername = $scusername.date('Y-m-d h:i:s');
      $res = $this->User_info_model->registerUser($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$sc_gender,$json_array);
     
      if($scemail_id!='')
      {
        $this->Schome_model->update_couponcode($scemail_id,'Welcome10');
      }
      if($res){ return $res; }else{ return '0'; }
  } 
	
  public function place_scbox_order($data)
  { //echo '<pre>';print_r($data);exit;
    $orderdata = array();
    $objectid = $data['userdata']['scbox_objectid'];
    
    if(!isset($_COOKIE['SCUniqueID'])) {
      $uniquecookie = '';
    }else{       
      $uniquecookie = $_COOKIE['SCUniqueID'];
    }

    if ($this->agent->is_browser())
    {
        $agent = 'Computer '.$this->agent->browser().' '.$this->agent->version();
    }
    elseif ($this->agent->is_robot())
    {
        $agent = 'Robot '.$this->agent->robot();
    }
    elseif ($this->agent->is_mobile())
    {
        $agent = 'Mobile '.$this->agent->mobile();
    }
    else
    {
        $agent = 'Unidentified';
    }

    $platform_info = $this->agent->platform();

    //echo '<pre>';print_r($data);
    $scbox_data = unserialize(SCBOX_PACKAGE);
    //$pid = $this->uri->segment(2);

    setcookie('discountcoupon_stylecracker', '', -1, "/");

    $scboxprice = $data['userdata']['scbox_price'];

    $productsize_arr = unserialize(SCBOX_SIZE);
    $productsize = $productsize_arr[0]->id!=''?$productsize_arr[0]->id:'2';
    $userid = $data['userdata']['user_id'];
    $productid = $this->Pa_model->get_scboxdata($data['userdata']['scbox_objectid'],'scbox_productid')->productid;  
    $lookid = 0; 
    $packageid = @$data['userdata']['scbox_pack'];
        
    //COD:1  
    $username = preg_replace('!\s+!', ' ', $data['userdata']['user_name']);  
    $username = explode(' ',$data['userdata']['user_name']);       
    $firstname = @$username[0];
    $lastname = @$username[1];

     $billing_first_name = $firstname;
     $billing_last_name = $lastname;
     $billing_mobile_no = $data['userdata']['user_mobile'];
     $billing_pincode_no = $data['userdata']['user_pincode'];
     $billing_address = $data['userdata']['user_shipaddress'];
     $billing_city = $data['userdata']['user_city'];
     $billing_state = $data['userdata']['user_stateid'];
     $shipping_first_name = $firstname;
     $shipping_last_name = $lastname;
     $shipping_mobile_no = $data['userdata']['user_mobile'];
     $shipping_pincode_no = $data['userdata']['user_pincode'];
     $shipping_address = $data['userdata']['user_shipaddress'];
     $shipping_city = $data['userdata']['user_city'];
     $shipping_state = $data['userdata']['user_stateid'];
     $cart_email = $data['userdata']['user_emailid'];
     $pay_mod = $data['userdata']['paymode'];
     $mihpayid = $data['userdata']['mihpayid'];
     $txnid = $data['userdata']['txnid'];   
     $payudiscount = $data['userdata']['payudiscount'];
     $discountsd = $data['userdata']['discountcoupon_stylecracker'];

      if($billing_first_name!='' && $billing_last_name!='' && $billing_mobile_no!='' && $billing_pincode_no!='' && $billing_address!='' && $billing_city!='' && $billing_state!='' && $shipping_first_name!='' && $shipping_last_name!='' && $shipping_mobile_no!='' && $shipping_pincode_no!='' && $shipping_address!='' && $shipping_city!='' && $shipping_state!='' && $cart_email!='' && $pay_mod!=''){
        
        $orderdata['user_info'] = array('billing_first_name'=>$billing_first_name,'billing_last_name'=>$billing_last_name,'billing_mobile_no'=>$billing_mobile_no,'billing_pincode_no'=>$billing_pincode_no,'billing_address'=>$billing_address,'billing_city'=>$billing_city,'billing_state'=>$billing_state,'shipping_first_name'=>$shipping_first_name,'shipping_last_name'=>$shipping_last_name,'shipping_mobile_no'=>$shipping_mobile_no,'shipping_pincode_no'=>$shipping_pincode_no,'shipping_address'=>$shipping_address,'shipping_city'=>$shipping_city,'shipping_state'=>$shipping_state,'cart_email'=>$cart_email,'pay_mod'=>$pay_mod,'existing_shipping_add'=>0);

        $orderdata['cart_info'] = $this->cartnew_model->get_scboxcart($userid);
       //echo '<pre>';print_r($orderdata);exit;
        $orderdata['coupon_code'] = $discountsd;

        $p_price = 0;  $orderdata['referral_point']=0;

        
      if(!empty($orderdata['cart_info'])){
        foreach($orderdata['cart_info'] as $val){
          $p_price = $p_price + $val['discount_price'];
        }
      }

        if(isset($discountsd) && $discountsd!=''){ 
          $orderdata['coupon_code_msg'] = $this->cartnew_model->check_coupon_exist(strtolower($discountsd),'','scboxcart');
          
          $p_price = $p_price-$data['coupon_code_msg']['coupon_discount_amount'];
        }else
        {
          $orderdata['coupon_code_msg']['coupon_discount_amount'] = 0;
        }    
        

        $order_unique_no = $this->cartnew_model->stylecracker_place_order($orderdata);
        
        if($order_unique_no > 0){
          //$user_id = $this->session->userdata('user_id');

          $points = $this->cartnew_model->get_refer_point(); 
                          if($points == 1 && $p_price >=MIN_CART_VALUE){
                              if(isset($_COOKIE['is_redeem_point']) == true && $_COOKIE['is_redeem_point'] == 1){
                                  $this->cartnew_model->redeem_points($order_unique_no);
                              }
                          }

          $this->cartnew_model->update_order_id_in_cart($order_unique_no);

          if($pay_mod=='online')
          {
              $data['stylecracker'] = 1;
              $data['mihpayid'] = $mihpayid;
              $data['txnid'] = $txnid;
              $data['order_unique_no'] = $order_unique_no; 
              $data['payudiscount'] = $payudiscount; 
              $data['discountcoupon_stylecracker'] = $discountsd;   
              $this->cartnew_model->change_order_status($data);                  
          }
          $orderdata['brand_price'] = $this->cartnew_model->su_get_order_price_info($order_unique_no);
            $user_id = $orderdata['brand_price'][0]['user_id'];
            $this->updateCouponInc($order_unique_no);
          //$this->orderPlaceMessage_sc($user_id,$order_unique_no,$billing_first_name,$billing_last_name);
          if(@$data['userdata']['wp_order_id']!='')
          {
            $resultupdate = $this->Cart_model->update_order_displayno($order_unique_no,$data['userdata']['wp_order_id']);
          }
         
          echo $order_unique_no;
         // $this->ordersuccessOrder($order_unique_no,$data['userdata']['scbox_objectid']);
          
        }

      }else
      {
        
      }
  }

  function updateCouponInc($order_unique_no){
    $order_unique_no = $this->cartnew_model->updateCouponInc($order_unique_no);
  }

  function getEmailAddress($user_id){
      $res = $this->cartnew_model->get_user_info($user_id);
      if(!empty($res)){
        return $res[0]['email'];
      }else{ return ''; }
  }

    function savescbox($userdata)
    {

       $brand_list = '';$arr_answers = array();$question_id='';  $i=0; 
       $objectid = $userdata['scbox_objectid'];
       $user_id = $userdata['user_id'];
       $gender = $userdata['scbox_gender'];  
       $scboxprice = $userdata['scbox_price']; 
       $utm_source = $userdata['utm_source'];
       $utm_medium = $userdata['utm_medium'];
       $scbox_page = $userdata['scbox_page'];

       $utm_campaign = $userdata['utm_campaign'];
       $scbox_style_new = array();
           
        $scusername = $userdata['firstname'].' '.$userdata['lastname'];       
        $scusername = preg_replace('/\s+/', ' ', trim($scusername));
        $username = explode(' ',trim($scusername));       
        $json_array['firstname'] = ucwords(strtolower(@$userdata['firstname']));
        $json_array['lastname'] = ucwords(strtolower(@$userdata['lastname']));
        $scemail_id = $userdata['user_emailid'];
        $scbox_mobile =  $userdata['user_mobile'];       
        $scpassword = 'scbox-'.$username[0];
        $sc_gender = $userdata['scbox_gender'];        
        $ip = $userdata['ip_address'];
        //$birthdate = $this->input->post('scbox_age');      
        //$json_array['age_range'] = $this->User_info_model->get_userage_range($birthdate,$sc_gender);
        $json_array['age_range'] = '';
        $json_array['scmobile'] = $userdata['user_mobile'];
        $json_array['question_id'] = $sc_gender==1 ? 8:17;
        $scbox_brandpref = '';
         if(!empty($scbox_brandpref)){ $brand_list = $scbox_brandpref; }else $brand_list = ''; 


        if($scusername!='' && $scemail_id!='' && $scpassword!='' && $sc_gender!='')
          {
              $webres = $this->signup('website',$scusername,$scemail_id,$scpassword,$ip,2,$sc_gender,$json_array);
          }
          
          $user_id = ($this->session->userdata('user_id')!='') ? $this->session->userdata('user_id') : 0; 
        
          $arr_answers[4]['question'] =  'scbox_brand';
          $arr_answers[4]['answer_id'] =  $brand_list;

          $arr_answers[5]['question'] =  'user_name';
          $arr_answers[5]['answer_id'] =  ($userdata['user_name']!='') ? $userdata['user_name'] : '';
         
          $arr_answers[6]['question'] =  'user_gender';
          $arr_answers[6]['answer_id'] =  (@$userdata['scbox_gender']!='') ? $userdata['scbox_gender'] : '';
         
          $arr_answers[7]['question'] =  'user_emailid';
          $arr_answers[7]['answer_id'] =  (@$userdata['user_emailid']!='') ? $userdata['user_emailid'] : '';

          $arr_answers[8]['question'] =  'user_dob';
          $arr_answers[8]['answer_id'] =  (@$userdata['scbox_age']!='') ? $userdata['scbox_age'] : '';
        
          $arr_answers[9]['question'] =  'user_mobile';
          $arr_answers[9]['answer_id'] =  (@$userdata['scbox_mobile']!='') ? $userdata['scbox_mobile'] : '';
        
          $arr_answers[10]['question'] =  'user_shipaddress';
          $arr_answers[10]['answer_id'] =  (@$userdata['scbox_shipaddress']!='') ? $userdata['scbox_shipaddress'] : '';
        
          $arr_answers[11]['question'] =  'user_pincode';
          $arr_answers[11]['answer_id'] =  (@$userdata['scbox_pincode']!='') ? $userdata['scbox_pincode'] : '';
         
          $arr_answers[12]['question'] =  'user_city';
          $arr_answers[12]['answer_id'] =  (@$userdata['scbox_city']!='') ? $userdata['scbox_city'] : '';
        
          $arr_answers[13]['question'] =  'user_state';
          $arr_answers[13]['answer_id'] =  (@$userdata['scbox_state']!='') ? $userdata['scbox_state'] : '';
        
          $arr_answers[14]['question'] =  'user_likes';
          $arr_answers[14]['answer_id'] =  (@$userdata['scbox_likes']!='') ? $userdata['scbox_likes'] : '';
         
          $arr_answers[15]['question'] =  'user_dislikes';
          $arr_answers[15]['answer_id'] =  (@$userdata['scbox_dislikes']!='') ? $userdata['scbox_dislikes'] : '';

          $arr_answers[25]['question'] =  'user_id';
          $arr_answers[25]['answer_id'] =  (@$userdata['scbox_userid']!='') ? $userdata['scbox_userid'] : '';
          
          $arr_answers[26]['question'] =  'scbox_pack';
          $arr_answers[26]['answer_id'] =  (@$userdata['scbox_package']!='') ? $userdata['scbox_package'] : '';
          
          $arr_answers[27]['question'] =  'scbox_price';
          $arr_answers[27]['answer_id'] =  (@$userdata['scbox_price']!='') ? $userdata['scbox_price'] : '';
         
          $arr_answers[28]['question'] =  'scbox_productid';
          $arr_answers[28]['answer_id'] =  (@$userdata['scbox_productid']!='') ? $userdata['scbox_productid'] : '';
        
          $arr_answers[29]['question'] =  'scbox_objectid';
          $arr_answers[29]['answer_id'] =  (@$userdata['scbox_objectid']!='') ? $userdata['scbox_objectid'] : '';

          $arr_answers[30]['question'] =  'utm_source';
          $arr_answers[30]['answer_id'] =  (@$userdata['utm_source']!='') ? $userdata['utm_source'] : '';

          $arr_answers[31]['question'] =  'utm_medium';
          $arr_answers[31]['answer_id'] =  (@$userdata['utm_medium']!='') ? $userdata['utm_medium'] : '';

          $arr_answers[32]['question'] =  'utm_campaign';
          $arr_answers[32]['answer_id'] =  (@$userdata['utm_campaign']!='') ? $userdata['utm_campaign'] : '';

         foreach($arr_answers as $key=>$json){
              $question = $json['question'];
              $question_id = @$data_answers[$question];
              $answer_id = $json['answer_id'];
              $this->Pa_model->save_scboxuser_answer($question_id,$answer_id,$user_id,$objectid,$question);
            }

          setcookie('billing_address',$this->input->post('scbox_shipaddress'),time() + 86400, "/"); 

          // Add the scbox product to cart
          $this->add_scboxordertocart($objectid,1,$scboxprice,$utm_source,$utm_medium,$utm_campaign);
          // Add the scbox product to cart
          
        return 'success';
    }

     function getpincodedata()
    {
      $pincode = $this->input->post('scbox_pincode');
      $data = $this->Schome_model->getpincodedata($pincode);
      if(!empty($data))
      {
         echo '{"id" : "'.(int)trim(@$data[0]['id']).'", "statename" : "'.trim(strip_tags(@$data[0]['state_name'])).'", "city" : "'.trim(@$data[0]['city']).'"}';
       }else
       {
          echo 0;
       }  
     
    }

    public function login($logEmailid,$logPassword){

         if(trim($logEmailid) == ''){
            echo "Please enter your email id";

          }else if(trim($logPassword) == ''){
            echo "Please enter the Password";

          }else{
           $result = $this->Scborough_model->login_user($logEmailid,$logPassword,$medium='website',$type='');
          }
    }
    
  function escapeJsonString($value) { # list from www.json.org: (\b backspace, \f formfeed)
      $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
      $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
      $result = str_replace($escapers, $replacements, $value);
      return $result;
  }

  function checkuserdata()
  {
    $gender = '';
    $useremail = $this->input->post('useremail');
    $user_data =  $this->Cart_model->getUserDataByEmail($useremail);

    if(!empty($user_data))
    {
      $gender = $user_data[0]['gender']!='' ? $user_data[0]['gender'] : 0;
    }else
    {
      $gender = 0;
    }
    echo $gender;
  }

   function add_scboxordertocart($objectid,$quantity,$scboxprice,$sc_source=null,$sc_medium=null,$sc_campaign=null)
  {
        $orderdata = array();          
        //$objectid = $this->input->post('scbox_objectid');
        //$quantity = $this->input->post('scbox_quantity');

       // $resultcart = $this->cartnew_model->deleteCart();
        if(!isset($_COOKIE['SCUniqueID'])) {
          $uniquecookie = '';
        }else{       
          $uniquecookie = $_COOKIE['SCUniqueID'];
        }

        if ($this->agent->is_browser())
        {
            $agent = 'Computer '.$this->agent->browser().' '.$this->agent->version();
        }
        elseif ($this->agent->is_robot())
        {
            $agent = 'Robot '.$this->agent->robot();
        }
        elseif ($this->agent->is_mobile())
        {
            $agent = 'Mobile '.$this->agent->mobile();
        }
        else
        {
            $agent = 'Unidentified';
        }

        $platform_info = $this->agent->platform();
       
        $scbox_data = unserialize(SCBOX_PACKAGE);
      
        //$scboxprice = $this->input->post('scbox_price');
        $productsize_arr = unserialize(SCBOX_SIZE);
        $productsize = $productsize_arr[0]->id!=''?$productsize_arr[0]->id:'2';
        //$userid = $data['userid'];
        $userid = $this->session->userdata('user_id');        
        $productid = $this->Pa_model->get_scboxdata($objectid,'id')->productid; 
      
        $lookid = 0;  
        $packageid = $this->Pa_model->get_scboxdata($objectid,'id')->packname;          
        if($packageid == 'package4')
        {
          $this->Cart_model->updateproductprice($productid,$scboxprice);
        }

        $sc_data = array();   
        $sc_data['utm_source'] = @$_COOKIE['sc_source'];
        $sc_data['utm_medium'] = @$_COOKIE['sc_medium'];
        $sc_data['utm_campaign'] = @$_COOKIE['sc_campaign'];
        
        //echo '<pre>';print_r($productsize);exit;
        //echo $uniquecookie.'==,=='.$productid.'=,='.$userid.'=,='.$productsize.'=,='.$agent.'=,='.$platform_info.'=,='.$lookid;
        //$res = $this->Cart_model->update_cart($uniquecookie,$productid,$userid,$productsize,$agent,$platform_info,$lookid);    
        $res = $this->Cart_model->update_scboxcart($uniquecookie,$productid,$userid,$productsize,$agent,$platform_info,$quantity,$scboxprice,$sc_data);   
  }

   function getpincodeinfo($pincode)
  {   
      $pincode_data = array();   
      $data = $this->Schome_model->getpincodedata($pincode);
      $pincode_data['id'] = (int)trim($data[0]['id']);
      $pincode_data['statename'] = trim($data[0]['state_name']);
      $pincode_data['city'] = trim($data[0]['city']);
     return $pincode_data;
  }
  

}
