<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

defined('BASEPATH') OR exit('No direct script access allowed');

class Schome extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
       parent::__construct();
       $this->load->model('Schome_model');
       $this->load->model('home_model');
       $this->load->model('Pa_model');
	   $this->load->model('Product_desc_model');
       $this->load->library('bitly');
       $this->load->library('email');
       $this->load->driver('cache');
       $this->load->library('user_agent');
       $this->load->library('curl');
   	}

  public function index(){
      if(!$this->session->userdata('user_id')){

        if($this->input->cookie('34b07cba21adf4cd95d35c1c0bd47ff0') && $this->input->cookie('1c0253b55e82e53890985a837274811b')){
          
          $logEmailid = $this->encryptOrDecrypt($this->input->cookie('34b07cba21adf4cd95d35c1c0bd47ff0'),'');
          $logPassword = $this->encryptOrDecrypt($this->input->cookie('1c0253b55e82e53890985a837274811b'),'');
          $result = $this->Schome_model->login_user(trim($logEmailid),trim($logPassword));
        }
        if(!$this->session->userdata('user_id')){
          $data = array();
          $this->seo_title = $this->lang->line('seo_title');
          $this->seo_desc = $this->lang->line('seo_desc');
          $this->seo_keyword = $this->lang->line('seo_keyword');
          $data['products'] = $this->home_model->get_latest_home_page_products();
          $this->load->view('common/header_view');
          $this->load->view('home/home_new',$data);
          $this->load->view('common/footer_view');
        }else{
          header("Location: ".base_url()); 
          exit;
        }
        //header("Location: ".base_url()."home");
        //exit;
      }else{
      $data = array();
      $filter_type = $filter_type = '';
      $this->seo_title = $this->lang->line('seo_title');
      $this->seo_desc = $this->lang->line('seo_desc');
      $this->seo_keyword = $this->lang->line('seo_keyword');
      $this->load->helper('cookie');
	  if(isset($_REQUEST['utm_source'])){
	 $utm_source = substr($_REQUEST['utm_source'],strpos($_REQUEST['utm_source'],"utmcsr="));
	 $array_field = array('utm_source' => $utm_source,'utm_source1' => trim($utm_source));
	 $this->session->set_userdata($array_field);
	 }

      if(($this->uri->segment(1)) != "" && ($this->uri->segment(1)) != "all"){
        setcookie('SCGender', $this->uri->segment(1), time() + ((86400 * 2) * 30), "/", "",  0);
        $filter_selection = $this->uri->segment(1);
        $filter_type = strtolower($this->uri->segment(1));
        $this->seo_canonical = base_url().$this->uri->segment(1);
      }else if($this->uri->segment(1) == "all"){
		$this->seo_canonical = base_url();
        setcookie('SCGender', $this->uri->segment(1), time()-3600, "/", "",  0);
      }else if(isset($_COOKIE['SCGender'])){
        
        $filter_selection = $_COOKIE['SCGender'];
        $filter_type = strtolower($_COOKIE['SCGender']);
      }

      if(isset($_COOKIE['SCGender'])){
        $uri = $this->uri->segment(1);
        if($uri != $filter_type && $uri==''){
          redirect($filter_type);
        }
      }
      

      if($filter_type == ''){ $filter_selection = 'all'; }
      else if($filter_type == 'all'){ $filter_selection = 'all'; }
      else if($filter_type == 'men'){ $filter_selection = 'men'; }
      else if($filter_type == 'women'){ $filter_selection = 'women'; }      
     
     if($this->session->userdata('gender') == 1) { $filter_selection = 'women'; }
      if($this->session->userdata('gender') == 2) { $filter_selection = 'men'; }

    if ( $this->session->userdata('user_id') >0 || $this->cache->file->get('filter_type_'.$filter_selection) == FALSE ) {
        
          $data['top_looks'] = $this->home_model->get_home_look($filter_selection);
          $data['top_categories'] = $this->home_model->get_home_categories($filter_selection);
          $data['top_brands'] = $this->home_model->get_home_brands($filter_selection);
          $data['top_products'] = $this->home_model->get_home_products($filter_selection);
          $data['page_filter_name'] =  $filter_selection;

          $filter_type1 = $filter_selection;
          $this->cache->file->save('filter_type_'.$filter_selection, $data,500000000000000000);
       
      }else{
          
          $data = $this->cache->file->get('filter_type_'.$filter_selection);
      }
      
       $slider_data = $this->Schome_model->get_all_sliders();
       $data['slider_data'] = $slider_data;
	if($_GET['test'] == 1){
		//echo "<pre>";print_r($data);exit;
	}
      $this->load->view('common/header_view');
      $this->load->view('common/slider_view',$data);
      $this->load->view('schome_view',$data);
      $this->load->view('common/footer_view');
    }

  }

  public function looks(){
    $data = array();

    $filter_type = strtolower(trim($this->uri->segment(3)));
    if($filter_type == ''){ $filter_selection = 'all'; }
    else if($filter_type == 'men'){ $filter_selection = 'men'; }
    else if($filter_type == 'women'){ $filter_selection = 'women'; }


    $offset = $this->uri->segment(2)!='' && $this->uri->segment(2)>0 ? $this->uri->segment(2) : 1;

    $homehtml = '';
    $filter_value = '';
    $pagination = '';
    $adjacents = 3;

    $stylist_offset = 0;
    $street_offset = 0;
    $blog_offset = 0;
    $filter = 0;
    $prev = 0;
    $next = 1;  
    $discount_tag = '';

    if($filter === 'hot-right-now'){ $filter_value = '3'; }
    else if($filter === 'looks'){ $filter_value = '1'; }
    else if($filter === 'street-style'){ $filter_value = '2'; }
    else if($filter === 'sc-celeb-style'){ $filter_value = '4'; }
    //else if($filter === 'sc-live-posts'){ $filter_value = '5'; }
    else if($filter === 'festive'){ $filter_value = '6'; }
    else if($filter === 'bridal'){ $filter_value = '7'; }

    if ( ! $homehtml1 = $this->cache->get('homehtml1')) {
    $looks = $this->Schome_model->get_homepage_looks($stylist_offset,$street_offset,$blog_offset,$filter_value);

    $length = array(4,8);

    $target_array = array();
    if(!empty($looks['stylist_look'])){
      foreach($length as $i) {
        $target_array['stylist_look'][] = array_splice($looks['stylist_look'], 0, $i);
    }}


    if(!empty($target_array['stylist_look'][0])){ $i = 0;
      foreach($target_array['stylist_look'][0] as $val){
        if($this->look_hv_discount($val['id']) == 1)
        {
          $discount_tag ='<div class="item-tag-sale"></div>';
        }else
        {
            $discount_tag = '';
        }
        $short_url = base_url().'looks/look-details/'.$val['slug'];
        $img_title_alt = str_replace('#','',$val['name']);
        $homehtml = $homehtml.'<div class="col-md-4 col-sm-6 grid-item">';
        $street_style_class = $val['look_type'] == 2 ? "item-street-style" : "";
        $product_style_class = $val['look_type'] == 3 ? "item-product" : "";
        $celeb_style_class = $val['look_type'] == 4 ? "item-celeb-style" : "";

        $homehtml = $homehtml.'<div class="item '.$street_style_class.' '.$product_style_class.' '.$celeb_style_class.'">';
        $homehtml = $homehtml.'<div class="item-img item-hover">';
          if($val['look_type'] == 1){
        $homehtml = $homehtml.'<a onclick="_targetClick('.$val['id'].',\'stylist\')" href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_look_image_url').$val['image'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker"  /></a>'.$discount_tag;
          $img_url = $this->config->item('sc_look_image_url').$val['image'];
          }else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
        $homehtml = $homehtml.'<a onclick="_targetClick('.$val['id'].',\'stylist\')" href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_street_style_image_url').$val['look_image'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker"  /></a>'.$discount_tag;
          $img_url = $this->config->item('sc_street_style_image_url').$val['look_image'];
        }else if($val['look_type'] == 3){
        $homehtml = $homehtml.'<a onclick="_targetClick('.$val['id'].',\'stylist\')" href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_promotional_look_image').$val['product_img'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker" /></a>'.$discount_tag;
          $img_url = $this->config->item('sc_promotional_look_image').$val['product_img'];
        }
        $homehtml = $homehtml.'<div class="item-look-hover">';
        $homehtml = $homehtml.'<a target="_blank" href="'.base_url().'looks/look-details/'.$val['slug'].'" onclick="_targetClick('.$val['id'].',\'stylist\')"><span class="btn btn-primary"> Get This </span></a>';
        $homehtml = $homehtml.'</div>';
        $homehtml = $homehtml.'</div>';
        $homehtml = $homehtml.'<div class="item-desc">';
        $homehtml = $homehtml.'<div class="item-title">';
        $homehtml = $homehtml.'<a target="_blank" href="'.base_url().'looks/look-details/'.$val['slug'].'" onclick="_targetClick('.$val['id'].',\'stylist\')" title="'.$val['name'].'">'.$val['name'].'</a>';
        $homehtml = $homehtml.'</div>';
        $homehtml = $homehtml.'<div class="item-media">';
        $homehtml = $homehtml.'<ul class="social-buttons">';
        $homehtml = $homehtml.'<li><a onClick="_targetFacebookShare(\''.$val['name'].'\',\''.base_url().'looks/look-details/'.$val['slug'].'\',\''.$this->config->item('sc_look_image_url').$val['image'].'\',\''.$val['id'].'\');"><i class="fa fa-facebook"></i></a></li>';
        $homehtml = $homehtml.'<li><a onclick="_targetTweet(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\');"><i class="fa fa-twitter"></i></a></li>';
        $homehtml = $homehtml.'<li><a onclick="_targetPinterest(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\',\''.$img_url.'\');"><i class="fa fa-pinterest"></i></a></li>';
       
        $homehtml = $homehtml.'</ul>';
        $homehtml = $homehtml.'<div class="user-likes">';
        if($this->session->userdata('user_id')){
          if($this->get_look_fav($val['id']) == 0){
            $homehtml = $homehtml.'<a target="_blank" title="Add to Wish List" id="whislist'.$val['id'].'" onclick="add_to_wishlist('.$val['id'].');"><i class="ic sc-love"></i></a>';
          }else{
            $homehtml = $homehtml.'<a target="_blank" title="Remove from Wish List" id="whislist'.$val['id'].'" onclick="remove_from_wishlist('.$val['id'].');"><i class="ic sc-love active"></i></a>';
          }
        }else{
          $homehtml = $homehtml.'<a target="_blank" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a>';
        }
        $homehtml = $homehtml.'</div>';
        $homehtml = $homehtml.'</div>';
        $homehtml = $homehtml.'</div>';
        $homehtml = $homehtml.'</div>';


        $homehtml = $homehtml.'</div>';

        $i++; } }

        if(!empty($target_array['stylist_look'][1])){
          $i = 0;
          foreach($target_array['stylist_look'][1] as $val){
            $short_url = base_url().'looks/look-details/'.$val['slug'];
            $img_title_alt = str_replace('#','',$val['name']);

            if($this->look_hv_discount($val['id']) == 1)
            {
              $discount_tag ='<div class="item-tag-sale"></div>';
            }else
            {
              $discount_tag = '';
            }

            $homehtml = $homehtml.'<div class="col-md-4 col-sm-6 grid-item">';
            $street_style_class = $val['look_type'] == 2 ? "item-street-style" : "";
            $product_style_class = $val['look_type'] == 3 ? "item-product" : "";
            $celeb_style_class = $val['look_type'] == 4 ? "item-celeb-style" : "";
            $homehtml = $homehtml.'<div class="item '.$street_style_class.' '.$product_style_class.' '.$celeb_style_class.'">';
            $homehtml = $homehtml.'<div class="item-img item-hover">';
            if($val['look_type'] == 1){
              $homehtml = $homehtml.'<a onclick="_targetClick('.$val['id'].',\'stylist\')" href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_look_image_url').$val['image'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker"  /></a>'.$discount_tag;
              $img_url = $this->config->item('sc_look_image_url').$val['image'];
            }else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
              $homehtml = $homehtml.'<a onclick="_targetClick('.$val['id'].',\'stylist\')" href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_street_style_image_url').$val['look_image'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker"  /></a>'.$discount_tag;
              $img_url = $this->config->item('sc_street_style_image_url').$val['look_image'];
            }else if($val['look_type'] == 3){
              $homehtml = $homehtml.'<a onclick="_targetClick('.$val['id'].',\'stylist\')" href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_promotional_look_image').$val['product_img'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker" /></a>'.$discount_tag;
              $img_url = $this->config->item('sc_promotional_look_image').$val['product_img'];
            }
            $homehtml = $homehtml.'<div class="item-look-hover">';
            $homehtml = $homehtml.'<a target="_blank" href="'.base_url().'looks/look-details/'.$val['slug'].'" onclick="_targetClick('.$val['id'].',\'stylist\')"><span class="btn btn-primary"> Get This </span></a>';
            $homehtml = $homehtml.'</div>';
            $homehtml = $homehtml.'</div>';
            $homehtml = $homehtml.'<div class="item-desc">';
            $homehtml = $homehtml.'<div class="item-title">';
            $homehtml = $homehtml.'<a target="_blank" href="'.base_url().'looks/look-details/'.$val['slug'].'" onclick="_targetClick('.$val['id'].',\'stylist\')" title="'.$val['name'].'">'.$val['name'].'</a>';
            $homehtml = $homehtml.'</div>';
            $homehtml = $homehtml.'<div class="item-media">';
            $homehtml = $homehtml.'<ul class="social-buttons">';
            $homehtml = $homehtml.'<li><a onClick="_targetFacebookShare(\''.$val['name'].'\',\''.base_url().'looks/look-details/'.$val['slug'].'\',\''.$this->config->item('sc_look_image_url').$val['image'].'\',\''.$val['id'].'\');"><i class="fa fa-facebook"></i></a></li>';
            $homehtml = $homehtml.'<li><a onclick="_targetTweet(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\');"><i class="fa fa-twitter"></i></a></li>';
            $homehtml = $homehtml.'<li><a onclick="_targetPinterest(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\',\''.$img_url.'\');"><i class="fa fa-pinterest"></i></a></li>';
        
            $homehtml = $homehtml.'</ul>';
            $homehtml = $homehtml.'<div class="user-likes">';
            if($this->session->userdata('user_id')){
              if($this->get_look_fav($val['id']) == 0){
                $homehtml = $homehtml.'<a target="_blank" title="Add to Wish List" id="whislist'.$val['id'].'" onclick="add_to_wishlist('.$val['id'].');"><i class="ic sc-love"></i></a>';
              }else{
                $homehtml = $homehtml.'<a target="_blank" title="Remove from Wish List" id="whislist'.$val['id'].'" onclick="remove_from_wishlist('.$val['id'].');"><i class="ic sc-love active"></i></a>';
              }
            }else{
              $homehtml = $homehtml.'<a target="_blank" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a>';
            }
            $homehtml = $homehtml.'</div>';
            $homehtml = $homehtml.'</div>';
            $homehtml = $homehtml.'</div>';
            $homehtml = $homehtml.'</div>';


            $homehtml = $homehtml.'</div>';

            $i++; 
          } 
        }

        $homehtml = $homehtml.'<input type="hidden" name="stylist_count" id="stylist_count" value="'.$this->get_stylist_look_count().'">';
        $homehtml = $homehtml.'<input type="hidden" name="blog_count" id="blog_count" value="'.$this->get_blog_count().'">';
        $homehtml = $homehtml.'<input type="hidden" name="street_count" id="street_count" value="'.$this->get_street_look_count().'">';

        if( isset($looks['offset']['stylist']) && ($looks['offset']['stylist'] * $this->config->item('stylist_plus_promo_look')) < $this->get_stylist_look_count()){
          $homehtml = $homehtml.'<input type="hidden" name="stylist_offset" id="stylist_offset" value="'.$looks['offset']['stylist'].'">';
        }

        if( isset($looks['offset']['blog']) && ($looks['offset']['blog'] * $this->config->item('blog_look')) < $this->get_blog_count()){
          $homehtml = $homehtml.'<input type="hidden" name="blog_offset" id="blog_offset" value="'.$looks['offset']['blog'].'">';
        }

        if( isset($looks['offset']['street']) && ($looks['offset']['street'] * $this->config->item('street_look')) < $this->get_street_look_count()){
          $homehtml = $homehtml.'<input type="hidden" name="street_offset" id="street_offset" value="'.$looks['offset']['street'].'">';
        }

        $stylist_look_count =  $this->get_stylist_look_count();
        $blog_count =  $this->get_blog_count();
        
        $stylist_count_ = ($stylist_look_count/12);
        $blog_count_ = ($blog_count/2);

        $total = $stylist_count_ > $blog_count_ ? $stylist_look_count : $blog_count;

        $perpage = $stylist_count_ > $blog_count_ ? 12 : 2;

        $lastpage = ceil($total/$perpage);
        $lpm1 = $lastpage - 1;


        if($lastpage > 1)
        { 
          $pagination .= "<div class='sc-pagination-wrp'><ul class=\"pagination sc-pagination\">";
          //previous button

          if ($offset != 1) 
            $pagination.= "<li><a href=".base_url()."page/".'1'.">First</a></li>";

          if ($offset > 1) 
            $pagination.= "<li><a href=".base_url()."page/".$prev.">previous</a></li>";
          
          
          $limit = $offset + 5;
          if($offset != $lastpage && $offset< $lastpage-5  ){
          for ($counter =  $offset; $counter <= $limit; $counter++)
            {
              if ($counter == $offset)
                $pagination.= "<li><span class=\"current\">$counter</span></li>";
              else
                $pagination.= "<li><a href=".base_url()."page/".$counter.">$counter</a></li>";          
            }
          }else{ 
            for ($counter = $lastpage-5; $counter <= $lastpage; $counter++)
            {
              if ($counter == $offset)
                $pagination.= "<li><span class=\"current\">$counter</span></li>";
              else
                $pagination.= "<li><a href=".base_url()."page/".$counter.">$counter</a></li>";          
            }

          }
          
          //next button
          if ($offset < $counter - 1) 
            $pagination.= "<li><a href=".base_url()."page/".$next.">next </a></li>";


          if ($offset != $lastpage) 
            $pagination.= "<li><a href=".base_url()."page/".$lastpage.">Last </a></li>";

          $pagination.= "</ul></div>\n";    
      }
      $data['pagination'] =  $pagination;
      $data['schomeview'] =  $homehtml;
      $homehtml1 = $data;

      // Save into the cache for 5 minutes
      $this->cache->save('homehtml1', $homehtml1, 300);
      }
      $data = $homehtml1;

      $this->load->view('common/header_view');
      $this->load->view('looks_view',$data);
      $this->load->view('common/footer_view');

  }

	public function get_looks(){
		//if ($this->input->is_ajax_request()) {
		$homehtml = '';
		$filter_value = '';

		$stylist_offset = $this->input->get('stylist_offset');
		$street_offset = $this->input->get('street_offset');
		$blog_offset = $this->input->get('blog_offset');
		$filter = $this->input->get('filter');

		if($filter == 'hot-right-now'){ $filter_value = '3'; }
		else if($filter == 'looks'){ $filter_value = '1'; }
		else if($filter == 'street-style'){ $filter_value = '2'; }
		else if($filter == 'sc-celeb-style'){ $filter_value = '4'; }
    else if($filter == 'sc-live-posts'){ $filter_value = '5'; }
    else if($filter == 'festive'){ $filter_value = '6'; }
    else if($filter == 'bridal'){ $filter_value = '7'; }
    else if($filter == 'sale'){ $filter_value = '8'; }

		$looks = $this->Schome_model->get_homepage_looks($stylist_offset,$street_offset,$blog_offset,$filter_value);


		$length = array(4,8);

		$target_array = array();
		if(!empty($looks['stylist_look'])){
			foreach($length as $i) {
    		$target_array['stylist_look'][] = array_splice($looks['stylist_look'], 0, $i);
		}}


		 if(!empty($target_array['stylist_look'][0])){ $i = 0;
            foreach($target_array['stylist_look'][0] as $val){
              if($this->look_hv_discount($val['id']) == 1)
              {
                $discount_tag ='<div class="item-tag-sale"></div>';
              }else
              {
                  $discount_tag = '';
              }
                //$short_url = $this->bitly->shorten(base_url().'looks/look_details/'.$val['slug']);
                $short_url = base_url().'looks/look-details/'.$val['slug'];
				$img_title_alt = str_replace('#','',$val['name']);

              			$homehtml = $homehtml.'<div class="col-md-4 col-sm-6 grid-item">';

                $street_style_class = $val['look_type'] == 2 ? "item-street-style" : "";
                $product_style_class = $val['look_type'] == 3 ? "item-product" : "";
                $celeb_style_class = $val['look_type'] == 4 ? "item-celeb-style" : "";

                $homehtml = $homehtml.'<div class="item '.$street_style_class.' '.$product_style_class.' '.$celeb_style_class.'">';
                $homehtml = $homehtml.'<div class="item-img item-hover">';
                  if($val['look_type'] == 1){
                $homehtml = $homehtml.'<a href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_look_image_url').$val['image'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker"  /></a>'.$discount_tag;
                  $img_url = $this->config->item('sc_look_image_url').$val['image'];
                  }else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
                $homehtml = $homehtml.'<a href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_street_style_image_url').$val['look_image'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker"  /></a>'.$discount_tag;
                  $img_url = $this->config->item('sc_street_style_image_url').$val['look_image'];
                }else if($val['look_type'] == 3){
                $homehtml = $homehtml.'<a href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_promotional_look_image').$val['product_img'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker" /></a>'.$discount_tag;
                  $img_url = $this->config->item('sc_promotional_look_image').$val['product_img'];
                }
                $homehtml = $homehtml.'<div class="item-look-hover">';
                $homehtml = $homehtml.'<a target="_blank" href="'.base_url().'looks/look-details/'.$val['slug'].'"><span class="btn btn-primary"> Get This </span></a>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'<div class="item-desc">';
                $homehtml = $homehtml.'<div class="item-title">';
                $homehtml = $homehtml.'<a target="_blank" href="'.base_url().'looks/look-details/'.$val['slug'].'" title="'.$val['name'].'">'.$val['name'].'</a>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'<div class="item-media">';
                $homehtml = $homehtml.'<ul class="social-buttons">';
                $homehtml = $homehtml.'<li><a onClick="_targetFacebookShare(\''.$val['name'].'\',\''.base_url().'looks/look-details/'.$val['slug'].'\',\''.$this->config->item('sc_look_image_url').$val['image'].'\',\''.$val['id'].'\');"><i class="fa fa-facebook"></i></a></li>';
                  $homehtml = $homehtml.'<li><a onclick="_targetTweet(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\');"><i class="fa fa-twitter"></i></a></li>';
                  $homehtml = $homehtml.'<li><a onclick="_targetPinterest(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\',\''.$img_url.'\');"><i class="fa fa-pinterest"></i></a></li>';
                /*commented by Hari  if($this->session->userdata('user_id')){
                  $homehtml = $homehtml.'<li><a onClick="_targetFacebookShare(\''.$val['name'].'\',\''.base_url().'looks/look-details/'.$val['slug'].'\',\''.$this->config->item('sc_look_image_url').$val['image'].'\',\''.$val['id'].'\');"><i class="fa fa-facebook"></i></a></li>';
                  $homehtml = $homehtml.'<li><a onclick="_targetTweet(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\');"><i class="fa fa-twitter"></i></a></li>';
                  $homehtml = $homehtml.'<li><a onclick="_targetPinterest(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\',\''.$img_url.'\');"><i class="fa fa-pinterest"></i></a></li>';
                //  $homehtml = $homehtml.'<li><a target="_blank" href="#"><i class="fa fa-google-plus"></i></a></li>';
                }else{
                  $homehtml = $homehtml.'<li><a target="_blank" data-toggle="modal" href="#myModal"><i class="fa fa-facebook"></i></a></li>';
                  $homehtml = $homehtml.'<li><a target="_blank" data-toggle="modal" href="#myModal"><i class="fa fa-twitter"></i></a></li>';
                  $homehtml = $homehtml.'<li><a target="_blank" data-toggle="modal" href="#myModal"><i class="fa fa-pinterest"></i></a></li>';
                }*/
                $homehtml = $homehtml.'</ul>';
                $homehtml = $homehtml.'<div class="user-likes">';
                if($this->session->userdata('user_id')){
                  if($this->get_look_fav($val['id']) == 0){
                    $homehtml = $homehtml.'<a target="_blank" title="Add to Wish List" id="whislist'.$val['id'].'" onclick="add_to_wishlist('.$val['id'].');"><i class="ic sc-love"></i></a>';
                  }else{
                    $homehtml = $homehtml.'<a target="_blank" title="Remove from Wish List" id="whislist'.$val['id'].'" onclick="remove_from_wishlist('.$val['id'].');"><i class="ic sc-love active"></i></a>';
                  }
                }else{
                  $homehtml = $homehtml.'<a target="_blank" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a>';
                }
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';


              	$homehtml = $homehtml.'</div>';

              	$i++; } }

              	if(!empty($looks['blog'])){

                  if(isset($looks['blog']['name'][0])){

                    	$blog_date = date_create($looks['blog']['post_date'][0]);
						$blog_posted_date = date_format($blog_date, 'd F,Y');

                    	$homehtml = $homehtml.'<div class="col-md-4 col-sm-6 grid-item">';
                		$homehtml = $homehtml.'<div class="item item-blog">';
                  		$homehtml = $homehtml.'<div class="item-img item-hover">';
                    	$homehtml = $homehtml.'<a href="'.$this->config->item('blog_url').$looks['blog']['post_name'][0].'" target="_blank">';
                      	$homehtml = $homehtml.'<img src="'.$looks['blog']['guid'][0].'" title="'.$looks['blog']['name'][0].' - StyleCracker" alt="'.$looks['blog']['name'][0].' - StyleCracker"/></a>';

                       $homehtml = $homehtml.'<div class="item-look-hover">';
                $homehtml = $homehtml.'<a href="'.$this->config->item('blog_url').$looks['blog']['post_name'][0].'" target="_blank"><span class="btn btn-primary"> Read More </span></a>';
                $homehtml = $homehtml.'</div>';
                        $homehtml = $homehtml.'</div>';
                  		$homehtml = $homehtml.'<div class="item-desc">';
                    	$homehtml = $homehtml.'<div class="item-title">';
                        $homehtml = $homehtml.'<span>SC Live Feature:</span> <a href="'.$this->config->item('blog_url').$looks['blog']['post_name'][0].'" target="_blank">'.$looks['blog']['name'][0].'</a>';
                    	$homehtml = $homehtml.'</div>';
                    	$homehtml = $homehtml.'<div class="item-media">';
                      	$homehtml = $homehtml.'<div class="post-date"><span>Posted: </span>'.$blog_posted_date.'</div>';
                      	$homehtml = $homehtml.'<ul class="social-buttons hide">';
                        $homehtml = $homehtml.'<li><a href="#"><i class="fa fa-facebook"></i></a></li>';
                        $homehtml = $homehtml.'<li><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></li>';
                        $homehtml = $homehtml.'<li><a target="_blank" href="#"><i class="fa fa-pinterest"></i></a></li>';
                      //  $homehtml = $homehtml.'<li><a target="_blank" href="#"><i class="fa fa-google-plus"></i></a></li>';
                      	$homehtml = $homehtml.'</ul>';
                      	$homehtml = $homehtml.'<div class="user-likes">';
                        //$homehtml = $homehtml.'<a target="_blank" href="#"><i class="ic sc-love"></i></a>';
                      	$homehtml = $homehtml.'</div>';
                    	$homehtml = $homehtml.'</div>';
                  		$homehtml = $homehtml.'</div>';
                		$homehtml = $homehtml.'</div>';
              			$homehtml = $homehtml.'</div>';
              			$homehtml = $homehtml.'</div>';
              			$homehtml = $homehtml.'</div>';
                  }

                  if(isset($looks['blog']['name'][1])){

                      $blog_date = date_create($looks['blog']['post_date'][1]);
            $blog_posted_date = date_format($blog_date, 'd F,Y');

                      $homehtml = $homehtml.'<div class="col-md-4 col-sm-6 grid-item">';
                    $homehtml = $homehtml.'<div class="item item-blog">';
                      $homehtml = $homehtml.'<div class="item-img item-hover">';
                      $homehtml = $homehtml.'<a href="'.$this->config->item('blog_url').$looks['blog']['post_name'][1].'" target="_blank">';
                        $homehtml = $homehtml.'<img src="'.$looks['blog']['guid'][1].'" title="'.$looks['blog']['name'][1].'- StyleCracker" alt="'.$looks['blog']['name'][1].'- StyleCracker"/></a>';

                       $homehtml = $homehtml.'<div class="item-look-hover">';
                $homehtml = $homehtml.'<a href="'.$this->config->item('blog_url').$looks['blog']['post_name'][1].'" target="_blank"><span class="btn btn-primary"> Read More </span></a>';
                $homehtml = $homehtml.'</div>';
                        $homehtml = $homehtml.'</div>';
                      $homehtml = $homehtml.'<div class="item-desc">';
                      $homehtml = $homehtml.'<div class="item-title">';
                        $homehtml = $homehtml.'<span>SC Live Feature:</span> <a href="'.$this->config->item('blog_url').$looks['blog']['post_name'][1].'" target="_blank">'.$looks['blog']['name'][1].'</a>';
                      $homehtml = $homehtml.'</div>';
                      $homehtml = $homehtml.'<div class="item-media">';
                        $homehtml = $homehtml.'<div class="post-date"><span>Posted: </span>'.$blog_posted_date.'</div>';
                        $homehtml = $homehtml.'<ul class="social-buttons hide">';
                        $homehtml = $homehtml.'<li><a href="#"><i class="fa fa-facebook"></i></a></li>';
                        $homehtml = $homehtml.'<li><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></li>';
                        $homehtml = $homehtml.'<li><a target="_blank" href="#"><i class="fa fa-pinterest"></i></a></li>';
                      //  $homehtml = $homehtml.'<li><a target="_blank" href="#"><i class="fa fa-google-plus"></i></a></li>';
                        $homehtml = $homehtml.'</ul>';
                        $homehtml = $homehtml.'<div class="user-likes">';
                        //$homehtml = $homehtml.'<a target="_blank" href="#"><i class="ic sc-love"></i></a>';
                        $homehtml = $homehtml.'</div>';
                      $homehtml = $homehtml.'</div>';
                      $homehtml = $homehtml.'</div>';
                    $homehtml = $homehtml.'</div>';
                    $homehtml = $homehtml.'</div>';
                    $homehtml = $homehtml.'</div>';
                    $homehtml = $homehtml.'</div>';
                  }

                   if(!$this->session->userdata('user_id')){
                      $homehtml = $homehtml.'<div class="col-md-4 col-sm-6 grid-item">';
                      $homehtml = $homehtml.'<div class="item item-blog">';
                      $homehtml = $homehtml.'<div class="item-img item-hover">';
                      $homehtml = $homehtml.'<a onclick="CheckURL();" href="javascript:void(0);" >';
                      $homehtml = $homehtml.'<img src="'.base_url().'assets/SignUpGIF.gif" title="Sign up" alt="Sign Up"/></a>';

                      $homehtml = $homehtml.'<div class="item-look-hover">';
                      $homehtml = $homehtml.'<a href="https://play.google.com/store/apps/details?id=com.stylecracker.android&hl=en" target="_blank"></a>';
                      $homehtml = $homehtml.'</div>';
                      $homehtml = $homehtml.'</div>';
                      $homehtml = $homehtml.'</div>';
                      $homehtml = $homehtml.'</div>';
                      $homehtml = $homehtml.'</div>';
                      $homehtml = $homehtml.'</div>';
                    }
                    
            	}



              if(!empty($target_array['stylist_look'][1])){ $i = 0;
            foreach($target_array['stylist_look'][1] as $val){
                if($this->look_hv_discount($val['id']) == 1)
              {
                $discount_tag ='<div class="item-tag-sale"></div>';
              }else
              {
                  $discount_tag = '';
              }
                //$short_url = $this->bitly->shorten(base_url().'looks/look_details/'.$val['slug']);
                $short_url = base_url().'looks/look-details/'.$val['slug'];
				$img_title_alt = str_replace('#','',$val['name']);

                    $homehtml = $homehtml.'<div class="col-md-4 col-sm-6 grid-item">';

                $street_style_class = $val['look_type'] == 2 ? "item-street-style" : "";
                $product_style_class = $val['look_type'] == 3 ? "item-product" : "";
                $celeb_style_class = $val['look_type'] == 4 ? "item-celeb-style" : "";

                $homehtml = $homehtml.'<div class="item '.$street_style_class.' '.$product_style_class.' '.$celeb_style_class.'">';
                $homehtml = $homehtml.'<div class="item-img item-hover">';
                  if($val['look_type'] == 1){
                $homehtml = $homehtml.'<a href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_look_image_url').$val['image'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker"  /></a>'.$discount_tag;
                  $img_url = $this->config->item('sc_look_image_url').$val['image'];
                  }else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
                $homehtml = $homehtml.'<a href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_street_style_image_url').$val['look_image'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker"  /></a>'.$discount_tag;
                  $img_url = $this->config->item('sc_street_style_image_url').$val['look_image'];
                }else if($val['look_type'] == 3){
                $homehtml = $homehtml.'<a href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_promotional_look_image').$val['product_img'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker" /></a>'.$discount_tag;
                  $img_url = $this->config->item('sc_promotional_look_image').$val['product_img'];
                }
                $homehtml = $homehtml.'<div class="item-look-hover">';
                $homehtml = $homehtml.'<a target="_blank" href="'.base_url().'looks/look-details/'.$val['slug'].'"><span class="btn btn-primary"> Get This </span></a>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'<div class="item-desc">';
                $homehtml = $homehtml.'<div class="item-title">';
                $homehtml = $homehtml.'<a target="_blank" href="'.base_url().'looks/look-details/'.$val['slug'].'" title="'.$val['name'].'">'.$val['name'].'</a>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'<div class="item-media">';
                $homehtml = $homehtml.'<ul class="social-buttons">';
                $homehtml = $homehtml.'<li><a onClick="_targetFacebookShare(\''.$val['name'].'\',\''.base_url().'looks/look-details/'.$val['slug'].'\',\''.$this->config->item('sc_look_image_url').$val['image'].'\',\''.$val['id'].'\');"><i class="fa fa-facebook"></i></a></li>';
                  $homehtml = $homehtml.'<li><a onclick="_targetTweet(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\');"><i class="fa fa-twitter"></i></a></li>';
                  $homehtml = $homehtml.'<li><a onclick="_targetPinterest(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\',\''.$img_url.'\');"><i class="fa fa-pinterest"></i></a></li>';
                /* commented by Hari if($this->session->userdata('user_id')){
                  $homehtml = $homehtml.'<li><a onClick="_targetFacebookShare(\''.$val['name'].'\',\''.base_url().'looks/look-details/'.$val['slug'].'\',\''.$this->config->item('sc_look_image_url').$val['image'].'\',\''.$val['id'].'\');"><i class="fa fa-facebook"></i></a></li>';
                  $homehtml = $homehtml.'<li><a onclick="_targetTweet(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\');"><i class="fa fa-twitter"></i></a></li>';
                  $homehtml = $homehtml.'<li><a onclick="_targetPinterest(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\',\''.$img_url.'\');"><i class="fa fa-pinterest"></i></a></li>';
                //  $homehtml = $homehtml.'<li><a target="_blank" href="#"><i class="fa fa-google-plus"></i></a></li>';
                }else{
                  $homehtml = $homehtml.'<li><a target="_blank" data-toggle="modal" href="#myModal"><i class="fa fa-facebook"></i></a></li>';
                  $homehtml = $homehtml.'<li><a target="_blank" data-toggle="modal" href="#myModal"><i class="fa fa-twitter"></i></a></li>';
                  $homehtml = $homehtml.'<li><a target="_blank" data-toggle="modal" href="#myModal"><i class="fa fa-pinterest"></i></a></li>';
                }*/
                $homehtml = $homehtml.'</ul>';
                $homehtml = $homehtml.'<div class="user-likes">';
                if($this->session->userdata('user_id')){
                  if($this->get_look_fav($val['id']) == 0){
                    $homehtml = $homehtml.'<a target="_blank" title="Add to Wish List" id="whislist'.$val['id'].'" onclick="add_to_wishlist('.$val['id'].');"><i class="ic sc-love"></i></a>';
                  }else{
                    $homehtml = $homehtml.'<a target="_blank" title="Remove from Wish List" id="whislist'.$val['id'].'" onclick="remove_from_wishlist('.$val['id'].');"><i class="ic sc-love active"></i></a>';
                  }
                }else{
                  $homehtml = $homehtml.'<a target="_blank" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a>';
                }
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';


                $homehtml = $homehtml.'</div>';

                $i++; } }

              $homehtml = $homehtml.'<input type="hidden" name="stylist_count" id="stylist_count" value="'.$this->get_stylist_look_count().'">';
              $homehtml = $homehtml.'<input type="hidden" name="blog_count" id="blog_count" value="'.$this->get_blog_count().'">';
              $homehtml = $homehtml.'<input type="hidden" name="street_count" id="street_count" value="'.$this->get_street_look_count().'">';

              if( isset($looks['offset']['stylist']) && ($looks['offset']['stylist'] * $this->config->item('stylist_plus_promo_look')) < $this->get_stylist_look_count()){
              	$homehtml = $homehtml.'<input type="hidden" name="stylist_offset" id="stylist_offset" value="'.$looks['offset']['stylist'].'">';
          	  }

          	  if( isset($looks['offset']['blog']) && ($looks['offset']['blog'] * $this->config->item('blog_look')) < $this->get_blog_count()){
              	$homehtml = $homehtml.'<input type="hidden" name="blog_offset" id="blog_offset" value="'.$looks['offset']['blog'].'">';
          	  }

          	  if( isset($looks['offset']['street']) && ($looks['offset']['street'] * $this->config->item('street_look')) < $this->get_street_look_count()){
              	$homehtml = $homehtml.'<input type="hidden" name="street_offset" id="street_offset" value="'.$looks['offset']['street'].'">';
          	  }

		echo $homehtml;
  /*}else{
    echo 'Somethings went wrong';

  }*/

	}

	function get_stylist_look_count(){
		$stylist_look = $this->Schome_model->get_stylist_look_count();
		return count($stylist_look);
	}

	function get_promotional_look_count(){
		$stylist_look = $this->Schome_model->get_promotional_look_count();
		return count($stylist_look);
	}

	function get_street_look_count(){
		$stylist_look = $this->Schome_model->get_street_look_count();
		return count($stylist_look);
	}

	function get_blog_count(){
		$stylist_look = $this->Schome_model->get_blog_count();
		return count($stylist_look);
	}

 public function register_user(){
    if ($this->input->is_ajax_request()) {

      @$sc_firstname = $this->input->post('sc_firstname');
      @$sc_lastname = $this->input->post('sc_lastname');
       $scmobile = $this->input->post('sc_mobile');
      @$json_array['firstname'] = $sc_firstname;
      @$json_array['lastname'] = $sc_lastname;
      @$scusername = $this->input->post('sc_username_name');
      @$sc_gender = $this->input->post('sc_gender');
      @$json_array['scmobile'] = $scmobile;
      $scemail_id = $this->input->post('sc_email_id');
      $scpassword = $this->input->post('sc_password');
      $promo = $this->input->post('registered_from') !='' ? $this->input->post('registered_from') : '';
      /* Added By Sudha For Event*/
      $event = $this->input->post('sc_event') !='' ? $this->input->post('sc_event') : '';

      /*if(trim($scusername) == ''){
        echo "Please enter your username";

      }else*/if(trim($sc_firstname) == ''){
        echo "Please enter the Firstname";

      }else if(trim($sc_lastname) == ''){
        echo "Please enter the Lastname";

      }else  if(trim($scemail_id) == ''){
        echo "Please enter the Email-id";

      }else if(trim($sc_gender) == ''){
        echo "Please select gender";

      }else if(trim($scpassword) == ''){
        echo "Please enter the Password";

      }else if(trim($scmobile) == '')
      {
        echo "Please enter the Mobile No.";
      }else if(filter_var($scemail_id, FILTER_VALIDATE_EMAIL) === false){
        echo "Please enter valid Email-id";

      }/*else if($this->usernameUnique($scusername) > 0){
        echo "Username already exist";

      }*/else if($this->emailUnique($scemail_id) > 0){
        echo "Email id already exist";

      }else if($event!=''){
        $ip = $this->input->ip_address();
        $scevent = strip_tags(trim($event));
        $eventcode = $scevent;
        $json_array['event_code'] = $eventcode;
        $this->signup('event',$scusername,$scemail_id,$scpassword,$ip,2,$sc_gender,$json_array);

      }else{
        $ip = $this->input->ip_address();
        $this->signup('website',$scusername,$scemail_id,$scpassword,$ip,2,$sc_gender,$json_array);
        if($promo=='comp'){ 
            $this->session->set_userdata('campaign','yes');
        }
        /*$this->send_welcome_email($scemail_id);*/
      }

      /*Mailchimp call For New User*/
       if(!empty($this->session->userdata('user_id')))
      {  
        if($this->Schome_model->get_nonscdomain($scemail_id))
        { 
          $result = $this->curl->simple_post(BACKENDURL.'/Mailchimp/newUserImport/'.$this->session->userdata('user_id'), false, array(CURLOPT_USERAGENT => true)); 
        }       
      }
/*End Mailchimp call For New User*/
      if($scemail_id!='')
      {
        $this->Schome_model->update_couponcode($scemail_id,'Welcome10');
      }


    }else{
      return 'Somethings went wrong';
    }

  }

  public function usernameUnique($username){
      $res = $this->Schome_model->usernameUnique($username);
      return count($res);
  }

  public function emailUnique($email){
      $res = $this->Schome_model->emailUnique($email);
      return count($res);
  }

  public function signup($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$sc_gender=1, $json_array = array()){
      $res = $this->Schome_model->registerUser($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$sc_gender,$json_array);
      if($scemail_id!='')
      {
        $this->Schome_model->update_couponcode($scemail_id,'Welcome10');
      }
      if($res){ return $res; }else{ return '0'; }

  }

  public function pa(){

    if($this->session->userdata('user_id')){

        if($this->Pa_model->get_gender($this->session->userdata('user_id')) == 2){
          $this->pa_mens();
          return false;
        }

        $data = array();

        if($this->uri->segment(3)){
          $edit_id = $this->uri->segment(3);
        }else{
          $edit_id = '';
        }

        $data['body_shape'] = $this->Pa_model->get_question_img_answers(1);
        $data['style'] = $this->Pa_model->get_question_img_answers(2);
        $data['age'] = $this->Pa_model->get_question_img_answers(8);
        $data['budget'] = $this->Pa_model->get_question_img_answers(7);
        $data['size'] = $this->Pa_model->get_question_img_answers(9);

        $data['user_body_shape'] = $this->Pa_model->get_users_answer(1);
        $data['user_style'] = $this->Pa_model->get_users_answer(2);
        $data['user_age'] = $this->Pa_model->get_users_answer(8);
        $data['user_budget'] = $this->Pa_model->get_users_answer(7);
        $data['user_size'] = $this->Pa_model->get_users_answer(9);

        if($edit_id!=''){
          $data['question_resume_id'] = $edit_id;
          $data['edited_data'] ='edit';
        }else{
          $data['question_resume_id'] = $this->Pa_model->get_question_resume_id();
          $data['edited_data'] ='';
        }


        $this->load->view('common/without_header_view');
        $this->load->view('pa',$data);

    }else{
      redirect('/');
    }

  }

  public function pa_mens(){

    if($this->session->userdata('user_id')){

        $data = array();

        if($this->uri->segment(3)){
          $edit_id = $this->uri->segment(3);
        }else{
          $edit_id = '';
        }

        $data['body_shape'] = $this->Pa_model->get_question_answers(13);
        $data['style'] = $this->Pa_model->get_question_answers(14);
        $data['age'] = $this->Pa_model->get_question_img_answers(15);
        $data['personal_style'] = $this->Pa_model->get_question_img_answers(16);
        $data['size'] = $this->Pa_model->get_question_answers(17);
        $data['budget'] = $this->Pa_model->get_question_answers(18);

        $data['quotient'][0] = $this->Pa_model->get_question_answers(19);
        $data['quotient'][1] = $this->Pa_model->get_question_answers(20);
        $data['quotient'][2] = $this->Pa_model->get_question_answers(21);
        $data['quotient'][3] = $this->Pa_model->get_question_answers(22);
        $data['quotient'][4] = $this->Pa_model->get_question_answers(23);

        $data['quotient'][0]['q'] = $this->Pa_model->get_questions(19);
        $data['quotient'][1]['q'] = $this->Pa_model->get_questions(20);
        $data['quotient'][2]['q'] = $this->Pa_model->get_questions(21);
        $data['quotient'][3]['q'] = $this->Pa_model->get_questions(22);
        $data['quotient'][4]['q'] = $this->Pa_model->get_questions(23);

        $data['user_body_shape'] = $this->Pa_model->get_users_answer(13);
        $data['user_style'] = $this->Pa_model->get_users_answer(14);
        $data['user_age'] = $this->Pa_model->get_users_answer(15);
        $data['user_personal_style'] = $this->Pa_model->get_users_answer(16);
        $data['user_size'] = $this->Pa_model->get_users_answer(17);
        $data['user_budget'] = $this->Pa_model->get_users_answer(18);

        $data['user_quotient1'] = $this->Pa_model->get_users_answer(19);
        $data['user_quotient2'] = $this->Pa_model->get_users_answer(20);
        $data['user_quotient3'] = $this->Pa_model->get_users_answer(21);
        $data['user_quotient4'] = $this->Pa_model->get_users_answer(22);
        $data['user_quotient5'] = $this->Pa_model->get_users_answer(23);
        
        if($edit_id!=''){
          $data['question_resume_id'] = $edit_id;
          $data['edited_data'] ='edit';
        }else{
          $data['question_resume_id'] = $this->Pa_model->get_question_resume_id();
          $data['edited_data'] ='';
        }


        $this->load->view('common/without_header_view');
        $this->load->view('pa_mens',$data);

    }else{
      redirect('/');
    }

  }

  public function update_pa(){
    //if ($this->input->is_ajax_request()) {
        $type = $this->input->post('type');
        $intCurrentTypeID = $this->input->post('intCurrentTypeID');
        $currentSelecteValue = $this->input->post('currentSelecteValue');
        $question_id = '';

        if($intCurrentTypeID == 0){ $question_id = '1'; }
        else if($intCurrentTypeID == 1){ $question_id = '2'; }
        else if($intCurrentTypeID == 2){ $question_id = '8'; }
        else if($intCurrentTypeID == 3){ $question_id = '7'; }
        else if($intCurrentTypeID == 4){ $question_id = '9'; }

        if($question_id!='' && $intCurrentTypeID!='' && $currentSelecteValue!=''){
          if($this->Pa_model->update_pa($question_id,$currentSelecteValue)) {
            if($intCurrentTypeID != 4){
                /*Mailchimp call For New User*/
                if(!empty($this->session->userdata('user_id')))
                {  
                  if($this->Schome_model->get_nonscdomain($this->session->userdata('email')))
                  {                 
                    $result = $this->curl->simple_post(BACKENDURL.'/Mailchimp/newUserImport/'.$this->session->userdata('user_id'), false, array(CURLOPT_USERAGENT => true)); 
                  }                  
                }
              /*End Mailchimp call For New User*/
              echo true;
            }else{
                /*Mailchimp call For New User*/
                if(!empty($this->session->userdata('user_id')))
                {  
                  if($this->Schome_model->get_nonscdomain($this->session->userdata('email')))
                  {                 
                    $result = $this->curl->simple_post(BACKENDURL.'/Mailchimp/newUserImport/'.$this->session->userdata('user_id'), false, array(CURLOPT_USERAGENT => true)); 
                  }                  
                }
              /*End Mailchimp call For New User*/
              echo 'feed';
            }
          }else{
            echo false;
          }
        }else{
          echo false;
        }


  //  }
  }

  public function update_pa_man(){
    //if ($this->input->is_ajax_request()) {
        $type = $this->input->post('type');
        $intCurrentTypeID = $this->input->post('intCurrentTypeID');
        $currentSelecteValue = $this->input->post('currentSelecteValue');
        $question_id = '';

        if($intCurrentTypeID == 0){ $question_id = '13'; }
        else if($intCurrentTypeID == 1){ $question_id = '14'; }
        else if($intCurrentTypeID == 2){ $question_id = '15'; }
        else if($intCurrentTypeID == 3){ $question_id = '16'; }
        else if($intCurrentTypeID == 4){ $question_id = '17'; }
        else if($intCurrentTypeID == 5){ $question_id = '18'; }
        else if($intCurrentTypeID == 6){ $question_id = '19'; }
        else if($intCurrentTypeID == 7){ $question_id = '20'; }
        else if($intCurrentTypeID == 8){ $question_id = '21'; }
        else if($intCurrentTypeID == 9){ $question_id = '22'; }
        else if($intCurrentTypeID == 10){ $question_id = '23'; }


        if($question_id!='' && $intCurrentTypeID!='' && $currentSelecteValue!=''){
          if($this->Pa_model->update_pa($question_id,$currentSelecteValue)) {
            if($intCurrentTypeID != 9){
              /*Mailchimp call For New User*/
                if(!empty($this->session->userdata('user_id')))
                {  
                  if($this->Schome_model->get_nonscdomain($this->session->userdata('email')))
                  {                 
                    $result = $this->curl->simple_post(BACKENDURL.'/Mailchimp/newUserImport/'.$this->session->userdata('user_id'), false, array(CURLOPT_USERAGENT => true)); 
                  }                  
                }
              /*End Mailchimp call For New User*/
              echo true;
            }else{
              /*Mailchimp call For New User*/
                if(!empty($this->session->userdata('user_id')))
                {  
                  if($this->Schome_model->get_nonscdomain($this->session->userdata('email')))
                  {                 
                    $result = $this->curl->simple_post(BACKENDURL.'/Mailchimp/newUserImport/'.$this->session->userdata('user_id'), false, array(CURLOPT_USERAGENT => true)); 
                  }                  
                }
              /*End Mailchimp call For New User*/
              echo 'feed';
            }
          }else{
            echo false;
          }
        }else{
          echo false;
        }


  //  }
  }

  public function update_pa_man_finish(){
    //if ($this->input->is_ajax_request()) {
        //$type = $this->input->post('type');
        //$intCurrentTypeID = $this->input->post('intCurrentTypeID');
        //$currentSelecteValue = $this->input->post('currentSelecteValue');
        //$question_id = '';

        $quotient1 = $this->Pa_model->get_question_answers(19);
        $quotient2 = $this->Pa_model->get_question_answers(20);
        $quotient3 = $this->Pa_model->get_question_answers(21);
        $quotient4 = $this->Pa_model->get_question_answers(22);
        $quotient5 = $this->Pa_model->get_question_answers(23);
        
        $ans1 = $quotient1[$this->input->post('question_0')]['id'];
        $ans2 = $quotient1[$this->input->post('question_1')]['id'];
        $ans3 = $quotient1[$this->input->post('question_2')]['id'];
        $ans4 = $quotient1[$this->input->post('question_3')]['id'];
        $ans5 = $quotient1[$this->input->post('question_4')]['id'];

        if($ans1!='' && $ans2!='' && $ans3!='' && $ans4!=''){

            $this->Pa_model->update_pa(19,$ans1);
            $this->Pa_model->update_pa(20,$ans2);
            $this->Pa_model->update_pa(21,$ans3);
            $this->Pa_model->update_pa(22,$ans4);
            $this->Pa_model->update_pa(23,$ans5);
            echo 'feed';
        }else{
          echo false;
        }


  //  }
  }

  public function logout(){
      $result = $this->Schome_model->track_logout();
      unset($_COOKIE['billing_first_name']);
      unset($_COOKIE['billing_last_name']);
      unset($_COOKIE['billing_mobile_no']);
      unset($_COOKIE['billing_address']);
      unset($_COOKIE['billing_city']);
      unset($_COOKIE['billing_state']);

      unset($_COOKIE['shipping_first_name']);
      unset($_COOKIE['shipping_last_name']);
      unset($_COOKIE['shipping_mobile_no']);
      unset($_COOKIE['shipping_address']);
      unset($_COOKIE['shipping_city']);
      unset($_COOKIE['shipping_state']);
      unset($_COOKIE['SCUniqueID']);

      setcookie('billing_first_name', '', time() - 3600, '/'); 
      setcookie('billing_last_name', '', time() - 3600, '/'); 
      setcookie('billing_mobile_no', '', time() - 3600, '/'); 
      setcookie('billing_address', '', time() - 3600, '/'); 
      setcookie('billing_city', '', time() - 3600, '/'); 
      setcookie('billing_state', '', time() - 3600, '/'); 

      setcookie('shipping_first_name', '', time() - 3600, '/'); 
      setcookie('shipping_last_name', '', time() - 3600, '/'); 
      setcookie('shipping_mobile_no', '', time() - 3600, '/'); 
      setcookie('shipping_address', '', time() - 3600, '/'); 
      setcookie('shipping_city', '', time() - 3600, '/'); 
      setcookie('shipping_state', '', time() - 3600, '/'); 
      setcookie('SCUniqueID', '', time() - 3600, '/'); 

      session_destroy();
      //md5(STYLELOGINID)   34b07cba21adf4cd95d35c1c0bd47ff0    
      //md5(STYLELOGINPWD) 1c0253b55e82e53890985a837274811b
      delete_cookie("34b07cba21adf4cd95d35c1c0bd47ff0");
      delete_cookie("1c0253b55e82e53890985a837274811b");
      redirect('/');
  }

  public function login(){
      if ($this->input->is_ajax_request()) {

        $logEmailid = $this->input->post('logEmailid');
        $logPassword = $this->input->post('logPassword');
        $logRemember = $this->input->post('logRemember');

         if(trim($logEmailid) == ''){
            echo "Please enter your email id";

          }else if(trim($logPassword) == ''){
            echo "Please enter the Password";

          }else{
           $result = $this->Schome_model->login_user($logEmailid,$logPassword);
            if($result == 1){

               if($logRemember == 1){    
              //md5(STYLELOGINID)   34b07cba21adf4cd95d35c1c0bd47ff0    
              //md5(STYLELOGINPWD) 1c0253b55e82e53890985a837274811b
              setcookie('34b07cba21adf4cd95d35c1c0bd47ff0',$this->encryptOrDecrypt($logEmailid,'encrypt'), time() + (86400 * 30), "/");
              setcookie('1c0253b55e82e53890985a837274811b',$this->encryptOrDecrypt($logPassword,'encrypt'), time() + (86400 * 30), "/");
              
             }

              $question_comp = $this->session->userdata('question_comp');
              $question_resume_id = $this->session->userdata('question_resume_id');



              if($this->session->userdata('bucket') == '' || $question_comp!=1 ){
                echo 'pa';
              }else{
                setcookie('is_redirect', 1, time() + (86400 * 30), "/");
                echo 'feed';
              }

              /*Mailchimp call For UserSignUp*/
                 if(!empty($this->session->userdata('user_id')))
                {  
                  if($this->Schome_model->get_nonscdomain($logEmailid))
                  { 
                    $result = $this->curl->simple_post(BACKENDURL.'/Mailchimp/newUserImport/'.$this->session->userdata('user_id'), false, array(CURLOPT_USERAGENT => true)); 
                  }       
                }
              /*End Mailchimp call For UserSignUp*/

              /*OneSignal Notification*/
              //$os_data = $this->senddata_onesignal();              
              //$this->session->set_userdata(array('onesignaldata'=>$os_data));             
              /*OneSignal Notification End*/


            }else{
              echo 'Invalid Email and Password';

            }

          }
     }
  }

/*Forgot Password Function*/
  public function forgotPassword()
  {
    $Emailid = $this->input->post('Emailid');
    if(trim($Emailid) == ''){
            echo "Please enter your email id";

          }else{
            $result = $this->Schome_model->forgot_password($Emailid);
            if($result != false){
               
               if($this->send_forgotpwd_email($Emailid,$result)) 
               {
                  echo 'New Password has been Send to your email-id '.$Emailid;
               }else
               {
                  echo "Email Error";
               }

            }else{
              echo 'Invalid Email-id ';

            }

          }  
  }


/*Reset Password Function*/

  public function resetPassword()
  {
    $oldpwd = $this->input->post('oldpwd');
    $newpwd = $this->input->post('newpwd');

    if(trim($oldpwd) == ''){
        echo "Please enter your Old Password";

          }if(trim($newpwd) == ''){
            echo "Please enter your New Password";

          }else{
            $result = $this->Schome_model->reset_password($oldpwd,$newpwd);
            if($result){         
              
             echo "Your Password has been reset";

            }else{

              if($result == false){               
                echo "Old Password is not correct";
              }else
              {
                echo $result;
              }
            }
          }    
  }


  public function aboutus(){

      $data = array();
      if ( $this->cache->file->get('sc_about_us') == FALSE ) {
        $data['stylist_data'] = $this->Schome_model->get_all_stylist();

        $this->cache->file->save('sc_about_us', $data, 500000000000000000);
      }else{
        
        $data = $this->cache->file->get('sc_about_us');
      }

      $this->seo_title = "About StyleCracker - India's personalized styling platform";
      $this->seo_desc = 'Get customised, shoppable looks & products to suit your body type, personal style & budget';
      $this->seo_keyword = $this->lang->line('seo_keyword');

      $this->load->view('common/header_view');
      $this->load->view('about_us',$data);
      $this->load->view('common/footer_view');
  }


  public function get_all_stylist_looks(){
      if ($this->input->is_ajax_request()) {
    $homehtml = '';
    $filter_value = '';

    $stylist_offset = $this->input->get('stylist_offset');
    $street_offset = $this->input->get('street_offset');
    $page_type = $this->input->get('page_type');
    $page_type_id = $this->input->get('page_type_id');
    $filter = $this->input->get('filter');

    if($filter == 'hot-right-now'){ $filter_value = '3'; }
    else if($filter == 'looks'){ $filter_value = '1'; }
    else if($filter == 'street-style'){ $filter_value = '2'; }
    else if($filter == 'sc-celeb-style'){ $filter_value = '4'; }
    else if($filter == 'sc-live-posts'){ $filter_value = '5'; }
    else if($filter == 'festive'){ $filter_value = '6'; }
    else if($filter == 'bridal'){ $filter_value = '7'; }

    $looks = $this->Schome_model->get_otherthan_homepage_looks($stylist_offset,$street_offset,$filter_value,$page_type,$page_type_id);


    $length = array(12);

    $target_array = array();
    if(!empty($looks['stylist_look'])){
      foreach($length as $i) {
        $target_array['stylist_look'][] = array_splice($looks['stylist_look'], 0, $i);
    }}


     if(!empty($target_array['stylist_look'][0])){ $i = 0;
            foreach($target_array['stylist_look'][0] as $val){
                if($this->look_hv_discount($val['id']) == 1)
              {
                $discount_tag ='<div class="item-tag-sale"></div>';
              }else
              {
                  $discount_tag = '';
              }
                //$short_url = $this->bitly->shorten(base_url().'looks/look_details/'.$val['slug']);
                $short_url = base_url().'looks/look-details/'.$val['slug'];
				$img_title_alt = str_replace('#','',$val['name']);

                    $homehtml = $homehtml.'<div class="col-md-4 col-sm-6 grid-item">';

                $street_style_class = $val['look_type'] == 2 ? "item-street-style" : "";
                $product_style_class = $val['look_type'] == 3 ? "item-product" : "";
                $celeb_style_class = $val['look_type'] == 4 ? "item-celeb-style" : "";

                $homehtml = $homehtml.'<div class="item '.$street_style_class.' '.$product_style_class.' '.$celeb_style_class.'">';
                $homehtml = $homehtml.'<div class="item-img item-hover">';
                  if($val['look_type'] == 1){
                $homehtml = $homehtml.'<a href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_look_image_url').$val['image'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker"  /></a>'.$discount_tag;
                  $img_url = $this->config->item('sc_look_image_url').$val['image'];
                  }else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
                $homehtml = $homehtml.'<a href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_street_style_image_url').$val['look_image'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker"  /></a>'.$discount_tag;
                  $img_url = $this->config->item('sc_street_style_image_url').$val['look_image'];
                }else if($val['look_type'] == 3){
                $homehtml = $homehtml.'<a href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_promotional_look_image').$val['product_img'].'" alt="'.$img_title_alt.' - StyleCracker" title="'.$img_title_alt.' - StyleCracker" /></a>'.$discount_tag;
                  $img_url = $this->config->item('sc_promotional_look_image').$val['product_img'];
                }
                $homehtml = $homehtml.'<div class="item-look-hover">';
                $homehtml = $homehtml.'<a target="_blank" href="'.base_url().'looks/look-details/'.$val['slug'].'" ><span class="btn btn-primary"> Get This </span></a>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'<div class="item-desc">';
                $homehtml = $homehtml.'<div class="item-title">';
                $homehtml = $homehtml.'<a target="_blank" href="'.base_url().'looks/look-details/'.$val['slug'].'" title="'.$val['name'].'">'.$val['name'].'</a>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'<div class="item-media">';
                $homehtml = $homehtml.'<ul class="social-buttons">';
                $homehtml = $homehtml.'<li><a onClick="_targetFacebookShare(\''.$val['name'].'\',\''.base_url().'looks/look-details/'.$val['slug'].'\',\''.$this->config->item('sc_look_image_url').$val['image'].'\',\''.$val['id'].'\');"><i class="fa fa-facebook"></i></a></li>';
                  $homehtml = $homehtml.'<li><a onclick="_targetTweet(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\');"><i class="fa fa-twitter"></i></a></li>';
                  $homehtml = $homehtml.'<li><a onclick="_targetPinterest(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\',\''.$img_url.'\');"><i class="fa fa-pinterest"></i></a></li>';
                /*Commented by Hari if($this->session->userdata('user_id')){
                  $homehtml = $homehtml.'<li><a onClick="_targetFacebookShare(\''.$val['name'].'\',\''.base_url().'looks/look_details/'.$val['slug'].'\',\''.$this->config->item('sc_look_image_url').$val['image'].'\',\''.$val['id'].'\');"><i class="fa fa-facebook"></i></a></li>';
                  $homehtml = $homehtml.'<li><a onclick="_targetTweet(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\');"><i class="fa fa-twitter"></i></a></li>';
                  $homehtml = $homehtml.'<li><a onclick="_targetPinterest(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\',\''.$img_url.'\');"><i class="fa fa-pinterest"></i></a></li>';
                //  $homehtml = $homehtml.'<li><a target="_blank" href="#"><i class="fa fa-google-plus"></i></a></li>';
                }else{
                  $homehtml = $homehtml.'<li><a target="_blank" data-toggle="modal" href="#myModal"><i class="fa fa-facebook"></i></a></li>';
                  $homehtml = $homehtml.'<li><a target="_blank" data-toggle="modal" href="#myModal"><i class="fa fa-twitter"></i></a></li>';
                  $homehtml = $homehtml.'<li><a target="_blank" data-toggle="modal" href="#myModal"><i class="fa fa-pinterest"></i></a></li>';
                }*/
                $homehtml = $homehtml.'</ul>';
                $homehtml = $homehtml.'<div class="user-likes">';
                if($this->session->userdata('user_id')){
                  if($this->get_look_fav($val['id']) == 0){
                    $homehtml = $homehtml.'<a target="_blank" title="Add to Wish List" id="whislist'.$val['id'].'" onclick="add_to_wishlist('.$val['id'].');"><i class="ic sc-love"></i></a>';
                  }else{
                    $homehtml = $homehtml.'<a target="_blank" title="Remove from Wish List" id="whislist'.$val['id'].'" onclick="remove_from_wishlist('.$val['id'].');"><i class="ic sc-love active"></i></a>';
                  }
                }else{
                  $homehtml = $homehtml.'<a target="_blank" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a>';
                }
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';


                $homehtml = $homehtml.'</div>';

                $i++; } }

              $homehtml = $homehtml.'<input type="hidden" name="stylist_count" id="stylist_count" value="'.$this->get_stylist_look_count().'">';
              $homehtml = $homehtml.'<input type="hidden" name="blog_count" id="blog_count" value="'.$this->get_blog_count().'">';
              $homehtml = $homehtml.'<input type="hidden" name="street_count" id="street_count" value="'.$this->get_street_look_count().'">';

              if( isset($looks['offset']['stylist']) && ($looks['offset']['stylist'] * $this->config->item('stylist_plus_promo_look')) < $this->get_stylist_look_count()){
                $homehtml = $homehtml.'<input type="hidden" name="stylist_offset" id="stylist_offset" value="'.$looks['offset']['stylist'].'">';
              }

              if( isset($looks['offset']['street']) && ($looks['offset']['street'] * $this->config->item('street_look')) < $this->get_street_look_count()){
                $homehtml = $homehtml.'<input type="hidden" name="street_offset" id="street_offset" value="'.$looks['offset']['street'].'">';
              }
              

    echo $homehtml;
  }else{
    echo 'Somethings went wrong';
  }

  }

  function get_look_fav($look_id){
    if($this->session->userdata('user_id') && $look_id){
      $res = $this->Schome_model->get_look_fav_or_not($look_id);
      return $res;
    }
  }

  public function faq(){
    $this->seo_title = 'FAQ - StyleCracker';
    $this->seo_desc = $this->lang->line('seo_desc');
    $this->seo_keyword = $this->lang->line('seo_keyword');

      $this->load->view('common/header_view');
      $this->load->view('faq');
      $this->load->view('common/footer_view');
  }

  public function privacy_policy(){

    $this->seo_title = 'Privacy Policy - StyleCracker';
    $this->seo_desc = $this->lang->line('seo_desc');
    $this->seo_keyword = $this->lang->line('seo_keyword');

      $this->load->view('common/header_view');
      $this->load->view('privacy_policy');
      $this->load->view('common/footer_view');
  }
  
   public function return_policy(){

    $this->seo_title = 'Return Policy - StyleCracker';
    $this->seo_desc = $this->lang->line('seo_desc');
    $this->seo_keyword = $this->lang->line('seo_keyword');

      $this->load->view('common/header_view');
      $this->load->view('return_policy');
      $this->load->view('common/footer_view');
  }

	public function terms_of_use(){
    $this->seo_title = 'Terms and Conditions - StyleCracker';
    $this->seo_desc = $this->lang->line('seo_desc');
    $this->seo_keyword = $this->lang->line('seo_keyword');

	  $this->load->view('common/header_view');
	  $this->load->view('terms_of_use');
	  $this->load->view('common/footer_view');
	}

  public function contact_us(){

      $data = array();
      $contact_name = $this->input->post('contact_name');
      $email_id = $this->input->post('email_id');
      $subject = $this->input->post('subject');
      $message = $this->input->post('message');

      if($contact_name!='' && $email_id!='' && $subject!='' && $message!=''){
        $data = array('name' => $contact_name,'email'=>$email_id,'message' => $message,'enquiry_date_time'=>$this->config->item('sc_date'),'subject' => $subject);

        $config['protocol'] = 'smtp';
  //      $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;
        $config['smtp_host'] = $this->config->item('smtp_host');
        $config['smtp_user'] = $this->config->item('smtp_user');
        $config['smtp_pass'] = $this->config->item('smtp_pass');
        $config['smtp_port'] = $this->config->item('smtp_port');

        $this->email->initialize($config);

        $this->email->from($this->config->item('from_email'), 'Stylecracker');
        $this->email->to('support@stylecracker.com');
        $this->email->subject('User Enquiry-'.$subject);
        $new_msg = 'Name : '.$contact_name.'<br/>Email ID - '.$email_id.'<br/>'.$message;
        $this->email->message($new_msg);

        $this->email->send();
        return $this->Schome_model->add_contact_us($data);

      }

  }

   public function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
  }


  public function register_user_google_signin(){

    if (isset($_GET['code'])) {

      $this->googleplus->getAuthenticate();
      $user_profile = $this->googleplus->getUserInfo();
      //$this->session->set_userdata('login',true);
      //$user_profile = $this->session->set_userdata('user_profile',$this->googleplus->getUserInfo());

      $scpassword = $this->randomPassword();
      $ip = $this->input->ip_address();
      if(isset($user_profile['given_name'])) $user_profile['firstname'] = $firstName = $user_profile['given_name']; else $firstName = '';
      if(isset($user_profile['family_name'])) $user_profile['lastname'] = $lastName = $user_profile['family_name']; else $lastName = '';
      $scusername = $firstName.$lastName;
      $scemail_id = $user_profile['email'];
      

      $gender = 1;
      if(@$user_profile['gender'] == 'male')
        $gender = 2;

      $result = $this->signup('google',$scusername,$scemail_id,$scpassword,$ip,2,$gender,$user_profile);
      /*Mailchimp call For UserSignIn*/
         if(!empty($this->session->userdata('user_id')))
        {  
            $result = $this->curl->simple_post(BACKENDURL.'/Mailchimp/newUserImport/'.$this->session->userdata('user_id'), false, array(CURLOPT_USERAGENT => true));                
        }
      /*End Mailchimp call For UserSignIn*/
      if($result == 'pa') redirect('schome/pa?ga=ga');
      else redirect('feed');       
    }

    $this->data['google_login_url'] = $this->googleplus->loginURL();
  }

  public function send_welcome_email($email){

      $config['protocol'] = 'smtp';
//      $config['mailpath'] = '/usr/sbin/sendmail';
      $config['charset'] = 'iso-8859-1';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');
      $data = array();
      $this->email->initialize($config);

      $this->email->from($this->config->item('from_email'), 'Stylecracker');
      $this->email->to($email);
     /* $this->email->subject('Welcome to StyleCracker');
      $this->email->message('Hi,<br>Welcome to StyleCracker! <br><br> StyleCracker is India\'s first online styling platform and we are here to make you the best version of youself! <br><br> When we started StyleCracker our aim was to make personalized styling available to all. We have a dedicated team of stylists who are online 24/7 waiting to answer ANY style query you may have. Never again will you have to worry about \'what do I wear\'- be it day-to-day or for special occasions, we will be there! <br><br> So, please explore the website and let us know how we can help. It would be great if you could also share your feedback with us, it will help us present the best product for you. <br><br> Love, <br>Archana Walavalkar & Dhimaan Shah <br>Founders<br> <a href="'.base_url().'">www.stylecracker.com</a>');
      $this->email->send();*/
      //$this->load->view('emailer/welcome-to-sc',$data);
      
      $this->email->subject('StyleCracker has a new friend');
      $message = $this->load->view('emailer/welcome-to-sc',$data,true);
      $this->email->message($message);
      $this->email->send();
      
  }

    public function get_all_wishlist_looks(){
      if ($this->input->is_ajax_request()) {
    $homehtml = '';
    $filter_value = '';

    $stylist_offset = $this->input->get('stylist_offset');
    $street_offset = $this->input->get('street_offset');
    $page_type = $this->input->get('page_type');
    $page_type_id = $this->input->get('page_type_id');
    $filter = $this->input->get('filter');

    if($filter == 'hot-right-now'){ $filter_value = '3'; }
    else if($filter == 'looks'){ $filter_value = '1'; }
    else if($filter == 'street-style'){ $filter_value = '2'; }
    else if($filter == 'sc-live-posts'){ $filter_value = '4'; }

    $looks = $this->Schome_model->get_otherthan_homepage_looks($stylist_offset,$street_offset,$filter_value,$page_type,$page_type_id);


    $length = array(12);

    $target_array = array();
    if(!empty($looks['stylist_look'])){
      foreach($length as $i) {
        $target_array['stylist_look'][] = array_splice($looks['stylist_look'], 0, $i);
    }}


     if(!empty($target_array['stylist_look'][0])){ $i = 0;
            foreach($target_array['stylist_look'][0] as $val){
               if($this->look_hv_discount($val['id']) == 1)
              {
                $discount_tag ='<div class="item-tag-sale"></div>';
              }else
              {
                  $discount_tag = '';
              }
                //$short_url = $this->bitly->shorten(base_url().'looks/look_details/'.$val['slug']);
                $short_url = base_url().'looks/look-details/'.$val['slug'];

                    $homehtml = $homehtml.'<div class="col-md-4 col-sm-6 grid-item" id="'.$val['id'].'">';

                $street_style_class = $val['look_type'] == 2 ? "item-street-style" : "";
                $product_style_class = $val['look_type'] == 3 ? "item-product" : "";
                $celeb_style_class = $val['look_type'] == 4 ? "item-celeb-style" : "";

                $homehtml = $homehtml.'<div class="item '.$street_style_class.' '.$product_style_class.' '.$celeb_style_class.'">';
                $homehtml = $homehtml.'<div class="item-img item-hover">';
                  if($val['look_type'] == 1){
                $homehtml = $homehtml.'<a href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_look_image_url').$val['image'].'" alt="'.$val['name'].' - StyleCracker" title="'.$val['name'].' - StyleCracker"  /></a>'.$discount_tag;
                  $img_url = $this->config->item('sc_look_image_url').$val['image'];
                  }else if($val['look_type'] == 2 || $val['look_type'] == 4 || $val['look_type'] == 6){
                $homehtml = $homehtml.'<a href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_street_style_image_url').$val['look_image'].'" alt="'.$val['name'].' - StyleCracker" title="'.$val['name'].' - StyleCracker"  /></a>'.$discount_tag;
                  $img_url = $this->config->item('sc_street_style_image_url').$val['look_image'];
                }else if($val['look_type'] == 3){
                $homehtml = $homehtml.'<a href="'.base_url().'looks/look-details/'.$val['slug'].'"><img src="'.$this->config->item('sc_promotional_look_image').$val['product_img'].'" alt="'.$val['name'].' - StyleCracker" title="'.$val['name'].' - StyleCracker" /></a>'.$discount_tag;
                  $img_url = $this->config->item('sc_promotional_look_image').$val['product_img'];
                }
                $homehtml = $homehtml.'<div class="item-look-hover">';
                $homehtml = $homehtml.'<a target="_blank" href="'.base_url().'looks/look-details/'.$val['slug'].'"><span class="btn btn-primary"> Get This </span></a>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'<div class="item-desc">';
                $homehtml = $homehtml.'<div class="item-title">';
                $homehtml = $homehtml.'<a target="_blank" href="'.base_url().'looks/look-details/'.$val['slug'].'" title="'.$val['name'].'">'.$val['name'].'</a>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'<div class="item-media">';
                $homehtml = $homehtml.'<ul class="social-buttons">';
                 $homehtml = $homehtml.'<li><a onClick="_targetFacebookShare(\''.$val['name'].'\',\''.base_url().'looks/look-details/'.$val['slug'].'\',\''.$this->config->item('sc_look_image_url').$val['image'].'\',\''.$val['id'].'\');"><i class="fa fa-facebook"></i></a></li>';
                  $homehtml = $homehtml.'<li><a onclick="_targetTweet(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\');"><i class="fa fa-twitter"></i></a></li>';
                  $homehtml = $homehtml.'<li><a onclick="_targetPinterest(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\',\''.$img_url.'\');"><i class="fa fa-pinterest"></i></a></li>';
                /*if($this->session->userdata('user_id')){
                  $homehtml = $homehtml.'<li><a onClick="_targetFacebookShare(\''.$val['name'].'\',\''.base_url().'looks/look_details/'.$val['slug'].'\',\''.$this->config->item('sc_look_image_url').$val['image'].'\',\''.$val['id'].'\');"><i class="fa fa-facebook"></i></a></li>';
                  $homehtml = $homehtml.'<li><a onclick="_targetTweet(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\');"><i class="fa fa-twitter"></i></a></li>';
                  $homehtml = $homehtml.'<li><a onclick="_targetPinterest(\''.$short_url.'\',\''.$val['name'].'\',\''.$val['id'].'\',\''.$img_url.'\');"><i class="fa fa-pinterest"></i></a></li>';
                //  $homehtml = $homehtml.'<li><a target="_blank" href="#"><i class="fa fa-google-plus"></i></a></li>';
                }else{
                  $homehtml = $homehtml.'<li><a target="_blank" data-toggle="modal" href="#myModal"><i class="fa fa-facebook"></i></a></li>';
                  $homehtml = $homehtml.'<li><a target="_blank" data-toggle="modal" href="#myModal"><i class="fa fa-twitter"></i></a></li>';
                  $homehtml = $homehtml.'<li><a target="_blank" data-toggle="modal" href="#myModal"><i class="fa fa-pinterest"></i></a></li>';
                }*/
                $homehtml = $homehtml.'</ul>';
                $homehtml = $homehtml.'<div class="user-likes">';
                if($this->session->userdata('user_id')){
                  if($this->get_look_fav($val['id']) == 0){
                    $homehtml = $homehtml.'<a target="_blank" title="Add to Wish List" id="whislist'.$val['id'].'" onclick="add_to_wishlist('.$val['id'].');"><i class="ic sc-love"></i></a>';
                  }else{
                    $homehtml = $homehtml.'<a target="_blank" title="Remove from Wish List" id="whislist'.$val['id'].'" onclick="remove_from__wishlist('.$val['id'].');"><i class="ic sc-love active"></i></a>';
                  }
                }else{
                  $homehtml = $homehtml.'<a target="_blank" title="Add to Wish List" data-toggle="modal" href="#myModal"><i class="ic sc-love"></i></a>';
                }
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';
                $homehtml = $homehtml.'</div>';


                $homehtml = $homehtml.'</div>';

                $i++; } }

              $homehtml = $homehtml.'<input type="hidden" name="stylist_count" id="stylist_count" value="'.$this->get_stylist_look_count().'">';
              $homehtml = $homehtml.'<input type="hidden" name="blog_count" id="blog_count" value="'.$this->get_blog_count().'">';
              $homehtml = $homehtml.'<input type="hidden" name="street_count" id="street_count" value="'.$this->get_street_look_count().'">';

              if( isset($looks['offset']['stylist']) && ($looks['offset']['stylist'] * $this->config->item('stylist_plus_promo_look')) < $this->get_stylist_look_count()){
                $homehtml = $homehtml.'<input type="hidden" name="stylist_offset" id="stylist_offset" value="'.$looks['offset']['stylist'].'">';
              }

              if( isset($looks['offset']['street']) && ($looks['offset']['street'] * $this->config->item('street_look')) < $this->get_street_look_count()){
                $homehtml = $homehtml.'<input type="hidden" name="street_offset" id="street_offset" value="'.$looks['offset']['street'].'">';
              }
              

    echo $homehtml;
  }else{
    echo 'Somethings went wrong';
  }

  }

  public function send_forgotpwd_email($email_id,$new_password){

      $config['protocol'] = 'smtp';
//      $config['mailpath'] = '/usr/sbin/sendmail';
 //     $config['charset'] = 'iso-8859-1';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');

      $this->email->initialize($config);

      $this->email->from($this->config->item('from_email'), 'Stylecracker');
      $this->email->to($email_id);
      $this->email->subject('StyleCracker: New Password');
      $this->email->message("Your login details are <br> Email : ".$email_id."<br><br>Your new password is:".$new_password."<br><br>You can change your password at any time by logging into your account. <br><br>Warm Regards,<br>StyleCracker");

      if($this->email->send())
      {
        return "Email Sent";
      }else
      {
        return "Email Error";
      }
      
  }

  public function redirect_thankyou_page(){
     $this->load->view('redirect_thankyou_page');
  }

   public function look_hv_discount($look_id){
      return $this->Schome_model->look_hv_discount($look_id);
  }

  public function redirect(){
    $this->load->view('redirecttoapp');
  }

  function fav_add(){
        $data['fav_id']="";
        $data['fav_for']="";

        $data = array_merge($data, $this->input->post());

        if($this->session->userdata('user_id'))
          $data['user_id']=$this->session->userdata('user_id');

        if(($data['fav_for'] == "product" || $data['fav_for'] == "brand") && is_numeric($data['user_id']) && is_numeric($data['fav_id'])){
          $this->load->model("Favorites_model");
          $response = $this->Favorites_model->add_fav($data);
          if($response == 1)
          {
            $result['success'] = "true";
            $result['msg'] = "Successful Added";
            print_r(json_encode($result));
          }else if($response == 2)
          {
            $result['success'] = "true";
            $result['msg'] = "Successful deleted";
            print_r(json_encode($result));
          }else{
            $result['success'] = "false";
            $result['msg'] = "Unable to add.";
            print_r(json_encode($result));
          }

        }else{
          $result['success'] = "false";
          $result['msg'] = "invalid request";
          print_r(json_encode($result));
        }
    }
	
	function confirm_email(){
		$email = base64_decode($this->uri->segment(3));
		$confirm_email = $this->Schome_model->confirm_email($email);
		$referee_id = $this->Schome_model->getreferee_id($email);
		if($confirm_email == '1'){
			
			// if($referee_id > 0){
				// $bid = '11257'; 
				// $secretKey = '72BB9BBBC9FDCA3165355D2F0985A983'; 
				// $campaignID = '10607';
				// $orderID = $referee_id; //set order id
				// $status = '1'; //status of the order
				// $event = 'register';
				// $http_val = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://';
				// $result = file_get_contents($http_val.'www.ref-r.com/campaign/t1/confirmConversion?secretKey='.$secretKey.'&bid='.$bid.'&campaignID='.$campaignID.'&orderID='.$orderID.'&status='.$status.'&event='.$event);
			// }
			
			$this->load->view('common/header_view');
			$this->load->view('login/login');
			$this->load->view('common/footer_view');
		}else {
			 header("Location: https://www.stylecracker.com/");
		}
		
	}

  public function home_new(){
    if($this->session->userdata('user_id') > 0){
      header("Location: ".base_url());
      exit;
    }

    if(isset($_REQUEST['utm_source'])){
      $utm_source = substr($_REQUEST['utm_source'],strpos($_REQUEST['utm_source'],"utmcsr="));
      $array_field = array('utm_source' => trim($utm_source),'utm_source1' => trim($utm_source));
      $this->session->set_userdata($array_field);
      
   }

   $this->seo_title = $this->lang->line('seo_title');
   $this->seo_desc = $this->lang->line('seo_desc');
   $this->seo_keyword = $this->lang->line('seo_keyword');

    if($this->cache->file->get('home_new') == FALSE){
      $data['products'] = $this->home_model->get_latest_home_page_products();
      $this->cache->file->save('home_new', $data['products'],500000000000000000);
    }else{
       $data['products'] = $this->cache->file->get('home_new');
    }
    $this->load->view('common/header_view');
    $this->load->view('home/home_new',$data);
    $this->load->view('common/footer_view');
  }
	
	public function rakhi_special(){
		$this->seo_title = ' Rakhi Special - Buy Online Now on StyleCracker';
		$this->load->view('common/header_view');
		$this->load->view('promo/rakhi_special');
		$this->load->view('common/footer_view');
	}


  public function scbox(){
    /*echo '<pre>';
    print_r($_SESSION);
    exit;*/
    $this->seo_title = 'StyleCracker Box - Buy Online Now on StyleCracker';
    $this->load->view('common/header_view');
    $this->load->view('promo/scbox');
    $this->load->view('common/footer_view');
  }

  public function sc_box_savemobilenumber(){    
    $sc_boxmobile = $this->input->post('sc_boxmobile');       
    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
    if($sc_boxmobile != '') {
        $save_data = $this->Pa_model->push_scbox_data($sc_boxmobile);    //echo $save_data;
        echo $save_data;       
        $email_id = "styling@stylecracker.com";
        $user_email_id = $_SESSION['email'];
        //$subject = "SC BOX";
        //$message = "Hello Arun";
        $this->send_scbox_email($email_id,$user_email_id,$sc_boxmobile,$first_name,$last_name);
    }else{
      echo 0;
    }
  }


  public function send_scbox_email($email_id,$user_email_id,$sc_boxmobile,$first_name,$last_name){

      $config['protocol'] = 'smtp';
//      $config['mailpath'] = '/usr/sbin/sendmail';
 //     $config['charset'] = 'iso-8859-1';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');

      $this->email->initialize($config);
      $email_id2 = "arun@stylecracker.com";
      $this->email->from($this->config->item('from_email'), 'Stylecracker');
      $this->email->to($email_id);
      $this->email->bcc($email_id2);
      $this->email->subject('SC Personalized Box Request');
      //$this->email->message("Your login details are <br> Email : ".$email_id."<br><br>Your new password is:".$new_password."<br><br>You can change your password at any time by logging into your account. <br><br>Warm Regards,<br>StyleCracker");
      $this->email->message("Dear Stylist <br>User <i><b>".$first_name." ".$last_name."</b>(".$user_email_id.")</i> with number <b><i>".$sc_boxmobile."</i></b> has requested to call.<br><br>Thanks");
      //$this->email->message("Testing");

      if($this->email->send())
      {
        return "Email Sent";
      }else
      {
        return "Email Error";
      }
      
  }
    function sc_change_gender(){
      $type = $this->input->post('type');
      $gender = $this->input->post('gender');
      if($this->session->userdata('gender')!=$gender){
        echo $this->Pa_model->sc_change_gender($gender);
      }else{
        echo '2';
      }
  }

   public function senddata_onesignal()
  {
        $osdata = array();
        $os_fname = (@$this->session->userdata('first_name')!='') ? $this->session->userdata('first_name') : ' ';
        $os_lname = (@$this->session->userdata('last_name')!='') ? $this->session->userdata('last_name') : ' ';
        $osdata['name'] =  $os_fname.' '.$os_lname;        
        $osdata['email'] =   (@$this->session->userdata('email')!='') ? $this->session->userdata('email') : ' ';
        $osdata['gender'] =   (@$this->session->userdata('gender')!='' && @$this->session->userdata('gender')==1 ) ? 'female' : 'men ';
        $osdata['user_id'] =   (@$this->session->userdata('user_id')!='') ? $this->session->userdata('user_id') : ' ';

        $pa_data = $this->home_model->get_user_pa($this->session->userdata('user_id'));
         $question_ans = array();
             if(!empty($pa_data)){
                foreach($pa_data as $val){
                     $question_ans[$val['question_id']] = $val['answer'];
                }
             } 
        $cart = $this->home_model->get_user_cart(@$this->session->userdata('user_id'));
        if(@$this->session->userdata('gender')!='' && @$this->session->userdata('gender')==1)
        { /*For Female*/         

           
          $osdata['user_age'] =   (@$question_ans[8]!='') ? $question_ans[8] : ' ';
          $osdata['bucket_id'] =   (@$this->session->userdata('bucket')!='') ? $this->session->userdata('bucket') : ' ';
          $osdata['user_size'] =   (@$question_ans[9]!='') ? $question_ans[9] : ' ';
          $osdata['user_style'] =   (@$question_ans[2]!='') ? $question_ans[2] : ' ';
          $osdata['user_budget'] =   (@$question_ans[7]!='') ? $question_ans[7] : ' ';
          $osdata['user_body_shape'] = (@$question_ans[1]!='') ? $question_ans[1] : ' ';
          $osdata['abandoned_cart'] =  (@$cart==1) ? 'yes' : 'no';   

        }else
        { /*For Male*/          
          $bt = strip_tags($question_ans[13]);
          $bodyType=substr($bt, 0, strrpos($bt, ' '));
          $osdata['user_age'] =   (@$question_ans[15]!='') ? $question_ans[15] : ' ';
          $osdata['body_type'] =   (@$bodyType!='') ? $bodyType : ' ';
          $osdata['bucket_id'] =   (@$this->session->userdata('bucket')!='') ? $this->session->userdata('bucket') : ' ';
          $osdata['work_style'] =   (@$question_ans[14]!='') ? strip_tags($question_ans[14]) : ' ';
          $osdata['body_height'] =   (@$question_ans[17]!='') ? $question_ans[17] : ' ';
          $osdata['user_budget'] =   (@$question_ans[18]!='') ? $question_ans[18] : ' ';
          $osdata['personal_style'] =   (@$question_ans[16]!='') ? $question_ans[16] : ' ';
          $osdata['abandoned_cart'] =   (@$cart==1) ? 'yes' : 'no';         
        }

        $encode_os = json_encode($osdata);
         //$this->session->set_userdata(array('onesignaldata'=>$os_data));
        return $encode_os;
  }



}