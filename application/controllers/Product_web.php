<?php
// defined('BASEPATH') OR exit('No direct script access allowed');

class Product_web extends MY_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('Pa_model');
		$this->load->model('Product_desc_model');
		$this->load->model('Collection_model');
		$this->load->model('Event_model');
		$this->load->model('Mobile_look_model');
		$this->load->model('Favorites_model');
		$this->load->model('Mobile_model_android');
	}

	public function index(){
	}
	
	public function product_details(){
		$product_id = $this->uri->segment(count($this->uri->segment_array()));
		if(strrpos($product_id,'-')) $product_id = substr($product_id,strrpos($product_id,'-',-1)+1);
		
		if(isset($product_id) && $product_id && is_numeric($product_id) && $product_id >0){
			$data = array();
			// $product_id = $this->uri->segment(3);
			$data['user_id'] = $this->session->userdata('user_id');
			$product_related = 0;
			$limit = 'limit 2';
			$data['product_data'] = (array)$this->Mobile_look_model->single_product_data($product_id,$data['user_id']);
			$data['related_product'] = (array)$this->Mobile_look_model->get_product_related_products($product_id,$product_related,$data['user_id']);
			$data['review'] = $this->Product_desc_model->get_review($product_id,$limit);
			$data['product_breadcrumps'] = $this->Product_desc_model->product_breadcrumps($product_id);
			$this->seo_title = ' '.@$data['product_data']['name'].' - Buy Online Now on StyleCracker';
			$this->seo_desc = '';
			$this->load->view('seventeen/common/header_view');
			$this->load->view('seventeen/product_details',$data);
			$this->load->view('seventeen/common/footer_view');
		}else{
			
			$this->load->view('common/header_view');
			$this->load->view('errors/error_404');
			$this->load->view('common/footer_view');

		}
		
	}
	
	public function post_review(){
		$data['user_id'] = $this->session->userdata('user_id');
		$data['object_id'] = $this->input->post('product_id');
		$data['ratings'] = $this->input->post('ratings');
		$data['user_name'] = $this->input->post('name');
		$data['review_text'] = $this->input->post('comment');
		$data['created_datetime'] = $this->config->item('sc_date');
		$data['created_by'] = $data['user_id'];
		$data['modified_by'] = $data['user_id'];
		$review_data = $this->Product_desc_model->insert_review($data);
		echo $review_data;
		
	}
	
	public function get_review(){
		$product_id = $this->input->post('product_id');
		$limit = 'limit 2';
		$review_html = '';
		$review = $this->Product_desc_model->get_review($product_id,$limit);
		if(!empty($review)){
			foreach($review as $value){
				$review_html = $review_html.'<li>
							  <div class="ratings">
								<span class="shape-diamond">'.$value['ratings'].'</span>
							  </div>
							  <div class="desc">
								'.$value['review_text'].'
							  </div>
							  <div class="meta">'.$value['user_name'].' | '.date("Y-m-d",strtotime($value['created_datetime'])).'</div>
							</li>';
			}
		}
	    echo $review_html;
	}
	
	public function get_all_reviews(){
		$product_id = $this->input->post('product_id');
		$limit = ' ';
		$review_html = '';
		$review = $this->Product_desc_model->get_review($product_id,$limit);
		// echo "<pre>";print_r($review);exit;
		if(!empty($review)){
			foreach($review as $value){
				$review_html = $review_html.'<li>
							  <div class="ratings">
								<span class="shape-diamond">'.$value['ratings'].'</span>
							  </div>
							  <div class="desc">
								'.$value['review_text'].'
							  </div>
							  <div class="meta">'.$value['user_name'].' | '.date("Y-m-d",strtotime($value['created_datetime'])).'</div>
							</li>';
			}
		}
	    echo $review_html;
	}
	
	
	
	
}