<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';

class Mobilescart_android extends TT_REST_Controller 
{
  function __construct()
	{
		parent::__construct();
		$this->load->model('Schome_model');
		$this->load->model('Mobile_model');
		$this->load->model('Cart_model');
		$this->load->model('Mcart_model_android');
		$this->load->model('Looks_model');
		$this->load->model('cartnew_model');
		$this->load->model('Pa_model');
		$this->load->library('user_agent');
		$this->load->library('curl');  
		$this->open_methods = array('scgenrate_hashes_post'); 
	}
	
	
	public function update_cart_post(){
		
		//$uniquecookie = $this->post('SCUniqueID');
		$productid = $this->post('productid');
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$productsize = $this->post('productsize');
		$paymod = $this->post('paymod');
		$type = $this->post('type');
		$medium = $this->post('medium');
		$look_id = $this->post('look_id');
		if($type == ''){
			$type	= '1';
		}
		if($paymod == ''){
			$paymod	= 'online';
		}
		if ($this->agent->is_browser())
		{
			$agent = 'Computer '.$this->agent->browser().' '.$this->agent->version();
		}
		elseif ($this->agent->is_robot())
		{
			$agent = 'Robot '.$this->agent->robot();
		}
		elseif ($this->agent->is_mobile())
		{
			$agent = 'Mobile '.$this->agent->mobile();
		}
		else
		{
			$agent = 'Unidentified';
		} 
		$product_click = $this->Mcart_model_android->add_product_click($userid,$productid,$type);
		$data = (object)$this->Mcart_model_android->update_cart($productid,$userid,$productsize,$paymod,$agent,$medium,$look_id);
		
			if($this->post('test') == 1){ 
				echo BACKENDURL.'/Mailchimp/mailchimp_update_cart/'.$data->items[0]->cart_id.'/'.$userid;exit;
			}
		
		if($data !=NULL){
			
			/*Mailchimp call For AddToCart*/
			// if($userid >0)
			// {  
				// $result = $this->curl->simple_post(BACKENDURL.'/Mailchimp/mailchimp_update_cart/'.$data->items[0]->cart_id.'/'.$userid, false, array(CURLOPT_USERAGENT => true));     
			// }

			/*End Mailchimp call For AddToCart*/
	
			$this->success_response($data);
		}else{
			$this->success_response($data);
		}
	}
	
	public function deleteCartProduct_post(){
			
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$productid = $this->input->post('product_id');
		$cart_id = $this->input->post('cart_id');
		$paymod = $this->post('paymod');
		$sc_credit = $this->post('sc_credit');
		$coupon_code = $this->post('coupon_code');
		
		if($paymod == ''){
			$paymod	= 'online';
		}
		$pincode = $this->post('pincode');
		if($this->checkpincode($pincode) != 2 && $pincode != ''){
        $this->failed_response(1010, "Pincode does not exist");
		}
		$data = (object)$this->Mcart_model_android->deleteCartProduct($productid,$userid,$cart_id,$paymod,$pincode,$sc_credit,$coupon_code);
		
		/* MailChimp Cart Remove*/
		if($userid > 0)
		{ 
			$this->curl->simple_post(BACKENDURL.'/Mailchimp/deleteCartLine/'.$userid.'/'.$userid.'/'.$cart_id, false, array(CURLOPT_USERAGENT => true));   
		}
		/* End MailChimp Cart Remove*/
	
		if($data !=NULL){
		 $this->success_response($data);
		}else{
			$res = array();
		   $this->success_response($data);
		   //$this->failed_response(1001, "No Record(s) Found");
		}
	}
	
	public function getBrandUsername_get(){
		$brand_id = $this->get('brand_id');
		$data = $this->Mcart_model_android->getBrandUsername($brand_id);
		
		if($data !=NULL){
		 $this->success_response($data);
		}else{
			$res = array();
		   $this->success_response($data);
		   //$this->failed_response(1001, "No Record(s) Found");
		}
	}
	
	public function productQtyUpdate_post(){
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$product_id = $this->input->post('product_id');
		$productQty = $this->input->post('productQty');
		$cart_id = $this->input->post('cart_id');
		$paymod = $this->post('paymod');
		$pincode = $this->post('pincode');
		$sc_credit = $this->post('sc_credit');
		$coupon_code = $this->post('coupon_code');
		
		if($this->checkpincode($pincode) != 2 && $pincode != ''){
        $this->failed_response(1010, "Pincode does not exist");
		}
                if($productQty>5){
                $this->failed_response(1001, "product quantity should be less than 5");
                 }
		if($paymod == ''){
			$paymod	= 'online';
		}
		//$productInfo = base64_decode(str_replace("product_cart_info_","",$this->input->post('productInfo')));
		if($productQty>0 && $productQty<=5){
			$data = (object)$this->Mcart_model_android->productQtyUpdate($product_id,$productQty,$userid,$cart_id,$paymod,$pincode,$sc_credit,$coupon_code);
			
			/*Mailchimp call For Update Cart */
			// if($userid > 0)
			// {  
				// $this->curl->simple_post(BACKENDURL.'/Mailchimp/mailchimp_update_cart/'.$cart_id.'/'.$userid, false, array(CURLOPT_USERAGENT => true));     
			// }
			/*End Mailchimp call For Update Cart */

			if($data !=NULL){
				$this->success_response($data);
			}else{
				$res = array();
				$this->success_response($res);
			}
		}
	}
	
	public function get_cart_post(){
		
		$userid 		= $this->post('user_id');
		$auth_token 	= $this->post('auth_token');
		$paymod 		= $this->post('paymod');
		$coupon_code 	= $this->post('coupon_code');
		$sc_credit 		= $this->post('sc_credit');
		
		if($paymod == ''){
			$paymod	= 'online';
		}
		$pincode = $this->post('pincode');
		if($this->checkpincode($pincode) != 2 && $pincode != ''){
        $this->failed_response(1010, "Pincode does not exist");
		}
		$data = (object)$this->Mcart_model_android->get_cart($userid,$paymod,$pincode,$chkShipping='',$chkCod='',$city='',$state='',$coupon_code,$sc_credit);
		if($data !=NULL && !empty($data)){
		 $this->success_response($data);
		}else{
			$res = new stdclass();
			$res->item = '0';
			$this->success_response($res);
		}
	}
		
	
		
		public function scbox_get_cart_post(){
		
		$userid 		= $this->post('user_id');
		$auth_token 	= $this->post('auth_token');
		$paymod 		= $this->post('paymod');
		$coupon_code 	= $this->post('coupon_code');
		$sc_credit 		= $this->post('sc_credit');
		$cartid 		= $this->post('cart_id');
		$address_id 	= $this->post('address_id');
		
		if($userid 	== ''){ $this->failed_response(1006, "Please check user id"); }
		if($auth_token 	== ''){ $this->failed_response(1006, "Please check auth token"); }
		if($cartid 		== ''){ $this->failed_response(1006, "Please check Cart id"); }
		if($address_id 	== ''){ $this->failed_response(1006, "Please check Address id"); }
		
		if($paymod == ''){
			$paymod	= 'online';
		}
		
		$pincode = $this->post('pincode');
		if($this->checkpincode($pincode) != 2 && $pincode != ''){
        $this->failed_response(1010, "Pincode does not exist");
		}
		$scbox_package = unserialize(SCBOX_PACKAGE); 
		$cart_data 		= $this->Mcart_model_android->get_product_by_cart_id($cartid);
		$address_data 	= $this->Mcart_model_android->get_address_by_address_id($userid, $address_id);
		if($address_data 	== '' && count($address_data)>0){ $this->failed_response(1006, "Please check Address id"); }
		
		if($address_data['pincode'] != ''){
			$productid 		= $this->Mcart_model_android->get_product_id($userid);
			
			$chkShipping 	= $this->Mcart_model_android->chkShipping2($productid,$address_data['pincode']);
			$chkCod 		= $this->Mcart_model_android->chkCod2($productid,$address_data['pincode']);
			//$city_name 		= $this->Mcart_model_android->get_city_name($pincode);
			
			if(!empty($address_data['city_name'])){
				$city 	= $address_data['city_name']; 
				$state 	= $address_data['state_name'];
			} 
		}
		
		// Add for first purchase
		if($coupon_code == '0')
		{
			$coupon_code ='';
		}
		else if (!empty($coupon_code))
		{
			$coupon_code = $coupon_code;
		}
		else if(!empty($userid))
		{
		 $ordercount_result = $this->cartnew_model->get_ordercount($userid);
		 if($ordercount_result==0)
		 {
		  $coupon_code = $this->cartnew_model->get_defaultcoupons($userid,'','scboxcart',$cart_data['product_price']);
		 }
		//Select count(id) as ordercount from order_info where user_id="45256" and brand_id = "21009" and order_status!=9 and order_status!=7
		}
		//End- Add for first purchase
		
		$arrCartData = array();
		$arrCartData['res']			= $cart_data;
		$arrCartData['userid']		= $userid;
		$arrCartData['paymod']		= $paymod;
		$arrCartData['coupon_code']	= $coupon_code;
		$arrCartData['sc_credit']	= $sc_credit;
		$arrCartData['auth_token']	= $auth_token;
		
		
		$cart_array = array();
		
		if(($coupon_code == 'NOCASH500' || $coupon_code == 'nocash500' || $coupon_code == 'NOCASH1000' || $coupon_code == 'nocash1000') && $paymod == 'cod')
		{ $cod_available = 0;$coupon_code=''; }
		else { $cod_available = 1;}
		
		$cart_object = new stdClass(); 
		$data 				=	new stdClass();
		$data->item_count 	= $cart_data['item_count'];
		$data->item_qty 	= $cart_data['item_qty'];
		$data->pincode 		= $address_data['pincode'];
		$data->city 		= $city;
		$data->state 		= $state;
		$res 				= $this->Mcart_model_android->product_stock($userid);
		
		$count 				= count($res);
		//$count1 			= count($chkShipping);
		$count2 			= '';
		
		if($count1=1 && !empty($chkShipping)){
			$notship = $this->check_ship($chkShipping);
			$data->error_message = $notship[0];
		}
		else if($count1>1){
			$data->error_message = (object)array('text' => "Remove non-shippable products from cart or try another pin code.", 'color' => '#dd4b39');
		}
		
		if(empty($data->error_message)){
			if($count=1 && !empty($res)) {
				$data->error_message = $res[0][0];
				}
			else  if($count>1){ 
				$data->error_message = (object)array('text' => "Remove Out Of Stock products to proceed.", 'color' => '#dd4b39');
			}
		}
		if(empty($data->error_message)){
			//$count2 = count($chkCod);
			if($count2=1 && !empty($chkCod)){
				$notcod = $this->check_cod($chkCod);
				//$data->error_message = $notcod[0];
				$data->error_message = (object)array('text' => "COD unavailable.Try another pincode OR proceed with Online Payment.", 'color' => '#dd4b39');
			}
			else  if($count2>1){
				$data->error_message = (object)array('text' => "COD not available. Proceed with online payment?", 'color' => '#dd4b39');
			}
		}
		if((!empty($chkShipping) && empty($data->error_message)) || $count>0) $data->is_checkout_allowed = '0'; else $data->is_checkout_allowed = '1';
		if($coupon_code == 'NOCASH500' || $coupon_code == 'nocash500' || $coupon_code == 'NOCASH1000' || $coupon_code == 'nocash1000'){
			$data->error_message = (object)array('text' => "COD unavailable. Try another coupon code OR proceed with Online Payment.", 'color' => '#dd4b39');
		}
		if($count2 >= 1 && !empty($chkCod)){
			$data->payment_methods = array('online');
		}else{
			$data->payment_methods = array('online', 'COD');
		}
		
		if($pincode != '' && empty($data->error_message)){ 
			$data->error_message = (object)array('text' => "Choose a payment option and place your order.", 'color' => '#00a65a');
		}
		
		//$data->total_summary = $this->Mcart_model_android->total_summary($userid,$paymod,$coupon_code,$sc_credit,$is_scbox);
		$data->total_summary = $this->Mcart_model_android->total_summary($userid,$paymod,$coupon_code,$sc_credit,1);
		//$data->total_summary = $this->Mcart_model_android->total_summary($userid,$paymod,$coupon_code,$sc_credit);
		$cart_object->data = $data;
		//$cart_object->items = $this->product_object($userid,$pincode,$is_scbox);
		$data->items = $cart_data;
		$data->total_summary->coupon_code = $coupon_code;
		if($cart_data['product_id'] == SCBOX_CUSTOM_ID){
			$data->is_cod_available 	= '0';
			$data->is_cod_available_msg = 'Cash On Delivery is unavailable for custom box.';
		}
		else{
			$data->is_cod_available 	= '1';
			$data->is_cod_available_msg = '';
		}
		 if($data !=NULL && !empty($data)){ 
		 $this->success_response($data);
		 }else{
			$res = new stdclass();
			$res->item = '0';
			$this->success_response($res);
		} 
		
	}
		
		
	public function sc_uc_order_post(){
		
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$address_id = $this->input->post('address_id');
		$billing_address_id = $this->input->post('billing_address_id');
		if($billing_address_id <= 0) $billing_address_id = $address_id;
		$billing_address = $this->Mcart_model_android->user_info($billing_address_id);
		$user_info = $this->Mcart_model_android->user_info($address_id);
		$paymod = $this->input->post('paymod');
		$coupon_code = $this->post('coupon_code');
		$sc_credit = $this->post('sc_credit');
		$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		if($paymod == 'cod'){
			$paymod_v = 1;
		}else {
			$paymod_v = 2;
		}
		
		$address = array();
		$address['billing_first_name'] 	= $billing_address['first_name'];
		$address['billing_last_name'] 	= $billing_address['last_name'];
		$address['billing_mobile_no'] 	= $billing_address['mobile_no'];
		$address['billing_pincode_no'] 	= $billing_address['pincode'];
		$address['billing_address'] 	= $billing_address['shipping_address'];
		$address['billing_email_id'] 	= $billing_address['email'];
		$address['billing_city'] 		= $billing_address['city_name'];
		$address['billing_state'] 		= $this->Mcart_model_android->get_state_name($billing_address['state_name']);
		
		$data = array();
		
		$data['user_id'] 			= $userid;
		$data['uc_first_name'] 		= $user_info['first_name'];
		$data['uc_last_name'] 		= $user_info['last_name'];
		$data['uc_email_id'] 		= $user_info['email'];
		$data['uc_mobile'] 			= $user_info['mobile_no'];
		$data['uc_address'] 		= $user_info['shipping_address'];
		$data['uc_pincode'] 		= $user_info['pincode'];
		$data['uc_city'] 			= $user_info['city_name'];
		$data['uc_state'] 			= $user_info['state_name'];
		$data['paymod'] 			= $paymod;
		$data['mihpayid'] 			= '';
		$data['txnid'] 				= '';
		$data['coupon_code'] 		= $coupon_code;
		$data['sc_credit'] 			= $sc_credit;
		$data['billing_address'] 	= json_encode($address);
		
		if($paymod == 'cod' or $paymod == 'online'){
		  $res = $this->Mcart_model_android->sc_uc_order2($data); 
		  
		  if($res !=NULL && $res != '2'){
			$order_no = $res->order_no; 
			if($paymod == 'cod'){
				/* MailChimp Remove From Cart after order*/
				$mc_user = explode('_',$order_no);	     
				if(@$mc_user[0]!='')
				{
				$this->curl->simple_post(BACKENDURL.'/Mailchimp/delete_mccart/'.$mc_user[0].'/'.$mc_user[0], false, array(CURLOPT_USERAGENT => true));
				}
				/* MailChimp Remove From Cart after order*/
			  @$this->orderPlaceMessage($userid,$order_no,$data['uc_first_name'],$data['uc_last_name']);
			  $this->orderPlaceMessagetoBrand($userid,$order_no,$data['uc_first_name'],$data['uc_last_name']);
			}
			if($paymod == 'online'){
				$paymod_online = '2';
				$result = $this->Mcart_model_android->failed_order($user_info['first_name'],$user_info['last_name'],$user_info['email'],$user_info['shipping_address'],$userid,$user_info['mobile_no'],$paymod_online,$user_info['pincode'],$user_info['city_name']);
			}
		   $this->success_response($res);
		}else{
		   $this->failed_response(1201, "Your cart is empty");
		}
		 
		}

	}
	
	
	
	public function scbox_uc_order_post(){
		/////// 04-12-2017
		$userid 			= $this->input->post('user_id');
		$auth_token 		= $this->input->post('auth_token');
		$billing_address_id = $this->input->post('billing_address_id');
		$shipping_address_id = $this->input->post('shipping_address_id');
		
		if($shipping_address_id <= 0) $shipping_address_id = $billing_address_id;
		
		$billing_address 	= $this->Mcart_model_android->user_info($shipping_address_id);
		$user_info 			= $this->Mcart_model_android->user_info($billing_address_id);
		$paymod 			= $this->input->post('paymod');
		$coupon_code 		= $this->input->post('coupon_code');
		$sc_credit 			= $this->input->post('sc_credit');
		
		$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		if($paymod == 'cod'){
			$paymod_v = 1;
		}else {
			$paymod_v = 2;
		}
		
		$address = array();
		$address['billing_first_name'] 	= $billing_address['first_name'];
		$address['billing_last_name'] 	= $billing_address['last_name'];
		$address['billing_mobile_no'] 	= $billing_address['mobile_no'];
		$address['billing_pincode_no'] 	= $billing_address['pincode'];
		$address['billing_address'] 	= $billing_address['shipping_address'];
		$address['billing_email_id'] 	= $billing_address['email'];
		$address['billing_city'] 		= $billing_address['city_name'];
		$address['billing_state'] 		= $this->Mcart_model_android->get_state_name($billing_address['state_name']);
		
		$data = array();
		
		$data['user_id'] 		= $userid;
		$data['uc_first_name'] 	= $user_info['first_name'];
		$data['uc_last_name'] 	= $user_info['last_name'];
		$data['uc_email_id'] 	= $user_info['email'];
		$data['uc_mobile'] 		= $user_info['mobile_no'];
		$data['uc_address'] 	= $user_info['shipping_address'];
		$data['uc_pincode'] 	= $user_info['pincode'];
		$data['uc_city'] 		= $user_info['city_name'];
		$data['uc_state'] 		= $user_info['state_name'];
		$data['paymod'] 		= $paymod;
		$data['mihpayid'] 		= '';
		$data['txnid'] 			= $txnid;
		$data['coupon_code'] 	= $coupon_code;
		$data['sc_credit'] 		= $sc_credit;
		$data['is_scbox'] 		= 1;
		$data['billing_address'] = json_encode($address);
		/* echo "<pre>";
		print_r($data);
		echo "</pre>"; */
		
		if($paymod == 'cod' or $paymod == 'online'){
		  $res = $this->Mcart_model_android->sc_uc_order2($data);
		  
		  if($res !=NULL && $res != '2'){
			
			$cart_id = $this->Mcart_model_android->get_cart_id_by_order_id($res->id);  
			$this->Pa_model->update_cartid_to_order_unique_no($res->order_display_no,$userid,$cart_id['id']);
		 
			$order_no = $res->order_no; 
			if($paymod == 'cod'){
				/* MailChimp Remove From Cart after order*/
				$mc_user = explode('_',$order_no);	     
				if(@$mc_user[0]!='')
				{
				$this->curl->simple_post(BACKENDURL.'/Mailchimp/delete_mccart/'.$mc_user[0].'/'.$mc_user[0], false, array(CURLOPT_USERAGENT => true));
				}
				/* MailChimp Remove From Cart after order*/
			  @$this->orderPlaceMessage($userid,$order_no,$data['uc_first_name'],$data['uc_last_name']);
			  $this->orderPlaceMessagetoBrand($userid,$order_no,$data['uc_first_name'],$data['uc_last_name']);
			}
			if($paymod == 'online'){
				$paymod_online = '2';
				$result = $this->Mcart_model_android->failed_order($user_info['first_name'],$user_info['last_name'],$user_info['email'],$user_info['shipping_address'],$userid,$user_info['mobile_no'],$paymod_online,$user_info['pincode'],$user_info['city_name']);
			}
		   $this->success_response($res);
		}else{
		   $this->failed_response(1201, "Your cart is empty");
		}
		 
		}

	}
	
	
    function getEmailAddress($user_id){
      $res = $this->Cart_model->get_user_info($user_id);
      if(!empty($res)){
        return $res[0]['email'];
      }else{ return ''; }
	}
	public function get_city_name_get(){
		
		$pincode = $this->input->get('pincode');
		$validate_pincode = $this->Mcart_model_android->validate_pincode($pincode);
		if(strlen($pincode) != 6){
        $this->failed_response(1012, "pincode should contain 6 digits only ");
		}else if($validate_pincode == 2){
        $this->failed_response(1013, "please enter valid pincode");
		}
		$data = (object)$this->Mcart_model_android->get_city_name($pincode);
		if($data !=NULL){
		 $this->success_response($data);
		}else{
		   $this->success_response($data);
		   //$this->failed_response(1001, "No Record(s) Found");
		}
	}
	
	public function get_order_info_get(){
      $uniquecookie = $this->get('SCUniqueID');
      $res['order_info']= $this->Mcart_model_android->get_order_info($uniquecookie);
      $res['brand_price'] = $this->Mcart_model_android->get_order_price_info($uniquecookie);
	  print_r($res);exit;
	}
	
	public function getPinCode_get(){
        $pincode = $this->get('pincode');
        $product_id = $this->get('productid');
		$validate_pincode = $this->Mcart_model_android->validate_pincode($pincode);
        $chkShipping = $this->Mcart_model_android->chkShipping($product_id,$pincode);
		$delivery_date = $this->Mcart_model_android->get_delivery_date($product_id);
		$deliver_message = '';
		if(!empty($delivery_date)){
		if($delivery_date['min_del_days']>0 && $delivery_date['max_del_days']>0){
						$deliver_message = 'Delivery in '.$delivery_date['min_del_days'].'-'.$delivery_date['max_del_days'].' working days.';
					}else if(($delivery_date['min_del_days']>0 && $delivery_date['max_del_days']=='') || ($delivery_date['min_del_days']=='' && $delivery_date['max_del_days']>0)){
						$deliver_message = 'Delivery in '.$delivery_date['min_del_days'].$delivery_date['max_del_days'].' working days.';
					}
		}else $deliver_message = '';
        $chkCod = $this->Mcart_model_android->chkCod($product_id,$pincode);
		if(strlen($pincode) != 6){
        $this->failed_response(1012, "pincode should contain 6 digits only ");
		}else if($validate_pincode == 2){
        $this->failed_response(1013, "please enter valid pincode");
		}
		if(empty($chkShipping)){ $shipping_message = 'Shipping to pincode '.$pincode.' is available'; } else $shipping_message = 'Shipping to pincode '.$pincode.' is not available'; 
		if(empty($chkCod)){ $cod_message = 'COD to pincode '.$pincode.' is available'; } else $cod_message = 'COD to pincode '.$pincode.' is not available';
		
		$data['shipping_message'] = $shipping_message;
		$data['cod_message'] =	$cod_message;
		$data['deliver_message'] = $deliver_message;
		/*
		if(!empty($chkShipping) && !empty($chkCod)){
          $this->success_response('Shipping to pincode '.$pincode.' is not-available');
        }else if(!empty($chkShipping) && empty($chkCod)){
          $this->success_response('Shipping to pincode '.$pincode.' is not-available');
        }else if(empty($chkShipping) && !empty($chkCod)){
          $this->success_response('COD unavailable for this product at '.$pincode.'.');
        }else{
          $this->success_response('Shipping to pincode '.$pincode.' is available');
        }*/
		$this->success_response(json_encode($data));
	}
	
	/* public function get_order_post(){
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$order_unique_no = $this->post('order_unique_no');
		$data = $this->Mcart_model_android->get_order($order_unique_no,$userid);
		if($data !=NULL){
		 $this->success_response($data);
		}else{
			$res = array();
		   $this->success_response($data);
		   //$this->failed_response(1001, "No Record(s) Found");
		}
	} */
	
	public function get_order_post(){
		$userid 			= $this->post('user_id');
		$auth_token 		= $this->post('auth_token');
		$order_unique_no 	= $this->post('order_unique_no');
		
		if($userid == ''){ $this->failed_response(1006, "Please check user id"); }
		if($auth_token == ''){ $this->failed_response(1006, "Please check auth token"); }
		if($order_unique_no == ''){ $this->failed_response(1006, "Please check order unique no"); }
		
		$data 					= $this->Mcart_model_android->get_order($order_unique_no,$userid);
		$cartAwbNumber 			= $this->cartnew_model->get_cartid_for_awbnumber($data->id);
		$cartMetaData  			= $this->cartnew_model->get_cart_meta_data($cartAwbNumber['id']);
		$delpartner  			= $this->cartnew_model->get_del_partner($cartMetaData[0]['cart_meta_value']);
		$data->awb_no 			= ($cartMetaData[1]['cart_meta_value'])?$cartMetaData[1]['cart_meta_value']:'';
		$data->tracking_link 	= ($delpartner['tracking_link'])?$delpartner['tracking_link']:'';
		$data->partner_name 	= ($delpartner['delivery_partner_name'])?$delpartner['delivery_partner_name']:'';
		if($data !=NULL){
		 $this->success_response($data);
		}else{
		$res = array();
		   $this->success_response($data);
		   //$this->failed_response(1001, "No Record(s) Found");
		}
	}
	
	/*public function get_order_list_post(){
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$limit = $this->post('limit');
		$offset = $this->post('offset');
		$limit = 50;$offset = 0;
		$data = $this->Mcart_model_android->get_order_list($userid,$limit,$offset);
		if($data !=NULL){
		 $this->success_response($data);
		}else{
			$data = array();
			$this->success_response($data);
		   //$this->failed_response(1001, "No Orders(s) Found please order something");
		}
	}*/
	
	public function get_order_list_post(){
		$userid 	= $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$limit 		= $this->post('limit');
		$offset 	= $this->post('offset');
		$limit 		= 50;$offset = 0;
		$data 		= $this->Mcart_model_android->get_order_list($userid,$limit,$offset);
		//exit();
		if($data !=NULL){
		 $this->success_response($data);
		}else{
			$data = array();
			$this->success_response($data);
		   //$this->failed_response(1001, "No Orders(s) Found please order something");
		}
	}
	
	public function validate_coupon_post(){
		
		$userid 				= $this->input->post('user_id');
		$auth_token 			= $this->input->post('auth_token');
		$total_product_price 	= $this->input->post('total_product_price');
		$email 					= $this->input->post('email');
		$coupon_code 			= strtolower($this->input->post('coupon_code'));
		$bin_no 				= $this->input->post('bin_no');
		$is_input 				= $this->input->post('is_input');
		if($is_input == '1'){
			$check_is_special_code = $this->Mcart_model_android->check_is_special_code($coupon_code);
			if($check_is_special_code == '0'){
				$this->failed_response(1010, "This coupon code is not valid");
			}
		}
		$response = $this->Mcart_model_android->check_coupon_exist($coupon_code,$email,$total_product_price,$bin_no);
		if(!empty($response)){
			if($response['msg_type'] == '1'){
			$this->success_response($response);
			}else if($response['msg_type'] == '0'){
				$this->failed_response(1010, $response['msg']);
			}
		}else{
			//$this->failed_response(1010, "Something went wrong please try again.");
			$this->failed_response(1010, "This coupon code is not valid. try another coupon code.");
		}
		
	}
	
	
	
	/*public function change_status_post(){
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$order_id = $this->post('order_id');
		$status = $this->post('status');
		$res = $this->Mcart_model_android->change_status($userid,$order_id,$status);
		if($res == 1){
			$result = $this->Mcart_model_android->get_order($order_id,$userid);
		}
		if($status == 1){
			// MailChimp Remove From Cart after order
				$mc_user = explode('_',$order_id);	     
				if(@$mc_user[0]!='')
				{
					$this->curl->simple_post(BACKENDURL.'/Mailchimp/delete_mccart/'.$mc_user[0].'/'.$mc_user[0], false, array(CURLOPT_USERAGENT => true));
				}
				// MailChimp Remove From Cart after order
				
			$res1 = $this->Mcart_model_android->update_order_cart_info_online_order($userid,$order_id);
			$change_failed_order_ststus = $this->Mcart_model_android->update_fail_order_status_online($userid,$order_id);
		$this->orderPlaceMessage($userid,$order_id,@$data['uc_first_name'],@$data['uc_last_name']);
		$this->orderPlaceMessagetoBrand($userid,$order_id,@$data['uc_first_name'],@$data['uc_last_name']);
		}else{
			$res1 = (string)$this->Mcart_model_android->transaction_failed($userid,$order_id);
		}
		if(!empty($result)){
			$this->success_response($result);
		}
		else{
			$this->failed_response(1300, "Wrong credentials!");
		}	
	}*/
	
	public function get_coupons_post(){
		$userid 	= $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$user_email = $this->post('user_email');
		$couponcodes = array();
		$couponcodes = $this->Mcart_model_android->get_coupons($userid,$user_email);
		//echo $this->db->last_query();
		//exit();
		if(!empty($couponcodes)){
			$this->success_response($couponcodes);
		}else {
			$res = array();
			$this->success_response($res);
		}
	}
	
		public function change_status_post(){
		$userid 			= $this->post('user_id');
		$auth_token 		= $this->post('auth_token');
		$order_id 			= $this->post('order_id');
		$status 			= $this->post('status');
		$is_offer_applied 	= $this->post('is_offer_applied');
		
		$res = $this->Mcart_model_android->change_status($userid,$order_id,$status,$is_offer_applied);
		if($res == 1){
			$result = $this->Mcart_model_android->get_order($order_id,$userid);
		}
		if($status == 1){
			/* MailChimp Remove From Cart after order*/
				$mc_user = explode('_',$order_id);	     
				if(@$mc_user[0]!='')
				{
					$this->curl->simple_post(BACKENDURL.'/Mailchimp/delete_mccart/'.$mc_user[0].'/'.$mc_user[0], false, array(CURLOPT_USERAGENT => true));
				}
				/* MailChimp Remove From Cart after order*/
				
			$res1 = $this->Mcart_model_android->update_order_cart_info_online_order($userid,$order_id);
			$change_failed_order_ststus = $this->Mcart_model_android->update_fail_order_status_online($userid,$order_id);
		$this->orderPlaceMessage($userid,$order_id,@$data['uc_first_name'],@$data['uc_last_name']);
		$this->orderPlaceMessagetoBrand($userid,$order_id,@$data['uc_first_name'],@$data['uc_last_name']);
		}else{
			$res1 = (string)$this->Mcart_model_android->transaction_failed($userid,$order_id);
		}
		if(!empty($result)){
			$this->success_response($result);
		}
		else{
			$this->failed_response(1300, "Wrong credentials!");
		}	
	}
	
	//productQtyUpdate
	public function productSizeUpdate_post(){
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$product_id = $this->input->post('product_id');
		$productsize = $this->input->post('productsize');
		$cart_id = $this->post('cart_id');
		//$productInfo = base64_decode(str_replace("product_cart_info_","",$this->input->post('productInfo')));
		$data = (object)$this->Mcart_model_android->productSizeUpdate($product_id,$productsize,$userid,$cart_id);
		
		
		if($data !=NULL){
		 $this->success_response($data);
		}else{
			$res = array();
		   $this->success_response($data);
		   //$this->failed_response(1001, "No Record(s) Found");
		}
	}
	/*
	function multiPinCode_get(){
        $pincode = $this->get('pincode');
		// $userid = $this->post('user_id');
		// $auth_token = $this->post('auth_token');
        $product_id = $this->get('productid');
		$payment_method = $this->get('payment_method');
		
		if($payment_method == 'online'){
			$i=0;
			$text_array = array();
			$chkShipping = $this->Mcart_model_android->chkShipping2($product_id,$pincode);
			if(!empty($chkShipping)){
				foreach($chkShipping as $val){
					//echo $val['id'];exit;
					$product_object = new stdClass();
					$product_info = $this->Mcart_model_android->product_info($val['id']);
					$product_object->text =  'Shipping not-available for '.$product_info[0]['name'].' please remove this item from shopping cart and continue to place order';
					$product_object->image_url = "https://www.stylecracker.com/sc_admin/assets/products/".$product_info[0]['image'];
					$text_array[$i] = $product_object;
					$i++; 
				}
			}
		}
		else if($payment_method == 'cod'){
			$j=0;
			$text_array = array();
			$chkCod = $this->Mcart_model_android->chkCod2($product_id,$pincode);
			if(!empty($chkCod)){
				foreach($chkCod as $val){
					//echo $val['id'];exit;
					$product_object = new stdClass(); 
					$product_info = $this->Mcart_model_android->product_info($val['id']);
					$product_object->text =  'COD not-available for '.$product_info[0]['name'].' please remove this item from shopping cart and continue to place order';
					$product_object->image_url = "https://www.stylecracker.com/sc_admin/assets/products/".$product_info[0]['image'];
					$text_array[$j] = $product_object;
					$j++; 
				}
			}
		}
		$this->success_response($text_array);
	}
	*/
	
	public function multiPinCode_post(){
        $pincode = $this->post('pincode');
		$user_id = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$sc_credit = $this->post('sc_credit');
		$coupon_code = $this->post('coupon_code');
		if(trim($pincode) == ''){
		$this->failed_response(1007, "Please enter the pincode");

		}else if(strlen($pincode) != 6){
        $this->failed_response(1013, "pincode should contain 6 digit only");
		}
		$product_id = $this->Mcart_model_android->get_product_id($user_id);
		$chkShipping = $this->Mcart_model_android->chkShipping2($product_id,$pincode);
		$chkCod = $this->Mcart_model_android->chkCod2($product_id,$pincode);
		$city_name = $this->Mcart_model_android->get_city_name($pincode);
        $paymod = $this->post('paymod');
		if($paymod == ''){
			$paymod	= 'online';
		}
		if(!empty($city_name)){
			$city = $city_name['city']; 
			$state = $city_name['state'];
		
		
		$data = (object)$this->Mcart_model_android->get_cart($user_id,$paymod,$pincode,$chkShipping,$chkCod,$city,$state,$coupon_code,$sc_credit);
		if($data !=NULL){
		 $this->success_response($data);
		}else{
			$res = array();
		   $this->success_response($data);
		   //$this->failed_response(1001, "No Record(s) Found");
		}
		
		}
		else $this->failed_response(1001, "Please enter valid Pincode");
		
	}
	public function getaddress_post(){
		$user_id = $this->post('user_id');
		$auth_token = $this->post('auth_token');
        $data = $this->Mcart_model_android->getaddress($user_id);		
		if($data !=NULL){
		 $this->success_response($data);
		}else{
		   $res = array();
		   $this->success_response($res);
		}
		
	}
	
	public function add_address_post(){
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$uc_first_name = $this->input->post('uc_first_name');
		$uc_email_id = $this->input->post('uc_email_id');
		$uc_mobile = $this->input->post('uc_mobile');
		$uc_address = $this->input->post('uc_address');
		$uc_pincode = $this->input->post('uc_pincode');
		$uc_city = $this->input->post('uc_city');
		$state = $this->Mcart_model_android->get_city_name($uc_pincode);
		//$uc_state = trim($state['state']);
		$uc_state = $this->Mcart_model_android->get_state_id($uc_pincode);
		$name =  explode(' ',$uc_first_name);
		$count = count($name);
		if(!empty($name) && $count >= 2){
			$first_name = $name[0];
			$last_name = $name[1].' '.@$name[2];
		}else {
			$this->failed_response(1005, "Please enter your full name.");
		}
		if(trim($uc_first_name) == ''){
		$this->failed_response(1005, "Please enter the first name");

		}else if(trim($uc_email_id) == ''){
		$this->failed_response(1006, "Please enter the Email-ID");

		}else if(trim($uc_mobile) == ''){
		$this->failed_response(1007, "Please enter the mobile number.");

		}else if(filter_var($uc_email_id, FILTER_VALIDATE_EMAIL) === false){
		$this->failed_response(1008, "Please enter valid Email-ID.");

		}else if(strlen($uc_mobile) != 10){
		$this->failed_response(1005, "Please enter valid mobile number.");

		}else if(trim($uc_address) == ''){
		$this->failed_response(1006, "Please enter the address.");

		}else if(trim($uc_pincode) == ''){
		$this->failed_response(1007, "Please enter the pincode.");

		}else if(trim($uc_city) == ''){
		$this->failed_response(1006, "Please enter the city.");

		}else if(trim($uc_state) == ''){
		$this->failed_response(1007, "Please enter the state.");

		}else if(strlen($uc_pincode) != 6){
        $this->failed_response(1013, "pincode should contain 6 number.");

		}else if($this->checkpincode($uc_pincode) != 2){
        $this->failed_response(1010, "Pincode does not exist.");
		}
		$data['user_id'] = $userid;
		$data['uc_first_name'] = $first_name;
		$data['uc_last_name'] = $last_name;
		$data['uc_email_id'] = $uc_email_id;
		$data['uc_mobile'] = $uc_mobile;
		$data['uc_address'] = $uc_address;
		$data['uc_pincode'] = $uc_pincode;
		$data['uc_city'] = $uc_city;
		$data['uc_state'] = $uc_state;
		
        $res = (object)$this->Mcart_model_android->add_address($data);
		
		if($res !=NULL){
		 $this->success_response($res);
		}else{
		   $this->success_response($res);
		}
		
	}
	
	public function notify_me_post()
	{
		$user_id = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$product_id = $this->post('product_id');
		$emailid = $this->post('emailid');
		$data['user_id'] = $user_id;
		$data['product_id'] = $product_id;
		$data['emailid'] = $emailid;
		$res = (object)$this->Mcart_model_android->notify_me($data);
		if($res !=NULL){
		//$text = "You will be informed when product will be in stock";
                $text = "We will let you know once this product is back in stock.";
		//$return_array = array($text);
		//$message = $this->success_response(json_encode($return_array));
             //   $message = $this->success_response(json_encode($text));	
		$this->success_response($text);
		}else{
		   $this->failed_response(1001, "No records found");
		}
	}
	
	public function checkpincode($pincode){
      $res = $this->Mcart_model_android->get_city_name($pincode);
	  //echo count($res);exit;
      return count($res);
	}
	
	public function sc_check_otp_post(){
		$user_id = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$uc_mobile_no = trim($this->post('mobile_no'));
		$data['otp_number'] = trim($this->post('otp_number'));
		$data['mobile_no'] = '91'.$uc_mobile_no;
		$res = $this->cart_model->sc_check_otp($data['mobile_no'],$data['otp_number']);
		//echo $res;exit;
		if($res!=0){
			$data = new stdclass();
			$data->message = 'mobile number validated successfully';
			$this->success_response($data);
		}else{
			 $this->failed_response(1001, "wrong OTP please try again");
		}
	}

	public function scgenrate_otp_post(){
		$user_id = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$uc_mobile_no = trim($this->post('mobile_no'));
		$data['mobile_no'] = '91'.$uc_mobile_no;
		$uc_first_name = $this->post('uc_first_name');
		$uc_last_name = $this->post('uc_last_name');
		$email_id = $this->post('uc_email_id');
		$uc_address = $this->post('uc_address');
		$uc_pincode = $this->post('uc_pincode');
		$uc_city = $this->post('uc_city');
		$paymod_cod = '1';
		if(strlen($uc_mobile_no) != 10){
        $this->failed_response(1013, "please enter 10 digit mobile number");
		}
		$otp_no = rand(4,10000);
		$digits = 4;
		$otp_no =rand(pow(10, $digits-1), pow(10, $digits)-1);
		//$otp_no = 99999;
		$res = $this->Cart_model->scgenrate_otp($data['mobile_no'],$otp_no);
		$result = $this->Mcart_model_android->failed_order($uc_first_name,$uc_last_name,$email_id,$uc_address,$user_id,$data['mobile_no'],$paymod_cod,$uc_pincode,$uc_city);
		
		/* Call SMS send API */
		$message = 'Thank+you+for+choosing+STYCKR.+Your+OTP+for+cash+on+delivery+is+'.$otp_no.'.+Please+use+this+to+confirm+your+order.+Stay+Stylish!';
		$ch = curl_init('https://www.txtguru.in/imobile/api.php?');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile='.$data['mobile_no'].'&message='.$message.'');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		$data = curl_exec($ch);
		/* end of code Call SMS send API */
		if($res = 1){
			$data = new stdclass();
			$data->message = 'success';
			$this->success_response($data);
		}else{
			 $this->failed_response(1001, "OTP generated successfully");
		}
	}
	
	public function online_payment_post(){
		
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$address_id = $this->input->post('address_id');
		$user_info = $this->Mcart_model_android->user_info($address_id);
		$paymod = $this->input->post('paymod');
		$user_credentials = $this->input->post('user_credentials');
		$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		$data = array();
		
		$data['user_id'] = $userid;
		$data['uc_first_name'] = $user_info['first_name'];
		$data['uc_last_name'] = $user_info['last_name'];
		$data['uc_email_id'] = $user_info['email'];
		$data['uc_mobile'] = $user_info['mobile_no'];
		$data['uc_address'] = $user_info['shipping_address'];
		$data['uc_pincode'] = $user_info['pincode'];
		$data['uc_city'] = $user_info['city_name'];
		$data['uc_state'] = $user_info['state_name'];
		$data['paymod'] = $paymod;
		//$data['SCUniqueID'] = $SCUniqueID;
		$data['mihpayid'] = '';
		$data['txnid'] = '';

		if($paymod == 'cod'){
		  $res = $this->Mcart_model_android->sc_uc_order2($data);
		  if($res !=NULL && $res != '2'){
		  $this->success_response($res);
		  }
		}else if($paymod == 'online'){
		  $res = (array)$this->Mcart_model_android->sc_uc_order2($data);
		  if($res !=NULL && $res != '2'){
			$amount = $res['order_total'];
			$productinfo = $res['product_details'][0][0]->name;
			$firstname = $res['first_name'];
			$email = $res['first_name'];
			 $data = $this->getHashes($txnid, $amount, $productinfo, $firstname, $email, $user_credentials);
			// print_r($data);exit;
			   if($data !=NULL && $res != '2'){
				  $this->success_response($data);
			   }
		  }
		}
		else {
		$this->failed_response(1201, "Your cart is empty");
		}

		

	}
	
	public function getHashes($txnid, $amount, $productinfo, $firstname, $email, $user_credentials=NULL)
	{
      // $firstname, $email can be "", i.e empty string if needed. Same should be sent to PayU server (in request params) also.
      $key = 'd9WSht';
      $salt = 'It1JXKPL';
  
      $payhash_str = $key . '|' . $txnid . '|' .$amount  . '|' .$productinfo  . '|' . $firstname . '|' . $email . '|' . $salt;
      $paymentHash = strtolower(hash('sha512', $payhash_str));
      $arr['payment_hash'] = $paymentHash;

      $cmnNameMerchantCodes = 'stylecracker pay U';
      $merchantCodesHash_str = $key . '|' . $cmnNameMerchantCodes . '|default|' . $salt ;
      $merchantCodesHash = strtolower(hash('sha512', $merchantCodesHash_str));
      $arr['get_merchant_stylecracker_codes_hash'] = $merchantCodesHash;

      $cmnMobileSdk = 'vas_for_mobile_sdk';
      $mobileSdk_str = $key . '|' . $cmnMobileSdk . '|default|' . $salt;
      $mobileSdk = strtolower(hash('sha512', $mobileSdk_str));
      $arr['vas_for_mobile_sdk_hash'] = $mobileSdk;

      $cmnPaymentRelatedDetailsForMobileSdk1 = 'payment_related_details_for_mobile_sdk';
      $detailsForMobileSdk_str1 = $key  . '|' . $cmnPaymentRelatedDetailsForMobileSdk1 . '|default|' . $salt ;
      $detailsForMobileSdk1 = strtolower(hash('sha512', $detailsForMobileSdk_str1));
      $arr['payment_related_details_for_mobile_sdk_hash'] = $detailsForMobileSdk1;

      if($user_credentials != NULL && $user_credentials != '')
      {
            $cmnNameDeleteCard = 'delete_user_card';
            $deleteHash_str = $key  . '|' . $cmnNameDeleteCard . '|' . $user_credentials . '|' . $salt ;
            $deleteHash = strtolower(hash('sha512', $deleteHash_str));
            $arr['delete_user_card_hash'] = $deleteHash;
            
            $cmnNameGetUserCard = 'get_user_cards';
            $getUserCardHash_str = $key  . '|' . $cmnNameGetUserCard . '|' . $user_credentials . '|' . $salt ;
            $getUserCardHash = strtolower(hash('sha512', $getUserCardHash_str));
            $arr['get_user_cards_hash'] = $getUserCardHash;
            
            $cmnNameEditUserCard = 'edit_user_card';
            $editUserCardHash_str = $key  . '|' . $cmnNameEditUserCard . '|' . $user_credentials . '|' . $salt ;
            $editUserCardHash = strtolower(hash('sha512', $editUserCardHash_str));
            $arr['edit_user_card_hash'] = $editUserCardHash;
            
            $cmnNameSaveUserCard = 'save_user_card';
            $saveUserCardHash_str = $key  . '|' . $cmnNameSaveUserCard . '|' . $user_credentials . '|' . $salt ;
            $saveUserCardHash = strtolower(hash('sha512', $saveUserCardHash_str));
            $arr['save_user_card_hash'] = $saveUserCardHash;
            
            $cmnPaymentRelatedDetailsForMobileSdk = 'payment_related_details_for_mobile_sdk';
            $detailsForMobileSdk_str = $key  . '|' . $cmnPaymentRelatedDetailsForMobileSdk . '|' . $user_credentials . '|' . $salt ;
            $detailsForMobileSdk = strtolower(hash('sha512', $detailsForMobileSdk_str));
            $arr['payment_related_details_for_mobile_sdk_hash'] = $detailsForMobileSdk;
      }
	   //print_r($arr);exit;
		return array('result'=>$arr);
		
	}

	public function scgenrate_hashes_post(){

	 	$amount =  $this->post('amount');
	    $email =  $this->post('email');
	    $firstname =  $this->post('firstname');
	    $phone =  $this->post('phone');
	    $productinfo =  $this->post('productinfo');
	    $txnid =  $this->post('txnid');
		$surl = $this->post('surl');
		$furl = $this->post('furl');
	    //$furl =  "https://payu.herokuapp.com/ios_failure";
	    $key =  'd9WSht';
	    $offer_key =  "test123@6622";
	    //$surl = "http://www.scflash.com/Sccart/successOrder";
	    $udf1 = '';
	    $udf2 = '';
	    $udf3 = '';
	    $udf4 = '';
	   	$udf5 = '';
	    $user_credentials =  'default';
	    $result = $this->mobileHashTestWs($txnid, $amount, $productinfo, $firstname, $email, $user_credentials, $udf1, $udf2, $udf3, $udf4, $udf5);
	    if(!empty($result)){
	    	$this->success_response($result);
	    }else return NULL; 

	}

	//new added for storing user card details
	public function scgenrate_hashes_android_post(){

	 	$amount =  $this->post('amount');
	    $email =  $this->post('email');
	    $firstname =  $this->post('firstname');
	    $phone =  $this->post('phone');
	    $productinfo =  $this->post('productinfo');
	    $txnid =  $this->post('txnid');
		$surl = $this->post('surl');
		$furl = $this->post('furl');
		$user_id = $this->post('user_id');
		$auth_token = $this->post('auth_token');
	    //$furl =  "https://payu.herokuapp.com/ios_failure";
	    $key =  'd9WSht';
	    $offer_key =  "test123@6622";
	    //$surl = "http://www.scflash.com/Sccart/successOrder";
	    $udf1 = '';
	    $udf2 = '';
	    $udf3 = '';
	    $udf4 = '';
	   	$udf5 = '';
	    $user_credentials =  'd9WSht:'.$user_id;
	    $result = $this->mobileHashTestWs($txnid, $amount, $productinfo, $firstname, $email, $user_credentials, $udf1, $udf2, $udf3, $udf4, $udf5);
	    if(!empty($result)){
	    	$this->success_response($result);
	    }else return NULL; 

	}
	
	/*public function scgenrate_hashes_test_post(){

	 	$amount =  $this->post('amount');
	    $email =  $this->post('email');
	    $firstname =  $this->post('firstname');
	    $phone =  $this->post('phone');
	    $productinfo =  $this->post('productinfo');
	    $txnid =  $this->post('txnid');
		$surl = $this->post('surl');
		$furl = $this->post('furl');
		$user_id = $this->post('user_id');
		$auth_token = $this->post('auth_token');
	    //$furl =  "https://payu.herokuapp.com/ios_failure";
	    $key =  'd9WSht';
	    $offer_key =  "test123@6622";
	    //$surl = "http://www.scflash.com/Sccart/successOrder";
	    $udf1 = $this->post('udf1');
	    $udf2 = $this->post('udf2');
	    $udf3 = $this->post('udf3');
	    $udf4 = $this->post('udf4');
	   	$udf5 = $this->post('udf5');
	    $user_credentials =  'd9WSht:'.$user_id;
	    $result = $this->mobileHashTestWs($txnid, $amount, $productinfo, $firstname, $email, $user_credentials, $udf1, $udf2, $udf3, $udf4, $udf5);
	    if(!empty($result)){
	    	$this->success_response($result);
	    }else return NULL; 

	}*/
	
	public function scgenrate_hashes_test_post(){

		$user_id 			= $this->input->post('user_id');
		$auth_token 		= $this->input->post('auth_token');
		$amount 			= $this->input->post('amount');
	    $email 				= $this->input->post('email');
	    $firstname 			= $this->input->post('firstname');
	    $phone 				= $this->input->post('phone');
	    $productinfo 		= $this->input->post('productinfo');
	    $txnid 				= $this->input->post('txnid');
		$surl 				= $this->input->post('surl');
		$furl 				= $this->input->post('furl');
	    //$furl 			= "https://payu.herokuapp.com/ios_failure";
	    $key 				= 'd9WSht';
	    $offer_key 			= "test123@6622";
	    //$surl 			= "http://www.scflash.com/Sccart/successOrder";
	    $udf1 				= $this->input->post('udf1');
	    $udf2 				= $this->input->post('udf2');
	    $udf3 				= $this->input->post('udf3');
	    $udf4 				= $this->input->post('udf4');
	   	$udf5 				= $this->input->post('udf5');
	    $user_credentials 	=  'd9WSht:'.$user_id;
	    $result = $this->mobileHashTestWs($txnid, $amount, $productinfo, $firstname, $email, $user_credentials, $udf1, $udf2, $udf3, $udf4, $udf5);
	    if(!empty($result)){
	    	$this->success_response($result);
	    }else return NULL; 

	}

	public function mobileHashTestWs($txnid, $amount, $productinfo, $firstname, $email, $user_credentials=NULL, $udf1, $udf2, $udf3, $udf4, $udf5)
	{
      // $firstname, $email can be "", i.e empty string if needed. Same should be sent to PayU server (in request params) also.
      $key = 'd9WSht';
      $salt = 'It1JXKPL';
      if($udf1 == NULL) {
            $udf1 = '';
      }
      if($udf2 == NULL) {
            $udf2 = '';
      }
      if($udf3 == NULL) {
            $udf3 = '';
      }
      if($udf4 == NULL) {
            $udf4 = '';
      }
      if($udf5 == NULL) {
            $udf5 = '';
      }
      // $payhash_str = $key . '|' . $txnid . '|' . $amount . '|' . $productinfo . '|' . $firstname . '|' . $email . '|' . $udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '||||||' . $salt;
      // $paymentHash = strtolower(hash('sha512', $payhash_str));
      // $arr['paymentHash'] = $paymentHash;
	  $payhash_str = $key . '|' . $txnid . '|' . $amount . '|' . $productinfo . '|' . $firstname . '|' . $email . '|' . $udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '||||||' . $salt;
      $paymentHash = strtolower(hash('sha512', $payhash_str));
      $arr['paymentHash'] = $paymentHash;
      $cmnNameMerchantCodes = 'get_merchant_ibibo_codes';
      $merchantCodesHash_str = $key . '|' . $cmnNameMerchantCodes . '|default|' . $salt ;
      $merchantCodesHash = strtolower(hash('sha512', $merchantCodesHash_str));
      $arr['merchantCodesHash'] = $merchantCodesHash;

      $cmnMobileSdk = 'vas_for_mobile_sdk';
      $mobileSdk_str = $key . '|' . $cmnMobileSdk . '|default|' . $salt;
      $mobileSdk = strtolower(hash('sha512', $mobileSdk_str));
      $arr['mobileSdk'] = $mobileSdk;

      $cmnPaymentRelatedDetailsForMobileSdk1 = 'payment_related_details_for_mobile_sdk';
      $detailsForMobileSdk_str1 = $key  . '|' . $cmnPaymentRelatedDetailsForMobileSdk1 . '|default|' . $salt ;
      $detailsForMobileSdk1 = strtolower(hash('sha512', $detailsForMobileSdk_str1));
      $arr['detailsForMobileSdk'] = $detailsForMobileSdk1;

      if($user_credentials != NULL && $user_credentials != '')
      {
            $cmnNameDeleteCard = 'delete_user_card';
            $deleteHash_str = $key  . '|' . $cmnNameDeleteCard . '|' . $user_credentials . '|' . $salt ;
            $deleteHash = strtolower(hash('sha512', $deleteHash_str));
            $arr['deleteHash'] = $deleteHash;
            
            $cmnNameGetUserCard = 'get_user_cards';
            $getUserCardHash_str = $key  . '|' . $cmnNameGetUserCard . '|' . $user_credentials . '|' . $salt ;
            $getUserCardHash = strtolower(hash('sha512', $getUserCardHash_str));
            $arr['getUserCardHash'] = $getUserCardHash;
            
            $cmnNameEditUserCard = 'edit_user_card';
            $editUserCardHash_str = $key  . '|' . $cmnNameEditUserCard . '|' . $user_credentials . '|' . $salt ;
            $editUserCardHash = strtolower(hash('sha512', $editUserCardHash_str));
            $arr['editUserCardHash'] = $editUserCardHash;
            
            $cmnNameSaveUserCard = 'save_user_card';
            $saveUserCardHash_str = $key  . '|' . $cmnNameSaveUserCard . '|' . $user_credentials . '|' . $salt ;
            $saveUserCardHash = strtolower(hash('sha512', $saveUserCardHash_str));
            $arr['saveUserCardHash'] = $saveUserCardHash;
            
            $cmnPaymentRelatedDetailsForMobileSdk = 'payment_related_details_for_mobile_sdk';
            $detailsForMobileSdk_str = $key  . '|' . $cmnPaymentRelatedDetailsForMobileSdk . '|' . $user_credentials . '|' . $salt ;
            $detailsForMobileSdk = strtolower(hash('sha512', $detailsForMobileSdk_str));
            $arr['detailsForMobileSdk'] = $detailsForMobileSdk;
      }
    	return array('result'=>$arr);
	}
	/*
	public function transaction_failed_post(){
		$user_id = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$order_id = $this->post('order_id');
		//$status = $this->post('status');
		$res = (string)$this->Mcart_model_android->transaction_failed($user_id,$order_id);
		if($res == 1){
			$response = array();
			$response['text'] = "Your transaction is failed";
			$this->success_response((object)$response);
		}
		else{
			$this->failed_response(1300, "Wrong credentials!");
		}	
		
	}*/
	
	public function transaction_failed_post(){
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$order_id = $this->post('order_id');
		$status = $this->post('status');
		$res = $this->Mcart_model_android->change_status($userid,$order_id,$status);
		print_r($res);exit;
		if($status == 1){
			$res1 = $this->Mcart_model_android->update_order_cart_info_online_order($userid,$order_id);
		// $this->orderPlaceMessage($userid,$order_id,$data['uc_first_name'],$data['uc_last_name']);
		// $this->orderPlaceMessagetoBrand($userid,$order_id,$data['uc_first_name'],$data['uc_last_name']);
		}else{
			$res1 = (string)$this->Mcart_model_android->transaction_failed($userid,$order_id);
		}
		if(!empty($res)){
			$this->success_response($res);
		}
		else{
			$this->failed_response(1300, "Wrong credentials!");
		}
	}
	
	public function successOrder_get(){
		 
	}
	
	public function failureOrder_get(){
		
	}
	
	public function check_version_get(){
		$version = '3.3.8';
		$this->success_response($version);
	}
	
	// added for coupan code
	function applyCoupon_post()
	{  
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$coupon_code = $this->post('coupon_code');
		$brand_ids = $this->post('brand_ids');
		
		$result = $this->Mcart_model_android->applyCoupon($coupon_code,$brand_ids,$userid);
		
		if($result){
			$this->success_response($result);
		}
		else{
			$this->failed_response(1300, "No Coupon Found");
		}

	}
	
	function calculateCouponAmount_old($cartproduct=array(),$totalProductPrice=null)
  {    
      if(!empty($cartproduct))
      {
        $total_product_price = 0; $coupon_product_price = 0;
        foreach($cartproduct as $val)
        {
            if(@$val['discount_percent']!='' || @$val['discount_percent']!=0)
            {
               $price = $val['discount_price'];            
            }else
            {
               $price = $val['product_price'];            
            }
            $total_product_price = $total_product_price+$price;
        }
        foreach($cartproduct as $val)
        {

          if(@$val['discount_percent']!='' || @$val['discount_percent']!=0)
          {
             $price = $val['discount_price'];            
          }else
          {
             $price = $val['product_price'];            
          }

           $coupon_info = $this->Cart_model->getUserCoupon($val['coupon_code'],$val['brand_id']);
            $coupon_brand = $coupon_info['brand_id'];
            $coupon_products = $coupon_info['coupon_products'];
            if($coupon_products!='' || $coupon_products!=0)
            {
              $coupon_products_arr =explode(',',$coupon_products);
            }
            $coupon_min_spend = $coupon_info['coupon_min_spend'];
            $coupon_max_spend = $coupon_info['coupon_max_spend'];
            $coupon_discount_type = $coupon_info['coupon_discount_type']; 
            $coupon_stylecracker =  $coupon_info['stylecrackerDiscount'];
            $individual_use_only =  $coupon_info['individual_use_only']; 

          if(isset($val['coupon_code']) && $val['coupon_code']!='')
          {
           
            /*$data['coupon_discount'] = $coupon_info['coupon_amount'];
            $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];*/
          
            $data['coupon_code'] = $val['coupon_code'];
            $coupon_product_price = 0;

              if($coupon_brand==$val['brand_id'] && $coupon_brand!=0)
              {        
                if($coupon_products!=''&& $coupon_products!=0)
                {
                   //coupon_discount_type =3 (Product discount)
                  if($coupon_discount_type==3)
                  {
                    if(in_array($val['product_id'], $coupon_products_arr))
                    {              
                      if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                      {
                        $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                        $data['coupon_amount'] =  $coupon_info['coupon_amount'];                      
                        $data['coupon_discount'] = $coupon_info['coupon_amount'];  
                        $data['coupon_code'] = $val['coupon_code'];                   
                      } 
                    }
                  } 
                   //coupon_discount_type =4 (Product % discount)
                  if($coupon_discount_type==4)
                  {
                    if(in_array($val['product_id'], $coupon_products_arr))
                    {              
                      if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                      {
                        $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                        $data['coupon_amount'] =  $coupon_info['coupon_amount'];  
                        $coupon_percent = $coupon_info['coupon_amount'];
                        $data['coupon_discount'] = $price*($coupon_percent/100); 
                        $data['coupon_code'] = $val['coupon_code'];                      
                      } 
                    }
                  }                 

                }else
                { 
                   //coupon_discount_type =1 (cart discount)
                  if($coupon_discount_type==1)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_info['coupon_amount']; 
                      $data['coupon_code'] = $val['coupon_code'];                   
                    }
                  }
                   //coupon_discount_type =2 (cart % discount)
                  if($coupon_discount_type==2)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];  
                      $coupon_percent = $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];                      
                    }
                  }
                }          
              }

             /*  if($coupon_brand==0 && strtolower($val['coupon_code'])=='sccart20')
              { 
                   $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $coupon_percent = $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];                      
                    }
              }*/
				
			 if(($coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only!=1  )|| ( $coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only==1 && $this->session->userdata('user_id')>0 ))        
              { 
                $coupon_product_price = $totalProductPrice;

                     //coupon_discount_type =1 (cart discount)
                  if($coupon_discount_type==1)
                  {
                    
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {	
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];  				  
                      $data['coupon_discount'] = $coupon_info['coupon_amount']; 
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                      $data['coupon_code'] = $val['coupon_code'];                   
                    }
                  }
                   //coupon_discount_type =2 (cart % discount)
                  if($coupon_discount_type==2)
                  {                   
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    { 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];                       
                      $coupon_percent = $coupon_info['coupon_amount']; 
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];  
                      $data['coupon_discount'] = $totalProductPrice*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];                      
                    }
                  }
              }
              

          }else
          {
            $data['coupon_discount']=0;
            $data['coupon_code'] = '';
          }         
        }      
      } 
      return $data;      
  }

  function getProductSku($productId,$productSize)
  {
    $product_sku = $this->Cart_model->getProductSku($productId,$productSize);
    return $product_sku;
  }
  
  function get_size_guide_get(){
		$size_guide = $this->get('size_guide');
		$product_id = $this->get('product_id');
		$brand_id = $this->get('brand_id');
		$size_guide_url = $this->Looks_model->get_size_guide_n($size_guide,$product_id,$brand_id);
		$data = array();
		$data['size_guide_url'] = $size_guide_url;
		$this->success_response($data);
  }
  
  /*added by rajesh 15jul2016*/
  function orderPlaceMessage($user_id,$order_id,$first_name,$last_last){   

  		if($this->getEmailAddress($user_id)!=''){
	      $config['protocol'] = 'smtp';
	      $totalProductPrice=0;
	      $referral_point = 0;
		  $referral_point = $this->cartnew_model->get_refre_amt($order_id);

	//      $config['mailpath'] = '/usr/sbin/sendmail';
	      $config['charset'] = 'iso-8859-1';
	      $config['mailtype'] = 'html';
	      $config['wordwrap'] = TRUE;
	      $config['smtp_host'] = $this->config->item('smtp_host');
	      $config['smtp_user'] = $this->config->item('smtp_user');
	      $config['smtp_pass'] = $this->config->item('smtp_pass');
	      $config['smtp_port'] = $this->config->item('smtp_port');
			
	      $this->email->initialize($config);

		  	$data['orderUniqueNo'] 		= $order_id;
		    $data['Username'] 			= $first_name.' '.$last_last;
		  	$data['order_product_info'] = $this->cartnew_model->su_get_order_info($order_id);    
	      	$data['order_info'] 		= $this->cartnew_model->su_get_order_price_info_res($order_id);
	      	$data['shipping_address'] 	= $data['order_info'][0]['shipping_address'];
	      	$data['city_name'] 			= $data['order_info'][0]['city_name'];
	      	$data['state_name'] 		= $data['order_info'][0]['state_name'];
	      	$data['pincode'] 			= $data['order_info'][0]['pincode'];
	      	$data['country_name'] 		= $data['order_info'][0]['country_name'];
	      	$data['paymentmode'] 		= $data['order_info'][0]['pay_mode'];
	      	$data['referral_point'] 	= $referral_point;
			$data['order_display_no'] 	= $data['order_info'][0]['order_display_no'];
			
	      	if($data['order_info'][0]['state_id'] == '36') { $data['state_name'] =  $data['order_info'][0]['it_state']; }

	      	$data['res'] =  $this->cartnew_model->su_get_order_info($order_id); 

	  		//$this->load->view('emails/order_successful_user_copy',$data);

		      $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Processing');
		      if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){		        
		         $this->email->to($this->config->item('sc_test_emaild'));
		         $this->email->cc($this->config->item('sc_testcc_emaild'));  
		      }else{
		     	$this->email->to($this->getEmailAddress($user_id));    
		      	$this->email->cc('styling@stylecracker.com'); 
		      }   
		    
			  $this->email->subject('StyleCracker Personalized Box Request');
		      //$this->load->view('emails/order_successful_user_copy',$data);
		     // $message = @$this->load->view('emails/order_successful_user_copy',@$data,true);
		  	  //$this->email->message($message);
			  
			  $this->email->message('Dear '.$first_name.',<br/><br/>
			  Hope all is well. Congratulations, we have just assigned you your very own personal stylist. We have just received your email regarding your interest in the StyleCracker Box with Order No. <b style="color:#13d792;">'.$data['order_display_no'].'</b>, and can\'t wait to work with you on curating the perfect box to make you look great. <br/><br/>           
			  Please do share a convenient date and time to connect and your stylist will be sure to call you then. <br/><br/>
			  In case of any queries, please feel free to contact us on our customer care number 02240007762 (10am to 7pm, Monday to Saturday) or email us on customercare@stylecracker.com and we will be more than happy to assist you with anything that you might need.<br/><br/>           
			  PS: We\'re here to ensure you love what you get in your box so please feel free to share as many details as possible when we speak.<br/><br/>
			  <br/><br/>Kind Regards<br/>Your Stylist<br/><img src="https://www.stylecracker.com/assets/images/logo.png" ><br/>
			  Facebook: <a href="https://www.facebook.com/StyleCracker/" target="_blank" >https://www.facebook.com/StyleCracker/</a><br/>
			  Instagram: @stylecracker<br/>
			  Twitter: Style_Cracker<br/>
			  SCBOX Web: <a href="https://www.stylecracker.com/sc-box" target="_blank" >https://www.stylecracker.com/sc-box</a><br/>');
			  
			    /* Order Processing sms to the user */
		  	   $message = 'Thanks+for+choosing+StyleCracker!+We\'re+processing+your+order+number+'.$data['order_info'][0]['order_display_no'].'.+Don\'t+worry+we\'ll+keep+you+updated.';
    		   file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=91'.$data['order_info'][0]['mobile_no'].'&message='.$message);
		  	 // $this->email->send();

	  }
	  }

	  function orderPlaceMessage_sc($user_id,$order_id,$first_name,$last_last){
	 

  		if($this->getEmailAddress($user_id)!=''){
	      $config['protocol'] = 'smtp';
	      $totalProductPrice=0;
	      $referral_point = 0;
		  $referral_point = $this->cartnew_model->get_refre_amt($order_id);


			//  $config['mailpath'] = '/usr/sbin/sendmail';
	      $config['charset'] = 'iso-8859-1';
	      $config['mailtype'] = 'html';
	      $config['wordwrap'] = TRUE;
	      $config['smtp_host'] = $this->config->item('smtp_host');
	      $config['smtp_user'] = $this->config->item('smtp_user');
	      $config['smtp_pass'] = $this->config->item('smtp_pass');
	      $config['smtp_port'] = $this->config->item('smtp_port');
			
	      $this->email->initialize($config);

		  	$data['orderUniqueNo'] = $order_id;
		    $data['Username'] = $first_name.' '.$last_last;
		  	$data['order_product_info'] = $this->cartnew_model->su_get_order_info($order_id);    
	      	$data['order_info'] = $this->cartnew_model->su_get_order_price_info_res($order_id);
	      	$data['shipping_address'] = $data['order_info'][0]['shipping_address'];
	      	$data['city_name'] = $data['order_info'][0]['city_name'];
	      	$data['state_name'] = $data['order_info'][0]['state_name'];
	      	$data['pincode'] = $data['order_info'][0]['pincode'];
	      	$data['country_name'] = $data['order_info'][0]['country_name'];
	      	$data['paymentmode'] = $data['order_info'][0]['pay_mode'];
	      	$data['referral_point'] = $referral_point;

	      	if($data['order_info'][0]['state_id'] == '36') { $data['state_name'] =  $data['order_info'][0]['it_state']; }

	      	//$data['res'] =  $this->cartnew_model->su_get_order_info($order_id); 

	  		//$this->load->view('emails/order_successful_user_copy',$data);

		      $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Processing');
		      $this->email->to($this->getEmailAddress($user_id));    
		      //$this->email->cc('order@stylecracker.com');    
		       $this->email->cc('arun@stylecracker.com');   	   
		       $this->email->cc('sudha@stylecracker.com');   
		      $this->email->subject('StyleCracker: Order Processing');
		      //$this->load->view('emails/order_successful_user_copy',$data);
		      $message = $this->load->view('emails/order_successful_user_copy',$data,true);
		  	  $this->email->message($message);
		  	  //**$this->email->send();

	  }
	}

    function orderPlaceMessagetoBrand($user_id,$orders_id,$first_name,$last_last){
	
	    if($this->getEmailAddress($user_id)!=''){

	      $totalProductPrice = 0; $coupon_discount_ = 0;
	      $referral_point = 0;
		  $referral_point = $this->cartnew_model->get_refre_amt($orders_id);
	      $res1 = $this->cartnew_model->su_get_order_info($orders_id);      
		  $data['res1']  = $res1;
	      $order_id = explode('_', $orders_id);
	      unset($order_id[0]);
	      if(!empty($order_id)){
	      foreach($order_id as $order_noval){
	      	  $config['protocol'] = 'smtp';
	     
		//    $config['mailpath'] = '/usr/sbin/sendmail';
		      $config['charset'] = 'iso-8859-1';
		      $config['mailtype'] = 'html';
		      $config['wordwrap'] = TRUE;
		      $config['smtp_host'] = $this->config->item('smtp_host');
		      $config['smtp_user'] = $this->config->item('smtp_user');
		      $config['smtp_pass'] = $this->config->item('smtp_pass');
		      $config['smtp_port'] = $this->config->item('smtp_port');

		      $this->email->initialize($config);
		      $data['orderUniqueNo'] = $order_noval;
		     
		      $data['order_info'] = $this->cartnew_model->br_get_order_price_info($order_noval);
		    /* echo '<pre>';print_r($data['order_info']);*/
		      $data['order_product_info'] = $this->cartnew_model->br_get_order_info($order_noval);
		      //echo '<pre>';print_r($data['order_product_info']);
		      $data['Username'] = $first_name.' '.$last_last;
		      $data['shipping_address'] = $data['order_info'][0]['shipping_address'];
		      $data['city_name'] = $data['order_info'][0]['city_name'];
		      $data['state_name'] = $data['order_info'][0]['state_name'];
		      $data['pincode'] = $data['order_info'][0]['pincode'];
		      $data['country_name'] = $data['order_info'][0]['country_name'];
		      $data['paymentmode'] = $data['order_info'][0]['pay_mode'];
		      $data['email'] = $data['order_info'][0]['email_id'];
		      $data['mobile_no'] = $data['order_info'][0]['mobile_no'];
		      $data['company_name'] = $data['order_product_info'][0]['company_name'];
		      $data['referral_point'] = $referral_point;

		      $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Processing');      
		      /*$this->email->to($this->getEmailAddress($data['order_info'][0]['brand_id']));
		      $this->email->cc('order@stylecracker.com'); */
		      $this->email->to('arun@stylecracker.com');
		      /*$this->email->to('sudha@stylecracker.com');		      
		      $this->email->cc('rajesh@stylecracker.com');*/
		      $this->email->subject('StyleCracker: Order Processing');
		      //$this->load->view('emails/order_successful_brand_copy',$data);
		      $message = @$this->load->view('emails/order_successful_brand_copy',@$data,true);
		  	  $this->email->message($message);
		  	  //**$this->email->send();
		     
			}	
			}
		}
	}
  
  
   function calculateCouponAmount($cartproduct=array(),$totalProductPrice=null)
  	{    
      if(!empty($cartproduct))
      {
        $total_product_price = 0; $coupon_product_price = 0;
        foreach($cartproduct as $val)
        {
            if(@$val['discount_percent']!='' || @$val['discount_percent']!=0)
            {
               $price = $val['discount_price'];            
            }else
            {
               $price = $val['product_price'];            
            }
            $total_product_price = $total_product_price+$price;
        }
        foreach($cartproduct as $val)
        {

          if(@$val['discount_percent']!='' || @$val['discount_percent']!=0)
          {
             $price = $val['discount_price'];            
          }else
          {
             $price = $val['product_price'];            
          }

           $coupon_info = $this->cartnew_model->getUserCoupon($val['coupon_code'],$val['brand_id']);
            $coupon_brand = $coupon_info['brand_id'];
            $coupon_products = $coupon_info['coupon_products'];
            if($coupon_products!='' || $coupon_products!=0)
            {
              $coupon_products_arr =explode(',',$coupon_products);
            }
            $coupon_min_spend = $coupon_info['coupon_min_spend'];
            $coupon_max_spend = $coupon_info['coupon_max_spend'];
            $coupon_discount_type = $coupon_info['coupon_discount_type']; 
            $coupon_stylecracker =  $coupon_info['stylecrackerDiscount'];
            $individual_use_only =  $coupon_info['individual_use_only']; 

          if(isset($val['coupon_code']) && $val['coupon_code']!='')
          {
           
            /*$data['coupon_discount'] = $coupon_info['coupon_amount'];
            $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];*/
          
            $data['coupon_code'] = $val['coupon_code'];            

              if($coupon_brand==$val['brand_id'] && $coupon_brand!=0)
              {        
                if($coupon_products!=''&& $coupon_products!=0)
                {
                   //coupon_discount_type =3 (Product discount)
                  if($coupon_discount_type==3)
                  {
                    if(in_array($val['product_id'], $coupon_products_arr))
                    {              
                      if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                      {
                        $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                        $data['coupon_amount'] =  $coupon_info['coupon_amount'];                      
                        $data['coupon_discount'] = $coupon_info['coupon_amount'];  
                        $data['coupon_code'] = $val['coupon_code'];                   
                      } 
                    }
                  } 
                   //coupon_discount_type =4 (Product % discount)
                  if($coupon_discount_type==4)
                  {
                    if(in_array($val['product_id'], $coupon_products_arr))
                    {              
                      if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                      {
                        $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                        $data['coupon_amount'] =  $coupon_info['coupon_amount'];  
                        $coupon_percent = $coupon_info['coupon_amount'];
                        $data['coupon_discount'] = $price*($coupon_percent/100); 
                        $data['coupon_code'] = $val['coupon_code'];                      
                      } 
                    }
                  }                 

                }else
                { 
                   //coupon_discount_type =1 (cart discount)
                  if($coupon_discount_type==1)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_info['coupon_amount']; 
                      $data['coupon_code'] = $val['coupon_code'];                   
                    }
                  }
                   //coupon_discount_type =2 (cart % discount)
                  if($coupon_discount_type==2)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];  
                      $coupon_percent = $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code']=="" ? '123': $val['coupon_code'];                      
                    }
                  }
                }          
              }

             /*  if($coupon_brand==0 && strtolower($val['coupon_code'])=='sccart20')
              { 
                   $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $coupon_percent = $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];                      
                    }
              }*/
        
       if(($coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only!=1  )|| ( $coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only==1 ))        
              { 
                $coupon_product_price = $totalProductPrice;

                     //coupon_discount_type =1 (cart discount)
                  if($coupon_discount_type==1)
                  {
                    
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    { 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];            
                      $data['coupon_discount'] = $coupon_info['coupon_amount']; 
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                      $data['coupon_code'] = $val['coupon_code'];                   
                    }
                  }
                   //coupon_discount_type =2 (cart % discount)
                  if($coupon_discount_type==2)
                  {                   
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    { 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];                       
                      $coupon_percent = $coupon_info['coupon_amount']; 
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];  
                      $data['coupon_discount'] = $totalProductPrice*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];                      
                    }
                  }
              }
              

          }else
          {
          	if(!isset($data['coupon_code']))
          	{
	            $data['coupon_discount']=0;
	            $data['coupon_code'] = '';
	        }
          }         
        }      
      } 
      //echo '<pre>';print_r($data);
      return $data;      
  }
  
	function test_post(){
		$user_id = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$cart_data  = $this->Mcart_model_android->get_cart2($user_id);
		if(!empty($cart_data)){
			$this->success_response($cart_data);
		}else{
			$this->success_response($cart_data);
		}
	}
	
	function delete_address_post(){
		$user_id = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$address_id = $this->post('address_id');
		
		$data = 'success';
		$res = $this->Mcart_model_android->delete_address($user_id,$address_id);
		$this->success_response($data);
	}
	
	public function update_address_post(){
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$uc_first_name = $this->input->post('uc_first_name');
		$uc_email_id = $this->input->post('uc_email_id');
		$uc_mobile = $this->input->post('uc_mobile');
		$uc_address = $this->input->post('uc_address');
		$uc_pincode = $this->input->post('uc_pincode');
		$uc_city = $this->input->post('uc_city');
		$address_id = $this->input->post('address_id');
		$uc_state = $this->Mcart_model_android->get_state_id($uc_pincode);
		$name =  explode(' ',$uc_first_name);
		$count = count($name);
		if(!empty($name) && $count >= 2){
			$first_name = $name[0];
			$last_name = $name[1].' '.@$name[2];
		}else {
			
			$this->failed_response(1010, "Please Enter full name.");			
		}
		
		$data['user_id'] = $userid;
		$data['uc_first_name'] = $first_name;
		$data['uc_last_name'] = $last_name;
		$data['uc_email_id'] = $uc_email_id;
		$data['uc_mobile'] = $uc_mobile;
		$data['uc_address'] = $uc_address;
		$data['uc_pincode'] = $uc_pincode;
		$data['uc_city'] = $uc_city;
		$data['uc_state'] = $uc_state;
		$data['address_id'] = $address_id;
		// print_r($data);exit;
        $res = (object)$this->Mcart_model_android->update_useraddress($data);
		if(!empty($res)){
			$this->success_response($res);
		}else{
			$this->success_response($res);
		}
	}
	
	public function clear_cart_post(){
		$userid = $this->post('user_id');
		$auth_token = $this->post('auth_token');
		$cart_ids = $this->post('cart_ids');
		$paymod = $this->post('paymod');
		$coupon_code = $this->post('coupon_code');
		$sc_credit = $this->post('sc_credit');
		if($paymod == ''){
			$paymod	= 'online';
		}
		$pincode = $this->post('pincode');
		if($this->checkpincode($pincode) != 2 && $pincode != ''){
			$this->failed_response(1010, "Pincode does not exist");
		}
		if($cart_ids == ''){
			$this->failed_response(1010, "Please add cart ids");
		}
		$clear_cart = $this->Mcart_model_android->clear_cart($userid,$cart_ids);
		$data = (object)$this->Mcart_model_android->get_cart($userid,$paymod,$pincode,$chkShipping='',$chkCod='',$city='',$state='',$coupon_code,$sc_credit);
		if($data !=NULL && !empty($data)){
			$this->success_response($data);
		}else{
			$res = new stdclass();
			$res->item = '0';
			$this->success_response($res);
		}
	}
	
	function order_return_or_exchange_post()
	{
		
		$save_data 			= array();
		$userid 			= $this->input->post('user_id');
		$auth_token 		= $this->input->post('auth_token');
	    $product_id 		= $this->input->post('product_id');
	    $order_id 			= $this->input->post('order_id');
	    $response_type 		= $this->input->post('response_type');
	    $user_response 		= $this->input->post('user_response');
        $order_unique_no 	= $this->input->post('order_unique_no');
        $user_recomment 	= $this->input->post('user_recomment');

       if($product_id==''){ $this->failed_response(1010, "Please specify product to exchange or return");  }
	   else if($order_id==''){ $this->failed_response(1010, "Please enter Order Id"); }
	   else if($response_type==''){ $this->failed_response(1010, "Please specify type of response for exchange or return");}
	   else if($user_response=='') { $this->failed_response(1010, "Please select return exchange response"); }
	   else if($order_unique_no==''){ $this->failed_response(1010, "Please enter order unique number"); }
        
	    if($product_id!='' && $order_id!='' && $response_type!='' && $user_response!='' && $order_unique_no!='')
	    {
		    $objectId = $this->Mcart_model_android->get_object('box_order_products')[0]['object_id'];
		    $metaValue = $this->Mcart_model_android->getSereializeProductDetail($order_id,'','',$objectId);
			$ObjectMetaValue  								= unserialize($metaValue['object_meta_value']);
			$ObjectMetaValue[$product_id]['status'] 		= $response_type;	
			$ObjectMetaValue[$product_id]['user_response'] 	= $user_response;
			$ObjectMetaValue[$product_id]['user_recomment'] = $user_recomment;
		
			$save_data['object_meta_value'] = serialize($ObjectMetaValue);			
			$res = $this->Mcart_model_android->updateObjectMeta($save_data,$objectId,$order_id);
			if($res)
			{
				$email_response = $this->orderReturnExchangeEmail($order_id,$order_unique_no,$product_id);
				$response = new stdclass();
				$response->msg = 'Your Return/Exchange request has been initiated. We will contact you soon.';
				$this->success_response($response);
				exit;
			}
		}
	}

	function orderReturnExchangeEmail($order_display_no,$order_unique_no,$product_id){   
	/*function orderReturnExchangeEmail(){   	
		$order_display_no = 'SC1508133359';
		$order_unique_no = '24958_1905';
		$product_id = '90638';*/
		$emailid = 'sudha@stylecracker.com';
		//$emailid = 'sudha@stylecracker.com';
		$objectId = $this->Mcart_model_android->get_object('box_order_products')[0]['object_id'];
		$metaValue = $this->Mcart_model_android->getSereializeProductDetail($order_display_no,'','',$objectId);
		$ObjectMetaValue  = unserialize($metaValue['object_meta_value']);
		$scbox['product_data']['size'] = $ObjectMetaValue[$product_id]['size'];
		$scbox['product_data']['user_status'] = $ObjectMetaValue[$product_id]['status'];
		$scbox['product_data']['is_delete'] = $ObjectMetaValue[$product_id]['is_delete'];
		$scbox['product_data']['user_response'] = $ObjectMetaValue[$product_id]['user_response'];
		$scbox['product_data']['sku'] = $ObjectMetaValue[$product_id]['sku'];
		$scbox['product_data']['details'] = $this->Mcart_model_android->getMetaValueData($product_id);
		
		$message = '';
  		if($emailid!=''){
	      $config['protocol'] = 'smtp';
	      $totalProductPrice=0;
	      $referral_point = 0;
		  $referral_point = $this->cartnew_model->get_refre_amt($order_unique_no);	  
	
	      $config['charset'] = 'iso-8859-1';
	      $config['mailtype'] = 'html';
	      $config['wordwrap'] = TRUE;
	      $config['smtp_host'] = $this->config->item('smtp_host');
	      $config['smtp_user'] = $this->config->item('smtp_user');
	      $config['smtp_pass'] = $this->config->item('smtp_pass');
	      $config['smtp_port'] = $this->config->item('smtp_port');
			
	      $this->email->initialize($config);

		  	$data['orderUniqueNo'] 		= $order_unique_no;
		  	$data['order_info'] 		= $this->cartnew_model->su_get_order_price_info_res($order_unique_no);
		    $data['Username'] 			= @$data['order_info'][0]['first_name'].' '.@$data['order_info'][0]['last_name'];
		    $data['first_name'] 		= @$data['order_info'][0]['first_name'];
		    $data['last_last'] 			= @$data['order_info'][0]['last_name'];
		  	$data['order_product_info'] = $this->cartnew_model->su_get_order_info($order_unique_no); 
	      	$data['email_id'] 			= $data['order_info'][0]['email_id'];
	      	$data['shipping_address'] 	= $data['order_info'][0]['shipping_address'];
	      	$data['city_name'] 			= $data['order_info'][0]['city_name'];
	      	$data['state_name'] 		= $data['order_info'][0]['state_name'];
	      	$data['pincode'] 			= $data['order_info'][0]['pincode'];
	      	$data['country_name'] 		= $data['order_info'][0]['country_name'];
	      	$data['paymentmode'] 		= $data['order_info'][0]['pay_mode'];
	      	$data['referral_point'] 	= $referral_point;
	      	$data['order_display_no'] 	= $data['order_info'][0]['order_display_no'];
	      	$data['mobile_no'] 			= $data['order_info'][0]['mobile_no'];
	      	$data['order_date'] 		= date('d-m-Y',strtotime($data['order_product_info'][0]['created_datetime']));
	      	$data['order_deliverydate'] = date('d-m-Y',strtotime($data['order_product_info'][0]['modified_datetime']));
	      	$data['scbox'] 				= $scbox;

	      	if($data['order_info'][0]['state_id'] == '36') { $data['state_name'] =  $data['order_info'][0]['it_state']; }

	      	  $data['res'] =  $this->cartnew_model->su_get_order_info($order_unique_no); 
		      $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Return/Exchange Request');
		      $this->email->to($data['email_id']);  
            
            if(trim($scbox['product_data']['user_status'])=='Return')
		      {
		      	$this->email->cc(array('refund@stylecracker.com'));      
		      }else if(trim($scbox['product_data']['user_status'])=='Exchange')
		      {
		      	$this->email->cc(array('exchange@stylecracker.com'));      
		      }
 
		      //$this->email->to('sudha@stylecracker.com');       
		      $this->email->subject('Order Return/Exchange Request');
		      //echo '<pre>';print_r($data);
		      $this->load->view('emails/order_returnexchange_email',$data);   

		      $message = $this->load->view('emails/order_returnexchange_email',$data,true);
		     
		  	  $this->email->message($message);

		  	  $sentmsg = $this->email->send();
		  	  if($sentmsg)
		  	  {
		  	  	return 'sent';
		  	  }else
		  	  {
		  	  	return 'notsent';
		  	  }

	  }
	  }
	
}
	
?>