<?php
if (!defined('BASEPATH'))
exit('No direct script access allowed'); 
 
class Instagram extends CI_Controller {
 
public function __construct() {
	parent::__construct();
	$this->load->model('user_info_model');
	$this->config->load('instagram');
}
 
public function index() {
  
  //$this->load->view('seventeen/microsites/scbox/header_view');exit;
 	// echo '<a  class="instagram social-button" target="_blank" href="https://api.instagram.com/oauth/authorize/?client_id='.$this->config->item('instagram_client_id').'&redirect_uri='.$this->config->item('instagram_callback_url').'&response_type=code&scope=basic+public_content+follower_list+comments+relationships+likes"> <i class="fa fa-instagram"></i> Log in with Instagram</a>';  
  echo '<a href="https://api.instagram.com/oauth/authorize/?client_id='.$this->config->item('instagram_client_id').'&redirect_uri='.$this->config->item('instagram_callback_url').'&response_type=code&scope=basic+public_content+follower_list+comments+relationships+likes" target="_blank"> Login to instagram</a>';
  exit;	
}
 
public function logout() {
	$signed_request_cookie = 'insta_' . $this->config->item('appID');
	setcookie($signed_request_cookie, '', time() - 3600, "/");
	$this->session->sess_destroy();  //session destroy
	redirect('/instagram/index');  //redirect to the home page
}
 
public function instalogin() {

	$instagram = array(
		'client_name' => $this->config->item('instagram_client_name'),
		'client_id' => $this->config->item('instagram_client_id'),
		'client_secret' => $this->config->item('instagram_client_secret'),
		'callback_url' => $this->config->item('instagram_callback_url'),
	);
}
 
 /*function instagramLogin() {
    
    //return 'https://api.instagram.com/oauth/authorize/?client_id=' . $this->codeigniter_instance->config->item('instagram_client_id') . '&redirect_uri=' . $this->codeigniter_instance->config->item('instagram_callback_url') . '&response_type=code&scope=basic+public_content+follower_list+comments+relationships+likes';

	$data = [
		'client_id' => $this->config->item('instagram_client_id'), 
		'client_secret' => $this->config->item('instagram_client_secret'), 
		'grant_type' => 'authorization_code', 
		'redirect_uri' => $this->config->item('instagram_callback_url'), 
		'code' => $_GET['code']
	];

	$uri = 'https://api.instagram.com/oauth/authorize/?client_id=' . $data['client_id'] . '&redirect_uri=' . $data['redirect_uri'] . '&response_type=code&scope=basic+public_content+follower_list+comments+relationships+likes'; 
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $uri); // uri
	curl_setopt($ch, CURLOPT_POST, true); // POST
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // POST DATA
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // RETURN RESULT true
	curl_setopt($ch, CURLOPT_HEADER, 0); // RETURN HEADER false
	curl_setopt($ch, CURLOPT_NOBODY, 0); // NO RETURN BODY false / we need the body to return
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); // VERIFY SSL HOST false
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // VERIFY SSL PEER false
	$result = json_decode(curl_exec($ch)); // execute curl
	echo '<pre>'; // preformatted view	
	// ecit directly the result
	//exit(print_r($result)); 
	print_r($result);    
 }*/

 function getAccessToken()
{
	$uri = 'https://api.instagram.com/oauth/access_token'; 
	$data = [
		'client_id' => $this->config->item('instagram_client_id'), 
		'client_secret' => $this->config->item('instagram_client_secret'), 
		'grant_type' => 'authorization_code', 
		'redirect_uri' => $this->config->item('instagram_callback_url'), 
		'code' => @$_GET['code']
	];
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $uri); // uri
	curl_setopt($ch, CURLOPT_POST, true); // POST
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // POST DATA
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // RETURN RESULT true
	curl_setopt($ch, CURLOPT_HEADER, 0); // RETURN HEADER false
	curl_setopt($ch, CURLOPT_NOBODY, 0); // NO RETURN BODY false / we need the body to return
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); // VERIFY SSL HOST false
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // VERIFY SSL PEER false
	$result = curl_exec($ch);
	$result_arr = json_decode($result); // execute curl
	//echo '<pre> getAccessToken--'; // preformatted view	
	$userid = $this->session->userdata('user_id');
	if(isset($result_arr->access_token) && @$result_arr->access_token!='')
	{
		$this->user_info_model->saveInstaData($userid,$result);
	}
	
	// ecit directly the result
	//exit(print_r($result)); 
	//echo $result;
	//print_r($result_arr);
	redirect('https://www.instagram.com');
}

function fetchMediaData($access_token,$geturl=null){
	//$access_token = 'access_token:1166367185.d624631.4cf0299518db423b8888cc4983c56ea8';
	//$access_token = '1166367185.d624631.4cf0299518db423b8888cc4983c56ea8';
	$data = [
		'client_id' => $this->config->item('instagram_client_id'), 
		'client_secret' => $this->config->item('instagram_client_secret'), 
		'grant_type' => 'authorization_code', 
		'redirect_uri' => $this->config->item('instagram_callback_url'), 
		'code' => @$_GET['code']
	];

	$url= ($geturl!='') ? $geturl : "https://api.instagram.com/v1/users/self/media/recent/?access_token=".$access_token;

 	$ch = curl_init($url);
    curl_setopt($ch, CURLOPT_USERPWD, $access_token);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');	    
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
   // curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $result = curl_exec($ch);
	curl_close($ch);
	
	return $result;
}

function showInstaData($userid)
{
	$result = $this->user_info_model->getInstaData($userid);	
	$result_arr = json_decode($result);
	//echo '<pre>';print_r($result_arr);exit;
	 $access_token = @$result_arr->access_token;
	 $limit = 8; // Amount of images to show 
	 $i = 0; 
	$media_result = $this->fetchMediaData($access_token);
	$media_result_arr = json_decode($media_result);
	//echo '<pre>';print_r($media_result);
	if(!empty($media_result_arr))
	{
		foreach (@$media_result_arr->data as $post):
			if ($i < $limit ):
				echo '<a href="'.$post->images->standard_resolution->url.'"><img src="'.$post->images->standard_resolution->url.'" width="500" height="500"></a>';
				echo '<br/>';
				 $i ++; 
			endif; 
		 endforeach; 
	}
	//echo $userid;exit;
}

public function test() {
	$this->load->view('test');
}
 

}
 
/* End of file instagram.php */
/* Location: ./application/controllers/instagram.php */