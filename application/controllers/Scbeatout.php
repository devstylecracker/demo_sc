<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Scbeatout extends MY_Controller {

	function __construct(){

       parent::__construct();
       $this->load->model('Scbeatout_model');
       $this->load->library('user_agent');
   	}

	public function index()
	{
	
	}

  public function add_to_cart(){
    if (!$this->input->is_ajax_request()) {
        exit('No direct script access allowed');
    }else{
       $uniquecookie = $this->input->post('uniquecookie');
       $productid = $this->input->post('productid');
       $user_id = $this->session->userdata('user_id')!='' ? $this->session->userdata('user_id') : 0;

       $res = $this->Scbeatout_model->add_to_cart($uniquecookie,$productid,$user_id); 
       echo json_encode($res[0]);
     
    }
  }
  
  public function removed_from_cart(){
  if (!$this->input->is_ajax_request()) {
        exit('No direct script access allowed');
    }else{
	$uniquecookie = $this->input->post('uniquecookie');
	$cartid = base64_decode($this->input->post('cartid'));
	
	$res = $this->Scbeatout_model->removed_from_cart($uniquecookie,$cartid); 
    echo json_encode($res[0]);
	}
  }
	
}
?>