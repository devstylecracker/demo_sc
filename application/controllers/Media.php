<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
       parent::__construct();
      // $this->load->model('Brands_model');
   	}

	public function index()
	{

  //  $data = array();
  //  $data['brands_list'] = $this->Brands_model->get_all_brands();


    $this->seo_title = 'Media - StyleCracker.com';
    $this->seo_desc = $this->lang->line('seo_desc');
    $this->seo_keyword = $this->lang->line('seo_keyword');

		$this->load->view('common/header_view');
		$this->load->view('media');
		$this->load->view('common/footer_view');
	}
/*
  public function search_brand(){

   $intCount = '-1';
    $search = $this->input->post('search');
    $type = $this->input->post('type');
    $brands_list = $this->Brands_model->search_brand(strtolower($search),$type);

    $brand_count = count($brands_list);
    if($brand_count > 0){
       $intCount = $brand_count / 3 ;
    }
    if($intCount < 1){ $intCount = 1; }
    $i = 1;

    $brandhtml = '';


    if(!empty($brands_list)){ $k = 1; $new_name = ''; $old_name = '';
    foreach($brands_list as $val){
    if(round($intCount) >= $k){

    if($k == 1){
        $brandhtml = $brandhtml.'<div class="col-md-4">';
     }
      $new_name = strtoupper(substr($val['first_name'], 0,1));
      if($new_name != $old_name){
      if($i != 1){
        $brandhtml=$brandhtml.'</ul>';
      }
       $brandhtml = $brandhtml.' <ul class="brand-listing"> ';
        $brandhtml = $brandhtml.'<li id="'.strtoupper($new_name).'" class="alpha">'.strtoupper($new_name);
        $brandhtml = $brandhtml.'<script type="text/Javascript">$('.strtoupper($new_name).').attr("href","#'.strtoupper($new_name).'");</script>';
        $brandhtml =$brandhtml.'</li>';
        $brandhtml = $brandhtml.'<li><a href="'.base_url().'brands/brand_details/'.$val['user_name'].'">'.$val['first_name'].''.$val['last_name'].'</a></li>';
        $old_name = $new_name;
       }else{
        $brandhtml =$brandhtml.'<li><a href="'.base_url().'brands/brand_details/'.$val['user_name'].'">'.$val['first_name'].''.$val['last_name'].'</a></li>';
       }
    }else{
      $brandhtml =$brandhtml.' </div>';
      $k = 0;
     }
 $k++; $i++; }
  }

      echo $brandhtml;
  }


  function brand_details(){
    $data = array();
    $brand_slug = $this->uri->segment(3);
    if($brand_slug){
    $data = $this->Brands_model->get_brands_info($brand_slug);

    $this->load->view('common/header_view');
    $this->load->view('brands_details',$data);
    $this->load->view('common/footer_view');
  }else{
    redirect('/schome');
  }
  }

  function redirect(){
    $product_id = $this->uri->segment(3);
    if($product_id){
      $data['product'] = $this->Brands_model->get_product_info($product_id);

      //$this->load->view('common/header_view');
      $this->load->view('redirect',$data);
     // $this->load->view('common/footer_view');
    }
  }*/
}
