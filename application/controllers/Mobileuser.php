<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';

class Mobileuser extends TT_REST_Controller 
{
  function __construct()
	{
		parent::__construct();
		$this->load->model('Schome_model');
		$this->load->model('Mobile_model');
		$this->load->model('Pa_model');
		$this->load->model('Mobile_pa_model');
		$this->load->library('email');
		$this->open_methods = array('register_user_post','login_post','contact_us_post');    
		//$this->open_methods = array('getuserinfo_get');
		 //$this->open_methods = array('pa');
		//$this->load->library('../controllers/Schome');
		//$this->open_methods = array('login_post');
	}
    
  public function register_user_post(){
    // if ($this->input->is_ajax_request()) {

      $scusername = $this->post('sc_username_name');
      $scemail_id = $this->post('sc_email_id');
      $scpassword = $this->post('sc_password');
       // $firstname = $this->post('firstname');
       // $lastname = $this->post('lastname');
	  $medium = $this->post('medium');
      if($medium == '') $medium = 'mobile';
      $medium_id =  $this->post('medium_id');
	  $gender =  $this->post('gender');
	   if($gender == 'male' || $gender == 'men'){ 
		$gender = 2; 
	  }else if($gender == 'female' || $gender == 'women'){ 
		$gender = 1;
	  }else $gender = 1;
	  $skip = 0;
		/*
		$scusername=""; 
        $scusername.=$firstname; 
		$scusername.=$lastname; 
      
      $scusername.= date('Y/m/dH:i:s');
		*/
	  if($medium == 'mobile_facebook' || $medium == 'mobile_ios_facebook' || $medium == 'mobile_android_facebook') $skip  = 1;
      if(trim($scusername) == ''){
        $this->failed_response(1005, "Please enter the username");

      }else if(trim($scemail_id) == ''){
        $this->failed_response(1006, "Please enter the Email-id");

      }else if(trim($scpassword) == ''){
        $this->failed_response(1007, "Please enter the Password");

      }else if(filter_var($scemail_id, FILTER_VALIDATE_EMAIL) === false){
        $this->failed_response(1008, "Please enter valid Email-id");

      }
      /*else if($this->usernameUnique($scusername) > 0 && $skip == 0){
        $this->failed_response(1009, "Username already exist");

      }*/
      else if($this->emailUnique($scemail_id) > 0 && $skip == 0){
        $this->failed_response(1010, "Email ID already exists.");
      }
	  else if(strlen($scpassword) < 8 && $skip == 0){
        $this->failed_response(1013, "Password should have atleast 8 character");
	  }
	  else if(strlen($scpassword) > 20 && $skip == 0){
        $this->failed_response(1014, "Password should not greater than 20 character");
	  }
	  else{
      
    	$name = str_replace(' ', '', $scusername);
    	$random = md5(uniqid(rand(0,2), true));
    	$random = substr($random, 0, 5);
		$firstname_split = explode(' ',$scusername);
		$firstname = $firstname_split[0];
    	$scusername = $name.$random;
    	
      	$ip = $this->input->ip_address();
        $res = $this->Mobile_model->registerUserMobile($firstname,$medium,$medium_id,$scusername,$scemail_id,$scpassword,$gender,$ip,2);
        if(($res != NULL)) { 
          $data = new stdClass();
          $data->user_info= $this->getuserinfo_get($res[0]['user_id']);//return value of get_users_info_mobile function

          $data->pa= $this->pa_get($res[0]['user_id']);
          $this->success_response($data);
        }
        else{ 
          $this->failed_response(1011, "Registration failed!");
          // return '0'; 
        }
    //mobile_google, mobile, mobile_facebook
      }


    // }else{
      // return 'Somethings went wrong';
    // }

  }
	
	public function user_details_post()
	{
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		//$data = new stdClass();
		
		$data = new stdClass();
    	$data->user_info = $this->getuserinfo_get($user_id);
		$data->pa = $this->pa_get($user_id);
	   if($data != NULL){
		$this->success_response($data);
	   }
	   else
			$this->failed_response(1001, "No Record(s) Found");
		
	}
	
	public function wishlist_ids_post()
	{
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		$data = $this->Mobile_model->get_look_id($user_id,$auth_token);
		if($data != NULL){
			//$data['user_id'] = $user_id;
			//$data['token'] = $auth_token;
			$this->success_response($data);
		}else
			$this->failed_response(1001, "No Record(s) Found");
			
	}
	
	public function wishlist_looks_post()
	{
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');		
		$limit= $this->post('limit');
        $offset = $this->post('offset');
		$limit = $this->checkLimit($limit);
		$offset = $this->checkOffset($offset);
		$data = $this->Mobile_model->get_wishlist_looks($user_id,$limit,$offset);
		
		if($data != NULL){
			//$data->user_id = $user_id;
			//$data->token = $auth_token;
			$this->success_response($data);
		}
		else
			$data = array();	
			$this->success_response($data);
			//$this->failed_response(1001, "No Record(s) Found");
			
	}
	
	public function update_mobile_pa_post()
	{
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');		
		$json_string = $this->post('json_string');
		$gender = '1';
		//extract the json string and send success response+
		$data_answers = array('user_body_shape'=>1,'user_style'=>2,'user_age'=>8,'user_budget'=>7,'user_size'=>9);
		$jsondecode = json_decode($json_string, true);
		foreach($jsondecode as $key=>$json){
			$question = $json['question'];
			$question_id = $data_answers[$question];
			$anser_id = $json['anser_id'];
			$this->Mobile_pa_model->update_mobile_pa($question_id,$anser_id,$user_id,$gender);
		}
		$data = new stdClass();
		$data->user_info = $this->getuserinfo_get($user_id);
		$data->pa = $this->pa_get($user_id);
		//$data->user_id = $user_id;
		//$data->auth_token = $auth_token;
		$this->success_response($data);
	}
	
	
	public function getuserinfo_get($user_id)
  {
    if($user_id){
      $user_info = (object)$this->Mobile_model->get_users_info_mobile($user_id);
      return $user_info;
    }else{
      $this->failed_response(1001, "No Record(s) Found");
    }
  }
    
  public function pa_get($user_id)
  {
    
    if($user_id){

        $data = new stdClass();
         
        $data -> body_shape = $this->Mobile_pa_model->get_question_img_answers_mobile(1,$user_id);
        $data -> style = $this->Mobile_pa_model->get_question_img_answers_mobile(2,$user_id);
        $data -> age = $this->Mobile_pa_model->get_question_img_answers_mobile(8,$user_id);
        $data -> budget = $this->Mobile_pa_model->get_question_img_answers_mobile(7,$user_id);
        $data -> size = $this->Mobile_pa_model->get_question_img_answers_mobile(9,$user_id);
        $data -> user_body_shape = $this->Mobile_pa_model->get_users_answer_mobile(1,$user_id);
        $data -> user_style = $this->Mobile_pa_model->get_users_answer_mobile(2,$user_id);
        $data -> user_age = $this->Mobile_pa_model->get_users_answer_mobile(8,$user_id);
        $data -> user_budget = $this->Mobile_pa_model->get_users_answer_mobile(7,$user_id);
        $data -> user_size = $this->Mobile_pa_model->get_users_answer_mobile(9,$user_id);
        $data -> question_resume_id = $this->Mobile_pa_model->get_question_resume_id_mobile($user_id);
        $data -> edited_data ='';
        return $data;



     }
  else
   {
       $this->failed_response(1001, "No Record(s) Found");
     }

  }
	
  public function usernameUnique($username){
      $res = $this->Schome_model->usernameUnique($username);
      return count($res);
  }

  public function emailUnique($email){
      $res = $this->Schome_model->emailUnique($email);
      return count($res);
  }

  public function signup($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$json_array = array()){
      $res = $this->Schome_model->registerUser($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$json_array);
      if($res){ return $res; }else{ return '0'; }
  }

  
   public function login_post(){

        $logEmailid = $this->post('sc_email_id');
        $logPassword = $this->post('sc_password');
		
         if(trim($logEmailid) == ''){
            echo "Please enter your email id";

          }else if(trim($logPassword) == ''){
            echo "Please enter the Password";

          }else{
           $result = $this->Mobile_model->login_user_mobile($logEmailid,$logPassword);

            if($result != NULL && $result != 2){
					
      				if(isset($result[0]['question_comp_mobile'])) $question_comp = $result[0]['question_comp_mobile']; else $question_comp = '';

      				//$question_resume_id = $this->session->userdata('question_resume_id');
      				$data = new stdClass();
      				if($result[0]['bucket_mobile'] == '' || $question_comp!=1 ){
      					$data->redirect_url = 'pa';
      				}else{
      					$data->redirect_url = 'profile';
      				}
					
      				$data->user_info = $this->getuserinfo_get($result[0]['user_id']);//return value of get_users_info_mobile function
      				$data->pa = $this->pa_get($result[0]['user_id']);
      				$this->success_response($data);
            }else{
                $this->failed_response(1012, "Authorization failed!");

            }

          }
  }
	
  /*public function forget_user_email_post(){
    $sc_email_id = $this->post('sc_email_id');
    $sc_user_id = $this->post('user_id');
    $auth_token = $this->post('auth_token');
    $res = $this->Mobile_model->forget_user_email_mobile($sc_email_id,$sc_user_id);     
    if($res === 1){
      $data = new stdClass();
      $data->user_id = $sc_user_id;
      $data->auth_token = $auth_token;
      $this->success_response($data);
    }else{
       $this->failed_response(1012, "Authorization failed!");
    }

  }*/

  public function change_password_post(){
    $sc_user_id 	= $this->post('user_id');
    $auth_token 	= $this->post('auth_token');
    $oldPassword 	= $this->input->post('oldPassword');
    $newPassword 	= $this->input->post('newPassword');
	if(trim($newPassword) == ''){
        $this->failed_response(1007, "Please enter the Password");

    }
	else if(strlen($newPassword) < 8 ){
        $this->failed_response(1013, "Password should have atleast 8 character");
	}else if(strlen($newPassword) > 20){
        $this->failed_response(1013, "Password should not greater than 20 character");
	}
    $res = $this->Mobile_model->change_password($sc_user_id,$oldPassword,$newPassword); 
	
    if($res === 1){

      $data = new stdClass();
      $data->message = 'success';
      $this->success_response($data);
    }else{
      $this->failed_response(1300, "Wrong credentials!");
    }

  }
  
  public function forget_user_email_get(){
	$sc_email_id = $this->get('sc_email_id');
	$res = $this->Mobile_model->forget_user_email_mobile($sc_email_id);
	if($res != false){
		if($this->send_forgotpwd_email_mobile($sc_email_id,$res)) 
		{
			   $data = new stdClass();
		       $data->message = 'New Password has been Send to your email-id '.$sc_email_id;
			   $this->success_response($data);
		}
		else{
		  $this->failed_response(1017, "Invalid Email-id ");
		}	
	}else{
		$this->failed_response(1012, "Authorization failed!");
	}
	
  }
  
   public function contact_us_post(){

	$data = array();
	$contact_name = $this->post('contact_name');
	$email_id = $this->post('email_id');
	$subject = $this->post('subject');
	$message = $this->post('message');
	
	if($contact_name!='' && $email_id!='' && $subject!='' && $message!=''){
		$data = array('name' => $contact_name,'email'=>$email_id,'message' => $subject.' '.$message,'enquiry_date_time'=>$this->config->item('sc_date'));
		$res = $this->Mobile_model->add_contact_us($data);
		
		if($res == 1)
		{
		
		// $query->message = 'success';
			$data = new stdClass();
			$data->message = 'success';
			$this->success_response($data);
		
		}
		else
		{
			$this->failed_response(1012, "Authorization failed!");
		}
	}
  }

	function checkLimit($limit){
		if($limit == '') $query_limit = ''; else $query_limit = " LIMIT $limit ";
		return $query_limit;
	}
	
	function checkOffset($offset){
		if($offset == '') $query_offset = ''; else $query_offset = " OFFSET $offset ";
		return $query_offset;
	}  
	
	public function send_forgotpwd_email_mobile($email_id,$new_password){

      $config['protocol'] = 'smtp';
//      $config['mailpath'] = '/usr/sbin/sendmail';
 //     $config['charset'] = 'iso-8859-1';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');

      $this->email->initialize($config);

      $this->email->from($this->config->item('from_email'), 'Stylecracker');
       if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){        
	      $this->email->to($this->config->item('sc_test_emaild'));
	    }else{
      	  $this->email->to($email_id);
      	}
      $this->email->subject('Welcome to Stylecracker');
      $this->email->message("Your login details are <br> Email : ".$email_id."<br><br>Your new password is:".$new_password."<br><br>You can change your password at any time by logging into your account. <br><br>Warm Regards,<br>Style Cracker");

      if($this->email->send())
      {
        return "Email Sent";
      }else
      {
        return "Email Error";
      }
      
  }
  
  public function upload_profile_post(){

		$status = "";
    	$msg = "";
		$user_id 			= $this->input->post('user_id');
        $auth_token 		= $this->input->post('auth_token');
    	$file_element_name 	= 'profile_img';
		
		/* $post = file_get_contents('php://input');
		print_r($_POST);
		print_r($_FILES); exit; */
		if ($status != "error")
    	{
			$config['upload_path'] = $this->config->item('profile_image_path');
			//$config['upload_path']= 'D:\xampp\htdocs\stylecracker\assets\profileimages';
	        $config['allowed_types'] = 'JPG|jpg|png|jpge|PNG|GIF|gif';
	        $config['max_size'] = 1024 * 8;
	        $config['file_name'] = time();
			//echo $config['upload_path']; exit; 

	        $this->load->library('upload', $config);
	        if (!$this->upload->do_upload($file_element_name))
	        {
	            $status = 'error';
	            $msg = $this->upload->display_errors('', '');
				//echo $msg."image is not uploading";
				$this->success_response($msg);
	        }
	        else
	        {
	            $data = $this->upload->data();
	            $result = $this->Mobile_model->update_profile_img(($data['file_name']),$user_id);
	            if($this->Mobile_model->update_profile_img(($data['file_name']),$user_id))
	            {
	               //echo $this->db->last_query();
					$status = "success";
	                $msg = "File successfully uploaded";
	            }
	            else
	            {
	                echo "error";
					$status = "error";
	                $msg = "Something went wrong when saving the file, please try again.";
	            }

	        }
    	}
			$data = new stdClass();
			$data = $this->Mobile_model->get_profile_img($user_id);
			$data = (object)($data[0]);
			//$data->pa = $this->pa_get($result[0]['user_id']);
			
			$this->success_response($data);
		
	}
	
	
	public function cover_image_post(){
		
		$status = "";
    	$msg = "";
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
    	$file_element_name = 'cover_img';

		if ($status != "error")
    	{
	        $config['upload_path'] = $this->config->item('cover_image_path');
			//$config['upload_path']= 'D:\xampp\htdocs\stylecracker\assets\coverimages';
	        $config['allowed_types'] = 'JPG|jpg|png|jpge|PNG';
	       // $config['max_size'] = 1024 * 8;
	        $config['file_name'] = time();


	        $this->load->library('upload', $config);

	        if (!$this->upload->do_upload($file_element_name))
	        {
	            $status = 'error';
	            $msg = $this->upload->display_errors('', '');
	             $response = array(
				"status" => 'error',
				"message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
				);
	        }
	        else
	        {
	            $data = $this->upload->data();
	            $width = $data['image_width'];
	            $height = $data['image_height'];
	            $fileext = $data['file_ext'];
	            //$file_id = $this->Mobile_model->update_profile_img(($data['file_name']),$user_id);
	            if($this->Mobile_model->update_cover_img(($data['file_name']),$user_id))
	            {
	                 $response = array(
					"status" => 'success',
					"url" => $this->config->item('cover_image_path').$config['file_name'].$fileext,
					"width" => $width,
					"height" => $height
				    );
	            }
	            else
	            {
	                 $response = array(
					"status" => 'error',
					"message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
					);
	            }

	        }
    	}
			
			$data = new stdClass();
			$data = $this->Mobile_model->get_cover_img($user_id);
			$data = (object)($data[0]);
			//$data->pa = $this->pa_get($result[0]['user_id']);
			
			$this->success_response($data);

		//echo json_encode($response);
	}

	
	public function upload_wordrobe_post(){
		$status = "";
    	$msg = "";
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		$file_element_name = 'upload_photoes[]';
		$product_name = $this->post('product_name');
		// code for tag
		//$tags = $this->post('tag');
		$tags = urldecode($this->post('tag'));
		$this->load->library('image_lib');		
		if ($status != "error")
    	{
	        $files = $_FILES;
			
			if(empty($files)){
					$this->failed_response(1201, "Please Upload Photo and try again!");
			}
			else{
    			$ext = pathinfo($files['upload_photoes']['name'], PATHINFO_EXTENSION);
		       		if($ext == 'JPG' || $ext == 'jpg' || $ext == 'png' || $ext == 'jpge' || $ext == 'PNG' ){
						$img_name = time().mt_rand(0,10000).'.'.$ext;
						$pathAndName = $this->config->item('wordrobe_path').$img_name;
						$moveResult = move_uploaded_file($files['upload_photoes']['tmp_name'], $pathAndName);
						//$this->createThumbs( $pathAndName, $this->config->item('wordrobe_thumb_path'), 100 ,$img_name);
						$config['image_library'] = 'gd2';
						$config['source_image'] = $pathAndName;
						$config['new_image'] =  $this->config->item('wordrobe_thumb_path');
						$config['create_thumb'] = TRUE;
						$config['thumb_marker'] = '';
						$config['width'] = 180;
						$config['height'] = 180;

						//$this->load->library('image_lib', $config);
						$this->image_lib->initialize($config);

						if ( ! $this->image_lib->resize())
						{
    						echo $this->image_lib->display_errors();
						}
						$data = new stdClass();
						$data = (object)$this->Mobile_model->upload_wordrobe_img($img_name,$user_id,$tags,$product_name);
					}
				}
    	}
			$this->success_response($data);

	}
	
	public function get_wordrobe_post(){
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		$data = $this->Mobile_model->get_wardrobe_img($user_id);
		$this->success_response($data);
	}
	
	public function delete_wordrobe_post(){
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		$wordrobe_id = $this->post('wordrobe_id');
		$res = $this->Mobile_model->delete_wardrobe_img($user_id,$wordrobe_id);
		//echo $res;exit;
		if($res == '1'){
		$data = new stdClass();
		$data->message = 'success';
		$this->success_response($data);
		}
		else{
		$this->failed_response(1300, "Wrong credentials!");
   		}
	}
	
	public function tags_get(){
		$data = $this->Mobile_model->get_all_tags();
		
		if($data != NULL){
			  
			$this->success_response($data);
		}
		else	
		
			$this->failed_response(1001, "No Record(s) Found");	
	}
	
	 
	
}

?>