<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// require APPPATH.'libraries/IntercomClient.php';
class Static_pages extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct(){
	 		 parent::__construct();
	 	}


// all static pages

public function page_about_us(){
	$this->load->view('seventeen/common/header_view');
	$this->load->view('seventeen/static/about_us');
	$this->load->view('seventeen/common/footer_view');
}

public function page_contact_us(){
	$this->load->view('seventeen/common/header_view');
	$this->load->view('seventeen/static/contact_us');
	$this->load->view('seventeen/common/footer_view');
}

public function page_faq(){
		$this->load->view('seventeen/common/header_view');
		$this->load->view('seventeen/static/faq');
		$this->load->view('seventeen/common/footer_view');
}

public function page_help_feedback(){
		$this->load->view('seventeen/common/header_view');
		$this->load->view('seventeen/static/help_feedback');
		$this->load->view('seventeen/common/footer_view');
}

public function page_privacy_policy(){
		$this->load->view('seventeen/common/header_view');
		$this->load->view('seventeen/static/privacy_policy');
		$this->load->view('seventeen/common/footer_view');
}

 public function page_return_policy(){
		$this->load->view('seventeen/common/header_view');
		$this->load->view('seventeen/static/return_policy');
		$this->load->view('seventeen/common/footer_view');
}

public function page_return_policy_scbox(){
	 $this->load->view('seventeen/common/header_view');
	 $this->load->view('seventeen/static/return_policy_scbox');
	 $this->load->view('seventeen/common/footer_view');
}

public function page_terms_of_use(){
	$this->load->view('seventeen/common/header_view');
	$this->load->view('seventeen/static/terms_of_use');
	$this->load->view('seventeen/common/footer_view');
}

public function page_brands(){
	$this->load->view('seventeen/common/header_view');
	$this->load->view('seventeen/brands');
	$this->load->view('seventeen/common/footer_view');
}

public function page_scbox(){
	$this->load->view('seventeen/common/header_view');
	$this->load->view('seventeen/home_abc');
	$this->load->view('seventeen/common/footer_view');
}

//


}
