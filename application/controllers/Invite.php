<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Invite extends MY_Controller {

	function __construct(){
		parent::__construct();
		// $this->load->model('Invite_model');
		$this->load->model('referal_model');
   	}

   	function index(){
   		if($this->session->userdata('user_id')){
   			/*$email = $this->session->userdata('email');
   			$unique_code = '';

	   		if($email!='' && $unique_code==''){
	   			$unique_code = substr($email,0,3).$this->generateRandomString(5);
	   		}else{
	   			$unique_code = $this->generateRandomString(8);
	   		}

	   		$data['unique_code'] = $unique_code;*/
			$data['user_data'] = $this->referal_model->getuser_data($this->session->userdata('user_id'));
	   		$this->load->view('seventeen/common/header_view');
	    	$this->load->view('sccredit',$data);
	    	$this->load->view('seventeen/common/footer_view');
    	}else{
    		redirect('./');
    	}
   	}
	
	function sccredit(){
		   		if($this->session->userdata('user_id')){
   			
			$data['user_data'] = $this->referal_model->getuser_data($this->session->userdata('user_id'));
	   		$this->load->view('seventeen/common/header_view');
	    	$this->load->view('sccredit',$data);
	    	$this->load->view('seventeen/common/footer_view');
    	}else{
    		redirect('./');
    	}
	}
	/*
   	function assign_referer_code(){
   		$this->Invite_model->update_old_users();
   	}*/
	
	public function get_params(){
		
		$conversion = $this->input->post('conversion');
		$referrer_name = $this->input->post('referrer_name');
		$referrer_email = $this->input->post('referrer_email');
		$referrer_email_org = $this->input->post('referrer_email_org');
		$referee_name = $this->input->post('referee_name');
		$referee_email = $this->input->post('referee_email');
		$referee_email_org = $this->input->post('referee_email_org');
		$orderID = $this->input->post('orderID');
		$purchaseValue = $this->input->post('purchaseValue');
		$referrer_converts = $this->input->post('referrer_converts');
		$referrer_customValue = $this->input->post('referrer_customValue');
		$goal_complete = $this->input->post('goal_complete');
		
		if($referrer_email_org != ''){ $referrer_email = $referrer_email_org; }
		if($referee_email_org != ''){ $referee_email = $referee_email_org; }
		
		$data = (object)$this->referal_model->update_referrals($conversion,$referrer_name,$referrer_email,$referee_name,$referee_email,$orderID,$purchaseValue,$referrer_converts,$referrer_customValue,$goal_complete);
		
		$res = $this->referal_model->getuser_id($referee_email);
		
			// if($res[0]['confirm_email'] == '1' && $orderID > 0){
				
					// $bid = '11257'; 
					// $secretKey = '72BB9BBBC9FDCA3165355D2F0985A983'; 
					// $campaignID = '10607';
					// $orderID = $orderID; //set order id
					// $status = '1'; //status of the order
					// $event = 'register';
					// $http_val = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://';
					// $result = file_get_contents($http_val.'www.ref-r.com/campaign/t1/confirmConversion?secretKey='.$secretKey.'&bid='.$bid.'&campaignID='.$campaignID.'&orderID='.$orderID.'&status='.$status.'&event='.$event);
			
			
			// }
			
	}
	
	public function verify_email(){
		
	  $email_id = $this->input->post('email');
	  $config['protocol'] = 'smtp';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');
	
	//added for confirm email
	  $email_en = base64_encode($email_id);
	  $user_id_en = base64_encode($this->session->userdata('user_id'));
	  $link = base_url()."schome/confirm_email/".$email_en."";
	 
      $this->email->initialize($config);
      $this->email->initialize($config);

      $this->email->from($this->config->item('from_email'), 'Stylecracker');
       if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){              
                $this->email->to($this->config->item('sc_test_emaild')); 
                $this->email->cc($this->config->item('sc_testcc_emaild'));      
        }else{
		      $this->email->to($email_id);
		}
      $this->email->subject('Verify to earn Referral Amount!');
      $this->email->message("Hi,<br></br> You have successfully signed up with StyleCracker. To redeem your Referral Amount, all you need to do is quickly verify your account by clicking on the link below. You’re just one step away from earning Rs.500. <br><br> ".$link." <br><br>Warm Regards,<br>StyleCracker");

      if($this->email->send())
      {
        echo "sent";
      }else
      {
        echo "error";
      }
	}
	
	public function remind_email(){
		$user_email = $this->input->post('user_email');
		$friend_email = $this->input->post('friend_email');
		$config['protocol'] = 'smtp';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');
	  
	  $email_en = base64_encode($friend_email);
	  $link = base_url()."schome/confirm_email/".$email_en."";
	  	  
      $this->email->initialize($config);
      $this->email->initialize($config);

      $this->email->from($this->config->item('from_email'), 'Stylecracker');
       if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){              
            $this->email->to($this->config->item('sc_test_emaild')); 
            $this->email->cc($this->config->item('sc_testcc_emaild'));      
        }else{
	      	$this->email->to($friend_email);
		}
      $this->email->subject('Reminder to verify your Email id');
      $this->email->message("Hi,<br></br> Your friend has sent this reminder link for you both to redeem your referral amount! All you need to do is verify your e-mail id by clicking on the link below. <br><br> ".$link." <br><br>Warm Regards,<br>StyleCracker");

      if($this->email->send())
      {
        echo "sent";
      }else
      {
        echo "error";
      }
	}

}
?>