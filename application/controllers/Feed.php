<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Feed extends MY_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see http://codeigniter.com/user_guide/general/urls.html

	 */



	function __construct(){

       parent::__construct();

       $this->load->model('Schome_model'); 

       $this->load->model('Pa_model'); 

      // $this->load->library('upload');

       $this->load->library('bitly');

   	} 

   

	public function index()

	{

		if($this->session->userdata('user_id')){



			$user_info = $this->Schome_model->get_users_info();

			

			if($user_info[0]->bucket <= 0 || $user_info[0]->question_comp!=1){

				redirect('schome/pa');

			}

			$gender = $this->Pa_model->get_gender($this->session->userdata('user_id'));
			if($gender == 1){
    			$data['user_body_shape'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(1)[0]['answer_id']);

	       	 	$data['user_style'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(2)[0]['answer_id']);

	        	$data['user_age'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(8)[0]['answer_id']);

	        	$data['user_budget'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(7)[0]['answer_id']);

	        	$data['user_size'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(9)[0]['answer_id']);
		    }
		    else if($gender == 2){
		    	$data['user_body_shape'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(13)[0]['answer_id']);

	       	 	$data['user_style'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(14)[0]['answer_id']);

	        	$data['user_age'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(17)[0]['answer_id']);

	        	$data['user_budget'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(18)[0]['answer_id']);

	        	/*
	        	$data['user_size'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(17)[0]['answer_id']);
	        	$data['user_size'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(18)[0]['answer_id']);

	        	$data['user_size'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(19)[0]['answer_id']);
	        	$data['user_size'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(20)[0]['answer_id']);
	        	$data['user_size'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(21)[0]['answer_id']);
	        	$data['user_size'] = $this->Pa_model->answer_caption($this->Pa_model->get_users_answer(22)[0]['answer_id']);*/
		    }

        	$data['user_info'] = $user_info;

        	



			$this->load->view('common/header_view');

			$this->load->view('feed',$data);

			$this->load->view('common/footer_view');
			$this->load->view('feed_js');
			

		}else{

			redirect('/');

		}

	}



	public function upload_profile(){

		

		$status = "";

    	$msg = "";

    	$file_element_name = 'profile_img';



		if ($status != "error")

    	{

	        $config['upload_path'] = $this->config->item('profile_image_path');

	        $config['allowed_types'] = 'JPG|jpg|png|jpge|PNG';

	        $config['max_size'] = 1024 * 8;

	        $config['file_name'] = time();

	        

	 

	        $this->load->library('upload', $config);

	 

	        if (!$this->upload->do_upload($file_element_name))

	        {

	            $status = 'error';

	            $msg = $this->upload->display_errors('', '');

	        }

	        else

	        {

	            $data = $this->upload->data();

	            //$file_id = $this->Pa_model->update_profile_img($data['file_name']);

	            if($this->Pa_model->update_profile_img($data['file_name']))

	            {

	                $status = "success";

	                $msg = "File successfully uploaded";

	            }

	            else

	            {

	                $status = "error";

	                $msg = "Something went wrong when saving the file, please try again.";

	            }



	        }

    	}

		

		echo json_encode(array('status' => $status, 'msg' => $msg));

	}



	public function cover_image(){

		$status = "";

    	$msg = "";

    	$file_element_name = 'cover_img';



		if ($status != "error")

    	{

	        $config['upload_path'] = $this->config->item('cover_image_path');

	        $config['allowed_types'] = 'JPG|jpg|png|jpge|PNG';

	       // $config['max_size'] = 1024 * 8;

	        $config['file_name'] = time();

	        

	 

	        $this->load->library('upload', $config);

	 

	        if (!$this->upload->do_upload($file_element_name))

	        {

	            $status = 'error';

	            $msg = $this->upload->display_errors('', '');

	        }

	        else

	        {

	            $data = $this->upload->data();

	            //$file_id = $this->Pa_model->update_profile_img($data['file_name']);

	            if($this->Pa_model->update_cover_img($data['file_name']))

	            {

	                $status = "success";

	                $msg = "File successfully uploaded";

	            }

	            else

	            {

	                $status = "error";

	                $msg = "Something went wrong when saving the file, please try again.";

	            }



	        }

    	}

		

		echo json_encode(array('status' => $status, 'msg' => $msg));

	}

	

	public function upload_wordrobe(){

		$status = "";

    	$msg = "";

    	$file_element_name = 'upload_photoes[]';



		if ($status != "error")

    	{

	        $files = $_FILES;

    		$cpt = count($_FILES['upload_photoes']['name']);

    		$k  = 0;

		    for($i=0; $i<$cpt; $i++)

		    {

		       

		       		$ext = pathinfo($files['upload_photoes']['name'][$i], PATHINFO_EXTENSION);

		       		if($ext == 'JPG' || $ext == 'jpg' || $ext == 'png' || $ext == 'jpge' || $ext == 'PNG' ){

						$img_name = time().mt_rand(0,10000).'.'.$ext;

						$pathAndName = $this->config->item('wordrobe_path').$img_name;

						$moveResult = move_uploaded_file($files['upload_photoes']['tmp_name'][$i], $pathAndName);

						/*

						$this->load->library('image_lib');

						$config['image_library'] = 'gd2';

						$config['source_image'] = $this->config->item('wordrobe_thumb_path');

						$config['create_thumb'] = TRUE;

						$config['maintain_ratio'] = TRUE;

						$config['width'] = 100;

						$config['height'] = 100;



						$this->load->library('image_lib', $config);



						if($this->image_lib->resize()){

							echo $this->image_lib->display_errors(); exit;

						}*/



						$this->Pa_model->upload_wordrobe_img($img_name);  

						$k++;

					}

		    }

		    



		    $status = "success";

	        $msg = $k." File successfully uploaded";

    	}

		

		echo json_encode(array('status' => $status, 'msg' => $msg));

	}



	public function set_upload_options(){



		$config = array();



			$config['upload_path'] = $this->config->item('wordrobe_path');

	        $config['allowed_types'] = 'JPG|jpg|png|jpge|PNG';

	        //$config['max_size'] = 1024 * 8;

	        $config['file_name'] = time().mt_rand(0,100);

	        return $config;

	}



	public function display_wardrobe(){

		if($this->session->userdata('user_id')){

			$this->Pa_model->display_wardrobe();  

		}

	}



	public function delete_wardrobe_img(){

		if($this->session->userdata('user_id')){

			$image_id = $this->input->post('id');

			$this->Pa_model->delete_wardrobe_img($image_id);  

			return true;

		}

	}



	public function add_to_wishlist(){

		if($this->session->userdata('user_id')){

			$look_id = $this->input->post('look_id');

			$this->Schome_model->add_to_wishlist($look_id);  

			return true;

		}

	}



	public function remove_from_wishlist(){

		if($this->session->userdata('user_id')){

			$look_id = $this->input->post('look_id');

			$this->Schome_model->remove_from_wishlist($look_id);  

			return true;

		}	

	}



	public function generate_bitly(){

		if($this->session->userdata('user_id')){

			$loc = $this->input->post('loc');

			echo $this->bitly->shorten($loc);  

		}

	}



	public function createThumbnail($filename){

	    

	    $path_to_image_directory =$this->config->item('wordrobe_thumb_path');

	    if(preg_match('/[.](jpg)$/', $filename)) {

	        $im = imagecreatefromjpeg($path_to_image_directory . $filename);

	    } else if (preg_match('/[.](gif)$/', $filename)) {

	        $im = imagecreatefromgif($path_to_image_directory . $filename);

	    } else if (preg_match('/[.](png)$/', $filename)) {

	        $im = imagecreatefrompng($path_to_image_directory . $filename);

	    }

	     

	    $ox = imagesx($im);

	    $oy = imagesy($im);

	     

	    $nx = $final_width_of_image;

	    $ny = floor($oy * ($final_width_of_image / $ox));

	     

	    $nm = imagecreatetruecolor($nx, $ny);

	     

	    imagecopyresized($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);

	     

	    if(!file_exists($path_to_thumbs_directory)) {

	      if(!mkdir($path_to_thumbs_directory)) {

	           die("There was a problem. Please try again!");

	      } 

	       }

	 

	    imagejpeg($nm, $path_to_thumbs_directory . $filename);

    

	}

	

}

