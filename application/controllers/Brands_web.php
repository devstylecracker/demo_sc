<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brands_web extends MY_Controller {
	
	function __construct(){
       parent::__construct();
       $this->load->model('Brands_model');
       $this->load->model('Home_model');
       $this->load->model('Product_desc_model');
	   $this->load->model('Brand_model');
	   $this->load->model('Mobile_look_model');
       $this->load->driver('cache');
   	}

	public function index(){
		
		$data = array();
		$brandlist = array();
		$alphabets = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		
		if( $this->cache->file->get('brand_list_page') == FALSE ) {
		  $data['brands_list'] = $this->Brands_model->get_all_brands();
		  $data['brands_banners'] = $this->Brands_model->get_brands_banners();
		  $data['brand_categories']['name'] = $this->Brands_model->get_brands_categories_list();
		  $data['brand_collections']['name'] = $this->Brands_model->get_brands_collections_list();

		
		  // echo '<pre>';print_r($data['brands_list']);exit;
		  foreach ($data['brands_list'] as $key => $value) {
			$brandintial = strtoupper(substr(trim($value->company_name),0,1));
			if(in_array($brandintial,$alphabets))
			{
			  $brandlist[$brandintial][$value->company_name]['company_name'] = $value->company_name;
			  $brandlist[$brandintial][$value->company_name]['user_id'] = $value->user_id;
			  $brandlist[$brandintial][$value->company_name]['user_name'] = $value->user_name;
			  $brandlist[$brandintial][$value->company_name]['store_id'] = $value->store_id;
			}else
			{
			  $brandlist['#'][$value->company_name]['company_name'] = $value->company_name;
			  $brandlist['#'][$value->company_name]['user_id'] = $value->user_id;
			  $brandlist['#'][$value->company_name]['user_name'] = $value->user_name;
			  $brandlist['#'][$value->company_name]['store_id'] = $value->store_id;
			}
		  }
		  //echo '<pre>';print_r($brandlist);exit;
		  $data['brands_dictionary'] = $brandlist;
		  /*foreach($data['brand_collections']['name'] as $row){
			$data['brand_collections']['brands'][$row['category_slug']] = $this->Brands_model->get_brands_categories_wize($row['category_slug']);
		  }*/
			$this->cache->file->save('brand_list_page', $data, 500000000000000000);
		  }else{
			$data = $this->cache->file->get('brand_list_page');
		  }
			$data['brands_banners'] = $this->Brands_model->get_brands_banners();

		  $this->seo_title = "India's Top Fashion Brands - Buy Online Now on StyleCracker";
		  $this->seo_desc = $this->lang->line('seo_desc');
		  $this->seo_keyword = $this->lang->line('seo_keyword');

		  $this->load->view('seventeen/common/header_view');
		  $this->load->view('seventeen/brands',$data);
		  $this->load->view('seventeen/common/footer_view');
	
	}

	function brand_details(){
		$data = array();
		$brand_slug = strip_tags($this->uri->segment(2));
		$user_id = $this->session->userdata('user_id');
		$sort_by = '';
		$limit = '20';
		$offset = '0';
		if($brand_slug != ''){
			if(strrpos($brand_slug,'-')) $brand_slug = substr($brand_slug,strrpos($brand_slug,'-',-1)+1);
			$brand_id = $brand_slug;
			$data['user_id'] = $user_id;
			$data['brand_id'] = $brand_id;
			$data['brand_slug'] = $brand_slug;
			$data['brand_info'] = $this->Brand_model->get_brand_info($brand_id);
			$data['products'] = $this->Brand_model->get_brand_products($brand_id,$user_id,$sort_by,$limit,$offset);
			$data['total_products'] = count($this->Brand_model->get_brand_products($brand_id,$user_id,$sort_by,$limit='',$offset));
			$this->seo_title = ' '.@$data['brand_info']['company_name'].' - Buy Online Now on StyleCracker';
			$this->seo_desc = '';
			$this->load->view('seventeen/common/header_view');
			$this->load->view('seventeen/brand_details',$data);
			$this->load->view('seventeen/common/footer_view');
		}else{
			echo "something went wrong please try again";
		}
	}
	
	function get_brand_products(){
		$data = array();
		$brand_slug = strip_tags($this->uri->segment(2));
		$user_id = $this->session->userdata('user_id');
		$sort_by = '';
		$limit = '20';
		$offset = '0';
		if($brand_slug != ''){
			$brand_data = explode('-',$brand_slug);
			$brand_id = $brand_data['1'];
			$data['user_id'] = $user_id;
			$data['brand_id'] = $brand_id;
			$data['products'] = $this->Brand_model->get_brand_products($brand_id,$user_id,$sort_by,$limit,$offset);
			$data['total_products'] = count($this->Brand_model->get_brand_products($brand_id,$user_id,$sort_by,$limit='',$offset));
			// echo "<pre>";print_r($data);exit;
			$this->load->view('seventeen/common/header_view');
			$this->load->view('seventeen/brand_products',$data);
			$this->load->view('seventeen/common/footer_view');
		}else{
			echo "something went wrong please try again";
		}
	}
	
	function more_brand_products(){
		
		$data = array();
		$user_id = $this->session->userdata('user_id');
		$brand_id = $this->input->post('brand_id');
		$sort_by = '';
		$offset = $this->input->post('offset')*20;
		$limit = '20';
		
		$data['user_id'] = $user_id;
		$data['products'] = $this->Brand_model->get_brand_products($brand_id,$user_id,$sort_by,$limit,$offset);
		
		$this->load->view('seventeen/more_product',$data);
		
	}
	
	function view_all_brand(){
		$user_data['user_id'] = $this->session->userdata('user_id');
		$user_data['bucket_id'] = $this->session->userdata('bucket');
		$user_data['limit_cond'] = " LIMIT 0,20";
		$data['top_brands'] = $this->Brand_model->top_brands_depends_on_pa($user_data);
		$user_data['limit_cond'] = " ";
		$data['total_brands'] = count($this->Brand_model->top_brands_depends_on_pa($user_data));
		if($_GET['test'] == 1){
			echo "<pre>";$this->session->userdata('bucket');exit;
		}
		$this->load->view('seventeen/common/header_view');
		$this->load->view('seventeen/allbrand_view',$data);
		$this->load->view('seventeen/common/footer_view');
	}
	
	function get_more_brands(){
		$user_data['bucket_id'] = $this->session->userdata('bucket');
		$user_data['offset'] = $this->input->post('offset')*20;
		$user_data['limit_cond'] = " LIMIT ".$user_data['offset'].",20 ";
		$top_brands = $this->Brand_model->top_brands_depends_on_pa($user_data);
		
		$brand_html = '';
			if(!empty($top_brands)){ foreach($top_brands as $val){
				
				$brand_html = $brand_html.'<div class="col-md-6 col-sm-12 col-xs-12 grid-col">
							  <div class="item-wrp overlay111">
								<div class="item">
								  <div class="item-img-wrp" style="border:1px solid #f5f5f5;">
									<a href="'.BRAND_URL.''.$val['brand_slug'].'-'.$val['brand_id'].'"><img src="'.@$val['home_page_promotion_img'].'" alt=""></a>
									<!-- <span class="ic-share">
									  <i class="icon-send"></i>
								   </span>-->
								  </div>
								</div>
							  </div>
							</div>';
				}//foreach
			}//if	
		echo $brand_html;
		
	}
	
 
}
