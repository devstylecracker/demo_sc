<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Looks extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
       parent::__construct();
       $this->load->model('Looks_model');
       $this->load->model('Product_desc_model');
   	}

	public function index()
	{


	}

  public function look_details(){
    $data = array();
    $look_slug = $this->uri->segment('3');
    $data['look'] = $this->Looks_model->get_look_info($look_slug);   
    $data['brands_review'] = $this->Looks_model->get_reviews($data['look']['look_info'][0]['id'],0);
     //echo'<pre>';print_r($data['look']);exit;    
    if(!empty($data['look']['look_info'])){
		
	if($data['look']['look_info']['0']['looks_for']=='1')
		$gender ='women';
	else 
		$gender ='men'; 
	$i= 0;$brand_name= '';
	
	foreach($data['look']['brands_info'] as $row){
		$brand_name = $brand_name.' '.ucwords($row['user_name']);
		$i++;
	}
	$seo_title = str_replace("#","",$data['look']['look_info'][0]['name']);
    $this->seo_title = $seo_title.'-'.$gender.'\'s Looks'.' - '.$brand_name.' - '.'Buy Online Now on StyleCracker';
    $this->seo_desc = '';
    $this->seo_keyword = '';
	  $this->seo_canonical = base_url().'looks/look-details/'.$data['look']['look_info'][0]['slug'];
    $this->seo_image = $this->config->item('sc_look_image_url').$data['look']['look_info'][0]['image'];
    $this->load->view('common/header_view');
    $this->load->view('look_details',$data);
    $this->load->view('common/footer_view');
	}else{
    $this->load->view('common/header_view');
		$this->load->view('errors/error_404');
    $this->load->view('common/footer_view');
	}
  }

  public function get_products_extra_info(){
  		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
			}else{
			$product_id = $this->input->post('product_id');
			$val = $this->Looks_model->get_products_extra_info($product_id);
			$images = $this->Looks_model->get_products_extra_images($product_id);
      $zoomclass="zoom";
      if(!empty($images)){
        $zoomclass="";
      }

			$product_html  ='<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content"><div class="modal-body"><button type="button" class="close" data-dismiss="modal">×</button>
        	<div class="quickview-innner" >
        			<div class="row">
        				<div class="col-md-6">
							<div class="quickview-zoom-wrp">
								<div class="quickview-image-slider">
									<div class="zoom-img-wrp '.$zoomclass.' single-img">
									<a href="'.$this->config->item('sc_promotional_look_image_url').@$val['image'].'">
										<img src="'.$this->config->item('sc_promotional_look_image_url').@$val['image'].'" data-zoom-image="'.$this->config->item('sc_promotional_look_image_url').@$val['image'].'" alt="'.@$val['name'].'"
										 title="'.@$val['name'].'"/>
										 	</a>
                      
                					</div>
													';
                				if(!empty($images)){
                					foreach($images as $img_val){
	          $product_html = $product_html.'	<div class="zoom-img-wrp extra-img">
																	<a href="'.$this->config->item('product_extra_images').@$img_val['product_images'].'">
	                  						<img src="'.$this->config->item('product_extra_images_thumb').@$img_val['product_images'].'" alt="'.$val['name'].'" title="'.$val['name'].'" data-zoom-image="'.$this->config->item('product_extra_images').@$img_val['product_images'].'"/>
																</a>

                					</div>';
                				} }
              $product_html  = $product_html.'</div>
<div class="ic-zoom-wrp">
                                <i class="fa fa-hand-o-up"></i>
                                 </div>
                  		</div>
											<div id="zoom-place-holder"></div>
              </div>
              <div class="col-md-6">
                <div class="quickview-desc-wrp">

                  <h1 class="quickview-item-title" id="quickview-item-title">
                    '.@stripslashes($val['name']).'
                  </h1>
                  <div class="quickview-item-desc-content" id="quickview-item-desc-content">
                   '.@htmlspecialchars_decode($val['description']).'
                  </div>


                </div>
              </div>
            </div>


        </div>
      </div>
    </div>
  </div>
';

echo $product_html;
		}
  }

   function post_review(){
    $post_review = $this->input->post('post_review');
    $ratings = $this->input->post('ratings');
    $brand_token = $this->input->post('brand_token');    

    if($post_review!='' && $ratings!='' && $brand_token!=''){     
        if($this->Looks_model->post_review($post_review,$ratings,$brand_token)){
          return true;
        }else{
          return false;
        }
    }

  }

  function get_reviews(){
      $htmldata = '';
      $brand_token = $this->input->post('brand_token');
      $review = $this->Looks_model->get_reviews($brand_token,2);
      if(!empty($review)){
        foreach($review as $val){
          $to_time = strtotime($this->config->item('sc_date'));
          $from_time = strtotime($val['created_datetime']);
          /*$htmldata = $htmldata.'<h3 class="title">Reviews:</h3>';
          $htmldata = $htmldata.'<div>'.$this->get_user($val['user_id']).'</div>';
          $htmldata = $htmldata.'<div>'.$val['review_text'].'</div>';
          $htmldata = $htmldata.'<div>Rated'.$val['ratings'].'</div>';*/

           $htmldata = $htmldata.'<div class="user-review">';
                  $htmldata = $htmldata.'<div class="user-info">';
                  if($this->view_profile($val['user_id'])){
                    $htmldata = $htmldata.'<div class="user-photo"><img src="'.base_url().'assets/images/brands/profile.png"></div>';
                  }else{
                    $htmldata = $htmldata.'<div class="user-photo"><img src="'.base_url().'assets/images/brands/profile.png"></div>';
                  }
                    $htmldata = $htmldata.'<div class="user-name-wrp">';
                      $htmldata = $htmldata.'<div class="user-name">'.$this->get_user($val['user_id']);
                      $htmldata = $htmldata.'</div>';
                      $htmldata = $htmldata.'<div class="user-activity">'.round(abs($to_time - $from_time) / 60,2).'mins ago';
                      $htmldata = $htmldata.'</div>';
                    $htmldata = $htmldata.'</div>';
                    $htmldata = $htmldata.'<div class="user-ratings">';
                      $htmldata = $htmldata.'<div class="ratings-wrp">';
                        $htmldata = $htmldata.'<div class="ratings-label">';
                        $htmldata = $htmldata.'</div>';
                        $htmldata = $htmldata.'<ul class="ratings rating-'.$val['ratings'].'">';
                          $htmldata = $htmldata.'<li class="star1"></li>';
                          $htmldata = $htmldata.'<li class="star2"></li>';
                          $htmldata = $htmldata.'<li class="star3"></li>';
                          $htmldata = $htmldata.'<li class="star4"></li>';
                          $htmldata = $htmldata.'<li class="star5"></li>';
                        $htmldata = $htmldata.'</ul>';
                      $htmldata = $htmldata.'</div>';
                    $htmldata = $htmldata.'</div>';
                    $htmldata = $htmldata.'<div class="clear">';
                    $htmldata = $htmldata.'</div>';
                  $htmldata = $htmldata.'</div>';
                  $htmldata = $htmldata.$val['review_text'];
                $htmldata = $htmldata.'</div>';          
          //$htmldata = $htmldata.'<div>'.round(abs($to_time - $from_time) / 60,2). " minute".'</div>';
         
        }
        $htmldata = $htmldata.'<div class="load-more-wrp" id="load_more" onclick="load_more_reviews();"><div class="load-more">Load More</div></div>';
      }
      
      echo $htmldata;
  }

   function get_all_reviews(){
      $htmldata = '';
      $brand_token = $this->input->post('brand_token');
      $review = $this->Looks_model->get_reviews($brand_token,0);
      if(!empty($review)){
        foreach($review as $val){
          $to_time = strtotime($this->config->item('sc_date'));
          $from_time = strtotime($val['created_datetime']);
          
         /* $htmldata = $htmldata.'<div>'.$this->get_user($val['user_id']).'</div>';
          $htmldata = $htmldata.'<div>'.$val['review_text'].'</div>';
          $htmldata = $htmldata.'<div>Rated'.$val['ratings'].'</div>';
           $htmldata = $htmldata.'<div>'.round(abs($to_time - $from_time) / 60,2). " minute".'</div>';*/

               $htmldata = $htmldata.'<div class="user-review">';
                  $htmldata = $htmldata.'<div class="user-info">';
                 if($this->view_profile($val['user_id'])){
                    $htmldata = $htmldata.'<div class="user-photo"><img src="'.base_url().'assets/images/brands/profile.png"></div>';
                  }else{
                    $htmldata = $htmldata.'<div class="user-photo"><img src="'.base_url().'assets/images/brands/profile.png"></div>';
                  }
                    $htmldata = $htmldata.'<div class="user-name-wrp">';
                      $htmldata = $htmldata.'<div class="user-name">'.$this->get_user($val['user_id']);
                      $htmldata = $htmldata.'</div>';
                      $htmldata = $htmldata.'<div class="user-activity">'.round(abs($to_time - $from_time) / 60,2).'mins ago';
                      $htmldata = $htmldata.'</div>';
                    $htmldata = $htmldata.'</div>';
                    $htmldata = $htmldata.'<div class="user-ratings">';
                      $htmldata = $htmldata.'<div class="ratings-wrp">';
                        $htmldata = $htmldata.'<div class="ratings-label">';
                        $htmldata = $htmldata.'</div>';
                        $htmldata = $htmldata.'<ul class="ratings rating-'.$val['ratings'].'">';
                          $htmldata = $htmldata.'<li class="star1"></li>';
                          $htmldata = $htmldata.'<li class="star2"></li>';
                          $htmldata = $htmldata.'<li class="star3"></li>';
                          $htmldata = $htmldata.'<li class="star4"></li>';
                          $htmldata = $htmldata.'<li class="star5"></li>';
                        $htmldata = $htmldata.'</ul>';
                      $htmldata = $htmldata.'</div>';
                    $htmldata = $htmldata.'</div>';
                    $htmldata = $htmldata.'<div class="clear">';
                    $htmldata = $htmldata.'</div>';
                  $htmldata = $htmldata.'</div>';
                  $htmldata = $htmldata.$val['review_text'];
                $htmldata = $htmldata.'</div>';
          
        }
      }
      echo $htmldata;
  }

   function view_profile($user_id){
       $get_user_info = $this->Looks_model->get_user($user_id);
       if($get_user_info){
          return $get_user_info[0]['profile_pic'];
        }else{
          return '';
      }
  }

  function get_user($user_id){
       $get_user_info = $this->Looks_model->get_user($user_id);
       if($get_user_info){
       return $get_user_info[0]['first_name'].' '.$get_user_info[0]['last_name'];
      }else{
        return '';
      }
  }


}
