<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brands_new extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
       parent::__construct();
       $this->load->model('Brands_model');
   	}

	public function index()
	{

    $data = array();
    $data['brands_list'] = $this->Brands_model->get_all_brands();
	  $data['brands_banners'] = $this->Brands_model->get_brands_banners();
    $data['brand_categories']['name'] = $this->Brands_model->get_brands_categories_list();
    $data['brand_collections']['name'] = $this->Brands_model->get_brands_collections_list();

    foreach($data['brand_collections']['name'] as $row){
      $data['brand_collections']['brands'][$row['category_slug']] = $this->Brands_model->get_brands_categories_wize($row['category_slug']);
    }
    
	// echo "<pre>";print_r($data['brands_banners']);exit;
		$this->load->view('common/header_view');
		$this->load->view('brands',$data);
		$this->load->view('common/footer_view');
	}

  public function search_brand(){
   $intCount = '-1'; $o=1;
    $search = $this->input->post('search');
    $type = $this->input->post('type');
    $brands_list = $this->Brands_model->search_brand(strtolower($search),$type);
    $number_array = array("0","1","2","3","4","5","6","7","8","9","!","@");
    $brand_count = count($brands_list);
    if($brand_count > 0){
       $intCount = $brand_count / 3 ;
    }
    if($intCount < 1){ $intCount = 1; }
    $i = 1;

    $brandhtml = '';


    if(!empty($brands_list)){ $k = 1; $new_name = ''; $old_name = '';
    foreach($brands_list as $val){
    if(round($intCount) >= $k){

    if($k == 1){
        $brandhtml = $brandhtml.'<div class="col-md-4">';
     }
      $new_name = strtoupper(substr($val['company_name'], 0,1));
      if($new_name != $old_name && !in_array($new_name, $number_array)){
      if($i != 1){
        $brandhtml=$brandhtml.'</ul>';
      }
       $brandhtml = $brandhtml.' <ul class="brand-listing"> ';
        $brandhtml = $brandhtml.'<li id="'.strtoupper($new_name).'" class="alpha">'.strtoupper($new_name);
        $brandhtml = $brandhtml.'<script type="text/Javascript">$('.strtoupper($new_name).').attr("href","#'.strtoupper($new_name).'");</script>';
        $brandhtml =$brandhtml.'</li>';
        $brandhtml = $brandhtml.'<li><a href="'.base_url().'brands/brand-details/'.$val['user_name'].'">'.$val['company_name'].'</a></li>';
        $old_name = $new_name;
       }else{
       if(in_array($new_name, $number_array) && $o == 1){
          $brandhtml = $brandhtml.'<li id="0-9" class="alpha">0-9</li>';
        }
        $brandhtml =$brandhtml.'<li><a href="'.base_url().'brands/brand-details/'.$val['user_name'].'">'.$val['company_name'].'</a></li>';
       }
    }else{
      $brandhtml =$brandhtml.' </div>';
      $k = 0;
     }
 $k++; $i++; $o++;}
  }

      echo $brandhtml;
  }


  function get_brand_category_wise(){


    $category_slug = $this->input->post("slug");
    $brands_list = $this->Brands_model->get_brands_categories_wize($category_slug);
    $number_array = array("0","1","2","3","4","5","6","7","8","9","!","@");
    $brand_count = count($brands_list);
    $intCount =0;
    if($brand_count > 0){
      $intCount = $brand_count / 3 ;
    }
    if($intCount < 1){ $intCount = 1; }
      $i = 0;
    $output="";
    if(!empty($brands_list)){ 
      $k = 100; $new_name = ''; $old_name = ''; $o = 1;
      
      $row_count = 0;
      $new_labal = 0;
      foreach($brands_list as $val){ 
          if(!isset($old_name_start)){
            $old_name_start = "";
            $output .= '<div class="col-md-4 col-sm-6">';
          }

          
             
            $new_name = strtoupper(substr(trim($val->company_name), 0,1));
            if($new_name != $old_name_start && $old_name_start != ""){
                $output .= "</ul>";
             }
             if($new_name != $old_name_start){
                $old_name_start = strtoupper(substr(trim($val->company_name), 0,1));
                $output .= '<ul class="brand-listing">';
                $output .= "<li id='".strtoupper($new_name)."' class='alpha'>".strtoupper($new_name)."<script type='text/Javascript'>var a = document.getElementsByClassName('".strtoupper($new_name)."'); a[0].href = '#".strtoupper($new_name)."';</script>
                </li>";
             }

             $row_count++;

             $output .= " <li><a href='".base_url()."brands/brand-details/".$val->user_name."' >".$val->company_name."</a></li>";
             
          if($intCount <= $row_count){
             $output .= '</div>';
             $output .= '<div class="col-md-4 col-sm-6">';
             $row_count=0;
          }

      }
    }
/*
            if(round($intCount) >= $k){  echo $val->user_name.", ";
              if($k == 1){
                $output .= '<div class="col-md-4 col-sm-6">';
                $old_name_start = strtoupper(substr(trim($val->company_name), 0,1));
               } 
              $new_name = strtoupper(substr(trim($val->company_name), 0,1));
              if($new_name != $old_name && !in_array($new_name, $number_array)){
                  if($i != 1){
                    $output .= "</ul>";
                  }
                  $output .= '<ul class="brand-listing">';
                  $output .= "<li id='".strtoupper($new_name)."' class='alpha'>".strtoupper($new_name);

                  $output .= "<script type='text/Javascript'>var a = document.getElementsByClassName('".strtoupper($new_name)."'); a[0].href = '#".strtoupper($new_name)."';</script>
                    </li>
                    <li><a href='".base_url()."brands/brand-details/".$val->user_name."' >".$val->company_name."</a></li>";
                  $old_name = $new_name;
                }else{
                  if(in_array($new_name, $number_array) && $o == 1){
                    $output .= '<li id="0-9" class="alpha">0-9
                                 <li><a href="'.base_url().'brands/brand-details/'.$val->user_name.'">'.$val->company_name. '</a></li>';
                  }
                  $o++; 
                }
              }
              else{
             
              $k = 1;
              //$new_name = strtoupper(substr(trim($val->company_name), 0,1));
             // echo $val->user_name.", ";
             
                $output .= '</div>';
            } 
            if(isset($old_name_start) && $old_name_start != $new_name){
              $k++;
              $i++;
              $old_name_start = strtoupper(substr(trim($val->company_name), 0,1));
            }
            
          }
      }*/
     echo $output;
  }

  function brand_details(){
    $data = array();
    $brand_slug = $this->uri->segment(3);
    if($brand_slug){
    $data = $this->Brands_model->get_brands_info($brand_slug);

    $this->load->view('common/header_view');
    $this->load->view('brands_details',$data);
    $this->load->view('common/footer_view');
  }else{
    redirect('/schome');
  }
  }

  function redirect(){
    $product_id = $this->uri->segment(4);
    $user_id = $this->uri->segment(3);
    if($product_id){
      $data['product'] = $this->Brands_model->get_product_info($product_id);
      $data['encode_product_id'] = $this->encryptOrDecrypt($product_id,'encrypt');
      $data['user_info'] = $user_id;
      //$this->load->view('common/header_view');
      $this->load->view('redirect',$data);
     // $this->load->view('common/footer_view');
    }
  }

  function post_review(){
    $post_review = $this->input->post('post_review');
    $ratings = $this->input->post('ratings');
    $brand_token = $this->input->post('brand_token');


    if($post_review!='' && $ratings!='' && $brand_token!=''){
        if($this->Brands_model->post_review($post_review,$ratings,$brand_token)){
          return true;
        }else{
          return false;
        }
    }

  }

  function get_reviews(){
      $htmldata = '';
      $brand_token = $this->input->post('brand_token');
      $review = $this->Brands_model->get_reviews($brand_token,2);
      if(!empty($review)){
        foreach($review as $val){
          $to_time = strtotime($this->config->item('sc_date'));
          $from_time = strtotime($val['created_datetime']);
          /*$htmldata = $htmldata.'<h3 class="title">Reviews:</h3>';
          $htmldata = $htmldata.'<div>'.$this->get_user($val['user_id']).'</div>';
          $htmldata = $htmldata.'<div>'.$val['review_text'].'</div>';
          $htmldata = $htmldata.'<div>Rated'.$val['ratings'].'</div>';*/

           $htmldata = $htmldata.'<div class="user-review">';
                  $htmldata = $htmldata.'<div class="user-info">';
                  if($this->view_profile($val['user_id'])){
                    $htmldata = $htmldata.'<div class="user-photo"><img src="'.base_url().'assets/images/brands/profile.png"></div>';
                  }else{
                    $htmldata = $htmldata.'<div class="user-photo"><img src="'.base_url().'assets/images/brands/profile.png"></div>';
                  }
                    $htmldata = $htmldata.'<div class="user-name-wrp">';
                      $htmldata = $htmldata.'<div class="user-name">'.$this->get_user($val['user_id']);
                      $htmldata = $htmldata.'</div>';
                      $htmldata = $htmldata.'<div class="user-activity">'.round(abs($to_time - $from_time) / 60,2).'mins ago';
                      $htmldata = $htmldata.'</div>';
                    $htmldata = $htmldata.'</div>';
                    $htmldata = $htmldata.'<div class="user-ratings">';
                      $htmldata = $htmldata.'<div class="ratings-wrp">';
                        $htmldata = $htmldata.'<div class="ratings-label">';
                        $htmldata = $htmldata.'</div>';
                        $htmldata = $htmldata.'<ul class="ratings rating-'.$val['ratings'].'">';
                          $htmldata = $htmldata.'<li class="star1"></li>';
                          $htmldata = $htmldata.'<li class="star2"></li>';
                          $htmldata = $htmldata.'<li class="star3"></li>';
                          $htmldata = $htmldata.'<li class="star4"></li>';
                          $htmldata = $htmldata.'<li class="star5"></li>';
                        $htmldata = $htmldata.'</ul>';
                      $htmldata = $htmldata.'</div>';
                    $htmldata = $htmldata.'</div>';
                    $htmldata = $htmldata.'<div class="clear">';
                    $htmldata = $htmldata.'</div>';
                  $htmldata = $htmldata.'</div>';
                  $htmldata = $htmldata.$val['review_text'];
                $htmldata = $htmldata.'</div>';


          //$htmldata = $htmldata.'<div>'.round(abs($to_time - $from_time) / 60,2). " minute".'</div>';

        }
        $htmldata = $htmldata.'<div class="load-more-wrp" id="load_more" onclick="load_more_reviews();"><div class="load-more">Load More</div></div>';
      }

      echo $htmldata;
  }

   function get_all_reviews(){
      $htmldata = '';
      $brand_token = $this->input->post('brand_token');
      $review = $this->Brands_model->get_reviews($brand_token,0);
      if(!empty($review)){
        foreach($review as $val){
          $to_time = strtotime($this->config->item('sc_date'));
          $from_time = strtotime($val['created_datetime']);

         /* $htmldata = $htmldata.'<div>'.$this->get_user($val['user_id']).'</div>';
          $htmldata = $htmldata.'<div>'.$val['review_text'].'</div>';
          $htmldata = $htmldata.'<div>Rated'.$val['ratings'].'</div>';
           $htmldata = $htmldata.'<div>'.round(abs($to_time - $from_time) / 60,2). " minute".'</div>';*/

               $htmldata = $htmldata.'<div class="user-review">';
                  $htmldata = $htmldata.'<div class="user-info">';
                 if($this->view_profile($val['user_id'])){
                    $htmldata = $htmldata.'<div class="user-photo"><img src="'.base_url().'assets/images/brands/profile.png"></div>';
                  }else{
                    $htmldata = $htmldata.'<div class="user-photo"><img src="'.base_url().'assets/images/brands/profile.png"></div>';
                  }
                    $htmldata = $htmldata.'<div class="user-name-wrp">';
                      $htmldata = $htmldata.'<div class="user-name">'.$this->get_user($val['user_id']);
                      $htmldata = $htmldata.'</div>';
                      $htmldata = $htmldata.'<div class="user-activity">'.round(abs($to_time - $from_time) / 60,2).'mins ago';
                      $htmldata = $htmldata.'</div>';
                    $htmldata = $htmldata.'</div>';
                    $htmldata = $htmldata.'<div class="user-ratings">';
                      $htmldata = $htmldata.'<div class="ratings-wrp">';
                        $htmldata = $htmldata.'<div class="ratings-label">';
                        $htmldata = $htmldata.'</div>';
                        $htmldata = $htmldata.'<ul class="ratings rating-'.$val['ratings'].'">';
                          $htmldata = $htmldata.'<li class="star1"></li>';
                          $htmldata = $htmldata.'<li class="star2"></li>';
                          $htmldata = $htmldata.'<li class="star3"></li>';
                          $htmldata = $htmldata.'<li class="star4"></li>';
                          $htmldata = $htmldata.'<li class="star5"></li>';
                        $htmldata = $htmldata.'</ul>';
                      $htmldata = $htmldata.'</div>';
                    $htmldata = $htmldata.'</div>';
                    $htmldata = $htmldata.'<div class="clear">';
                    $htmldata = $htmldata.'</div>';
                  $htmldata = $htmldata.'</div>';
                  $htmldata = $htmldata.$val['review_text'];
                $htmldata = $htmldata.'</div>';

        }
      }
      echo $htmldata;
  }

  function get_user($user_id){
       $get_user_info = $this->Brands_model->get_user($user_id);
       if($get_user_info){
       return $get_user_info[0]['first_name'].' '.$get_user_info[0]['last_name'];
      }else{
        return '';
      }
  }
  function view_profile($user_id){
       $get_user_info = $this->Brands_model->get_user($user_id);
       if($get_user_info){
          return $get_user_info[0]['profile_pic'];
        }else{
          return '';
      }
  }
  function encryptOrDecrypt($mprhase, $crypt) {
       $MASTERKEY = "STYLECRACKERAPI";
       $td = mcrypt_module_open('tripledes', '', 'ecb', '');
       $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
       mcrypt_generic_init($td, $MASTERKEY, $iv);
       if ($crypt == 'encrypt')
       {
           $return_value = base64_encode(mcrypt_generic($td, $mprhase));
       }
       else
       {
           $return_value = mdecrypt_generic($td, base64_decode($mprhase));
       }
       mcrypt_generic_deinit($td);
       mcrypt_module_close($td);
       return $return_value;
  }
}
