<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
class Collection extends TT_REST_Controller {

	function __construct(){

       parent::__construct();
	   $this->load->model('Collection_model');
	   $this->load->model('Mobile_look_model');
	   $this->load->library('curl'); 
	   $this->load->model('Category_filter_model');
   	}
	
	public function get_all_collection_post(){
		
		$data['user_id'] = $this->post('user_id');
		$data['auth_token'] = $this->post('auth_token');
		$data['limit'] = $this->post('limit');
		$data['offset'] = $this->post('offset');
		$data['body_shape'] = $this->post('body_shape');
		$data['pa_pref1'] = $this->post('pa_pref1');
		$data['type'] = $this->post('type');
		$data['gender'] = $this->post('gender');
		
		$data['limit_cond'] = " LIMIT ".$data['offset'].",".$data['limit'];
		$data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
		$result = $this->Collection_model->get_all_collection($data);
		$this->success_response($result);
	}
	
	public function get_single_object_post(){
		
		$data['user_id'] = $this->post('user_id');
		$data['auth_token'] = $this->post('auth_token');
		$data['object_id'] = $this->post('object_id');
		$data['limit'] = $this->post('limit');
		$data['offset'] = $this->post('offset');
		$data['body_shape'] = $this->post('body_shape');
		$data['pa_pref1'] = $this->post('pa_pref1');
		$data['type'] = 'single';
		$data['gender'] = $this->post('gender');
		$sort_by = $this->post('sort_by');
		if($sort_by == '2'){
			$data['order_by'] = 'order by p.price asc';
        }else if($sort_by == '1'){
			$data['order_by'] = 'order by p.price desc';
        }
		$data['product_link'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_310/';
		$result = $this->Collection_model->get_single_collection($data['object_id'],$data);
		
		$data['limit_cond'] = " LIMIT 0,5";
		$result['recommended_collection'] = $this->Collection_model->get_all_collection($data);
		if(!empty($result)){
			$this->success_response($result);
		}else {
			$this->failed_response(1001, "No Record(s) Found");
		}
		
		
	}
	
	public function add_to_wishlist_post(){
		$user_id = $this->post('user_id');
        $auth_token = $this->post('auth_token');
		$fav_for = $this->post('fav_for');
		$fav_id = $this->post('fav_id');
		$res = $this->Collection_model->user_wishlist($user_id,$fav_for,$fav_id);
		if($res === 1){ 
			$data = new stdClass();
			$data->message = 'success';
			$this->success_response($data);
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
		
	}
	
	function filter_collection_products_get(){
		$data = array();
		$user_id = $this->input->get('user_id');
		$object_id = $this->input->get('object_id');
        $lastlevelcat = $this->input->get('lastlevelcat');
        $brandId = $this->input->get('brandId');
        $priceId = $this->input->get('priceId');
        $sizeId = $this->input->get('sizeId');
        $attributesId = $this->input->get('attributesId');
        $sort_by = $this->input->get('sort_by');
		$limit = $this->input->get('limit');
		$offset = $this->input->get('offset');
		
		$product_id = '';
		$collection_data = $this->Collection_model->get_collection_info($object_id);
		// echo count(unserialize($collection_data['collection_products']));exit;
		$product_id = implode(',',unserialize($collection_data['collection_products']));
		
		$data = $this->Collection_model->filter_collection_products($user_id,$object_id,$lastlevelcat,$brandId,$priceId,$sort_by,$attributesId,$sizeId,$limit,$offset);
		$data['filter']['category'] = $this->Category_filter_model->get_all_product_category($product_id); //category list
		$data['product_data']['total_row'] = $data['total_products'];
		$data['filter']['sort_by'] = array('0'=>'latest','1'=>'Price high to low','2'=>'Price low to high'); // sort by
		$data['filter']['price_list'] = array('0'=>'Below 999','1'=>'1000 - 1999','2'=>'2000 - 3499','3'=>'3500 - 4999','4'=>'5000 +'); //price list 
		/*
		$data['filter']['global_attributes'] = $this->Category_filter_model->global_attributes();
		
		if(!empty($data['filter']['global_attributes'])){
			foreach($data['filter']['global_attributes'] as $val){
				$i=0;
				foreach($this->Category_filter_model->attributes_values($val['id']) as $values){
					$data['filter'][$val['attribute_slug']][$i]['id'] = $values['id'];
					$data['filter'][$val['attribute_slug']][$i]['attr_value_name'] = $values['attr_value_name'];
					@$data['filter'][$val['attribute_slug']][$i]['attr_hashtag'] = $values['attr_hashtag'];
					$i++;
				}
			}
		}*/
		$data['product_data']['collection_data'] = $collection_data;
		$data['product_data']['collection_data']['collection_id'] = $object_id;
		$data['user_id'] = $user_id;
		$data['body_shape'] = $this->post('body_shape');
		$data['pa_pref1'] = $this->post('pa_pref1');
		$data['type'] = 'single';
		$data['gender'] = $this->post('gender');
		$data['limit_cond'] = " LIMIT 0,5";
		$data['object_id'] = $object_id;
		$data['recommended_collection'] = $this->Collection_model->get_all_collection($data);
		$this->success_response($data);
		
	}
}