<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Analytics extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
       parent::__construct();
       $this->load->model('Schome_model'); 
       $this->load->model('Pa_model'); 
       $this->load->model('Analytics_model'); 
      // $this->load->library('upload');
   	} 
   
	public function index()
	{
		
	}


	public function add_look_click(){
		if($this->session->userdata('user_id')) $user_id = $this->session->userdata('user_id'); else $user_id = $this->input->post('user_id');
		$look_id = $this->input->post('look_id');
		$type = $this->input->post('type');
		$this->Analytics_model->add_look_click($user_id,$look_id, $type);  
		
	}

	public function add_product_click(){
		if($this->session->userdata('user_id')) $user_id = $this->session->userdata('user_id'); else $user_id = $this->input->post('user_id');
		$product_id = $this->input->post('product_id');
		$frmsource = $this->input->post('frmsource');
		$SCUniqueID =$this->input->post('SCUniqueID');
		$this->Analytics_model->add_product_click($user_id,$product_id,$frmsource,$SCUniqueID);  
		
	}

	public function add_brand_click(){
		if($this->session->userdata('user_id')) $user_id = $this->session->userdata('user_id'); else $user_id = $this->input->post('user_id');
		$brand_id = $this->input->post('brand_id');
		$this->Analytics_model->add_brand_click($user_id,$brand_id);  
		
	}

	public function social_media_share(){
		if($this->session->userdata('user_id')) $user_id = $this->session->userdata('user_id'); else $user_id = $this->input->post('user_id');
		$look_id = $this->input->post('look_id');
		$source = $this->input->post('source');
		$this->Analytics_model->social_media_share($user_id,$look_id, $source);  
	}

	public function add_geo_loc(){
		if($this->session->userdata('user_id')) $user_id = $this->session->userdata('user_id'); else $user_id = $this->input->post('user_id');
		$geo = $this->input->post('geo');
		$this->Analytics_model->add_geo_loc($user_id,$geo);  
		
	}
        public function track_user_activity(){


    	        if($this->session->userdata('user_id')) $user_id = $this->session->userdata('user_id'); else $user_id = $this->input->post('user_id');
		$p_visited= $this->input->post('p_visited');
		$p_id= $this->input->post('p_id');
		$page_scroll= $this->input->post('page_scroll');
		$time_spent= $this->input->post('time_spent');


		//$user_id = $this->input->post('user_id');
                $this->Analytics_model->track_user_activity_model($p_visited, $p_id, $page_scroll, $time_spent, $user_id);
        }

        public function track_user_session(){
    	        if($this->session->userdata('user_id')) $user_id = $this->session->userdata('user_id'); else $user_id = $this->input->post('user_id');
		$session_counter= $this->input->post('session_counter');
		$source= $this->input->post('source');
		$pvisit= $this->input->post('pvisit');
		$fvisit= $this->input->post('fvisit');
		$cvisit= $this->input->post('cvisit');
                $this->Analytics_model->update_user_session($session_counter, $user_id, $source, $pvisit, $fvisit, $cvisit);
        }
		
	public function add_collection_click(){
		
		if($this->session->userdata('user_id')) $user_id = $this->session->userdata('user_id'); else $user_id = $this->input->post('user_id');
		$object_id = $this->input->post('object_id');
		$frmsource = $this->input->post('frmsource');
		$SCUniqueID =$this->input->post('SCUniqueID');
		$this->Analytics_model->add_product_click($user_id,$object_id,$frmsource,$SCUniqueID);  
		return true;
		
	}	
}
