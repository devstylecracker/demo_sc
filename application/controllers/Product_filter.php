<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';

class Product_filter extends TT_REST_Controller
{
  function __construct()
	{
		parent::__construct();
		$this->load->model('Favorites_model');
		$this->load->model('Product_filter_model');
		$this->load->model('Mobile_look_model');
		$this->load->model('Filterbrand_model');
		$this->load->model('home_model');
		$this->load->model('Category_filter_model');
		$this->open_methods = array('my_feed','single_look_post');    
	}
    
	public function filter_page_get(){
		
		$data = array();
		
		$user_id = $this->get('user_id');
        $cat = $this->get('cat');
        $brandId = $this->get('brandId');
        $colorId = $this->get('colorId');
        $lastCatId = $this->get('lastCatId');
        $priceId = $this->get('priceId');
        $min_price = $this->get('min_price');
        $max_price = $this->get('max_price');
        $isButton = $this->get('isButton');
        $sort_by = $this->get('sort_by');
        $attributesId = $this->get('attributesId');
        $sizeId = $this->get('sizeId');
        $search = $this->get('search');
        $is_recommended = $this->get('is_recommended');
		$limit = $this->get('limit');
		$offset = $this->get('offset');
		$platform = 2;
		$bucket = $this->get('bucket');
		$parent_cat = $this->get('parent_cat');
        if($isButton == 0){ $min_price =0; $max_price=0; }
         $data['all_data'] = $this->Product_filter_model->get_data($cat,$brandId,$colorId,$priceId,$discount=null,$sort_by,$offset,$lastCatId,$min_price,$max_price,$attributesId,$sizeId,$search,$is_recommended,$is_brand=0,$platform,$limit,$user_id);
		
		$data['all_data']['filter']['price_list'] = array('0','Below 999','1000 - 1999','2000 - 3499','3500 - 4999','5000 +');
		if(!empty($data['all_data']['filter']['global_attributes'])){
            foreach($data['all_data']['filter']['global_attributes'] as $val){
				$i=0;
				foreach($this->Product_filter_model->attributes_values($val['id'],0,0) as $values){
					$data['all_data']['filter'][$val['attribute_slug']][$i]['id'] = $values['id'];
					$data['all_data']['filter'][$val['attribute_slug']][$i]['attr_value_name'] = $values['attr_value_name'];
					$data['all_data']['filter'][$val['attribute_slug']][$i]['attr_color'] = $values['attr_hashtag'];
					$i++;
				}
			}
		}
		
		if(!empty($data['all_data']['filter']['category_attributes'])){
            foreach($data['all_data']['filter']['category_attributes'] as $val){
				$i=0;
				foreach($this->Product_filter_model->attributes_values($val['id'],$data['all_data']['is_recommended'],$val['attribute_parent']) as $values){
					$data['all_data']['filter'][$val['attribute_slug']][$i]['id'] = $values['id'];
					$data['all_data']['filter'][$val['attribute_slug']][$i]['attr_value_name'] = $values['attr_value_name'];
					$data['all_data']['filter'][$val['attribute_slug']][$i]['attr_color'] = $values['attr_hashtag'];
					$i++;
				}
			}
		}
		
		if(!empty($data)){
			$this->success_response($data);
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
		
	}
	
	public function get_cat_list_get(){
		$cat_id = $this->get('cat_id');
		$data = array();
		if($cat_id == ''){
			$data['category_list'] = $this->Category_filter_model->get_gender(-1);
		}else{
			$data['category_list'] = $this->Category_filter_model->get_child_list($cat_id);
		}
		
		if(!empty($data)){
			$this->success_response($data);
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
	}
	
	public function filter_products_get(){
		$user_id = $this->get('user_id');
		$bucket = $this->get('bucket');
        $cat_id = $this->get('cat_id');
        $brandId = $this->get('brandId');
		$lastlevelcat = $this->get('lastlevelcat');
        $priceId = $this->get('priceId');
        $min_price = $this->get('min_price');
        $max_price = $this->get('max_price');
		$sort_by = $this->get('sort_by');
        $attributesId = $this->get('attributesId');
        $sizeId = $this->get('sizeId');
        $search = $this->get('search');
		$limit = $this->get('limit');
		$offset = $this->get('offset');
		$is_all = $this->get('is_all');
		if($user_id >0 || $bucket > 0){ $is_recommended = 1; }else{ $is_recommended = 0; }
		$data['all_data'] = $this->Category_filter_model->get_products($user_id,$bucket,$cat_id,$lastlevelcat,$brandId,$priceId,$min_price,$max_price,$sort_by,$attributesId,$sizeId,$search,$limit,$offset,$is_recommended);
		
		// price_list
		$data['all_data']['filter']['price_list'] = array('0'=>'Below 999','1'=>'1000 - 1999','2'=>'2000 - 3499','3'=>'3500 - 4999','4'=>'5000 +');
		
		// sort by
		$data['all_data']['filter']['sort_by'] = array('0'=>'latest','1'=>'Price high to low','2'=>'Price low to high');
		
		// global_attributes
		if(!empty($data['all_data']['filter']['global_attributes'])){
            foreach($data['all_data']['filter']['global_attributes'] as $val){
				$i=0;
				foreach($this->Category_filter_model->attributes_values($val['id']) as $values){
					$data['all_data']['filter'][$val['attribute_slug']][$i]['id'] = $values['id'];
					$data['all_data']['filter'][$val['attribute_slug']][$i]['attr_value_name'] = $values['attr_value_name'];
					@$data['all_data']['filter'][$val['attribute_slug']][$i]['attr_color'] = $values['attr_hashtag'];
					$i++;
				}
			}
		}
		
		// category_attributes
		if(!empty($data['all_data']['filter']['category_attributes'])){
            foreach($data['all_data']['filter']['category_attributes'] as $val){
				$i=0;
				foreach($this->Category_filter_model->attributes_values($val['id']) as $values){
					$data['all_data']['filter'][$val['attribute_slug']][$i]['id'] = $values['id'];
					$data['all_data']['filter'][$val['attribute_slug']][$i]['attr_value_name'] = $values['attr_value_name'];
					$data['all_data']['filter'][$val['attribute_slug']][$i]['attr_color'] = $values['attr_hashtag'];
					$i++;
				}
			}
		}
		
		if(!empty($data)){
			$this->success_response($data);
		}else{
			$this->failed_response(1001, "No Record(s) Found");
		}
	}
	
}