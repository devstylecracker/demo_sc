<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Singlebrand extends MY_Controller {


	function __construct(){

       parent::__construct();
       $this->load->model('Allproducts_model');
       $this->load->model('Singlebrand_model');
       $this->load->model('Product_desc_model');
       $this->load->model('Home_model');
	   $this->load->model('Scborough_model');
   	}

    function index(){
      $data = array();
      $url = $this->uri->uri_string();
      $brand = ''; $color = ''; $cat = ''; $offset = 0; $search = ''; $is_brand=0;
	  
	  /* added for auto login */
		$user_id = $this->input->get('id');
		if($user_id >0) $array_field=array('is_app' => 1);
		$user_id = base64_decode($user_id);
		$get_user_info = $this->Scborough_model->get_user_info($user_id);
		if(!empty($get_user_info)){
			$res = $this->login($get_user_info[0]['email'],$get_user_info[0]['password']);
		}
		/* added for auto login */

      $url_data = explode('/', $url);
      unset($url_data[0]);
      if(!empty($url_data)){
        $url_segment_count = count($url_data);  
        if(in_array("brand", $url_data)) {
          $key = array_search('brand', $url_data);
          $brand = trim($url_data[$key+1]);
          $brand = $this->Singlebrand_model->get_brand_slug($brand);
          $is_brand = 1;
        }else if(in_array("page", $url_data)){
          $key = array_search('page', $url_data);
          $offset = trim($url_data[$key+1]);
          $cat = $url_data[$url_segment_count-2];
        }else{
           $cat = $url_data[$url_segment_count];
        }
        
      }
      if(isset($_GET['search']) && $_GET['search']!=''){
        $search = $_GET['search'];
      }
      $is_recommended = 0;
      if(isset($_GET['is_recommended']) && $_GET['is_recommended']==1){
        $is_recommended = $_GET['is_recommended'];
      }
      $brand = SINGLE_BRANDS;
      $data['all_data'] = $this->Singlebrand_model->get_data($cat,$brand,$color,'','','',$offset,'','','','','',$search,$is_recommended,$is_brand);
	// echo "<pre>";print_r($data['all_data']);exit;
          if(SINGLE_BRANDS_PAGE_LAYOUT == 3){
            $data['left_filter'] = $this->load->view('common/single_brand_filter', $data, true);
          }
          $pagination_url = current_url();
          $pagination_url = explode("/page", $pagination_url)[0]; 
          $pagination = '';
          $adjacents = 3;
          $prev = $offset-1;
          $next = 1;  
          $perpage = 12;
          $lastpage = ceil($data['all_data']['total_row']/$perpage);
          $lpm1 = $lastpage - 1; 
          if($lastpage > 1)
              { 
                $pagination .= "<div class='sc-pagination-wrp'><ul class=\"pagination sc-pagination\">";
                //previous button

                if ($offset != 1) 
                     $pagination.= "<li><a href=".$pagination_url.'/page/1'.">First</a></li>";
                  
                if ($offset > 1) 
                   
                     $pagination.= "<li><a href=".$pagination_url.'/page/'.$prev.">previous</a></li>";
                     $this->seo_prev = $pagination_url.'/page/'.$prev;
                   
                $limit = $offset + 5;
                if($offset != $lastpage && $offset< $lastpage-5  ){
                for ($counter =  $offset; $counter <= $limit; $counter++)
                  {
                    if ($counter == $offset){
                      $pagination.= "<li><span class=\"current\">$counter</span></li>";
                      $this->seo_canonical = $pagination_url.'/page/'.$counter;
                    }
                    else
                     {
                      $pagination.= "<li><a href=".$pagination_url.'/page/'.$counter.">$counter</a></li>";         
                     }
                  }
                }else{ 
                  for ($counter = $lastpage-5; $counter <= $lastpage; $counter++)
                  {
                    if ($counter == $offset){
                      $pagination.= "<li><span class=\"current\">$counter</span></li>";
                      $this->seo_canonical = $pagination_url.'/page/'.$counter;
                    }
                    else
                     if($counter > 0){
                      
                      $pagination.= "<li><a href=".$pagination_url.'/page/'.$counter.">$counter</a></li>";          
                        
                     }
                  }

                }
                
                //next button
                if ($offset < $counter - 1) 
                  $next = $offset + 1; 
                 
                     $pagination.= "<li><a href=".$pagination_url.'/page/'.$next.">next</a></li>";  
                     $this->seo_next = $pagination_url.'/page/'.$next;        
                 

                if ($offset != $lastpage) 
                  $pagination.= "<li><a href=".$pagination_url.'/page/'.$lastpage.">Last</a></li>"; 
                 
                $pagination.= "</ul></div>\n";    
                  }
                  
         $data['pagination'] =  $pagination;

      if($cat!=''){
        $this->seo_title =$this->Singlebrand_model->get_cat_name($cat)[0]['category_name'].' - Buy Online Now on StyleCracker' ;
      }

      if($brand!=''){
        $this->seo_title =$this->Singlebrand_model->get_brand_name($brand).' - Buy Online Now on StyleCracker' ; 
      }

      $this->load->view('common/header_view');
      $this->load->view('single_brand',$data);
      $this->load->view('common/footer_view');
    }

    function get_more_products(){
        
      if (!$this->input->is_ajax_request()) {
        exit('No direct script access allowed');
      }else{
        $data = array();
        $cat = $this->input->post('cat_id');
        $offset = $this->input->post('group_no');
        $brandId = $this->input->post('brandId');
        $colorId = $this->input->post('colorId');
        $lastCatId = $this->input->post('lastCatId');
        $priceId = $this->input->post('priceId');
        $min_price = $this->input->post('min_price');
        $max_price = $this->input->post('max_price');
        $isButton = $this->input->post('isButton');
        $sort_by = $this->input->post('sort_by');
        $attributesId = $this->input->post('attributesId');
        $sizeId = $this->input->post('sizeId');
        $search = $this->input->post('search');
        if($priceId!=''){$min_price =0; $max_price=0; }
        $is_recommended = $this->input->post('is_recommended');
        $data['all_data'] = $this->Singlebrand_model->get_data($cat,$brandId,$colorId,$priceId,'',$sort_by,$offset,$lastCatId,$min_price,$max_price,$attributesId,$sizeId,$search,$is_recommended);

        $this->load->view('more_single_brand_products',$data);
      }
    }

    function filter_products(){
      if (!$this->input->is_ajax_request()) {
        exit('No direct script access allowed');
      }else{
        $data = array();
        $cat = $this->input->post('cat_id');
        $brandId = $this->input->post('brandId');
        $colorId = $this->input->post('colorId');
        $lastCatId = $this->input->post('lastCatId');
        $priceId = $this->input->post('priceId');
        $min_price = $this->input->post('min_price');
        $max_price = $this->input->post('max_price');
        $isButton = $this->input->post('isButton');
        $sort_by = $this->input->post('sort_by');
        $attributesId = $this->input->post('attributesId');
        $sizeId = $this->input->post('sizeId');
        $search = $this->input->post('search');
        $is_recommended = $this->input->post('is_recommended');
        if($priceId!=''){ $min_price =0; $max_price=0; }
        $data['all_data'] = $this->Singlebrand_model->get_data($cat,$brandId,$colorId,$priceId,'',$sort_by,0,$lastCatId,$min_price,$max_price,$attributesId,$sizeId,$search,$is_recommended);

        $this->load->view('filter_single_brand_products',$data);
      }
    }
	
	public function login($logEmailid,$logPassword){

         if(trim($logEmailid) == ''){
            echo "Please enter your email id";

          }else if(trim($logPassword) == ''){
            echo "Please enter the Password";

          }else{
           $result = $this->Scborough_model->login_user($logEmailid,$logPassword);


          }
	}

}
