<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sccache extends MY_Controller {
    
    function __construct(){
        parent::__construct();
        $this->load->driver('cache');
    }  

    function clear_home_page_filter_type_all(){
        if($this->cache->file->delete('filter_type_all')){
            echo 'clear cache filter_type_all';
        }
    }

    function clear_home_page_filter_type_men(){
        if($this->cache->file->delete('filter_type_men')){
            echo 'clear cache filter_type_men';
        }
    }

    function clear_home_page_filter_type_women(){
        if($this->cache->file->delete('filter_type_women')){
            echo 'clear cache filter_type_women';
        }
    }

    function clear_category_list(){
        if($this->cache->file->delete('category_filter_data_all')){
            echo 'clear cache category_filter_data_all';
        }
        if($this->cache->file->delete('category_filter_data_men')){
            echo 'clear cache category_filter_data_men';
        }
         if($this->cache->file->delete('category_filter_data_women')){
            echo 'clear cache category_filter_data_women';
        }
    }

    function clear_brand_list(){
        if($this->cache->file->delete('brand_filter_type_men')){
            echo 'clear cache brand_filter_type_men';
        }
        if($this->cache->file->delete('brand_filter_type_women')){
            echo 'clear cache brand_filter_type_women';
        }
         if($this->cache->file->delete('brand_filter_type_all')){
            echo 'clear cache brand_filter_type_all';
        }
    }

    function clear_brand_listing_page(){
        if($this->cache->file->delete('brand_listing_page')){
            echo 'clear clear_brand_listing_page';
        }
    }
    
    function clear_sc_about_us(){
        if($this->cache->file->delete('sc_about_us')){
            echo 'clear clear_sc_about_us';
        }
    }

    function clear_get_megamenu_mens(){
        if($this->cache->file->delete('get_megamenu_mens')){
            echo 'clear clear_sc_about_us';
        }
    }

    function clear_get_megamenu_men_html(){
        if($this->cache->file->delete('get_megamenu_men_html')){
            echo 'clear clear_sc_about_us';
        }
    }

    function clear_get_megamenu_womens(){
        if($this->cache->file->delete('get_megamenu_womens')){
            echo 'clear clear_sc_about_us';
        }
    }

    function clear_get_megamenu_women_html(){
        if($this->cache->file->delete('get_megamenu_women_html')){
            echo 'clear clear_sc_about_us';
        }
    }

    function clear_look_women(){
        if($this->cache->file->delete('look_all')){
            echo 'clear clear_sc_about_us';
        }  
        if($this->cache->file->delete('look')){
            echo 'clear look individual';
        }   
        if($this->cache->file->delete('look_women')){
            echo 'clear clear_sc_about_us';
        }   
    }

    function clear_look_men(){
        if($this->cache->file->delete('look_all')){
            echo 'clear clear_sc_about_us';
        }   
        if($this->cache->file->delete('look')){
            echo 'clear look individual';
        }  
        if($this->cache->file->delete('look_men')){
            echo 'clear clear_sc_about_us';
        }   
    }
}