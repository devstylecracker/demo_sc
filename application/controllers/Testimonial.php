<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonial extends MY_Controller {

	function __construct(){
       parent::__construct();
      
   	}


    function index(){
      
      $this->load->view('seventeen/common/header_view');
      $this->load->view('seventeen/static/testimonials');
      $this->load->view('seventeen/common/footer_view');
    }

}