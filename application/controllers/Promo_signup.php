<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo_signup extends MY_Controller {
	function __construct(){
       parent::__construct();		
       $this->load->model('Schome_model');		 
       $this->load->library('bitly');         
       $this->load->library('email');		 
       $this->load->helper('form');
   	}

   	public function index()
	{
		$this->load->view('common/header_view');
		$this->load->view('promo_signup');
		$this->load->view('common/footer_view');
	}		       
}