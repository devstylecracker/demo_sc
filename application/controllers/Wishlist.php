<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wishlist extends MY_Controller {

	function __construct(){
       parent::__construct();
       $this->load->model('wishlist_model');
       $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
   	}

  function look_wishlist(){
      $data = array();
       if($this->session->userdata('user_id')){
          $user_id = $this->session->userdata('user_id');
          $data['looks'] = $this->wishlist_model->show_my_wishlist_looks($user_id);
          $data['looks_total'] = $this->wishlist_model->show_my_wishlist_looks_total($user_id);

          if(empty($data['looks'])){
            $data['looks'] = $this->wishlist_model->show_recommended_looks($user_id);
          }

          $this->load->view('common/header_view');
          $this->load->view('wishlist_looks',$data);
          $this->load->view('common/footer_view');

       }else{
          redirect('/');
       }
  }

  function get_more_wishlist_looks(){
      $data = array();
      if($this->session->userdata('user_id')){
          $user_id = $this->session->userdata('user_id');
          $offset = $this->input->post('offset');
          $data['looks'] = $this->wishlist_model->show_my_more_wishlist_looks($offset,$user_id);

          $this->load->view('more_looks',$data);

       }
  }

  function brands_wishlist(){
     $data = array();
     if($this->session->userdata('user_id')){
        $user_id = $this->session->userdata('user_id');
        $data['top_brands'] = $this->wishlist_model->show_my_wishlist_brands($user_id);
        $data['brands_total'] = 1;
        if(empty($data['top_brands'])){
          $data['brands_total'] = 0;
          $data['top_brands'] = $this->wishlist_model->show_recommended_brands($user_id);
        }
        
        $this->load->view('common/header_view');
        $this->load->view('wishlist_brands',$data);
        $this->load->view('common/footer_view');

       }else{
          redirect('/');
       }
  }

  function product_wishlist(){
     $data = array();
     if($this->session->userdata('user_id')){
        $user_id = $this->session->userdata('user_id');
        $data['products'] = $this->wishlist_model->show_my_wishlist_products($user_id);
        $data['products_total'] = 1;
        if(empty($data['products'])){
          $data['products_total'] = 0;
          $data['products'] = $this->wishlist_model->show_recommended_products($user_id);
        }

        $this->load->view('common/header_view');
        $this->load->view('wishlist_products',$data);
        $this->load->view('common/footer_view');

       }else{
          redirect('/');
       }
  }

}

?>