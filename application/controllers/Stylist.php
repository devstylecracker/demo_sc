<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Stylist extends MY_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see http://codeigniter.com/user_guide/general/urls.html

	 */



	function __construct(){

       parent::__construct();

       $this->load->model('Schome_model'); 
       $this->load->model('Home_model'); 

   	} 

   

	public function index()

	{

		

		$this->load->view('common/header_view');

		$this->load->view('stylist_detail');

		$this->load->view('common/footer_view');

	}



	public function stylist_detail()

	{

		$data = array();

		$stylist_name = $this->uri->segment(3);

		

      	$data['stylist_data'] = $this->Schome_model->get_stylist_details($stylist_name);
      	$data['stylist_looks'] = $this->Home_model->get_stylist_looks('1',$data['stylist_data'][0]['id']);

      	if($stylist_name!='' && !empty($data['stylist_data'])){

			$this->load->view('common/header_view',$data);

			$this->load->view('stylist_detail');

			$this->load->view('common/footer_view');

		}else{

			redirect('schome/aboutus');

		}

	}

function get_all_stylist_looks(){
	$stylist_id = $this->input->post('stylist_id');
   	$offset = $this->input->post('offset');

   	$data['looks'] = $this->home_model->get_stylist_looks($offset,$stylist_id);
    $this->load->view('more_looks',$data);
}

	

}

