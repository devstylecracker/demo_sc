<?php
ini_set('max_execution_time', 30000000000);
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Test extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('image_lib');
        $this->load->model('SCProductinventory_model');  
       
    }
	
	function data_get(){
    	$ftp_server = "182.72.235.75";
		$ftp_user = "f21_affliate";
		$ftp_pass = "f21_affliate";
		// set up a connection or die
		$conn_id = ftp_connect($ftp_server) or die("Couldn't connect to $ftp_server");

		// try to login
		if (@ftp_login($conn_id, $ftp_user, $ftp_pass)) {
		     "Connected as $ftp_user@$ftp_server\n";
		} else {
		     "Couldn't connect as $ftp_user\n";
		}
		$contents = ftp_nlist($conn_id, ".");

		$master="";
		foreach($contents as $row){
			if (strpos($row, 'MASTER') !== false) { 
			    $master = $row;
			}
			/*if (strpos($row, 'DELTA') !== false) {
			    $delta = $row;
			}*/
		}

		//echo "<pre>"; print_r($contents); exit;
		$filename = "ftp://".$ftp_user.":".$ftp_pass."@".$ftp_server."/f21_affliate/".$master;//AmazonFeed_MASTER_.date('Y_m_d').".xml";
		
		//$tempHandle = fopen('php://temp', 'r+');
		//$handle = fopen($filename, "r");
		//$contents = fread($handle, filesize($filename));
		$data = file_get_contents($filename);
		//ftp_fget($conn_id, $tempHandle, $filename, FTP_ASCII, 0);
		//$data = $this->ftp_get_contents($conn_id, $filename);
		$myfile = fopen("resource/forever_json/forever_master.xml", "w") or die("Unable to open file!");
		fwrite($myfile, $data);
		fclose($myfile);
		
		// close the connection
	//	ftp_close($conn_id); 

		$myXMLData = file_get_contents('resource/forever_json/forever_master.xml');
		$xml=simplexml_load_string($myXMLData) or die("Error: Cannot create object");
		if(!empty($xml->item)){
			$product_json = '[{';
			$i = 0;	
			foreach($xml->item as $val){ 		
				if($i!=0){
					$product_json = $product_json.'},{';
				}
					$product_json = $product_json.'"product_sku":"'.strip_tags($val->SKU).'",';
					$product_json = $product_json.'"product_category":"'.strip_tags($val->Category).'",';
					$product_json = $product_json.'"product_subcategory":"'.strip_tags($val->SubCategory).'",';						
					$arr =  $val->Title;
					$text_one = str_replace('<![CDATA[', '', $arr);
					$text_two = str_replace(']]>', '', $text_one);
					$product_json = $product_json.'"product_name":"'.strip_tags($text_two).'",';						
					$product_json = $product_json.'"product_url":"'.strip_tags($val->Link).'",';
					$description_arr =  $val->Description;
					$description_one = str_replace('<![CDATA[', '', $description_arr);
					$description_two = str_replace(']]>', '', $description_one);			
					$product_json = $product_json.'"product_description":"'.strip_tags(str_replace('"','',(trim($description_two)))).'",';			
					$product_json = $product_json.'"price":"'.strip_tags($val->Price).'",';
					$product_json = $product_json.'"age_group":"",';
					if(strip_tags(@$val->OtherImageUrl1)){ $otherimageurl1 = strip_tags(@$val->OtherImageUrl1);  }else{$otherimageurl1 = ''; }
					if(strip_tags(@$val->Image)){ $image = strip_tags(@$val->Image);  }else{$image = ''; }						
					$product_json = $product_json.'"product_img":["'.strip_tags($otherimageurl1).'","'.strip_tags($image).'"],';			
					if($val->Availability == 'TRUE'){$availabil=1;} else{$availabil=0;}						
					$product_json = $product_json.'"product_stock":[{"'.strip_tags($val->SizeMap).'":"'.strip_tags($availabil=1).'"}],';
					$product_json = $product_json.'"product_discount":"",';
					$product_json = $product_json.'"discount_start_date":"",';
					$product_json = $product_json.'"discount_end_date":"",';			
					if(strip_tags(@$val->Keyword1)){ $keyword1 = strip_tags(@$val->Keyword1);  }else{$keyword1 = ''; }
					if(strip_tags(@$val->Keyword2)){ $keyword2 = strip_tags(@$val->Keyword2);  }else{$keyword2 = ''; }
					if(strip_tags(@$val->Keyword3)){ $keyword3 = strip_tags(@$val->Keyword3);  }else{$keyword3 = ''; }
					if(strip_tags(@$val->Keyword4)){ $keyword4 = strip_tags(@$val->Keyword4);  }else{$keyword4 = ''; }
					if(strip_tags(@$val->ColorName)){ $colorname = strip_tags(@$val->ColorName);  }else{$colorname = ''; }
					if(strip_tags(@$val->Material)){ $material = strip_tags(@$val->Material);  }else{$material = ''; }
					if(strip_tags(@$val->TargetGender)){ $targetgender = strip_tags(@$val->TargetGender);  }else{$targetgender = ''; }
					$product_json = $product_json.'"product_tags":["'.strip_tags($keyword1).'","'.strip_tags($keyword2).'","'.strip_tags($keyword3).'","'.strip_tags($keyword4).'","'.strip_tags($colorname).'","'.strip_tags($material).'","'.strip_tags($targetgender).'"]';
				$i++;
				if($i == 2000){  $product_json = $product_json.'}]'; break;echo $product_json; exit; }
			}
			//$product_json = $product_json.'}]';
		}else{
			echo "Data not found";
			exit;
		}
		
		$myfile = fopen("resource/forever_json/forever_master.json", "w") or die("Unable to open file!");
		$txt = $product_json;
		if(fwrite($myfile, $txt)){
			$file = 'resource/forever_json/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Done - $master\n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}else{
			$file = 'resource/forever_json/logs.txt';
			// Open the file to get existing content
			$current = file_get_contents($file);
			// Append a new person to the file
			$current .= date("dd/mm/Y H:i:s")."Failed - $master \n";
			// Write the contents back to the file
			file_put_contents($file, $current);
		}
		fclose($myfile);
	}
}

?>