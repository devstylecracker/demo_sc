<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/TT_REST_Controller.php';

class Invite_referrals extends TT_REST_Controller 
{
  function __construct()
	{
		parent::__construct();
		$this->load->model('referal_model');
	}
	
	
	public function get_params_get(){
		
		$conversion = $this->get('conversion');
		$referrer_name = $this->get('referrer_name');
		$referrer_email = $this->get('referrer_email');
		$referee_name = $this->get('referee_name');
		$referee_email = $this->get('referee_email');
		$orderID = $this->get('orderID');
		$purchaseValue = $this->get('purchaseValue');
		$referrer_converts = $this->get('referrer_converts');
		$referrer_customValue = $this->get('referrer_customValue');
		$goal_complete = $this->get('goal_complete');
		
		$data = (object)$this->referal_model->update_referrals($conversion,$referrer_name,$referrer_email,$referee_name,$referee_email,$orderID,$purchaseValue,$referrer_converts,$referrer_customValue,$goal_complete);
	
		if($data !=NULL){
	      	 $this->success_response($data);
		    }else{
	      	   $this->success_response($data);
		}
	}
	
	public function get_user_data_get(){
		$user_id = $this->get('user_id');
        $auth_token = $this->get('auth_token');
		$data['user_data'] = $this->referal_model->getuser_data($user_id);
		$data['min_cart_value'] = MIN_CART_VALUE;
		$data['per_point_value'] = POINT_VAlUE;
		$data['string'] = 'You can redeem <font color="#1BB19D"> &#8377;'.POINT_VAlUE.'</font> per transaction on a minimum purchase of <font color="#1BB19D"> &#8377;'.MIN_CART_VALUE.'</font> ';
		if(!empty($data)){
			$this->success_response($data);
		}else {
			$res = array();
			$this->success_response($data);
		}
	}
	
	public function verify_email_get(){

		$user_id = $this->get('user_id');
		$auth_token = $this->get('auth_token');
		$email_id = $this->get('email');
		$config['protocol'] = 'smtp';
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;
		$config['smtp_host'] = $this->config->item('smtp_host');
		$config['smtp_user'] = $this->config->item('smtp_user');
		$config['smtp_pass'] = $this->config->item('smtp_pass');
		$config['smtp_port'] = $this->config->item('smtp_port');

		//added for confirm email
		$email_en = base64_encode($email_id);
		$link = base_url()."schome/confirm_email/".$email_en."";		

		$this->email->initialize($config);
		$this->email->initialize($config);

		$this->email->from($this->config->item('from_email'), 'Stylecracker');
		if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){              
            $this->email->to($this->config->item('sc_test_emaild')); 
            $this->email->cc($this->config->item('sc_testcc_emaild'));      
        }else{
			$this->email->to($email_id);
			$this->email->cc($this->config->item('sc_test_emaild'));
		}
		$this->email->subject('Verify to earn Referral Amount!');
      $this->email->message("Hi,<br></br> You have successfully signed up with StyleCracker. To redeem your Referral Amount, all you need to do is quickly verify your account by clicking on the link below. You’re just one step away from earning Rs.500. <br><br> ".$link." <br><br>Warm Regards,<br>StyleCracker");

		if($this->email->send())
		{
			$data = array('sent');
			$this->success_response($data);
		}else
		{
			$this->failed_response(1001, "Something went wrong please try again");
		}
	}
	
	public function remind_email_get(){
		
		$user_id = $this->get('user_id');
		$auth_token = $this->get('auth_token');
		$user_email = $this->get('user_email');
		$friend_email = $this->get('friend_email');
		$config['protocol'] = 'smtp';
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;
		$config['smtp_host'] = $this->config->item('smtp_host');
		$config['smtp_user'] = $this->config->item('smtp_user');
		$config['smtp_pass'] = $this->config->item('smtp_pass');
		$config['smtp_port'] = $this->config->item('smtp_port');

		$email_en = base64_encode($friend_email);
		$link = base_url()."schome/confirm_email/".$email_en."";

		$this->email->initialize($config);
		$this->email->initialize($config);

		$this->email->from($this->config->item('from_email'), 'Stylecracker');
		if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){              
                $this->email->to($this->config->item('sc_test_emaild')); 
                $this->email->cc($this->config->item('sc_testcc_emaild'));      
        }else{
			$this->email->to($friend_email);
			$this->email->cc($this->config->item('sc_test_emaild'));
		}
		$this->email->subject('Reminder to verify your Email id');
        $this->email->message("Hi,<br></br> Your friend has sent this reminder link for you both to redeem your referral amount! All you need to do is verify your e-mail id by clicking on the link below. <br><br> ".$link." <br><br>Warm Regards,<br>StyleCracker");

		if($this->email->send())
		{
			$data = array('sent');
			$this->success_response($data);
		}else
		{
			$this->failed_response(1001, "Something went wrong please try again");
		}
	}

}
	
?>