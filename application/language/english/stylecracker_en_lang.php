<?php
$lang['homepage_caption_one'] = 'Looks Handpicked for You'; 
$lang['homepage_caption_second'] = 'Styles Recommended for you'; 
$lang['homepage_caption_third'] = 'Brands picked for you';
$lang['scshop_brand_list'] = 'Scshop';
$lang['homepage_caption_fourth'] = 'Trending Products';

$lang['homepage_filter_all'] = 'All'; 
$lang['homepage_filter_men'] = 'Men'; 
$lang['homepage_filter_women'] = 'Women'; 
$lang['seo_title'] = 'Online Personal Stylists – Your Own 24x7 Fashion Stylist & Wardrobe Consultant | Stylecracker '; 
$lang['seo_desc'] = 'Stylecracker is India’s 1st Online Personal Styling Platform. Our famous fashion stylists advises an individual on new fashion trends and clothing styles according to your body type. Stylists are available for fashion & styling advice round the clock. You can also shop for curated looks directly from Stylecracker.com. Signup for free!'; 
$lang['seo_keyword'] = 'personal stylist, personal stylist online, fashion stylist, online personal stylist, wardrobe consultant, fashion websites'; 
$lang['get_this'] = 'Get this';
$lang['category_caption_one'] = 'Looks';
$lang['category_caption_two'] = 'Trending must have';
$lang['category-list-page'] = 'Categories';

?>