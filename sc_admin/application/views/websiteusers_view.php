<?php //echo"<pre>"; print_r($extra_info);?>
<section class="content-header">
  <h1>Website User</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>/websiteusers">Manage Website Users</a></li>
    <li class="active">Website User</li>
  </ol>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">User Info</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="datatable">
              <tr>
                <th style="width:180px;">ID</th>
                <td><?php echo $user_data[0]['id']; ?></td>
              </tr>
              <tr>
                <th>Username</th>
                <td><?php echo $user_data[0]['user_name']; ?></td>
              </tr>
              <tr>
                <th>First Name</th>
                <td><?php echo $user_data[0]['first_name']; ?></td>
              </tr>

              <tr>
                <th>Last Name</th>
                <td><?php echo $user_data[0]['last_name']; ?></td>
              </tr>
              <tr>
                <th>Email</th>
                <td><?php echo $user_data[0]['email']; ?></td>
              </tr>
              <tr>
                <th>Mobile Number</th>
                <td><?php echo $user_data[0]['contact_no']; ?><?php //echo $user_data[0]['contact_no']; ?></td>
              </tr>
              <tr>
                <th>Role</th>
                <td><?php echo 'Website User'; ?></td>
              </tr>
              <tr>
                <th>Gender</th>
                <td><?php 
                    if($user_data[0]['gender']==2)
                    {
                      echo 'Male';
                    }else
                    {
                      echo 'Female';
                    }
                 ?></td>
              </tr>
              <tr>
                <th>SC Box Mobile Number</th>
                <td><?php echo $sc_box_mobile_number[0]['meta_value']; ?></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">PA Info</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
         <?php 
             $question_ans = array();
             if(!empty($pa_data)){
                foreach($pa_data as $val){
                     $question_ans[$val['question_id']] = $val['answer'];
                }
             } 
          ?>
          <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="datatable">
            <?php 
                if($user_data[0]['gender']==1)
                {
            ?>
              <tr>
                <th style="width:180px;">Body Type</th>
                <td><?php if(isset($question_ans[1])){ echo $question_ans[1]; } ?></td>
              </tr>
              <tr>
                <th>Personal Style</th>
                <td><?php if(isset($question_ans[2])){ echo $question_ans[2]; } ?></td>
              </tr>
              <tr>
                <th>Age</th>
                <td><?php if(isset($question_ans[8])){ echo $question_ans[8]; } ?></td>
              </tr>
              <tr>
                <th>Budget</th>
                <td><?php if(isset($question_ans[7])){ echo $question_ans[7]; } ?></td>
              </tr>
              <tr>
                <th>Size</th>
                <td><?php if(isset($question_ans[9])){ echo $question_ans[9]; } ?></td>
              </tr>
              <tr><td></td><td>&nbsp;</td></tr>
            </table>
          <?php
                }else
                {
          ?>
              <table class="table table-bordered table-striped dataTable" id="datatable">
              <tr>
                <th style="width:180px;">Body Type</th>
                <td><?php if(isset($question_ans[13])){ echo $question_ans[13]; } ?></td>
              </tr>
              <tr>
                <th>Body Height</th>
                <td><?php if(isset($question_ans[14])){ echo $question_ans[14]; } ?></td>
              </tr>
              <tr>
                <th>Work Style</th>
                <td><?php if(isset($question_ans[15])){ echo $question_ans[15]; } ?></td>
              </tr>
              <tr>
                <th>Personal Style</th>
                <td><?php if(isset($question_ans[16])){ echo $question_ans[16]; } ?></td>
              </tr>
              <tr>
                <th>Age</th>
                <td><?php if(isset($question_ans[17])){ echo $question_ans[17]; } ?></td>
              </tr>
              <tr>
                <th>Budget</th>
                <td><?php if(isset($question_ans[18])){ echo $question_ans[18]; } ?></td>
              </tr>
              <tr>
                <th>Style Quotient</th>
                <td><?php if(isset($question_ans[19])){ 
                  echo '<b> Shirts :  </b>'.@$question_ans[19].'<br/>'; 
                  echo '<b> Jeans :  </b>'.@$question_ans[20].'<br/>'; 
                  echo '<b> Trousers :  </b>'.@$question_ans[21].'<br/>'; 
                  echo '<b> T-shirts :  </b>'.@$question_ans[22].'<br/>'; 
                  echo '<b> Jackets & Blazers :  </b>'.@$question_ans[23].'<br/>'; 
                } ?></td>
              </tr>

              <tr><td></td><td>&nbsp;</td></tr>
            </table>

          <?php
                }
          ?>
          </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
    
    <div class="col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Extra Info</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="datatable">
              <tr>
                <th style="width:180px;">First Login</th>
                <!-- <td><?php //echo $extra_info[0]['first_visit']?></td> -->
                <td><?php echo $extra_info[0]['login_time']?></td>
              </tr>
              <tr>
                <th style="width:180px;">Returning Count</th>
                <!-- <td><?php //echo $extra_info[0]['visit_count']?></td> -->
                <td><?php echo $visit_count_user[0]['visit_count']?></td>
              </tr>
              <tr>
                <th style="width:180px;">Last Login</th>
                <td><?php echo $last_login[0]['login_time']; ?></td>
              </tr>
              <tr>
                <th style="width:180px;">Total Look Click</th>
                <td><?php echo $total_look_click_count[0]['count']; ?> <a href="<?php echo base_url(); ?>websiteusers/user_look/look/<?php echo $this->uri->segment(3); ?>" target="_blank"><?php echo "View Details" ?></a><td>
              </tr>
               <tr>
                <th style="width:180px;">Total Product Click</th>
                <td><?php echo $total_product_click_count[0]['count']; ?> <a href="<?php echo base_url(); ?>websiteusers/user_look/product/<?php echo $this->uri->segment(3); ?>" target="_blank"><?php echo "View Details" ?></a></td>
              </tr>
              <tr>
               <th style="width:180px;">Total Fav Look Click</th>
               <td><?php echo  $fav_look_count[0]['count']; ?><a href="<?php echo base_url(); ?>websiteusers/user_look/fav/<?php echo $this->uri->segment(3); ?>" target="_blank"><?php echo "View Details" ?></a></td>
                
              </tr>
            </table>
          </div>
        </div>
      </div>
     </div>




    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Wordrobe</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
				 <ul class="user-wardrobe-images">
				<?php if(!empty($wordrobe)) {
				foreach($wordrobe as $val){
				?>
				<li>
				  <!-- <img src="<?php echo base_url(); ?>assets/wardrobes/thumb/<?php //echo $val['image_name']; ?>" alt=""></img> -->
          <a href="<?php echo base_url(); ?>assets/wardrobes/<?php echo $val['image_name']; ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/wardrobes/thumb/<?php echo $val['image_name']; ?>" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';"></img></a>
				</li>
				<?php }} else{ ?>
				<li style="width:300px; text-align:left;"><?php echo 'Data not found.'; ?></li>
				<?php } ?>
      </ul>
              </div>

        </div><!-- /.box-body -->
        <div>
          <a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>websiteusers"><i class="fa fa-reply"></i> Back</a>
        </div>
      </div><!-- /.box -->
    </div>

  </section>
  <script type="text/Javascript">
$( document ).ready(function() {
	$('.nav-item-user').addClass("active");
  $('.nav-website-user').addClass("active");
});
</script>
