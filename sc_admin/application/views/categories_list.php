<?php 
	if(!empty($cat_data)){
		foreach ($cat_data as $key => $val) {
			if($val['status'] == 0){ $status = '<span class="text-orange">(Hide)</span>'; }else{ $status = ''; }
			?>
			<tr>
			<td><?php echo $val['id']; ?></td>
			<td><input type="checkbox" name="cat_show[]" value="<?php echo $val['id']; ?>" class="checkbox"/></td>
			<td><?php echo $val['category_name'].$status; ?></td>
			<td><?php echo $val['category_slug']; ?></td>
			<td><?php echo $val['count']; ?></td>
			<td style="padding: 2px; display: inline-table;">
				<a href="<?php echo base_url(); ?>categories/categories_edit/<?php echo $val['id']; ?>"><button class="btn btn-primary btn-xs editProd" type="button" ><i class="fa fa-edit"></i>Edit</button></a>
				<button class="btn btn-xs btn-primary btn-delete" type="button" onclick="delete_category(<?php echo $val['id']; ?>);"><i class="fa fa-trash-o"></i>Delete</button>
			</td>
			</tr>
			<?php
		}
	}
?>