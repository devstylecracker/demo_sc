<section class="content-header">
        <h1>Manage Categories</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>">Home</a></li>
            <li class="active">Manage Categories</li>
          </ol>
</section>

<section class="content">
	<div class="row">
	 <div class="col-md-4">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">
				 	<?php if($this->uri->segment(3) > 0){ ?>
					Edit Category
					<?php }else{ ?>
						Add New Category
					<?php } ?>
				</h3>
		</div><!-- /.box-header -->
<div class="box-body">
<?php if($this->uri->segment(3) > 0){ ?>
	<form name="frmcategory" id="frmcategory" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>categories/categories_edit/<?php echo $this->uri->segment(3); ?>">
<?php }else{ ?>
	<form name="frmcategory" id="frmcategory" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>categories">
<?php } ?>
		<div class="form-group">
			<label class="control-label">Name</label>
			<input type="text" placeholder="" name="category_name" id="category_name" value="<?php if(isset($cat_data_data[0]['category_name'])){ echo @$cat_data_data[0]['category_name']; }else{ echo @$_POST['category_name']; } ?>" class="form-control" onblur="genarate_slug();"> <span class="text-red"><?php echo form_error('category_name'); ?></span>
			<p class="help-block">The name is how it appears on your site.</p>
							</div>

							<div class="form-group">
			<label class="control-label">Slug
			</label>
			<input type="text" placeholder="" name="category_slug" id="category_slug" value="<?php if(isset($cat_data_data[0]['category_slug'])){ echo @$cat_data_data[0]['category_slug']; }else{ echo @$_POST['category_slug']; } ?>" class="form-control" >
			<p class="help-block">The "slug" is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.</p>
							</div>

							<div class="form-group">
			<label class="control-label">Parent</label>
			<select name="category_parent" id="category_parent" class="form-control chosen-select" tabindex="7">
				<option value="-1">None</option>
				<?php if(!empty($cat_data)){
					foreach($cat_data as $val){
						echo $val;
					}

					} ?>
			</select>
			<p class="help-block">Totally optional.</p>
			</div>
			<div class="form-group">
			<label class="control-label">Description
			</label>
			<textarea name="category_description" id="category_description" class="form-control" rows="3">
			<?php if(isset($cat_data_data[0]['category_content'])){ echo @$cat_data_data[0]['category_content']; }else{ echo @$_POST['category_description']; } ?>
			</textarea>

							</div>

							
			<?php if($this->uri->segment(3) > 0){ ?>
					<div class="form-group">
						<label class="control-label">Personality</label>
						<select name="bucket[]" id="bucket" class="form-control chosen-select" tabindex="7" multiple="">
							<option value="-1">None</option>
							<?php if(!empty($bucket)){
								foreach($bucket as $val){ 
									if(in_array($val['id'], $bucket_data)){ ?>
									<option value="<?php echo $val['id']; ?>" selected><?php echo $val['body_shape'].' '.$val['style'].' '.$val['work_style'].' '.$val['personal_style']; ?></option>

									<?php }else{
									?>
									<option value="<?php echo $val['id']; ?>"><?php echo $val['body_shape'].' '.$val['style'].' '.$val['work_style'].' '.$val['personal_style']; ?></option>
								<?php } }

								} ?>
						</select>
					<p class="help-block">Assign category to the personality (Totally optional).</p>
					</div>

					<div class="form-group">
						<label class="control-label">Age</label>
						<select name="age[]" id="age" class="form-control chosen-select" tabindex="7" multiple="">
							<option value="-1">None</option>
							<?php if(!empty($age)){
								foreach($age as $val){ 
								if(in_array($val['id'], $age_data)){ ?>
									<option value="<?php echo $val['id']; ?>" selected><?php echo $val['answer']; ?></option>
								<?php }else{ ?>
									<option value="<?php echo $val['id']; ?>"><?php echo $val['answer']; ?></option>
								<?php } }

								} ?>
						</select>
					<p class="help-block">Assign category to the age (Totally optional).</p>
					</div>

					<div class="form-group">
						<label class="control-label">Budget</label>
						<select name="budget[]" id="budget" class="form-control chosen-select" tabindex="7" multiple="" >
							<option value="-1">None</option>
							<?php if(!empty($budget)){
								foreach($budget as $val){ 
									if(in_array($val['id'], $budget_data)){
									?>
									<option value="<?php echo $val['id']; ?>" selected><?php echo $val['answer']; ?></option>
								<?php }else{ ?>
									<option value="<?php echo $val['id']; ?>"><?php echo $val['answer']; ?></option>
								<?php } }

								} ?>
						</select>
					<p class="help-block">Assign category to the budget (Totally optional).</p>
					</div>
			<?php } ?>

			
			<div class="form-group">
				<label class="control-label">Select Size Guide</label>
					<select name="size_guide" id="size_guide" class="form-control chosen-select" tabindex="7">
						<option value="0" <?php if($cat_data_data[0]['size_guide'] == 0) { echo 'selected'; } ?> >Default</option>
						<option value="1" <?php if($cat_data_data[0]['size_guide'] == 1) { echo 'selected'; } ?>>No Size Chart</option>
						<option value="2" <?php if($cat_data_data[0]['size_guide'] == 2) { echo 'selected'; } ?>>Top</option>
						<option value="3" <?php if($cat_data_data[0]['size_guide'] == 3) { echo 'selected'; } ?>>Bottom</option>
						<option value="4" <?php if($cat_data_data[0]['size_guide'] == 4) { echo 'selected'; } ?>>Footwear</option>
						<option value="5" <?php if($cat_data_data[0]['size_guide'] == 5) { echo 'selected'; } ?>>Top & Bottom</option>
						<option value="6" <?php if($cat_data_data[0]['size_guide'] == 6) { echo 'selected'; } ?>>Top Men</option>
						<option value="7" <?php if($cat_data_data[0]['size_guide'] == 7) { echo 'selected'; } ?>>Bottom Men</option>
						<option value="8" <?php if($cat_data_data[0]['size_guide'] == 8) { echo 'selected'; } ?>>Footwear Men</option>
						<option value="9" <?php if($cat_data_data[0]['size_guide'] == 9) { echo 'selected'; } ?>>Top & Bottom Men</option>
					</select>
			</div>

			<div class="form-group">
			<label class="control-label">Order</label>
			<input type="text" placeholder="" name="sort_order" id="sort_order" value="<?php if(isset($cat_data_data[0]['sort_order'])){ echo @$cat_data_data[0]['sort_order']; }else{ echo @$_POST['sort_order']; } ?>" class="form-control" >
			<p class="help-block">(Only numbers allowed) Display on the website on filter page by ascending order</p>
			</div>

			<?php if($this->uri->segment(3) > 0){ ?>

			<div class="form-group">
			<label class="control-label">Complementary Category</label>
			<select name="comp_cat" id="comp_cat" class="form-control chosen-select" tabindex="7">
				<option value="-1">None</option>
				<?php if(!empty($comp_cat)){
					foreach($comp_cat as $val){
						echo $val;
					}

					} ?>
			</select>
			<p class="help-block">Personalisation Category Mapping</p>
			</div>

			<?php } ?>

			<div class="form-group">
			<label class="control-label">Image</label>
			<input type="file" name="category_image" id="category_image">
			<p class="help-block">Only jpg allow (530 X 530)</p>
			<span class="text-red"><?php echo @$error; ?>
			<?php if(isset($cat_data_data[0]['category_image']) && $cat_data_data[0]['category_image']!='') { ?>
				<div style="width:100px;"><img src="../../../assets/images/products_category/<?php echo $cat_data_data[0]['category_image']; ?>"></div>
			<?php } ?>
							</div>

							<div class="form-group">
							<?php if($this->uri->segment(3) > 0){ ?>
			<button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-edit"></i> Edit Category</button>
		<?php }else{ ?>
			<button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Add New Category</button>
		<?php } ?>

							</div>

	</form>

</div>
</div>
</div>
 <div class="col-md-8">
	 <div class="box">
		 <div class="box-header">
			 <h3 class="box-title">
					View Categories
				 </h3>
				 <div class="pull-right frmsearch" style="width:55%">
				 <div class="row">
				
							 <div class="col-md-6">
							  <div class="row" style="display:none;" id="hide_show_container">
					      	<div class="col-md-8">
							 <select name="cat_hide_show" id="cat_hide_show" class="form-control input-sm pull-right">
							 	<option value="1">Show in filter page</option>
							 	<option value="0">Hide from filter page</option>
							 </select>
							 </div> 
							 <div class="col-md-4">
							  <button type="button" name="submit" id="submit" value="Apply" onclick="apply_action();" class="btn btn-sm btn-default">Apply</button>						

							 </div> 
							 </div>
							  </div>					
<div class="col-md-6">
				 <div class="input-group">
						 <input name="search_text" id="search_text" value=""  placeholder="Search" class="form-control input-sm pull-right" required>
							 <div class="input-group-btn">
								 <button type="button" name="submit" id="submit" value="Search" onclick="search_cat();" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
							 </div>
							 </div> 
							  </div> 
					
					 </div>
					 </div>
				
		 </div><!-- /.box-header -->
	<div class="box-body">

	<div class="table-responsive" style="height: 737px;min-block-size: 0.01%;overflow-x: scroll;">
		<table class="table table-bordered table-striped dataTable" id="datatable">
								<thead>
								<tr>
										<th>ID</th>
										<th><input type="checkbox" name="" id="chkall" onClick="toggle(this)"></th>
										<th>Name</th>
										<th>Slug</th>
										<th>Count</th>
										<th width="140">Action</th>

								</tr>
								</thead>
			<tbody id="category_list">
			
			<?php 
				if(!empty($get_cat)){
					foreach ($get_cat as $key => $value) {
						echo $value;
					}
				}

			?>
			</tbody>
		</table>
	</div>
</div>
</div>
</div>
</div>

</section>

<script type="text/javascript">
	$(document).ready(function(e){
		$(".chosen-select").chosen({
		 	max_shown_results : 5
		 });

		$('.checkbox').click(function() {
			var val = [];
		  	$(':checkbox:checked').each(function(i){
	        	val[i] = $(this).val();
	        });
		  	if (val.length != 0) { $('#hide_show_container').show(); }else{ $('#hide_show_container').hide(); } 
		});

	});
	function toggle(source) {
  		checkboxes = document.getElementsByClassName('checkbox');
	  		for(var i=0, n=checkboxes.length;i<n;i++) {
	    		checkboxes[i].checked = source.checked;
	  		}
	  	var val = [];
	  	$(':checkbox:checked').each(function(i){
        	val[i] = $(this).val();
        });
	  	if (val.length != 0) { $('#hide_show_container').show(); }else{ $('#hide_show_container').hide(); } 
	}
	function apply_action(){
		var cat_hide_show = $('#cat_hide_show').val();
		var val = [];
        $(':checkbox:checked').each(function(i){
          val[i] = $(this).val();
        });

        if(cat_hide_show!=''){
        $.ajax({
				url: "<?php echo base_url(); ?>categories/hide_show_cat",
				type: 'POST',
				data: { 'cat_hide_show' : cat_hide_show,'cat':val },
				cache :true,
				async: true,
				success: function(response) {
					location.reload();
				}
			});
    	}else{
    		alert('Select value from dropdown');
    	}
	}
	function genarate_slug(){
		var category_name = $('#category_name').val();
		var slug = category_name.replace(/[^a-zA-Z0-9]/g, "-");
		$('#category_slug').val(slug.toLowerCase());
	}
	function delete_category(id){
		var result = confirm("Want to delete?");
		if (result) {
			$.ajax({
				url: "<?php echo base_url(); ?>categories/check_product_count",
				type: 'POST',
				data: { id : id },
				cache :true,
				async: true,
				success: function(response) {
					/*if(response > 0){
						alert('Already have products in to the category still you want to delete the category');
					}else{*/
						$.ajax({
						url: "<?php echo base_url(); ?>categories/delete_category",
						type: 'POST',
						data: { id : id },
						cache :true,
						async: true,
						success: function(response) {
							window.location="<?php echo base_url(); ?>categories";
						},
						error: function(xhr) {
							alert('Something goes wrong');
							hide_cart_loader();
						}
					});
				/*}*/

				},
				error: function(xhr) {
					alert('Something goes wrong');
					hide_cart_loader();
				}
			});
		}
	}

	function search_cat(){
		var search_text = $('#search_text').val();
		$.ajax({
			url: "<?php echo base_url(); ?>categories/search_category",
			type: 'POST',
			data: { search_text : search_text },
			cache :true,
			async: true,
				success: function(response) {
					$('#category_list').html('');
					$('#category_list').html(response);

					$('.checkbox').click(function() {
						var val = [];
					  	$(':checkbox:checked').each(function(i){
				        	val[i] = $(this).val();
				        });
					  	if (val.length != 0) { $('#hide_show_container').show(); }else{ $('#hide_show_container').hide(); } 
					});
				},
				error: function(xhr) {
					alert('Something goes wrong');
					hide_cart_loader();
			}
		});



	}

</script>