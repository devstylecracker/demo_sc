<?php
$users_menu=$this->data['users_menu'];
//echo "<pre>"; print_r($variationvalue_details);exit;
// print_r($sub_categories);exit;

if(@$variationvalue_details!=NULL)
{
  $page="Edit";
}else
{
  $page="Add";
}
?>
<section class="content-header">
  <h1>Variation Value </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>category">Categories</a></li>
    <li class="active"><?php echo $page; ?> Variation Value</li>
  </ol>
</section>
<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo $page;?> Variation Value</h3>
    </div><!-- /.box-header -->
    <!-- form start -->
    <form role="form" name="frmcategory" id="frmcategory" action="" method="post"  enctype="multipart/form-data">
      <div class="box-body">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="categoryName" class="control-label">Product Category
                <span class="text-red">*</span></label>
                <select class="form-control select2" name="category" id="category" onchange="get_subCategory(this.value,'');">
                  <option value="">Select Category</option>
                  <?php foreach($categories as $cat){ ?>
                    <?php if($cat->is_delete==0){ ?>
                      <option value="<?php echo $cat->id; ?>"
                        <?php if(@$variationvalue_details[0]->cat_id == @$cat->id) echo "selected";?>
                        ><?php echo $cat->name; ?></option>
                        <?php }}?>
                      </select>
                      <span class="text-red"><?php echo form_error('category'); ?></span>
                    </div>

                    <div class="form-group">
                      <label for="subcategory" class="control-label">Sub-Category Name
                        <span class="text-red">*</span></label>

                        <select class="form-control select2" name="subcategory" id="subcategory" onchange="get_variationtype(this.value,'');" >
                          <option value="">Select Subcategory </option>
                        </select>
                        <span class="text-red"><?php echo form_error('subcategory'); ?></span>
                      </div>
                      <div class="form-group">
                        <label for="variationtype" class="control-label">Variation Type
                          <span class="text-red">*</span></label>
                          <select class="form-control select2" name="variationtype" id="variationtype"  >
                            <option value="">Select Variation Type </option>
                          </select>
                          <span class="text-red"><?php echo form_error('variationtype'); ?></span>
                        </div>
                        <div class="form-group">
                          <label for="variationName" class="control-label">Variation Name
                            <span class="text-red">*</span></label>
                            <input type="text" placeholder="Enter Variation Name" name="variationName" id="variationName" value="<?php echo @$variationvalue_details[0]->name;?>"  onblur="assignSlug()" class="form-control"><span class="text-red"><?php echo form_error('variationName'); ?></span>
                          </div>

                          <div class="form-group">
                            <label for="variationSlug" class="control-label">Slug
                              <span class="text-red">*</span></label>
                              <input type="text" placeholder="Enter Variation Slug" name="variationSlug" id="variationSlug" value="<?php echo @$variationvalue_details[0]->slug;?>" <?php //if(@$subcategory_details->slug!="") echo 'disabled'; ?> class="form-control col-md-6"><span class="text-red"><?php echo form_error('variationSlug'); ?></span> </div>

                              <div class="form-group">
                                <label class="control-label">Active</label>
                                  <?php if(isset($variationvalue_details[0]->status) && $variationvalue_details[0]->status==0) { $check_no =  'checked="true"'; }else { $check_ok = 'checked="true"'; } ?>
                                <div>
                                  <label class="radio-inline"> <input type="radio" value="1" name="active" checked="true" <?php echo @$check_ok; ?> > Yes</label>
                                  <label class="radio-inline"> <input type="radio" value="0" name="active" <?php echo @$check_no; ?> > No</label>
                                </div>
                              </div>


                 <div class="form-group">

                      <label class="control-label">Image</label>

                      <div>

                        <input type="file" name="category_img" id="category_img" >

                      </div>


                      <?php 
                        if(@$variationvalue_details!=NULL){
                          if($variationvalue_details[0]->variation_value > 0){
                            $images = scandir('../assets/images/products_category/variation_values/'.$variationvalue_details[0]->variation_value,true);
                            if(!empty($images)){
                              echo '<img src="'.'../../../assets/images/products_category/variation_values/'.$variationvalue_details[0]->variation_value.'/'.$images[0].'" style="width:50%;">';
                            }
                          }
                        }
                      ?>
                    </div>

                            </div>
                              </div>
                          </div><!-- /.box-body -->

                          <div class="box-footer">
                            <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Submit</button>
                            <a href="<?php echo base_url(); ?>category/variationvalue"><button type="button" class="btn btn-primary  btn-sm btn-rad"><i class="fa fa-reply"></i> Back</button></a>
                            <button class="btn btn-default  btn-sm" type="reset"><i class="fa fa-close"></i> Cancel</button>
                          </div>
                        </form>
                      </div><!-- /.box -->
                    </div>
                  </div>
                </section>
                <script type="text/javascript">

                function get_subCategory(cat_id,selected_id)
                {
                  //alert(cat_id);
                  $.ajax({
                    type:"POST",
                    url:"<?php echo base_url();?>category/get_sub_categories",
                    data :{ cat_id:cat_id,selected_id:selected_id},
                    datatype : 'html',
                    success: function(data){
                      $('#subcategory').html(data);
                    }
                  });
                }

                function get_variationtype(sub_cat_id,selected_id)
                {
                  $.ajax({
                    type:"POST",
                    url:"<?php echo base_url();?>category/get_variation_type_byId",
                    data :{ sub_cat_id:sub_cat_id,selected_id:selected_id},
                    datatype : 'html',
                    success: function(data){
                      $('#variationtype').html(data);
                    }
                  });
                }

                 function assignSlug() {
                  var x = document.getElementById("variationName");
                  var slug = x.value.toLowerCase();
                  var newslug = slug.replace(/ /g,"-");
                  $("#variationSlug").val(newslug);

                 }

                $(document).ready(function(){

                  <?php
                  if($page=="Edit")
                  {
                    ?>

                    get_subCategory('<?php echo @$variationvalue_details[0]->cat_id; ?>','<?php echo @$variationvalue_details[0]->sub_cat_id; ?>');

                    get_variationtype('<?php echo @$variationvalue_details[0]->sub_cat_id; ?>','<?php echo @$variationvalue_details[0]->vart_id; ?>');

                    <?php
                  }
                  ?>

                  $('.nav-item-category').addClass("active");
                  $('.nav-manage-var-val').addClass("active");

                });

                </script>
