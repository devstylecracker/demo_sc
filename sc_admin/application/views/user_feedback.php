<style type="text/css">
	th{
		font-size: 11px;
	}

</style>
<?php $users_menu=$this->data['users_menu'];
	  $total_rows = $this->data['total_rows'];
	  //echo "<pre>"; print_r($categories);exit;
?>
<section class="content-header">
        <h1>View Feedback </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>
            <li><a href="<?php echo base_url(); ?>feedback">Feedback</a></li>
            <li class="active">View Feedback</li>
          </ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
      	<h3 class="box-title">List of Feedback <small class="label label-info"><?php echo $total_rows; ?></small></h3>
    	
		</div>
		<div class="box-body">
				 <?php echo $this->session->flashdata('feedback'); ?>

  <form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>Manage_orders" method="post">
  <input type="hidden"  name="is_download" id="is_download" value="0" />
    <div class="row">
      <div class="col-md-2">
        <?php $search_by = set_value('search_by')=='' ? $search_by : set_value('search_by'); ?>
        <select name="search_by" id="search_by" class="form-control input-sm" >
          <option value="0">Search by</option>
          <option value="1" <?php echo $search_by==1 ? 'selected' : ''; ?> >Order ID</option>
          <option value="9" <?php echo $search_by==9 ? 'selected' : ''; ?> >Customer Order ID</option>
          <option value="2" <?php echo $search_by==2 ? 'selected' : ''; ?> >Customer Name</option>
         <!--  <option value="3" <?php echo $search_by==3 ? 'selected' : ''; ?> >Company Name</option> -->
          <option value="4" <?php echo $search_by==4 ? 'selected' : ''; ?> >Date</option>
          <option value="5" <?php echo $search_by==5 ? 'selected' : ''; ?> >Status</option>
          <option value="6" <?php echo $search_by==6 ? 'selected' : ''; ?> >Order Unique No.</option>
          <option value="8" <?php echo $search_by==8 ? 'selected' : ''; ?> >Date Range</option>
          <option value="10" <?php echo $search_by==10 ? 'selected' : ''; ?> >Box Attributes</option>
        </select>
      </div>
      <div class="col-md-3">
        <div class="input-group">
         <div id ="filter" style="display: block;" >
          <input type="text" name="table_search" value="<?php echo set_value('table_search')=='' && set_value('table_search')==0 ? $table_search : set_value('table_search'); ?>" id="table_search" class="form-control input-sm pull-right" placeholder="Search"  required=""/>
         </div>
        <div id ="daterange" style="display: none;">
            <div class="row">
              <div class="col-md-5">
                <div class="form-group">
                  <input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm" id="date_from" value="<?php echo set_value('date_from'); ?>">
                </div>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="<?php echo set_value('date_to'); ?>">
              </div>
             </div>
            </div>
        </div>
        <div id ="box_filter" style="display: none;" >      
            <select name="table_search_box" id="table_search_box" class="form-control input-sm pull-right" >
                <option value="" >Filter by</option>
                <option value="gift" <?php echo $table_search=='gift' ? 'selected' : ''; ?> >Gift Orders</option>
                <option value="curated" <?php echo $table_search=='curated' ? 'selected' : ''; ?> >Curated Orders</option>
            </select>
         </div>
        <div id ="status_filter" style="display: none;" >      
            <select name="table_search_status" id="table_search_status" class="form-control input-sm pull-right" >
                <option value="" >Filter by</option>
                <option value="processing" <?php echo $table_search=='processing' ? 'selected' : ''; ?> >Processing</option>
                <option value="confirmed" <?php echo $table_search=='confirmed' ? 'selected' : ''; ?> >Confirmed</option>
                <option value="dispatched" <?php echo $table_search=='dispatched' ? 'selected' : ''; ?> >Dispatched</option>
                <option value="delivered" <?php echo $table_search=='delivered' ? 'selected' : ''; ?> >Delivered</option>
                <option value="cancelled" <?php echo $table_search=='cancelled' ? 'selected' : ''; ?> >Cancelled</option>
                <!-- <option value="return" <?php echo $table_search=='return' ? 'selected' : ''; ?> >Return</option> -->
                <option value="fake" <?php echo $table_search=='fake' ? 'selected' : ''; ?> >Fake</option>
            </select>
         </div>
        <div class="input-group-btn">
          <button class="btn btn-sm btn-default" type="submit" onclick="submitForm();" ><i class="fa fa-search"></i></button>
        </div>
      </div>
      </div>
	  
      <div class="col-md-1">
        <a href="<?php echo base_url(); ?>Manage_orders" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
      </div>
	   
      <div class="col-md-4">
       <?php //if($this->session->userdata('role_id') != 6){ ?>
       
              <?php //} ?>
			 
			  <button title="User Extracted Data" id="download_excel2" class="btn btn-primary btn-sm"  onclick="download_stylist_extracted_data();"><i class="fa fa-download"></i> Feedback</button>		
        <div class="pull-right" style="padding-top: 10px;">		 
          <?php echo $this->pagination->create_links(); ?>
        </div>
      </div>
    </div>
<div></div>
</form>

						<div class="table-responsive">
			          <table class="table table-bordered table-striped dataTable" id="datatable">
				        <thead><tr>
				          <th>Id</th>
				          <th>Name</th>
				          <th>Email</th>
				          <th>Mobile</th>
				          <th>StyleCracker Box Experience from start to finish?</th>
				          <th>Stylist communication skills</th>
				          <th>Stylists ability to understand your likes and dislikes</th>
				          <th>Timeliness of box delivery</th>
				          <th>Packaging of the delivered box</th>
				          <th>Quality of products</th>
				          <th>Uniqueness of products</th>
				          <th>Value for money</th>
				          <th>Communication with StyleCracker team in case of any queries relating to order status and delivery</th>
				          <th>Return process (if applicable)</th>
				          <th>Refund process (if applicable)</th>
				          <th>Would you recommend the SC Box to your friends and family?</th>
				          <th>Did the stylist go out of the way to make your experience a little extra pleasurable?</th>
				          <th>Is there anything we could do in the entire process to further enhance your experience?</th>
				          <th>Any other feedback</th>
				          <th>Would you order an SC Box again?</th>
				        </tr>
				        </thead>
						<tbody>
				        
						</tbody>
					</table>
			 <?php echo $this->pagination->create_links(); ?>

			</div>
		</div>
	</div>

</section>
<style>
.table{width: 100%; font-size: 12px; border-collapse: collapse;}
.table > tbody .tr-collapse td{padding: 0;}
.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td{border: 1px solid #d2d6de; }

.table > caption + thead > tr:first-child > td, .table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table > thead:first-child > tr:first-child > th{border: 1px solid #d2d6de; }

.table.table-orders th{vertical-align: top; }

.tr-collapse .row{margin: 0; padding: 10px; }
.tr-collapse label{font-weight: normal;}
.modal-header{background: #000; color:#fff; height: auto; padding: 8px 15px;}
.modal-header h4{font-size: 16px;margin: 0; float: left;}
.order-view-popup dd{margin-bottom: 10px;}
.order-view-product-list .img-wrp{
    background: #fff none repeat scroll 0 0;
    border: 1px solid #ececec;
    height: 150px;
    padding: 10px;
    text-align: center;
    margin-bottom: 5px;
}
.order-view-popup .close{color:#fff; opacity:1;}
.order-view-product-list .img-wrp img{
    height: auto;
    max-height: 100%;
    max-width: 100%;
    width: auto;
}
.order-view-popup .name{line-height: 1.3; margin-bottom: 5px;}
select.form-control{min-width: 94px; padding: 5px;}

.table-tr-collapse .box-quick-view .img-wrp{width:50px; height:50px; border:1px solid #eee;}
.table-tr-collapse .box-quick-view .img-wrp img{width:auto; height:auto; max-width:50px; max-height:50px;}
.table-tr-collapse .box-quick-view .order-address-box{bottom: -8px; position: absolute; right: 17px; text-align: right; }
.table-tr-collapse .seller-amount-box{text-align: right; padding-right: 20px; background: #ECF0F5;}
.table-tr-collapse .seller-amount-box .price{width:70px; display: inline-block;}

.table-tr-collapse tr.tr-quick-view{display: none;}
.table-tr-collapse tr.tr-quick-view .box-quick-view{display: block;}
.table-bordered { border: 1px solid #ddd; }
</style>



<script type="text/Javascript">
$(function () {
      $("#date_from").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true});
      $("#date_to").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true });
	  // $('#date').datepicker({format: "yyyy-mm-dd"});
      $(".nav-tabs a").click(function(){
        $(this).tab('show');
      });
      $('.nav-tabs a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
      });
    });

$( document ).ready(function() {
$("body").addClass('sidebar-collapse');


 $('.nav-item-order').addClass("active");
  $('.nav-manage-orders').addClass("active");
});

 $("#search_by").change(function(){

    var filterType = $(this).val();
    if(filterType == 8)
    {
        $("#filter").hide();
        $("#daterange").show();
        $("#table_search").removeAttr('required');
        $("#date_from").attr('required','');
        $("#box_filter").hide();
        $("#status_filter").hide();
    }else if(filterType == 10)
    {
      $("#status_filter").hide();
      $("#filter").hide();     
      $("#box_filter").show();
      $("#daterange").hide();

    }else if(filterType == 5)
    {
      $("#filter").hide(); 
      $("#status_filter").show();
      $("#box_filter").hide();
      $("#daterange").hide();
    }else
    {
      $("#filter").show();
      $("#daterange").hide();
      $("#box_filter").hide();
      $("#status_filter").hide();
    }
    if(filterType == 6){
        $(".msg-days-lapsed").show();
    }else
    {
        $(".msg-days-lapsed").hide();
    }

  });


</script>

