<?php
  $users_menu=$this->data['users_menu'];
  if(@$subcategory_details!=NULL)
  {
    $page="Edit";
  }else
  {
    $page="Add";
  }
?>
<section class="content-header">
        <h1>Sub-Category </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>
            <li><a href="<?php echo base_url(); ?>category/subcategory">Sub-Categories</a></li>
            <li class="active"><?php echo $page;?> Sub-Category</li>
          </ol>
</section>

<section class="content">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php echo $page;?> Sub-Category</h3>
                </div><!-- /.box-header -->

                <form role="form" name="frmcategory" id="frmcategory" action="" method="post" enctype="multipart/form-data">
                  <div class="box-body">
                    <div class="row">
              <div class="col-md-4">
                    <div class="form-group">
                      <label for="categoryName" class="control-label">Product Category
                      <span class="text-red">*</span></label>
                      <span class="text-red"><?php echo form_error('categoryName'); ?></span>
                        <select class="form-control select2" name="category">
                       <option value="">Select Category</option>
                        <?php foreach($categories as $cat){ ?>
                        <?php if($cat->is_delete==0){ ?>
                        <option value="<?php echo $cat->id; ?>"
                          <?php if(@$subcategory_details->product_cat_id == @$cat->id) echo "selected";?>
                          ><?php echo $cat->name; ?></option>
                        <?php }}?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="subcategoryName" class="control-label">Sub-Category Name
                      <span class="text-red">*</span></label>
                      <input type="text" placeholder="Enter Sub-Category Name" name="subcategoryName" id="subcategoryName" value="<?php echo @$subcategory_details->name;?>" onblur="assignSlug()"  class="form-control"><span class="text-red"><?php echo form_error('subcategoryName'); ?></span>
                    </div>
                    <div class="form-group">
                      <label for="subcategorySlug" class="control-label">Slug
						         <span class="text-red">*</span></label>
                      <input type="text" placeholder="Enter Category Slug" name="subcategorySlug" id="subcategorySlug" value="<?php echo @$subcategory_details->slug;?>" <?php //if(@$subcategory_details->slug!="") echo 'disabled'; ?> class="form-control"><span class="text-red"><?php echo form_error('subcategorySlug'); ?></span>
                    </div>

                    <div class="form-group">
                      <label class="control-label">Active</label>
                         <?php if(isset($subcategory_details->status) && $subcategory_details->status==0) { $check_no =  'checked="true"'; }else { $check_ok = 'checked="true"'; } ?>
                      <div>
                        <label class="radio-inline"> <input type="radio" value="1" name="active" <?php echo @$check_ok; ?> > Yes</label>
                        <label class="radio-inline"> <input type="radio" value="0" name="active" <?php echo @$check_no; ?> > No</label>
                      </div>
                    </div>

                      <div class="form-group">

                      <label class="control-label">Image</label>

                      <div>

                        <input type="file" name="category_img" id="category_img" >

                      </div>


                      <?php 
                        if(@$subcategory_details!=NULL){
                          if($subcategory_details->id > 0){
                            $images = scandir('../assets/images/products_category/sub_category/'.$subcategory_details->id,true);
                            if(!empty($images)){
                              echo '<img src="'.'../../../assets/images/products_category/sub_category/'.$subcategory_details->id.'/'.$images[0].'" style="width:50%;">';
                            }
                          }
                        }
                      ?>
                    </div>

                    </div>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i>  Submit</button>
                    <a href="<?php echo base_url(); ?>category/subcategory"><button type="button" class="btn btn-primary  btn-sm btn-rad"><i class="fa fa-reply"></i> Back</button></a>
                    <button class="btn btn-default btn-sm" type="reset"><i class="fa fa-close"></i> Cancel</button>
                  </div>
                </form>
              </div><!-- /.box -->
            </div>
          </div>
</section>
<script type="text/Javascript">
$( document ).ready(function() {
	$('.nav-item-category').addClass("active");
  $('.nav-manage-sub-cat').addClass("active");
});

function assignSlug() {

    var x = document.getElementById("subcategoryName");
    var slug = x.value.toLowerCase();
    var newslug = slug.replace(/ /g,"-");
    $("#subcategorySlug").val(newslug);

}
</script>
