<?php 
$users_menu=$this->data['users_menu'];
//print_r($signup);
//print_r($dashboard);exit;
//echo "Auclick==".$auclick;exit;
?>
<script src="<?php echo base_url(); ?>assets/js/dbreport.js"></script>

<section class="content-header">
  <h1>Users</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Dashboard</a></li>
  </ol>
</section>

<section class="content">
  <form name="myform" id="myform" class="frm-search" method="post">
    <div class="row">
      <div class="col-md-2">
        <div class="input-group">
          <input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm" id="date_from" value="<?php echo set_value('date_from'); ?>">
        </div>
      </div>
      <div class="col-md-2">
        <div class="input-group">
          <input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="<?php echo set_value('date_to'); ?>">
          <div class="input-group-btn">
            <button type="submit" class="btn btn-sm btn-default" id="submit"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </div>
      <div class="col-md-2">
        <a class="btn btn-default btn-sm" href="<?php echo base_url(); ?>home"><i class="fa fa-refresh"></i> Reset</a>
      </div>
    </div>
  </form>

  <br/>
  <div class="row">
    <?php if($this->session->userdata('role_id')!=6)
    {
      ?>
      <div class="col-md-4">
        <div class="box box-report">
          <div class="box-header with-border">
            <h3 class="box-title">User CPC-CPA <small>(default current day)</small></h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <a href="<?php echo base_url(); ?>reports/top_5" target="_blank"><div class="chart-responsive">
                  <canvas id="pieChart" height="150"></canvas>
                </div></a>
              </div>

            </div>
          </div>
          <div class="box-footer no-padding">
            <ul class="nav-report">
              <li class="title">Authenticated</li>
              <li>
                <i class="fa fa-circle text-light-blue"></i>
                CPC - <a href="<?php echo base_url(); ?>reports/top_5cpa/Authenticated_CPC/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank">
                  <span class="label"><?php echo $dashboard['authcpccount']; ?> </a>
                    <span class="pull-right">
                      Value - <span class="label"><i class="fa fa-inr"></i> <?php echo $dashboard['authcpcvalue']; ?></span></span>
                    </li>

                    <li>
                      <i class="fa fa-circle text-green"></i>
                      CPA - <a href="<?php echo base_url(); ?>reports/top_5cpa/Authenticated_CPA/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank">
                        <span class="label"><?php echo $dashboard['authcpaCount']; ?></span></a>
                        <span class="pull-right">
                          Value - <span class="label"><i class="fa fa-inr"></i> <?php echo $dashboard['authcpavalue']; ?></span></span>
                        </li>
                        <li class="title">Unauthenticated</li>
                        <li><i class="fa fa-circle text-light-blue"></i>
                          CPC - <a href="<?php echo base_url(); ?>reports/top_5cpa/Unauthenticated_CPC/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank">
                            <span class="label"><?php echo $dashboard['unauthcpccount']; ?></span></a>
                            <span class="pull-right">Value - <span class="label"><i class="fa fa-inr"></i> <?php echo $dashboard['unauthcpcvalue']; ?></span></span></li>

                            <li><i class="fa fa-circle text-green"></i>
                              CPA - <a href="<?php echo base_url(); ?>reports/top_5cpa/Unauthenticated_CPA/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank">
                                <span class="label"><?php echo $dashboard['unauthcpaCount']; ?></span></a>
                                <span class="pull-right">Value - <span class="label"><i class="fa fa-inr"></i> <?php echo $dashboard['unauthcpavalue']; ?></span></span></li>
                                <li class="title">Total</li>
                                <li>
                                  <i class="fa fa-circle text-light-blue"></i>
                                   CPC - <span class="label"><?php echo $dashboard['totalcpc']; ?> </span>
                                  <span class="pull-right">Value -  <span class="label"><i class="fa fa-inr"></i> <?php echo $dashboard['totalcpcval'];?></span></span>
                                </li>
                                <li>
                                  <i class="fa fa-circle text-green"></i>
                                  CPA - <span class="label"><?php echo $dashboard['totalcpa']; ?></span>
                                  <span class="pull-right">Value -  <span class="label"> <i class="fa fa-inr"></i> <?php echo $dashboard['totalcpaval']; ?></span></span>
                                </li>
                              </ul>
                            </div>

                          </div>
                        </div>

                        <div class="col-md-4">
                          <div class="box box-report">
                            <div class="box-header with-border">
                              <h3 class="box-title">User Authenticated-Unauthenticated Clicks   <small>(default current day)</small> </h3>
                              <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                              </div>
                            </div>
                            <div class="box-body">
                              <a href="<?php echo base_url(); ?>websiteusers" target="_blank"> <div class="chart-responsive">
                                <canvas id="pieChart1" height="150"></canvas>
                              </div></a>
                            </div>
                            <div class="box-footer no-padding">
                              <ul class="nav-report" style="padding-bottom:168px;">
                                <li>
                                  <i class="fa fa-circle text-light-blue"></i>
                                  Authenticated
                                  <a href="<?php echo base_url(); ?>reports/user_auth_unath_click/Authenticated/<?php echo set_value('date_from')!='' ?set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!='' ? set_value('date_to') : '0'; ?>" target="_blank"><span class="label pull-right"><?php echo $dashboard['authCount']; ?>
                                  </span></a>
                                </li>
                                <li><i class="fa fa-circle text-green"></i> Unauthenticated
                                  <a href="<?php echo base_url(); ?>reports/user_auth_unath_click/UnAuthenticated/<?php echo set_value('date_from')!='' ?set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!='' ? set_value('date_to') : '0'; ?>" target="_blank"> <span class="label pull-right"><?php echo $dashboard['unauthCount']; ?>
                                  </span></a></li>
                                  <li><b>Total Clicks  </b><span class="label pull-right"><?php echo $dashboard['authCount']+$dashboard['unauthCount'];?></span>
                                  </ul>

                                </div>
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="box box-report">

                                <div class="box-header with-border">
                                  <h3 class="box-title">User Signups </h3>
                                  <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                  </div>
                                </div>
                                <div class="box-body">
                                  <div class="row">
                                    <div class="col-md-12">
                                      <a href="<?php echo base_url(); ?>websiteusers" target="_blank"><div class="chart-responsive">
                                        <canvas id="pieChart2" height="150"></canvas>
                                      </div></a>
                                    </div>
                                  </div>
                                </div>
                                <div class="box-footer no-padding">
                                  <ul class="nav-report" style="padding-bottom:122px;">
                                    <li><i class="fa fa-circle text-red111 text-light-blue"></i> Website
                                      <a href="<?php echo base_url();?>reports/user_details/website/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class=" pull-right">
                                      <span class="label"><?php echo $user_info['website']; ?></span>
                                      </a>
                                    </li>

                                    <li><i class="fa fa-circle text-green"></i> Facebook
                                      <a href="<?php echo base_url();?>reports/user_details/facebook/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class=" pull-right">
                                      <span class="label"><?php echo $user_info['facebook']; ?></span>
                                      </a></li>


                                      <li><i class="fa fa-circle text-yellow"></i> Google
                                        <a href="<?php echo base_url();?>reports/user_details/google/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class=" pull-right">
                                        <span class="label"><?php echo $user_info['google']; ?></span>
                                        </a></li>


                                        <li><i class="fa fa-circle text-aqua"></i> Mobile <a href="<?php echo base_url();?>reports/user_details/mobile/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="pull-right"> <span class="label"><?php echo $user_info['mobile']; ?></span>
                                        </a></li>

                                       <li><i class="fa fa-circle text-red"></i> Mobile Facebook  <a href="<?php echo base_url();?>reports/user_details/mobile_facebook/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="pull-right">
                                       <span class="label"><?php echo $user_info['mobile_facebook']; ?></span>
                                       </a></li>


                                        <!--<li><i class="fa fa-circle text-purple"></i> Mobile Google <a href="<?php echo base_url();?>reports/user_details/mobile_google/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="pull-right">
                                        <span class="label"><?php echo $user_info['mobile_google']; ?></span>
                                        </a></li> -->


                                        <li><b>Total Users </b><span class="pull-right label"><?php echo $user_info['website']+$user_info['facebook']+$user_info['google']+$user_info['mobile']+$user_info['mobile_facebook']+$user_info['mobile_google'];?></span></li>


                                      </ul>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-md-12">
                                  <div class="box box-report">
                                    <div class="box-header">
                                      <h3 class="box-title">Brands </h3>
                                      <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <!--  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                                      </div>
                                    </div>
                                    <div class="box-body">
                                      <div class="table-responsive">
                                        <table class="table table-striped no-margin table-report">
                                          <thead>
                                          <tr><th>
                                            Brands
                                          </th>
                                          <th>
                                            Count
                                          </th>
                                          <th>
                                            Price
                                          </th>
                                        </tr>
                                      </thead>
                                        <tbody>
                                          <?php //if(!empty($most_returning_users)) { foreach($most_returning_users as $val){ ?>

                                            <tr>
                                              <td>Brands Onboarded </td>
                                              <td>
                                                <a href="<?php echo base_url(); ?>reports/brand_product_info/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="label" ><?php echo $dashboard['ObBrandCount']; ?></a>
                                              </td>
                                              <td>- </td>
                                            </tr>
                                            <tr>
                                              <td>
                                                Onboarded Brand Product Uploaded
                                              </td>
                                              <td>

                                                <a href="<?php echo base_url(); ?>reports/onboarded_brand_product_uploaded/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="label">
                                                  <!-- <a href="<?php echo base_url(); ?>reports/brand_product_info/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="badge"> -->
                                                  <?php echo $dashboard['ObProductCount']; ?>
                                                </a>
                                              </td>
                                              <td>
                                                <span class="label"><i class="fa fa-inr"></i> <?php echo $dashboard['ObProductPrice']; ?></span>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>Onboarded Brand Product Used</td>
                                              <td>
                                                <a href="<?php echo base_url(); ?>reports/onboarded_brand_product_used/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="label">
                                                  <?php echo $dashboard['ObProductUsedCount']; ?>
                                                </a>
                                              </td>
                                              <td>
                                                <span class="label"><i class="fa fa-inr"></i> <?php echo $dashboard['obPrdUsedPrice']; ?></span>

                                              </td>
                                            </tr>
                                            <tr>
                                              <td> Unpaid Brands</td>
                                              <td>
                                                <a href="<?php echo base_url(); ?>reports/un_paid_brand_count/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="label">
                                                  <?php echo $dashboard['UnpaidBrandCount']; ?>
                                                </a>
                                              </td>
                                              <td>-
                                              </td>
                                            </tr>
                                            <tr>
                                              <td> Unpaid Brands Products</td>
                                              <td>
                                                <a href="<?php echo base_url(); ?>reports/unpaid_brand_prd_count/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="label">
                                                  <?php echo $dashboard['UnpaidBrandPrdCount']; ?>
                                                </a>
                                              </td>
                                              <td>
                                                -
                                              </td>
                                            </tr>
                                            <tr>
                                              <td> Unpaid Brands Products Value</td>
                                              <td>-</td>
                                              <td> <span class="label"><i class="fa fa-inr"></i> <?php echo $dashboard['UnpaidBrandPrdPrice']; ?><span>
                                              </td>
                                            </tr>

                                            <?php //} } ?>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>

                                </div>
                          <?php } ?>
  </div>
                    <div class="row">
                        <?php if($this->session->userdata('role_id')!=6)
                        {
                          ?>

                          <div class="col-md-6">
                            <div class="info-box">
                              <span class="info-box-icon bg-green" ><i class="fa fa-thumbs-up"></i></span>
                              <div class="info-box-content">
                                <span class="info-box-text">Top 5</span>
                                <span class="info-box-text">(CPC | CPA | Brands Clicks | Looks Clicks | Buyers)</span>
                                <span class="info-box-number"></span>
                                <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>reports/top_5">
                                  More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                              </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="info-box">
                              <span class="info-box-icon bg-green" ><i class="fa fa-fw fa-users"></i></span>
                              <div class="info-box-content">
                                <span class="info-box-text">Users</span>
                                <span class="info-box-number"><?php echo $signup_data['current_date']; ?></span>
                                <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>reports/user_info">
                                  More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                              </div>
                            </div>
                          </div>

                          <?php if($this->session->userdata('role_id')!=6 )
                          {
                            ?>
                            <div class="col-md-6">
                              <div class="info-box">
                                <span class="info-box-icon bg-green"><i class="fa fa-bold"></i></span>
                                <div class="info-box-content">
                                  <span class="info-box-text">Brand Data</span>
                                  <span class="info-box-number"><?php echo $brand_data; ?></span>
                                  <a href="<?php echo base_url(); ?>reports/brand_product_info/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" >
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                  </a>

                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="info-box">
                                <span class="info-box-icon bg-green"><i class="fa fa-photo"></i></span>
                                <div class="info-box-content">
                                  <span class="info-box-text">Look Broadcasted: Today</span>
                                  <span class="info-box-number"><?php echo $look['current_look_broadcast']; ?></span>
                                  <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>reports/look_info">
                                    Bucketwise Details <i class="fa fa-arrow-circle-right"></i></a> |
                                    <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>reports/look_details2">
                                      Look Details <i class="fa fa-arrow-circle-right"></i></a>

                                    </div>
                                  </div>
                                </div>



                            <div class="col-md-6">
                              <div class="info-box">
                                <span class="info-box-icon bg-green"><i class="fa fa-pie-chart"></i></span>
                                  <div class="info-box-content">
                                    <span class="info-box-text">Daily Report</span>
                                    <!--<span class="info-box-number"><?php //echo $look['current_look_broadcast']; ?></span>--> 
                                      <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>Dailyreports/current_report">
                                          Report <i class="fa fa-arrow-circle-right"></i></a> <!-- | -->
                                      <!-- <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>reports/look_details2">
                                          Look Details <i class="fa fa-arrow-circle-right"></i></a> -->
                                  </div>
                              </div>
                            </div>

                                 <div class="col-md-6">
                              <div class="info-box">
                                <span class="info-box-icon bg-green"><i class="fa fa-photo"></i></span>
                                  <div class="info-box-content">
                                    <span class="info-box-text">Universal Cart</span> 
                                      <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>Ucartreports">
                                          Report <i class="fa fa-arrow-circle-right"></i></a> <!-- | -->
                                      <!-- <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>reports/look_details2">
                                          Look Details <i class="fa fa-arrow-circle-right"></i></a> -->
                                  </div>
                              </div>
                            </div>
                            
							<div class="col-md-6">
                              <div class="info-box">
                                <span class="info-box-icon bg-green"><i class="fa fa-photo"></i></span>
                                  <div class="info-box-content">
                                    <span class="info-box-text">User Data</span> 
                                      <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>All_report">
                                          Report <i class="fa fa-arrow-circle-right"></i></a> <!-- | -->
                                      <!-- <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>reports/look_details2">
                                          Look Details <i class="fa fa-arrow-circle-right"></i></a> -->
                                  </div>
                              </div>
                            </div>
							
                  <div class="col-md-6">
                              <div class="info-box">
                                <span class="info-box-icon bg-green"><i class="fa fa-photo"></i></span>
                                  <div class="info-box-content">
                                    <span class="info-box-text">Google Shopping Ads</span> 
                                      <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>All_report/google_ads">
                                          Download Report <i class="fa fa-download"></i></a>
                                  </div>
                              </div>
                            </div>

                             <div class="col-md-6">
                              <div class="info-box">
                                <span class="info-box-icon bg-green"><i class="fa fa-photo"></i></span>
                                  <div class="info-box-content">
                                    <span class="info-box-text">Facebook Shopping Ads Data</span> 
                                      <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>All_report/facebook_ads">
                                          Download Report <i class="fa fa-download"></i></a>
                                  </div>
                              </div>
                            </div>
                            
                         <?php } ?>
                      </div>
                   <?php } ?>
                </section>

                            <!-- ChartJS 1.0.1 -->
                            <script src="<?php echo base_url(); ?>assets/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
                            <script type="text/javascript">
                            $(function () {
                              $("#date_from").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true});
                              $("#date_to").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true });
                              users_click_info();
                              users_click_au();
                              users_signup_info();
                              $(".nav-tabs a").click(function(){
                                $(this).tab('show');
                              });
                              $('.nav-tabs a').on('shown.bs.tab', function(event){
                                var x = $(event.target).text();         // active tab
                                var y = $(event.relatedTarget).text();  // previous tab
                                $(".act span").text(x);
                                $(".prev span").text(y);
                              });
                            });
                            
                            function users_click_info(){
                              //-------------
                              //- PIE CHART -
                              //-------------
                              // Get context with jQuery - using jQuery's .get() method.
                              $.ajax({
                                url: '<?php echo base_url(); ?>reports/get_users_info',
                                type: 'post',
                                data: { 'type' : 'get_click_data' },
                                success: function(data, status) {
                                  var pieChartCanvas = $("#pieChart").get(0).getContext("2d");

                                  var pieChart = new Chart(pieChartCanvas);
                                  var PieData = <?php echo $click; ?>;
                                  var pieOptions = {
                                    //Boolean - Whether we should show a stroke on each segment
                                    segmentShowStroke: true,
                                    //String - The colour of each segment stroke
                                    segmentStrokeColor: "#fff",
                                    //Number - The width of each segment stroke
                                    segmentStrokeWidth: 0.0001,
                                    //Number - The percentage of the chart that we cut out of the middle
                                    percentageInnerCutout: 0, // This is 0 for Pie charts
                                    //Number - Amount of animation steps
                                    animationSteps: 100,
                                    //String - Animation easing effect
                                    animationEasing: "easeOutBounce",
                                    //Boolean - Whether we animate the rotation of the Doughnut
                                    animateRotate: true,
                                    //Boolean - Whether we animate scaling the Doughnut from the centre
                                    animateScale: false,
                                    //Boolean - whether to make the chart responsive to window resizing
                                    responsive: true,
                                    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                                    maintainAspectRatio: false,
                                    //String - A legend template
                                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
                                    //String - A tooltip template
                                    tooltipTemplate: "<%=value %> <%=label%> Clicks"
                                  };
                                  //Create pie or douhnut chart
                                  // You can switch between pie and douhnut using the method below.
                                  pieChart.Doughnut(PieData, pieOptions);
                                  //-----------------
                                  //- END PIE CHART -
                                  //-----------------
                                },
                                error: function(xhr, desc, err) {
                                  console.log(err);
                                }
                              });
                            }

                            function users_click_au(){
                              //-------------
                              //- PIE CHART -
                              //-------------
                              // Get context with jQuery - using jQuery's .get() method.
                              $.ajax({
                                url: '<?php echo base_url(); ?>reports/get_users_info',
                                type: 'post',
                                data: { 'type' : 'get_click_au' },
                                success: function(data, status) {
                                  var pieChartCanvas1 = $("#pieChart1").get(0).getContext("2d");

                                  var pieChart = new Chart(pieChartCanvas1);
                                  var PieData1 = <?php echo $auclick; ?>;
                                  var pieOptions = {
                                    //Boolean - Whether we should show a stroke on each segment
                                    segmentShowStroke: true,
                                    //String - The colour of each segment stroke
                                    segmentStrokeColor: "#fff",
                                    //Number - The width of each segment stroke
                                    segmentStrokeWidth: 0.0001,
                                    //Number - The percentage of the chart that we cut out of the middle
                                    percentageInnerCutout: 0, // This is 0 for Pie charts
                                    //Number - Amount of animation steps
                                    animationSteps: 100,
                                    //String - Animation easing effect
                                    animationEasing: "easeOutBounce",
                                    //Boolean - Whether we animate the rotation of the Doughnut
                                    animateRotate: true,
                                    //Boolean - Whether we animate scaling the Doughnut from the centre
                                    animateScale: false,
                                    //Boolean - whether to make the chart responsive to window resizing
                                    responsive: true,
                                    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                                    maintainAspectRatio: false,
                                    //String - A legend template
                                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
                                    //String - A tooltip template
                                    tooltipTemplate: "<%=value %> <%=label%> Clicks"
                                  };
                                  //Create pie or douhnut chart
                                  // You can switch between pie and douhnut using the method below.
                                  pieChart.Doughnut(PieData1, pieOptions);
                                  //-----------------
                                  //- END PIE CHART -
                                  //-----------------
                                },
                                error: function(xhr, desc, err) {
                                  console.log(err);
                                }
                              });
                            }

                            function users_signup_info(){
                              $.ajax({
                                url: '<?php echo base_url(); ?>reports/get_users_info',
                                type: 'post',
                                data: { 'type' : 'get_variation' },
                                success: function(data, status) {
                                  var pieChartCanvas = $("#pieChart2").get(0).getContext("2d");
                                  var pieChart = new Chart(pieChartCanvas);
                                  var PieData = <?php echo $signup; ?>;
                                  var pieOptions = {
                                    //Boolean - Whether we should show a stroke on each segment
                                    segmentShowStroke: true,
                                    //String - The colour of each segment stroke
                                    segmentStrokeColor: "#fff",
                                    //Number - The width of each segment stroke
                                    segmentStrokeWidth: 0.0001,
                                    //Number - The percentage of the chart that we cut out of the middle
                                    percentageInnerCutout: 0, // This is 0 for Pie charts
                                    //Number - Amount of animation steps
                                    animationSteps: 100,
                                    //String - Animation easing effect
                                    animationEasing: "easeOutBounce",
                                    //Boolean - Whether we animate the rotation of the Doughnut
                                    animateRotate: true,
                                    //Boolean - Whether we animate scaling the Doughnut from the centre
                                    animateScale: false,
                                    //Boolean - whether to make the chart responsive to window resizing
                                    responsive: true,
                                    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                                    maintainAspectRatio: false,
                                    //String - A legend template
                                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
                                    //String - A tooltip template
                                    tooltipTemplate: "<%=value %> <%=label%> users"
                                  };
                                  //Create pie or douhnut chart
                                  // You can switch between pie and douhnut using the method below.
                                  pieChart.Doughnut(PieData, pieOptions);
                                  //-----------------
                                  //- END PIE CHART -
                                  //-----------------
                                },
                                error: function(xhr, desc, err) {
                                  console.log(err);
                                }
                              });
                            }
                            $( document ).ready(function() {
                            $("body").addClass('sidebar-collapse');


                             $('.nav-item-dashboard').addClass("active");
                             
                            });
                            </script>