<?php

$users_menu=$this->data['users_menu'];
$total_rows = $this->data['total_rows'];

//echo "<pre>"; print_r($variationvalue);exit;
//echo "<pre>"; print_r($subcategory);
?>
<section class="content-header">
	<h1>Variation Values</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url(); ?>home">Home</a></li>
		<li><a href="<?php echo base_url(); ?>category">Categories</a></li>
		<li><a href="<?php echo base_url(); ?>category/variationtype">Variation Types</a></li>
		<li class="active">Manage Variation Values</li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">List of Variation Values <small class="label label-info"><?php echo $total_rows; ?></small></h3>
			<div class="pull-right">
				<?php if(in_array("25",$users_menu)) { ?>
					<a href="<?php echo base_url(); ?>category/variationvalue_addedit"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>  New Variation </button></a>
					<?php } ?>
				</div>
			</div>
			<div class="box-body">
				<?php echo $this->session->flashdata('feedback'); ?>
				<form name="frmsearch" class="form-group" id="frmsearch" action="<?php echo base_url(); ?>category/variationvalue" method="post">
					<div class="row">
						<div class="col-md-2">
							<select name="search_by" id="search_by" class="form-control input-sm" >
								<option value="0">Search by</option>
								<option selected value="1" <?php echo set_value('search_by')==1 ? 'selected' : ''; ?>>Variation Name</option>
								<option value="2" <?php echo set_value('search_by')==2 ? 'selected' : ''; ?>>Variation Slug</option>
								<option value="3" <?php echo set_value('search_by')==3 ? 'selected' : ''; ?>>Variation Type</option>
								<option value="4" <?php echo set_value('search_by')==4 ? 'selected' : ''; ?>>Sub-Category</option>
								<option value="5" <?php echo set_value('search_by')==5 ? 'selected' : ''; ?>>Category</option>
								<option value="6" <?php echo set_value('search_by')==6 ? 'selected' : ''; ?>>Is Active</option>
							</select>
						</div>
						<div class="col-md-3">
							<div class="input-group">
								<input type="text" name="table_search" id="table_search" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo set_value('table_search'); ?>" required=""/>
								<div class="input-group-btn">
									<button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<a href="variationvalue" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
						</div>
						<div class="col-md-5">
							<!--div class="records-per-page pull-right">
								<select name="records-per-page" class="form-control input-sm">
									<option>Per Page</option>
									<option value="10" selected="selected">10</option>
									<option value="25">25</option>
									<option value="50">50</option>
								</select>
							</div-->
							<div class="pull-right">
								<?php echo $this->pagination->create_links(); ?>

							</div>
						</div>
					</div>
				</form>

				<div class="table-responsive">
					<table class="table table-bordered table-striped dataTable" id="datatable">
						<thead><tr>
							<th>ID</th>
							<th>Variation</th>
							<th>Slug</th>
							<th>Variation Type</th>
							<th>Sub-Category</th>
							<th>Category</th>
							<th>Is Active</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(!empty($variationvalue))
						{
							foreach($variationvalue as $v)
							{
								?>
								<tr>
									<td><?php echo $v->variation_value; ?></td>
									<td><?php echo $v->name; ?></td>
									<td><?php echo $v->slug; ?></td>
									<td><?php echo $v->variation_type; ?></td>
									<td><?php echo $v->sub_cat; ?></td>
									<td><?php echo $v->cat_name; ?></td>
									<td> <?php if($v->status==1){echo 'Yes';}else{ echo "No";} ?></td>
									<td>
										<?php if(in_array("30",$users_menu)) { ?>
											<a href="<?php echo base_url(); ?>category/variationvalue_addedit/<?php echo $v->variation_value; ?>"><button type="button" class="btn btn-primary btn-xs btn-rad editVar"><i class="fa fa-edit"></i> Edit</button></a>
											<?php } ?>
											<?php if(in_array("31",$users_menu)) { ?>
												<button type="button" class="btn btn-primary btn-xs btn-delete btn-rad deleteVar" data-id="<?php echo $v->variation_value; ?>" data-toggle="confirmation"><i class="fa fa-trash-o"></i> Delete</button>
												<?php } ?>
											</td>
										</tr>
										<?php
									}
								}else
								{
									?>
									<tr>
										<td >No Records Found</td>
									</tr>
									<?php

								}
								?>
							</tbody>
						</table>
						<?php echo $this->pagination->create_links(); ?>
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</div>
		</div>
	</div>

</section>
<script type="text/Javascript">
$( document ).ready(function() {
	$('[data-toggle=confirmation]').confirmation({
		title:'Are you sure?',
		onConfirm : function(){
			var del_id = $(this).closest('td').find('.deleteVar').attr('data-id');
			$.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>"+"Category/delete_variationvalue/",
				data: { del_id: del_id },
				cache:false,
				success:
				function(data){
					location.reload(); //as a debugging message.
				}

			});
		}
	});
	$('.nav-item-category').addClass("active");
	$('.nav-manage-var-val').addClass("active");
});
</script>
