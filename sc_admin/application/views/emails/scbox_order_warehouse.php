
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>SC Order for warehouse</title>

</head>
<body style="background:#f3f3f3; color:#333333; text-align:left; font-family:Arial,Helvetica,sans-serif; font-weight: normal; line-height: 1.5;
font-size: 14px; padding:0; margin:0 auto;">
<table style="width:600px;background:#fff;margin-bottom:20px; font-family:Arial,Helvetica,sans-serif;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
          <tbody>
            <tr>
              <td style="text-align:left;border-bottom:3px solid #00b19c;padding:20px">
                <table style="width:100%; font-family:Arial,Helvetica,sans-serif;" align="center" border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="text-align:left"> <img src="https://www.stylecracker.com/assets/images/logo.png" style="min-height:34px"> </td>
                      
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td style="padding:10px">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style=" font-family:Arial,Helvetica,sans-serif;">
                  <tbody>
                    <tr>
                      <td style="padding:15px 0 30px;vertical-align:middle" align="center"> <span style="padding:8px 0;border-bottom:1px solid #cccccc;font-size:18px;text-transform:uppercase"> <img src="https://www.stylecracker.com/assets/images/icons/ic-order-processing-1.png" style="vertical-align:middle">
                        <span> Curated Products </span> </td>
                    </tr>
                    <tr>
                      <td style="font-size:15px;line-height:1.7">
                        <p style="margin-bottom:20px"> Hi <strong><?php //echo $Username; ?>
                          </strong>,<br>
                          Please find the product(s) curated for Order No. (<b><?php echo $selorderid; ?></b>).<br/> </p>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <br>
                <div style="text-transform:uppercase;font-weight:bold;line-height:1;font-size:16px;margin-bottom:20px">
                  Order Details  </div>
                <table style="font-size:12px; font-family:Arial,Helvetica,sans-serif;" border="0" cellpadding="8" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th style="background-color:#f3f3f3;font-size:13px; vertical-align:top; font-weight:normal;"><strong>Image</strong></th>
                      <th style="background-color:#f3f3f3;font-size:13px; vertical-align:top; font-weight:normal;"><strong>Product ID</strong></th>
                      <th style="background-color:#f3f3f3;font-size:13px; vertical-align:top; font-weight:normal;"><strong>SKU</strong></th>
					  <th style="background-color:#f3f3f3;font-size:13px; vertical-align:top; font-weight:normal;"><strong>Product Price</strong></th>
					 
                    </tr></thead><tbody>
                  <?php 
				  
				 $sum = 0;
					if(is_array($product_data) && count($product_data))
					{
						foreach($product_data as $key => $proData)
						{
							$sum =  ($proData[0]['price']+$sum);
                   ?>

                    <tr>
                      <td><img src="<?php echo base_url();?>assets/products/<?php echo $proData[0]['image']; ?>" style="border:1px solid #cccccc;padding:2px;width:auto;min-height:auto;max-width:50px;max-height:50px" width="50"></td>
                      <td style="vertical-align:top;"><?php echo $proData[0]['id']; ?> </td>
                      <td style="vertical-align:top;"><?php echo $pdata[$proData[0]['id']]['sku']; ?></td>
                      <td style="vertical-align:top;">Rs.<?php echo number_format($proData[0]['price'],2); ?> </td>
                    </tr>

                    <?php
					
						}
            ?>
             <tr style="border-top: 1px dashed #ccc">
                    <th colspan="3" style="text-align: right;"><b>Total:</b></th>
                    <th><b>Rs.<?php  echo number_format($sum,2); ?></b></th>
                    </tr> 
					<?php
          }
					?>
                  
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td style="padding:0px 30px">
                <div style="border-bottom:1px solid #ccc;padding-bottom:30px"> Thank you for shopping with <strong>StyleCracker</strong>.<br>
                  For any query or assistance, feel free to <a href="mailto:customercare@stylecracker.com" style="color:#00b19c;text-decoration:none" target="_top">Contact
                    Us</a> </div>
              </td>
            </tr>
            <tr>
              <td style="padding:10px 0;">
              <a href="https://www.stylecracker.com">
                 <img src="https://www.stylecracker.com/assets/seventeen/images/scbox/scbox-promo-emailer.jpg">
                 </a>
              </td>
            </tr>
            <tr>
              <td style="vertical-align:middle;padding:20px 30px"> <span style="color:#888;font-weight:bold;line-height:1.3;vertical-align:top"> Follow Us: </span> <a href="https://www.facebook.com/StyleCracker" title="Facebook" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/facebook.png" alt="Facebook" style="width:24px"></a> <a href="https://twitter.com/Style_Cracker" title="Twitter" target="_blank">
                <img src="https://www.stylecracker.com/assets/images/social-media/twitter.png" alt="Twitter" style="width:24px"></a>
                <a href="https://www.youtube.com/user/StyleCrackerTV" title="Youtube" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/youtube.png" alt="Youtube" style="width:24px"></a> <a href="https://instagram.com/stylecracker" title="Instagram" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/instagram.png" alt="Instagram" style="width:24px"></a>
                 <a href="https://www.pinterest.com/stylecracker" title="Pintrest" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/pintrest.png" alt="Pintrest" style="width:24px"></a> </td>
            </tr>
			<img src="https://www.google-analytics.com/collect?
v=1&tid=UA-64209384-1&cid=555&t=event&ec=Email&ea=open&el=order_warehouse&dp=%2Femail%2Forder_warehouse&dt=Email%20Tracking"/>
          </tbody>
        </table>
         <table width="600" cellpadding="0" cellspacing="0" align="center" border="0" style="width:600px; background:#fff; border:1px solid #cccccc; margin-bottom:20px; font-family:Arial,Helvetica,sans-serif;">
        <tr>
          <td align="left" style="padding:20px 0 0 20px; width:160px;">
              <img src="https://www.stylecracker.com/assets/images/logo.png" style="width:150px; margin-bottom:5px;">
          <div>
            <a href="https://www.stylecracker.com" style="color:#00b19c; text-decoration:none;">www.stylecracker.com</a>
          </div>
          </td>
            <td align="left"  style="padding:20px 0 0; vertical-align:bottom;">
              <img src="https://www.stylecracker.com/assets/images/icons/ic-download-app-1.png">
            <td>

            </td>
          <td align="right"  style="padding:20px 20px  0 0; text-align:center; ">
            <div style="font-size:18px; margin-bottom:10px;">
              StyleCracker App is India\'s only <br /> personalised styling platform.
            </div>

             <a href="https://play.google.com/store/apps/details?id=com.stylecracker.android" target="_blank" style="color:#333; text-decoration:none;">
                  <img src="https://www.stylecracker.com/assets/images/icons/en_generic_rgb_wo_45.png" style="height:30px;" target="_blank"></a>

            <a href="https://itunes.apple.com/us/app/stylecracker/id1061463812?mt=8&uo=4" target="_blank" style="color:#333; text-decoration:none; ">
              <img src="https://www.stylecracker.com/assets/images/icons/apple_store.png" style="height:30px;" target="_blank"></a>


          </td>
        </tr>
      </table>
      </body>
      </html>
