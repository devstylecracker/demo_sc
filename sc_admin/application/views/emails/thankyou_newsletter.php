<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Thank You</title>

</head>
<body style="background:#f3f3f3; color:#333333; text-align:left; font-family:Arial,Helvetica,sans-serif; font-weight: normal; line-height: 1.5;
font-size: 14px; padding:0; margin:0 auto;">
<table style="width:600px;background:#fff;margin-bottom:20px; font-family:Arial,Helvetica,sans-serif;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
          <tbody>
            <tr>
              <td style="padding:10px">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style=" font-family:Arial,Helvetica,sans-serif;">
                  <tbody>                  
                    <tr>
                       <td style="padding:0px 30px">
                        <div style="padding-bottom:30px">Dear <?php echo ucfirst(strtolower(@$first_name));  ?>,<br/><br/>
                        Wow, what a journey it's been! From Archana and I chatting over coffee about this crazy idea of making personal styling accessible to every Indian, to having India's biggest Bollywood star invest in our company, it's been an amazing ride. We have always dreamt big and with Alia Bhatt investing in StyleCracker, we will continue to dream bigger.<br/><br/>

                        StyleCracker is and always will be a technology and styling company. It's what we know best and what we do well. With the launch of the StyleCracker Box, we were able to translate our expertise in personal styling at an even bigger scale and make it available to each one of you! Our mission is to be able to empower each and every one of our users, customers and followers to be the best version of themselves. It is this aspect of our business that excites us every day and drives us to provide you all - our StyleCracker Family, with the most personal, seamless and value driven fashion experience.<br/><br/>

                        I wanted to personally share the news that Alia Bhatt is now part of the StyleCracker team, details of which you can find on our Facebook page at <a href="https://www.facebook.com/StyleCracker/" title="StyleCracker" target="_blank" style="color: #13d792;text-decoration: none;"> https://www.facebook.com/StyleCracker/ </a><br/><br/>

                        I am also writing this to say thank you to each and every one of you without whom this dream would have simply remained just that, a dream. It would be mine, Archana, our team and our investors' absolute honour if you continued to support us and join us on this very exciting journey together. 
                         </div>
                       </td>
                    </tr>
                    <tr>
                        <td style="padding:0px 30px 30px;"><div>Kind Regards,<br/><br/>Dhimaan Shah<br/>CEO Founder<br/>StyleCracker</div></td>
                    </tr>  
                    <tr>
                        <td><img src="https://i.imgur.com/Qju0axE.jpg"></td>
                    </tr>                                   
                  </tbody>
                </table>               
              </td>
            </tr>           
          </tbody>
  <img src="https://www.google-analytics.com/collect?v=1&tid=UA-64209384-1&cid=555&t=event&ec=Email&ea=open&el=thankyou_newsletter&dp=%2Femail%2FThankyou Newsletter&dt=Email%20Tracking"/>

</table>
</body>
</html>