<?php
  $users_menu=$this->data['users_menu'];
?>
<!-- <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/redmond/jquery-ui.css" />    
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script> -->

<!--pqSelect dependencies-->
<!--link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev.js"></script-->

<section class="content-header">
  <h1>Brand Category/Collection Relation </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>home">Home</a></li>
      <li><a href="<?php echo base_url(); ?>brandCategory">Brand Categories/Collection</a></li>
      <li class="active"><?php echo $page;?> Brand Category/Collection Relation</li>
    </ol>
</section>
<section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"><?php echo $page;?> Brand Category/Collection Relation</h3>
      </div><!-- /.box-header -->
      <form role="form" name="frmbrandcategory" id="frmbrandcategory"  method="post">
        <div class="box-body">
         <?php echo $this->session->flashdata('feedback'); ?>
          <div class="row">
            <div class="col-md-4">
               <div class="form-group">
                <label for="Brand">Store  </label>
                <select tabindex="5" id="brand" name="brand" class="form-control" tabindex="-1" onchange="get_brands_categories();">
                  <option value="0">Select Store</option>
                  <?php
                  if(!empty($paid_stores)){
                    foreach($paid_stores as $val){
                      if(set_value('brand') == $val->brand_id ){
                        echo '<option value="'.$val->brand_id.'" selected>'.$val->company_name.'</option>';
                      }else{

                        echo '<option value="'.$val->brand_id.'">'.$val->company_name.'</option>';
                      }
                    }
                  }
                  ?>
                </select>
               </div>              
            </div>
            <div class="col-md-4">              
               <div class="form-group">
                <label for="Brand">Brand Category/Collection</label>
                <!-- class="form-control multi-select-box chosen-select" -->
                <select data-placeholder="Choose Category"  class="form-control input-lg" name="brandCat[]" id="brandCat" multiple='true' tabindex="">
                <option value="">Select Category/Collection</option>              
                   
                </select>
              </div>                
            </div>             
          </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Submit</button>
          <a href="<?php echo base_url(); ?>BrandCategory"><button type="button" class="btn btn-primary btn-sm btn-rad"><i class="fa fa-reply"></i> Back</button></a>
          <button class="btn btn-default btn-sm" type="reset"><i class="fa fa-close"></i> Cancel</button>
        </div>
      </form>
    </div><!-- /.box -->
</section>

<script type="text/Javascript">

function assignSlug() {
    var x = document.getElementById("categoryName");
    var slug = x.value.toLowerCase();
    var newslug = slug.replace(/ /g,"-");
    $("#categorySlug").val(newslug);
}

function get_brands_categories(){
  var brand_id = $('#brand').val();
  if(brand_id > 0){
    $.ajax({
          url: '<?php echo base_url(); ?>BrandCategory/get_brand_categories',     
          type: 'post',
          data: { 'brand_id' : brand_id },
            success: function(data, status) {
              $('#brandCat').html(data);
          }
    });
   }else{
    alert('Select Brand ');
   }

  }

</script>


