<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Stylecracker Admin | Log in</title>
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <!-- Bootstrap 3.3.4 -->
  <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- Font Awesome Icons -->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <!-- Theme style -->
  <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet" type="text/css" />

</head>
<body class="login-page">
  <div class="login-box">
    <div class="login-logo">
      StyleCracker <strong>Admin</strong>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg">Sign in to start your session</p>
	   <?php echo $this->session->flashdata('feedback'); ?>
      <?php if(isset($error)) { echo $error; } ?>
      <form action="<?php echo base_url(); ?>login/check_login" method="post">
        <div class="form-group has-feedback">
          <input type="email" name="email" id="email" class="form-control" placeholder="Email"/>
          <div class="error"><?php form_error('email'); ?></div>
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" name="password" id="password" class="form-control" placeholder="Password"/>
          <div class="error"><?php echo form_error('password'); ?></div>
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          <div class="login-show-password-wrp">
            <div class="row">
             <div class="col-md-6">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="remember_me" id="remember_me" value="1"> Remember me
                  </label>
                </div></div>
              </div>
            </div></div>
            <div class="row">

              <div class="col-xs-12">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
              </div><!-- /.col -->
            </div>
          </form>
		<div class="footer-links">
			<a href="<?php echo base_url(); ?>brand_onboarding/store_registration/forgot_password">I forgot my password</a>
			<a href="<?php echo base_url(); ?>brand_onboarding/store_registration" class="pull-right">Register</a>
        </div>


          <!--a href="#">I forgot my password</a><br-->


        </div><!-- /.login-box-body -->
      </div><!-- /.login-box -->

      <!-- jQuery 2.1.4 -->
      <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
      <!-- Bootstrap 3.3.2 JS -->
      <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
      <!-- iCheck -->
      <script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

    </body>
    </html>
