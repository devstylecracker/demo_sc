<?php //echo "<pre>"; print_r($order_product_info);      
      //echo "<pre>"; print_r($order_info);
      
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title></title>

  </head>
  <body style="background:#f3f3f3; color:#333333; text-align:left; font-family:Arial,Helvetica,sans-serif; font-weight: normal; line-height: 1.5;
               font-size: 14px; padding:0; margin:0 auto;">
    <table style="width: 600px; background: #fff; margin-bottom: 20px; font-family: Arial,Helvetica,sans-serif;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
      <tbody>
        <tr>
          <td style="text-align: left; border-bottom: 3px solid #00b19c; padding: 20px;">
            <table style="width: 100%; font-family: Arial,Helvetica,sans-serif;" align="center" border="0" cellpadding="0" cellspacing="0">
              <tbody>
                <tr>
                  <td style="text-align: left;"><img src="https://www.stylecracker.com/assets/images/logo.png" style="min-height: 34px;" /></td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td style="padding: 10px;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family: Arial,Helvetica,sans-serif;">
              <tbody>
                <tr>
                  <td style="font-size: 15px; line-height: 1.7;">
                    <p style="margin-bottom: 20px;">Hi <b style="text-transform: capitalize;"><?php echo $first_name; ?></b>,<br />This is your stylist from StyleCracker checking in to make sure you're on top of your style game.</p>
                    <p style="margin-bottom: 20px;">It's been a while since you ordered your last StyleCracker box and we hope it's because you've discovered your inner fashionista. If it was something at our end that put you off, we'd love to hear back from you. Feedback is critical to the StyleCracker service. Not just to curate better boxes for you, but to help us improve our service for every single customer.</p>
                    <p style="margin-bottom: 20px; text-align: left;">If you are planning to order a box sometime soon, below is a coupon code valid for 2 weeks that gives you 30% off on your next StyleCracker Box.</p>
                    <p style="margin-bottom: 20px; text-align: left;">Coupon Code : <strong><span style="color: #00b19c;">SCBOX30</span></strong></p>
                    <p>We can't wait to curate another box of fashion just for you!</p>
                    <p>Thanks,<br />Team StyleCracker</p>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td style="padding: 10px 0;"><a href="https://www.stylecracker.com"> <img src="https://www.stylecracker.com/assets/seventeen/images/scbox/scbox-promo-emailer.jpg" /> </a></td>
        </tr>
        <tr>
          <td style="vertical-align: middle; padding: 20px 30px;"><span style="color: #888; font-weight: bold; line-height: 1.3; vertical-align: top;"> Follow Us: </span> <a href="https://www.facebook.com/StyleCracker" title="Facebook" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/facebook.png" alt="Facebook" style="width: 24px;" /></a> <a href="https://twitter.com/Style_Cracker" title="Twitter" target="_blank"> <img src="https://www.stylecracker.com/assets/images/social-media/twitter.png" alt="Twitter" style="width: 24px;" /></a> <a href="https://www.youtube.com/user/StyleCrackerTV" title="Youtube" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/youtube.png" alt="Youtube" style="width: 24px;" /></a> <a href="https://instagram.com/stylecracker" title="Instagram" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/instagram.png" alt="Instagram" style="width: 24px;" /></a> <img src="https://www.stylecracker.com/assets/images/social-media/pintrest.png" alt="Pintrest" style="width: 24px;" /></td>
        </tr>
      </tbody>
    </table>
  </body>
</html>



