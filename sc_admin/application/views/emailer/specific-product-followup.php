<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Specific Product Follow up</title>
</head>
<body style="margin:0; padding:0; ">
<table bgcolor="#fff" style="margin:0px auto; padding:0; color:#000000;  font-family:'Lucida Sans Unicode', 'Lucida Grande', sans-serif;
font-weight: normal; line-height: 1.5; font-size: 14px; " align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td>
<table style="" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
  <tbody>
    <tr>
      <td colspan="4" style="text-align:center; padding:10px 10px 10px">
        <a href="https://www.stylecracker.com/" target="_blank"><img src="https://www.stylecracker.com/assets/images/sc-logo-m.png" style="height:47px;"></a>
      </td>
    </tr>
    <tr>
      <td style="text-align:center; padding:6px 10px; background:#000;">
        <a href="https://www.stylecracker.com/all-products/women" target="_blank" style="color:#fff; font-size:16px; text-transform:uppercase; font-weight:bold; text-decoration:none;">Women</a>
      </td>
      <td style="text-align:center; padding:6px 10px;  background:#000;">
        <a href="https://www.stylecracker.com/all-products/men" target="_blank" style="color:#fff; font-size:16px; text-transform:uppercase;  font-weight:bold; text-decoration:none;">Men</a>
      </td>
      <td style="text-align:center; padding:6px 10px; background:#000; ">
        <a href="https://www.stylecracker.com/brands" target="_blank" style="color:#fff; font-size:16px; text-transform:uppercase;  font-weight:bold; text-decoration:none;">Brands</a>
      </td>
      <td style="text-align:center; padding:6px 10px; background:#000; ">
        <a href="https://www.stylecracker.com/blog/" target="_blank" style="color:#fff; font-size:16px; text-transform:uppercase;  font-weight:bold; text-decoration:none;">SCLive</a>
      </td>
    </tr>
    <tr>
      <td colspan="4" style=" padding-top:20px 0;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"  style="margin-top:10px; ">
          <tbody>
            <tr>
              <td colspan="3" align="center" style="background:#50f4f4; padding:10px 0;">
                <img src="https://www.stylecracker.com/assets/images/emailer/specific-products-followup.jpg" style="vertical-align:middle">
              </td>
            </tr>
            <tr>
              <td  colspan="3" style="line-height:1.4;  font-size:18px; text-align:center; padding:10px 10px;">
                <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/1472823350626.png" width="200" height="200" />
                <div style="font-size:21px; margin-top:10px; margin-bottom:5px;">Our stylists at StyleCracker<br />
                have recommended a few products<br /> that are a perfect match<br /> with your order.
                </div>
              </td>
            </tr>

          </tbody>
        </table>
      </td>
    </tr>

    <tr>
      <td colspan="4" style=" padding-top:20px 0; background:#000;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="486" >
          <tbody>
            <tr>
              <td width="33.3%"  style="line-height:1.4;  font-size:18px; text-align:center; padding:10px 5px; 10px 10px; text-align:center;">
                  <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/1472823350626.png" width="150" height="150" style="width:auto; max-width:150px; text-align:center; margin:0 auto;" />
                  <div style="font-size:12px; color:#fff; margin-top:15px;">
                    <div>Collar Embroidered Tunic</div>
                    <div>Rs. 1689</div>
                  </div>
              </td>
              <!--<td width="33.3%"  style="line-height:1.4;  font-size:18px; text-align:center; padding:10px 5px;">
                  <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/1472823350626.png" width="150" height="150" style="width:auto; max-width:150px;"/>
                  <div style="font-size:12px; color:#fff; margin-top:15px;">
                  <div>Collar Embroidered Tunic</div>
                  <div>Rs. 1689</div>
                  </div>
              </td>
              <td width="33.3%"  style="line-height:1.4;  font-size:18px; text-align:center; padding:10px 10px 10px 5px;">
                  <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/1472823350626.png" width="150" height="150" style="width:auto; max-width:150px;"/>
                  <div style="font-size:12px; color:#fff; margin-top:15px;">
                  <div>Collar Embroidered Tunic</div>
                  <div>Rs. 1689</div>
                  </div>
              </td>-->

            </tr>

          </tbody>
        </table>
      </td>
    </tr>

    <tr>
      <td colspan="4" style="vertical-align:middle;padding:20px 0px 10px; text-align:center; ">
        <div>
          <a href="https://www.facebook.com/StyleCracker" title="Facebook" target="_blank">
            <img src="https://www.stylecracker.com/assets/images/social-media/facebook.png" alt="Facebook" style="width:20px"></a>
            <a href="https://twitter.com/Style_Cracker" title="Twitter" target="_blank">
              <img src="https://www.stylecracker.com/assets/images/social-media/twitter.png" alt="Twitter" style="width:20px"></a>
              <a href="https://www.youtube.com/user/StyleCrackerTV" title="Youtube" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/youtube.png" alt="Youtube" style="width:20px"></a>
              <a href="https://www.instagram.com/stylecracker/" title="Instagram" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/instagram.png" alt="Instagram" style="width:20px"></a>
              <a href="https://www.pinterest.com/stylecracker/" title="Pintrest" target="_blank"><img src="https://www.stylecracker.com/assets/images/social-media/pintrest.png" alt="Pintrest" style="width:20px"></a>
            </div>
            <div style="font-size:13px; text-transform:uppercase;">
            <a href="https://www.stylecracker.com/" target="_blank" style="color:#000; text-decoration:none;">www.stylecracker.com</a>
            </div>
            <div style="font-size:11px;">
                <a href="mailto:support@stylecracker.com" target="_blank" style="color:#000; text-decoration:none;">support@stylecracker.com</a>  |  +91-22-61738500
            </div>

          </td>
        </tr>
      </tbody>
    </table>
  </td>
</tr>
</table>
  </body>
  </html>
