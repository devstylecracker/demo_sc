<section class="content-header">
    <h1>Product Discount Export / Import </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>home">Home</a></li>
      <li><a href="#">Discount Management</a></li>
      <li class="active">Product Discount Export / Import </li>
    </ol>
</section>
<section class="content">

  <div class="row">
  <?php echo $this->session->flashdata('feedback'); ?>

   <form role="form" id="download" enctype="multipart/form-data" method="POST" action="<?php echo base_url(); ?>DiscountManagement/download_cuesheet">
        <div class="col-md-6">
          <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-download"></i> Download Discount CueSheet<small class="label label-info"></small></h3>
                <div >
                  <p class="text-orange">Download Brandwise Current Discount</p>
                </div>
                </div>
                <div class="box-body">                     
                    <div class="form-group">
                       <div class="col-md-2">
                        <p> Brand&nbsp;&nbsp;  </p>
                        </div>                           
                         <div class="col-md-6">                          
                            <select class="form-control chosen-select" name="brand" id="brand" tabindex="5">
                             <?php if(!empty($paid_brands)){ 
                              foreach($paid_brands as $val){ ?>
                              <option value="<?php echo $val['user_id']; ?>"><?php echo $val['company_name']; ?></option>
                              <?php } ?>
                             <?php }else{ ?>
                             <option>Sorry no brand fount</option>
                             <?php } ?>
                            </select>  
                         </div>   
                        <div class="col-md-4">
                          <a href="javascript:void(0);" onclick="javascript:void(0);" class="btn btn-primary btn-sm" id="file_download"><i class="fa fa-download"></i> Download</a>
                        </div>
                    </div>                     
                    <div class="col-md-12">
                    <p class="help-block"> File format: .xls | Date Format : YYYY-MM-DD H:i:s</p>
                    </div>                    
                  </div>                      
                </div>
            </div>
         </form>

      <form role="form" id="product_uploadform" enctype="multipart/form-data" method="POST" action="<?php echo base_url(); ?>DiscountManagement/upload_discount" onsubmit="return validate();">
        <div class="col-md-6">
          <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-upload"></i> Upload Discount CueSheet <small class="label label-info"></small></h3>
                <div class="pull-right">
                </div>
                </div>
                <div class="box-body">
                    <p>Browse Que Sheet <input id="product_upload" name="product_upload" class="file1" type="file"></p>
                    <p class="help-block">Max file size: 5MB | Allowed format: .xls, .xlsx</p>
                    <button class="btn btn-primary btn-sm" type="submit" name = "Save"  value="Save"><i class="fa fa-upload"></i> Upload</button>
                    <a href="<?php echo base_url(); ?>product_new" ><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i> Back</button></a>
                </div>
                <span class="text-red"> <?php /*if(@$errorCount!="") { echo "Error Inserting Records: ".@$errorCount."<br/>";}
                                                  if(@$successcount!="") { echo "Success  Inserting Records: ".@$successcount."<br/>"; } */
                                                  if(@$imagemsg!="") { echo "<b> Error in Uploading Image :</b> ".@$imagemsg."<br/>"; }

                                                  if(@$excelmsg!="")
                                                    {
                                                       echo "<b> Error in Excel Sheet :</b> <br/>";
                                                      foreach($excelmsg  as $ex)
                                                      {
                                                        echo @$ex."<br/>";
                                                      }

                                                    }

                                                  ?>
                </span>
           </div>
        </div>
        </form>   
        
      </div>
  </section><!-- /.content -->

<script type="text/Javascript">
$(document).ready(function(){
  $('#file_download').click(function(e){
    var brand = $('#brand').val();
    form = $('#download');
    window.location = "<?php echo base_url(); ?>DiscountManagement/download_cuesheet/"+brand;
  });
});
function validate(){
    var extension;
    var filename=document.getElementById('product_upload').value;
    extension=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();
  
    if(filename == "")
    {
        alert('Browse Excel File');
        return false;

    }else
    {//extension == 'xlsx' || extension == 'xls' || 
      if(extension == 'xlsx' || extension == 'xls')
      {
        return true;
      }else
      {
        alert('Check File Extenstion');
        return false;
      }

    }
}
$( document ).ready(function() {
$("body").addClass('sidebar-collapse');
$('.nav-discount-mgmt').addClass("active"); 
});
</script>
