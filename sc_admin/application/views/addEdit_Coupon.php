<!-- Main content -->
<?php //echo '<pre>'; print_r($coupon_data); 
/*echo $coupon_data[0]->coupon_code;exit;*/
/*echo $coupon_data[0]->individual_use_only;exit;*/
$coupon_id = $this->uri->segment(3);
if($coupon_id!='')
{
  $page = 'Edit';
}else
{
   $page = 'Add';
}
?>

<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/redmond/jquery-ui.css" />    
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<!--pqSelect dependencies-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/coupon.js" type="text/javascript"></script>

<!-- Main content -->
<section class="content-header">
  <h1>Coupon Settings</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>CouponSettings">Manage Coupon</a></li>
    <!-- <li><a href="<?php echo base_url(); ?>brandusers/manage_store/<?php echo $brand_id; ?>">Manage Brands</a></li> -->
    <li class="active"><?php echo  $page; ?> Coupon</li>
  </ol>
</section>
<section class="content">
  <div class="box ">
    <div class="box-header">
      <h3 class="box-title"><?php echo  $page; ?> Coupon</h3>
    </div><!-- /.box-header -->
    <!-- form start -->
    <form name="frmcoupon" id="frmcoupon" role="form" method="post" enctype="multipart/form-data">
      <div class="box-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Coupon Code<span class="text-red">*</span></label>
              <input type="text" name="coupon_code" id="coupon_code" value="<?php if(isset($coupon_data[0]->coupon_code) && $coupon_data[0]->coupon_code!='') { echo $coupon_data[0]->coupon_code; } else { echo set_value('coupon_code'); } ?>" <?php if(isset($coupon_data[0]->coupon_code) && $coupon_data[0]->coupon_code!='') { echo 'readonly'; } ?> placeholder="Enter Coupon Code" class="form-control" required>
              <span class="text-red"> <?php echo form_error('coupon_code'); ?></span>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Coupon Description</label>
                   <textarea name="coupon_desc" id="coupon_desc" placeholder="Enter Coupon Description" rows="3" class="form-control" required><?php if(isset($coupon_data[0]->coupon_desc) && $coupon_data[0]->coupon_desc!=''){ echo $coupon_data[0]->coupon_desc; }else{ echo set_value('coupon_desc'); } ?></textarea>
                    <span class="text-red"> <?php echo form_error('coupon_desc'); ?></span>                 
                  </div>
                </div>                
            </div>  
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Start Date<span class="text-red">*</span></label>
                  <input type="text" name="date_from" id="date_from" value="<?php if(isset($coupon_data[0]->coupon_start_datetime) && $coupon_data[0]->coupon_start_datetime !=''){ echo $coupon_data[0]->coupon_start_datetime; }else{ echo set_value('date_from'); } ?>" placeholder="Coupon Start Date" class="form-control datetimepicker">
                  <span class="text-red"> <?php echo form_error('date_from'); ?></span>
                </div>                 
                <div class="form-group">
                  <label>Minimum Spend<span class="text-red">*</span></label>
                  <input type="text" name="min_spend" id="min_spend" placeholder="Enter Minimum Spend" class="form-control datetimepicker" required value="<?php if(isset($coupon_data[0]->min_spend) && $coupon_data[0]->min_spend!=''){ echo $coupon_data[0]->min_spend; }else{ echo set_value('min_spend'); } ?>">
                  <span class="text-red"> <?php echo form_error('min_spend'); ?></span>
                </div>  
                <div class="form-group">
                  <label for="discounttype">Discount Type </label>
                  <select tabindex="5" id="discount_type" name="discount_type" class="form-control" tabindex="-1" <?php echo ($page=='Edit') ? 'disabled' : ''; ?> >
                    <option value="">Select Discount Type</option>
                    <?php
                    if(!empty($discount_type)){
                      $i=1;
                      foreach($discount_type as $val){
                        if(set_value('discount_type') == $i || $coupon_data[0]->discount_type == $i){
                          echo '<option value="'.$i.'" selected>'.$val.'</option>';
                        }else{

                          echo '<option value="'.$i.'">'.$val.'</option>';
                        }
                        $i++;
                      }
                    }
                    ?>
                  </select>
                </div>                                    
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>End Date<span class="text-red">*</span></label>
                  <input type="text" name="date_to" id="date_to" placeholder="Select End Date" class="form-control" required value="<?php if(isset($coupon_data[0]->coupon_end_datetime) && $coupon_data[0]->coupon_end_datetime!=''){ echo $coupon_data[0]->coupon_end_datetime; }else{ echo set_value('date_to'); } ?>">
                  <span class="text-red"> <?php echo form_error('date_to'); ?></span>
                </div>
                <div class="form-group">
                  <label>Maximum Spend<span class="text-red">*</span></label>
                  <input type="text" name="max_spend" id="max_spend" placeholder="Enter Maximum Spend" class="form-control" required value="<?php if(isset($coupon_data[0]->max_spend) && $coupon_data[0]->max_spend!=''){ echo $coupon_data[0]->max_spend; }else{ echo set_value('max_spend'); } ?>">
                  <span class="text-red"> <?php echo form_error('max_spend'); ?></span>
                </div>               
              </div>              
              <div class="col-md-6">             
                 <div class="form-group">
                  <label>Coupon Amount<span class="text-red">*</span></label>
                  <input type="text" name="coupon_amount" id="coupon_amount" placeholder="Enter State" class="form-control" required value="<?php if(isset($coupon_data[0]->coupon_amount) && $coupon_data[0]->coupon_amount!=''){ echo $coupon_data[0]->coupon_amount; }else{ echo set_value('coupon_amount'); } ?>" <?php echo ($page=='Edit') ? 'disabled' : ''; ?> >
                  <span class="text-red"> <?php echo form_error('coupon_amount'); ?></span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">                             
                    <div class="checkbox">
                      <label>
                      <input type="checkbox" name="individual_use_only" id="individual_use_only"  value="<?php echo $coupon_data[0]->individual_use_only; ?>" <?php if(isset($coupon_data[0]->individual_use_only) && $coupon_data[0]->individual_use_only==1){ echo 'checked'; }else{ echo ''; } ?> >Individual Use Only
                      </label>
                       <label>Check this box if the coupon to applied to individual email-ids </label>
                      <!--<label>Check this box if the coupon cannot be used in conjunction with other coupons</label>-->
                    </div> 
                </div>
              </div> 
              <div class="col-md-6">
                <div class="form-group">   
                    <div class="checkbox">
                      <label>
                      <input type="checkbox" name="exclude_sale_items" id="exclude_sale_items" disabled="true" value="0" <?php if(isset($coupon_data[0]->exclude_sale_items) && $coupon_data[0]->exclude_sale_items==1){ echo 'checked'; }else{ echo ''; } ?> >Exclude Sale Items
                      </label>
                      <label>Check this box if the coupon should not apply to the items on sale. Per-item coupons will only work if the item is not on sale. Per-cart coupons will only work if there are no sale items in the cart</label>
                    </div>                  
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-6">            
             <div class="col-md-6">
                <div class="form-group">
                  <label>Usage Limit Per Coupon<span class="text-red">*</span></label>
                  <input type="text" name="usage_limit_per_coupon" id="usage_limit_per_coupon" placeholder="Enter Usage Limit Per Coupon" class="form-control" required value="<?php if(isset($coupon_data[0]->usage_limit_per_coupon) && $coupon_data[0]->usage_limit_per_coupon){ echo $coupon_data[0]->usage_limit_per_coupon; }else{ echo set_value('usage_limit_per_coupon'); } ?>">
                  <span class="text-red"> <?php echo form_error('usage_limit_per_coupon'); ?></span>
                </div>    
                <!-- <div class="form-group">
                  <label>Usage Limit To Items<span class="text-red">*</span></label>
                  <input type="text" name="usage_limit_to_items" id="usage_limit_to_items" placeholder="Enter Usage Limit To Items" class="form-control" required value="<?php if(isset($coupon_data[0]->usage_limit_to_items) && $coupon_data[0]->usage_limit_to_items){ echo $coupon_data[0]->usage_limit_to_items; }else{ echo set_value('usage_limit_to_items'); } ?>">
                  <span class="text-red"> <?php echo form_error('usage_limit_to_items'); ?></span>
                </div>   -->              
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Usage Limit To User<span class="text-red">*</span></label>
                  <input type="text" name="usage_limit_to_user" id="usage_limit_to_user" placeholder="Enter Usage Limit To User" class="form-control" required value="<?php if(isset($coupon_data[0]->usage_limit_to_user) && $coupon_data[0]->usage_limit_to_user!=''){ echo $coupon_data[0]->usage_limit_to_user; }else{ echo set_value('usage_limit_to_user'); } ?>">
                  <span class="text-red"> <?php echo form_error('usage_limit_to_user'); ?></span>
                </div>         
                <!-- <div class="form-group">
                  <label>Email<span class="text-red">*</span></label>
                  <input type="text" placeholder="Enter Email" id="email" name="email" required class="form-control" value="<?php if(isset($coupon_data[0]->email_restriction) && $coupon_data[0]->email_restriction!=''){ echo $coupon_data[0]->email_restriction; }else{ echo set_value('email'); } ?>">
                  <span class="text-red" > <?php echo form_error('email'); ?> </span>
                </div> -->
              </div>          
              
              <div class="form-group">
                <label>Email-Id Applicable</label>
                 <textarea name="coupon_email_applied" id="coupon_email_applied" placeholder="Enter Valid Email-id For Coupon" rows="3" class="form-control" ><?php if(isset($coupon_data[0]->coupon_email_applied) && $coupon_data[0]->coupon_email_applied!=''){ echo $coupon_data[0]->coupon_email_applied; }else{ echo set_value('coupon_email_applied'); } ?></textarea>  <span class="text">Note: Add comma separated email-ids which can avail this coupon-code</span>                             
              </div>

              <div class="col-md-12">
                <div class="form-group">   
                    <div class="checkbox">
                      <label>
                      <input type="checkbox" name="stylecracker_discount" id="stylecracker_discount"  value="<?php echo $coupon_data[0]->stylecrackerDiscount; ?>" <?php if(isset($coupon_data[0]->stylecrackerDiscount) && $coupon_data[0]->stylecrackerDiscount==1){ echo 'checked'; }else{ echo ''; } ?> >Stylecracker Discount
                      </label>
                      <label>Check this box if the coupon should apply to all the products of stylecracker.</label>
                    </div>                  
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">   
                    <div class="checkbox">
                      <label>
                      <input type="checkbox" name="showEmailPopUp" id="showEmailPopUp"  value="0" <?php if(isset($coupon_data[0]->showEmailPopUp) && $coupon_data[0]->showEmailPopUp==1){ echo 'checked'; }else{ echo ''; } ?> >Show Email Popup
                      </label>
                      <label>Check this box to show email pop-up to users</label>
                    </div>                  
                </div>
              </div>
               
              <div class="col-md-12">
              <div class="form-group">
                <label for="Brand">Store  </label>
                <select tabindex="5" id="brand" name="brand" class="form-control" tabindex="-1">
                  <option value="0">Select Store</option>
                  <?php
                  if(!empty($paid_stores)){
                    foreach($paid_stores as $val){
                      if(set_value('brand') == $val['brand_id'] ||  $coupon_data[0]->brand_id == $val['brand_id']){
                        echo '<option value="'.$val["brand_id"].'" selected>'.$val["company_name"].'</option>';
                      }else{

                        echo '<option value="'.$val["brand_id"].'">'.$val["company_name"].'</option>';
                      }
                    }
                  }
                  ?>
                </select>
              </div>          
              <div class="form-group">
                <label for="productTags">Products</label>
                <select placeholder="Select Products" tabindex="19" data-placeholder="Choose Products" name="products[]" id="products" class="form-control multi-select-box " multiple tabindex="">
                <option value="">Select Products</option>                 
                    <?php
                    if(!empty($product_info)){
                          foreach($product_info as $val){
                             if(in_array($val,$products)){
                               echo '<option value="'.$val.'" selected>'.$val->name.'</option>';
                             }else{

                              echo '<option value="'.$val.'">'.$val->name.'</option>';
                             }
                          }
                    }
                    ?>
                </select>
              </div> 

        			<div class="col-md-6">             
        				<div class="form-group">
        					<label>BIN Number(if any)</label>
        					<input type="text" name="bin_no" id="bin_no" placeholder="Enter BIN Number" class="form-control" value="<?php if(isset($coupon_data[0]->bin_no) && $coupon_data[0]->bin_no!=''){ echo $coupon_data[0]->bin_no; }else{ echo set_value('bin_no'); } ?>">
        					<span class="text-red"> <?php echo form_error('bin_no'); ?></span>
        				</div>
        			</div>
        			<div class="col-md-6">
        				<div class="form-group">    
        					<label>Show on web</label>				
        					<div class="checkbox">
        						<label>
        						<input type="checkbox" name="show_on_web" id="show_on_web" value="<?php echo $coupon_data[0]->show_on_web; ?>" <?php if(isset($coupon_data[0]->show_on_web) && $coupon_data[0]->show_on_web==1){ echo 'checked'; }else{ echo ''; } ?> >Active
        						</label>
        					</div> 
        				</div>
        			</div>		

               <div class="col-md-12">
                   <div class="form-group">
                      <label for="coupontype">Coupon Type </label>
                      <select tabindex="5" id="coupon_type" name="coupon_type" class="form-control" tabindex="-1">
                        <option value="">Select Coupon Type</option>
                        <?php
                        if(!empty($coupon_type)){
                          $i=1;
                          foreach($coupon_type as $key=>$val){
                            if(set_value('coupon_type') == $key || $coupon_data[0]->is_special_code == $key){
                              echo '<option value="'.$key.'" selected>'.$val.'</option>';
                            }else{

                              echo '<option value="'.$key.'">'.$val.'</option>';
                            }
                            $i++;
                          }
                        }
                        ?>
                      </select>
                    </div>  
              </div> 

            </div>
          </div>  
        </div> 
          <input type="hidden" name="coupon_id" id="coupon_id" value="<?php echo 1;//echo $coupon_id; ?>">
         <!--  <input type="hidden" name="store_id" id="store_id" value="<?php if(isset($coupon_data[0]->id) && $coupon_data[0]->id!=''){ echo $coupon_data[0]->id; } ?>"> -->
      </div><!-- /.box-body -->

        <div class="box-footer">
          <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Save</button>
          <a href="<?php echo base_url(); ?>CouponSettings"><button class="btn btn-primary btn-sm btn-rad" type="button"><i class="fa fa-reply"></i> Back</button></a>
          <button type="reset" class="btn btn-default btn-sm"><i class="fa fa-close"></i> Cancel</button>
        </div>
      </form>
    </div>   
  </section>


<script type="text/javascript">
  $( document ).ready(function() {
	  
	$('#show_on_web').change(function () {
		if($('#show_on_web').prop('checked'))
		{
			$('#show_on_web').val('1');
		}else
		{
			$('#show_on_web').val('0');
		}
		
	});
	
	$('#individual_use_only').change(function () {
		if($('#individual_use_only').prop('checked'))
		{
			$('#individual_use_only').val('1');
		}else
		{
			$('#individual_use_only').val('0');
		}
		
	});

$('#stylecracker_discount').change(function () {
    if($('#stylecracker_discount').prop('checked'))
    {
      $('#stylecracker_discount').val('1');
    }else
    {
      $('#stylecracker_discount').val('0');
    }
    
  });

   $('#showEmailPopUp').change(function () {
      if($('#showEmailPopUp').prop('checked'))
      {
        $('#showEmailPopUp').val('1');
      }else
      {
        $('#showEmailPopUp').val('0');
      }    
    });
	
   /* $("#date_from").datepicker({ dateFormat: "yy-mm-dd" , endDate: "+0d", autoclose: true });
    $("#date_to").datepicker({ dateFormat: "yy-mm-dd", endDate: "+0d", autoclose: true });  */  
    $("#date_from").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii" , autoclose: true });
    $("#date_to").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii" , autoclose: true });

    $('.nav-item-discount').addClass("active");
    $('.nav-coupon-setting').addClass("active");
    
    var products = [<?php echo $products; ?>];
    get_store_products('<?php echo $coupon_data[0]->brand_id; ?>',products);
  });

 
</script>
