<?php  //echo "<pre>"; print_r($stores);exit;
?>
<style>
.block-flat, .block-wizard {
  margin-bottom: 40px;
  padding: 20px 20px;
  background: #FFF;
  border-radius: 3px;
  -webkit-border-radius: 3px;
  border-left: 1px solid #efefef;
  border-right: 1px solid #efefef;
  border-bottom: 1px solid #e2e2e2;
  box-shadow: 0 1px 0 0 rgba(0, 0, 0, 0.04);
}
</style>


<script>
   $(document).ready(function() {
    //for first textbox focus.

      //for image upload
      $("#images").fileinput({
          'showPreview' : true,
           'maxFileCount' : 1,
          'allowedFileExtensions' : ['jpg', 'jpeg', 'png','gif'],
          'elErrorContainer': '#errorBlock',
          'maxFileSize' : 2048,
          'showUpload' : false,
          'layoutTemplates':'modal',
          'uploadAsync': false,
          'uploadUrl': "<?php echo site_url('product/product_addedit'); ?>" ,
          'fileActionSettings' :{
          'uploadIcon' : '',
          'uploadClass' : '',
          'uploadTitle' : '',
          'initialPreview': [
              "<img src='/images/desert.jpg' class='file-preview-image' alt='Desert' title='Desert'>",
              "<img src='/images/jellyfish.jpg' class='file-preview-image' alt='Jelly Fish' title='Jelly Fish'>",
           ],
          }
      });
   });

 $(document).on('change','#category',function(){
      if($('#category option:selected').val() == 0){
        var opt = $('<option />');
        opt.val(0);
        opt.text('Sub Category');
        $('#sub_category').empty().append(opt);
      }else{
        var category_id = { category:$('#category option:selected').val() };
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('product/get_sub_category'); ?>",
            data:{catid : category_id },
            dataType:"json",//return type expected as json
            success: function(subcat){
                var opt = $('<option />');
                opt.val(0);
                opt.text('Sub Category');
                $('#sub_category').empty().append(opt);
                for (var i = 0; i < subcat.length; i++) {
                    var opt = $('<option />');
                    opt.val(subcat[i]['id']);
                    opt.text(subcat[i]['name']);
                    $('#sub_category').append(opt);
                }
              //console.log(subcat[0].id);
            },
        });
      }
});

 toggleDiv = function(id){
  var variation_type = {variation_type:id};
  $("#var_type_img_"+id).show();
  $.ajax({
            type: "POST",
            url: "<?php echo site_url('product/get_variation_value'); ?>",
            data:variation_type,
            dataType:"json",//return type expected as json
            success: function(varvalue){
              $( '#variation_typevalue_'+id ).toggle( "slow", function() {});
              $("#var_type_img_"+id).hide();
              $('#variation_typevalue_'+id).html('');
              for (var i = 0; i < varvalue.length; i++) {
                  var varvalueString = '<div class="col-md-6"><div class="checkbox"><label><input name="variation_value_'+i+'[]" type="checkbox" value="'+varvalue[i]['id']+'" id="variation_value_check_'+varvalue[i]['id']+'">';
                  varvalueString += varvalue[i]['name'];
                  varvalueString += '</label> </div></div>';
                  $('#variation_typevalue_'+id).append(varvalueString);
                }
            },
        });

 }
 $(document).on('change','#sub_category',function(){

   if($('#sub_category option:selected').val() == 0){
      $('#variation_type').html('');
   }
   else{
      var subcategory_id = { subcategory:$('#sub_category option:selected').val() };
      $.ajax({
            type: "POST",
            url: "<?php echo site_url('product/get_variation_type'); ?>",
            data:subcategory_id,
            dataType:"json",//return type expected as json
            success: function(vartype){
              $('#variation_type').html('');
              for (var i = 0; i < vartype.length; i++) {
                  var vartypeString = '<div class="col-md-4"><div class="box box-warning box-solid"><div class="box-header with-border"><h3 class="box-title"> <input name="variation_type[]"  onClick=toggleDiv('+vartype[i]['id']+') id="variation_type_check_'+vartype[i]['id']+'" value="'+vartype[i]['id']+'" type="checkbox"/>';
                  vartypeString += vartype[i]['name'];
                  vartypeString +='</h3><div class="box-tools pull-right"> '
                  vartypeString += '</div></div><div class="box-body" style="display: block;"><div style="display: none;" class="row" id="variation_typevalue_';
                  vartypeString += vartype[i]['id'];
                  vartypeString += '"><img src="" style="display:none;" alt="loading..." id=var_type_img_"'+vartype[i]['id']+'"</div></div></div></div>';

                  $('#variation_type').append(vartypeString);
                //console.log(subcat[0].id);

              }


            },
        });
    }
});
</script>
<?php
//echo form_open('productvariation'); ?>
<!-- Content Header (Page header) -->

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Add Product</h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>/product"><i class="fa fa-dashboard"></i> Products</a></li>
      <li class="active">Add Product</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box ">
        <div class="box-header">
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" id="product_form" enctype="multipart/form-data" method="POST">
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">

                <div class="form-group">
                  <label for="ProductName">Product Name <span class="text-red">*</span></label>
                  <input value="" type="text" name="product_name" tabindex="1" id="product_name" placeholder="Enter Product Name" class="form-control">
                  <span class="text-red"> <?php echo form_error('product_name'); ?></span>
                </div>

                <div class="form-group">
                  <label for="ProductDesc">Product Description</label>
                  <textarea tabindex="3" name="product_desc" id="product_desc" placeholder="Enter Description" rows="3" class="form-control"></textarea>
                </div>

                <div class="form-group">
                  <label for="Brand">Brand  </label>
                  <select tabindex="5"  required="Please select any brand" id="brand" name="brand" class="form-control" tabindex="-1">
                    <option>Select Brand</option>
                    <?php
                        foreach($brand as $singlebrand)
                        {
                    ?>
                          <option value="<?php echo $singlebrand->id;?>"><?php echo $singlebrand->name;?></option>
                    <?php
                        }
                    ?>
                  </select>
                </div>

                <div class="form-group">
                  <label for="productTags">Tags</label>
                  <select placeholder="Select Tags" tabindex="19" data-placeholder="Choose Tags" name="product_tags[]" id="product_tags" class="form-control chosen-select" multiple tabindex="">
                    <?php
                      if(!empty($tag))
                     { //check whether the category array is empty.
                      foreach($tag as $singletag)
                      {
                      ?>
                         <option value="<?php echo $singletag->id;?>"><?php echo $singletag->name;?></option>
                      <?php
                      }
                     }
                    ?>
                    </select>
                </div>

                <div class="form-group">
                  <label for="ProductURL">Product URL </label>
                  <input type="text" tabindex="15" placeholder="Enter Product URL" id="product_url" name="product_url" class="form-control">
                </div>
              <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="category">Category  <span class="text-red">*</span></label>
                  <select class="form-control" tabindex="7"   name="category" id="category">
                    <option value="0">Select Category</option>
                    <?php if(empty($category)){ //check whether the category array is empty.
                    ?> <option value="0">No Category Yet</option><?php }
                    else{ // to load the category array.

                      foreach($category as $singlecat)
                      {
                        ?>
                          <option value="<?php echo $singlecat->id;?>"><?php echo $singlecat->name;?></option>
                        <?php
                      }
                      ?>
                    <?php //category array ends
                    }
                    ?>
                  </select>
                   <span class="text-red"> <?php echo form_error('category'); ?></span>
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                  <label for="subCategory">Sub Category  <span class="text-red">*</span></label>
                  <select tabindex="9" class="form-control"  name="sub_category" id="sub_category">
                    <option value="0">Select Subcategory</option>
                  </select>
                  <span class="text-red"> <?php echo form_error('sub_category'); ?></span>
                </div>
                </div>
                <div class="col-md-12">
                <div class="form-group">
                  <div class="row" id="variation_type"></div>
                </div>
                </div>

                <div class="col-md-6">

                <!--div class="form-group">
                  <label for="exampleInputEmail1">Tags </label>
                  <input type="text" placeholder="Enter Tags" id="exampleInputEmail1" class="form-control">
                </div-->
                <div class="form-group">
                  <label for="budget">Price Range  <span class="text-red">*</span></label>
                  <?php echo form_dropdown('budget', $budget, '', 'tabindex="10" class="form-control" id="budget"');?>
                  <span class="text-red"> <?php echo form_error('budget'); ?></span>
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                  <label for="Price">Price <span class="text-red">*</span></label>
                  <input type="text" tabindex="11" placeholder="Enter Price" name="product_price" id="product_price" class="form-control"> <span class="text-red"> <?php echo form_error('product_price'); ?></span>
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                  <label for="targetMarket">Target Market</label>
                  <select placeholder="Select Target Market" tabindex="23" id="product_target_market" name="product_target_market[]" class="form-control chosen-select" multiple>
                     <?php
                      if(!empty($target_market))
                     { //check whether the target_market array is empty.
                        foreach($target_market as $singletarget)
                        {
                     ?>
                           <option value="<?php echo $singletarget->id;?>"><?php echo $singletarget->target_market;?></option>
                    <?php
                        }
                     }
                    ?>
                  </select>
                </div>
                </div>
                <div class="col-md-6">
                 <div class="form-group">
                  <label for="ConsumeType">Consumer Type</label>
                  <select placeholder="Select Consumer Type" tabindex="25" id="product_consumer_type" name="product_consumer_type[]" class="form-control" >
                    <option value="">Select Consumer Type </option>
                    <option value="Maternity &amp; Baby" >Maternity & Baby</option>
                    <option value="Female" >Female</option>
                    <option value="Male" >Male</option>
                    <option value="Kidswear" >Kidswear</option>
                    <option value="Plus Male" >Plus Male</option>
                    <option value="Plus Female" >Plus Female</option>
                  </select>
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                  <label for="ProductAvail">Product Availability  <span class="text-red">*</span></label>
                  <select placeholder="Select Product Availabilty" tabindex="27" id="product_avail" value="" name="product_avail[]"class="form-control chosen-select" multiple>
                    <?php
                      if(!empty($stores))
                     { //check whether the target_market array is empty.
                        foreach($stores as $singlestore)
                        {
                     ?>
                           <option value="<?php echo $singlestore->id;?>"><?php echo $singlestore->brand_store_name;?></option>
                    <?php
                        }
                     }
                    ?>
                  </select>
                  <span class="text-red"> <?php echo form_error('product_avail'); ?></span>
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                  <label for="Rating">Rating</label>
                  <input type="text" tabindex="10" placeholder="Enter your rating" name="product_rate" id="product_rate" class="form-control">
                </div>
              </div>
            </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="Uploadimage">Upload Product Image</label>
                  <!--input type="file" id="product_image" name="product_image" tabindex="13"-->
                    <input id="images" name="images" class="file" type="file" >
                    <span class="help-block">Max file size: 2MB. | Allowed format: .jpg, .jpeg, .png, .gif</span>
                     <span class="text-red"> <?php if(isset($product_image_error)) echo $product_image_error;?></span>
                </div>

                <div class="row">
                <div class="col-md-6">
                <div class="form-group">
                 <label for="BodyPart">Body Part</label>
                  <select placeholder="Select Body Part" tabindex="6" id="body_part" name="body_part" class="form-control" >
                    <option value="">Select Body Part </option>
                    <option value="Head gear / Hair">Head gear / Hair</option>
                    <option value="Ears">Ears</option>
                    <option value="Eyes">Eyes</option>
                    <option value="Nose">Nose</option>
                    <option value="Neck">Neck</option>
                    <option value="Arms">Arms</option>
                    <option value="Wrist">Wrist</option>
                    <option value="Fingers">Fingers</option>
                    <option value="Torso">Torso</option>
                    <option value="Feet">Feet</option>
                    <option value="Shoes">Shoes</option>
                    <option value="Accessories">Accessories</option>
                  </select>
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                 <label for="BodyType">Body Type</label>
                  <?php echo form_dropdown('body_type', $body_type, '', 'tabindex="8" class="form-control" id="body_type"');?>
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                 <label for="BodyShape">Body Shape</label>
                  <?php echo form_dropdown('body_shape', $body_shape, '', 'tabindex="9" class="form-control" id="body_shape"');?>
                </div>
                </div>
                <div class="col-md-6">

                <div class="form-group">
                 <label for="Personality">Personality</label>
                  <?php echo form_dropdown('personality', $personality, '', 'tabindex="10" class="form-control" id="personality"');?>
                </div>
                </div>
                <div class="col-md-6">

                 <div class="form-group">
                 <label class="control-label" for="age">Age Group</label>
                  <?php echo form_dropdown('age', $age, '', 'tabindex="11" class="form-control" id="age"');?>
                </div>
                </div>
                <div class="col-md-6">

                <div class="form-group">
                  <label class="control-label">Active</label> &nbsp;&nbsp;&nbsp;&nbsp;
                  <div>  <label class="radio-inline"> <input type="radio"  name="active" value="1">Yes</label>
                    <label class="radio-inline"> <input type="radio" name="active" value="0"> No</label>
                  </div>
                </div>
                </div>
                </div>

              </div>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
          <button class="btn btn-sm btn-primary" tabindex="30" type="submit"><i class="fa fa-save"></i> Save</button>
          <button class="btn btn-sm btn-default" tabindex="31" type="reset"><i class="fa fa-close"></i> Cancel</button>
          <!-- <button class="btn btn-primary" type="submit">Edit</button>
          <button class="btn btn-primary" type="submit">Approved/Pending</button> -->
        </div>
        </form>
      </div>
    </div>
  </div>
</section><!-- /.content -->
<script type="text/javascript">
      var config = {
        '.chosen-select'           : {},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
        '.chosen-select-width'     : {width:"95%"}
      }
      for (var selector in config) {
        $(selector).chosen(config[selector]);
      }

$( document ).ready(function() {
	$('.nav-item-product').addClass("active");
});
</script>
