
<?php
$users_menu=$this->data['users_menu'];

//echo $shoppingclicks;
//print_r($signup);
//print_r($dashboard);exit;
//echo "Auclick==".$auclick;exit;
//echo $this->session->userdata('user_id');
?>
<script src="<?php echo base_url(); ?>assets/js/dbreport.js"></script>
  <?php if($this->session->userdata('role_id')!=6){
      ?>
<section class="content-header">
  <h1>Universal Cart</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Dashboard</a></li>
  </ol>
</section>

<section class="content">
  
  <form name="myform" id="myform" class="frm-search" action="<?php echo base_url(); ?>All_report" method="post">
  <input type="hidden"  name="is_download" id="is_download" value="0" />
    <div class="row">
      <div class="col-md-2">
        <div class="input-group">
          <input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm" id="date_from" value="<?php echo set_value('date_from'); ?>">
        </div>
      </div>
      <div class="col-md-2">
        <div class="input-group">
          <input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="<?php echo set_value('date_to'); ?>">
          <!-- <div class="input-group-btn">
            <button type="submit" class="btn btn-sm btn-default" id="submit"><i class="fa fa-search"></i></button>
          </div>-->
        </div>
      </div>
      <div class="col-md-2">
        <a class="btn btn-default btn-sm" href="<?php echo base_url(); ?>home"><i class="fa fa-refresh"></i> Reset</a>
      </div>
	  
    </div>
	<div class="table-responsive">
      <table class="table table-bordered table-striped dataTable" id="example2">
        <thead>
          <tr>
            <th>Report Name</th>
            <th>Download link</th>
          </tr>
        </thead>
        <tbody>
			  <tr>
				<td><?php echo "Abandoned Cart Data"; ?></td>
				<td> <!-- <a href="<?php echo base_url(); ?>All_report/download_exel/<?php echo set_value('date_from'); ?>"> 
					  </a>--> <button class="btn btn-xs btn-primary" onclick="downloadExcel(1);"><i class="fa fa-eye"></i> Download</button> 
					   </td>
			  </tr>
			  
				<tr>
					<td><?php echo "Failed Order Data"; ?></td>
					<td> <!-- <a href="<?php echo base_url(); ?>All_report/download_exel/<?php echo set_value('date_from'); ?>"> 
						  </a>--> <button class="btn btn-xs btn-primary" onclick="downloadExcel(2);"><i class="fa fa-eye"></i> Download</button> 
						   </td>
				</tr>	 
				  
      <?php if($this->session->userdata('user_id')=='18126'){
      ?>
      <tr>
          <td><?php echo "Failed Online Order Data"; ?></td>
          <td><button class="btn btn-xs btn-primary" onclick="downloadExcel(3);"><i class="fa fa-eye"></i> Download</button> 
          </td>
      </tr> 
      <?php } ?>
                  </tbody>
                </table>
              </div>
  </form>
 
              </div>
              </div>
              </div>
              </div>
		
  <?php } ?>     
                </section>

                            <!-- ChartJS 1.0.1 -->
<script src="<?php echo base_url(); ?>assets/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function () {
  $("#date_from").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true});
  $("#date_to").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true });
	
  $(".nav-tabs a").click(function(){
	$(this).tab('show');
  });
  $('.nav-tabs a').on('shown.bs.tab', function(event){
	var x = $(event.target).text();         // active tab
	var y = $(event.relatedTarget).text();  // previous tab
	$(".act span").text(x);
	$(".prev span").text(y);
  });
});

function downloadExcel(i)
	{   
		$("#is_download").val('1');
        var dateFrom = $("#date_from").val();
        var dateTo = $("#date_to").val();
		var is_download = $("#is_download").val();
		if(i == 1){
			var newhref = '<?php echo base_url(); ?>'+'All_report/download_exel/downloadExcel'+'/'+dateFrom+'/'+dateTo+'/'+is_download;
		}else if(i == 2){
			var newhref = '<?php echo base_url(); ?>'+'All_report/failed_order_exel/downloadExcel'+'/'+dateFrom+'/'+dateTo+'/'+is_download;
		}else if(i == 3){
      var newhref = '<?php echo base_url(); ?>'+'All_report/failed_onlineorder_excel/downloadExcel'+'/'+dateFrom+'/'+dateTo+'/'+is_download;
    }
		// alert(newhref);
        $('#myform').attr('action',newhref);
       
    }
	

</script>
