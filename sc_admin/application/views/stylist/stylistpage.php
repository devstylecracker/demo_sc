<?php

	/*Added for facebook access_token */
	$access_token = '';$access_token_array = array();$access_token_insta='';$access_token_insta_arr=array();
	$access_token_array = unserialize($user_data[0]['fb_data']);
	$access_token = $access_token_array['access_token'];
	/*Added for facebook access_token */

  /*Added for instagram access_token*/
  $access_token_insta_arr = json_decode($user_data[0]['instagram_data']);
  //echo '<pre>';print_r($access_token_insta_arr);
  $access_token_insta = $access_token_insta_arr->access_token;
  /*Added for instagram access_token*/

     /*Added for facebook google pic*/
        if($user_data[0]['registered_from'] == 'facebook'){
          $profilePic = 'https://graph.facebook.com/'.@$user_data[0]['facebook_id'].'/picture?type=large';
        }elseif($user_data[0]->registered_from == 'google'){
           $profilePic = $this->config->item('profile_image_path').@$user_data[0]['profile_pic'];
        }elseif($user_data[0]['profile_pic'] != '')
        {
           //$profilePic = $this->config->item('profile_image_path').@$user_data[0]['profile_pic'];
          $profilePic = @$user_data[0]['profile_pic'];
        }else
        {
          if($user_data[0]['gender']==2)
          {
            $profilePic = SC_SITE_URL.'assets/images/profile/profile-men-n.png';
          }else
          {
            $profilePic = SC_SITE_URL.'assets/images/profile/profile-women-n.png';
          }
        }


    /*Added for facebook google pic*/
     $question_ans = array();
     if(!empty($pa_data)){
        foreach($pa_data as $val){
             $question_ans[$val['question_id']] = $val['answer'];
        }
     }
?>
<section class="content-header">
  <h1>User Management</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url();?>">Home</a></li>
    <li class="active">Stylist Management</li>
  </ol>
  <input type="hidden" name="user_id" id="user_id" value="<?php echo $this->uri->segment(3); ?>">
</section>
<section class="content">
  <div class="box box-widget widget-user-2 box-default" style="border-top-style:solid;">
    <div class="box-body">
      <div class="row">
        <div class="col-md-1">
          <div class="widget-user-image text-center">
            <img  src="<?php echo $profilePic; ?>" alt="User Avatar" style="width: 80px;" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
          </div>
        </div>


        <div class="col-md-4  text-muted">
          <div><strong>Email:</strong> <span><?php echo $user_data[0]['email']; ?></span></div>
          <div><strong>Mobile:</strong> <span><?php echo $user_data[0]['contact_no']; ?></span></div>
          <div><strong>Age:</strong> <span><?php echo $userAgeRange;  ?></span></div>
          <div><strong>Body Shape:</strong> <span><?php echo $userBodyShape;  ?></span></div>
        </div>

        <div class="col-md-3 text-muted">
           <div><strong>Personal Style Pref 1:</strong> <span><?php  echo $userStylePref1;  ?></span></div>
           <div><strong>Personal Style Pref 2:</strong> <span><?php  echo $userStylePref2;  ?></span></div>
           <div><strong>Personal Style Pref 3:</strong> <span><?php echo $userStylePref3; ?></span></div>
           <div><strong>Brands Preference:</strong> <span><?php echo $userBrandPref; ?></span></div>
           <!--<div><strong></strong> <span></span></div>-->
        </div>

       <!-- <div class="col-md-4 text-muted">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <input name="date_from" placeholder="Discount Start Date" class="form-control input-sm datetimepicker" id="date_from" value="" type="text">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <input name="date_to" placeholder="Discount End Date" class="form-control input-sm datetimepicker" id="date_to" value="" type="text">
                <div class="input-group-btn"></div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input name="max_amount" id="max_amount" value="max_amount" placeholder="Max Amount" class="form-control input-sm" type="number">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input name="discount" id="discount" value="discount" placeholder="Discount %" class="form-control input-sm" type="number">
              </div>
            </div>
            <div class="col-md-4">
              <input class="btn btn-primary btn-sm" name="save" value="Save" type="button">
            </div>
          </div>
        </div>-->

      </div>
    </div>
  </div>


  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">User Info</a></li>
          <!-- <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">Collections</a></li>
          <li class=""><a href="#tab_5" data-toggle="tab" aria-expanded="false">Events</a></li> -->
          <!-- <li class="" onclick="dispalyNotification(<?php echo $this->uri->segment(3); ?>)"><a href="#tab_6" data-toggle="tab" aria-expanded="false">Notification</a></li> -->
          <li class="" id="abundant_cart1" onclick="dispalyCartDetail(<?php echo $this->uri->segment(3); ?>)"><a href="#tab_3" data-toggle="tab" aria-expanded="false">Cart</a></li>
          <!-- <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">User History</a></li> -->
          <li class="" id="order_history" onclick="dispalyOrderDetail(<?php echo $this->uri->segment(3); ?>)"><a href="#tab_7" data-toggle="tab" aria-expanded="false">Order History</a></li>
          <li class="" onclick="dispalyWardrobe(<?php echo $this->uri->segment(3); ?>)" ><a href="#tab_8" data-toggle="tab" aria-expanded="false">User Wardrobe</a></li>

		<!--<li class="" ><a href="<?php echo base_url(); ?>scbox/scbox_edit/<?php echo $this->uri->segment(3); ?>" target="_blank">User SCbox data</a></li>-->
		<li class="" onclick="get_user_scbox_data(<?php echo $this->uri->segment(3); ?>);" ><a href="#tab_11" data-toggle="tab" aria-expanded="false">User SCbox Data</a></li>
    		  <li class="" id="fb_images" onclick="get_fb_images(<?php echo $this->uri->segment(3); ?>)"><a href="#tab_9" data-toggle="tab" aria-expanded="false">FB Images</a></li>

          <li class="" id="insta_images" onclick="get_insta_images(<?php echo $this->uri->segment(3); ?>);"><a href="#tab_10" data-toggle="tab" aria-expanded="false">Instagram Images</a></li>

           <li class="" id="user_feedback" onclick="get_user_feedback(<?php echo $this->uri->segment(3); ?>);"><a href="#tab_12" data-toggle="tab" aria-expanded="false">User Feedback</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab_1">

          <!--   <div class="box box-widget widget-user"> -->
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <!-- <div class="widget-user-header bg-black" style="background: url('<?php echo base_url(); ?>assets/admintheme/dist/img/photo1.png') center center; height: 220px;">
                <h3 class="widget-user-username"><?php echo $user_data[0]['user_name']!='' ? $user_data[0]['user_name'] :$user_data[0]['first_name'] ; ?></h3>
                <h5 class="widget-user-desc"><?php echo $user_data[0]['contact_no']; ?></h5>
              </div>
              <div class="widget-user-image">
                <img class="img-circle" src="<?php echo base_url(); ?>assets/admintheme/dist/img/user3-128x128.jpg" alt="User Avatar" style="width: 120px;">
              </div>

            </div> -->

            <div class="row">
              <div class="col-md-4">
                <div class="box box-solid box-primary ">
                  <div class="box-header with-border">
                    <h3 class="box-title">User Info</h3>
                  </div>
                  <div class="box-body">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped">
                        <tbody>
                          <tr>
                            <th style="width:180px;">ID</th>
                            <td><?php echo $user_data[0]['id']; ?></td>
                          </tr>
                          <!-- <tr>
                            <th>Username</th>
                            <td><?php echo $user_data[0]['user_name']; ?></td>
                          </tr> -->
                          <tr>
                            <th>First Name</th>
                            <td><?php echo $user_data[0]['first_name']; ?></td>
                          </tr>

                          <tr>
                            <th>Last Name</th>
                            <td><?php echo $user_data[0]['last_name']; ?></td>
                          </tr>
                          <tr>
                            <th>Email</th>
                            <td><?php echo $user_data[0]['email']; ?></td>
                          </tr>
                          <tr>
                            <th>Mobile</th>
                            <td><?php echo $user_data[0]['contact_no']; ?></td>
                          </tr>
                          <tr>
                            <th>Gender</th>
                            <td><?php
                                if($user_data[0]['gender']==2)
                                {
                                  echo 'Male';
                                }else
                                {
                                  echo 'Female';
                                }
                             ?></td>
                          </tr>

                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-4">
                <div class="box box-solid box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">PA Info</h3>
                  </div>
                  <div class="box-body">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped">
                        <tbody>
                           <?php
                                if($user_data[0]['gender']==1)
                                {
                            ?>
                              <tr>
                                <th>Age</th>
                                <td><?php echo $userAgeRange;  ?></td>
                              </tr>
                              <tr>
                                <th style="width:180px;">Body Shape</th>
                                <td><?php  echo $userBodyShape;  ?></td>
                              </tr>
                              <tr>
                                <th>Personal Style Pref 1</th>
                                <td><?php  echo $userStylePref1;  ?></td>
                              </tr>
                              <tr>
                                <th>Personal Style Pref 2</th>
                                <td><?php  echo $userStylePref2;  ?></td>
                              </tr>
                              <tr>
                                <th>Personal Style Pref 3</th>
                                <td><?php  echo $userStylePref3;  ?></td>
                              </tr>
                              <tr>
                                <th>Brand Preference</th>
                                <td><?php echo $userBrandPref; ?></td>
                              </tr>

                              <tr><td></td><td>&nbsp;</td></tr>
                            </table>
                          <?php
                                }else
                                {
                          ?>
                           <tr>
                                <th>Age</th>
                                <td><?php echo $userAgeRange;  ?></td>
                              </tr>
                              <tr>
                                <th style="width:180px;">Body Shape</th>
                                <td><?php  echo $userBodyShape;  ?></td>
                              </tr>
                              <tr>
                                <th>Personal Style Pref 1</th>
                                <td><?php  echo $userStylePref1;  ?></td>
                              </tr>
                              <tr>
                                <th>Personal Style Pref 2</th>
                                <td><?php  echo $userStylePref2;  ?></td>
                              </tr>
                              <tr>
                                <th>Personal Style Pref 3</th>
                                <td><?php  echo $userStylePref3;  ?></td>
                              </tr>
                              <tr>
                                <th>Brand Preference</th>
                                <td><?php echo $userBrandPref; ?></td>
                              </tr>

                              <tr><td></td><td>&nbsp;</td></tr>
                            <!--<tr><td></td><td>&nbsp;</td></tr>-->
                            <?php
                            }
                      ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="box box-solid box-primary ">
                  <div class="box-header with-border">
                    <h3 class="box-title">Extra Info</h3>
                  </div>
                  <div class="box-body">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped">
                        <tbody>

                          <tr>
                            <th>SC Box Mobile</th>
                            <td><?php echo $sc_box_mobile_number[0]['meta_value']; ?></td>
                          </tr>
                          <tr>
                            <th>User Platform</th>
                            <td><?php echo $user_data[0]['registered_from']; ?></td>
                          </tr>
                          <tr>
                            <th>First Login</th>
                            <td><?php echo $extra_info[0]['login_time']?></td>
                          </tr>
                          <tr>
                            <th>Last Login</th>
                            <td><?php echo $last_login[0]['login_time']; ?></td>
                          </tr>
                          <tr>
                            <th>Returning Count</th>
                            <td><?php echo $visit_count_user[0]['visit_count']?></td>
                          </tr>
                          <tr>
                            <th colspan="2"> Chat with User</th>
                          </tr>
                          <tr>
                            <th colspan="2"> </th>
                          </tr>
                          <tr>
                            <th colspan="2"> </th>
                          </tr>

                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

            </div>

          </div>
          <!-- /.tab-pane -->
          <div class="tab-pane" id="tab_2">

            <div class="row">
              <div class="col-md-6">
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Product View</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="products-list-wrp">
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14622792724380.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14622792724380.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14622792724380.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                      </ul>
                    </div>
                    <!-- /.box-body -->

                  </div>
                </div>

                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title"> Collections View</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Boxi Feet Short Sleeves Tee Brown</h6>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14622792724380.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Boxi Feet Short Sleeves Tee Brown</h6>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14622792724380.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Boxi Feet Short Sleeves Tee Brown</h6>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14622792724380.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Boxi Feet Short Sleeves Tee Brown</h6>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14622792724380.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <!-- /.box-body -->
                </div>

                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Notification View</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">50% off on Chrismas</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Remove this from user list"><i class="fa fa-close"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                          <div class="item-info">
                            <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>

                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">50% off on Chrismas</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Remove this from user list"><i class="fa fa-close"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                          <div class="item-info">
                            <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">50% off on Chrismas</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Remove this from user list"><i class="fa fa-close"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                          <div class="item-info">
                            <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                    <!-- /.box-body -->
                  </div>
                </div>

                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Notification View(Short)</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="products-list-wrp">
                      <ul style="padding:0; list-style-position: inside;">
                        <li>
                          50% off on Chrismas
                        </li>
                        <li>
                          50% off on Chrismas
                        </li>
                        <li>
                          50% off on Chrismas
                        </li>
                      </ul>
                    </div>
                    <!-- /.box-body -->
                  </div>
                </div>


              </div>
              <div class="col-md-6">
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Events View</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Stylecracker Borough Mumbai</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Remove this from user list"><i class="fa fa-close"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                          <div class="item-info">
                            <div><span class="title">Start Date:</span> 18 Dec 2016
                            </div>
                            <div><span class="title">End Date:</span> 18 Dec 2016
                            </div>
                            <div><span class="title">Venue:</span> Mahalaxmi Racecourse, Mumbai
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Stylecracker Borough Mumbai</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Remove this from user list"><i class="fa fa-close"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                          <div class="item-info">
                            <div><span class="title">Start Date:</span> 18 Dec 2016
                            </div>
                            <div><span class="title">End Date:</span> 18 Dec 2016
                            </div>
                            <div><span class="title">Venue:</span> Mahalaxmi Racecourse, Mumbai
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Stylecracker Borough Mumbai</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Remove this from user list"><i class="fa fa-close"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                          <div class="item-info">
                            <div><span class="title">Start Date:</span> 18 Dec 2016
                            </div>
                            <div><span class="title">End Date:</span> 18 Dec 2016
                            </div>
                            <div><span class="title">Venue:</span> Mahalaxmi Racecourse, Mumbai
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>

                    <!-- /.box-body -->
                  </div>
                </div>
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Events View(Short)</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="products-list-wrp">
                      <ul style="padding:0; list-style-position: inside;">
                        <li>
                          Stylecracker Borough Mumbai
                        </li>
                        <li>
                          Stylecracker Borough Pune
                        </li>
                        <li>
                          Stylecracker Borough Mumbai
                        </li>
                      </ul>
                      <!-- /.box-body -->
                    </div>
                  </div>
                </div>

                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Brands View</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="products-list-wrp">
                      <ul style="padding:0; list-style-position: inside;">
                        <li>SHIVANSH FASHIONS</li>
                        <li>SHOE THAT FITS YOU</li>
                        <li>SHOP THAT OUTFIT</li>
                        <li>SILVETTE</li>
                        <li>SNUGONS</li>
                        <li>SOUL TREE</li>
                        <li>SPRING BREAK</li>
                        <li>SSOUL</li>
                        <li>STRAWLOUS</li>
                        <li>STREETHOPPER</li>
                      </ul>
                      <!-- /.box-body -->
                    </div>
                  </div>
                </div>

                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Categories View</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="products-list-wrp">
                      <ul style="padding:0; list-style-position: inside;">
                        <li>
                          TOPS & SHIRTS</li>
                        <li>
                          T-Shirts</li>
                        <li>
                          Crop Tops</li>
                        <li>
                          Shirts</li>
                        <li>
                          Tops</li>
                        <li>
                          Sweatshirts</li>
                        <li>
                          BOTTOMS</li>
                        <li>
                          Jeans & Jeggings</li>
                        <li>
                          Skirts</li>
                        <li>
                          Trousers & Pants</li>
                      </ul>
                      <!-- /.box-body -->
                    </div>
                  </div>
                </div>
              </div>

            </div>

          </div>
          <!-- /.tab-pane -->
          <div class="tab-pane" id="tab_3">
                <div class="box box-solid1 box-info">
                  <div class="box-header with-border">
                    <h3 class="box-title">Cart Details</h3>

                    <div class="box-tools pull-right">
                          <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#products-popup" id="popup_button" data-attr="stylist-abundant-cart">Add Products</button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th style="width:70px;">Image</th>
                            <th>Item</th>
                            <th>Size</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Date Added</th>
                            <th>DaysLapsed</th>
                            <th width="70">Action</th>
                          </tr>
                        </thead>
                        <tbody id="abundant_cart_body">
                          <?php
                              if(!empty(@$cart))
                              {
                                $i=1;
                                foreach($cart as $value)
                                {
                          ?>
                          <tr>
                            <td><?php echo $i; ?></td>
                            <td>
                              <div class="img-wrp">
                                <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_310/"<?php echo $value['product_image']; ?> onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';" >
                              </div>
                            </td>
                            <td><?php echo $value['product_name']; ?></td>
                            <td><?php echo $value['size_text']; ?></td>
                            <td >
                              <?php echo $value['product_price']; ?>
                            </td>
                            <td>
                              <button type="button" class="btn btn-box-tool"><i class="fa fa-times"></i></button>
                            </td>
                          </tr>
                          <?php
                              }
                            }
                          ?>

                        </tbody>
                        <tbody>
                        </tbody id="cart_coupon_body" >
                        </tbody>


                      <tbody id="StylistAddedProduct">
                        </tbody>
                      </table>
                      </div>
                      <div class="spacer"><br></div>
                    <!-- /.table-responsive -->
                    <!-- <div class="form-group clearfix">
                      <div class="col-md-2 input-group">
                          <input name="couponcode" value="" id="couponcode" class="form-control input-sm pull-right1" placeholder="Enter coupon code" required="" type="text">
                            <div class="input-group-btn">
                              <button class="btn btn-sm btn-default" onclick="applyCoupon(<?php echo $this->uri->segment(3); ?>);" >Apply Coupon</button>
                            </div>
                        </div>
                    </div> -->

                  </div>
                  <!-- /.box-body -->
            </div>
          </div>
          <!-- /.tab-pane -->
          <div class="tab-pane" id="tab_4">
            <div class="row">
              <div class="col-md-6">
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Already Assigned Collections</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Boxi Feet Short Sleeves Tee Brown</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Remove this colloction from user list"><i class="fa fa-close"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14622792724380.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Boxi Feet Short Sleeves Tee Brown</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Remove this colloction from user list"><i class="fa fa-close"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14622792724380.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Boxi Feet Short Sleeves Tee Brown</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Remove this colloction from user list"><i class="fa fa-close"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14622792724380.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Boxi Feet Short Sleeves Tee Brown</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Remove this colloction from user list"><i class="fa fa-close"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14622792724380.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <!-- /.box-body -->

                </div>
              </div>
              <div class="col-md-6">
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Assign Collection to User</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Boxi Feet Short Sleeves Tee Brown</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Assign this colloction to user"><i class="fa fa-plus"></i>
                          </button>
                        </div>
                      </div>

                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14622792724380.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Boxi Feet Short Sleeves Tee Brown</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Assign this colloction to user"><i class="fa fa-plus"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14622792724380.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Boxi Feet Short Sleeves Tee Brown</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Assign this colloction to user"><i class="fa fa-plus"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14622792724380.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Boxi Feet Short Sleeves Tee Brown</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Assign this colloction to user"><i class="fa fa-plus"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14622792724380.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14539782755564.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <!-- /.box-body -->

                </div>
              </div>
            </div>
          </div>
          <!-- /.tab-pane -->
          <div class="tab-pane" id="tab_5">
            <div class="row">
              <div class="col-md-6">
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Already Assigned Events</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Stylecracker Borough Mumbai</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Remove this from user list"><i class="fa fa-close"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                          <div class="item-info">
                            <div><span class="title">Start Date:</span> 18 Dec 2016
                            </div>
                            <div><span class="title">End Date:</span> 18 Dec 2016
                            </div>
                            <div><span class="title">Venue:</span> Mahalaxmi Racecourse, Mumbai
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Stylecracker Borough Mumbai</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Remove this from user list"><i class="fa fa-close"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                          <div class="item-info">
                            <div><span class="title">Start Date:</span> 18 Dec 2016
                            </div>
                            <div><span class="title">End Date:</span> 18 Dec 2016
                            </div>
                            <div><span class="title">Venue:</span> Mahalaxmi Racecourse, Mumbai
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Stylecracker Borough Mumbai</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Remove this from user list"><i class="fa fa-close"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                          <div class="item-info">
                            <div><span class="title">Start Date:</span> 18 Dec 2016
                            </div>
                            <div><span class="title">End Date:</span> 18 Dec 2016
                            </div>
                            <div><span class="title">Venue:</span> Mahalaxmi Racecourse, Mumbai
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>

                    <!-- /.box-body -->
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Assign Events</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Stylecracker Borough Mumbai</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Assign this to user"><i class="fa fa-plus"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                          <div class="item-info">
                            <div><span class="title">Start Date:</span> 18 Dec 2016
                            </div>
                            <div><span class="title">End Date:</span> 18 Dec 2016
                            </div>
                            <div><span class="title">Venue:</span> Mahalaxmi Racecourse, Mumbai
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Stylecracker Borough Mumbai</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Assign this to user"><i class="fa fa-plus"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                          <div class="item-info">
                            <div><span class="title">Start Date:</span> 18 Dec 2016
                            </div>
                            <div><span class="title">End Date:</span> 18 Dec 2016
                            </div>
                            <div><span class="title">Venue:</span> Mahalaxmi Racecourse, Mumbai
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">Stylecracker Borough Mumbai</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Assign this to user"><i class="fa fa-plus"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                          <div class="item-info">
                            <div><span class="title">Start Date:</span> 18 Dec 2016
                            </div>
                            <div><span class="title">End Date:</span> 18 Dec 2016
                            </div>
                            <div><span class="title">Venue:</span> Mahalaxmi Racecourse, Mumbai
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>

                    <!-- /.box-body -->
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /.tab-pane -->
          <div class="tab-pane" id="tab_6">
            <div class="row">
              <div class="col-md-6">
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Notification sent to User</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">50% off on Chrismas</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Remove this from user list"><i class="fa fa-close"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                          <div class="item-info">
                            <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>

                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">50% off on Chrismas</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Remove this from user list"><i class="fa fa-close"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                          <div class="item-info">
                            <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="products-list-wrp">
                      <div class="header">
                        <h6 class="title">50% off on Chrismas</h6>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Remove this from user list"><i class="fa fa-close"></i>
                          </button>
                        </div>
                      </div>
                      <ul class="products-list product-list-in-box product-list-inline">
                        <li class="item">
                          <div class="product-img">
                            <img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14667456166718.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
                          </div>
                          <div class="item-info">
                            <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                    <!-- /.box-body -->
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Send New Notifications to User</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                      <div class="form-group clearfix">
                          <label for="inputName" class="col-sm-2 control-label">Title</label>
                          <div class="col-sm-10">
                              <input class="form-control" id="inputName" placeholder="Title" name="title" id="title" type="text">
                          </div>
                      </div>
                      <div class="form-group clearfix">
                          <label for="inputExperience" class="col-sm-2 control-label">Content</label>
                          <div class="col-sm-10">
                              <textarea class="form-control" id="content" placeholder="Content" name="content" id="content" ></textarea>
                          </div>
                      </div>
                      <div class="form-group clearfix">
                          <label for="inputName" class="col-sm-2 control-label">Deeplink</label>
                          <div class="col-sm-4">
                              <select name="deeplink_for" id="deeplink_for" class="form-control">
                                  <option value="look">Look</option>
                                  <option value="product">Product</option>
                                  <option value="blog">Blog</option>
                              </select>
                          </div>
                          <div class="col-sm-6">
                              <input class="form-control" id="deeplink_id" placeholder="Deeplink ID" name="deeplink_id" id="deeplink_id">
                          </div>
                      </div>
                      <div class="form-group clearfix">
                          <label for="inputName" class="col-sm-2 control-label">Image Link</label>
                          <div class="col-sm-10">
                              <input class="form-control" id="img_link" placeholder="Image Link" name="img_link" id="img_link" type="url">
                          </div>
                      </div>
                      <div class="form-group clearfix">
                          <div class="col-sm-offset-2 col-sm-10">
                              <button class="btn btn-danger" onclick="sendNotification(<?php echo $this->uri->segment(3); ?>);" >Send</button>
                          </div>
                      </div>
                      <div id="notif_message" class="text-green hide">Notification Sent Successfully</div>
                      <div id="notif_error" class="text-red hide">Error</div>
                    <!-- /.box-body -->
                  </div>
                </div>

              </div>
            </div>
            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
          <div class="tab-pane" id="tab_7">
            <div class="row">
              <div class="col-md-12">
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Order History</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="table-responsive">
                      <input id="selected_orderid" name="selected_orderid" type="hidden" value="">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th class="text-center">Order ID</th>
                            <th class="text-center">Sr. No.</th>
                            <th>Image</th>
                            <th>Product</th>
                            <th>Size</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Brand</th>
                            <th>Status</th>
                            <th>Order Total</th>
                            <th>Date</th>
                            <th>Shipping Address</th>
                            <th>Action</th>
                          </tr>
                          </thead>
                          <tbody id="orderhistory_body">

                          </tbody>
                      </table>
                    </div>


                    <!-- /.box-body -->
                  </div>
                </div>
              </div>

            </div>
            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
          <div class="tab-pane" id="tab_8">
            <div class="row">
              <div class="col-md-12">
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">User Wardrobe</h3>
					<span id="wardrobeAlert"></span>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
					<form role="form" name="frmuploadimg" id="frmuploadimg"  action="<?php echo base_url(); ?>stylist/upload_wardrobe_images" method="post" enctype="multipart/form-data" onsubmit="return validate_image_form()">
						<div class="box">
							<div class="box-body">
								<div class="box-drag-drop">
									<input id="images" name="images[]" class="file" type="file" multiple >
									<input id="user_id" name="user_id" type="hidden" value="<?php echo $this->uri->segment(3); ?>">
									<span class="text-red"> </span>
								</div>
                <br>
                  <button type="submit"> Upload Images</button>
							</div>

						</div>
					
					</form>	  
					  
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="products-list-wrp">
                      <ul class="products-list product-list-inline products-list-grid popup-gallery" id="userWardrobe_body">
					  
                      </ul>
                    </div>
                    <!-- /.box-body -->

                  </div>
                </div>

                <!-- /.box-body -->
              </div>
            </div>
          </div>
		  
        <div class="tab-pane" id="tab_9">
  		    <div class="row">
            <div class="col-md-12">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Facebook Images</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  </div>
                </div>
        				<div class="box-body">
        					<ul class="products-list product-list-inline products-list-grid popup-gallery" id="fb_img_data">
        						<li class="item">
        						<div class="product-img">
        						<a href="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14688165063345.png" >
        						<img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/14688165063345.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
        						</a>
        						</div>
        						</li>
        						<li class="item">
        						<div class="product-img">
        						<a href="https://www.stylecracker.com/sc_admin/assets/products/14660618032911.png" >
        						<img src="https://www.stylecracker.com/sc_admin/assets/products/14660618032911.png" alt="" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
        						</a>
        						</div>
        						</li>
        					  </ul>
                    <div><a id="userfb_profile_link"  href="" target="_blank" >Facebook Profile Link</a></div> 
        				</div>
  				  </div>
  				  </div>
				  </div>
		    </div>

        <div class="tab-pane" id="tab_10">
          <div class="row">
            <div class="col-md-12">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Instagram Images</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  </div>
                </div>
                <div class="box-body">
                  <ul class="products-list product-list-inline products-list-grid popup-gallery" id="insta_img_data">                  
                  </ul>
                </div>
            </div>
            </div>
          </div>
        </div>
		
		    <div class="tab-pane" id="tab_11">
          <div class="row">
            <div class="col-md-12">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"> User SCBOX Data</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  </div>
                </div>
                <div class="box-body" id="user_scbox_data">
                  
                </div>
            </div>
            </div>
          </div>
        </div>

        <div class="tab-pane" id="tab_12">
          <div class="row">
            <div class="col-md-12">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"> User Feedaback</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  </div>
                </div>
                <div class="box-body" id="user_feedback_data">
                  
                </div>
            </div>
            </div>
          </div>
        </div>
		
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    <!-- nav-tabs-custom -->
  </div>
  <!-- /.col -->

  <!-- /.col -->
  </div>
</section>



  
  
        <div class="modal fade" id="addProductModal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Product</h4>
              </div>
              <div class="modal-body">
			  
			 <div id="productData"></div>
			  
					<form class="form-horizontal" method="post" action="" id="prodct-form" enctype="multipart/form-data">
					<input id="orderId" name="orderId" type="hidden" value="">
					  <div class="box-body">
						<div class="form-group">
						  <label for="product_name" class="col-sm-3 control-label">Product Name</label>
						  <div class="col-sm-9">
							<input name="product_name" type="text" class="form-control" id="product_name" placeholder="Product Name">
							<span class="text-danger"></span>
						  </div>
						</div>
						<div class="form-group">
						  <label for="producte_price" class="col-sm-3 control-label">Producte Price</label>

						  <div class="col-sm-9">
							<input name="producte_price" type="text" class="form-control" id="producte_price" placeholder="Product Price">
							<span class="text-danger"></span>
						  </div>
						</div>

						<div class="form-group">
						  <label class="control-label col-sm-3" for="product_description">Description</label>
						  <div class="col-sm-9">
							<div>
							  <textarea class="form-control" name="product_description" id="product_description" rows="3" placeholder="Product Description"></textarea>
							 </div>
						   </div>
						</div>
						
						
						<div class="form-group">
							<label class="control-label col-sm-3" for="product_image">Product Image <span class="text-danger"> </span></label>
							<div class="col-sm-9">
								<div>
									<input type="file" class="form-control" id="product_image" name="product_image" placeholder="">
									<span class="text-danger"><!--(File Size Limit: 2 MB max.)--></span> 
								</div>
							</div>
						</div>
					  </div>
					  <!-- /.box-footer -->
					   <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
						<button type="submit" id="productAdd" name="productAdd" class="btn btn-primary">Insert Product</button>
					  </div>
					</form>
              </div>
             
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
		
		
		
		<!-- start product detail modal -->
		<!--<div class="modal fade" id="productDetail">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Default Modal</h4>
              </div>
              <div class="modal-body" id="objectMeta">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>-->
          <!-- /.modal-dialog -->
		  
        </div>
        <!-- /. End product detail modal -->


<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<script>
$(document).ready(function() {
    $('#prodct-form').validate({
		ignore: 'input[type="hidden"]',
        rules: {
            product_name: {
                required: true
            },
            producte_price: {
                //required:true,
				number:true,
            },
            product_image: {
                required:true,
            }
        },
			 messages: {
					product_name: {
						required: "Please enter name",
					},
					producte_price: {
						  //required:'Please Enter Product Price',
						  number:'It Provide Price only in Digit',
					},
					product_image: {
						  required:'Please Select Product Image',
					}
				 },

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

});

		function addProductModal(val) {
			$('#orderId').val(val);
			$('#addProductModal').modal();
		}
		
		$(document).ready(function (e) {
			$('#prodct-form').on('submit',(function(e) {
				e.preventDefault();
				var product_name = $('#product_name').val();
				//var producte_price = $('#producte_price').val();
				var product_description = $('#product_description').val();
				var product_image = $('#product_image').val();
				var formData = new FormData(this);
				if(product_name!='' && product_image!='')
				{
					$.ajax({
						type:'POST',
						url: "<?php echo base_url(); ?>Stylist/addproduct",
						data:formData,
						cache:false,
						contentType: false,
						processData: false,
						success:function(data){
							var html  = ('<div class="alert alert-success"><strong>Success!</strong> Product Added Successfully.</div>');
							$('#productData').append(html);
							setTimeout(function() {$('#addProductModal').modal('hide');}, 1500);
						},
						error: function(data){
							console.log("error");
							console.log(data);
						}
					});
				}
			}));
		});
		
	
		/* function productDetail(meky, userid) {
			var val = meky+'-'+userid;
			$.post("<?php echo site_url('stylist/getproduct');?>/"+val,{}, function (data) {
			var metaData 	= data.objectMetaData;
			var metakey 	= data.metakey;
			$('#objectMeta').empty(html);
			
			if(metaData != false)
			{
				//alert(meky);
				var mkey = mkey;
				var html = '';
				html += '<div class="box">';
				html += '<div class="box-body no-padding">';
				html += '<table class="table table-condensed">';
                html += '<tbody>';
				html += '<tr>';
                html += '<th>Product Name</th>';
                html += '<th>Product Price</th>';
                html += '<th>Product Description</th>';
                html += '<th>Product Image</th>';
                html += '<th>Action</th>';
                html += '</tr>';
				
				$.each(metaData, function(i, item) {	
					if(metaData[i] != false)
					{
						html += '<tr>';
						html += '<td>'+ metaData[i].name +'</td>';
						html += '<td>'+ metaData[i].price +'</td>';
						html += '<td>'+ metaData[i].description +'</td>';
						html += '<td><img class="img-responsive" style="width:50px;" src="https://www.stylecracker.com/sc_admin/assets/products/thumb_310/'+ metaData[i].image +'"></td>';
						html += '<td><button type="button" class="btn btn-box-tool" onclick="delete_orderprd(\''+metakey+'\',' +metaData[i].id+');" ><i class="fa fa-times"></i></button></td>';
						html += '</tr>';
					}
				});
				html += '</tbody>';
				html += '</table>';
				html += '</div>';
				html += '</div>';
			}
			$('#objectMeta').append(html);
		}, "json");
		$('#productDetail').modal();
		} */

		
		function imageDelete(val, userid)
		{
			var data = val+'-'+userid;
      if(confirm("Are you sure you want to delete image?")){
  			$.post("<?php echo site_url('stylist/imageDelete');?>/"+data,{}, function (data) {
  				if(data == 'success')
  				{
  					var html = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Success!</strong> User Wardrobe Data Deleted Successfully...</div>';
  					window.location.href=window.location.href;
  				}
  				$('#wardrobeAlert').append(html);
  			}, "json");
      }else
      {
          return false;
      }
		}

  </script>
  
     
<style>
  .img-wrp {
    width: 70px;
    height: 70px;
    border: 1px solid #eee;
    text-align: center;
  }

  .img-wrp img {
    max-width: 100%;
    max-height: 100%;
	
  }
  .btn_del{
    position: absolute;
    bottom: 3%;
    z-index: 999;
	color: red;
	font-size: 21px;
  }
</style>
<script type="text/javascript">

$(document).ready(function(){
  $('#notif_message').addClass('hide');
  $('#notif_error').addClass('hide');
});

/*function check(){
    var result = window.confirm('Are you sure?');
    return result;
  }*/

  function dispalyCartDetail(userid)
  {
    var couponcode = $('#couponcode').val();
    if(userid!='')
    {
       $.ajax({
                url: "<?php echo base_url(); ?>Stylist/getUserCart",
                type: 'POST',
                data: {'user_id':userid,'couponcode':couponcode},
                success: function(response) {
                    $('#abundant_cart_body').html(response);
                },
                error: function(xhr) {

                }
            });

       $.ajax({
                url: "<?php echo base_url(); ?>Stylist/getUserCartByStylist",
                type: 'POST',
                data: {'user_id':userid},
                success: function(response) {
                    $('#abundantCartByStylist').html(response);
                },
                error: function(xhr) {

                }
            });
    }
  }

  function delete_cart(cartid)
  {
    var result = confirm("Are you sure you want to remove this product from your cart?");
    if(result && cartid!='')
    {
        $.ajax({
                url:"<?php echo base_url(); ?>Stylist/removeCartProduct",
                type: 'POST',
                data: {'cartid':cartid},
                success: function(response)
                {
                  if(response=='1')
                  {
                    dispalyCartDetail(<?php echo $this->uri->segment(3); ?>);
                  }
                },
                error: function(xhr)
                {

                }
              });
    }
  }

  function dispalyOrderDetail(userid)
  {
    $('#notif_message').addClass('hide');
    $('#notif_error').addClass('hide');
    if(userid!='')
      {
         $.ajax({
                  url: "<?php echo base_url(); ?>Stylist/getUserOrder",
                  type: 'POST',
                  data: {'user_id':userid},
                  success: function(response) {
                      $('#orderhistory_body').html(response);
                      //$('#productDetail').html(response);
                  },
                  error: function(xhr) {

                  }
              });

    }
  }

  function dispalyWardrobe(userid)
  {
    $('#notif_message').addClass('hide');
    $('#notif_error').addClass('hide');
    if(userid!='')
      {
         $.ajax({
                  url: "<?php echo base_url(); ?>Stylist/display_wardrobe",
                  type: 'POST',
                  data: {'user_id':userid},
                  success: function(response) {
                      $('#userWardrobe_body').html(response);
                  },
                  error: function(xhr) {

                  }
              });

    }
  }

  function dispalyNotification(userid)
  {
    $('#notif_message').addClass('hide');
    $('#notif_error').addClass('hide');
    if(userid!='')
    {

    }
  }

  function sendNotification(userid)
  {
    $('#notif_message').addClass('hide');
    $('#notif_error').addClass('hide');
    if(userid!='')
    {
      var title = $('#title').val();
      var content = $('#content').val();
      var deeplink_for = $('#deeplink_for').val();
      var deeplink_id = $('#deeplink_id').val();
      var img_link = $('#img_link').val();
      if(title=='' || content=='' || deeplink_for=='' || deeplink_id=='' || img_link=='' )
      {
         $('#notif_error').html('Fields cannot be empty');
         $('#notif_error').removeClass('hide');
      }else
      {
        var result = window.confirm('Are you sure?');
        if(result)
        {
          $.ajax({
                  url:"<?php echo base_url(); ?>Stylist/sendMessage",
                  type:'POST',
                  data:{'userid':userid,'title':title,'content':content,'deeplink_for':deeplink_for,'deeplink_id':deeplink_id,'img_link':img_link},
                  success:function(response){
                    if(response==1)
                    {
                      $('#notif_message').removeClass('hide');
                    }else
                    {
                      $('#notif_error').removeClass('hide');
                    }

                  },
                  error:function(xhr){
                    console.log(xhr);
                  }
                });
        }
      }
    }
  }

  function Setproductsize(productid,cartid,userid)
  {
    var productsize = $('#productsize_'+productid).val();
      if(productsize==null)
      {
        alert("Select Product Size");
      }else
      {
        if(userid!='' && cartid!='')
        {
          $.ajax({
                  url:"<?php echo base_url(); ?>Stylist/addToCart",
                  type:"POST",
                  data:{'func_type':'updateCart','productid':productid,'productsize':productsize,'userid':userid,'cartid':cartid},
                  success:function(response){
                    alert(response);
                    dispalyCartDetail(userid);
                    remove_product(productid);
                    change_list(productid);
                  },
                  error:function(xhr){
                    console.log(xhr);
                  }
                });
        }
      }
  }

  function addtocart(productid,userid)
  {
    var productsize = $('#productsize_'+productid).val();
      if(productsize==null)
      {
        alert("Select Product Size");
      }else
      {
        if(userid!='')
        {
          $.ajax({
                  url:"<?php echo base_url(); ?>Stylist/addToCart",
                  type:"POST",
                  data:{'func_type':'addCart','productid':productid,'productsize':productsize,'userid':userid},
                  success:function(response){
                    dispalyCartDetail(userid);
                    remove_product(productid);
                    change_list(productid);
                    alert(response);
                  },
                  error:function(xhr){
                    console.log(xhr);
                  }
                });
        }
      }
  }

  function applyCoupon(userid)
  {
    var couponcode = $('#couponcode').val();
    if(couponcode!='')
    {
      if(userid!='')
        {
          dispalyCartDetail(userid);
        }
    }else
    {
       dispalyCartDetail(userid);
      alert("Enter Valid Coupon Code");
    }
  }

  function remove_productbystylist(id){
      var confirm = window.confirm('Are you sure?');
      if(confirm)
      {
        $('#final_list_'+id).remove();
      }
  }

	window.fbAsyncInit = function() {
	  FB.init({
		appId      : '1650358255198436',
		cookie     : true,  // enable cookies to allow the server to access 
							// the session
		xfbml      : true,  // parse social plugins on this page
		version    : 'v2.8' // use graph api version 2.8
	  });

	  FB.getLoginStatus(function(response) {
		statusChangeCallback(response);
	  });

	 };
	  
	function get_fb_images(user_id){
		var response = ''; 

     get_fb_profilelink(user_id);
		var accessToken = '<?php echo $access_token; ?>';
		if(accessToken != ''){
			$.ajax({
				url: 'https://graph.facebook.com/me/photos?type=uploaded&limit=8&access_token='+accessToken+'&fields=images,name,created_time',
				type: 'get',
				data: { 'response' : response },
				success: function(data, status) {
					// alert(JSON.stringify(data));
					show_images(data,'fb');         
				}
			});
		}else {
			$('#fb_img_data').html('no images found.');
			return false;
		}
		
	}

  function get_insta_images(user_id){ 
    var response = ''; 
    var accessTokenInsta = '<?php echo $access_token_insta; ?>';
       
    if(accessTokenInsta != ''){ 
      $.ajax({ 
        url: 'https://api.instagram.com/v1/users/self/media/recent/?access_token='+accessTokenInsta,
        crossDomain: true,
        dataType: 'jsonp',
        type: 'get',
        data: { 'response' : response },
        success: function(data, status) { 
          // alert(JSON.stringify(data));
          show_images(data,'insta');
        }
      });
    }else {
      $('#insta_img_data').html('no images found.');
      return false;
    }
    
  }

	function show_images(data,socialtype){
		$.ajax({
			url: "<?php echo base_url(); ?>stylist/show_images",
			type: 'post',
			data: { 'data' : data ,'socialtype' : socialtype},
			success: function(response, status) {
        if(socialtype=='fb')
        {
          $('#fb_img_data').html(response);
        }else if(socialtype=='insta'){
          $('#insta_img_data').html(response);          
        }
				
			}
		});
	}
	
function get_user_scbox_data(user_id){
	$.ajax({
		url: "<?php echo base_url(); ?>stylist/get_user_scbox_data",
		type: 'post',
		data: { 'user_id' : user_id },
		success: function(response, status) {
			$('#user_scbox_data').html(response);
		}
	});
}

function validate_image_form(){
	var nme = document.getElementById("images");
	if(nme.value.length < 4) {
		alert('Must Select any of your photo for upload!');
		return false;
	}
}


function get_user_feedback(user_id){ 
    $('#notif_message').addClass('hide');
    $('#notif_error').addClass('hide');
    if(user_id!='')
      {
         $.ajax({
                  url: "<?php echo base_url(); ?>Stylist/get_user_feedback_data",
                  type: 'POST',
                  data: {'user_id':user_id},
                  success: function(response) {
                      $('#user_feedback_data').html(response);
                  },
                  error: function(xhr) {

                  }
              });

    }
    
  }

  function delete_orderprd(orderid,productid)
  { 
    var result = confirm("Are you sure you want to remove this product from the order?");
    //var selorder = $('#selected_orderid').val();
    alert('selorder=='+orderid+'=productid='+productid);
    if(result && orderid!='' && productid!='')
    {
        $.ajax({
                url:"<?php echo base_url(); ?>Stylist/remove_order_product",
                type: 'POST',
                data: {'selected_orderid':orderid, 'orderprdid' : productid},
                success: function(response)
                {
                  
				  //alert(response);
                   if(response=='1')
                  {
                    setTimeout(function() {$('#productDetail').modal('hide');}, 1000);
					//dispalyOrderDetail(<?php echo $this->uri->segment(3); ?>);
                  }
                },
                error: function(xhr)
                {

                }
              });
    }
   
  }

  function get_fb_profilelink(user_id){
    var response = ''; 
    var accessToken = '<?php echo $access_token; ?>';
    if(accessToken != ''){
      $.ajax({
        url: 'https://graph.facebook.com/me?fields=link&access_token='+accessToken,
        type: 'get',
        data: { 'response' : response },
        success: function(data, status) {
         // alert(JSON.stringify(data));
          //var fb_profile_link = JSON.stringify(data);
          //$('#userfb_profile_link').html(fb_profile_link.link);
          $('#userfb_profile_link').attr('href',data.link);          
        }
      });
    }else {
      $('#fb_img_data').html('no images found.');
      return false;
    }
    
  }

</script>
