<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stylist extends MY_Controller {	
	
	function __construct(){
		parent::__construct();
		$this->load->model('Stylist_model'); 
		$this->load->model('websiteusers_model'); 
		$this->load->model('users_model'); 
		$this->load->model('Scbox_model'); 
		$this->load->model('Pa_model');	   

	} 
   
	public function index($user_id = null)
	{ echo 'Stylist';exit;
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{ 
			$data_post = array();			
		
			 $data['roles'] = $this->users_model->get_roles(); 
			 $user_id =  $this->uri->segment(3);
			 $data['user_data'] = $this->users_model->get_user_data($user_id);			
			 $data['sc_box_mobile_number'] = $this->users_model->sc_box_mobile_number($user_id);
			 $data['pa_data'] = $this->users_model->get_user_pa($user_id); 
			 $data['wordrobe'] = $this->users_model->get_wordrobe_image($user_id); 
			 $data['extra_info'] = $this->users_model->extra_info($user_id); 
			 $data['visit_count_user'] = $this->users_model->visit_count($user_id); 
			 $data['last_login'] =  $this->users_model->last_login($user_id); 
			 $data['total_look_click_count'] =  $this->users_model->total_look_click_count($user_id); 
			 $data['total_product_click_count'] =  $this->users_model->total_product_click_count($user_id); 
			 $data['fav_look_count'] =  $this->users_model->fav_look_count($user_id); 
			
			if($this->has_rights(15) == 1){
				
				$this->load->view('common/header2',$data_post);
				$this->load->view('pages/stylist');
				$this->load->view('common/footer2');
			}else{
				$this->load->view('common/header2',$data_post);
				$this->load->view('not_permission');
				$this->load->view('common/footer2');
			}
		}
	}
	
	function user_view()
	{
		 $data = array();
		 $brand_list = array();
		 $data['roles'] = $this->users_model->get_roles(); 
		 $user_id =  $this->uri->segment(3);
		 $data['user_data'] = $this->users_model->get_user_data($user_id);
		  //echo '<pre>';print_r($data['user_data']);
		 $data['sc_box_mobile_number'] = $this->users_model->sc_box_mobile_number($user_id);
		 $data['pa_data'] = $this->users_model->get_user_pa($user_id); 
		 $data['wordrobe'] = $this->users_model->get_wordrobe_image($user_id); 
		 $data['extra_info'] = $this->users_model->extra_info($user_id); 
		 $data['visit_count_user'] = $this->users_model->visit_count($user_id); 
		 $data['last_login'] =  $this->users_model->last_login($user_id); 
		 $data['total_look_click_count'] =  $this->users_model->total_look_click_count($user_id); 
		 $data['total_product_click_count'] =  $this->users_model->total_product_click_count($user_id); 
		 $data['fav_look_count'] =  $this->users_model->fav_look_count($user_id); 
		 $data['pa_data_extra'] = $this->users_model->get_user_pa_extras($user_id); 
		//echo '<pre>';print_r($data['pa_data']);exit;
	    if(!empty($data['pa_data'])){
		  foreach($data['pa_data'] as $val)
	      {
	          if(@$val['object_meta_key'] == 'user_body_shape'){               
	                $data['userBodyShape']= $val['answer']; 
			      } 
	  		    if(@$val['object_meta_key'] == 'user_style_p1'){ 
	                  $data['userStylePref1']= $val['answer'];
	  			  } 
	  			 if(@$val['object_meta_key'] == 'user_style_p2'){ 
	  			   $data['userStylePref2']=$val['answer']; 
	  		    }
	  			 if(@$val['object_meta_key'] == 'user_style_p3'){ 
	                 $data['userStylePref3']=  $val['answer']; 
	  			 } 
	      }
		}

		if($data['user_data'][0]['gender']==1)
		{
			$brand_list = unserialize(PA_BRAND_LIST_WOMEN);
			 $age_question=8;
		}else
		{
			$brand_list = unserialize(PA_BRAND_LIST_MEN);
			$age_question=17;
		}
			
		//echo '<pre>';print_r($data['pa_data_extra']);
		$brands = '';
		if(isset($data['pa_data']['0']['brand_ids'])){ 
			$brand_ids = explode(',',$data['pa_data']['0']['brand_ids']);				
			foreach($brand_ids as $value){
				// print_r($brand_ids);exit;
				if(empty($brands))
				{
					$brands = @$brand_list[$value]->brand_name;	
				}else
				{
					$brands = $brands.', '.$brand_list[$value]->brand_name;
				}					
			}
		}
		$data['userBrandPref'] = $brands;
			
		 $question_ans = array();
		 if(!empty($data['pa_data_extra'])){
			foreach($data['pa_data_extra'] as $val){
				 $question_ans[$val['question_id']] = $val['answer'];
			}
		 } 

		 $data['userAgeRange'] =   @$question_ans[$age_question]; 

		if($this->has_rights(15) == 1){			
			$this->load->view('common/header2');
			$this->load->view('stylist/stylistpage',$data);	
			$this->load->view('common/footer2');
			
		}else{
			
			$this->load->view('common/header2',$data);
			$this->load->view('not_permission');
			$this->load->view('common/footer2');
		 }
			
	}

	function user_look($type,$user_id){
			 $data = array();
		
			 $data['roles'] = $this->users_model->get_roles(); 
			 $type =  $this->uri->segment(3);	
			 $user_id =  $this->uri->segment(4);	
			 

			 if($type == 'look')
			 {
			 	 $data['user_click'] =  $this->users_model->user_look_click_id($user_id);
			 	 $data['total_rows'] = count($this->users_model->user_look_click_id($user_id));
			 	 $data['type'] = 'Look';
			 }
			
			if($type == 'product')
			{
			 	$data['user_click'] =  $this->users_model->user_product_click_id($user_id);
			 	$data['total_rows'] = count($this->users_model->user_product_click_id($user_id));
			 	$data['type'] = 'Product';
			}

			if($type == 'fav')
			{
				$data['user_click'] =  $this->users_model->fav_look_detail($user_id);
				$data['total_rows'] = count($this->users_model->fav_look_detail($user_id));
			 	$data['type'] = 'Favourite';
		
			}
			 
			/*if($this->has_rights(15) == 1){*/
				
				$this->load->view('common/header2',$data);
				$this->load->view('user_click');
				$this->load->view('common/footer2');
				
			/*}else{
				
				$this->load->view('common/header2',$data);
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			 }*/
			
	}

	function getUserCart()
	{
	  	if (!$this->input->is_ajax_request()) {
          exit('No direct script access allowed');
      	}else{

      		$cart_html = '';$sizeDiv = '';
      		$userid = $this->input->post('user_id');
      		$discountcoupon_stylecracker = $this->input->post('couponcode');

			//$cart_data = $this->Stylist_model->getUserCart($userid);
			$data['cart_info'] = $this->Stylist_model->getUserCartData($userid); 			
			
			//echo '<pre>';print_r($data);exit;        
			$p_price = 0;  $data['referral_point']=0;
			if(!empty($data['cart_info'])){
				foreach($data['cart_info'] as $val){
					$p_price = $p_price + $val['discount_price'];
				}
				$useremail = $data['cart_info'][0]['useremail'];
				$data['product_total_for_msg'] = $p_price;			
				if(isset($discountcoupon_stylecracker) && $discountcoupon_stylecracker!=''){ 
					$data['coupon_code_msg'] = $this->Stylist_model->check_coupon_exist(strtolower($discountcoupon_stylecracker),$useremail,$userid);
					$p_price = $p_price-$data['coupon_code_msg']['coupon_discount_amount'];				
				}	
			}	
			
			//echo '<pre>';print_r($data['cart_info']);exit;
	          if(!empty($data['cart_info']))
	          {
	            $i=0;
	            foreach($data['cart_info'] as $value)
	            {  $i++;
	              $productsize = $this->Stylist_model->getProductSize($value['product_id']);

	              $sizeDiv = ' <td>
		            <select name="productsize" id="productsize_'.$value['product_id'].'" class="form-control productsize"  onchange="Setproductsize(\''.$value['product_id'].'\',\''.$value['id'].'\',\''.$userid.'\')" >';
		            if(!empty($productsize))
		            {
			            foreach($productsize as $prdsize)
			            {
			              if($prdsize['size_id'] == $value['product_size'])
			              {
			                $sizeDiv=$sizeDiv.'<option  value="'.$prdsize['size_id'].'" selected >'.$prdsize['size_text'].'</option>';
			               }else{
			                $sizeDiv=$sizeDiv.'<option  value="'.$prdsize['size_id'].'" >'.$prdsize['size_text'].'</option>';
			              }
			            }
			        }else
			        {
			        	$sizeDiv=$sizeDiv.'<option  value="" >Product Out of Stock</option>';
			        }
            	  $sizeDiv=$sizeDiv.'</select></td> '; 
				// added for new product url  
				$productUrl = $this->config->item('product_url_124').$value['product_image'];
				if(@getimagesize($productUrl))
				{
					$fileExist = 1;
				}else
				{
					$fileExist = 0;
					$productUrl = $this->config->item('products_small').$value['product_image'];
				}
				// added for new product url
	              $cart_html= $cart_html.'<tr>
	                <td>'.$i.'</td>
	                <td>
	                  <div class="img-wrp">
	                    <img src="'.$productUrl.'" >
	                  </div>
	                </td>
	                <td>'.$value['product_name'].'</td>
	               	'.$sizeDiv.'
	               	<td class="text-center">'.$value['product_qty'].'</td>
	                <td><i class="fa fa-inr"></i> '.$value['price'].'</td>
	                <td>'.$value['dateadded'].'</td>	
	                <td class="text-center">'.$value['datediff'].'</td>                               
	                <td class="text-center">
	                  <button type="button" class="btn btn-danger btn-xs" onclick="delete_cart(\''.$value['id'].'\');" ><i class="fa fa-times"></i></button>
	                </td>
	              </tr>
	              ';                      
	                        
		        }

		        if(isset($discountcoupon_stylecracker) && $discountcoupon_stylecracker!='')
		        { 	
		        	if($data['coupon_code_msg']['coupon_discount_amount']>0)
		        	{	
						$cart_html= $cart_html.'<tr><td colspan="4"></td><td><label>'.$data['coupon_code_msg']['msg'].' ('.$data['coupon_code_msg']['coupon_code'].') '.'</label></td><td class="text-success"><label> - <i class="fa fa-inr"></i>'.$data['coupon_code_msg']['coupon_discount_amount'].'</label></td><td></td></tr>';		
					}else
					{
						$cart_html= $cart_html.'<tr><td colspan="4"></td><td>No Coupon Found or Expired  </td><td></td><td></td></tr>';
					}		
				}
					$cart_html= $cart_html.'<tr><td colspan="4"></td><td><label>Order Total : </label></td><td  class="text-success"><label><i class="fa fa-inr"></i> '.$p_price.'</label></td><td></td></tr> <tr><td class="colspan="9">&nbsp;</td></tr>';
		       }else
		       {
		       		$cart_html = '<tr><td class="colspan="9">No items in the cart</td></tr>  
                    <tr><td class="colspan="9">&nbsp;</td></tr>';
		       }                       

			echo $cart_html;
				//echo '<pre>';print_r($cart_data);
			}
	}

	function removeCartProduct()
	{
		if(!$this->input->is_ajax_request())
		{
			exit("No direct script access allowed");
		}else
		{
			$cartid = $this->input->post('cartid');
			if($cartid!='')
			{
				$this->Stylist_model->scremovecart($cartid);
				echo '1';
			}else
			{
				echo '0';
			}
		}
	}

	function getUserCartByStylist()
	{
	  	if (!$this->input->is_ajax_request()) {
          exit('No direct script access allowed');
      	}else{

      		$cart_html = '';
      		$userid = $this->input->post('user_id');
      		$logged_in_user = $this->session->userdata('user_id');
			$cart_data = $this->Stylist_model->getUserCartByStylist($userid,$logged_in_user);

	          if(!empty($cart_data))
	          {
	            $i=1;
	            foreach($cart_data as $value)
	            {     
	              $cart_html= $cart_html.'<tr>
	                <td>'.$i.'</td>
	                <td>
	                  <div class="img-wrp">
	                    <img src="'.LIVE_SITE_URL.'assets/products/thumb_310/'.$value['product_image'].'" >
	                  </div>
	                </td>
	                <td>'.$value['product_name'].'</td>
	                <td>'.$value['size_text'].'</td>
	                <td><i class="fa fa-inr"></i> '.$value['product_price'].'</td>
	                <td>
	                  <button type="button" class="btn btn-box-tool" onclick="delete_cart(\''.$value['id'].'\');" ><i class="fa fa-times"></i></button>
	                </td>
	              </tr>';                      
	                        
		        }
		       }else
		       {
		       		$cart_html = "No items in the cart";
		       }                

			echo $cart_html;				
			}
	}

	function getUserOrder()
	{
	  	if (!$this->input->is_ajax_request()) {
          exit('No direct script access allowed');
      	}else{

      		$order_html = '';
      		$order_status = '';
      		$order_date = '';
      		$userid = $this->input->post('user_id');
      		$logged_in_user = $this->session->userdata('user_id');
      		$order_data = $this->Stylist_model->getUserOrders($userid);
			$orderprd_data = $this->Stylist_model->getUserProductsOrders($userid);
			//echo '<pre>'; print_r($orderprd_data);exit;

			if(!empty($order_data)){  $i = 0;
                                $priceProduct =0;
                        $scDiscountPrice = 0;
            foreach($order_data as $val){
            	/*	$val['order_display_no'];*/
            	$order_date = date_format(date_create($val['created_datetime']),'d/m/Y');

            	if(!empty($orderprd_data)){$i=0;
                            foreach ($orderprd_data as $pro_info) { 
                              if($pro_info['order_unique_no'] == $val['order_unique_no'])
                              { $i++;
                              	if($pro_info['order_status']==0 || $pro_info['order_status']==1){ $order_status='Processing'; }else if($pro_info['order_status'] == 2){ $order_status='Confirmed'; }else if($pro_info['order_status'] == 3){ $order_status='Dispatched';  }else if($pro_info['order_status'] == 4){ $order_status='Delivered'; }else if($pro_info['order_status'] == 5){ $order_status='Cancelled'; }else if($pro_info['order_status'] == 6){ $order_status='Return'; }
								
								// added for new product url  
								$productUrl = $this->config->item('product_url_124').$pro_info['image'];
								if(@getimagesize($productUrl))
								{
									$fileExist = 1;
								}else
								{
									$fileExist = 0;
									$productUrl = $this->config->item('products_small').$pro_info['image'];
								}
								// added for new product url
								
	                              $order_html = $order_html.'<tr>
		                            <td class="text-center">'.$pro_info['order_display_no'].'</td>
		                            <td class="text-center">'.$i.'</td>
		                            <td>
		                              <div class="item-img">
		                                <img src="'.$productUrl.'" alt="" style="width:40px;">
		                              </div>
		                            </td>
		                            <td>'.$pro_info['name'].'</td>
		                            <td>'.$pro_info['size_text'].'</td>
		                            <td>'.$pro_info['product_qty'].'</td>
		                            <td><i class="fa fa-inr"></i> '.$pro_info['product_price'].'</td>
		                            <td>'.$pro_info['company_name'].'</td>
		                            <td><span class="text-orange">'.$order_status.'</span></td>
		                            <td><i class="fa fa-inr"></i> '.$val['order_total'].'</td>
		                            <td>'.$order_date.'</td>
		                            <td>'.$val['shipping_address'].'</td>
		                          </tr>';                                 
                              }
                          	}
                         }
                    }
                }else
		       {
		       		$order_html = '<tr><td colspan="12">No items in the cart</td></tr>';
		       }	          
			echo $order_html;				
			}
	}

	function display_wardrobe(){
        if (!$this->input->is_ajax_request()) {
          exit('No direct script access allowed');
      	}else{
      		$userid = $this->input->post('user_id');
      		$result = $this->Stylist_model->display_wardrobe($userid);
			$wardrobe_html = '';
			foreach($result as $val){
				$image_name = explode('-',$val['image_name']);
				/*Hardcoded for wardrobe look on 2-8-17*/
				$wardrobe_html = $wardrobe_html.'<li class="item" style="padding:0 10px;">
        						<div class="product-img" style="width:100px;height:100px;">
        						<a href="'.$this->config->item('wordrobe_path_url').''.$val['image_name'].'" >
        						<img src="'.$this->config->item('wordrobe_path_url').''.$val['image_name'].'" alt="">
        						</a>
								<div style="word-wrap: break-word;max-width:100px;">'.$image_name[0].'</div>
        						</div>
        						</li>';
			}
			echo $wardrobe_html;
      	}

    }

   /* public function stylist_manage_notifications($userid){
	
		if($_POST){
			$title = $this->input->post('title');
			$content = $this->input->post('content');
			$deeplink_for = $this->input->post('deeplink_for');
			$deeplink_id = $this->input->post('deeplink_id');
			$img_link = $this->input->post('img_link');
			$gender = $this->input->post('gender');
			
			///$notification = $this->sendMessage($content,$deeplink_for,$deeplink_id,$img_link,$bucket_ids);
		}
		$this->load->view('common/header2');
		$this->load->view('pages/stylist-manage-notifications',$data);
		$this->load->view('common/footer2');

	}*/

	function sendMessage(){	

		// if(!$this->input->is_ajax_request())
		// {
			// exit('No direct script access allowed');
		// }else
		// {
			$userid = $this->input->post('userid');
			$title = $this->input->post('title');
			$content = $this->input->post('content');
			$deeplink_for = $this->input->post('deeplink_for');
			$deeplink_id = $this->input->post('deeplink_id');
			$img_link = $this->input->post('img_link');	
			$useremail = $this->Stylist_model->getUserEmail($userid);
			
			
			$title = '#Formal #Officewear #Shirt #Trousers #Pink ';
			$deeplink = 'https://www.stylecracker.com/looks/look-details/formal-officewear-shirt-trousers-pink--14799020129737';
			$content = array("en" => 'Hi, your personal stylish has created a curated look for you.');
			$deeplink_for = 'look';
			$deeplink_id = 22001;
			$img_link = 'https://www.stylecracker.com/sc_admin/assets/looks/1479902012491.png';
			$useremail = 'amit@stylecracker.com';
			
			if($content!='' && $deeplink_for!='' && $deeplink_id!='' && $img_link!='' && $useremail!='')
			{
				// $content = array("en" => $content);
				$fields = array(
					'app_id' => "774c695f-4257-45ad-bbd7-76a22b12117b",
					'filters' => array(array("field" => "tag", "key" => "email", "relation" => "=", "value" => $useremail) ),
					'data' => array($deeplink_for => $deeplink_id),
					'contents' => $content,
					'big_picture'=> $img_link,
				);

				$fields = json_encode($fields);
				//print("\nJSON sent:\n");
				//print($fields);exit;

				/*$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
														   'Authorization: Basic MjdkNWEyNjgtNjA1Ni00OGE1LTlhMDgtNTA4MjdlMzQ4NTQ5'));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($ch, CURLOPT_HEADER, FALSE);
				curl_setopt($ch, CURLOPT_POST, TRUE);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

				$response = curl_exec($ch);
				curl_close($ch);
				$result = json_decode($response);
				echo "<pre>";print_r($result);*/
				// echo '1';			
			// }else
			// {
				// echo '0';
			// }
		}
	}

	function get_product_list(){

		if (!$this->input->is_ajax_request()) {
          exit('No direct script access allowed');
      	}else{
			$segment = $this->input->post('segment');
			$collection_products = $this->input->post('collection_products');
			$userid = $this->input->post('user_id');
			$cart_data = $this->Stylist_model->getUserCart($userid);
				
			if($segment=='stylist-abundant-cart')
			{   
				if(!empty($collection_products)){
					foreach($collection_products as $val){
							$product_ids[]  = str_replace("list_","",$val);
					}
				}
				$product_id = implode(",",$product_ids);
				$product_data = $this->Stylist_model->get_products($product_id);
				$product_list ='';$sizeDiv='';$cartid='';
				$i = 0;
				
				foreach($product_data as $val){$i++;
					$productsize = $this->Stylist_model->getProductSize($val['id']);
					 $sizeDiv = ' <td>
		            <select name="productsize" id="productsize_'.$val['id'].'" class="form-control productsize" onchange="Setproductsize(\''.$val['id'].'\',\''.$cartid.'\',\''.$userid.'\')" >';
		            foreach($productsize as $prdsize)
		            {
		              /*if($prdsize['size_id'] == $val['product_size'])
		              {*/
		               $sizeDiv=$sizeDiv.'<option  value="'.$prdsize['size_id'].'" selected >'.$prdsize['size_text'].'</option>';
		               /*}else{
		                $sizeDiv=$sizeDiv.'<option  value="'.$prdsize['size_id'].'" >'.$prdsize['size_text'].'</option>';
		              }*/
		            }
            	 	$sizeDiv=$sizeDiv.'</select></td> '; 

					$product_list = $product_list.'<tr class="bg-gray-light" id="final_list_'.$val['id'].'">
			                            <td>'.$i.'</td>
			                            <td>
			                              <div class="img-wrp">
			                                <img data-img-link="'.LIVE_SITE_URL.'assets/products/'.$val['image'].'" class="lazy" id="'.$val['id'].'" src="'.LIVE_SITE_URL.'assets/products/thumb_310/'.$val['image'].'" alt="" style="width:40px;" >
			                              </div>
			                            </td>
			                            <td>'.$val['name'].'</td>
			                            '.$sizeDiv.'
			                            <td class="text-center">1</td>
			                            <td>
			                             <i class="fa fa-inr"></i> '.$val['price'].'
			                            </td>
			                            <td>'.date('Y-m-d h:i:s').'</td>
			                            <td  class="text-center">0</td>
			                            <td class="text-center">
			                              <button type="button" class="btn btn-primary btn-xs" onclick="addtocart(\''.$val['id'].'\',\''.$userid.'\');"><i class="fa fa-plus"></i></button>
			                              <a class="btn btn-danger btn-xs" href="javascript:void(0);" onclick="remove_productbystylist('.$val['id'].');change_list('.$val['id'].');"><i class="fa fa-times"></i></a>
			                            </td>
			                          </tr>';			                          
					
				}
				$product_list = $product_list.'<input type="hidden" class="form-control" name="product_ids" id="product_ids" value="'.$product_id.'">';
				echo $product_list;
			}
		}
	}

	public function addtocart(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$func_type = $this->input->post('func_type');
			$productid = $this->input->post('productid');
			$userid = $this->input->post('userid');
			$productsize = $this->input->post('productsize');
			$cartid = $this->input->post('cartid');
			if($func_type=='addCart')
			{				
				$uniquecookie = @$_COOKIE['SCUniqueID'];				
				$agent = '';
				$platform_info = '';
				$lookid = '';
			     
			      /*if (@$this->agent->is_browser())
			      {
			          $agent = 'Computer '.$this->agent->browser().' '.$this->agent->version();
			      }
			      elseif (@$this->agent->is_robot())
			      {
			          $agent = 'Robot '.$this->agent->robot();
			      }
			      elseif (@$this->agent->is_mobile())
			      {
			          $agent = 'Mobile '.$this->agent->mobile();
			      }
			      else
			      {
			          $agent = 'Unidentified';
			      }

			      $platform_info = @$this->agent->platform();*/

				$response = $this->Stylist_model->update_cart($uniquecookie,$productid,$userid,$productsize,$agent,$platform_info,$lookid);
	     		echo "Product Added to the Cart";
     		}else if($func_type=='updateCart')
     		{
     			$cartid = $this->input->post('cartid');
     			if($cartid!='')
     			{
	     			$updateResult = $this->Stylist_model->updatecartproductsize($cartid,$productid,$productsize,$userid);
	     			if($updateResult==1)
	     			{
	     				echo "Cart Product Size Updated";
	     			}else
	     			{
	     				echo "Product already exist in cart";
	     			}
	     			
	     		}else
	     		{
	     			echo "Error";
	     		}
     		}else
     		{
     			echo "Error";
     		}
		}
	}
	
	function displayInstagramPhotos(){
        if (!$this->input->is_ajax_request()) {
          exit('No direct script access allowed');
      	}else{
      		$userid = $this->input->post('user_id');
      		//$this->Stylist_model->display_wardrobe($userid);
      	}

    }


	function show_images(){
		$img_data = $this->input->post('data');
		$socialtype = $this->input->post('socialtype');
		$img_html = '';
		// echo "<pre>";print_r($img_data);
		if($socialtype=='fb')
		{
			foreach($img_data['data'] as $val){
				// echo "<pre>";print_r($img_data['data']);exit;
				// $img_html = $img_html.'<div class="img-wrp"><img src="'.$val['images'][1]['source'].'" alt=""></div>';
				/*$img_html = $img_html.'<li class="item"><div class="product-img"><a href="'.$val['images'][1]['source'].'" >
				<img src="'.$val['images'][1]['source'].'" alt=""></a></div></li>';*/
				$img_html = $img_html.'<li class="item">
											<div class="product-img">
											<a href="'.$val['images'][1]['source'].'" >
											<img src="'.$val['images'][1]['source'].'" alt="">
											</a>
											</div>
											</li>';
			}
		}else if($socialtype=='insta')
		{
			if(!empty($img_data['data']))
			{
				foreach($img_data['data'] as $val){
					$img_html = $img_html.'<li class="item">
											<div class="product-img">
											<a href="'.$val['images']['standard_resolution']['url'].'">
											<img src="'.$val['images']['thumbnail']['url'].'" alt="">
											</a>
											</div>
											</li>';
				}	
			}	
		}
		echo $img_html;
	}
	
	function upload_wardrobe_images(){
		$files = $_FILES;
		$count = count($_FILES['images']['name']);
		$output = [];
		$user_id = $this->input->post('user_id');
		// echo "<pre>";print_r($files);exit;
		if($count>0){
			for($i=0; $i<$count; $i++){ 
				$img_name = preg_replace('/\\.[^.\\s]{3,4}$/', '', $files['images']['name'][$i]);
				$_FILES['images']['name']= $files['images']['name'][$i];
				$_FILES['images']['type']= $files['images']['type'][$i];
				$_FILES['images']['tmp_name']= $files['images']['tmp_name'][$i];
				$_FILES['images']['error']= $files['images']['error'][$i];
				$_FILES['images']['size']= $files['images']['size'][$i];    
				$ext = pathinfo($files['images']['name'][$i], PATHINFO_EXTENSION);
				$img_name = $img_name.'-'.time().mt_rand(0,10000).'.'.$ext;
				$pathAndName = "assets/wardrobes/".$img_name;
				// Run the move_uploaded_file() function here
				$moveResult = move_uploaded_file($files['images']['tmp_name'][$i], $pathAndName);
				$this->Stylist_model->upload_wardrobe_images($img_name,$user_id);  
				if($ext!='gif'){
				$this->compress_image("assets/wardrobes/".$img_name, "assets/wardrobes/".$img_name, '70');
				}
			}
			redirect('stylist/user_view/'.$user_id);
		}else{
			echo "please select images to upload";
		}
		
	}
	
	function get_user_scbox_data(){
		$user_id = $this->input->post('user_id');
		$data = array();
		$data['user_id'] = $user_id;
		$data['user_info'] = $this->Scbox_model->get_user_info($user_id);
		$data['user_data'] = $this->Scbox_model->get_scbox_userdata($user_id);
		//common data
		$data['SCBOX_SKINTONE_LIST'] = unserialize(SCBOX_SKINTONE_LIST);
		$data['SCBOX_COLOR_LIST'] = unserialize(SCBOX_COLOR_LIST);
		$data['SCBOX_PRINT_LIST'] = unserialize(SCBOX_PRINT_LIST);

		// men data
		$data['men_body_shape'] = $this->Pa_model->load_questAns(24);
		$data['men_style'] = $this->Pa_model->load_questAns(25);
		$data['SCBOX_MENTOP_LIST'] = $this->Pa_model->get_size_data('mentopsize');
		$data['SCBOX_MENBOTTOM_LIST'] = $this->Pa_model->get_size_data('menbottomsize');
		$data['SCBOX_MENFOOT_LIST'] = $this->Pa_model->get_size_data('menfootsize');

		//women data 
		$data['women_body_shape'] = $this->Pa_model->load_questAns(1);
		$data['women_style'] = $this->Pa_model->load_questAns(2);
		$data['SCBOX_WOMENTOP_LIST'] = $this->Pa_model->get_size_data('womentopsize');
		$data['SCBOX_WOMENBOTTOM_LIST'] = $this->Pa_model->get_size_data('womenbottomsize');
		$data['SCBOX_WOMENFOOT_LIST'] = $this->Pa_model->get_size_data('womenfootsize');
		$data['SCBOX_BANDSIZE_LIST'] = $this->Pa_model->get_size_data('womenbandsize');
		$data['SCBOX_CUPSIZE_LIST'] = $this->Pa_model->get_size_data('womencupsize');
		
		$data['form_type'] = 'edit';
		$this->load->view('scbox/scbox_booking',$data);
	}

			
}
