<?php
/*echo "<pre>";print_r($product_data); exit;*/
//echo "<pre>";print_r($stylist_assigned_pa);
?>
<section class="content-header">
    <h1>Assign PA</h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>home">Home</a></li>
      <li><a href="<?php echo base_url(); ?>lookcreator/manage_look">Manage User </a></li>
      <li class="active">Assign PA</li>
    </ol>
</section>
<section class="content page-look-details">

	<div class="box">
       	<label style="margin:10px 0px 20px 10px;"><!--input style="position: relative;top: 3px;" type="radio" class="gender" id="gender" name="gender" value="1"--> Female</label>    
       	<input type="hidden" name="user_id" id="user_id" value="<?php echo $this->uri->segment(3); ?>">
		    <!-- Female -->
	    <div class="box-body female">
		   <div class="row">
		    <div class="col-md-2">
				<div class="form-group">
				  <label>Body Shape<span class="text-red">*</span></label>
					<select name="body_shape" id="body_shape"  class="form-control input-sm" multiple="true">
					<?php if(!empty($body_shape)) {
						foreach($body_shape as $key=>$val){
							if(in_array($key, $stylist_assigned_pa['body_shape']))
							{
								echo '<option value="'.$key.'" selected="true" >'.$val.'</option>';
							}else
							{
								echo '<option value="'.$key.'" >'.$val.'</option>';
							}
						}
					} ?>
					</select>
				</div>
			</div>
		    <div class="col-md-2">
				<div class="form-group">
				<label>Body Style<span class="text-red">*</span></label>
				<select name="body_style" id="body_style"  class="form-control input-sm" multiple="true">
				 <?php if(!empty($style)) {
							foreach($style as $key=>$val){
								if(in_array($key, $stylist_assigned_pa['body_style']))
								{							
									echo '<option value="'.$key.'" selected="true" >'.$val.'</option>';
								}else
								{									
									echo '<option value="'.$key.'">'.$val.'</option>';
								}
							}
					} ?>
				</select>
				</div>
			</div>			
								
			<!--<div class="col-md-2">
				<div class="form-group">
				 <label>Select Stylist<span class="text-red">*</span> </label>
				 <select name="stylist_name" id="stylist_name" class="form-control input-sm">
				 <?php if(!empty($stylist_data)) {
					 echo '<option value="0">Select</option>';
							foreach($stylist_data as $val){
									echo '<option value="'.$val['id'].'">'.$val['stylist_name'].'</option>';
							}
					} ?>
				 </select>
				 </div>
			</div>-->

			<!--<div class="col-md-2">
				<div class="form-group">
				 <label>&nbsp;</label>
				  <div><button value="" type="button" name="broadcast" id="broadcast"  class="btn btn-primary btn-sm">
					  <i class="fa fa fa-thumbs-up"></i> SAVE </button>				 	
				 </div> 
			 	</div>
			</div>-->
		</div>	 	

        </div><!-- /.box-body Female -->

       	<label style="margin:10px 0px 20px 10px;"><!--input style="position: relative;top: 3px;" type="radio" class="gender" id="gender" name="gender" value="2"--> Male</label>

		<!-- Male -->
    	<div class="box-body male">
			<div class="row">
				<div class="col-md-2">
				 	<div class="form-group">
					  <label>Body Shape<span class="text-red">*</span></label>
						<select name="mens_body_shape" id="mens_body_shape"  class="form-control input-sm" multiple="true">
						<?php if(!empty($mens_body_shape)) {
							foreach($mens_body_shape as $key=>$val){
								if(in_array($key, $stylist_assigned_pa['mens_body_shape']))
								{							
									echo '<option value="'.$key.'" selected="true" >'.$val.'</option>';
								}else
								{									
									echo '<option value="'.$key.'">'.$val.'</option>';
								}
							}
						} ?>
						</select>
					</div>
				</div>
				 <div class="col-md-2">
					<div class="form-group">
						<label>Body Style<span class="text-red">*</span></label>
						<select name="mens_body_style" id="mens_body_style"  class="form-control input-sm" multiple="true">
						 <?php if(!empty($mens_style)) {
									foreach($mens_style as $key=>$val){
										if(in_array($key, $stylist_assigned_pa['mens_body_style']))
										{							
											echo '<option value="'.$key.'" selected="true" >'.$val.'</option>';
										}else
										{									
											echo '<option value="'.$key.'">'.$val.'</option>';
										}
									}
							} ?>
						</select>
					</div>
				</div>

				<div class="col-md-2">
					<div class="form-group">
						<label>Work Style<span class="text-red">*</span></label>
						<select name="mens_work_style" id="mens_work_style"  class="form-control input-sm" multiple="true">
						 <?php if(!empty($mens_work_style)) {
									foreach($mens_work_style as $key=>$val){
										if(in_array($key, $stylist_assigned_pa['mens_work_style']))
										{							
											echo '<option value="'.$key.'" selected="true" >'.$val.'</option>';
										}else
										{									
											echo '<option value="'.$key.'">'.$val.'</option>';
										}
									}
							} ?>
						</select>
					</div>
				</div>
									
				<div class="col-md-2">
					<div class="form-group">
						<label>Body Personal Style<span class="text-red">*</span></label>
						<select name="mens_personal_style" id="mens_personal_style"  class="form-control input-sm" multiple="true">
						 <?php if(!empty($mens_personal_style)) {
									foreach($mens_personal_style as $key=>$val){
										if(in_array($key, $stylist_assigned_pa['mens_personal_style']))
										{							
											echo '<option value="'.$key.'" selected="true" >'.$val.'</option>';
										}else
										{									
											echo '<option value="'.$key.'">'.$val.'</option>';
										}
									}
							} ?>
						</select>
					</div>
				</div>			

				<!--<div class="col-md-2">
					<div class="form-group">
						 <label>Select Stylist<span class="text-red">*</span> </label>
						 <select name="mens_stylist_name" id="mens_stylist_name" class="form-control input-sm">
						 <?php if(!empty($stylist_data)) {
							 echo '<option value="0">Select</option>';
									foreach($stylist_data as $val){
											echo '<option value="'.$val['id'].'">'.$val['stylist_name'].'</option>';
									}
							} ?>
						 </select>
					</div>
				</div>-->
				
			</div>
			<div id="message_display"></div>
    	</div><!-- /.box-body Male -->
    	<div class="col-md-2">
			<div class="form-group">
		 		<label>&nbsp;</label>
		  		<div>
		  		<button value="" type="button" name="save_pa" id="save_pa"  class="btn btn-primary btn-sm">
			  	<i class="fa fa fa-thumbs-up"></i> SAVE </button>				 		
	 		</div> 	 		
		</div>

	 	</div>
  </div><!-- /.box -->
</section>
<script type="text/Javascript">
 	var gender=0;
 	$('.gender').click(function() {
 		gender = $(this).val();
 		if(gender == 1){
 			$(".male").addClass("hide");
 			$(".female").removeClass("hide");
 		}else if(gender == 2){
 			$(".male").removeClass("hide");
 			$(".female").addClass("hide");
 		}
 	});

	$( document ).ready(function() {
		$( "#save_pa" ).click(function() {
			//var gender = $("input[type='radio'].gender:checked").val();
			var user_id = $('#user_id').val();			
			var body_shape = $('#body_shape').val();
			var body_style = $('#body_style').val();			
			//var stylist_name = $('#stylist_name').val();
			var mens_body_shape = $('#mens_body_shape').val();
			var mens_body_style = $('#mens_body_style').val();
			var mens_personal_style = $('#mens_personal_style').val();
			var mens_work_style = $('#mens_work_style').val();				

			if(user_id.trim() == ''){
				$('#message_display').html('Something went wrong');
				}else if(body_shape == '' || body_shape<=0){
					$('#message_display').html('<span class="text-red" >Please select Body Shape</span>');
					}else if(body_style == '' || body_style<=0){
						$('#message_display').html('<span class="text-red" >Please select Body Style</span>');
					}else if(mens_body_shape == '' || mens_body_shape<=0){
						$('#mens_message_display').html('<span class="text-red" >Please select Body Shape</span>');
					}else if(mens_body_style == '' || mens_body_style<=0){
						$('#mens_message_display').html('<span class="text-red" >Please select Body Style</span>');
					}else if(mens_personal_style == null || mens_personal_style.toString().trim() == '' || mens_personal_style<=0){
						$('#mens_message_display').html('<span class="text-red" >Please select Personal Style</span>');
						}else if(mens_work_style == null || mens_work_style.toString().trim() == '' || mens_work_style<=0){
						$('#mens_message_display').html('<span class="text-red" >Please select Work Style</span>');	
						}else{
							$.ajax({
							 type: "POST",
							 url: "<?php echo base_url(); ?>"+"Users/assign_pa_save/",
							 data: { user_id: user_id , body_shape: body_shape, body_style:body_style,mens_body_shape: mens_body_shape,mens_personal_style:mens_personal_style,mens_work_style:mens_work_style, mens_body_style:mens_body_style },
							 cache:false,
							 success:
								  function(result){
									  if(result == 'Ok')
									  {
									  	$('#message_display').html('<span class="text-green" >Successfully Saved</span>');
									  }else
									  {
									  	if(result == 'No')
									  	{
										  	$('#message_display').html('<span class="text-red" >Error  </span>');							  	
										  	$("#broadcast").attr("disabled","disabled");
										}else
										{
											$('#message_display').html('<span class="text-red" >Error</span>');
										}
									  }							  
									
								  }

							});

						}
		});

		/*$( "#mens_broadcast" ).click(function() {
			var gender = $("input[type='radio'].gender:checked").val();
			var user_id = $('#user_id').val();
			var mens_body_shape = $('#mens_body_shape').val();
			var mens_body_style = $('#mens_body_style').val();
			var mens_personal_style = $('#mens_personal_style').val();
			var mens_work_style = $('#mens_work_style').val();			
			var stylist_name = $('#mens_stylist_name').val();
			

			if(user_id.trim() == ''){
				$('#mens_message_display').html('Something went wrong');
				}else if(mens_body_shape == '' || mens_body_shape<=0){
					$('#mens_message_display').html('<span class="text-red" >Please select Body Shape</span>');
					}else if(mens_body_style == '' || mens_body_style<=0){
						$('#mens_message_display').html('<span class="text-red" >Please select Body Style</span>');
						}else if(stylist_name.trim() == '' || stylist_name<=0){
						$('#mens_message_display').html('<span class="text-red" >Please select Stylist</span>');
						}else if(mens_personal_style == null || mens_personal_style.toString().trim() == '' || mens_personal_style<=0){
						$('#mens_message_display').html('<span class="text-red" >Please select Personal Style</span>');
						}else if(mens_work_style == null || mens_work_style.toString().trim() == '' || mens_work_style<=0){
						$('#mens_message_display').html('<span class="text-red" >Please select Work Style</span>');
						}else{
							$.ajax({
							 type: "POST",
							 url: "<?php echo base_url(); ?>"+"Users/assign_pa_save/",
							 data: { user_id: user_id , mens_body_shape: mens_body_shape,mens_personal_style:mens_personal_style,mens_work_style:mens_work_style, mens_body_style:mens_body_style,stylist_name:stylist_name,gender:gender },
							 cache:false,
							 success:
								  function(result){								  
									  console.log(result);
									  if(result == 'Ok')
									  {
									  	$('#mens_message_display').html('<span class="text-green" >Successfully Broadcast the look</span>');
									  }else
									  {
									  	if(result == 'No')
									  	{
										  	$('#mens_message_display').html('<span class="text-red" >Look Cannot be Broadcasted As Product used count exceeded the set limit  </span>');							  	
										  	$("#broadcast").attr("disabled","disabled");
										}else
										{
											$('#mens_message_display').html('<span class="text-red" >Error</span>');
										}
									  }							  
									
								  }

							});

						}
		});*/

		$('.nav-item-look').addClass("active");
	});

</script>
