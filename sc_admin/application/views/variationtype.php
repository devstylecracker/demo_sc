<?php
$users_menu=$this->data['users_menu'];
// print_r($variationtype_details);exit;
// echo "<pre>"; print_r($categories);exit;
// echo $variationtype_details->prod_sub_cat_id;exit;

if(@$variationtype_details!=NULL)
{
  $page="Edit";
}else
{
  $page="Add";
}
?>
<section class="content-header">
  <h1>Variation Type </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>category">Categories</a></li>
    <li class="active"><?php echo $page;?> Variation Type</li>
  </ol>
</section>

<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo $page;?> Variation-Type</h3>
    </div><!-- /.box-header -->
    <form role="form" name="frmcategory" id="frmcategory" action="" method="post" enctype="multipart/form-data">
      <div class="box-body">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
                <label for="categoryName" class="control-label">Product Category
                <span class="text-red">*</span></label>
                <select class="form-control select2" name="category" id="category" onchange="get_subCategory(this.value,'');">
                  <option value="">Select Category</option>
                  <?php foreach($categories as $cat){ ?>
                    <?php if($cat->is_delete==0){ ?>
                      <option value="<?php echo $cat->id; ?>"
                        <?php if(@$variationtype_details[0]->cat_id == @$cat->id) echo "selected";?>
                        ><?php echo $cat->name; ?></option>
                        <?php }}?>
                </select>
                <span class="text-red"><?php echo form_error('category'); ?></span>
              </div>
              <div class="form-group">
                      <label for="subcategoryName" class="control-label">Sub-Category Name
                      <span class="text-red">*</span></label>
                      <select class="form-control select2" name="sub_category" id="sub_category" >
                          <option value="">Select Subcategory </option>
                      </select>
                      <span class="text-red"><?php echo form_error('sub_category'); ?></span>
              </div>
              <div class="form-group">
                      <label for="variationtypeName" class="control-label">VariationType Name
                      <span class="text-red">*</span></label>
                      <input type="text" placeholder="Enter Variation-Type Name" name="variationtypeName" id="variationtypeName" value="<?php echo @$variationtype_details[0]->name;?>"  onblur="assignSlug()"  class="form-control"><span class="text-red"><?php echo form_error('variationtypeName'); ?></span>
              </div>
              <div class="form-group">
                      <label for="variationSlug" class="control-label">Slug
                      <span class="text-red">*</span></label>
                      <input type="text" placeholder="Enter Variation-Type Slug" name="variationtypeSlug" id="variationtypeSlug" value="<?php echo @$variationtype_details[0]->slug;?>" <?php //if(@$subcategory_details->slug!="") echo 'disabled'; ?> class="form-control"><span class="text-red"><?php echo form_error('variationtypeSlug'); ?></span>
                    </div>

                <div class="form-group">
                      <label class="control-label">Active</label>
                       <?php if(isset($variationtype_details[0]->status) && $variationtype_details[0]->status==0) { $check_no =  'checked="true"'; }else { $check_ok = 'checked="true"'; } ?>
                  <div>
                      <label class="radio-inline"> <input type="radio" value="1" name="active" checked="true" <?php echo @$check_ok; ?> > Yes</label>
                      <label class="radio-inline"> <input type="radio" value="0" name="active" <?php echo @$check_no; ?> > No</label>
                  </div>
                </div>

                <div class="form-group">
                      <label class="control-label">Select Size Guide</label>
                  <div> 


                      <select name="size_guide" id="size_guide" class="form-control select2" >
                        <option value="0" <?php if(isset($variationtype_details[0]->size_guide) && $variationtype_details[0]->size_guide == 0) { echo 'selected="true"'; } ?>>Default</option>
                        <option value="1" <?php if(isset($variationtype_details[0]->size_guide) && $variationtype_details[0]->size_guide == 1) { echo 'selected="true"'; } ?>>No Size Chart</option>
                        <option value="2" <?php if(isset($variationtype_details[0]->size_guide) && $variationtype_details[0]->size_guide == 2) { echo 'selected="true"'; } ?>>Top</option>
                        <option value="3" <?php if(isset($variationtype_details[0]->size_guide) && $variationtype_details[0]->size_guide == 3) { echo 'selected="true"'; } ?>>Bottom</option>
                        <option value="4" <?php if(isset($variationtype_details[0]->size_guide) && $variationtype_details[0]->size_guide == 4) { echo 'selected="true"'; } ?>>Footwear</option>
                        <option value="5" <?php if(isset($variationtype_details[0]->size_guide) && $variationtype_details[0]->size_guide == 5) { echo 'selected="true"'; } ?>>Top & Bottom</option>
                        <option value="6" <?php if(isset($variationtype_details[0]->size_guide) && $variationtype_details[0]->size_guide == 6) { echo 'selected="true"'; } ?>>Top Men</option>
                        <option value="7" <?php if(isset($variationtype_details[0]->size_guide) && $variationtype_details[0]->size_guide == 7) { echo 'selected="true"'; } ?>>Bottom Men</option>
                        <option value="8" <?php if(isset($variationtype_details[0]->size_guide) && $variationtype_details[0]->size_guide == 8) { echo 'selected="true"'; } ?>>Footwear Men</option>
                        <option value="9" <?php if(isset($variationtype_details[0]->size_guide) && $variationtype_details[0]->size_guide == 5) { echo 'selected="true"'; } ?>>Top & Bottom Men</option>
                      </select>
                  </div>
                </div>

                 <div class="form-group">

                      <label class="control-label">Image</label>

                      <div>

                        <input type="file" name="category_img" id="category_img" >

                      </div>


                      <?php 
                        if(@$variationtype_details[0]!=NULL){
                          if($variationtype_details[0]->id > 0){
                            $images = scandir('../assets/images/products_category/variation_types/'.$variationtype_details[0]->id,true);
                            if(!empty($images)){
                              echo '<img src="'.'../../../assets/images/products_category/variation_types/'.$variationtype_details[0]->id.'/'.$images[0].'" style="width:50%;">';
                            }
                          }
                        }
                      ?>
                    </div>

                </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                      <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i>  Submit</button>
                      <a href="<?php echo base_url(); ?>category/variationtype"><button type="button" class="btn btn-primary btn-sm btn-rad"><i class="fa fa-reply"></i> Back</button></a>
                      <button class="btn btn-default btn-sm" type="reset"><i class="fa fa-close"></i> Cancel</button>
                </div>
                </form>
              </div>
              </div><!-- /.box -->
                </section>
                <script type="text/javascript">
                function get_subCategory(cat_id,selected_id)
                {
                  //alert(cat_id);
                  $.ajax({
                    type:"POST",
                    url:"<?php echo base_url();?>Category/get_sub_categories",
                    data :{ cat_id:cat_id,selected_id:selected_id},
                    datatype : 'html',
                    success: function(data){
                      $('#sub_category').html(data);
                    }
                  });
                }

                function assignSlug() {
                  var x = document.getElementById("variationtypeName");
                  var slug = x.value.toLowerCase();
                  var newslug = slug.replace(/ /g,"-");
                  $("#variationtypeSlug").val(newslug);

                 }

                $(document).ready(function(){

                  <?php
                  if($page=="Edit")
                  {
                    ?>

                    get_subCategory('','<?php echo @$variationtype_details[0]->subcat_id; ?>');

                    <?php
                  }
                  ?>

                  $('.nav-item-category').addClass("active");
                  $('.nav-manage-var-type').addClass("active");

                });
                </script>
