<?php                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  ?><?php
//print_r(@$category_details);
//echo @$category_details->status;
//print_r($tagtype);
$users_menu=$this->data['users_menu'];
//echo "<pre>"; print_r($variationvalue_details);exit;
// print_r($sub_categories);exit;

if(@$tag!=NULL)
{
  $page="Edit";
}else
{
  $page="Add";
}
?>


<section class="content-header">
        <h1>Tags</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>
            <li><a href="<?php echo base_url(); ?>tag">Tags</a></li>
            <li class="active"><?php echo $page; ?> Tag</li>
          </ol>
</section>

<section class="content">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php echo $page; ?> New Tag</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" name="frmtag" id="frmtag" action="" method="post">
                  <div class="box-body">
                    <div class="row">
              <div class="col-md-4">
                    <div class="form-group">
                      <label for="tag_name" class="control-label">Tag Name
						          <span class="text-red">*</span></label>
                      <input type="text" placeholder="Enter New Tag Name" name="tag_name" id="tag_name" value="<?php if(@$tag->name!="") echo @$tag->name; else echo set_value('tag_name');?>"  onblur="assignSlug()" class="form-control"> <span class="text-red"><?php echo form_error('tag_name'); ?></span>
                    </div>
                    <div class="form-group">
                      <label for="tagSlug" class="control-label">Slug
						          <span class="text-red">*</span></label>
                      <input type="text" placeholder="Enter tag Slug" name="tag_slug" id="tag_slug" value="<?php if(@$tag->slug!="") echo @$tag->slug; else echo set_value('tag_slug');?>"  class="form-control"><span class="text-red"><?php echo form_error('tag_slug'); ?></span>
                    </div>
                    <div class="form-group">
                      <label for="tagtype" class="control-label">Tag Type
                      <span class="text-red">*</span></label>
                        <select class="form-control select2" name="tagtype">
                       <option value="">Select Tag Type</option>
                        <?php foreach($tagtype as $tagt){ ?>
                        <?php if($tagt->is_delete==0){ ?>
                        <option value="<?php echo $tagt->id; ?>"
                          <?php if(@$tag->tag_type_id == @$tagt->id) echo "selected";?>
                          ><?php echo $tagt->name; ?></option>
                        <?php }}?>
                      </select>
                      <span class="text-red"><?php echo form_error('tagtype'); ?></span>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Active</label>
                       <?php if(isset($tag->status) && $tag->status==0) { $check_no =  'checked="true"'; }else { $check_ok = 'checked="true"'; } ?>
                      <div>
                        <label class="radio-inline"> <input type="radio" value="1" name="active" checked="true"  <?php echo @$check_ok; ?> > Yes</label>
                        <label class="radio-inline"> <input type="radio" value="0" name="active"  <?php echo @$check_no; ?> > No</label>
                      </div>
                    </div>
                    </div>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Submit</button>
                    <a href="<?php echo base_url(); ?>tag"><button type="button" class="btn btn-primary btn-sm btn-rad"><i class="fa fa-reply"></i> Back</button></a>
                    <button class="btn btn-default btn-sm" type="reset"><i class="fa fa-close"></i> Cancel</button>
                  </div>
                </form>
              </div><!-- /.box -->
            </div>
          </div>
</section>
<script type="text/Javascript">
$( document ).ready(function() {
	$('.nav-item-category').addClass("active");
  $('.nav-manage-tags').addClass("active");
});

function assignSlug() {

    var x = document.getElementById("tag_name");
    var slug = x.value.toLowerCase();
    var newslug = slug.replace(/ /g,"-");
    $("#tag_slug").val(newslug);

}
</script>
