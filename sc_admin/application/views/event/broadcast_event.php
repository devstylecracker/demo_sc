<?php 
// echo "<pre>";print_r($event);?>
<section class="content-header">
  <h1>View Event</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url();?>">Home</a></li>
    <li><a href="<?php echo base_url();?>event/event_mgmt">Event Management</a></li>
    <li class="active">View Event</li>
  </ol>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Details</h3>

        </div>
        <!-- /.box-header -->
        <div class="box-body">
		
		<p><label>Name</label><br />
		<?php echo @$event['event_name']; ?>
		</p>
			
        <p><label>Short Description</label><br />
        <?php echo @$event['event_short_desc']; ?>
		</p>
		
		<p><label>Long Description</label><br />
         <?php echo @$event['event_long_desc']; ?>
		</p>
		
		<p><label>Start Date</label><br />
			<?php echo @$event['event_start_date']; ?>
		</p>
		
		<p><label>End date</label><br />
			<?php echo @$event['event_end_date']; ?>
		</p>
		
		</div>
      </div>
	  <div class="box box-primary">
	  <form action="<?php echo base_url().'event/broadcast_event/'.@$event['object_id']; ?>" role="form" id="collection_form" enctype="multipart/form-data" method="POST">
	<!-- pa -->
			<input type="hidden" class="form-control" name="pa_body_shape" id="pa_body_shape" value="">
			<input type="hidden" class="form-control" name="pa_age" id="pa_age" value="">
			<input type="hidden" class="form-control" name="pa_budget" id="pa_budget" value="">
			<input type="hidden" class="form-control" name="pa_pref1" id="pa_pref1" value="">
			<input type="hidden" class="form-control" name="pa_pref2" id="pa_pref2" value="">
			<input type="hidden" class="form-control" name="pa_pref3" id="pa_pref3" value="">
			<input type="hidden" class="form-control" name="gender" id="gender" value="">
			
		   <div class="box-header">
		   <h3 class="box-title">PA Info</h3>	
			<div class="pull-right">
			 <a href="#pa-popup" data-toggle="modal" onclick="get_pa_answers();" class="btn btn-info btn-sm" id="pa_popup_button" data-attr="broadcast-event">PA Setting...</a>
		   </div>	   
		   </div>
			<!-- /.box-header -->
			<div class="box-body" style="min-height:140px;">
			 <div class="form-group" id="pa_html">
			 <?php if(@$event['pa_body_shape'] != ''){ ?>
			<p><label>Body Shape</label><br />
				<?php echo @$this->Pa_model->get_answer(unserialize($event['pa_body_shape']),'pa_body_shape'); ?>
			</p>
			<?php } ?>
			<?php if(@$event['pa_age'] != ''){ ?>
			<p><label>Age Range</label><br />
				<?php echo @$this->Pa_model->get_answer(unserialize($event['pa_age']),'pa_age'); ?>
			</p>
			<?php } ?>
			<?php if(@$event['pa_budget'] != ''){ ?>
			<p><label>Budget</label><br />
				<?php echo @$this->Pa_model->get_answer(unserialize($event['pa_budget']),'pa_budget'); ?>
			</p>
			<?php } ?>
			<?php if(@$event['pa_pref1'] != ''){ ?>
			<p><label>Style Preference 1</label><br />
				<?php echo @$this->Pa_model->get_answer(unserialize($event['pa_pref1']),'pa_pref1'); ?>
			</p>
			<?php } ?>
			<?php if(@$event['pa_pref2'] != ''){ ?>
			<p><label>Style Preference 2</label><br />
				<?php echo @$this->Pa_model->get_answer(unserialize($event['pa_pref2']),'pa_pref2'); ?>
			</p>
			<?php } ?>
			<?php if(@$event['pa_pref3'] != ''){ ?>
			<p><label>Style Preference 3</label><br />
				<?php echo @$this->Pa_model->get_answer(unserialize($event['pa_pref3']),'pa_pref3'); ?>
			</p>
			<?php } ?>
			
				  </div>
			</div>
			<div class="box-footer text-right">
				<button class="btn btn-danger">Save</button>  
			</div>
	 <!-- /.pa -->
	 </form>
</div>
    </div>
	
	  
    <div class="col-md-6">
      <div class="box box-primary">
	  <div class="box-header with-border">
        <h3 class="box-title">Product Info</h3>
		</div>
        <div class="box-body">
          <ul class="products-list product-list-inline products-list-grid">
			
			<?php if(!empty($event['event_products'])){
				foreach($event['event_products'] as $val){ ?>
			
				<li class="item">
				  <div class="product-img">
					<img src="<?php echo $val['image']; ?>" alt="">
				  </div>
				</li>
			
			<?php }
				}else{
					echo "No Products found";
				} ?>
          </ul>
        </div>

      </div>
	  
	  <div class="box box-primary">
	  <div class="box-header with-border">
       <h3 class="box-title">Event Image</h3>
	   </div>
        <div class="box-body">
          <ul class="products-list product-list-inline products-list-grid">
			
			<?php if(@$event['object_image'] != ''){ ?>
			
				<li class="item">
				  <div class="product-img">
					<img src="<?php echo $this->config->item('event_img_url'); ?><?php echo $event['object_image']; ?>" alt="">
				  </div>
				</li>
			
			<?php }else{
					echo "No Image found";
				} ?>
          </ul>
        </div>

      </div>
		

    </div>
  </div>
  </div>
</section>
