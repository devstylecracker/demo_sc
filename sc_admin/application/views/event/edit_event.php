<?php
$users_menu=$this->data['users_menu'];
// echo "<pre>";print_r($event_data);exit;
?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<section class="content-header">
        <h1>Event</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>
            <li><a href="<?php echo base_url(); ?>lookcreator/manage_look">Event Manage</a></li>
            <li class="active">Edit Event</li>
          </ol>
</section>
<section class="content">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Event</h3>
                </div>
                <form role="form" name="frmtag" id="frmtag" action="<?php echo base_url(); ?>event/edit_event/<?php echo $event_data['object_id']; ?>" method="post" enctype="multipart/form-data">
                  <div class="box-body">
                    <div class="row">
				<div class="col-md-4">
                    <div class="form-group">
                      <label for="event_name" class="control-label">Event Name
					  <span class="text-red">*</span></label>
					  <input type="text" name="event_name" class="form-control input-sm" id="event_name" placeholder="Enter Event name" value="<?php if($event_data['object_name']!="") echo $event_data['object_name']; else echo set_value('event_name');?>" required > 
					   <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_data['object_id'];?>" > 
					  <span class="text-red"><?php echo form_error('event_name'); ?></span>
                    </div>

                    <div class="form-group">
                      <label for="event_short_desc" class="control-label">Enter short description
					  <span class="text-red">*</span></label>
					  <input type="text" name="event_short_desc" class="form-control input-sm" id="event_short_desc" placeholder="Enter short description" value="<?php if($event_data['event_short_desc']!="") echo $event_data['event_short_desc']; else echo set_value('event_short_desc');?>" required > 
					  <span class="text-red"><?php echo form_error('event_short_desc'); ?></span>
                    </div>
					
					<div class="form-group">
                      <label for="event_long_desc" class="control-label">Enter long description
					  <span class="text-red">*</span></label>
					  <textarea rows="4" cols="50" name="event_long_desc" id="event_long_desc" class="form-control input-sm" placeholder="Enter long description" required><?php if($event_data['event_long_desc']!="") echo $event_data['event_long_desc']; else echo set_value('event_long_desc');?></textarea>
					  <!--<input type="text" name="event_long_desc" class="form-control input-sm" id="event_long_desc" placeholder="Enter long description" value="<?php if($event_data['event_long_desc']!="") echo $event_data['event_long_desc']; else echo set_value('event_long_desc');?>" required > -->
					  <span class="text-red"><?php echo form_error('event_long_desc'); ?></span>
                    </div>

                    <div class="form-group">
                      <label for="event_venue" class="control-label">Enter Event venue
					  <span class="text-red">*</span></label>
					  <input type="text" name="event_venue" class="form-control input-sm" id="event_venue" placeholder="Enter Event venue" value="<?php if($event_data['event_venue']!="") echo $event_data['event_venue']; else echo set_value('event_venue');?>" required > 
					  <span class="text-red"><?php echo form_error('event_venue'); ?></span>
                    </div>
					
					<div class="form-group">
                      <label for="event_lat" class="control-label">Enter Event Latitide
					  <span class="text-red">*</span></label>
					  <input type="text" name="event_lat" class="form-control input-sm" id="event_lat" placeholder="Enter Event Latitide" value="<?php if($event_data['event_lat']!="") echo $event_data['event_lat']; else echo set_value('event_lat');?>" required > 
					  <span class="text-red"><?php echo form_error('event_lat'); ?></span>
                    </div>
					
					<div class="form-group">
                      <label for="event_long" class="control-label">Enter Event Longitude
					  <span class="text-red">*</span></label>
					  <input type="text" name="event_long" class="form-control input-sm" id="event_long" placeholder="Enter Event Longitude" value="<?php if($event_data['event_long']!="") echo $event_data['event_long']; else echo set_value('event_long');?>" required > 
					  <span class="text-red"><?php echo form_error('event_long'); ?></span>
                    </div>
					
					<div class="form-group">
                      <label for="event_start_date" class="control-label">Date From
					  <span class="text-red">*</span></label>
					  <input type="text" name="event_start_date" class="form-control input-sm" id="date_from" placeholder="Enter Date From" value="<?php if($event_data['event_start_date']!="") echo $event_data['event_start_date']; else echo set_value('event_start_date');?>" required > 
					  <span class="text-red"><?php echo form_error('event_start_date'); ?></span>
                    </div>

                    <div class="form-group">
                      <label for="event_end_date" class="control-label">Date to
					  <span class="text-red">*</span></label>
					  <input type="text" name="event_end_date" class="form-control input-sm" id="date_to" placeholder="Date to" value="<?php if($event_data['event_end_date']!="") echo $event_data['event_end_date']; else echo set_value('event_end_date');?>" required > 
					  <span class="text-red"><?php echo form_error('event_end_date'); ?></span>
                    </div>
					
					<div class="form-group">
                      <label for="event_link" class="control-label">Event Link</label>
					  <input type="text" name="event_link" class="form-control input-sm" id="event_link" placeholder="Event Link" value="<?php if(@$event_data['event_link']!="") echo $event_data['event_link']; else echo set_value('event_link');?>" required > 
					  <span class="text-red"><?php echo form_error('event_link'); ?></span>
                    </div>
					
					 <div class="form-group">
                      <label for="event_end_date" class="control-label">Event Image</label>
					  <input class="form-control1" id="event_img"  name="event_img" type="file">
                    </div>
					
                  </div>
                    </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                    <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i>  Submit</button>
                    <a href="<?php echo base_url(); ?>event/event_mgmt"><button type="button" class="btn btn-sm btn-primary btn-rad"><i class="fa fa-reply"></i> Back</button></a>
                    <button class="btn btn-sm btn-default" type="reset"><i class="fa fa-close"></i> Cancel</button>
                  </div>
                </form>
              </div>
</section>
<script type="text/Javascript">
	$( document ).ready(function() {
		$('.nav-item-event').addClass("active");
		$('.nav-manage-event').addClass("active");
	});
	
	$(function () {
		var date_from =  $("#date_from").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii" , autoclose: true });
		var date_to = $("#date_to").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii", autoclose: true });
	});
</script>
