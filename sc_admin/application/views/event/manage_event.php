<?php
// echo "<pre>";print_r($event_list);exit;
$users_menu=$this->data['users_menu'];
    if(!empty($this->data['total_rows']))
    {
      $total_rows = $this->data['total_rows'];

    }else
    {
       $total_rows="";
    }
?>
  <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Manage Event</h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>home">Home</a></li>
      <li class="active">Manage Event</li>
    </ol>
</section>
<section class="content">
        <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">List of Event <small class="label label-info"><?php echo @$total_rows; ?></small></h3>
                <div class="pull-right">
                  </div>
                </div>
                  <div class="box-body">

                       <?php echo $this->session->flashdata('feedback'); ?>
					<form name="frmsearch" class="form-group" id="frmsearch" action="<?php echo base_url(); ?>event/event_mgmt" method="post">
						<div class="row">
							<div class="col-md-2">
								<select name="search_by" id="search_by" class="form-control input-sm" >
									<option value="0">Search by</option>
									<option selected value="1" <?php echo set_value('search_by')==1 ? 'selected' : ''; ?>>Event ID</option>
									<option value="2" <?php echo set_value('search_by')==2 ? 'selected' : ''; ?>>Event Name</option>
									<option value="3" <?php echo set_value('search_by')==3 ? 'selected' : ''; ?>>Status</option>
								</select>
							</div>
							
							<div class="col-md-3">
								<div class="input-group">
									<input type="text" name="table_search" id="table_search" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo set_value('table_search'); ?>"/>
									<div class="input-group-btn">
										<button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</div>
						
							<div class="col-md-2">
								<a href="<?php echo base_url(); ?>event/event_mgmt" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
							</div>
							
							<div class="col-md-5">
								<div class="pull-right">
									<?php echo $this->pagination->create_links(); ?>
								</div>
							</div>
							
						</div>
					</form>

				  <div class="row">

					<?php if(!empty($event_list)) {
					 foreach($event_list as $val)
					{
					?>
        				<div class="col-md-6">
        				  <div class="product-row-wrp manage-look-wrp">
          				   <div class="row">
              					<div class="col-md-4">
              					<div class="product-img-wrp">
								<?php if($val['image'] != ''){ ?>
									<img src="<?php echo $this->config->item('event_img_url'); ?><?php echo $val['image']; ?>" >
								<?php }else { ?>
									<img src="<?php echo $this->config->item('event_img_url').'14843852381731.jpg'; ?>" >
								<?php } ?>
              					</div>
              					</div>
              				   <div class="col-md-8">
                                 <dl class="dl-horizontal">
                                  <dt>Name</dt>
                                  <dd><?php echo $val['name']; ?> <span class="badge"><?php echo $val['id']; ?></span></dd>
								  <dt>Created on</dt>
                                  <dd><?php echo $val['created_datetime']; ?></dd>
                                  <?php if($val['status']==1){ ?>
                                  <dt>Broadcasted Date</dt>
                                  <dd><?php echo $val['modified_datetime']; ?></dd>
                                  <?php } ?>
                                  <dt>Status</dt>
                                  <dd><?php if($val['status']==0){ echo "<span class='text-orange'>Pending</span>"; }else if($val['status']==3){ echo "<span class='text-green'>Broadcast</span>"; }else { echo "<span class='text-red'>Rejected</span>"; } ?></dd>
              					          <!--dt>Product Code</dt>
                                  <dd>25062015</dd-->
                                </dl>
                            <div>
                                <?php if(in_array("52",$users_menu)) { ?>
                                  <a href="<?php echo base_url(); ?>event/view_event/<?php echo $val['id']; ?>"><button class="btn btn-primary btn-xs" type="submit"><i class="fa fa-eye"></i> View</button></a>
                                <?php } ?>
                                <?php if(in_array("53",$users_menu) && $val['deflag']!=1 && $val['status']!=2 ) { ?>
                                  <a href="<?php echo base_url(); ?>event/edit_event/<?php echo $val['id']; ?>"><button class="btn btn-primary btn-xs editProd" type="button"> <i class="fa fa-edit"></i> Edit</button></a>
                                <?php } ?>
                                <?php if(in_array("54",$users_menu)) { ?>
                                  <a href="<?php echo base_url(); ?>event/broadcast_event/<?php echo $val['id']; ?>"><button class="btn btn-primary btn-xs editProd" type="button"> <i class="fa fa-thumbs-up"></i> Broadcast</button></a>
                                <?php } ?>
								
								<?php if(in_array("55",$users_menu) && $val['deflag']!=1) { ?>
									<span class="test_delete_id">
									<a onclick="javascript:void(0)">
									<button type="button" class="btn btn-xs btn-primary btn-delete" data-id='<?php echo $val['id']; ?>' data-toggle="delete"><i class="fa fa-trash-o"></i> Delete</button></a></span>
								<?php } ?>

                            </div>
              				   </div>
          				    </div>
						   </div>
        				</div>
					<?php
					 } }else{ echo "Sorry no records found."; }
					?>





				</div><!-- /.box-body -->


		  <div><?php echo $this->pagination->create_links(); ?></div>
		</div>
  </section><!-- /.content -->
<script type="text/Javascript">
	$( document ).ready(function() {
		
			$('[data-toggle=delete]').confirmation({
			title:'Are you sure?',
			onConfirm : function(){
				var del_id = $(this).closest('.test_delete_id').find('button').attr('data-id');
				$.ajax({
					 type: "POST",
					 url: "<?php echo base_url(); ?>"+"event/delete_object/",
					 data: { id: del_id },
					 cache:false,
					 success:
						  function(data){
							location.reload(); //as a debugging message.
						  }

					});
				}
			});
			
		$('.nav-item-event').addClass("active");
		$('.nav-manage-event').addClass("active");
	});


</script>