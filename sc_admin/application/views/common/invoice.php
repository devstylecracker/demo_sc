
<!doctype html>
<html>
<head>
<meta charset="utf-8">

</head>

<body>
<div style="text-align: center;font-weight: 700;font-size: 16px;padding: 5px;">
<strong>TAX INVOICE</strong>
</div>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="4" style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;">
                    <table>
                        <tr>
                             <td>
                                <p class="boldup"><strong> KANVAS CONSULTANCY SERVICES PRIVATE LIMITED </strong></p>
                                <p>Impression House, First Floor, 42A, G D Ambekar Marg, Wadala(west), Maharashtra , Mumbai-400031</p>
                                
                                   <strong>CIN NO: U74140MH2005PTC153356<br />
                                    GSTIN/UN: 27AACCK7589B1ZC<br />
                                    CONTACT: 08048130591<br /></strong>
                                
                                <p>EMAIL: customercare@stylecracker.com<br />
                                www.stylecracker.com </p>
                            </td>
                            
                            <td>

                                
                            </td>
                        </tr>
                    </table>
                </td>
                <td colspan="2" style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;">
                    <table>
                        <tr>
                             <td>
                                    
                            </td>
                            
                            <td>

                                <p style="font-weight: 700;"><strong>Invoice No.</strong></p>
                                 <p><span style="text-transform: uppercase;"><?php echo $order_no; ?></span><br /></p>
                                 
                                <p style="font-weight: 700;"><strong>Supplier's Ref </strong></p>
                                 <p><span style="text-transform: uppercase;"><?php echo $order_no; ?></span></p>

                                  <p style="font-weight: 700;"><strong>Other Reference(s)</strong></p>
                                 
                            </td>
                        </tr>
                    </table>
                </td>
                <td colspan="2" style="border-bottom: 1px solid #ddd;">
                    <table>
                        <tr>
                             <td>
                                    
                            </td>
                            
                            <td>

                                <p style="font-weight: 700;"><strong>Dated</strong> </p>
                                 <p><span style="text-transform: uppercase;"><?php echo $created_datetime; ?></span><br /></p>
                                 
                                 <p style="font-weight: 700;"><strong>Mode/Terms of Payment</strong></p>
                                 <p style="text-transform: uppercase;"><?php echo $paymod; ?><br /></p>
                                 
                               
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="4" style="border-right: 1px solid #ddd;">
                    <table>
                        <tr>
                           <td>                               
                                <?php if(!empty($gift))
                                      {
                                ?>
                                <p>Address</p>
                                <p class="boldup"><strong><?php echo $gift['name']; ?></strong></p>
                                <p  style="text-transform: capitalize;"><?php echo $gift['address']; ?>  <?php echo $gift['pincode']; ?>  <?php echo $gift['city']; ?>,<?php echo $gift['state']; ?><br /><?php echo $gift['mobileno']; ?></p><br/>
                                <?php
                                      }else
                                      {
                                 ?> 
                                    <p>Buyer</p>
                                    <p class="boldup"><strong><?php echo $first_name.' '.$last_name; ?></strong></p>
                                    <p  style="text-transform: capitalize;"><?php echo $shipping_address; ?>  <?php echo $pincode; ?>  <?php echo $city_name; ?>,<?php echo $state_name; ?><br /><?php echo $mobile_no; ?></p>
                                
                                <?php      
                                      }
                                ?>
                            </td>
                            
                           
                        </tr>
                    </table>
                </td>
                <td colspan="3">
                    <table>
                        <tr>
                            <td>
                                <p style="font-weight: 700;"><strong>Terms of Delivery</strong></p>
                                <p style="text-transform: uppercase;"><?php echo $paymod; ?></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td>SR No.</td>
                <td> Particulars</td>
                <td>HSN/SAC</td>
                <td>GST Rate</td>
                <td>Quantity</td>
                <td>Rate</td>
                <td>Per</td>
                <td> Amount</td>
            </tr>
            
            
            <tr class="item">
                <td>1</td>
				<td>SCBOX <?php if($state_id != '21') echo "Interstate"; ?> </td>
				<td>00440225</td>
				<td>18%</td>
                <td>1</td>
                <td></td>
                <td></td>
                <td><?php echo $scbox_price; ?></td>

            </tr>
            
            <tr class="item">
                <td>2</td>
                <td>Less: Discount on SC Boxes</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
				<td></td>
                <td>(-)<?php echo $discount; ?></td>

            </tr>
            
			<?php if($state_id != '21'){ //out of state order ?>
				<tr class="item">
					<td> 3 </td>
					<td>OUTPUT IGST @ 18%</td>
					<td></td>
					<td></td>
					<td></td>
					<td>18</td>
					<td> %</td>
					<td><?php echo $order_tax_amount; ?></td>
				</tr>
            <?php }else{ //maharashtra order ?>
				<tr class="item">
					<td> 3 </td>
					<td>OUTPUT CGST @ 9%</td>
					<td></td>
					<td></td>
					<td></td>
					<td>9</td>
					<td> %</td>
					<td><?php echo $order_tax_amount/2; ?></td>
				</tr>
				<tr class="item">
					<td> 4 </td>
					<td>OUTPUT SGST @ 9%</td>
					<td></td>
					<td></td>
					<td></td>
					<td>9</td>
					<td> %</td>
					<td><?php echo $order_tax_amount/2; ?></td>
				</tr>
			
			<?php } ?>
            <tr class="item">
                <td> </td>
                <td style="font-weight: 700;text-align: right;">Total:  </td>               
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="font-weight: 700;"> Rs. <?php echo $order_total; ?> </td>
            </tr>
            <tr class="item">
                <td colspan="8">
                   Amount Chargeable (in words) <span style="font-weight: 400;font-size: 12px;text-align: right;font-style: italic;float: right;"> E. & E.O.</span><br />
                   <p class="boldup"><strong>Indian Rupees <?php echo $order_total_in_words; ?> only</strong></p> 
                </td>
               
            </tr>
            <tr class="heading">
                <td colspan="3">HSN/SAC</td>
				<td colspan="1"> Taxable Value</td>
				<?php if($state_id == '21'){ ?>
                <td colspan="2"> Central Tax</td>
                <td colspan="2"> State tax</td>
				<?php }else{ ?>
				<td colspan="4"> Integrated tax</td>
				<?php } ?>
            </tr>
             <tr class="item">
                <td colspan="3"></td>
                <td></td>
				<?php if($state_id == '21'){ ?>
					<td colspan="1"> Rate</td>
					<td colspan="1">Amount</td>
					<td colspan="1">Rate</td>
					<td colspan="1"> Amount</td>
				<?php }else{ ?>
					<td colspan="2">Rate</td>
					<td colspan="2"> Amount</td>
				<?php } ?>
            </tr>
             <tr class="item">
                <td colspan="3">00440225</td>
                <td><?php echo $tax_payble_amount; ?></td>     
				<?php if($state_id == '21'){ ?>				
					<td colspan="1"> 9%</td>
					<td colspan="1"><?php echo $order_tax_amount/2; ?></td>
					<td colspan="1">9% </td>
					<td colspan="1"><?php echo $order_tax_amount/2; ?></td>
				<?php }else{ ?>
					<td colspan="2">18% </td>
					<td colspan="2"><?php echo $order_tax_amount; ?></td>
				<?php } ?>
            </tr>
             <tr class="item">
                <td colspan="3">Total</td>
                <td><?php echo $tax_payble_amount; ?></td>
				<?php if($state_id == '21'){ ?>	
					<td colspan="1"></td>
					<td colspan="1"><?php echo $order_tax_amount/2; ?></td>
					<td colspan="1"></td>
					<td colspan="1"><?php echo $order_tax_amount/2; ?></td>
				<?php }else{ ?>
					<td colspan="2"></td>
					<td colspan="2"><?php echo $order_tax_amount; ?></td>
				<?php } ?>
            </tr>
             <tr class="item">
                <td colspan="8">
                    <p>Tax Amount(in words): <span style="text-transform: uppercase;font-weight: 700;"><strong>Indian Rupees <?php echo $order_tax_amount_in_words; ?> only<br /></strong></span></p>
                    <p>Remarks:<br />
                    Being Sales Booked To <?php echo $first_name.' '.$last_name; ?> Towards SC Box.</p>
                    <p> Company's PAN: <strong>AACCK7589C</strong></p>
                </td>
            </tr>
            <tr class="item">
                <td colspan="3">
                    
                </td>
                    
                <td colspan="5">
                    <p  style="text-transform: uppercase;font-weight: 700;"><strong>for KANVAS CONSULTANCY SERVICES PRIVATE LIMITED</strong></p>
                    <p style="text-align: right;">Authorised Signatory<br /></p>

                </td>
            </tr>
        </table>
        <footer style="text-align: center;padding-top: 5px;">
            SUBJECT TO MUMBAI JURISDICTION<br />
            This is a Computer Generated Invoice
        </footer>
    </div>
</body>
</html>
