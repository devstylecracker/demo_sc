<?php //error_reporting(0); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>StyleCracker | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min_v1.css" rel="stylesheet" type="text/css" />
 
 <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>

    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="<?php echo base_url(); ?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
       <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/normalize.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/main.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/spinner.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/slidePanel.css">
   
  <script src="<?php echo base_url(); ?>assets/dist/js/toc.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/prism.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/jquery-slidePanel.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/jquery.decapitate.js"></script>

    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
     <link href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/chosen/chosen.css">
     
    <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/chosen.css">-->
  <!--  <style type="text/css" media="all">
    /* fix rtl for demo */
    .chosen-rtl .chosen-drop { left: -9000px; }
  </style> -->
<style type="text/css">
  .loader {
  position: fixed;
  left: 0px;
  top: 0px;
  width: 100%;
  height: 100%;
  z-index: 9999;
  background: url(https://i.imgur.com/HvS8yF1.gif) center no-repeat #fff;
}
</style>

	 <!-- Custom css -->
     <link href="<?php echo base_url(); ?>assets/css/custom.css?v=2.0" rel="stylesheet" type="text/css" />

  
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
    
    <!--autosave-->
     <!--<script type="text/javascript" src="<?php //echo base_url(); ?>assets/plugins/sisyphus/sisyphus.js"></script>-->

    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js" type="text/javascript"></script>

    <!-- SlimScroll 1.3.0 -->
    <script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!--<script src="<?php //echo base_url(); ?>assets/js/jquery.elevatezoom.js" type="text/javascript"></script>-->

    <!-- Tool tip-->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap-tooltip.js" type="text/javascript"></script>
    <!-- Bootstrap Confirmation -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap-confirmation.js" type="text/javascript"></script>
   <!-- <script src="<?php echo base_url(); ?>assets/js/custom.js" type="text/javascript"></script>-->
    <script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/plugins/salvattore/salvattore.min.js" type="text/javascript" async></script>
  <script type="text/javascript">
$(window).load(function() {
  $(".loader").fadeOut("slow");
})
</script>

  </head>
  <body class="skin-blue sidebar-mini">
  <div class="loader"></div>

	<div class="wrapper">
	<?php
	//error_reporting(-1);
	include_once('nav.php'); ?>
