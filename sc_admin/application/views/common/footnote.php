<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 2.0 </div>
    <strong>Copyright &copy; 2016 <a href="https://stylecracker.com">Stylecracker</a>.</strong> All rights reserved. &nbsp;&nbsp;&nbsp;<i>Built with <i class="fa fa-heart text-red" ></i> by TECH Team </i>
  </footer>