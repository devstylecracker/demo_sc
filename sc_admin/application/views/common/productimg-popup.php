<head>

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/example.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/pygments.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/easyzoom.css" />

 <style type="text/css">
 .thumbnails {
    overflow-x: auto;
    margin: 1em 0;
    padding: 0;
    text-align: center;
    white-space: nowrap;
}

 </style>

</head>
<div class="container">

    <!-- Introduction -->
    <section id="introduction">
      <div class="easyzoom easyzoom--overlay easyzoom--with-thumbnails">
        <a href="<?php echo base_url(); ?>assets/example-images/1.jpg">
          <img src="<?php echo base_url(); ?>assets/example-images/1.jpg" alt="" width="auto" height="450" />
        </a>
      </div>

      <ul class="thumbnails">
        
        <li>
          <a href="<?php echo base_url(); ?>assets/example-images/2.jpg" data-standard="<?php echo base_url(); ?>assets/example-images/2.jpg">
            <img src="<?php echo base_url(); ?>assets/example-images/2.jpg" alt="" />
          </a>
        </li>
        <li>
          <a href="<?php echo base_url(); ?>assets/example-images/1.jpg" data-standard="<?php echo base_url(); ?>assets/example-images/1.jpg">
            <img src="<?php echo base_url(); ?>assets/example-images/1.jpg" alt="" />
          </a>
        </li>
        <li>
          <a href="<?php echo base_url(); ?>assets/example-images/2.jpg" data-standard="<?php echo base_url(); ?>assets/example-images/2.jpg">
            <img src="<?php echo base_url(); ?>assets/example-images/2.jpg" alt="" />
          </a>
        </li>
        <li>
          <a href="<?php echo base_url(); ?>assets/example-images/1.jpg" data-standard="<?php echo base_url(); ?>assets/example-images/1.jpg">
            <img src="<?php echo base_url(); ?>assets/example-images/1.jpg" alt="" />
          </a>
        </li>
        <li>
          <a href="<?php echo base_url(); ?>assets/example-images/2.jpg" data-standard="<?php echo base_url(); ?>assets/example-images/2.jpg">
            <img src="<?php echo base_url(); ?>assets/example-images/2.jpg" alt="" />
          </a>
        </li>
        <li>
          <a href="<?php echo base_url(); ?>assets/example-images/1.jpg" data-standard="<?php echo base_url(); ?>assets/example-images/1.jpg">
            <img src="<?php echo base_url(); ?>assets/example-images/1.jpg" alt="" />
          </a>
        </li>
        <li>
          <a href="<?php echo base_url(); ?>assets/example-images/2.jpg" data-standard="<?php echo base_url(); ?>assets/example-images/2.jpg">
            <img src="<?php echo base_url(); ?>assets/example-images/2.jpg" alt="" />
          </a>
        </li>
        <li>
          <a href="<?php echo base_url(); ?>assets/example-images/3.jpg" data-standard="<?php echo base_url(); ?>assets/example-images/3.jpg">
            <img src="<?php echo base_url(); ?>assets/example-images/3.jpg" alt="" />
          </a>
        </li>
        <li>
          <a href="<?php echo base_url(); ?>assets/example-images/4.jpg" data-standard="<?php echo base_url(); ?>assets/example-images/4.jpg">
            <img src="<?php echo base_url(); ?>assets/example-images/4.jpg" alt="" />
          </a>
        </li>

      </ul>

    </section>





    

  </div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/easyzoom.js"></script>
  <script>
    // Instantiate EasyZoom instances
    var $easyzoom = $('.easyzoom').easyZoom();

    // Setup thumbnails example
    var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');

    $('.thumbnails').on('click', 'a', function(e) {
      var $this = $(this);

      e.preventDefault();

      // Use EasyZoom's `swap` method
      api1.swap($this.data('standard'), $this.attr('href'));
    });

    // Setup toggles example
    var api2 = $easyzoom.filter('.easyzoom--with-toggle').data('easyZoom');

    $('.toggle').on('click', function() {
      var $this = $(this);

      if ($this.data("active") === true) {
        $this.text("Switch on").data("active", false);
        api2.teardown();
      } else {
        $this.text("Switch off").data("active", true);
        api2._init();
      }
    });
  </script>