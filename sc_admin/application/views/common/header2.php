<?php error_reporting(0); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>StyleCracker | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/seventeen/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url(); ?>assets/seventeen/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>assets/seventeen/bootstrap/js/bootstrap.min.js"></script>

  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/seventeen/plugins/iCheck/flat/blue.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/seventeen/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/seventeen/plugins/daterangepicker/daterangepicker.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/seventeen/plugins/select2/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/seventeen/dist/css/AdminLTE.min.css?v=1.5">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/seventeen/dist/css/skins/_all-skins.min.css?v=1.0">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/seventeen/plugins/magnific-popup/magnific-popup.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />
  <!-- Custom css -->
  <link href="<?php echo base_url(); ?>assets/css/style.css?v=1.2" rel="stylesheet" type="text/css" />


  <!-- <link href="<?php echo base_url(); ?>assets/custom.css?v=1.0" rel="stylesheet" type="text/css" /> -->


  <style type="text/css">
  .loader {
  position: fixed;
  left: 0px;
  top: 0px;
  width: 100%;
  height: 100%;
  z-index: 9999;
  background: url(https://i.imgur.com/HvS8yF1.gif) center no-repeat transparent;
}
.loadproduct {
 position: fixed;
    float: right;
    top: 0;
    width: 161%;
    height: 100%;
    z-index: 9999;
  background: url(https://i.imgur.com/HvS8yF1.gif) center no-repeat transparent;
}
.loadproduct1 {
 position: fixed;
    float: right;
    top: 0;
    width: 178%;
    height: 100%;
    z-index: 9999;
  background: url(https://i.imgur.com/HvS8yF1.gif) center no-repeat transparent;
}
.loaders {
  position: relative;
  left: 50%;
  top: 50%;
  width: 100%;
  height: 100%;
  z-index: 9999;
 
}
</style>
<script type="text/javascript">
$(window).load(function() {
  $(".loader").fadeOut("slow");
})
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113226308-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-113226308-1');
</script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
 <div class="loader"></div>
 <div class="loadproduct" style="display: none;"></div>
 <div class="loadproduct1" style="display: none;"></div>
<div class="wrapper">
	<?php
      if(strtolower($this->uri->segment(1))=='product_curation')
      {
        
      }else
      {
        include('nav.php');
      }

?>

  
