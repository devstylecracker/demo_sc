<?php $users_menu=$this->data['users_menu']; ?>
      <aside class="main-sidebar">
        <section class="sidebar">
          <ul class="sidebar-menu">
            <?php if($this->session->userdata('role_id') != '7'  && $this->session->userdata('role_id') != '8' )
                  {
            ?>
              <li class="nav-item-dashboard"><a href="<?php echo base_url(); ?>home"><i class="fa fa-dashboard"></i>
                <span>Dashboard</span></a></li>
            <?php
                  }
            ?>
         <?php if(in_array("1",$users_menu)) { ?>
            <li class="nav-item-category treeview">
             <a href="#">
               <i class="fa fa-sitemap"></i>
               <span>Category Management</span>
               <i class="fa fa-angle-left pull-right"></i>
             </a>

             <ul class="treeview-menu">		    

            <?php if($this->session->userdata('role_id')!=6){ ?>
             <?php if(in_array("1",$users_menu)) { ?>
               <li  class="nav-manage-cat"><a href="<?php echo base_url(); ?>categories"><i class="fa fa-circle-o"></i>Manage Categories</a></li>
            <?php } ?>
            <?php if(in_array("1",$users_menu)) { ?>
               <!-- <li  class="nav-manage-cat1"><a href="<?php echo base_url(); ?>brandpa"><i class="fa fa-circle-o"></i>Brand Personalisation Mapping</a></li> -->
            <?php } ?>
            <?php if(in_array("1",$users_menu)) { ?>
              <!--  <li  class="nav-manage-cat2"><a href="<?php echo base_url(); ?>brand_categories_manage"><i class="fa fa-circle-o"></i>Brand Categories Manage</a></li> -->
            <?php } ?>
            <?php if(in_array("1",$users_menu)) { ?>
               <li  class="nav-manage-cat3"><a href="<?php echo base_url(); ?>attributes"><i class="fa fa-circle-o"></i>Manage Attributes</a></li>
            <?php } ?>
            <?php } ?>

            <?php if(in_array("34",$users_menu)) { ?>
            <li class="nav-manage-tags">
              <a href="<?php echo base_url(); ?>Tag"><i class="fa fa-circle-o"></i><span>Manage Tags</span>
             </a>
           </li>
            <?php } ?>
             </ul>
           </li>
       <?php } ?>

         <?php if(in_array("39",$users_menu) || in_array("70",$users_menu)) { ?>
           <li class="nav-item-product"><a href="<?php echo base_url(); ?>product_new"><i class="fa fa-th-large"></i> <span>Product Management</span> 
           <i class="fa fa-angle-left pull-right"></i>
           </a>
             <ul class="treeview-menu">
              <?php /*if(in_array("39",$users_menu)) { ?>
                 <li class="nav-item-product"><a href="<?php echo base_url(); ?>product_new"><i class="fa fa-circle-o"></i> <span>Product Management</span> </a></li>
                 <!-- <li class="nav-item-product"><a href="<?php echo base_url(); ?>products_tagging"><i class="fa fa-circle-o"></i> <span>Product Tagging</span> </a></li> -->

              <?php }*/ ?>
              
               <?php if(in_array("39",$users_menu)) { ?>
                 <li class="nav-item-product"><a href="<?php echo base_url(); ?>product_management"><i class="fa fa-circle-o"></i> <span>Product Management</span> </a></li>
                 <!-- <li class="nav-item-product"><a href="<?php echo base_url(); ?>products_tagging"><i class="fa fa-circle-o"></i> <span>Product Tagging</span> </a></li> -->
              <?php } ?>
              <?php if(in_array("70",$users_menu)) { ?>
                 <!-- <li class="nav-item-product"><a href="<?php echo base_url(); ?>Product_personalisation"><i class="fa fa-circle-o"></i> <span>Product Personalisation Logic</span> </a></li> -->
                 <!-- <li class="nav-item-product"><a href="<?php echo base_url(); ?>products_tagging"><i class="fa fa-circle-o"></i> <span>Product Tagging</span> </a></li> -->
              <?php } ?>
              <?php if(in_array("70",$users_menu)) { ?>
                 <li class="nav-item-product"><a href="<?php echo base_url(); ?>Product_personalisation_email"><i class="fa fa-circle-o"></i> <span>Personalised Email Logic</span> </a></li>
                 <!-- <li class="nav-item-product"><a href="<?php echo base_url(); ?>products_tagging"><i class="fa fa-circle-o"></i> <span>Product Tagging</span> </a></li> -->
              <?php } ?>

              <?php if(in_array("39",$users_menu)) { ?>
                 <!-- <li class="nav-item-product"><a href="<?php echo base_url(); ?>size"><i class="fa fa-circle-o"></i> <span>Size Sort</span> </a></li> -->
                 <!-- <li class="nav-item-product"><a href="<?php echo base_url(); ?>products_tagging"><i class="fa fa-circle-o"></i> <span>Product Tagging</span> </a></li> -->

              <?php } ?>

              <?php if( (in_array("39",$users_menu) && $this->session->userdata('user_id') == '18126') || ($this->session->userdata('user_id') == '20772') ) { ?>
                 <li class="nav-item-product"><a href="<?php echo base_url(); ?>product_delete"><i class="fa fa-circle-o"></i> <span>Product Delete</span> </a></li>
                 <!-- <li class="nav-item-product"><a href="<?php echo base_url(); ?>products_tagging"><i class="fa fa-circle-o"></i> <span>Product Tagging</span> </a></li> -->
              <?php } ?>
			  
			   <?php if(in_array("70",$users_menu)) { ?>
                 <li class="nav-item-product"><a href="<?php echo base_url(); ?>Products_tagging"><i class="fa fa-circle-o"></i> <span>Products Tagging</span> </a></li>
              <?php } ?>
			  
              </ul>
            </li>
         <?php } ?>

         <?php if( (in_array("62",$users_menu)) || (in_array("63",$users_menu))  ) { ?>
            <li class="nav-item-order"><a href="<?php echo base_url(); ?>manage_orders"><i class="fa fa-shopping-cart"></i> <span>Manage Orders</span> 
           <i class="fa fa-angle-left pull-right"></i>
           </a>
             <ul class="treeview-menu">
              <?php if(in_array("62",$users_menu)) { ?>
                <li class="nav-manage-orders"><a href="<?php echo base_url(); ?>manage_orders"><i class="fa fa-circle-o"></i> Manage Orders</a></li>
              <?php } ?>              
              <?php if(in_array("63",$users_menu)) { ?>
                <li class="nav-manage-orders-payment"><a href="<?php //echo base_url(); ?>view_orders_payment"><i class="fa fa-circle-o"></i> Manage Orders Payment</a></li>
              <?php } ?>
              </ul>
            </li>

            <?php } ?>

            
             
            <?php /*if(in_array("45",$users_menu)) { ?>
            <li class="nav-item-brand">
			 <a href="<?php echo base_url(); ?>brand"><i class="fa fa-bold"></i><span>Brands</span>
             </a>
           </li>
            <?php } */?>

        <!--<?php if( (in_array("52",$users_menu)) || (in_array("32",$users_menu)) || (in_array("50",$users_menu)) || (in_array("51",$users_menu)) || (in_array("49",$users_menu)) || (in_array("59",$users_menu)) ) { ?>
           <li class="nav-item-look treeview"><a href="#">
               <i class="fa fa-image"></i>
               <span>Look Management</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
             <ul class="treeview-menu">
				      <?php if(in_array("52",$users_menu)) { ?>
               <li class="nav-manage-looks"><a href="<?php echo base_url(); ?>lookcreator/manage_look"><i class="fa fa-circle-o"></i> Manage Looks</a></li>
				      <?php } ?>
			        <?php /*if(in_array("32",$users_menu)) { ?>
                <li class="nav-look-creator"><a href="<?php echo base_url(); ?>lookcreator"><i class="fa fa-circle-o"></i> Look Creator </a></li>
               <?php }*/ ?>

                <?php if(in_array("32",$users_menu)) { ?>
                <li class="nav-look-creator"><a href="<?php echo base_url(); ?>lookcreator_v2"><i class="fa fa-circle-o"></i> Look Creator </a></li>
               <?php } ?>

            <?php /*if(in_array("50",$users_menu)) { ?>
              <li class="nav-ss-look-creator"><a href="<?php echo base_url(); ?>Streetlook"><i class="fa fa-circle-o"></i><span>Street Style Look Creator </span>
               </a>
             </li>
            <?php }*/ ?>

            <?php if(in_array("50",$users_menu)) { ?>
              <li class="nav-ss-look-creator"><a href="<?php echo base_url(); ?>Streetlook_v2"><i class="fa fa-circle-o"></i><span>Street Style Look Creator</span>
               </a>
             </li>
            <?php } ?>

           <?php /*if(in_array("51",$users_menu)) { ?>
            <li class="nav-promo-look-creator"><a href="<?php echo base_url(); ?>Promotionallook"><i class="fa fa-circle-o"></i><span>Promotional Look Creator </span></a>
           </li>
            <?php }*/ ?>
          <?php if(in_array("49",$users_menu)) { ?>
            <li class="nav-upload-ss"><a href="<?php echo base_url(); ?>Imageupload"><i class="fa fa-circle-o"></i><span>Upload - Street Style</span></a>
              </li>
          <?php } ?>
            <?php if(in_array("59",$users_menu)) { ?>
            <li class="nav-upload-ss"><a href="<?php echo base_url(); ?>Stickerupload"><i class="fa fa-circle-o"></i><span>Upload - Sticker</span></a>
              </li>
          <?php } ?>
              </ul>
           </li>
        <?php } ?>-->


            <?php if((in_array("11",$users_menu)) || (in_array("15",$users_menu)) || (in_array("18",$users_menu)) ) { ?>
             <!--li class="header">Users</li-->
             <li class="nav-item-user treeview">
            <a href="#">
                 <i class="fa fa-users"></i>
                 <span>User Management</span>
                 <i class="fa fa-angle-left pull-right"></i>
               </a>
               <ul class="treeview-menu">
               <?php if(in_array("11",$users_menu)) { ?>
                 <li class="nav-admin-user"><a href="<?php echo base_url(); ?>users"><i class="fa fa-circle-o"></i> Admin Users </a></li>
                <?php } ?>
                <?php if(in_array("15",$users_menu)) { ?>
                     <li class="nav-website-user"><a href="<?php echo base_url(); ?>websiteusers"><i class="fa fa-circle-o"></i> Website Users </a></li>
                <?php } ?>
                <?php if(in_array("18",$users_menu)) { ?>
                   <li class="nav-brand-user"><a href="<?php echo base_url(); ?>brandusers"><i class="fa fa-circle-o"></i> Store Users </a></li>
                <?php } ?>
                 <?php if(in_array("15",$users_menu)) { ?>
                     <li class="nav-scbox-user"><a href="<?php echo base_url(); ?>websiteusers/scbox"><i class="fa fa-circle-o"></i> SC-Box Users </a></li>
                <?php } ?>
               </ul>
             </li>
            <?php } ?>


              <?php if(in_array("65",$users_menu)) { ?>
            <!--  <li class="nav-item-banners treeview">
            <a href="#">
                 <i class="fa fa-globe"></i>
                 <span>Homepage UI Settings</span>
                 <i class="fa fa-angle-left pull-right"></i>
               </a> -->
               <ul class="treeview-menu">
               <?php if(in_array("65",$users_menu)) { ?>
                 <li class="nav-banners-list"><a href="<?php echo base_url(); ?>webui/Banners/"><i class="fa fa-circle-o"></i> Default Banners </a></li>
                <?php } ?>
                <?php if(in_array("65",$users_menu)) { ?>
                 <li class="nav-banners-list2"><a href="<?php echo base_url(); ?>webui/Banners/get_event_based_sliders"><i class="fa fa-circle-o"></i> Event Based Banners</a></li>
                <?php } ?>
                <?php if(in_array("65",$users_menu)) { ?>
                 <li class="nav-banners-list3"><a href="<?php echo base_url(); ?>webui/Homepage_settings/top_looks"><i class="fa fa-circle-o"></i> Top Looks </a></li>
                <?php } ?>
                <?php if(in_array("65",$users_menu)) { ?>
                 <li class="nav-banners-list4"><a href="<?php echo base_url(); ?>webui/Homepage_settings/top_category"><i class="fa fa-circle-o"></i> Top Product Categories </a></li>
                <?php } ?>
                <?php if(in_array("65",$users_menu)) { ?>
                 <li class="nav-banners-list5"><a href="<?php echo base_url(); ?>webui/Homepage_settings/top_brands"><i class="fa fa-circle-o"></i> Top Brands </a></li>
                <?php } ?>
                 <?php //if(in_array("11",$users_menu)) { ?>
                <!--  <li class="nav-banners-list2"><a href="<?php echo base_url(); ?>webui/Homepage_settings/megamenu_category"><i class="fa fa-circle-o"></i> Mega-menu Settings </a></li> -->
                <?php // } ?>

                <?php  if(in_array("65",$users_menu)) { ?>
                <li class="nav-megamenu-women-backend"><a href="<?php echo base_url(); ?>webui/megamenu"><i class="fa fa-circle-o"></i> Mega-menu Women Settings </a></li>
               <?php } ?>

                <?php  if(in_array("65",$users_menu)) { ?>
                <li class="nav-megamenu-men-backend"><a href="<?php echo base_url(); ?>webui/megamenu/men"><i class="fa fa-circle-o"></i> Mega-menu Men Settings </a></li>
               <?php  } ?>

                <?php /* if(in_array("65",$users_menu)) { ?>
                <li class="nav-megamenu-men-backend"><a href="<?php echo base_url(); ?>webui/filtermanagement"><i class="fa fa-circle-o"></i> Filter Management </a></li>
               <?php  }*/ ?>
                <?php if(in_array("65",$users_menu)) { ?>
                 <li class="nav-banners-list6"><a href="<?php echo base_url(); ?>webui/Popups"><i class="fa fa-circle-o"></i> Popups </a></li>
                <?php } ?>
				
				<?php if(in_array("39",$users_menu) && $this->session->userdata('user_id') == '18126' ) { ?>
                 <li class="nav-item-list7"><a href="<?php echo base_url(); ?>Save_products"><i class="fa fa-circle-o"></i> <span>Manage Guest Home Page</span> </a></li>

				<?php } ?>
               
               </ul>
             </li>
             <?php } ?>
             <!--Brand setting-->
             <?php if(in_array("66",$users_menu)) { ?>
             <li class="nav-item-brand_setting treeview">
              <!--  <a href="#">
                 <i class="fa fa-globe"></i>
                 <span>Brand Page Settings</span>
                 <i class="fa fa-angle-left pull-right"></i>
               </a> -->
               <ul class="treeview-menu">
                     <?php if(in_array("66",$users_menu)) { ?>
                       <li class="nav-brand-list"><a href="<?php echo base_url(); ?>webui/Brand_setting/"><i class="fa fa-circle-o"></i>Brand Default Banners </a></li>
                      <?php } ?>
                      <?php if(in_array("66",$users_menu)) { ?>
                       <li class="nav-brand-list2"><a href="<?php echo base_url(); ?>webui/Brand_setting/get_event_based_sliders"><i class="fa fa-circle-o"></i> Brand Event Based Banners</a></li>
                      <?php } ?>
                      <?php if(in_array("66",$users_menu)) { ?>
                      <li class="nav-manage-brand-cat">
                        <a href="<?php echo base_url(); ?>BrandCategory"><i class="fa fa-circle-o"></i><span>Manage Brand Category</span>
                       </a>
                     </li>
                      <?php } ?>
                </ul>
              </li>
              <?php } ?>
			  
		<?php if(in_array("78",$users_menu)) { ?>
			<li class="nav-item-scbox treeview">
				<a href="#">
				  <i class="fa fa-image"></i><span>SC-Box Management</span><i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
				  <?php if(in_array("79",$users_menu)) { ?>
				   <li class="nav-add-scboxdata"><a href="<?php echo base_url(); ?>scbox/scbox_booking"><i class="fa fa-circle-o"></i> Add SC-Box User </a></li>
				  <?php } ?>
				</ul>
			</li>
		<?php } ?>
		
		<?php if(in_array("71",$users_menu)) { ?>
           <li class="nav-item-collection treeview"><!-- <a href="#">
               <i class="fa fa-image"></i>
               <span>Collection Management</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a> -->
             <ul class="treeview-menu">
				      <?php if(in_array("73",$users_menu)) { ?>
               <li class="nav-manage-collection"><a href="<?php echo base_url(); ?>collection_new/collection_mgmt"><i class="fa fa-circle-o"></i> Manage Collections</a></li>
				      <?php } ?>

                <?php if(in_array("74",$users_menu)) { ?>
                <li class="nav-collection-creator"><a href="<?php echo base_url(); ?>collection_new"><i class="fa fa-circle-o"></i> Create Collection </a></li>
               <?php } ?>
			   <?php if(in_array("75",$users_menu)) { ?>
			   <li class="nav-collections-img"><a href="<?php echo base_url(); ?>Collectionsimageupload"><i class="fa fa-circle-o"></i>Image Upload </a></li>
			    <?php } ?>
              </ul>
           </li>
        <?php } ?>
		
		<?php if(in_array("72",$users_menu)) { ?>
           <li class="nav-item-event treeview"><!-- <a href="#">
               <i class="fa fa-image"></i>
               <span>Event Management</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a> -->
             <ul class="treeview-menu">
				      <?php if(in_array("76",$users_menu)) { ?>
               <li class="nav-manage-event"><a href="<?php echo base_url(); ?>event/event_mgmt"><i class="fa fa-circle-o"></i> Manage Events</a></li>
				      <?php } ?>

                <?php if(in_array("77",$users_menu)) { ?>
                <li class="nav-event-creator"><a href="<?php echo base_url(); ?>event"><i class="fa fa-circle-o"></i> Create Event </a></li>
               <?php } ?>
              </ul>
           </li>
        <?php } ?>
		
               <!--Delivery Process-->
             <?php if(in_array("68",$users_menu)) { ?>
              <li class="nav-item-deliver_process treeview">
                <a href="javascript:void(0);">
                  <i class="fa fa-globe"></i>
                  <span>Delivery Process</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                      <?php if( (in_array("68",$users_menu)) &&  $this->session->userdata('role_id') != '7' &&  $this->session->userdata('role_id') != '6') { ?>
                        <li class="partner_mapping"><a href="<?php echo base_url(); ?>delivery_partner_mapping/"><i class="fa fa-circle-o"></i>Delivery Partner Mapping </a></li>
                       <?php } ?>
                       <?php if(in_array("68",$users_menu)) { ?>
                        <li class="nav_delivery_process"><a href="<?php echo base_url(); ?>delivery_process"><i class="fa fa-circle-o"></i> Delivery Process</a></li>
                       <?php } ?>
                </ul>
               </li>
              <?php } ?>
              
             <?php if(in_array("44",$users_menu)) { ?>
    			  <li class="nav-item-enquiry treeview">
    				<a href="<?php echo base_url(); ?>User_Enquiry">
    				  <i class="fa fa-envelope"></i>
    				  <span>User Enquiry</span>
    				</a>
    			   </li>
             <?php } ?>

            <?php if(in_array("58",$users_menu)) { ?>
            <li class="nav-item-database treeview">
        				<a href="#">
        				  <i class="fa fa-database"></i>
        				  <span>Databases</span>
        				</a>

                <ul class="treeview-menu">
                <li class="nav-item-dcat"><a href="<?php echo base_url(); ?>databases/database_category"><i class="fa fa-circle-o"></i> Category </a></li>
                <li class="nav-item-dimage"><a href="<?php echo base_url(); ?>databases/Dbimageupload"><i class="fa fa-circle-o"></i> Image Database </a></li>
                <li class="nav-item-dbimagecatalog"><a href="<?php echo base_url(); ?>databases/Dbimageupload/Dbimagecatalog"><i class="fa fa-circle-o"></i> <span>Image Catalog</span></a></li>
                <li class="nav-item-dcontacts"><a href="<?php echo base_url(); ?>databases/contacts"><i class="fa fa-circle-o"></i> Contacts Database </a></li>
               </ul>
    			  </li>
            <?php } ?>

             <?php if(in_array("60",$users_menu)) { ?>
              <li class="nav-item-inventory treeview">
              <a href="#">
                   <i class="fa fa-users"></i>
                   <span>Product Inventory</span>
                   <i class="fa fa-angle-left pull-right"></i>
              </a>
                <ul class="treeview-menu">
                  <?php if(in_array("60",$users_menu)) { ?>
                    <li class="nav-product-inventory"><a href="<?php echo base_url(); ?>productinventory_new"><i class="fa fa-circle-o"></i> Product Inventory </a></li>
                  <?php } ?>
                </ul>
              </li>
            <?php } ?>
			
			
			   
              <li class="nav-item-user treeview">
              <a href="#">
                   <i class="fa fa-inr"></i>
                   <span>Cost Control Setting</span>
                   <i class="fa fa-angle-left pull-right"></i>
              </a>
                <ul class="treeview-menu">
                 
                    <li class="nav-product-inventory"><a href="<?php echo base_url(); ?>cost_control"><i class="fa fa-circle-o"></i> Cost Control Setting </a></li>
					 <li class="nav-product-inventory"><a href="<?php echo base_url(); ?>cost_control/sales_report"><i class="fa fa-circle-o"></i> Sales Report</a></li>
                 
                </ul>
              </li>
           
			

              <?php if(in_array("64",$users_menu)) { ?>
              <li class="nav-item-discount treeview">
              <a href="#">
                   <i class="fa fa-gift"></i>
                   <span>Discount Setting</span>
                   <i class="fa fa-angle-left pull-right"></i>
              </a>
                <ul class="treeview-menu">     
                    <li class="nav-discount-mgmt"><a href="<?php echo base_url(); ?>DiscountManagement"><i class="fa fa-circle-o"></i> Discount Management </a></li>             
                    <li class="nav-discount-setting"><a href="<?php echo base_url(); ?>DiscountSetting"><i class="fa fa-circle-o"></i> Discount Setting </a></li>               
                    <li class="nav-discount-view"><a href="<?php echo base_url(); ?>DiscountSetting/discount"><i class="fa fa-circle-o"></i> Discount Setting view </a></li>
                    <li class="nav-coupon-setting"><a href="<?php echo base_url(); ?>CouponSettings"><i class="fa fa-circle-o"></i> Coupon Setting </a></li>    
                </ul>
              </li>
              <?php } ?>

             <?php //if(in_array("44",$users_menu)) { ?>
            <!-- <li class="nav-item-enquiry treeview">
            <a href="<?php //echo base_url(); ?>User_Enquiry">
              <i class="fa fa-envelope"></i>
              <span>User Enquiry</span>
            </a>
             </li> -->
             <?php //} ?>

            <!--li class="nav-item-enquiry treeview">
            <a href="http://192.168.1.59/DataDashboard/image-upload.html" target="_blank">
              <i class="fa fa-database"></i>
              <span>Databases</span>
            </a>
            </li-->          

           <?php if(in_array("67",$users_menu)) { ?>
           <li class="nav-item-cache">
               <a href="<?php echo base_url(); ?>Clearcache"><i class="fa fa-question"></i><span>Clear Cache</span>
             </a>
           </li>
           <?php } ?>


             <?php if($this->session->userdata('role_id') == '6') { ?>
            <li class="nav-item-api treeview">
            <a href="<?php echo base_url(); ?>Api_Details">
              <i class="fa fa-envelope"></i>
              <span>API</span>
            </a>
             </li>
             <?php } ?>

        <?php if(in_array("81",$users_menu)) { ?>
          <li class="nav-item-affiliate-users treeview">
            <a href="#">
                 <i class="fa fa-user"></i>
                 <span>Affiliate Users</span>                   
            </a>
            <ul class="treeview-menu">
              <li class="nav-affiliate-users"><a href="<?php echo base_url(); ?>AffiliateUsers"><i class="fa fa-shopping-cart"></i> View Orders </a></li>
            </ul>
          </li>
        <?php } ?>            

			
		<?php if(in_array("80",$users_menu)) { ?>	 
				<li class="nav-item-sms"> <a href="<?php echo base_url(); ?>send_sms"><i class="fa fa-send"></i><span>Send SMS</span>
             </a></li>  

		 <?php } ?>  

    <?php if( ($this->session->userdata('role_id')!=6) && ($this->session->userdata('role_id')!=8) ){ ?>
      <li class="nav-item-help"> <a href="<?php echo base_url(); ?>help"><i class="fa fa-info"></i><span>Help</span>
             </a></li>  
    <?php } ?>  
      <li class="nav-item-feedback">
               <a href="<?php echo base_url(); ?>feedback"><i class="fa fa-question"></i><span>Feedback</span>
             </a>
      </li>         
            

            
                
              

          
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
