<header class="main-header">

  <!-- Logo -->
  <a href="<?php echo base_url(); ?>home" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>SC</b></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>StyleCracker</b></span>
  </a>

  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <li class="dropdown user user-menu">
          <a >
          <img src="<?php echo base_url(); ?>assets/images/avatar.png" class="user-image" alt=""/>
          <span class="hidden-xs"><?php if(isset($_SESSION['first_name']) ||  isset($_SESSION['last_name'])) { echo $_SESSION['first_name'].' '.$_SESSION['last_name']; } ?></span>
        </a>
        </li>
        <li>
          <a href="<?php echo base_url(); ?>login/logout" class="">Sign out</a>
        </li>
      </ul>
    </div>

  </nav>
</header>
<?php include_once('left_nav.php'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <?php // echo $this->session->flashdata('feedback'); ?>
