 </div><!-- /.content-wrapper -->
 <!-- Upload js -->

<!--<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js" type="text/javascript"></script>-->
 <?php include_once('right_nav.php'); ?>

 <!--<script>
$( ".file" ).wrap( '<div class="btn btn-primary btn-file"> <i class="glyphicon glyphicon-folder-open"></i> &nbsp;Browse …</div>');
</script>-->

<link rel="stylesheet" href="<?php //echo base_url('assets/chosen/docsupport/prism.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/chosen/chosen.css'); ?>">

<script src="<?php echo base_url('assets/chosen/chosen.jquery.js'); ?>"></script>
<script src="<?php echo base_url('assets/chosen/docsupport/prism.js'); ?>" charset="utf-8"></script>
<script src="<?php echo base_url('assets/chosen/docsupport/init.js'); ?>" charset="utf-8"></script>
<div class="page-overlay">
<img class="loader" src="<?php echo base_url(); ?>assets/images/loading.gif">
</div>
 <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0
        </div>
        <strong>Copyright &copy; 2015 <a href="https://stylecracker.com">StyleCracker</a>.</strong> All rights reserved. &nbsp;&nbsp;&nbsp;<i>Built with <i class="fa fa-heart text-red" ></i> by TECH Team </i>
      </footer>
<?php $cookie_value = base_url(uri_string()); 
setcookie('sc_admin_last_url', $cookie_value, time() + (86400 * 30), "/");
?>
<?php include('popups.php'); ?>
  </body>
  <script src="<?php echo base_url(); ?>assets/js/scbox.js"></script>
</html>
