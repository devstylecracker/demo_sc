<!-- manage order popup start -->
<div id="modal-order-info" class="modal fade modal-order-info" role="dialog">
  <div class="modal-dialog  modal-sm">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><i class="icon-close"></i></button>
        <div class="box-inner"  id="box_order_data">
        </div>
      </div>
    </div>
  </div>
</div>
<!-- manage order popup end -->

<!-- scbox user popup start -->
<div id="modal-scbox-remark" class="modal fade modal-scbox-info" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><i class="icon-close"></i></button>
        <div class="box-inner"  id="box_remark_data">
        </div>
      </div>
    </div>
  </div>
</div>
<!-- scbox user popup end -->