<?php error_reporting(0);
// echo "<pre>";print_r($_SESSION);exit;?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>StyleCracker</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css?v=1.5" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/chosen/chosen.css">
  <!--  <style type="text/css" media="all">
    /* fix rtl for demo */
    .chosen-rtl .chosen-drop { left: -9000px; }
  </style> -->
	 <!-- Custom css -->
     <link href="<?php echo base_url(); ?>assets/css/custom.css?v=2.0" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

      <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/chosen/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/custom.js" type="text/javascript"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113226308-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-113226308-1');
</script>
  </head>
  <body class="skin-sc">
<!--	<div class="wrapper">-->
<header id="header" class="header1">
<div class="container">
<div class="navbar-header-md">
<span class="navbar-brand1">
<img src="https://www.stylecracker.com/assets/images/logo.png" class="logo logo-desktop" id="logo-desktop">
</span>
<nav id="bs-navbar" class="navbar-right primary-navbar">

<ul class="nav navbar-nav">
  <li><a class="user-name"><?php echo $this->session->userdata('first_name');?></a></li>
<li>
<a href="<?php echo base_url(); ?>login/logout" class="">Sign Out</a>
</li>
</ul>
</nav>
</div>
</div>

</header>
<main class="content-wrp">
