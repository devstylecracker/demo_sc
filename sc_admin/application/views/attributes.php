<section class="content">
<section class="content-header">
        <h1>Manage Attributes</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>">Home</a></li>
            <li class="active">Manage Attributes</li>
          </ol>
</section>
<section class="content">
	<div class="row">
	 <div class="col-md-4">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">
				 	<?php if($this->uri->segment(3) > 0){ ?>
					Edit Attributes
					<?php }else{ ?>
						Add New Attributes
					<?php } ?>
				</h3>
		</div><!-- /.box-header -->
<div class="box-body">
<?php if($this->uri->segment(3) > 0){ ?>
	<form name="frmcategory" id="frmcategory" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>attributes/attributes_edit/<?php echo $this->uri->segment(3); ?>">
<?php }else{ ?>
	<form name="frmcategory" id="frmcategory" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>attributes">
<?php } ?>

						<div class="form-group">
							<label class="control-label">Name
							</label>
							<input type="text" placeholder="" name="attribute_name" id="attribute_name" value="<?php if(isset($cat_data_data[0]['attribute_name'])){ echo @$cat_data_data[0]['attribute_name']; }else{ echo @$_POST['attribute_name']; } ?>" class="form-control" onblur="genarate_slug();"> <span class="text-red"><?php echo form_error('attribute_name'); ?></span>
							<p class="help-block">The name is how it appears on your site.</p>
                    	</div>

                    	<div class="form-group">
							<label class="control-label">Slug
							</label>
							<input type="text" placeholder="" name="attribute_slug" id="attribute_slug" value="<?php if(isset($cat_data_data[0]['attribute_slug'])){ echo @$cat_data_data[0]['attribute_slug']; }else{ echo @$_POST['attribute_slug']; } ?>" class="form-control" >
							<p class="help-block">The "slug" is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.</p>
                    	</div>

                    	<div class="form-group">
							<label class="control-label">Description
							</label>
							<textarea name="attribute_content" id="attribute_content" class="form-control" rows="3">
							<?php if(isset($cat_data_data[0]['attribute_content'])){ echo @$cat_data_data[0]['attribute_content']; }else{ echo @$_POST['attribute_content']; } ?>
							</textarea>
						</div>

                    	<div class="form-group">
							<label class="control-label">Attribute Type( Global and Category Specific )
							</label>
							<select name="attribute_parent[]" id="attribute_parent" class="form-control chosen-select" tabindex="7" multiple>
								<option value="-1">Global</option>
								<?php if(!empty($cat_data)){
									foreach($cat_data as $val){
										echo $val;
									}

									} ?>
							</select>
							<p class="help-block">Totally optional.</p>
                    	</div>

                    	
                    	<div class="form-group">
							<label class="control-label">Type</label>
							<select name="attribute_type" id="attribute_type" class="form-control" tabindex="7">
								<?php if(isset($cat_data_data[0]['attribute_type']) && $cat_data_data[0]['attribute_type']==0){ ?>
                    				<option value="0" selected>Drop Down</option>
                    			<?php }else{ ?>
                    				<option value="0">Drop Down</option>
                    			<?php } ?>

                    			<?php /* if(isset($cat_data_data[0]['attribute_type']) && $cat_data_data[0]['attribute_type']==1){ ?>
                    				<option value="1" selected>Text</option>
                    			<?php }else{ ?>
                    				<option value="1">Text</option>
                    			<?php }*/ ?>

                    		</select>
                    	</div>

                    	<div class="form-group">
							<label class="control-label">Show on web</label>
							<select name="show_on_web" id="show_on_web" class="form-control" tabindex="7">
                    			
                    			<?php if(isset($cat_data_data[0]['show_on_web']) && $cat_data_data[0]['show_on_web']==0){ ?>
                    				<option value="0" selected>No</option>
                    			<?php }else{ ?>
                    				<option value="0">No</option>
                    			<?php } ?>

                    			<?php if(isset($cat_data_data[0]['show_on_web']) && $cat_data_data[0]['show_on_web']==1){ ?>
                    				<option value="1" selected>Yes</option>
                    			<?php }else{ ?>
                    				<option value="1">Yes</option>
                    			<?php } ?>

                    		</select>
                    	</div>

                    	<div class="form-group">
							<label class="control-label">Default sort order</label>
							<select name="order" id="order" class="form-control" tabindex="7">
							<?php if(isset($cat_data_data[0]['order']) && $cat_data_data[0]['order']==0){ ?>
								<option value="0" selected>Default</option>
							<?php }else{ ?>
								<option value="0">Default</option>
							<?php }  ?>

							<?php if(isset($cat_data_data[0]['order']) && $cat_data_data[0]['order']==1){ ?>
								<option value="1" selected>Name</option>
							<?php }else{ ?>
								<option value="1">Name</option>
							<?php }  ?>

							<?php if(isset($cat_data_data[0]['order']) && $cat_data_data[0]['order']==2){ ?>
								<option value="2" selected>Id</option>
							<?php }else{ ?>
								<option value="2">Id</option>
							<?php }  ?>

                    			
                    		</select>
                    	</div>

                    	<div class="form-group">
							<label class="control-label">Sort</label>
							<input type="text" placeholder="" name="sort" id="sort" value="<?php if(isset($cat_data_data[0]['sort'])){ echo @$cat_data_data[0]['sort']; }else{ echo @$_POST['sort']; } ?>" class="form-control"> <span class="text-red"><?php echo form_error('sort'); ?></span>
							<p class="help-block">It will be display on website by ascending order</p>
                    	</div>


                    	<div class="form-group">

                    	<?php if($this->uri->segment(3) > 0){ ?>
							<button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Edit Attribute</button>
						<?php }else{ ?>
							<button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Add New Attribute</button>
						<?php } ?>

                    	</div>

					</form>
				
</div>
</div>
</div>
 <div class="col-md-8">
	 <div class="box">
		 <div class="box-header">
			 <h3 class="box-title">
					View Attributes
				 </h3>
				 <div class="pull-right">
				 <div class="input-group">
						 <input name="search_text" id="search_text" value=""  placeholder="Search" class="form-control input-sm pull-right" required>
						 <div class="input-group-btn">
							 <button type="button" name="submit" id="submit" value="Search" onclick="search_cat();" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
						 </div>
					 </div>
				 </div>
		 </div><!-- /.box-header -->
	<div class="box-body">

	<div class="table-responsive">
		<table class="table table-bordered table-striped dataTable" id="datatable">
								<thead>
								<tr>
										<th>ID</th>
										<th>Name</th>
										<th>Slug</th>
										<!--th>Count</th-->
										<th>Action</th>

								</tr>
								</thead>
			<tbody id="category_list">
			<?php if(!empty($attr_data)){
				foreach($attr_data as $val){
			?>
			<tr>
				<td><?php echo $val['id']; ?></td>
				<td><?php echo $val['attribute_name']; ?></td>
				<td><?php echo $val['attribute_slug']; ?></td>
				<!--td><?php echo $val['count']; ?></td-->
				<td>
					<a href="<?php echo base_url(); ?>attributes/attributes_edit/<?php echo $val['id']; ?>"><button class="btn btn-primary btn-xs editProd" type="button" ><i class="fa fa-edit"></i> Edit</button></a>
					<a href="<?php echo base_url(); ?>attrsettings/index/<?php echo $val['id']; ?>"><button class="btn btn-secondary btn-xs editProd" type="button" ><i class="fa fa-edit"></i> Settings </button></a>
					<button class="btn btn-xs btn-primary btn-delete" type="button" onclick="delete_category(<?php echo $val['id']; ?>);"><i class="fa fa-trash-o"></i> Delete</button>
				</td>
			</tr>
			<?php } } ?>
			</tbody>
		</table>
	</div>
</div>
</div>
</div>
</div>

</section>
<script type="text/javascript">
	
	function genarate_slug(){
		var category_name = $('#attribute_name').val();
		var slug = category_name.replace(/[^a-zA-Z0-9]/g, "-");
		$('#attribute_slug').val(slug.toLowerCase());
	}
	function delete_category(id){ 
		var result = confirm("Want to delete?");
		if (result) {
			$.ajax({
				url: "<?php echo base_url(); ?>attributes/check_product_count",
				type: 'POST',
				data: { id : id },
				cache :true,
				async: true,
				success: function(response) {
					if(response > 0){
						alert('Already have products in to the category still you want to delete the category');
					}else{
						$.ajax({
						url: "<?php echo base_url(); ?>attributes/delete_category",
						type: 'POST',
						data: { id : id },
						cache :true,
						async: true,
						success: function(response) {
							window.location="<?php echo base_url(); ?>attributes";
						},
						error: function(xhr) {
							alert('Something goes wrong');
							hide_cart_loader();
						}
					});
				}

				},
				error: function(xhr) {
					alert('Something goes wrong');
					hide_cart_loader();
				}
			});
		}
	}	

	function search_cat(){
		var search_text = $('#search_text').val();
		$.ajax({
			url: "<?php echo base_url(); ?>attributes/search_category",
			type: 'POST',
			data: { search_text : search_text },
			cache :true,
			async: true,
				success: function(response) {
					$('#category_list').html('');
					$('#category_list').html(response);
				},
				error: function(xhr) {
					alert('Something goes wrong');
					hide_cart_loader();
			}
		});

	}

</script>