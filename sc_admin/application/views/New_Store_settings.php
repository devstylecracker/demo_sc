<?php
// echo "<pre>";print_r($child_brand);exit;
$i=1;
?>
<section class="content-header">
  <h1>Store Settings</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>brandusers">Manage Brand Users</a></li>
    <li class="active">Store Settings</li>
  </ol>
</section>
<section class="content">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Store Settings</h3>
    </div>
    <div class="box-body">
      <!-- form start -->
      <form name="frmcontact" id="frmcontact" role="form" method="post" enctype="multipart/form-data" >
        <div class="row">
          <div class="col-md-6">

            <div class="form-group">
              <label>
                <?php if(@set_value('ispaid') == 1){ $checked = 'checked';  }else if(empty($_POST) && $user_data[0]['is_paid'] == 1){ $checked = 'checked'; }else{ $checked = ''; } ?>
                <input  type="checkbox" name="ispaid"  value="1" <?php echo $checked; ?> style='<?php echo $this->session->userdata('role_id') == 6 ? 'display:none;':' display:inline-block;'; ?>'>
                <span class="text-red"> <?php echo form_error('ispaid'); ?></span>
                <?php echo $this->session->userdata('role_id') == 6 ? '':' Paid Brand'; ?>
                </label>
              </div>
              <div class="form-group">

                <?php echo $this->session->userdata('role_id') == 6 ? '':' <label for="Onboarded Date">On-board Date </label>'; ?>
                <input type="<?php echo $this->session->userdata('role_id') == 6 ? 'hidden':'text'; ?>" class="form-control" name="date_from" id="date_from" value="<?php echo $user_data[0]['onboarded_date']!='0000-00-00' ? $user_data[0]['onboarded_date'] :  set_value('date_from'); ?>">
              </div>
			<?php if(empty($child_brand)){ ?>
              <div class="form-group">
              <?php echo $this->session->userdata('role_id') == 6 ? '':'<label>Brand CPA (%)</label>'; ?>

                <input  type="<?php echo $this->session->userdata('role_id') == 6 ? 'hidden':'text'; ?>" name="cpa" class="form-control"  value="<?php if(isset ($user_data[0]['cpa_percentage'])&& set_value('cpa')=='') echo $user_data[0]['cpa_percentage']; else echo set_value('cpa');?>" />
                <span class="text-red"> <?php echo form_error('cpa'); ?></span>
              </div>
			<?php }else{  foreach($child_brand as $val){ ?>
			<div class="form-group">
				<label>
					<?php if(@set_value('show_on_web_'.$i) == 1){ $checked = 'checked';  }else if($val['show_on_web'] == 1){ $checked = 'checked'; }else{ $checked = ''; } ?>
					<input  type="checkbox" name="show_on_web_<?php echo $i; ?>"  value="1" <?php echo $checked; ?> style='<?php echo $this->session->userdata('role_id') == 6 ? 'display:none;':' display:inline-block;'; ?>'>
					<span class="text-red"> <?php echo form_error('show_on_web_'.$i); ?></span>
					<?php echo $this->session->userdata('role_id') == 6 ? '':' Show on Web'; ?>
				</label>
			</div>

			<div class="form-group">
              <?php echo $this->session->userdata('role_id') == 6 ? '':'<label> Brand CPA (%) <i style="color:brown;">'.$val["brand_store_name"].'</i></label>'; ?>

                <input  type="<?php echo $this->session->userdata('role_id') == 6 ? 'hidden':'text'; ?>" name="cpa_<?php echo $i; ?>" class="form-control"  value="<?php if(isset ($val['cpa_percentage'])&& set_value('cpa_'.$i)=='') echo $val['cpa_percentage']; else echo set_value('cpa_'.$i);?>" />
				<input type="hidden" name="brand_Store_id_<?php echo $i; ?>" id="brand_Store_id_<?php echo $i; ?>" value="<?php echo $val['id']; ?>">
                <span class="text-red"> <?php echo form_error('cpa_'.$i); ?></span>
              </div>

			  <div class="form-group">
        <?php if($this->session->userdata('role_id') != 6){ ?>
				<label>
				<input type="radio" name="commission_<?php echo $i; ?>" value="1" <?php if(isset ($val['commission_cat']) && $val['commission_cat'] == 1){ echo 'checked'; } ?> style='<?php echo $this->session->userdata('role_id') == 6 ? 'display:none;':' display:inline-block;'; ?>' > <?php echo $this->session->userdata('role_id') == 6 ? '':' All exclusive '; ?>
        </label><br>
        <label>
        <input type="radio" name="commission_<?php echo $i; ?>" value="2" <?php if(isset ($val['commission_cat']) && $val['commission_cat'] == 2){ echo 'checked'; } ?> style='<?php echo $this->session->userdata('role_id') == 6 ? 'display:none;':' display:inline-block;'; ?>' >  <?php echo $this->session->userdata('role_id') == 6 ? '':'  Flat Commission model '; ?><br>
				</label>
        <br>
        <label>
        <input type="radio" name="commission_<?php echo $i; ?>" value="3" <?php if(isset ($val['commission_cat']) && $val['commission_cat'] == 3){ echo 'checked'; } ?> style='<?php echo $this->session->userdata('role_id') == 6 ? 'display:none;':' display:inline-block;'; ?>' > <?php echo $this->session->userdata('role_id') == 6 ? '':' Flat Commission excluding taxes '; ?>
				</label>
        <?php } ?>
			</div>
			<?php	$i++;}
			 }?><input type="hidden" name="counter" id="counter" value="<?php echo $i; ?>">
       <div class="form-group">
              <?php echo $this->session->userdata('role_id') == 6 ? '':' <label>Brand Initials </label>  <small> e.g (STYCR)</small>'; ?>

                <input  type="<?php echo $this->session->userdata('role_id') == 6 ? 'hidden':'text'; ?>" name="brand_code" class="form-control"  value="<?php if(isset ($user_data[0]['brand_code'])&& set_value('brand_code')=='') echo $user_data[0]['brand_code']; else echo set_value('brand_code'); ?>" >
                <span class="text-red"> <?php echo form_error('brand_code'); ?></span>
              </div>
                <div class="form-group">
                      <label for="exampleInputEmail1">Company Logo</label>
                    <div>
                       <input type="file" id="company_logo" name="company_logo">
                    </div>
                      <p class="help-block">Max file size: 2MB and dimension(530 X 530).</p>
                      <span class="text-red"> <?php if(isset($error)){ foreach($error as $val){ echo $val.'<br>'; } } ?></span>                      
                      <?php //if(isset($user_data[0]['logo'])) { ?>
                      <?php if($user_data[0]['logo'] != '') { ?>
                        <img src="<?php echo base_url(); ?>assets/brand_logo/<?php echo $user_data[0]['logo']; ?>" height="100px" width="100px">
                      <?php } ?>
           </div>
           <div class="form-group">
            <label class="control-label">Upload Contract</label>
            <div>
              <input type="file" name="contract_file" id="contract_file" class="" placeholder="Upload Contract"/>
              <span class="text-red">
                <span class="text-red"> <?php if(isset($error2)){ foreach($error2 as $val2){ echo $val2.'<br>'; } } ?></span>
                <?php //print_r($error2);?>
              </span>  
              <!-- <a href='<?php echo base_url(); ?>assets/images/brands/brand_contract/<?php echo $user_data[0]['logo']; ?>'>Linking from here</a> -->                     
            </div>
             <p class="help-block">Only .pdf format allowed </p>
              <?php if($user_data[0]['contract_name'] != '') { ?>  
              <!-- <a target="_blank" href='<?php echo base_url(); ?>assets/images/brands/brand_contract/1478175175.pdf?>'>Linking from here</a> -->
              <a target="_blank" href='<?php echo base_url(); ?>assets/images/brands/brand_contract/<?php echo $user_data[0]['contract_name']; ?>'>Download Contract File</a>
            <?php } ?>   
          </div>
          <div class="form-group">
               <label >Verify Brand</label>
              <div class="radio">
              <label class="control-label"> <input type="radio" name="is_verify" id="is_verify" <?php if(isset($user_data[0]['is_verify'])){ echo ($user_data[0]['is_verify'] == '1') ?  "checked" : "" ; }else{echo set_value('is_verify') == 1 ? 'checked' : '';} ?> value = "1" /> Yes</label> &nbsp;&nbsp;&nbsp;
              <label class="control-label"> <input type="radio" name="is_verify" id="is_verify" <?php if(isset($user_data[0]['is_verify'])){ echo ($user_data[0]['is_verify'] == '0') ?  "checked" : "" ; }else{echo set_value('is_verify') == 0 ? 'checked' : '';} ?> value = "0" /> No</label>
              <div class="error text-red"><?php echo form_error('is_verify'); ?></div>
              </div>
          </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>
                  <?php if(@set_value('show_in') == 1){ $checked = 'checked';  }else if(empty($_POST) && $user_data[0]['show_in brand_list'] == 1){ $checked = 'checked'; }else{ $checked = ''; } ?>
                  <input  type="checkbox" name="show_in"  value="1" <?php echo $checked; ?> style='<?php echo $this->session->userdata('role_id') == 6 ? 'display:none;':' display:inline-block;'; ?>'>
                  <span class="text-red"> <?php echo form_error('show_in'); ?></span>
                  <?php echo $this->session->userdata('role_id') == 6 ? '':' Show on Web'; ?>
                  </label>
                </div>


          <!-- <div class="form-group">

            <?php echo $this->session->userdata('role_id') == 6 ? '':' <label>Brand CPC (%) </label>'; ?>
            <input  type="<?php echo $this->session->userdata('role_id') == 6 ? 'hidden':''; ?>" name="cpc" class="form-control"  value="<?php if(isset($user_data[0]['cpc_percentage']) && set_value('cpc')=='') echo $user_data[0]['cpc_percentage']; else echo set_value('cpc');?>" >
            <span class="text-red"> <?php echo form_error('cpc'); ?></span>
          </div> -->

          <div class="form-group">

            <?php echo $this->session->userdata('role_id') == 6 ? '':' <label>Payment Geteway charges (%) </label>'; ?>
            <input  type="<?php echo $this->session->userdata('role_id') == 6 ? 'hidden':''; ?>" name="payment_gateway_charges" class="form-control"  value="<?php if(isset($user_data[0]['payment_gateway_charges']) && set_value('payment_gateway_charges')=='') echo $user_data[0]['payment_gateway_charges']; else echo set_value('payment_gateway_charges');?>" >
            <span class="text-red"> <?php echo form_error('payment_gateway_charges'); ?></span>
          </div>

          <div class="form-group">

            <?php echo $this->session->userdata('role_id') == 6 ? '':' <label>Service Tax (%) </label>'; ?>
            <input  type="<?php echo $this->session->userdata('role_id') == 6 ? 'hidden':''; ?>" name="service_tax" class="form-control"  value="<?php if(isset($user_data[0]['service_tax']) && set_value('service_tax')=='') echo $user_data[0]['service_tax']; else echo set_value('service_tax');?>" >
            <span class="text-red"> <?php echo form_error('service_tax'); ?></span>
          </div>

          <div class="form-group">

            <?php echo $this->session->userdata('role_id') == 6 ? '':' <label>Integration Charges (%) </label>'; ?>
            <input  type="<?php echo $this->session->userdata('role_id') == 6 ? 'hidden':''; ?>" name="integration_charges" class="form-control"  value="<?php if(isset($user_data[0]['integration_charges']) && set_value('integration_charges')=='') echo $user_data[0]['integration_charges']; else echo set_value('integration_charges');?>" >
            <span class="text-red"> <?php echo form_error('integration_charges'); ?></span>
          </div>       

        
  </div>
  </div>
</div>
<div class="box-footer">
  <div class="form-group">
    <button class="btn btn-primary" type="submit">Submit</button>
  </div>
  </div>
</form>
</div>
</section>

<script>
$( document ).ready( function(){
  $(function () {
    $("#date_from").datepicker({ format: "yyyy-mm-dd",endDate: '+0d', autoclose: true, todayHighlight: true
  });
  hide_how_extra_fields();
});
hide_cod_det();
$( "input#cell" ).on( "click", function (){
  hide_how_extra_fields();
} );

hide_how_extra_area();

$( "input#cod" ).on( "click", function (){
  hide_how_extra_area();
} );

$('input[type=radio][name=cod_available]').change(function() {

        if (this.value == '1') {
           $('#cod_info').show();
        }
        else if (this.value == '0') {
           $('#cod_info').hide();
        }
    });


});
function hide_cod_det(){
  var cod_chk = "<?php echo $user_data[0]['cod_available']!='' ? $user_data[0]['cod_available'] : 0; ?>";
   if (cod_chk == '1') {
           $('#cod_info').show();
           $('input[type=radio][name=cod_available]').trigger('change');
        }
        else if (cod_chk == '0') {
           $('#cod_info').hide();
        }
}
function hide_how_extra_fields(){
  var is_shipping = <?php echo $user_data[0]['is_shipping']; ?>;

  if($( "input#cell:checked" ).val()!=1 )

  {
    $(".input-max" ).hide();
    //$('#sc').val(this.checked ? 1 : 0);
    //$('#min').val(this.checked ? 1 : 0);
    //$('#max').val(this.checked ? 1 : 0);
  }
  else
  {
    $(".input-max" ).show();
     //$('#sc').val(this.checked ? 1 : '');
    //$('#min').val(this.checked ? 1 : '');
    //$('#max').val(this.checked ? 1 : '');

  }
}

function hide_how_extra_area (){
  var is_cod = <?php echo $user_data[0]['is_cod']; ?>;


  if($( "input#cod:checked" ).val()!=1 )
  {
    $(".codis").hide();

    //$('#cod_charge').val(this.checked ? 1 : 0);
    //$('#mincod').val(this.checked ? 1 : 0);
    //$('#maxcod').val(this.checked ? 1 : 0);

  }
  else
  {
    $(".codis").show();
    //$('#cod_charge').val(this.checked ? 1 : '');
    //$('#mincod').val(this.checked ? 1 : '');
  //  $('#maxcod').val(this.checked ? 1 : '');


  }


}

function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : evt.keyCode;

         if (charCode != 44 && charCode > 31 && (charCode < 48 || charCode > 57))
           return false;
          return true;
      }

</script>
