<?php $users_menu=$this->data['users_menu'];
	  $total_rows = $this->data['total_rows'];
	  //echo "<pre>"; print_r($categories);exit;
?>
<section class="content-header">
        <h1>View Feedback </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>
            <li><a href="<?php echo base_url(); ?>feedback">Feedback</a></li>
            <li class="active">View Feedback</li>
          </ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
      	<h3 class="box-title">List of Feedback <small class="label label-info"><?php echo $total_rows; ?></small></h3>
    	
		</div>
		<div class="box-body">
				 <?php echo $this->session->flashdata('feedback'); ?>

	        <form name="frmsearch" class="form-group" id="frmsearch" action="<?php echo base_url(); ?>feedback/feedback_list" method="post">
				<div class="row">
					<div class="col-md-2">
						<select name="search_by" id="search_by" class="form-control input-sm" >
							<option value="0">Search by</option>
							<!--option value="1" <?php //echo set_value('search_by')==1 ? 'selected' : ''; ?>>id</option-->
							<option value="1" <?php echo set_value('search_by')==1 ? 'selected' : ''; ?>>Username</option>
							<option value="2" <?php echo set_value('search_by')==2 ? 'selected' : ''; ?>>Created Date</option>								
						</select>
					</div>

					<div class="col-md-3">
							<div class="input-group">
							<input type="text" name="table_search" id="table_search" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo set_value('table_search'); ?>"/>
							<div class="input-group-btn">
								<button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
							</div>
							</div>
					</div>
					<div class="col-md-2">
						<a href="<?php echo base_url(); ?>feedback/feedback_list" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
					</div>
					<div class="col-md-7">
							<div class="pull-right">
								<?php echo $this->pagination->create_links(); ?>
       						 </div>
					</div>
				</div>
			</form>

						<div class="table-responsive">
			          <table class="table table-bordered table-striped dataTable" id="datatable">
				        <thead><tr>
				          <th>Id</th>
				          <th>Username</th>
				          <th>Feedback</th>
				          <th>Created Datetime</th>
				          <!--th>Created By User</th-->
				        </tr>
				        </thead>
						<tbody>
				        <?php

				        	if(!empty($feedback ))
				        	{	$i=0;
								foreach($feedback as $singlefeedback)
								{	$i++;
						?>
									<tr>
				                    	<td><?php echo $singlefeedback->id; ?></td>
										<td><?php echo $singlefeedback->username; ?></td>
										<td><?php echo $singlefeedback->feedback; ?></td>
										<td><?php echo $singlefeedback->created_datetime; ?></td>						
									</tr>
						<?php
								}
							}else
							{
						?>
								<tr>
									<td >No Records Found</td>
								</tr>
						<?php
							}
						?>
						</tbody>
					</table>
			 <?php echo $this->pagination->create_links(); ?>

			</div>
		</div>
	</div>

</section>



