<?php $users_menu=$this->data['users_menu'];
	  $total_rows = $this->data['total_rows'];
	  /*echo "<pre>"; print_r($discount_type);exit;*/
	 /* echo '<pre>';print_r($coupons);*/
?>

<section class="content-header">
    <h1>Manage Coupon</h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>home">Home</a></li>
        <li><a href="<?php echo base_url(); ?>category">Coupon</a></li>
        <li class="active">Manage Coupon</li>
      </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">List of Coupon <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3>
			<div class="pull-right">
				<?php //if(in_array("2",$users_menu)) { ?>
				<a href="<?php echo base_url(); ?>CouponSettings/coupon_addedit"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add Coupon</button></a>
				<?php //} ?>
				<?php //if(in_array("60",@$users_menu)){ ?>        		
        		<a href="<?php echo base_url(); ?>CouponSettings/upload_couponSettings">
        			<button type="button" class="btn btn-primary btn-sm">
	        			<i class="fa fa-plus"></i> 
	        				Upload Coupon Settings
        			</button>
        		</a>
        	<?php //} ?>
			</div>
		</div>
        <div class="box-body">
		   <?php echo $this->session->flashdata('feedback'); ?>
            <!-- <form name="frmsearch" class="form-group" id="frmsearch" action="<?php echo base_url(); ?>CouponSettings" method="post">
            	<div class="row">
					<div class="col-md-2">
						<select name="search_by" id="search_by" class="form-control input-sm" >
							<option value="0">Search by</option>
							<option selected value="1" <?php echo set_value('search_by')==1 ? 'selected' : ''; ?>>Coupon Code</option>
						</select>
					</div>
					<div class="col-md-3">
						<div class="input-group">
							<input type="text" name="table_search" id="table_search" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo set_value('table_search'); ?>" required=""/>
							<div class="input-group-btn">
								<button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<a href="CouponSettings" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
					</div>
					<div class="col-md-5">
						<div class="pull-right"><?php echo $this->pagination->create_links(); ?></div>
          			</div>
				</div>
			</form> -->
			<form name="frmsearch" class="form-group" id="frmsearch" action="<?php echo base_url(); ?>CouponSettings" method="post">
            	<input type="hidden"  name="is_download" id="is_download" value="0" />
            	<div class="row">
					<div class="col-md-2">
						<?php @$search_by = set_value('search_by')=='' ? $search_by : set_value('search_by'); ?>
						<select name="search_by" id="search_by" class="form-control input-sm" >
							<option value="0" >Search by</option>		
							<option value="1" <?php echo $search_by==1 ? 'selected' : ''; ?>>Coupon Code</option>
							<option value="2" <?php echo $search_by==2 ? 'selected' : ''; ?>>E-Mail</option>
							<option value="3" <?php echo $search_by==3 ? 'selected' : ''; ?>>Date Range</option>
						</select>
					</div>
					 <div class="col-md-3">
				        <div class="input-group">
				         <div id ="filter" style="display: block;" >
				          <input type="text" name="table_search" value="<?php echo set_value('table_search')=='' && set_value('table_search')==0 ? @$table_search : set_value('table_search'); ?>" id="table_search" class="form-control input-sm pull-right" placeholder="Search"  required=""/>
				         </div>
				        <div id ="daterange" style="display: none;">
				            <div class="row">
				              <div class="col-md-5">
				                <div class="form-group">
				                  <input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm" id="date_from" value="<?php echo set_value('date_from'); ?>">
				                </div>
				            </div>
				            <div class="col-md-7">
				              <div class="form-group">
				                <input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="<?php echo set_value('date_to'); ?>">
				              </div>
				             </div>
				            </div>
				        </div>
				        <div class="input-group-btn">
				          <!-- <button class="btn btn-sm btn-default" type="submit" onclick="submitForm();" ><i class="fa fa-search"></i></button> -->
				          <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
				        </div>
				      </div>
				      </div>
					
					<div class="col-md-2">
						<a href="CouponSettings" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
					</div>
					<div class="col-md-2">
						<div class="input-group">                
							<!-- <span class="info-box-text">Download Product Inventory</span>  -->
							<a class="small-box-footer" target="_blank" id ="inventory_download" onclick="download_coupon_settings(1);" >
							<button type="button" class="btn btn-primary btn-sm">  Download Inventory Report <i class="fa fa-download"></i></button></a>
						</div>
						</div> 
					<div class="col-md-3">
						<div class="pull-right"><?php echo $this->pagination->create_links(); ?></div>
          			</div>
				</div>
			</form>
	  		<div class="table-responsive">
	      		<table class="table table-bordered table-striped dataTable" id="datatable">
		        <thead>
		        <tr>
	              <th>ID</th>
	              <th>COUPON CODE</th>
	              <th>DESCRIPTION</th>
	              <th>BRAND</th>
	              <th>DISCOUNT TYPE</th>
	              <th>START DATETIME</th>
	              <th>END DATETIME</th>
	              <th>MINIMUM SPEND</th>
	              <th>MAXIMUM SPEND</th>
	              <th>INDIVIDUAL USE ONLY</th>
	              <th>EXCLUDE SALE ITEMS</th>
	              <th>COUPON AMOUNT</th>
	              <!--<th>PRODUCTS</th>
	              <th>EXCLUDE PRODUCTS</th>
	              <th>PRODUCT CATEGORY</th>
	              <th>EXCLUDE PRODUCT CATEGORY</th> -->
	              <!--<th>EMAIL RESTRICTIONS</th>-->
	              <th>EMAIL-ID APPLICABLE FOR COUPON CODE</th>
	              <th>USAGE LIMIT PER COUPON</th>
	              <!--<th>USAGE LIMIT TO ITEMS</th>-->	              
	              <th>USAGE LIMIT TO USER</th>
	              <th>STYLECRACKER DISCOUNT</th>
	              <th>COUPON USED COUNT </th>
	              <th>STATUS</th>
	              <th>ACTION</th>
		        </tr>
		        </thead>
				<tbody>
	                <?php
	                	if(!empty($coupons))
	                	{	$i=0;
							foreach($coupons as $coupon)
							{	$i++;
					?>
	            <tr>
	              	<td><?php echo $coupon->id; ?></td>
					<td><?php echo $coupon->coupon_code; ?></td>
					<td><?php echo $coupon->coupon_desc; ?></td>
					<td><?php echo $coupon->company_name; ?></td>
					<td><?php
					if(!empty($discount_type))
					{
						if(array_key_exists($coupon->discount_type,$discount_type))
						{
							echo $discount_type[$coupon->discount_type];
						}
					}
					 ?></td>
					<td><?php echo $coupon->coupon_start_datetime; ?></td>
					<td><?php echo $coupon->coupon_end_datetime; ?></td>
					<td><?php echo $coupon->min_spend; ?></td>
					<td><?php echo $coupon->max_spend; ?></td>
					<td><?php
						$indiv_use = $coupon->individual_use_only ==1 ? 'YES' : 'NO';
						echo $indiv_use; ?></td>
					<td><?php
						$exclude_sale = $coupon->exclude_sale_items ==1 ? 'YES' : 'NO';
						echo $exclude_sale; ?></td>
					<td><?php echo $coupon->coupon_amount; ?></td>
					<td title="<?php echo $coupon->coupon_email_applied; ?>"><?php 
					$rest = substr($coupon->coupon_email_applied, 0, 50);
					echo $rest; ?></td>		
					<!--<td><?php echo $coupon->email_restriction; ?></td>-->
					<td><?php echo $coupon->usage_limit_per_coupon; ?></td>
					<!--<td><?php echo $coupon->usage_limit_to_items; ?></td>-->
					<td><?php echo $coupon->usage_limit_to_user; ?></td>
					<td> <?php if($coupon->stylecrackerDiscount==1){echo 'Yes';}else{ echo "No";} ?></td>
					<td><?php echo $coupon->coupon_used_count; ?></td>					
					<td> <?php if($coupon->status==1){echo 'Yes';}else{ echo "No";} ?></td>
					<td>
					<?php //if(in_array("3",$users_menu)) { ?>
	                <a href="<?php echo base_url(); ?>CouponSettings/coupon_addedit/<?php echo $coupon->id; ?>"><button type="button" class="btn btn-xs btn-primary btn-rad editCoup"><i class="fa fa-edit"></i> Edit</button></a>
	            	<?php //} ?>
	            	<?php //if(in_array("4",$users_menu)) { ?>
	                <button type="button" class="btn btn-xs btn-primary btn-delete btn-rad deleteCoup"  data-id="<?php echo $coupon->id; ?>" data-toggle="confirmation" ><i class="fa fa-trash-o"></i> Delete</button>
	            	<?php //} ?>
	             	</td>
					</tr>
					<?php
							}
						}else
						{
					?>
					<tr>
					<td >No Records Found</td>
				</tr>
					<?php
						}
					?>
				</tbody>
				</table>
				<div class="pull-right"><?php echo $this->pagination->create_links(); ?></div>
	        </div>

          </div>

    </div>
</section>


<script type="text/Javascript">
	$( document ).ready(function() {

		$("body").addClass('sidebar-collapse');

		$('[data-toggle=confirmation]').confirmation({
			title:'Are you sure?',
			onConfirm : function(){
				var del_id = $(this).closest('td').find('.deleteCoup').attr('data-id');
				$.ajax({
					 type: "POST",
					 url: "<?php echo base_url(); ?>"+"CouponSettings/delete_coupon/",
					 data: { couponID: del_id },
					 cache:false,
					 success:
						  function(data){
							location.reload(); //as a debugging message.
						  }
					});
				}
			});

			$('.nav-item-coupon').addClass("active");		
			$('.nav-coupon-setting').addClass("active");
	});

$(function () {
	var search_by = $('#search_by').val();	
	if(search_by==3)
	{
		$("#filter").hide();
        $("#daterange").show();
        $("#table_search").removeAttr('required');
        $("#date_from").attr('required','');
	}else{
		$("#filter").show();
      	$("#daterange").hide();
	}
      //$('#table_search').val('');
      $("#date_from").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true});
      $("#date_to").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true });
      users_click_info();
      users_click_au();
      users_signup_info();
      $(".nav-tabs a").click(function(){
        $(this).tab('show');
      });
      $('.nav-tabs a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
      });
    });	

function download_coupon_settings(attr){		
	var search_by = '';
    var table_search = '';    
    var date_from = '';
    var date_to = '';
	var search_by = $('#search_by').val();	
	var table_search = $('#table_search').val();
	/*var table_search =  table_search_1;	*/
/*	var table_search =  <?php echo base64_encode(table_search_1); ?>*/
    var date_from = $('#date_from').val();
    var date_to = $('#date_to').val();
    //var table_search = $('#table_search').val();    
    //if(search_by != '' || date_from != '' || date_to != '' || table_search != '' ){
    if(search_by != '3' && table_search != '' ){
     $('#inventory_download').attr('href','<?php echo base_url(); ?>CouponSettings/download_coupon/downloadExcel'+'/'+search_by+'/0/0/'+table_search);	
     //$('#inventory_download').attr('href','<?php echo base_url(); ?>CouponSettings/download_coupon/downloadExcel'+'/'+search_by+'/'+date_from+'/'+date_to);	
 	}else{
 		$('#inventory_download').attr('href','<?php echo base_url(); ?>CouponSettings/download_coupon/downloadExcel'+'/'+search_by+'/'+date_from+'/'+date_to+'/'+table_search);	
 	}
}

 $("#search_by").change(function(){

    var filterType = $(this).val();
    if(filterType == '3'){
    	//$('#table_search').val('');
        $("#filter").hide();
        $("#daterange").show();
        $("#table_search").removeAttr('required');
        $("#date_from").attr('required','');
    }else{    	
	    $("#filter").show();
	    $("#daterange").hide();    	
	    //$("#table_search").removeAttr('required');
    	/*$('#date_from').val('');
    	$('#date_to').val('');*/
    }
      /*if(filterType == 2){
        $(".msg-days-lapsed").show();
    }else
    {
        $(".msg-days-lapsed").hide();
    }*/
  });	
</script>
