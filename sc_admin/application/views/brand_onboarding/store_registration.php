<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Stylecracker Admin | Sign Up</title>
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <!-- Bootstrap 3.3.4 -->
  <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- Font Awesome Icons -->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <!-- Theme style -->
  <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet" type="text/css" />

</head>
<body class="login-page page-signup">
  <div class="login-box">
    <div class="login-logo">
      <img src="https://www.stylecracker.com/assets/images/logo.png" />
      <i>Admin</i>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg">Sign Up</p>
      <?php if(isset($error)) {
         echo '<div class="box-error">' .$error.'</div>';
           } ?>
      <form action="" method="post">
        <div class="form-group has-feedback">
          <select class="form-control" name = "store_type" id = "store_type" onchange="getval(this);">
            <option disabled="" selected="">Select Store Type</option>
          <option value="1" >Individual</option>
          <option value="2" >Aggregator</option>
          <option value="3" >Distributor</option>
        </select>
		 <label class="error" id="store_error"><?php echo form_error('store_type'); ?></label>
        </div>
		 <div class="form-group has-feedback">
          <input type="company_name" name="company_name" id="company_name" class="form-control" value="<?php if(isset($user_data['0']['company_name'])) echo $user_data['0']['company_name']; else echo set_value('company_name'); ?>" placeholder="Enter Brand Name"/>
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          <label class="error"><?php echo form_error('company_name'); ?></label>
        </div>
        <div class="form-group has-feedback">
          <input type="email" name="email" id="email" onblur="check_email()" class="form-control" value="<?php if(isset($user_data['0']['email'])) echo $user_data['0']['email']; else echo set_value('email'); ?>" placeholder="Enter Email"/>
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          <label class="error" id="un_email"><?php echo form_error('email'); ?></label>
        </div>
        <div class="form-group has-feedback">
          <input type="password" name="password" id="password" class="form-control" placeholder="Enter Password"/>
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          <label class="error" ><?php echo form_error('password'); ?></label>
        </div>
          <div class="login-show-password-wrp">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="remember_me" id="showHide" value="1"> Show Password
                  </label>
              </div>
            </div>

            <div class="form-group has-feedback">
				<!--	<label for="categorySlug" class="control-label">Confirm password<span class="text-red">*</span></label>-->
					<input type="password" placeholder="Enter Confirm password" name="confirm_password" id="confirm_password" class="form-control">
              <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
              <label class="error"><?php echo form_error('confirm_password'); ?></label>
            </div>
            <div class="footer-links">
                <button type="submit" class="btn btn-primary">Submit</button>              
                <div class="pull-right"><a href="<?php echo base_url();?>login">Login</a></div>
            </div>

          </form>


        </div><!-- /.login-box-body -->
      </div><!-- /.login-box -->

      <!-- jQuery 2.1.4 -->
      <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
      <!-- Bootstrap 3.3.2 JS -->
      <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
      <!-- iCheck -->
      <script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

    </body>
    </html>
<script type="text/Javascript">
$( document ).ready(function() {

	$("#showHide").click(function(){

		if($("#password").attr("type") == "password")
		{
			$("#password").attr("type","text");
		}else
		{
			$("#password").attr("type","password");
		}

	 });
});
function check_email(){
	 var email = $('#email').val();
	 $.ajax({
            url: "<?php echo base_url() ?>users/check_email",
            type: 'POST',
            data: {'email':email},
            cache :true,
            async: false,
            success: function(response) {

              $("#un_email").html(response);

            }
        });
}

function getval(sel) {
	var select_value = sel.value;
	if(select_value==1){
		alert('If you have one single brand.');
	}else if(select_value==2){
		alert('If you have multiple brands under your label but you want to display them under your label as one.');
	}else if(select_value==3){
		alert('If you have multiple labels under your brand and you want to display them as individual brands on the platform.');
	}
	
}
</script>
