 <?php
 // echo "<pre>";print_r($excelmsg_shipping);print_r($excelmsg_cod);?>
 <div class="container">
    <div class="page-brand-onboarding">
    <div class="box box-solid box-brand-step box-step1">
      <div class="box-header text-center">
        <h3 class="box-title text-center">Store Setting</span></h3>
        <div class="box-steps pull-right"><span>Step 2/6</span></div>
      </div>
    <div class="box-body">
      <form class="form-horizontal" role="form" id="brand_form" enctype="multipart/form-data" method="POST" action="<?php echo base_url(); ?>brand_onboarding/Store_registration/brand_step2/" onsubmit="return validate()">
         <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label col-sm-4">VAT TIN Number</label>
                    <div class="col-sm-8">
                      <input type="text" name="vat_tin_number" id="vat_tin_number" class="form-control" value="<?php if(isset($brand_info[0]['vat_tin_number'])){ echo $brand_info[0]['vat_tin_number']; } else{echo set_value('vat_tin_number');} ?>" placeholder="VAT TIN Number"/>
					   <div class="error text-red"><?php echo form_error('vat_tin_number'); ?></div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-sm-4">Brand Tax(%)<i>(Please mention, if your product prices on the StyleCracker store are exclusive of VAT)</i></label>
                    <div class="col-sm-8">
                      <input type="text" name="brand_tax" id="brand_tax" class="form-control" value="<?php if(isset($brand_info[0]['store_tax'])){ echo $brand_info[0]['store_tax']; } else{ echo set_value('brand_tax');} ?>" placeholder="Brand Tax(VAT)"/>
            <div class="error text-red"><?php echo form_error('brand_tax'); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4">CST Number</label>
                    <div class="col-sm-8">
                      <input type="text" name="cst_no" id="cst_no" class="form-control" value="<?php if(isset($brand_info[0]['cst_number'])){ echo $brand_info[0]['cst_number']; } else{ echo set_value('cst_no');} ?>" placeholder="CST Number"/>
            <div class="error text-red"><?php echo form_error('cst_no'); ?></div>
                    </div>
                  </div>
                  <!--<div class="form-group">
                           <label class="control-label col-sm-4">Return Policy<span class="text-red">*</span></label>
                           <div class="col-sm-8">
                           <textarea class="form-control" name="return_policy" id="return_policy" value="<?php if(isset($brand_info[0]['return_policy'])){ echo $brand_info[0]['return_policy']; } else{ echo set_value('return_policy');} ?>" ><?php if(isset($brand_info[0]['return_policy'])){ echo $brand_info[0]['return_policy']; } else{ echo set_value('return_policy');} ?></textarea>
                 <div class="error text-red"><?php echo form_error('return_policy'); ?></div>
                           </div>
                     </div>-->
					 
				
					
                </div>
                <div class="col-md-6" style="min-height:250px;">
				  <div class="form-group" style="padding:0 0 0px 40px;">
						In case you have agreed to ship your product(s) through StyleCracker's delivery partner, please download and upload the sample sheet.
				   </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4">COD<span class="text-red">*</span></label>
                    <div class="col-sm-8">
                    <label class="control-label"> <input type="radio" name="cod" id="cod" <?php if(isset($brand_info[0]['is_cod'])){ echo ($brand_info[0]['is_cod'] == '1') ?  "checked" : "" ; }else{echo set_value('is_cod') == 1 ? 'checked' : '';} ?> value = "1" onclick="show_cod_pin(1)"/> Yes</label> &nbsp;&nbsp;&nbsp;
                    <label class="control-label"> <input type="radio" name="cod" id="cod" <?php if(isset($brand_info[0]['is_cod'])){ echo ($brand_info[0]['is_cod'] == '0') ?  "checked" : "" ; }else{echo set_value('is_cod') == 0 ? 'checked' : '';}?> value = "0" onclick="show_cod_pin(0)"/> No</label>
					<div class="error text-red"><?php echo form_error('cod'); ?></div>
                    </div>
                  </div>
            <div class="form-group" id="cod_upload" style="<?php if(isset($brand_info[0]['is_cod'])){ echo ($brand_info[0]['is_cod'] == '1' || $this->input->post('cod') == '1') ?  "" : "display:none" ; }?>" >
              <label class="control-label col-sm-4">COD Pincode</label>
              <div class="col-sm-8">
                <input id="cod_pin" name="cod_pin" type="file" class="file" placeholder="Upload Pin Code"/>
                <p class="help-block f13">Only .xls format allowed | <a href="<?php echo base_url(); ?>brand_onboarding/Store_registration/download_cod_sheet"><i class="fa fa-download"></i> Download sample</a></p>
				 <div class="error text-red"><?php echo $excelmsg_cod[0]; ?></div>
              </div>
				<div class="error text-red"><?php echo form_error('cod_pin'); ?></div>
            </div>
			<?php 
			// echo $this->input->post('shipping');?>
                  <div class="form-group">
                    <label class="control-label col-sm-4">Shipping<span class="text-red">*</span></label>
                    <div class="col-sm-8">
                    <label class="control-label"> <input type="radio" name="shipping" id="shipping" <?php if(isset($brand_info[0]['is_shipping'])){ echo ($brand_info[0]['is_shipping'] == '1') ?  "checked" : "" ; }else{echo set_value('is_shipping') == 1 ? 'checked' : '';} ?> value = "1" onclick="show_shipping_pin(1)"/> Yes</label> &nbsp;&nbsp;&nbsp;
                    <label class="control-label"> <input type="radio" name="shipping" id="shipping" <?php if(isset($brand_info[0]['is_shipping'])){ echo ($brand_info[0]['is_shipping'] == '0') ?  "checked" : "" ; }else{echo set_value('is_shipping') == 0 ? 'checked' : '';} ?> value = "0" onclick="show_shipping_pin(0)"/> No</label>
					<div class="error text-red"><?php echo form_error('shipping'); ?></div>
                    </div>
                      </div>

                      <div class="form-group"  id="shipping_upload" style="<?php if(isset($brand_info[0]['is_shipping'])){ echo ($brand_info[0]['is_shipping'] == '1' || $this->input->post('shipping') == '1') ?  "" : "display:none" ; }?>">
                        <label class="control-label col-sm-4">Shipping Pincode</label>
                        <div class="col-sm-8">
                          <input type="file" name="shipping_pin" id="shipping_pin" value="save" class="" placeholder="Upload Pin Code"/>
                          <p class="help-block f13">Only .xls format allowed | <a href="<?php echo base_url(); ?>brand_onboarding/Store_registration/download_shipping_sheet"><i class="fa fa-download"></i> Download sample</a></p>
						  <div class="error text-red"><?php echo $excelmsg_shipping[0]; ?></div>
                        </div>
						<div class="error text-red"><?php echo form_error('shipping_pin'); ?></div>
                  </div>
                </div>
        </div>

        	<div>
						<label class="control-label">Return Policy<span class="text-red">*</span></label>
						<div class="" style="height1:200px;font-size:12px;">
							StyleCracker.com brings to you utmost quality and the coolest choices in fashion, all at prices unparalleled on its platform. In the rare occasion that the fit, colour or quality of the product doesn’t flatter you the most, lucky for you, we would be happy to take a return within 15 days of purchase and in addition, you would also be refunded appropriately. However, do call our customer service executives within 48 hours of receiving the product, to help us offer the best, most effective and instant services to you.<br>
							<b>What condition should the product be in?</b><br>
							As a courtesy to the next customer, the returned items should be unwashed and unused. Please keep the product intact as they need to be returned in their original packaging with the tags.<br>
							<b>What if I receive a damaged product?</b><br>
							We strive to deliver the very best! But unfortunately, if you receive a damaged/defective product- do notify us within 48 hours of receipt. Share a picture with us at customercare@stylecracker.com and we’ll make it our top priority!<br>
							<b>Non-refundable products?</b><br>
							Following product categories fall under "No Return/Exchange" unless there is an inherent damage/quality issue at the time of delivery due to a miss from our quality team.
							<ul style="list-style:disc;margin-left:15px;">
								<li>Lingerie</li>
								<li>Briefs</li>
								<li>Swimwear</li>
								<li>Beachwear</li>
								<li>Accessories</li>
								<li>Beauty products</li>
								<li>Socks</li>
								<li>Any freebies</li>
							</ul>
							<b>What if the returned products don't meet the conditions?</b><br>
							We try our best to make the return process seamless for you! <br>
							However, the product being returned needs to reach the above mentioned requirements, or else, we would not be able to offer a return/refund to you from our end.<br>
							<b>What are my return options?</b><br>
							Ways to refund your product:
							<ul style="list-style:disc;margin-left:15px;">
								<li>A Coupon Code of the refund value, which can be redeemed against any purchase.</li>
								<li>Cash refund via bank transfer into your account for all COD orders within 10 working days.</li>
								<li>Money-back to the paid account/credit card for all online orders.</li>
							</ul>
							Do feel free to write to us with any further queries at customercare@stylecracker.com. 
						</div>
					</div>

			<div class="box-footer text-right">
				<a class="btn btn-primary" href="<?php echo base_url(); ?>brand_onboarding/Store_registration/brand_step1?test=1">&laquo; Previous</a>
				<button class="btn btn-primary" tabindex="30" name="submit" value="1" type="submit" onclick="showLoader()">Save & Next &raquo;</button>
			 </div>

      </form>
    </div>

  </div>
</div>
</div>
<script type="text/Javascript">

$(document).ready( function(){
	$('.page-overlay, .page-overlay img').hide();
})
	function showLoader(){
		$('.page-overlay, .page-overlay img').show();
	}

    function validate() {

		cod_check = 0;
		shipping_check = 0;
		if (document.getElementById('cod').checked) {
			cod_check = document.getElementById('cod').value;
		}
		if (document.getElementById('shipping').checked) {
			shipping_check = document.getElementById('shipping').value;
		}
		// alert(cod_check);
		// alert(shipping_check);
		  var cod = 1;
		  var shipping_check = shipping_check;

		  /* code for check cod extension start*/
		  if(cod_check == '1'){
			  var extension;
			  var filename=document.getElementById('cod_pin').value;

			  extension=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();



			  if(extension == 'xlsx' || extension == 'xls')

			  {

				cod = 1;

			  }else

			  {

				alert('Please upload the "COD Pincode" sheet.');
				$('.page-overlay, .page-overlay img').hide();
				return false;

			  }

		  }
		  /* code for check cod extension end*/

		  /* code for check shipping extension start*/

		  if(shipping_check == '1'){
			  var extension2;
			  var filename2=document.getElementById('shipping_pin').value;

			  extension2=filename2.substr(filename2.lastIndexOf('.')+1).toLowerCase();


			  if(extension2 == 'xlsx' || extension2 == 'xls')

			  {

				shipping = 1;

			  }else

			  {

				alert('Please upload the "Shipping Pincode" sheet.');
				$('.page-overlay, .page-overlay img').hide();	
				return false;

			  }
		  }

		 /* code for check shipping extension end*/

		if(cod == 1 && shipping == 1){
			return true;
		}

    }

	function show_cod_pin(id){

		if(id == 1){
			// $('#cod_upload').show();
			$("#cod_upload").css("display", "block");
		}else{
			// $('#cod_upload').hide();
			$("#cod_upload").css("display", "none");

		}

	}

	function show_shipping_pin(id){

		if(id == 1){
			// $('#shipping_upload').show();
			$("#shipping_upload").css("display", "block");
		}else{
			// $('#shipping_upload').hide();
			$("#shipping_upload").css("display", "none");
		}
	}
</script>
