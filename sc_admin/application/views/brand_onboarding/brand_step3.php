  <?php
  // echo "<pre>";print_r($brand_info);exit;?><script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
  <div class="container">
    <div class="page-brand-onboarding">
    <div class="box box-solid box-brand-step box-step3">
      <div class="box-header text-center">
        <h3 class="box-title text-center">Brand Business Info</span></h3>
        <div class="box-steps pull-right"><span>Step 3/6</span></div>
      </div>
      <div class="box-body">
        <form class="form-horizontal" method="post" action="" enctype="multipart/form-data" name="frmbusinessinfo" id="frmbusinessinfo">
          <div class="row">
            <div class="col-md-6">
			  <div class="form-group">
                <label class="control-label col-sm-4">Company Registration ID</label></label>
                <div class="col-sm-8">
                  <input type="text" name="registration_no" id="registration_no" class="form-control" placeholder="Registered Company ID" value="<?php if(isset($brand_info[0]['registration_no'])){ echo $brand_info[0]['registration_no']; } else{echo set_value('registration_no');} ?>"/>
                  <div class="error text-red"><?php echo form_error('registration_no'); ?></div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-4">Registered Company Name<span class="text-red">*</span></label></label>
                <div class="col-sm-8">
                  <input type="text" name="company_name" id="company_name" class="form-control" placeholder="Registered Company Name" value="<?php if(isset($brand_info[0]['company_name'])){ echo $brand_info[0]['company_name']; } else{echo set_value('company_name');} ?>"/>
                  <div class="error text-red"><?php echo form_error('company_name'); ?></div>
				   <div class="error text-red"><?php if(isset($company_name))print_r($company_name); ?></div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-4">Registered Company Address<span class="text-red">*</span></label>
                <div class="col-sm-8">
                  <textarea class="form-control" placeholder="Registered Address" name="registered_address" id="registered_address" value="<?php if(isset($brand_info[0]['address'])){ echo $brand_info[0]['address']; } else{ echo set_value('registered_address');} ?>"><?php if(isset($brand_info[0]['address'])){ echo $brand_info[0]['address']; } else{ echo set_value('registered_address');} ?></textarea>
                  <div class="error text-red"><?php echo form_error('registered_address'); ?></div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-4">Brand Details Description<span class="text-red">*</span></label>
                <div class="col-sm-8">
                  <textarea class="form-control" placeholder="Brand Details Description" name="long_description" id="long_description" value="<?php if(isset($brand_info[0]['long_description'])){ echo $brand_info[0]['long_description']; } else{ echo set_value('long_description');} ?>"><?php if(isset($brand_info[0]['long_description'])){ echo $brand_info[0]['long_description']; } else{ echo set_value('long_description');} ?></textarea>
                  <div class="error text-red"><?php echo form_error('long_description'); ?></div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-4">Upload Lookbook</label>
                <div class="col-sm-8"><input type="file" name="look_book_file" id="look_book_file" class="" placeholder="Upload Lookbook"/>
				<p class="help-block f13">Only .pdf/.jpg/.png/.jpeg format allowed </p>
				</div>
              </div>

              <div class="form-group">
                <label class="control-label col-sm-4">Lookbook URL</label>
                <div class="col-sm-8">
                  <input type="text" name="look_book_url" id="look_book_url" class="form-control" placeholder="Lookbook URL" value="<?php if(isset($brand_info[0]['look_book_url'])){ echo $brand_info[0]['look_book_url']; } else{echo set_value('look_book_url');} ?>"/>
                </div>
				<div class="error text-red"><?php echo form_error('look_book_url'); ?></div>
              </div>

            </div>


            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-sm-4">Demographic<span class="text-red">*</span></label>
                <div class="col-sm-8 checkbox">
                    <label><input type="checkbox" name="is_male" id="is_male" class="" value="1"  <?php if($brand_info[0]['is_male']==1){echo 'checked'; } else { echo set_value('is_male') == 1 ? 'checked' : ''; } ?> />Male</label>
                    <label><input type="checkbox" name="is_female" id="is_female" class="" value="1" <?php if($brand_info[0]['is_female']==1){echo 'checked';}else { echo set_value('is_female') == 1 ? 'checked' : ''; }?> />Female</label>
					<label><input type="checkbox" name="is_children" id="is_children" class="" value="1" <?php if($brand_info[0]['is_children']==1){echo 'checked';}else { echo set_value('is_children') == 1 ? 'checked' : ''; }?> />Children</label>
					<div class="error text-red"><?php echo form_error('is_male'); ?></div>
                </div>
				
              </div>
	<?php if(!empty($_POST)) { $t_market = $_POST['target_market']; } ?>
              <?php if(!empty($target_market)){ ?>
					<div class="form-group">
                      <label for="exampleInputEmail1" class="control-label col-sm-4">Target Market<span class="text-red">*</span></label>
                       <div class="checkbox col-sm-8">
						   <?php foreach($target_market as $val) {  ?>
                      <label>
						  <?php if(isset($t_market) && in_array($val->id,$t_market)) {  ?>
                        <input type="checkbox" name="target_market[]" id="" <?php echo set_checkbox('target_market[]'); ?> value="<?php echo $val->id;?>" checked="checked"> <?php echo $val->target_market;?>
                        <?php }else { ?>
						<input type="checkbox" name="target_market[]" id="" <?php echo set_checkbox('target_market[]'); ?> value="<?php echo $val->id;?>"> <?php echo $val->target_market;?>
							<?php } ?>
                      </label>
							<?php } ?>
							<span class="text-red"> <?php echo form_error('target_market[]'); ?></span>
							
					</div>
					<?php }else{ echo "Contact System Admin"; }  ?>
					
          </div>


              <div class="form-group">
                <label class="control-label col-sm-4">Age Group<span class="text-red">*</span></label>
                <div class="col-sm-4">
                  <input type="text" name="age_group_from" id="age_group_from" class="form-control" placeholder="From" value="<?php if(isset($brand_info[0]['age_group_from'])){ echo $brand_info[0]['age_group_from']; } else{echo set_value('age_group_from');} ?>"/>
				  <div class="error text-red"><?php echo form_error('age_group_from'); ?></div>
                </div>
                <div class="col-sm-4">
                  <input type="text" name="age_group_to" id="age_group_to" class="form-control" placeholder="To" value="<?php if(isset($brand_info[0]['age_group_to'])){ echo $brand_info[0]['age_group_to']; } else{echo set_value('age_group_to');} ?>"/>
				  <div class="error text-red"><?php echo form_error('age_group_to'); ?></div>

                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-sm-4">Price Range<span class="text-red">*</span></label>
                <div class="col-sm-4">
                  <input type="text" name="price_range_from" id="price_range_from" class="form-control" placeholder="From" value="<?php if(isset($brand_info[0]['price_range_from'])){ echo $brand_info[0]['price_range_from']; } else{echo set_value('price_range_from');} ?>" />
				  <div class="error text-red"><?php echo form_error('price_range_from'); ?></div>
                </div>
                <div class="col-sm-4">
                  <input type="text" name="price_range_to" id="price_range_to" class="form-control" placeholder="To" value="<?php if(isset($brand_info[0]['price_range_to'])){ echo $brand_info[0]['price_range_to']; } else{echo set_value('price_range_to');} ?>"/>
				  <div class="error text-red"><?php echo form_error('price_range_to'); ?></div>

                </div>
              </div>


              <div class="form-group">
                <label class="control-label col-sm-4"></label>
                <div class="col-sm-8 checkbox">
                  <label><input type="checkbox" name="is_online" id="is_online" value="1" class="" <?php if($brand_info[0]['is_online']==1){echo 'checked';}else { echo set_value('is_online') == 1 ? 'checked' : ''; }?> />Does the brands have a website?</label>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-4">Platform Name</label>
                <div class="col-sm-8">
                  <input type="text" name="platform_name" id="platform_name" class="form-control" placeholder="Platform Name (eg.shopify, magento, custom, etc.)" value="<?php if(isset($brand_info[0]['platform_name'])){ echo $brand_info[0]['platform_name']; } else{echo set_value('platform_name');} ?>"/>
				  <div class="error text-red"><?php echo form_error('platform_name'); ?></div>
                </div>
              </div>
            </div>
          </div>

			<div class="box-footer text-right">
				<a class="btn btn-primary" href="<?php echo base_url(); ?>brand_onboarding/Store_registration/brand_step2?test=1">&laquo; Previous</a>
				<button class="btn btn-primary" type="submit" onclick="showLoader()">Save & Next &raquo;</button>
		  </div>
        </form>
      </div>

    </div>
    </form>
  </div>
    </div>
<script type="text/javascript">

	$(document).ready( function(){
		$('.page-overlay, .page-overlay img').hide();
	})
	
	function showLoader(){
		$('.page-overlay, .page-overlay img').show();
	}
	
  // $(document).ready(function(e){
      // $("#frmbusinessinfo").validate({
        // rules: {
            // company_name: "required",
            // registered_address: "required",
            // long_description: "required",

            // look_book_url : "required",
            // age_group_from : "required",
            // age_group_to : "required",
            // price_range_from : "required",
            // price_range_to : "required",
          // },
         // messages: {
            // company_name: "Please enter Company Address",
            // registered_address: "Please enter city",
            // long_description: "Please enter state",

            // look_book_url : "Please enter book url",
            // age_group_from : "Please enter age group from",
            // age_group_to : "Please enter age group to",
            // price_range_from : "Please enter price from",
            // price_range_to : "Please enter price to",
        // },submitHandler: function(form) {
            // form.submit();
        // }
      // });
    // });
</script>
