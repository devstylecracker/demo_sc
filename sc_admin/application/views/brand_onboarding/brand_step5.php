<?php //print_r($error);?>
<div class="container">
        <div class="page-brand-onboarding">
      <div class="box box-solid box-brand-step box-step5">
        <div class="box-header text-center">
          <h3 class="box-title text-center">Account Details</span></h3>
          <div class="box-steps pull-right"><span>Step 5/6</span></div>
        </div>
        <div class="box-body">
          <form class="form-horizontal" id="brand_form" enctype="multipart/form-data" method="POST">

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label col-sm-4">Upload Pancard Copy<span class="text-red">*</span></label>
                  <div class="col-sm-8"><input type="file" name="pancard" id="pancard" placeholder="Upload Pancard Copy"/>
            				   	<div class="help-block f13">Allowed format: .jpg, .jpeg, .png, .gif</div>
            				    <div class="error text-red f13"><?php if(isset($error)){echo $error[0];} ?></div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label col-sm-4">Upload Cancelled Cheque<span class="text-red">*</span></label>
                  <div class="col-sm-8"><input type="file" name="cheque" id="cheque" placeholder="Upload Cancelled Cheque" />
                  <div class="help-block f13">Allowed format: .jpg, .jpeg, .png, .gif</div>
				          <div class="error text-red f13"><?php if(isset($error)){echo $error[0];} ?></div>
                  </div>
                </div>
              </div>
			 
            </div>
			  <div>
              <label>
                <?php if(@set_value('is_confirm') == 1){ $checked = 'checked';  }else if(empty($_POST) && $brand_info_table[0]['is_confirm'] == 1){ $checked = 'checked'; }else{ $checked = ''; } ?>
                <input  type="checkbox" name="is_confirm"  value="1" <?php echo $checked; ?>>  I hereby confirm that all the information provided by us is correct.
                <span class="text-red"> <?php echo form_error('is_confirm'); ?></span>
                </label>
              </div>
            <br /><br /><br />
            <div>
              <p>
                <strong>Terms & Conditions</strong><br />
                These all information correct. Click on submit buttion for StyleCracker approval.
              </p>
            </div>

			<!--<div class="box-footer text-center">
			  <button class="btn btn-primary" type="submit" >Submit for Approval</button>
			</div-->

			<div class="box-footer text-right">
				<a class="btn btn-primary" href="<?php echo base_url(); ?>brand_onboarding/Store_registration/brand_step4?test=1">&laquo; Previous</a>
				<button class="btn btn-primary" type="submit" onclick="showLoader()">Submit for Approval</button>
			</div>

			<input type="hidden" name="test" value="1">
          </form>
        </div>

      </div>
    </div>
  </div>

<script type="text/Javascript">

	$(document).ready( function(){
		$('.page-overlay, .page-overlay img').hide();
	})
	
	function showLoader(){
		$('.page-overlay, .page-overlay img').show();
	}
	
</script>
