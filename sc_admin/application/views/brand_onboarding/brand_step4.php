<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
  <div class="container">
    <div class="page-brand-onboarding">
    <div class="box box-solid box-brand-step box-step4">
      <div class="box-header text-center">
        <h3 class="box-title text-center">Contact Details</span></h3>
        <div class="box-steps pull-right"><span>Step 4/6</span></div>
      </div>
          <div class="box-body">
            <form class="form-horizontal" method="post" action="" name="frmContact" id="frmContact">

              <div class="row">
                <div class="col-md-6">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title">Address Info</h3>
                    </div>
                    <div class="panel-body">
                  <div class="form-group">
                    <label class="control-label col-sm-4">Office Address<span class="text-red">*</span></label></label>
                    <div class="col-sm-8">
                      <textarea class="form-control" name="company_address" id="company_address" placeholder="Office Address"><?php if(isset($contact_info[0]['company_address'])){ echo $contact_info[0]['company_address']; } else{echo set_value('company_address');}?></textarea>
                      <div class="error text-red"><?php echo form_error('company_address'); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4">City<span class="text-red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" name="city" id="city" class="form-control" placeholder="City" value="<?php if(isset($contact_info[0]['city'])){ echo $contact_info[0]['city']; } else{echo set_value('city');}?>"/>
                      <div class="error text-red"><?php echo form_error('city'); ?></div>
                    </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-4">State<span class="text-red">*</span></label>
                        <div class="col-sm-8">
                          <input type="text" name="state" id="state" class="form-control" placeholder="State" value="<?php if(isset($contact_info[0]['state'])){ echo $contact_info[0]['state']; } else{echo set_value('state');}?>"/>
                          <div class="error text-red"><?php echo form_error('state'); ?></div>
                        </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-4">Pincode<span class="text-red">*</span></label>
                            <div class="col-sm-8">  <input type="text" name="pincode" id="pincode" class="form-control" placeholder="Pincode" value="<?php if(isset($contact_info[0]['pincode'])){ echo $contact_info[0]['pincode']; } else{echo set_value('pincode');} ?>" maxlength="6"/><div class="error text-red"><?php echo form_error('pincode'); ?></div></div>

                          </div>

                          <div class="form-group">
                            <label class="control-label col-sm-4">Country<span class="text-red">*</span></label>
                              <div class="col-sm-8">
                                <input type="text" name="country" id="country" class="form-control" placeholder="Country" value="<?php if(isset($contact_info[0]['country'])){ echo $contact_info[0]['country']; } else{echo set_value('country');} ?>"/><div class="error text-red"><?php echo form_error('country'); ?></div>
                              </div>
                              </div>

                </div>
                  </div>


				  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title">Warehouse Info</h3>
                    </div>
                    <div class="panel-body">
                  <div class="form-group">
                    <label class="control-label col-sm-4">Office Address</label></label>
                    <div class="col-sm-8">
                      <textarea class="form-control" name="warehouse_address" id="warehouse_address" placeholder="Office Address"><?php if(isset($contact_info[0]['warehouse_address'])){ echo $contact_info[0]['warehouse_address']; } else{echo set_value('warehouse_address');} ?></textarea>
                      <div class="error text-red"><?php echo form_error('warehouse_address'); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4">City</label>
                    <div class="col-sm-8">
                      <input type="text" name="warehouse_city" id="warehouse_city" class="form-control" placeholder="City" value="<?php if(isset($contact_info[0]['warehouse_city'])){ echo $contact_info[0]['warehouse_city']; } else{echo set_value('warehouse_city');} ?>"/>
                      <div class="error text-red"><?php echo form_error('warehouse_city'); ?></div>
                    </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-4">State</label>
                        <div class="col-sm-8">
                          <input type="text" name="warehouse_state" id="warehouse_state" class="form-control" placeholder="State" value="<?php if(isset($contact_info[0]['warehouse_state'])){ echo $contact_info[0]['warehouse_state']; } else{echo set_value('warehouse_state');} ?>"/>
                          <div class="error text-red"><?php echo form_error('warehouse_state'); ?></div>
                        </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-4">Pincode</label>
                            <div class="col-sm-8">  <input type="text" name="warehouse_pincode" id="warehouse_pincode" class="form-control" placeholder="Pincode" value="<?php if(isset($contact_info[0]['warehouse_pincode'])){ echo $contact_info[0]['warehouse_pincode']; } else{echo set_value('warehouse_pincode');} ?>" maxlength="6"/><div class="error text-red"><?php echo form_error('warehouse_pincode'); ?></div></div>

                          </div>

                          <div class="form-group">
                            <label class="control-label col-sm-4">Country</label>
                              <div class="col-sm-8">
                                <input type="text" name="warehouse_country" id="warehouse_country" class="form-control" placeholder="Country" value="<?php if(isset($contact_info[0]['warehouse_country'])){ echo $contact_info[0]['warehouse_country']; } else{echo set_value('warehouse_country');} ?>"/><div class="error text-red"><?php echo form_error('warehouse_country'); ?></div>
                              </div>
                              </div>

                </div>
                  </div>


                    </div>


                  <div class="col-md-6">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">Business Contact Details<span class="text-red">*</span></h3>
                      </div>
                      <div class="panel-body">
                    <div class="form-group">
                      <label class="control-label col-sm-4">Name<span class="text-red">*</span></label>
                      <div class="col-sm-8">  <input type="text" name="contact_person" id="contact_person" class="form-control" placeholder="Name" value="<?php if(isset($contact_info[0]['contact_person'])){ echo $contact_info[0]['contact_person']; } else{echo set_value('contact_person');} ?>"/><div class="error text-red"><?php echo form_error('contact_person'); ?></div></div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-sm-4">Phone Number<span class="text-red">*</span></label>
                      <div class="col-sm-8">  <input type="text" name="phone_one" id="phone_one" class="form-control" placeholder="Phone Number" value="<?php  if(isset($contact_info[0]['phone_one'])){ echo $contact_info[0]['phone_one']; } else{echo set_value('phone_one');} ?>" maxlength="13"/><div class="error text-red"><?php echo form_error('phone_one'); ?></div></div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-sm-4">Email<span class="text-red">*</span></label>
                      <div class="col-sm-8">  <input type="text" name="emailid" id="emailid" class="form-control" placeholder="Email" value="<?php if(isset($contact_info[0]['emailid'])){ echo $contact_info[0]['emailid']; } else{echo set_value('emailid');} ?>"/><div class="error text-red"><?php echo form_error('emailid'); ?></div></div>

                    </div>
                  </div>
                  </div>


                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title">Tech Contact Details</h3>
                  </div>
                  <div class="panel-body">
                <div class="form-group">
                  <label class="control-label col-sm-4">Name<span class="text-red">*</span></label>
                  <div class="col-sm-8">  <input type="text" name="contact_person_two" id="contact_person_two" class="form-control" placeholder="Name" value="<?php if(isset($contact_info[0]['contact_person_two'])){ echo $contact_info[0]['contact_person_two']; } else{echo set_value('contact_person_two');} ?>"/>
				  <div class="error text-red"><?php echo form_error('contact_person_two'); ?></div></div>

                </div>
                <div class="form-group">
                  <label class="control-label col-sm-4">Phone Number<span class="text-red">*</span></label>
                  <div class="col-sm-8">  <input type="text" name="phone_two" id="phone_two" class="form-control" placeholder="Phone Number" value="<?php if(isset($contact_info[0]['phone_two'])){ echo $contact_info[0]['phone_two']; } else{echo set_value('phone_two');} ?>" maxlength="13"/>
				  <div class="error text-red"><?php echo form_error('phone_two'); ?></div>
				  </div>
                </div>
				
                <div class="form-group">
                  <label class="control-label col-sm-4">Email<span class="text-red">*</span></label>
                  <div class="col-sm-8">  <input type="text" name="alt_emailid" id="alt_emailid" class="form-control" placeholder="Email" value="<?php if(isset($contact_info[0]['alt_emailid'])){ echo $contact_info[0]['alt_emailid']; } else{echo set_value('alt_emailid');} ?>"/>
				  <div class="error text-red"><?php echo form_error('alt_emailid'); ?></div>
				  </div>
                  
                </div>
              </div>
              </div>

              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">Order Contact Details</h3>
                </div>
                <div class="panel-body">
              <div class="form-group">
                <label class="control-label col-sm-4">Name<span class="text-red">*</span></label>
                <div class="col-sm-8">  <input type="text" name="contact_person_three" id="contact_person_three" class="form-control" placeholder="Name" value="<?php if(isset($contact_info[0]['contact_person_three'])){ echo $contact_info[0]['contact_person_three']; } else{echo set_value('contact_person_three');} ?>"/>
				<div class="error text-red"><?php echo form_error('contact_person_three'); ?></div>
				</div>
                
              </div>
              <div class="form-group">
                <label class="control-label col-sm-4">Phone Number<span class="text-red">*</span></label>
                <div class="col-sm-8">  <input type="text" name="phone_three" id="phone_three" class="form-control" placeholder="Phone Number" value="<?php if(isset($contact_info[0]['phone_three'])){ echo $contact_info[0]['phone_three']; } else{echo set_value('phone_three');} ?>" maxlength="13"/>
				<div class="error text-red"><?php echo form_error('phone_three'); ?></div>
				</div>
                
              </div>
              <div class="form-group">
                <label class="control-label col-sm-4">Email<span class="text-red">*</span></label>
                <div class="col-sm-8">  <input type="text" name="alt_emailid_three" id="alt_emailid_three" class="form-control" placeholder="Email" value="<?php if(isset($contact_info[0]['alt_emailid_three'])){ echo $contact_info[0]['alt_emailid_three']; } else{echo set_value('alt_emailid_three');} ?>"/>
				<div class="error text-red"><?php echo form_error('alt_emailid_three'); ?></div>
				</div>
                
              </div>
            </div>
          </div>
                </div>
              </div>

          </div>
          <div class="box-footer text-right">
            <a class="btn btn-primary" href="<?php echo base_url(); ?>brand_onboarding/Store_registration/brand_step3?test=1">&laquo; Previous</a>
            <button class="btn btn-primary" onclick="showLoader()">Save & Next &raquo;</button>
          </div>
        </div>
        </form>
      </div>
    </div>
<style>
.error{ color :red; }
</style>
<script type="text/javascript">

	$(document).ready( function(){
		$('.page-overlay, .page-overlay img').hide();
	})
	
	function showLoader(){
		$('.page-overlay, .page-overlay img').show();
	}
/*
  $(document).ready(function(e){
    //$("body").addClass('sidebar-collapse');

    $("#frmContact").validate({
        rules: {
            company_address: "required",
            city: "required",
            state: "required",
            pincode: {required: true,number: true,minlength:6,maxlength:6},
            country: "required",
            contact_person: "required",
            phone_one : {required: true,number: true,minlength:10,maxlength:13},
            emailid : {required: true,email: true},
            contact_person_two: "required",
            phone_two : {required: true,number: true,minlength:10,maxlength:13},
            alt_emailid : {required: true,email: true},
            contact_person_three: "required",
            phone_three : {required: true,number: true,minlength:10,maxlength:13},
            alt_emailid_three : {required: true,email: true},
          },
         messages: {
            company_address: "Please enter Company Address",
            city: "Please enter city",
            state: "Please enter state",
            pincode: {required: "Please enter pincode"},
            country: "Please enter country",
            contact_person: "Please enter Contact Person Name",
            phone_one : {required: "Please enter phone number"},
            emailid : {required: "Please enter emailid",email: "Please enter valid email id"},
            contact_person_two: "Please enter Contact Person Name",
            phone_two : {required: "Please enter phone number"},
            alt_emailid : {required: "Please enter emailid",email: "Please enter valid email id"},
            contact_person_three: "Please enter Contact Person Name",
            phone_three :{required: "Please enter phone number"},
            alt_emailid_three : {required: "Please enter emailid",email: "Please enter valid email id"},
        },submitHandler: function(form) {
            form.submit();
        }
      });
    });
*/
</script>
