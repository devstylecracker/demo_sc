<?php  $count = COUNT($brand_info); $i=1;
 // echo "<pre>";print_r($brand_info_table);exit;?>
<div class="container">
    <div class="page-brand-onboarding">
          <div class="box box-solid box-brand-step box-step1">
          <div class="box-header text-center">
            <h3 class="box-title text-center">Brand Signup</span></h3>
            <div class="box-steps pull-right"><span>Step 1/6</span></div>
          </div>
		  <!-- <form class="form-horizontal"> -->
          <div class="box-body" id="brand_info">
				<?php if(empty($brand_info)){ ?>
				<form role="form" id="brand_form" enctype="multipart/form-data" method="POST">
				<div class="panel-body" id="new_brand_info">
				 <div id="new_row_1" class="panel panel-default" style="border:none;">
					<div class="form-group row" >
						<label class="control-label col-sm-3">Brand Name (Display on Web)<span class="text-red">*</span></label>
						<div class="col-sm-3">
						  <input type="text" name="brand_name_1" id="brand_name_1" class="form-control" placeholder="Brand Name"/>
						  <span class="error text-red"><?php echo form_error('brand_name_1'); ?></span>
						</div>
			<?php if(@$brand_info_table[0]['store_type'] != 1){ ?>
						<div class="col-sm-2">
							<span onclick="add_row(1)" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i></span>
						</div>
			<?php } ?>			
					  </div>
				</div>
				
				</div>
				 <input type="hidden" name="counter" id="counter" value="1">
				<div class="box-footer text-right">
					<button class="btn btn-primary" >Save & Next &raquo;</button>
				</div>
				</form>
				<?php }else { 
				// echo "<pre>";print_r($brand_info);?>
				<form role="form" id="brand_form" enctype="multipart/form-data" method="POST">
					
						<div class="panel-body" id="new_brand_info">
						<?php foreach($brand_info as $val){
									if($i==1){ ?>
						 <div id="new_row_<?php echo $i; ?>" class="panel panel-default" style="border:none;">
							<div class="form-group row" >
								<label class="control-label col-sm-3">Brand Name (Display on Web)<span class="text-red">*</span></label>
								
								<div class="col-sm-3">
								  <input type="text" name="brand_name_<?php echo $i; ?>" id="brand_name_<?php echo $i; ?>" class="form-control" placeholder="Brand Name"/ value="<?php echo $val['brand_store_name'];?>">
								  <span class="error text-red"><?php echo form_error('brand_name_1'); ?></span>
								</div>
								<?php if($brand_info_table[0]['store_type'] != 1){ ?>
								<div class="col-sm-2">
									<span onclick="add_row(<?php echo $i; ?>);" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i></span>
								</div></br>
							<?php } ?>
							  </div>
						</div>
					<?php }else { ?>
						<div id="new_row_<?php echo $i; ?>" class="panel panel-default" style="border:none;">
							<div class="form-group row" >
								<label class="control-label col-sm-3"></label>
								
								<div class="col-sm-3">
								  <input type="text" name="brand_name_<?php echo $i; ?>" id="brand_name_<?php echo $i; ?>" class="form-control" placeholder="Brand Name"/ value="<?php echo $val['brand_store_name'];?>">
								  <span class="error text-red"><?php echo form_error('brand_name_'.$i); ?></span>
								</div>
								
								<div class="col-sm-2">
									<span onclick="remove_row(<?php echo $i; ?>);" class="btn btn-primary btn-xs btn-delete"><i class="fa fa-minus"></i></span>
								</div></br>
							
							  </div>
						</div>
					<?php }
					 $i++; } ?>
						</div>
					<input type="hidden" name="counter" id="counter" value="<?php echo $count; ?>">
				<div class="box-footer text-right">
					<button class="btn btn-primary">Save & Next &raquo;</button>
				</div>
				</form>
				
				<?php } ?>
				</div>
		 
         
        </div>
      </div>
</div>
<script type="text/Javascript">

	$(document).ready( function(){
		$('.page-overlay, .page-overlay img').hide();
	})
	
	function showLoader(){
		$('.page-overlay, .page-overlay img').show();
	}
function add_row(id){ 
	var counter = $('#counter').val();
	counter = parseInt(counter) + 1; 
	if(counter >= 1)
        {
			$.ajax({
				url: "<?php echo base_url() ?>brand_onboarding/Store_registration/add_row_new",
				type: 'POST',
				data: {'counter':counter},
				cache: false,
				async: false,
				success: function(response) {
					$('#new_brand_info').append(response);
					$('#counter').val(counter);
				}
			});
		  	
		}
}

function remove_row(id){
	var brand_name = $('#brand_name_'+id).val();
	$('#new_row_'+id).remove();
	if(brand_name != '')
        {
			$.ajax({
				url: "<?php echo base_url() ?>brand_onboarding/Store_registration/remove_row",
				type: 'POST',
				data: {'brand_name':brand_name},
				cache: false,
				async: false,
				success: function(response) {
					alert(response);
					$('#new_row_'+id).remove();
				}
			});
		  	
		}
}
</script>