 <?php
 // echo "<pre>";print_r($brand_info_table);exit;
 // echo base_url().'assets/brand_contract/'.$brand_info_table[0]['contract_name'];exit;
 ?>
 <div class="container">
    <div class="page-brand-onboarding">
    <div class="box box-solid box-brand-step box-step5">
      <div class="box-header text-center">
        <h3 class="box-title text-center">Brand Contract Details</span></h3>
        <div class="box-steps pull-right"><span>Step 6/6</span></div>
      </div>
          <div class="box-body text-center">
            <h3>
              <i>Thankyou!</i> </br></br>We will get back to you shortly.
            </h3>
			<?php if(isset($brand_info_table) && $brand_info_table[0]['contract_name'] != ''){ ?>
            <p>
              Please download your contract copy to verify.</p>
              <p><a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>brand_onboarding/Store_registration/download_contract">Download</a>
            </p>

			<?php } ?>
          </div>
        </div>
      </div>
    </div>
<script type="text/Javascript">

	$(document).ready( function(){
		$('.page-overlay, .page-overlay img').hide();
	})
	
	function showLoader(){
		$('.page-overlay, .page-overlay img').show();
	}
	
</script>