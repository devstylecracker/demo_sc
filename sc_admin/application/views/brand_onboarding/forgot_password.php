<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Stylecracker Admin | Forgot Password</title>
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <!-- Bootstrap 3.3.4 -->
  <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- Font Awesome Icons -->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <!-- Theme style -->
  <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet" type="text/css" />

</head>
<body class="login-page page-signup">
  <div class="login-box">
    <div class="login-logo">
      <img src="https://www.stylecracker.com/assets/images/logo.png" />
      <i>Admin</i>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg">Forgot Password</p>
      <?php if(isset($error)) {
         echo '<div class="box-error">' .$error.'</div>';
           } ?>
      <form action="" method="post">

        <div class="form-group has-feedback">
          <input type="email" name="email" id="email" class="form-control" value="<?php if(isset($user_data['0']['email'])) echo $user_data['0']['email']; else echo set_value('email'); ?>" placeholder="Enter Email"/>
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          <label class="error" id="un_email"><?php echo form_error('email'); ?></label>
          <label class="error" id="un_email2"></label>
        </div>

            <div class="footer-links">
                <button type="button" onclick = "forgot_password()" class="btn btn-primary">Submit</button>
                  <div class="pull-right"><a href="<?php echo base_url();?>login">Login</a></div>
            </div>

          </form>


        </div><!-- /.login-box-body -->
      </div><!-- /.login-box -->

      <!-- jQuery 2.1.4 -->
      <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
      <!-- Bootstrap 3.3.2 JS -->
      <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
      <!-- iCheck -->
      <script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

    </body>
    </html>
<script type="text/Javascript">
$( document ).ready(function() {

	$("#showHide").click(function(){

		if($("#password").attr("type") == "password")
		{
			$("#password").attr("type","text");
		}else
		{
			$("#password").attr("type","password");
		}

	 });
});
function check_email(){
	 var email = $('#email').val();
	 $.ajax({
            url: "<?php echo base_url() ?>users/check_email",
            type: 'POST',
            data: {'email':email},
            cache :true,
            async: false,
            success: function(response) {

              $("#un_email").html(response);

            }
        });
}

function forgot_password(){
	 var email = $('#email').val();
	 $.ajax({
            url: "<?php echo base_url() ?>brand_onboarding/Store_registration/send_forgotemail",
            type: 'POST',
            data: {'email':email},
            cache :true,
            async: false,
            success: function(response) {

              $("#un_email2").html(response);


            }
        });
}
</script>
