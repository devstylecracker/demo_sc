<?php 
    //echo "<pre>"; print_r($body_type);
?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>View Product Details</h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>/product">Product</a></li>
      <li><a href="<?php echo base_url();?>/product"></i>View Product Details</a></li>
      <li class="active">Here</li>
    </ol>    
  </section>

  <!-- Main content -->
 <section class="content">
<div class="row"> 
<div class="content"> 
      <div class="box">
                <div class="box-header">
                  <h3 class="box-title">View Product Details</h3>
                  
                </div><!-- /.box-header -->
                <tr class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>Field</th>
                      <th>Data</th>                     
                    </tr>
                
                 <tr>
                 <td>Uploaded Image</td>
                 <!--<?php echo  @$productData[0]->image; ?>-->
                  <td><img src="<?php echo base_url();?>assets/products/thumb/<?php  echo  @$productData[0]->image;?>"  ></td>  
                </tr>  

                <tr>
                  <td>Product Name </td>
                  <td><?php echo @$productData[0]->name; ?></td>
                 </tr>

                <tr>
                  <td>Product Description</td>                  
                   <td><?php echo @$productData[0]->description; ?></td>                  
                </tr>

                <tr >
                  <td>Brand  </td>
                   <td><?php 
                   $brand_name="";
                   foreach($brand as $singlebrand)
                   {
                     if($productData[0]->brand_id== $singlebrand['id'])
                     {
                        $brand_name = $singlebrand['user_name'];
                     }
                   }
                  
                   echo $brand_name ; 

                   ?></td> 
                </tr>

                <tr >
                  <td>Category  </td>
                  <td><?php echo $productData[0]->cat_name; ?></td>                   
                </tr>

                <tr >
                  <td >Sub Category </td>
                  <td><?php echo $productData[0]->subcat_name; ?></td>  
                </tr>
              
                <tr>
                  <td>Product URL </td>
                  <td><?php echo @$productData[0]->url; ?></td>
                </tr>

                <tr>
                  <td>Tags</td> 
                  <td><?php echo @$prdtag->name; ?></td>                           
                </tr>
                
                <tr>
                  <td>Price Range </td> 
                  <td><?php 
                      $price_range="";

                      if($productData[0]->price_range!="")
                      {
                          $price_range = $budget[$productData[0]->price_range];
                      }                  

                  echo $price_range; ?></td>                    
                </tr>

                <tr>
                  <td>Price </td>
                  <td><?php echo @$productData[0]->price; ?></td>  
                </tr>

                <tr>
                  <td>Target Market</td>
                 <td><?php echo ""; ?></td>  
                </tr>

                 <tr>
                  <td>Consumer Type</td> 
                   <td><?php echo $productData[0]->consumer_type; ?></td>  
                </tr>         

                <tr>
                  <td>Product Availability </td>
                   <td><?php echo ""; ?></td>
                </tr>

                <tr>
                  <td>Rating</td>
                  <td><?php echo  @$productData[0]->rating;?></td>                 
                </tr> 

                <tr>
                 <td>Body Part</td>
                 <td><?php 
                  echo  @$productData[0]->body_part;?></td>  
                </tr>

                <tr>
                 <td>Body Type</td>
                  <td><?php 
                      $body_type_name="";

                      if($productData[0]->body_type!="")
                      {
                          $body_type_name = $body_type[$productData[0]->body_type];
                      }  

                  echo $body_type_name;?></td> 
                </tr>

                <tr>
                 <td>Body Shape</td>
                  <td><?php echo 

                    $body_shape_name="";

                    if($productData[0]->body_shape!="")
                    {
                        $body_shape_name = $body_shape[$productData[0]->body_shape];
                    }  
                   echo $body_shape_name;?></td> 
                </tr>

                <tr>
                 <td>Personality</td>
                  <td><?php 
                      $personality_name="";

                    if($productData[0]->personality!="")
                    {
                        $personality_name = $personality[$productData[0]->personality];
                    }  

                  echo @$personality_name;?></td>
                </tr>

                 <tr>
                 <td>Age Group</td>
                  <td><?php 
                       $age_group_name="";

                    if($productData[0]->age_group!="")
                    {
                        $age_group_name = $age[$productData[0]->age_group];
                    }  

                  echo  @$age_group_name;?></td>                   
                </tr>

                <tr>
                  <td>Promotional </td> 
                    <td><?php if(@$productData[0]->is_promotional == '1') echo "YES"; else echo "NO"; ?></td>
                </tr>
               
                <tr>
                  <td>Active </td> 
                  <td><?php if(@$productData[0]->is_active == '1') echo "YES"; else echo "NO"; ?></td> 
                </tr>
             </div>
             
             
				<?php if(@$productData[0]->approve_reject == 'R') { ?>
               <tr>
                 <td>Reason </td> 
                 <td><?php if(@$productData[0]->reason){ echo $productData[0]->reason; } ?></td> 
               </tr>
               <?php } ?>
        </table>
        </div>
  </div><!-- /.box-body -->
</div><!-- /.box -->

</section>
