<?php
//print_r(@$category_details);
//echo @$category_details->status;
$users_menu=$this->data['users_menu'];
?>
<section class="content-header">
  <h1>Brand</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>brand">Brand</a></li>
    <li class="active">New Brand</li>
  </ol>
</section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Add New Brand</h3>
      </div><!-- /.box-header -->
      <form role="form" name="frmtag" id="frmtag" action="" method="post">
        <div class="box-body">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="tag_name" class="control-label">Brand Name
                  <span class="text-red">*</span></label>
                  <input type="text" placeholder="Enter New Tag Name" name="tag_name" id="tag_name" value="<?php if(@$tag->name!="") echo @$tag->name; else echo set_value('tag_name');?>" class="form-control"> <span class="text-red"><?php echo form_error('tag_name'); ?></span>
                </div>
                <div class="form-group">
                  <label for="categorySlug" class="control-label">Slug
                    <span class="text-red">*</span></label>
                    <input type="text" placeholder="Enter tag Slug" name="tag_slug" id="tag_slug" value="<?php if(@$tag->slug!="") echo @$tag->slug; else echo set_value('tag_slug');?>"  <?php //if(@$tag->slug!="") echo 'disabled'; ?> class="form-control"><span class="text-red"><?php echo form_error('tag_slug'); ?></span>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Active</label>
                    <div>
                      <label class="radio-inline"> <input type="radio" value="1" name="active" <?php if(@$tag->status==1) echo 'checked="true"'; ?> > Yes</label>
                      <label class="radio-inline"> <input type="radio" value="0" name="active" <?php if(@$tag->status==0) echo 'checked="true"';?> > No</label>
                    </div>
                  </div>
                </div>
              </div><!-- /.box-body -->

              <div class="box-footer">
                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i>  Submit</button>
                <a href="<?php echo base_url(); ?>tag"><button type="button" class="btn btn-sm btn-primary btn-rad"><i class="fa fa-reply"></i> Back</button></a>
                <button class="btn btn-sm btn-default" type="reset"><i class="fa fa-close"></i> Cancel</button>
              </div>
            </form>
          </div><!-- /.box -->
        </section>
        <script type="text/Javascript">
$( document ).ready(function() {
	$('.nav-item-brand').addClass("active");
});
</script>
