﻿
<script type="text/javascript">

    //## Arr declaration ----------------------

    var arrUserCategorySelected = [];

    var arrUserAnswerCategory = [];

    var arrUserAnswerAfterCategory = [];

    var arrUserAnswerSelected = [];

    var arrUserAnswerAfterSelected = [];

    $(document).ready(function(e){

        //## Global Events ------------------------

        $('input').on('change', function(e){

            CheckUserSelectedData();

        });

        

        $('select').on('change', function(e){

            CheckUserSelectedData();

        });

        //## Check the values selected by user -----

        function CheckUserSelectedData(){

            arrUserCategorySelected = [];

            arrUserAnswerCategory = [];

            arrUserAnswerAfterCategory = [];

            arrUserAnswerSelected = [];

            arrUserAnswerAfterSelected = [];

            $.each($(".pa-data input"),function(e,val){

                var obj = $(this);

                if(obj.prop("checked")){

                    var arrName = obj.closest('div').attr('push-to');

                    var objValue = $(this).val();

                    StoreData(arrName,objValue);

                }

            });

            $.each($(".pa-data select"),function(e,val){

                var obj = $(this);

                if($(this).val()!=""){

                    var arrName = obj.closest('div').attr('push-to');

                    var objValue = $(this).val();

                    StoreData(arrName,objValue);

                }

            });

            msg("arrUserCategorySelected: "+arrUserCategorySelected);

            msg("arrUserAnswerCategory: "+arrUserAnswerCategory);

            msg("arrUserAnswerAfterCategory: "+arrUserAnswerAfterCategory);

            msg("arrUserAnswerSelected: "+arrUserAnswerSelected);

            msg("arrUserAnswerAfterSelected: "+arrUserAnswerAfterSelected);

            msg("_________________________________________________");

        }

        //## Store the values ------------------------

        function StoreData(arrName,objValue){

            msg(arrName+" :: "+objValue);

            if(arrName == 'arrUserCategorySelected'){

                arrUserCategorySelected.push(objValue);

            }if(arrName == 'arrUserAnswerCategory'){

                arrUserAnswerCategory.push(objValue);

            }if(arrName == 'arrUserAnswerAfterCategory'){

                arrUserAnswerAfterCategory.push(objValue);

            }if(arrName == 'arrUserAnswerSelected'){

                arrUserAnswerSelected.push(objValue);

            }if(arrName == 'arrUserAnswerAfterSelected'){

                arrUserAnswerAfterSelected.push(objValue);

            }

        }

        

    });

    function msg(m) {

        console.log(m);

    }

    

    function SavePA(){

        msg("SavePA function initialized..");

        alert("arrUserCategorySelected: "+arrUserCategorySelected+"\narrUserAnswerCategory:: "+arrUserAnswerCategory+"\narrUserAnswerAfterCategory :: "+arrUserAnswerAfterCategory+"\narrUserAnswerSelected :: "+arrUserAnswerSelected+"\narrUserAnswerAfterSelected :: "+arrUserAnswerAfterSelected);

        var order_unq_no = $('#order_number').val();
       // alert('order_unq_no--'+order_unq_no);

        if(order_unq_no!='' && arrUserCategorySelected!='' && arrUserAnswerCategory!=''  && arrUserAnswerSelected!=''  )
        { 
            $.ajax({
                type:'POST',
                url: "<?php echo base_url(); ?>Scboxusers/user_order_pa_save",
                data:{'order_unq_no':order_unq_no,'arrUserCategorySelected':arrUserCategorySelected,'arrUserAnswerCategory':arrUserAnswerCategory,'arrUserAnswerAfterCategory':arrUserAnswerAfterCategory,'arrUserAnswerSelected':arrUserAnswerSelected,'arrUserAnswerAfterSelected':arrUserAnswerAfterSelected },
                dataType: "json",
                cache:false,
                success:function(data){  
                alert(data);
                
                },
                error: function(data){
                    console.log("error");
                    console.log(data);
                }
            });
        }

    }

    function ResetAll(){

        msg("ResetAll function initialized..");

    }

    

    

</script>


<style>
	.label-text{
		font-weight: bold;
	}
 	
 	.col-md-4{
 		margin: 10px;
 		width: 26%;
 	}

 	.col-ms-6{
 		margin: 10px;
 		width: 44%;
 	}
 	.pallet{
 		width: 20px;
 		height: 20px;


 	}
 	.checkbox-inline{
 		padding-top: 10px;
 	}
</style>

<section class="content-header">
  <h1>PA Edit Data</h1>
</section>
<section class="content">
<div class="box">
<div class="box-body">
    <div class="pa-base">

        <form action="pa-form">

           <div class="row text-center">
	            <div class="col-sm-12 col-md-12">
	            	<span class="label-text">Order Number:</span> <input id="order_number" type="text" name="order_number"><br><br>

	                <span class="label-text">Gender:</span>
	                 <label class="radio-inline">
					      <input type="radio" name="gender" value="1">Female
					  </label>
					 <label class="radio-inline">
	                	<input type="radio" name="gender" value="2"> Male
					</label>
	            </div>
  
           </div>

        
           <div class="pa-data">
           	<div class="row text-center" style="padding-top: 30px;">   	
	            <div class="q_0" push-to="arrUserCategorySelected">

	                <label>Category Selection</label><br>

	                <label class="checkbox-inline"><input type="checkbox" value="3"> Apparel</label>

	                <label class="checkbox-inline"><input type="checkbox" value="4"> Bags</label>

	                <label class="checkbox-inline"><input type="checkbox" value="5"> Footwear</label>

	                <label class="checkbox-inline"><input type="checkbox" value="6"> Accessories</label>

	                <label class="checkbox-inline"><input type="checkbox" value="8"> Jewellery</label>

	                <label class="checkbox-inline"><input type="checkbox" value="7"> Beauty</label>

	            </div>
            </div>
            
        <div class="row" style="padding-top:30px; "> 
        <div class="col-md-6" style="border-right:1px dashed gray">   
            <div class="apparel">
            	<h3 class="text-center">APPAREL</h3>
                <div class="q_1" push-to="arrUserAnswerSelected">

                    <label>This Is What My Body Looks Like</label><br>

                    <label class="radio-inline"><input type="radio" name="body_shape" value="289">Hourglass</label>

                    <label class="radio-inline"><input type="radio" name="body_shape" value="290">Apple</label>

                    <label class="radio-inline"><input type="radio" name="body_shape" value="291">Column</label>

                    <label class="radio-inline"><input type="radio" name="body_shape" value="292">Pear</label>

                    <label class="radio-inline"><input type="radio" name="body_shape" value="293">Inverted Traingle</label>

                    <label class="radio-inline"><input type="radio" name="body_shape" value="294">Goblet</label>

                </div><br>



                <div class="q_2" push-to="arrUserAnswerCategory">

                    <label>Stuff I Don't Need</label><br>

                    <label class="checkbox-inline"><input type="checkbox" value="-9">Tops</label>

                    <label class="checkbox-inline"><input type="checkbox" value="-10">Dresses</label>

                    <label class="checkbox-inline"><input type="checkbox" value="-11">Shorts And Skirts</label>

                    <label class="checkbox-inline"><input type="checkbox" value="-13">Jeans and Trousers</label>

                    <label class="checkbox-inline"><input type="checkbox" value="-14">Outerwear</label>

                    <label class="checkbox-inline"><input type="checkbox" value="-12">Jumpsuits</label>

                </div><br>

                <div class="q_3 row" push-to="arrUserAnswerSelected">

                    <label>Size And Fit</label><br>
          
                    <select class="form-control size-top col-md-4" type="size-top" name="scbox_sizetop">

                      <option value="">Select top size</option>

                       <option value="306">xs/uk 4/us 2/eu 32</option>

                       <option value="307">s/uk 6/us 4/eu 34</option>

                       <option value="308">s/uk 8/us 6/eu 36</option>

                       <option value="309">m/uk 10 /us 8/eu 38</option>

                       <option value="310">m/uk 12/us 10/eu 40</option>

                       <option value="311">l/uk 14/us 12/eu 42</option>

                       <option value="312">l/uk 16/us 14/eu 44</option>

                       <option value="313">xl/uk 18/us 16/eu 46</option>

                       <option value="314">xxl/uk 20/us 18/eu 48</option>

                       <option value="315">xxxl/uk 22/us 20/eu 50</option>

                    </select>
				

				
                    <select class="form-control dress_size col-md-4" name="dress_size" type="dress_size">

                       <option value="">Select dress size</option>

                       <option value="325">xs/uk 4/us 2/eu 32</option>

                       <option value="326">s/uk 6/us 4/eu 34</option>

                       <option value="327">s/uk 8/us 6/eu 36</option>

                       <option value="328">m/uk 10 /us 8/eu 38</option>

                       <option value="329">m/uk 12/us 10/eu 40</option>

                       <option value="330">l/uk 14/us 12/eu 42</option>

                       <option value="331">l/uk 16/us 14/eu 44</option>

                       <option value="332">xl/uk 18/us 16/eu 46</option>

                       <option value="333">xxl/uk 20/us 18/eu 48</option>

                       <option value="334">xxxl/uk 22/us 20/eu 50</option>

                    </select>
               

                    <select class="form-control trouser-size col-md-4" type="trouser-size" name="scbox_sizetrouser">

                       <option value="">Select trouser size</option>

                       <option value="340">xs/ 25</option>

                       <option value="341">s/26</option>

                       <option value="342">m/28</option>

                       <option value="343">l/30</option>

                       <option value="344">xl/32</option>

                       <option value="345">xxl/34</option>

                       <option value="346">xxxl/36</option>

                    </select>
               


                    <select class="form-control jeans-size col-md-4" type="jeans-size" name="scbox_denim_size">

                       <option value="">select jeans size</option>

                       <option value="347">xs/ 25</option>

                       <option value="348">s/26</option>

                       <option value="349">m/28</option>

                       <option value="350">l/30</option>

                       <option value="351">xl/32</option>

                       <option value="352">xxl/34</option>

                       <option value="353">xxxl/36</option>

                    </select>
				 


                    <select class="form-control band-size col-md-4" type="band-size">

                       <option value="">Select Bra: band size</option>

                       <option value="316">30</option>

                       <option value="317">32</option>

                       <option value="318">34</option>

                       <option value="319">36</option>

                       <option value="320">38</option>

                       <option value="321">40</option>

                    </select>
               
                    <select class="form-control cup-size col-md-4" type="cup-size">

                       <option value="">Select Bra: cup size</option>

                       <option value="436">a</option>

                       <option value="437">b</option>

                       <option value="438">c</option>

                       <option value="439">d</option>

                       <option value="440">dd</option>

                       <option value="441">f</option>

                    </select>
               

                </div><br>

                <div class="q_4" push-to="arrUserAnswerSelected">

                    <label>This Is What My Body Looks Like</label><br>

                     <label class="checkbox-inline"><input type="checkbox" value="322">Fitted</label>

                     <label class="checkbox-inline"><input type="checkbox" value="323">Relaxed</label>

                     <label class="checkbox-inline"><input type="checkbox" value="324">Flowy</label>

                </div><br>

                <div class="q_5" push-to="arrUserAnswerSelected">

                    <label>My Dress Should Fit Like This</label><br>

                     <label class="checkbox-inline"><input type="checkbox" value="335">Bodycon</label>

                     <label class="checkbox-inline"><input type="checkbox" value="336">Shift</label>

                     <label class="checkbox-inline"><input type="checkbox" value="337">A line</label>

                     <label class="checkbox-inline"><input type="checkbox" value="338">Skater</label>

                     <label class="checkbox-inline"><input type="checkbox" value="339">Flowy</label>

                </div><br>

                <div class="q_6" push-to="arrUserAnswerSelected">

                    <label>I Like The Length Of My Skirts And Dresses To Be Like This</label><br>

                     <label class="checkbox-inline"><input type="checkbox" value="358">Mini</label>

                     <label class="checkbox-inline"><input type="checkbox" value="359">Above the knee</label>

                     <label class="checkbox-inline"><input type="checkbox" value="360">Below the knee</label>

                     <label class="checkbox-inline"><input type="checkbox" value="361">Midi</label>

                     <label class="checkbox-inline"><input type="checkbox" value="362">Maxi</label>

                </div><br>

                <div class="q_12" push-to="arrUserAnswerSelected">

                    <label>My Bottoms Should Fit Like This</label><br>

                     <label class="checkbox-inline"><input type="checkbox" value="354">Skinny</label>

                     <label class="checkbox-inline"><input type="checkbox" value="355">Slim</label>

                     <label class="checkbox-inline"><input type="checkbox" value="356">Straight</label>

                     <label class="checkbox-inline"><input type="checkbox" value="357">Flared</label>

                </div><br>



                <div class="q_7 row" push-to="arrUserAnswerSelected">

                    <label>My Do's And Don'ts</label><br>

               

                    <span class="col-md-3">

                        <label>Sleeveless</label><br>

                         <label class="radio-inline"><input type="radio" name="yes-no1" value="494"> Yes</label>

                         <label class="radio-inline"><input type="radio" name="yes-no1" value="-494"> No</label>

                    </span>
               

                 

                    <span class="col-md-3">

                        <label>Halter</label><br>

                         <label class="radio-inline"><input type="radio" name="yes-no2" value="495"> Yes</label>

                        <label class="radio-inline"> <input type="radio" name="yes-no2" value="-495"> No</label>

                    </span>

                 

                    <span class="col-md-3">

                        <label>Strappy</label><br>

                         <label class="radio-inline"><input type="radio" name="yes-no3" value="496"> Yes</label>

                         <label class="radio-inline"><input type="radio" name="yes-no3" value="-496"> No</label>

                    </span>

              
                    <span class="col-md-3">

                        <label>Off Shoulder</label><br>

                         <label class="radio-inline"><input type="radio" name="yes-no4" value="497"> Yes</label>

                         <label class="radio-inline"><input type="radio" name="yes-no4" value="-497"> No</label>

                    </span>
               
                     <span class="col-md-3">

                        <label>Tube</label><br>

                         <label class="radio-inline"><input type="radio" name="yes-no5" value="498"> Yes</label>

                         <label class="radio-inline"><input type="radio" name="yes-no5" value="-498"> No</label>

                    </span>
                
                    <span class="col-md-3">

                        <label>Crop Top</label><br>

                         <label class="radio-inline"><input type="radio" name="yes-no6" value="499"> Yes</label>

                         <label class="radio-inline"><input type="radio" name="yes-no6" value="-499"> No</label>

                    </span>
                


                    <span class="col-md-3">

                        <label>Backless</label><br>

                        <label class="radio-inline"> <input type="radio" name="yes-no7" value="500"> Yes</label>

                         <label class="radio-inline"><input type="radio" name="yes-no7" value="-500"> No</label>

                    </span>
               
                    <span class="col-md-3">

                        <label>Deep Neck</label><br>

                         <label class="radio-inline"><input type="radio" name="yes-no8" value="501"> Yes</label>

                         <label class="radio-inline"><input type="radio" name="yes-no8" value="-501"> No</label>

                    </span>
                </div>

                </div><br>



                <div class="q_8 row" push-to="arrUserAnswerSelected">

                    <label>My Style Preference</label><br>
                  
                    <span class="col-md-6">

                        <label>Classicist</label><br>

                         <label class="radio-inline"><input type="radio" name="love-like-dislike1" value="295"> Love</label>

                         <label class="radio-inline"><input type="radio" name="love-like-dislike1" value="295"> Like</label>

                         <label class="radio-inline"><input type="radio" name="love-like-dislike1" value="-295"> Dislike</label>

                    </span>

                    <span class="col-md-6">

                        <label>Romantic-Feminine</label><br>

                         <label class="radio-inline"><input type="radio" name="love-like-dislike2" value="296"> Love</label>

                         <label class="radio-inline"><input type="radio" name="love-like-dislike2" value="296"> Like</label>

                         <label class="radio-inline"><input type="radio" name="love-like-dislike2" value="-296"> Dislike</label>

                    </span>
                    <span class="col-md-6">

                        <label>Free Spirit</label><br>

                         <label class="radio-inline"><input type="radio" name="love-like-dislike3" value="297"> Love</label>

                         <label class="radio-inline"><input type="radio" name="love-like-dislike3" value="297"> Like</label>

                         <label class="radio-inline"><input type="radio" name="love-like-dislike3" value="-297"> Dislike</label>

                    </span>
                    <span class="col-md-6">

                        <label>Bombshell</label><br>

                         <label class="radio-inline"><input type="radio" name="love-like-dislike4" value="298"> Love</label>

                         <label class="radio-inline"><input type="radio" name="love-like-dislike4" value="298"> Like</label>

                         <label class="radio-inline"><input type="radio" name="love-like-dislike4" value="-298"> Dislike</label>

                    </span>
                    <span class="col-md-6">

                        <label>Bold and Edgy</label><br>

                         <label class="radio-inline"><input type="radio" name="love-like-dislike5" value="299"> Love</label>

                         <label class="radio-inline"><input type="radio" name="love-like-dislike5" value="299"> Like</label>

                         <label class="radio-inline"><input type="radio" name="love-like-dislike5" value="-299"> Dislike</label>

                    </span>
                    <span class="col-md-6">

                        <label>Athleisure</label><br>

                         <label class="radio-inline"><input type="radio" name="love-like-dislike6" value="300"> Love</label>

                         <label class="radio-inline"><input type="radio" name="love-like-dislike6" value="300"> Like</label>

                         <label class="radio-inline"><input type="radio" name="love-like-dislike6" value="-300"> Dislike</label>

                    </span>

                </div><br>





                <div class="q_9" push-to="arrUserAnswerSelected">

                    <label>Colors I Avoid</label><br>

                     <label class="checkbox-inline"><input type="checkbox" value="-419"><div class="pallet" color="#90140c" style="background-color:#90140c;border:1px solid #90140c;"></div></label>

                     <label class="checkbox-inline"><input type="checkbox" value="-424"> <div class="pallet" color="#b8bebe" style="background-color:#b8bebe;border:1px solid #b8bebe;"></div></label>

                     <label class="checkbox-inline"><input type="checkbox" value="-403"> <div class="pallet" color="#be914a" style="background-color:#be914a;border:1px solid #be914a;"></div></label>

                     <label class="checkbox-inline"><input type="checkbox" value="-404"><div class="pallet" color="#ff0080" style="background-color:#FF0080;border:1px solid #FF0080;"></div></label>

                     <label class="checkbox-inline"><input type="checkbox" value="-405"> <div class="pallet" color="#734021" style="background-color:  #734021;border:1px solid #734021;"></div></label>

                     <label class="checkbox-inline"><input type="checkbox" value="-406"><div class="pallet" color="#fec4c3" style="background-color:#fec4c3;border:1px solid #fec4c3;"></div></label>

                     <label class="checkbox-inline"><input type="checkbox" value="-407"><div class="pallet" color="#0b0706" style="background-color:#0b0706;border:1px solid #0b0706;"></div></label>

                     <label class="checkbox-inline"><input type="checkbox" value="-408"><div class="pallet" color="#98012e" style="background-color:  #98012e;border:1px solid #98012e;"></div></label>

                     <label class="checkbox-inline"><input type="checkbox" value="-409"><div class="pallet" color="#ffffff" style="background-color:#ffffff;border:1px solid gray;"></div></label>

                     <label class="checkbox-inline"><input type="checkbox" value="-410"> <div class="pallet" color="#f57d31" style="background-color:#f57d31;border:1px solid #f57d31;"></div></label>

                     <label class="checkbox-inline"><input type="checkbox" value="-411"> <div class="pallet" color="#87ceeb" style="background-color:#87ceeb;border:1px solid #87ceeb;"></div></label>

                     <label class="checkbox-inline"><input type="checkbox" value="-412"><div class="pallet" color="#cdd1d1" style="background-color:#cdd1d1;border:1px solid #cdd1d1;"></div></label>

                     <label class="checkbox-inline"><input type="checkbox" value="-413"> <div class="pallet" color="#d5011a" style="background-color:#d5011a;border:1px solid #d5011a;"></div></label>

                     <label class="checkbox-inline"><input type="checkbox" value="-414"><div class="pallet" color="#800080" style="background-color:#800080;border:1px solid #800080;"></div></label>

                     <label class="checkbox-inline"><input type="checkbox" value="-415"><div class="pallet" color="#bcd2ee" style="background-color:#bcd2ee;border:1px solid #bcd2ee;"></div></label>

                     <label class="checkbox-inline"><input type="checkbox" value="-416"><div class="pallet" color="#576331" style="background-color:#576331;border:1px solid #576331;"></div></label>

                     <label class="checkbox-inline"><input type="checkbox" value="-417"><div class="pallet" color="#041531" style="background-color:#041531;border:1px solid #041531;"></div></label>

                     <label class="checkbox-inline"><input type="checkbox" value="-418"><div class="pallet" color="#f7b719" style="background-color:#f7b719;border:1px solid #f7b719;"></div></label>

                </div><br>



                <div class="q_9" push-to="arrUserAnswerSelected">

                    <label class="checkbox-inline"><input type="checkbox" value="516">I am a rainbow I like all colors</label>

                </div><br>



                <div class="q_10" push-to="arrUserAnswerSelected">

                    <label>Prints And Patterns I Avoid</label><br>

                     <label class="checkbox-inline"><input type="checkbox" value="-198">Polka Dots</label>

                     <label class="checkbox-inline"><input type="checkbox" value="-184">Floral</label>

                     <label class="checkbox-inline"><input type="checkbox" value="-199">Strips</label>

                     <label class="checkbox-inline"><input type="checkbox" value="-251">Animal</label>

                     <label class="checkbox-inline"><input type="checkbox" value="-253">Checks</label>

                     <label class="checkbox-inline"><input type="checkbox" value="-255">Solid</label>

                </div><br>

                

                <div class="q_11 row" push-to="arrUserAnswerAfterSelected">

                    <label>How Tall Are You?</label><br>

                    

                    <select name="height-height_feet" type="height-feet" class="form-control col-md-4 height_feet">

                        <option value="">Select height feet</option>

                        <option value="517">3 ft</option>

                        <option value="518">4 ft</option>

                        <option value="519">5 ft</option>

                        <option value="520">6 ft</option>

                    </select>
                   
                    <select name="height-height_inches" type="height-inches" class="form-control col-md-4 height_inches">

                        <option value="">Select height inches</option>

                        <option value="521">0 inch</option>

                        <option value="522">1 inch</option>

                        <option value="523">2 inch</option>

                        <option value="524">3 inch</option>

                        <option value="525">4 inch</option>

                        <option value="526">5 inch</option>

                        <option value="527">6 inch</option>

                        <option value="528">7 inch</option>

                        <option value="529">8 inch</option>

                        <option value="530">9 inch</option>

                        <option value="531">10 inch</option>

                        <option value="532">11 inch</option>

                    </select>  
                    </div>                

                </div>

         
         <div class="col-md-6">   

            <div class="bags">
            		<h3 class="text-center">BAGS</h3>
                <div class="q_1" push-to="arrUserAnswerCategory">

                    <label>The Kind Of Bag I Am Looking For</label><br>

                     <label class="checkbox-inline"><input type="checkbox" value="15">Sling</label>

                     <label class="checkbox-inline"><input type="checkbox" value="16">Clutch</label>

                     <label class="checkbox-inline"><input type="checkbox" value="17">Handbag</label>

                     <label class="checkbox-inline"><input type="checkbox" value="18">Tote</label>

                     <label class="checkbox-inline"><input type="checkbox" value="19">Backpack</label>

                </div><br>             

                
             <div class="row">   
                <div class="q_2 col-md-6" push-to="arrUserAnswerAfterSelected">

                    <label>The Kind Of Bag You Want</label><br>

                    <label class="radio-inline"><input type="radio" name="bags1" value="363"> Everybody</label>

                    <label class="radio-inline"><input type="radio" name="bags1" value="364"> Work</label>

                    <label class="radio-inline"><input type="radio" name="bags1" value="365"> Party</label>

                </div>

                

                <div class="q_3 col-md-6" push-to="arrUserAnswerAfterSelected">

                    <label>I Want The Size Of My Bag To Be</label><br>

                        <select class="form-control bag-size" type="bag-size">

                           <option value="">Select bag size</option>

                           <option value="366">small</option>

                           <option value="367">medium</option>

                           <option value="368">large</option>

                        </select>

                </div></div><br>

            </div>

            

            <div class="footwear">
            	<h3 class="text-center">FOOTWEAR</h3>
            	<div class="row">   
                <div class="q_1 col-md-6" push-to="arrUserAnswerSelected">

                     <label>Footwear Size</label><br>

                    <select class="form-control footwear-size" type="footwear-size">

                      <option value="">Select footwear size</option>

                      <option value="380">UK 3/US 5/EU 36</option>

                      <option value="381">UK 4/US 6/EU 37</option>

                      <option value="382">UK 5/US 7/EU 38</option>

                      <option value="383">UK 6/US 8/EU 39</option>

                      <option value="581">UK 6.5/US 7/EU 40</option>

                      <option value="384">UK 7/US 9/EU 41</option>

                      <option value="385">UK 8/US 10/EU 42</option>

                      <option value="386">UK 9/US 11/EU 43</option>

                   </select>

                </div>

                <div class="q_2 col-md-6" push-to="arrUserAnswerSelected">

                    <label>Foot Width</label><br>

                    <label class="radio-inline"><input type="radio" name="footwear1" value="387"> I have broad feet</label>

                    <label class="radio-inline"><input type="radio" name="footwear1" value="388"> I have narrow feet</label>

                </div></div><br>

                
                <div class="row">
                <div class="q_3 col-md-6" push-to="arrUserAnswerCategory">

                    <label>The Type Of Shoes I Am Looking For</label><br>

                     <label class="checkbox-inline"><input type="checkbox" value="20"> Flats</label>

                     <label class="checkbox-inline"><input type="checkbox" value="21"> Sneakers</label>

                     <label class="checkbox-inline"><input type="checkbox" value="22"> Heels</label>

                </div>


                

                <div class="q_4 col-md-6" push-to="arrUserAnswerAfterSelected">

                    <label>The Kind Of Bag I Am Looking For</label><br>

                     <label class="checkbox-inline"><input type="checkbox" value="389"> Wedge</label>

                     <label class="checkbox-inline"><input type="checkbox" value="390"> Block</label>

                     <label class="checkbox-inline"><input type="checkbox" value="391"> Stilletos</label>

                </div>
            </div><br>

                

                <div class="q_5" push-to="arrUserAnswerAfterSelected">

                    <label>Footwear Heel Height</label><br>

                    <select class="form-control footwear-heel-height" type="footwear-heel-height">

                        <option value="">Select heel height</option>

                        <option value="392">low: 1.5”-2.5”</option>

                        <option value="393">medium: 2.5”-3.5” </option>

                        <option value="394">high: 3.5” and above</option>

                    </select>

                </div><br>

            

            </div>

            

            <div class="accessories">
            	<h3 class="text-center">ACCESSORIES</h3>
                <div class="q_1" push-to="arrUserAnswerCategory">

                    <label>Accessories</label><br>

                    <label class="radio-inline"><input type="radio" name="acc1" value="23"> Sunglasses</label>

                   <label class="radio-inline"> <input type="radio" name="acc1" value="29"> Belts</label>

                    <label class="radio-inline"><input type="radio" name="acc1" value="30"> Stationery</label>

                    <label class="radio-inline"><input type="radio" name="acc1" value="31"> Caps</label>

                    <label class="radio-inline"><input type="radio" name="acc1" value="32"> Scarves</label>

                </div><br>

                

                <div class="q_2" push-to="arrUserAnswerAfterCategory">

                    <label>You Are Looking For Sunglasses ! Tell Us What Kind?</label><br>

                     <label class="checkbox-inline"><input type="checkbox" value="24"> Aviator</label>

                     <label class="checkbox-inline"><input type="checkbox" value="25"> Cat Eyes</label>

                     <label class="checkbox-inline"><input type="checkbox" value="26"> Round</label>

                     <label class="checkbox-inline"><input type="checkbox" value="27"> Square</label>

                     <label class="checkbox-inline"><input type="checkbox" value="28"> Wayfares</label>

                </div><br>

                

                <div class="q_3" push-to="arrUserAnswerAfterSelected">

                    <label>Reflectors</label><br>

                    <label class="radio-inline"> <input type="radio" name="acc2" value="533"> Yes</label>

                    <label class="radio-inline"> <input type="radio" name="acc2" value="534"> No</label>

                </div><br>

                

                <div class="q_4" push-to="arrUserAnswerAfterSelected">

                    <label>Could You Tell Us What Kind Of Belt?</label><br>

                     <label class="checkbox-inline"><input type="checkbox" value="371"> Slim</label>

                     <label class="checkbox-inline"><input type="checkbox" value="372"> Medium</label>

                     <label class="checkbox-inline"><input type="checkbox" value="373"> Chunky</label>

                </div><br>

            </div>

            

            <div class="jewellery">
            	<h3 class="text-center">JEWELLERY</h3>
                <div class="q_1" push-to="arrUserAnswerSelected">

                    <label>The Kind Of Jewellery I Am Looking For</label><br>

                     <label class="checkbox-inline"><input type="checkbox" value="395"> Statement</label>

                     <label class="checkbox-inline"><input type="checkbox" value="396"> Minimal</label>

                     <label class="checkbox-inline"><input type="checkbox" value="397"> Bold/Chunky</label>

                </div><br>

                

                <div class="q_2" push-to="arrUserAnswerAfterSelected">

                    <label>The Tone Of Jewellery I Am Looking For</label><br>

                     <label class="checkbox-inline"><input type="checkbox" value="398"> Gold</label>

                     <label class="checkbox-inline"><input type="checkbox" value="399"> Silver</label>

                     <label class="checkbox-inline"><input type="checkbox" value="400"> Oxidised</label>

                     <label class="checkbox-inline"><input type="checkbox" value="401"> Multicolored</label>

                     <label class="checkbox-inline"><input type="checkbox" value="402"> Tonal</label>

                </div><br>

                

                

            </div>

            

            <div class="beauty">
            	<h3 class="text-center">BEAUTY</h3>
                <div class="q_1" push-to="arrUserAnswerCategory">

                    <label>The Beauty Product I'm Looking For</label><br>

                     <label class="radio-inline"><input type="radio" name="beauty1" value="35"> Eye Shadow</label>

                     <label class="radio-inline"><input type="radio" name="beauty1" value="34"> Liner</label>

                     <label class="radio-inline"><input type="radio" name="beauty1" value="33"> Lipstick</label>

                     <label class="radio-inline"><input type="radio" name="beauty1" value="36"> Nail Paint</label>

                     <label class="radio-inline"><input type="radio" name="beauty1" value="39"> Mascara</label>

                     <label class="radio-inline"><input type="radio" name="beauty1" value="38"> Hand Creme</label>

                     <label class="radio-inline"><input type="radio" name="beauty1" value="37"> Dry Shampoo</label>

                </div><br>

               <div class="row"> 
                <div class="q_2 col-md-6" push-to="arrUserAnswerSelected">    

                    <label>Shade</label><br>

                     <label class="radio-inline"><input type="radio" name="beauty2" value="374"> Red</label>

                     <label class="radio-inline"><input type="radio" name="beauty2" value="375"> Nude</label>

                     <label class="radio-inline"><input type="radio" name="beauty2" value="376"> Pink</label>

                     <label class="radio-inline"><input type="radio" name="beauty2" value="377"> Deep</label>

                </div>

                <div class="q_3 col-md-6" push-to="arrUserAnswerAfterSelected">

                        <label>The Finish Of The Beauty Product</label><br>

                         <label class="radio-inline"><input type="radio" name="beauty3" value="378"> Matte</label>

                         <label class="radio-inline"><input type="radio" name="beauty3" value="379"> Shiny</label>

                </div>
            </div><br>

                    

            </div>

         </div>
         </div>   
            </div>

            <button type="submit" form="pa-form" onclick="SavePA();" value="Submit" class="btn btn-primary">Submit</button>

            <button type="reset" value="Reset" class="btn btn-default">Reset</button> 

            

        </form>

        

    </div>

</div>
</div>
</section>