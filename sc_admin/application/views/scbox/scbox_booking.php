<style type="text/css">
  .listingstyle {
  margin-top: 0;
  margin-bottom: 10px;
  list-style: none;
  }
  .listingstyle li{
  padding-right: 20px;
  }
</style>
<?php
  $selected = '';
  // echo "<pre>";print_r($user_info);exit;
  $user_colors_array = explode(',',@$user_data['user_colors']['object_meta_value']);
  $user_prints_array = explode(',',@$user_data['user_prints']['object_meta_value']);
  
  ?>
<?php if($form_type == 'add'){ ?>
<section class="content-header">
  <h1>SCBox Booking</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">SCBox Booking</li>
  </ol>
</section>
<?php } ?>

<section class="content">
  <!--<form name="scbox-form" id="scbox-form" method="post" action="<?php if($form_type == 'edit'){ echo base_url().'scbox/new_scbox_edit/'.$user_id;} ?>" onsubmit="return validateForm()">-->
 <?php
	// After Payment JSON Data
	/* echo "<pre>";
	print_r($paSelectionAfter);
	echo "</pre>"; */
 ?>
  <form name="scbox-form" id="scbox-form" method="post" action="<?php if($form_type == 'edit'){ echo base_url().'scbox/new_scbox_edit/'.$user_id;} ?>" >
    <div class="box">
      <div class="box-header">
        <h2 class="box-title">ADD PA DATA</h2>
      </div>
      <input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>">
      <div class="box-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label">SCBox Package
              <span class="text-red">*</span></label>
              <label id="scbox_package-error" class="text-red hide" for="scbox_package">Please select scbox package.</label>
              <label id="scbox_package-error2" class="text-red hide" for="scbox_package">Please enter price of the box.</label>
              <div id="scbox_package_all">
                <label class="radio-inline">
                <input type="radio" name="scbox_package" id='scbox_package_276' value="276" <?php if(@$user_data['user_gender']['object_id'] == '276'){ echo "checked"; } ?> ><i class="fa fa-inr"></i> 2,999</label>
                <label class="radio-inline">
                <input type="radio" name="scbox_package" id='scbox_package_275' value="275" <?php if(@$user_data['user_gender']['object_id'] == '275'){ echo "checked"; } ?> ><i class="fa fa-inr"></i> 4,999</label>
                <label class="radio-inline">
                <input type="radio" name="scbox_package" id='scbox_package_274' value="274" <?php if(@$user_data['user_gender']['object_id'] == '274'){ echo "checked"; } ?> ><i class="fa fa-inr"></i> 6,999</label>
                <label class="radio-inline">
                <input type="radio" name="scbox_package" id='scbox_package_277' value="277" <?php if(@$user_data['user_gender']['object_id'] == '277'){ echo "checked"; } ?> >
                <input type="text" name="box_price" id='box_price' placeholder="Amount" pattern="[0-9]{1,}" class="form-control input-sm" value="<?php if(@$user_data['user_gender']['object_id'] == '277'){ echo @$user_data['scbox_price']['object_meta_value']; } ?>" onclick="add_checked();" >
                </label>
                <span class="text-red"></span>
              </div>
            </div>
          </div>
          <div class="col-md-6">
             <label class="control-label">Buying Options</label>
            <ul class="listingstyle" style="display: flex;">
              <li><label for="chkNo" class="recipient_add">
                <input type="radio" id="chkNo" name="chkrecipient_add" onclick="ShowHideDiv()" checked />
                For Me
                </label>  
              </li>
              <li> <label for="chkYes" class="recipient_add">
                <input type="radio" id="chkYes" name="chkrecipient_add" onclick="ShowHideDiv()" />
                This is a Gift
                </label>
              </li>
            </ul>
          </div>
        </div>
        <!---->
        <div class="row">
          <div class="col-md-6">
            <div class="row">
              <div class="box-header">
               <h2 class="box-title"><b>RECIPIENT'S INFORMATION</b></h2>
              </div> 
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Select Gender</label>
				  <?php
					echo $user_data['user_gender']['object_meta_value'];
				  ?>
                  <label id="scb_gender-error" class="text-red hide" for="scb_gender">Please select gender.</label>
                  <span class="text-red">*</span></label>
                  <select name="scb_gender" id="scb_gender" class="form-control input-sm" onchange="get_pa_attr()" required>
                    <option value="">Select</option>
                    <option <?php if(@$user_data['user_gender']['object_meta_value'] == '1'){ echo "selected"; } ?> value="1">Female</option>
                    <option <?php if(@$user_data['user_gender']['object_meta_value'] == '2'){ echo "selected"; } ?> value="2">Male</option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label" for="">First Name</label><span class="text-red">*</span></label>
                  <input type="text" class="form-control" name="f_name" id="f_name" placeholder="First Name" required value="<?php echo @$user_info[0]['first_name']; ?>" >
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label" for="">Last Name</label><span class="text-red">*</span></label>
                  <input type="text" class="form-control" name="l_name" id="l_name" placeholder="Last Name" required value="<?php echo @$user_info[0]['last_name']; ?>" >
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label" for="">Mobile</label><span class="text-red">*</span></label>
                  <input type="text" class="form-control" name="mobile_no" id="mobile_no" placeholder="Mobile" maxlength="10" pattern="[1-9]{1}[0-9]{9}" value="<?php echo @$user_data['user_mobile']['object_meta_value']; ?>" required>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label" for="">Email</label><span class="text-red">*</span></label>
                  <input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="form-control" name="email" id="email" placeholder="Email" <?php if($form_type == 'add'){ echo 'onblur="check_email()"'; }else{ echo ' readonly="" '; } ?> value="<?php echo @$user_info[0]['email']; ?>" required>
                  <input type="hidden" id="email_exists" value="0">
                  <span class="text-red hide" id="un_email"> Email already exists</span>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Date Of Birth</label><span class="text-red">*</span></label>
                  <input type="text" name="date_from" id="date_from2" placeholder="Date Of Birth" class="form-control input-sm datepicker" style="height:34px;" required value="<?php echo @$user_data['user_dob']['object_meta_value']; ?>" >
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label" for="">Pincode (6 digit)</label><span class="text-red" >*</span></label>
                  <input type="text" class="form-control" name="pincode" id="pincode" maxlength="7" pattern="[0-9]{6}" placeholder="Pincode" required value="<?php echo @$user_data['user_pincode']['object_meta_value']; ?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label" for="">Profession</label><span class="text-red" >*</span></label>
                  <input type="text" class="form-control" name="profession" id="profession" placeholder="profession" required value="<?php echo @$user_data['user_profession']['object_meta_value']; ?>">
                </div>
              </div>
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="control-label" for="">Address</label><span class="text-red">*</span></label>
                      <textarea class="form-control" name="address" id="address" placeholder="Address" required ><?php echo @$user_data['user_shipaddress']['object_meta_value']; ?></textarea>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!---->
          <div class="col-md-6" style="border-left: 1px dashed #000;">
            <div class="row" id="recipient_address" style="display: none;">
              <div class="box-header">
               <h2 class="box-title"><b>SENDER'S INFORMATION</b></h2>
              </div> 
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Select Gender</label>
                  <label id="scb_gender-error" class="text-red hide" for="scb_gender">Please select gender.</label>
                  <span class="text-red">*</span></label>
                  <select name="scb_gender" id="scb_gender" class="form-control input-sm" onchange="get_pa_attr()" required>
                    <option value="">Select</option>
                    <option <?php if(@$user_data['user_gender']['object_meta_value'] == '1'){ echo "selected"; } ?> value="1">Female</option>
                    <option <?php if(@$user_data['user_gender']['object_meta_value'] == '2'){ echo "selected"; } ?> value="2">Male</option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label" for="">First Name</label><span class="text-red">*</span></label>
                  <input type="text" class="form-control" name="f_name" id="f_name" placeholder="First Name" required value="<?php echo @$user_info[0]['first_name']; ?>" >
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label" for="">Last Name</label><span class="text-red">*</span></label>
                  <input type="text" class="form-control" name="l_name" id="l_name" placeholder="Last Name" required value="<?php echo @$user_info[0]['last_name']; ?>" >
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label" for="">Mobile</label><span class="text-red">*</span></label>
                  <input type="text" class="form-control" name="mobile_no" id="mobile_no" placeholder="Mobile" maxlength="10" pattern="[1-9]{1}[0-9]{9}" value="<?php echo @$user_data['user_mobile']['object_meta_value']; ?>" required>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label" for="">Email</label><span class="text-red">*</span></label>
                  <input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="form-control" name="email" id="email" placeholder="Email" <?php if($form_type == 'add'){ echo 'onblur="check_email()"'; }else{ echo ' readonly="" '; } ?> value="<?php echo @$user_info[0]['email']; ?>" required>
                  <input type="hidden" id="email_exists" value="0">
                  <span class="text-red hide" id="un_email"> Email already exists</span>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label" for="">Pincode (6 digit)</label><span class="text-red" >*</span></label>
                  <input type="text" class="form-control" name="pincode" id="pincode" maxlength="7" pattern="[0-9]{6}" placeholder="Pincode" required value="<?php echo @$user_data['user_pincode']['object_meta_value']; ?>">
                </div>
              </div>
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="control-label" for="">Address</label><span class="text-red">*</span></label>
                      <textarea class="form-control" name="address" id="address" placeholder="Address" required ><?php echo @$user_data['user_shipaddress']['object_meta_value']; ?></textarea>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!---->
        </div>
      </div>
    </div>
    <div class="box">
      <div class="box-header">
        <h2 class="box-title">PA</h2>
        <label id="pa-error" class="text-red hide" for="pa">Please Select all fields.</label>
      </div>
   <?php 
		if($user_data['user_gender']['object_meta_value'] == '1')
		{
			/* echo "<pre>";
			print_r($jsonStuffWearing[0]['objApparel']);
			echo "</pre>";  */
			$jsonData 				= $jsonStuffWearing[0]['objApparel'];

			if(!empty($jsonData['apparel_stuff_dont_like']))
			{
				$dontWant 				= array_map('strtolower', $jsonData['apparel_stuff_dont_like']);
			}	
			else{
				$dontWant = '';
			}
			if(!empty($jsonData['apparel_dos_and_donts']))
			{
				$apparelDoAnddonts 		= $jsonData['apparel_dos_and_donts'];
			}
			else{
				$apparelDoAnddonts = '';
			}
			if(!empty($jsonData['apparel_dos_and_donts']))
			{
				$loveLikeDislike 		= $jsonData['apparel_dos_and_donts'];	
			}
			else{
				$loveLikeDislike = '';
			}
			if(!empty($jsonData['apparel_print_avoid']))
			{
				$avoidPrintsAndColor 	= array_map('strtolower', $jsonData['apparel_print_avoid']);	
			}
			else{
				$avoidPrintsAndColor = '';
			}
			if(!empty($jsonData['apparel_color_avoid']))
			{
				$apparelColorAvoid 		= $jsonData['apparel_color_avoid'];		
			}
			else{
				$apparelColorAvoid = '';
			}
			if(!empty(@$jsonData['apparel_top_size']))
			{
				@$apparelTopSize 		= strtolower(@$jsonData['apparel_top_size']);
			}
			else{
				$apparelTopSize = '';
			}
			if(!empty($jsonData['apparel_top_fit_like_this']))
			{
				$topFitLikeThis 		= array_map('strtolower', $jsonData['apparel_top_fit_like_this']);	
			}
			else{
				$topFitLikeThis = '';
			}
			if(!empty($jsonData['apparel_dress_size']))
			{
				$apparelDressSize 		= strtolower($jsonData['apparel_dress_size']);	
			}
			else{
				$apparelDressSize = '';
			}
			if(!empty($jsonData['apparel_dress_fit_like_this']))
			{
				$apparelDressFitLike 	= array_map('strtolower', $jsonData['apparel_dress_fit_like_this']);	
			}
			else{
				$apparelDressFitLike = '';
			}
			if(!empty($jsonData['apparel_trouser_size']))
			{
				$apparelTrouserSize 	= strtolower($jsonData['apparel_trouser_size']);	
			}
			else{
				$apparelTrouserSize = '';
			}
			if(!empty($jsonData['apparel_jeans_size']))
			{
				$apparelJeansSize 		= strtolower($jsonData['apparel_jeans_size']);		
			}
			else{
				$apparelJeansSize = '';
			}
			if(!empty($jsonData['apparel_bottom_fit_like_this']))
			{
				$apparelBottomFitLike 	= array_map('strtolower', $jsonData['apparel_bottom_fit_like_this']);
			}
			else{
				$apparelBottomFitLike = '';
			}
			if(!empty($jsonData['apparel_band_size']))
			{
				$apparelBandSize 		= $jsonData['apparel_band_size'];	
			}
			else{
				$apparelBandSize = '';
			}
			if(!empty($jsonData['apparel_cup_size']))
			{
				$apparelCupSize 		= $jsonData['apparel_cup_size'];	
			}
			else{
				$apparelCupSize = '';
			}
			if(!empty($jsonData['apparel_skirt_fit_like_this']))
			{
				$apparelSkirtFitLike 	= array_map('strtolower', $jsonData['apparel_skirt_fit_like_this']);	
			}
			else{
				$apparelSkirtFitLike = '';
			}
			
			if(!empty($jsonData['apparel_style_preference']))
			{
				$apparelStylePreference 	= $jsonData['apparel_style_preference'];
			}
			else{
				$apparelStylePreference = '';
			}

			
			// After Payment JSON Data
			/* echo "<pre>";
			print_r($paSelectionAfter);
			echo "</pre>"; */

			if(!empty($paSelectionAfter[0]['objApparel']['apparel_height_feet']))
			{
				$apparelHeightFeet = $paSelectionAfter[0]['objApparel']['apparel_height_feet'];
			}
			else
			{
				$apparelHeightFeet = '';
			}
			if(!empty($paSelectionAfter[0]['objApparel']['apparel_height_inches']))
			{
				$apparelHeightInch = $paSelectionAfter[0]['objApparel']['apparel_height_inches'];
			}
			else{
				$apparelHeightInch = '';
			}
			if(!empty($paSelectionAfter[2]['objAccessories']['accessories_sunglasses_type']))
			{
				$sunglassesType 	= array_map('strtolower', $paSelectionAfter[2]['objAccessories']['accessories_sunglasses_type']);
			}
			else{
				$sunglassesType = '';
			}
			if(!empty($paSelectionAfter[2]['objAccessories']['accessories_belt_type']))
			{
				$beltType 			= array_map('strtolower', $paSelectionAfter[2]['objAccessories']['accessories_belt_type']);
			}
			else{
				$beltType = '';
			}
			if(!empty($paSelectionAfter[2]['objAccessories']['accessories_refelector_type']))
			{
				$refelectorType 	= $paSelectionAfter[2]['objAccessories']['accessories_refelector_type']; 
			}
			else{
				$refelectorType = '';
			}
			if(!empty($paSelectionAfter[3]['objJewellery']['jewellery_tone']))
			{
				$jewelleryTone 		= $paSelectionAfter[3]['objJewellery']['jewellery_tone']; 
			}
			else{
				$jewelleryTone = '';
			}
			if(!empty($paSelectionAfter[5]['objBag']['bag_i_want']))
			{
				$bagWant 			= $paSelectionAfter[5]['objBag']['bag_i_want']; 
			}
			else{
				$bagWant = '';
			}
			if(!empty($paSelectionAfter[5]['objBag']['bag_size']))
			{
				$bagSize 			= $paSelectionAfter[5]['objBag']['bag_size']; 
			}
			else{
				$bagSize = '';
			}
			
			if(!empty($paSelectionAfter[1]['objFootwear']['footwear_heel_type']))
			{
				$footwearHeelType 			= $paSelectionAfter[1]['objFootwear']['footwear_heel_type']; 
			}
			else{
				$footwearHeelType = '';
			}

			if(!empty($paSelectionAfter[1]['objFootwear']['footwear_heel_height']))
			{
				$footwearHeelHeight 			= $paSelectionAfter[1]['objFootwear']['footwear_heel_height']; 
			}
			else{
				$footwearHeelHeight = '';
			}

			if(!empty($paSelectionAfter[4]['objBeauty']))
			{
				$beautyFinishingType 			= $paSelectionAfter[4]['objBeauty']; 
			}
			else{
				$beautyFinishingType = '';
			}

			
		}
		else if($user_data['user_gender']['object_meta_value'] == '2')
		{

			/*echo "<pre>";
			print_r($jsonStuffWearing[0]['objApparel']);
			echo "</pre>"; */
			$jsonData 					= $jsonStuffWearing[0]['objApparel'];
			if(!empty($jsonData['apparel_stuff_dont_like']))
			{
				$dontWant 					= array_map('strtolower', $jsonData['apparel_stuff_dont_like']);
			}
			else{
				$dontWant = '';
			}
			if(!empty($jsonData['apparel_tshirt_size']))
			{
				$apparelTshirtSize 			= trim(strtolower($jsonData['apparel_tshirt_size']));
			}
			else{
				$apparelTshirtSize = '';
			}
			if(!empty($jsonData['apparel_stuff_dont_like']))
			{
				$apparelTshirtFitLikeThis 	= array_map('strtolower', $jsonData['apparel_tshirt_fit_like_this']);
			}
			else{
				$apparelTshirtFitLikeThis = '';
			}
			if(!empty($jsonData['apparel_shirt_size']))
			{
				$apparelShirtSize 			= trim(strtolower($jsonData['apparel_shirt_size']), '"');
			}
			else{
				$apparelShirtSize = '';
			}
			if(!empty($jsonData['apparel_shirt_fit_like_this']))
			{
			$apparelShirtFitLikeThis 	= array_map('strtolower', $jsonData['apparel_shirt_fit_like_this']);
			}
			else{
				$apparelShirtFitLikeThis = '';
			}
			if(!empty($jsonData['apparel_trouser_size']))
			{
				$apparelTrouserSize 		= strtolower($jsonData['apparel_trouser_size']);
			}
			else{
				$apparelTrouserSize = '';
			}
			if(!empty($jsonData['apparel_trouser_fit_like_this']))
			{
				$apparelTrouserFitLikeThis 	= array_map('strtolower', $jsonData['apparel_trouser_fit_like_this']);
			}
			else{
				$apparelTrouserFitLikeThis = '';
			}
			if(!empty($jsonData['apparel_jeans_size']))
			{
				$apparelJeansSize 		  = strtolower($jsonData['apparel_jeans_size']);
			}
			else{
				$apparelTshirtSize = '';
			}
			if(!empty($jsonData['apparel_jeans_fit_like_this']))
			{
			$apparelJeansFitLikeThis 	= array_map('strtolower', $jsonData['apparel_jeans_fit_like_this']);
			}
			else{
				$apparelJeansFitLikeThis = '';
			}
			if(!empty($jsonData['apparel_dos_and_donts']))
			{
				$apparelDosAndDonts 		= $jsonData['apparel_dos_and_donts'];
			}
			else{
				$apparelDosAndDonts = '';
			}
			if(!empty($jsonData['apparel_style_preference']))
			{
				$apparelStylePreference 	= $jsonData['apparel_style_preference'];
			}
			else{
				$apparelStylePreference = '';
			}
			if(!empty($jsonData['apparel_print_avoid_men']))
			{
				$apparelPrintAvoidMen 		= array_map('strtolower', $jsonData['apparel_print_avoid_men']);
			}
			else{
				$apparelPrintAvoidMen = '';
			}
			if(!empty($jsonData['apparel_color_avoid']))
			{
				$apparelColorAvoid 			= $jsonData['apparel_color_avoid'];
			}
			else{
				$apparelColorAvoid = '';
			}

			// After Payment JSON Data
			/*echo "<pre>";
			print_r($paSelectionAfter);
			echo "</pre>";*/ 
			if(!empty($paSelectionAfter[0]['objApparel']['apparel_height_feet']))
			{
			$apparelHeightFeet = $paSelectionAfter[0]['objApparel']['apparel_height_feet'];
			}
			else{
				$apparelHeightFeet = '';
			}
			if(!empty($paSelectionAfter[0]['objApparel']['apparel_height_inches']))
			{
				$apparelHeightInch = $paSelectionAfter[0]['objApparel']['apparel_height_inches'];
			}
			else{
				$apparelHeightInch = '';
			}
			if(!empty($paSelectionAfter[2]['objAccessories']['accessories_sunglasses_type']))
			{
				$sunglassesType 	= array_map('strtolower', $paSelectionAfter[2]['objAccessories']['accessories_sunglasses_type']);
			}
			else{
				$sunglassesType = '';
			}
			if(!empty($paSelectionAfter[2]['objAccessories']['accessories_refelector_type']))
			{
				$refelectorType 	= $paSelectionAfter[2]['objAccessories']['accessories_refelector_type'];
			}
			else{
				$refelectorType = '';
			}
		}
	
	if($user_data['user_gender']['object_meta_value'] == '1')
	  {
	?>
     
	  <div class="box-body <?php if($form_type == 'edit' && @$user_data['user_gender']['object_meta_value'] != '1'){ echo "hide"; } ?>" id="women_pa">
      <div class="col-md-12 women-category-container">
            <div class="form-group">
              <label class="control-label">Categories</label><span class="text-red">*</span>
              <ul class="listingstyle">
                <li class="checkbox-inline"><input type="checkbox" name="women_category" value="apparel">apparel</li>
                <li class="checkbox-inline"><input type="checkbox" name="women_category" value="bags">bags</li>
                <li class="checkbox-inline"><input type="checkbox" name="women_category" value="footwear">footwear</li>
                <li class="checkbox-inline"><input type="checkbox" name="women_category" value="accessories">accessories</li>
                <li class="checkbox-inline"><input type="checkbox" name="women_category" value="jewellery">jewellery</li>
                <li class="checkbox-inline"><input type="checkbox" name="women_category" value="beauty">beauty</li>
              </ul>
            </div>
          </div>
        <div class="women-apparel-container" id="pa_attr"> 
          <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">THIS IS WHAT MY BODY LOOKS LIKE</label><span class="text-red">*</span>
                    <select name="user_body_shape_women" id="user_body_shape" class="form-control input-sm">
                      <option value="hourglass" <?php echo (@$jsonData['pa_bodshape'] == "hourglass") ? 'selected' : ''; ?>>Hourglass</option>
                      <option value="apple" <?php echo (@$jsonData['pa_bodshape'] == 'apple') ? 'selected' : ''; ?>>Apple</option>
                      <option value="inverted triangle"<?php echo (@$jsonData['pa_bodshape'] == "inverted triangle") ? 'selected' : ''; ?>>Inverted Triangle</option>
                      <option value="pear" <?php echo (@$jsonData['pa_bodshape'] == "pear") ? 'selected' : ''; ?>>Pear</option>
                      <option value="rectangle" <?php echo (@$jsonData['pa_bodshape'] == "rectangle") ? 'selected' : ''; ?>>Rectangle</option>
                      <option value="goblet" <?php echo (@$jsonData['pa_bodshape'] == "goblet") ? 'selected' : ''; ?>>Goblet</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-3">
                  <label class="control-label">Height</label><span class="text-red">*</span>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <select name="height-height_feet" id="height-height_feet" class="form-control hwidth">
                          <option value="">--</option>
                          <option value="3 ft" <?php echo (@$apparelHeightFeet == "3 ft")?"selected":""; ?>>3 ft</option>
                          <option value="4 ft" <?php echo (@$apparelHeightFeet == "4 ft")?"selected":""; ?>>4 ft</option>
                          <option value="5 ft" <?php echo (@$apparelHeightFeet == "5 ft")?"selected":""; ?>>5 ft</option>
                          <option value="6 ft" <?php echo (@$apparelHeightFeet == "6 ft")?"selected":""; ?>>6 ft</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <select name="height-height_inches" id="height-height_inches" class="form-control hwidth">
                          <option value="">--</option>
                          <option value="0 inch" <?php echo (@$apparelHeightInch == "1 inch")?"selected":""; ?>>0 in</option>
                          <option value="1 inch" <?php echo (@$apparelHeightInch == "2 inch")?"selected":""; ?>>1 in</option>
                          <option value="2 inch" <?php echo (@$apparelHeightInch == "3 inch")?"selected":""; ?>>2 in</option>
                          <option value="3 inch" <?php echo (@$apparelHeightInch == "4 inch")?"selected":""; ?>>3 in</option>
                          <option value="4 inch" <?php echo (@$apparelHeightInch == "5 inch")?"selected":""; ?>>4 in</option>
                          <option value="5 inch" <?php echo (@$apparelHeightInch == "6 inch")?"selected":""; ?>>5 in</option>
                          <option value="6 inch" <?php echo (@$apparelHeightInch == "7 inch")?"selected":""; ?>>6 in</option>
                          <option value="7 inch" <?php echo (@$apparelHeightInch == "8 inch")?"selected":""; ?>>7 in</option>
                          <option value="8 inch" <?php echo (@$apparelHeightInch == "9 inch")?"selected":""; ?>>8 in</option>
                          <option value="9 inch" <?php echo (@$apparelHeightInch == "10 inch")?"selected":""; ?>>9 in</option>
                          <option value="10 inch" <?php echo (@$apparelHeightInch == "11 inch")?"selected":""; ?>>10 in</option>
                          <option value="11 inch" <?php echo (@$apparelHeightInch == "12 inch")?"selected":""; ?>>11 in</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="box">
            <div class="box-header">
              <h2 class="box-title">Choices</h2>
            </div>
            <div class="box-body">
              <div class="row">
			  <?php
				/* echo "<pre>";
				print_r($dontWant);
				echo "</pre>"; */
			  ?>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label">What i don't want</label><span class="text-red">*</span>                  
           <ul class="listingstyle">
                      <li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="tops" <?php if(in_array('tops', @$dontWant)){echo "checked";}?>>tops</li>
                      <li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="dresses" <?php if(in_array('dresses', @$dontWant)){echo "checked";}?>>dresses</li>
                      <li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="shorts and skirts" <?php if(in_array('shorts and skirts', @$dontWant)){echo "checked";}?>>shorts and skirts</li>
                      <li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="jeans and trousers"  <?php if(in_array('jeans and trousers', @$dontWant)){echo "checked";}?>>jeans and trousers</li>
                      <li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="jumpsuits" <?php if(in_array('what_dont_want', @$dontWant)){echo "checked";}?>>jumpsuits </li>
                      <li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="outerwear" <?php if(in_array('outerwear', @$dontWant)){echo "checked";}?>>outerwear</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="row">
			  <?php 
				/* echo "<pre>";
				print_r($apparelTopSize);
				echo "</pre>";  */
			  
			  ?>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">Top Size</label><span class="text-red">*</span>
                    <select class="form-control hwidth" id="scbox_sizetop" name="scbox_sizetop">
                      <option value=""></option>
                      <option value="xs/uk-4/us-2/eu-32" <?php echo ($apparelTopSize == "xs/uk 4/us 2/eu 32") ? 'selected':''; ?>>xs/uk 4/us 2/eu 32</option>
                      <option value="s/uk-6/us-4/eu-34" <?php echo ($apparelTopSize == "s/uk-6/us-4/eu-34") ? 'selected':''; ?>>s/uk 6/us 4/eu 34</option>
                      <option value="s/uk-8/us-6/eu-36" <?php echo ($apparelTopSize == "s/uk-8/us-6/eu-36") ? 'selected':''; ?>>s/uk 8/us 6/eu 36</option>
                      <option value="m/uk-10/us-8/eu-38" <?php echo ($apparelTopSize == "M/UK 10 /US 8/EU 38") ? 'selected':''; ?>>m/uk 10 /us 8/eu 38</option>
                      <option value="m/uk-12/us-10/eu-40" <?php echo ($apparelTopSize == "m/uk-12/us-10/eu-40") ? 'selected':''; ?>>m/uk 12/us 10/eu 40</option>
                      <option value="l/uk-14/us-12/eu-42" <?php echo ($apparelTopSize == "l/uk-14/us-12/eu-42") ? 'selected':''; ?>>l/uk 14/us 12/eu 42</option>
                      <option value="l/uk-16/us-14/eu-44" <?php echo ($apparelTopSize == "l/uk-16/us-14/eu-44") ? 'selected':''; ?>>l/uk 16/us 14/eu 44</option>
                      <option value="xl/uk-18/us-16/eu-46" <?php echo ($apparelTopSize == "xl/uk 18/us 16/eu 46") ? 'selected':''; ?>>xl/uk 18/us 16/eu 46</option>
                      <option value="xxl/uk-20/us-18/eu-48" <?php echo ($apparelTopSize == "xs/uk-4/us-2/eu-32") ? 'selected':''; ?>>xxl/uk 20/us 18/eu 48</option>
                      <option value="xxxl/uk-22/us-20/eu-50" <?php echo ($apparelTopSize == "xxxl/uk-22/us-20/eu-50") ? 'selected':''; ?>>xxxl/uk 22/us 20/eu 50</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-9">
				<?php 
					/* echo "<pre>";
					print_r($topFitLikeThis);
					echo "</pre>";  */
				?>
                  <div class="form-group">
                    <label class="control-label">Top Fit</label><span class="text-red">*</span>
                    <div>
                      <label class="radio-inline"><input type="checkbox" name="top-fit" value="fitted" <?php if(in_array('fitted', @$topFitLikeThis)){echo "checked" ; }?> >Fitted</label>
                      <label class="radio-inline"><input type="checkbox" name="top-fit" value="relaxed" <?php if(in_array('relaxed', @$topFitLikeThis)){echo "checked" ; }?> >Relaxed</label>
                      <label class="radio-inline"><input type="checkbox" name="top-fit" value="flowy" <?php if(in_array('flowy', @$topFitLikeThis)){echo "checked" ; }?> >Flowy</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
			  <?php
				/* echo "<pre>";
				print_r($apparelDressSize);
				echo "</pre>"; */
			  ?>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">Dress Size</label><span class="text-red">*</span>
                    <select class="form-control hwidth" id="dress_size" name="dress_size">
                      <option value=""></option>
                      <option value="xs/uk 4/us 2/eu 32" <?php echo ($apparelDressSize == "xs/uk 4/us 2/eu 32")?'selected':'';?>>xs/uk 4/us 2/eu 32</option>
                      <option value="s/uk 6/us 4/eu 34" <?php echo ($apparelDressSize == "s/uk 6/us 4/eu 34")?'selected':'';?>>s/uk 6/us 4/eu 34</option>
                      <option value="s/uk 8/us 6/eu 36" <?php echo ($apparelDressSize == "s/uk 8/us 6/eu 36")?'selected':'';?>>s/uk 8/us 6/eu 36</option>
                      <option value="m/uk 10 /us 8/eu 38" <?php echo ($apparelDressSize == "m/uk 10 /us 8/eu 38")?'selected':'';?>>m/uk 10 /us 8/eu 38</option>
                      <option value="m/uk 12/us 10/eu 40" <?php echo ($apparelDressSize == "m/uk 12/us 10/eu 40")?'selected':'';?>>m/uk 12/us 10/eu 40</option>
                      <option value="l/uk 14/us 12/eu 42" <?php echo ($apparelDressSize == "l/uk 14/us 12/eu 42")?'selected':'';?>>l/uk 14/us 12/eu 42</option>
                      <option value="l/uk 16/us 14/eu 44" <?php echo ($apparelDressSize == "l/uk 16/us 14/eu 44")?'selected':'';?>>l/uk 16/us 14/eu 44</option>
                      <option value="xl/uk 18/us 16/eu 46" <?php echo ($apparelDressSize == "xl/uk 18/us 16/eu 46")?'selected':'';?>>xl/uk 18/us 16/eu 46</option>
                      <option value="xxl/uk 20/us 18/eu 484" <?php echo ($apparelDressSize == "xxl/uk 20/us 18/eu 48")?'selected':'';?>>xxl/uk 20/us 18/eu 48</option>
                      <option value="xxxl/uk 22/us 20/eu 50" <?php echo ($apparelDressSize == "xxxl/uk 22/us 20/eu 50")?'selected':'';?>>xxxl/uk 22/us 20/eu 50</option>
                    </select>
                  </div>
                </div>
				  <?php
					/* echo "<pre>";
					print_r($apparelDressFitLike);
					echo "</pre>"; */
				  ?>
                <div class="col-md-9">
                  <div class="form-group">
                    <label class="control-label">Dress Fit</label><span class="text-red">*</span>
                    <div>
                      <label class="radio-inline"><input type="checkbox" name="dress-fit"  value="bodycon"  <?php if(in_array('bodycon', @$apparelDressFitLike)){echo "checked" ; }?>>BODYCON</label>
                      <label class="radio-inline"><input type="checkbox" name="dress-fit"  value="shift"  <?php if(in_array('shift', @$apparelDressFitLike)){echo "checked" ; }?>>SHIFT</label>
                      <label class="radio-inline"><input type="checkbox" name="dress-fit"  value="a line"  <?php if(in_array('a line', @$apparelDressFitLike)){echo "checked" ; }?>>A LINE</label>
                      <label class="radio-inline"><input type="checkbox" name="dress-fit"  value="skater"  <?php if(in_array('skater', @$apparelDressFitLike)){echo "checked" ; }?>>SKATER</label>
                      <label class="radio-inline"><input type="checkbox" name="dress-fit"  value="flowy"  <?php if(in_array('flowy', @$apparelDressFitLike)){echo "checked" ; }?>>FLOWY</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">Trouser Size</label><span class="text-red">*</span>
                    <select class="form-control hwidth" id="scbox_sizetrouser" name="scbox_sizetrouser">
                      <option value=""></option>
                      <option value="xs/25" <?php echo ($apparelTrouserSize == 'xs/25')?'selected':'';?>>xs/25 </option>
                      <option value="s/26" <?php echo ($apparelTrouserSize == 's/26')?'selected':'';?>>s/26</option>
                      <option value="m/28" <?php echo ($apparelTrouserSize == 'm/28')?'selected':'';?>>m/28</option>
                      <option value="l/30" <?php echo ($apparelTrouserSize == 'l/30')?'selected':'';?>>l/30</option>
                      <option value="xl/32" <?php echo ($apparelTrouserSize == 'xl/32')?'selected':'';?>>xl/32</option>
                      <option value="xxl/34" <?php echo ($apparelTrouserSize == 'xxl/34')?'selected':'';?>>xxl/34</option>
                      <option value="xxxl/36" <?php echo ($apparelTrouserSize == 'xxxl/36')?'selected':'';?>>xxxl/36</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-9">
				 <?php
					/* echo "<pre>";
					print_r($apparelSkirtFitLike);
					echo "</pre>";  */
				  ?>
                  <div class="form-group">
                    <label class="control-label">I LIKE THE LENGTH OF MY SKIRTS AND DRESSES TO BE LIKE THIS</label><span class="text-red">*</span>
                    <div>
                      <label class="radio-inline"><input type="checkbox" name="trouser_fit"  <?php if(!empty($apparelSkirtFitLike)){if(in_array('mini', @$apparelSkirtFitLike)){echo "checked" ; }}?> value="mini">MINI</label>
                      <label class="radio-inline"><input type="checkbox" name="trouser_fit" <?php if(!empty($apparelSkirtFitLike)){if(in_array('above the knee', @$apparelSkirtFitLike)){echo "checked" ; }}?> value="above the knee">ABOVE THE KNEE</label>
                      <label class="radio-inline"><input type="checkbox" name="trouser_fit" <?php if(!empty($apparelSkirtFitLike)){if(in_array('below the knee', @$apparelSkirtFitLike)){echo "checked" ; }}?> value="below the knee">BELOW THE KNEE</label>
                      <label class="radio-inline"><input type="checkbox" name="trouser_fit" <?php if(!empty($apparelSkirtFitLike)){if(in_array('midi', @$apparelSkirtFitLike)){echo "checked" ; }}?> value="midi">MIDI</label>
						<label class="radio-inline"><input type="checkbox" name="trouser_fit" <?php if(!empty($apparelSkirtFitLike)){if(in_array('maxi', @$apparelSkirtFitLike)){echo "checked" ; }}?> value="maxi">MAXI</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
				<?php
					/*  echo "<pre>";
					print_r($apparelJeansSize);
					echo "</pre>";  */
				?>
                  <div class="form-group">
                    <label class="control-label">Jeans Size</label><span class="text-red">*</span>
                    <select class="form-control hwidth" id="scbox_denim_size" name="scbox_denim_size">
                      <option value=""></option>
                      <option value="xs/ 25 " <?php echo ($apparelJeansSize == 'xs/ 25 ')?'selected':'';?>>xs/ 25 </option>
                      <option value="s/26" <?php echo ($apparelJeansSize == 's/26')?'selected':'';?>>s/26</option>
                      <option value="m/28" <?php echo ($apparelJeansSize == 'm/28')?'selected':'';?>>m/28</option>
                      <option value="l/30" <?php echo ($apparelJeansSize == 'l/30')?'selected':'';?>>l/30</option>
                      <option value="xl/32" <?php echo ($apparelJeansSize == 'xl/32')?'selected':'';?>>xl/32</option>
                      <option value="xxl/34" <?php echo ($apparelJeansSize == 'xxl/34')?'selected':'';?>>xxl/34</option>
                      <option value="xxxl/36" <?php echo ($apparelJeansSize == 'xxxl/36')?'selected':'';?>>xxxl/36</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-9">
				<?php
					/* echo "<pre>";
					print_r($apparelBottomFitLike);
					echo "</pre>"; */
				?>
                  <div class="form-group">
                    <label class="control-label">Bottoms Fit</label><span class="text-red">*</span>
                    <div>
                      <label class="radio-inline"><input type="checkbox" name="bottom-fit" value="slim" <?php if(!empty($apparelBottomFitLike)){echo (in_array('slim', @$apparelBottomFitLike))?'checked':'';}?>>Slim</label>
                      <label class="radio-inline"><input type="checkbox" name="bottom-fit" value="straight" <?php if(!empty($apparelBottomFitLike)){echo (in_array('straight', @$apparelBottomFitLike))?'checked':'';}?>>STRAIGHT</label>
                      <label class="radio-inline"><input type="checkbox" name="bottom-fit" value="skinny" <?php if(!empty($apparelBottomFitLike)){echo (in_array('skinny', @$apparelBottomFitLike))?'checked':'';}?> >Skinny</label>
                      <label class="radio-inline"><input type="checkbox" name="bottom-fit" value="flared" <?php if(!empty($apparelBottomFitLike)){echo (in_array('flared', @$apparelBottomFitLike))?'checked':'';}?>>Flared</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label"> Lingerie:  Band Size </label><span class="text-red">*</span>
                    <select name="user_sizeband_women" id="user_sizeband" class="form-control input-sm">
                      <option value=""></option>
                      <option value="30" <?php echo ($apparelBandSize=='30')?'selected':'';?>>30</option>
                      <option value="32" <?php echo ($apparelBandSize=='32')?'selected':'';?>>32</option>
                      <option value="34" <?php echo ($apparelBandSize=='34')?'selected':'';?>>34</option>
                      <option value="36" <?php echo ($apparelBandSize=='36')?'selected':'';?>>36</option>
                      <option value="38" <?php echo ($apparelBandSize=='38')?'selected':'';?>>38</option>
                      <option value="40" <?php echo ($apparelBandSize=='40')?'selected':'';?>>40</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">Cup Size </label><span class="text-red">*</span>
                    <select name="user_sizecup_women" id="user_sizecup" class="form-control input-sm">
                      <option value=""></option>
                      <option value="a" <?php echo ($apparelCupSize=='a')?'selected':'';?>>A</option>
                      <option value="b" <?php echo ($apparelCupSize=='b')?'selected':'';?>>B</option>
                      <option value="c" <?php echo ($apparelCupSize=='c')?'selected':'';?>>C</option>
                      <option value="d" <?php echo ($apparelCupSize=='d')?'selected':'';?>>D</option>
                      <option value="dd" <?php echo ($apparelCupSize=='dd')?'selected':'';?>>DD</option>
                      <option value="e" <?php echo ($apparelCupSize=='e')?'selected':'';?>>E</option>
                      <option value="f" <?php echo ($apparelCupSize=='f')?'selected':'';?>>F</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
               <div class="box">
            <div class="box-header">
              <h2 class="box-title">STUFF I LIKE WEARING </h2>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">sleeveless</label>
                    <div>
                      <label class="radio-inline"><input type="radio" name="sleeveless" value="yes" <?php if(@$apparelDoAnddonts[0][1] == 'yes'){echo "checked";}?>>Yes</label>
                      <label class="radio-inline"><input type="radio" name="sleeveless" value="no" <?php if(@$apparelDoAnddonts[0][1] == 'no'){echo "checked";}?>>No</label>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">halter</label>
                    <div>
                      <label class="radio-inline"><input type="radio" name="halter" value="yes" <?php if(@$apparelDoAnddonts[1][1] == 'yes'){echo "checked";}?>>Yes</label>
                      <label class="radio-inline"><input type="radio" name="halter" value="no" <?php if(@$apparelDoAnddonts[1][1] == 'no'){echo "checked";}?>>No</label>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">off shoulder</label>
                    <div>
                      <label class="radio-inline"><input type="radio" name="off_shoulder" value="yes" <?php if(@$apparelDoAnddonts[2][1] == 'yes'){echo "checked";}?>>Yes</label>
                      <label class="radio-inline"><input type="radio" name="off_shoulder" value="no" <?php if(@$apparelDoAnddonts[2][1] == 'no'){echo "checked";}?>>No</label>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">strappy</label>
                    <div>
                      <label class="radio-inline"><input type="radio" name="strappy" value="yes" <?php if(@$apparelDoAnddonts[3][1] == 'yes'){echo "checked";}?>>Yes</label>
                      <label class="radio-inline"><input type="radio" name="strappy" value="no" <?php if(@$apparelDoAnddonts[3][1] == 'no'){echo "checked";}?>>No</label>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">tube</label>
                    <div>
                      <label class="radio-inline"><input type="radio" name="tube" value="yes" <?php if(@$apparelDoAnddonts[4][1] == 'yes'){echo "checked";}?>>Yes</label>
                      <label class="radio-inline"><input type="radio" name="tube" value="no" <?php if(@$apparelDoAnddonts[4][1] == 'no'){echo "checked";}?>>No</label>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">crop top</label>
                    <div>
                      <label class="radio-inline"><input type="radio" name="crop_top" value="yes" <?php if(@$apparelDoAnddonts[5][1] == 'yes'){echo "checked";}?>>Yes</label>
                      <label class="radio-inline"><input type="radio" name="crop_top" value="no" <?php if(@$apparelDoAnddonts[5][1] == 'no'){echo "checked";}?>>No</label>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">Deep Neck</label>
                    <div>
                      <label class="radio-inline"><input type="radio" name="deep_neck" value="yes" <?php if(@$apparelDoAnddonts[7][1] == 'yes'){echo "checked";}?>>Yes</label>
                      <label class="radio-inline"><input type="radio" name="deep_neck" value="no" <?php if(@$apparelDoAnddonts[7][1] == 'no'){echo "checked";}?>>No</label>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">backless</label>
                    <div>
                      <label class="radio-inline"><input type="radio" name="backless" value="yes" <?php if(@$apparelDoAnddonts[6][1] == 'yes'){echo "checked";}?>>Yes</label>
                      <label class="radio-inline"><input type="radio" name="backless" value="no" <?php if(@$apparelDoAnddonts[6][1] == 'no'){echo "checked";}?>>No</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="box">
            <div class="box-header">
              <h2 class="box-title">MY STYLE PREFERENCE ( LOVE/LIKE/DISLIKE) </h2>
            </div>
            <div class="box-body">
			<?php
			/* 	echo "<pre>";
				print_r($apparelStylePreference);
				echo "</pre>"; */
			?>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">          
                    <label class="control-label">Classic</label>
                    <div>
                      <label class="radio-inline"><input type="radio" name="classic" value="love" <?php if(@$apparelStylePreference[0][1] == 'love'){echo "checked";}?>>LOVE</label>
                      <label class="radio-inline"><input type="radio" name="classic" value="like" <?php if(@$apparelStylePreference[0][1] == 'like'){echo "checked";}?>>LIKE</label>
                      <label class="radio-inline"><input type="radio" name="classic" value="dislike" <?php if(@$apparelStylePreference[0][1] == 'dislike'){echo "checked";}?>>DISLIKE</label>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">Free Spirit</label>
                    <div>
                      <label class="radio-inline"><input type="radio" name="free_spirit" value="love" <?php if(@$apparelStylePreference[1][1] == 'love'){echo "checked";}?>>LOVE</label>
                      <label class="radio-inline"><input type="radio" value="like" <?php if(@$apparelStylePreference[1][1] == 'like'){echo "checked";}?>>LIKE</label>
                      <label class="radio-inline"><input type="radio" value="dislike" <?php if(@$apparelStylePreference[1][1] == 'dislike'){echo "checked";}?>>DISLIKE</label>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">Bombshell</label>
                    <div>
                      <label class="radio-inline"><input type="radio" name="bombshell" value="love" <?php if(@$apparelStylePreference[3][1] == 'love'){echo "checked";}?>>LOVE</label>
                      <label class="radio-inline"><input type="radio" name="bombshell" value="like" <?php if(@$apparelStylePreference[3][1] == 'like'){echo "checked";}?>>LIKE</label>
                      <label class="radio-inline"><input type="radio" name="bombshell" value="dislike" <?php if(@$apparelStylePreference[3][1] == 'dislike'){echo "checked";}?>>DISLIKE</label>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">Bold & Edgy</label>
                    <div>
                      <label class="radio-inline"><input type="radio" name="rockstar" value="love" <?php if(@$apparelStylePreference[4][1] == 'love'){echo "checked";}?>>LOVE</label>
                      <label class="radio-inline"><input type="radio" name="rockstar"value="like" <?php if(@$apparelStylePreference[4][1] == 'like'){echo "checked";}?>>LIKE</label>
                      <label class="radio-inline"><input type="radio" name="rockstar"value="dislike" <?php if(@$apparelStylePreference[4][1] == 'dislike'){echo "checked";}?>>DISLIKE</label>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">Atheleisure</label>
                    <div>
                      <label class="radio-inline"><input type="radio" name="atheleisure" value="love" <?php if(@$apparelStylePreference[5][1] == 'love'){echo "checked";}?>>LOVE</label>
                      <label class="radio-inline"><input type="radio" name="atheleisure" value="like" <?php if(@$apparelStylePreference[5][1] == 'like'){echo "checked";}?>>LIKE</label>
                      <label class="radio-inline"><input type="radio" name="atheleisure" value="dislike" <?php if(@$apparelStylePreference[5][1] == 'dislike'){echo "checked";}?>>DISLIKE</label>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">Romantic</label>
                    <div>
                      <label class="radio-inline"><input type="radio" name="romantic" value="love" <?php if(@$apparelStylePreference[1][1] == 'love'){echo "checked";}?>>LOVE</label>
                      <label class="radio-inline"><input type="radio" name="romantic" value="like" <?php if(@$apparelStylePreference[1][1] == 'like'){echo "checked";}?>>LIKE</label>
                      <label class="radio-inline"><input type="radio" name="romantic" value="dislike" <?php if(@$apparelStylePreference[1][1] == 'dislike'){echo "checked";}?>>DISLIKE</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="box">
            <div class="box-header">
              <h2 class="box-title">Prints and Colors to avoid </h2>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label">Prints to avoid</label><span class="text-red">*</span>
                    <ul class="listingstyle">
                      <li class="checkbox-inline"><input type="checkbox" name="prints_avoid" value="checks" <?php if(in_array('checks', @$avoidPrintsAndColor)){echo "checked";}?>>checks</li>
                      <li class="checkbox-inline"><input type="checkbox" name="prints_avoid" value="floral" <?php if(in_array('floral', @$avoidPrintsAndColor)){echo "checked";}?>>floral</li>
                      <li class="checkbox-inline"><input type="checkbox" name="prints_avoid" value="stripes" <?php if(in_array('stripes', @$avoidPrintsAndColor)){echo "checked";}?>>stripes</li>
                      <li class="checkbox-inline"><input type="checkbox" name="prints_avoid" value="polka dots " <?php if(in_array('polka dots', @$avoidPrintsAndColor)){echo "checked";}?>>polka dots  </li>
                      <li class="checkbox-inline"><input type="checkbox" name="prints_avoid" value="animal_print/motifs" <?php if(in_array('animal', @$avoidPrintsAndColor)){echo "checked";}?>>animal print /motifs</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label">Colors to avoid</label><span class="text-red">*</span>
                    <ul class="listingstyle">

					 <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php if(in_array('#90140c', @$apparelColorAvoid)){echo "checked";}?> value="#90140c">maroon</li>
					<li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php if(in_array('#b8bebe', @$apparelColorAvoid )){echo "checked";}?> value="#b8bebe">grey</li>		 
                      <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php if(in_array('#87ceeb', @$apparelColorAvoid)){echo "checked";}?> value="#87ceeb">sky blue</li>
                      <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" value="#041531" <?php if(in_array('#041531', @$apparelColorAvoid)){echo "checked";}?>>navy blue</li>
                      <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php if(in_array('#87ceeb', @$apparelColorAvoid)){echo "checked";}?> value="#682860">purple</li>
                      <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php if(in_array('#0b0706', @$apparelColorAvoid)){echo "checked";}?> value="#0b0706">black</li>
                      <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php if(in_array('#ffffff', @$apparelColorAvoid)){echo "checked";}?> value="#ffffff">white</li>
                      <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php if(in_array('#f7b719', @$apparelColorAvoid)){echo "checked";}?> value="#f7b719">mustard</li>
                      <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php if(in_array('#fec4c3', @$apparelColorAvoid)){echo "checked";}?> value="#fec4c3">blush pink</li>
                      <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" value="#ff0080" <?php if(in_array('#ff0080', @$apparelColorAvoid)){echo "checked";}?>>fuschia</li>
					  <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" value="#98012e" <?php if(in_array('#98012e', @$apparelColorAvoid)){echo "checked";}?>>berry</li>
                      <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" value="#576331" <?php if(in_array('#576331', @$apparelColorAvoid)){echo "checked";}?>>olive</li>
                      <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php if(in_array('#bcd2ee', @$apparelColorAvoid)){echo "checked";}?> value="#bcd2ee">powder blue</li>
                      <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php if(in_array('#734021', @$apparelColorAvoid)){echo "checked";}?> value="#734021" >brown</li>
                      <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" value="#d5011a" <?php if(in_array('#d5011a', @$apparelColorAvoid)){echo "checked";}?>>red</li>
                      <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php if(in_array('#b3914a', @$apparelColorAvoid)){echo "checked";}?> value="#b3914a">gold</li>
                      <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php if(in_array('#cdd1d1', @$apparelColorAvoid)){echo "checked";}?> value="#cdd1d1">silver</li>
                      <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" value="#f57d31" <?php if(in_array('#f57d31', @$apparelColorAvoid)){echo "checked";}?>>tangerine</li>
                     

                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="women-bag-container">  
          <div class="box">

            <div class="box-header">
              <h2 class="box-title">Choices</h2>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label">What i don't want</label><span class="text-red">*</span>                  
				   <ul class="listingstyle">
                      <li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="tops" <?php if(in_array("tops", @$dontWant)){echo "checked";}?>>tops</li>
                      <li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="dresses" <?php if(in_array("dresses", @$dontWant)){echo "checked";}?>>dresses</li>
                      <li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="shorts and skirts" <?php if(in_array("shorts and skirts", @$dontWant)){echo "checked";}?>>shorts and skirts</li>
                      <li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="jeans and trousers" <?php if(in_array("jeans and trousers", @$dontWant)){echo "checked";}?>>jeans and trousers</li>
                      <li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="jumpsuits" <?php if(in_array("jumpsuits", @$dontWant)){echo "checked";}?>>jumpsuits </li>
                      <li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="outerwear" <?php if(in_array("outerwear", @$dontWant)){echo "checked";}?>>outerwear</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">Top Size</label><span class="text-red">*</span>
                    <select class="form-control hwidth" id="scbox_sizetop" name="scbox_sizetop">
                      <option value=""></option>
                      <option value="xs/uk-4/us-2/eu-32" <?php echo (@$apparelTopSize == "xs/uk-4/us-2/eu-32") ? 'selected':''; ?>>xs/uk 4/us 2/eu 32</option>
                      <option value="s/uk-6/us-4/eu-34" <?php echo (@$apparelTopSize == "s/uk-6/us-4/eu-34") ? 'selected':''; ?>>s/uk 6/us 4/eu 34</option>
                      <option value="s/uk-8/us-6/eu-36" <?php echo (@$apparelTopSize == "s/uk-8/us-6/eu-36") ? 'selected':''; ?>>s/uk 8/us 6/eu 36</option>
                      <option value="m/uk-10/us-8/eu-38" <?php echo (@$apparelTopSize == "M/UK 10 /US 8/EU 38") ? 'selected':''; ?>>m/uk 10 /us 8/eu 38</option>
                      <option value="m/uk-12/us-10/eu-40" <?php echo (@$apparelTopSize == "m/uk-12/us-10/eu-40") ? 'selected':''; ?>>m/uk 12/us 10/eu 40</option>
                      <option value="l/uk-14/us-12/eu-42" <?php echo (@$apparelTopSize == "l/uk-14/us-12/eu-42") ? 'selected':''; ?>>l/uk 14/us 12/eu 42</option>
                      <option value="l/uk-16/us-14/eu-44" <?php echo (@$apparelTopSize == "l/uk-16/us-14/eu-44") ? 'selected':''; ?>>l/uk 16/us 14/eu 44</option>
                      <option value="xl/uk-18/us-16/eu-46" <?php echo (@$apparelTopSize == "xl/uk 18/us 16/eu 46") ? 'selected':''; ?>>xl/uk 18/us 16/eu 46</option>
                      <option value="xxl/uk-20/us-18/eu-48" <?php echo (@$apparelTopSize == "xs/uk-4/us-2/eu-32") ? 'selected':''; ?>>xxl/uk 20/us 18/eu 48</option>
                      <option value="xxxl/uk-22/us-20/eu-50" <?php echo (@$apparelTopSize == "xxxl/uk-22/us-20/eu-50") ? 'selected':''; ?>>xxxl/uk 22/us 20/eu 50</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-9">
                  <div class="form-group">
                    <label class="control-label">Top Fit</label><span class="text-red">*</span>
                    <div>
                      <label class="radio-inline"><input type="radio" name="top-fit" value="fitted" <?php if(in_array('fitted', @$topFitLikeThis)){echo "checked";}?>>Fitted</label>
                      <label class="radio-inline"><input type="radio" name="top-fit" value="relaxed" <?php if(in_array('relaxed', @$topFitLikeThis)){echo "checked";}?>>Relaxed</label>
                      <label class="radio-inline"><input type="radio" name="top-fit" value="flowy" <?php if(in_array('flowy', @$topFitLikeThis)){echo "checked";}?>>Flowy</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">Dress Size</label><span class="text-red">*</span>
                    <select class="form-control hwidth" id="dress_size" name="dress_size">
                      <option value=""></option>
                      <option value="xs/uk 4/us 2/eu 32" <?php echo (@$apparelDressSize == "xs/uk 4/us 2/eu 32")?'selected':'';?>>xs/uk 4/us 2/eu 32</option>
                      <option value="s/uk 6/us 4/eu 34" <?php echo (@$apparelDressSize == "s/uk 6/us 4/eu 34")?'selected':'';?>>s/uk 6/us 4/eu 34</option>
                      <option value="s/uk 8/us 6/eu 36" <?php echo (@$apparelDressSize == "s/uk 8/us 6/eu 36")?'selected':'';?>>s/uk 8/us 6/eu 36</option>
                      <option value="m/uk 10/us 8/eu 38" <?php echo (@$apparelDressSize == "m/uk 10/us 8/eu 38")?'selected':'';?>>m/uk 10 /us 8/eu 38</option>
                      <option value="m/uk 12/us 10/eu 40" <?php echo (@$apparelDressSize == "m/uk 12/us 10/eu 40")?'selected':'';?>>m/uk 12/us 10/eu 40</option>
                      <option value="l/uk 14/us 12/eu 42" <?php echo (@$apparelDressSize == "l/uk 14/us 12/eu 42")?'selected':'';?>>l/uk 14/us 12/eu 42</option>
                      <option value="l/uk 16/us 14/eu 44" <?php echo (@$apparelDressSize == "l/uk 16/us 14/eu 44")?'selected':'';?>>l/uk 16/us 14/eu 44</option>
                      <option value="xl/uk 18/us 16/eu 46" <?php echo (@$apparelDressSize == "xl/uk 18/us 16/eu 46")?'selected':'';?>>xl/uk 18/us 16/eu 46</option>
                      <option value="xxl/uk 20/us 18/eu 48" <?php echo (@$apparelDressSize == "xxl/uk 20/us 18/eu 48")?'selected':'';?>>xxl/uk 20/us 18/eu 48</option>
                      <option value="xxxl/uk 22/us 20/eu 50" <?php echo (@$apparelDressSize == "xxxl/uk 22/us 20/eu 50")?'selected':'';?>>xxxl/uk 22/us 20/eu 50</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-9">
                  <div class="form-group">
                    <label class="control-label">Dress Fit</label><span class="text-red">*</span>
                    <div>
                      <label class="radio-inline"><input type="radio" name="dress-fit"  value="bodycon" <?php if(in_array('bodycon', @$apparelDressFitLike)){echo "checked";} ?>>BODYCON</label>
                      <label class="radio-inline"><input type="radio" name="dress-fit"  value="shift" <?php if(in_array('shift', @$apparelDressFitLike)){echo "checked";} ?>>SHIFT</label>
                      <label class="radio-inline"><input type="radio" name="dress-fit"  value="a line" <?php if(in_array('a line', @$apparelDressFitLike)){echo "checked";} ?>>A LINE</label>
                      <label class="radio-inline"><input type="radio" name="dress-fit"  value="skater" <?php if(in_array('skater', @$apparelDressFitLike)){echo "checked";} ?>>SKATER</label>
                      <label class="radio-inline"><input type="radio" name="dress-fit"  value="flowy" <?php if(in_array('flowy', @$apparelDressFitLike)){echo "checked";} ?>>FLOWY</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">Trouser Size</label><span class="text-red">*</span>
                    <select class="form-control hwidth" id="scbox_sizetrouser" name="scbox_sizetrouser">
                      <option value=""></option>
                      <option value="xs/25" <?php echo (@$apparelTrouserSize == 'xs/25')?'selected':'';?>>xs/25 </option>
                      <option value="s/26" <?php echo (@$apparelTrouserSize == 's/26')?'selected':'';?>>s/26</option>
                      <option value="m/28" <?php echo (@$apparelTrouserSize == 'm/28')?'selected':'';?>>m/28</option>
                      <option value="l/30" <?php echo (@$apparelTrouserSize == 'l/30')?'selected':'';?>>l/30</option>
                      <option value="xl/32" <?php echo (@$apparelTrouserSize == 'xl/32')?'selected':'';?>>xl/32</option>
                      <option value="xxl/34" <?php echo (@$apparelTrouserSize == 'xxl/34')?'selected':'';?>>xxl/34</option>
                      <option value="xxxl/36" <?php echo (@$apparelTrouserSize == 'xxxl/36')?'selected':'';?>>xxxl/36</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-9">
                  <div class="form-group">
                    <label class="control-label">I LIKE THE LENGTH OF MY SKIRTS AND DRESSES TO BE LIKE THIS</label><span class="text-red">*</span>
                    <div>
                      <label class="radio-inline"><input type="radio" name="trouser_fit" <?php if(!empty($apparelSkirtFitLike)){echo (in_array('mini', @$apparelSkirtFitLike))?'checked':'';}?> value="mini">MINI</label>
                      <label class="radio-inline"><input type="radio" name="trouser_fit" <?php if(!empty($apparelSkirtFitLike)){ echo (in_array('above the knee', @$apparelSkirtFitLike))?'checked':'';}?> value="above the knee">ABOVE THE KNEE</label>
                      <label class="radio-inline"><input type="radio" name="trouser_fit" <?php if(!empty($apparelSkirtFitLike)){ echo (in_array('below the knee', @$apparelSkirtFitLike))?'checked':'';}?> value="below the knee">BELOW THE KNEE</label>
                      <label class="radio-inline"><input type="radio" name="trouser_fit" <?php if(!empty($apparelSkirtFitLike)){ echo (in_array('midi', @$apparelSkirtFitLike))?'checked':'';}?> value="midi">MIDI</label>
					  <label class="radio-inline"><input type="radio" name="trouser_fit" <?php if(!empty($apparelSkirtFitLike)){ echo (in_array('maxi', @$apparelSkirtFitLike))?'checked':'';}?> value="maxi">MAXI</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">Jeans Size</label><span class="text-red">*</span>
                    <select class="form-control hwidth" id="scbox_denim_size" name="scbox_denim_size">
                      <option value=""></option>
                      <option value="xs/25" <?php echo (@$apparelJeansSize == 'xs/25')?'selected':'';?>>xs/25 </option>
                      <option value="s/26" <?php echo (@$apparelJeansSize == 's/26')?'selected':'';?>>s/26</option>
                      <option value="m/28" <?php echo (@$apparelJeansSize == 'm/28')?'selected':'';?>>m/28</option>
                      <option value="l/30" <?php echo (@$apparelJeansSize == 'l/30')?'selected':'';?>>l/30</option>
                      <option value="xl/32" <?php echo (@$apparelJeansSize == 'xl/32')?'selected':'';?>>xl/32</option>
                      <option value="xxl/34" <?php echo (@$apparelJeansSize == 'xxl/34')?'selected':'';?>>xxl/34</option>
                      <option value="xxxl/36" <?php echo (@$apparelJeansSize == 'xxxl/36')?'selected':'';?>>xxxl/36</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-9">
                  <div class="form-group">
                    <label class="control-label">Bottoms Fit</label><span class="text-red">*</span>
                    <div>
                      <label class="radio-inline"><input type="radio" name="bottom-fit" value="slim" <?php if(!empty($apparelBottomFitLike)) {echo (in_array('slim', @$apparelBottomFitLike))?'checked':''; } ?>>Slim</label>
                      <label class="radio-inline"><input type="radio" name="bottom-fit" value="straight" <?php if(!empty($apparelBottomFitLike)) {echo (in_array('straight', @$apparelBottomFitLike))?'checked':''; } ?>>STRAIGHT</label>
                      <label class="radio-inline"><input type="radio" name="bottom-fit" value="skinny" <?php if(!empty($apparelBottomFitLike)) {echo (in_array('skinny', @$apparelBottomFitLike))?'checked':''; }?>>Skinny</label>
                      <label class="radio-inline"><input type="radio" name="bottom-fit" value="flared" <?php if(!empty($apparelBottomFitLike)) {echo (in_array('flared', @$apparelBottomFitLike))?'checked':''; }?> >Flared</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label"> Lingerie:  Band Size </label><span class="text-red">*</span>
                    <select name="user_sizeband_women" id="user_sizeband" class="form-control input-sm">
                      <option value=""></option>
                      <option value="30" <?php echo (@$apparelBandSize=='30')?'selected':'';?>>30</option>
                      <option value="32" <?php echo (@$apparelBandSize=='32')?'selected':'';?>>32</option>
                      <option value="34" <?php echo (@$apparelBandSize=='34')?'selected':'';?>>34</option>
                      <option value="36" <?php echo (@$apparelBandSize=='36')?'selected':'';?>>36</option>
                      <option value="38" <?php echo (@$apparelBandSize=='38')?'selected':'';?>>38</option>
                      <option value="40" <?php echo (@$apparelBandSize=='40')?'selected':'';?>>40</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">Cup Size </label><span class="text-red">*</span>
                    <select name="user_sizecup_women" id="user_sizecup" class="form-control input-sm">
                      <option value=""></option>
                      <option value="a" <?php echo (@$apparelCupSize=='a')?'selected':'';?>>A</option>
                      <option value="b" <?php echo (@$apparelCupSize=='b')?'selected':'';?>>B</option>
                      <option value="c" <?php echo (@$apparelCupSize=='c')?'selected':'';?>>C</option>
                      <option value="d" <?php echo (@$apparelCupSize=='d')?'selected':'';?>>D</option>
                      <option value="dd" <?php echo (@$apparelCupSize=='dd')?'selected':'';?>>DD</option>
                      <option value="e" <?php echo (@$apparelCupSize=='e')?'selected':'';?>>E</option>
                      <option value="f" <?php echo (@$apparelCupSize=='f')?'selected':'';?>>F</option>
                    </select>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
 </div>
 <div class="women-accessories-container">
  <div class="box">
          <div class="box-header">
            <h2 class="box-title">Accesories</h2>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Select Accessory</label><span class="text-red">*</span>
                  <ul class="listingstyle">
                    <li class="radio-inline"><input type="radio" name="accessory" value="sunglasses">SUNGLASSES</li>
                    <li class="radio-inline"><input type="radio" name="accessory" value="belts">BELTS</li>
                    <li class="radio-inline"><input type="radio" name="accessory" value="caps">CAPS</li>
                    <li class="radio-inline"><input type="radio" name="accessory" value="stationary">STATIONARY</li>
                    <li class="radio-inline"><input type="radio" name="accessory" value="scarves">SCARVES</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                  <label class="control-label">Sunglasses</label><span class="text-red">*</span>
                  <ul class="listingstyle">

                    <li class="checkbox-inline"><input type="checkbox" <?php if(!empty($sunglassesType)) { echo (in_array("aviators", @$sunglassesType))?"checked":"";} ?> name="sunglass" value="aviators">AVIATORS</li>
                    <li class="checkbox-inline"><input type="checkbox" <?php if(!empty($sunglassesType)) { echo (in_array("wayfarers", @$sunglassesType))?"checked":""; }?> name="sunglass" value="wayfarers">WAYFARERS</li>
                    <li class="checkbox-inline"><input type="checkbox" <?php if(!empty($sunglassesType)) { echo (in_array("round", @$sunglassesType))?"checked":"";} ?> name="sunglass" value="round">ROUND</li>
                    <li class="checkbox-inline"><input type="checkbox" <?php if(!empty($sunglassesType)) { echo (in_array("square", @$sunglassesType))?"checked":"";} ?> name="sunglass" value="square">SQUARE</li>
                    <li class="checkbox-inline"><input type="checkbox" <?php if(!empty($sunglassesType)) { echo (in_array("aviators", @$sunglassesType))?"checked":""; }?> name="sunglass" value="oval">OVAL</li>
                    <li class="checkbox-inline"><input type="checkbox" <?php if(!empty($sunglassesType)) { echo (in_array("cat eyes", @$sunglassesType))?"checked":""; } ?> name="sunglass" value="cat eyes">CAT EYES</li>

                  </ul>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Reflectors</label>
                  <div>
                    <label class="radio-inline"><input type="radio" name="reflectors" value="yes" <?php echo (@$refelectorType == "yes")?"checked":""; ?>>Yes</label>
                    <label class="radio-inline"><input type="radio" name="reflectors" value="no" <?php echo (@$refelectorType == "no")?"checked":""; ?>>No</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Belts</label>
                  <div>

                    <label class="radio-inline"><input type="checkbox" name="belts" <?php if(!empty($beltType)) { echo (in_array("chunky", @$beltType))?"checked":"";} ?> value="chunky">Chunky</label>
                    <label class="radio-inline"><input type="checkbox" name="belts" <?php if(!empty($beltType)) { echo (in_array("slim", @$beltType))?"checked":"";} ?> value="slim">Slim</label>
                    <label class="radio-inline"><input type="checkbox" name="belts" <?php if(!empty($beltType)) { echo (in_array("medium", @$beltType))?"checked":"";} ?> value="medium">Medium</label>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
 </div>
 <div class="women-jewellery-container">
  <div class="box">
          <div class="box-header">
            <h2 class="box-title">Jewellery</h2>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Jewellery</label><span class="text-red">*</span>
                  <ul class="listingstyle">
                    <li class="radio-inline"><input type="radio" name="jewellery" value="statement">STATEMENT</li>
                    <li class="radio-inline"><input type="radio" name="jewellery" value="minimal">MINIMAL</li>
                    <li class="radio-inline"><input type="radio" name="jewellery" value="bold/chunky ">BOLD/CHUNKY </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Jewellery Tone</label><span class="text-red">*</span>
                  <ul class="listingstyle">

                    <li class="radio-inline"><input type="checkbox" name="jewellery_tone" <?php if(!empty($jewelleryTone)){ echo (in_array("gold", @$jewelleryTone))?"checked":"";}?> value="gold">GOLD</li>
                    <li class="radio-inline"><input type="checkbox" name="jewellery_tone" <?php if(!empty($jewelleryTone)){echo (in_array("silver", @$jewelleryTone))?"checked":"";}?> value="silver">SILVER</li>
                    <li class="radio-inline"><input type="checkbox" name="jewellery_tone" <?php if(!empty($jewelleryTone)){echo (in_array("oxidised", @$jewelleryTone))?"checked":"";}?> value="oxidised">OXIDISED</li>
                    <li class="radio-inline"><input type="checkbox" name="jewellery_tone" <?php if(!empty($jewelleryTone)){echo (in_array("multicolored", @$jewelleryTone))?"checked":"";}?> value="multicolored">MULTICOLORED</li>
                    <li class="radio-inline"><input type="checkbox" name="jewellery_tone" <?php if(!empty($jewelleryTone)){echo (in_array("tonal", @$jewelleryTone))?"checked":""; }?> value="tonal">TONAL</li>

                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
 </div>
 <div class="women-beauty-container">
  <div class="box">
          <div class="box-header">
            <h2 class="box-title">Beauty</h2>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Beauty Product</label><span class="text-red">*</span>
                  <ul class="listingstyle">
                    <li class="radio-inline"><input type="radio" name="beauty_product" value="eye-shadow ">EYE SHADOW </li>
                    <li class="radio-inline"><input type="radio" name="beauty_product" value="liner">LINER</li>
                    <li class="radio-inline"><input type="radio" name="beauty_product" value="lipstick">LIPSTICK</li>
                    <li class="radio-inline"><input type="radio" name="beauty_product" value="nailpaint">NAILPAINT</li>
                    <li class="radio-inline"><input type="radio" name="beauty_product" value="mascara">MASCARA</li>
                    <li class="radio-inline"><input type="radio" name="beauty_product" value="hand-creme">HAND CREME</li>
                    <li class="radio-inline"><input type="radio" name="beauty_product" value="dry-shampoo">DRY SHAMPOO</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Shade</label><span class="text-red">*</span>
                  <ul class="listingstyle">
                    <li class="checkbox-inline"><input type="checkbox" name="shade" value="skyblue">RED</li>
                    <li class="checkbox-inline"><input type="checkbox" name="shade" value="deep">DEEP</li>
                    <li class="checkbox-inline"><input type="checkbox" name="shade" value="pink">PINK</li>
                    <li class="checkbox-inline"><input type="checkbox" name="shade" value="nude">NUDE</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
			  <?php
			/* 	echo "<pre>";
				print_r($beautyFinishingType);
				echo "</pre>"; */
			  ?>
                <div class="form-group">
                  <label class="control-label">Finish</label><span class="text-red">*</span>
                  <ul class="listingstyle">
                    <li class="radio-inline"><input type="radio" name="finish" value="matte" <?php if(!empty($beautyFinishingType)){if(in_array('matte', @$beautyFinishingType)){echo "checked" ; }}?>>Matte</li>
                    <li class="radio-inline"><input type="radio" name="finish" value="shiny"  <?php if(!empty($beautyFinishingType)){if(in_array('shiny', @$beautyFinishingType)){echo "checked" ; }}?>>Shiny</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="box">
          <div class="box-header">
            <h2 class="box-title">Bags</h2>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Bag Types</label><span class="text-red">*</span>
                  <ul class="listingstyle">
                    <li class="checkbox-inline"><input type="checkbox" name="bag" value="sling">SLING</li>
                    <li class="checkbox-inline"><input type="checkbox" name="bag" value="clutch">CLUTCH</li>
                    <li class="checkbox-inline"><input type="checkbox" name="bag" value="handbag">HANDBAG</li>
                    <li class="checkbox-inline"><input type="checkbox" name="bag" value="tote">TOTE</li>
                    <li class="checkbox-inline"><input type="checkbox" name="bag" value="backpack">BACKPACK</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">Bag Size</label><span class="text-red">*</span>
                  <select class="form-control hwidth" id="scbox_bag-size" name="scbox_bag-size_w">
                    <option value=""></option>
                    <option value="small" <?php echo (@$bagSize == "small")?"selected":"";?>>Small</option>
                    <option value="medium" <?php echo (@$bagSize == "medium")?"selected":"";?>>Medium </option>
                    <option value="large" <?php echo (@$bagSize == "large")?"selected":"";?>>Large</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">Bag Purpose</label><span class="text-red">*</span>
                  <select class="form-control hwidth" id="scbox_bag-purpose" name="scbox_bag-purpose_w">
                    <option value=""></option>
                    <option value="everyday" <?php echo (@$bagWant == "everyday")?"selected":"";?>>EVERYDAY</option>
                    <option value="work" <?php echo (@$bagWant == "work")?"selected":"";?>>WORK </option>
                    <option value="party" <?php echo (@$bagWant == "party")?"selected":"";?>>PARTY</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="box">
          <div class="box-header">
            <h2 class="box-title">Footwear</h2>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
			  
                <div class="form-group">
                  <label class="control-label">Footwear Type</label><span class="text-red">*</span>
                  <ul class="listingstyle">
                    <li class="checkbox-inline"><input type="checkbox" name="footwear_type"  value="flats" >Flats</li>
                    <li class="checkbox-inline"><input type="checkbox" name="footwear_type" value="sneakers" >SNEAKERS</li>
                    <li class="checkbox-inline"><input type="checkbox" name="footwear_type" value="heels" >Heels</li>
                  </ul>
                </div>
              </div>
			  
              <div class="col-md-6">
			  <?php 
				/* echo "<pre>";
				print_r($footwearHeelType);
				echo "</pre>"; */
			  ?>
                <div class="form-group">
                  <label class="control-label">Heel Types</label><span class="text-red">*</span>
                  <ul class="listingstyle">
                    <li class="checkbox-inline"><input type="checkbox" name="heel_type" value="wedges" <?php if(!empty($footwearHeelType)){if(in_array('wedge', @$footwearHeelType)){echo "checked" ; }}?>>Wedges</li>
                    <li class="checkbox-inline"><input type="checkbox" name="heel_type" value="block" <?php if(!empty($footwearHeelType)){if(in_array('block', @$footwearHeelType)){echo "checked" ; }}?>>Block</li>
                    <li class="checkbox-inline"><input type="checkbox" name="heel_type" value="stilettoes" <?php if(!empty($footwearHeelType)){if(in_array('stilletos', @$footwearHeelType)){echo "checked" ; }}?>>Stilettoes</li>
                  </ul>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">Foot Width</label>
                  <div>
                    <label class="radio-inline"><input type="radio" name="user_foot-width" value="wide">Wide</label>
                    <label class="radio-inline"><input type="radio" name="user_foot-width" value="narrow">Narrow</label>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">Footwear (IND) </label><span class="text-red">*</span>
                  <select class="form-control hwidth" id="scbox_foot-size" name="scbox_foot-size_w">
                    <option value=""></option>
                    <option value="uk3/us5/eu36">uk 3/us 5/eu 36 </option>
                    <option value="uk4/us6/eu37">uk 4/us 6/eu 37 </option>
                    <option value="uk5/us7/eu38">uk 5/us 7/eu 38 </option>
                    <option value="uk6/us8/eu39">uk 6/us 8/eu 39</option>
                    <option value="uk7/us9/eu41">uk 7/us 9/eu 41</option>
                    <option selected="" value="uk8/us10/eu42 ">uk 8/us 10/eu 42 </option>
                    <option value="uk9/us11/eu43 ">uk 9/us 11/eu 43 </option>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
			  <?php 
			  /* echo "<pre>";
			  print_r($footwearHeelHeight);
			  echo "</pre>"; */
			  ?>
                <div class="form-group">
                  <label class="control-label">Heel Height</label><span class="text-red">*</span>
                  <select class="form-control hwidth" name="scbox_sizeheelh" id="scbox_sizeheelh">
                    <option value="">--</option>
                    <option value="low:1.5”-2.5”" <?php echo ($footwearHeelHeight == 'low:1.5"-2.5"')?"selected":""; ?>>LOW:   1.5”-2.5”</option>
                    <option value="medium:2.5”-3.5”" <?php echo ($footwearHeelHeight == 'medium:2.5"-3.5"')?"selected":""; ?>>MEDIUM: 2.5”-3.5” </option>
                    <option value="high:3.5”andabove" <?php echo ($footwearHeelHeight == 'medium:2.5"-3.5"')?"selected":""; ?>>HIGH: 3.5” and above</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

	   
      <?php
	  }
	  if($user_data['user_gender']['object_meta_value'] == '2')
	  {
	  ?>
	  

    <div class="box-body <?php if($form_type == 'edit' && @$user_data['user_gender']['object_meta_value'] != '2'){ echo "hide"; }else if($form_type == 'add'){ echo "hide"; } ?>" id="men_pa">
         <div class="col-md-12 men-category-container">
            <div class="form-group">
              <label class="control-label">Categories</label><span class="text-red">*</span>
              <ul class="listingstyle">
                <li class="checkbox-inline"><input type="checkbox" name="men_category" value="apparel">apparel</li>
                <li class="checkbox-inline"><input type="checkbox" name="men_category" value="bags">bags</li>
                <li class="checkbox-inline"><input type="checkbox" name="men_category" value="footwear">footwear</li>
                <li class="checkbox-inline"><input type="checkbox" name="men_category" value="accessories">accessories </li>
                <li class="checkbox-inline"><input type="checkbox" name="men_category" value="grooming">grooming </li>
              </ul>
            </div>
          </div>
        <div class="men-apparel-container" id="pa_attr">  
          <div class="row">
           <div class="col-md-3">
            <div class="form-group">
              <label class="control-label" >THIS IS WHAT MY BODY LOOKS LIKE</label><span class="text-red">*</span>
                 <?php $bodyshape =  strtolower($jsonData['pa_bodshape']); ?>
              <select name="user_body_shape_men" id="user_body_shape" class="form-control input-sm">
                <option value="slim"<?php echo (@$bodyshape == 'slim') ? 'selected' : ''; ?>>Slim</option>
                <option value="average" <?php echo (@$bodyshape == 'average') ? 'selected' : ''; ?>>Average</option>
                <option value="athletic" <?php echo (@$bodyshape == 'athletic') ? 'selected' : ''; ?>>Athletic</option>
                <option value="heavy" <?php echo (@$bodyshape == 'heavy') ? 'selected' : ''; ?>>Heavy</option>
                <option value="triangular" <?php echo (@$bodyshape == 'triangular') ? 'selected' : ''; ?>>Triangular</option>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <label class="control-label">Height</label><span class="text-red">*</span>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <select name="height-height_feet" id="height-height_feet" class="form-control hwidth">
                    <option value="">--</option>

                    <option value="3 ft" <?php echo (@$apparelHeightFeet == "3 ft")?"selected":""; ?>>3 ft</option>
                    <option value="4 ft" <?php echo (@$apparelHeightFeet == "4 ft")?"selected":""; ?>>4 ft</option>
                    <option value="5 ft" <?php echo (@$apparelHeightFeet == "5 ft")?"selected":""; ?>>5 ft</option>
                    <option value="6 ft" <?php echo (@$apparelHeightFeet == "6 ft")?"selected":""; ?>>6 ft</option>
                    <option value="7 ft" <?php echo (@$apparelHeightFeet == "7 ft")?"selected":""; ?>>7 ft</option>

                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <select name="height-height_inches" id="height-height_inches" class="form-control hwidth">
                    <option value="">--</option>
                    <option value="0 inch" <?php echo (@$apparelHeightInch == "0 inch")?"selected":""; ?>>0 in</option>
                    <option value="1 inch" <?php echo (@$apparelHeightInch == "1 inch")?"selected":""; ?>>1 in</option>
                    <option value="2 inch" <?php echo (@$apparelHeightInch == "2 inch")?"selected":""; ?>>2 in</option>
                    <option value="3 inch" <?php echo (@$apparelHeightInch == "3 inch")?"selected":""; ?>>3 in</option>
                    <option value="4 inch" <?php echo (@$apparelHeightInch == "4 inch")?"selected":""; ?>>4 in</option>
                    <option value="5 inch" <?php echo (@$apparelHeightInch == "5 inch")?"selected":""; ?>>5 in</option>
                    <option value="6 inch" <?php echo (@$apparelHeightInch == "6 inch")?"selected":""; ?>>6 in</option>
                    <option value="7 inch" <?php echo (@$apparelHeightInch == "7 inch")?"selected":""; ?>>7 in</option>
                    <option value="8 inch" <?php echo (@$apparelHeightInch == "8 inch")?"selected":""; ?>>8 in</option>
                    <option value="9 inch" <?php echo (@$apparelHeightInch == "9 inch")?"selected":""; ?>>9 in</option>
                    <option value="10 inch" <?php echo (@$apparelHeightInch == "10 inch")?"selected":""; ?>>10 in</option>
                    <option value="11 inch" <?php echo (@$apparelHeightInch == "11 inch")?"selected":""; ?>>11 in</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>  
          <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">What i don't want</label><span class="text-red">*</span>
                  <ul class="listingstyle">

				  <?php
					/* echo "<pre>";
					print_r($dontWant);
					echo "</pre>"; */
				  ?>
                    <li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="t-shirts" <?php if(in_array("t-shirts", @$dontWant)){echo "checked";}?>>T-SHIRTS</li>
					<li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="casualshirts" <?php if(in_array("casual shirts", @$dontWant)){echo "checked";}?>>CASUAL SHIRTS</li>
                    <li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="formalshirts" <?php if(in_array("formal shirts", @$dontWant)){echo "checked";}?>>FORMAL SHIRTS </li>
                    <li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="shorts" <?php if(in_array("shorts", @$dontWant)){echo "checked";}?>>SHORTS</li>
                    <li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="joggers" <?php if(in_array("joggers", @$dontWant)){echo "checked";}?>>JOGGERS </li>
                    <li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="jeans" <?php if(in_array("jeans", @$dontWant)){echo "checked";}?>>JEANS</li>
                    <li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="chinos" <?php if(in_array("chinos", @$dontWant)){echo "checked";}?>>CHINOS</li>
                    <li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="trousers" <?php if(in_array("trousers", @$dontWant)){echo "checked";}?>>TROUSERS</li>
                    <li class="checkbox-inline"><input type="checkbox" name="what_dont_want" value="outerwear" <?php if(in_array("tops", @$dontWant)){echo "checked";}?>>OUTERWEAR</li>

                  </ul>
                </div>
              </div>  
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
				
                  <label class="control-label">T-shirt Size</label><span class="text-red">*</span>
                  <select class="form-control hwidth" id="scbox_tshirt_size" name="scbox_tshirt_size">
                    <option value=""></option>
                    <option value="xs/34-36" <?php echo (@$apparelTshirtSize == "xs/34-36")?'selected':''; ?>>xs/ 34-36 </option>
                    <option value="s/36.5-38" <?php echo (@$apparelTshirtSize == "s/36.5-38")?'selected':''; ?>>s/ 36.5-38 </option>
                    <option value="m/38-40" <?php echo (@$apparelTshirtSize == "m/38-40")?'selected':''; ?>>m/ 38-40</option>
                    <option value="l/40-42" <?php echo (@$apparelTshirtSize == "l/40-42")?'selected':''; ?>>l/ 40-42</option>
                    <option value="xl/42-44" <?php echo (@$apparelTshirtSize == "xl/42-44")?'selected':''; ?>>xl/ 42-44</option>
                    <option value="xxl/44-46" <?php echo (@$apparelTshirtSize == "xxl/44-46")?'selected':''; ?>>xxl/ 44-46</option>
                    <option value="xxxl/46-48" <?php echo (@$apparelTshirtSize == "xxxl/46-48")?'selected':''; ?>>xxxl/ 46-48</option>
                  </select>
                </div>
              </div>
			 
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">T-shirt Fit</label><span class="text-red">*</span>
                  <div>
                    <label class="radio-inline"><input type="checkbox" name="jeans_fit"  value="slim" <?php echo (@$apparelTshirtFitLikeThis[0] == "slim")?"checked":"";?> >Slim</label>
                    <label class="radio-inline"><input type="checkbox" name="jeans_fit"  value="regular" <?php echo (@$apparelTshirtFitLikeThis[1] == "regular")?"checked":"";?>>Regular</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
			 <?php
				/* echo "<pre>";
				print_r($apparelShirtSize);
				echo "</pre>"; */
				
			  ?>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label" >Shirt Size</label><span class="text-red">*</span>
				  <p><?php echo $apparelShirtSize; ?></p>
                  <!--<select class="form-control hwidth" id="shirt_size" name="shirt_size">
                    <option value=""></option>
                    <option value="xs/chest 34-36/neck 15\"" <?php echo (@$apparelShirtSize == "xs/chest 34-36/neck 15")?'selected':''; ?>>xs/chest 34-36 / neck 15</option>
                    <option value="s/chest36.5-38/neck 15.5" <?php echo (@$apparelShirtSize == "s/chest36.5-38/neck 15.5")?'selected':''; ?>>s/chest 36.5-38 / neck 15.5</option>
                    <option value="m/chest38-40/neck 16" <?php echo (@$apparelShirtSize == "m/chest38-40/neck 16")?'selected':''; ?>>m/chest 38-40/ neck 16</option>
                    <option value="l/chest0-42/neck17" <?php echo (@$apparelShirtSize == "l/chest0-42/neck17")?'selected':''; ?>>l/chest 40-42/ neck 17”</option>
                    <option value="xl/chest42-44/neck 17.5" <?php echo (@$apparelShirtSize == "xl/chest42-44/neck 17.5")?'selected':''; ?>>xl/chest 42-44/ neck 17.5</option>
                    <option value="xxl/chest44-46/neck 18.5" <?php echo (@$apparelShirtSize == "xxl/chest44-46/neck 18.5")?'selected':''; ?>>xxl/chest 44-46/ neck 18.5</option>
                    <option value="xxxl/chest46-48/neck 19.5" <?php echo (@$apparelShirtSize == "xxxl/chest46-48/neck 19.5")?'selected':''; ?>>xxxl/chest 46-48/ neck 19.5</option>
                  </select>-->
                </div>
              </div>
              <div class="col-md-3">
			   <?php
				/*  echo "<pre>";
				print_r($apparelShirtFitLikeThis);
				echo "</pre>";  */
				
			  ?>
                <div class="form-group">
                  <label class="control-label">Shirt Fit</label><span class="text-red">*</span>
                  <div>
                    <label class="radio-inline"><input type="checkbox" name="jeans_fit"  <?php echo (in_array("slim", $apparelShirtFitLikeThis))?"checked":"";?>  value="slim" >Slim</label>
                    <label class="radio-inline"><input type="checkbox" name="jeans_fit" slim <?php echo (in_array("regular", $apparelShirtFitLikeThis))?"checked":"";?> value="regular" >Regular</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
			 <?php
				/* echo "<pre>";
				print_r($apparelTrouserSize);
				echo "</pre>";   */
				
			  ?>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">Trouser Size</label><span class="text-red">*</span>
                  <select class="form-control hwidth" id="scbox_sizetrouser" name="scbox_sizetrouser">
                    <option value=""></option>
                    <option value="xs/ waist inches - 28" <?php echo (@$apparelTrouserSize == "xs/ waist inches - 28")?'selected':''; ?>>xs/ waist inches - 28</option>
                    <option value="s/ waist inches - 30" <?php echo (@$apparelTrouserSize == "s/ waist inches - 30")?'selected':''; ?>>s/ waist inches - 30</option>
                    <option value="m/ waist inches - 32" <?php echo (@$apparelTrouserSize == "m/ waist inches - 32")?'selected':''; ?>>m/ waist inches - 32</option>
                    <option value="l/ waist inches - 34" <?php echo (@$apparelTrouserSize == "l/ waist inches - 34")?'selected':''; ?>>l/ waist inches - 34</option>
                    <option value="xl/ waist inches - 36" <?php echo (@$apparelTrouserSize == "xl/waistinches - 36")?'selected':''; ?>>xl/ waist inches - 36</option>
                    <option value="xxl/ waist inches - 38" <?php echo (@$apparelTrouserSize == "xxl/ waist inches - 38")?'selected':''; ?>>xxl/ waist inches - 38</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">Trouser Fit</label><span class="text-red">*</span>
                  <div>
                    <label class="radio-inline"><input type="checkbox" name="jeans_fit" <?php echo (in_array("slim", @$apparelTrouserFitLikeThis))?"checked":"";?> value="slim" >Slim</label>
                    <label class="radio-inline"><input type="checkbox" name="jeans_fit" <?php echo (in_array("regular", @$apparelTrouserFitLikeThis))?"checked":"";?> value="regular" >Regular</label>
                    <label class="radio-inline"><input type="checkbox" name="jeans_fit" <?php echo (in_array("skinny", @$apparelTrouserFitLikeThis))?"checked":"";?> value="skinny" >Skinny</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">Jeans Size</label><span class="text-red">*</span>
                  <select class="form-control hwidth" id="scbox_denim_size" name="scbox_denim_size">
                    <option value=""></option>
                    <option value="xs/waistinches-28" <?php echo (@$apparelJeansSize == "xs/ waist inches - 28")?'selected':''; ?>>xs/ waist inches - 28</option>
                    <option value="s/waistinches-30" <?php echo (@$apparelJeansSize == "s/ waist inches - 30")?'selected':''; ?>>s/ waist inches - 30</option>
                    <option value="m/waistinches-32" <?php echo (@$apparelJeansSize == "m/ waist inches - 32")?'selected':''; ?>>m/ waist inches - 32</option>
                    <option value="l/waistinches-34" <?php echo (@$apparelJeansSize == "l/ waist inches - 34")?'selected':''; ?>>l/ waist inches - 34</option>
                    <option value="xl/waistinches-36" <?php echo (@$apparelJeansSize == "xl/ waist inches - 36")?'selected':''; ?>>xl/ waist inches - 36</option>
                    <option value="xxl/waistinches-38" <?php echo (@$apparelJeansSize == "xxl/ waist inches - 38")?'selected':''; ?>>xxl/ waist inches - 38</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">Jeans Fit</label><span class="text-red">*</span>
                  <div>
                    <label class="radio-inline"><input type="checkbox" name="jeans_fit" <?php echo (in_array("slim", @$apparelJeansFitLikeThis))?'checked':''; ?> value="slim" >Slim</label>
                    <label class="radio-inline"><input type="checkbox" name="jeans_fit" <?php echo (in_array("regular", @$apparelJeansFitLikeThis))?'checked':''; ?> value="regular" >Regular</label>
                    <label class="radio-inline"><input type="checkbox" name="jeans_fit" <?php echo (in_array("skinny", @$apparelJeansFitLikeThis))?'checked':''; ?> value="skinny" >Skinny</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="box">
          <div class="box-header">
            <h2 class="box-title">STUFF I LIKE WEARING </h2>
          </div>

          <div class="box-body">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">ROUND NECK</label>
                  <div>
                    <label class="radio-inline"><input type="radio" name="round_neck" value="yes" <?php echo (@$apparelDosAndDonts[8][1] == 'yes')?"checked":"";?>>Yes</label>
                    <label class="radio-inline"><input type="radio" name="round_neck" value="no" <?php echo (@$apparelDosAndDonts[8][1] == 'no')?"checked":"";?>>No</label>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">V NECK</label>
                  <div>
                    <label class="radio-inline"><input type="radio" name="vneck" <?php echo (@$apparelDosAndDonts[9][1] == 'yes')?"checked":"";?> value="yes">Yes</label>
                    <label class="radio-inline"><input type="radio" name="vneck" <?php echo (@$apparelDosAndDonts[9][1] == 'no')?"checked":"";?> value="no">No</label>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">Henleys</label>
                  <div>
                    <label class="radio-inline"><input type="radio" name="henleys" <?php echo (@$apparelDosAndDonts[10][1] == 'yes')?"checked":"";?> value="yes">Yes</label>
                    <label class="radio-inline"><input type="radio" name="henleys" <?php echo (@$apparelDosAndDonts[10][1] == 'no')?"checked":"";?> value="no">No</label>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">COLLARED T-SHIRT</label>
                  <div>
                    <label class="radio-inline"><input type="radio" name="collared_tshirt" <?php echo (@$apparelDosAndDonts[11][1] == 'yes')?"checked":"";?> value="yes">Yes</label>
                    <label class="radio-inline"><input type="radio" name="collared_tshirt" <?php echo (@$apparelDosAndDonts[11][1] == 'no')?"checked":"";?> value="no">No</label>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">HALF SLEEVES SHIRTS </label>
                  <div>
                    <label class="radio-inline"><input type="radio" <?php echo (@$apparelDosAndDonts[12][1] == 'yes')?"checked":"";?> name="half_sleeves" value="yes">Yes</label>
                    <label class="radio-inline"><input type="radio" <?php echo (@$apparelDosAndDonts[12][1] == 'no')?"checked":"";?> name="half_sleeves" value="no">No</label>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">FULL SLEEVES SHIRTS</label>
                  <div>
                    <label class="radio-inline"><input type="radio" <?php echo (@$apparelDosAndDonts[13][1] == 'yes')?"checked":"";?> name="full_sleeves" value="yes">Yes</label>
                    <label class="radio-inline"><input type="radio" <?php echo (@$apparelDosAndDonts[13][1] == 'no')?"checked":"";?> name="full_sleeves" value="no">No</label>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">JACKETS</label>
                  <div>
                    <label class="radio-inline"><input type="radio" <?php echo (@$apparelDosAndDonts[14][1] == 'yes')?"checked":"";?> name="jackets" value="yes">Yes</label>
                    <label class="radio-inline"><input type="radio" <?php echo (@$apparelDosAndDonts[14][1] == 'no')?"checked":"";?> name="jackets" value="no">No</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="box">
          <div class="box-header">
            <h2 class="box-title">MY STYLE PREFERENCE ( LOVE/LIKE/DISLIKE) </h2>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">Casuals</label>
                  <div>
                    <label class="radio-inline"><input type="radio" name="casuals" value="love" <?php echo (@$apparelStylePreference[6][1] == "love")?"checked":""; ?>>LOVE</label>
                    <label class="radio-inline"><input type="radio" name="casuals" <?php echo (@$apparelStylePreference[6][1] == "like")?"checked":""; ?> value="like">LIKE</label>
                    <label class="radio-inline"><input type="radio" name="casuals" <?php echo (@$apparelStylePreference[6][1] == "dislike")?"checked":""; ?> value="dislike">DISLIKE</label>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">9-5</label>
                  <div>
                    <label class="radio-inline"><input type="radio" <?php echo (@$apparelStylePreference[8][1] == "love")?"checked":""; ?> name="9-5" value="love">LOVE</label>
                    <label class="radio-inline"><input type="radio" <?php echo (@$apparelStylePreference[8][1] == "like")?"checked":""; ?> name="9-5" value="like">LIKE</label>
                    <label class="radio-inline"><input type="radio" <?php echo (@$apparelStylePreference[8][1] == "dislike")?"checked":""; ?> name="9-5" value="dislike">DISLIKE</label>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">Well Groomed</label>
                  <div>
                    <label class="radio-inline"><input type="radio" <?php echo (@$apparelStylePreference[9][1] == "love")?"checked":""; ?> name="well_groomed" value="love">LOVE</label>
                    <label class="radio-inline"><input type="radio" <?php echo (@$apparelStylePreference[9][1] == "like")?"checked":""; ?> name="well_groomed" value="like">LIKE</label>
                    <label class="radio-inline"><input type="radio" <?php echo (@$apparelStylePreference[9][1] == "dislike")?"checked":""; ?> name="well_groomed" value="dislike">DISLIKE</label>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">Atheleisure</label>
                  <div>
                    <label class="radio-inline"><input type="radio" <?php echo (@$apparelStylePreference[5][1] == "love")?"checked":""; ?> name="atheleisure" value="love">LOVE</label>
                    <label class="radio-inline"><input type="radio" <?php echo (@$apparelStylePreference[5][1] == "like")?"checked":""; ?> name="atheleisure" value="like">LIKE</label>
                    <label class="radio-inline"><input type="radio" <?php echo (@$apparelStylePreference[5][1] == "dislike")?"checked":""; ?> name="atheleisure" value="dislike">DISLIKE</label>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">Bold</label>
                  <div>
                    <label class="radio-inline"><input type="radio" <?php echo (@$apparelStylePreference[7][1] == "love")?"checked":""; ?> name="bold" value="love">LOVE</label>
                    <label class="radio-inline"><input type="radio" <?php echo (@$apparelStylePreference[7][1] == "like")?"checked":""; ?> name="bold" value="like">LIKE</label>
                    <label class="radio-inline"><input type="radio" <?php echo (@$apparelStylePreference[7][1] == "dislike")?"checked":""; ?> name="bold" value="dislike">DISLIKE</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="box">
          <div class="box-header">
            <h2 class="box-title">Prints and Colors to avoid </h2>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Prints to avoid</label><span class="text-red">*</span>
                  <ul class="listingstyle">
                    <li class="checkbox-inline"><input type="checkbox" <?php echo (in_array("checks", @$apparelPrintAvoidMen))?"checked":"";?> name="prints_avoid" value="checks">CHECKS</li>
                    <li class="checkbox-inline"><input type="checkbox" <?php echo (in_array("tartans", @$apparelPrintAvoidMen))?"checked":"";?> name="prints_avoid" value="tartans">TARTANS</li>
                    <li class="checkbox-inline"><input type="checkbox" <?php echo (in_array("floral", @$apparelPrintAvoidMen))?"checked":"";?> name="prints_avoid" value="floral">FLORAL</li>
                    <li class="checkbox-inline"><input type="checkbox" <?php echo (in_array("stripes", @$apparelPrintAvoidMen))?"checked":"";?> name="prints_avoid" value="stripes">STRIPES</li>
                    <li class="checkbox-inline"><input type="checkbox" <?php echo (in_array("polka dots", @$apparelPrintAvoidMen))?"checked":"";?> name="prints_avoid" value="polka dots ">POLKA DOTS  </li>
                    <li class="checkbox-inline"><input type="checkbox" <?php echo (in_array("houndstooth", @$apparelPrintAvoidMen))?"checked":"";?> name="prints_avoid" value="houndstooth">HOUNDSTOOTH</li>
                    <li class="checkbox-inline"><input type="checkbox" <?php echo (in_array("animal", @$apparelPrintAvoidMen))?"checked":"";?> name="prints_avoid" value="animal">ANIMAL PRINT /MOTIFS</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Colors to avoid</label><span class="text-red">*</span>
                  <ul class="listingstyle">

                    <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php echo (in_array("#8bf5cd",@$apparelColorAvoid))?"checked":""; ?> value="#8bf5cd">MINT</li>
                    <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php echo (in_array("#89aad5", @$apparelColorAvoid))?"checked":""; ?> value="#89aad5">LIGHT BLUE</li>
                    <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php echo (in_array("#bf94e4", @$apparelColorAvoid))?"checked":""; ?> value="#bf94e4">LAVENDER</li>
                    <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php echo (in_array("#aeb0ae", @$apparelColorAvoid))?"checked":""; ?> value="#aeb0ae">GREY</li>
                    <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php echo (in_array("#fffefe", @$apparelColorAvoid))?"checked":""; ?> value="#fffefe">WHITE</li>
                    <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php echo (in_array("#0b0706", @$apparelColorAvoid))?"checked":""; ?> value="#0b0706">BLACK</li>
					<li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php echo (in_array("#734021", @$apparelColorAvoid))?"checked":""; ?> value="#734021">BROWN</li>
                    <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php echo (in_array("#2082ef", @$apparelColorAvoid))?"checked":""; ?> value="#2082ef">BRIGHT BLUE</li>
                    <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php echo (in_array("#576331", @$apparelColorAvoid))?"checked":""; ?> value="#576331">OLIVE</li>
                    <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php echo (in_array("#0a6148", @$apparelColorAvoid))?"checked":""; ?> value="#0a6148">BOTTLE GREEN</li>
                    <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php echo (in_array("#68dbd6", @$apparelColorAvoid))?"checked":""; ?> value="#68dbd6">AGUA</li>
                    <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php echo (in_array("#f5dc05", @$apparelColorAvoid))?"checked":""; ?> value="#f5dc05">YELLOW</li>
                    <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php echo (in_array("#0c4c8a", @$apparelColorAvoid))?"checked":""; ?> value="#0c4c8a">ROYAL BLUE</li>
                    <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php echo (in_array("#041531", @$apparelColorAvoid))?"checked":""; ?> value="#041531">NAVY BLUE</li>
                    <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php echo (in_array("#de443a", @$apparelColorAvoid))?"checked":""; ?> value="#de443a">RED</li>
                    <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php echo (in_array("#bde4f5", @$apparelColorAvoid))?"checked":""; ?> value="#bde4f5">PASTEL BLUE</li>
                    <li class="checkbox-inline"><input type="checkbox" name="colors_avoid" <?php echo (in_array("#ffbbc7", @$apparelColorAvoid))?"checked":""; ?> value="#ffbbc7">BABY PINK</li>

                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
          </div>

           <div class="men-bag-container">
            <div class="box">
          <div class="box-header">
            <h2 class="box-title">Bags</h2>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Bag Types</label><span class="text-red">*</span>
                  <ul class="listingstyle">
                    <li class="checkbox-inline"><input type="checkbox" name="bag" value="wallets">WALLETS</li>
                    <li class="checkbox-inline"><input type="checkbox" name="bag" value="messengerbags">MESSENGER BAGS</li>
                    <li class="checkbox-inline"><input type="checkbox" name="bag" value="laptopbags">LAPTOP  BAGS</li>
                    <li class="checkbox-inline"><input type="checkbox" name="bag" value="backpacks">BACKPACKS</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>  
           </div>

           <div class="men-footwear-container">
            <div class="box">
          <div class="box-header">
            <h2 class="box-title">Footwear</h2>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">Footwear (IND) </label><span class="text-red">*</span>
                  <select class="form-control hwidth" id="scbox_foot-size_m" name="scbox_foot-size_m">
                    <option value=""></option>
                    <option value="eu38/uk5/us6"> eu 38/uk 5/us 6 </option>
                    <option value="eu40/uk6/us7">eu 40/uk 6/us 7 </option>
                    <option value="eu41/uk7/us8">eu 41/uk 7/us 8 </option>
                    <option value="42.5/uk8/us9">42.5/uk 8/us 9</option>
                    <option value="44/uk9/us10">44/uk 9/us 10</option>
                    <option selected="" value="45/uk10/us11 ">45/uk 10/ us 11 </option>
                    <option value="46/uk11/us12 ">46/uk 11/ us 12  </option>
                    <option value="47.5/uk12/us13 ">47.5/uk 12/ us 13 </option>
                  </select>
                </div>
              </div>
              <div class="col-md-9">
                <div class="form-group">
                  <label class="control-label">Footwear Type </label><span class="text-red">*</span>
                  <ul class="listingstyle">
                    <li class="checkbox-inline"><input type="checkbox" name="footwear_type" value="flip_flops_sandals">FLIP FLOPS and SANDALS  </li>
                    <li class="checkbox-inline"><input type="checkbox" name="footwear_type" value="sneakers">SNEAKERS</li>
                    <li class="checkbox-inline"><input type="checkbox" name="footwear_type" value="casual_shoeS">CASUAL SHOES</li>
                    <li class="checkbox-inline"><input type="checkbox" name="footwear_type" value="loafers">LOAFERS</li>
                    <li class="checkbox-inline"><input type="checkbox" name="footwear_type" value="formal_shoes">FORMAL SHOES</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>

           </div>

       <div class="men-accessories-container">     
        <div class="box">
          <div class="box-header">
            <h2 class="box-title">Accesories</h2>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Select Accessory</label><span class="text-red">*</span>
                  <ul class="listingstyle">
                    <li class="radio-inline"><input type="radio" name="accessory" value="sunglasses">SUNGLASSES</li>
                    <li class="radio-inline"><input type="radio" name="accessory" value="belts">BELTS</li>
                    <li class="radio-inline"><input type="radio" name="accessory" value="caps">CAPS</li>
                    <li class="radio-inline"><input type="radio" name="accessory" value="pocket_square">POCKET SQUARE</li>
                    <li class="radio-inline"><input type="radio" name="accessory" value="ties_and_bow_ties ">TIES AND BOW TIES </li>
                    <li class="radio-inline"><input type="radio" name="accessory" value="caps">CAPS</li>
                    <li class="radio-inline"><input type="radio" name="accessory" value="lapel_pins_and_collar_pins">LAPEL PINS  and COLLAR PINS</li>
                    <li class="radio-inline"><input type="radio" name="accessory" value="cuff_links ">CUFF LINKS </li>
                    <li class="radio-inline"><input type="radio" name="accessory" value="jewellery">JEWELLERY</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="row">
			<?php 
				/* echo "<pre>";
				print_r($sunglassesType);
				echo "</pre>"; */
			?>
              <div class="col-md-8">
                <div class="form-group">
                  <label class="control-label">Sunglasses</label><span class="text-red">*</span>
                  <ul class="listingstyle">
                    <li class="checkbox-inline"><input type="checkbox" <?php if(!empty($sunglassesType)){echo (in_array("aviators", @$sunglassesType))?"checked":""; } ?> name="sunglass" value="aviators">AVIATORS</li>
                    <li class="checkbox-inline"><input type="checkbox" <?php if(!empty($sunglassesType)){echo (in_array("wayfarers", @$sunglassesType))?"checked":"";  }?> name="sunglass" value="wayfarers">WAYFARERS</li>
                    <li class="checkbox-inline"><input type="checkbox" <?php if(!empty($sunglassesType)){echo (in_array("round", @$sunglassesType))?"checked":"";  }?> name="sunglass" value="round">ROUND</li>
                    <li class="checkbox-inline"><input type="checkbox" <?php if(!empty($sunglassesType)){echo (in_array("square", @$sunglassesType))?"checked":""; } ?> name="sunglass" value="squaaccessoryre">SQUARE</li>
                  </ul>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Reflectors</label>
                  <div>
                    <label class="radio-inline"><input type="radio" name="reflectors" <?php echo (@$refelectorType == "yes")?"checked":""; ?> value="yes">Yes</label>
                    <label class="radio-inline"><input type="radio" name="reflectors" <?php echo (@$refelectorType == "no")?"checked":""; ?> value="no">No</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
       </div> 

    </div>  
	  
    
	
	  <?php 
		} 
	  ?>
	</div>
    <div class="box">
      <div class="box-header">
        <h2 class="box-title">Optional</h2>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label" for="">IS THERE ANYTHING WE MISSED ?</label>
              <textarea class="form-control" name="comments" id="comments"  placeholder="" ></textarea>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label" for="">Image Upload</label>
              <input type="file"></input>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="box-footer text-right">
      <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i> Submit</button>
    </div>
    <!--  <div class="box-footer text-right">
      <button class="btn btn-sm btn-primary" onclick="place_scbox_order();"><i class="fa fa-save"></i> Place Order</button>
      </div> -->
  </form>
</section>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.js"></script>
<script>
  $(document).ready(function (){
  	$("#scbox-form").validate({
  	 ignore: 'input[type="hidden"]',
  	 rules: {
  		add1: {
  		 required:true,
  		},
  		pin: {
  			 required:true
  		},
  		city: {
  			required:true,
  		},
  		state: {
  			required:true,
  		},
  		country: {
  			required:true,
  		}					 
  	 },
  	 messages: {
  		add1: {
  		 required:'Please provide flat or house no.',
  		},
  		pin: {
  			 required:'Please provide pincode.',
  		},
  		city: {
  			required:'Please provide city.',
  		},
  		state: {
  			required:'Please provide state.',
  		},
  		country: {
  			required:'Please provide country.',
  		}
  	 },
  	 errorPlacement: function(error, element){
  		error.appendTo( element.parent().next('.help-block') );
  	},
  	submitHandler: function(form){
  	  $.post("<?php echo site_url('scbox/new_scbox_edit');?>",$(form).serialize(), function (data) {
  			
  			 if (data.msg == "success") {
  				$('#add-add-section').html('<p>Address has been saved successfully.</p>');
  				
  			}//
  			else
  			{
  				$('#add-add-section').html('<p class="text-danger">Some erro occured, please try after some time.</p>');
  			}
  			setTimeout(function(){
  			  window.parent.closeAddressModal();
  	
  			}, 1500);
  
  		}, "json");
  		
  	}
   });
  });
  
  
  
</script>
<script type="text/Javascript">
  function ShowHideDiv() {
          var chkYes = document.getElementById("chkYes");
          var recipient_address = document.getElementById("recipient_address");
          recipient_address.style.display = chkYes.checked ? "block" : "none";
      }
  
  $( document ).ready(function() {
  	$('.nav-item-scbox').addClass("active");
  	$('.nav-add-scboxdata').addClass("active");
  });
  var startDate = new Date('01/01/1990');
  var date_from = $("#date_from2").datepicker({ dateFormat: "yyyy-mm-dd hh:ii" , endDate: startDate , autoclose: true  });  
  
  function check_email(){
  	 var email = $('#email').val();
  	 $.ajax({
              url: "<?php echo base_url() ?>users/check_email",
              type: 'POST',
              data: {'email':email},
              cache :true,
              async: false,
              success: function(response) {
  				if(response == 'Email id already exists'){
  					$("#email_exists").val('1');
  				}else $("#email_exists").val('0');
              }
          });
  }
  
  /*$('#scbox-form').sisyphus({
   customKeyPrefix: '',
          timeout: 10,
          locationBased: true,
          autoRelease: true,
          onSave: function() {
            console.log('Data is now saved to Local Storage');
     }
  });*/
  
  /*function place_scbox_order()
  {
      var billing_first_name = '';
      var billing_last_name = ''; 
      var error = '';    
      var payment_mode = 'cod';      
      var pay_mod = payment_mode;    
      var page_type = $('#page-type').val(); 
  
      var regexp = /[^a-zA-Z\.]/g;
      var regexp_mobile = /[^0-9]/g;  
  
      var front_url = "<?php echo $this->config->item('front_url'); ?>";   
      var billing_first_name = $('#f_name').val().trim();
      var billing_last_name = $('#l_name').val().trim();
      var billing_mobile_no = $('#mobile_no').val().trim();
      var billing_pincode_no = $('#pincode').val().trim();
      var billing_address = $('#address').val().trim();
      var value = '';
      //var billing_city = $('#billing_city').val().trim();
     // var billing_state = $('#billing_state').val();
      billing_address = billing_address.replace(/[^a-zA-Z0-9\\-\\s\\b\/\\,\(\)]/gi, ' ');
      //billing_address = billing_address.replace(/\s+/g, " ");
  
     // var cart_total = $('#cart_total').val();
     // var scbox_price = $('#scbox_price').val();
  
      $('input[name=scbox_package]:checked').each(function() {
        value = $(this).attr('value');
      });
      alert('value--'+value);
      exit;
      //document.cookie = "scbox_cartotal="+cart_total+";path=/";
      var isTrue = 0;
  
      // if(billing_first_name.match(regexp) || billing_first_name==''){
      //   error = 'Enter Valid First Name';
      //   $('#cart_place_order_msg').html('Enter Valid First Name');
      //   isTrue = 1;
      // }else{
      //   document.cookie = "billing_first_name="+billing_first_name+";path=/";
      //   $('#cart_place_order_msg').html('');      
      // }
  
      // if(billing_last_name.match(regexp) || billing_last_name==''){
      //   error = 'Enter Valid Last Name';
      //   $('#cart_place_order_msg').html('Enter Valid Last Name');
      //   isTrue = 1;
      // }else{
      //   document.cookie = "billing_last_name="+billing_last_name+";path=/";
      //   $('#cart_place_order_msg').html('');    
      // }
  
      // if(billing_mobile_no.match(regexp_mobile) || billing_mobile_no=='' || billing_mobile_no.length < 10){
      //   error = 'Enter Valid Mobile No';
      //   $('#cart_place_order_msg').html('Enter Valid Mobile No');
      //   isTrue = 1;
      // }else{
      //   document.cookie = "billing_mobile_no="+billing_mobile_no+";path=/";
      //   $('#cart_place_order_msg').html('');     
      // }
  
      // if(billing_pincode_no.match(regexp_mobile) || billing_pincode_no=='' || billing_pincode_no.length < 6){
      //   error = 'Enter Valid Pincode';
      //   $('#cart_place_order_msg').html('Enter Valid Pincode');
      //   isTrue = 1;
      // }else{
      //   document.cookie = "stylecracker_shipping_pincode="+billing_pincode_no+";path=/";
      //   $('#cart_place_order_msg').html('');      
      // }
  
      // if(billing_address==''){
      //   error = 'Enter Shipping Address';
      //   $('#cart_place_order_msg').html('Enter Shipping Address');
      //   isTrue = 1;
      // }else{
      //   document.cookie = "billing_address="+billing_address+";path=/";
      //   $('#cart_place_order_msg').html('');      
      // }
  
      // if(billing_city==''){
      //   error = 'Enter Valid Pincode';
      //   $('#cart_place_order_msg').html('Enter Shipping City');
      //   isTrue = 1;
      // }else{
      //   document.cookie = "billing_city="+billing_city+";path=/";
      //   $('#cart_place_order_msg').html('');      
      // }
  
      // if(billing_state==''){
      //   error = 'Enter Shipping State';
      //   $('#cart_place_order_msg').html('Enter Shipping State');
      //   isTrue = 1;
      // }else{
      //   document.cookie = "billing_state="+billing_state+";path=/";
      //   $('#cart_place_order_msg').html('');      
      // }
  
      if(isTrue == 1){ 
        hide_cart_loader();
        $('#cart_place_order_msg').html('Please enter shipping address').removeClass('hide');
        $('#cart_place_order_msg').addClass('message error');
        $("#place_order_sc_cart").attr('disabled',true);  
      }
      else if(stock_exist > 0){ 
        hide_cart_loader();
        $('#cart_place_order_msg').html('Some of the products are out of stock').removeClass('hide');
        $("#place_order_sc_cart").attr('disabled',true);  
      }
      else if(pay_mod !='cod' && pay_mod != 'card'){
        hide_cart_loader();
        $('#cart_place_order_msg').html('Select payment option').removeClass('hide');
        $("#place_order_sc_cart").attr('disabled',true);  
      }else if(pay_mod =='cod' && codna>0){ 
        hide_cart_loader();     
        
        if(getCookie('discountcoupon_stylecracker') == 'NOCASH500' || getCookie('discountcoupon_stylecracker') == 'NOCASH1000'){ 
          
          $("#place_order_sc_cart").attr('disabled',true);      
          
          $('#cart_place_order_msg').html('COD is not avaliable for applied coupon code').removeClass('hide');
          
          }else{
          $('#cart_place_order_msg').html('COD is not available for some products').removeClass('hide');
        }
      }else if(shina > 0){
        hide_cart_loader();
        $('#cart_place_order_msg').html('Shipping is not available for some products').removeClass('hide');
        $("#place_order_sc_cart").attr('disabled',true);  
      }else{  
  
        var shipping_first_name = billing_first_name;
        var shipping_last_name = billing_last_name;
        var shipping_mobile_no = billing_mobile_no;
        var shipping_pincode_no = billing_pincode_no;
        var shipping_address = billing_address;
        var shipping_city = billing_city;
        var shipping_state = billing_state;
        var cart_email = $('#email').val().trim(); 
  
        var cart_total = $('#cart_total').val();
        var scbox_objectid = $('#scbox_objectid').val();
        var scbox_objectid = lastparam.split('?')[0];
        var cart_total = $('#cart_total').val();
        var scbox_price = getCookie('Scbox_price');
        var scbox_objectid = getCookie('scbox_objectid');
        var scbox_quantity = $('#scbox_quantity').val();
              
       // msg(is_order_placed+'==billing_first_name=='+billing_first_name+'==billing_last_name=='+billing_last_name+'==billing_mobile_no== '+billing_mobile_no+'==billing_pincode_no=='+billing_pincode_no+'==billing_address=='+billing_address+'==billing_city=='+billing_city+'==billing_state=='+billing_state+'==shipping_first_name=='+shipping_first_name+'==shipping_last_name=='+shipping_last_name+'==shipping_mobile_no=='+shipping_mobile_no+'==shipping_pincode_no=='+shipping_pincode_no+'==shipping_address=='+shipping_address+'==shipping_city=='+shipping_city+'==shipping_state=='+shipping_state+'==cart_email=='+cart_email+'==scbox_price=='+scbox_price);
  
        if(is_order_placed==false &&billing_first_name!='' && billing_last_name!='' && billing_mobile_no!='' && billing_pincode_no!='' && billing_address!='' && billing_city!='' && billing_state!='' && shipping_first_name!='' && shipping_last_name!='' && shipping_mobile_no!='' && shipping_pincode_no!='' && shipping_address!='' && cart_email!='' && scbox_price!=''){
         
          msg("before: is_order_placed: "+is_order_placed);
          is_order_placed = true;
          if(pay_mod == 'cod'){   
             $("#page-loader").show();
  
            $.ajax({
                    type: "post",
                    url: front_url+"scbox/place_order",
                    data: { billing_first_name:billing_first_name,billing_last_name:billing_last_name,billing_mobile_no:billing_mobile_no,billing_pincode_no:billing_pincode_no,billing_address:billing_address,billing_city:billing_city,billing_state:billing_state,shipping_first_name:shipping_first_name,shipping_last_name:shipping_last_name,shipping_mobile_no:shipping_mobile_no,shipping_pincode_no:shipping_pincode_no,shipping_address:shipping_address,shipping_city:shipping_city,shipping_state:shipping_state,cart_email:cart_email,pay_mod:pay_mod,cart_total:cart_total,scbox_price:scbox_price,scbox_objectid:scbox_objectid,scbox_quantity:scbox_quantity,page_type:page_type},
                    cache :true,
                    async: true,
                    success: function (response) {
                      // $("#page-loader").hide();  
                        hide_cart_loader(); remove_address_cookies('SHIP'); remove_address_cookies('BILL');
                        if(response!='')
                        { 
                          var final_cookies = '';
                          document.cookie="billing_address="+final_cookies+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";                     
                          //var redirction = front_url+'book-scbox/'+scbox_objectid+'?order='+response+'&step=6';
                         
                          // $('#order-message').html('Your SCBOX amount of Rs.'+cart_total+' has been booked. Please fill the below details to know more about you. (COD)');                      
                           $('#order-message').removeClass('hide');
                        }else
                        {
                          var redirction = front_url+'book-scbox/'+scbox_objectid+'?step=1';
                          // $('#order-message').html('Error');
                          // $('#order-message').removeClass('hide');
                        }
                       // var redirction = sc_baseurl+'scbox/ordersuccessOrder/'+response;
                        
                        // var final_cookies = '';
                        // document.cookie="SCUniqueID="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
                        // document.cookie="discountcoupon_stylecracker="+final_cookies+Math.floor((Math.random() * 10000) + 1)+"; expires=Thu, 18 Dec 2014 12:00:00 UTC; path=/";
                    window.location =redirction;
                    money_formatter(".price");
                    money_formatter(".cart-price");
                    },
                    error: function (response) {
                      hide_cart_loader();                    
                        msg('error=='+response);
                    }
                });
    }
    }
    }}*/
</script>