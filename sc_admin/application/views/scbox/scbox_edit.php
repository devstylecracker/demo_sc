<?php
// echo "<pre>";print_r($user_data);exit;
?>
<section class="content-header">
  <h1>SCBox Booking</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">SCBox Booking</li>
  </ol>
</section>
<section class="content">
  <form name="scbox-form" id="scbox-form" method="post" action="" onsubmit="return validateForm()">
  <div class="box">
    <div class="box-header">
      <h2 class="box-title">HELP US KNOW YOU BETTER</h2>
    </div>
    <div class="box-body">
        <div class="row">
          <div class="col-md-6 col-md-offset-6 col-md-pull-6">
            <div class="form-group">
              <label class="control-label">SCBox Package
                <span class="text-red">*</span></label>
				<label id="scbox_package-error" class="text-red hide" for="scbox_package">Please select scbox package.</label>
				<label id="scbox_package-error2" class="text-red hide" for="scbox_package">Please enter price of the box.</label>
              <div>
                <label class="radio-inline">
                  <input type="radio" name="scbox_package" id='scbox_package' value="276" <?php if($user_data['user_gender']['object_id'] == '276'){ echo "checked"; } ?> ><i class="fa fa-inr"></i> 2,999</label>
                <label class="radio-inline">
                  <input type="radio" name="scbox_package" id='scbox_package' value="275" <?php if($user_data['user_gender']['object_id'] == '275'){ echo "checked"; } ?> ><i class="fa fa-inr"></i> 4,999</label>
                <label class="radio-inline">
                  <input type="radio" name="scbox_package" id='scbox_package' value="274" <?php if($user_data['user_gender']['object_id'] == '274'){ echo "checked"; } ?> ><i class="fa fa-inr"></i> 6,999</label>
                <label class="radio-inline">
                  <input type="radio" name="scbox_package" id='scbox_package' value="277" <?php if($user_data['user_gender']['object_id'] == '277'){ echo "checked"; } ?> >
                  <input type="text" name="box_price" id='box_price' placeholder="Amount" pattern="[0-9]{1,}" class="form-control input-sm">
                </label>
                <span class="text-red"></span>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Select Gender</label>
			  <label id="scb_gender-error" class="text-red hide" for="scb_gender">Please select gender.</label>
				<span class="text-red">*</span></label>
              <select name="scb_gender" id="scb_gender" class="form-control input-sm" onchange="get_pa_attr()" required>
                <option value="">Select</option>
                <option <?php if($user_data['user_gender']['object_meta_value'] == '1'){ echo "selected"; } ?> value="1">Female</option>
                <option <?php if($user_data['user_gender']['object_meta_value'] == '2'){ echo "selected"; } ?> value="2">Male</option>
              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label" for="">First Name</label><span class="text-red">*</span></label>
              <input type="text" class="form-control" name="f_name" id="f_name" placeholder="First Name" required value=<?php echo $user_data['user_name']['object_meta_value']; ?> >
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label" for="">Last Name</label><span class="text-red">*</span></label>
              <input type="text" class="form-control" name="l_name" id="l_name" placeholder="Last Name" required value=<?php echo $user_data['user_name']['object_meta_value']; ?> >
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label" for="">Mobile (10 digit mobile number)</label><span class="text-red">*</span></label>
              <input type="text" class="form-control" name="mobile_no" id="mobile_no" placeholder="Mobile" maxlength="10" pattern="[1-9]{1}[0-9]{9}" value=<?php echo $user_data['user_mobile']['object_meta_value']; ?> required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label" for="">Email</label><span class="text-red">*</span></label>
              <input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="form-control" name="email" id="email" placeholder="Email" onblur="check_email()" value=<?php echo $user_data['user_emailid']['object_meta_value']; ?> required>
			  <input type="hidden" id="email_exists" value="0">
			  <span class="text-red hide" id="un_email"> Email already exists</span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Date Of Birth</label><span class="text-red">*</span></label>
              <input type="text" name="date_from" id="date_from" placeholder="Date Of Birth" class="form-control input-sm datepicker" style="height:34px;" required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label" for="">Pincode (6 digit Pincode)</label><span class="text-red" >*</span></label>
              <input type="text" class="form-control" name="pincode" id="pincode" maxlength="7" pattern="[0-9]{6}" placeholder="Pincode" required>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label" for="">Address</label><span class="text-red">*</span></label>
                  <textarea class="form-control" name="address" id="address" placeholder="Address" required></textarea>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label" for="">Tell us what you like to wear.</label>
                  <textarea class="form-control" name="user_like" id="user_like" placeholder="Tell us what you like to wear."></textarea>
                  <span class="help">Example: Ruffles, sneakers, backpack etc.</span>

                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label" for="">Tell us what you wouldn't want us to buy for you. </label>
                  <textarea class="form-control" name="user_dislike" id="user_dislike" placeholder="Tell us what you wouldn't want us to buy for you."></textarea>
                  <span class="help">Example: Sleeveless, high heels, etc.</span>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>

  <div class="box" >
    <div class="box-header">
      <h2 class="box-title">PA</h2>
	  <label id="pa-error" class="text-red hide" for="pa">Please Select all fields.</label>
    </div>
    <div class="box-body" id="women_pa">
        <div class="row" id="pa_attr">
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Body Shape</label><span class="text-red">*</span>
              <select name="user_body_shape_women" id="user_body_shape" class="form-control input-sm">
				<?php foreach($women_body_shape as $key=>$val){
					echo '<option value="'.$key.'">'.$val.'</option>';
				} ?>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Style Preferences 1</label><span class="text-red">*</span>
              <select name="user_style_p1_women" id="user_style_p1" class="form-control input-sm">
                <?php foreach($women_style as $key=>$val){
					echo '<option value="'.$key.'">'.$val.'</option>';
				} ?>
              </select>
            </div>
          </div>
		  <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Style Preferences 2</label><span class="text-red">*</span>
              <select name="user_style_p2_women" id="user_style_p2" class="form-control input-sm">
				<?php foreach($women_style as $key=>$val){
					echo '<option value="'.$key.'">'.$val.'</option>';
				} ?>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Style Preferences 3</label><span class="text-red">*</span>
              <select name="user_style_p3_women" id="user_style_p3" class="form-control input-sm">
                <?php foreach($women_style as $key=>$val){
					echo '<option value="'.$key.'">'.$val.'</option>';
				} ?>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Skin Tone</label><span class="text-red">*</span>
              <select name="user_skintone_women" id="user_skintone" class="form-control input-sm">
                <option value="0">Select</option>
				 <?php foreach($SCBOX_SKINTONE_LIST as $val){
					echo '<option value="'.$val->colorid.'">'.$val->name.'</option>';
				} ?>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Colour Palette</label><span class="text-red">*</span>
              <select name="user_colors_women" id="user_colors" class="form-control input-sm" multiple="">
                <option value="0">Select</option>
				 <?php foreach($SCBOX_COLOR_LIST as $val){
					echo '<option value="'.$val->id.'">'.$val->name.'</option>';
				} ?>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Prints</label><span class="text-red">*</span>
              <select name="user_prints_women" id="user_prints" class="form-control input-sm" multiple="">
                <option value="0">Select</option>
				 <?php foreach($SCBOX_PRINT_LIST as $val){
					echo '<option value="'.$val->id.'">'.$val->name.'</option>';
				} ?>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Shirt/T-shirt Size</label><span class="text-red">*</span>
              <select name="user_sizetop_women" id="user_sizetop" class="form-control input-sm">
                <option value="0">Select</option>
				<?php foreach($SCBOX_WOMENTOP_LIST as $val){
					echo '<option value="'.$val['id'].'">'.$val['answer'].'</option>';
				} ?>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">WAIST SIZE (INCH) </label><span class="text-red">*</span>
              <select name="user_sizebottom_women" id="user_sizebottom" class="form-control input-sm">
                <option value="0">Select</option>
				<?php foreach($SCBOX_WOMENBOTTOM_LIST as $val){
					echo '<option value="'.$val['id'].'">'.$val['answer'].'</option>';
				} ?>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Footwear (IND) </label><span class="text-red">*</span>
              <select name="user_sizefoot_women" id="user_sizefoot" class="form-control input-sm">
                <option value="0">Select</option>
				<?php foreach($SCBOX_WOMENFOOT_LIST as $val){
					echo '<option value="'.$val['id'].'">'.$val['answer'].'</option>';
				} ?>
              </select>
            </div>
          </div>
		  <div class="col-md-3">
            <div class="form-group">
              <label class="control-label"> Lingerie:  Band Size </label><span class="text-red">*</span>
              <select name="user_sizeband_women" id="user_sizeband" class="form-control input-sm">
                <option value="0">Select</option>
				<?php foreach($SCBOX_BANDSIZE_LIST as $val){
					echo '<option value="'.$val['id'].'">'.$val['answer'].'</option>';
				} ?>
              </select>
            </div>
          </div>
		  <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Cup Size </label><span class="text-red">*</span>
              <select name="user_sizecup_women" id="user_sizecup" class="form-control input-sm">
                <option value="0">Select</option>
				<?php foreach($SCBOX_CUPSIZE_LIST as $val){
					echo '<option value="'.$val['id'].'">'.$val['answer'].'</option>';
				} ?>
              </select>
            </div>
          </div>
        </div>
    </div>
	
	<div class="box-body hide" id="men_pa">
        <div class="row" id="pa_attr">
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Body Shape</label><span class="text-red">*</span>
              <select name="user_body_shape_men" id="user_body_shape" class="form-control input-sm">
				<?php foreach($men_body_shape as $key=>$val){
					echo '<option value="'.$key.'">'.$val.'</option>';
				} ?>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Style Preferences 1</label><span class="text-red">*</span>
              <select name="user_style_p1_men" id="user_style_p1" class="form-control input-sm">
                <?php foreach($men_style as $key=>$val){
					echo '<option value="'.$key.'">'.$val.'</option>';
				} ?>
              </select>
            </div>
          </div>
		  <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Style Preferences 2</label><span class="text-red">*</span>
              <select name="user_style_p2_men" id="user_style_p2" class="form-control input-sm">
				<?php foreach($men_style as $key=>$val){
					echo '<option value="'.$key.'">'.$val.'</option>';
				} ?>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Style Preferences 3</label><span class="text-red">*</span>
              <select name="user_style_p3_men" id="user_style_p3" class="form-control input-sm">
                <?php foreach($men_style as $key=>$val){
					echo '<option value="'.$key.'">'.$val.'</option>';
				} ?>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Skin Tone</label><span class="text-red">*</span>
              <select name="user_skintone_men" id="user_skintone" class="form-control input-sm">
                <option value="0">Select</option>
				 <?php foreach($SCBOX_SKINTONE_LIST as $val){
					echo '<option value="'.$val->colorid.'">'.$val->name.'</option>';
				} ?>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Colour Palette</label><span class="text-red">*</span>
              <select name="user_colors_men" id="user_colors" class="form-control input-sm" multiple="">
                <option value="0">Select</option>
				 <?php foreach($SCBOX_COLOR_LIST as $val){
					echo '<option value="'.$val->id.'">'.$val->name.'</option>';
				} ?>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Prints</label><span class="text-red">*</span>
              <select name="user_prints_men" id="user_prints" class="form-control input-sm" multiple="">
                <option value="0">Select</option>
				 <?php foreach($SCBOX_PRINT_LIST as $val){
					echo '<option value="'.$val->id.'">'.$val->name.'</option>';
				} ?>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Shirt/T-shirt Size</label><span class="text-red">*</span>
              <select name="user_sizetop_men" id="user_sizetop" class="form-control input-sm">
                <option value="0">Select</option>
				<?php foreach($SCBOX_MENTOP_LIST as $val){
					echo '<option value="'.$val['id'].'">'.$val['answer'].'</option>';
				} ?>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">WAIST SIZE (INCH) </label><span class="text-red">*</span>
              <select name="user_sizebottom_men" id="user_sizebottom" class="form-control input-sm">
                <option value="0">Select</option>
				<?php foreach($SCBOX_MENBOTTOM_LIST as $val){
					echo '<option value="'.$val['id'].'">'.$val['answer'].'</option>';
				} ?>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Footwear (IND) </label><span class="text-red">*</span>
              <select name="user_sizefoot_men" id="user_sizefoot" class="form-control input-sm">
                <option value="0">Select</option>
				<?php foreach($SCBOX_MENFOOT_LIST as $val){
					echo '<option value="'.$val['id'].'">'.$val['answer'].'</option>';
				} ?>
              </select>
            </div>
          </div>
        </div>
    </div>
  </div>

  <div class="box">
    <div class="box-header">
      <h2 class="box-title">Optional</h2>
    </div>
    <div class="box-body">
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Profession</label>
              <input type="text" name="user_profession" id="user_profession" class="form-control input-sm"/>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Occasion</label>
			   <div>
                  <label class="radio-inline"><input type="radio" name="user_occassion" id="user_occassion" value="CASUAL">CASUAL</label>
                  <label class="radio-inline"><input type="radio" name="user_occassion" id="user_occassion" value="WORK">WORK</label>
				  <label class="radio-inline"><input type="radio" name="user_occassion" id="user_occassion" value="PARTY">PARTY</label>
                  <label class="radio-inline"><input type="radio" name="user_occassion" id="user_occassion" value="PARTY">PARTY</label>
				  <label class="radio-inline"><input type="radio" name="user_occassion" id="user_occassion" value="5">Please Specify below</label>
				  <input type="text" name="user_occassion_text" id="user_occassion_text" class="form-control" />
                </div>
            </div>
          </div>
            </div>
          <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label" for="">What does your wardrobe mainly consist of?</label>
              <textarea class="form-control" name="user_wardrobe" id="user_wardrobe"  placeholder=""></textarea>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Willingness to experiment?</label>
                <div>
                  <label class="radio-inline">
                    <input type="radio" name="user_willtoexpmnt" id="user_willtoexpmnt"  value="1">Yes</label>
                  <label class="radio-inline">
                    <input type="radio" name="user_willtoexpmnt" id="user_willtoexpmnt"  value="0">No</label>
                  <span class="text-red"></span>
                </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label" for="">Any brand preferences? </label>
              <textarea class="form-control" name="scbox_brand" id="scbox_brand"  placeholder=""></textarea>
            </div>
          </div>

        </div>
      
    </div>

  </div>
   <div class="box-footer text-right">
      <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i> Submit</button>
   </div>
</form>
</section>
<script type="text/Javascript">
var date_from = $("#date_from").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii" , autoclose: true });  

function check_email(){
	 var email = $('#email').val();
	 $.ajax({
            url: "<?php echo base_url() ?>users/check_email",
            type: 'POST',
            data: {'email':email},
            cache :true,
            async: false,
            success: function(response) {
				if(response == 'Email id already exists'){
					$("#email_exists").val('1');
				}else $("#email_exists").val('0');
            }
        });
}
</script>