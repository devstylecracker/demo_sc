<?php 
// echo "<pre>";print_r($user_manual_data);exit;
$user_colors_array = explode(',',$user_manual_data['user_box_prints']);
?>
<section class="content-header">
  <h1>Add - Order Specific Data (<?php echo $this->uri->segment(3); ?>)</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">SCBox Order Specific Data</li>
  </ol>
</section>
<section class="content">
<form id="scbox-form" method="post" action="">
  <div class="box">
    <div class="box-header">
      <h2 class="box-title">Questions</h2>
    </div>
    <div class="box-body">
         <?php echo $this->session->flashdata('feedback_scbox'); ?>
         <input type="hidden" name="order_id" class="form-control" value="<?php echo $this->uri->segment(3); ?>" />
         <input type="hidden" name="user_id" class="form-control" value="<?php echo $this->uri->segment(4); ?>" />
      
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label" for="">How would you describe your personal style? </label>
              <textarea class="form-control" name="personal_style_desc" placeholder=""><?php echo $user_manual_data['personal_style_desc']; ?></textarea>
              <span class="help">Example: Preppy, Sporty, Classic, etc.</span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label" for="">What is your Colour Preference?  </label>
              <textarea class="form-control" name="user_box_color_pref" placeholder=""><?php echo $user_manual_data['user_box_color_pref']; ?></textarea>
              <span class="help">Example: Pop, Neutral, Earthy, Pastel, Monochrome, etc.</span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label" for="">Brands you generally opt for: </label>
              <textarea class="form-control" name="user_box_brand_pref" placeholder=""><?php echo $user_manual_data['user_box_brand_pref']; ?></textarea>
            <span class="help">&nbsp;</span>
            </div>
          </div>


          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label">How do you dress to work?
                </label>
            </div>
              <div class="row">
    <div class="col-md-2">
            <div class="form-group">
              <label class="control-label">Bottom</label>
              <div>
                <label class="radio-inline">
                  <input type="radio" name="user_box_bottom_pref" value="1" <?php if($user_manual_data['user_box_bottom_pref'] == '1'){ echo "checked"; } ?> >Dress</label>
                <label class="radio-inline">
                  <input type="radio" name="user_box_bottom_pref" value="2" <?php if($user_manual_data['user_box_bottom_pref'] == '2'){ echo "checked"; } ?> >Pants</label>
                <span class="text-red"></span>
              </div>
            </div>
          </div>
  <div class="col-md-2">
            <div class="form-group">
              <label class="control-label">Footewear</label>
              <div>
                <label class="radio-inline">
                  <input type="radio" name="user_box_footewear_pref" value="1" <?php if($user_manual_data['user_box_footewear_pref'] == '1'){ echo "checked"; } ?> >Flats</label>
                <label class="radio-inline">
                  <input type="radio" name="user_box_footewear_pref" value="2" <?php if($user_manual_data['user_box_footewear_pref'] == '2'){ echo "checked"; } ?> >Heels</label>
                <span class="text-red"></span>
              </div>
            </div>
          </div>
  <div class="col-md-2">
            <div class="form-group">
              <label class="control-label">Women Top</label>
              <div>
                <label class="radio-inline">
                  <input type="radio" name="user_box_women_top_pref" value="1" <?php if($user_manual_data['user_box_women_top_pref'] == '1'){ echo "checked"; } ?> >Blouse</label>
                <label class="radio-inline">
                  <input type="radio" name="user_box_women_top_pref" value="2" <?php if($user_manual_data['user_box_women_top_pref'] == '2'){ echo "checked"; } ?> >Shirt</label>
                <span class="text-red"></span>
              </div>
            </div>
          </div>
  <div class="col-md-2">
            <div class="form-group">
              <label class="control-label">Bottom </label>
              <div>
                <label class="radio-inline">
                  <input type="radio" name="user_box_men_top_pref" value="1" <?php if($user_manual_data['user_box_men_top_pref'] == '1'){ echo "checked"; } ?> >Blazer</label>
                <label class="radio-inline">
                  <input type="radio" name="user_box_men_top_pref" value="2" <?php if($user_manual_data['user_box_men_top_pref'] == '2'){ echo "checked"; } ?> >No Blazer</label>
                <span class="text-red"></span>
              </div>
            </div>
          </div>
  </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Prints</label>
              <div>
                <label class="radio-inline">
                  <input type="radio" name="user_box_prints_pref" value="1" <?php if($user_manual_data['user_box_prints_pref'] == '1'){ echo "checked"; } ?> >Prints</label>
                <label class="radio-inline">
                  <input type="radio" name="user_box_prints_pref" value="2" <?php if($user_manual_data['user_box_prints_pref'] == '2'){ echo "checked"; } ?> >Solids</label>
                <span class="text-red"></span>
              </div>
            </div>
          </div>
          <div class="col-md-8">
            <div class="form-group">
              <label class="control-label">If yes to prints, which of the following would you prefer?</label>
              <div>
                <div class="checkbox-inline">
                  <label>
                    <input type="checkbox" name="user_box_prints[]" value="checks" <?php if(in_array('checks',$user_colors_array)){ echo "checked"; } ?> > Checks</label>
                </div>
                <div class="checkbox-inline">
                  <label>
                    <input type="checkbox" name="user_box_prints[]" value="stripes" <?php if(in_array('stripes',$user_colors_array)){ echo "checked"; } ?> > Stripes</label>
                </div>
                <div class="checkbox-inline">
                  <label>
                    <input type="checkbox" name="user_box_prints[]" value="abstract" <?php if(in_array('abstract',$user_colors_array)){ echo "checked"; } ?> > Abstract</label>
                </div>
                <div class="checkbox-inline">
                  <label>
                    <input type="checkbox" name="user_box_prints[]" value="floral" <?php if(in_array('floral',$user_colors_array)){ echo "checked"; } ?> > Floral</label>
                </div>

                <span class="text-red"></span>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label">Would you be open to experimenting with your clothing? </label>
              <div>
                <label class="radio-inline">
                  <input type="radio" name="is_experimenting" value="1" <?php if($user_manual_data['is_experimenting'] == '1'){ echo "checked"; } ?> >Yes</label>
                <label class="radio-inline">
                  <input type="radio" name="is_experimenting" value="2" <?php if($user_manual_data['is_experimenting'] == '2'){ echo "checked"; } ?> >No</label>
                <span class="text-red"></span>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label" for="">Anything in specific that we should avoid </label>
              <textarea class="form-control" name="user_dislike" placeholder=""><?php echo $user_manual_data['user_dislike']; ?></textarea>
              <span class="help">Example: a particular style/silhouette, a colour, etc. </span>
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label" for="">Any other specifications? </label>
              <textarea class="form-control" name="other_specifications" placeholder="" rows="20"><?php echo $user_manual_data['other_specifications']; ?></textarea>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label class="control-label" for="">Height </label>
              <input type="text" name="height" class="form-control" value="<?php echo $user_manual_data['height']; ?>" />
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label class="control-label" for="">Weight </label>
              <input type="text" name="weight" class="form-control" value="<?php echo $user_manual_data['weight']; ?>" />
            </div>
          </div>


        </div>

    </div>
    <div class="box-footer text-right">
      <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i> Submit</button>
    </div>
  </div>
</form>
</section>
<!--<script type="text/javascript">
//  $('#scbox-form').sisyphus({
  //      timeout: 10,
//        onSave: function() {
//          console.log('Data is now saved to Local Storage');
//   }
//});
</script>-->