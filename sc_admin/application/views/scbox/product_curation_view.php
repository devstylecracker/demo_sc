<style type="text/css"> 
table {
    border-collapse: collapse;
    width: 100%;
    margin-bottom:5px; 
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
    font-size: 12px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
a{
  cursor: pointer;
}
body{
  overflow-y: hidden;
  font-size: 12px;
}
.skin-blue .wrapper, .skin-blue .main-sidebar, .skin-blue .left-side {
    background-color: #d3d3d3;
}
.content {
    min-height: 250px;
    padding: 0px;
    margin-right: auto;
    margin-left: auto;
    padding-left: 5px;
    padding-right: 5px;
}
#categories-adjust {
     display:table;
}
 #categories-adjust .col {
     display:table-cell !important;
     padding-right: 5px;
     width: 16% 
}
/* Small screens */
 @media all and (max-width: 500px) {
     .mypanel, .mypanel .col {
         display:block;
    }
}
 .products-list{
    display: table;
    margin: 0 auto;
}
 .product-details-inner{
    min-height: 100%;
    height: 420px;
    overflow: scroll;
}

.btn-box-tool {
    color: #a52a2a;
    font-weight: 700;
}
 .products-list.search-results > .item{
    width:120px;
     vertical-align:top;
    border: 1px dashed #ccc;
    margin:3px;
}
 .products-list.search-results > .item .desc{
    font-size:9px;
     height:70px;
     overflow:hidden;
    color:#000;
     clear:both;
    font-weight: bold;
    letter-spacing: 1px;
    padding-left: 3px;
}
 .products-list.search-results > .item.show-more{
    line-height:110px;
    /*background:#00B19C;
     color:#fff;
     font-weight:bold;
     cursor:pointer;
    */
     text-align:center;
}
 .products-list.search-results > .item.show-more:hover{
    opacity:0.85;
}
 .products-list .product-img {
     width: 118px;
     height: 110px;
     border: 1px solid #eee;
     text-align: center;
     position: relative;
     margin: 0 auto;
}
 .products-popup .products-list.search-results > .item{
    margin-bottom:10px;
     width:120px;
     vertical-align:top;
}
 .products-popup .products-list.search-results > .item .desc{
    font-size:11px;
     height:46px;
     overflow:hidden;
    color:#999;
     clear:both;
}
 .products-popup .products-list.search-results > .item.show-more{
    line-height:110px;
    /*background:#00B19C;
     color:#fff;
     font-weight:bold;
     cursor:pointer;
    */
     text-align:center;
}
 .products-popup .products-list.search-results > .item.show-more:hover{
    opacity:0.85;
}

.desc{
  padding-left: 5px;
  padding-bottom: 4px;
}
.products-list>.item{
  padding: 2px 0;
}
tr:nth-child(even) {
    background-color: #fff;
}
    .box{box-shadow: 0 1px 1px rgba(0,0,0,0.5) !important;}
    .b-color{border-color: #00B19C;}

    .col{
      text-align: center;
    }



    #categories-adjust {
          display:table;
          width:100%;
        }
        #categories-adjust .col {
          display:table-cell !important;
          padding-right: 5px;
          width: 16%
        }
        
      /* Small screens */
        @media all and (max-width: 500px) {
          .mypanel, .mypanel .col {
              display:block;
          }
        }
 </style>


<section class="content">
  <div class="box">
    <div class="box-body">
      <div class="row">
        <div class="col-sm-9">
          <div style="text-align: center;">
                <ul class="nav nav-tabs nav-justified">
                   <li class="active"><a data-toggle="tab" href="#recommended" onclick="getProductRecommendation(<?php echo $order_data['cart_id']; ?>,<?php echo '\''.$order_data['order_display_no'].'\''; ?>)" ><b>Recommended Product</b></a></li>
                   <li><a data-toggle="tab" href="#manual"><b>Manual Curation</b></a></li>
                </ul>
                <input type="hidden" id="popup_button" data-attr="scbox-order-products" >
                <input type="hidden" id="popup_page_type" data-attr="scbox-order-products" value="scbox-order-products-panel" >
                <input type="hidden" id="cart_id" value="<?php echo $order_data['cart_id']; ?>" >
                <input type="hidden" id="selected_orderid" value="<?php echo $order_data['order_display_no']; ?>">
                <input type="hidden" id="max_curation_limit_set" value="<?php echo $package_cost['max_box_cost']; ?>">
            </div>
        </div>
        <div class="col-sm-3">
           <div class="row">
            <div class="col-sm-3">
               <div>
         <!--  <a onclick="myFunction()">s1</a> -->

                <!--  <script>
                  function myFunction() {
                      window.open("http://localhost/projectbitb/sc_admin/manage_new", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=0,left=0,width=1340,height=640");
                  }
                  </script> -->
                </div>
               <label class="control-label" style="color:#00B19C;"> 
                <span style="font-weight: bold;"><i class="fa fa-inr"></i> <?php echo (int)$order_data['product_price']; ?> Box</span></label>
            </div>
         
            <div class="col-sm-9">
               <form>
                  <!-- <div class="input-group">
                    <span class="input-group-addon">Max.Product Limit:</span>
                     <input type="text" class="form-control" placeholder="">
                     <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                        OK
                        </button>
                     </div>
                  </div> -->
               </form>
               <div class="input-group">
                    <span class="input-group-addon" style="font-size: 10px;font-weight: bold;">Curation Limit</span>                     
                      <input type="text" id="max_box_curation_limit" class="form-control" placeholder="" value="<?php echo ($package_cost['max_box_cost_overide']>$package_cost['max_box_cost']) ? $package_cost['max_box_cost_overide'] : $package_cost['max_box_cost']; ?>" disabled>
                     <div class="input-group-btn">
                        <button class="btn btn-default b-color" data-toggle="modal" data-target="#curationModal">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </button>
                     </div>
                  </div>
                    <div class="modal fade" id="curationModal" role="dialog">
                      <div class="modal-dialog">                      
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add Curation Amount</h4>
                          </div>
                          <div class="modal-body">
                             <div class="row">
                               <form class="form" action="">
                                <div class="form-group">
                                     <label>Box Max Limit: </label>
                                   <input type="text" id="max_box_curation_limit_" class="form-control"  value="<?php echo ($package_cost['max_box_cost_overide']>$package_cost['max_box_cost']) ? $package_cost['max_box_cost_overide'] : $package_cost['max_box_cost']; ?>" disabled>
                                 </div>
                                 <div class="form-group hidelowval">
                                    <label for="overideCurationAmt">Amount to proceed: </label><?php echo ($package_cost['max_box_cost_overide']>$package_cost['max_box_cost']) ? $package_cost['max_box_cost_overide'] : $package_cost['max_box_cost']; ?>+<label class="balance_amount_1"></label>
                                    <input class="form-control" id="overideCurationAmt">
                                  </div>
                                 <div class="form-group hidelowval">
                                    <label for="password">Enter Password: </label>
                                    <input type="password" class="form-control" id="overideCurationPassword">
                                  </div>
                                  
                                </form>
                             </div>
                          </div>
                          <div class="modal-footer">
                            <!-- <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="overideCurationAmt()">Submit</button> -->
                             <a class="btn btn-primary" id="add_curation_amt" onclick="overideCurationAmt()">Submit</a>
                          </div>
                        </div>
                        
                      </div>
                    </div>
            </div>
           
         
            
         </div> 
        </div> 
      </div>
    <div class="row">
        <div class="col-sm-9">
             <div class="tab-content" style="min-height:100%; position:relative;">
            <div id="recommended" class="tab-pane fade in active"  style="min-height: 450px;">
               <!-- <div class="row">
                  <div class="col-md-12" id="categories-adjust">
                     <div class="col">
                        <div style="font-weight: 700;text-transform: uppercase;">Apparel</div>
                        <div class="box box-primary box-body-products" style="max-height: 400px;overflow: scroll;">
                           <div class="products-list-wrp">                             
                             <ul id="recom_product_list" class="products-list product-list-in-box product-list-inline hover search-results">
                             </ul>
                           </div>
                        </div>
                     </div>
                  </div>                  
               </div> -->
            </div>
            <div id="manual" class="tab-pane fade">
               <div class="row">
                  <div class="col-md-12">
                    <div style="font-weight: 700;text-transform: uppercase;">ALL CATEGORY PRODUCTS</div>
                     <div class="box box-primary"  style="overflow: scroll;height: 420px;">
                        <input id="selected_orderid" name="selected_orderid" type="hidden" value="">
                        <!-- /.box-header -->
                        <div class="box-body">
                           <div class="row">
                              <div class="col-md-2">
                                 <div class="form-group1">
                                    <select name="gender_n" id="gender_n" class="form-control input-sm inline select2" style="width:100%;" data-placeholder="Select Gender">
                                       <option value=""></option>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-md-2">
                                 <div class="form-group1">
                                    <select name="child_cat" id="child_cat" class="form-control input-sm inline select2" style="width:100%;" data-placeholder="Category">
                                       <option value=""></option>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-md-3">
                                 <div class="form-group1">
                                    <select name="brand_id" id="brand_id" class="form-control input-sm inline select2" style="width:100%;" data-placeholder="Brands">
                                       <option value=""></option>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="form-group1">
                                    <!--<input type="text" name="popup_tags" id="popup_tags" class="form-control" style="width:100%;">-->
                                    <!--<select name="popup_tags[]" id="popup_tags" multiple class="form-control select2" style="width:100%;">
                                       </select>-->
                                    <div class="side-by-side clearfix">
                                       <div>
                                          <select data-placeholder="Choose a Tags..." style="width:100%;" id="popup_tags" name="popup_tags" class="form-control input-sm inline select2" tabindex="2">
                                             <option value=""></option>
                                             <?php 
                                                if(is_array($getAllTag) && count($getAllTag)>0)
                                                 {
                                                   foreach($getAllTag as $getTag)
                                                   {
                                                     ?>
                                             <option value="<?php echo $getTag['slug']; ?>"><?php echo $getTag['name']; ?></option>
                                             <?php
                                                }
                                                } 
                                                
                                                ?>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-1">
                                 <div class="form-group1">
                                    <button id="search_product" name="search_product" class="btn btn-default btn-sm">Search</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body box-body-products">
                           <div class="products-list-wrp">
                              <div class="loaders"></div>
                              <ul id="product_list" class="products-list product-list-in-box product-list-inline hover search-results text-center">
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>                  
               </div>
            </div>
        </div>
        </div>
        <div class="col-sm-3">
           <div class="col" style="width: 100%;">
              <div style="font-weight: 700;text-transform: uppercase;"> Product Details</div>
              <div class="box box-primary box-product-details" id="single_product_desc">
                 
              </div>
           </div>
        </div>
    </div>
     <div class="box-footer box box-primary"><!--  style="position: fixed;bottom: 0;width: 70%" -->
               <div class="row">
                  <div class="col-md-10">
                     <div class="products-list-wrp">
                        <ul id="product_list_curation" class="products-list product-list-in-box product-list-inline thumbs">
                                                    
                        </ul>
                     </div>
                  </div>
                  <div class="col-md-2">
                    <div class="row">
                      <div class="col-md-12">
                        <table>
                          <tr>
                            <th>Curated</th>
                            <th>Balance</th>
                          </tr>
                          <tr>
                            <td style="background: #00B19C;color: #fff;" class="curated_amount"><i class="fa fa-inr"></i>0</td>
                            <td style="background: #a52a2a;color: #fff;" class="balance_amount"><i class="fa fa-inr"></i>0</td>
                          </tr>
                        </table>
                      </div>  
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                         <button type="button" class="btn btn-danger pull-right" onclick="save_product_data()">Add to box</button>
                      </div>
                    </div>
                    
                  </div>
               </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

$(document).ready(function(e){
  getProductRecommendation(<?php echo $order_data['cart_id']; ?>,<?php echo '\''.$order_data['order_display_no'].'\''; ?>);

  InitCurationCost();
  setTimeout(function(){ ReCheckCurationLimit(); console.log("triggereddddds."); },500);
  


});
      $('div.product-img img').error( function() {
            $(this).attr('src', 'http://www.adbazar.pk/frontend/images/default-image.jpg');
            });

/*


var arrPrice = [];
    $.each($("ul#product_list_curation li"), function(i, value) { 
            arrPrice.push($(this).find("div p.price").text());
    });
    var arrPricetrim = arrPrice.map(Number);
    var sum = arrPricetrim.reduce(add);

      function add(a, b) {
          return a + b;
      }
      console.log('arrPricetrim--'+arrPricetrim);
      console.log('sum--'+sum);

*/


</script>