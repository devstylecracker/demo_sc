<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<!--
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
-->
<!-- search select box implementation -->
<!--<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js" type="text/javascript"></script>-->

<section class="content-header">
  <h1>Add Banner</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>home">Website UI</a></li>
    <li class="active">Homepage Slider</li>
  </ol>
</section>

<section class="content">


  <div class="box">
    <form role="form" id="product_form" enctype="multipart/form-data" method="POST">
      <div class="box-header with-border">
        <h3 class="box-title">Slide</h3>
        
        
        
        <div class="form-inline pull-right">
          <label class="form-label">Slide Order
            <select class="form-control" name="order">
              <option>
                1
              </option>
              <option>
                2
              </option>
              <option>
                3
              </option>
              <option>
                4
              </option>
              <option>
                5
              </option>
            </select>
          </label>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <input  value="" placeholder="Slide Caption Line 1" name="line1" id="line1" class="form-control" type="text">
            </div>
            <div class="form-group">
              <input  value="" placeholder="Slide Caption Line 2" name="line2" id="line2" class="form-control" type="text">
            </div>
          </div>
          <div class="col-md-2">
            <!-- <div class="form-group">
              <input   value="" placeholder="Label for Link Button" name="button_text"  id="button_text" class="form-control hide" type="text">
            </div> -->
            <div class="form-group">
              <input  value="" placeholder="URL" class="form-control" type="text" id="url" name="url">
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group" style="display: none;">
              <label class="radio-inline"><input type="radio" name="status" id="status" value = "1" checked> Active</label>
              <label class="radio-inline"><input type="radio" name="status" id="status" value = "0"> Deactive</label>
            </div>
              <div id ="daterange" style="display:;">
										
				  <div class="form-group">
					<input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm datetimepicker" id="date_from" value="<?php echo set_value('date_from'); ?>">
					<span class="text-red"> <?php echo form_error('date_from'); ?></span>
				  </div>           
				
				  <div class="form-group">
					<input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="<?php echo set_value('date_to'); ?>"> 
					<span class="text-red"> <?php echo form_error('date_to'); ?></span>					
				  </div>
					
				</div>
			<span class="text-red"> <?php if(isset($date_error)) { echo $date_error[0]; } ?></span>
            
          </div>
          <div class="col-md-3">
		  <div class="upload-banner" id="upload_banner" >Upload Banner</div>
            <div id="show_image"> <img name ="banner_image" id="previewing" src="" value="" /> </div>
			<span class="help-block">Max file weight: 130KB<br/>WD/HT: 1366px/371px<br/>Allowed format: .jpg, .jpeg, .png, .gif</span>
			<span class="text-red"> <?php if(isset($error)) { echo $error[0]; } ?></span>
          </div>
          <div class="col-md-1">

            <div class="form-group">
              <div class="btn btn-default btn-file btn-xs"> Browse<input id="file" name="file" class="file" type="file" onchange="change_img()"></div>
			 
			 <!--  <input value="" placeholder="Upload" class="form-control input-sm" type="file">-->
            </div>
           
			<button class="btn btn-sm btn-primary" tabindex="30" type="submit"><i class="fa fa-save"></i> Save</button>
          </div>
        </div>

      </div>
    </form>
  </div>


</section>
<style>
.upload-banner {
	border: 2px solid blue;
	font-weight: bold;
	height: 70px;
	padding: 25px 0 0;
	text-align: center;
	width: 90%;	
}

</style>

<script type="text/javascript">
 $(document).ready(function(){
	$('#show_image').hide();
 });
 $(function () {
	var date_from =  $("#date_from").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii" , autoclose: true });
	var date_to = $("#date_to").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii", autoclose: true });
	});
 function imageIsLoaded(e) {
  $("#file").css("color","green");
  $('#image_preview').css("display", "block");
  $('#previewing').attr('src', e.target.result);
  $('#previewing').attr('width', '250px');
  $('#previewing').attr('height', '70px');
  $('#show_image').show();
  $('#upload_banner').hide();
  };
   
	$(function () {
		// $("#date_from").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true});
		// $("#date_to").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true });     
		$(".nav-tabs a").click(function(){
		$(this).tab('show');
		});
		$('.nav-tabs a').on('shown.bs.tab', function(event){
		var x = $(event.target).text();         // active tab
		var y = $(event.relatedTarget).text();  // previous tab
		$(".act span").text(x);
		$(".prev span").text(y);
		});
	});
	
  function change_img(){
   $("#message").empty(); // To remove the previous error message
  var file = $("#file").get(0).files[0];
  var imagefile = file.type;
  var match= ["image/gif","image/png","image/jpg","image/jpeg"];
  if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]) || (imagefile==match[3])))
  {
  $('#previewing').attr('src','noimage.png');
  $("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
  return false;
  }
  else
  {
  var reader = new FileReader();
  reader.onload = imageIsLoaded;
  reader.readAsDataURL($("#file").get(0).files[0]);
  }
  }


  $( document ).ready(function() {
    $('#loading').hide();
    $('.nav-item-banners').addClass("active");
    $('.nav-banners-add').addClass("active");
	
    $('.datepicker').datepicker();
    
    });

  // Function to preview image after validation
  /*$(function() {
  $("#file").change(function() {
  function change_img(){
  $("#message").empty(); // To remove the previous error message
  var file = this.files[0];
  var imagefile = file.type;
  var match= ["image/jpeg","image/png","image/jpg"];
  if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
  {
  $('#previewing').attr('src','noimage.png');
  $("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
  return false;
  }
  else
  {
  var reader = new FileReader();
  reader.onload = imageIsLoaded;
  reader.readAsDataURL(this.files[0]);
  }

  }
  });
  });*/

 
</script>