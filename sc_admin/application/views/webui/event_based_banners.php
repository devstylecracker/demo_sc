<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<!--
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
-->
<!-- search select box implementation -->
<!--<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js" type="text/javascript"></script>-->

<section class="content-header">
  <h1>Manage Banners</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>home">Website UI</a></li>
    <li class="active">Homepage Slider</li>
  </ol>
</section>

<section class="content">
<form id="myForm">
<div class="box">
 <div class="box-header with-border">
<h3 class="box-title">Active Banners</h3>
 <div class="pull-right"><a href="<?php echo base_url(); ?>webui/Banners/add"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add Banner</button></a>
</div></div>
<div class="box-body">
 <?php 
 
 $count = count($data);
 $i = 1;
 // $data = array_slice($data,0,5);
 foreach($active_banners as $row){ ?>
 <div class="box box-solid" style="background: #eee;
    border: 1px solid #e1e1e1;" >
  
    <div class="box-header">
      <h3 class="box-title">Banner <?php echo $i; ?></h3>
      <div class="form-inline pull-right">
        <label class="form-label">Slide Order
          <select class="form-control" name="order" id="order<?php echo $row['id']; ?>">
            <option value="1" <?php if($row['banner_position'] == 1){ echo 'selected="selected"';}?> >
              1
            </option>
            <option value="2" <?php if($row['banner_position'] == 2){ echo 'selected="selected"';}?>>
              2
            </option>
            <option value="3" <?php if($row['banner_position'] == 3){ echo 'selected="selected"';}?> >
              3
            </option>
            <option value="4" <?php if($row['banner_position'] == 4){ echo 'selected="selected"';}?>>
              4
            </option>
            <option value="5" <?php if($row['banner_position'] == 5){ echo 'selected="selected"';}?>>
              5
            </option> 
          </select>
        </label>
      </div>
    </div><!-- /.box-header -->
    <div class="box-body" style="background-color111:#519aaa;">
      <div class="row" >
        <div class="col-md-4">
          <div class="form-group">
            <input  value="<?php echo $row['caption_line_1']; ?>" id="line1<?php echo $row['id']; ?>" name="line1<?php echo $row['id']; ?>" placeholder="Slide Caption Line 1" class="form-control" type="text">
          </div>
          <div class="form-group">
            <input  value="<?php echo $row['caption_line_2']; ?>" id="line2<?php echo $row['id']; ?>" name="line2<?php echo $row['id']; ?>" placeholder="Slide Caption Line 2" class="form-control" type="text">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <input   value="<?php echo $row['banner_url']; ?>" name="button_text"  id="button_text<?php echo $row['id']; ?>" placeholder="javascript function" class="form-control" type="text">
          </div>
          <div class="form-group">
            <input  value="<?php echo $row['lable_link']; ?>" placeholder="URL" id="url<?php echo $row['id']; ?>" name="url" class="form-control" type="text">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group radio-group">
			 <label class="radio-inline"><input type="radio" name="status<?php echo $row['id']; ?>" value = "active" checked> Active</label>
            <label class="radio-inline"><input type="radio" name="status<?php echo $row['id']; ?>" value = "deactive"> Deactive</label>
          </div>
          <div id ="daterange<?php echo $row['id']; ?>" style="display:;">
										
				  <div class="input-group">
					<input type="text" name="date_from<?php echo $row['id']; ?>"  placeholder="Date From" class="form-control input-sm datetimepicker" id="date_from<?php echo $row['id']; ?>" value="<?php echo $row['start_date']; ?>">
				  </div>           
				
				 <div class="input-group">
					<input type="text" name="date_to<?php echo $row['id']; ?>" placeholder="Date To" class="form-control input-sm datetimepicker" id="date_to<?php echo $row['id']; ?>" value="<?php echo $row['end_date']; ?>">          
				  </div>
					
			</div>
        </div>
        <div class="col-md-3">
		  <img src="<?php echo base_url(); ?>assets/banners/<?php echo $row['slide_url']; ?>">

        </div>
        <div class="col-md-1">

         <!-- <div class="form-group">
            <div class="btn btn-default btn-file btn-xs"> Browse<input name="image" class="file" type="file"></div>           
		 </div>-->
		 <div class="form-group">
          <button class="btn btn-primary btn-sm save-btn" slide_id="<?php echo $row['id']; ?>"><i class="fa fa-save"></i> Save</button>
</div>		  
		  <div class="form-group">
		  <button class="btn btn-primary btn-delete btn-sm" onclick="remove_banner(<?php echo $row['id']; ?>);"><i class="fa fa-trash"></i> Delete</button>
		  </div>
        </div>
      </div>

    </div>
 
</div>

<?php $i++; } ?>
</form>
 </div>
 </div>
 <div class="box">
 <div class="box-header with-border">
<h3 class="box-title">Upcoming Banners</h3>
</div>
<div class="box-body">
 <?php 
 $count = count($data);
 //$i = 1;
 // $data = array_slice($data,0,5);
 foreach($upcomming_banners as $row){ ?>
  <div class="box box-solid" style="background: #eee;
    border: 1px solid #e1e1e1;" >  
    <div class="box-header">
      <h3 class="box-title">Banners <?php echo $i; ?></h3>
      <div class="form-inline pull-right">
        <label class="form-label">Slide Order
          <select class="form-control" name="order" id="order<?php echo $row['id']; ?>" >
            <option value="1" <?php if($row['banner_position'] == 1){ echo 'selected="selected"';}?> >
              1
            </option>
            <option value="2" <?php if($row['banner_position'] == 2){ echo 'selected="selected"';}?>>
              2
            </option>
            <option value="3" <?php if($row['banner_position'] == 3){ echo 'selected="selected"';}?> >
              3
            </option>
            <option value="4" <?php if($row['banner_position'] == 4){ echo 'selected="selected"';}?>>
              4
            </option>
            <option value="5" <?php if($row['banner_position'] == 5){ echo 'selected="selected"';}?>>
              5
            </option> 
          </select>
        </label>
      </div>
    </div><!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <input  value="<?php echo $row['caption_line_1']; ?>" placeholder="Slide Caption Line 1" id="line1<?php echo $row['id']; ?>" name="line1<?php echo $row['id']; ?>" class="form-control" type="text">
          </div>
          <div class="form-group">
            <input  value="<?php echo $row['caption_line_2']; ?>" placeholder="Slide Caption Line 2" id="line2<?php echo $row['id']; ?>" name="line2<?php echo $row['id']; ?>" class="form-control" type="text">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <input value="<?php echo $row['banner_url']; ?>" placeholder="javascript function" name="button_text"  id="button_text<?php echo $row['id']; ?>" class="form-control" type="text">
          </div>
          <div class="form-group">
            <input  value="<?php echo $row['lable_link']; ?>" placeholder="URL" class="form-control" type="text" id="url<?php echo $row['id']; ?>" name="url">
          </div>
        </div>
        <div class="col-md-2">
        <div class="form-group radio-group">
			 <label class="radio-inline"><input type="radio" name="status<?php echo $row['id']; ?>" value = "active" checked> Active</label>
            <label class="radio-inline"><input type="radio" name="status<?php echo $row['id']; ?>" value = "deactive"> Deactive</label>
          </div>
          <div id ="daterange<?php echo $row['id']; ?>" style="display:;">
										
				  <div class="input-group">
					<input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm datetimepicker" id="date_from<?php echo $row['id']; ?>" value="<?php echo $row['start_date']; ?>">
				  </div>           
				
				  <div class="input-group">
					<input type="text" name="date_to" placeholder="Date To" class="form-control input-sm datetimepicker" id="date_to<?php echo $row['id']; ?>" value="<?php echo $row['end_date']; ?>">          
				  </div>
					
			</div>
        </div>
        <div class="col-md-3">
		  <img src="<?php echo base_url(); ?>assets/banners/<?php echo $row['slide_url']; ?>">

        </div>
        <div class="col-md-1">

          <div class="form-group">
            <div class="btn btn-default btn-file btn-xs"> Browse<input name="image" class="file" type="file"></div>
            <!--  <input value="" placeholder="Upload" class="form-control input-sm" type="file">-->
          </div>
		  <div class="form-group">
          <button class="btn btn-primary btn-sm save-btn" slide_id="<?php echo $row['id']; ?>" ><i class="fa fa-save"></i> Save</button>
		  </div>
		  <div class="form-group">
		  <button class="btn btn-primary btn-delete btn-sm" onclick="remove_banner(<?php echo $row['id']; ?>);"><i class="fa fa-trash"></i>Delete</button>
		  </div>
        </div>
      </div>

    </div>
  </div>


<?php $i++;} ?>
  </div>
  </div>
</section>

<script type="text/javascript">

$(function () {
	//var date_from =  $("#date_from").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii" , autoclose: true });
	//var date_to = $("#date_to").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii", autoclose: true });
	
	var datetime = $(".datetimepicker").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii", autoclose: true });
	
	});
  $( document ).ready(function() {
    $('.nav-item-banners').addClass("active");
    // $('.nav-banners-list').addClass("active");
	$('.nav-banners-list2').addClass("active");
    //$('.datepicker').datepicker();
	
	$('.save-btn').on('click',function(){
		var id = $(this).attr('slide_id');
		var status = $(this).closest('.row').find("input[name='status"+id+"']:checked").val();
		update_banner(id,status)
	});

	
    });
	
	function msg(m){ console.log(m); }
	
	function update_banner(id,status){
		var order = $('#order'+id).val();
		var line1 = $('#line1'+id).val();
		var line2 = $('#line2'+id).val();
		//var status =  $('#status39').val();
		var date_from =  $('#date_from'+id).val();
		var date_to = $('#date_to'+id).val();
		var button_text = $('#button_text'+id).val();
		var url = $('#url'+id).val();
		// alert($('input[name="status"+id]:checked').val()); 
		// alert(status);
		var result = window.confirm('Are you sure?');
		if(result == true)
        {
			if(id != null){
					$.ajax({
						url: "<?php echo base_url() ?>webui/Banners/update_banner",
						type: 'POST',
						data: {'id':id,'line1':line1,'line2':line2,'status':status,'date_from':date_from,'date_to':date_to,'button_text':button_text,'url':url,'order':order},
						cache: false,
						async: false,
						success: function(response) {
							alert("Data Saved Sucessfully");
							if(response == 1)
							{
								location.reload();
							}else{
								location.reload();
							}

						}
					});
				  }	
		}
		
	}
	function remove_banner(id){
		var result = window.confirm('Are you sure?');
		if(result == true)
        {
			if(id != null){
					$.ajax({
						url: "<?php echo base_url() ?>webui/Banners/remove_banner",
						type: 'POST',
						data: {'id':id},
						cache: false,
						async: false,
						success: function(response) {
							alert("Banner Removed Sucessfully ");
							if(response == 1)
							{
								location.reload();
							}else{
								location.reload();
							}

						}
					});
				  }	
		}
		else {}
	}
</script>