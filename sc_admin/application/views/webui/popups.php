<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>

<section class="content-header">
        <h1>Manage Popups</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>">Home</a></li>
            <li class="active">Manage Popups</li>
          </ol>
</section>

<section class="content">
  <div class="row">
   <div class="col-md-4">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">
          <?php if($this->uri->segment(3) > 0){ ?>
          Edit Popup
          <?php }else{ ?>
            Add New Popup
          <?php } ?>
        </h3>
    </div><!-- /.box-header -->
<div class="box-body">
<?php if($this->uri->segment(4) > 0){ ?>
  <form name="frmcategory" id="frmcategory" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>webui/popups/popups_edit/<?php echo $this->uri->segment(4); ?>">
<?php }else{ ?>
  <form name="frmcategory" id="frmcategory" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>webui/popups">
<?php } ?>
    <div class="form-group">
      <label class="control-label">Name</label>
      <input type="text" placeholder="Popup Name" name="popup_name" id="popup_name" value="<?php if(isset($cat_data_data[0]['popup_name'])){ echo @$cat_data_data[0]['popup_name']; }else{ echo @$_POST['popup_name']; } ?>" class="form-control"> <span class="text-red"><?php echo form_error('popup_name'); ?></span>
      <p class="help-block">The name is not display on the site ( Only for your reference ).</p>
    </div>

    <div class="form-group">
      <!--label class="control-label">Content</label-->
      <input type="hidden" placeholder="Content" name="popup_content" id="popup_content" value="<?php if(isset($cat_data_data[0]['popup_content'])){ echo @$cat_data_data[0]['popup_content']; }else{ echo @$_POST['popup_content']; } ?>" class="form-control"> <span class="text-red"><?php echo form_error('popup_content'); ?></span>
      <!--p class="help-block">Message or Content which will appear on the popup( Totally optional and not more than 50 characters)</p-->
    </div>

    <div class="form-group">
      <label class="control-label">Target Link</label>
      <input type="text" placeholder="" name="target_link" id="target_link" value="<?php if(isset($cat_data_data[0]['target_link'])){ echo @$cat_data_data[0]['target_link']; }else{ echo @$_POST['target_link']; } ?>" class="form-control"> <span class="text-red"><?php echo form_error('target_link'); ?></span>
      <p class="help-block">Once someone click on the popup it will redirect to the above mentioned link</p>
    </div>

    <div class="form-group">
      <label class="control-label">Page Link</label>
      <input type="text" placeholder="" name="page_link" id="page_link" value="<?php if(isset($cat_data_data[0]['page_link'])){ echo @$cat_data_data[0]['page_link']; }else{ echo @$_POST['page_link']; } ?>" class="form-control"> <span class="text-red"><?php echo form_error('page_link'); ?></span>
      <p class="help-block">Specify the page link where you want to show the popup( Keep this input box empty if you want to show on all the pages)</p>
    </div>

    <div class="form-group">
      <label class="control-label">Show popup Instance</label>
      <select name="popup_interval" id="popup_interval" class="form-control chosen-select">
          <option value="1" <?php echo @$cat_data_data[0]['popup_interval_chosen'] == '1' ? 'selected' : ''; ?>>30 min</option>
          <option value="2" <?php echo @$cat_data_data[0]['popup_interval_chosen'] == '2' ? 'selected' : ''; ?>>40 min</option>
          <option value="3" <?php echo @$cat_data_data[0]['popup_interval_chosen'] == '3' ? 'selected' : ''; ?>>50 min</option>
          <option value="4" <?php echo @$cat_data_data[0]['popup_interval_chosen'] == '4' ? 'selected' : ''; ?>>60 min</option>
          <option value="5" <?php echo @$cat_data_data[0]['popup_interval_chosen'] == '5' ? 'selected' : ''; ?>>90 min</option>
      </select>
      <p class="help-block">Popup will show to the  user after the specific interval( Default will be 30 min)</p>
    </div>

    <div class="form-group">
      <label class="control-label">Desktop Image ( 1366x140 )</label>
      <input type="file" placeholder="" name="desktop_img" id="desktop_img" value="<?php if(isset($cat_data_data[0]['desktop_img'])){ echo @$cat_data_data[0]['desktop_img']; }else{ echo @$_POST['desktop_img']; } ?>" class="form-control"> <span class="text-red"><?php echo form_error('desktop_img'); ?></span>
      <span class="text-red"><?php echo @$desks_error; ?></span>
      <p class="help-block">Image will be display on the website</p>
       <?php if(isset($cat_data_data[0]['desktop_img']) && $cat_data_data[0]['desktop_img']!='') { ?>
        <div style="width:100px;"><img src="../../../../assets/images/popups/desktop/<?php echo $cat_data_data[0]['desktop_img']; ?>"></div>
      <?php } ?>
    </div>

    <div class="form-group">
      <label class="control-label">Mobile Image ( 360x70 ) </label>
      <input type="file" placeholder="" name="mobile_img" id="mobile_img" value="<?php if(isset($cat_data_data[0]['mobile_img'])){ echo @$cat_data_data[0]['mobile_img']; }else{ echo @$_POST['mobile_img']; } ?>" class="form-control"> <span class="text-red"><?php echo form_error('mobile_img'); ?></span>
      <span class="text-red"><?php echo @$mobile_error; ?></span>
      <p class="help-block">Image will be display on the Mobile Web and App</p>
      <?php if(isset($cat_data_data[0]['mobile_img']) && $cat_data_data[0]['mobile_img']!='') { ?>
        <div style="width:100px;"><img src="../../../../assets/images/popups/mobile/<?php echo $cat_data_data[0]['mobile_img']; ?>"></div>
      <?php } ?>
    </div>

    <div class="form-group">
      <label class="control-label">Start Datetime</label>
      <input type="text" placeholder="" name="start_datetime" id="start_datetime" value="<?php if(isset($cat_data_data[0]['start_datetime'])){ echo @$cat_data_data[0]['start_datetime']; }else{ echo @$_POST['start_datetime']; } ?>" class="form-control datetimepicker"> <span class="text-red"><?php echo form_error('start_datetime'); ?></span>
      <p class="help-block"></p>
    </div>

    <div class="form-group">
      <label class="control-label">End Datetime</label>
      <input type="text" placeholder="" name="end_datetime" id="end_datetime" value="<?php if(isset($cat_data_data[0]['end_datetime'])){ echo @$cat_data_data[0]['end_datetime']; }else{ echo @$_POST['end_datetime']; } ?>" class="form-control datetimepicker"> <span class="text-red"><?php echo form_error('end_datetime'); ?></span>
      <p class="help-block"></p>
    </div>

    <div class="form-group">
      <label class="control-label">Status</label>
      <select name="status" id="status" class="form-control chosen-select">
          <option value="1" <?php echo @$cat_data_data[0]['status'] == '1' ? 'selected' : ''; ?>>Active</option>
          <option value="0" <?php echo @$cat_data_data[0]['status'] == '0' ? 'selected' : ''; ?>>Deactive</option>
      </select>
      <p class="help-block">Default will be active</p>
    </div>
    
              <div class="form-group">
              <?php if($this->uri->segment(4) > 0){ ?>
      <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-edit"></i> Edit popup</button>
    <?php }else{ ?>
      <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Add New popup</button>
    <?php } ?>

              </div>

  </form>

</div>
</div>
</div>
 <div class="col-md-8">
   <div class="box">
     <div class="box-header">
       <h3 class="box-title">
          View Popups
         </h3>
         <div class="pull-right frmsearch" style="width:55%">
         <div class="row">
        
               <div class="col-md-6">
                <div class="row" style="display:none;" id="hide_show_container">
                  <div class="col-md-8">
               <select name="cat_hide_show" id="cat_hide_show" class="form-control input-sm pull-right">
                <option value="1">Show in filter page</option>
                <option value="0">Hide from filter page</option>
               </select>
               </div> 
               <div class="col-md-4">
                <button type="button" name="submit" id="submit" value="Apply" onclick="apply_action();" class="btn btn-sm btn-default">Apply</button>           

               </div> 
               </div>
                </div>          
<!--div class="col-md-6">
         <div class="input-group">
             <input name="search_text" id="search_text" value=""  placeholder="Search" class="form-control input-sm pull-right" required>
               <div class="input-group-btn">
                 <button type="button" name="submit" id="submit" value="Search" onclick="search_cat();" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
               </div>
               </div> 
                </div--> 
          
           </div>
           </div>
        
     </div><!-- /.box-header -->
  <div class="box-body">

  <div class="table-responsive" style="height: 737px;min-block-size: 0.01%;overflow-x: scroll;">
    <table class="table table-bordered table-striped dataTable" id="datatable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Instance</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Status</th>
                    <th width="140">Action</th>

                </tr>
                </thead>
      <tbody id="category_list">
      
      <?php 
        if(!empty($get_popup)){
          foreach($get_popup as $key => $value) {
            ?>
            <tr>
            <td><?php echo $value['id']; ?></td>
            <td><?php echo $value['popup_name']; ?></td>
            <td><?php if($value['popup_interval_chosen'] == 1){ echo '30 Min'; }
                      else if($value['popup_interval_chosen'] == 2){ echo '40 Min'; }
                      else if($value['popup_interval_chosen'] == 3){ echo '50 Min'; }
                      else if($value['popup_interval_chosen'] == 4){ echo '60 Min'; }
                      else if($value['popup_interval_chosen'] == 5){ echo '90 Min'; }
             ?></td>
            <td><?php echo $value['start_datetime']; ?></td>
            <td><?php echo $value['end_datetime']; ?></td>
            <td><?php echo $value['status']==0 ? 'Inactive' : 'Active' ; ?></td>
            <td>
              <a href='<?php echo base_url(); ?>webui/popups/popups_edit/<?php echo $value['id']; ?>'><button class='btn btn-primary btn-xs editProd' type='button' ><i class='fa fa-edit'></i> Edit</button></a> <button class='btn btn-xs btn-primary btn-delete' type='button' onclick='delete_category(<?php echo $value['id']; ?>);'><i class='fa fa-trash-o'></i> Delete</button>
            </td>
            </tr>
            <?php
          }
        }

      ?>
      </tbody>
    </table>
  </div>
</div>
</div>
</div>
</div>

</section>

<script type="text/javascript">
  $(function () {
    $('.datetimepicker').datetimepicker();
  });

    function delete_category(id){
    var result = confirm("Want to delete?");
    if (result) {
        $.ajax({
            url: "<?php echo base_url(); ?>webui/popups/delete_popups",
            type: 'POST',
            data: { id : id },
            cache :true,
            async: true,
            success: function(response) {
              window.location="<?php echo base_url(); ?>webui/popups";
            },
            error: function(xhr) {
              alert('Something goes wrong');
              hide_cart_loader();
            }
        });
    }
  }
   $( document ).ready(function() {
    $('.nav-item-banners').addClass("active");
    // $('.nav-banners-list').addClass("active");
  $('.nav-banners-list6').addClass("active");
    //$('.datepicker').datepicker();
  
    });
</script>