<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/redmond/jquery-ui.css" />
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<!--pqSelect dependencies-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev.js"></script>
<section class="content-header">
  <h1>Homepage - Top Product Category Settings </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>home">Website UI</a></li>
    <li class="active">Homepage - Top Product Category Settings </li>
  </ol>
</section>
<section class="content">
  <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Top Category</h3>
      </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-1">
                <div class="form-group">
                  <strong>ALL</strong>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <select name="all_cat_one_select" id="all_cat_one_select" class="form-control" onchange="getCatList('all_cat_one_select','all_cat_one','<?php echo @$top_cat_all[0]['cat']; ?>');">
                    <option value="">Select</option>
                    <option value="1" <?php echo @$top_cat_all[0]['type'] == 1 ? 'selected' : ''; ?>>Category</option>
                    <!--option value="2" <?php echo @$top_cat_all[0]['type'] == 2 ? 'selected' : ''; ?>>Sub Category</option>
                    <option value="3" <?php echo @$top_cat_all[0]['type'] == 3 ? 'selected' : ''; ?>>Variation Types</option>
                    <option value="4" <?php echo @$top_cat_all[0]['type'] == 4 ? 'selected' : ''; ?>>Variation Values</option-->
                  </select>

                  <select name="all_cat_one" id="all_cat_one" class="form-control">

                  </select>


                  <span id="all_cat_one_error"></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <select name="all_cat_two_select" id="all_cat_two_select" class="form-control" onchange="getCatList('all_cat_two_select','all_cat_two','<?php echo @$top_cat_all[1]['cat']; ?>');">
                    <option value="">Select</option>
                    <option value="1" <?php echo @$top_cat_all[1]['type'] == 1 ? 'selected' : ''; ?>>Category</option>
                    <!--option value="2" <?php echo @$top_cat_all[1]['type'] == 2 ? 'selected' : ''; ?>>Sub Category</option>
                    <option value="3" <?php echo @$top_cat_all[1]['type'] == 3 ? 'selected' : ''; ?>>Variation Types</option>
                    <option value="4" <?php echo @$top_cat_all[1]['type'] == 4 ? 'selected' : ''; ?>>Variation Values</option-->
                  </select>
                  <select name="all_cat_two" id="all_cat_two" class="form-control">

                  </select>
                  <span id="all_cat_two_error"></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <select name="all_cat_three_select" id="all_cat_three_select" class="form-control" onchange="getCatList('all_cat_three_select','all_cat_three','<?php echo @$top_cat_all[2]['cat']; ?>');">
                    <option value="">Select</option>
                    <option value="1" <?php echo @$top_cat_all[2]['type'] == 1 ? 'selected' : ''; ?>>Category</option>
                    <!--option value="2" <?php echo @$top_cat_all[2]['type'] == 2 ? 'selected' : ''; ?>>Sub Category</option>
                    <option value="3" <?php echo @$top_cat_all[2]['type'] == 3 ? 'selected' : ''; ?>>Variation Types</option>
                    <option value="4" <?php echo @$top_cat_all[2]['type'] == 4 ? 'selected' : ''; ?>>Variation Values</option-->
                  </select>
                  <select name="all_cat_three" id="all_cat_three" class="form-control">

                  </select>
                  <span id="all_cat_three_error"></span>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-primary btn-xs" id="all_cat_apply" name="all_cat_apply" type="button" onclick="top_cat('all');">Apply
                </div>
              </div>
             </div>
            </div>


            <div class="row">
            <div class="col-md-12">
              <div class="col-md-1">
                <div class="form-group">
                  <strong>MEN</strong>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <select name="men_cat_one_select" id="men_cat_one_select" class="form-control" onchange="getCatList('men_cat_one_select','men_cat_one','<?php echo @$top_cat_men[0]['cat']; ?>');">
                    <option value="">Select</option>
                    <option value="1" <?php echo @$top_cat_men[0]['type'] == 1 ? 'selected' : ''; ?>>Category</option>
                    <!--option value="2" <?php echo @$top_cat_men[0]['type'] == 2 ? 'selected' : ''; ?>>Sub Category</option>
                    <option value="3" <?php echo @$top_cat_men[0]['type'] == 3 ? 'selected' : ''; ?>>Variation Types</option>
                    <option value="4" <?php echo @$top_cat_men[0]['type'] == 4 ? 'selected' : ''; ?>>Variation Values</option-->
                  </select>
                  <select name="men_cat_one" id="men_cat_one" class="form-control">

                  </select>
                  <span id="men_cat_one_error"></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <select name="men_cat_two_select" id="men_cat_two_select" class="form-control" onchange="getCatList('men_cat_two_select','men_cat_two','<?php echo @$top_cat_men[1]['cat']; ?>');">
                    <option value="">Select</option>
                    <option value="1" <?php echo @$top_cat_men[1]['type'] == 1 ? 'selected' : ''; ?>>Category</option>
                    <!--option value="2" <?php echo @$top_cat_men[1]['type'] == 2 ? 'selected' : ''; ?>>Sub Category</option>
                    <option value="3" <?php echo @$top_cat_men[1]['type'] == 3 ? 'selected' : ''; ?>>Variation Types</option>
                    <option value="4" <?php echo @$top_cat_men[1]['type'] == 4 ? 'selected' : ''; ?>>Variation Values</option-->
                  </select>
                  <select name="men_cat_two" id="men_cat_two" class="form-control">

                  </select>
                  <span id="men_cat_two_error"></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <select name="men_cat_three_select" id="men_cat_three_select" class="form-control" onchange="getCatList('men_cat_three_select','men_cat_three','<?php echo @$top_cat_men[2]['cat']; ?>');">
                    <option value="">Select</option>
                    <option value="1" <?php echo @$top_cat_men[2]['type'] == 1 ? 'selected' : ''; ?>>Category</option>
                    <!--option value="2" <?php echo @$top_cat_men[2]['type'] == 2 ? 'selected' : ''; ?>>Sub Category</option>
                    <option value="3" <?php echo @$top_cat_men[2]['type'] == 3 ? 'selected' : ''; ?>>Variation Types</option>
                    <option value="4" <?php echo @$top_cat_men[2]['type'] == 4 ? 'selected' : ''; ?>>Variation Values</option-->
                  </select>
                  <select name="men_cat_three" id="men_cat_three" class="form-control">

                  </select>
                  <span id="men_cat_three_error"></span>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-primary btn-xs" id="men_cat_apply" name="men_cat_apply" type="button"  onclick="top_cat('men');">Apply
                </div>
              </div>
             </div>
            </div>

            <div class="row">
            <div class="col-md-12">
              <div class="col-md-1">
                <div class="form-group">
                  <strong>WOMEN</strong>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <select name="women_cat_one_select" id="women_cat_one_select" class="form-control" onchange="getCatList('women_cat_one_select','women_cat_one','<?php echo @$top_cat_women[0]['cat']; ?>');">
                    <option value="">Select</option>
                    <option value="1" <?php echo @$top_cat_women[0]['type'] == 1 ? 'selected' : ''; ?>>Category</option>
                    <!--option value="2" <?php echo @$top_cat_women[0]['type'] == 2 ? 'selected' : ''; ?>>Sub Category</option>
                    <option value="3" <?php echo @$top_cat_women[0]['type'] == 3 ? 'selected' : ''; ?>>Variation Types</option>
                    <option value="4" <?php echo @$top_cat_women[0]['type'] == 4 ? 'selected' : ''; ?>>Variation Values</option-->
                  </select>
                  <select name="women_cat_one" id="women_cat_one" class="form-control">

                  </select>
                  <span id="women_cat_one_error"></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <select name="women_cat_two_select" id="women_cat_two_select" class="form-control" onchange="getCatList('women_cat_two_select','women_cat_two','<?php echo @$top_cat_women[1]['cat']; ?>');">
                    <option value="">Select</option>
                    <option value="1" <?php echo @$top_cat_women[1]['type'] == 1 ? 'selected' : ''; ?>>Category</option>
                    <!--option value="2" <?php echo @$top_cat_women[1]['type'] == 2 ? 'selected' : ''; ?>>Sub Category</option>
                    <option value="3" <?php echo @$top_cat_women[1]['type'] == 3 ? 'selected' : ''; ?>>Variation Types</option>
                    <option value="4" <?php echo @$top_cat_women[1]['type'] == 4 ? 'selected' : ''; ?>>Variation Values</option-->
                  </select>
                  <select name="women_cat_two" id="women_cat_two" class="form-control">

                  </select>
                  <span id="women_cat_two_error"></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <select name="women_cat_three_select" id="women_cat_three_select" class="form-control" onchange="getCatList('women_cat_three_select','women_cat_three','<?php echo @$top_cat_women[2]['cat']; ?>');">
                   <option value="">Select</option>
                    <option value="1" <?php echo @$top_cat_women[2]['type'] == 1 ? 'selected' : ''; ?>>Category</option>
                    <!--option value="2" <?php echo @$top_cat_women[2]['type'] == 2 ? 'selected' : ''; ?>>Sub Category</option>
                    <option value="3" <?php echo @$top_cat_women[2]['type'] == 3 ? 'selected' : ''; ?>>Variation Types</option>
                    <option value="4" <?php echo @$top_cat_women[2]['type'] == 4 ? 'selected' : ''; ?>>Variation Values</option-->
                  </select>
                  <select name="women_cat_three" id="women_cat_three" class="form-control">

                  </select>
                  <span id="women_cat_three_error"></span>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-primary btn-xs" id="women_cat_apply" name="women_cat_apply" type="button" onclick="top_cat('women');">Apply
                </div>
              </div>
             </div>
            </div>

            <span class="error"></span>


          <div class="box-header with-border">
            <h3 class="box-title">View All Category Page</h3>
          </div>
         <!--  <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-3">
                  <strong>Category</strong>
                </div>
                <div class="col-md-9">
                  <select multiple="true" name="category" id="category" class="form-control">
                    <?php echo $category; ?>
                  </select>
                </div>
              </div>
            </div>

             <div class="row">
              <div class="col-md-12">
                <div class="col-md-3">
                  <strong>Sub Category</strong>
                </div>
                <div class="col-md-9">
                  <select multiple="true" name="sub_category" id="sub_category" class="form-control">
                    <?php echo $subcategory; ?>
                  </select>
                </div>
              </div>
            </div> -->

             <div class="row">
              <div class="col-md-12">
                <div class="col-md-3">
                  <strong>Variation Types</strong>
                </div>
                <div class="col-md-9">
                  <select multiple="true" name="variation_types" id="variation_types" class="form-control">
                    <?php echo $variationtypes; ?>
                  </select>
                </div>
              </div>
            </div>

            <!--  <div class="row">
              <div class="col-md-12">
                <div class="col-md-3">
                  <strong>Variation Types</strong>
                </div>
                <div class="col-md-9">
                 <select multiple="true" name="variation_values" id="variation_values" class="form-control">
                    <?php echo $variationvalues; ?>
                  </select>
                </div>
              </div>
            </div> -->

             <div class="row">
              <div class="col-md-12" style="text-align:right;">
                <button class="btn btn-primary btn-xs" id="all_cat_save" name="all_cat_save" type="button" onclick="all_cat_save();">Apply</button>
              </div>
            </div>

          </div>
          <!--span class="all_cat_error" id="all_cat_error"></span-->

          </div>
        </div>


  </div>
</section>
<script type="text/javascript">
$( document ).ready(function() {
  $('.nav-item-banners').addClass("active");
  $('.nav-banners-list4').addClass("active");

  $('.page-overlay').show();
   $('#all_cat_one_select').trigger('change');
   $('#all_cat_two_select').trigger('change');
   $('#all_cat_three_select').trigger('change');

   $('#men_cat_one_select').trigger('change');
   $('#men_cat_two_select').trigger('change');
   $('#men_cat_three_select').trigger('change');

   $('#women_cat_one_select').trigger('change');
   $('#women_cat_two_select').trigger('change');
   $('#women_cat_three_select').trigger('change');

   $('#category').pqSelect({
      bootstrap: { on: true },
      multiplePlaceholder: 'Select',
      checkbox: true, //adds checkbox to options
      }).on("change", function(evt) {
      var val = $(this).val();
   });

   $('#sub_category').pqSelect({
      bootstrap: { on: true },
      multiplePlaceholder: 'Select',
      checkbox: true, //adds checkbox to options
      }).on("change", function(evt) {
      var val = $(this).val();
   });

   $('#variation_types').pqSelect({
      bootstrap: { on: true },
      multiplePlaceholder: 'Select',
      checkbox: true, //adds checkbox to options
      }).on("change", function(evt) {
      var val = $(this).val();
   });

   $('#variation_values').pqSelect({
      bootstrap: { on: true },
      multiplePlaceholder: 'Select',
      checkbox: true, //adds checkbox to options
      }).on("change", function(evt) {
      var val = $(this).val();
   });

});

  function top_cat(type){
    var cat_id_one,cat_id_two,cat_id_three,cat_one_select,cat_two_select,cat_three_select = 0;
    if(type.trim()==="all"){
        var cat_id_one = $('#all_cat_one').val();
        var cat_id_two = $('#all_cat_two').val();
        var cat_id_three = $('#all_cat_three').val();
        var cat_one_select = $('#all_cat_one_select').val();
        var cat_two_select = $('#all_cat_two_select').val();
        var cat_three_select = $('#all_cat_three_select').val();
    }else if(type.trim()==="men"){
        var cat_id_one = $('#men_cat_one').val();
        var cat_id_two = $('#men_cat_two').val();
        var cat_id_three = $('#men_cat_three').val();
        var cat_one_select = $('#men_cat_one_select').val();
        var cat_two_select = $('#men_cat_two_select').val();
        var cat_three_select = $('#men_cat_three_select').val();
    }else if(type.trim()==="women"){
        var cat_id_one = $('#women_cat_one').val();
        var cat_id_two = $('#women_cat_two').val();
        var cat_id_three = $('#women_cat_three').val();
        var cat_one_select = $('#women_cat_one_select').val();
        var cat_two_select = $('#women_cat_two_select').val();
        var cat_three_select = $('#women_cat_three_select').val();
    }
    //console.log(cat_id_one +'-'+ cat_id_two +'-'+ cat_id_three);
    if(cat_id_one > 0 && cat_id_two >0 && cat_id_three > 0 && cat_one_select>0 && cat_two_select>0 && cat_three_select>0){
       $.ajax({
          url: '<?php echo base_url(); ?>webui/Homepage_settings/save_top_cat',
          type: 'post',
          data: { 'type' : type,'cat_id_one' : cat_id_one,'cat_id_two' : cat_id_two,'cat_id_three' : cat_id_three,'cat_one_select':cat_one_select,'cat_two_select':cat_two_select,'cat_three_select':cat_three_select },
          success: function(data, status) {
            var obj = jQuery.parseJSON(data);
            if(type=="all"){
                  //if(obj[0]!='') { $('#all_cat_one_error').html(obj[0]).addClass('text-red'); }
                  //if(obj[1]!='') { $('#all_cat_two_error').html(obj[1]).addClass('text-red'); }
                  //if(obj[2]!='') { $('#all_cat_three_error').html(obj[2]).addClass('text-red'); }
                  if(data=='[""]') {
                    $('#all_cat_three_error').html('Done').addClass('text-green');
                  }else{
                    $('#all_cat_three_error').html(obj).addClass('text-red');
                  }
                }else if(type=="men"){
                  /*if(obj[0]!='') { $('#men_cat_one_error').html(obj[0]).addClass('text-red'); }
                  if(obj[1]!='') { $('#men_cat_two_error').html(obj[1]).addClass('text-red'); }
                  if(obj[2]!='') { $('#men_cat_three_error').html(obj[2]).addClass('text-red'); }*/
                  if(data=='[""]') {
                  $('#men_cat_three_error').html('Done').addClass('text-green'); }
                  else{
                    $('#men_cat_three_error').html(obj).addClass('text-red');
                  }
                }else if(type=="women"){
                 /* if(obj[0]!='') { $('#women_cat_one_error').html(obj[0]).addClass('text-red'); }
                  if(obj[1]!='') { $('#women_cat_two_error').html(obj[1]).addClass('text-red'); }
                  if(obj[2]!='') { $('#women_cat_three_error').html(obj[2]).addClass('text-red'); }*/
                  if(data=='[""]') {
                  $('#women_cat_three_error').html('Done').addClass('text-green'); }
                  else{
                    $('#women_cat_three_error').html(obj).addClass('text-red');
                  }
                }
          },
          error: function(xhr, desc, err) {
            console.log(err);
          }
      });
    }else{
      $('.error').html('Please enter cat IDs').addClass('text-red');
    }
  }

  function get_cat_img(type,catId){
      $.ajax({
          url: '<?php echo base_url(); ?>webui/Homepage_settings/get_cat_img',
          type: 'post',
          data: { 'catId' : catId },
          success: function(data, status) {
            if(type == 'all'){
              $('#all_cats').append(data);
            }else if(type == 'men'){
              $('#men_cats').append(data);
            }else if(type == 'women'){
              $('#women_cats').append(data);
            }
          },
          error: function(xhr, desc, err) {
            console.log(err);
          }
      });

  }

  function getCatList(selectbox_name,id,preselected){
    var cat_id= $('#'+selectbox_name).val();
    if(cat_id !='' && cat_id > 0){
      $('.page-overlay').show();
      $.ajax({
          url: '<?php echo base_url(); ?>webui/Homepage_settings/get_category',
          type: 'post',
          data: { 'cat_id' : cat_id,'preselected':preselected },
          success: function(data, status) {
            $('#'+id).html('');
            $('#'+id).append(data);
            $('#'+id).pqSelect({
                       bootstrap: { on: true },
                       multiplePlaceholder: 'Select',
                       checkbox: true, //adds checkbox to options
                }).on("change", function(evt) {
                    var val = $(this).val();
            });
            $('#'+id).pqSelect( "refreshData" );
            $('.page-overlay').hide();
          }
      });
    }else{
      $('#'+selectbox_name+'_error').html('Please Select the Category Level');
      $('.page-overlay').hide();
    }
  }

  function all_cat_save(){
    var cat_id= $('#category').val();
    var sub_cat_id= $('#sub_category').val();
    var variation_type= $('#variation_types').val();
    var variation_values= $('#variation_values').val();

    $('.page-overlay').show();
      $.ajax({
          url: '<?php echo base_url(); ?>webui/Homepage_settings/save_view_cat',
          type: 'post',
          data: { 'cat_id' : cat_id,'sub_cat_id':sub_cat_id,'variation_type':variation_type,'variation_values':variation_values },
          success: function(data, status) {
            alert(data);
            //$('#all_cat_error').html('Done');
            $('.page-overlay').hide();
          }
      });

  }
</script>
