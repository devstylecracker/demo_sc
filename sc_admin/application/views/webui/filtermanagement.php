<script src="<?php echo base_url(); ?>assets/plugins/chosen/chosen.jquery.min.js" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>assets/plugins/chosen/chosen.css" rel="stylesheet" type="text/css">
<section class="content-header">
  <h1>Filter Management</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">Filter Management</li>
  </ol>
</section>
<section class="content">

  <div class="box">
    <div class="box-body">  
      <div class="row">

        <div class="col-md-3">
          <div class="form-group">
            <select id="menu-tem-type" class="form-control chosen-select" data-placeholder="Select">          
              <option value="0">
                Select
              </option>
              <option value="1">
                Variation Types
              </option>
              <option value="2">
                Variations
              </option>
            </select>
          </div>

        </div>

        <div class="col-md-3">
          <div class="form-group">
            <span class="btn btn-primary btn-sm" onclick="load_type();"> Load</span>
          </div>
        </div>
        
      </div>

      <div class="row">
        <div class="col-md-9">
          <div class="form-group">
            <select name="show_in_filters" id="show_in_filters" class="form-control chosen-select" multiple>

            </select>
            </div>
        </div>

        <div class="col-md-3">
          <div class="form-group">
            <span class="btn btn-primary btn-sm" onclick="save_type();"> Save Filters </span>
          </div>
        </div>

      </div>

    </div>
  </div>

  <script type="text/javascript">

  $(document).ready(function(){
    $(".chosen-select").chosen();
  });
    function load_type(){
      $('.page-overlay').show();
      var select_type= $('#menu-tem-type').val();
      if(select_type != 0){
          $.ajax({
              url: '<?php echo base_url(); ?>webui/Filtermanagement/get_data',     
              type: 'post',
              data: { 'select_type' : select_type },
              success: function(data, status) {
                $('#show_in_filters').html(data);
                 $("#show_in_filters").trigger("chosen:updated");
                 $('.page-overlay').hide();
              },
              error: function(xhr, desc, err) {
                console.log(err);
              }
          });
      }else{
        alert('Select Variation Types or Variations from drowpdown');
      }
    }

    function save_type(){
       $('.page-overlay').show();
       var select_type= $('#menu-tem-type').val();
       var show_in_filters= $('#show_in_filters').val();
       if(select_type != 0){
          $.ajax({
              url: '<?php echo base_url(); ?>webui/Filtermanagement/save_type',     
              type: 'post',
              data: { 'select_type' : select_type,'show_in_filters':show_in_filters },
              success: function(data, status) {
                $('.page-overlay').hide();
                alert('Done');
              },
              error: function(xhr, desc, err) {
                console.log(err);
              }
          });
       }else{
        alert('Select Variation Types or Variations from drowpdown');
      }
    }
  </script>