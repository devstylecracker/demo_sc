<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<!--
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
-->
<!-- search select box implementation -->
<!--<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js" type="text/javascript"></script>-->

<section class="content-header">
  <h1>Default Banners</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>home">Website UI</a></li>
    <li class="active">Homepage Slider</li>
  </ol>
</section>

<section class="content">
 <?php 
 $count = count($data);
 // $data = array_slice($data,0,5);
 foreach($data as $row){ ?>
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Slide</h3>
      <div class="form-inline pull-right">
        <label class="form-label">Slide Order
          <select class="form-control">
            <option>
              <?php echo $row['banner_position']; ?>
            </option><!-- 
            <option>
              2
            </option>
            <option>
              3
            </option>
            <option>
              4
            </option>
            <option>
              5
            </option> -->
          </select>
        </label>
      </div>
    </div><!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <input  value="<?php echo $row['caption_line_1']; ?>" placeholder="Slide Caption Line 1" class="form-control" type="text">
          </div>
          <div class="form-group">
            <input  value="<?php echo $row['caption_line_2']; ?>" placeholder="Slide Caption Line 2" class="form-control" type="text">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <input   value="<?php echo $row['banner_url']; ?>" placeholder="Label for Link Button" class="form-control" type="text">
          </div>
          <div class="form-group">
            <input  value="<?php echo $row['lable_link']; ?>" placeholder="URL" class="form-control" type="text">
          </div>
        </div>
        <div class="col-md-3">
		  <img src="<?php echo base_url(); ?>assets/banners/<?php echo $row['slide_url']; ?>">

        </div>
      </div>

    </div>
  </div>


<?php } ?>

</section>

<script type="text/javascript">
$(function () {
	var date_from =  $("#date_from").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii" , autoclose: true });
	var date_to = $("#date_to").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii", autoclose: true });
	});
  $( document ).ready(function() {
    $('.nav-item-banners').addClass("active");
    $('.nav-banners-list').addClass("active");
	// $('.nav-banners-list2').addClass("active");
    $('.datepicker').datepicker();
    });
</script>