<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<!--
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
-->
<!-- search select box implementation -->
<!--<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js" type="text/javascript"></script>-->

<section class="content-header">
  <h1>Manage Banners</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>home">Website UI</a></li>
    <li class="active">Homepage Slider</li>
  </ol>
</section>

<section class="content">
<form role="form" id="product_form" enctype="multipart/form-data" method="POST">
<div class="box">
 <div class="box-header with-border">
<h3 class="box-title">Active Banners</h3>
 <div class="pull-right"><a href="<?php echo base_url(); ?>webui/Brand_setting/add"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add Banner</button></a>
</div></div>
<div class="box-body">
 <?php 
 
 $count = count($data);
 $i = 1;
 // $data = array_slice($data,0,5);
 foreach($active_banners as $row){ ?>
 <div class="box box-solid" style="background: #eee;
    border: 1px solid #e1e1e1;" >
  
    <div class="box-header">
      <h3 class="box-title">Banner <?php echo $i; ?></h3>
      <div class="form-inline pull-right">
        <label class="form-label">Slide Order
          <select class="form-control" name="order<?php echo $row['id']; ?>" id="order<?php echo $row['id']; ?>">
            <option value="1" <?php if($row['banner_position'] == 1){ echo 'selected="selected"';}?> >
              1
            </option>
            <option value="2" <?php if($row['banner_position'] == 2){ echo 'selected="selected"';}?>>
              2
            </option>
            <option value="3" <?php if($row['banner_position'] == 3){ echo 'selected="selected"';}?> >
              3
            </option>
            <option value="4" <?php if($row['banner_position'] == 4){ echo 'selected="selected"';}?>>
              4
            </option>
            <option value="5" <?php if($row['banner_position'] == 5){ echo 'selected="selected"';}?>>
              5
            </option> 
          </select>
        </label>
      </div>
    </div><!-- /.box-header -->
    <div class="box-body" style="background-color111:#519aaa;">
      <div class="row" >
        <div class="col-md-4">
          <div class="form-group">
            <input  value="<?php echo $row['caption_line_1']; ?>" id="line1<?php echo $row['id']; ?>" name="line1<?php echo $row['id']; ?>" placeholder="Slide Caption Line 1" class="form-control" type="text">
          </div>
          <div class="form-group">
            <input  value="<?php echo $row['caption_line_2']; ?>" id="line2<?php echo $row['id']; ?>" name="line2<?php echo $row['id']; ?>" placeholder="Slide Caption Line 2" class="form-control" type="text">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <input   value="<?php echo $row['banner_url']; ?>" name="button_text"  id="button_text<?php echo $row['id']; ?>" placeholder="javascript function" class="form-control" type="text">
          </div>
          <div class="form-group">
            <input  value="<?php echo $row['lable_link']; ?>" placeholder="URL" id="url<?php echo $row['id']; ?>" name="url<?php echo $row['id']; ?>" class="form-control" type="text">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group radio-group">
			 <label class="radio-inline"><input type="radio" name="status<?php echo $row['id']; ?>" value = "active" checked> Active</label>
            <label class="radio-inline"><input type="radio" name="status<?php echo $row['id']; ?>" value = "deactive"> Deactive</label>
          </div>
          <div id ="daterange<?php echo $row['id']; ?>" style="display:;">
										
				  <div class="input-group">
					<input type="text" name="date_from<?php echo $row['id']; ?>"  placeholder="Date From" class="form-control input-sm datetimepicker" id="date_from<?php echo $row['id']; ?>" value="<?php echo $row['start_date']; ?>">
				  </div>           
				
				 <div class="input-group">
					<input type="text" name="date_to<?php echo $row['id']; ?>" placeholder="Date To" class="form-control input-sm datetimepicker" id="date_to<?php echo $row['id']; ?>" value="<?php echo $row['end_date']; ?>">          
				  </div>
					
			</div>
        </div>
         <div id="show_image1<?php echo $row['id']; ?>" class="col-md-3">
		  <img src="<?php echo base_url(); ?>assets/brand_banners/<?php echo $row['slide_url']; ?>">
        </div>
		<div id="show_image<?php echo $row['id']; ?>" class="col-md-3" style="display:none;"> <img name ="banner_image" id="previewing<?php echo $row['id']; ?>" src="" value="" /> </div>
        <div class="col-md-1">

         <div class="form-group">
            <!-- <div class="btn btn-default btn-file btn-xs"> Browse<input name="image" class="file" type="file"></div> --> 
		<div class="btn btn-default btn-file btn-xs"> Browse<input id="file<?php echo $row['id']; ?>" name="file<?php echo $row['id']; ?>" class="file" type="file" onchange="change_img(<?php echo $row['id']; ?>)"></div>			
		 </div>
		 <div class="form-group">
          <!-- <button class="btn btn-primary btn-sm save-btn" slide_id="<?php echo $row['id']; ?>"><i class="fa fa-save"></i> Save</button> -->
		   <button class="btn btn-primary btn-sm save-btn" tabindex="30" name="submit" value="<?php echo $row['id']; ?>" type="submit"><i class="fa fa-save"></i> Save</button>
</div>		  
		  <div class="form-group">
		  <button class="btn btn-primary btn-delete btn-sm" onclick="remove_banner(<?php echo $row['id']; ?>);"><i class="fa fa-trash"></i> Delete</button>
		  </div>
        </div>
      </div>

    </div>
 
</div>

<?php $i++; } ?>
</form>
 </div>
 </div>
 <div class="box">
 <form role="form" id="product_form" enctype="multipart/form-data" method="POST">
 <div class="box-header with-border">
<h3 class="box-title">Upcoming Banners</h3>
</div>
<div class="box-body">
 <?php 
 $count = count($data);
 //$i = 1;
 // $data = array_slice($data,0,5);
 foreach($upcomming_banners as $row){ ?>
  <div class="box box-solid" style="background: #eee;
    border: 1px solid #e1e1e1;" >  
    <div class="box-header">
      <h3 class="box-title">Banners <?php echo $i; ?></h3>
      <div class="form-inline pull-right">
        <label class="form-label">Slide Order
          <select class="form-control" name="order<?php echo $row['id']; ?>" id="order<?php echo $row['id']; ?>" >
            <option value="1" <?php if($row['banner_position'] == 1){ echo 'selected="selected"';}?> >
              1
            </option>
            <option value="2" <?php if($row['banner_position'] == 2){ echo 'selected="selected"';}?>>
              2
            </option>
            <option value="3" <?php if($row['banner_position'] == 3){ echo 'selected="selected"';}?> >
              3
            </option>
            <option value="4" <?php if($row['banner_position'] == 4){ echo 'selected="selected"';}?>>
              4
            </option>
            <option value="5" <?php if($row['banner_position'] == 5){ echo 'selected="selected"';}?>>
              5
            </option> 
          </select>
        </label>
      </div>
    </div><!-- /.box-header -->
	<input type="hidden" name="row_id" value = "<?php echo $row['id']; ?>">
    <div class="box-body">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <input  value="<?php echo $row['caption_line_1']; ?>" placeholder="Slide Caption Line 1" id="line1<?php echo $row['id']; ?>" name="line1<?php echo $row['id']; ?>" class="form-control" type="text">
          </div>
          <div class="form-group">
            <input  value="<?php echo $row['caption_line_2']; ?>" placeholder="Slide Caption Line 2" id="line2<?php echo $row['id']; ?>" name="line2<?php echo $row['id']; ?>" class="form-control" type="text">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <input value="<?php echo $row['banner_url']; ?>" placeholder="javascript function" name="button_text<?php echo $row['id']; ?>"  id="button_text<?php echo $row['id']; ?>" class="form-control" type="text">
          </div>
          <div class="form-group">
            <input  value="<?php echo $row['lable_link']; ?>" placeholder="URL" class="form-control" type="text" id="url<?php echo $row['id']; ?>" name="url<?php echo $row['id']; ?>">
          </div>
        </div>
        <div class="col-md-2">
        <div class="form-group radio-group">
			 <label class="radio-inline"><input type="radio" name="status<?php echo $row['id']; ?>" value = "active" checked> Active</label>
            <label class="radio-inline"><input type="radio" name="status<?php echo $row['id']; ?>" value = "deactive"> Deactive</label>
          </div>
          <div id ="daterange<?php echo $row['id']; ?>" style="display:;">
										
				  <div class="input-group">
					<input type="text" name="date_from<?php echo $row['id']; ?>"  placeholder="Date From" class="form-control input-sm datetimepicker" id="date_from<?php echo $row['id']; ?>" value="<?php echo $row['start_date']; ?>">
				  </div>           
				
				  <div class="input-group">
					<input type="text" name="date_to<?php echo $row['id']; ?>" placeholder="Date To" class="form-control input-sm datetimepicker" id="date_to<?php echo $row['id']; ?>" value="<?php echo $row['end_date']; ?>">          
				  </div>
					
			</div>
        </div>
        <div id="show_image1<?php echo $row['id']; ?>" class="col-md-3">
		  <img src="<?php echo base_url(); ?>assets/brand_banners/<?php echo $row['slide_url']; ?>">
        </div>
		<div id="show_image<?php echo $row['id']; ?>" class="col-md-3" style="display:none;"> <img name ="banner_image" id="previewing<?php echo $row['id']; ?>" src="" value="" /> </div>
        <div class="col-md-1">

          <div class="form-group">
             <div class="btn btn-default btn-file btn-xs"> Browse<input id="file<?php echo $row['id']; ?>" name="file<?php echo $row['id']; ?>" class="file" type="file" onchange="change_img(<?php echo $row['id']; ?>)"></div>
            <!--  <input value="" placeholder="Upload" class="form-control input-sm" type="file">-->
          </div>
		 <!-- <button class="btn btn-primary btn-sm" onclick="remove_banner(<?php echo $row['id']; ?>);"><i class="fa fa-save"></i>Remove</button>
		  <button class="btn btn-sm btn-primary" tabindex="30" name="submit" value="<?php echo $row['id']; ?>" type="submit"><i class="fa fa-save"></i> Save</button>-->
		  <div class="form-group">
		   <button class="btn btn-primary btn-sm save-btn" tabindex="30" name="submit" value="<?php echo $row['id']; ?>" type="submit"><i class="fa fa-save"></i> Save</button>
			</div>		  
		  <div class="form-group">
		  <button class="btn btn-primary btn-delete btn-sm" onclick="remove_banner(<?php echo $row['id']; ?>);"><i class="fa fa-trash"></i> Delete</button>
		  </div>
        </div>
      </div>

    </div>
  </div>


<?php $i++;} ?>
  </div>
  </form>
  </div>
</section>

<script type="text/javascript">
$(document).ready(function(){
	//$('#show_image').hide();
 });
$(function () {
	//var date_from =  $("#date_from").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii" , autoclose: true });
	//var date_to = $("#date_to").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii", autoclose: true });
	
	var datetime = $(".datetimepicker").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii", autoclose: true });
	
	});
  $( document ).ready(function() {
	  $('#loading').hide();
    $('.nav-item-brand_setting').addClass("active");
	$('.nav-brand-list2').addClass("active");
    //$('.datepicker').datepicker();
	
	$('.save-btn').on('click',function(){
		var id = $(this).attr('slide_id');
		var status = $(this).closest('.row').find("input[name='status"+id+"']:checked").val();
		update_banner(id,status)
	});

	
    });
	
	function msg(m){ console.log(m); }
	
	function update_banner(id,status){
		var order = $('#order'+id).val();
		var line1 = $('#line1'+id).val();
		var line2 = $('#line2'+id).val();
		//var status =  $('#status39').val();
		var date_from =  $('#date_from'+id).val();
		var date_to = $('#date_to'+id).val();
		var button_text = $('#button_text'+id).val();
		var url = $('#url'+id).val();
		var file = $('#file'+id).files[0];
		// alert(file);
		var result = window.confirm('Are you sure?');
		if(result == true)
        {
			if(id != null){
					$.ajax({
						url: "<?php echo base_url() ?>webui/Brand_setting/update_banner",
						type: 'POST',
						data: {'id':id,'line1':line1,'line2':line2,'status':status,'date_from':date_from,'date_to':date_to,'button_text':button_text,'url':url,'order':order},
						cache: false,
						async: false,
						success: function(response) {
							alert("Data Saved Sucessfully");
							if(response == 1)
							{
								location.reload();
							}else{
								location.reload();
							}

						}
					});
				  }	
		}
		
	}
	function remove_banner(id){
		var result = window.confirm('Are you sure?');
		if(result == true)
        {
			if(id != null){
					$.ajax({
						url: "<?php echo base_url() ?>webui/Brand_setting/remove_banner",
						type: 'POST',
						data: {'id':id},
						cache: false,
						async: false,
						success: function(response) {
							alert("Banner Removed Sucessfully ");
							if(response == 1)
							{
								location.reload();
							}else{
								location.reload();
							}

						}
					});
				  }	
		}
		else {}
	}
	
	function imageIsLoaded(e,id) { 
	$("#file"+id).css("color","green");
	$('#image_preview').css("display", "block");
	$('#previewing'+id).attr('src', e.target.result);
	$('#previewing'+id).attr('width', '250px');
	$('#previewing'+id).attr('height', '70px');
	$('#show_image'+id).show();
	$('#show_image1'+id).hide();
	};
	
	function change_img(id){ 
	$("#message").empty(); // To remove the previous error message
	var file = $("#file"+id).get(0).files[0];
	var imagefile = '';
	var imagefile = file.type;
	// alert(file);
	var match= ["image/gif","image/png","image/jpg","image/jpeg"];
	if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]) || (imagefile==match[3])))
	{
	$('#previewing'+id).attr('src','noimage.png');
	$("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
	return false;
	}
	else
	{ 
	var reader = new FileReader();
	reader.onload = function(e){ imageIsLoaded(e,id); }
	//path = $(this).closest('.row').find("img");
	//alert(path);
	
	reader.readAsDataURL($("#file"+id).get(0).files[0],id);
	}
  }
</script>