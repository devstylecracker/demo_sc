<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/redmond/jquery-ui.css" />    
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<!--pqSelect dependencies-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev.js"></script>
<section class="content-header">
  <h1>Homepage - Trending Brands Settings </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>home">Website UI</a></li>
    <li class="active">Homepage - Trending Brands Settings </li>
  </ol>
</section>
<section class="content">
  <div class="box box-warning">
      <div class="box-header with-border">
        <h3 class="box-title">Trending Brands</h3>   
      </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-1">
                <div class="form-group">
                  <strong>ALL</strong>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <select name="all_brand_one_id" id="all_brand_one_id" class="form-control">
                      <?php echo $all_brand_one_id; ?>
                  </select>
                  <span id="all_look_one_error"></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <select name="all_brand_two_id" id="all_brand_two_id" class="form-control">
                  <?php echo $all_brand_two_id; ?>
                  </select>
                  <span id="all_look_two_error"></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <select name="all_brand_three_id" id="all_brand_three_id" class="form-control">
                    <?php echo $all_brand_three_id; ?>
                  </select>
                  <span id="all_look_three_error"></span>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-primary btn-xs" id="all_look_apply" name="all_look_apply" type="button" onclick="trending_brands('all');">Apply
                </div>
              </div>
             </div>
            </div>

            <div class="row">
            <div class="col-md-12" id="all_looks">

            </div>
            </div>

            <div class="row">
            <div class="col-md-12">
              <div class="col-md-1">
                <div class="form-group">
                  <strong>MEN</strong>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <select name="men_brand_one_id" id="men_brand_one_id" class="form-control">
                    <?php echo $men_brand_one_id; ?>
                  </select>
                  <span id="men_look_one_error"></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <select name="men_brand_two_id" id="men_brand_two_id" class="form-control">
                    <?php echo $men_brand_two_id; ?>
                  </select>
                  <span id="men_look_two_error"></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <select name="men_brand_three_id" id="men_brand_three_id" class="form-control">
                    <?php echo $men_brand_three_id; ?>
                  </select>
                  <span id="men_look_three_error"></span>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-primary btn-xs" id="men_look_apply" name="men_look_apply" type="button"  onclick="trending_brands('men');">Apply
                </div>
              </div>
             </div>
            </div>

            <div class="row">
            <div class="col-md-12" id="men_looks">

            </div>
            </div>

            <div class="row">
            <div class="col-md-12">
              <div class="col-md-1">
                <div class="form-group">
                  <strong>WOMEN</strong>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <select name="women_brand_one_id" id="women_brand_one_id" class="form-control">
                  <?php echo $women_brand_one_id; ?>
                  </select>
                  <span id="women_look_one_error"></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                 <select name="women_brand_two_id" id="women_brand_two_id" class="form-control">
                  <?php echo $women_brand_two_id; ?>
                  </select>
                  <span id="women_look_two_error"></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                 <select name="women_brand_three_id" id="women_brand_three_id" class="form-control">
                  <?php echo $women_brand_three_id; ?>
                  </select>
                  <span id="women_look_three_error"></span>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-primary btn-xs" id="women_look_apply" name="women_look_apply" type="button" onclick="trending_brands('women');">Apply
                </div>
              </div>
             </div>
            </div>

            <div class="row">
            <div class="col-md-12" id="women_looks">

            </div>
            </div>
           
            <span class="error"></span>

          </div>

           <!--div class="box-header with-border">
            <h3 class="box-title"> Top Brands on Listing Page </h3>   
          </div--> 
          <!--div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-3">
                  <strong>Brands</strong>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                  <select multiple="true" name="top_brand_list" id="top_brand_list" class="form-control">
                    <?php echo $paid_brand_list; ?>
                  </select>
                </div>
                </div>
                <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-primary btn-xs" id="women_look_apply" name="women_look_apply" type="button" onclick="save_top_brand();">Apply
                </div>
              </div>
                <span class="error_"></span>
              </div>
            </div>

        </div-->
  </div>
</section>
<script type="text/javascript">
$( document ).ready(function() {
   $('#all_brand_one_id').pqSelect({bootstrap: { on: true },multiplePlaceholder: 'Select',checkbox: true,}).on("change", function(evt) {var val = $(this).val();});
   $('#all_brand_two_id').pqSelect({bootstrap: { on: true },multiplePlaceholder: 'Select',checkbox: true,}).on("change", function(evt) {var val = $(this).val();});
   $('#all_brand_three_id').pqSelect({bootstrap: { on: true },multiplePlaceholder: 'Select',checkbox: true,}).on("change", function(evt) {var val = $(this).val();});

   $('#men_brand_one_id').pqSelect({bootstrap: { on: true },multiplePlaceholder: 'Select',checkbox: true,}).on("change", function(evt) {var val = $(this).val();});
   $('#men_brand_two_id').pqSelect({bootstrap: { on: true },multiplePlaceholder: 'Select',checkbox: true,}).on("change", function(evt) {var val = $(this).val();});
   $('#men_brand_three_id').pqSelect({bootstrap: { on: true },multiplePlaceholder: 'Select',checkbox: true,}).on("change", function(evt) {var val = $(this).val();});

   $('#women_brand_one_id').pqSelect({bootstrap: { on: true },multiplePlaceholder: 'Select',checkbox: true,}).on("change", function(evt) {var val = $(this).val();});
   $('#women_brand_two_id').pqSelect({bootstrap: { on: true },multiplePlaceholder: 'Select',checkbox: true,}).on("change", function(evt) {var val = $(this).val();});
   $('#women_brand_three_id').pqSelect({bootstrap: { on: true },multiplePlaceholder: 'Select',checkbox: true,}).on("change", function(evt) {var val = $(this).val();});

   $('#top_brand_list').pqSelect({bootstrap: { on: true },multiplePlaceholder: 'Select',checkbox: true, maxSelect: 4 }).on("change", function(evt) {var val = $(this).val();});
   

});
function trending_brands(type){
  var brand_id_one,brand_id_two,brand_id_three = 0;
    if(type.trim()==="all"){
        var brand_id_one = $('#all_brand_one_id').val();
        var brand_id_two = $('#all_brand_two_id').val();
        var brand_id_three = $('#all_brand_three_id').val();
    }else if(type.trim()==="men"){
        var brand_id_one = $('#men_brand_one_id').val();
        var brand_id_two = $('#men_brand_two_id').val();
        var brand_id_three = $('#men_brand_three_id').val();
    }else if(type.trim()==="women"){ 
        var brand_id_one = $('#women_brand_one_id').val();
        var brand_id_two = $('#women_brand_two_id').val();
        var brand_id_three = $('#women_brand_three_id').val();
    }
    if(brand_id_one > 0 && brand_id_two >0 && brand_id_three > 0){
       $.ajax({
          url: '<?php echo base_url(); ?>webui/Homepage_settings/save_trending_brands',     
          type: 'post',
          data: { 'type' : type,'brand_id_one' : brand_id_one,'brand_id_two' : brand_id_two,'brand_id_three' : brand_id_three },
          success: function(data, status) {
            $('#error').html('Done');
          }
        });
    }
}

function save_top_brand(){
  var top_brand_list = $('#top_brand_list').val();
  if(top_brand_list){
    $.ajax({
          url: '<?php echo base_url(); ?>webui/Homepage_settings/save_top_brands',     
          type: 'post',
          data: { 'top_brand_list' : top_brand_list },
          success: function(data, status) {
            $('#error_').html('Done');
          }
    });
  }
}
 $( document ).ready(function() {
    $('.nav-item-banners').addClass("active");
    // $('.nav-banners-list').addClass("active");
  $('.nav-banners-list5').addClass("active");
    //$('.datepicker').datepicker();
 
    });
</script>