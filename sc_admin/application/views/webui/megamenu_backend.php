<?php
 /* echo $mm_women_html;exit;*/ 
?>
<script src="<?php echo base_url(); ?>assets/js/ajaxfileupload.js"></script> 
<section class="content-header">
  <h1>Mega Menu</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="#">WebUI</a></li>
    <li class="active">Mega Menu</li>
  </ol>
</section>
<section class="content">

  <div class="box">
    <div class="box-body">
    <div class="row">
      <div class="col-md-2 menu-item-type">
          <div class="form-group">
            <label for="" class="control-label">MegaMenu Type </label>
            <select id="menu-tem-type" class="form-control chosen-select" data-placeholder="Select">          
              <option value="men">Men</option>
              <option value="women">Women</option>
            </select>
          </div>
        </div>
      </div>
      <div class="row">      
        <div class="col-md-2 menu-item-type">
          <div class="form-group">
            <label for="" class="control-label">Menu Item Type </label>
            <select id="menu-tem-type" class="form-control chosen-select" data-placeholder="Select">          
              <option value="dynamic">
                Dynamic
              </option>
              <option value="custom">
                Custom
              </option>
            </select>
          </div>
        </div>
    		<div class="col-md-4 menu-category-type">
      		<div class="row  dynamic-menu-item-wrp">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="" class="control-label">Type </label>
                  <select class="form-control chosen-select" onchange="get_var_value($(this).val())" >
                    <option>
                    </option>
                    <option value="1" name="variation" >
                      Variation Type
                    </option>
                    <option Value="2" name="variation">
                      Variation Value
                    </option>
                  </select>
                </div>
              </div>
              <div class="col-md-6 menu-item-selected">
          		 <div class="form-group" id="variations_default">
                  <label for="" class="control-label">Select </label>
          			   <select class="form-control chosen-select ">                      
                   </select>
                </div>    		  
                <div class="form-group" id="variations">               
                </div>
              </div>    		
      		</div>
      		<!-- custom-->
      		<div class="row custom-menu-item-wrp" style="display:none;">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="" class="control-label">Label </label>
      			       <input class="form-control custom-label-text">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="" class="control-label">URL </label>
      			       <input class="form-control custom-url-text">
                </div>
              </div>
      		</div>
      		<!-- /custom-->
		  </div>		
      <div class="col-md-2 column-number-selected">
          <div class="form-group">
            <label for="" class="control-label">Column </label>
            <select class="form-control chosen-select" name="option">
              <option>
              </option>
              <option value="1">
                1
              </option>
              <option value="2" >
                2
              </option>
              <option value="3">
                3
              </option>
              <option value="4">
                4
              </option>
              <option value="5">
                5
              </option>
              <option value="6">
                6
              </option>
            </select>
          </div>
      </div>
		  <div class="col-md-2">
          <div class="form-group">
            <label for="" class="control-label">Class Name </label>
			       <input class="form-control custom-style-class">
          </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <div>&nbsp;</div>
          <span onclick="AddOptionToTable('women')" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add</span>
        </div>
      </div>
      </div>
    </div>
  </div>
      
  <div class="box box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Mega Menu Preview</h3>
      </div><!-- /.box-header -->
  		<div class="col-md-12 header-column">
  			<div class="col-md-2">COLUMN 1</div>
  			<div class="col-md-2">COLUMN 2</div>
  			<div class="col-md-2">COLUMN 3</div>
  			<div class="col-md-2">COLUMN 4</div>
  			<div class="col-md-2">COLUMN 5</div>
  			<div class="col-md-2">COLUMN 6</div>
  		</div>
    <div class="box-body megamenu-list">
   <?php  //echo @$mm_women_html; ?>
   <?php if(empty($mm_women_html)) 
          {
   ?>
      		<div class="col-md-2 col-1 connectedColumns">      		
      		</div>
      		<div class="col-md-2 col-2 connectedColumns">      		
      		</div>
      		<div class="col-md-2 col-3 connectedColumns">      		
      		</div>
      		<div class="col-md-2 col-4 connectedColumns">      		
      		</div>
      		<div class="col-md-4 col-7">
      			<div id="show_image">
      				<img name ="menu_image" id="previewing" src="<?php echo $mm_image; ?>" value="" />
      			</div>
      		</div>
      		<div class="col-md-2 col-5 connectedColumns">
      		</div>
      		<div class="col-md-2 col-6 connectedColumns">
      		</div>	
    <?php
          }else
          {
            echo $mm_women_html;
          }
    ?>	  
    </div>
  </div>

  <div class="box">
    <!--<div class="box-header with-border">
      <h3 class="box-title">Megamenu Setting</h3>
    </div> -->
    <div class="box-body">    
      <div class="row">
       <form role="form" id="megamenu_form" enctype="multipart/form-data"  method="POST" >      
        <div class="col-md-3">
          <div class="form-group">
            <label for="" class="control-label">Banner </label>
            <!-- <input type="file" class="form-control"> -->
      			<input id="file" name="file" class="file" type="file" >
            <input type="submit" name="submit" value="submit">
          </div>
        </div>
        </form>

        <div class="col-md-3">
          <div class="form-group">
            <label for="" class="control-label">URL </label>
            <input type="text" class="form-control">
          </div>
        </div>
        <div class="col-md-4">       
          <div class="form-group"> 
            <div>&nbsp;</div>
            <input type="submit" class="btn btn-primary btn-sm" ><i class="fa fa-save"></i> Upload Image</button>
            <div>&nbsp;</div>
            <div class="btn btn-primary btn-sm" onclick="PublishMenu()"  ><i class="fa fa-save"></i> Publish Menu</div>
          </div>        
          <div id="message"><?php @$error[0]; ?></div>
        </div>       
      </div>      
    </div>
  </div>
</section>
<style>
  .table-striped > tbody > tr:nth-of-type(2n+1){background: #fff;}
  table th{background: #eee; text-transform: uppercase; width:16%;}
  table img{margin-bottom: 20px; width:auto;}
  table .title{font-weight: bold;}
  .megamenu-list{margin:0;}
  .megamenu-list li{position: relative; padding: 5px 10px; list-style: none;}
  .table-striped li:hover{background: #f1f1f1; }
  table .btn-delete{ font-size: 12px; color:#333; position: absolute; right:10px; top:5px; cursor: pointer; }
  table .btn-delete:hover{color:#ff0000;}
  
  
  .megamenu-list .col-1, .megamenu-list .col-2, .megamenu-list .col-3, .megamenu-list .col-4 { height: 370px; }
  .header-column { font-weight:bold; background-color: #f1f1f1; padding: 5px 0 5px 15px; margin-bottom:5px;   }
  .megamenu-list li a {color: #333;}
  .megamenu-list .title{font-weight: bold;}
  .megamenu-list li:hover{ background-color: #f1f1f1; }
  .megamenu-list .btn-delete{ font-size: 12px; color:#333; position: absolute; right:10px; top:9px; cursor: pointer;  }
  .megamenu-list .btn-delete:hover{color:#ff0000;}
  #show_image { width: 315px; height: 180px; overflow:hidden; }
  #show_image img { visibility: hidden; }
  

</style>
<script type="text/Javascript">
$( document ).ready(function() {
	$('.nav-item-banners').addClass("active");
	$('.nav-megamenu-backend').addClass("active");
	$('.variations').hide();
	//$('#show_image').hide();
	
$('#menu-tem-type').change(function(){
  if($(this).val() == 'dynamic'){ 
    $('.dynamic-menu-item-wrp').show();
	$('.custom-menu-item-wrp').hide();
  }
  else if($(this).val() == 'custom'){ 	
	$('.custom-menu-item-wrp').show();
	$('.dynamic-menu-item-wrp').hide();
  }
});
  
});

function get_var_value(id){	
	if(id !=''){
        $.ajax({
            url: "<?php echo base_url() ?>webui/Megamenu/get_variation_type",
            type: 'POST',
            data: {'id':id},
            cache :true,
            async: false,
            success: function(response) {	
				$('#variations').html(response);
				// $("#variations").append(response); 
				$('.variations').show();
				document.getElementById('variations_default').style.display = 'none';
				//##-------------------------------------------------------------
				$(".chosen-select").chosen();
				$('.menu-item-selected select').change(function(){
					CategoryItemSelected();
				});
            }
        });
      }
}

function imageIsLoaded(e) {
  var img_path = '<?php echo $mm_image; ?>';
  img_path = e.target.result;
  $("#file").css("color","green");
  $('#image_preview').css("display", "block");
  $('#previewing').attr('src', img_path);
  $('#previewing').attr('width', '374px');
  $('#previewing').attr('height', '212px');
  $('#show_image').show();
  $('#show_image img').css('visibility','visible');
  $('#upload_banner').hide();
};
  
function change_img(){
  $("#message").empty(); // To remove the previous error message
  var file = $("#file").get(0).files[0];
  var imagefile = file.type; 
  var match= ["image/gif","image/png","image/jpg","image/jpeg"];
  if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]) || (imagefile==match[3])))
  {
      $('#previewing').attr('src','noimage.png');
      $("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
      return false;
  }
  else
  {
      var img_path = '<?php echo $mm_image; ?>';
      $('#previewing').attr('src',img_path);
      /*var reader = new FileReader();
      reader.onload = imageIsLoaded;
      reader.readAsDataURL($("#file").get(0).files[0]);*/      
  }
}
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/mega-menu-admin.js"></script>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript">
  $(function() {
  $('#Form1').submit(function(e) {
        e.preventDefault();
    $.ajaxFileUpload({
          url       :'<?php echo base_url(); ?>webui/megamenu/upload_img',
          fileElementId :'file',
          dataType    : 'json',
          success : function (data, status)
          {
            if(data.status == 'success'){
              location.reload();
            }else{
              alert('error');
            }

          },error: function(xhr) {
            console.log(xhr);
          }
        });
  });
});

</script>