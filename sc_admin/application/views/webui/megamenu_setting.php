<section class="content-header">
  <h1>Megamenu Setting</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="javascript: history.go(-1);">Megamenu Setting</a></li>
    <li class="active">Megamenu Setting</li>
  </ol>
</section>
<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Megamenu Setting</h3>
    </div><!-- /.box-header -->
    <form name="assignPer" id="assignPer" class="form-group" action="<?php echo base_url(); ?>webui/Homepage_settings/megamenu_category" method="post">
      <div class="box-body">
         <?php echo $this->session->flashdata('feedback'); ?>
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label for="category">Category  <span class="text-red">*</span></label>
            <select class="form-control" tabindex="7"  name="category" id="category" onchange='show_categoryList();'>
              <option value="">Select Category</option>
              <?php
              if(!empty($category)){
                foreach($category as $key=>$val){
                  echo '<option value="'.$key.'">'.$val.'</option>';
                }
              }
              ?>
            </select>
            <span class="text-red"> <?php echo form_error('category'); ?></span>
          </div>
        </div>
        </div>
        <div class="row">
          <div id="show_subcategory_list">
          <label class="control-label" for="categoryList"></label>
          </div>          
        </div>
               
        </div>
    </div>
   
    </form>

      </section>

  <script type="text/javascript">

function show_categoryList()
{
    var cat = $('#category').val();   
    
     if( cat !='' )
     {
       $(".page-overlay").show();
       $(".page-overlay .loader").css('display','inline-block');

          $.ajax({
              url: "<?php echo base_url() ?>webui/Homepage_settings/category_display/",
              type: 'POST',
              data: {'category':cat},
              cache: false,
              async: false,
              success: function(response) {
              $(".page-overlay").hide();
              $(".page-overlay .loader").hide();             
              $('#show_subcategory_list').html(response);
             
              }
          });
    }
}

  </script>
