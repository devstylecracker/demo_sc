<section class="content-header">
  <h1>Homepage - Top Look Settings </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>home">Website UI</a></li>
    <li class="active">Homepage - Top Look Settings </li>
  </ol>
</section>
<section class="content">
  <div class="box box-warning">
      <div class="box-header with-border">
        <h3 class="box-title">Top Looks</h3>   
      </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-1">
                <div class="form-group">
                  <strong>ALL</strong>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <input placeholder="Look ID" value="<?php echo @$top_looks_all[0]; ?>" name="all_look_one" id="all_look_one" class="form-control" type="text">
                  <span id="all_look_one_error"></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <input value="<?php echo @$top_looks_all[1]; ?>" placeholder="Look ID" name="all_look_two" id="all_look_two" class="form-control" type="text">
                  <span id="all_look_two_error"></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <input value="<?php echo @$top_looks_all[2]; ?>" placeholder="Look ID" name="all_look_three" id="all_look_three" class="form-control" type="text">
                  <span id="all_look_three_error"></span>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-primary btn-xs" id="all_look_apply" name="all_look_apply" type="button" onclick="top_look('all');">Apply
                </div>
              </div>
             </div>
            </div>

            <div class="row">
            <div class="col-md-12" id="all_looks">

            </div>
            </div>

            <div class="row">
            <div class="col-md-12">
              <div class="col-md-1">
                <div class="form-group">
                  <strong>MEN</strong>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <input value="<?php echo @$top_looks_men[0]; ?>" placeholder="Look ID" name="men_look_one" id="men_look_one" class="form-control" type="text">
                  <span id="men_look_one_error"></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <input value="<?php echo @$top_looks_men[1]; ?>" placeholder="Look ID" name="men_look_two" id="men_look_two" class="form-control" type="text">
                  <span id="men_look_two_error"></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <input value="<?php echo @$top_looks_men[2]; ?>" placeholder="Look ID" name="men_look_three" id="men_look_three" class="form-control" type="text">
                  <span id="men_look_three_error"></span>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-primary btn-xs" id="men_look_apply" name="men_look_apply" type="button"  onclick="top_look('men');">Apply
                </div>
              </div>
             </div>
            </div>

            <div class="row">
            <div class="col-md-12" id="men_looks">

            </div>
            </div>

            <div class="row">
            <div class="col-md-12">
              <div class="col-md-1">
                <div class="form-group">
                  <strong>WOMEN</strong>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <input value="<?php echo @$top_looks_women[0]; ?>" placeholder="Look ID" name="women_look_one" id="women_look_one" class="form-control" type="text">
                  <span id="women_look_one_error"></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <input value="<?php echo @$top_looks_women[1]; ?>" placeholder="Look ID" name="women_look_two" id="women_look_two" class="form-control" type="text">
                  <span id="women_look_two_error"></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <input value="<?php echo @$top_looks_women[2]; ?>" placeholder="Look ID" name="women_look_three" id="women_look_three" class="form-control" type="text">
                  <span id="women_look_three_error"></span>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-primary btn-xs" id="women_look_apply" name="women_look_apply" type="button" onclick="top_look('women');">Apply
                </div>
              </div>
             </div>
            </div>

            <div class="row">
            <div class="col-md-12" id="women_looks">

            </div>
            </div>
           
            <span class="error"></span>

          </div>
        </div>
  </div>
</section>
<script type="text/javascript">
  $('#all_looks').html('');
  get_look_img('all',"<?php echo @$top_looks_all[0]; ?>"); get_look_img('all',"<?php echo @$top_looks_all[1]; ?>"); get_look_img('all',"<?php echo @$top_looks_all[2]; ?>");
  $('#men_look').html('');
  get_look_img('men',"<?php echo @$top_looks_men[0]; ?>"); get_look_img('men',"<?php echo @$top_looks_men[1]; ?>"); get_look_img('men',"<?php echo @$top_looks_men[2]; ?>");
  $('#women_look').html('');
  get_look_img('women',"<?php echo @$top_looks_women[0]; ?>"); get_look_img('women',"<?php echo @$top_looks_women[1]; ?>"); get_look_img('women',"<?php echo @$top_looks_women[2]; ?>");
  function top_look(type){
    var look_id_one,look_id_two,look_id_three = 0;
    if(type.trim()==="all"){
        var look_id_one = $('#all_look_one').val();
        var look_id_two = $('#all_look_two').val();
        var look_id_three = $('#all_look_three').val();
    }else if(type.trim()==="men"){
        var look_id_one = $('#men_look_one').val();
        var look_id_two = $('#men_look_two').val();
        var look_id_three = $('#men_look_three').val();
    }else if(type.trim()==="women"){ 
        var look_id_one = $('#women_look_one').val();
        var look_id_two = $('#women_look_two').val();
        var look_id_three = $('#women_look_three').val();
    }
    //console.log(look_id_one +'-'+ look_id_two +'-'+ look_id_three);
    if(look_id_one > 0 && look_id_two >0 && look_id_three > 0){
       $.ajax({
          url: '<?php echo base_url(); ?>webui/Homepage_settings/save_top_look',     
          type: 'post',
          data: { 'type' : type,'look_id_one' : look_id_one,'look_id_two' : look_id_two,'look_id_three' : look_id_three },
          success: function(data, status) {
            var obj = jQuery.parseJSON(data);
            if(type=="all"){
                  if(obj[0]!='') { $('#all_look_one_error').html(obj[0]).addClass('text-red'); }
                  if(obj[1]!='') { $('#all_look_two_error').html(obj[1]).addClass('text-red'); }
                  if(obj[2]!='') { $('#all_look_three_error').html(obj[2]).addClass('text-red'); }
                  if(obj[0]=='' && obj[1]=='' && obj[2]=='') {  $('#all_looks').html('');
                  get_look_img('all',look_id_one); get_look_img('all',look_id_two); get_look_img('all',look_id_three);
                  $('#all_look_three_error').html('This will reflect after 10 mins on platform').addClass('text-green'); }
                }else if(type=="men"){
                  if(obj[0]!='') { $('#men_look_one_error').html(obj[0]).addClass('text-red'); }
                  if(obj[1]!='') { $('#men_look_two_error').html(obj[1]).addClass('text-red'); }
                  if(obj[2]!='') { $('#men_look_three_error').html(obj[2]).addClass('text-red'); }
                  if(obj[0]=='' && obj[1]=='' && obj[2]=='') {  $('#men_look').html('');
                  get_look_img('men',look_id_one); get_look_img('men',look_id_two); get_look_img('men',look_id_three);
                  $('#men_look_three_error').html('This will reflect after 10 mins on platform').addClass('text-green'); }
                }else if(type=="women"){
                  if(obj[0]!='') { $('#women_look_one_error').html(obj[0]).addClass('text-red'); }
                  if(obj[1]!='') { $('#women_look_two_error').html(obj[1]).addClass('text-red'); }
                  if(obj[2]!='') { $('#women_look_three_error').html(obj[2]).addClass('text-red'); }
                  if(obj[0]=='' && obj[1]=='' && obj[2]=='') { $('#women_look').html('');
                  get_look_img('women',look_id_one); get_look_img('women',look_id_two); get_look_img('women',look_id_three); $('#women_look_three_error').html('This will reflect after 10 mins on platform').addClass('text-green'); }
                }
          },
          error: function(xhr, desc, err) {
            console.log(err);
          }
      });
    }else{
      $('.error').html('Please enter LOOK IDs').addClass('text-red');
    }
  }

  function get_look_img(type,lookId){
      $.ajax({
          url: '<?php echo base_url(); ?>webui/Homepage_settings/get_look_img',     
          type: 'post',
          data: { 'lookId' : lookId },
          success: function(data, status) {
            if(type == 'all'){
              $('#all_looks').append(data);
            }else if(type == 'men'){
              $('#men_looks').append(data);
            }else if(type == 'women'){
              $('#women_looks').append(data);
            }
          },
          error: function(xhr, desc, err) {
            console.log(err);
          }
      });

  }
   $( document ).ready(function() {
    $('.nav-item-banners').addClass("active");
    // $('.nav-banners-list').addClass("active");
  $('.nav-banners-list3').addClass("active");
    //$('.datepicker').datepicker();
  
    });
</script>