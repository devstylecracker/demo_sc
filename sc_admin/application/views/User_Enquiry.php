<?php $users_menu=$this->data['users_menu'];
$total_rows = $this->data['total_rows'];
@$table_search = $table_search == '0' ? '' : $table_search;
?>
<section class="content-header">
	<h1>Manage User Enquiries </h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url(); ?>home">Home</a></li>
		<li><a href="<?php echo base_url(); ?>User_Enquiry">User Enquiry</a></li>
		<li class="active">Manage User Enquiries</li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">List of User Enquiries <small class="label label-info"><?php echo @$total_rows; ?></small></h3>
			<div class="pull-right">
			</div>
		</div>
		<div class="box-body">
			<?php echo $this->session->flashdata('feedback'); ?>
			<form name="frmsearch" class="form-group" id="frmsearch" action="<?php echo base_url(); ?>User_Enquiry" method="post">
			<input type="hidden"  name="is_download" id="is_download" value="0" />
				<div class="row">
					<div class="col-md-2">
					 <?php @$search_by = set_value('search_by')=='' ? @$search_by : set_value('search_by'); ?>
						<select name="search_by" id="search_by" class="form-control input-sm" >
							<option value="0">Search by</option>
							<option selected value="1" <?php echo set_value('search_by')==1 ? 'selected' : ''; ?>>User Name</option>
							<option value="2" <?php echo set_value('search_by')==2 ? 'selected' : ''; ?>>E-Mail</option>
							<option value="3" <?php echo set_value('search_by')==3 ? 'selected' : ''; ?>>Contact No</option>
							<option value="4" <?php echo set_value('search_by')==4 ? 'selected' : ''; ?>>Message</option>
							<option value="5" <?php echo set_value('search_by')==5 ? 'selected' : ''; ?>>Enquiry Date Time</option>
							<option value="6" <?php echo $search_by==6 ? 'selected' : ''; ?> >Date Range</option>
						</select>
					</div>
					<div class="col-md-3">
						<div class="input-group">
						 <div id ="filter" style="display: block;" >
							<input type="text" name="table_search" id="table_search" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo set_value('table_search'); ?>" required=""/>
						</div>
							<div class="input-group-btn">
								<div id ="daterange" style="display: none;">
										
										  <div class="input-group">
											<input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm" id="date_from" value="<?php echo set_value('date_from'); ?>">
										  </div>           
										
										  <div class="input-group">
											<input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="<?php echo set_value('date_to'); ?>">          
										  </div>
										
							    </div>
								<button class="btn btn-sm btn-default" type="button" onclick="submitForm();"><i class="fa fa-search"></i></button>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<a href="" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
					</div>
					<div class="col-md-5">
						<!--div class="records-per-page pull-right">
							<select name="records-per-page" class="form-control input-sm">
								<option>Per Page</option>
								<option value="10" selected="selected">10</option>
								<option value="25">25</option>
								<option value="50">50</option>
							</select>
						</div-->
						<button title="Download Excel" id="download_excel" class="btn btn-primary btn-xs"  onclick="downloadExcel();"><i class="fa fa-file-excel-o"></i> Download Excel</button>
						<div class="pull-right">
							<?php echo $this->pagination->create_links(); ?>
						</div>
					</div>
				</div>
			</form>

			<div class="table-responsive">
				<table class="table table-bordered table-striped dataTable" id="datatable">
					<thead><tr>
						<th>ID</th>
						<th>Name</th>
						<th>Email</th>
						<th>Contact No</th>
						<th>Message</th>
						<th>Subject</th>
						<th>Enquiry Date Time</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if(!empty($User_enquirys ))
					{	$i=0;
						foreach($User_enquirys as $User_enquiry)
						{	$i++;
							?>
							<tr>
								<td><?php echo $User_enquiry->id; ?></td>
								<td><?php echo $User_enquiry->name; ?></td>
								<td><?php echo $User_enquiry->email; ?></td>
								<td><?php echo $User_enquiry->contact_no; ?></td>
								<td><?php echo $User_enquiry->message; ?></td>
								<td><?php echo $User_enquiry->subject;?></td>
								<td><?php echo $User_enquiry->enquiry_date_time; ?></td>
							</tr>
							<?php
						}
					}else
					{
						?>
						<tr>
							<td >No Records Found</td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
			<?php echo $this->pagination->create_links(); ?>
		</div>
	</div>
</section>
<script type="text/Javascript">
$( document ).ready(function() {
	$('.nav-item-enquiry').addClass("active");
});
 
$(function () {
      $("#date_from").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true});
      $("#date_to").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true });     
      $(".nav-tabs a").click(function(){
        $(this).tab('show');
      });
      $('.nav-tabs a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
      });
    });

var filterType = $('#search_by').val();
if(filterType == 6)
{   
    $("#filter").hide();
    $("#daterange").show();
    $("#table_search").removeAttr('required');
    $("#date_from").attr('required','');
}

 $("#search_by").change(function(){

    var filterType = $(this).val();
    if(filterType == 6)
    {
        $("#filter").hide();
        $("#daterange").show();
		//$("#table_search").hide();
        $("#table_search").removeAttr('required');
        $("#date_from").attr('required','');
    }else
    {
       $("#filter").show();
      $("#daterange").hide();

    }
   
  });

function downloadExcel()
	{  
     var search_by = '';
     var table_search = '';
     $("#is_download").val('1');
     search_by = $('#search_by').val();
     search_by = search_by.trim();
     table_search = $('#table_search').val();
     $('#table_search').removeAttr('required');
     table_search = table_search.trim();
     var is_download = $("#is_download").val();
	 if(search_by == 6)
     {
        var dateFrom = $("#date_from").val();
        var dateTo = $("#date_to").val();
         
        var date_search = dateFrom+','+dateTo;
        encode_table_search = window.btoa(date_search);
     }
     else 
     {
        encode_table_search = table_search;
     }
     if(!encode_table_search){ encode_table_search = '0'; }  
     var newhref = '<?php echo base_url(); ?>'+'User_Enquiry/exportExcel/downloadExcel'+'/'+search_by+'/'+encode_table_search+'/'+is_download;
     $('#frmsearch').attr('action',newhref);

	}
function submitForm(){ 
   $("#is_download").val('0');
   var newhref = '<?php echo base_url(); ?>'+'User_Enquiry/';
     
   $('#frmsearch').attr('action',newhref);
   $('#frmsearch').submit();
}	
</script>
