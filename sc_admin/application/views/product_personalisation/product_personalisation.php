<section class="content-header">
    <h1>Product Personalisation Logic</h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>home">Home</a></li>
      <li class="active">Product Personalisation Logic</li>
    </ol>    
</section>
<section class="content">
    <div class="box box-solid">
    <div class="box-body">
         <div class="row">
         <div class="col-md-4">
                    <div class="form-group">
                      <label for="gender">Gender</label>
                      <select class="form-control" name="gender" id="gender" tabindex="1" placeholder="Select Gender">
                        <option value="">Select</option>
                        <option value="1">Women</option>
                        <option value="3">Men</option>
                        </select>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="peronality">Personality</label>
                      <select class="form-control chosen-select" name="personality" id="personality" tabindex="2" placeholder="">
                        
                      </select>
                    </div>
                  </div>                 
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="">Category</label>
                      <select class="form-control chosen-select" name="category" id="category" tabindex="3" placeholder="">
                       
                      </select>
                    </div>
                  </div>
                  </div> 

                <div class="row" id="all_attributes_data">
 
                </div>   

                </div>

                <div class="box-footer text-right">
                <button type="submit" tabindex="30" class="btn btn-sm btn-primary" id="save_data"><i class="fa fa-save"></i> Save</button>                
              </div>

              </div>

                <div class="box box-solid">
                  <div class="box-header">
                  <h3 class="box-title">Existing Logic</h3>                  
                  </div>
            <div class="box-body">

            <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th width="40%">Personality</th>
                          <th>Category</th>
                          <th width="40%">Attributes</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="personalisation_logic">
                      <?php if(!empty($existing_data)) {  $i=1;
                        foreach($existing_data as $val){
                      ?> 
                        <tr class="personalisation_logic">
                            <td width="40%"><?php echo strip_tags($this->product_personalisation_model->get_bucket_name($val['object_value'])); ?></td>
                            <td><?php echo $val['cat_name']; ?></td>
                            <td width="40%"><?php echo $this->product_personalisation_model->attribute_value_data($val['attribute_value_ids']); ?></td>
                            <td><button onclick="" type="button" class="btn btn-xs btn-primary btn-delete deleteProd" data-id="<?php echo $val['id']; ?>" data-toggle="confirmation"><i class="fa fa-trash-o"></i> Delete</button></td>
                        </tr>
                      <?php $i++; } ?>
                        <tr id="personalisation_logic_id">
                            <td colspan="3"><button onclick="load_more_data(1);" type="button" class="btn btn-xs btn-primary"> Load More</button></td>
                        </tr>
                      <?php }else{ ?>
                        <tr>
                            <td colspan="3">No Records Found</td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>

                   </div>
                  </div>
</section>
<script type="text/javascript">
  $(document).ready(function(){

    $('#save_data').click(function(){
        var personality = $('#personality').val();
        var category = $('#category').val();
        var attributes = $('#attributes').val();
        var attr = ''; var i=0;
        $('select[name="attributes[]"]').each(function(){
          if($(this).val()!='' && $(this).val()!=null){
            if(i!=0){ attr = attr+','; }
            var newArray = $(this).val();
            newArray = newArray.filter(function(v){return v!==''});
            attr = attr+newArray.join(",");
            i=parseInt(i)+1;
          }
        });

        $.ajax({
          url: "<?php echo base_url(); ?>product_personalisation/save_data",
          type: 'POST',
          data: { 'personality' : personality,'category':category,'attributes':attr },
          cache :true,
          async: true,
          success: function(response) {
            location.reload();
          }
        });
    });
    
    $('#gender').change(function(){
      var gender = $(this).val();
      $('#all_attributes_data').html('');
      $.ajax({
          url: "<?php echo base_url(); ?>product_personalisation/show_gender_specific_personalisation",
          type: 'POST',
          data: { 'gender' : gender },
          cache :true,
          async: true,
          success: function(response) {
            var obj = jQuery.parseJSON(response);
            var perosonalisation_html = '';

            $.each(obj.perosonalisation, function(index, value) {
              if(gender==3){
                perosonalisation_html=perosonalisation_html+'<option value="'+value.id+'">'+value.body_shape+' '+value.style+' '+' '+value.work_style+' '+value.personal_style+'</option>';  
              }else{
                perosonalisation_html=perosonalisation_html+'<option value="'+value.id+'">'+value.body_shape+' '+value.style+'</option>';
              }
            });
            if(perosonalisation_html!=''){
              perosonalisation_html = '<option value="0">Select</option>'+perosonalisation_html;
              $('#personality').html(perosonalisation_html);
              $("#personality").val('').trigger("chosen:updated");
            }

          }
      });

    });


   $('#personality').change(function(){
      var personality = $(this).val();
      $('#all_attributes_data').html('');
      $.ajax({
          url: "<?php echo base_url(); ?>product_personalisation/show_category_specific_personalisation",
          type: 'POST',
          data: { 'personality' : personality },
          cache :true,
          async: true,
          success: function(response) {
            var obj = jQuery.parseJSON(response);
            var perosonalisation_html = '';

            $.each(obj.category, function(index, value) {
              perosonalisation_html=perosonalisation_html+'<option value="'+value.object_id+'">'+value.category_name+'</option>';
            });
            if(perosonalisation_html!=''){
              perosonalisation_html = '<option value="0">Select</option>'+perosonalisation_html;
              $('#category').html(perosonalisation_html);
              $("#category").val('').trigger("chosen:updated");
            }

          }
      });

    });

   $('#category').change(function(){
      var personality = $('#personality').val();
      var category = $('#category').val();

      $.ajax({
          url: "<?php echo base_url(); ?>product_personalisation/show_attributes",
          type: 'POST',
          data: { 'personality' : personality,'category':category },
          cache :true,
          async: true,
          success: function(response) {
            $('#all_attributes_data').html(response);
            $(".chosen-select").chosen();
          }
      });

    });

   delete_pa_id();

  });

  function delete_pa_id(){
    /* Delete pa_object_mapping */
    $('[data-toggle=confirmation]').confirmation({
      title:'Are you sure?',
      onConfirm : function(){
        var del_id = $(this).closest('.personalisation_logic').find('.deleteProd').attr('data-id');
        $.ajax({
           type: "POST",
           url: "<?php echo base_url(); ?>"+"Product_personalisation/delete_pa/",
           data: { prodID: del_id },
           cache:false,
           success:
              function(data){
                  location.reload();
              }

          });
        }
    });
  }

  function load_more_data(id){
    $('#personalisation_logic_id').remove();
    $.ajax({
          url: "<?php echo base_url(); ?>product_personalisation/load_more_data",
          type: 'POST',
          data: { 'id' : id },
          cache :true,
          async: true,
          success: function(response) {
            id = parseInt(id)+1; var data='';
            data=response+'<tr id="personalisation_logic_id"><td colspan="3"><button onclick="load_more_data('+id+');" type="button" class="btn btn-xs btn-primary"> Load More</button></td></tr>';
            $('#personalisation_logic').append(data);
            delete_pa_id();
          }
    });
  }

</script>