<section class="content-header">
  <h1>Personalised Product Up-sell Emails Logic</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">Personalised Product Up-sell Emails Logic</li>
  </ol>
</section>
<section class="content">
  <!--<div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title">Map Up-Selling Categories</h3>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-2">
          <div class="form-group1">
            <select class="form-control" name="gender" id="gender" tabindex="1" placeholder="Select Gender">
              <option value="">Select</option>
              <option value="1">Women</option>
              <option value="3">Men</option>
            </select>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group1">
            <select class="form-control chosen-select" name="category[]" id="category" tabindex="3" data-placeholder="Select Category" multiple>
            </select>
          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group1">
            <label for=""></label>
            <button tabindex="30" class="btn btn-sm btn-primary" id="save_data"><i class="fa fa-save"></i> Save</button>
          </div>
        </div>
      </div>
    </div>
  </div>-->

  <div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title">Check Logic</h3>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-2">
          <div class="form-group">
            <input type="text" name="order_id" id="order_id" class="form-control" placeholder="Order Id">
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <!--<button tabindex="30" class="btn btn-sm btn-primary" id="show_results"><i class="fa fa-save"></i> Show Results</button>-->
            <button tabindex="30" class="btn btn-sm btn-secondary" id="show_comp_results"><i class="fa fa-save"></i> Show Complementary Results</button>
          </div>
        </div>
      </div>

      <div class="row111" id="rec_res"></div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function() {
    $('#gender').change(function() {
      var gender = $('#gender').val();
      $.ajax({
        url: "<?php echo base_url(); ?>product_personalisation_email/show_gender_specific_categories",
        type: 'POST',
        data: {
          'gender': gender
        },
        cache: true,
        async: true,
        success: function(response) {
          var obj = jQuery.parseJSON(response);
          var perosonalisation_html = '';
          $.each(obj.category, function(index, value) {
            if (jQuery.inArray(value.id, obj.save_cat) >= 0) {
              perosonalisation_html = perosonalisation_html + '<option value="' + value.id + '" selected >' + value.category_name + '</option>';
            } else {
              perosonalisation_html = perosonalisation_html + '<option value="' + value.id + '">' + value.category_name + '</option>';
            }
          });
          $('#category').html(perosonalisation_html);
          $(".chosen-select").chosen();
          $("#category").trigger("chosen:updated");

        }
      });
    });

    $('#save_data').click(function() {
      var gender = $('#gender').val();
      var category = $('#category').val();

      $.ajax({
        url: "<?php echo base_url(); ?>product_personalisation_email/save_data",
        type: 'POST',
        data: {
          'gender': gender,
          'category': category
        },
        cache: true,
        async: true,
        success: function(response) {

        }
      });

    });

    $('#show_results').click(function() {
      $('.page-overlay').show();
      $('.loader').show();
      var order_id = $('#order_id').val();
      $.ajax({
        url: "<?php echo base_url(); ?>product_personalisation_email/show_results",
        type: 'POST',
        data: {
          'order_id': order_id
        },
        cache: true,
        async: true,
        success: function(response) {
          $('#rec_res').html(response);
          $('.page-overlay').hide();
          $('.loader').hide();
        }
      });
    });


    $('#show_comp_results').click(function() {
      $('.page-overlay').show();
      $('.loader').show();
      var order_id = $('#order_id').val();
      var orderprdid = $('#order_id').val();
      
      $.ajax({
        url: "<?php echo base_url(); ?>product_personalisation_email/show_comp_results",
        type: 'POST',
        data: {
          'order_id': order_id,'orderprdid': orderprdid
        },
        cache: true,
        async: true,
        success: function(response) {
          $('#rec_res').html(response);
          $('.page-overlay').hide();
          $('.loader').hide();
        }
      });
    });

  });
</script>
