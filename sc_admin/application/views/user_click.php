<?php 
$users_menu=$this->data['users_menu'];
    //$total_rows = $this->data['total_rows'];     
?>
<section class="content-header">
        <h1>User <?php echo $type; ?> Click Data</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>
            <li><a href="<?php echo base_url(); ?>tag"></a></li>
            <li class="active">User <?php echo $type; ?> Click Data</li>
          </ol>
</section>
<section class="content">
        <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">List of User <?php echo $type; ?> Click <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3>
                  <div class="pull-right">                  
                    </div>
                  </div>
                    <div class="box-body">
                     <?php echo $this->session->flashdata('feedback'); ?>                  

                <div class="table-responsive">
                  <table class="table table-bordered table-striped dataTable" id="datatable">
                    <thead><tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Datetime</th>                      
                    </tr>
                    </thead>
                    <tbody>
                          <?php

                            if(!empty($user_click ))
                            { $i=0;
                                foreach($user_click as $val)
                                { $i++;
                            ?>
                              <tr>
                              <?php 
                                if($type =='Look')
                                {
                              ?>
                               <td><a href="<?php echo base_url(); ?>lookcreator/look_view/<?php echo $val['id']; ?>" target="_blank" ><?php echo $val['id']; ?></a></td>
                              <?php
                                }elseif($type =='Product')
                                {
                               ?>
                                <td><a href="<?php echo base_url(); ?>product_new/product_view/<?php echo $val['id']; ?>" target="_blank"><?php echo $val['id']; ?></a></td>
                               <?php
                                }elseif($type=='Favourite')
                                {
                                ?>
                                <td><a href="<?php echo base_url(); ?>lookcreator/look_view/<?php echo $val['id']; ?>" target="_blank" ><?php echo $val['id']; ?></a></td>
                                <?php  }
                              
                              ?>
                        <td><?php echo $val['name']; ?></td> 
                        <td><?php echo $val['datetime']; ?></td>                           
                      </tr>
                    <?php
                        }
                      }else
                      {
                    ?>
                        <tr>
                          <td >No Records Found</td>
                        </tr>
                    <?php
                      }
                    ?>
                    </tbody>
        </table>  
           <?php //echo $this->pagination->create_links(); ?>

            </div>
      </div>
      </div>
</section>

