<section class="content">
<div class="error-page">
		<div class="col-md-offset-4">
            <h1 class="headline text-yellow"> <i class="fa fa-eye-slash"></i></h1>
        </div>
            <div class="error-content">
              <h3>Permission Denied</h3>
              <p>
                We could not find the page you were looking for.
                Meanwhile, you may <a href="<?php echo base_url(); ?>home">return to dashboard</a> or try using the search form.
              </p>
              
            </div><!-- /.error-content -->
</div><!-- /.error-page -->
</section>
