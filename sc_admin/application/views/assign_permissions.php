<section class="content-header">
	<h1>Assign Permissions</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url(); ?>home">Home</a></li>
		<li><a href="javascript: history.go(-1);">User Management</a></li>
		<li class="active">Assign Permissions</li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Assign Permissions</h3>
		</div><!-- /.box-header -->
		<label for="selectall" id="selectControl">Select/Unselect All</label>
    	<input type="checkbox" id="selectall"/>
		<form name="assignPer" id="assignPer" class="form-group" action="<?php echo base_url(); ?>users/add_permissions" method="post">
			<div class="box-body">
				<?php
				$users_menu = array();
				foreach($menu_permissionss as $val){ ?>

					<?php		$users_menu[] = $val->module_id;
				}
				if(!empty($modules_data)){
					$old_module_name = '';?>


					<?php
					foreach($modules_data as $val){

						$module_name = explode("_", $val->module_name); ?>

						<?php if($module_name['0'] != $old_module_name){ ?>
						<br><label class="control-label" for="categoryName"><?php echo ucfirst($module_name[0]); ?></label>
							<?php }	?>

							<?php if(in_array($val->id,$users_menu)) { ?>
								<input type="checkbox" name="assign_per[]" value="<?php echo $val->id; ?>" checked>
								<?php }else{ ?>
									<input type="checkbox" name="assign_per[]" value="<?php echo $val->id; ?>">
									<?php } ?>

									<?php //echo str_replace('_',' ',$val->module_name); ?>
									<?php echo $module_name['1']; ?>

									<?php
									$old_module_name = $module_name['0']; ?>

									<?php 	} ?>


									<?php	}
									else
									{ echo "Nothing to display"; }
									?>
								</div>
							</div>
							<input type="hidden" name="user_id" id="user_id" value="<?php echo $edit_user; ?>">

							<div class="box-footer">
								<button class="btn btn-primary" type="submit">Submit</button>
							</div>

					</form>


				</div>
			</section>



<script type="text/javascript">
	$(function() {  
	$("#selectall").click(function(){
		 $('input:checkbox').not(this).prop('checked', this.checked);
		});
	});
</script>


