<?php 
// echo "<pre>";print_r($collection);?>
<section class="content-header">
  <h1>View Collection</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url();?>">Home</a></li>
    <li><a href="<?php echo base_url();?>collection/collection_mgmt">Collections Management</a></li>
    <li class="active">View Collection</li>
  </ol>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Details</h3>

        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <p>
            <label>Name</label><br />
          <?php echo @$collection['collection_name']; ?>
            </p>
        <p>
          <label>Short Description</label><br />
        <?php echo @$collection['collection_short_desc']; ?>
          </p>
		<p><label>Long Description</label><br />
         <?php echo @$collection['collection_long_desc']; ?>
            </p>

		<p><label>Tags</label><br />
			<?php if(!empty($tag_data)){
				foreach($tag_data as $val){
					echo $val->id.' : '.$val->name.'</br>';
				}
			} ?>
		</p>
		<p><label>Start Date</label><br />
			<?php echo @$collection['collection_date_from']; ?>
		</p>
		<p><label>End date</label><br />
			<?php echo @$collection['collection_date_to']; ?>
		</p>
		<?php if($collection['pa_body_shape'] != ''){ ?>
		<p><label>Body Shape</label><br />
			<?php echo @$this->Pa_model->get_answer(unserialize($collection['pa_body_shape']),'pa_body_shape'); ?>
		</p>
		<?php } ?>
		<?php if($collection['pa_age'] != ''){ ?>
		<p><label>Age Range</label><br />
			<?php echo @$this->Pa_model->get_answer(unserialize($collection['pa_age']),'pa_age'); ?>
		</p>
		<?php } ?>
		<?php if($collection['pa_budget'] != ''){ ?>
		<p><label>Budget</label><br />
			<?php echo @$this->Pa_model->get_answer(unserialize($collection['pa_budget']),'pa_budget'); ?>
		</p>
		<?php } ?>
		<?php if($collection['pa_pref1'] != ''){ ?>
		<p><label>Style Preference 1</label><br />
			<?php echo @$this->Pa_model->get_answer(unserialize($collection['pa_pref1']),'pa_pref1'); ?>
		</p>
		<?php } ?>
		<?php if($collection['pa_pref2'] != ''){ ?>
		<p><label>Style Preference 2</label><br />
			<?php echo @$this->Pa_model->get_answer(unserialize($collection['pa_pref2']),'pa_pref2'); ?>
		</p>
		<?php } ?>
		<?php if($collection['pa_pref3'] != ''){ ?>
		<p><label>Style Preference 3</label><br />
			<?php echo @$this->Pa_model->get_answer(unserialize($collection['pa_pref3']),'pa_pref3'); ?>
		</p>
		<?php } ?>
		<!--p><label>Collection Type</label><br />
			<?php echo @$collection['collection_type']; ?>
		</p-->

              </div>
      </div>

    </div>
    <div class="col-md-6">
      <div class="box box-primary">
	  <div class="box-header with-border">
        <h3 class="box-title">Product Info</h3>
		</div>
        <div class="box-body">
          <ul class="products-list product-list-inline products-list-grid">
			
			<?php if(!empty($collection['collection_products'])){
				foreach($collection['collection_products'] as $val){ ?>
			
				<li class="item">
				  <div class="product-img">
					<img src="https://www.stylecracker.com/sc_admin/assets/products/thumb_340/<?php echo $val['image']; ?>" alt="">
				  </div>
				</li>
			
			<?php }
				}else{
					echo "No Products found";
				} ?>
          </ul>
        </div>

      </div>
	  
	  <div class="box box-primary">
	  <div class="box-header with-border">
       <h3 class="box-title">Collection Image</h3>
	   </div>
        <div class="box-body">
          <ul class="products-list product-list-inline products-list-grid">
			
			<?php if(@$collection['collection_img'] != ''){ ?>
			
				<li class="item">
				  <div class="product-img">
					<img src="<?php echo $this->config->item('collection_img_url'); ?><?php echo $collection['collection_img']; ?>" alt="">
				  </div>
				</li>
			
			<?php }else{
					echo "No Image found";
				} ?>
          </ul>
        </div>

      </div>
		

    </div>
  </div>
  </div>
</section>
