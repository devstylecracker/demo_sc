<?php 
$product_ids = array();
// if(!empty($_POST)){echo "<pre>";print_r($_POST);}?>
<section class="content-header">
  <h1>Edit Collection</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url();?>">Home</a></li>
    <li><a href="<?php echo base_url();?>/collections-management">Collections Management</a></li>
    <li class="active">Edit Collection</li>
  </ol>
</section>
<section class="content">

  <div class="row">
  <form action="<?php echo base_url().'edit-collection/'.@$collection['collection_id']; ?>" role="form" id="collection_form" enctype="multipart/form-data" method="POST">
     <div class="col-md-6">
      <div class="box box-info">
	   <div class="box-header">
	   <h3 class="box-title">Products</h3>
	   <div class="pull-right">
		<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#products-popup" id="popup_button" data-attr="edit-collection">Add Products...</button>
	   </div>
	   </div>
        <!-- /.box-header -->
        <div class="box-body" style="min-height:240px;">
          <ul id="final_list" class="products-list product-list-inline products-list-grid">
			<?php if(!empty($collection['collection_products'])){
					foreach($collection['collection_products'] as $val){ ?>
					<li class="item" id="final_list_<?php echo $val['product_id']; ?>">
						<div class="product-img">
							<img data-img-link="<?php echo LIVE_SITE_URL; ?>assets/products/<?php echo $val['image']; ?>" class="lazy" id="<?php echo $val['product_id']; ?>" src="<?php echo LIVE_SITE_URL; ?>assets/products/thumb_160/<?php echo $val['image']; ?>" width="80px" height="100px">
						</div>
						<a class="btn-prod-remove" href="javascript:void(0);" onclick="remove_product(<?php echo $val['product_id']; ?>);change_list(<?php echo $val['product_id']; ?>);"><i class="fa fa-times-circle-o"></i></a>
					</li>
			<?php $product_ids[] = $val['product_id']; }//end foreach
			}//end if ?>
          </ul>
        </div>
      </div>
	  <div class="error text-red"><?php echo @$error[0]; ?></div>
	 
	  <input type="hidden" class="form-control" name="product_ids" id="product_ids" value="<?php $product_id = implode(",",$product_ids); echo $product_id; ?>">
	  <input type="hidden" class="form-control" name="product_array" id="product_array" value="">
	  <input type="hidden" class="form-control" name="pa_body_shape" id="pa_body_shape" value="">
	  <input type="hidden" class="form-control" name="pa_age" id="pa_age" value="">
	  <input type="hidden" class="form-control" name="pa_budget" id="pa_budget" value="">
	  <input type="hidden" class="form-control" name="pa_pref1" id="pa_pref1" value="">
	  <input type="hidden" class="form-control" name="pa_pref2" id="pa_pref2" value="">
	  <input type="hidden" class="form-control" name="pa_pref3" id="pa_pref3" value="">
		
	
 <!-- pa -->
  <div class="box box-info">
	   <div class="box-header">
	   <h3 class="box-title">PA Info</h3>	
		<div class="pull-right">
		 <a href="#pa-popup" data-toggle="modal" onclick="get_pa_answers();" class="btn btn-info btn-sm" id="pa_popup_button" data-attr="edit-collection">PA Setting...</a>
	   </div>	   
	   </div>
        <!-- /.box-header -->
        <div class="box-body" style="min-height:140px;">
		 <div class="form-group" id="pa_html">
		 <?php if($collection['pa_body_shape'] != ''){ ?>
		<p><label>Body Shape</label><br />
			<?php echo @$this->Pa_model->get_answer(unserialize($collection['pa_body_shape']),'pa_body_shape'); ?>
		</p>
		<?php } ?>
		<?php if($collection['pa_age'] != ''){ ?>
		<p><label>Age Range</label><br />
			<?php echo @$this->Pa_model->get_answer(unserialize($collection['pa_age']),'pa_age'); ?>
		</p>
		<?php } ?>
		<?php if($collection['pa_budget'] != ''){ ?>
		<p><label>Budget</label><br />
			<?php echo @$this->Pa_model->get_answer(unserialize($collection['pa_budget']),'pa_budget'); ?>
		</p>
		<?php } ?>
		<?php if($collection['pa_pref1'] != ''){ ?>
		<p><label>Style Preference 1</label><br />
			<?php echo @$this->Pa_model->get_answer(unserialize($collection['pa_pref1']),'pa_pref1'); ?>
		</p>
		<?php } ?>
		<?php if($collection['pa_pref2'] != ''){ ?>
		<p><label>Style Preference 2</label><br />
			<?php echo @$this->Pa_model->get_answer(unserialize($collection['pa_pref2']),'pa_pref2'); ?>
		</p>
		<?php } ?>
		<?php if($collection['pa_pref3'] != ''){ ?>
		<p><label>Style Preference 3</label><br />
			<?php echo @$this->Pa_model->get_answer(unserialize($collection['pa_pref3']),'pa_pref3'); ?>
		</p>
		<?php } ?>
			  </div>
		</div>
		</div>
 <!-- /.pa -->
	</div>
		<div class="col-md-6">
      <div class="box box-info">
	   <div class="box-header">
	   <h3 class="box-title">Details</h3>
	   </div>
        <!-- /.box-header -->
        <div class="box-body"> 
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <input class="form-control" name="collection_name" id="collection_name" placeholder="Enter collection name" value="<?php if($_POST['collection_name'] == ''){echo @$collection['collection_name'];}else { echo $_POST['collection_name']; } ?>" type="text" required>
              </div>
            </div>
           
            <div class="col-md-12">
              <div class="form-group">
                <textarea class="form-control" name="short_desc" id="short_desc" placeholder="Enter Collection Cover Description"><?php if($_POST['short_desc'] == ''){echo @$collection['collection_short_desc'];}else { echo $_POST['short_desc']; } ?></textarea>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <textarea class="form-control" name="long_desc" id="long_desc" placeholder="Enter Collection Description"><?php if($_POST['long_desc'] == ''){echo @$collection['collection_long_desc'];}else { echo $_POST['long_desc']; } ?></textarea>
              </div>
            </div>
			 <div class="col-md-12">
              <div class="form-group">
                <!--<input class="form-control" name="collection_tags" id="collection_tags" placeholder="Enter tags" type="text" value="<?php echo @$collection['collection_tags']; ?>">-->
				<select name="collection_tags[]" id="collection_tags[]" multiple class="form-control select2 tags" style="width:100%;">
				<?php
                    if(!empty($tag_data)){
                      foreach($tag_data as $val){
						$selected='';if(in_array($val->id,$tag_ids) || in_array($val->id,$_POST['collection_tags'])){ $selected = 'selected';}
                        if(set_value('product_tags') == $val->id){
                          echo '<option '. $selected .' value="'.$val->id.'">'.$val->name.'</option>';
                        }else{

                          echo '<option '. $selected .' value="'.$val->id.'">'.$val->name.'</option>';
                        }
                      }
                    }
				?>
				</select>
              </div>
            </div>
			<div class="col-md-6">
				<div class="form-group">
				<input type="text" name="collection_date_from"  placeholder="Date From" class="form-control input-sm date_from" id="date_from" value="<?php if($_POST['collection_date_from'] == ''){echo @$collection['collection_date_from'];}else { echo $_POST['collection_date_from']; } ?>" required>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				<input type="text" name="collection_date_to" placeholder="Date To" class="form-control input-sm date_to" id="date_to" value="<?php if($_POST['collection_date_to'] == ''){echo @$collection['collection_date_to'];}else { echo $_POST['collection_date_to']; } ?>" required>
				</div>
			</div>
			<!--div class="col-sm-5">
				<div class="form-group">
				<select name="collection_type" class="form-control">
					<option <?php if($collection['collection_type'] == '1'){echo 'selected';}?> value="1">type1</option>
					<option <?php if($collection['collection_type'] == '2'){echo 'selected';}?> value="2">type2</option>
					<option <?php if($collection['collection_type'] == '3'){echo 'selected';}?> value="3">type3</option>
				</select>
				</div>
			</div-->
			 <div class="col-md-6">
              <div class="form-group">
			    <label>Collection Image</label>
                <input class="form-control1" id="collection_img"  name="collection_img" type="file">
              </div>
            </div>
			<div class="col-md-6">
              <div class="form-group">			   
			   <?php if($collection['collection_img'] != ''){ ?>
					<img src="<?php echo $this->config->item('collection_img_url'); ?><?php echo $collection['collection_img']; ?>" style="height:60px;">
				<?php }else { ?>
					<img src="<?php echo $this->config->item('collection_img_url').'14810245102586.jpg'; ?>" style="height:60px;">
				<?php } ?>
              </div>
            </div>
			
			
			<!--div class="product-img-wrp">
				<?php if($collection['collection_img'] != ''){ ?>
					<img src="<?php echo $this->config->item('collection_img_url'); ?><?php echo $collection['collection_img']; ?>" >
				<?php }else { ?>
					<img src="<?php echo $this->config->item('collection_img_url').'14810245102586.jpg'; ?>" >
				<?php } ?>
			</div-->
			<!--<div class="col-md-5">
              <div class="form-group">
			     <a href="#pa-popup" data-toggle="modal" onclick="get_pa_answers();" class="btn btn-danger btn-sm" id="pa_popup_button" data-attr="edit-collection">PA Setting</a>
              </div>
			  <div class="form-group" id="pa_html">
			  </div>
            </div>-->
          </div>
        </div>		
		
		<div class="box-footer clearfix">  
			<div class="pull-right1"> 		 
				<button class="btn btn-danger">Save</button>             
			</div>
		</div>
			
      </div>
	  </div>
	  
	</form>
  </div>
</section>
<script type="text/Javascript">/*
function change_list(product_id){
	var product_ids = $("#product_ids").val();
	// alert(product_ids);
	if(product_ids.indexOf(","+product_id)>0){
		// alert("1");
		var product_ids = product_ids.replace(","+product_id, "");
	}else if(product_ids.indexOf(product_id+",") == 0){
		// alert("2");
		var product_ids = product_ids.replace(product_id+",", "");
	}else if((product_ids.indexOf(product_id)) == 0){
		// alert("3");
		var product_ids = product_ids.replace(product_id, "");
	}else {
		// alert("4");
	}
	// alert(product_ids);
	document.getElementById("product_ids").value = product_ids;
}*/
</script>