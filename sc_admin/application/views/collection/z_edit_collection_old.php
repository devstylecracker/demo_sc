<?php 
// echo "<pre>";print_r($collection);?>
<section class="content-header">
  <h1>Add Collection</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url();?>">Home</a></li>
    <li><a href="<?php echo base_url();?>/collections-management">Collections Management</a></li>
    <li class="active">Add Collection</li>
  </ol>
</section>
<section class="content">

  <div class="row">
  <form action="<?php echo base_url().'add-collection'; ?>" role="form" id="collection_form" enctype="multipart/form-data" method="POST">
     <div class="col-md-6">
      <div class="box box-info">
	   <div class="box-header">
	   <h3 class="box-title">Products</h3>
	   <div class="pull-right">
		<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#products-popup">Add Products</button>
	   </div>
	   </div>
        <!-- /.box-header -->
        <div class="box-body" style="min-height:240px;">
          <ul id="final_list" class="products-list product-list-inline products-list-grid">
           
          </ul>
        </div>
      </div>
	  </div>
	     <div class="col-md-6">
      <div class="box box-info">
	   <div class="box-header">
	   <h3 class="box-title">Details</h3>
	   </div>
        <!-- /.box-header -->
        <div class="box-body"> 
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <textarea class="form-control" name="short_desc" id="short_desc" placeholder="Enter short description" ><?php echo @$collection['collection_short_desc']; ?></textarea>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <textarea class="form-control" name="long_desc" id="long_desc" placeholder="Enter long description" ><?php echo @$collection['collection_long_desc']; ?></textarea>
              </div>
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <input class="form-control" name="collection_name" id="collection_name" placeholder="Enter collection name" value="<?php echo @$collection['collection_name']; ?>" type="text">
              </div>
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <input class="form-control" name="collection_tags" id="collection_tags" placeholder="Enter tags" value="<?php echo @$collection['collection_tags']; ?>" type="text">
              </div>
            </div>
			 <div class="col-md-5">
              <div class="form-group">
			    <label>Collection Image</label>
                <input class="form-control1" id="collection_img"  name="collection_img" type="file">
              </div>
            </div>           
          </div>
        </div>		
		
		<div class="box-footer clearfix">  
			<div class="pull-right1"> 		 
				<button class="btn btn-danger">Save</button>             
			</div>
		</div>
			
      </div>
	  </div>
	  
	</form>
  </div>
</section>