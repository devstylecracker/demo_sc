<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/redmond/jquery-ui.css" />    
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.lazy.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/collection.js" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet"/>

<div class="container">
<section class="content">
	<div class="look-creater-wrp">
	<div class="row">
		  <div class="col-xs-12 col-md-7 col-lg-7">
			  <div class="canvas-wrp street-canvas-wrp">
					 <div class="canvas-middle">
						 <div class="row">
							 <div class="col-xs-12 col-md-5 col-lg-5">
								 <div class="panel">
								<div class="panel-recom-prod">
							   <div class="title"> Recommended Products </div>
								 <ul id="recommended_products" class="recom-prods">

								 </ul>
						 </div></div>
						 </div>
							<div class="col-xs-12 col-md-7 col-lg-7">
								<div class="panel canvas-outer">
								  <div class="canvas" id="street_canvas">
									<img src="" alt=""  id="street_canvas_img">
									 <input type="hidden" name="street_canvas_img_id" id="street_canvas_img_id" value="">
								  </div>
							</div>
							</div>
		</div>
	
	<div class="panel panel-look-desc-wrp">
		<div class="panel-look-desc">
			<div class="row">
				<div class="col-xs-12 col-md-7 col-lg-7">
					<input type="text" name="collection_name" class="form-control input-sm" id="collection_name" placeholder="Enter collection name" >
				</div>
				<div class="col-xs-12 col-md-7 col-lg-7">
					<input type="text" name="short_desc" id="short_desc" class="form-control input-sm"  placeholder="Enter short description">
				</div>
				<div class="col-xs-12 col-md-7 col-lg-7">
					<input type="text" name="long_desc" id="long_desc" class="form-control input-sm"  placeholder="Enter long description">
				</div>
				<div class="col-md-7 col-lg-7">
					<input name="collection_tags" id="collection_tags" class="form-control input-sm tag-txt" type="text" placeholder=" Add tags for this collection " >
				</div>
			</div>
		</div>
		<div class="panel-footer">
		<input type="submit" name="save_look" class="btn btn-primary btn-sm" id="save_look" value="Save" onclick="save_street_look();">
		<div id="error_text" class="error_text pull-right"></div>
		</div>

	</div>

</div>
</div></div>

		   <div class="col-xs-12 col-md-5 col-lg-5">
			   <div class="canvas-data-wrp">
				    <div class="canvas-data-selection">
							<div class="steps">
						   <ul>
							  <li class="step1 active">Collection Image <span class="caret"></span></li>
							   <li class="step2">Select Products <span class="caret"></span></li>
						   </ul>
						   </div>
				   </div>

				    <div class="canvas-data">
						<div class="step-content canvas-template-wrp street-image-wrp">

							<ul>

							 <?php if(!empty($street_data)) {
									foreach($street_data as $val){
									?>
									<li><img src="<?php echo base_url(); ?>assets/collection_img/<?php echo $val['img_name']; ?>" onclick="put_in_canvas(<?php echo $val['id']; ?>,'<?php echo $val['img_name']; ?>');" ></li>
									<?php }} ?>

							</ul>

							</div>
						<div class="step-content canvas-products-wrp" style="display:block;">
							<div class="panel-step-level2">
						<div class="row">
						
						<div class="col-md-4">
							<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#products-popup">Add Products</button>
						</div>
						</div>

							    <div class="panel search-container">
									<span class="btn btn-primary btn-sm backBtn">Back</span>
									<input type="hidden" name="tag_name" id="tag_name" placeholder="Search by tag" class="form-control input-sm inline" style="width:140px;"></input>
									
								</div>


							   <div class="cat-content-wrp panel">
							   <div  id="cat-1-content" class="products-container cat-content cat-1-content sub-categories">

								<ul id="getVariationVal">

							   </ul>
							   </div>

							    <div  id="cat-2-content" class="cat-content cat-2-content sub-categories">
							   </div>




							   </div>
						<div class="step-content canvas-effects-wrp">

						  <div  id="cat-1-content" class="cat-content cat-1-content sub-categories11 panel-step-level2">

								<div>Effects</div>
									<ul>
										<li>Bg Color
											<div id="colorSelector"><div style="background-color: #0000ff"></div></div>
										</li>
									   <li class="btn btn-sm add-text-btn">Text</li>
									   <li class="btn btn-sm add-big-image">Bg Image</li>

								   </ul>

						</div>

						<div class="cat-content-wrp panel">

							   <div class="product-container cat-content cat-1-content sub-categories bg-container">
									<ul>
										<li><img src="<?php echo base_url(); ?>assets/backgrounds/bg-1.jpg" /></li>
										<li><img src="<?php echo base_url(); ?>assets/backgrounds/bg-2.jpg" /></li>
										<li><img src="<?php echo base_url(); ?>assets/backgrounds/bg-3.jpg" /></li>
										<li><img src="<?php echo base_url(); ?>assets/backgrounds/bg-4.jpg" /></li>
										<li><img src="<?php echo base_url(); ?>assets/backgrounds/bg-5.jpg" /></li>
									</ul>
							   </div>


						</div>




						</div>
					</div>

			  </div>


		  </div>
	</div>
	</div>

 </div>

</section>
</div>
<script type="text/Javascript">

	function put_in_canvas(id,img){
		$('#street_canvas_img_id').val(img);
		$('#street_canvas_img').attr('src', 'assets/collection_img/'+img);
	}
	$('.nav-item-stylist').addClass("active");
</script>
