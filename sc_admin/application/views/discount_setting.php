<section class="content-header">
  <h3 class="box-title">Discounts <small class="label label-info"><?php echo  @$count; ?></small></h3>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">Discounts</li>
  </ol>
</section>

<section class="content">
  <div class="box">
<div class="box-body">
  <?php echo $this->session->flashdata('feedback'); ?>
  <div class="box-body">
        <div class="row">

          <div class="col-md-2">
            <select class="form-control input-sm" id="discount_type" name="discount_type" onchange="show_products(1);">
           <option value="">Select Discount Type</option>

                    <?php foreach($discount_type as $val)
                          { ?>
                      <option value="<?php echo $val['discount_id']; ?>" ><?php echo $val['discount_name']; ?></option>
                  <?php } ?>
              </select>
            </div>
            </div>
            <br>

<div class="table-responsive">
  <table class="table table-bordered table-striped dataTable table-orders ">
    <thead>
      <tr>
       <th>Sr. No.</th>
      <th>Discount Name</th>
        <th>Brand</th>
        <th>Product Name</th>
        <th>Product Id</th>
        <th>Discount percent</th>
        <th>Max amount</th>
        <!--<th>Discount price</th>-->
        <th>Start date</th>
        <th>End date</th>

      </tr>
    </thead>
    <tbody id="discountRecords">
     </tbody>

  </table>
  <input type="hidden" name="min_offset" id="min_offset" value="0">
</div>

  <?php echo $this->pagination->create_links(); ?>
  </div>
  </div>

</section>

<script type="text/Javascript">
$('.nav-item-discount').addClass("active");
$('.nav-discount-view').addClass("active");
$('#load_more').click(function(){
    load_more();

  });
  function show_products(attr){
  //alert("show_products");
  var discount_type = $('#discount_type').val();
  //alert(discount_type);
	
	if($('#min_offset').length > 0){
    var min_offset = $('#min_offset').val(); }
    else{ var min_offset =0 };
    if($('#max_offset').length > 0){
    var max_offset = $('#max_offset').val(); }
    else{ var max_offset =0 };

    if(attr == 1){ min_offset =0; }

   if(discountType !='')
    {  var discountType = $('#discount_type').val();

        if(discountType !=''){
        $.ajax({
            url: "<?php echo base_url() ?>discountSetting/display_records",
            type: 'POST',
            data: {'discountType':discountType,'min_offset':min_offset,'max_offset':max_offset},
            cache :true,
            async: false,
            success: function(response) {
              // $("#discountRecords").html(response);
			if(attr == 1){
                $('#discountRecords').html(response);
              }else{
                $('#discountRecords').append(response);
              }
              $('#min_offset').val(parseInt(min_offset)+1);

            }
        });
      }

    }
  }
  
  function load_more(){
	  
	  if($('#min_offset').length > 0){
		var min_offset = $('#min_offset').val(); }
	  else{ var min_offset =0 };
	  if($('#max_offset').length > 0){
		var max_offset = $('#max_offset').val(); }
	  else{ var max_offset =0 };

	  min_offset_val = parseInt(min_offset)-1;

	  $('#remove'+min_offset_val).remove();
	  show_products(0);
	  
}
  </script>
