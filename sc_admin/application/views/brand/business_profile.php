<?php
$users_menu=$this->data['users_menu'];
?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/sumoselect/sumoselect.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/sumoselect/jquery.sumoselect.min.js"></script>

<section class="content-header">
	<h1>Business Details</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url(); ?>home">Home</a></li>
		<li><a href="<?php echo base_url(); ?>brandusers">Manage Store Users</a></li>
		<li class="active">Business Details</li>
	</ol>
</section>
<section class="content">
 <div class="row">
		   <div class="col-md-12">
			<div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Business Profile</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form name="frmbusiness" id="frmbusiness" role="form" method="post" enctype="multipart/form-data">
                  <div class="box-body">
				  <div class="row">
		   <div class="col-md-6">
				    <div class="form-group">
                      <label for="exampleInputEmail1">Brand Name<span class="text-red">*</span></label>
                      <input type="text" name="company_name" id="company_name" placeholder="Enter Company Name" id="company_name" class="form-control" value="<?php echo isset($user_data[0]->company_name) ? $user_data[0]->company_name : set_value('company_name'); ?>">
                       <span class="text-red"> <?php echo form_error('company_name'); ?></span>
                    </div>
					<?php if($_SESSION['role_id'] != 6) { ?>
					<div class="form-group">
                      <label for="exampleInputEmail1">Short Description<span class="text-red">*</span></label>
					  <textarea name="short_description" id="short_description" placeholder="Enter Short Description" rows="3" class="form-control"><?php echo isset($user_data[0]->short_description) ? $user_data[0]->short_description : set_value('short_description'); ?></textarea>
						<span class="text-red"> <?php echo form_error('short_description'); ?></span>
                    </div>
					<?php }?>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Detailed Description</label>
                      <textarea name="long_description" id="long_description" placeholder="Enter Detailed Description" rows="3" class="form-control"><?php echo isset($user_data[0]->long_description) ? $user_data[0]->long_description :  set_value('long_description'); ?></textarea>
                    </div>
                    <div class="row">


 <div class="col-md-6">
          <div class="form-group">
                      <label for="exampleInputEmail1">Year of Establishment</label>
                      <input type="text" name="year_establish" id="year_establish" placeholder="Enter Year of Establishment" class="form-control" value="<?php echo isset($user_data[0]->year_establish) ? $user_data[0]->year_establish : set_value('year_establish'); ?>">
                      <span class="text-red"> <?php echo form_error('year_establish'); ?></span>
                    </div>  </div>
                     <div class="col-md-6">
          <div class="form-group">
                      <label for="exampleInputEmail1">Registration No.</label>
                      <input type="text" name="registration_no" id="registration_no" placeholder="Enter Registration No." class="form-control" value="<?php echo isset($user_data[0]->registration_no) ? $user_data[0]->registration_no : set_value('registration_no'); ?>">
                    </div>
          </div>  </div>
					<!-- <div class="form-group">
                      <label for="exampleInputEmail1">Company Logo</label>
                    <div>
                       <input type="file" id="company_logo" name="company_logo">
                    </div>
                      <p class="help-block">Max file size: 1MB and dimension(380 X 250).</p>
                      <span class="text-red"> <?php if(isset($error)){ foreach($error as $val){ echo $val.'<br>'; } } ?></span>
                      <?php if(isset($user_data[0]->logo)) { ?>
                      <img src="<?php echo base_url(); ?>assets/brand_logo/<?php echo $user_data[0]->logo; ?>" height="100px" width="100px">
                      <?php } ?>
                    </div> -->
					<?php if(in_array("10",$users_menu)) { ?>
					<!-- contract upload code start -->
					<!-- <div class="form-group">
						<label class="control-label col-sm-4">Upload Contract</label>
						<div class="col-sm-8"><input type="file" name="contract_file" id="contract_file" class="" placeholder="Upload Contract"/>
						<p class="help-block">Only .pdf format allowed </p>
						</div>
					</div>
					
					 <div class="form-group">
						   <label >Verify Brand</label>
							<div class="radio">
							<label class="control-label"> <input type="radio" name="is_verify" id="is_verify" <?php if(isset($user_data[0]->is_verify)){ echo ($user_data[0]->is_verify == '1') ?  "checked" : "" ; }else{echo set_value('is_verify') == 1 ? 'checked' : '';} ?> value = "1" /> Yes</label> &nbsp;&nbsp;&nbsp;
							<label class="control-label"> <input type="radio" name="is_verify" id="is_verify" <?php if(isset($user_data[0]->is_verify)){ echo ($user_data[0]->is_verify == '0') ?  "checked" : "" ; }else{echo set_value('is_verify') == 0 ? 'checked' : '';} ?> value = "0" /> No</label>
							<div class="error text-red"><?php echo form_error('is_verify'); ?></div>
							</div>
                      </div> -->
				<!-- contract upload code End -->
				<?php } ?>
                  </div>
		   <div class="col-md-6">
         <div class="row">
<div class="col-md-6">
					<div class="form-group">
                      <label for="exampleInputEmail1">Website</label>
                      <input type="url" name="website_url" id="website_url" value="<?php echo isset($user_data[0]->url) ? $user_data[0]->url : set_value('website_url'); ?>" placeholder="e.g. http://www.example.com" class="form-control">
                      <span class="text-red"> <?php echo form_error('website_url'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-6">
					<div class="form-group">
                      <label for="exampleInputEmail1">Shop URL</label>
                      <input type="url" name="shop_url" id="shop_url" value="<?php echo isset($user_data[0]->shop_url) ? $user_data[0]->shop_url : set_value('shop_url'); ?>" placeholder="e.g. http://www.example.com" class="form-control">
                      <span class="text-red" > <?php echo form_error('shop_url'); ?></span>
                    </div>
                      </div>
                        <div class="col-md-6">
					<div class="form-group">
                      <label for="exampleInputEmail1">Designer Name</label>
                      <input type="text" name="designer_name" id="designer_name" placeholder="Enter Designer Name" class="form-control" value="<?php echo isset($user_data[0]->designer_name) ? $user_data[0]->designer_name : set_value('designer_name'); ?>">
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                                <label for="exampleInputEmail1">Age Group<span class="text-red">*</span></label>
                               <div class="row">
          						 <div class="col-xs-4">
                               <input class="form-control " type="text" name="age_group_from" id="age_group_from" value="<?php echo isset($user_data[0]->age_group_from) ? $user_data[0]->age_group_from : set_value('age_group_from'); ?>" placeholder="From">
                               <span class="text-red"> <?php echo form_error('age_group_from'); ?></span>
          						</div>
          						<div class="col-xs-4">
          					  <input class="form-control" type="text" name="age_group_to" id="age_group_to" value="<?php echo isset($user_data[0]->age_group_to) ? $user_data[0]->age_group_to : set_value('age_group_to'); ?>" placeholder="To">
          						<span class="text-red"> <?php echo form_error('age_group_to'); ?></span>
          						</div>
          					</div>
                    </div>  </div>
                    <div class="col-md-6">
          					<div class="form-group">
                        <label for="exampleInputEmail1">Price Range<span class="text-red">*</span></label>
          						 <div class="row">
          							 <div class="col-xs-4">
          						 <input class="form-control " type="text" name="price_range_from" id="price_range_from" value="<?php echo isset($user_data[0]->price_range_from) ? $user_data[0]->price_range_from : set_value('price_range_from'); ?>" placeholder="From">
          						 <span class="text-red"> <?php echo form_error('price_range_from'); ?></span>
          							</div>
          							<div class="col-xs-4">
          						  <input class="form-control" type="text" name="price_range_to" id="price_range_to" value="<?php echo isset($user_data[0]->price_range_to) ? $user_data[0]->price_range_to : set_value('price_range_to'); ?>" placeholder="To">
          						  <span class="text-red"> <?php echo form_error('price_range_to'); ?></span>
          							</div>
          						</div>
                    </div>
                    </div>

                    
                  </div>

                    <?php if(!empty($target_market)){ ?>
					<div class="form-group">
                      <label for="exampleInputEmail1">Target Market<span class="text-red">*</span></label>
                       <div class="checkbox">
						   <?php foreach($target_market as $val) {  ?>
                      <label>
						  <?php if(isset($t_market) && in_array($val->id,$t_market)) {  ?>
                        <input type="checkbox" name="target_market[]" id="" <?php echo set_checkbox('target_market[]'); ?> value="<?php echo $val->id;?>" checked="checked"> <?php echo $val->target_market;?>
                        <?php }else { ?>
						<input type="checkbox" name="target_market[]" id="" <?php echo set_checkbox('target_market[]'); ?> value="<?php echo $val->id;?>"> <?php echo $val->target_market;?>
							<?php } ?>
                      </label>
							<?php } ?>
							<span class="text-red"> <?php echo form_error('target_market'); ?></span>
					</div>
					<?php }else{ echo "Contact System Admin"; }  ?>
          </div>


					<div class="form-group">
            <label for="exampleInputEmail1">Demographic<span class="text-red">*</span></label>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="is_male" id="is_male" value="1" <?php echo isset($user_data[0]->is_male) && $user_data[0]->is_male==1 ? 'checked' :''; ?>> Male
              </label>
					    <label>
                <input type="checkbox" name="is_female" id="is_female" value="1" <?php echo isset($user_data[0]->is_female) && $user_data[0]->is_female==1 ? 'checked' :''; ?>> Female
              </label>
					    <label>
                <input type="checkbox" name="is_children" id="is_children" value="1" <?php echo isset($user_data[0]->is_children) && $user_data[0]->is_children==1  ? 'checked' :''; ?>> Children
              </label>
              <span class="text-red"> <?php echo form_error('is_male'); ?></span>
            </div>
          </div>
          <div class="form-group">
              <label for="exampleInputEmail1">Store Type<span class="text-red">*</span></label>
               <div class="checkbox">
              <label>
                <input type="checkbox" name="is_online" id="is_online" value="1" <?php echo isset($user_data[0]->is_online) && $user_data[0]->is_online==1  ? 'checked' :''; ?>> Online
              </label>
              <label>
                <input type="checkbox" name="is_offline" id="is_offline" value="1" <?php echo isset($user_data[0]->is_offline) && $user_data[0]->is_offline==1  ? 'checked' :''; ?>> Offline
              </label>
              <span class="text-red"> <?php echo form_error('is_online'); ?></span>
            </div>
          </div>  

           <div class="col-md-6">
              <div class="form-group">
                <label for="category">Category </label>
                <select class="form-control" tabindex="7"  name="category" id="category">
                  <option value="">Select Category</option>
                  <?php
                  if(!empty($category)){
                    foreach($category as $val){
                      if($val->id == $brand_cat[0]['prod_cat_id'])
                      {
                        echo '<option value="'.$val->id.'" selected>'.$val->name.'</option>';
                      }else
                      {
                        echo '<option value="'.$val->id.'">'.$val->name.'</option>';
                      }
                      
                    }
                  }
                  ?>
                </select>
                <!--span class="text-red"> <?php echo form_error('category'); ?></span-->
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="subCategory">Sub Category </label>
                <select tabindex="9" class="form-control"  name="sub_category" id="sub_category">
                  <option value="">Select Subcategory</option>
                </select>
                <!--span class="text-red"> <?php echo form_error('sub_category'); ?></span-->
              </div>
            </div>

            <div class="col-md-6">
                <div id="variation_type" class="prod-variation-type" ></div>
            </div>

           </div> </div>
						<input type="hidden" name="user_id" id="user_id" value="<?php echo isset($user_data[0]->user_id) ? $user_data[0]->user_id : set_value('user_id'); ?>">
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Save</button>
                      <a href="<?php echo base_url(); ?>brandusers"><button class="btn btn-primary btn-sm btn-rad" type="button"><i class="fa fa-reply"></i> Back</button></a>
                      <button type="reset" class="btn btn-default btn-sm"><i class="fa fa-close"></i> Cancel</button>
                  </div>
                </form>
            </div>

		 </div>
		</div>

</div>
</section>
<script type="text/Javascript">
$( document ).ready(function() {
	$('.nav-item-user').addClass("active");
	$('.nav-brand-user').addClass("active");

  $("select[multiple=''].multi-select-box").SumoSelect({selectAll: true });
    
    $( "#category" ).change(function() {
    var cat_id = $("#category").val();  
    get_sub_cat(cat_id,'');
  });

  

  $( "#sub_category" ).change(function() {
    var cat_id = $("#sub_category").val();
    get_variation_type(cat_id,'');
  });

  /* Get Sub category */
function get_sub_cat(id,sel_id){  
  var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
  $.ajax({
      url: baseUrl+'/brandusers/get_sub_cat/',     
      type: 'post',
      data: { 'type' : 'get_sub_cat','cat_id': id,'sel_id' : sel_id },
      success: function(data, status) {
        if(status == 'success'){
          $('#sub_category').html(data);
        }
      },
      error: function(xhr, desc, err) {
        console.log(err);
      }
  });
}

/* Get Variation Type */
function get_variation_type(id,sel_id){ 
  var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
  $.ajax({
      url: baseUrl+'/brandusers/get_var_value_type',     
      type: 'post',
      data: { 'type' : 'get_var_value_type','sub_cat_id': id ,'sel_id' : sel_id },
      success: function(data, status) {
        if(status == 'success'){
          $('#variation_type').html(data);
        }
      },
      error: function(xhr, desc, err) {
        console.log(err);
      }
  });
}


get_sub_cat('<?php echo @$brand_cat[0]['prod_cat_id']; ?>','<?php echo @$brand_cat[0]['prod_sub_cat_id']; ?>'); 
get_variation_type('<?php echo @$brand_cat[0]['prod_sub_cat_id']; ?>','<?php echo @$brand_cat[0]['var_type_id']; ?>');

});
</script>
