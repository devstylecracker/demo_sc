<section class="content-header">
	<h1>Brand Details</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url(); ?>home">Home</a></li>
<li><a href="<?php echo base_url(); ?>brandusers">Manage Store Users</a></li>
<li><a href="<?php echo base_url().'brandusers/manage_store/'.$store_info[0]['brand_id']; ?>">Manage Brand</a></li>
		<li class="active">Brand Details</li>
	</ol>
</section>
<section class="content page-product-details">
  <div class="row">
    <div class="col-md-6">
      <div class="box product-details">
        <div class="box-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable table-vth" id="datatable">

                    <tr>
                      <th style="width:30%;">Brand Name</th>
                      <td><?php echo $store_info[0]['brand_store_name']; ?></td>
                    </tr>
                    <tr>
                      <th>URL</th>
                      <td><?php echo '<a href="'.$store_info[0]['url'].'" target="_blank">'.$store_info[0]['url'].'</a>'; ?></td>
                    </tr>
                    <tr>
                      <th>Contact No.</th>
                      <td><?php echo $store_info[0]['contact']; ?></td>
                    </tr>
                    <tr>
                      <th>Address</th>
                      <td><?php echo $store_info[0]['address']; ?></td>
                    </tr>
                    <tr>
                      <th>Country </th>
                      <td><?php echo $store_info[0]['country']; ?></td>
                    </tr>
                     <tr>
                      <th>State </th>
                      <td><?php echo $store_info[0]['state']; ?></td>
                    </tr>
                     <tr>
                      <th>Pincode </th>
                      <td><?php echo $store_info[0]['pincode']; ?></td>
                    </tr>
                    <tr>
                      <th>City </th>
                      <td><?php echo $store_info[0]['city']; ?></td>
                    </tr>
                    <tr>
                      <th>Year of Establishment</th>
                      <td><?php echo $store_info[0]['established_at']; ?></td>
                    </tr>
                     <tr>
                      <th>Contact Person</th>
                      <td><?php echo $store_info[0]['contact_person']; ?></td>
                    </tr>
                     <tr>
                      <th>Mobile Number</th>
                      <td><?php echo $store_info[0]['mobile_no']; ?></td>
                    </tr>
                     <tr>
                      <th>Email-Id</th>
                      <td><?php echo $store_info[0]['email_id']; ?></td>
                    </tr>
                     <tr>
                      <th>Price Range</th>
                      <td><?php echo $store_info[0]['price_range_from'] .' to '.$store_info[0]['price_range_to'] ; ?></td>
                    </tr>
                    <tr>
                      <th>Product in stock </th>
                      <td><?php echo $store_info[0]['product_in_stock_count']; ?></td>
                    </tr>
                    <tr>
                      <th>Age </th>
                      <td><?php echo $store_info[0]['age_group_from'] .' to '.$store_info[0]['age_group_to'] ; ?></td>
                    </tr>
                    <tr>
                      <th>Demographic </th>
                      <td><?php echo $store_info[0]['is_male'] == 1 ? ' Male' : ''; echo  $store_info[0]['is_female'] == 1 ? ' Female' : '';  echo $store_info[0]['is_children'] == 1 ? ' Children' : ''; ?></td>
                    </tr>

                    <tr>
                      <th>Brand Type </th>
                      <td><?php echo $store_info[0]['is_online'] == 1 ? ' Online' : ''; echo $store_info[0]['is_offline'] == 1 ? ' Offline' : ''; ?></td>
                    </tr>

                  <tr>
                      <th>Target Market </th>
                      <td><?php if(!empty($target_market)) { foreach($target_market as $val){ echo $val['target_market'].', '; } }else{ echo ' - ';} ?></td>
                    </tr>
                    <tr>
                     <th>Ratings </th>
                     <td><?php echo $store_info[0]['rating']; ?></td>
                   </tr>

                  </table>
                </div>
              </div>

            </div>
					<div>
						<a href="<?php echo base_url()?>brandusers/manage_store/"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i> Back</button></a>
					</div>
          </div>
          <div class="col-md-6">
            <div class="box">
              <div class="box-body">
                  <div class="table-responsive">
                <table class="table table-bordered table-striped dataTable table-vth" id="datatable">
                  <tr>
                    <th style="width:30%;">Brand Image</th>
                    <td>
                      <div class="image-auto-size">
                        <img src="<?php echo base_url().'/assets/store_logo/'.$store_info[0]['logo']; ?>">
                      </div>
                      </td>
                  </tr>

                  </table>
                  </div>
                    </div>
                  </div>


                  </div>

            </div>
</section>

<script type="text/Javascript">
$( document ).ready(function() {
$('.nav-item-user').addClass("active");
$('.nav-brand-user').addClass("active");
});
</script>
