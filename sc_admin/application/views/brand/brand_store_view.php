<?php
$users_menu=$this->data['users_menu'];
?>
<section class="content-header">
	<h1>Manage Brand</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url(); ?>home">Home</a></li>
		<li><a href="<?php echo base_url(); ?>brandusers">Manage Store Users</a></li>
		<li class="active">Manage Brand</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box ">
        <div class="box-header">
			<h3 class="box-title">Brands  <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3>
			<div class="pull-right">
				<?php if(in_array("23",$users_menu)) { ?>
					<a href="<?php echo base_url(); ?>store/store_add/<?php echo $brand_id; ?>"><button class="btn btn-sm btn-primary" type="button"><i class="fa fa-plus"></i> Add</button></a>
					<?php } ?>
				</div>
			</div>
			<div class="box-body">
				<form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>brandusers/manage_store/<?php echo $brand_id; ?>/" method="post">
					<div class="row">
						<div class="col-md-2">
							<select name="search_by" id="search_by" class="form-control input-sm" >

								<option value="0">Search by</option>
								<option selected="" value="1" <?php echo set_value('search_by')==1 ? 'selected' : ''; ?>>Brand Name</option>

							</select>
						</div>
						<div class="col-md-3">
							<div class="input-group">
								<input type="text" name="table_search" id="table_search" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo set_value('table_search'); ?>" required=""/>
								<div class="input-group-btn">
									<button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<a href="<?php echo base_url(); ?>brandusers/manage_store/<?php echo $brand_id; ?>/" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
						</div>
						<div class="col-md-5">
							<!--div class="records-per-page pull-right">
								<select name="records-per-page" class="form-control input-sm">
									<option>Per Page</option>
									<option value="10" selected="selected">10</option>
									<option value="25">25</option>
									<option value="50">50</option>
								</select>
							</div-->
							<div class="pull-right">
								<?php echo $this->pagination->create_links(); ?>
							</div>
						</div>
					</div>
				</form>

				<div class="table-responsive">
					<table class="table table-bordered table-striped dataTable" id="datatable">
						<thead>
							<tr>
								<th>Brand Name</th>
								<th>Address</th>
								<th>contact</th>
								<th>Online</th>
								<th>Offline</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php if(!empty($get_all_store)) { ?>
								<?php foreach($get_all_store as $val){ ?>
									<tr>
										<td><?php echo $val['brand_store_name']; ?></td>
										<td><?php echo $val['address']; ?></td>
										<td><?php echo $val['contact']; ?></td>
										<td><?php echo $val['is_online']==1 ? 'yes' : '-'  ; ?></td>
										<td><?php echo $val['is_offline']==1 ? 'yes' : '-'  ; ?></td>
										<td>
											<?php if(in_array("22",$users_menu)) { ?>
												<a href="<?php echo base_url(); ?>store/store_view/<?php echo $brand_id; ?>/<?php echo $val['id']; ?>">
													<button type="button" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> View</button></a>
													<?php } ?>

											<?php if(in_array("37",$users_menu)) { ?>
												<a href="<?php echo base_url(); ?>store/store_edit/<?php echo $brand_id; ?>/<?php echo $val['id']; ?>">
													<button type="button" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</button></a>
													<?php } ?>

													<?php if(in_array("38",$users_menu)) { ?>
														<a onclick="javascript:void(0)">
															<button type="button" class="btn btn-xs btn-primary btn-delete" data-id='<?php echo $val['id']; ?>' data-toggle="confirmation"><i class="fa fa-trash-o"></i> Delete</button>
														</a>
													 <?php } ?>
																</td>
															</tr>
															<?php } ?>
															<?php } ?>
														</tbody>
													</table>
												</div>
												<?php echo $this->pagination->create_links(); ?>
											</div>
										</div>

									</section>

									<script type="text/Javascript">
									$( document ).ready(function() {
										$('[data-toggle=confirmation]').confirmation({
											title:'Are you sure?',
											onConfirm : function(){
												var del_id = $(this).closest('td').find('.btn-delete').attr('data-id');
												$.ajax({
													type: "POST",
													url: "<?php echo base_url(); ?>" + "store/store_delete",
													data: { id: del_id },
													cache:false,
													success:
													function(data){
														location.reload(); //as a debugging message.
													}

												});
											}
										});
										$('.nav-item-user').addClass("active");
										$('.nav-brand-user').addClass("active");
									});
									</script>
