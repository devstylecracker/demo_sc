<section class="content-header">
  <h1>Brands - Images Upload </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">Brands - Images Upload </li>
  </ol>
</section>
<section class="content">
<?php echo $show_msg; ?>
  <div class="box box-warning">
      <div class="box-header with-border">
        <h3 class="box-title">Upload</h3>   
      </div>
      <div class="box-body">
        
      <div class="row">
            <div class="col-md-5">
              <strong>Brand</strong>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <select name="paid_brands" id="paid_brands">
                  <?php if(!empty($paid_brands)) { ?>
                    <?php foreach($paid_brands as $val) { ?>
                    
                    <?php if($brand_id == $val['user_id']){ ?>
                    <option value="<?php echo $val['user_id']; ?>" selected><?php echo $val['company_name']; ?></option>
                    <?php }else{ ?>
                    <?php if($this->session->userdata('role_id') != 6 ){ ?>
                    <option value="<?php echo $val['user_id']; ?>"><?php echo $val['company_name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                    <?php } ?>
                  <?php } ?>
                </select>
              </div>
            </div>
        </div>

      <form name="" id="" action="" method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-5">
              <strong>Brands Logo</strong>
              <p class="help-block">Max file size: 1MB and dimension(530 X 530).</p>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <input type="file" name="brands_logo" id="brands_logo">
                <span class="text-red"> <?php echo form_error('brands_logo'); ?></span>
                <?php
                  if(@$images[0]['logo']!=''){ ?>
                  <img src="<?php echo base_url(); ?>assets/brand_logo/<?php echo $images[0]['logo']; ?>" style="width:20%">
                <?php  }
                ?>
              </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
              <strong>Brands Cover Photo</strong>
              <p class="help-block">Max file size: 100kb and dimension(1366 X 370).</p>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <input type="file" name="cover_img_" id="cover_img_">
                <span class="text-red"> <?php echo form_error('cover_img_'); ?></span>
                <?php 
                  if(@$images[0]['cover_image']!=''){ ?>
                  <img src="../../../assets/images/brands/<?php echo $images[0]['cover_image']; ?>" style="width:20%">
                <?php  }
                ?>
              </div>
            </div>
        </div>
		
		<div class="row">
            <div class="col-md-5">
              <strong>Brands Mobile Cover Photo</strong>
              <p class="help-block">Max file size: 50kb and dimension(375 X 200).</p>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <input type="file" name="cover_img_mobile" id="cover_img_mobile">
                <span class="text-red"> <?php echo form_error('cover_img_mobile'); ?></span>
                <?php 
                  if(@$images[0]['cover_image']!=''){ ?>
                  <img src="../../../assets/images/brands/mobile_cover_img/<?php echo $images[0]['cover_image']; ?>" style="width:20%">
                <?php  }
                ?>
              </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
              <strong>Brands Home Page Promotions</strong>
			  <p class="help-block">Max file size: 50kb and dimension(455 X 300).</p>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <input type="file" name="home_page_promotion" id="home_page_promotion">
                <span class="text-red"> <?php echo form_error('home_page_promotion'); ?></span>
                <?php 
                  if(@$images[0]['home_page_promotion_img']!=''){ ?>
                  <img src="../../../assets/images/brands/homepage/<?php echo $images[0]['home_page_promotion_img']; ?>" style="width:20%">
                <?php  }
                ?>
              </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
              <strong>Brands Listing Page</strong>
			  <p class="help-block">Max file size: 1MB and dimension(530 X 530).</p>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <input type="file" name="brand_list_img" id="brand_list_img">
                <span class="text-red"> <?php echo form_error('brand_list_img'); ?></span>
                 <?php 
                  if(@$images[0]['listing_page_img']!=''){ ?>
                  <img src="../../../assets/images/brands/list/<?php echo $images[0]['listing_page_img']; ?>" style="width:20%">
                <?php  }
                ?>
              </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
              <strong><u>Upload Size Guide</u></strong>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
              <strong>Top</strong>
              <p class="help-block">Only .jpg image will be accepted </p>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <input type="file" name="top_img" id="top_img">
                <span class="text-red"> <?php echo form_error('top_img'); ?></span>
                <?php
                 if($images[0]['user_id'] > 0){
                            $images_array = scandir('../assets/images/size_guide/brands/'.$images[0]['user_id'],true);

                            if(!empty($images_array)){
                              echo '<img src="'.'../../../assets/images/size_guide/brands/'.$images[0]['user_id'].'/top.jpg" style="width:50%;">';
                            }
                          }
                ?>
              </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
              <strong>Bottom</strong>
              <p class="help-block">Only .jpg image will be accepted </p>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <input type="file" name="bottom_img" id="bottom_img">
                <span class="text-red"> <?php echo form_error('bottom_img'); ?></span>
                  <?php
                 if($images[0]['user_id'] > 0){
                            $images_array = scandir('../assets/images/size_guide/brands/'.$images[0]['user_id'],true);

                            if(!empty($images_array)){
                              echo '<img src="'.'../../../assets/images/size_guide/brands/'.$images[0]['user_id'].'/bottom.jpg" style="width:50%;">';
                            }
                          }
                ?>
              </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
              <strong>Footwear</strong>
              <p class="help-block">Only .jpg image will be accepted </p>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <input type="file" name="footwear_img" id="footwear_img">
                <span class="text-red"> <?php echo form_error('footwear_img'); ?></span>
                 <?php
                 if($images[0]['user_id'] > 0){
                            $images_array = scandir('../assets/images/size_guide/brands/'.$images[0]['user_id'],true);

                            if(!empty($images_array)){
                              echo '<img src="'.'../../../assets/images/size_guide/brands/'.$images[0]['user_id'].'/footwear.jpg" style="width:50%;">';
                            }
                          }
                ?>
              </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
              <strong>Top & Bottom</strong>
              <p class="help-block">Only .jpg image will be accepted </p>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <input type="file" name="top_bottom_img" id="top_bottom_img">
                <span class="text-red"> <?php echo form_error('top_bottom_img'); ?></span>
                  <?php
                 if($images[0]['user_id'] > 0){
                            $images_array = scandir('../assets/images/size_guide/brands/'.$images[0]['user_id'],true);

                            if(!empty($images_array)){
                              echo '<img src="'.'../../../assets/images/size_guide/brands/'.$images[0]['user_id'].'/top_bottom.jpg" style="width:50%;">';
                            }
                          }
                ?>
              </div>
            </div>
        </div>

		<!-- added for men size guide -->
        <div class="row">
            <div class="col-md-5">
              <strong>Top Men</strong>
              <p class="help-block">Only .jpg image will be accepted </p>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <input type="file" name="top_img_men" id="top_img_men">
                <span class="text-red"> <?php echo form_error('top_img_men'); ?></span>
                <?php
                 if($images[0]['user_id'] > 0){
                            $images_array = scandir('../assets/images/size_guide/brands/'.$images[0]['user_id'],true);

                            if(!empty($images_array)){
                              echo '<img src="'.'../../../assets/images/size_guide/brands/'.$images[0]['user_id'].'/top_men.jpg" style="width:50%;">';
                            }
                          }
                ?>
              </div>
            </div>
        </div>
		
		  <div class="row">
            <div class="col-md-5">
              <strong>Bottom Men</strong>
              <p class="help-block">Only .jpg image will be accepted </p>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <input type="file" name="bottom_img_men" id="bottom_img_men">
                <span class="text-red"> <?php echo form_error('bottom_img'); ?></span>
                  <?php
                 if($images[0]['user_id'] > 0){
                            $images_array = scandir('../assets/images/size_guide/brands/'.$images[0]['user_id'],true);

                            if(!empty($images_array)){
                              echo '<img src="'.'../../../assets/images/size_guide/brands/'.$images[0]['user_id'].'/bottom_men.jpg" style="width:50%;">';
                            }
                          }
                ?>
              </div>
            </div>
        </div>

		<div class="row">
            <div class="col-md-5">
              <strong>Footwear Men</strong>
              <p class="help-block">Only .jpg image will be accepted </p>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <input type="file" name="footwear_img_men" id="footwear_img_men">
                <span class="text-red"> <?php echo form_error('footwear_img'); ?></span>
                 <?php
                 if($images[0]['user_id'] > 0){
                            $images_array = scandir('../assets/images/size_guide/brands/'.$images[0]['user_id'],true);

                            if(!empty($images_array)){
                              echo '<img src="'.'../../../assets/images/size_guide/brands/'.$images[0]['user_id'].'/footwear_men.jpg" style="width:50%;">';
                            }
                          }
                ?>
              </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
              <strong>Top & Bottom Men</strong>
              <p class="help-block">Only .jpg image will be accepted </p>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <input type="file" name="top_bottom_img_men" id="top_bottom_img_men">
                <span class="text-red"> <?php echo form_error('top_bottom_img'); ?></span>
                  <?php
                 if($images[0]['user_id'] > 0){
                            $images_array = scandir('../assets/images/size_guide/brands/'.$images[0]['user_id'],true);

                            if(!empty($images_array)){
                              echo '<img src="'.'../../../assets/images/size_guide/brands/'.$images[0]['user_id'].'/top_bottom_men.jpg" style="width:50%;">';
                            }
                          }
                ?>
              </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
              <button name="submit" id="submit" class="btn btn-primary btn-xs">Upload</button>
            </div>
        </div>

      </form>

      </div>
  </div>
</section>
<script type="text/javascript">
  $( document ).ready(function() {
    $('#paid_brands').change(function() { 
      if($(this).val()!=''){
       window.location = '<?php echo base_url(); ?>brand_image/upload_images/'+$(this).val();
      }
    });
  });
</script>