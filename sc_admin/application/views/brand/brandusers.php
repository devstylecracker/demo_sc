<?php
$users_menu=$this->data['users_menu'];
?>
<style type="text/css">
  .btn-suc{
    background: transparent;
    color: #00b19c;
    font-weight: 700;
    text-decoration: underline;
  }
  .btn-edit{
    background: transparent;
    color: #a52a2a;
    font-weight: 700;
    text-decoration: underline;
  }
  
  .btn-del{
    background: transparent;
    color: red;
    font-weight: 700;
    text-decoration: underline;
  }
</style>
<section class="content-header">
  <h1>Manage Store Users</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">Store Users</li>
  </ol>
</section>
<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">List of Store Users <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3>
      <div class="pull-right">
    </div>
      </div><!-- /.box-header -->

  <div class="box-body">
    <?php echo $this->session->flashdata('feedback'); ?>
    <form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>brandusers" method="post">
      <div class="row">
        <div class="col-md-2">
          <select name="search_by" id="search_by" class="form-control input-sm" >
            <option value="0">Search by</option>
            <option selected value="1" <?php echo $search_by==1 ? 'selected' : ''; ?>>Username</option>
            <option value="2" <?php echo $search_by==2 ? 'selected' : ''; ?>>Email</option>
            <option value="4" <?php echo $search_by==4 ? 'selected' : ''; ?>>Brand ID</option>
            <option value="8" <?php echo $search_by==8 ? 'selected' : ''; ?> >Date Range</option>
          </select>
        </div>
        <div class="col-md-3">
          <div class="input-group">
            <div id ="filter" style="display: block;" >
              <input type="text" name="table_search" id="table_search" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo set_value('table_search'); ?>" required=""/>
             </div>
            <div id ="daterange" style="display: none;">
              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    <input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm" id="date_from" value="<?php echo set_value('date_from'); ?>">
                  </div>
              </div>
              <div class="col-md-7">
                <div class="form-group">
                  <input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="<?php echo set_value('date_to'); ?>">
                </div>
               </div>
              </div>
            </div>
            <div class="input-group-btn">
              <button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
        <div class="col-md-2"><!-- <label>Filter by : </label> -->
         <select name="filter_by" id="filter_by" class="form-control input-sm" onchange = "submitForm();" >
              <option value="0" >All</option>
              <option value="5" <?php echo $filterby==5 ? 'selected' : ''; ?> >SC Box Stores</option>
              <option value="6" <?php echo $filterby==6 ? 'selected' : ''; ?> >Website Stores</option>
          </select>
        </div>
        <div class="col-md-1">
          <a href="<?php echo base_url(); ?>Brandusers" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
        </div>
        <div class="col-md-4">
          <!--div class="records-per-page pull-right">
            <select name="records-per-page" class="form-control input-sm">
              <option>Per Page</option>
              <option value="10" selected="selected">10</option>
              <option value="25">25</option>
              <option value="50">50</option>
            </select>
          </div-->
          <div class="pull-right">
            <?php echo $this->pagination->create_links(); ?>
          </div>
        </div>
      </div>
    </form>
    <div class="table-responsive">
      <table class="table table-bordered table-striped dataTable" id="datatable">
        <thead>
          <tr>
            <th>ID</th>
            <th>Username</th>
            <th>Company name</th>
            <th>Email</th>
            <th>Created Date</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          <?php if(!empty($users_data)) { $i = 1;
            foreach($users_data as $val){
              ?>
              <tr>
                <td><?php echo $val->id; ?></td>
                <td><?php echo $val->user_name; ?></td>
                <td><?php echo $val->company_name; ?></td>
                <td><?php echo $val->email; ?></td>
                <td><?php //echo $val->created_datetime;
                    $date = new DateTime($val->created_datetime);
                    echo $date->format('d-m-Y H:i:s');
                 ?></td>
                <td>
                  <div class="row">
                    <div class="col-md-3">
                  <?php if(in_array("18",$users_menu)) { ?>
                    <a href="<?php echo base_url(); ?>brandusers/brand_view/<?php echo $val->id; ?>">
                      <button type="button" class="btn btn-xs btn-suc"><i class="fa fa-eye"></i> View</button></a>
                      <?php } ?>


                      <?php if(in_array("22",$users_menu)) { ?>
                        <a href="<?php echo base_url(); ?>users/user_edit/<?php echo $val->id; ?>">
                        <button type="button" class="btn btn-xs btn-edit"><i class="fa fa-edit"></i> Edit </button></a>
                      <?php } ?>

                            <?php if(in_array("10",$users_menu)) { ?>
                              <a href="<?php echo base_url(); ?>users/assign_permission/<?php echo $val->id; ?>">
                                <button type="button" class="btn btn-xs btn-suc"><i class="fa fa-key"></i>
                                  Permissions</button></a>
                                  <?php } ?>

                                  <?php if(in_array("21",$users_menu)) { ?>
                                            <a onclick="javascript:void(0)">
                                              <button type="button" class="btn btn-xs btn-del" data-id='<?php echo $val->id; ?>' data-toggle="confirmation"><i class="fa fa-trash-o"></i> Delete</button></a>
                                              <?php } ?>
                                  </div>
                                        <div class="col-md-4">

                                           <?php if(in_array("22",$users_menu)) { ?>
                        <a href="<?php echo base_url(); ?>brandusers/manage_store/<?php echo $val->id; ?>">
                          <button type="button" class="btn btn-xs btn-suc"><i class="fa fa-th-large"></i>
                            Manage Brands</button></a>
                            <?php } ?>
                             <?php if(in_array("22",$users_menu)) { ?>
                                        <a href="<?php echo base_url(); ?>brand_image/upload_images/<?php echo $val->id; ?>">
                                          <button type="button" class="btn btn-xs btn-edit"><i class="fa fa-edit"></i> Upload Image </button></a>
                                          <?php } ?>

                                  <?php if(in_array("19",$users_menu)) { ?>
                                    <a href="<?php echo base_url(); ?>brandusers/brand_business_edit/<?php echo $val->id; ?>">
                                      <button type="button" class="btn btn-xs btn-edit"><i class="fa fa-edit"></i> Edit Business Details</button></a>
                                      <?php } ?>

                                      <?php if(in_array("20",$users_menu)) { ?>
                                        <a href="<?php echo base_url(); ?>brandusers/brand_contact_edit/<?php echo $val->id; ?>">
                                          <button type="button" class="btn btn-xs btn-edit"><i class="fa fa-edit"></i> Edit Contact Details</button></a>
                                          <?php } ?>
                                        


                                          
                                          </div>
                                        <div class="col-md-3">
                                            <?php if(in_array("60",$users_menu)) { ?>
                                        <a href="<?php echo base_url(); ?>brandusers/Store_Settings/<?php echo $val->id; ?>">
                                          <button type="button" class="btn btn-xs btn-edit"><i class="fa fa-edit"></i> Store Settings</button></a>
                                          <?php } ?>
										  
										<?php if(in_array("10",$users_menu)) { ?>
                                        <a href="<?php echo base_url(); ?>brandusers/new_store_settings/<?php echo $val->id; ?>">
                                          <button type="button" class="btn btn-xs btn-edit"><i class="fa fa-edit"></i> SC Store Settings</button></a>
                                          <?php } ?>     
										  
                                          
                                            </div>
                                            </div>


                                            </td>
                                          </tr>
                                          <?php $i++; } }else{ ?>
                                            <tr>
                                              <td colspan="5">Sorry no record found</td>
                                            </tr>
                                            <?php } ?>
                                          </tbody>
                                        </table>
                                      </div>

                                      <?php echo $this->pagination->create_links(); ?>
                                    </div>
                                  </div>

                                </section>
                                <!-- /.content -->

  <script type="text/Javascript">

  $(function () {
      $("#date_from").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true});
      $("#date_to").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true });   
    });


  function submitForm(){
    $("#is_download").val('0');
    var newhref = '<?php echo base_url(); ?>'+'Brandusers/';
    $('#frmsearch').attr('action',newhref);
    $('#frmsearch').submit();
  }

  $( document ).ready(function() {
    $('[data-toggle=confirmation]').confirmation({
      title:'Are you sure?',
      onConfirm : function(){
        var del_id = $(this).closest('td').find('.').attr('data-id');                                     
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "brandusers/brand_user_delete",
          data: { id: del_id },
          cache:false,
          success:
          function(data){
            location.reload(); //as a debugging message.
          }

        });
      }
    });
    $('.nav-item-user').addClass("active");
    $('.nav-brand-user').addClass("active");
  });

  var filterType = $('#search_by').val();

if(filterType == 8)
{
    $("#filter").hide();
    $("#daterange").show();
    $("#table_search").removeAttr('required');
    $("#date_from").attr('required','');
}

 $("#search_by").change(function(){

    var filterType = $(this).val();
    if(filterType == 8)
    {
        $("#filter").hide();
        $("#daterange").show();
        $("#table_search").removeAttr('required');
        $("#date_from").attr('required','');

    }else
    {
       $("#filter").show();
      $("#daterange").hide();


    }
    $("#table_search").val(0);
     

  });
  </script>
