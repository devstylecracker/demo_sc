<!--sumoselect-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/sumoselect/sumoselect.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/sumoselect/jquery.sumoselect.min.js"></script>
<script type="text/javascript">
$( document ).ready(function() {
  $("#brand").SumoSelect({selectAll: true });
});
</script>
<section class="content-header">
  <h1>Brand Details</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>brandusers">Manage Store Users</a></li>
    <li><a href="<?php echo base_url().'brandusers/manage_store/'.$store_data[0]['brand_id']; ?>">Manage Brand</a></li>
    <li class="active">Edit Brand</li>
  </ol>
</section>
<section class="content store">
  <div class="box ">
    <div class="box-header">
      <h3 class="box-title">Edit Brand</h3>
    </div><!-- /.box-header -->
    <!-- form start -->
    <form name="frmbusiness" id="frmbusiness" role="form" method="post" enctype="multipart/form-data">
      <div class="box-body">
        <div class="row">
          <div class="col-md-6">

            <div class="form-group">
              <label>Brand Name<span class="text-red">*</span></label>
              <input type="text" name="brand_store_name" id="brand_store_name" value="<?php if(isset($store_data[0]['brand_store_name']) && $store_data[0]['brand_store_name']) { echo $store_data[0]['brand_store_name']; } else { echo set_value('brand_store_name'); } ?>" placeholder="Enter Store Name" class="form-control">
              <span class="text-red"> <?php echo form_error('brand_store_name'); ?></span>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>URL</label>
                  <input type="url" name="url" id="url" value="<?php if(isset($store_data[0]['url']) && $store_data[0]['url']){ echo $store_data[0]['url']; }else{ echo set_value('url'); } ?>" placeholder="e.g. http://www.example.com" class="form-control">
                    <span class="text-red"> <?php echo form_error('url'); ?></span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Phone</label>
                    <input type="text" name="contact" id="contact" placeholder="Enter Contact Details" class="form-control" value="<?php if(isset($store_data[0]['contact']) && $store_data[0]['contact']){ echo $store_data[0]['contact']; }else{ echo set_value('contact'); } ?>">
                  </div>
                </div></div>
                <div class="form-group">
                  <label>Address<span class="text-red">*</span></label>
                  <textarea name="address" id="address" placeholder="Enter Address" rows="3" class="form-control"><?php if(isset($store_data[0]['address']) && $store_data[0]['address']){ echo $store_data[0]['address']; }else{ echo set_value('address'); } ?></textarea>
                  <span class="text-red"> <?php echo form_error('address'); ?></span>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Country<span class="text-red">*</span></label>
                      <input type="text" name="country" id="country" placeholder="Enter Country" class="form-control" value="<?php if(isset($store_data[0]['country']) && $store_data[0]['country']){ echo $store_data[0]['country']; }else{ echo set_value('country'); } ?>">
                      <span class="text-red"> <?php echo form_error('country'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-6">

                    <div class="form-group">
                      <label>State<span class="text-red">*</span></label>
                      <input type="text" name="state" id="state" placeholder="Enter State" class="form-control" value="<?php if(isset($store_data[0]['state']) && $store_data[0]['state']){ echo $store_data[0]['state']; }else{ echo set_value('state'); } ?>">
                      <span class="text-red"> <?php echo form_error('state'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-6">

                    <div class="form-group">
                      <label>City<span class="text-red">*</span></label>
                      <input type="text" name="city" id="city" placeholder="Enter City" class="form-control" value="<?php if(isset($store_data[0]['city']) && $store_data[0]['city']){ echo $store_data[0]['city']; }else{ echo set_value('city'); } ?>">
                      <span class="text-red"> <?php echo form_error('city'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Pincode<span class="text-red">*</span></label>
                      <input type="text" name="pincode" id="pincode" placeholder="Enter Pincode" class="form-control" value="<?php if(isset($store_data[0]['pincode']) && $store_data[0]['pincode']){ echo $store_data[0]['pincode']; }else{ echo set_value('pincode'); } ?>">
                      <span class="text-red"> <?php echo form_error('pincode'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Store<span class="text-red">*</span></label>
                      <select id="brand" name="brand[]" multiple="multiple" class="form-control">
                        <?php if(!empty($all_storewise_brands)) { $storewise_brands = array();
                          foreach($all_storewise_brands as $val){
                            $storewise_brands[] = $val['brand_id'];
                          }
                        } ?>
                        <?php if(!empty($all_brands)) {
                          foreach($all_brands as $val){ ?>
                            <?php if(isset($storewise_brands) && in_array($val['id'],$storewise_brands)) {  ?>
                              <option value="<?php echo $val['id']; ?>" selected><?php echo $val['user_name']; ?></option>
                              <?php }else{ ?>
                                <option value="<?php echo $val['id']; ?>"><?php echo $val['user_name']; ?></option>
                                <?php } ?>
                                <?php }
                              } ?>
                            </select>
                            <span class="text-red" > <?php echo form_error('brand'); ?></span>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Year of Establishment</label>
                            <input type="text" name="established_at" id="established_at" placeholder="Enter Year of Establishment" class="form-control" value="<?php if(isset($store_data[0]['established_at']) && $store_data[0]['established_at']){ echo $store_data[0]['established_at']; }else{ echo set_value('established_at'); } ?>">
                          </div>
                        </div>
                        <div class="col-md-12">


                          <div class="form-group">
                            <div>
                              <label>Logo</label>
                            </div>

                            <?php if(isset($store_data[0]['logo']) && $store_data[0]['logo']!='') { ?>
                              <div class="store-logo"><img src="<?php echo base_url().'/assets/store_logo/'.$store_data[0]['logo']; ?>"></div>
                              <?php } ?>
                              <input type="file" id="logo" name="logo" class="file">
                              <p class="help-block">Max file size: 1MB and dimension(500 X 500).</p>
                              <span class="text-red"> <?php if(isset($error)){ foreach($error as $val){ echo $val.'<br>'; } } ?></span>

                            </div>
                          </div>

                      </div>

                    </div>
                    <div class="col-md-6">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Contact Person<span class="text-red">*</span></label>
                            <input type="text" required placeholder="Enter Contact Person" id="contact_person" name="contact_person" required class="form-control" value="<?php if(isset($store_data[0]['contact_person']) && $store_data[0]['contact_person']){ echo $store_data[0]['contact_person']; }else{ echo set_value('contact_person'); } ?>">
                            <span class="text-red" > <?php echo form_error('contact_person'); ?></span>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Designation / Job Title</label>
                            <input type="text" placeholder="Enter Designation" id="job_title" name="job_title" class="form-control" value="">
                            <span class="text-red" > <?php echo form_error('job_title'); ?></span>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Mobile<span class="text-red">*</span></label>
                            <input type="text" placeholder="Enter Mobile" id="mobile_no" name="mobile_no" required class="form-control" value="<?php if(isset($store_data[0]['mobile_no']) && $store_data[0]['mobile_no']){ echo $store_data[0]['mobile_no']; }else{ echo set_value('mobile_no'); } ?>">
                            <span class="text-red" ><?php echo form_error('mobile_no'); ?></span>                            
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Email<span class="text-red">*</span></label>
                            <input type="text" placeholder="Enter Email" id="email" name="email" required class="form-control" value="<?php if(isset($store_data[0]['email_id']) && $store_data[0]['email_id']){ echo $store_data[0]['email_id']; }else{ echo set_value('email_id'); } ?>">
                            <span class="text-red" ><?php echo form_error('email'); ?></span>
                          </div>
                        </div>
                        <div class="col-md-6">

                          <div class="form-group">
                            <label>Product in stock count<span class="text-red">*</span></label>
                            <input type="text" name="product_in_stock_count" id="product_in_stock_count" placeholder="Enter Stock Count" class="form-control" value="<?php if(isset($store_data[0]['product_in_stock_count']) && $store_data[0]['product_in_stock_count']){ echo $store_data[0]['product_in_stock_count']; }else{ echo set_value('product_in_stock_count'); } ?>">
                            <span class="text-red"> <?php echo form_error('product_in_stock_count'); ?></span>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Price Range<span class="text-red">*</span></label>
                            <div class="row">
                              <div class="col-xs-4">
                                <input class="form-control " type="text" name="price_range_from" id="price_range_from" value="<?php if(isset($store_data[0]['price_range_from']) && $store_data[0]['price_range_from']){ echo $store_data[0]['price_range_from']; }else{ echo set_value('price_range_from'); } ?>" placeholder="From">
                                <span class="text-red"> <?php echo form_error('price_range_from'); ?></span>
                              </div>
                              <div class="col-xs-4">
                                <input class="form-control" type="text" name="price_range_to" id="price_range_to" value="<?php if(isset($store_data[0]['price_range_to']) && $store_data[0]['price_range_to']){ echo $store_data[0]['price_range_to']; }else{ echo set_value('price_range_to'); } ?>" placeholder="To">
                                <span class="text-red"> <?php echo form_error('price_range_to'); ?></span>
                              </div>
                            </div>

                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Ratings</label>
                            <input type="number" name="rating" id="rating"  min="1" max="5" placeholder="Enter Ratings" class="form-control" value="<?php if(isset($store_data[0]['rating']) && $store_data[0]['rating']){ echo $store_data[0]['rating']; }else{ echo set_value('rating'); } ?>">
                            <span class="text-red"> <?php echo form_error('rating'); ?></span>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Age Group<span class="text-red">*</span></label>
                            <div class="row">
                              <div class="col-xs-4">
                                <input class="form-control " type="text" name="age_group_from" id="age_group_from" value="<?php if(isset($store_data[0]['age_group_from']) && $store_data[0]['age_group_from']){ echo $store_data[0]['age_group_from']; }else{ echo set_value('age_group_from'); } ?>" placeholder="From">
                                <span class="text-red"> <?php echo form_error('age_group_from'); ?></span>
                              </div>
                              <div class="col-xs-4">
                                <input class="form-control" type="text" name="age_group_to" id="age_group_to" value="<?php if(isset($store_data[0]['age_group_to']) && $store_data[0]['age_group_to']){ echo $store_data[0]['age_group_to']; }else{ echo set_value('age_group_to'); } ?>" placeholder="To">
                                <span class="text-red"> <?php echo form_error('age_group_to'); ?></span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12">

                          <?php if(!empty($target_market)){ ?>
                            <div class="form-group">
                              <label>Target Market<span class="text-red">*</span></label>
                              <div class="checkbox">
                                <?php

                                foreach($brand_target_market as $val){
                                  $brand_target_market_new[] = $val['target_market_id'];

                                }

                                foreach($target_market as $val) {  ?>
                                  <label>
                                    <?php if(isset($brand_target_market_new) && in_array($val->id,$brand_target_market_new)) {  ?>
                                      <input type="checkbox" name="target_market[]" id="" <?php echo set_checkbox('target_market[]'); ?> value="<?php echo $val->id;?>" checked="checked"> <?php echo $val->target_market;?>
                                      <?php }else { ?>
                                        <input type="checkbox" name="target_market[]" id="" <?php echo set_checkbox('target_market[]'); ?> value="<?php echo $val->id;?>"> <?php echo $val->target_market;?>
                                        <?php } ?>
                                      </label>
                                      <?php } ?>
                                      <span class="text-red"> <?php echo form_error('target_market'); ?></span>
                                    </div>
                                    <?php }else{ echo "Contact System Admin"; }  ?>

                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label>Demographic<span class="text-red">*</span></label>
                                    <div class="checkbox">
                                      <label>
                                        <input type="checkbox" name="is_male" id="is_male" value="1" <?php if(isset($store_data[0]['is_male']) && $store_data[0]['is_male']){ echo 'checked'; }else{ echo ''; } ?>> Male
                                      </label>
                                      <label>
                                        <input type="checkbox" name="is_female" id="is_female" value="1" <?php if(isset($store_data[0]['is_female']) && $store_data[0]['is_female']){ echo 'checked'; }else{ echo ''; } ?>> Female
                                      </label>
                                      <label>
                                        <input type="checkbox" name="is_children" id="is_children" value="1" <?php if(isset($store_data[0]['is_children']) && $store_data[0]['is_children']){ echo 'checked'; }else{ echo ''; } ?>> Children
                                      </label>
                                      <span class="text-red"> <?php echo form_error('is_male'); ?></span>
                                    </div>

                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label>Store Type<span class="text-red">*</span></label>
                                    <div class="checkbox">
                                      <label>
                                        <input type="checkbox" name="is_online" id="is_online" value="1" <?php if(isset($store_data[0]['is_online']) && $store_data[0]['is_online'] == 1 ){ echo 'checked'; }else{ echo ''; } ?>> Online
                                      </label>
                                      <label>
                                        <input type="checkbox" name="is_offline" id="is_offline" value="1" <?php if(isset($store_data[0]['is_offline']) && $store_data[0]['is_offline'] == 1 ){ echo 'checked'; }else{ echo ''; } ?>> Offline
                                      </label>
                                      <span class="text-red"> <?php echo form_error('is_online'); ?></span>
                                    </div>
                                  </div>
                                </div>





                                </div> </div> </div>
                                <input type="hidden" name="brand_id" id="brand_id" value="<?php echo $brand_id; ?>">
                                <input type="hidden" name="store_id" id="store_id" value="<?php if(isset($store_data[0]['id']) && $store_data[0]['id']){ echo $store_data[0]['id']; } ?>">
                              </div><!-- /.box-body -->

                              <div class="box-footer">
                                <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Save</button>
                                <a href="<?php echo base_url(); ?>brandusers/manage_store/<?php echo $brand_id; ?>"><button class="btn btn-primary btn-sm btn-rad" type="button"><i class="fa fa-reply"></i> Back</button></a>
                                <button type="reset" class="btn btn-default btn-sm"><i class="fa fa-close"></i> Cancel</button>
                              </div>
                            </form>
                          </div>

                        </section>
                        <script type="text/javascript">
                        $( document ).ready(function() {
                          $("select:not([multiple])").addClass("select-box");
                          $(".select-box").SumoSelect();
                          $("select[multiple=''].multi-select-box").SumoSelect({selectAll: true });
                          $('.nav-item-user').addClass("active");
                          $('.nav-brand-user').addClass("active");
                        });
                        </script>
