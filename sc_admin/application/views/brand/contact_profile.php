<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<section class="content-header">
	<h1>Contact Details</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url(); ?>home">Home</a></li>
		<li><a href="<?php echo base_url(); ?>brandusers">Manage Store Users</a></li>
		<li class="active">Contact Details</li>
	</ol>
</section>
<section class="content">

    <form class="form-horizontal" method="post" action="" name="frmContact" id="frmContact">

              <div class="row">
                <div class="col-md-6">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title">Address Info</h3>
                    </div>
                    <div class="panel-body">
                  <div class="form-group">
                    <label class="control-label col-sm-4">Office Address<span class="text-red">*</span></label></label>
                    <div class="col-sm-8">
                      <textarea class="form-control" name="company_address" id="company_address" placeholder="Office Address"><?php echo @$contact_info[0]['company_address']; ?></textarea>
                      <div class="error text-red"><?php echo form_error('company_address'); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4">City<span class="text-red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" name="city" id="city" class="form-control" placeholder="City" value="<?php echo @$contact_info[0]['city']; ?>"/>
                      <div class="error text-red"><?php echo form_error('city'); ?></div>
                    </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-4">State<span class="text-red">*</span></label>
                        <div class="col-sm-8">
                          <input type="text" name="state" id="state" class="form-control" placeholder="State" value="<?php echo @$contact_info[0]['state']; ?>"/>
                          <div class="error text-red"><?php echo form_error('state'); ?></div>
                        </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-4">Pincode<span class="text-red">*</span></label>
                            <div class="col-sm-8">  <input type="text" name="pincode" id="pincode" class="form-control" placeholder="Pincode" value="<?php echo @$contact_info[0]['pincode']; ?>" maxlength="6"/><div class="error text-red"><?php echo form_error('pincode'); ?></div></div>

                          </div>

                          <div class="form-group">
                            <label class="control-label col-sm-4">Country<span class="text-red">*</span></label>
                              <div class="col-sm-8">
                                <input type="text" name="country" id="country" class="form-control" placeholder="Country" value="<?php echo @$contact_info[0]['country']; ?>"/><div class="error text-red"><?php echo form_error('country'); ?></div>
                              </div>
                              </div>

                </div>
                  </div>


				  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title">Warehouse Info</h3>
                    </div>
                    <div class="panel-body">
                  <div class="form-group">
                    <label class="control-label col-sm-4">Office Address</label></label>
                    <div class="col-sm-8">
                      <textarea class="form-control" name="warehouse_address" id="warehouse_address" placeholder="Office Address"><?php echo @$contact_info[0]['warehouse_address']; ?></textarea>
                      <div class="error text-red"><?php echo form_error('warehouse_address'); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4">City</label>
                    <div class="col-sm-8">
                      <input type="text" name="warehouse_city" id="warehouse_city" class="form-control" placeholder="City" value="<?php echo @$contact_info[0]['warehouse_city']; ?>"/>
                      <div class="error text-red"><?php echo form_error('warehouse_city'); ?></div>
                    </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-4">State</label>
                        <div class="col-sm-8">
                          <input type="text" name="warehouse_state" id="warehouse_state" class="form-control" placeholder="State" value="<?php echo @$contact_info[0]['warehouse_state']; ?>"/>
                          <div class="error text-red"><?php echo form_error('warehouse_state'); ?></div>
                        </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-4">Pincode</label>
                            <div class="col-sm-8">  <input type="text" name="warehouse_pincode" id="warehouse_pincode" class="form-control" placeholder="Pincode" value="<?php echo @$contact_info[0]['warehouse_pincode']; ?>" maxlength="6"/><div class="error text-red"><?php echo form_error('warehouse_pincode'); ?></div></div>

                          </div>

                          <div class="form-group">
                            <label class="control-label col-sm-4">Country</label>
                              <div class="col-sm-8">
                                <input type="text" name="warehouse_country" id="warehouse_country" class="form-control" placeholder="Country" value="<?php echo @$contact_info[0]['warehouse_country']; ?>"/><div class="error text-red"><?php echo form_error('warehouse_country'); ?></div>
                              </div>
                              </div>

                </div>
                  </div>


                    </div>


                  <div class="col-md-6">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">Business Contact Details<span class="text-red">*</span></h3>
                      </div>
                      <div class="panel-body">
                    <div class="form-group">
                      <label class="control-label col-sm-4">Name<span class="text-red">*</span></label>
                      <div class="col-sm-8">  <input type="text" name="contact_person" id="contact_person" class="form-control" placeholder="Name" value="<?php echo @$contact_info[0]['contact_person']; ?>"/><div class="error text-red"><?php echo form_error('contact_person'); ?></div></div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-sm-4">Phone Number<span class="text-red">*</span></label>
                      <div class="col-sm-8">  <input type="text" name="phone_one" id="phone_one" class="form-control" placeholder="Phone Number" value="<?php echo @$contact_info[0]['phone_one']; ?>" maxlength="13"/><div class="error text-red"><?php echo form_error('phone_one'); ?></div></div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-sm-4">Email<span class="text-red">*</span></label>
                      <div class="col-sm-8">  <input type="text" name="emailid" id="emailid" class="form-control" placeholder="Email" value="<?php echo @$contact_info[0]['emailid']; ?>"/><div class="error text-red"><?php echo form_error('emailid'); ?></div></div>

                    </div>
                  </div>
                  </div>


                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title">Tech Contact Details</h3>
                  </div>
                  <div class="panel-body">
                <div class="form-group">
                  <label class="control-label col-sm-4">Name<span class="text-red">*</span></label>
                  <div class="col-sm-8">  <input type="text" name="contact_person_two" id="contact_person_two" class="form-control" placeholder="Name" value="<?php echo @$contact_info[0]['contact_person_two']; ?>"/><div class="error text-red"><?php echo form_error('contact_person_two'); ?></div></div>

                </div>
                <div class="form-group">
                  <label class="control-label col-sm-4">Phone Number<span class="text-red">*</span></label>
                  <div class="col-sm-8">  <input type="text" name="phone_two" id="phone_two" class="form-control" placeholder="Phone Number" value="<?php echo @$contact_info[0]['phone_two']; ?>" maxlength="13"/></div>
                  <div class="error text-red"><?php echo form_error('contact_person_two'); ?>
                </div></div>
                <div class="form-group">
                  <label class="control-label col-sm-4">Email<span class="text-red">*</span></label>
                  <div class="col-sm-8">  <input type="text" name="alt_emailid" id="alt_emailid" class="form-control" placeholder="Email" value="<?php echo @$contact_info[0]['alt_emailid']; ?>"/></div>
                  <div class="error text-red"><?php echo form_error('alt_emailid'); ?></div>
                </div>
              </div>
              </div>

              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">Order Contact Details</h3>
                </div>
                <div class="panel-body">
              <div class="form-group">
                <label class="control-label col-sm-4">Name<span class="text-red">*</span></label>
                <div class="col-sm-8">  <input type="text" name="contact_person_three" id="contact_person_three" class="form-control" placeholder="Name" value="<?php echo @$contact_info[0]['contact_person_three']; ?>"/></div>
                <div class="error text-red"><?php echo form_error('contact_person_three'); ?></div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-4">Phone Number<span class="text-red">*</span></label>
                <div class="col-sm-8">  <input type="text" name="phone_three" id="phone_three" class="form-control" placeholder="Phone Number" value="<?php echo @$contact_info[0]['phone_three']; ?>" maxlength="13"/></div>
                <div class="error text-red"><?php echo form_error('phone_three'); ?></div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-4">Email<span class="text-red">*</span></label>
                <div class="col-sm-8">  <input type="text" name="alt_emailid_three" id="alt_emailid_three" class="form-control" placeholder="Email" value="<?php echo @$contact_info[0]['alt_emailid_three']; ?>"/></div>
                <div class="error text-red"><?php echo form_error('alt_emailid_three'); ?></div>
              </div>
            </div>
          </div>
                </div>
              </div>


<!--// -->
 <div class="">
                    <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Save</button>
                    <a href="<?php echo base_url(); ?>brandusers"><button class="btn btn-primary btn-sm" type="button"><i class="fa fa-reply"></i> Back</button></a>
                    <button type="reset" class="btn btn-default btn-sm"><i class="fa fa-close"></i> Cancel</button>
                  </div>
				  
</section>
<script type="text/Javascript">
$( document ).ready(function() {
	$('.nav-item-user').addClass("active");
	$('.nav-brand-user').addClass("active");
});
</script>
