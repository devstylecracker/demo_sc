<?php //echo "<pre>"; print_r($brand_profile_data); ?>
	<?php //echo "<pre>"; print_r($contact_profile_data);
		$users_menu=$this->data['users_menu'];
		$brandID = "";
		if(isset($brand_profile_data[0]->id))
		{
			$brandID = $brand_profile_data[0]->id;
		}

		//echo $brand_profile_data[0]->reject_reason;

?>

<section class="content-header">
<h1>Brand Details</h1>
<ol class="breadcrumb">
	<li><a href="<?php echo base_url(); ?>home">Home</a></li>
	<li><a href="<?php echo base_url(); ?>brandusers">Manage Brand Users</a></li>
	<li class="active">Brand Details </li>
</ol>
</section>
<section class="content">
<div class="row">
	<div class="col-md-6">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Business Details <small class="label pull-right label-info"><?php if(isset($brand_profile_data[0]->approve_reject) && $brand_profile_data[0]->approve_reject == 'A') {echo "Approved"; }
	else{ if(isset($brand_profile_data[0]->approve_reject) && $brand_profile_data[0]->approve_reject == 'R') {echo "Reject"; }else { echo "Pending"; } } ?></small></h3>
			</div>
			<div class="box-body">
				<div class="table-responsive">
					<table class="table table-bordered table-striped dataTable table-vth" id="datatable">
						<tr>
							<th style="width:35%;"><label>Company Name</label></th>
							<td><?php if(isset($brand_profile_data[0]->company_name))echo $brand_profile_data[0]->company_name; ?></td>
						</tr>
						<tr>
							<th>Brand Logo</th>
							<td> <?php if(isset($brand_profile_data[0]->logo)) { ?>
								<img src="<?php echo base_url(); ?>assets/brand_logo/<?php echo $brand_profile_data[0]->logo; ?>" height="100px" width="100px">
								<?php } ?></td>
							</tr>

							<tr>
								<th>Store Type</th>
								<!--<td><?php if(isset($brand_profile_data[0]->	is_online))echo $brand_profile_data[0]->is_online ? 'Online' : ''; ?> and <?php  if(isset($brand_profile_data[0]->	is_offline)) echo $brand_profile_data[0]->	is_offline ? 'Offline' : ''; ?></td>-->
								<td><?php if(isset($brand_profile_data[0]->	is_online) || isset($brand_profile_data[0]-> is_offline)){
									if($brand_profile_data[0]->is_online == '1' && $brand_profile_data[0]->is_offline == '1'){
										echo "Online and Offline";
									}else if($brand_profile_data[0]->is_online == '1'){
										echo "Online";
									}else if($brand_profile_data[0]->is_offline == '1'){
										echo "Offline";
									}else {
										echo "";
									}
								}else if(isset($brand_profile_data[0]->	is_online)){ 
										echo $brand_profile_data[0]->is_online ? 'Online' : ''; 
								}else if(isset($brand_profile_data[0]->	is_offline)){
									echo $brand_profile_data[0]->	is_offline ? 'Offline' : '';
								}else "";
								
								?></td>
								</tr>
								<tr>
									<th>Designer</th>
									<td><?php if(isset($brand_profile_data[0]->	designer_name)) echo $brand_profile_data[0]->	designer_name; ?></td>

								</tr>
								<tr>
									<th>Year of Establishment</th>
									<td><?php if(isset($brand_profile_data[0]->	year_establish)) echo $brand_profile_data[0]->	year_establish; ?></td>
								</tr>
								<tr>
									<th>Registration No</th>
									<td><?php if(isset($brand_profile_data[0]->registration_no)) echo $brand_profile_data[0]->registration_no; ?></td>
								</tr>
								<tr>
									<th>Website Url</th>
									<td><?php if(isset($brand_profile_data[0]->url)) echo $brand_profile_data[0]->url; ?></td>
								</tr>
								<tr>
									<th>Shop Url</th>
									<td><?php if(isset($brand_profile_data[0]->shop_url))echo $brand_profile_data[0]->shop_url; ?></td>
								</tr>
								<tr>
									<th>Short Description</th>
									<td><?php if(isset($brand_profile_data[0]->short_description)) echo $brand_profile_data[0]->short_description; ?></td>
								</tr>
								<tr>
									<th>Long Description</th>
									<td><?php if(isset($brand_profile_data[0]->long_description)) echo $brand_profile_data[0]->long_description; ?></td>
								</tr>
								<tr>
									<th>Age Group</th>
									<td><?php if(isset($brand_profile_data[0]->	age_group_from)) echo $brand_profile_data[0]->	age_group_from; ?> - <?php if(isset($brand_profile_data[0]->age_group_to)) echo $brand_profile_data[0]->age_group_to; ?></td>
								</tr>
								<tr>
									<th>Price Range</th>
									<td><?php if(isset($brand_profile_data[0]->price_range_from)) echo $brand_profile_data[0]->price_range_from; ?> - <?php if(isset($brand_profile_data[0]->price_range_to)) echo $brand_profile_data[0]->price_range_to; ?></td>
								</tr>
									<?php if(isset($brand_profile_data[0]->reject_reason) && ($brand_profile_data[0]->approve_reject!='A') )
									{
								?>
									<tr>
									<th>Reject Reason</th>
									<td><?php if(isset($brand_profile_data[0]->reject_reason)) echo $brand_profile_data[0]->	reject_reason; ?></td>
									</tr>
								<?php
									}
								?>


							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Contact Details</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-bordered table-striped dataTable table-vth" id="datatable">
								<tr>
									<th  style="width:35%;">Company Address</th>
									<td><?php if(isset($contact_profile_data[0]->company_address))echo $contact_profile_data[0]->company_address; ?> <?php if(isset($contact_profile_data[0]->city))echo $contact_profile_data[0]->city; ?> <?php if(isset($contact_profile_data[0]->state))echo $contact_profile_data[0]->	state; ?> <?php if(isset($contact_profile_data[0]->country))echo $contact_profile_data[0]->country; ?><?php if(isset($contact_profile_data[0]->pincode))echo $contact_profile_data[0]->pincode; ?></td>

								</tr>
								<tr>
									<th>Contact Person</th>
									<td><?php if(isset($contact_profile_data[0]->contact_person))echo $contact_profile_data[0]->	contact_person; ?></td>
								</tr>
								<tr>
									<th>Designation / Job Title</th>
									<td><?php if(isset($contact_profile_data[0]->job_title))echo $contact_profile_data[0]->	job_title; ?></td>

								</tr>
								<tr>
									<th>Phone 1</th>
									<td><?php if(isset($contact_profile_data[0]->phone_one))echo $contact_profile_data[0]->	phone_one; ?></td>
								</tr>
								<tr>
									<th>Phone 2</th>
									<td><?php if(isset($contact_profile_data[0]->phone_two))echo $contact_profile_data[0]->	phone_two; ?></td>
								</tr>
								<tr>
									<th>Mobile 1</th>
									<td><?php if(isset($contact_profile_data[0]->mobile_one))echo $contact_profile_data[0]->	mobile_one; ?></td>
								</tr>
								<tr>
									<th>Mobile 2</th>
									<td><?php if(isset($contact_profile_data[0]->mobile_two))echo $contact_profile_data[0]->	mobile_two; ?></td>
								</tr>
								<tr>
									<th>Fax </th>
									<td><?php if(isset($contact_profile_data[0]->fax))echo $contact_profile_data[0]->	fax; ?></td>
								</tr>
								<tr>
									<th>Email  /  Alternate Email </th>
									<td><?php if(isset($contact_profile_data[0]->emailid)) echo $contact_profile_data[0]->emailid; ?> / <?php if(isset($contact_profile_data[0]->	alt_emailid)) echo $contact_profile_data[0]->	alt_emailid; ?></td>
								</tr>
							</table>
							<br/>	<br/>	<br/>	<br/>	<br/>	<br/>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div id="res" class="col-md-offset-2 col-md-8" style="display:none;">
                <div class="form-group">
                  <label class=" control-label">Reason</label>
                  <div>
                    <textarea name="reason" class="form-control"  id="reason"><?php if(isset($contact_profile_data[0]->reject_reason)) { echo $brand_profile_data[0]->reject_reason; }?></textarea>
                  </div>
                </div>
            </div>
        </div>
        <div class="row">
			<div class="col-md-offset-4" >
			 	<?php
	          	if(in_array("43",$users_menu)) {
	           	?>
	            <input type="button" class="btn btn-primary btn-sm" name="approve" id="approve" value="Approve" onclick="approve_reject_brand('approve');">
	            <input type="button" class="btn btn-primary btn-sm" name="reject" value="Reject" onclick="approve_reject_brand('reject');">
	           <a href="<?php echo base_url(); ?>brandusers"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i> Back</button></a>
	            <!--input type="button" class="btn btn-primary" name="submit" value="Submit" -->
	          <?php } ?>
          </div>
		</div>
	</div>
</section>

<script type="text/javascript">

/*==function  to approve and reject brand ==*/
function approve_reject_brand(apr_status)
{
	if(apr_status == 'reject')
	{
		$("#res").show();
		$("#approve").attr('disabled','disabled');
	}

    var conf;
    var brand_id;
    var apr_status_var = apr_status;
    var reason = $('#reason').val();

    brand_id = <?php if($brandID!="") {echo $brandID;} else { echo 0; }  ?>;


    if(apr_status!="" && brand_id!="")
    {
    	if(apr_status=="approve")
    	{
    		conf = confirm('Are you sure you want to approve');
    	}

    	if(apr_status=="reject")
    	{
    		conf = confirm('Are you sure you want to reject');
    	}

	    if(conf)
	    {
	    	 if(reason=='' && apr_status == 'reject')
			 {
			     alert('Please enter reason');
			  }
			  else
			  {

			      $.ajax({
			          type:'POST',
			          url:'<?php echo base_url();?>brandusers/approveReject_brand/',
			          data:{apr_flag:apr_status_var, brandID:brand_id ,reason:reason},
			          success: function(data)
			          {
				            if(data)
				            {
				        		 alert(data+' successfully');
				        		 window.location = '<?php echo base_url();?>brandusers';
				            }
				            else
				            {
				              alert('OOPS!something went wrong');
				            }
			          }
			      });
			  }
	    }
    }else
    {
    	alert("This Brand Bussiness Details has not been filled");
    }

}

$('.nav-item-user').addClass("active");
$('.nav-brand-user').addClass("active");
</script>
