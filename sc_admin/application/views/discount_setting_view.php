<?php
  if(!empty($this->data['users_menu']!=""))
    {
       $users_menu=$this->data['users_menu'];
    }else
    {
       $users_menu=array();
    }
?>

<!--pqSelect dependencies-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev.js"></script>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>

<section class="content-header">
  <h1>Discount Setting</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">Discount Setting</li>
  </ol>
</section>
<section class="content">
  <div class="box">
  <div class="box-header with-border">
      <!--<h3 class="box-title">List of Products <small class="label label-info" id="total_count"><?php echo $total_rows; ?></small></h3>-->
 <?php /*echo $this->session->flashdata('feedback');*/ ?>
         <!--  <div class="pull-right">
            <?php if(in_array("60",@$users_menu))
            {
            ?>
             <a href="<?php echo base_url(); ?>Productinventory_upload"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Upload Product through CueSheet</button></a>
            <?php
            }
            ?>
          </div> -->
    <div id="message_discount"></div>

    <div class="box-body">
        <div class="row">

            <!--<div class="col-md-2">-->
              <div class="form-group col-md-2" id="brand_single_wrp">
                <select class="form-control input-sm" id="search_by" name="search_by" >
                    <option value="">Select Store</option>
                      <?php foreach($paid_store as $val)
                            {
                      ?>
                        <option value="<?php echo $val['brand_id']; ?>" <?php echo $store_id==$val['brand_id'] ? 'selected' : ''; ?> ><?php echo $val['company_name']; ?></option>
                    <?php } ?>
                </select>
              </div>

              <div class="col-md-2">
                <select class="form-control input-sm" id="discount_type" name="discount_type" onchange="show_products(1);">
                    <option value="">Select Discount Type</option>
                      <?php foreach($discount_type as $val)
                            {
                      ?>
                        <option value="<?php echo $val['discount_id']; ?>" ><?php echo $val['discount_name']; ?></option>
                    <?php } ?>
                </select>
              </div>


           <!-- </div>-->

            <div class="col-md-3" id="product_search">
                <div class="input-group" >
                <input type="text" placeholder="Product Id or SKU" class="form-control input-sm pull-right" id="table_search" value="" name="table_search">                
                <div class="input-group-btn">
                  <button type="button" class="btn btn-sm btn-default" onclick="show_products(1);"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </div>
        </div>
  </div>
   <div class="form-group hide-elm" id="brand_multi_wrp">
      <div class="row">
         <div class="col-md-1">
      <label for="brand_multi">Store</label>
      </div>
       <div class="col-md-7">
        <select tabindex="5" id="brand_multi" name="brand_multi[]" class="form-control" multiple tabindex="-1">
       <option value="">Select Store</option>
       <?php foreach($paid_store as $val)
                {
          ?>
            <option value="<?php echo $val['brand_id']; ?>" <?php echo $store_id==$val['brand_id'] ? 'selected' : ''; ?> ><?php echo $val['company_name']; ?></option>
        <?php } ?>
        </select>
      </div>
      </div>
   </div>
  <div id="sucess_failed_html"></div>
  <div id="show_discount_details"></div>
  <div id="show_brand_discount_ontotal"></div>
  <div id="show_productwise_discount">
  <table class="table table-bordered table-striped table-enventory"><thead><tr><th>Sr no.</th><th>Product ID</th><th>SKU</th><th width="50px">Product Name</th><th width="100px">MRP</th><th>Brand Discount Price</th><th>date_from</th><th>date_to</th><th width="40px">Discount</th><th>Save</th></tr></thead>
  <tbody id="show_product_list" style="word-break:break-all;">
  </tbody></table>
  </div>
  <input type="hidden" name="min_offset" id="min_offset" value="0">

</div>
</section>

<style>
.hide-elm{
  height: 0;
    margin: 0;
    visibility: hidden;
  }
</style>


<script type="text/Javascript">
$( document ).ready(function() {

  $('#show_productwise_discount').hide();
  $('#product_search').hide();

  $('#load_more').click(function(){
    load_more();
  });

  $("#brand_multi").pqSelect({
                    bootstrap: { on: true },
                    multiplePlaceholder: 'Select Brands',
                    checkbox: true, //adds checkbox to options
                }).on("change", function(evt) {
                    var val = $(this).val();
                   // console.log(val);
    });

});



function show_products(attr){
    var store_id = $('#search_by').val();
    var product_id = $('#table_search').val();        
    var discountType = $('#discount_type').val();
    if($('#min_offset').length > 0){
    var min_offset = $('#min_offset').val(); }
    else{ var min_offset =0 };
    if($('#max_offset').length > 0){
    var max_offset = $('#max_offset').val(); }
    else{ var max_offset =0 };

    if(attr == 1){ min_offset =0; }

    //$('#show_product_list').html('');
    $('#show_discount_details').html('');
    $(".page-overlay").show();
    $(".page-overlay .loader").css('display','inline-block');


    if((discountType != '4' && store_id=='') || (discountType == '0' && store_id!=''))
    {
      $(".page-overlay").hide();
      $(".page-overlay .loader").hide();
    }


    if(discountType == '1')
    { 
      $('#brand_multi_wrp').hide();
  		$('#brand_single_wrp').removeClass('hide-elm');
  		$('#show_discount_details').show;
      var date_from = $('#date_from').val();
      var date_to = $('#date_to').val();
      var date_from = $('#date_from').val();
      var date_from = $('#date_from').val();

      if(store_id!='')
      {
          $('#show_productwise_discount').hide();
          if( store_id !=''){
          $.ajax({
                url: "<?php echo base_url() ?>discountSetting/get_brandsetting",
                type: 'POST',
                data: {'store_id':store_id},
                cache: false,
                async: false,
                success: function(response) {

                $(".page-overlay").hide();
                $(".page-overlay .loader").hide();

                $('#show_discount_details').append(response);
               /* $("#date_from").datepicker({ dateFormat: "yy-mm-dd" , endDate: "+0d", autoclose: true });
                $("#date_to").datepicker({ dateFormat: "yy-mm-dd", endDate: "+0d", autoclose: true });*/

                $("#date_from").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii" , autoclose: true });
                $("#date_to").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii" , autoclose: true });

                /* $(".datetimepicker").datetimepicker({format: 'yyyy-mm-dd hh:ii'});*/

                }
            });
          }

      }else
      {
        alert("Select Store ");
      }

    }else
    if(discountType == '2')
    { 	
      $('#brand_single_wrp').removeClass('hide-elm');
		  $('#product_search').show();
		  $('#brand_multi_wrp').hide();
        //$('#show_discount_details').hide();
        $('#show_productwise_discount').show();
        if( store_id !='' || product_id !=''){
          if (product_id == parseInt(product_id, 10)){
                product_id = $('#table_search').val();                
          }else{
              product_id = $('#table_search').val();  
          }
        $.ajax({
            url: "<?php echo base_url() ?>discountSetting/get_products",
            type: 'POST',
            data: {'store_id':store_id,'discountType':discountType,'product_id':product_id,'min_offset':min_offset,'max_offset':max_offset,'date_from':date_from,'date_to':date_to},
            cache :true,
            async: false,
            success: function(response) {

              $(".page-overlay").hide();
              $(".page-overlay .loader").hide();

              if(attr == 1){
                $('#show_product_list').html(response);
              }else{
                $('#show_product_list').append(response);
              }
			  var total_products = $('#max_offset').val(); var i=1;

				  while(i<=total_products){
						/*$("#date_from"+i).datepicker({ dateFormat: "yy-mm-dd", altFormat : "yy-mm-dd", endDate: "+0d", autoclose: true});
						$("#date_to"+i).datepicker({ dateFormat: "yy-mm-dd", altFormat : "yy-mm-dd", endDate: "+0d", autoclose: true });*/
            $("#date_from"+i).datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii" , autoclose: true });
            $("#date_to"+i).datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii", autoclose: true });
					i++;
				  }
              $('#min_offset').val(parseInt(min_offset)+1);
              $('#total_count').html($('#max_offset').val());
            }
        });
      }

    }else
    if(discountType == '3'){
		$('#brand_single_wrp').removeClass('hide-elm');
		$('#brand_multi_wrp').hide();
		$('#product_search').hide();
		$('#show_productwise_discount').hide();

          if( store_id !=''){
          $.ajax({
              url: "<?php echo base_url() ?>discountSetting/get_brand_discount_ontotal",
              type: 'POST',
              data: {'store_id':store_id,'discountType':discountType},
              cache: false,
              async: false,
              success: function(response) {

                $(".page-overlay").hide();
                $(".page-overlay .loader").hide();

                $('#show_discount_details').html(response);
                /*$("#date_from").datepicker({ dateFormat: "yy-mm-dd" , endDate: "+0d", autoclose: true });
                $("#date_to").datepicker({ dateFormat: "yy-mm-dd", endDate: "+0d", autoclose: true });*/
                $("#date_from").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii" , autoclose: true });
                $("#date_to").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii", autoclose: true });

              }
          });
        }
    }else
    if(discountType == '4')
    {
		//$('#brand_multi_wrp').show();
    $('#brand_multi_wrp').hide();
	  $('#product_search').hide();
      var store_id = $('#brand_multi_wrp').val();
      var product_id = $('#table_search').val();      
      var discountType = $('#discount_type').val();

         $('#brand_single_wrp').addClass('hide-elm');
         $('#brand_multi_wrp').removeClass('hide-elm');
         // $('#brand_single_wrp').css('visibility','hidden');
         // $('#brand_multi_wrp').css('visibility','visible');
         //$('#table_search').css('visibility','hidden');
         $('#table_search1').css('visibility','hidden');

          if( store_id !='' || discountType == '4'){
          $.ajax({
              url: "<?php echo base_url() ?>discountSetting/get_stylecracker_discount",
              type: 'POST',
              data: {'discountType':discountType , 'store_id':store_id},
              cache: false,
              async: false,
              success: function(response) {

                $(".page-overlay").hide();
                $(".page-overlay .loader").hide();

                $('#show_discount_details').append(response);
               /* $("#date_from").datepicker({ dateFormat: "yy-mm-dd" , endDate: "+0d", autoclose: true });
                $("#date_to").datepicker({ dateFormat: "yy-mm-dd", endDate: "+0d", autoclose: true });*/
                $("#date_from").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii" , autoclose: true });
                $("#date_to").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii", autoclose: true });

              }

          });
        }


    }else
    if(discountType == '5')
    {

    }else
    if(discountType == '6')
    {

    }else
    if(discountType == '7')
    {

    }else
    if(discountType == '8')
    {

    }

}

function load_more(){
      if($('#min_offset').length > 0){
        var min_offset = $('#min_offset').val(); }
      else{ var min_offset =0 };
      if($('#max_offset').length > 0){
        var max_offset = $('#max_offset').val(); }
      else{ var max_offset =0 };

      min_offset_val = parseInt(min_offset)-1;

      $('#remove'+min_offset_val).remove();
      show_products(0);
}

/*function addnew_row(id,product_id){
  var new_id = parseInt(id) + 1;
  var old_count = $('#row_count'+product_id).val();
  var new_count = parseInt($('#row_count'+product_id).val())+1;
  $('#product_info'+id).append('<div id="row_'+product_id+'_'+new_count+'" class="product-size-qty-wrp"><div class="input-group-label-wrp"><input type="text" name="product_size'+product_id+'_'+new_count+'" id="product_size'+product_id+'_'+new_count+'" placeholder="Size" class="form-control input-xs"></div><div class="input-group-label-wrp"><input pattern="[0-9]" type="text" name="product_qty'+product_id+'_'+new_count+'" id="product_qty'+product_id+'_'+new_count+'" placeholder="Qty" class="form-control input-xs"></div><span onclick="remove_row('+new_count+','+product_id+');" class="btn btn-primary btn-xs btn-delete"><i class="fa fa-minus"></i></span></div>');
  $('#row_count'+product_id).val(new_count);
}

function remove_row(id,product_id){
  $('#row_'+product_id+'_'+id).remove();
}*/

/*function save_product_size(product_id){
  var row_count = $('#row_count'+product_id).val();
  var product_size = ''; var product_qty = '';
  var arrProductSizeAndCount = [];
  for(var i = 1;i<=row_count;i++){
    if(($('#product_size'+product_id+'_'+i).val() != null) && ($('#product_qty'+product_id+'_'+i).val() != null)){
      $('#product_size'+product_id+'_'+i).val();
      $('#product_qty'+product_id+'_'+i).val();
      arrProductSizeAndCount.push([$('#product_size'+product_id+'_'+i).val(),$('#product_qty'+product_id+'_'+i).val()]);
    }
  }
  var myJsonString = JSON.stringify(arrProductSizeAndCount);

  $.ajax({
            url: "<?php echo base_url() ?>productinventory_new/save_product_size",
            type: 'POST',
            data: {'product_id':product_id,'product_info':myJsonString},
            cache :true,
            async: false,
            success: function(response) {
                $('#message'+product_id).html('Save Successfully');
            },
            error: function(xhr) {

            }
  });

}*/

function save_brand_discount_ontotal(){
	var store_id = $('#search_by').val();
	var discountType = $('#discount_type').val();
	var date_from = $('#date_from').val();
	var date_to = $('#date_to').val();
  var d = new Date(); // for now
/*  var fromdatetext = date_from+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
  var todatetext = date_to+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();*/
  var fromdatetext = date_from+":00";
  var todatetext = date_to+":00";
	var max_amount = $('#max_amount').val();
	var discount = $('#discount').val();

	if(date_from>date_to)
	{
		alert('from date should be less than to date');
	}else if(date_from == ''){
		alert('date from should not be empty');
	}else if(date_to == ''){
		alert('date to should not be empty');
	}else if(max_amount == ''){
		alert('max amount should not be empty');
	}else if(discount == ''){
		alert('discount should not be empty');
	}else if(discount >= 100){
		alert('discount % should be less than 100%');
	}else
		if( store_id !=''){
				$.ajax({
					url: "<?php echo base_url() ?>discountSetting/save_brand_discount_ontotal",
					type: 'POST',
					data: {'store_id':store_id,'date_from':fromdatetext,'date_to':todatetext,'max_amount':max_amount,'discount':discount,'discountType':discountType},
					cache: false,
					async: false,
					success: function(response) {

						if(response == 1)
						{
							alert("Success");
						}else{
							alert("Error");
						}

						//$('#sucess_failed_html').html(response);

					}
				});
			  }
}

function save_product_discount(product_id,i){
	var store_id = $('#search_by').val();
	var discountType = $('#discount_type').val();
	var date_from = $('#date_from'+i).val();
	var date_to = $('#date_to'+i).val();
	var discount = $('#product_discount'+i).val();

 var d = new Date(); // for now
 /* var fromdatetext = date_from+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
 var todatetext = date_to+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();*/
  var fromdatetext = date_from+":00";
  var todatetext = date_to+":00";

	if(date_from>date_to)
	{
		alert('from date should be less than to date');
	}else if(date_from == ''){
		alert('date from should not be empty');
	}else if(date_to == ''){
		alert('date to should not be empty');
	}else if(discount == ''){
		alert('discount should not be empty');
	}else if(discount >= 100){
		alert('discount % should be less than 100%');
	}else
		if( store_id !=''){
				$.ajax({
					url: "<?php echo base_url() ?>discountSetting/save_product_discount",
					type: 'POST',
					data: {'store_id':store_id,'date_from':fromdatetext,'date_to':todatetext,'discount':discount,'discountType':discountType,'product_id':product_id},
					cache: false,
					async: false,
					success: function(response) {

            /*$("#date_from"+i).datepicker({ dateFormat: "yy-mm-dd" , endDate: "+0d", autoclose: true });
            $("#date_to"+i).datepicker({ dateFormat: "yy-mm-dd", endDate: "+0d", autoclose: true });*/
            $("#date_from").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii" , autoclose: true });
            $("#date_to").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii", autoclose: true });

						if(response == 1)
						{
								alert("Success");
						}else{
							alert("Error");
						}

					}
				});
			  }
  }



function save_stylecracker_discount(){

  var store_id = $('#brand_multi').val();
  var discountType = $('#discount_type').val();
  var date_from = $('#date_from').val();
  var date_to = $('#date_to').val();
  var max_amount = $('#max_amount').val();
  var discount = $('#discount').val();

  var d = new Date(); // for now
 /* var fromdatetext = date_from+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
  var todatetext = date_to+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();*/
  var fromdatetext = date_from+":00";
  var todatetext = date_to+":00";

  if(date_from>date_to)
  {
    alert('from date should be less than to date');
  }else if(date_from == ''){
    alert('date from should not be empty');
  }else if(date_to == ''){
    alert('date to should not be empty');
  }else if(max_amount == ''){
    alert('max amount should not be empty');
  }else if(discount == ''){
    alert('discount should not be empty');
  }else if(discount >= 100){
    alert('discount % should be less than 100%');
  }else  if( store_id !='')
  {
        $.ajax({
          url: "<?php echo base_url() ?>discountSetting/save_stylecracker_discount",
          type: 'POST',
          data: {'store_id':store_id,'date_from':fromdatetext,'date_to':todatetext,'max_amount':max_amount,'discount':discount,'discountType':discountType},
          cache: false,
          async: false,
          success: function(response) {

            if(response == 1)
            {
                location.reload();
            }else{
              location.reload();
            }

            //$('#sucess_failed_html').html(response);

          }
        });
  }
  }


  function save_brand_discount(store_id){

  var discountType = $('#discount_type').val();
  var date_from = $('#date_from').val();
  var date_to = $('#date_to').val();
  var discount = $('#discount').val();
  var d = new Date(); // for now
  /*var fromdatetext = date_from+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
  var todatetext = date_to+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();*/
  var fromdatetext = date_from+":00";
  var todatetext = date_to+":00";
  

    if(date_from>date_to)
    {
      alert('from date should be less than to date');
    }else if(date_from == ''){
      alert('date from should not be empty');
    }else if(date_to == ''){
      alert('date to should not be empty');
    }else if(discount == ''){
      alert('discount should not be empty');
    }else if(discount >= 100){
      alert('discount % should be less than 100%');
    }else  if( store_id !='')
    {
          $.ajax({
            url: "<?php echo base_url() ?>discountSetting/save_brand_discount",
            type: 'POST',
            data: {'store_id':store_id,'date_from':fromdatetext,'date_to':todatetext,'discount':discount,'discountType':discountType},
            cache: false,
            async: false,
            success: function(response) {
              if(response == 1)
              {
                  $('#message_discount').html('<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>discount Added Successfully</div>');
                   location.reload();

              }else if(response == 3)
              {
                  $('#message_discount').html('<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>Discount already set for the store</div>');
                    $('#date_from').val('');
                    $('#date_to').val('');
                    $('#discount').val('');


              }else if(response == 2)
              {
                  $('#message_discount').html('<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>Something goes wrong Please try again</div>');
                    $('#date_from').val('');
                    $('#date_to').val('');
                    $('#discount').val('');

              }
              //$('#sucess_failed_html').html(response);

            }
          });
    }
  }

</script>
