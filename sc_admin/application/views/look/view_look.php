<section class="content-header">
    <h1>Look Details</h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>home">Home</a></li>
      <li><a href="<?php echo base_url(); ?>lookcreator/manage_look">Manage Looks </a></li>
      <li class="active">Look Details</li>
    </ol>
</section>
<section class="content page-look-details">
  <div class="row">
    <div class="col-md-6">
      <div class="box look-details">

        <div class="box-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable table-vth" id="datatable">
                    <tr>
                      <th style="width:25%;">Look ID</th>
                      <td><?php echo $look_data['0']['id']; ?></td>
                    </tr>
                    <tr>
                      <th>Name</th>
                      <td><?php echo $look_data['0']['name']; ?></td>
                    </tr>
                    <tr>
                      <th>Description</th>
                      <td><?php echo $look_data['0']['description']; ?></td>
                    </tr>
                    <tr>
                      <th>Look Type</th>
                      <td><?php if($look_data['0']['look_type'] == 1){ echo 'Stylist'; }else if($look_data['0']['look_type'] == 2){ echo 'Street Style Look'; }else if($look_data['0']['look_type'] == 3){ echo "Promotional Look"; } ?></td>
                    </tr>
                    <tr>
                      <th>Status</th>
                      <td><?php if($look_data['0']['status']==0){ echo "<span class='text-orange'>Pending</span>"; }else if($look_data['0']['status']==1){ echo "<span class='text-green'>Approved</span>"; }else { echo "<span class='text-red'>Rejected</span>"; } ?></td>
                    </tr>
                    <tr><th>Product Images</th>
                      <td class="prod">
            						<?php
            						if(!empty($product_data)){
            							foreach($product_data as $val){  ?>
            								<img src="<?php echo base_url(); ?>assets/products/thumb_160/<?php echo $val['image']; ?>" >
            							<?php	}
            							}
            						?>
            						</td>
            					</tr>
                    </table>
                  </div>
                </div>
              </div>
</div>
<div class="col-md-6">
  <div class="box">
    <div class="box-body">
      <div class="look-img">
            <?php if($look_data['0']['look_type']== 2 || $look_data['0']['look_type']== 4) { ?>
              <img src="<?php echo base_url(); ?>assets/street_img/<?php echo $look_data['0']['look_image']; ?>" >
          <?php }else if($look_data['0']['look_type']== 3){ ?>
          <img src="<?php echo base_url(); ?>assets/products/thumb/<?php echo $look_data['0']['product_img']; ?>" >
          <?php }else{ ?>
            <img src="<?php echo base_url(); ?>assets/looks/<?php echo $look_data['0']['image']; ?>" >
            <?php } ?>
  </div>
        </div>
        </div>
        </div>
        </div>
</section>
<script type="text/Javascript">
	$( document ).ready(function() {
		$('.nav-item-look').addClass("active");
    $('.nav-manage-looks').addClass("active");
	});
</script>
