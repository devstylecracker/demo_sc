<?php
$users_menu=$this->data['users_menu'];
?>
<section class="content-header">
        <h1>Look</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>
            <li><a href="<?php echo base_url(); ?>lookcreator/manage_look">Look Manage</a></li>
            <li class="active">Edit Look</li>
          </ol>
</section>
<section class="content">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Look</h3>
                </div>
                <form role="form" name="frmtag" id="frmtag" action="<?php echo base_url(); ?>lookcreator/look_edit/<?php echo $look_data[0]['id']; ?>" method="post">
                  <div class="box-body">
                    <div class="row">
              <div class="col-md-4">
                    <div class="form-group">
                      <label for="look_name" class="control-label">Look Name
						          <span class="text-red">*</span></label>
                      <input type="text" placeholder="Look Name" name="look_name" id="look_name" value="<?php if($look_data[0]['name']!="") echo $look_data[0]['name']; else echo set_value('look_name');?>" class="form-control"> <span class="text-red"><?php echo form_error('look_name'); ?></span>
                    </div>

                    <div class="form-group">
                      <label for="categorySlug" class="control-label">Description	<span class="text-red">*</span></label>
                      <input type="text" placeholder="Enter Description" name="look_desc" id="look_desc" value="<?php if($look_data['0']['description']!="") echo $look_data['0']['description']; else echo set_value('description');?>"  <?php //if(@$tag->slug!="") echo 'disabled'; ?> class="form-control">
                      <span class="text-red"><?php echo form_error('description'); ?></span>
                    </div>
                  </div>
                    </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                    <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i>  Submit</button>
                    <a href="<?php echo base_url(); ?>lookcreator/manage_look"><button type="button" class="btn btn-sm btn-primary btn-rad"><i class="fa fa-reply"></i> Back</button></a>
                    <button class="btn btn-sm btn-default" type="reset"><i class="fa fa-close"></i> Cancel</button>
                  </div>
                </form>
              </div>
</section>
<script type="text/Javascript">
	$( document ).ready(function() {
		$('.nav-item-look').addClass("active");
    $('.nav-manage-looks').addClass("active");
	});
</script>
