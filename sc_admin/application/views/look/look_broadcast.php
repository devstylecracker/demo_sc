<?php
/*echo "<pre>";print_r($product_data); exit;*/
?>
<section class="content-header">
    <h1>Look Broadcast</h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>home">Home</a></li>
      <li><a href="<?php echo base_url(); ?>lookcreator/manage_look">Manage Looks </a></li>
      <li class="active">Look Broadcast</li>
    </ol>
</section>
<section class="content page-look-details">
  <div class="row">
    <div class="col-md-6">
      <div class="box look-details">

        <div class="box-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable table-vth" id="datatable">

                    <tr>
                      <td>Look ID</td>
                      <td><?php echo $look_data['0']['id']; ?></td>
                    </tr>
                    <tr>
                      <td>Name</td>
                      <td><?php echo $look_data['0']['name']; ?></td>
                    </tr>
                    <tr>
                      <td>Description</td>
                      <td><?php echo $look_data['0']['description']; ?></td>
                    </tr>
                    <tr>
                      <td>Look Type</td>
                      <td><?php if($look_data['0']['look_type'] == 1){ echo 'Stylist'; }else if($look_data['0']['look_type'] == 2){ echo 'Street Style Look'; }else if($look_data['0']['look_type'] == 3){ echo "Promotional Look"; } ?></td>
                    </tr>
                     <tr>
                      <td>Status</td>
                      <td><?php if($look_data['0']['status']==0){ echo "<span class='text-orange'>Pending</span>"; }else if($look_data['0']['status']==1){ echo "<span class='text-green'>Approved</span>"; }else { echo "<span class='text-red'>Rejected</span>"; } ?></td>
                    </tr>

                     <tr>
						<td>Product Images</td>
						<td class="prod">
						<div class="image-thumbs">
						<ul id="product_list" class="product_imgs">
						<?php
						if(!empty($product_data)){
							foreach($product_data as $val){  ?>
							<li id='productSelect' style=" display: inline-block; ">
								<img src="<?php echo base_url(); ?>assets/products/thumb_160/<?php echo $val['image']; ?>" title="<?php echo "Product_count : ".$val['product_used_count'];?> Price : <?php echo $val['price']; ?>" data-price="<?php echo $val['price']; ?>" data-product="<?php echo $val['product_id']; ?>" >				
							</li>
							<?php	}
							}
						?>
						</ul>
						</div>
						</td>
					</tr>
					<tr>
						<td>Look Cost</td>
						<td><div id="final_look_cost" value="0"></div></td>	
					</tr>

                     </table>
                  </div>
                </div>
              </div>
</div>
<div class="col-md-6">
  <div class="box">
    <div class="box-body">
      <div class="look-img">
      <?php if($look_data['0']['look_type']== 2 || $look_data['0']['look_type']== 4 || $look_data['0']['look_type']== 6 ) { ?>
					<img height="300px" width="100px" src="<?php echo base_url(); ?>assets/street_img/<?php echo $look_data['0']['look_image']; ?>" >
		<?php }else if($look_data['0']['look_type']== 3){ ?>
		<img height="300px" width="100px" src="<?php echo base_url(); ?>assets/products/thumb/<?php echo $look_data['0']['product_img']; ?>" >
		<?php }else{ ?>
			<img height="300px" width="100px" src="<?php echo base_url(); ?>assets/looks/<?php echo $look_data['0']['image']; ?>" >
			<?php } ?>

  	  		</div>
        </div>
        </div>
        </div>
        </div>


               <div class="box">
               	<label style="margin:10px 0px 20px 10px;"><input style="position: relative;top: 3px;" type="radio" class="gender" id="gender" name="gender" value="1"> Female</label>
               	<label style="margin:10px 0px 20px 10px;"><input style="position: relative;top: 3px;" type="radio" class="gender" id="gender" name="gender" value="2"> Male</label>
				    <!-- Female -->
				    <div class="box-body female hide">
						  <div class="row">
				    <div class="col-md-2">
								 <div class="form-group">
									  <label>Body Shape<span class="text-red">*</span></label>
										<select name="body_shape" id="body_shape"  class="form-control input-sm" multiple="true">
										<?php if(!empty($body_shape)) {
											foreach($body_shape as $key=>$val){
												echo '<option value="'.$key.'">'.$val.'</option>';
											}
										} ?>
										</select>
										</div>
										</div>
										 <div class="col-md-2">
										<div class="form-group">

										<label>Body Style<span class="text-red">*</span></label>
										<select name="body_style" id="body_style"  class="form-control input-sm" multiple="true">
										 <?php if(!empty($style)) {
													foreach($style as $key=>$val){
															echo '<option value="'.$key.'">'.$val.'</option>';
													}
											} ?>
										</select>
				</div></div>
										 <div class="col-md-2">
										<div class="form-group">
										<label>Budget<span class="text-red">*</span></label>
										<select name="budget" id="budget"  class="form-control input-sm">
										 <?php if(!empty($budget)) {
													foreach($budget as $key=>$val){
															echo '<option value="'.$key.'">'.$val.'</option>';
													}
											} ?>
										</select>
				</div>
				</div>
										 <div class="col-md-2">
										<div class="form-group">
										<label>Age</label>
										<select name="age" id="age"  class="form-control input-sm">
										 <?php if(!empty($age)) {
													foreach($age as $key=>$val){
															echo '<option value="'.$key.'">'.$val.'</option>';
													}
											} ?>
										 </select>
				</div>
				</div>
										 <div class="col-md-2">
										<div class="form-group">
										<label>Size</label>
										<select name="size" id="size"  class="form-control input-sm">
										 <?php if(!empty($size)) {
													foreach($size as $key=>$val){
															echo '<option value="'.$key.'">'.$val.'</option>';
													}
											} ?>
										 </select>
										 </div>
										 </div>
										 <div class="col-md-2">
										<div class="form-group">
										 <label>Look Category<span class="text-red">*</span></label>
										 <select name="look_cat" id="look_cat"  class="form-control input-sm">
										 <?php if(!empty($look_sub_cat)) {
											 echo '<option value="0">Select</option>';
													foreach($look_sub_cat as $val){
															echo '<option value="'.$val['id'].'">'.$val['category_name'].'</option>';
													}
											} ?>
										 </select>
										 </div>
										 </div>

										  <div class="col-md-2">
										<div class="form-group">
										 <label>Select Stylist<span class="text-red">*</span> </label>
										 <select name="stylist_name" id="stylist_name" class="form-control input-sm">
										 <?php if(!empty($stylist_data)) {
											 echo '<option value="0">Select</option>';
													foreach($stylist_data as $val){
															echo '<option value="'.$val['id'].'">'.$val['stylist_name'].'</option>';
													}
											} ?>
										 </select>
										 </div>
										 </div>



										 <div class="col-md-2">
										<div class="form-group">
										 <label>&nbsp;</label>
									  <div><button value="" type="button" name="broadcast" id="broadcast"  class="btn btn-primary btn-sm">
										  <i class="fa fa fa-thumbs-up"></i> Women Broadcast </button>
									 <input type="hidden" name="look_id" id="look_id" value="<?php echo $look_data['0']['id']; ?>">
									 </div> </div>
									 </div>
										</div>
									 <div id="message_display"></div>

				                </div><!-- /.box-body Female -->

				                <!-- Male -->
				                <div class="box-body male hide">
		  							<div class="row">
    									<div class="col-md-2">
										 	<div class="form-group">
											  <label>Body Shape<span class="text-red">*</span></label>
												<select name="mens_body_shape" id="mens_body_shape"  class="form-control input-sm" multiple="true">
												<?php if(!empty($mens_body_shape)) {
													foreach($mens_body_shape as $key=>$val){
														echo '<option value="'.$key.'">'.$val.'</option>';
													}
												} ?>
												</select>
												</div>
											</div>
											 <div class="col-md-2">
												<div class="form-group">
													<label>Body Style<span class="text-red">*</span></label>
													<select name="mens_body_style" id="mens_body_style"  class="form-control input-sm" multiple="true">
													 <?php if(!empty($mens_style)) {
																foreach($mens_style as $key=>$val){
																		echo '<option value="'.$key.'">'.$val.'</option>';
																}
														} ?>
													</select>
												</div>
											</div>

											<div class="col-md-2">
												<div class="form-group">
													<label>Work Style<span class="text-red">*</span></label>
													<select name="mens_work_style" id="mens_work_style"  class="form-control input-sm" multiple="true">
													 <?php if(!empty($mens_work_style)) {
																foreach($mens_work_style as $key=>$val){
																		echo '<option value="'.$key.'">'.$val.'</option>';
																}
														} ?>
													</select>
												</div>
											</div>
											
											<div class="col-md-2">
												<div class="form-group">
													<label>Body Personal Style<span class="text-red">*</span></label>
													<select name="mens_personal_style" id="mens_personal_style"  class="form-control input-sm" multiple="true">
													 <?php if(!empty($mens_personal_style)) {
																foreach($mens_personal_style as $key=>$val){
																		echo '<option value="'.$key.'">'.$val.'</option>';
																}
														} ?>
													</select>
												</div>
											</div>
						 <div class="col-md-2">
							<div class="form-group">
								<label>Budget<span class="text-red">*</span></label>
								<select name="mens_budget" id="mens_budget"  class="form-control input-sm">
						 			<?php if(!empty($mens_budget)) {
										foreach($mens_budget as $key=>$val){
												echo '<option value="'.$key.'">'.$val.'</option>';
										}
									} ?>
								</select>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label>Age</label>
								<select name="mens_age" id="mens_age"  class="form-control input-sm">
								 <?php if(!empty($mens_age)) {
											foreach($mens_age as $key=>$val){
													echo '<option value="'.$key.'">'.$val.'</option>';
											}
									} ?>
								 </select>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label>Size</label>
								<select name="mens_size" id="mens_size"  class="form-control input-sm">
								 <?php if(!empty($mens_size)) {
											foreach($mens_size as $key=>$val){
													echo '<option value="'.$key.'">'.$val.'</option>';
											}
									} ?>
								 </select>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								 <label>Look Category<span class="text-red">*</span></label>
								 <select name="mens_look_cat" id="mens_look_cat"  class="form-control input-sm">
								 <?php if(!empty($look_sub_cat)) {
									 echo '<option value="0">Select</option>';
											foreach($look_sub_cat as $val){
													echo '<option value="'.$val['id'].'">'.$val['category_name'].'</option>';
											}
									} ?>
								 </select>
						 	</div>
						</div>

						<div class="col-md-2">
							<div class="form-group">
								 <label>Select Stylist<span class="text-red">*</span> </label>
								 <select name="mens_stylist_name" id="mens_stylist_name" class="form-control input-sm">
								 <?php if(!empty($stylist_data)) {
									 echo '<option value="0">Select</option>';
											foreach($stylist_data as $val){
													echo '<option value="'.$val['id'].'">'.$val['stylist_name'].'</option>';
											}
									} ?>
								 </select>
							</div>
						 </div>



						<div class="col-md-2">
							<div class="form-group">
						 		<label>&nbsp;</label>
						  		<div>
						  		<button value="" type="button" name="mens_broadcast" id="mens_broadcast"  class="btn btn-primary btn-sm">
							  	<i class="fa fa fa-thumbs-up"></i> Men Broadcast </button>
						 		<input type="hidden" name="mens_look_id" id="mens_look_id" value="<?php echo $look_data['0']['id']; ?>">
					 		</div> 
						</div>
					 </div>
					</div>
					<div id="mens_message_display"></div>

                </div><!-- /.box-body Male -->

              </div><!-- /.box -->


</section>
 <script type="text/Javascript">
 	var gender=0;
 	$('.gender').click(function() {
 		gender = $(this).val();
 		if(gender == 1){
 			$(".male").addClass("hide");
 			$(".female").removeClass("hide");
 		}else if(gender == 2){
 			$(".male").removeClass("hide");
 			$(".female").addClass("hide");
 		}
 	});

	$( document ).ready(function() {
		$( "#broadcast" ).click(function() {
			var gender = $("input[type='radio'].gender:checked").val();
			var look_id = $('#look_id').val();
			var body_shape = $('#body_shape').val();
			var body_style = $('#body_style').val();
			var look_cat = $('#look_cat').val();
			var stylist_name = $('#stylist_name').val();
			var budget = $('#budget').val();
			var size = $('#size').val();
			var age = $('#age').val();

			if(look_id.trim() == ''){
				$('#message_display').html('Something went wrong');
				}else if(body_shape == '' || body_shape<=0){
					$('#message_display').html('<span class="text-red" >Please select Body Shape</span>');
					}else if(body_style == '' || body_style<=0){
						$('#message_display').html('<span class="text-red" >Please select Body Style</span>');
						}else if(look_cat.trim() == '' || look_cat<=0){
						$('#message_display').html('<span class="text-red" >Please select Look Category</span>');
						}else if(stylist_name.trim() == '' || stylist_name<=0){
						$('#message_display').html('<span class="text-red" >Please select Stylist</span>');
						}else if(budget.trim() == '' || budget<=0){
						$('#message_display').html('<span class="text-red" >Please select Budget</span>');
						}
						else{
							$.ajax({
							 type: "POST",
							 url: "<?php echo base_url(); ?>"+"Lookcreator/look_broadcast_save/",
							 data: { look_id: look_id , body_shape: body_shape, body_style:body_style,look_cat:look_cat,stylist_name:stylist_name,budget:budget,size:size,age:age,gender:gender },
							 cache:false,
							 success:
								  function(result){
									  if(result == 'Ok')
									  {
									  	$('#message_display').html('<span class="text-green" >Successfully Broadcast the look</span>');
									  }else
									  {
									  	if(result == 'No')
									  	{
										  	$('#message_display').html('<span class="text-red" >Look Cannot be Broadcasted As Product used count exceeded the set limit  </span>');							  	
										  	$("#broadcast").attr("disabled","disabled");
										}else
										{
											$('#message_display').html('<span class="text-red" >Error</span>');
										}
									  }							  
									
								  }

							});

						}
		});

		$( "#mens_broadcast" ).click(function() {
			var gender = $("input[type='radio'].gender:checked").val();
			var look_id = $('#mens_look_id').val();
			var body_shape = $('#mens_body_shape').val();
			var body_style = $('#mens_body_style').val();
			var mens_personal_style = $('#mens_personal_style').val();
			var mens_work_style = $('#mens_work_style').val();
			var look_cat = $('#mens_look_cat').val();
			var stylist_name = $('#mens_stylist_name').val();
			var budget = $('#mens_budget').val();
			var size = $('#mens_size').val();
			var age = $('#mens_age').val();

			if(look_id.trim() == ''){
				$('#mens_message_display').html('Something went wrong');
				}else if(body_shape == '' || body_shape<=0){
					$('#mens_message_display').html('<span class="text-red" >Please select Body Shape</span>');
					}else if(body_style == '' || body_style<=0){
						$('#mens_message_display').html('<span class="text-red" >Please select Body Style</span>');
						}else if(look_cat.trim() == '' || look_cat<=0){
						$('#mens_message_display').html('<span class="text-red" >Please select Look Category</span>');
						}else if(stylist_name.trim() == '' || stylist_name<=0){
						$('#mens_message_display').html('<span class="text-red" >Please select Stylist</span>');
						}else if(mens_personal_style == null || mens_personal_style.toString().trim() == '' || mens_personal_style<=0){
						$('#mens_message_display').html('<span class="text-red" >Please select Personal Style</span>');
						}else if(mens_work_style == null || mens_work_style.toString().trim() == '' || mens_work_style<=0){
						$('#mens_message_display').html('<span class="text-red" >Please select Work Style</span>');
						}else if(budget.trim() == '' || budget<=0){
						$('#mens_message_display').html('<span class="text-red" >Please select Budget</span>');
						}else{
							$.ajax({
							 type: "POST",
							 url: "<?php echo base_url(); ?>"+"Lookcreator/look_broadcast_save/",
							 data: { look_id: look_id , body_shape: body_shape,mens_personal_style:mens_personal_style,mens_work_style:mens_work_style, body_style:body_style,look_cat:look_cat,stylist_name:stylist_name,budget:budget,size:size,age:age,gender:gender },
							 cache:false,
							 success:
								  function(result){								  
									  console.log(result);
									  if(result == 'Ok')
									  {
									  	$('#mens_message_display').html('<span class="text-green" >Successfully Broadcast the look</span>');
									  }else
									  {
									  	if(result == 'No')
									  	{
										  	$('#mens_message_display').html('<span class="text-red" >Look Cannot be Broadcasted As Product used count exceeded the set limit  </span>');							  	
										  	$("#broadcast").attr("disabled","disabled");
										}else
										{
											$('#mens_message_display').html('<span class="text-red" >Error</span>');
										}
									  }							  
									
								  }

							});

						}
		});

		$('.nav-item-look').addClass("active");

		function msg(m){ console.log(m); }		
	//##Image Select/Deselect Image
	 var arrProductID = [];
	$('ul.product_imgs li').on('mouseup',function(){

		//##Getting Product ID--------------------
	    intProductID = $(this).find('img').attr('data-product');

		if(!$(this).hasClass('selected')){
			$(this).addClass('selected');
			var intValue = Number($('#final_look_cost').text());
			$('#final_look_cost').text(intValue+Number($(this).find('img').attr('data-price')));
			arrProductID.push(intProductID);  
			var final_look_cost = $('#final_look_cost').text();			
			setBudget(final_look_cost);
			msg("add: "+ intProductID+" :: arrProductID: "+arrProductID);
			
		} else {
			$(this).removeClass('selected');
			 arrProductID = $.grep(arrProductID, function( a ) {
			        return a !== intProductID;
			    });

			    msg("delete: "+ intProductID+" :: arrProductID: "+arrProductID);
			var intValue = Number($('#final_look_cost').text());
			$('#final_look_cost').text(intValue-Number($(this).find('img').attr('data-price')));

			var final_look_cost = $('#final_look_cost').text();
			setBudget(final_look_cost);			
		}
	});


	});

	function setBudget(finallookcost)
	{
		var final_look_cost = finallookcost;
		
		if(final_look_cost >= 50000)
		{
			$('#budget').val(4670);				
			$('#mens_budget').val(4691);
		}else
		if(final_look_cost > 15000 && final_look_cost < 50000 )
		{
			$('#budget').val(38);
			$('#mens_budget').val(4690);
		}else 
		if(final_look_cost > 5000 && final_look_cost < 15000)
		{
			$('#budget').val(37);
			$('#mens_budget').val(4689);
		}else 
		if(final_look_cost > 0 && final_look_cost < 5000 )
		{
			$('#budget').val(36);		
			$('#mens_budget').val(4688);		
		}else 
		if(final_look_cost == 0)
		{
			$('#budget').val(0);
			$('#mens_budget').val(0);
		}
		$('#budget').attr('selected');		
		$('#mens_budget').attr('selected');
	}

 </script>

 <style> .selected { border:2px solid green;}</style>
