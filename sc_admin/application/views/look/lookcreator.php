<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/redmond/jquery-ui.css" />    
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<!--pqSelect dependencies-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.lazy.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/look_creator.js?v=1.1" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/fabric.min.js" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet"/>
<script src="<?php echo base_url(); ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>

<div class="container11">
<section class="content">
	<div class="look-creater-wrp">
	<div class="row">
		  <div class="col-xs-12 col-md-7 col-lg-7">
			  <div class="canvas-wrp">
				  <!--  <div class="canvas-top">
					</div> -->
					 <div class="canvas-middle">
						 <div class="row">
							<div class="col-xs-2 col-md-2 col-lg-2">
								<div class="canvas-left-panel panel">
									<!--span class="btn">New Look</span>
									<span class="btn">Open</span>
									<span class="btn">Save</span>
									<span class="btn">Submit</span>
									<span class="btn">Publish</span-->
									<!--<span class="btn">Undo</span>
									<span class="btn">Redo</span>-->

									<span class="btn btn btn-sm btn-primary delete-bg-btn">Delete BG</span>
									<span class="btn btn btn-sm btn-primary reset-btn">Reset All</span>
									<span class="btn btn btn-sm btn-primary publish-btn">Save</span>

									<!--<span class="btn btn btn-sm btn-primary get-look-btn">Get Look</span>
									<span class="btn btn btn-sm btn-primary set-look-btn">Set Look</span>-->
								</div>

								<!-- Added by saylee start -->
									<div class="panel">
										<div class="panel-recom-prod">
									   		<div class="title"> Products </div>
										 		<ul id="recommended_products" class="recom-prods">
												</ul>
								 		</div>
						 			</div>
						 		<!-- Added by saylee end -->

							</div>
							<div class="col-xs-10 col-md-10 col-lg-10">
								 <div class="canvas-top-panel panel">
									 <div class="row">
										 	<div class="col-md-6 col-lg-6">
									
								</div>
								<div class="col-md-6 col-lg-6">
									<span class="btn btn-sm btn-primary delete-btn">Remove</span>
									<span class="btn btn-sm btn-primary flip-btn">Flip</span>
									<span class="btn btn-sm btn-primary flop-btn">Flop</span>
									<span class="btn btn-sm btn-primary clone-btn">Clone</span>
								</div>
							</div>
									<div class="panel-text-style text-properties">

										<select class="form-control input-sm inline cmb-font">
											<option>Times New Roman</option>
											<option>Arial</option>
											<option>Comic Sans MS</option>
											<option>Georgia</option>
											<option>Impact</option>
											<option>Verdana</option>
										</select>

										<select class="form-control input-sm inline cmb-color">
											<option>Black</option>
											<option>Red</option>
											<option>Blue</option>
											<option>Green</option>
										</select>

										<select class="form-control input-sm inline cmb-shadow">
											<option selected disabled>Select Shadow</option>
											<option>Show</option>
											<option>Hide</option>
										</select>
									 </div>


								</div>
								<div class="panel canvas-outer">
							  <div class="canvas">
								 <!--  <img class="canvas-sc-logo" src='<?php echo base_url(); ?>/assets/images/sc-logo.jpg' /> -->
								  <canvas id="canvas" class="main-canvas" width="530" height="530"></canvas>
							  </div>
							  <!--
							   <div class="canvas-bottom panel">
								   <select>
										<option selected disabled>Shape</option>
										<option>Shape 1</option>
										<option>Shape 2</option>
										<option>Shape 3</option>
									</select>
									 <select>
										<option selected disabled>Size</option>
										<option>Size 1</option>
										<option>Size 2</option>
										<option>Size 3</option>
									</select>
									<select>
										<option selected disabled>Budget</option>
										<option>Budget 1</option>
										<option>Budget 2</option>
										<option>Budget 3</option>
									</select>
									<select>
										<option selected disabled>Age</option>
										<option>Age 1</option>
										<option>Age 2</option>
										<option>Age 3</option>
									</select>

								</div> -->

							</div>
							<div class="panel panel-look-desc-wrp">
								<div class="panel-look-desc">
								<div class="row">
									<div class="col-md-5">
							<input type="text" name="look_name" class="form-control input-sm" id="look_name" placeholder="Enter look name" >
						<div id="error_text" class="error_text"></div>

						
							 <input class="form-control input-sm tag-txt" type="text" name="look_tags" id="look_tags" placeholder="Add tags for this look, separated by #"/>
							 </div>

								<div class="col-md-7">
							<textarea name="look_desc" id="look_desc" class="form-control input-sm"  placeholder="Enter look description"></textarea>
							 </div>

							

							</div>
							 </div>
						<!--	<div class="panel-footer">
							<input type="submit" name="save_look" class="btn btn-primary btn-sm" id="save_look" value="Save" onclick="save_street_look();">

						</div>-->

						</div>
							</div>
						</div>
					</div>


			  </div>
		  </div>
		   <div class="col-xs-12 col-md-5 col-lg-5">
			   <div class="canvas-data-wrp">
				    <div class="canvas-data-selection">
							<div class="steps">
						   <ul class="">
							   <!--
							   <li class="step1 active">Select Template</li> -->
							   <li class="step2 active">Select Products <span class="caret"></span></li>
							   <li class="step3">Apply Effects <span class="caret"></span></li>
							   <li class="step4">Stickers <span class="caret"></span></li>
						   </ul>
						   </div>
				   </div>

				    <div class="canvas-data">
						<div class="step-content canvas-template-wrp">

							<ul>
							<li class="active">T1</li>
							<li>T2</li>
							<li>T3</li>
							<li>T4</li>
							<li>T1</li>
							<li>T2</li>
							<li>T3</li>
							<li>T4</li>
							</ul>

							</div>
						<div class="step-content canvas-products-wrp" style="height:auto;display:block;">
							<div class="panel-step-level2">
							<select name="sub_cat" id="sub_cat" class="form-control input-sm inline">
								<option value="">Select Category</option>
								<?php if(!empty($category_list)) { foreach($category_list as $val) { ?>
										 <option value="<?php echo $val['id']; ?>"><?php echo $val['name']; ?></option>
								<?php } }  ?>
							</select>
							<select id="brand_combo" name="brand_combo" class="form-control input-sm inline">
										<option value="">Store</option>
										<?php if(!empty($brand_list)){
												foreach($brand_list as $val){
											?>
												<option value="<?php echo $val['id']; ?>"><?php echo $val['user_name'] .'-'. $val['cpa_percentage'].' %'; ?></option>
											<?php } } ?>
							</select>
						</div>
							<div class="main-categories panel">
								<ul id="getVariation">
								   <!--li id="cat-1" class="cat active">Clothing</li>
								   <li id="cat-2" class="cat">Footwear</li>
								   <li id="cat-3" class="cat">Accessories</li>
								   <li id="cat-4" class="cat">Bags</li-->
							   </ul>

							    <select id="sc_variations_value_type" name="sc_variations_value_type" class="form-control input-sm inline">
									<option value="">Variations Type</option>
									
								</select>

							   <select id="sc_variations_type" name="sc_variations_type" class="form-control input-sm inline" multiple>
									<option value="">Variations</option>
									
								</select>

								<div id="show_products" class="btn btn-primary btn-sm lc_show_products"> Show Products </div>

							   </div>
							   
							   </div>

							    <div class="panel search-container">
									<span class="btn btn-primary btn-sm backBtn">Back</span>
									<input type="text" placeholder="Search by tag" id="tag_name" name="tag_name" class="form-control input-sm inline" style="width:140px;"></input>
									
									
								</div>


							   <div class="cat-content-wrp panel">
							   <div  id="cat-1-content" class="products-container cat-content cat-1-content sub-categories">

								<ul id="getVariationVal">
								   <!--li>Top</li>
								   <li>Jeans</li>
								   <li>Skirts</li>
								   <li>Kurtas</li>
								   <li>Tops</li>
								   <li>Jeans</li>
								   <li>Skirts</li>
								   <li>Kurtas</li>
								   <li>Jeans</li>
								   <li>Skirts</li>
								   <li>Kurtas</li>
								   <li>Tops</li>
								   <li>Jeans</li>
								   <li>Skirts</li>
								   <li>Kurtas</li-->

							   </ul>
							   </div>

							    <div  id="cat-2-content" class="cat-content cat-2-content sub-categories">
									 <div>Footwear</div>
								<ul>
								   <li class="active">Footwear</li>
								   <li>Footwear 1</li>
								   <li>Footwear 2</li>
								   <li>Footwear 3</li>
								   <li>Footwear 4</li>
								   <li>Footwear 5</li>
								   <li>Footwear 6</li>
								   <li>Footwear 7</li>
							   </ul>
							   </div>

							   <div  id="cat-3-content" class="cat-content cat-3-content sub-categories">
									 <div>Accessories</div>
								<ul>
								   <li class="active">Accessories</li>
								   <li>Accessories 1</li>
								   <li>Accessories 2</li>
								   <li>Accessories 3</li>
								   <li>Accessories 4</li>
								   <li>Accessories 5</li>
								   <li>Accessories 6</li>
								   <li>Accessories 7</li>
							   </ul>
							   </div>

							   <div  id="cat-4-content" class="cat-content cat-4-content sub-categories">
									 <div>Bags</div>
								<ul>
								   <li class="active">Footwear</li>
								   <li>Bags 1</li>
								   <li>Bags 2</li>
								   <li>Bags 3</li>
								   <li>Bags 4</li>
								   <li>Bags 5</li>
								   <li>Bags 6</li>
								   <li>Bags 7</li>
							   </ul>
							   </div>
							   </div>




							   </div>
						<div class="step-content canvas-effects-wrp">

						  <div  id="cat-1-content" class="cat-content cat-1-content sub-categories11 panel-step-level2 effects-cat">

							<!--	<div class="title">Effects</div>-->
									<ul>
										<li>
											<!--<div id="colorSelector"><div style="background-color: #0000ff"></div></div>-->
											<div id="colorSelector" class="input-group my-colorpicker1 colorpicker-element">
                      <label class="form-control">Bg Color</label>
                      <div class="input-group-addon">
                        <i style="background-color: rgb(0, 0, 0);"></i>
                      </div>
                    </div>
										<!--	<div id="colorSelector1" class="input-group my-colorpicker1">
														  <div class="input-group-addon" style="width:40px;">
															<i></i>
														  </div>
														</div>-->
										</li>
									   <li class="add-big-image">
										 <label class="form-control">Bg Image</label></li>
										 <li class="add-text-btn"><label class="form-control">Text</label></li>
									  <!-- <li>Texture</li>
									   <li>Frames</li>
									   -->
								   </ul>

						</div>
						<div class="cat-content-wrp panel">

							   <div class="product-container cat-content cat-1-content sub-categories11 bg-container">
									<ul>
									<?php if(!empty($sticker_list)) { foreach($sticker_list as $val) { ?>
										<li><img src="<?php echo base_url(); ?>assets/backgrounds/<?php echo $val['img_name']; ?>" /></li>
									<?php } } ?>
									</ul>
							   </div>
						</div>
						
						</div>

							<div class="step-content canvas-effects-wrp-movable">

						  <div  id="cat-1-content" class="cat-content cat-1-content sub-categories11 panel-step-level2 effects-cat">

							<!--	<div class="title">Effects</div>-->
									<ul>
										<li>
											<!--<div id="colorSelector"><div style="background-color: #0000ff"></div></div>-->
											<div id="colorSelector" class="input-group my-colorpicker1 colorpicker-element">
                      <label class="form-control">Bg Color</label>
                      <div class="input-group-addon">
                        <i style="background-color: rgb(0, 0, 0);"></i>
                      </div>
                    </div>
										<!--	<div id="colorSelector1" class="input-group my-colorpicker1">
														  <div class="input-group-addon" style="width:40px;">
															<i></i>
														  </div>
														</div>-->
										</li>
									   <li class="add-big-image">
										 <label class="form-control">Bg Image</label></li>
										 <li class="add-text-btn"><label class="form-control">Text</label></li>
									  <!-- <li>Texture</li>
									   <li>Frames</li>
									   -->
								   </ul>

						</div>
						<div class="cat-content-wrp panel">

							   <div class="product-container cat-content cat-1-content sub-categories11 bg-container-movable">
									<ul>
									<?php if(!empty($sticker_list)) { foreach($sticker_list as $val) { ?>
										<li><img src="<?php echo LIVE_SITE_URL; ?>assets/backgrounds/<?php echo $val['img_name']; ?>" /></li>
									<?php } } ?>
									</ul>
							   </div>
						</div>
						
						</div>
					</div>

			  </div>


		  </div>
	</div>

	</div>
</section>
	</div>
	<script type="text/Javascript">
		$( document ).ready(function() {
			$('.nav-item-look').addClass("active");
	    $('.nav-look-creator').addClass("active");
		});
	</script>
