<section class="content-header">
        <h1>Brand & Delivery Partner Mapping </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>
            <li><a href="#">Manage Delivery Partner</a></li>
            <li class="active">Brand & Delivery Partner Mapping</li>
          </ol>
</section>

<section class="content">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Mapping</h3>
                </div><!-- /.box-header -->

                <div class="box-body">
                   <?php echo $this->session->flashdata('feedback'); ?>
                    <form name="frmsearch" class="form-group" id="frmsearch" action="<?php echo base_url(); ?>Delivery_partner_mapping" method="post">
                        <div class="row">
                          <div class="col-md-2">
                            <select name="search_by" id="search_by" class="form-control input-sm" >
                              <option value="0">Search by</option>
                              <option value="1">Brand</option>                              
                            </select>
                          </div>
                          <div class="col-md-3">
                              <div class="input-group">
                          <input type="text" name="table_search" id="table_search" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo set_value('table_search'); ?>"/>
                            <div class="input-group-btn">
                              <button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
                            </div>
                          </div>
                          </div>
                          <div class="col-md-2">
                            <a href="<?php echo base_url(); ?>Delivery_partner_mapping" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
                          </div>
                          <div class="col-md-5">
                              <div class="pull-right">
                                <?php echo $this->pagination->create_links(); ?>
                                </div>
                          </div>
                        </div>
                    </form>

                    <form role="form" name="dp_form" id="dp_form" method="post"  action="<?php echo base_url(); ?>Delivery_partner_mapping"  enctype="multipart/form-data">
                    <div class="table-responsive">
                    <table class="table table-bordered table-striped dataTable">
                    <thead>
                      <tr>
                        <th width="70">Sr. No.</th>
                        <th width="70">Brand ID</th>
                        <th>Brand</th>

                        <?php foreach($dpartners as $val)
                              {
                        ?>
                        <th class="text-center" ><?php echo $val['delivery_partner_name']; ?></th>
                        <?php 
                              }
                        ?>
                        <th width="70"></th>
                      </tr>
                    </thead>
                    <tbody>
                       <?php $i=0;
                              foreach($brands as $value)
                              { $i++;
                        ?>
                          <tr>
                            <td><?php echo $i;?></td>
                            <td><?php echo $value['brand_id']; ?></td>
                            <td><?php echo $value['company_name']; ?></td>
                             <?php foreach($dpartners as $val)
                              {
                             ?>
                              <td class="text-center"><input type="checkbox"  name="dp[<?php echo $value['brand_id']; ?>][]" <?php if(in_array($val['id'], $mapped_ids[$value['brand_id']])){ echo 'checked';}?> value="<?php echo $val['id']; ?>"></td>
                            <?php 
                              }
                            ?>
                            
                            <td class="text-center"><button type="submit" class="btn btn-primary btn-xs" name="dpsave" value="<?php echo $value['brand_id']; ?>"  ><i class="fa fa-save"></i> Save</button></td>
                          </tr>                     
                       <?php 
                              }
                        ?>                           
                          
                      </tbody>
                      </table>

                  </div><!-- /.box-body -->

                  <!--<div class="box-footer text-right">
                    <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Save</button>
                    <button class="btn btn-default btn-sm" type="reset"><i class="fa fa-close"></i> Cancel</button>
                  </div>-->

                </form>
              </div><!-- /.box -->

</section>

<script type="text/Javascript">

$( document ).ready(function() {
	$('.nav-item-deliver_process').addClass("active");
  $('.partner_mapping').addClass("active");
});

</script>
