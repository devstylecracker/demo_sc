<?php //echo '<pre>';print_r($orders);
     // echo '<pre>';print_r($dp_orders);
//echo '<pre>';print_r($dpartners);
//echo $count_rows;
?>
<style type="text/css">
  thead,tbody{
    font-size: 12px;
  }
  .btn-suc{
    background: transparent;
    color: #00b19c;
    text-decoration: underline;
    font-size:11px;
    font-weight: 700;
  }
 tbody, .form-control
{
  font-size: 11px;
}

</style>
<section class="content-header">
  <h1>Delivery Process </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>home">Home</a></li>
      <li><a href="#">Delivery Process</a></li>
      <li class="active">Delivery Process</li>
    </ol>
</section>

<section class="content">
  <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Delivery Process- Delivery Partner</h3>
      </div><!-- /.box-header -->
      <div class="box-body">
       <?php echo $this->session->flashdata('feedback'); ?>
        <form name="frmsearch" class="form-group" id="frmsearch" action="<?php echo base_url(); ?>Delivery_process" method="post">
         <input type="hidden"  name="is_download" id="is_download" value="0" />
          <div class="row">
            <div class="col-md-2">
                <?php $search_by = set_value('search_by')=='' ? $search_by : set_value('search_by'); ?>
                <select name="search_by" id="search_by" class="form-control input-sm" >
                  <option value="0">Search by</option>
                  <option value="1" <?php echo $search_by==1 ? 'selected' : ''; ?>  >Order Id</option>
                  <option value="2" <?php echo $search_by==2 ? 'selected' : ''; ?>  >AWB No</option>
                  <option value="3" <?php echo $search_by==3 ? 'selected' : ''; ?>  >Order Status</option>
                </select>
            </div>
            <div class="col-md-3">
              <div class="input-group">
                  <input type="text" name="table_search" id="table_search" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo set_value('table_search'); ?>"/>
                <div class="input-group-btn">
                  <button class="btn btn-sm btn-default" type="submit" onclick="submitForm();" ><i class="fa fa-search"></i></button>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <a href="<?php echo base_url(); ?>Delivery_process" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
            </div>
            <div class="col-md-5">
                <?php if($this->session->userdata('role_id') != 7){ ?>
                  <button title="Download Excel" id="download_excel" class="btn btn-primary btn-sm"  onclick="downloadExcel();"><i class="fa fa-file-excel-o"></i> Download Excel</button>
                <?php } ?>
                <div class="pull-right">
                  <?php echo $this->pagination->create_links(); ?>
                </div>
            </div>                      
        </form>
      </div>
      <div>
	  
        <form role="form" name="" id="" action="" method="post" enctype="multipart/form-data">
        <div class="table-responsive">
        <table class="table table-bordered table-striped dataTable">
        <thead>
          <tr>
            <!--<th width="50">Sr. No.</th>-->
            <th width="60">Order ID</th>
           <!-- <th width="60">Item ID</th>-->
            <th >Product Name</th>
            <th width="100">Store</th>
            <th width="150">Delivery Partner<span class="text-red">*</span></th>
            <th width="200">Pickup Address<span class="text-red">*</span></th>
            <th width="200">Shipping Address<span class="text-red">*</span></th>
			<!-- added by rajesh -->
			<th width="100">Amount to be Collected</th>
			<th width="90">Payment Type</th>
			<!-- added by rajesh -->
            <th>Order Status</th>
            <th width="120">AWB</th><!-- AWB is same as AWP -->
            <th width="80">DP Charges</th>
            <th width="150">DP Status</th>
            <?php 
                if($this->session->userdata('role_id') != '6')
                {
            ?>
            <th width="70">Action</th>
            <?php
                }
            ?>
          </tr>
        </thead>
        <tbody>              
              <?php 
                    $i  = 0;
                foreach($orders as $value)
                { $i++;
                  $shippingAddress = @$dp_orders[$value['order_prd_id']]!='' ? @$dp_orders[$value['order_prd_id']]['shipping_address'] : $value['shipping_address'];
                  $pickupAddress = @$dp_orders[$value['order_prd_id']]!='' ? @$dp_orders[$value['order_prd_id']]['pickup_address'] : $value['company_address'];
              ?>
              <tr>
                <!--<td><?php echo $i; ?></td>-->
                <!--<td><?php echo $value['order_unique_no']; ?></td>-->
                <td><?php echo $value['order_id']; ?></td>
                <!--<td><?php echo $value['order_prd_id']; ?></td>-->
                <td><?php echo $value['product_name']; ?></td>
                <td><?php echo $value['company_name']; ?></td>
                <td>
                <?php                      
                         $dpmbrand = $this->dp_model->getBrandDelPart($value['brand_id']);
                          if(!empty($dpmbrand))
                          {
                            $dpart = $dpmbrand;
                          }else
                          {
                            $dpart = $dpartners;
                          }
                      
                     
                ?>
                  <select class="form-control input-sm" name="dpartner_<?php echo $value['order_prd_id']; ?>" id="dpartner_<?php echo $value['order_prd_id'];?>" <?php echo ($this->session->userdata('role_id') == '7' || $this->session->userdata('role_id') == '6') ? 'disabled':''; ?> >
                    <option value="">Select </option>
                    <?php foreach ($dpart as $val)
                          {
                    ?>
                    <option value="<?php echo $val['id']; ?>" <?php echo $dp_orders[$value['order_prd_id']]['dp_id']==$val['id'] ? 'selected': ''; ?> ><?php echo $val['delivery_partner_name'];
                    ?>       </option>
                  <?php   } ?>
                  </select>
                </td>                          
                <td><textarea class="form-control input-sm" rows="5" cols="60" name="pickupAddr_<?php echo $value['order_prd_id']; ?>" id="pickupAddr_<?php echo $value['order_prd_id']; ?>" <?php echo ($this->session->userdata('role_id') == '7' || $this->session->userdata('role_id') == '6')? 'disabled':''; ?> ><?php echo $pickupAddress; ?></textarea></td>
                <td><textarea class="form-control input-sm" rows="5" cols="60" name="shipAddr_<?php echo $value['order_prd_id']; ?>" id="shipAddr_<?php echo $value['order_prd_id']; ?>" <?php echo ($this->session->userdata('role_id') == '7' || $this->session->userdata('role_id') == '6')? 'disabled':''; ?> ><?php echo $shippingAddress; ?></textarea></td>
				<!-- added by rajesh -->
				<td><?php if($value['pay_mode'] == 1){ echo $value['product_price']; }else echo 0; ?></td>
				<td><?php if($value['pay_mode'] == 1){ echo 'COD'; }else echo 'Online'; ?></td>
				<!-- added by rajesh -->
				
              <td style="font-weight:700;"><?php echo $orderStatusList[$value['order_status']]; ?></td>
			  <span id="awbno_'<?php echo $value['order_unique_no']; ?>">
                <!--  <td> <input class="form-control input-sm" name="awp_<?php echo $value['order_prd_id']; ?>" id="awp_<?php echo $value['order_prd_id']; ?>" value="<?php echo @$dp_orders[$value['order_prd_id']]['awp_no']; ?>" <?php echo $this->session->userdata('role_id') == '6'? 'disabled':''; ?>  /></td>-->
				
                <td> 
					<?php echo @$dp_orders[$value['order_prd_id']]['awp_no']; 
						if(!empty($dp_orders[$value['order_prd_id']]['awp_no']))
						{
					?>
						<a class="btn btn-success btn-xs" onclick="trackdata('<?php echo @$dp_orders[$value['order_prd_id']]['awp_no']; ?>')" data-toggle="modal" data-target="#trackDataModal">Track Data</a>
					<?php
						}
					?>
				</td>
				
                <td> <input class="form-control input-sm" name="dpCharges_<?php echo $value['order_prd_id']; ?>" id="dpCharges_<?php echo $value['order_prd_id']; ?>" value="<?php echo @$dp_orders[$value['order_prd_id']]['dpCharges']; ?>" <?php echo ( $this->session->userdata('role_id') == '6')? 'disabled':''; ?> /></td>
                <td>
                    <select class="form-control input-sm" name="dpstatus_<?php echo $value['order_prd_id']; ?>" id="dpstatus_<?php echo $value['order_prd_id']; ?>" onchange="changeDpStatus('<?php echo $value['order_prd_id']; ?>');" <?php echo $this->session->userdata('role_id') == '6'? 'disabled':''; ?> >
                      <option value="1" <?php echo @$dp_orders[$value['order_prd_id']]['dp_status']=='1'? 'selected': ''; ?> >Pending </option>
                      <option value="5" <?php echo @$dp_orders[$value['order_prd_id']]['dp_status']=='5'? 'selected': ''; ?> >Confirmed </option>
                      <option value="2" <?php echo @$dp_orders[$value['order_prd_id']]['dp_status']=='2'? 'selected': ''; ?> >Dispatched </option>
                      <option value="3" <?php echo @$dp_orders[$value['order_prd_id']]['dp_status']=='3'? 'selected': ''; ?> >Delivered </option>
                      <option value="4" <?php echo @$dp_orders[$value['order_prd_id']]['dp_status']=='4'? 'selected': ''; ?> >Return </option>
                    </select>
                </td><!-- type="submit"-->
                <td class="text-center">
				<!-- For Generation of AWB number -->  
               <!--<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#addAwb" onclick="get_aws_no('<?php echo $value['order_unique_no'];?>')"> Generate AWB</button>-->
               <button type="button" class="btn btn-suc btn-xs" data-toggle="modal" data-target="#addAwb" onclick="addAwb('<?php echo $value['order_unique_no'];?>')"><i class="fa fa-edit"></i> Generate AWB</button>
			   
                <?php 
                    if($this->session->userdata('role_id') != '6')
                    {
                      if($value['doid']!='') 
                      {
                ?>
                <div  class="btn btn-suc btn-xs" onclick="save_dp_order('<?php echo $value['order_prd_id']; ?>','<?php echo $value['order_id']; ?>','<?php echo $value['order_unique_no']; ?>','<?php echo $value['product_id']; ?>','<?php echo $value['brand_id']; ?>');" ><i class="fa fa-save"></i> Update</div>
                <div id="message_<?php echo $value['order_prd_id']; ?>" >  </div><br>
                
               
                <?php
                      }
					  else
                      {
                ?>
                <div  class="btn btn-suc btn-xs"  name="add_<?php echo $value['order_prd_id']; ?>" id="add_<?php echo $value['order_prd_id']; ?>"  onclick="save_dp_order('<?php echo $value['order_prd_id']; ?>','<?php echo $value['order_id']; ?>','<?php echo $value['order_unique_no']; ?>','<?php echo $value['product_id']; ?>','<?php echo $value['brand_id']; ?>');" ><i class="fa fa-save"></i> Add</div>
                <div id="message_<?php echo $value['order_prd_id']; ?>" >  </div>                    
                <?php
                      }
                  }
                ?>
                 <input type="hidden" id="doid_<?php echo $value['order_prd_id']; ?>" name="doid_<?php echo $value['order_prd_id']; ?>" value="<?php echo $value['doid']; ?>" /> 
                 <input type="hidden" id="emailSent_<?php echo $value['order_prd_id']; ?>" name="emailSent_<?php echo $value['order_prd_id']; ?>" value="0" />            
               
                </td>                            
              </tr>
              <?php
                }
              ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
      <div class="box-footer text-right">
      </div>
    </form>
  </div><!-- /.box -->
</section>

 
			   
		<div class="modal fade" id="addAwb">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Generate AWB</h4>
              </div>
              <div class="modal-body">
			
         <!-- <form class="form-horizontal" method="post" action="" id="track_awb_data" name="track_awb_data" enctype="multipart/form-data">-->
        
				<div class="box-body">
				<div class="form-group" id="in_data">
				  <div class="col-sm-6">
				  <input name="box_weight" type="text" class="form-control" id="box_weight" placeholder="Box weight">
				  <span class="text-danger box_weight_empty"></span>
				  </div>
				  <div class="form-group">
				  <div class="col-sm-6">
				 <select name="delivery_type" id="delivery_type" class="form-control">
				  <option value="empty">Select Delivery Type</option>
				  <option value="Express">Express</option>
				  <option value="Economy">Economy</option>
				</select>
				  <span class="text-danger delivery_type_empty"></span>
				  </div>
				</div>
				</div>
			   <div id="trackLoadeawb"></div>
			   <span id="awbno"></span>
				</div>
				<!-- /.box-footer -->
				 <div class="modal-footer">
				<!--<button type="submit" id="track_submit" name="track_submit" class="btn btn-primary">Generate</button>-->
				<a class="btn btn-success btn-xs" onclick="get_aws_no()" >Generate AWB</a>
				<input type="hidden" name="awb_hidden_id" id="awb_hidden_id">
				</div>
			  <!--</form>-->
				  </div>
				 
				</div>
				<!-- /.modal-content -->
			  </div>
			  <!-- /.modal-dialog -->
			</div>
			<!-- /.modal --> 

			   
			   



	<div class="modal fade" id="trackDataModal" role="dialog">
		<div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Track Detail</h4>
		</div>
		<div class="modal-body" id="trackingModalData">
			<div id="trackLoade"></div>
		</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
<!--<script src="<?php echo base_url('assets/js/jquery.validate.min.js'); ?>"></script>-->
<script>

function addAwb(unique_number)
{
	//alert(unique_number);
	$('#awb_hidden_id').val(unique_number);
}

	function get_aws_no()
		{
			//alert(order_unique_number);
			var order_unique_number = $('#awb_hidden_id').val();
			var boxWeight 		= $('#box_weight').val();
			var deliveryType 	= $('#delivery_type').val();
			if(boxWeight == ''){
				$('.box_weight_empty').html('Please Enter Box Weight.');
			}
			else if(deliveryType == 'empty'){
				$('.delivery_type_empty').html('Please Select Delivery Type.');
			}
			else{
				$('#trackLoadeawb').html('<img src="https://i.imgur.com/HvS8yF1.gif">');
				$.ajax({
					type:'POST',
					url: "<?php echo base_url(); ?>delivery_process/get_awb_no",
					data:{'order_unique_number':order_unique_number,'boxWeight':boxWeight,'deliveryType':deliveryType},
					dataType: "json",
					cache:false,
					success:function(data){  
					//alert(data.awp_no);
					$('#trackLoadeawb').css("display", "none");  //<--- hide loader
					$('#in_data').css("display", "none");  //<--- hide loader
					$('#awbno').html('AWP No. for this order '+data.awp_no);
					setTimeout(function() {$('#addAwb').modal('hide');}, 4000);
					location.reload();
					},
					error: function(data){
						console.log("error");
						console.log(data);
					}
				});
			}
		}
		
		function trackdata(awbno)
		{			
			$('#trackLoade').html('<img src="https://i.imgur.com/HvS8yF1.gif">');
			$.ajax({
				type:'POST',
				url: "<?php echo base_url(); ?>delivery_process/add_tracking_data",
				data:{'awbno':awbno},
				dataType: "html",
				cache:false,
				success:function(data){
					$("#trackDataModal").modal('show');
					$('#trackLoade').css("display", "none");  //<--- hide loader					
					$('#trackingModalData').html(data);
				},
				error: function(data){
					console.log("error");
				}
			});
		}
	
</script>

<script type="text/Javascript">

$( document ).ready(function() {
  $("body").addClass('sidebar-collapse');
	$('.nav-item-deliver_process').addClass("active");
  $('.nav_delivery_process').addClass("active");

});

function save_dp_order(orderPrdId,orderId,orderUnqId,productId,brandId){  
  var dpOrderArray = [];
  if(orderPrdId!='')
  {
    var dpartner = $('#dpartner_'+orderPrdId).val();
    var pickupAddr = $('#pickupAddr_'+orderPrdId).val();
    var shipAddr = $('#shipAddr_'+orderPrdId).val();
    var awp = $('#awp_'+orderPrdId).val();
    var dpCharges = $('#dpCharges_'+orderPrdId).val();
    var dpstatus = $('#dpstatus_'+orderPrdId).val();
    var doId = $('#doid_'+orderPrdId).val();   
  }


  /*var row_count = $('#row_count'+product_id).val();
  var product_size = ''; var product_qty = '';
  var arrProductSizeAndCount = [];
  for(var i = 1;i<=row_count;i++){
    if(($('#product_size'+product_id+'_'+i).val() != null) && ($('#product_qty'+product_id+'_'+i).val() != null)){
      $('#product_size'+product_id+'_'+i).val();
      $('#product_qty'+product_id+'_'+i).val();
      $('#product_sku'+product_id+'_'+i).val();
      arrProductSizeAndCount.push([$('#product_size'+product_id+'_'+i).val(),$('#product_qty'+product_id+'_'+i).val(),$('#product_sku'+product_id+'_'+i).val()]);
    }
  }
  //console.log(arrProductSizeAndCount);
  var myJsonString = JSON.stringify(arrProductSizeAndCount);

  $.ajax({
            url: "<?php echo base_url(); ?>productinventory_new/save_product_size",
            type: 'POST',
            data: {'product_id':product_id,'product_info':myJsonString},
            cache :true,
            async: false,
            success: function(response) {
                $('#message'+product_id).html('Save Successfully');
            },
            error: function(xhr) {

            }
  });*/
  if(dpartner=='')
  {
    $('#message_'+orderPrdId).html('<span class="text-red">Select Delivery Partner</span>');
  }else if(pickupAddr=='')
  {
    $('#message_'+orderPrdId).html('<span class="text-red">Pickup Address cannot be blank</span>');
  }else if(shipAddr=='')
  {
    $('#message_'+orderPrdId).html('<span class="text-red">Shipping Address cannot be blank</span>');
  }/*else if(awp=='')
  {
    $('#message_'+orderPrdId).html('<span class="text-red">AWP no. cannot be blank</span>');
  }*/else if(dpstatus=='')
  {
    $('#message_'+orderPrdId).html('<span class="text-red">Select status</span>');
  }else
  {
     var result = window.confirm('Are you sure?');
     if(result)
     {
      var emailSent = $("#emailSent_"+orderPrdId).val();      
      $.ajax({
                url : '<?php echo base_url(); ?>Delivery_process/DpOrderSave',
                type : 'POST',
                data : {'orderPrdId': orderPrdId,'dpartner':dpartner,'pickupAddr':pickupAddr,'shipAddr':shipAddr,'awp':awp,'dpCharges':dpCharges,'dpstatus':dpstatus, 'orderUnqId':orderUnqId,'orderId': orderId ,'productId': productId,'brandId':brandId,'doid':doId,'emailSent':emailSent },
                cache : true,
                async : false,
                success : function(response){                          
                  if(response == 1)
                  {
                    $('#message_'+orderPrdId).html('<span class="text-green">Record Save Successfully</span>');
                    $('#add_'+orderPrdId).attr("disabled","disabled");
                  }else if(response == 2)
                  {
                    $('#message_'+orderPrdId).html('<span class="text-green">Email Sent and Record Save Successfully  </span>');
                  }else if(response == 4)
                  {
                    $('#message_'+orderPrdId).html('<span class="text-green">Enter AWP Number </span>');           
                  }else
                  {
                    $('#message_'+orderPrdId).html('<span class="text-red">Error</span>');
                  }
                  
                },
                error: function(xhr){

                }
          });
      }
    }
  }

  function changeDpStatus(ordprdId)
  {    
    if(ordprdId!='')
    {      
       $("#emailSent_"+ordprdId).val(1);      
    }   
  }

  function downloadExcel()
  {
       var search_by = '';
       var table_search = '';
       $("#is_download").val('1');
       search_by = $('#search_by').val();
       search_by = search_by.trim();
       table_search = $('#table_search').val();
       $('#table_search').removeAttr('required');
       table_search = table_search.trim();
       var is_download = $("#is_download").val();

       //encode_table_search = Base64.encode(table_search);
       //encode_table_search = <?php echo utf8_encode(table_search); ?>;

       if(search_by == 6)
       {
          encode_table_search = window.btoa(table_search);
       }else if(search_by == 8)
       {
          var dateFrom = $("#date_from").val();
          var dateTo = $("#date_to").val();

          var date_search = dateFrom+','+dateTo;
          encode_table_search = window.btoa(date_search);
       }
       else
       {
          encode_table_search = table_search;
       }
       if(!encode_table_search){ encode_table_search = '0'; }

       //alert(encode_table_search);
       //var _href = '<?php echo base_url(); ?>'+'View_orders_payment/exportExcel/downloadExcel';

       var newhref = '<?php echo base_url(); ?>'+'Delivery_process/exportExcel/downloadExcel'+'/'+search_by+'/'+encode_table_search+'/'+is_download;
       //alert(newhref);

       $('#frmsearch').attr('action',newhref);

       //location.reload();
       /*var link = document.createElement('a');
       link.href = newhref;
       document.body.appendChild(link);
       link.click();*/
  }


</script>
