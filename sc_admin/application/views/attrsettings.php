<section class="content">
<section class="content-header">
        <h1>Manage Attributes Values  - <?php echo @$parent_attr_name; ?></h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>">Home</a></li>
            <li><a href="<?php echo base_url();?>attributes">Manage Attributes</a></li>
            <li class="active">Manage Attributes Values - <?php echo @$parent_attr_name; ?></li>
          </ol>
</section>
<section class="content">
	<div class="row">
	 <div class="col-md-4">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">
				 	<?php if($this->uri->segment(4) > 0){ ?>
						Edit <?php echo @$parent_attr_name; ?> value 
					<?php }else{ ?>
						Add New <?php echo @$parent_attr_name; ?> value
					<?php } ?>
				</h3>
		</div><!-- /.box-header -->
<div class="box-body">
<?php if($this->uri->segment(4) > 0){ ?>
	<form name="frmcategory" id="frmcategory" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>attrsettings/attrsettings_edit/<?php echo $this->uri->segment(3); ?>/<?php echo $this->uri->segment(4); ?>">
<?php }else{ ?>
	<form name="frmcategory" id="frmcategory" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>attrsettings/index/<?php echo $this->uri->segment(3); ?>">
<?php } ?>

						<div class="form-group">
							<label class="control-label">Name
							</label>
							<input type="text" placeholder="" name="attr_value_name" id="attr_value_name" value="<?php if(isset($cat_data_data[0]['attr_value_name'])){ echo @$cat_data_data[0]['attr_value_name']; }else{ echo @$_POST['attr_value_name']; } ?>" class="form-control" onblur="genarate_slug();"> <span class="text-red"><?php echo form_error('attr_value_name'); ?></span>
							<p class="help-block">The name is how it appears on your site.</p>
                    	</div>

                    	<div class="form-group">
							<label class="control-label">Slug
							</label>
							<input type="text" placeholder="" name="attr_value_slug" id="attr_value_slug" value="<?php if(isset($cat_data_data[0]['attr_value_slug'])){ echo @$cat_data_data[0]['attr_value_slug']; }else{ echo @$_POST['attr_value_slug']; } ?>" class="form-control" >
							<p class="help-block">The "slug" is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.</p>
                    	</div>

                    	<div class="form-group">
							<label class="control-label">Description
							</label>
							<textarea name="attr_value_content" id="attr_value_content" class="form-control" rows="3">
							<?php if(isset($cat_data_data[0]['attr_value_content'])){ echo @$cat_data_data[0]['attr_value_content']; }else{ echo @$_POST['attr_value_content']; } ?>
							</textarea>
						</div>
						
						<div class="form-group">
							<label class="control-label">Colour Hex value</label>
							<input type="text" placeholder="" name="attr_hashtag" id="attr_hashtag" value="<?php if(isset($cat_data_data[0]['attr_hashtag'])){ echo @$cat_data_data[0]['attr_hashtag']; }else{ echo @$_POST['attr_hashtag']; } ?>" class="form-control" >
							<p class="help-block">Add Colour Hex value.</p>
                    	</div>

								<div class="form-group">
							<label class="control-label">Display
							</label>
							<select name="display" id="display" class="form-control chosen-select">
							<?php if(isset($cat_data_data[0]['display']) && $cat_data_data[0]['display'] == 0){ ?>
								<option value="0" selected>Both</option>
							<?php }else{ ?>
								<option value="0">Both</option>
							<?php } ?>

							<?php if(isset($cat_data_data[0]['display']) && $cat_data_data[0]['display'] == 1){ ?>
								<option value="1" selected>Web</option>
							<?php }else{ ?>
								<option value="1">Web</option>
							<?php } ?>

							<?php if(isset($cat_data_data[0]['display']) && $cat_data_data[0]['display'] == 2){ ?>
								<option value="2" selected>Backend</option>
							<?php }else{ ?>
								<option value="2">Backend</option>
							<?php } ?>
							</select>
						</div>


						<input type="hidden" name="attribute_id" id="attribute_id" value="<?php echo $this->uri->segment(3); ?>">
                    	<div class="form-group">

                    	<?php if($this->uri->segment(4) > 0){ ?>
							<button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Edit Attribute</button>
						<?php }else{ ?>
							<button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Add New Attribute</button>
						<?php } ?>

                    	</div>

					</form>
				
</div>
</div>
</div>
 <div class="col-md-8">
	 <div class="box">
		 <div class="box-header">
			 <h3 class="box-title">
					View Attributes Value
				 </h3>
				 <div class="pull-right">
				 <div class="input-group">
						 <input name="search_text" id="search_text" value=""  placeholder="Search" class="form-control input-sm pull-right" required>
						 <div class="input-group-btn">
							 <button type="button" name="submit" id="submit" value="Search" onclick="search_cat();" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
						 </div>
					 </div>
				 </div>
		 </div><!-- /.box-header -->
	<div class="box-body">

	<div class="table-responsive">
		<table class="table table-bordered table-striped dataTable" id="datatable">
								<thead>
								<tr>
										<th>ID</th>
										<th>Name</th>
										<th>Slug</th>
										<th>Display</th>
										<th>Count</th>
										<th>Action</th>

								</tr>
								</thead>
			<tbody id="category_list">
			<?php if(!empty($cat_data)){
				foreach($cat_data as $val){
			?>
			<tr>
				<td><?php echo $val['id']; ?></td>
				<td><?php echo $val['attr_value_name']; ?></td>
				<td><?php echo $val['attr_value_slug']; ?></td>
				<td><?php echo $val['count']; ?></td>
				<td>
					<select name="display" id="display_<?php echo $val['id']; ?>" class="form-control" onchange="change_attr_value_display('<?php echo $val['id']; ?>');">
							<?php if(isset($val['display']) && $val['display'] == 0){ ?>
								<option value="0" selected>Both</option>
							<?php }else{ ?>
								<option value="0">Both</option>
							<?php } ?>

							<?php if(isset($val['display']) && $val['display'] == 1){ ?>
								<option value="1" selected>Web</option>
							<?php }else{ ?>
								<option value="1">Web</option>
							<?php } ?>

							<?php if(isset($val['display']) && $val['display'] == 2){ ?>
								<option value="2" selected>Backend</option>
							<?php }else{ ?>
								<option value="2">Backend</option>
							<?php } ?>
					</select>
				</td>
				<td>
					<a href="<?php echo base_url(); ?>Attrsettings/attrsettings_edit/<?php echo $val['attribute_id']; ?>/<?php echo $val['id']; ?>"><button class="btn btn-primary btn-xs editProd" type="button" ><i class="fa fa-edit"></i> Edit</button></a>
					<button class="btn btn-xs btn-primary btn-delete" type="button" onclick="delete_category(<?php echo $val['id']; ?>,<?php echo $val['attribute_id']; ?>);"><i class="fa fa-trash-o"></i> Delete</button>
				</td>
			</tr>
			<?php } } ?>
			</tbody>
		</table>
	</div>
</div>
</div>
</div>
</div>

</section>
<script type="text/javascript">
	
	function genarate_slug(){
		var category_name = $('#attr_value_name').val();
		var slug = category_name.replace(/[^a-zA-Z0-9]/g, "-");
		$('#attr_value_slug').val(slug.toLowerCase());
	}
	function delete_category(id,aid){ 
		var result = confirm("Want to delete?");
		if (result) {
			$.ajax({
				url: "<?php echo base_url(); ?>Attrsettings/check_product_count",
				type: 'POST',
				data: { id : id },
				cache :true,
				async: true,
				success: function(response) {
					/*if(response > 0){
						alert('Already have products in to the category still you want to delete the category');
					}else{*/
						$.ajax({
						url: "<?php echo base_url(); ?>Attrsettings/delete_category",
						type: 'POST',
						data: { id : id },
						cache :true,
						async: true,
						success: function(response) {
							window.location="<?php echo base_url(); ?>Attrsettings/index/"+aid;
						},
						error: function(xhr) {
							alert('Something goes wrong');
							hide_cart_loader();
						}
					});
				/*}*/

				},
				error: function(xhr) {
					alert('Something goes wrong');
					hide_cart_loader();
				}
			});
		}
	}	

	function search_cat(){
		var search_text = $('#search_text').val();
		$.ajax({
			url: "<?php echo base_url(); ?>attrsettings/search_category/<?php echo $this->uri->segment(3); ?>/<?php echo $this->uri->segment(4); ?>",
			type: 'POST',
			data: { search_text : search_text },
			cache :true,
			async: true,
				success: function(response) {
					$('#category_list').html('');
					$('#category_list').html(response);
				},
				error: function(xhr) {
					alert('Something goes wrong');
					hide_cart_loader();
			}
		});

	}
	function change_attr_value_display(a){
		var display_val = $('#display_'+a).val();
		var attr_val = a;
		$.ajax({
			url: "<?php echo base_url(); ?>Attrsettings/update_display",
			type: 'POST',
			data: { 'display_val' : display_val,'attr_val':attr_val },
			cache :true,
			async: true,
			success: function(response) {
				alert('done');
			},
			error: function(xhr) {
				alert('Something goes wrong');
			}
		});
	}
</script>