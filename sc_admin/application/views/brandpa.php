<section class="content-header">
        <h1>Brands - Personalisation Mapping</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>">Home</a></li>
            <li class="active">Brands - Personalisation Mapping</li>
          </ol>
</section>

<section class="content">
	<div class="row">
	 <div class="col-md-12" style="margin-bottom:80px;">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">
				 	Personalisation Mapping
				</h3>
		</div><!-- /.box-header -->
<div class="box-body">
<div class="row">
	 <div class="col-md-6">
	<div class="form-group">
			<label class="control-label">Brands</label>
			<select name="brands" id="brands" class="form-control chosen-select" tabindex="7" onchange="get_info();">
				<option value="">Select</option>
				<?php if(!empty($brands_data)){
					foreach($brands_data as $val){
						?>
							<option value="<?php echo $val['user_id']; ?>"><?php echo $val['company_name']; ?></option>
						<?php
					}

					} ?>
			</select>
			<p class="help-block">Select Paid Brand From Drop Down</p>
			</div>


			<div class="form-group">
			<label class="control-label">Category</label>
			<select name="category[]" id="category" class="form-control chosen-select" tabindex="7" multiple="">
				<option value="">Select</option>
				<?php if(!empty($cat_data)){
					foreach($cat_data as $val){
						echo $val;
					}

					} ?>
			</select>
			<p class="help-block">Select Paid Brand From Drop Down</p>
			</div>

			<div class="form-group">
						<label class="control-label">Personality</label>
						<select name="bucket[]" id="bucket" class="form-control chosen-select" tabindex="7" multiple="">
							
							<?php if(!empty($bucket)){
								foreach($bucket as $val){ 
									if(in_array($val['id'], $bucket_data)){ ?>
									<option value="<?php echo $val['id']; ?>" selected><?php echo $val['body_shape'].' '.$val['style'].' '.$val['work_style'].' '.$val['personal_style']; ?></option>

									<?php }else{
									?>
									<option value="<?php echo $val['id']; ?>"><?php echo $val['body_shape'].' '.$val['style'].' '.$val['work_style'].' '.$val['personal_style']; ?></option>
								<?php } }

								} ?>
						</select>
					<p class="help-block">Assign Brand to the personality (Totally optional).</p>
					</div>

					<div class="form-group">
						<label class="control-label">Age</label>
						<select name="age[]" id="age" class="form-control chosen-select" tabindex="7" multiple="">
							
							<?php if(!empty($age)){
								foreach($age as $val){ 
								if(in_array($val['id'], $age_data)){ ?>
									<option value="<?php echo $val['id']; ?>" selected><?php echo $val['answer']; ?></option>
								<?php }else{ ?>
									<option value="<?php echo $val['id']; ?>"><?php echo $val['answer']; ?></option>
								<?php } }

								} ?>
						</select>
					<p class="help-block">Assign Brand to the age (Totally optional).</p>
					</div>

					<div class="form-group">
						<label class="control-label">Budget</label>
						<select name="budget[]" id="budget" class="form-control chosen-select" tabindex="7" multiple="" >
							
							<?php if(!empty($budget)){
								foreach($budget as $val){ 
									if(in_array($val['id'], $budget_data)){
									?>
									<option value="<?php echo $val['id']; ?>" selected><?php echo $val['answer']; ?></option>
								<?php }else{ ?>
									<option value="<?php echo $val['id']; ?>"><?php echo $val['answer']; ?></option>
								<?php } }

								} ?>
						</select>
					<p class="help-block">Assign Brand to the budget (Totally optional).</p>
					</div>

					<div class="form-group">
							<button class="btn btn-primary btn-sm" type="button" onclick="save_brands_data();"><i class="fa fa-save"></i> Assign</button>

							</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<script type="text/javascript">
		function get_info(){
			var brand_info = $('#brands').val();
			//$('.page-overlay').show();
			$.ajax({
				url: "<?php echo base_url(); ?>brandpa/get_brands_data",
				type: 'POST',
				data: { 'id' : brand_info },
				async: true,
				success: function(response,data) {
					var obj = jQuery.parseJSON(response);

					jQuery("#category").val(obj.category_data);
					$('#category').trigger("chosen:updated");

					jQuery("#bucket").val(obj.bucket_data);
					$('#bucket').trigger("chosen:updated");
					
					jQuery("#age").val(obj.age_data);
					$('#age').trigger("chosen:updated");
					
					jQuery("#budget").val(obj.budget_data);
					$('#budget').trigger("chosen:updated");

				}
			});

		}

		function save_brands_data(){
			var brand_info = $('#brands').val();
			$('.page-overlay').show();
			if(!brand_info){
				alert('Please select the brands');
			}else{
				var category_data = $('#category').val();
				var bucket_data = $('#bucket').val();
				var age_data = $('#age').val();
				var budget_data = $('#budget').val();


				$.ajax({
					url: "<?php echo base_url(); ?>brandpa/save_brands_data",
					type: 'POST',
					data: { 'brand_info' : brand_info,'age_data':age_data,'budget_data':budget_data,'bucket_data':bucket_data,'category_data':category_data },
					cache :true,
					async: true,
					success: function(response) {
						alert('Updated Successfullys')
					}
				});

			}
			$('.page-overlay').hide();
		}
$(document).ready(function(){
	 $('.nav-item-category').addClass("active");
    // $('.nav-banners-list').addClass("active");
		$('.nav-manage-cat1').addClass("active");
});
</script>