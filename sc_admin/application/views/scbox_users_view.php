<?php //echo"<pre>"; print_r($scboxuser);
$age_question = 0;
	 $question_ans = array();
	 if(!empty($pa_data_extra)){
		foreach($pa_data_extra as $val){
			 $question_ans[$val['question_id']] = $val['answer'];
		}
	 }    
   //echo"<pre>"; print_r($pa_data);
 
?>
<section class="content-header">
  <h1>Scbox User</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>/websiteusers">Manage Scbox Users</a></li>
    <li class="active">Scbox User</li>
  </ol>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">User Info</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="datatable">
              <tr>
                <th style="width:180px;">ID</th>
                <td><?php echo ($scboxuser['user_id']!='') ? $scboxuser['user_id'] : $this->uri->segment(3); ?></td>
              </tr>
              <tr>
                <th>Username</th>
                <td><?php echo $scboxuser['user_name']; ?></td>
              </tr>
              <tr>
                <th>Date of Birth</th>
                <td><?php echo @$scboxuser['user_dob']; ?></td>
              </tr>
              <tr>
                <th>Email</th>
                <td><?php echo $scboxuser['user_emailid']; ?></td>
              </tr>
              <tr>
                <th>Mobile Number</th>
                <td><?php echo $scboxuser['user_mobile']; ?><?php //echo $user_data[0]['contact_no']; ?></td>
              </tr>
              <tr>
                <th>Shipping Address</th>
                <td><?php echo @$scboxuser['user_shipaddress']; ?></td>
              </tr>
              <tr>
                <th>Pincode</th>
                <td><?php echo @$scboxuser['user_pincode']; ?></td>
              </tr>
              <tr>
                <th>City</th>
                <td><?php echo @$scboxuser['user_city']; ?></td>
              </tr>
              <tr>
                <th>State</th>
                <td><?php echo @$scboxuser['user_state_name']; ?></td>
              </tr>
               <tr>
                <th>User Likes</th>
                <td><?php echo @$scboxuser['user_likes']; ?></td>
              </tr>
               <tr>
                <th>User Dislikes</th>
                <td><?php echo @$scboxuser['user_dislikes']; ?></td>
              </tr>
               <tr>
                <th>User Profession</th>
                <td><?php echo @$scboxuser['user_profession']; ?></td>
              </tr>
               <tr>
                <th>User Occassion</th>
                <td><?php echo @$scboxuser['user_occassion']; ?></td>
              </tr>
               <tr>
                <th>User Wardrobe</th>
                <td><?php echo @$scboxuser['user_wardrobe']; ?></td>
              </tr>
               <tr>
                <th>User Willingness to experiment</th>
                <td><?php echo (@$scboxuser['user_willtoexpmnt']==1) ? 'Yes' : 'No'; ?></td>
              </tr>
              <tr>
                <th>Role</th>
                <td><?php echo 'Website User'; ?></td>
              </tr>
              <tr>
                <th>Gender</th>
                <td><?php 
                    if($scboxuser['user_gender']==2)
                    {
					           $age_question=17;
                      echo 'Male';
                    }else
                    {
					           $age_question=8;
                      echo 'Female';
                    }
                 ?></td>
              </tr>
              <tr>
                <th>SC Box Mobile Number</th>
                <td><?php echo $sc_box_mobile_number[0]['meta_value']; ?></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">PA Info</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="datatable">
            <?php 
                if(!empty($pa_data)){
            					foreach($pa_data as $val){
                        ?>
            			<?php if($val['object_meta_key'] == 'user_body_shape'){ ?>
                          <tr>
                            <th style="width:180px;">Body Shape</th>
                            <td><?php echo $val['answer']; ?></td>
                          </tr>
            			<?php } ?>
            			<?php if($val['object_meta_key'] == 'user_style_p1'){ ?>
                          <tr>
                            <th>Personal Style Pref 1</th>
                            <td><?php echo $val['answer']; ?></td>
                          </tr>
            			<?php } ?>
            			<?php if($val['object_meta_key'] == 'user_style_p2'){ ?>
            			   <tr>
                            <th>Personal Style Pref 2</th>
                            <td><?php echo $val['answer']; ?></td>
                          </tr>
            			<?php } ?>
            			<?php if($val['object_meta_key'] == 'user_style_p3'){ ?>
            			   <tr>
                            <th>Personal Style Pref 3</th>
                            <td><?php echo $val['answer']; ?></td>
                          </tr>
            			<?php } ?>
          <?php
                }//foreach
			       }//if
          ?>		  
			<tr>
			<th>Age Range</th>
			<td><?php if(isset($question_ans[$age_question])){ echo $question_ans[$age_question]; } ?></td>
			</tr>

          <?php 
               if($scboxuser['user_gender']==2)
              {
          ?>
                <tr>
                  <th>Size Top</th>
                  <td><?php echo @$scboxuser['user_sizetop_name']; ?></td>
                </tr>
                <tr>
                  <th>Size Bottom</th>
                  <td><?php echo @$scboxuser['user_sizebottom_name']; ?></td>
                </tr>
                <tr>
                  <th>Size Footwear</th>
                  <td><?php echo @$scboxuser['user_sizefoot_name']; ?></td>
                </tr>          
          <?php  
             }else if($scboxuser['user_gender']==1)
             {
          ?>
               <tr>
                  <th>Size Top</th>
                  <td><?php echo @$scboxuser['user_sizetop_name']; ?></td>
                </tr>
                <tr>
                  <th>Size Bottom</th>
                  <td><?php echo @$scboxuser['user_sizebottom_name']; ?></td>
                </tr>
                <tr>
                  <th>Size Footwear</th>
                  <td><?php echo @$scboxuser['user_sizefoot_name']; ?></td>
                </tr> 
                <tr>
                  <th>Size Band</th>
                  <td><?php echo @$scboxuser['user_sizeband_name']; ?></td>
                </tr> 
                <tr>
                  <th>Size Cup</th>
                  <td><?php echo @$scboxuser['user_sizecup_name']; ?></td>
                </tr>          
          <?php  
             }
          ?>
          <tr>
            <th>Skintone</th>
            <td><?php echo @$scboxuser['user_skintone_name'].'- ('.@$scboxuser['user_skintone'].')'; ?></td>
          </tr>
          <tr>
            <th>Colors</th>
            <td><?php echo ltrim(@$scboxuser['user_colors_name'],','); ?></td>
          </tr>
          <tr>
            <th>Prints</th>
            <td><?php echo ltrim(@$scboxuser['user_prints_name'],','); ?></td>
          </tr>
			
			<tr>
			<th>Selected Brands</th>
			<td><?php echo $brand_list; ?></td>
			</tr>
      <tr>
      <th>Brands preferences</th>
      <td><?php echo $scboxuser['scbox_brand_list']; ?></td>
      </tr>
			<tr><td></td><td>&nbsp;</td></tr>
            </table>
          </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
    
    <div class="col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Extra Info</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="datatable">
              <tr>
                <th style="width:180px;">First Login</th>
                <!-- <td><?php //echo $extra_info[0]['first_visit']?></td> -->
                <td><?php echo $extra_info[0]['login_time']?></td>
              </tr>
              <tr>
                <th style="width:180px;">Returning Count</th>
                <!-- <td><?php //echo $extra_info[0]['visit_count']?></td> -->
                <td><?php echo $visit_count_user[0]['visit_count']?></td>
              </tr>
              <tr>
                <th style="width:180px;">Last Login</th>
                <td><?php echo $last_login[0]['login_time']; ?></td>
              </tr>             
               <tr>
                <th style="width:180px;">Total Product Click</th>
                <td><?php echo $total_product_click_count[0]['count']; ?> <a href="<?php echo base_url(); ?>websiteusers/user_look/product/<?php echo $this->uri->segment(3); ?>" target="_blank"><?php echo "View Details" ?></a></td>
              </tr>           
            </table>
          </div>
        </div>
      </div>
     </div>




    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Wordrobe</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
				 <ul class="user-wardrobe-images">
				<?php if(!empty($wordrobe)) {
				foreach($wordrobe as $val){
				?>
				<li>
				  <!-- <img src="<?php echo base_url(); ?>assets/wardrobes/thumb/<?php //echo $val['image_name']; ?>" alt=""></img> -->
          <a href="<?php echo base_url(); ?>assets/wardrobes/<?php echo $val['image_name']; ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/wardrobes/thumb/<?php echo $val['image_name']; ?>" alt=""></img></a>
				</li>
				<?php }} else{ ?>
				<li style="width:300px; text-align:left;"><?php echo 'Data not found.'; ?></li>
				<?php } ?>
      </ul>
              </div>

        </div><!-- /.box-body -->
        <div>
          <a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>websiteusers"><i class="fa fa-reply"></i> Back</a>
        </div>
      </div><!-- /.box -->
    </div>

  </section>
  <script type="text/Javascript">
$( document ).ready(function() {
	$('.nav-item-user').addClass("active");
  $('.nav-website-user').addClass("active");
});
</script>
