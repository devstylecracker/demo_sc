<script src="<?php echo base_url(); ?>assets/plugins/sortable/jquery.sortable.min.js"></script>

<section class="content-header">
        <h1>Manage Product Size Order</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>">Home</a></li>
            <li class="active">Manage Product Size Order</li>
          </ol>
</section>

<section class="content">
	<div class="row">
	 <div class="col-md-4">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">
				 	<input type="button" class="btn btn-primary btn-sm change_order" name="change_order" id="change_order" value="Save">
				</h3>
		</div><!-- /.box-header -->
		<div class="box-body">
			<ul id="sortable" class="list-group">
			<?php 
				if(!empty($sizes)){
					foreach ($sizes as $key => $value) {
						?>
					<li class="list-group-item" id="<?php echo $value['id']; ?>"> <i class="fa fa-arrows"></i> &nbsp;<b><?php echo $value['size_text']; ?></b></li>
						<?php
					}
				}
			?>
			</ul>

			<input type="button" class="btn btn-primary btn-sm change_order" name="change_order" id="change_order1" value="Save">
		</div>
</div>
</div>
</div>
</div>

</section>
<style type="text/css">
	ul li{list-style: none;}
	.list-group-item{ background-color: #c6c6c6;cursor: pointer; }
</style>
<script type="text/javascript">
	$(document).ready(function(e){
		
    	$('#sortable').sortable();
	
		$('.change_order').click(function(){ 
			/*$('#sortable').sortable({
        		items: ':not(.disabled)'
    		}).bind('sortupdate', function() {*/
    			 $('.page-overlay').show();
    			 var dataIDList = $('#sortable li').map(function(){  return $(this).attr("id"); }).get().join(",");
    			 
    				$.ajax({
				        type: "POST",
				        url: "<?php base_url(); ?>size/update_sort_order",
				        data: {
				            data: dataIDList
				        },
				        success: function (data) {
				           $('.page-overlay').hide();
				        }
    				});
    		/*});*/
		});

	});
	

</script>
