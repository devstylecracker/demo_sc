<section class="content-header">
  <h1>Store Settings</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>brandusers">Manage Brand Users</a></li>
    <li class="active">Store Settings</li>
  </ol>
</section>
<section class="content">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Store Settings</h3>
    </div>
    <div class="box-body">
      <!-- form start -->
      <form name="frmcontact" id="frmcontact" role="form" method="post" enctype="multipart/form-data" >
        <div class="row">
          <div class="col-md-6">

            <div class="form-group">
              <label>
                <?php if(@set_value('ispaid') == 1){ $checked = 'checked';  }else if(empty($_POST) && $user_data[0]['is_paid'] == 1){ $checked = 'checked'; }else{ $checked = ''; } ?>
                <input  type="checkbox" name="ispaid"  value="1" <?php echo $checked; ?> style='<?php echo $this->session->userdata('role_id') == 6 ? 'display:none;':' display:inline-block;'; ?>'>
                <span class="text-red"> <?php echo form_error('ispaid'); ?></span>
                <?php echo $this->session->userdata('role_id') == 6 ? '':' Paid Brand'; ?>
                </label>
              </div>
              <div class="form-group">

                <?php echo $this->session->userdata('role_id') == 6 ? '':' <label for="Onboarded Date">On-board Date </label>'; ?>
                <input type="<?php echo $this->session->userdata('role_id') == 6 ? 'hidden':'text'; ?>" class="form-control" name="date_from" id="date_from" value="<?php echo $user_data[0]['onboarded_date']!='0000-00-00' ? $user_data[0]['onboarded_date'] :  set_value('date_from'); ?>">
              </div>
			  
			  <div class="form-group">

                <?php echo $this->session->userdata('role_id') == 6 ? '':' <label for="Contract end Date">Contract end Date</label>'; ?>
                <input type="<?php echo $this->session->userdata('role_id') == 6 ? 'hidden':'text'; ?>" class="form-control" name="contract_end_date" id="contract_end_date" value="<?php echo $user_data[0]['contract_end_date']!='0000-00-00' ? $user_data[0]['contract_end_date'] :  set_value('contract_end_date'); ?>">
              </div>
			  
              <div class="form-group">
              <?php echo $this->session->userdata('role_id') == 6 ? '':'<label>Brand CPA (%)</label>'; ?>

                <input  type="<?php echo $this->session->userdata('role_id') == 6 ? 'hidden':'text'; ?>" name="cpa" class="form-control"  value="<?php if(isset ($user_data[0]['cpa_percentage'])&& set_value('cpa')=='') echo $user_data[0]['cpa_percentage']; else echo set_value('cpa');?>" />
                <span class="text-red"> <?php echo form_error('cpa'); ?></span>
              </div>

              <div class="form-group">
              <?php echo $this->session->userdata('role_id') == 6 ? '':' <label>Brand Initials </label>  <small> e.g (STYCR)</small>'; ?>

                <input  type="<?php echo $this->session->userdata('role_id') == 6 ? 'hidden':'text'; ?>" name="brand_code" class="form-control"  value="<?php if(isset ($user_data[0]['brand_code'])&& set_value('brand_code')=='') echo $user_data[0]['brand_code']; else echo set_value('brand_code'); ?>" >
                <span class="text-red"> <?php echo form_error('brand_code'); ?></span>
              </div>

              <div class="form-group">
                <label>VAT/TIN Number</label>
                <input  type="text" name="vat" class="form-control" value="<?php if(isset ($user_data[0]['vat_tin_number']) && set_value('vat')=='') echo $user_data[0]['vat_tin_number']; else echo set_value('vat');?>" >
                <span class="text-red"> <?php echo form_error('vat'); ?></span>
              </div>

              <div class="form-group">
                <label>COD Available</label>
                <input type="radio" name="cod_available" id="cod_available" value="1" <?php if(isset ($user_data[0]['cod_available']) && $user_data[0]['cod_available'] == 1){ echo 'checked'; } ?> > Yes
                <input type="radio" name="cod_available" id="cod_available" value="0" <?php if(isset ($user_data[0]['cod_available']) && $user_data[0]['cod_available'] == 0){ echo 'checked'; } ?>> No
              </div>

<span id="cod_info">
   <div class="form-group">
            <div>
              <label>  <?php if($user_data[0]['is_cod'] == '1'){ ?>
                <input  type="checkbox" name="cod"  id="cod" value="1" checked>
                <?php }
                else{ ?>
                  <input  type="checkbox" name="cod"  id="cod" value="1" >
                  <?php } ?> COD Charges (<i class="fa fa-inr"></i>) <I>(Please check mark to add COD charges)</I></label>

                  <span class="text-red"> <?php echo form_error('is_cod'); ?></span>
                </div>
                <div class="codis" style="display:none">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <input  type="text" name="cod_charge" placeholder="COD Amount" id="cod_charge" class="form-control" value="<?php if(isset($user_data[0]['cod_charges']) && $user_data[0]['cod_charges']!='') echo $user_data[0]['cod_charges']; else echo set_value('cod_charges'); ?>" >
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <input  type="text" id="mincod" placeholder="Min Price Amount" class="form-control"  name="mincod" value="<?php if(isset( $user_data[0]['cod_min_value'])&&  $user_data[0]['cod_min_value']!='') echo $user_data[0]['cod_min_value']; else echo set_value('mincod'); ?>">
                      </div>

              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <input  type="text" id="maxcod" placeholder="Max Price Amount" class="form-control"  name="maxcod" value="<?php if(isset($user_data[0]['code_max_value'])&& $user_data[0]['code_max_value']!='') echo $user_data[0]['code_max_value']; else echo set_value('maxcod'); ?>" >
                </div>


          </div>
          </div>
          </div>
          </div>
            <div class="form-group">
              <label>Non-Area COD Location </label>  <small>e.g (123456,678907,124568)</small>
              <textarea  type="text" onkeypress="return isNumberKey(event)" name="cod_location" class="form-control" cols="50"><?php if(isset($user_data[0]['cod_exc_location']) && set_value('cod_location')=='') echo $user_data[0]['cod_exc_location']; else echo set_value('cod_location'); ?></textarea>
              <span class="text-red"> <?php echo form_error('cod_location'); ?></span>
            </div>
</span>
            <label>Product Delivery Days</label>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input  type="text" name="min_del" placeholder="Min" class="form-control"  value="<?php if(isset ($user_data[0]['min_del_days'])&& set_value('min_del')=='') echo $user_data[0]['min_del_days']; else echo set_value('min_del');?>" >
                  <span class="text-red"> <?php echo form_error('min_del'); ?></span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input  type="text" name="max_del" placeholder="Max" class="form-control"  value="<?php if(isset($user_data[0]['max_del_days'])&& set_value('max_del')=='') echo $user_data[0]['max_del_days']; else echo set_value('max_del');?>" >
                  <span class="text-red"> <?php echo form_error('max_del'); ?></span>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label>Registered Company Name And Office Address</label>
              <textarea rows="4" name="sto_radd" class="form-control" cols="50" ><?php if(isset( $user_data[0]['registered_address'])&& set_value('sto_radd')=='' ) echo $user_data[0]['registered_address']; else echo set_value('sto_radd'); ?></textarea>
            </div>

            <div class="form-group">
              <label>Email ID</label><small> ( Users Order related Information )</small>
              <input  type="text" class="form-control" name="cea"  value="<?php if(isset($user_data[0]['contact_email_address']) && set_value('cea')=='') echo $user_data[0]['contact_email_address']; else echo set_value('cea'); ?>" >
              <span class="text-red"> <?php echo form_error('cea'); ?></span>
            </div>

            <div class="form-group">
                <label>Return Policy<span class="text-red">*</span></label><small> ( Max 150 characters )</small>
                <textarea rows="3" name="return_policy" id="return_policy" class="form-control" name="return_policyreturn_policy" maxlength="150" cols="50"><?php if(isset($user_data[0]['return_policy']) && set_value('cea')=='') echo $user_data[0]['return_policy']; else echo set_value('return_policy'); ?></textarea>
                <span class="text-red"> <?php echo form_error('return_policy'); ?></span>
            </div>

            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>
                  <?php if(@set_value('show_in') == 1){ $checked = 'checked';  }else if(empty($_POST) && $user_data[0]['show_in brand_list'] == 1){ $checked = 'checked'; }else{ $checked = ''; } ?>
                  <input  type="checkbox" name="show_in"  value="1" <?php echo $checked; ?> style='<?php echo $this->session->userdata('role_id') == 6 ? 'display:none;':' display:inline-block;'; ?>'>
                  <span class="text-red"> <?php echo form_error('show_in'); ?></span>
                  <?php echo $this->session->userdata('role_id') == 6 ? '':' Show on Web'; ?>
                  </label>
                </div>

			<div class="form-group">
        <?php if($this->session->userdata('role_id') != 6){ ?>
				<label>
				<input type="radio" name="commission" value="1" <?php if(isset ($user_data[0]['commission_cat']) && $user_data[0]['commission_cat'] == 1){ echo 'checked'; } ?> style='<?php echo $this->session->userdata('role_id') == 6 ? 'display:none;':' display:inline-block;'; ?>' > <?php echo $this->session->userdata('role_id') == 6 ? '':' All exclusive '; ?>
        </label><br>
        <label>
        <input type="radio" name="commission" value="2" <?php if(isset ($user_data[0]['commission_cat']) && $user_data[0]['commission_cat'] == 2){ echo 'checked'; } ?> style='<?php echo $this->session->userdata('role_id') == 6 ? 'display:none;':' display:inline-block;'; ?>' >  <?php echo $this->session->userdata('role_id') == 6 ? '':'  Flat Commission model '; ?><br>
				</label>
        <br>
        <label>
        <input type="radio" name="commission" value="3" <?php if(isset ($user_data[0]['commission_cat']) && $user_data[0]['commission_cat'] == 3){ echo 'checked'; } ?> style='<?php echo $this->session->userdata('role_id') == 6 ? 'display:none;':' display:inline-block;'; ?>' > <?php echo $this->session->userdata('role_id') == 6 ? '':' Flat Commission excluding taxes '; ?>
				</label>
        <?php } ?>
			</div>


          <!-- <div class="form-group">

            <?php echo $this->session->userdata('role_id') == 6 ? '':' <label>Brand CPC (%) </label>'; ?>
            <input  type="<?php echo $this->session->userdata('role_id') == 6 ? 'hidden':''; ?>" name="cpc" class="form-control"  value="<?php if(isset($user_data[0]['cpc_percentage']) && set_value('cpc')=='') echo $user_data[0]['cpc_percentage']; else echo set_value('cpc');?>" >
            <span class="text-red"> <?php echo form_error('cpc'); ?></span>
          </div> -->

          <div class="form-group">

            <?php echo $this->session->userdata('role_id') == 6 ? '':' <label>Payment Gateway charges (%) </label>'; ?>
            <input  type="<?php echo $this->session->userdata('role_id') == 6 ? 'hidden':''; ?>" name="payment_gateway_charges" class="form-control"  value="<?php if(isset($user_data[0]['payment_gateway_charges']) && set_value('payment_gateway_charges')=='') echo $user_data[0]['payment_gateway_charges']; else echo set_value('payment_gateway_charges');?>" >
            <span class="text-red"> <?php echo form_error('payment_gateway_charges'); ?></span>
          </div>

          <div class="form-group">

            <?php echo $this->session->userdata('role_id') == 6 ? '':' <label>Service Tax (%) </label>'; ?>
            <input  type="<?php echo $this->session->userdata('role_id') == 6 ? 'hidden':''; ?>" name="service_tax" class="form-control"  value="<?php if(isset($user_data[0]['service_tax']) && set_value('service_tax')=='') echo $user_data[0]['service_tax']; else echo set_value('service_tax');?>" >
            <span class="text-red"> <?php echo form_error('service_tax'); ?></span>
          </div>

          <div class="form-group">

            <?php echo $this->session->userdata('role_id') == 6 ? '':' <label>Integration Charges (%) </label>'; ?>
            <input  type="<?php echo $this->session->userdata('role_id') == 6 ? 'hidden':''; ?>" name="integration_charges" class="form-control"  value="<?php if(isset($user_data[0]['integration_charges']) && set_value('integration_charges')=='') echo $user_data[0]['integration_charges']; else echo set_value('integration_charges');?>" >
            <span class="text-red"> <?php echo form_error('integration_charges'); ?></span>
          </div>

          <div class="form-group">
            <label for=""> 	Brand Tax (%) <I>(Please mention, if your product prices on the StyleCracker store are exclusive of VAT)</I> </label>
            <input  type="text" name="tax" class="form-control" value="<?php if(isset($user_data[0]['store_tax']) && set_value('tax')=='') echo $user_data[0]['store_tax']; else echo set_value('tax'); ?>" >
            <span class="text-red"> <?php echo form_error('tax'); ?></span>
          </div>
          <div class="form-group">
            <label>Brand Registration ID</label>
            <input  type="text" name="str_id" class="form-control" value="<?php if(isset($user_data[0]['registration_number']) && set_value('str_id')=='') echo $user_data[0]['registration_number']; else echo set_value('str_id');?>" >
            <span class="text-red"> <?php echo form_error('str_id'); ?></span>
          </div>
          <div class="form-group">
            <label>CST Number</label>
            <input  type="text" name="cst" class="form-control"  value="<?php if(isset($user_data[0]['cst_number']) && set_value('cst')=='') echo $user_data[0]['cst_number']; else echo set_value('cst'); ?>" >
            <span class="text-red"> <?php echo form_error('cst'); ?></span>
          </div>


           <div class="form-group">
                <label><?php if($user_data[0]['is_shipping'] == '1'){ ?>
                  <input  type="checkbox" name="cell"  id="cell" value="1" checked>
                  <?php }
                  else{ ?>
                    <input  type="checkbox" name="cell"  id= "cell" value="1" >
                    <?php } ?> Shipping Charges (<i class="fa fa-inr"></i>) <I>(Please check mark to add Shipping Charges)</I></label>
                    <span class="text-red"> <?php echo form_error('is_shipping'); ?></span>
                  </div>

                  <div class="input-max" style="display:none">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <input  type="text" class="form-control" placeholder="Shipping Amount" id="sc" name="sc"  value="<?php if(isset($user_data[0]['shipping_charges'])&& $user_data[0]['shipping_charges']!='') echo $user_data[0]['shipping_charges']; else echo set_value('sc');?>" />
                        </div>  </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <input class="form-control" placeholder="Min Price Amount" type="text" id="min" checked="checked" name="min" value="<?php if(isset ($user_data[0]['shipping_min_values'])&& $user_data[0]['shipping_min_values']!='') echo $user_data[0]['shipping_min_values']; else echo set_value('min'); ?>">

                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <input class="form-control" placeholder="Max Price Amount" type="text" id="max" name="max" checked="checked" value="<?php if(isset($user_data[0]['shipping_max_values']) && $user_data[0]['shipping_max_values']!='') echo $user_data[0]['shipping_max_values']; else echo set_value('max');?>" >


                </div>
              </div>
            </div>
            </div>

          <div class="form-group">
            <label>Non-Area Shipping Location </label>  <small>e.g (123456,678907,124567)</small>
            <textarea  onkeypress="return isNumberKey(event)"  type="text" name="shipping_location" class="form-control" cols="50"><?php if(isset($user_data[0]['shipping_exc_location']) && set_value('shipping_location')=='') echo $user_data[0]['shipping_exc_location']; else echo set_value('shipping_location'); ?> </textarea>
            <span class="text-red"> <?php echo form_error('shipping_location'); ?></span>
          </div>

          <div class="form-group">
            <?php if(isset($user_data[0]['sign_img']) && $user_data[0]['sign_img']!='') { ?>
              <img src="<?php echo base_url().'assets/brand_signature/'.$user_data[0]['sign_img']; ?>" alt="<?php echo $user_data[0]['sign_img']; ?>" style="width:50px; height:50px; margin-bottom:10px;" />
              <?php } ?>

              <label>Upload Signature (.jpg,.png)</label>
              <input type="file" name="fileToUpload" class="form-control" value="<?php echo set_value('fileToUpload'); ?>" >
              <span class="text-red"> <?php echo form_error('fileToUpload'); ?></span>
            </div>
            <div class="form-group">
              <label>Store Enquiry Office Address</label>
              <textarea rows="4" class="form-control" name="sto_ra" class="form-control" cols="50" ><?php if(isset($user_data[0]['query_address']) && set_value('sto_ra')=='' ) echo $user_data[0]['query_address']; else echo set_value('sto_ra');?></textarea>
            </div>
            <div class="form-group">
              <label>Enquiry Email ID</label><small> ( Displayed to Customer )</small>
              <input  type="text" class="form-control" name="str_email"  value="<?php if(isset($user_data[0]['query_email_id'])&& set_value('str_email')=='') echo $user_data[0]['query_email_id']; else echo set_value('str_email');?>" >
              <span class="text-red"> <?php echo form_error('str_email'); ?></span>
            </div>
            <div class="form-group">
              <label>Enquiry Phone No. </label>
              <input  type="text" name="str_phn_no" class="form-control" value="<?php if(isset($user_data[0]['query_contact_no']) && set_value('str_phn_no')=='') echo $user_data[0]['query_contact_no']; else echo set_value('str_phn_no'); ?>" >
              <span class="text-red"> <?php echo form_error('str_phn_no'); ?></span>
            </div>


            </div>
  </div>

</div>
<div class="box-footer">
  <div class="form-group">
    <button class="btn btn-primary" type="submit">Submit</button>
  </div>
  </div>
</div>
</form>
</section>

<script>
$( document ).ready( function(){
  $(function () {
    $("#date_from").datepicker({ format: "yyyy-mm-dd",endDate: '+0d', autoclose: true, todayHighlight: true
  });
  hide_how_extra_fields();
});
$(function () {
    $("#contract_end_date").datepicker({ format: "yyyy-mm-dd",endDate: '+400d', autoclose: true, todayHighlight: true
  });
  hide_how_extra_fields();
});
hide_cod_det();
$( "input#cell" ).on( "click", function (){
  hide_how_extra_fields();
} );

hide_how_extra_area();

$( "input#cod" ).on( "click", function (){
  hide_how_extra_area();
} );

$('input[type=radio][name=cod_available]').change(function() {

        if (this.value == '1') {
           $('#cod_info').show();
        }
        else if (this.value == '0') {
           $('#cod_info').hide();
        }
    });


});
function hide_cod_det(){
  var cod_chk = "<?php echo $user_data[0]['cod_available']!='' ? $user_data[0]['cod_available'] : 0; ?>";
   if (cod_chk == '1') {
           $('#cod_info').show();
           $('input[type=radio][name=cod_available]').trigger('change');
        }
        else if (cod_chk == '0') {
           $('#cod_info').hide();
        }
}
function hide_how_extra_fields(){
  var is_shipping = <?php echo $user_data[0]['is_shipping']; ?>;

  if($( "input#cell:checked" ).val()!=1 )

  {
    $(".input-max" ).hide();
    //$('#sc').val(this.checked ? 1 : 0);
    //$('#min').val(this.checked ? 1 : 0);
    //$('#max').val(this.checked ? 1 : 0);
  }
  else
  {
    $(".input-max" ).show();
     //$('#sc').val(this.checked ? 1 : '');
    //$('#min').val(this.checked ? 1 : '');
    //$('#max').val(this.checked ? 1 : '');

  }
}

function hide_how_extra_area (){
  var is_cod = <?php echo $user_data[0]['is_cod']; ?>;


  if($( "input#cod:checked" ).val()!=1 )
  {
    $(".codis").hide();

    //$('#cod_charge').val(this.checked ? 1 : 0);
    //$('#mincod').val(this.checked ? 1 : 0);
    //$('#maxcod').val(this.checked ? 1 : 0);

  }
  else
  {
    $(".codis").show();
    //$('#cod_charge').val(this.checked ? 1 : '');
    //$('#mincod').val(this.checked ? 1 : '');
  //  $('#maxcod').val(this.checked ? 1 : '');


  }


}

function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : evt.keyCode;

         if (charCode != 44 && charCode > 31 && (charCode < 48 || charCode > 57))
           return false;
          return true;
      }

</script>
