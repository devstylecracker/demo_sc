<?php if(!empty($cat_data)){
	foreach($cat_data as $val){
?>
	<tr>
		<td><?php echo $val['id']; ?></td>
		<td><?php echo $val['attribute_name']; ?></td>
		<td><?php echo $val['attribute_slug']; ?></td>
		<td><?php echo $val['count']; ?></td>
		<td>
					<a href="<?php echo base_url(); ?>attributes/attributes_edit/<?php echo $val['id']; ?>"><button class="btn btn-primary btn-xs editProd" type="button" ><i class="fa fa-edit"></i> Edit</button></a>
					<a href="<?php echo base_url(); ?>attrsettings/index/<?php echo $val['id']; ?>"><button class="btn btn-secondary btn-xs editProd" type="button" ><i class="fa fa-edit"></i> Settings </button></a>
					<button class="btn btn-xs btn-primary btn-delete" type="button" onclick="delete_category(<?php echo $val['id']; ?>);"><i class="fa fa-trash-o"></i> Delete</button>
		</td>
	</tr>
<?php } } ?>
		