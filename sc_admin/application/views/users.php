<?php
$users_menu=$this->data['users_menu'];
?>
<section class="content-header">
        <h1>Manage Admin Users</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>          
            <li class="active">Manage Admin Users</li>
          </ol>
</section>
<section class="content">
        <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">List of Admin Users <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3>
                  <div class="pull-right">
                  <?php if(in_array("12",$users_menu)) { ?>
                  <a href="<?php echo base_url(); ?>users/user_add"><button class="btn btn-sm btn-primary" type="button"><i class="fa fa-plus"></i> Add User</button></a>
                  <?php } ?>
                  </div>
                  </div>
                  <div class="box-body">
                    <?php echo $this->session->flashdata('feedback'); ?>
						<form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>users" method="post">
              <div class="row">
        <div class="col-md-2">
									<select name="search_by" id="search_by" class="form-control input-sm" >

										<option value="0">Search by</option>
										<option selected value="1" <?php echo set_value('search_by')==1 ? 'selected' : ''; ?>>Username</option>
										<option value="2" <?php echo set_value('search_by')==2 ? 'selected' : ''; ?>>Email</option>
										<option value="3" <?php echo set_value('search_by')==3 ? 'selected' : ''; ?>>Date</option>
										<option value="4" <?php echo set_value('search_by')==4 ? 'selected' : ''; ?>>User ID</option>
									</select>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
								<input type="text" name="table_search" id="table_search" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo set_value('table_search'); ?>" required=""/>
									<div class="input-group-btn">
										<button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
									</div>
                </div>
                </div>
                <div class="col-md-2">
                  <a href="users" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
                </div>
                <div class="col-md-5">
                  <!--div class="records-per-page pull-right">
                    <select name="records-per-page" class="form-control input-sm">
                      <option>Per Page</option>
                      <option value="10" selected="selected">10</option>
                      <option value="25">25</option>
                      <option value="50">50</option>
                    </select>
                  </div-->
                    <div class="pull-right">
                        <?php echo $this->pagination->create_links(); ?>
                      </div>
                </div>
              </div>
						</form>
            <div class="table-responsive">
                <table class="table table-bordered table-striped dataTable" id="datatable">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Role</th>
						            <th>Created Date</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
						<?php if(!empty($users_data)) { $i = 1;
						foreach($users_data as $val){
						?>
					  <tr>
                        <td><?php echo $val->id; ?></td>
                        <td><?php echo $val->user_name; ?></td>
                        <td><?php echo $val->email; ?></td>
                        <td><?php echo $val->role_name; ?></td>
                        <td><?php //echo $val->created_datetime;
                                  $date = new DateTime($val->created_datetime);
                                  echo $date->format('d-m-Y H:i:s');
                        ?></td>
                        <td>
                          <?php if(in_array("11",$users_menu)) { ?>
                          <a href="<?php echo base_url(); ?>users/user_view/<?php echo $val->id; ?>">
                          <button type="button" class="btn  btn-xs btn-primary"><i class="fa fa-eye"></i> View</button></a>
                          <?php } ?>



							<?php if(in_array("13",$users_menu)) { ?>
							<a href="<?php echo base_url(); ?>users/user_edit/<?php echo $val->id; ?>">
							<button type="button" class="btn  btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</button></a>
							<?php } ?>
              <?php if(in_array("10",$users_menu)) { ?>
              <a href="<?php echo base_url(); ?>users/assign_permission/<?php echo $val->id; ?>">
              <button type="button" class="btn btn-xs btn-primary"><i class="fa fa-key"></i>
Permissions</button></a>
              <?php } ?>

							<?php if(in_array("14",$users_menu)) { ?>
							<a onclick="javascript:void(0)">
							<button type="button" class="btn  btn-xs btn-delete btn-primary" data-id='<?php echo $val->id; ?>' data-toggle="confirmation"><i class="fa fa-trash-o"></i> Delete</button></a>
							<?php } ?>

            </td>
                      </tr>
                       <?php $i++; } }else{ ?>
					  <tr>
						<td colspan="5">Sorry no record found</td>
					  </tr>
					   <?php } ?>
                    </tbody>

                  </table>
                </div>

                  <?php echo $this->pagination->create_links(); ?>
                  </div>
                  </div>

                </section>

<script type="text/Javascript">
	$( document ).ready(function() {
		$('[data-toggle=confirmation]').confirmation({
			title:'Are you sure?',
			onConfirm : function(){
				var del_id = $(this).closest('td').find('.btn-delete').attr('data-id');
				$.ajax({
					 type: "POST",
					 url: "<?php echo base_url(); ?>" + "users/user_delete",
					 data: { id: del_id },
					 cache:false,
					 success:
						  function(data){
							location.reload(); //as a debugging message.
						  }

					});
				}
			});
      $('.nav-item-user').addClass("active");
      $('.nav-admin-user').addClass("active");
	});
</script>
