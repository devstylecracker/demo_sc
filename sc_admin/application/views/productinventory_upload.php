<?php

if(!isset($error))
{
  $error = "";
}
//print_r($brand_data);

?>
<section class="content-header">
  <h1>Upload inventory report</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>product_new">Manage Products</a></li>
    <li class="active">Upload inventory report</li>
  </ol>
</section>
<section class="content">
  <div class="row">
    <?php echo $this->session->flashdata('feedback'); ?>
    <form role="form" id="product_uploadform" enctype="multipart/form-data" method="POST" action="<?php echo base_url(); ?>Productinventory_upload/product_upload/" onsubmit="return validate()">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-upload"></i> Upload Que Sheet <small class="label label-info"></small></h3>
            <div class="pull-right">
            </div>
          </div>
          <div class="box-body  text-center">
            <br/><br/>
            <p>Browse Que Sheet <input  id="product_upload" name="product_upload" type="file" class="file"></p>
            <!--p class="help-block">Max file size: 5MB | Allowed format: .xls, .xlsx </p-->
          <br/>
          <p>
            <button class="btn btn-primary btn-sm" type="submit" name = "Save"  value="Save" ><i class="fa fa-upload"></i> Upload</button>
              <a href="<?php echo base_url(); ?>Product_upload" ><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i> Back</button></a>
          </p>

          </div>
          <span class="text-red">



            
        <!--   if(!empty($excelmsg))
          {
            echo "<b> Error in Excel Sheet :</b> <br/>";
            foreach($excelmsg  as $ex)
            {
              echo @$ex."<br/>";
            }

          }
 -->
          
        </span>
      </div>
    </div>
  </form>
  <div class="col-md-12">
    <div class="box quesheet-doc">
      <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-book"></i> Help <small class="label label-info"></small></h3>
        <div class="pull-right">
        </div>
      </div>
      <div class="box-body">
        <p class="text-orange">Download sample Que Sheet   <a href="<?php echo base_url(); ?>Productinventory_upload/download_sheet/" class="btn btn-default btn-xs"><i class="fa fa-download"></i> Download</a></p>
     <!--    <div class="seperator"><br></div> -->

<script>
function validate() {
    var extension;
    var filename=document.getElementById('product_upload').value;
    extension=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();
  
    if(filename == "")
    {
        alert('Browse Excel File');
        return false;

    }else
    {
      if(extension == 'xlsx' || extension == 'xls')
      {
        return true;
      }else
      {
        alert('This File Is Not Excel');
        return false;
      }

    }
    

  }

  </script>