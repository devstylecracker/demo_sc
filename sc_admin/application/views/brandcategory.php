<?php
  $users_menu=$this->data['users_menu'];

  if(@$category_details!=NULL)
  {
    $page="Edit";
  }else
  {
    $page="Add";
  }
?>
<section class="content-header">
  <h1>Brand Category/Collection </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>home">Home</a></li>
      <li><a href="<?php echo base_url(); ?>brandCategory">Brand Categories/Collection</a></li>
      <li class="active"><?php echo $page;?> Brand Category/Collection</li>
    </ol>
</section>
<section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"><?php echo $page;?> Brand Category/Collection</h3>
      </div><!-- /.box-header -->
      <form role="form" name="frmbrandcategory" id="frmbrandcategory" enctype="multipart/form-data" method="post">
        <div class="box-body">
         <?php echo $this->session->flashdata('feedback'); ?>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="categoryName" class="control-label">Category/Collection Name
    	          <span class="text-red">*</span></label>
                <input type="text" placeholder="Enter New Category Name" name="categoryName" id="categoryName" value="<?php if(@$category_details->category_name!="") echo @$category_details->category_name; else echo set_value('categoryName');?>" onblur="assignSlug()" class="form-control" > <span class="text-red"><?php echo form_error('categoryName'); ?></span>
              </div>            
              <div class="form-group">
                <label for="categorySlug" class="control-label">Slug
    	          <span class="text-red">*</span></label>
                <input type="text" placeholder="Enter Brand Category Slug" name="categorySlug" id="categorySlug" value="<?php if(@$category_details->category_slug!="") echo @$category_details->category_slug; else echo set_value('categorySlug');?>"  <?php //if(@$category_details->category_slug!="") echo 'disabled'; ?> class="form-control"><span class="text-red"><?php echo form_error('categorySlug'); ?></span>
              </div>
              <div class="form-group">
                  <label for="categoryParent" class="control-label">Parent                      
                  <select class="form-control select2" name="categoryParent" id="categoryParent" >
                      <option value="0">None </option>
                      <?php foreach($brandCat as $cat){ 
                      ?>
                    <option value="<?php echo $cat->id; ?>"
                    <?php if(@$category_details->parent == @$cat->id) echo "selected";?>
                    ><?php echo $cat->category_name; ?></option>
                    <?php }?>
                  </select>
                  <span class="text-red"><?php echo form_error('categoryParent'); ?></span>
              </div>
              
              <div class="form-group">
                  <label for="category_type" class="control-label"> Type
                  <select class="form-control select2" name="category_type" id="category_type" >
                    <option value="brand_cat"  <?php if(@$category_details->category_type == 'brand_cat') echo "selected"; else echo ''; ?> >Brand Category</option>
                    <option value="brand_collection"  <?php if(@$category_details->category_type == 'brand_collection') echo "selected"; else echo ''; ?> >Brand Collection</option>
                  </select>
                  <span class="text-red"><?php echo form_error('category_type'); ?></span>
              </div>

              <div class="form-group">
                <label class="control-label">Active</label>
                <div>
                  <label class="radio-inline">
                  <?php if(isset($category_details->status) && $category_details->status==0) { $check_no =  'checked="true"'; }else { $check_ok = 'checked="true"'; } ?>
                    <input type="radio" value="1" name="active" <?php echo @$check_ok; ?> > Yes</label>
                  <label class="radio-inline"> <input type="radio" value="0" name="active" <?php echo @$check_no; ?> > No</label>
                </div>
              </div>
              <div class="form-group prod-image-outer">
                  <label for="Uploadimage">Upload Category/Collection Image</label>
                  <?php echo $category_details->category_image; if(isset($category_details->category_image) && $category_details->category_image!='') {
                      echo '<div class="prod-image-wrp">';
                      echo '<img src="'.base_url().'assets/brandCategory_images/thumb/'.$category_details->category_image.'">';
                      echo '</div>';
                    } ?>
                  <input id="category_image" name="category_image" class="file1" type="file" >
                  <span class="help-block">Max file size: 2MB. | Allowed format: .jpg, .jpeg, .png, .gif</span>
                  <span class="text-red"> <?php if(isset($image_error)) { echo $image_error[0]; } ?></span>
              </div>
            </div>
          </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Submit</button>
          <a href="<?php echo base_url(); ?>BrandCategory"><button type="button" class="btn btn-primary btn-sm btn-rad"><i class="fa fa-reply"></i> Back</button></a>
          <button class="btn btn-default btn-sm" type="reset"><i class="fa fa-close"></i> Cancel</button>
        </div>
      </form>
    </div><!-- /.box -->
</section>

<script type="text/Javascript">
$( document ).ready(function() {
	$('.nav-item-category').addClass("active");
  $('.nav-manage-brand-cat').addClass("active");
});

function assignSlug() {
    var x = document.getElementById("categoryName");
    var slug = x.value.toLowerCase();
    var newslug = slug.replace(/ /g,"-");
    $("#categorySlug").val(newslug);
}

</script>

