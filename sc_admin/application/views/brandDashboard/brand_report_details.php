<?php $users_menu=$this->data['users_menu']; ?>
 <section class="content-header">
          <h1><?php echo $page_title; ?><small class="label label-info"><?php echo $total_rows; ?></small></h1>
            <ol class="breadcrumb">
              <li><a href="<?php echo base_url(); ?>home">Home</a></li>
              <li class="active">Details</li>
            </ol>
  </section>
	<section class="content">
    <div class="box">
      <div class="box-header with-border">
        <!--h3 class="box-title"><?php echo $page_title; ?> <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3-->
          <div class="pull-right">
        <!--form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>view_all_users" method="post">
          <?php echo $this->pagination->create_links(); ?>
        </form-->
        </div>
      </div>
    <div class="box-body">

    <?php if($type == 'total_sales'){ ?>
	   <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th>Order ID</th>
              <th>Product ID</th>
              <th>Product Name</th>
              <th>Price</th>
              <th>Created Datetime</th>
              
            </tr>
          </thead>
          <tbody>
            <?php if(!empty($total_sales_details)){ $total_price = 0; $i = 0;
              foreach($total_sales_details as $val){ $total_price =  $total_price + $val['product_price'];
            ?>
            <tr>
              <td><?php echo $val['order_id']; ?></td>
              <td><a href="<?php echo base_url(); ?>product_new/product_view/<?php echo $val['product_id']; ?>" target="_blank"><?php echo $val['product_id']; ?></a></td>
              <td><?php echo $val['name']; ?></td>
              <?php $product_price_nf = number_format($val['product_price'], 2, '.', ','); ?>
              <td><?php echo $product_price_nf; ?></td>
              <td><?php echo $val['created_datetime']; ?></td>
            </tr>
            <?php $i++; } ?>
            <tr>
              <th colspan="3">Total:-  <?php echo $i++; ?></th>
              <?php $total_price_nf = number_format($total_price, 2, '.', ','); ?>
              <th><?php echo  $total_price_nf; ?></th>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
       <!--div class="pull-right">
          <?php echo $this->pagination->create_links(); ?>
        </div-->
    </div>
   


    <?php } else if($type == 'products' || $type == 'approved_products' || $type=='view_products_click'){ ?>
     <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              
              <th>Product ID</th>
              <th>Product Name</th>
              <th>Price</th>
              <?php if($type=='view_products_click'){ ?>
              <th>Click Count</th>
              <th>User ID</th>
              <?php } ?>
              <th>Created Datetime</th>
              
            </tr>
          </thead>
          <tbody>
            <?php if(!empty($products)){ $total_price = 0; $i = 0; $product_click_count = 0;
              foreach($products as $val){ $total_price =  $total_price + $val['price']*$val['click_count'];
            ?>
            <tr>
             
              <td><a href="<?php echo base_url(); ?>product_new/product_view/<?php echo $val['id']; ?>" target="_blank"><?php echo $val['id']; ?></a></td>
              <td><?php echo $val['name']; ?></td>
              <?php   $price_click = $val['price']*$val['click_count'];
              $price_click_nf = number_format($price_click, 2, '.', ','); ?>
              <td><?php echo $price_click_nf; ?></td>
              <?php if($type=='view_products_click') { $product_click_count = $product_click_count +  $val['click_count']; ?>
              <td><?php echo $val['click_count']; ?></td>
              <td><?php if($val['user_id'] > 0){ ?><a href="<?php echo base_url(); ?>websiteusers/user_view/<?php echo $val['user_id']; ?>" target="_blank"><?php echo $val['user_id']; ?></a><?php }else{ ?><?php echo $val['user_id']; ?></a><?php } ?></td>
              <?php } ?>
              <td><?php echo $val['created_datetime']; ?></td>
            </tr>
            <?php $i++;  } ?>
            <tr>
              <th colspan="2">Total :- <?php echo $i; ?> </th>
               <?php $total_price_nf = number_format($total_price, 2, '.', ','); ?>
              <th>&#8377;<?php echo  $total_price_nf; ?></th>
              <?php if($type=='view_products_click'){ ?><th><?php echo $product_click_count; ?></th><?php } ?>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
       <!--div class="pull-right">
          <?php echo $this->pagination->create_links(); ?>
        </div-->
    </div>
    <?php }else if($type == 'looks'){ ?>
         <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              
              <th>Look ID</th>
              <th>Look Name</th>
              <th>Status</th>
              <th>Created Datetime</th>
              
            </tr>
          </thead>
          <tbody>
            <?php if(!empty($products)){ $total_price = 0; $i = 0;
              foreach($products as $val){ //$total_price =  $total_price + $val['price'];
            ?>
            <tr>
             
              <td><a href="<?php echo base_url(); ?>lookcreator/look_view/<?php echo $val['id']; ?>" target="_blank"><?php echo $val['id']; ?></a></td>
              <td><?php echo $val['name']; ?></td>
              <td><?php if($val['status']==0){ echo "<span class='text-orange'>Pending</span>"; }else if($val['status']==1){ echo "<span class='text-green'>Broadcast</span>"; }else { echo "<span class='text-red'>Rejected</span>"; } ?></td>
              <td><?php echo $val['created_datetime']; ?></td>
            </tr>
            <?php $i++;  } ?>
            <tr>
              <th colspan="2">Total :- <?php echo $i; ?> </th>
              
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
       <!--div class="pull-right">
          <?php echo $this->pagination->create_links(); ?>
        </div-->
    </div>
    <?php }else if($type == "view_products_used"){ ?>
          <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th>Product ID</th>
              <th>Product Name</th>
              <th>Look ID</th>
              <th>Look Name</th>
              <th>Look Status</th>
              <th>Created Datetime</th>
              
            </tr>
          </thead>
          <tbody>
            <?php if(!empty($products)){ $total_price = 0; $i = 0;
              foreach($products as $val){ //$total_price =  $total_price + $val['price'];
            ?>
            <tr>
              <td><a href="<?php echo base_url(); ?>product_new/product_view/<?php echo $val['product_id']; ?>" target="_blank"><?php echo $val['product_id']; ?></a></td>
              <td><?php echo $val['product_name']; ?></td>
              <td><a href="<?php echo base_url(); ?>lookcreator/look_view/<?php echo $val['id']; ?>" target="_blank"><?php echo $val['id']; ?></a></td>
              <td><?php echo $val['name']; ?></td>
              <td><?php if($val['status']==0){ echo "<span class='text-orange'>Pending</span>"; }else if($val['status']==1){ echo "<span class='text-green'>Broadcast</span>"; }else { echo "<span class='text-red'>Rejected</span>"; } ?></td>
              <td><?php echo $val['created_datetime']; ?></td>
            </tr>
            <?php $i++;  } ?>
            <tr>
              <th colspan="2">Total :- <?php echo $i; ?> </th>
              
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
       <!--div class="pull-right">
          <?php echo $this->pagination->create_links(); ?>
        </div-->
    </div>
    <?php } ?>

  </div>
</div>
</section>