<?php $users_menu=$this->data['users_menu']; ?>
 <section class="content-header">
          <h1><?php echo $page_title; ?></h1>
            <ol class="breadcrumb">
              <li><a href="<?php echo base_url(); ?>home">Home</a></li>
              <li class="active">Details</li>
            </ol>
  </section>
  
	<section class="content">
  <h5>  Top 5 Buy Now</h5>
    <div class="box">
      <div class="box-header with-border">
        <!--h3 class="box-title"><?php echo $page_title; ?> <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3-->
          <div class="pull-right">
        <!--form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>view_all_users" method="post">
          <?php echo $this->pagination->create_links(); ?>
        </form-->
        </div>
      </div>

    <div class="box-body">

    
	   <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
             
              <th>Product ID</th>
              <th>Product Name</th>
              <th>Count</th>
              <th>Price</th>
              <th>Created Datetime</th>
              
            </tr>
          </thead>
          <tbody>
            <?php if(!empty($products['top_five_product_buy'])){ $total_price = 0; $i = 0;
              foreach($products['top_five_product_buy'] as $val){ $total_price =  $total_price + $val['product_price'];
            ?>
            <tr>
              
              <td><a href="<?php echo base_url(); ?>product_new/product_view/<?php echo $val['product_id']; ?>" target="_blank"><?php echo $val['product_id']; ?></a></td>
              <td><?php echo $val['name']; ?></td>
              <td><?php echo $val['count_product_id']; ?></td>
              <td><?php echo $val['product_price']; ?></td>
              <td><?php echo $val['created_datetime']; ?></td>
            </tr>
            <?php $i++; } ?>
            <tr>
              <th colspan="3">Total:-  <?php echo $i++; ?></th>
              <th><?php echo  $total_price; ?></th>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
       <!--div class="pull-right">
          <?php echo $this->pagination->create_links(); ?>
        </div-->
    </div>
    </div>
    </div>

    <h5>  Top 5 Product Click</h5>
    <div class="box">
      <div class="box-header with-border">
        <!--h3 class="box-title"><?php echo $page_title; ?> <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3-->
          <div class="pull-right">
        <!--form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>view_all_users" method="post">
          <?php echo $this->pagination->create_links(); ?>
        </form-->
        </div>
      </div>

    <div class="box-body">

    
     <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
             
              <th>Product ID</th>
              <th>Product Name</th>
              <th>Count</th>
              <th>Price</th>
              <th>Created Datetime</th>
              
            </tr>
          </thead>
          <tbody>
            <?php if(!empty($products['top_five_product_click'])){ $total_price = 0; $i = 0;
              foreach($products['top_five_product_click'] as $val){ $total_price =  $total_price + $val['price'];
            ?>
            <tr>
              
              <td><a href="<?php echo base_url(); ?>product_new/product_view/<?php echo $val['id']; ?>" target="_blank"><?php echo $val['id']; ?></a></td>
              <td><?php echo $val['name']; ?></td>
              <td><?php echo $val['click_count']; ?></td>
              <td><?php echo $val['price']; ?></td>
              <td><?php echo $val['created_datetime']; ?></td>
            </tr>
            <?php $i++; } ?>
            <tr>
              <th colspan="3">Total:-  <?php echo $i++; ?></th>
              <th><?php echo  $total_price; ?></th>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
       <!--div class="pull-right">
          <?php echo $this->pagination->create_links(); ?>
        </div-->
    </div>
    </div>
    </div>
    </section>