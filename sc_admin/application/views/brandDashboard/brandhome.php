<script src="<?php echo base_url(); ?>assets/js/dbreport.js"></script>
<script type="text/Javascript">
$(document).ready(function(e){
		
		$("body").addClass('sidebar-collapse');
});
</script>
<section class="content-header">
  <h1>Welcome <?php echo $this->session->userdata('first_name').' '.$this->session->userdata('last_name'); ?></h1>
  <!--ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Dashboard</a></li>
  </ol-->
</section>
<section class="content">
  <form name="myform" id="myform" class="frm-search" method="post">
    <div class="row">
      <div class="col-md-2">
        <div class="input-group">
          <input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm" id="date_from" value="<?php echo set_value('date_from'); ?>">
        </div>
      </div>
      <div class="col-md-2">
        <div class="input-group">
          <input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="<?php echo set_value('date_to'); ?>">
          <div class="input-group-btn">
            <button type="submit" class="btn btn-sm btn-default" id="submit"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </div>
      <div class="col-md-2">
        <a class="btn btn-default btn-sm" href="<?php echo base_url(); ?>home"><i class="fa fa-refresh"></i> Reset</a>
      </div>
    </div>
  </form>

  <br>
  <div class="row">

  	<div class="col-md-3">

  	<div class="box box-report">
          <div class="box-header with-border">
            <h3 class="box-title">Order Status</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
          </div>

	  	<div class="box-body">
	            <div class="row">
	              <div class="col-md-12">
	                <a href="<?php echo base_url(); ?>manage_orders" target="_blank"><div class="chart-responsive">
	                  <canvas id="pieChart" height="150"></canvas>
	                </div></a>
	              </div>

	            </div>
	  	</div>

	  	<div class="box-footer no-padding">
            <ul class="nav-report" style="padding-bottom:168px;">
                <li>
                                  <i class="fa fa-circle text-light-blue1" style="color:#f39c12;"></i>
                                  Processing
                                  <a href="<?php echo base_url(); ?>/manage_orders" target="_blank"><span class="label pull-right"><?php echo $processing; ?></span></a>
                </li>
                <li><i class="fa fa-circle text-green"></i> Confirmed
                                  <a href="<?php echo base_url(); ?>/manage_orders" target="_blank"><span class="label pull-right"><?php echo $confirmed; ?></span></a>
                </li>
                <li><i class="fa fa-circle text-green1" style="color:#00c0ef;"></i> Dispatched
                                 <a href="<?php echo base_url(); ?>/manage_orders" target="_blank"><span class="label pull-right"><?php echo $dispatched; ?></span></a>
                </li>
                <li><i class="fa fa-circle text-green1" style="color:#990099;"></i> Delivered
                                  <a href="<?php echo base_url(); ?>/manage_orders" target="_blank"><span class="label pull-right"><?php echo $delivered; ?></span></a>
                </li>
                <li><i class="fa fa-circle text-red"></i> Cancelled
                                  <a href="<?php echo base_url(); ?>/manage_orders" target="_blank"><span class="label pull-right"><?php echo $cancelled; ?></span></a>
                </li>
                <li><i class="fa fa-circle text-green1" style="color:#3266CC;"></i> Return
                                  <a href="<?php echo base_url(); ?>/manage_orders" target="_blank"><span class="label pull-right"><?php echo $returned; ?></span></a>
                </li>
                <li><b>Total Orders</b><a href="<?php echo base_url(); ?>/manage_orders" target="_blank"><span class="label pull-right"><?php echo $processing+$confirmed+$dispatched+$delivered+$cancelled+$returned; ?></span></a>
            </ul>

        </div>

	  	</div>
	 </div>

    <!--div class="col-md-3">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-money"></i></span>
                <div class="info-box-content">
                <span class="info-box-text">Total Sales</span>
                <span class="info-box-number">&#8377;<?php echo $basic_details['total_sales'] ? $basic_details['total_sales'] : 0; ?></span> 
                <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>brandDashboard/total_sales/<?php echo set_value('date_from') ? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to') ? set_value('date_to') : '0'; ?>">More info <i class="fa fa-arrow-circle-right"></i></a>
                     
                </div>
        </div>
    </div-->

    <div class="col-md-3">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-shopping-basket"></i></span>
                <div class="info-box-content">
                <span class="info-box-text">Products</span>
                <span class="info-box-number"><?php echo $basic_details['total_products']; ?></span> 
                <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>brandDashboard/products/<?php echo set_value('date_from') ? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to') ? set_value('date_to') : '0'; ?>">More info <i class="fa fa-arrow-circle-right"></i></a>
                     
                </div>
        </div>
    </div>

    <!--div class="col-md-3">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-shopping-basket"></i></span>
                <div class="info-box-content">
                <span class="info-box-text">Approved Products</span>
                <span class="info-box-number"><?php echo $basic_details['approved_products']; ?></span> 
                <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>brandDashboard/approved_products/<?php echo set_value('date_from') ? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to') ? set_value('date_to') : '0'; ?>">More info <i class="fa fa-arrow-circle-right"></i></a>
                     
                </div>
        </div>
    </div-->

        <div class="col-md-3">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-shopping-basket"></i></span>
                <div class="info-box-content">
                <span class="info-box-text">Looks</span>
                <span class="info-box-number"><?php echo $basic_details['looks_created']; ?></span> 
               	<a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>brandDashboard/looks/<?php echo set_value('date_from') ? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to') ? set_value('date_to') : '0'; ?>">More info <i class="fa fa-arrow-circle-right"></i></a>
                     
                </div>
        </div>
    </div>

    <!--div class="col-md-3">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-shopping-basket"></i></span>
                <div class="info-box-content">
                <span class="info-box-text">Products Used</span>
                <span class="info-box-number"><?php echo $basic_details['product_used_in_looks']; ?></span> 
                <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>brandDashboard/view_products_used/<?php echo set_value('date_from') ? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to') ? set_value('date_to') : '0'; ?>">More info <i class="fa fa-arrow-circle-right"></i></a>
                     
                </div>
        </div>
    </div-->

    <div class="col-md-3">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-shopping-basket"></i></span>
                <div class="info-box-content">
                <span class="info-box-text">Product Click</span>
                <span class="info-box-number"><?php echo $basic_details['cpc_track']; ?></span> 
                <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>brandDashboard/view_products_click/<?php echo set_value('date_from') ? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to') ? set_value('date_to') : '0'; ?>">More info <i class="fa fa-arrow-circle-right"></i></a>
                     
                </div>
        </div>
    </div>

        <!--div class="col-md-3">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-money"></i></span>
                <div class="info-box-content">
                <span class="info-box-text">Total Click Value</span>
                <span class="info-box-number">&#8377;<?php echo $basic_details['cpa_price_track'] ? $basic_details['cpa_price_track'] : 0; ?></span> 
                <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>brandDashboard/view_products_click/<?php echo set_value('date_from') ? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to') ? set_value('date_to') : '0'; ?>">More info <i class="fa fa-arrow-circle-right"></i></a>
                     
                </div>
        </div>
    </div-->

     <!--div class="col-md-3">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-money"></i></span>
                <div class="info-box-content">
                <span class="info-box-text">CPC</span>
                <span class="info-box-number">&#8377;<?php echo $basic_details['cpc']; ?></span> 
                <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>brandDashboard/view_products_click/<?php echo set_value('date_from') ? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to') ? set_value('date_to') : '0'; ?>">More info <i class="fa fa-arrow-circle-right"></i></a>
                     
                </div>
        </div>
    </div-->

    <div class="col-md-3">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-shopping-basket"></i></span>
                <div class="info-box-content">
                <span class="info-box-text">Total Order</span>
                <span class="info-box-number"><?php //echo $basic_details['total_orders']; ?></span> 
                <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>manage_orders">More info <i class="fa fa-arrow-circle-right"></i></a>
                     
                </div>
        </div>
    </div>

    <!--div class="col-md-6">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-thumbs-up"></i></span>
                <div class="info-box-content">
                <span class="info-box-text">Top 5</span>
                <span class="info-box-text">(PRODUCT BUY | PRODUCT CLICK)</span>
                <span class="info-box-number"></span> 
                <a class="small-box-footer" target="_blank" href="<?php echo base_url(); ?>brandDashboard/top_five/<?php echo set_value('date_from') ? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to') ? set_value('date_to') : '0'; ?>">More info <i class="fa fa-arrow-circle-right"></i></a>
                     
                </div>
        </div>
    </div-->

   </div>
</section>

<script src="<?php echo base_url(); ?>assets/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $("#date_from").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true});
        $("#date_to").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true });
        users_click_info();

         function users_click_info(){
                var pieChartCanvas = $("#pieChart").get(0).getContext("2d");

                                  var pieChart = new Chart(pieChartCanvas);
                                  var PieData = <?php echo $clicks; ?>;
                                  var pieOptions = {
                                    //Boolean - Whether we should show a stroke on each segment
                                    segmentShowStroke: true,
                                    //String - The colour of each segment stroke
                                    segmentStrokeColor: "#fff",
                                    //Number - The width of each segment stroke
                                    segmentStrokeWidth: 0.0001,
                                    //Number - The percentage of the chart that we cut out of the middle
                                    percentageInnerCutout: 0, // This is 0 for Pie charts
                                    //Number - Amount of animation steps
                                    animationSteps: 100,
                                    //String - Animation easing effect
                                    animationEasing: "easeOutBounce",
                                    //Boolean - Whether we animate the rotation of the Doughnut
                                    animateRotate: true,
                                    //Boolean - Whether we animate scaling the Doughnut from the centre
                                    animateScale: false,
                                    //Boolean - whether to make the chart responsive to window resizing
                                    responsive: true,
                                    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                                    maintainAspectRatio: false,
                                    //String - A legend template
                                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
                                    //String - A tooltip template
                                    tooltipTemplate: "<%=value %> <%=label%> Clicks"
                                  };
                                  //Create pie or douhnut chart
                                  // You can switch between pie and douhnut using the method below.
                                  pieChart.Doughnut(PieData, pieOptions);
                                  //-----------------
                                  //- END PIE CHART -
                                  //-----------------
        }
    });
</script>