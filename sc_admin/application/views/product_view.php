<?php
    if(!empty($this->data['users_menu']!=""))
    {
       $users_menu=$this->data['users_menu'];

    }else
    {
       $users_menu=array();
    }

    if(!empty($this->data['total_rows']))
    {
      $total_rows = $this->data['total_rows'];

    }else
    {
       $total_rows="";
    }
    //echo "<pre>"; print_r($product);exit;
?>
  <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Manage Products</h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>home">Home</a></li>
      <li><a href="#">Products</a></li>
      <li class="active">Manage Products</li>
    </ol>
</section>
<section class="content">
        <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">List of Products <small class="label label-info"><?php echo @$total_rows; ?></small></h3>
                	<div class="pull-right">
                    <?php if(in_array("40",@$users_menu)) { ?>
                      <a href="<?php echo base_url(); ?>product/product_addedit"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add Product</button></a>
                    <?php } ?>
                    </div>
                  </div>
                    <div class="box-body">
                         <?php echo $this->session->flashdata('feedback'); ?>
                    <form name="frmsearch" class="form-group" id="frmsearch" action="<?php echo base_url(); ?>product" method="post">
                      <div class="row">
                   <div class="col-md-2">
                            <select name="search_by" id="search_by" class="form-control input-sm">
                              <option value="0">Search by</option>                            
                              <option value="1" <?php echo set_value('search_by')==1 ? 'selected' : ''; ?>>Product Name</option>
                              <option value="2" <?php echo set_value('search_by')==2 ? 'selected' : ''; ?>>Category</option>
                              <option value="3" <?php echo set_value('search_by')==3 ? 'selected' : ''; ?>>Sub-Category</option>
                              <!--option value="4" <?php echo set_value('search_by')==4 ? 'selected' : ''; ?>>Created Date On</option-->
                              <option value="5" <?php echo set_value('search_by')==5 ? 'selected' : ''; ?>>Status</option>
                            </select>
                        </div>
    										<div class="col-md-3">
    												<div class="input-group">
                          <input type="text" name="table_search" id="table_search" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo set_value('table_search'); ?>"/>
                            <div class="input-group-btn">
                              <button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </div>

                        </div>
    										<div class="col-md-7">
    												<div class="pull-right">
                              <?php echo $this->pagination->create_links(); ?>

                          </div>
                        </div>
                        </div>
                    </form>


        <div>
				  <div class="row">

					<?php 

          if(!empty($product))
          {
            foreach($product as $singleproduct)
  					{
					?>
        				<div class="col-md-6">
        				  <div class="product-row-wrp manage-look-wrp">
          				   <div class="row">
              					<div class="col-md-4">
              					<div class="product-img-wrp">
              					<img src="<?php echo base_url(); ?>assets/products/thumb/<?php echo $singleproduct->image; ?>">
                      <!-- <img src="assets/images/image-na.png">-->
              					</div>
              					</div>
              				   <div class="col-md-8">
                                 <dl class="dl-horizontal">
                                  <dt style="width:100px;">Product Name</dt>
                                  <dd><?php echo $singleproduct->name; ?></dd>
                                  <dt style="width:100px;">Category</dt>
                                  <dd><?php echo $singleproduct->cat_name; ?></dd>
                                  <dt style="width:100px;">Sub-Category</dt>
                                  <dd><?php echo $singleproduct->subcat_name; ?></dd>
                                  <dt style="width:100px;"> Date </dt>
                                  <dd><?php echo $singleproduct->created_datetime; ?></dd>
                                  <dt style="width:100px;">Product Status</dt>
                                  <?php if($singleproduct->approve_reject=='A')
                                        {
                                          $apr_status = '<span class="text-green">Approved</span>';
                                        }else
                                        {
                                            if($singleproduct->approve_reject=='R')
                                            {
                                              $apr_status = '<span class="text-red">Reject</span>';
                                            }else
                                            {
                                              $apr_status = '<span class="text-orange">Pending</span>';
                                            }
                                        }
                                    ?>
                                  <dd><?php echo $apr_status; ?></dd>                               
                                </dl>
                            <div class="box-footer11">
                                <?php if(in_array("39",$users_menu)) { ?>     
                                   <a href="<?php echo base_url(); ?>product/product_addedit/<?php echo $singleproduct->id; ?>/verify"><button class="btn btn-primary btn-sm"  type="submit">View</button></a>
                                <?php } ?>
                                <?php if(in_array("41",$users_menu) || in_array("43",$users_menu)) { ?>  
                                  <a href="<?php echo base_url(); ?>product/product_addedit/<?php echo $singleproduct->id; ?>"><button class="btn btn-primary btn-sm editProd" type="button"> <i class="fa fa-edit"></i>Edit</button></a>
                                <?php } ?>
                                <?php if(in_array("42",$users_menu)) { ?>
                        					<button  type="button" class="btn btn-primary btn-sm btn-delete deleteProd"  data-id="<?php echo $singleproduct->id; ?>" data-toggle="confirmation" ><i class="fa fa-trash-o"></i> Delete</button>
                                <?php } ?>                        				
                            </div>
              				   </div>
          				    </div>
						   </div>
        				</div>
				<?php
					}
					}else
          {
        ?>
              <div class="col-md-offset-4">No Records Found.</div>   
        <?php
          }
        ?>       


				  </div>
				</div><!-- /.box-body -->
      </div>
		 </div>
		</div>
  </section><!-- /.content -->

  <script type="text/Javascript">
	$( document ).ready(function() {
		$('[data-toggle=confirmation]').confirmation({
			title:'Are you sure?',
			onConfirm : function(){
				//var del_id = $(this).closest('td').find('.deleteProd').attr('data-id');
				var del_id = $('.deleteProd').attr('data-id');
				$.ajax({
					 type: "POST",
					 url: "<?php echo base_url(); ?>"+"product/delete_product/",
					 data: { prodID: del_id },
					 cache:false,
					 success:
						  function(data){
							location.reload(); //as a debugging message.
						  }

					});
				}
			});
	});

/*	$( document ).ready(function() {
		$('[data-toggle=confirmation]').confirmation({
			title:'Are you sure?',
			onConfirm : function(){
				var p_id = $(this).closest('td').find('.approveRejectProd').attr('data-id');
				$.ajax({
					 type: "POST",
					 url: "<?php echo base_url(); ?>"+"product/approveReject_product/",
					 data: { prodID: p_id },
					 cache:false,
					 success:
						  function(data){
							location.reload(); //as a debugging message.
						  }

					});
				}
			});

	$('.nav-item-product').addClass("active");

	});*/
</script>
