<script src="<?php echo base_url(); ?>assets/js/data_custom.js" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>assets/css/data_style.css" rel="stylesheet" type="text/css" />

<!-- search select box implementation -->
<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js" type="text/javascript"></script>


<script type="text/javascript">
var config = {
  '.chosen-select'           : {},
  '.chosen-select-deselect'  : {allow_single_deselect:true},
  '.chosen-select-no-single' : {disable_search_threshold:10},
  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
  '.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}


$(document).ready(function() {


  //for image upload
  $("#images").fileinput({
    'showPreview' : true,
    'allowedFileExtensions' : ['jpg', 'jpeg', 'png','gif'],
    'elErrorContainer': '#errorBlock',
    'maxFileSize' : 2048,
    'showUpload' : true,
    'layoutTemplates':'modal',
    'uploadAsync': false,
    'uploadUrl': "<?php echo site_url().'Stickerupload/upload_images'; ?>" ,
    'fileActionSettings' :{
      'uploadIcon' : '',
      'uploadClass' : '',
      'uploadTitle' : '',
      'initialPreview': [
        "<img src='/images/desert.jpg' class='file-preview-image' alt='Desert' title='Desert'>",
        "<img src='/images/jellyfish.jpg' class='file-preview-image' alt='Jelly Fish' title='Jelly Fish'>",
      ],

    },
    ajaxSettings:{
      success : function(){
        location.reload();
      }
    }


  });
});
</script>
<section class="content-header">
  <h1>Upload Sticker</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">Upload Sticker</li>
  </ol>
</section>
<section class="content page-image-upload ss-image-upload">

  <div id="image-upload">
    <form role="form" name="frmuploadimg" id="frmuploadimg"  method="post" enctype="multipart/form-data">
      <div class="box">
        <div class="box-body">
          <div class="box-drag-drop">
            <input id="images" name="images[]" class="file" type="file" multiple >
            <span class="text-red"> </span>
          </div>
        </div>
      </div>


      <div class="box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Images <small class="label label-info"></small></h3>
          <div class="pull-right">
            <?php echo $this->pagination->create_links(); ?>
          </div>
        </div>
        <div class="box-body">
          <div class="image-thumbs">
            <ul>
              <?php if(!empty($street_pic)) { ?>
                <?php foreach($street_pic as $val) { ?>
                  <li>
                    <img src="<?php echo base_url(); ?>/assets/backgrounds/<?php echo $val['img_name']; ?>">
                    <i class="fa fa-times-circle-o" onclick="delete_img(<?php echo $val['id']; ?>)"></i>
                  </li>
                  <?php } ?>
                  <?php } ?>
                </ul>
              </div>
              <div><?php echo $this->pagination->create_links(); ?></div>
            </div>
          </div>

        </form>
      </div>
    </section><!-- /.content -->
    <link href="<?php echo base_url(); ?>assets/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <script src="<?php echo base_url(); ?>assets/js/fileinput.min.js" type="text/javascript"></script>
    <script type="text/Javascript">
    function delete_img(id){
      var result = confirm("Want to delete?");
      if (result) {
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "Stickerupload/delete_street_pic",
          data: { id: id },
          cache:false,
          success:
          function(data){
            location.reload(); //as a debugging message.
          }

        });
      }
    }

    $('.nav-item-look').addClass("active");
    $('.nav-upload-ss').addClass("active");
    </script>
