<?php // echo "<pre>";print_r($stores);

//echo "<pre>".$prdvar[0]['product_var_type_id'];
//echo $productData[0]->name;
//print_r($prdtag);exit;
//print_r($prdvar);exit;
//echo "<pre>";print_r($stores);exit;
//echo $prdvar[0]['product_var_val_id'];exit;
//print_r($prdstore);exit;
echo validation_errors();
$users_menu=$this->data['users_menu'];

if(!empty(@$productData[0]))
{
  $page="Edit";

}else
{
  $page="Add";
}
?>
<style>
.block-flat, .block-wizard {
  margin-bottom: 40px;
  padding: 20px 20px;
  background: #FFF;
  border-radius: 3px;
  -webkit-border-radius: 3px;
  border-left: 1px solid #efefef;
  border-right: 1px solid #efefef;
  border-bottom: 1px solid #e2e2e2;
  box-shadow: 0 1px 0 0 rgba(0, 0, 0, 0.04);
}
</style>


<script>
   $(document).ready(function() {
    //for first textbox focus.

      //for image upload
      $("#images").fileinput({
          'showPreview' : true,
          'maxFileCount' : 1,
          'allowedFileExtensions' : ['jpg', 'jpeg', 'png','gif'],
          'elErrorContainer': '#errorBlock',
          'maxFileSize' : 2048,
          'showUpload' : true,
          'layoutTemplates':'modal',
          'uploadAsync': false,
          'uploadUrl': "<?php echo site_url('product/product_addedit'); ?>" ,
          'fileActionSettings' :{
          'uploadIcon' : '',
          'uploadClass' : '',
          'uploadTitle' : '',
          'initialPreview': [
              "<img src = '/assets/products/thumb/<?php echo @$productData[0]->image; ?>'  class='file-preview-image' alt='<?php echo @$productData[0]->image; ?>' title='<?php echo @$productData[0]->image; ?>'>",

           ],
          }
      });
   });

 $(document).on('change','#category',function(){
     get_sub_cat('');
});

/* $(document).on('change','#sub_category',function(){
      get_var_type('');

  });*/

 toggleDiv = function(id){
  var variation_type = {variation_type:id};
  $("#var_type_img_"+id).show();

    var prd_var_val = 0;

  <?php if(!empty($prdvar[0]['product_var_val_id']) && $prdvar[0]['product_var_val_id']>0) { ?>
    prd_var_val = <?php echo $prdvar[0]['product_var_val_id'];  ?>;
  <?php }else{ ?>
    prd_var_val = 0;
  <?php } ?>

  $.ajax({
            type: "POST",
            url: "<?php echo site_url('product/get_variation_value'); ?>",
            data:variation_type,
            dataType:"json",//return type expected as json
            success: function(varvalue){

              var checked_val = '';


              $( '#variation_typevalue_'+id ).toggle( "slow", function() {});
             // $("#var_type_img_"+id).hi = e();
             $('#variation_typevalue_'+id).html('');
              for (var i = 0; i < varvalue.length; i++) {
                 if(varvalue[i]['id'] == prd_var_val){ checked_val = 'checked'; }else{checked_val = ''; }
                  var varvalueString = '<div class="col-md-6"><div class="checkbox"><label><input name="variation_value_'+i+'[]" type="checkbox" value="'+varvalue[i]['id']+'" id="variation_value_check_'+varvalue[i]['id']+'" '+checked_val+'>';
                  varvalueString += varvalue[i]['name'];
                  varvalueString += '</label> </div></div>';
                  $('#variation_typevalue_'+id).append(varvalueString);
                }
            },
        });

 }

function get_sub_cat(id){

  if($('#category option:selected').val() == 0){

        var opt = $('<option />');
        opt.val(0);
        opt.text('Sub Category');
        $('#sub_category').empty().append(opt);

      }else{

        var category_id = { category:$('#category option:selected').val() };

        $.ajax({
            type: "POST",
            url: "<?php echo site_url('product/get_sub_category'); ?>",
            data:{catid : category_id },
            dataType:"json",//return type expected as json
            success: function(subcat){
                var opt = $('<option />');
                opt.val(0);
                opt.text('Sub Category');
                $('#sub_category').empty().append(opt);
                var subcat1 = <?php if(!empty(@$productData[0]->product_sub_cat_id)) echo @$productData[0]->product_sub_cat_id; else echo 0; ?>;

                for (var i = 0; i < subcat.length; i++) {
                    var opt = $('<option />');
                    opt.val(subcat[i]['id']);
                    opt.text(subcat[i]['name']);
                    if(subcat[i]['id'] == subcat1){
                      opt.attr('selected','selected');
                    }else{
                      opt.attr('selected',false);
                    }
                    $('#sub_category').append(opt);
                }
              //console.log(subcat[0].id);
            },
        });
      }

      //alert($('#sub_category').val());
  }

  function get_var_type(id)
  {
     var prd_var_type =0;
     var vartype = [];
     alert("Category is loading...");
     if($('#sub_category option:selected').val() == 0 ){
      $('#variation_type').html('');
       }
       else{
          var subcategory_id = { subcategory:$('#sub_category option:selected').val() };
          <?php if(!empty($prdvar[0]['product_var_type_id']) && $prdvar[0]['product_var_type_id']>0) { ?>
            prd_var_type = <?php echo $prdvar[0]['product_var_type_id'];  ?>;
          <?php }else{ ?>
            prd_var_type = 0;
          <?php } ?>

          $.ajax({
                type: "POST",
                url: "<?php echo site_url('product/get_variation_type'); ?>",
                data:subcategory_id,
                dataType:"json",//return type expected as json
                success: function(vartype){
                  $('#variation_type').html('');   var checked = '';
                  for (var i = 0; i < vartype.length; i++) {
                      if(vartype[i]['id']!='' && (vartype[i]['id'] == prd_var_type)){ checked = 'checked'; }else{checked = ''; }

                      var vartypeString = '<div class="col-md-4"><div class="box box-warning box-solid"><div class="box-header with-border"><h3 class="box-title"> <input name="variation_type[]"  onClick="toggleDiv('+vartype[i]['id']+')" id="variation_type_check_'+vartype[i]['id']+'" value="'+vartype[i]['id']+'" type="checkbox" '+checked+'/>';

                        vartypeString += vartype[i]['name'];

                        vartypeString +='</h3><div class="box-tools pull-right"> '

                        vartypeString += '</div></div><div class="box-body" style="display: block;"><div style="display: none;" class="row" id="variation_typevalue_';

                        vartypeString += vartype[i]['id'];

                        vartypeString += '"><img src="" style="display:none;" alt="loading..." id=var_type_img_"'+vartype[i]['id']+'"</div></div></div></div>';

                      $('#variation_type').append(vartypeString);

                    //console.log(subcat[0].id);

                  }
                   toggleDiv(prd_var_type);
                },
            });
        }
    }
</script>
<?php
//echo form_open('productvariation'); ?>
<!-- Content Header (Page header) -->

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><?php echo $page; ?> Product</h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>/product"><i class="fa fa-dashboard"></i> Product</a></li>
      <li class="active">Here</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  <div><?php //echo validation_errors(); ?></div>
  <div class="row">
    <div class="col-md-12">
      <div class="box ">
        <div class="box-header">
          <h3 class="box-title"></h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" id="product_form" enctype="multipart/form-data" method="POST">
          <input type="hidden" class="form-control" id="id" placeholder="product_id" name="product_id" value="<?php echo @$productData[0]->id; ?>" />
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="ProductName">Product Name <span class="text-red">*</span></label>
                  <input  type="text" name="product_name" tabindex="1" id="product_name" placeholder="Enter Product Name" class="form-control" value="<?php if(@$productData[0]->name!="") echo @$productData[0]->name; else echo set_value('product_name');?>" >
                  <span class="text-red"> <?php echo form_error('product_name'); ?></span>
                </div>

                <div class="form-group">
                  <label for="ProductDesc">Product Description</label>
                  <textarea tabindex="3" name="product_desc" id="product_desc" placeholder="Enter Description" rows="3" class="form-control"><?php if(@$productData[0]->description!="") echo @$productData[0]->description; else echo set_value('product_desc');?></textarea>
                </div>

                <div class="form-group">
                  <label for="Brand">Brand  </label>
                  <select tabindex="5"  required="Please select any brand" id="brand" name="brand" class="form-control" tabindex="-1">
                    <option>Select Brand</option>
                    <?php
                        foreach($brand as $singlebrand)
                        {
                    ?>

                            <option value="<?php echo $singlebrand['id'];?>"
                             <?php if(@$productData[0]->brand_id == @$singlebrand['id']) echo "selected";?>
                            ><?php echo $singlebrand['user_name'];?></option>
                    <?php
                        }
                    ?>
                  </select>
                </div>

                <div class="form-group">
                  <label for="category">Category  <span class="text-red">*</span></label>
                  <select class="form-control" tabindex="7"   name="category" id="category">
                    <option value="0">Select Category</option>
                    <?php if(empty($category)){ //check whether the category array is empty.
                    ?> <option value="0">No Category Yet</option><?php }
                    else{ // to load the category array.

                      foreach($category as $singlecat)
                      {
                        ?>
                          <option value="<?php echo $singlecat->id;?>"
                            <?php if(@$productData[0]->product_cat_id == @$singlecat->id) echo "selected";?>
                            ><?php echo $singlecat->name;?></option>
                        <?php
                      }
                      ?>
                    <?php //category array ends
                    }
                    ?>
                  </select>
                   <span class="text-red"> <?php echo form_error('category'); ?></span>
                </div>

                <div class="form-group">
                  <label for="subCategory">Sub Category  <span class="text-red">*</span></label>
                  <select tabindex="9" class="form-control"  name="sub_category" id="sub_category">
                    <option value="0">Select Subcategory</option>
                  </select>
                  <span class="text-red"> <?php echo form_error('sub_category'); ?></span>
                </div>

                <div class="form-group">
                  <div class="row" id="variation_type"></div>
                </div>

                <div class="form-group">
                  <label for="ProductURL">Product URL </label>
                  <input type="text" tabindex="15" placeholder="Enter Product URL" id="product_url" name="product_url" class="form-control" value="<?php if(@$productData[0]->url!="") echo @$productData[0]->url; else echo set_value('product_url');?>" >
                </div>

                <div class="form-group">
                  <label for="productTags">Tags</label>
                  <select placeholder="Select Tags" tabindex="19" data-placeholder="Choose Tags" name="product_tags[]" id="product_tags" class="form-control chosen-select" multiple tabindex="">
                    <?php
                      if(!empty($tag))//check whether the tag array is empty.
                     {
                      foreach($tag as $singletag)
                      {
                      ?>
                         <!--option value="<?php echo $singletag->id;?>"
                          <?php if(in_array($singletag->id,$prdtag['tag_id'])) echo selected; ?>
                          ><?php echo $singletag->name;?></option-->

                          <?php if(in_array($singletag->id, array_column($prdtag, 'tag_id'))) { ?>
                           <option value="<?php echo $singletag->id;?>" selected><?php echo $singletag->name;?></option>
                          <?php }else{ ?>
                            <option value="<?php echo $singletag->id;?>" ><?php echo $singletag->name;?></option>
                          <?php } ?>
                      <?php
                      }
                     }
                    ?>
                    </select>
                </div>

                <!--div class="form-group">
                  <label for="exampleInputEmail1">Tags </label>
                  <input type="text" placeholder="Enter Tags" id="exampleInputEmail1" class="form-control">
                </div-->
                <div class="form-group">
                  <label for="budget">Price Range  <span class="text-red">*</span></label>
                  <?php echo form_dropdown('budget', $budget, @$productData[0]->price_range, 'tabindex="10" class="form-control" id="budget"');?>
                  <span class="text-red"> <?php echo form_error('budget'); ?></span>
                </div>

                <div class="form-group">
                  <label for="Price">Price <span class="text-red">*</span></label>
                  <input type="text" tabindex="11" placeholder="Enter Price" name="product_price" id="product_price" class="form-control" value="<?php if(@$productData[0]->price!="") echo @$productData[0]->price; else echo set_value('product_price');?>"> <span class="text-red"> <?php echo form_error('product_price'); ?></span>
                </div>

                <div class="form-group">
                  <label for="targetMarket">Target Market</label>
                  <select placeholder="Select Target Market" tabindex="23" id="product_target_market" name="product_target_market" class="form-control chosen-select" >
                     <?php
                      if(!empty($target_market))
                     { //check whether the target_market array is empty.
                        foreach($target_market as $singletarget)
                        {
                     ?>
                           <option value="<?php echo $singletarget->id;?>"><?php echo $singletarget->target_market;?></option>
                    <?php
                        }
                     }
                    ?>
                  </select>
                </div>

                 <div class="form-group">
                  <label for="ConsumeType">Consumer Type</label>
                   <?php echo form_dropdown('product_consumer_type', $consumer_type, @$productData[0]->consumer_type, 'tabindex="8" class="form-control" id="product_consumer_type"');?>
                </div>

                <div class="form-group">
                  <label for="ProductAvail">Product Availability  <span class="text-red">*</span></label>
                  <select placeholder="Select Product Availabilty" tabindex="27" id="product_avail" value="" name="product_avail[]" class="form-control chosen-select" multiple>
                    <?php
                      if(!empty($stores))
                     { //check whether the target_market array is empty.

                        foreach($stores as $singlestore)
                        {
;
                     ?>
                           <option value="<?php echo $singlestore->id;?>"
                            <?php
                              if(!empty($prdstore))
                              {
                                if(in_array($singlestore->id,array_column($prdstore,'store_id')))
                                  {
                                    echo "selected";
                                  }
                              }
                            ?>
                            ><?php echo $singlestore->brand_store_name;?></option>

                    <?php
                        }
                     }
                    ?>
                  </select>
                  <span class="text-red"> <?php echo form_error('product_avail'); ?></span>
                </div>

                <div class="form-group">
                  <label for="Rating">Rating</label>
                  <input type="text" tabindex="10" placeholder="Enter your rating" name="product_rate" id="product_rate" class="form-control" value="<?php echo  @$productData[0]->rating;?>" >
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="Uploadimage">Upload Product Image</label>
                  <!--input type="file" id="product_image" name="product_image" tabindex="13"-->
                  <p class="help-block">Max file size: 2MB.</p>
                  <p class="help-block">Allowed format: jpg jpeg png gif</p>
                 <?php  if($page=='Edit')
                 { ?>
                  <p><img src="<?php echo base_url();?>assets/products/thumb/<?php  echo  @$productData[0]->image;?>"  ></p>
                <?php } ?>
                </div>
                <div class="form-group">
                    <input id="images" name="images" class="file" type="file" >
                     <span class="text-red"> <?php if(isset($product_image_error)) echo $product_image_error;?></span>
                </div>

                <div class="form-group">
                 <label for="BodyPart">Body Part</label>
                  <?php echo form_dropdown('body_part', $body_part, @$productData[0]->body_part, 'tabindex="8" class="form-control" id="body_part"');?>
                </div>
                <div class="form-group">
                 <label for="BodyType">Body Type</label>
                  <?php echo form_dropdown('body_type', $body_type, @$productData[0]->body_type, 'tabindex="8" class="form-control" id="body_type"');?>
                </div>
                <div class="form-group">
                 <label for="BodyShape">Body Shape</label>
                  <?php echo form_dropdown('body_shape', $body_shape, @$productData[0]->body_shape, 'tabindex="9" class="form-control" id="body_shape"');?>
                </div>

                <div class="form-group">
                 <label for="Personality">Personality</label>
                  <?php echo form_dropdown('personality', $personality, @$productData[0]->personality, 'tabindex="10" class="form-control" id="personality"');?>
                </div>

                 <div class="form-group">
                 <label for="age">Age Group</label>
                  <?php echo form_dropdown('age', $age, @$productData[0]->age_group, 'tabindex="11" class="form-control" id="age"');?>
                </div>

                <div class="form-group">
                  <label class=" control-label">Promotional <span class="text-red">*</span></label> &nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="radio-inline"> <input type="radio"  name="promotional" value="1" <?php if(@$productData[0]->is_promotional == '1') echo "checked"; ?> >Yes</label>
                    <label class="radio-inline"> <input type="radio" name="promotional" value="0" <?php if(@$productData[0]->is_promotional == '0') echo "checked";  ?> > No</label>
                     <span class="text-red"> <?php echo form_error('promotional'); ?></span>
                </div>

                <div class="form-group">
                  <label class=" control-label">Active <span class="text-red">*</span></label> &nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="radio-inline"> <input type="radio"  name="active" value="1" <?php if(@$productData[0]->status == '1') echo "checked";  ?> >Yes</label>
                    <label class="radio-inline"> <input type="radio" name="active" value="0" <?php if(@$productData[0]->status == '0') echo "checked"; ?> > No</label>
                    <span class="text-red"> <?php echo form_error('active'); ?></span>
                </div>

                <div id="res" style="display:none;">
                <div class="form-group">
                  <label class=" control-label">Reason</label>
                  <div>
                    <textarea name="reason" class="form-control" id="reason"></textarea>
                  </div>
                </div>
                </div>

              </div>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer col-md-offset-4" >
          <?php
          if(in_array("40",$users_menu) || in_array("41",$users_menu) ) {
           ?>
            <button class="btn btn-primary" tabindex="30" type="submit" >Save</button>

            <button class="btn btn-primary" tabindex="31" type="reset">Cancel</button>
           <?php } ?>

          <?php
          if(in_array("43",$users_menu)) {
           ?>
            <input type="button" class="btn btn-primary" name="approve" value="Approve" onclick="approve_product();">
            <input type="button" class="btn btn-primary" name="reject" value="Reject" onclick="reject_product();">
            <!--input type="button" class="btn btn-primary" name="submit" value="Submit" -->
          <?php } ?>

          <a href="<?php echo base_url(); ?>product" ><button type="button" class="btn btn-primary btn-rad"><i class="fa fa-reply"></i> Back</button></a>
          <!-- <button class="btn btn-primary" type="submit">Edit</button>
          <button class="btn btn-primary" type="submit">Approved/Pending</button> -->
        </div>
        </form>
      </div>
    </div>
  </div>
</section><!-- /.content -->
<script type="text/javascript">

      var config = {
        '.chosen-select'           : {},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
        '.chosen-select-width'     : {width:"95%"}
      }
      for (var selector in config) {
        $(selector).chosen(config[selector]);
      }

/*==function  to approve product==*/
function approve_product()
{
    var product_id = document.getElementById('id').value;
    //alert(product_id);

    var conf = confirm('Are you sure you want to approve');
    if(conf)
    {
      $.ajax({
          type:'POST',
          url:'<?php echo base_url();?>product/approveReject_product',
          data:{apr_status:'approved',prodID:product_id},
          dataType:'html',
          success: function(data)
          {//alert(data);
            if(data==1)
            {
              alert('Approved successfully');
              window.location = '<?php echo base_url();?>product';
            }
            else
            {
              alert('OOPS!something went wrong');
            }
          }
      });
    }
}

/*
function reject_product()
{
    $("#res").toggle();
    $("#reason").val("");
}*/
function reject_product()
{
 //  $("#res").toggle();
 //  $("#reason").val("");
 $("#res").show();
    //var product_id = document.getElementById('id').value;
 var product_id = $('#id').val();
 var reason = $('#reason').val();
// alert(reason);

  if(reason=='')
  {
     alert('Please enter reason');
  }
  else
  {
     var conf = confirm('Are you sure you want to reject');
     if(conf)
     {
       $.ajax({
           type:'POST',
           url:'<?php echo base_url();?>product/approveReject_product',
           data:{apr_status:'reject',prodID:product_id,reason:reason},
           success: function(data)
           {
             if(data==1)
             {
               alert('Rejected successfully');
               window.location = '<?php echo base_url();?>product';
             }
             else
             {
               alert('OOPS!something went wrong');
             }
           }
       });
     }
  }
}
/*
function reject_reason()
{
  //var product_id = document.getElementById('id').value;
  var product_id = $('#id').val();
  var reason = $('#reason').val();
 // alert(reason);

   if(reason=='')
   {
      alert('Please enter reason');
   }
   else
   {
      var conf = confirm('Are you sure you want to reject');
      if(conf)
      {
        $.ajax({
            type:'POST',
            url:'<?php echo base_url();?>product/approveReject_product',
            data:{apr_status:'reject',prodID:product_id,reason:reason},
            dataType:'html',
            success: function(data)
            {alert(data);
              if(data==1)
              {
                alert('Rejected successfully');
                window.location = '<?php echo base_url();?>product';
              }
              else
              {
                alert('OOPS!something went wrong');
              }
            }
        });
      }
   }
}
*/
$(function() {

  if ($('#category').val()!=''){
    get_sub_cat($('#category').val());
  }

   if ($('#sub_category').val()!=0){
      get_var_type($('#sub_category').val());
    }

    $( "#sub_category" ).change(function() {
      get_var_type($('#sub_category').val());
    });
  get_var_type($('#sub_category').val());

});
</script>
