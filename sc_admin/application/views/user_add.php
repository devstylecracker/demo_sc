<section class="content-header">
	<?php if(!empty($user_data)) { ?>
		<h1>Edit User </h1>
		<?php }else{ ?>
			<h1>New User </h1>
			<?php } ?>
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url(); ?>home">Home</a></li>
				<li><a href="<?php echo base_url(); ?>users">Manage Admin Users</a></li>
				<?php if(!empty($user_data)) { ?>
					<li class="active">Edit Users</li>
					<?php }else{ ?>
						<li class="active">Add New Users</li>
						<?php } ?>
					</ol>
				</section>
				<section class="content">
					<div class="box">
						<div class="box-header with-border">
							<?php if(!empty($user_data)) { ?>
								<h3 class="box-title">Edit User</h3>
								<?php }else{ ?>
									<h3 class="box-title">Add New User</h3>
									<?php } ?>
								</div><!-- /.box-header -->

								<?php if(isset($user_data['0']['id'])) $action = 'users/user_edit/'.$user_data['0']['id'] ; else $action = 'users/user_add/'; ?>
								<form role="form" name="frmadduser" id="frmadduser" action="<?php echo base_url().$action; ?>" method="post">
									<div class="box-body">
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label for="categoryName" class="control-label">User type<span class="text-red">*</span></label>
													<select name="user_type" id="user_type" class="form-control">
														<option value="">Select User Type</option>
														<?php if(!empty($roles)){ ?>
															<?php foreach($roles as $val) { ?>
																<?php if(isset($user_data['0']['role_id']) && $user_data['0']['role_id'] == $val->id){ ?>
																	<option value="<?php echo $val->id; ?>" selected><?php echo $val->role_name; ?></option>
																	<?php }else{ ?>
																		<option value="<?php echo $val->id; ?>"><?php echo $val->role_name; ?></option>
																		<?php } ?>
																		<?php } ?>
																		<?php } ?>
																	</select>
																	<span class="text-red"> <?php echo form_error('user_type'); ?></span>
																</div>
																<div class="form-group">
																	<label for="categorySlug" class="control-label">First Name<span class="text-red">*</span></label>
																	<input type="text" placeholder="Enter First Name" name="first_name" id="first_name" class="form-control" value="<?php if(isset($user_data['0']['first_name'])) echo $user_data['0']['first_name']; else echo set_value('first_name'); ?>"><span class="text-red"><?php echo form_error('first_name'); ?></span>

																</div>
																<div class="form-group">
																	<label for="categorySlug" class="control-label">Last Name<span class="text-red">*</span></label>
																	<input type="text" placeholder="Enter Last Name" name="last_name" id="last_name" class="form-control" value="<?php if(isset($user_data['0']['last_name'])) echo $user_data['0']['last_name']; else echo set_value('last_name'); ?>"><span class="text-red"><?php echo form_error('last_name'); ?></span>
																</div>
																<div class="form-group">
																	<label for="categorySlug" class="control-label">Username<span class="text-red">*</span></label>
																	<input type="text" placeholder="Enter Username" name="username" id="username" class="form-control" value="<?php if(isset($user_data['0']['user_name'])) { echo $user_data['0']['user_name']; $disabled="disabled";} else { echo set_value('username'); $disabled=""; } ?>" <?php echo $disabled; ?>>  <span class="text-red"><?php echo form_error('username'); ?></span>

																</div>
																<div class="form-group">
																	<label for="categorySlug" class="control-label">Email<span class="text-red">*</span></label>
																	<input type="text" placeholder="Enter Email" name="email" id="email" class="form-control" value="<?php if(isset($user_data['0']['email'])) echo $user_data['0']['email']; else echo set_value('email'); ?>" <?php echo $disabled; ?>> <span class="text-red"><?php echo form_error('email'); ?></span>

																</div>
																<div class="form-group">
																	<label for="categorySlug" class="control-label">Password<span class="text-red">*</span></label>
																	<input type="password" placeholder="Enter Password" name="password" id="password" class="form-control"><span class="text-red"><?php echo form_error('password'); ?></span> </div>
																	<div class="form-group">
														                <div class="checkbox">
														                  <label>
														                    <input type="checkbox" id="showHide"> Show Password
														                  </label>
														                </div>
														              </div>

																	<div class="form-group">
																		<label for="categorySlug" class="control-label">Confirm password<span class="text-red">*</span></label>
																		<input type="password" placeholder="Enter Confirm password" name="confirm_password" id="confirm_password" class="form-control"> <span class="text-red"><?php echo form_error('confirm_password'); ?></span></div>

																		<input type="hidden" name="user_id" id="user_id" value="<?php if(isset($user_data['0']['id'])) echo $user_data['0']['id']; ?>">
																	</div>
																</div><!-- /.box-body -->
																<div class="box-footer">
																	<button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Submit</button>

																	<?php if($user_data['0']['role_id']=='6')
																	{ ?>
																	<a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>brandusers"><i class="fa fa-reply"></i> Back</a>
																	<?php }
                                                                     else
                                                                     { ?>
                                                                     <a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>users"><i class="fa fa-reply"></i> Back</a>
                                                                 <?php }  ?>

																	<!--a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>users"><i class="fa fa-reply"></i> Back</a-->
															<button class="btn btn-default btn-sm" type="reset"><i class="fa fa-close"></i> Cancel</button>
																</div>
															</form>
														</div><!-- /.box -->
													</section>
													<script type="text/Javascript">
$( document ).ready(function() {

	$('.nav-item-user').addClass("active");
	$('.nav-admin-user').addClass("active");

	$("#showHide").click(function(){

		if($("#password").attr("type") == "password")
		{
			$("#password").attr("type","text");
		}else
		{
			$("#password").attr("type","password");
		}

	 });
});
</script>
