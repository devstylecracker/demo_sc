<?php
    $users_menu=$this->data['users_menu'];
    $table_search = $table_search == '0' ? '' : $table_search;  
	$date_decode = base64_decode($this->uri->segment(4));
	$date_range = explode(',',$date_decode);
    //echo '<pre>';print_r($orders);
    //echo '<pre>';print_r($orderStatusList);exit;
    $statusDiv = '';
?>

<style type="text/css">
  .modal-header{
    background: #fff !important;
    color: #000 !important;
  }
</style>
<section class="content-header">
  <h1>Manage Orders  <small class="label label-info"><?php echo @$total_rows; ?></small></h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">Manage Orders</li>
  </ol>
</section>
<section class="content">
<div class="box">
<div class="box-body">
  <?php echo $this->session->flashdata('feedback'); ?>
  <form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>manage_orders" method="post">
  <input type="hidden"  name="is_download" id="is_download" value="0" />
    <div class="row">
      <div class="col-md-2">
        <?php $search_by = set_value('search_by')=='' ? $search_by : set_value('search_by'); ?>
        <select name="search_by" id="search_by" class="form-control input-sm" >
          <option value="0">Search by</option>
          <option value="1" <?php echo $search_by==1 ? 'selected' : ''; ?> >Order ID</option>
          <option value="9" <?php echo $search_by==9 ? 'selected' : ''; ?> >Customer Order ID</option>
          <option value="2" <?php echo $search_by==2 ? 'selected' : ''; ?> >Customer Name</option>
         <!--  <option value="3" <?php echo $search_by==3 ? 'selected' : ''; ?> >Company Name</option> 
          <option value="4" <?php echo $search_by==4 ? 'selected' : ''; ?> >Date</option>-->
          <option value="5" <?php echo $search_by==5 ? 'selected' : ''; ?> >Status</option>
          <option value="6" <?php echo $search_by==6 ? 'selected' : ''; ?> >Order Unique No.</option>
          <option value="8" <?php echo $search_by==8 ? 'selected' : ''; ?> >Date Range</option>
          <option value="10" <?php echo $search_by==10 ? 'selected' : ''; ?> >Box Attributes</option>
        </select>
      </div>
      <div class="col-md-3">
        <div class="input-group">
         <div id ="filter" style="display: block;" >
          <input type="text" name="table_search" value="<?php echo set_value('table_search')=='' && set_value('table_search')==0 ? $table_search : set_value('table_search'); ?>" id="table_search" class="form-control input-sm pull-right" placeholder="Search"  required=""/>
         </div>
        <div id ="daterange" style="display: none;">
            <div class="row">
              <div class="col-md-5">
                <div class="form-group">
                  <input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm" id="date_from" value="<?php echo set_value('date_from')?set_value('date_from'):$date_range[0]; ?>">
                </div>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="<?php echo set_value('date_to')?set_value('date_to'):$date_range[1]; ?>">
              </div>
             </div>
            </div>
        </div>
        <div id ="box_filter" style="display: none;" >      
            <select name="table_search_box" id="table_search_box" class="form-control input-sm pull-right" >
                <option value="" >Filter by</option>
                <option value="gift" <?php echo $table_search=='gift' ? 'selected' : ''; ?> >Gift Orders</option>
                <option value="curated" <?php echo $table_search=='curated' ? 'selected' : ''; ?> >Curated Orders</option>
            </select>
         </div>
        <div id ="status_filter" style="display: none;" >      
            <select name="table_search_status" id="table_search_status" class="form-control input-sm pull-right" >
                <option value="" >Filter by</option>
                <option value="processing" <?php echo $table_search=='processing' ? 'selected' : ''; ?> >Processing</option>
                <option value="confirmed" <?php echo $table_search=='confirmed' ? 'selected' : ''; ?> >Confirmed</option>
                <option value="curated" <?php echo $table_search=='curated' ? 'selected' : ''; ?> >Curated</option>
                <option value="dispatched" <?php echo $table_search=='dispatched' ? 'selected' : ''; ?> >Dispatched</option>
                <option value="delivered" <?php echo $table_search=='delivered' ? 'selected' : ''; ?> >Delivered</option>
                <option value="cancelled" <?php echo $table_search=='cancelled' ? 'selected' : ''; ?> >Cancelled</option>
                <!-- <option value="return" <?php echo $table_search=='return' ? 'selected' : ''; ?> >Return</option> -->
                <option value="fake" <?php echo $table_search=='fake' ? 'selected' : ''; ?> >Fake</option>
            </select>
         </div>
        <div class="input-group-btn">
          <button class="btn btn-sm btn-default" type="submit" onclick="submitForm();" ><i class="fa fa-search"></i></button>
        </div>
      </div>
      </div>
	   <div class="col-md-2">
	   <select name="search_by2" id="search_by2" class="form-control input-sm" onchange = "submitForm();" >
          <option value="0" >Filter by</option>
          <option value="1" <?php echo $search_by2==1 ? 'selected' : ''; ?> >Normal Orders</option>
		  <option value="2" <?php echo $search_by2==2 ? 'selected' : ''; ?> >SCBOX Orders</option>
		  </select>
	</div>
      <div class="col-md-1">
        <a href="<?php echo base_url(); ?>manage_orders" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
      </div>
	   
      <div class="col-md-4">
       <?php //if($this->session->userdata('role_id') != 6){ ?>
       
              <?php //} ?>
			  <button title="Orders Data" id="download_excel" class="btn btn-primary btn-sm"  onclick="downloadExcel();"><i class="fa fa-download"></i> Orders Data</button>
        <button title="User Extracted Data" id="download_excel2" class="btn btn-primary btn-sm"  onclick="download_stylist_extracted_data();"><i class="fa fa-download"></i> User Extracted Data</button>       
        <button title="PA Data" id="download_excel3" class="btn btn-primary btn-sm"  onclick="excelDownload();"><i class="fa fa-download"></i> PA Data</button>
        <div class="pull-right" style="padding-top: 10px;">    

          <?php echo $this->pagination->create_links(); ?>
        </div>
      </div>
    </div>
<div><br/><b>Note:</b> Grand Total is inclusive of Service-Tax,COD, Shipping Charges, Coupon Code Deduction (if applicable) <br/>(ADP - Assign Delivery Partner)</div>
</form>
<div class="table-responsive">
  <table class="table table-bordered table-striped table-orders dataTable table-tr-collapse">
    <thead>
      <tr>
        <th width="20">Sr. No.</th>
        <th width="90">Date</th>
        <th>Order Unique ID</th>
        <th>Coupon Code & Referral</th>
        <th width="90">Total</th>
        <th width="90">Discounted Price </th>
        <th width="90">Order Total</th>
        <th width="60">Payment Type</th>
        <th width="70">Status</th>
        <th>Customer Name</th>
        <th width="90">Mobile Number</th>
        <th width="120">Platform<br/>UTM-source<br/>UTM-medium<br/>UTM-campaign</th>
       <!--  <th>Send Email</th> -->
        <!--<th width="90">Last updated</th>-->
		<th width="90">Order Status</th>
        <th width="70">Details</th>
		<th width="70">Payment Status</th>
      </tr>
    </thead>
    <tbody>
    <?php
      $i = 0;
      $new_order_id = ''; $old_order_id = '';
      $coupon_product_price = 0;
	  
    if(!empty($orders))
    {
      foreach($orders as $val)
      { $i++; 
          $imageString ='';$data['coupon_discount'] = 0;$scbox_productids=array();
          $referral_point = 0;$refDiscount = 0;$user_link = '';
          $referral_point = $this->Manage_orders_model->get_refre_amt($val['order_unique_no']);
          if($referral_point>0 && $val['coupon_code']!='')
          {
            $couponReferral_text = $val['coupon_code'].' & '.REDEEM_POINTS_CART_TEXT.'-'.$referral_point;
          }else if($val['coupon_code']!='')
          {
            $couponReferral_text = $val['coupon_code'];
          }else if($referral_point>0)
          {
            $couponReferral_text = REDEEM_POINTS_CART_TEXT.'-'.$referral_point;
          }else
          {
            $couponReferral_text = '';
          }

          $order_product_sum = $this->Manage_orders_model->get_order_price_ref($val['order_unique_no']);

          $scbox_productids = unserialize(SCBOX_PRODUCTID);
          if(in_array($val['product_id'], $scbox_productids))
          {
            //$user_link = base_url().'websiteusers/scboxuser_view/'.$val['user_id'];
            $user_link = base_url().'Stylist/user_view/'.$val['user_id'];
          }else
          {
            //$user_link = base_url().'/websiteusers/user_view/'.$val['user_id'];
            $user_link = base_url().'Stylist/user_view/'.$val['user_id'];
          }

           $gift_html = '';
          $scbox_gift_object = $this->Scbox_model->get_object('scbox_gift')[0]['object_id'];
		      if($val['order_display_no']!='')
          {
            $gift_data = $this->Scbox_model->get_gift_data($scbox_gift_object,$val['order_display_no']);
          }          
		  
          if(!empty($gift_data))
          {
            $gift_html = '<br/><i style="color:red" font-size:12px;>GIFT</i>';
          }else
          {
            $gift_html = '';
          }

          //if($this->Manage_orders_model->get_morder_pa($val['cart_id']))
          if($val['created_datetime']>'2018-01-16 00:00:00')
          {
            $orderpa_class = 'matchparam1';
          }else
          {
            $orderpa_class = '';
          }

          

    ?>
        <tr class="<?php echo $orderpa_class; ?>">
        <?php if($sr_offset > 0){  ?>
          <td><?php echo $i+($sr_offset*10); ?></td>
        <?php }else{ ?>
          <td><?php echo $i; ?></td>
        <?php } ?>
          <td><?php echo $val['created_datetime']; ?></td>
          <td><?php echo $val['order_unique_no']; ?><br><span class="text-blue"><?php echo $val['order_display_no']; ?></span><?php echo $gift_html; ?></td>
          <td><?php echo $couponReferral_text; ?></td>
          <?php
            $valuee = explode('_', $val['order_unique_no']);                
              $ussid = $valuee[0];
              $orrid    = $valuee[1];
              $online_pay_percent =  $this->Manage_orders_model->get_scdiscount_data($orrid, $ussid);
              $scbox_product_price = $this->Manage_orders_model->get_scboxproduct_data($orrid, $ussid);
              if($val['pay_mode']==2 && $online_pay_percent>0)
              {               
                $online_payment_discount = ($scbox_product_price-$val['coupon_amount'])*$online_pay_percent/100;
              }else
              {
                $online_payment_discount = 0;
              }
          ?>
          <td><?php echo number_format(round($val['product_sum'])+round($online_payment_discount), 2, '.', ','); ?></td>
          <!--<td><?php echo number_format(round($val['OrderGrandTotal']), 2, '.', ','); ?></td> -->
          <?php
/* Coupon Setting Page */
              if(isset($val['coupon_code']) && $val['coupon_code']!='')
              {
                $coupon_info = $this->Manage_orders_model->getUserCoupon($val['coupon_code'],$val['created_datetime'],$val['brand_id']);

                /*$data['coupon_discount'] = $coupon_info['coupon_amount'];
                $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];*/
                $coupon_brand = $coupon_info['brand_id'];
                $coupon_products = $coupon_info['coupon_products'];
                if($coupon_products!='' || $coupon_products!=0)
                {
                  $coupon_products_arr =explode(',',$coupon_products);
                }
                $coupon_min_spend = $coupon_info['coupon_min_spend'];
                $coupon_max_spend = $coupon_info['coupon_max_spend'];
                $coupon_discount_type = $coupon_info['coupon_discount_type'];
                $coupon_stylecracker =  $coupon_info['stylecrackerDiscount'];
                $individual_use_only =  $coupon_info['individual_use_only'];
                $data['coupon_discount'] = $coupon_info['coupon_amount'];
                $data['coupon_code'] = $val['coupon_code'];
                $coupon_product_price = 0;
                $price = $val['product_price'];


                 if($coupon_brand==$val['brand_id'] && $coupon_brand!=0)
                  {
                    if($coupon_products!=''&& $coupon_products!=0)
                    {
                       //coupon_discount_type =3 (Product discount)
                      if($coupon_discount_type==3)
                      {
                        if(in_array($val['product_id'], $coupon_products_arr))
                        {
                          if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                          {
                            $data['coupon_discount'] = $coupon_info['coupon_amount'];
                            $data['coupon_code'] = $val['coupon_code'];
                          }
                        }
                      }
                       //coupon_discount_type =4 (Product % discount)
                      if($coupon_discount_type==4)
                      {
                        if(in_array($val['product_id'], $coupon_products_arr))
                        {
                          if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                          {
                            $coupon_percent = $coupon_info['coupon_amount'];
                            $data['coupon_discount'] = $price*($coupon_percent/100);
                            $data['coupon_code'] = $val['coupon_code'];
                          }
                        }
                      }

                    }else
                    {
                       //coupon_discount_type =1 (cart discount)
                      if($coupon_discount_type==1)
                      {
                        $coupon_product_price = $coupon_product_price+$price;
                        if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                        {
                          $data['coupon_discount'] = $coupon_info['coupon_amount'];
                          $data['coupon_code'] = $val['coupon_code'];
                        }
                      }
                       //coupon_discount_type =2 (cart % discount)
                      if($coupon_discount_type==2)
                      {
                        $coupon_product_price = $coupon_product_price+$price;
                        if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                        {
                          $coupon_percent = $coupon_info['coupon_amount'];
                          $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                          $data['coupon_code'] = $val['coupon_code'];
                        }
                      }
                    }
                  }

                if(($coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only!=1  )|| ( $coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only==1 ))
                {
                    //coupon_discount_type =1 (cart discount)
                     $coupDiscountPrdPrice = $this->Manage_orders_model->calculate_product_discountAmt_order($val['product_price'],$val['OrderProductTotal'],$coupon_discount_type,$coupon_info['coupon_amount']);

                        if($coupon_discount_type==1)
                        {
                         /* $coupon_product_price = $coupon_product_price+$price;*/
                          
                          if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                          {
                            $data['coupon_discount'] = round($val['OrderProductTotal']-$coupDiscountPrdPrice);
                            $data['coupon_code'] = $val['coupon_code'];
                          }
                        }
                      //coupon_discount_type =2 (cart % discount)
                        if($coupon_discount_type==2)
                        {
                          //$coupon_product_price = $coupon_product_price+$price;   
                          //echo $val['product_sum'].' , '.$val['OrderProductTotal'].' , '.$coupon_discount_type.' , '.$coupon_info['coupon_amount'].'<br/>';
                           /* $coupDiscountPrdPrice1 = $this->Manage_orders_model->calculate_product_discountAmt($val['OrderTotal'],$val['OrderProductTotal'],$coupon_discount_type,$coupon_info['coupon_amount']);*/
                            $coupDiscountPrdPrice1 = $this->Manage_orders_model->calculate_product_discountAmt($val['OrderTotal'],$val['OrderProductTotal'],$coupon_discount_type,$coupon_info['coupon_amount']);
                          
                          $coupon_product_price = $val['OrderProductTotal'];
                          if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                          {
                            $coupon_percent = $coupon_info['coupon_amount'];
                            //$data['coupon_discount'] = $coupDiscountPrdPrice1;
                            $data['coupon_discount'] = $val['coupon_amount'];
                            $data['coupon_code'] = $val['coupon_code'];
                          }
                        }


                }

              }else
              {
                $data['coupon_discount']=0;
                $data['coupon_code'] = '';
              }

/* Coupon Setting Page End */
          ?>
          <td><?php 
            if($this->session->userdata('role_id') == 6 ){
                if($referral_point>0)
                {
                  $orderPercent = ($val['product_sum']*100)/$order_product_sum;  
                  $refDiscount = round(($orderPercent*500)/100,2);
                }
                
              echo number_format(round($val['product_sum'],2)-(round($data['coupon_discount'],2)+$refDiscount), 2, '.', ',');
          }else
          {
            
             echo number_format(round($val['product_sum'],2)-(round($data['coupon_discount'],2)+$referral_point), 2, '.', ',');
          }
          ?></td>
          <?php
          if($this->session->userdata('role_id') == 6 ){
                if($referral_point>0)
                {
                  $orderPercent = ($val['product_sum']*100)/$order_product_sum;  
                  $refDiscount = round(($orderPercent*500)/100,2);
                }
           $order_total_mf = number_format((round($val['product_sum'],2)-(round($data['coupon_discount'],2)+$refDiscount)), 2, '.', ',');
          }else
          {
            
            //$order_total_mf = number_format((round($val['product_sum'],2)-(round($data['coupon_discount'],2)+$referral_point+$online_payment_discount)), 2, '.', ',');
            $order_total_mf = number_format((round($val['product_sum'],2)-(round($val['coupon_amount'],2)+$referral_point)), 2, '.', ',');
            

          } 

          ?>
          <td style="width:80px;" ><i class="fa fa-inr"></i> <?php echo $order_total_mf; ?></td>
          <td><?php
            if($val['pay_mode'] == 1)
            {
              echo 'COD';
            }else
            {
              echo 'Online';
            }
            ?></td>
           <td >
           <?php echo $this->Manage_orders_model->getOrderStatus($val['order_unique_no']); ?>
            <?php //if($val['confirm_mobile_no'] == 1 || $val['pay_mode']!=1) { ?>
            <?php //echo $this->Manage_orders_model->getOrderStatus($val['order_unique_no']); ?>
            <?php //}else{ ?>
             <!-- <select class="form-control input-sm"  name="otp_confirmation_<?php echo $val['order_unique_no']; ?>" id="otp_confirmation_<?php echo $val['order_unique_no']; ?>" onchange="change_order_status('<?php echo $val['order_unique_no']; ?>','<?php echo $val['first_name']; ?>','<?php echo $val['last_name']; ?>','<?php echo $val['user_id']; ?>');">
             <option value="0">OTP Pending</option>
             <option value="1">Mark as Processing</option>
             <option value="2">Mark as Fake</option>
            </select> -->
            <?php //} ?>
          </td>
          <td><a href="<?php echo $user_link; ?>" target="_blank" ><?php echo $val['first_name'].' '.$val['last_name']; ?></a></td>
          <td><a href="<?php echo base_url().'send_sms?mobile_no='.$val['mobile_no'].'&f_name='.trim($val['first_name']); ?>" target="_blank" ><?php echo $val['mobile_no']; ?></a>
          </td>
          <td><?php echo $val['platform_info'];
					if($val['sc_source'] != ''){
						echo '<span style="color:blue;"><i><p>'.$val['sc_source'].'</p><p>'.$val['sc_medium'].'</p><p style="word-wrap:break-word;width:80px;">'.$val['sc_campaign'].'</p></i></span>';
					}
			   ?>
		  </td>
           <?php
          /*if(($val['order_status'] == 5 && $val['send_email'] == 1) || ($val['order_status'] == 2 && $val['send_email'] == 2) || ($val['order_status'] == 3 && $val['send_email'] == 3) || ($val['order_status'] == 4 && $val['send_email'] == 4)){ $show_mail = 1; }
          else if($val['order_status'] != 5 && $val['send_email'] == 0){$show_mail = 0; }
          else { $show_mail = 0; }*/
          ?>
          <?php /*if($val['order_status'] < 7 && $val['order_status'] != 1) { ?>
          <td><a class="btn btn-primary btn-xs" <?php echo $show_mail==1 ? 'disabled' : ''; ?> data-toggle="modal" onclick="send_email('<?php echo $val['order_unique_no']; ?>');">Send E-mail</a></td>
          <?php }else{ echo '<td></td>'; } */?>
          <!--<td><?php //echo $val['modified_datetime']; ?></td>-->
		  
		  <!-- <td><select class="form-control input-sm"  name="final_order_status<?php //echo $val['order_unique_no']; ?>" id="final_order_status<?php //echo $val['order_unique_no']; ?>" onchange="change_final_order_status('<?php //echo $val['order_unique_no']; ?>','<?php //echo $val['user_id']; ?>');">
				<option value="0">Not Completed</option>
				<option <?php //if($val['is_completed'] == 1){ echo "selected"; } ?> value="1">Completed</option>
            </select></td> -->
      <?php

          $statusDiv = ' <td>
            <select name="orderStatus" id="orderStatus_'.$val['order_prd_id'].'" class="orderStatus form-control" order="'.$val['order_prd_id'].'" onchange="SetOrderStatus(\''.$val['order_prd_id'].'\',\''.$val['order_unique_no'].'\',\''.$val['cart_id'].'\')" >';
            foreach($orderStatusList as $key=>$value)
            {   
              if($val['order_prd_status'] == $key)
              {
                $statusDiv=$statusDiv.'<option  value="'.$key.'" selected >'.$value.'</option>';
              }else if(($val['order_prd_status'] == 1 && ($key==2 || $key==5 ||  $key==7 ))){ /*'Processing' Status*/
                $statusDiv=$statusDiv.'<option  value="'.$key.'" >'.$value.'</option>';
              }else if(($val['order_prd_status'] == 2 && ($key==3 || $key==4 || $key==5 || $key==7 || $key==8 ))){ /*'Confirmed' Status*/
                $statusDiv=$statusDiv.'<option  value="'.$key.'" >'.$value.'</option>';
              }else if(($val['order_prd_status'] == 8 && ($key==3 || $key==4 || $key==5 || $key==7 || $key==8 ))){ /*'Curated' Status*/
                $statusDiv=$statusDiv.'<option  value="'.$key.'" >'.$value.'</option>';
              }else if(($val['order_prd_status'] == 3 && ($key==3 || $key==4 || $key==5 || $key==6 ))){ /*'Dispatch' Status*/
                $statusDiv=$statusDiv.'<option  value="'.$key.'" >'.$value.'</option>';
              }else if(($val['order_prd_status'] == 4 && ($key==4 || $key==6 ))){ /*'Delivered' Status*/
                $statusDiv=$statusDiv.'<option  value="'.$key.'" >'.$value.'</option>';
              }               
           }
            $statusDiv=$statusDiv.'</select></td> ';
          echo $statusDiv;
      ?>
			
		  <td>
            <div id="product_info"> </div>
              <a class="btn btn-primary btn-xs btn-quick-view" id="<?php echo $val['order_unique_no']; ?>">Details <i class="fa fa-angle-down fa-angle-up"></i></a>
			  <br><br>
			  <!--<button type="button" class="btn btn-success btn-xs" data-toggle="modal" onClick="addProductModal('<?php echo $val['order_display_no']; ?>')">Add Product</button>-->
			 <div style="display:inline-flex;">
       <?php  
              $order_product_status = $this->Manage_orders_model->getOrderProductStatus($val['order_unique_no'],$val['product_id']);
              // if($order_product_status=='2' || $order_product_status=='3' || $order_product_status=='4' || $order_product_status=='6')
              if($order_product_status=='2' )
              {
       ?>
      <!--  <a data-toggle="modal" data-target="#products-popup" id="popup_button" data-attr="scbox-order-products" onclick="selectOrder('<?php echo $val['order_display_no']; ?>');"><div data-toggle="tooltip" title="Add Product" style="border:1px dashed #333;padding: 4px; width: 30px;height: 30px;margin-right:3px;"><img style="width:21px;" src="<?php echo base_url('assets/seventeen/add.png');?>"></div></a> -->
       <a onclick="myFunction1(<?php echo $val['cart_id']; ?>)" style="cursor: pointer;"><div data-toggle="tooltip" title="Recommended Product" style="border:1px dashed #333;padding: 4px; width: 30px;height: 30px;margin-right:3px;"><img style="width:21px;" src="<?php echo base_url('assets/seventeen/idea.png');?>"></div></a>
                 <script>
                  function myFunction1(cartid) {
                      window.open(sc_backend_url+"Product_curation/order_cart/"+cartid, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=0,left=0,width=1340,height=640");                     
                  }
                  </script>
        <?php
              if($orderpa_class == 'matchparam1')
              {
        ?>
        <!--  <a data-toggle="modal" data-target="#products-popup1" id="popup_button" data-attr="scbox-order-products" data-cart-id="<?php echo $val['cart_id']; ?>" onclick="getProductRecommendation('<?php echo $val['cart_id']; ?>','<?php echo $val['order_display_no']; ?>');" ><div data-toggle="tooltip" title="Recommended Product" style="border:1px dashed #333;padding: 4px; width: 30px;height: 30px;margin-right:3px;"><img style="width:21px;" src="<?php echo base_url('assets/seventeen/idea.png');?>"></div></a> -->
        <?php  } ?>
      
       <?php  } ?>
			 <?php 


          $value     = explode('-', $val['order_unique_no']);
          $mkey   = $val['order_display_no'];
          $oid = $value[0];
          $uid    = $value[1];
          $objectId     = $this->Scbox_model->get_object('box_order_products')[0]['object_id'];
          $metaValue = $this->Manage_orders_model->getBoxProductDetail($mkey, $oid, $uid,$objectId);         
          if(!empty($metaValue))
          {
     ?>


			  <a data-toggle="modal" onClick="productDetail('<?php echo $val['order_display_no']; ?>','<?php echo $val['user_id']; ?>')"><div data-toggle="tooltip" title="Product Info" style="border:1px dashed #333;padding: 4px;  width: 30px;height: 30px;"><img style="width:21px;" src="<?php echo base_url('assets/seventeen/info.png');?>"></div></a>
        <?php


          }
        ?>
        </div>
          </td>
		  <td>
            <div id="box_order_info"> 
				<a href="javascript:void(0)" data-orderid="<?php echo $val['order_unique_no']; ?>" onclick="get_order_popup(this)"><?php if($val['is_paymentdone'] == '1' || $val['pay_mode'] == '2'){ echo 'Paid'; }else{ echo 'Pending'; }?></a>
			</div>
             
          </td>
      </tr>
      <tr class="tr-quick-view" >
        <td colspan="18" id="orderDetail_<?php echo $val['order_unique_no']; ?>" >
        </td>
      </tr>
      <?php
         }
      }else
      {
      ?>
         <tr>
          <td colspan="18">Sorry no record found</td>
        </tr>
    <?php
      }
      ?>
    </tbody>

  </table>
</div>

<?php echo $this->pagination->create_links(); ?>
</div>
</div>

</section>



        <div class="modal fade" id="addProductModal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Product</h4>
              </div>
              <div class="modal-body">
			  
			 <div id="productData"></div>
			  
					<form class="form-horizontal" method="post" action="" id="prodct-form" enctype="multipart/form-data">
					<input id="orderId" name="orderId" type="hidden" value="">
					
					  <input id="selected_orderid" name="selected_orderid" type="hidden" value="">
					  <div class="box-body">
						<div class="form-group">
						  <label for="product_name" class="col-sm-3 control-label">Product Name</label>
						  <div class="col-sm-9">
							<input name="product_name" type="text" class="form-control" id="product_name" placeholder="Product Name">
							<span class="text-danger"></span>
						  </div>
						</div>
						<div class="form-group">
						  <label for="producte_price" class="col-sm-3 control-label">Producte Price</label>

						  <div class="col-sm-9">
							<input name="producte_price" type="text" class="form-control" id="producte_price" placeholder="Product Price">
							<span class="text-danger"></span>
						  </div>
						</div>

						<div class="form-group">
						  <label class="control-label col-sm-3" for="product_description">Description</label>
						  <div class="col-sm-9">
							<div>
							  <textarea class="form-control" name="product_description" id="product_description" rows="3" placeholder="Product Description"></textarea>
							 </div>
						   </div>
						</div>
						
						
						<div class="form-group">
							<label class="control-label col-sm-3" for="product_image">Product Image <span class="text-danger"> </span></label>
							<div class="col-sm-9">
								<div>
									<input type="file" class="form-control" id="product_image" name="product_image" placeholder="">
									<span class="text-danger"><!--(File Size Limit: 2 MB max.)--></span> 
								</div>
							</div>
						</div>
					  </div>
					  <!-- /.box-footer -->
					   <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
						<button type="submit" id="productAdd" name="productAdd" class="btn btn-primary">Insert Product</button>
					  </div>
					</form>
              </div>
             
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
		<div class="modal fade addAwb" id="addAwb">
          <div class="modal-dialog" style="width: 60%;">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add AWB</h4>
              </div>
              <div class="modal-body">
        <div id="returnCartData"></div>
		<div class="row">
		          <div class="col-sm-6">
            <h4 class="text-center">Edit Shipping Details <a id="edit" href="" style="color: red;font-weight: 700;"><i class="fa fa-edit"></i></a></h4>
              <form id="awb_customer_frm" name="awb_customer_frm" class="form-horizontal" method="post">
                 <div class="box-body">
					<input type="hidden" name="awb_order_unique_no" id="awb_order_unique_no" value=""> 
					<input type="hidden" name="awb_order_display_no" id="awb_order_display_no" value=""> 
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="awb_customer_fname"> First Name</label>
                    <div class="col-sm-8"> <input type="text" name="awb_customer_fname" class="form-control" disabled id="awb_customer_fname"></div>
                  </div>
				   <div class="form-group">
                    <label class="control-label col-sm-4" for="awb_customer_lname">Last Name</label>
                    <div class="col-sm-8"> <input type="text" name="awb_customer_lname" class="form-control" disabled id="awb_customer_lname"></div>
                  </div>
                   <div class="form-group">
                     <label class="control-label col-sm-4" for="awb_customer_mobile">Mobile</label>
                     <div class="col-sm-8"><input type="text" name="awb_customer_mobile" class="form-control" disabled id="awb_customer_mobile"></div>
                  </div>
                   <div class="form-group">
                    <label class="control-label col-sm-4" for="awb_customer_pincode">Pincode</label>
                     <div class="col-sm-8"><input type="text" name="awb_customer_pincode" class="form-control" disabled id="awb_customer_pincode"></div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="awb_customer_address">Address:</label>
                      <div class="col-sm-8"><textarea id="awb_customer_address" name="awb_customer_address" class="form-control" disabled></textarea></div>
                  </div>
                   <div class="form-group">
                    <label class="control-label col-sm-4" for="awb_customer_city">City:</label>
                     <div class="col-sm-8"><input type="text" name="awb_customer_city" class="form-control" disabled id="awb_customer_city"></div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="awb_customer_state">State:</label>
                     <div class="col-sm-8">
					 <!--<input type="text" name="awb_customer_state" class="form-control" disabled id="awb_customer_state">-->
					 <select id="awb_customer_state" name="awb_customer_state" class="form-control" disabled>
					 <option>Select State</option>
						<?php 
							if(is_array($getAllState) && count($getAllState)>0)
							{
								foreach($getAllState as $state)
								{
									?>
									<option value="<?php echo $state['id'];?>"><?php echo trim($state['state_name']);?></option>
									<?php
								}
							}
						
						
						?>
					 </select>
					 </div>
                  </div>
                  <div class="box-footer">
					  <input type="hidden" name="submit_user_order" id="submit_user_order" value="Save" disabled class="btn btn-info pull-right">
					 
					   <button type="submit" class="btn btn-info pull-right" id="submit_user_order" name="submit_user_order" disabled>Save Details</button>
                  </div>
                </div>
              </form>
          </div>
		<div class="col-sm-6">
          <form class="form-horizontal" method="post" action="<?php //echo base_url('manage_order/addawb')?>" id="awb_number" name="awb_number" enctype="multipart/form-data">
          <div class="box-body">
          <input name="cartidforawb" type="hidden" class="form-control" id="cartidforawb" value="">
          <input name="orderprdidforawb" type="hidden" class="form-control" id="orderprdidforawb" value="">
          <input name="orderuniqueidforawb" type="hidden" class="form-control" id="orderuniqueidforawb" value="">
           <div class="row">
            <div class="col-sm-12">
             <h4 class="text-center">Assign Delivery Partner</h4>
             <div class="box-body">
			 <div class="checkbox">
			  <label><input type="checkbox" value="" id="manual_checkbox_partner">Manual</label>
			</div>

             <div class="form-group">
                <label for="product_name" class="col-sm-3 control-label">Delivery Partner</label>
                <div class="col-sm-9">

                <select id="delivery_partner" data-mini="true" class="form-control" name="delivery_partner" onchange="get_aws_no(this)">
                  <option value="">Select Delivery Partner</option>
                  <?php 
                    if(is_array($dpartners) && count($dpartners))
                    {
                      foreach($dpartners as $key=>$delivery_partner)
                      {
                        ?>
                        <option value="<?php echo $delivery_partner['user_id'];?>"><?php echo $delivery_partner['delivery_partner_name'];?></option>
                        <?php
                      }
                    }
                  ?>
                </select>
              </div>
            </div>
              <div class="form-group">
              <label for="product_name" class="col-sm-3 control-label">AWB Number</label>
			  <label id="awb_num_label" style="margin-top:10px;margin-left:10px;"></label>
              <div class="col-sm-9">
              <input name="awb_num" type="hidden" class="form-control" id="awb_num" placeholder="AWB Number" value="">
              <span class="text-danger"></span>
              </div>
			  <div class="col-sm-9" id="show_manual" style="display:none;">
				<input name="manual_awb_num" type="text" class="form-control" id="manual_awb_num" placeholder="AWB Number" value="">
				</div>
            </div> 
			<div class="form-group">
              <span id="awb_reason"></span>
              </div>
            </div>
             <div class="box-footer">
                  <input type="submit" name="dispatch" id="dispatch" value="Dispatch" class="btn btn-info pull-right">
                  </div>
          </div>
          </div>
          </div>
          </div>
          </form>
		  </div>
		  </div>

              </div>
            </div>
          </div>
		  
		  
		  
	



        <!-- /. End product detail modal -->
		
		
		
		<!-- start PA Data modal -->
		<div class="modal fade" id="getPaData">
          <div class="modal-dialog">
            <div class="modal-content">
              <!--<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Default Modal</h4>
              </div>-->
              <div class="modal-body">
				<div class="row" id="productPAData">
    

				</div>

			  
			  
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
          <!-- /.modal-dialog -->
		  
        </div>
        <!-- /. End PA Data modal -->
<script src="<?php echo base_url('assets/js/jquery.validate.min.js'); ?>"></script>
<style>
.table{width: 100%; font-size: 12px; border-collapse: collapse;}
.table > tbody .tr-collapse td{padding: 0;}
.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td{border: 1px solid #d2d6de; }

.table > caption + thead > tr:first-child > td, .table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table > thead:first-child > tr:first-child > th{border: 1px solid #d2d6de; }

.table.table-orders th{vertical-align: top; }

.tr-collapse .row{margin: 0; padding: 10px; }
.tr-collapse label{font-weight: normal;}
.modal-header{background: #000; color:#fff; height: auto; padding: 8px 15px;}
.modal-header h4{font-size: 16px;margin: 0; float: left;}
.order-view-popup dd{margin-bottom: 10px;}
.order-view-product-list .img-wrp{
    background: #fff none repeat scroll 0 0;
    border: 1px solid #ececec;
    height: 150px;
    padding: 10px;
    text-align: center;
    margin-bottom: 5px;
}
.order-view-popup .close{color:#fff; opacity:1;}
.order-view-product-list .img-wrp img{
    height: auto;
    max-height: 100%;
    max-width: 100%;
    width: auto;
}
.order-view-popup .name{line-height: 1.3; margin-bottom: 5px;}
select.form-control{min-width: 94px; padding: 5px;}

.table-tr-collapse .box-quick-view .img-wrp{width:50px; height:50px; border:1px solid #eee;}
.table-tr-collapse .box-quick-view .img-wrp img{width:auto; height:auto; max-width:50px; max-height:50px;}
.table-tr-collapse .box-quick-view .order-address-box{bottom: -8px; position: absolute; right: 17px; text-align: right; }
.table-tr-collapse .seller-amount-box{text-align: right; padding-right: 20px; background: #ECF0F5;}
.table-tr-collapse .seller-amount-box .price{width:70px; display: inline-block;}

.table-tr-collapse tr.tr-quick-view{display: none;}
.table-tr-collapse tr.tr-quick-view .box-quick-view{display: block;}
.table-bordered { border: 1px solid #ddd; }
label#delivery_partner-error{color: #a94442; font-size: 12px;}

</style>
 
 
 <script>
 
    $(document).on('change','.orderStatus',function(){
  

      $('#edit').click(function(event) {
          event.preventDefault();
          $('#awb_customer_fname,#awb_customer_lname,#awb_customer_mobile,#awb_customer_pincode,#awb_customer_address,#awb_customer_city,#awb_customer_state,#submit_user_order').prop("disabled", false);
      });

      $('#cancel').click(function(event) {
          event.preventDefault();

         $('#cancel').prop("disabled", true);
         //alert("cancel button was clicked");
      });

      $('#submit_user_order').click(function(event) {
          event.preventDefault();
          //alert("Submit button was clicked");
      });
});
 
 
 $(document).ready(function(){
	 
	   $("#awb_customer_frm").validate({
		ignore: "input[type='text']:hidden",
		rules: {
		  awb_customer_fname: { required: true, },
		  awb_customer_lname: { required: true, },
		  awb_customer_mobile: { required: true, },
		  awb_customer_pincode: { required: true, },
		  awb_customer_address: { required: true, },
		  awb_customer_city: { required: true, },
		  awb_customer_state: { required: true, } 
		},
		messages: {
		  awb_customer_fname: { required: "Please provide first name", },
		  awb_customer_lname: { required: "Please provide last name", },
		  awb_customer_mobile: { required: "Please provide mobile number", },
		  awb_customer_pincode: { required: "Please select provide pincode", },
		  awb_customer_address: { required: "Please select provide address", },
		  awb_customer_city: { required: "Please select provide city", },
		  awb_customer_state: { required: "Please select state", } 
		},
		submitHandler: function(form) {
			
	 	  $.ajax({
				type:'POST',
				url: "<?php echo base_url(); ?>manage_orders/update_user_order_info",
				 data:  $('#awb_customer_frm').serialize(),
				 dataType:'json',
				 cache:false,
				 success:function(data){ 
					//alert(data);				 
					if(data == 'success')
					  { 
						alert('Data added Successfully and order status set to dispatch'); 
						$('#edit').click(function(event) {
								  event.preventDefault();
								  $('#awb_customer_fname,#awb_customer_lname,#awb_customer_mobile,#awb_customer_pincode,#awb_customer_address,#awb_customer_city,#awb_customer_state,#submit_user_order').prop("disabled", true);
								  $(".awb_customer_frm").find("#profile_mail").val("TEST_AJAX");
							  });						
					   }else
					  {
						alert('error');
					  } 
					//location.reload();
				},
				error: function(data){
					console.log("error");
					console.log(data);
				}
			}); 
		}
	  });
	 
	 	 
	  $("#awb_number").validate({
		ignore: "input[type='text']:hidden",
		rules: {
		  awb_num: {
			required: true,
		  },
		  delivery_partner: {
			required: true,
		  }
		},
		messages: {
		  awb_num: {
			required: "Please provide awb number",
		  },
		  delivery_partner: {
			required: "Please select delivery partner",
		  }
		},
		submitHandler: function(form) {
		  $.ajax({
				type:'POST',
				url: "<?php echo base_url(); ?>manage_orders/addawb",
				 data:  $('#awb_number').serialize(),
				 cache:false,
				success:function(data){  
					//alert(data);
					//$('#productPAData').html(data);
          if(data!='' && data!='false')
          {
            alert('Data added Successfully and order status set to dispatch');
            setTimeout(function() {$('#addAwb').modal('hide');}, 1500);            
          }else
          {
            alert('error');
          }
					location.reload();
				},
				error: function(data){
					console.log("error");
					console.log(data);
				}
			});
		}
	  });
 });
 
 </script>
<script>

function getPaData(oid, user_id) {
	$('#getPaData').modal();
  $('#productPAData').html('');
		$.ajax({
				type:'POST',
				url: "<?php echo base_url(); ?>websiteusers/padata",
				//url: "<?php echo base_url(); ?>websiteusers/wesite_users_pa_data",
				data:{'oid':oid, 'user_id':user_id},
				cache:false,
				success:function(data){  //alert(data);
					$('#productPAData').html(data);
					//setTimeout(function() {$('#addProductModal').modal('hide');}, 1500);
				},
				error: function(data){
					console.log("error");
					console.log(data);
				}
			});
}


	function addProductModal(val) {
		$('#orderId').val(val);
		$('#addProductModal').modal();
	}
		
	$(document).ready(function (e) {
			$('#prodct-form').on('submit',(function(e) {
				e.preventDefault();
				var product_name = $('#product_name').val();
				//var producte_price = $('#producte_price').val();
				var product_description = $('#product_description').val();
				var product_image = $('#product_image').val();
				var formData = new FormData(this);
				if(product_name!='' && product_image!='')
				{
					$.ajax({
						type:'POST',
						url: "<?php echo base_url(); ?>Stylist/addproduct",
						data:formData,
						cache:false,
						contentType: false,
						processData: false,
						success:function(data){
							var html  = ('<div class="alert alert-success"><strong>Success!</strong> Product Added Successfully.</div>');
							$('#productData').append(html);
							setTimeout(function() {$('#addProductModal').modal('hide');}, 1500);
						},
						error: function(data){
							console.log("error");
							console.log(data);
						}
					});
				}
			}));
			// e.preventDefault();
		});	
	$("#manual_awb_num").on("change",function(){
		var value = $(this).val();
		$("#awb_num").val(value);
	});
	$("#manual_checkbox_partner").on("change",function(){
		if($(this).prop("checked")==true){
			$("#awb_num_label, #awb_reason").hide().html("");
			$("#awb_num").val("");
			$("#show_manual").show();
		} else {
			$("#awb_num_label").show();
			$("#show_manual").hide().val("");
		}
	});
		
 function get_aws_no(partnerId)
		{
			var isManualChecked = $("#manual_checkbox_partner").prop("checked");
			var order_unique_number = $("#orderuniqueidforawb").val();
			$("#awb_num_label").empty();
			if(isManualChecked == false){
				
			if(partnerId.value == 66002)
			{
				//$('#awb_num').prop('disabled', true);
				$('.loader').show();
				$('.page-overlay').show();
				$.ajax({
					type:'POST',
					url: "<?php echo base_url(); ?>Manage_orders/get_awb_no",
					data:{'order_unique_number':order_unique_number},
					dataType: "json",
					cache:false,
					success:function(data){  
					//alert(data.awp_no);
					$('.loader').css('display','none');
					if(data.succeed == 'Yes')
					  {
						$('#awb_num').val(data.awp_no);
						$('#awb_num_label').html(data.awp_no);
						$('#awb_reason').html('<span style="color:green">'+data.reason+'</span>');
						}
					else if(data.succeed == 'No')
					  {
							//alert('Somthing went wrong. Please try after sometime');
							$('#awb_reason').html('<span style="color:red">'+data.reason+'. Please try another delivery partner. </span>');
							//get_aws_no(partnerId);
						}
					},
					error: function(data){
						console.log("error");
						console.log(data);
					}
				});
			}
		if(partnerId.value == 66422)
			{
				//$('#awb_num').prop('disabled', true);
				$('.loader').show();
				$('.page-overlay').show();
				$.ajax({
					type:'POST',
					url: "<?php echo base_url(); ?>Manage_orders/pickerr_awb_no",
					data:{'order_unique_number':order_unique_number},
					dataType: "json",
					cache:false,
					success:function(data){  
					//alert(data.awp_no);
					if(data.success!='true')
					  {
						$('.loader').css('display','none');
						$('#awb_num').val(data.awp_no);
						$('#awb_num_label').html(data.awp_no);
						$('#awb_reason').html('<span style="color:green">Order Created Successfully ...!</span>');
						}
					else{
							$('#awb_reason').html('<span style="color:red">'+data.err+'. Please try another delivery partner. </span>');
							//alert('Somthing went wrong. Please try after sometime');
							//get_aws_no(partnerId);
							//location.reload();
							
						}
					},
					error: function(data){
						console.log("error");
						console.log(data);
					}
				});
			}
			}
			 else {
				 $("#")
			 }
		}		
		
		
 
		
</script>

<script type="text/Javascript">
$(function () {
      $("#date_from").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true});
      $("#date_to").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true });
	  // $('#date').datepicker({format: "yyyy-mm-dd"});
      $(".nav-tabs a").click(function(){
        $(this).tab('show');
      });
      $('.nav-tabs a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
      });
    });

$( document ).ready(function() {
$("body").addClass('sidebar-collapse');

$('.btn-quick-view').click( function(event){

  var orderUniqueId = $(this).attr('id');
     event.preventDefault();


  if(orderUniqueId!='')
  {
    dispalyOrderDetail(orderUniqueId);
  }

//  $(this).parents('tr').next('.tr-quick-view').find('.box-quick-view').slideToggle(500);
  //$(this).parents('tr').toggleClass("open");
  var elem = $(this).parents('tr').next('tr.tr-quick-view');
  elem.slideToggle('10');
  $(this).parents('tr').toggleClass("open");

});

/*$('.btn-quick-close').click( function(){
  alert('hhii');
  alert($(this).attr(id));
  $('.table-tr-collapse tr.tr-quick-view .box-quick-view').slideUp();
  $('.table-tr-collapse tr').removeClass("open");

});*/


 /*$('[data-toggle=confirmation]').confirmation({
    title:'Are you sure?',
    onConfirm : function(){

      var edit_id = $(this).closest('td').find('.btn-edit').attr('data-id');
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "View_orders/",
        data: { id: edit_id },
        cache:false,
        success:
        function(data){
          location.reload(); //as a debugging message.
        }

      });
    }
  });
*/
/*
  $( ".orderStatus" ).change(function() {

    var orderStatus = $(this).val();
    var order_prd_id = $(this).attr('order');

    if(order_prd_id!='')
    {
       var result = window.confirm('Are you sure?');
       if(result == true)
       {
          $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "Manage_orders/updateOrder",
            data: { id: order_prd_id, orderStatus: orderStatus },
            cache:false,
            success:
            function(data){
              location.reload(); //as a debugging message.
            }
          });

       }else
       {

       }
    }
  });*/

 $('.nav-item-order').addClass("active");
  $('.nav-manage-orders').addClass("active");
});

function downloadExcel()
{
     var search_by = '';
     var table_search = '';
     var encode_table_search = '';
     $("#is_download").val('1');
     search_by = $('#search_by').val();
     search_by = search_by.trim();
     table_search = $('#table_search').val();     
     $('#table_search').removeAttr('required');
     table_search = table_search.trim();
     var is_download = $("#is_download").val();

     //encode_table_search = Base64.encode(table_search);
     //encode_table_search = <?php echo utf8_encode(table_search); ?>;

     if(search_by == 6)
     {
        encode_table_search = window.btoa(table_search);
     }if(search_by == 5)
     {
        encode_table_search = $('#table_search_status').val();        
     }else if(search_by == 8)
     {
        var dateFrom = $("#date_from").val();
        var dateTo = $("#date_to").val();

        var date_search = dateFrom+','+dateTo;
        encode_table_search = window.btoa(date_search);
     }
     else
     {
        encode_table_search = table_search;
     }
     if(!encode_table_search){ encode_table_search = '0'; }
    	search_by2 = $('#search_by2').val();
    	search_by2 = search_by2.trim();
     //alert(encode_table_search);
     //var _href = '<?php echo base_url(); ?>'+'View_orders_payment/exportExcel/downloadExcel';

     var newhref = '<?php echo base_url(); ?>'+'manage_orders/exportExcel/downloadExcel'+'/'+search_by+'/'+encode_table_search+'/'+is_download+'/'+search_by2;
     //alert(newhref);

     $('#frmsearch').attr('action',newhref);

     //location.reload();
     /*var link = document.createElement('a');
     link.href = newhref;
     document.body.appendChild(link);
     link.click();*/
}

/*function send_order_email(order_unique_id)
{
  var result = window.confirm('Are you sure ?');
   if(result == true)
    {
      if(order_id != null){
          $.ajax({
            url: "<?php echo base_url(); ?>Manage_orders/send_mail",
            type: 'POST',
            data: {'order_id':order_id,'product_id':product_id,'order_status':order_status},
            cache: false,
            async: false,
            success: function(response) {
              alert("Email sent to user");
              if(response == 1)
              {
                location.reload();
              }else{
                location.reload();
              }

            }
          });
          }
     }else
     {

     }
}
*/
function send_email(order_id,product_id,order_status,orderUniqueId,brand_id){
	var result = window.confirm('Are you sure?');
	 if(result == true)
    {
		  if(order_id != null){
					$.ajax({
						url: "<?php echo base_url() ?>manage_orders/send_mail",
						type: 'POST',
						data: {'order_id':order_id,'product_id':product_id,'order_status':order_status,'brand_id':brand_id},
						cache: false,
						async: false,
						success: function(response) {
							alert("Email sent to user");
							if(response == 1)
							{
								dispalyOrderDetail(orderUniqueId);
							}else{
								dispalyOrderDetail(orderUniqueId);
							}

						}
					});
				  }
	   }else
     {

     }
}

function submitForm(){
   $("#is_download").val('0');
   var newhref = '<?php echo base_url(); ?>'+'manage_orders/';
   $('#frmsearch').attr('action',newhref);
   $('#frmsearch').submit();
}

var filterType = $('#search_by').val();
if(filterType == 8)
{
    $("#filter").hide();
    $("#daterange").show();
    $("#table_search").removeAttr('required');
    $("#date_from").attr('required','');
    $("#box_filter").hide();
}else if(filterType == 10)
{
  $("#status_filter").hide();
  $("#filter").hide();  
  $("#box_filter").show();
  $("#daterange").hide();

}else if(filterType == 5)
{
  $("#filter").hide(); 
  $("#status_filter").show();
  $("#box_filter").hide();
  $("#daterange").hide();  
}



 $("#search_by").change(function(){

    var filterType = $(this).val();
    if(filterType == 8)
    {
        $("#filter").hide();
        $("#daterange").show();
        $("#table_search").removeAttr('required');
        $("#date_from").attr('required','');
        $("#box_filter").hide();
        $("#status_filter").hide();
    }else if(filterType == 10)
    {
      $("#status_filter").hide();
      $("#filter").hide();     
      $("#box_filter").show();
      $("#daterange").hide();

    }else if(filterType == 5)
    {
      $("#filter").hide(); 
      $("#status_filter").show();
      $("#box_filter").hide();
      $("#daterange").hide();
    }else
    {
      $("#filter").show();
      $("#daterange").hide();
      $("#box_filter").hide();
      $("#status_filter").hide();
    }
    if(filterType == 6){
        $(".msg-days-lapsed").show();
    }else
    {
        $(".msg-days-lapsed").hide();
    }

  });

 /* function getproduct_info(order_id){

	  $.ajax({
            url: "<?php echo base_url() ?>Manage_orders/getorder_info",
            type: 'POST',
            data: {'order_id':order_id},
            cache :true,
            async: false,
            success: function(response) {

			 // $('#product_info').html(response);
            }
        });
}*/

function order_close(orderUniqId)
{
  $('.table-tr-collapse tr.tr-quick-view').slideUp(10);
  $('.table-tr-collapse tr').removeClass("open");
}

function SetOrderStatus(orderId,orderUniqueId,cartId)
{
    var orderStatus = $('#orderStatus_'+orderId).val();
    var order_prd_id = orderId;
	
	if(orderStatus == 3)
	{
		//alert(orderStatus);
		$('#cartidforawb').val(cartId);
        $('#orderprdidforawb').val(order_prd_id);
        $('#orderuniqueidforawb').val(orderUniqueId);
		
		var isDone = false;

		$.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Manage_orders/gat_cart_data",
            data: { 'cartId': cartId, 'id' : order_prd_id, 'orderStatus': orderStatus},
            cache:false,
			dataType:'json',
            success:
            function(data){
				$.each(data, function(i, cartMeta){

					if(isDone ==false){
						$("#delivery_partner").val(cartMeta.cart_meta_value);
						isDone = true;
					}
					$("#awb_num").val(cartMeta.cart_meta_value);
					if(i>0){
						msg('disable.....');
						$("#delivery_partner, #awb_num, #dispatch").attr('disabled',true);
					} 
				});
				
            }
          });
		  $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Manage_orders/get_user_data",
            data: {'orderUniqueId': orderUniqueId },
            cache:false,
			dataType:'json',
            success:
            function(data){
				$.each(data, function(i, userData){
					//alert(userData.order_unique_no);
					//alert(userData.state_name);
					$("#awb_order_unique_no").val(userData.order_unique_no);
					$("#awb_order_display_no").val(userData.order_display_no);
					$("#awb_customer_fname").val(userData.first_name);
					$("#awb_customer_lname").val(userData.last_name);
					$("#awb_customer_mobile").val(userData.mobile_no);
					$("#awb_customer_pincode").val(userData.pincode);
					$("#awb_customer_address").val(userData.shipping_address);
					$("#awb_customer_city").val(userData.city_name);
					$("#awb_customer_state").val(userData.state_name);
					$("#awb_customer_state").val(userData.state_name);
					
				});
            }
          });
		$('#addAwb').modal('show');
		$('#addAwb').removeData();
		
	}
    if(order_prd_id!='' && orderStatus !=3)
    {
       var result = window.confirm('Are you sure?');
       if(result == true)
       {
        
          $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "manage_orders/updateOrder",
            data: { id: order_prd_id, orderStatus: orderStatus,orderUniqueId:orderUniqueId },
            dataType:"json",
            cache:false,
            success:
            function(data){ 
              if(data.response)
              {
                alert(data.message);
              }else
              {
                alert(data.message);
              }
             // alert("Order Status updated Successfully");              
               dispalyOrderDetail(orderUniqueId);
                $(".loader").show();
                location.reload(); //as a debugging message.
            }
          });

       }else
       {

       }
    }
	/*if(orderStatus == 8){
		//alert(orderId+'-'+orderUniqueId+'-'+cartId);
		var result = window.confirm('Are you sure?');
		   if(result == true)
		   {
				 $.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>" + "stylist/sendwarehouse_email",
				data: { 'id': orderId, 'orderUniqueId':orderUniqueId,'cartId':cartId },
				//dataType:"json",
				cache:false,
				success:
				function(data){ 
					alert('Email Send successfully...');
				}
			  }); 
			}
		}*/
}




function saveRemark(recordId)
{  
    if(recordId!='')
    {
       var remarkRecord = $('#remark_'+recordId).val();       
       if(remarkRecord!='')
       {
          $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "manage_orders/updateRemark",
            data: { id: recordId, remark: remarkRecord },
            cache:false,
            success:
            function(data){             
              if(data==true)
              {
                $('#remarkMsg_'+recordId).html('Remark Updated');              
              }                
            }
          });
      }
       
    }
}

function dispalyOrderDetail(orderUniqueId)
{
	
   $.ajax({
              url: "<?php echo base_url(); ?>manage_orders/orderDetailsView",
              type: 'POST',
              data: {'order_unique_id':orderUniqueId},
              cache :true,
              async: false,
              success: function(response) {
                  $('#orderDetail_'+orderUniqueId).html(response);
				  $(".date_to").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true });
              },
              error: function(xhr) {

              }
    });
}

function change_order_status(order_id,first_name,last_name,user_id){
    
    var result = window.confirm('Are you sure?');
    if(result == true){
      $('.page-overlay').show();
      $('.loader').show();
      var order_status = $('#otp_confirmation_'+order_id).val();

      $.ajax({
              url: "<?php echo base_url(); ?>manage_orders/change_order_status",
              type: 'POST',
              data: {'order_id':order_id,'order_status':order_status,'first_name':first_name,'last_name':last_name,'user_id':user_id},
              cache :true,
              async: false,
              success: function(response) {
                  location.reload();
              },
              error: function(xhr) {

              }
      });

    }
}

function assignOrderDp(orderUniqueNo)
{
  /*var dop = $('#dop_'+orderUniqueNo).val();*/  
  var docheck = [];  
  $.each($("input[name='dop_"+orderUniqueNo+"']:checked"), function(){            
      docheck.push($(this).val());
  });
     
    if(docheck!='') 
    {      
      $.ajax({
              url : "<?php echo base_url(); ?>manage_orders/saveOrderDp",
              type : "POST",
              data : {'dop':docheck},
              cache : true,
              async : false,
              success : function(response)
              {
                $('#Updatemessage_'+orderUniqueNo).html('<span class="text-green">Order Assigned to Delivery Partner Successfully</span>');
              },
              error : function(xhr)
              {
              }
            });
    }else
    {
      alert("Check Order to Assign Delivery Partner");
    }
}

function change_final_order_status(order_id,user_id){
	var result = window.confirm('Are you sure?');
    if(result == true){
		$('.page-overlay').show();
		$('.loader').show();
		var final_order_status = $('#final_order_status'+order_id).val();
		
		$.ajax({
			  url: "<?php echo base_url(); ?>manage_orders/change_final_order_status",
			  type: 'POST',
			  data: {'order_id':order_id,'order_status':final_order_status,'user_id':user_id},
			  cache :true,
			  async: false,
			  success: function(response) {
				  location.reload();
			  },
			  error: function(xhr) {

			  }
		});

    }
}

function save_order_data(order_id){
	
	var txnid = $('#txnid_'+order_id).val();
	var payu_id = $('#payu_id_'+order_id).val();
	var paid_price = $('#paid_price_'+order_id).val();
	var payment_date = $('#payment_date_'+order_id).val();
	var is_paymentdone = $('#is_paymentdone'+order_id).val();
	$.ajax({
			  url: "<?php echo base_url(); ?>manage_orders/save_tax_id",
			  type: 'POST',
			  data: {'order_id':order_id,'txnid':txnid,'payu_id':payu_id,'paid_price':paid_price,'payment_date':payment_date,'is_paymentdone':is_paymentdone},
			  cache :true,
			  async: false,
			  success: function(response) {
				  location.reload();
			  },
			  error: function(xhr) {

			  }
		});
	
}

function get_order_popup(container){
	var order_id = $(container).attr("data-orderid");
	
	$.ajax({
		url: "<?php echo base_url(); ?>manage_orders/get_order_popup",
		type: 'post',
		data: { 'order_id' : order_id },
		success: function(data, status) {
			$('#box_order_data').html(data);
			$(".date_to").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true });
			$("#modal-order-info").modal('show');
		}
	});
}

function download_pdf(order_display_no){
	var newhref = '<?php echo base_url(); ?>'+'manage_orders/download_pdf/'+order_display_no;
	// $('#frmsearch').attr('action',newhref);
	window.open(newhref, '_blank');
}

function download_stylist_extracted_data(){
	
	var search_by = '';
	var table_search = '';
	$("#is_download").val('1');
	search_by = $('#search_by').val();
	//alert('name'+search_by);
	search_by = search_by.trim();
	table_search = $('#table_search').val();
	
	$('#table_search').removeAttr('required');
	table_search = table_search.trim();
	var is_download = $("#is_download").val();
	if(search_by == 6){
		encode_table_search = window.btoa(table_search);
	}else if(search_by == 8){
		var dateFrom = $("#date_from").val();
		var dateTo = $("#date_to").val();
		var date_search = dateFrom+','+dateTo;
		encode_table_search = window.btoa(date_search);
	}else{
		encode_table_search = table_search;
	}
	if(search_by == 2)
	{
		encode_table_search = table_search.replace(/\s+/g, '-');
		//alert('search '+search_by);
		//alert('name '+encode_table_search);
	}
	if(!encode_table_search){ encode_table_search = '0'; }
	search_by2 = $('#search_by2').val();
	search_by2 = search_by2.trim();

	var newhref = '<?php echo base_url(); ?>'+'manage_orders/download_stylist_extracted_data/'+search_by+'/'+encode_table_search+'/'+is_download+'/'+search_by2;
	$('#frmsearch').attr('action',newhref);
	
	// var newhref = '<?php echo base_url(); ?>'+'Manage_orders/download_stylist_extracted_data/'+search_by+'/'+table_search+'/'+is_download;
	// $('#frmsearch').attr('action',newhref);
}

function excelDownload(){
  
  var search_by = '';
  var table_search = '';
  $("#is_download").val('1');
  search_by = $('#search_by').val();  
  search_by = search_by.trim();
  table_search = $('#table_search').val();
  
  $('#table_search').removeAttr('required');
  table_search = table_search.trim();
  var is_download = $("#is_download").val();
  if(search_by == 6){
    encode_table_search = window.btoa(table_search);
  }else if(search_by == 8){
    var dateFrom = $("#date_from").val();
    var dateTo = $("#date_to").val();
    var date_search = dateFrom+','+dateTo;
    encode_table_search = window.btoa(date_search);
  }else{
    encode_table_search = table_search;
  }
  if(search_by == 2)
  {
    encode_table_search = table_search.replace(/\s+/g, '-');   
  }
  if(!encode_table_search){ encode_table_search = '0'; }
  search_by2 = $('#search_by2').val();
  search_by2 = search_by2.trim();

  var newhref = '<?php echo base_url(); ?>'+'manage_orders/export_stylist_data/'+search_by+'/'+encode_table_search+'/'+is_download+'/'+search_by2;
  $('#frmsearch').attr('action',newhref);  
}

function myFunction() {
    document.getElementById("demo").innerHTML = Math.random();
    document.getElementById("btnSubmit").disabled = true;
}
</script>
<script type="text/javascript">


      $('div.product-img img').error( function() {
            $(this).attr('src', 'http://www.adbazar.pk/frontend/images/default-image.jpg');
            });

</script>
