<?php
$start = microtime(true);
//echo "view_order"."<br/><pre>";
//print_r($orders);
//print_r($orderStatusList);

//echo '<pre>';print_r($this->session->userdata());
$imagify_brandid = unserialize(IMAGIFY_BRANDID);
$image_array = array();
$imageString  ='';
$productString = '';
foreach($orders as $val)
{ $i++; $new_order_id = $val['order_id'];
   //echo $new_order_id."<br/>";
   if($new_order_id != $old_order_id)
    {
      $imageString = $val['image'];
      $productString = $val['product_name'];
    }else
    {
      $imageString = $imageString.','.$val['image'];
      $productString =  $productString.','.$val['product_name'];
    }
    $image_array[$val['order_id']] =  $imageString;
    $product_array[$val['order_id']] = $productString;
    $old_order_id = $val['order_id'];
}

$users_menu=$this->data['users_menu'];
$table_search = $table_search == '0' ? '' : $table_search;

?>
<section class="content-header">
  <h1>Manage Orders Payment <small class="label label-info"><?php echo @$total_rows; ?></small></h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">Manage Orders Payment</li>
  </ol>
</section>
<section class="content">
<div class="box">
<div class="box-body">
  <?php echo $this->session->flashdata('feedback'); ?>
  <form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>View_orders_payment" method="post">
    <input type="hidden"  name="is_download" id="is_download" value="0" />
    <div class="row">
      <div class="col-md-2">
        <?php $search_by = set_value('search_by')=='' ? $search_by : set_value('search_by'); ?>
        <select name="search_by" id="search_by" class="form-control input-sm" >
          <option value="0">Search by</option>
          <option value="1" <?php echo $search_by==1 ? 'selected' : ''; ?> >Order ID</option>
          <option value="2" <?php echo $search_by==2 ? 'selected' : ''; ?> >Username</option>
          <option value="3" <?php echo $search_by==3 ? 'selected' : ''; ?> >Company Name</option>
          <option value="4" <?php echo $search_by==4 ? 'selected' : ''; ?> >Date</option>
          <option value="5" <?php echo $search_by==5 ? 'selected' : ''; ?> >Status</option>
          <option value="6" <?php echo $search_by==6 ? 'selected' : ''; ?> >Days Lapsed</option>
          <option value="7" <?php echo $search_by==7 ? 'selected' : ''; ?> >Payment Status</option>
          <option value="8" <?php echo $search_by==8 ? 'selected' : ''; ?> >Date Range</option>
        </select>
      </div>
      <div class="col-md-3">
        <div class="input-group" >
        <div id ="filter" style="display: block;" >
          <input type="text" id="table_search" name="table_search" value="<?php echo set_value('table_search')=='' && set_value('table_search')==0 ? $table_search : set_value('table_search'); ?>" id="table_search" class="form-control input-sm pull-right" placeholder="Search"  required=""/>
          </div>

          <div id ="daterange" style="display: none;">
            <div class="row">
              <div class="col-md-5">
                <div class="form-group">
                  <input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm" id="date_from" value="<?php echo set_value('date_from'); ?>">
                </div>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="<?php echo set_value('date_to'); ?>">
              </div>
          </div>
            </div>
          </div>
          <div class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button" onclick="submitForm();"><i class="fa fa-search"></i></button>
            <!--<button title="Download Excel"  id="download_excel" class="btn btn-primary btn-xs"  href="<?php echo base_url(); ?>View_orders_payment/exportExcel/downloadExcel"  ><i class="fa fa-file-excel-o"></i> Download Excel</button>-->
          </div>
        </div>
      </div>
      <div class="col-md-2">
        <a href="<?php echo base_url(); ?>View_orders_payment" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
      </div>


      <!--<a title="Download Excel" href="<?php echo base_url(); ?>View_orders_payment/index/downloadExcel" class="btn btn-primary btn-xs"><i class="fa fa-file-excel-o"></i> Download Excel</a>-->
      <!--<a title="Download Excel" href="<?php echo base_url(); ?>View_orders_payment/exportExcel/downloadExcel"  class="btn btn-primary btn-xs"><i class="fa fa-file-excel-o"></i> Download Excel</a>-->
    <!--  <form name="frmdownload" id="frmdownload" class="form-group" action="<?php echo base_url(); ?>View_orders_payment" method="post"> -->
      <div class="col-md-5">
      <div>
       <button title="Download Excel" id="download_excel" class="btn btn-primary btn-sm"  onclick="downloadExcel();"><i class="fa fa-file-excel-o"></i> Download Excel</button>
       &nbsp;
       <?php //if($this->session->userdata('role_id')==1)
              //{
       ?>
        <button title="Download Excel" id="download_excel_new" class="btn btn-primary btn-sm"  onclick="downloadExcelnew();"><i class="fa fa-file-excel-o"></i>GST Download Excel</button>
       <?php
              //}
        ?>
      <div class="pull-right">
        <?php echo $this->pagination->create_links(); ?>
      </div>
      </div>
  </div>
</div>
<p class="text-muted msg-days-lapsed" style="display:none;">Note: Days Lapsed should be entered as (>,=,<) days. e.g. >10</p>
</form>
<div class="table-responsive">
  <table class="table table-bordered table-striped dataTable table-orders ">
    <thead>
      <tr>
        <!--<th>Sr. No.</th>-->
        <th>Date</th>
        <th>Order ID</th>
        <th>Product Name</th>
        <th>Qty</th>
        <!--<th>Size</th>-->
        <th>Company Name</th>
        <th>Order Total</th>
        <th>Payment Type</th>
        <th>Status</th>
        <th>Days Lapsed</th>
        <th width='13%'>Payment Status</th>
        <th>User Info</th>
      </tr>
    </thead>
    <tbody>
    <?php
      $i = 0;
       $new_order_id = ''; $old_order_id = '';
    if(!empty($orders))
    {
      foreach($orders as $val)
      { $i++; $new_order_id = $val['order_id'];
    if($new_order_id != $old_order_id){
    ?>
        <tr>
        <!--
        <?php if($sr_offset > 0){  ?>
          <td><?php echo $i+($sr_offset*10); ?></td>
        <?php }else{ ?>
          <td><?php echo $i; ?></td>
        <?php } ?>-->
          <td><?php echo $val['created_datetime']; ?></td>
          <td><?php echo $val['order_id']; ?></td>
          <td><?php echo $val['product_name']; ?></td>
          <td><?php echo $val['product_qty']; ?></td>
          <!--<td><?php echo $val['size_text']; ?></td>-->
          <td><?php echo $val['company_name']; ?></td>
          <?php $order_total_mf = number_format($val['order_total'], 2, '.', ','); ?>
          <td style="width:90px;"><i class="fa fa-inr"></i> <?php echo $order_total_mf; ?></td>
          <td><?php
          if($val['pay_mode'] == 1)
          {
            echo 'COD';
          }else
          {
            echo 'Online';
          }
          ?></td>
          <td>
            <?php foreach($orderStatusList as $key=>$value)
                {
                  if($val['order_status'] == $key)
                  {
            ?>
                    <?php echo $value;?>
            <?php }
                }     ?>
          </td>
          <td><?php echo $val['daysLapsed']." day"; ?></td>
          <td>
            <select class="form-control input-sm orderpayStatus" name="orderpayStatus" <?php echo $val['payment_status']==2 ? 'disabled' : ''; ?> order="<?php echo $val['order_prd_id'];?>" >
            <?php foreach($orderPayStatusList as $key=>$value)
                {
                  if($val['payment_status'] == $key)
                  {
            ?>
                    <option  value='<?php echo $key;?>' selected ><?php echo $value;?></option>
            <?php }else{?>
                    <option  value='<?php echo $key;?>' ><?php echo $value;?></option>
            <?php      }
                }?>
            </select>
          </td>
          <td><a class="btn btn-primary btn-xs" data-toggle="modal" data-target="#<?php echo $val['order_prd_id'];?>">View</a></td>
          </tr>

            <div id="<?php echo $val['order_prd_id'];?>" class="modal" role="dialog">
             <div class="modal-dialog order-view-popup">
               <div class="modal-content">
            <div class="modal-header">
            <h4>Order Details: <?php echo $val['order_id']; ?></h4>
            <button type="button" class="close pull-right" data-dismiss="modal">×</button>
            <div class="clearfix"></div>
            </div>
               <div class="modal-body">

              <div class="row">
                <div class="col-md-5">
                  <dl>
                  <dt>Name</dt>
                  <dd><?php echo $val['first_name']." ".$val['last_name']; ?>
                    </dd>
                    <dt>
                    Phone  </dt>
                        <dd><?php echo $val['mobile_no']; ?>
                    </dd>
                    <dt>
                      Email  </dt>
                        <dd><?php echo $val['email_id']; ?>
                    </dd>

                    <dt>
                    Shipping Address  </dt>
                    <dd> <?php echo $val['shipping_address']; ?>
                    <br/>
                      <?php echo $val['city_name']." ".$val['pincode']." ".$val['state_name']; ?>
                    </dt>
                  </dl>
                  </div>
                  <div class="col-md-7">
                    <div class="order-view-product-list">
                     <div class="row">
                    <?php
                    $image = explode(',',$image_array[$val['order_id']]);
                    $productName = explode(',',$product_array[$val['order_id']]);
                    $k = 0;
                    $imgcount = count($image);
                          foreach($image as $img)
                          {
                            // added for new product url  
							$productUrl = '';
							if(in_array($val['brand_id'],$imagify_brandid)){
								$productUrl = $this->config->item('products_tiny').$val['image'];
							}else{
								$productUrl = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_124/'.$val['image'];                
							} 
							// added for new product url
                    ?>
                    <div class="<?php  echo $imgcount >1 ? 'col-md-6' : 'col-md-12'; ?>">
                    <div class="img-wrp">
                        <img src="<?php echo $productUrl; ?>">
                         </div>
                        <div class="name">
                         <strong><?php echo $productName[$k]; ?></strong>
                        </div>

                    </div>
                      <?php
                          $k++;
                          }
                      ?>
                    </div>

                  </div>
              </div>

              </div>
              </div>
              </div>


      <?php
        } else{ ?>
        <tr>
          <td></td><td></td>
          <td><?php echo $val['product_name']; ?></td><td><?php echo $val['product_qty']; ?></td><td><?php //echo $val['size_text']; ?></td><td></td><td></td>
          <td></td>
          <td></td><td>
          <select class="form-control input-sm orderpayStatus" name="orderpayStatus" <?php echo $val['payment_status']==2 ? 'disabled' : ''; ?> order="<?php echo $val['order_prd_id'];?>" >
            <?php foreach($orderPayStatusList as $key=>$value)
                {
                  if($val['payment_status'] == $key)
                  {
            ?>
                    <option  value='<?php echo $key;?>' selected ><?php echo $value;?></option>
            <?php }else{?>
                    <option  value='<?php echo $key;?>' ><?php echo $value;?></option>
            <?php      }
                }?>
            </select>
            </td><td></td>
        </tr>
      <?php } 
        $old_order_id = $val['order_id']; }
      }else
      {
      ?>
         <tr>
          <td colspan="10">Sorry no record found</td>
        </tr>
    <?php
      }
      ?>
    </tbody>

  </table>
</div>

<?php echo $this->pagination->create_links(); ?>
</div>
</div>

</section>
<style>
.table > tbody .tr-collapse td{padding: 0;}
.table.table-orders th{vertical-align: top;}
.tr-collapse .row{margin: 0; padding: 10px; }
.tr-collapse label{font-weight: normal;}
.modal-header{background: #000; color:#fff; height: auto; padding: 8px 15px;}
.modal-header h4{font-size: 16px;margin: 0; float: left;}
.order-view-popup dd{margin-bottom: 10px;}
.order-view-product-list .img-wrp{ /*width: 48%; display: inline-block; padding:0 5px; margin-bottom: 10px; */

    background: #fff none repeat scroll 0 0;
    border: 1px solid #ececec;
    height: 150px;
    padding: 10px;
    text-align: center;
    margin-bottom: 5px;
}
.order-view-popup .close{color:#fff; opacity:1;}
.order-view-product-list .img-wrp img{
    height: auto;
    max-height: 100%;
    max-width: 100%;
    width: auto;
}
.order-view-popup .name{line-height: 1.3; margin-bottom: 5px;}
.msg-days-lapsed{margin-top: 10px; font-size: 12px;}
</style>

<script type="text/Javascript">

$(function () {
      $("#date_from").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true});
      $("#date_to").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true });
      $(".nav-tabs a").click(function(){
        $(this).tab('show');
      });
      $('.nav-tabs a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
      });
    });

    $('.nav-item-order').addClass("active");
    $('.nav-manage-orders-payment').addClass("active");

function downloadExcel()
{
     var search_by = '';
     var table_search = '';
     $("#is_download").val('1');
     search_by = $('#search_by').val();
     search_by = search_by.trim();
     table_search = $('#table_search').val();
     $('#table_search').removeAttr('required');
     table_search = table_search.trim();
     var is_download = $("#is_download").val();

     //encode_table_search = Base64.encode(table_search);
     //encode_table_search = <?php echo utf8_encode(table_search); ?>;

     if(search_by == 6)
     {
        encode_table_search = window.btoa(table_search);
     }else if(search_by == 8)
     {
        var dateFrom = $("#date_from").val();
        var dateTo = $("#date_to").val();

        var date_search = dateFrom+','+dateTo;
        encode_table_search = window.btoa(date_search);
     }
     else
     {
        encode_table_search = table_search;
     }
     if(!encode_table_search){ encode_table_search = '0'; }

     //alert(encode_table_search);
     //var _href = '<?php echo base_url(); ?>'+'View_orders_payment/exportExcel/downloadExcel';

     var newhref = '<?php echo base_url(); ?>'+'View_orders_payment/exportExcel/downloadExcel'+'/'+search_by+'/'+encode_table_search+'/'+is_download;
     //alert(newhref);

     $('#frmsearch').attr('action',newhref);

     //location.reload();
     /*var link = document.createElement('a');
     link.href = newhref;
     document.body.appendChild(link);
     link.click();*/
}

function downloadExcelnew()
{
     var search_by = '';
     var table_search = '';
     $("#is_download").val('1');
     search_by = $('#search_by').val();
     search_by = search_by.trim();
     table_search = $('#table_search').val();
     $('#table_search').removeAttr('required');
     table_search = table_search.trim();
     var is_download = $("#is_download").val();

     //encode_table_search = Base64.encode(table_search);
     //encode_table_search = <?php echo utf8_encode(table_search); ?>;

     if(search_by == 6)
     {
        encode_table_search = window.btoa(table_search);
     }else if(search_by == 8)
     {
        var dateFrom = $("#date_from").val();
        var dateTo = $("#date_to").val();

        var date_search = dateFrom+','+dateTo;
        encode_table_search = window.btoa(date_search);
     }
     else
     {
        encode_table_search = table_search;
     }
     if(!encode_table_search){ encode_table_search = '0'; }

     //alert(encode_table_search);
     //var _href = '<?php echo base_url(); ?>'+'View_orders_payment/exportExcel/downloadExcel';

     var newhref = '<?php echo base_url(); ?>'+'View_orders_payment/exportExcelgst/downloadExcelgst'+'/'+search_by+'/'+encode_table_search+'/'+is_download;

     $('#frmsearch').attr('action',newhref);

}

function submitForm(){
   $("#is_download").val('0');
   var newhref = '<?php echo base_url(); ?>'+'View_orders_payment/';

   $('#frmsearch').attr('action',newhref);
   $('#frmsearch').submit();
}

$( document ).ready(function() {

  $( ".orderpayStatus" ).change(function() {

    var orderStatus = $(this).val();
    var order_prd_id = $(this).attr('order');

    if(order_prd_id!='')
    {
       var result = window.confirm('Are you sure?');
       if(result == true)
       {
          $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "View_orders_payment/updatepayOrder",
            data: { id: order_prd_id, orderpay_Status: orderStatus },
            cache:false,
            success:
            function(data){
              location.reload(); //as a debugging message.
            }
          });

       }else
       {
       }
    }
  });

});

var filterType = $('#search_by').val();
if(filterType == 8)
{
    $("#filter").hide();
    $("#daterange").show();
    $("#table_search").removeAttr('required');
    $("#date_from").attr('required','');
}

 $("#search_by").change(function(){

    var filterType = $(this).val();
    if(filterType == 8)
    {
        $("#filter").hide();
        $("#daterange").show();
        $("#table_search").removeAttr('required');
        $("#date_from").attr('required','');

    }else
    {
       $("#filter").show();
      $("#daterange").hide();


    }
      if(filterType == 6){      
        $(".msg-days-lapsed").show();
    }else
    {
        $(".msg-days-lapsed").hide();
    }

  });
</script>
