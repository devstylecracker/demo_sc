<?php 
if($status == 1){ $order_status = " processing";}
else if($status == 2){ $order_status = " Confirmed";}
else if($status == 3){	$order_status = " Dispatched";}
else if($status == 4){ $order_status = " Delivered";}
else if($status == 5){ $order_status = " Cancelled";}
else if($status == 6){ $order_status = " Return";}
else if($status == 7){ $order_status = " Fake";}
$total_rows = count($result);
//echo $order_status;
 ?>
 <section class="content-header">
          <h1>Orders <?php echo $order_status; ?>&nbsp;&nbsp;<small class="label label-info"><?php echo $total_rows; ?></small></h1>
            <ol class="breadcrumb">
              <li><a href="<?php echo base_url(); ?>home">Home</a></li>
              <li class="active">Orders</li>
            </ol>
  </section> 
	<section class="content">
    <div class="box">
    <div class="box-body">  
    
	<div class="box-body">
		  <div class="table-responsive">
      <table class="table table-bordered table-striped dataTable" id="example2">
        <thead>
          <tr>
            <th>Sr.No.</th>
			<th>User id</th>
            <th>order ID</th>
			<th>Username</th>
			<th>order status</th>
            <th>Product ID</th>
            <th>Product Name</th>   
			<th>Price</th>
			<th>created datetime</th> 
          </tr>
        </thead>
        <tbody>
      		 <?php if(!empty($result)) { 
               $i = $offsetvar+1;     

            foreach($result as $val){ ?>
      		  <tr>
                <td><?php echo $i; ?></td>
      			  <td><a href="<?php echo base_url(); ?>websiteusers/user_view/<?php echo $val['user_id']; ?>" target="_blank"><?php echo $val['user_id']; ?></a></td>
				<td><?php echo $val['order_id']; ?></td>
                <td><?php echo $val['first_name']." ".$val['last_name']; ?></td>
				<td><?php echo $val['order_status']; ?></td>
				<td><?php echo $val['product_id']; ?></td>
				<td><?php echo $val['name']; ?></td>
                 <td><?php echo $val['price']; ?></td>
				  <td><?php echo $val['created_datetime']; ?></td>
      		  </tr>
      		  <?php 
            $i++;} } ?>         
        </tbody>
      </table>
      </div>
		  <div>
          <?php echo $this->pagination->create_links(); ?>
      </div>
	 </div>
  </div>
</div>
</section>
