<?php
    $users_menu=$this->data['users_menu'];
    $table_search = $table_search == '0' ? '' : $table_search;  
    // echo '<pre>';print_r($orders);
?>
<section class="content-header">
  <h1>Manage Orders  <small class="label label-info"><?php echo @$total_rows; ?></small></h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">Manage Orders</li>
  </ol>
</section>
<section class="content">
<div class="box">
<div class="box-body">
  <?php echo $this->session->flashdata('feedback'); ?>
  <form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>AffiliateUsers" method="post">
  <input type="hidden"  name="is_download" id="is_download" value="0" />
    <div class="row">
      <div class="col-md-2">
        <?php $search_by = set_value('search_by')=='' ? $search_by : set_value('search_by'); ?>
        <select name="search_by" id="search_by" class="form-control input-sm" >
          <option value="0">Search by</option>
          <!-- <option value="1" <?php //echo $search_by==1 ? 'selected' : ''; ?> >Order ID</option> -->
          <option value="9" <?php echo $search_by==9 ? 'selected' : ''; ?> >Customer Order ID</option>
          <option value="2" <?php echo $search_by==2 ? 'selected' : ''; ?> >Customer Name</option>
         <!--  <option value="3" <?php echo $search_by==3 ? 'selected' : ''; ?> >Company Name</option> -->
          <!-- <option value="4" <?php echo $search_by==4 ? 'selected' : ''; ?> >Date</option> -->
          <!--<option value="5" <?php echo $search_by==5 ? 'selected' : ''; ?> >Status</option>-->
          <!-- <option value="6" <?php echo $search_by==6 ? 'selected' : ''; ?> >Order Unique No.</option> -->
          <option value="8" <?php echo $search_by==8 ? 'selected' : ''; ?> >Date Range</option>
        </select>
      </div>
      <div class="col-md-3">
        <div class="input-group">
         <div id ="filter" style="display: block;" >
          <input type="text" name="table_search" value="<?php echo set_value('table_search')=='' && set_value('table_search')==0 ? $table_search : set_value('table_search'); ?>" id="table_search" class="form-control input-sm pull-right" placeholder="Search"  required=""/>
         </div>
        <div id ="daterange" style="display: none;">
            <div class="row">
              <div class="col-md-5">
                <div class="form-group">
                  <input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm" id="date_from" value="<?php echo set_value('date_from'); ?>">
                </div>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="<?php echo set_value('date_to'); ?>">
              </div>
             </div>
            </div>
        </div>
        <div class="input-group-btn">
          <button class="btn btn-sm btn-default" type="submit" onclick="submitForm();" ><i class="fa fa-search"></i></button>
        </div>
      </div>
      </div>
	   <div class="col-md-2 hide">
	   <select name="search_by2" id="search_by2" class="form-control input-sm" onchange = "submitForm();" >
          <option value="0" >Filter by</option>
          <option value="1" <?php echo $search_by2==1 ? 'selected' : ''; ?> >Normal Orders</option>
		  <option value="2" <?php echo $search_by2==2 ? 'selected' : ''; ?> >SCBOX Orders</option>
		  </select>
	</div>
      <div class="col-md-1">
        <a href="<?php echo base_url(); ?>AffiliateUsers" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
      </div>
	   
      <div class="col-md-4">
       <?php //if($this->session->userdata('role_id') != 6){ ?>
       
              <?php //} ?>
			 <!--  <button title="Download Excel" id="download_excel" class="btn btn-primary btn-sm"  onclick="downloadExcel();"><i class="fa fa-file-excel-o"></i> Download Excel</button> -->
        <div class="pull-right">		 
          <?php echo $this->pagination->create_links(); ?>
        </div>
      </div>
    </div>
<div><br/><b>Note:</b> Grand Total is inclusive of Service-Tax,COD, Shipping Charges, Coupon Code Deduction (if applicable) <br/>(ADP - Assign Delivery Partner)</div>
</form>
<div class="table-responsive">
  <table class="table table-bordered table-striped table-orders dataTable table-tr-collapse">
    <thead>
      <tr>
        <th width="20">Sr. No.</th>
        <th width="90">Date</th>
        <th>Order Unique ID</th>
        <!-- <th>Coupon Code & Referral</th> -->
        <th width="90">Total</th>
        <th width="90">Discounted Price </th>
        <th width="90">Order Total</th>
        <th width="60">Payment Type</th>
        <!-- <th width="70">Status</th> -->
        <th>Customer Name</th>
       <!--  <th width="90">Mobile Number</th>
        <th width="120">Platform<br/>UTM-source<br/>UTM-medium<br/>UTM-campaign</th> -->
        <!--  <th>Send Email</th> -->
        <!--<th width="90">Last updated</th>-->
		    <!--  <th width="90">Order Status</th> -->
        <!--  <th width="70">Details</th> -->
		    <!--  <th width="70">Payment Status</th> -->
      </tr>
    </thead>
    <tbody>
    <?php
      $i = 0;
      $new_order_id = ''; $old_order_id = '';
      $coupon_product_price = 0;
    if(!empty($orders))
    {
      foreach($orders as $val)
      { $i++;
          $imageString ='';$data['coupon_discount'] = 0;$scbox_productids=array();
          $referral_point = 0;$refDiscount = 0;$user_link = '';
          $referral_point = $this->Manage_orders_model->get_refre_amt($val['order_unique_no']);
          if($referral_point>0 && $val['coupon_code']!='')
          {
            $couponReferral_text = $val['coupon_code'].' & '.REDEEM_POINTS_CART_TEXT.'-'.$referral_point;
          }else if($val['coupon_code']!='')
          {
            $couponReferral_text = $val['coupon_code'];
          }else if($referral_point>0)
          {
            $couponReferral_text = REDEEM_POINTS_CART_TEXT.'-'.$referral_point;
          }else
          {
            $couponReferral_text = '';
          }

          $order_product_sum = $this->Manage_orders_model->get_order_price_ref($val['order_unique_no']);

          $scbox_productids = unserialize(SCBOX_PRODUCTID);
          if(in_array($val['product_id'], $scbox_productids))
          {
            $user_link = base_url().'websiteusers/scboxuser_view/'.$val['user_id'];
          }else
          {
            $user_link = base_url().'/websiteusers/user_view/'.$val['user_id'];
          }

    ?>
        <tr>
        <?php if($sr_offset > 0){  ?>
          <td><?php echo $i+($sr_offset*10); ?></td>
        <?php }else{ ?>
          <td><?php echo $i; ?></td>
        <?php } ?>
          <td><?php echo $val['created_datetime']; ?></td>
          <td><span class="text-blue"><?php echo $val['order_display_no']; ?><br/><?php //echo $val['order_unique_no']; ?></span></td>
          <!--<td><?php //echo $couponReferral_text; ?></td>-->
          <td><?php echo number_format(round($val['product_sum']), 2, '.', ','); ?></td>
          <!--<td><?php echo number_format(round($val['OrderGrandTotal']), 2, '.', ','); ?></td> -->
          <?php
/* Coupon Setting Page */
              if(isset($val['coupon_code']) && $val['coupon_code']!='')
              {
                $coupon_info = $this->Manage_orders_model->getUserCoupon($val['coupon_code'],$val['created_datetime'],$val['brand_id']);

                /*$data['coupon_discount'] = $coupon_info['coupon_amount'];
                $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];*/
                $coupon_brand = $coupon_info['brand_id'];
                $coupon_products = $coupon_info['coupon_products'];
                if($coupon_products!='' || $coupon_products!=0)
                {
                  $coupon_products_arr =explode(',',$coupon_products);
                }
                $coupon_min_spend = $coupon_info['coupon_min_spend'];
                $coupon_max_spend = $coupon_info['coupon_max_spend'];
                $coupon_discount_type = $coupon_info['coupon_discount_type'];
                $coupon_stylecracker =  $coupon_info['stylecrackerDiscount'];
                $individual_use_only =  $coupon_info['individual_use_only'];
                $data['coupon_discount'] = $coupon_info['coupon_amount'];
                $data['coupon_code'] = $val['coupon_code'];
                $coupon_product_price = 0;
                $price = $val['product_price'];


                 if($coupon_brand==$val['brand_id'] && $coupon_brand!=0)
                  {
                    if($coupon_products!=''&& $coupon_products!=0)
                    {
                       //coupon_discount_type =3 (Product discount)
                      if($coupon_discount_type==3)
                      {
                        if(in_array($val['product_id'], $coupon_products_arr))
                        {
                          if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                          {
                            $data['coupon_discount'] = $coupon_info['coupon_amount'];
                            $data['coupon_code'] = $val['coupon_code'];
                          }
                        }
                      }
                       //coupon_discount_type =4 (Product % discount)
                      if($coupon_discount_type==4)
                      {
                        if(in_array($val['product_id'], $coupon_products_arr))
                        {
                          if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                          {
                            $coupon_percent = $coupon_info['coupon_amount'];
                            $data['coupon_discount'] = $price*($coupon_percent/100);
                            $data['coupon_code'] = $val['coupon_code'];
                          }
                        }
                      }

                    }else
                    {
                       //coupon_discount_type =1 (cart discount)
                      if($coupon_discount_type==1)
                      {
                        $coupon_product_price = $coupon_product_price+$price;
                        if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                        {
                          $data['coupon_discount'] = $coupon_info['coupon_amount'];
                          $data['coupon_code'] = $val['coupon_code'];
                        }
                      }
                       //coupon_discount_type =2 (cart % discount)
                      if($coupon_discount_type==2)
                      {
                        $coupon_product_price = $coupon_product_price+$price;
                        if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                        {
                          $coupon_percent = $coupon_info['coupon_amount'];
                          $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                          $data['coupon_code'] = $val['coupon_code'];
                        }
                      }
                    }
                  }

                if(($coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only!=1  )|| ( $coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only==1 ))
                {
                    //coupon_discount_type =1 (cart discount)
                     $coupDiscountPrdPrice = $this->Manage_orders_model->calculate_product_discountAmt_order($val['product_price'],$val['OrderProductTotal'],$coupon_discount_type,$coupon_info['coupon_amount']);

                        if($coupon_discount_type==1)
                        {
                         /* $coupon_product_price = $coupon_product_price+$price;*/
                          
                          if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                          {
                            $data['coupon_discount'] = round($val['OrderProductTotal']-$coupDiscountPrdPrice);
                            $data['coupon_code'] = $val['coupon_code'];
                          }
                        }
                      //coupon_discount_type =2 (cart % discount)
                        if($coupon_discount_type==2)
                        {
                          //$coupon_product_price = $coupon_product_price+$price;   
                          //echo $val['product_sum'].' , '.$val['OrderProductTotal'].' , '.$coupon_discount_type.' , '.$coupon_info['coupon_amount'].'<br/>';
                           /* $coupDiscountPrdPrice1 = $this->Manage_orders_model->calculate_product_discountAmt($val['OrderTotal'],$val['OrderProductTotal'],$coupon_discount_type,$coupon_info['coupon_amount']);*/
                            $coupDiscountPrdPrice1 = $this->Manage_orders_model->calculate_product_discountAmt($val['OrderTotal'],$val['OrderProductTotal'],$coupon_discount_type,$coupon_info['coupon_amount']);
                          
                          $coupon_product_price = $val['OrderProductTotal'];
                          if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                          {
                            $coupon_percent = $coupon_info['coupon_amount'];
                            $data['coupon_discount'] = $coupDiscountPrdPrice1;
                            $data['coupon_code'] = $val['coupon_code'];
                          }
                        }


                }

              }else
              {
                $data['coupon_discount']=0;
                $data['coupon_code'] = '';
              }

/* Coupon Setting Page End */
          ?>
          <td><?php 
            if($this->session->userdata('role_id') == 6 ){
                if($referral_point>0)
                {
                  $orderPercent = ($val['product_sum']*100)/$order_product_sum;  
                  $refDiscount = round(($orderPercent*500)/100,2);
                }
              echo number_format(round($val['product_sum'],2)-(round($data['coupon_discount'],2)+$refDiscount), 2, '.', ',');
          }else
          {
             echo number_format(round($val['product_sum'],2)-(round($data['coupon_discount'],2)+$referral_point), 2, '.', ',');
          }
          ?></td>
          <?php
          if($this->session->userdata('role_id') == 6 ){
                if($referral_point>0)
                {
                  $orderPercent = ($val['product_sum']*100)/$order_product_sum;  
                  $refDiscount = round(($orderPercent*500)/100,2);
                }
           $order_total_mf = number_format((round($val['product_sum'],2)-(round($data['coupon_discount'],2)+$refDiscount)), 2, '.', ',');
          }else
          {
            $order_total_mf = number_format((round($val['product_sum'],2)-(round($data['coupon_discount'],2)+$referral_point)), 2, '.', ',');
          } 

          ?>
          <td style="width:80px;" ><i class="fa fa-inr"></i> <?php echo $order_total_mf; ?></td>
          <td><?php
            if($val['pay_mode'] == 1)
            {
              echo 'COD';
            }else
            {
              echo 'Online';
            }
            ?></td>
           <!--<td >
            <?php //if($val['confirm_mobile_no'] == 1 || $val['pay_mode']!=1) { ?>
            <?php //echo $this->Manage_orders_model->getOrderStatus($val['order_unique_no']); ?>
            <?php// }else{ ?>
             <select class="form-control input-sm"  name="otp_confirmation_<?php //echo $val['order_unique_no']; ?>" id="otp_confirmation_<?php //echo $val['order_unique_no']; ?>" onchange="change_order_status('<?php //echo $val['order_unique_no']; ?>','<?php //echo $val['first_name']; ?>','<?php //echo $val['last_name']; ?>','<?php //echo $val['user_id']; ?>');">
             <option value="0">OTP Pending</option>
             <option value="1">Mark as Processing</option>
             <option value="2">Mark as Fake</option>
            </select>
            <?php //} ?>
          </td>-->
          <td><?php echo $val['first_name'].' '.$val['last_name']; ?></td>
          <!--<td><?php //echo $val['mobile_no']; ?></td>
          <td><?php// echo $val['platform_info'];
					// if($val['sc_source'] != ''){
					// 	echo '<span style="color:blue;"><i><br/>'.$val['sc_source'].'<br/>'.$val['sc_medium'].'<br/>'.$val['sc_campaign'].'</i></span>';
					// }
			   ?>
		  </td>-->
           <?php
          /*if(($val['order_status'] == 5 && $val['send_email'] == 1) || ($val['order_status'] == 2 && $val['send_email'] == 2) || ($val['order_status'] == 3 && $val['send_email'] == 3) || ($val['order_status'] == 4 && $val['send_email'] == 4)){ $show_mail = 1; }
          else if($val['order_status'] != 5 && $val['send_email'] == 0){$show_mail = 0; }
          else { $show_mail = 0; }*/
          ?>
          <?php /*if($val['order_status'] < 7 && $val['order_status'] != 1) { ?>
          <td><a class="btn btn-primary btn-xs" <?php echo $show_mail==1 ? 'disabled' : ''; ?> data-toggle="modal" onclick="send_email('<?php echo $val['order_unique_no']; ?>');">Send E-mail</a></td>
          <?php }else{ echo '<td></td>'; } */?>
          <!--<td><?php echo $val['modified_datetime']; ?></td>-->
		  
		  <!--<td><select class="form-control input-sm"  name="final_order_status<?php //echo $val['order_unique_no']; ?>" id="final_order_status<?php //echo $val['order_unique_no']; ?>" onchange="change_final_order_status('<?php //echo $val['order_unique_no']; ?>','<?php //echo $val['user_id']; ?>');">
				<option value="0">Not Completed</option>
				<option <?php //if($val['is_completed'] == 1){ echo "selected"; } ?> value="1">Completed</option>
            </select></td>-->
			
		  <!--<td>
            <div id="product_info"> </div>
              <a class="btn btn-primary btn-xs btn-quick-view" id="<?php //echo $val['order_unique_no']; ?>">
              Details <i class="fa fa-angle-down fa-angle-up"></i></a>
          </td>-->
		  <!--<td>
            <div id="box_order_info"> 
      				<a href="javascript:void(0)" data-orderid="<?php //echo $val['order_unique_no']; ?>" onclick="get_order_popup(this)"><?php //if($val['is_paymentdone'] == '1' || $val['pay_mode'] == '2'){ echo 'Paid'; }else{ echo 'Pending'; }?></a>
      			</div>             
      </td>-->
      </tr>
      <tr class="tr-quick-view" >
        <td colspan="18" id="orderDetail_<?php echo $val['order_unique_no']; ?>" >
        </td>
      </tr>
      <?php
         }
      }else
      {
      ?>
         <tr>
          <td colspan="18">Sorry no record found</td>
        </tr>
    <?php
      }
      ?>
    </tbody>

  </table>
</div>

<?php echo $this->pagination->create_links(); ?>
</div>
</div>

</section>
<style>
.table{width: 100%; font-size: 12px; border-collapse: collapse;}
.table > tbody .tr-collapse td{padding: 0;}
.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td{border: 1px solid #d2d6de; }

.table > caption + thead > tr:first-child > td, .table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table > thead:first-child > tr:first-child > th{border: 1px solid #d2d6de; }

.table.table-orders th{vertical-align: top; }

.tr-collapse .row{margin: 0; padding: 10px; }
.tr-collapse label{font-weight: normal;}
.modal-header{background: #000; color:#fff; height: auto; padding: 8px 15px;}
.modal-header h4{font-size: 16px;margin: 0; float: left;}
.order-view-popup dd{margin-bottom: 10px;}
.order-view-product-list .img-wrp{
    background: #fff none repeat scroll 0 0;
    border: 1px solid #ececec;
    height: 150px;
    padding: 10px;
    text-align: center;
    margin-bottom: 5px;
}
.order-view-popup .close{color:#fff; opacity:1;}
.order-view-product-list .img-wrp img{
    height: auto;
    max-height: 100%;
    max-width: 100%;
    width: auto;
}
.order-view-popup .name{line-height: 1.3; margin-bottom: 5px;}
select.form-control{min-width: 94px; padding: 5px;}

.table-tr-collapse .box-quick-view .img-wrp{width:50px; height:50px; border:1px solid #eee;}
.table-tr-collapse .box-quick-view .img-wrp img{width:auto; height:auto; max-width:50px; max-height:50px;}
.table-tr-collapse .box-quick-view .order-address-box{bottom: -8px; position: absolute; right: 17px; text-align: right; }
.table-tr-collapse .seller-amount-box{text-align: right; padding-right: 20px; background: #ECF0F5;}
.table-tr-collapse .seller-amount-box .price{width:70px; display: inline-block;}

.table-tr-collapse tr.tr-quick-view{display: none;}
.table-tr-collapse tr.tr-quick-view .box-quick-view{display: block;}
.table-bordered { border: 1px solid #ddd; }
</style>

<script type="text/Javascript">
$(function () {
      $("#date_from").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true});
      $("#date_to").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true });
	  // $('#date').datepicker({format: "yyyy-mm-dd"});
      $(".nav-tabs a").click(function(){
        $(this).tab('show');
      });
      $('.nav-tabs a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
      });
    });

$( document ).ready(function() {
$("body").addClass('sidebar-collapse');

$('.btn-quick-view').click( function(event){

  var orderUniqueId = $(this).attr('id');
     event.preventDefault();


  if(orderUniqueId!='')
  {
    dispalyOrderDetail(orderUniqueId);
  }

//  $(this).parents('tr').next('.tr-quick-view').find('.box-quick-view').slideToggle(500);
  //$(this).parents('tr').toggleClass("open");
  var elem = $(this).parents('tr').next('tr.tr-quick-view');
  elem.slideToggle('10');
  $(this).parents('tr').toggleClass("open");

});


 $('.nav-item-order').addClass("active");
  $('.nav-manage-orders').addClass("active");
});

function downloadExcel()
{
     var search_by = '';
     var table_search = '';
     $("#is_download").val('1');
     search_by = $('#search_by').val();
     search_by = search_by.trim();
     table_search = $('#table_search').val();
     $('#table_search').removeAttr('required');
     table_search = table_search.trim();
     var is_download = $("#is_download").val();

     //encode_table_search = Base64.encode(table_search);
     //encode_table_search = <?php echo utf8_encode(table_search); ?>;

     if(search_by == 6)
     {
        encode_table_search = window.btoa(table_search);
     }else if(search_by == 8)
     {
        var dateFrom = $("#date_from").val();
        var dateTo = $("#date_to").val();

        var date_search = dateFrom+','+dateTo;
        encode_table_search = window.btoa(date_search);
     }
     else
     {
        encode_table_search = table_search;
     }
     if(!encode_table_search){ encode_table_search = '0'; }
	   search_by2 = $('#search_by2').val();
	   search_by2 = search_by2.trim();
     //alert(encode_table_search);
     //var _href = '<?php echo base_url(); ?>'+'View_orders_payment/exportExcel/downloadExcel';

     var newhref = '<?php echo base_url(); ?>'+'AffiliateUsers/exportExcel/downloadExcel'+'/'+search_by+'/'+encode_table_search+'/'+is_download+'/'+search_by2;
     //alert(newhref);

     $('#frmsearch').attr('action',newhref);

     //location.reload();
     /*var link = document.createElement('a');
     link.href = newhref;
     document.body.appendChild(link);
     link.click();*/
}


function submitForm(){
   $("#is_download").val('0');
   var newhref = '<?php echo base_url(); ?>'+'AffiliateUsers/';
   $('#frmsearch').attr('action',newhref);
   $('#frmsearch').submit();
}

var filterType = $('#search_by').val();
if(filterType == 8)
{
    $("#filter").hide();
    $("#daterange").show();
    $("#table_search").removeAttr('required');
    $("#date_from").attr('required','');
}

 $("#search_by").change(function(){

    var filterType = $(this).val();
    if(filterType == 8)
    {
        $("#filter").hide();
        $("#daterange").show();
        $("#table_search").removeAttr('required');
        $("#date_from").attr('required','');

    }else
    {
       $("#filter").show();
      $("#daterange").hide();


    }
      if(filterType == 6){
        $(".msg-days-lapsed").show();
    }else
    {
        $(".msg-days-lapsed").hide();
    }

  });



function order_close(orderUniqId)
{
  $('.table-tr-collapse tr.tr-quick-view').slideUp(10);
  $('.table-tr-collapse tr').removeClass("open");
}

</script>
