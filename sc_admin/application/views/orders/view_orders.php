<?php
//echo "view_order"."<br/><pre>";
//echo "<pre>";print_r($orders);
//print_r($orderStatusList);

//echo '<pre>';print_r($couponAmt);
$image_array = array();
$imageString  ='';
$productString = '';
foreach($orders as $val)
{ $i++; $new_order_id = $val['order_id'];
   //echo $new_order_id."<br/>";
   if($new_order_id != $old_order_id)
    {
      $imageString = $val['image'];
      $productString = $val['product_name'];
    }else
    {
      $imageString = $imageString.','.$val['image'];
      $productString =  $productString.','.$val['product_name'];
    }
    $image_array[$val['order_id']] =  $imageString;
    $product_array[$val['order_id']] = $productString;
    $old_order_id = $val['order_id'];
}

$users_menu=$this->data['users_menu'];
$table_search = $table_search == '0' ? '' : $table_search;

?>
<section class="content-header">
  <h1>Manage Orders  <small class="label label-info"><?php echo @$total_rows; ?></small></h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">Manage Orders</li>
  </ol>
</section>
<section class="content">
  <div class="box">
    <!--  <div class="box-header with-border">
    <h3 class="box-title">List of Admin Users <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3>
    <div class="pull-right">
    <?php if(in_array("12",$users_menu)) { ?>
    <a href="<?php echo base_url(); ?>users/user_add"><button class="btn btn-sm btn-primary" type="button"><i class="fa fa-plus"></i> Add User</button></a>
    <?php } ?>
  </div>
</div>-->
<div class="box-body">
  <?php echo $this->session->flashdata('feedback'); ?>
  <form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>View_orders" method="post">
  <input type="hidden"  name="is_download" id="is_download" value="0" />
    <div class="row">
      <div class="col-md-2">
        <?php $search_by = set_value('search_by')=='' ? $search_by : set_value('search_by'); ?>
        <select name="search_by" id="search_by" class="form-control input-sm" >
          <option value="0">Search by</option>
          <option value="1" <?php echo $search_by==1 ? 'selected' : ''; ?> >Order ID</option>
          <option value="2" <?php echo $search_by==2 ? 'selected' : ''; ?> >Username</option>
          <option value="3" <?php echo $search_by==3 ? 'selected' : ''; ?> >Company Name</option>
          <option value="4" <?php echo $search_by==4 ? 'selected' : ''; ?> >Date</option>
          <option value="5" <?php echo $search_by==5 ? 'selected' : ''; ?> >Status</option>
        </select>
      </div>
      <div class="col-md-3">
        <div class="input-group">
          <input type="text" name="table_search" value="<?php echo set_value('table_search')=='' && set_value('table_search')==0 ? $table_search : set_value('table_search'); ?>" id="table_search" class="form-control input-sm pull-right" placeholder="Search"  required=""/>
          <div class="input-group-btn">
            <button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </div>
      <div class="col-md-2">
        <a href="<?php echo base_url(); ?>View_orders" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
      </div>
      <div class="col-md-5">
       <?php if($this->session->userdata('role_id') != 6){ ?>
                <button title="Download Excel" id="download_excel" class="btn btn-primary btn-sm"  onclick="downloadExcel();"><i class="fa fa-download"></i> Download Data</button>
              <?php } ?>
        <!--div class="records-per-page pull-right">
        <select name="records-per-page" class="form-control input-sm">
        <option>Per Page</option>
        <option value="10" selected="selected">10</option>
        <option value="25">25</option>
        <option value="50">50</option>
      </select>
    </div-->
    <div class="pull-right">
      <?php echo $this->pagination->create_links(); ?>
    </div>
  </div>
</div>
<div><br/><b>Note:</b> Grand Total is inclusive of Service-Tax,COD, Shipping Charges, Coupon Code Deduction (if applicable) </div>
</form>
<div class="table-responsive">
  <table class="table table-bordered table-striped dataTable table-orders " style="width:100%">
    <thead>
      <tr>
        <th>Sr. No.</th>
        <th>Date</th>
        <th>Order ID</th>
        <th>Product SKU</th>
        <th>Product Name(Look Id)</th>
        <th>Qty</th>
        <th>Size</th>
        <th>Company Name</th>
        <th>Product Price</th>
        <th>Grand Total</th>
        <th>Discount Percent</th>
        <th>Coupon Code </th>
        <th>Payment Type</th>
        <th>Status</th>
        <th>Send Cancel E-mail</th>
        <th>Last updated</th>
        <th>Platform</th>
        <th>User Info</th>
      </tr>
    </thead>
    <tbody>
    <?php
      $i = 0;
      $new_order_id = ''; $old_order_id = '';
      $coupon_product_price = 0;
    if(!empty($orders))
    {
      foreach($orders as $val)
      { $i++; $new_order_id = $val['order_id'];
          $imageString ='';$data['coupon_discount'] = 0;

    if($new_order_id != $old_order_id){
    ?>
        <tr>
        <?php if($sr_offset > 0){  ?>
          <td><?php echo $i+($sr_offset*10); ?></td>
        <?php }else{ ?>
          <td><?php echo $i; ?></td>
        <?php } ?>
          <td><?php echo $val['created_datetime']; ?></td>
          <td><?php echo $val['order_id']; ?></td>
          <td><?php echo $val['product_sku']; ?></td>
          <td><?php echo $val['product_name']; echo '<a href="'.base_url().'reports/look_view/'.$val['look_id'].'" target="_blank">('.
             $val['look_id'].")</a>"; ?></td>
          <!--td><?php echo $val['product_name']; ?></td-->
          <td><?php echo $val['product_qty']; ?></td>
          <td><?php echo $val['size_text']; ?></td>
          <td><?php echo $val['company_name']; ?></td>
          <td style="width:80px;"><i class="fa fa-inr"></i> <?php echo number_format($val['product_price'], 2, '.', ','); ?></td>
          <?php 
/* Coupon Setting Page */   
                   
              if(isset($val['coupon_code']) && $val['coupon_code']!='')
              {
                $coupon_info = $this->View_orders_model->getUserCoupon($val['coupon_code'],$val['created_datetime'],$val['brand_id']);
                /*$data['coupon_discount'] = $coupon_info['coupon_amount'];
                $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];*/
                $coupon_brand = $coupon_info['brand_id'];
                $coupon_products = $coupon_info['coupon_products'];
                if($coupon_products!='' || $coupon_products!=0)
                {
                  $coupon_products_arr =explode(',',$coupon_products);
                }
                $coupon_min_spend = $coupon_info['coupon_min_spend'];
                $coupon_max_spend = $coupon_info['coupon_max_spend'];
                $coupon_discount_type = $coupon_info['coupon_discount_type']; 
                $coupon_stylecracker =  $coupon_info['stylecrackerDiscount'];
                $individual_use_only =  $coupon_info['individual_use_only'];   
                $data['coupon_code'] = $val['coupon_code'];
                $coupon_product_price = 0;
                $price = $val['product_price'];

                 if($coupon_brand==$val['brand_id'] && $coupon_brand!=0 )
                  {        
                    if($coupon_products!=''&& $coupon_products!=0)
                    {
                       //coupon_discount_type =3 (Product discount)
                      if($coupon_discount_type==3)
                      {
                        if(in_array($val['product_id'], $coupon_products_arr))
                        {              
                          if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                          {                        
                            $data['coupon_discount'] = $coupon_info['coupon_amount'];  
                            $data['coupon_code'] = $val['coupon_code'];                   
                          } 
                        }
                      } 
                       //coupon_discount_type =4 (Product % discount)
                      if($coupon_discount_type==4)
                      {
                        if(in_array($val['product_id'], $coupon_products_arr))
                        {              
                          if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                          {
                            $coupon_percent = $coupon_info['coupon_amount'];
                            $data['coupon_discount'] = $price*($coupon_percent/100); 
                            $data['coupon_code'] = $val['coupon_code'];                      
                          } 
                        }
                      }                 

                    }else
                    { 
                       //coupon_discount_type =1 (cart discount)
                      if($coupon_discount_type==1)
                      {
                        $coupon_product_price = $coupon_product_price+$price;
                        if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                        {
                          $data['coupon_discount'] = $coupon_info['coupon_amount']; 
                          $data['coupon_code'] = $val['coupon_code'];                   
                        }
                      }
                       //coupon_discount_type =2 (cart % discount)
                      if($coupon_discount_type==2)
                      {
                        $coupon_product_price = $coupon_product_price+$price;
                        if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                        {
                          $coupon_percent = $coupon_info['coupon_amount'];  
                          $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                          $data['coupon_code'] = $val['coupon_code'];                      
                        }
                      }
                    }          
                  }

                  if(($coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only!=1  )|| ( $coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only==1 ))
                { 
                    //coupon_discount_type =1 (cart discount)
                        $coupDiscountPrdPrice = $this->View_orders_model->calculate_product_discountAmt($val['OrderIdProductSum'],$val['OrderProductTotal'],$coupon_discount_type,$coupon_info['coupon_amount']);
                    
                        if($coupon_discount_type==1)
                        {
                         /* $coupon_product_price = $coupon_product_price+$price;*/
                          $coupon_product_price = $val['OrderProductTotal'];  
                          if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                          {
                           $data['coupon_discount'] = round($coupDiscountPrdPrice) ;
                            $data['coupon_code'] = $val['coupon_code'];                   
                          }
                        }
                         //coupon_discount_type =2 (cart % discount)
                        if($coupon_discount_type==2)
                        {
                         /* $coupon_product_price = $coupon_product_price+$price;*/
                          $coupon_product_price = $val['OrderProductTotal'];  
                          if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                          {
                            $coupon_percent = $coupon_info['coupon_amount'];  
                            $data['coupon_discount'] = round($coupDiscountPrdPrice);
                            $data['coupon_code'] = $val['coupon_code'];                      
                          }
                        }
                }                       

              }else
              {
                $data['coupon_discount']=0;
                $data['coupon_code'] = '';
              }         
           
/* Coupon Setting Page End */
          ?>
          <?php $order_total_mf = number_format(($val['order_total']-$data['coupon_discount']), 2, '.', ','); ?>
          <td style="width:80px;" ><i class="fa fa-inr"></i> <?php echo $order_total_mf; ?></td>
          <td><?php echo $val['discount_percent']; ?></td>
          <td><?php echo $val['coupon_code']; ?></td>          
          <td><?php
          if($val['pay_mode'] == 1)
          {
            echo 'COD';
          }else
          {
            echo 'Online';
          }
          ?></td>
          <td style="width:120px;" >
            <select class="form-control input-sm orderStatus" <?php echo $val['order_status']==5 ? 'disabled' : ''; ?> name="orderStatus" order="<?php echo $val['order_prd_id'];?>" >
            <?php foreach($orderStatusList as $key=>$value)
                {
                  if($val['order_status'] == $key)
                  {
            ?>
                    <option  value='<?php echo $key;?>' selected ><?php echo $value;?></option>
            <?php }else{?>
                    <option  value='<?php echo $key;?>' ><?php echo $value;?></option>
            <?php      }
                }?>
            </select>
          </td>
		  <?php
			if($val['order_status'] == 5 && $val['send_email'] == 1){ $show_mail = 1; }
			else if($val['order_status'] != 5 && $val['send_email'] == 0){$show_mail = 0; }
			else { $show_mail = 0; }
			?>
			<?php if($val['order_status'] ==5 ) { ?>
		  <td><a class="btn btn-primary btn-xs" <?php echo $show_mail==1 ? 'disabled' : ''; ?> data-toggle="modal" onclick="send_email(<?php echo $val['order_id']; ?>,<?php echo $val['product_id']; ?>);">Send E-mail</a></td>
			<?php }else{ echo '<td></td>'; } ?>
          <td><?php echo $val['modified_datetime']; ?></td>
          <td><?php echo $val['platform_info']; ?></td>
          <td><a class="btn btn-primary btn-xs" data-toggle="modal" data-target="#<?php echo $val['order_prd_id'];?>">View</a></td>
        </tr>

            <div id="<?php echo $val['order_prd_id'];?>" class="modal" role="dialog">
             <div class="modal-dialog order-view-popup">
               <div class="modal-content">
            <div class="modal-header">
            <h4>Order Details: <?php echo $val['order_id']; ?></h4>
            <button type="button" class="close pull-right" data-dismiss="modal">×</button>
            <div class="clearfix"></div>
            </div>
               <div class="modal-body">

              <div class="row">
                <div class="col-md-5">
                  <dl>
                  <dt>Name</dt>
                  <dd><?php echo $val['first_name']." ".$val['last_name']; ?>
                    </dd>
                    <dt>
                    Phone  </dt>
                        <dd><?php echo $val['mobile_no']; ?>
                    </dd>
                    <dt>
                      Email  </dt>
                        <dd><?php echo $val['email_id']; ?>
                    </dd>

                    <dt>
                    Shipping Address  </dt>
                    <dd> <?php echo $val['shipping_address']; ?>
                    <br/>
                      <?php echo $val['city_name']." ".$val['pincode']." ".$val['state_name']; ?>
                    </dt>
                  </dl>
                  </div>
                  <div class="col-md-7">
                    <div class="order-view-product-list">
                     <div class="row">
                    <?php
                    $image = explode(',',$image_array[$val['order_id']]);
                    $productName = explode(',',$product_array[$val['order_id']]);
                    $k = 0;
                    $imgcount = count($image);
                          foreach($image as $img)
                          {
                    ?>
                    <div class="<?php  echo $imgcount >1 ? 'col-md-6' : 'col-md-12'; ?>">
                    <div class="img-wrp">
                        <img src="<?php echo $this->config->item('product_160_url'); ?><?php echo $img; ?>">
                         </div>
                        <div class="name">
                         <strong><?php echo $productName[$k]; ?></strong>
                        </div>
                    </div>
                      <?php
                          $k++;
                          }
                      ?>
                      <!--<li>
                        <img src="http://www.stylecracker.com/blog/wp-content/uploads/2015/07/kaia-gerber2.jpg">
                        <div class="name">
                          Black V Neck Florals Chiffon Dress
                        </div>
                          <div class="price text-green">
                            Rs. 5800
                          </div>
                      </li>-->

                    </div>

                  </div>
              </div>

              </div>
              </div>
              </div>


      <?php
        } else{ ?>
        <tr>
          <td></td><td></td><td></td>
          <td><?php echo $val['product_sku']; ?></td>
          <td><?php echo $val['product_name']; echo '<a href="'.base_url().'reports/look_view/'.$val['look_id'].'" target="_blank">('.
             $val['look_id'].")</a>"; ?></td><td><?php echo $val['product_qty']; ?></td><td><?php echo $val['size_text']; ?></td><td></td>
          <td><i class="fa fa-inr"></i><?php echo number_format($val['product_price'], 2, '.', ','); ?></td><td></td><td></td><td></td>
          <td></td><td><select class="form-control input-sm orderStatus" <?php echo $val['order_status']==5 ? 'disabled' : ''; ?> name="orderStatus" order="<?php echo $val['order_prd_id'];?>" >
            <?php foreach($orderStatusList as $key=>$value)
                {
                  if($val['order_status'] == $key)
                  {
            ?>
                    <option  value='<?php echo $key;?>' selected ><?php echo $value;?></option>
            <?php }else{?>
                    <option  value='<?php echo $key;?>' ><?php echo $value;?></option>
            <?php      }
                }?>
            </select></td><td></td><td><?php echo $val['modified_datetime']; ?></td>
			<?php
			if($val['order_status'] == 5 && $val['send_email'] == 1){ $show_mail = 1; }
			else if($val['order_status'] != 5 && $val['send_email'] == 0){$show_mail = 0; }
			else { $show_mail = 0; }
			?>
			<?php if($val['order_status'] ==5 ) { ?>
		  <td><a class="btn btn-primary btn-xs" <?php echo $show_mail==1 ? 'disabled' : ''; ?> data-toggle="modal" onclick="send_email(<?php echo $val['order_id']; ?>,<?php echo $val['product_id']; ?>);">Send E-mail</a></td>
			<?php }else{ echo '<td></td>'; } ?>
      <td><?php echo $val['modified_datetime']; ?></td>
        </tr>
      <?php }
        $old_order_id = $val['order_id']; }
      }else
      {
      ?>
         <tr>
          <td colspan="10">Sorry no record found</td>
        </tr>
    <?php
      }
      ?>
    </tbody>

  </table>
</div>

<?php echo $this->pagination->create_links(); ?>
</div>
</div>

</section>
<style>
.table{max-width: 110%; width:110%; font-size: 12px; border-collapse: collapse;}
.table > tbody .tr-collapse td{padding: 0;}
.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td{border: 1px solid #d2d6de; }

.table > caption + thead > tr:first-child > td, .table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table > thead:first-child > tr:first-child > th{border: 1px solid #d2d6de; }

.table.table-orders th{vertical-align: top; }

.tr-collapse .row{margin: 0; padding: 10px; }
.tr-collapse label{font-weight: normal;}
.modal-header{background: #000; color:#fff; height: auto; padding: 8px 15px;}
.modal-header h4{font-size: 16px;margin: 0; float: left;}
.order-view-popup dd{margin-bottom: 10px;}
.order-view-product-list .img-wrp{ /*width: 48%; display: inline-block; padding:0 5px; margin-bottom: 10px; */

    background: #fff none repeat scroll 0 0;
    border: 1px solid #ececec;
    height: 150px;
    padding: 10px;
    text-align: center;
    margin-bottom: 5px;
}
.order-view-popup .close{color:#fff; opacity:1;}
.order-view-product-list .img-wrp img{
    height: auto;
    max-height: 100%;
    max-width: 100%;
    width: auto;
}
.order-view-popup .name{line-height: 1.3; margin-bottom: 5px;}
select.form-control{min-width: 94px; padding: 5px;}
</style>

<script type="text/Javascript">
$( document ).ready(function() {
$("body").addClass('sidebar-collapse');
 /*$('[data-toggle=confirmation]').confirmation({
    title:'Are you sure?',
    onConfirm : function(){

      var edit_id = $(this).closest('td').find('.btn-edit').attr('data-id');
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "View_orders/",
        data: { id: edit_id },
        cache:false,
        success:
        function(data){
          location.reload(); //as a debugging message.
        }

      });
    }
  });
*/

  $( ".orderStatus" ).change(function() {

    var orderStatus = $(this).val();
    var order_prd_id = $(this).attr('order');

    if(order_prd_id!='')
    {
       var result = window.confirm('Are you sure?');
       if(result == true)
       {
          $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "View_orders/updateOrder",
            data: { id: order_prd_id, orderStatus: orderStatus },
            cache:false,
            success:
            function(data){
              location.reload(); //as a debugging message.
            }
          });

       }else
       {

       }
    }

  });
 $('.nav-item-order').addClass("active");
  $('.nav-manage-orders').addClass("active");

});
function downloadExcel()
      {
           var search_by = '';
           var table_search = '';
           $("#is_download").val('1');
           search_by = $('#search_by').val();
           search_by = search_by.trim();
           table_search = $('#table_search').val();
           $('#table_search').removeAttr('required');
           table_search = table_search.trim();
           var is_download = $("#is_download").val();

           //encode_table_search = Base64.encode(table_search);
           //encode_table_search = <?php echo utf8_encode(table_search); ?>;

           if(search_by == 6)
           {
              encode_table_search = window.btoa(table_search);
           }else if(search_by == 8)
           {
              var dateFrom = $("#date_from").val();
              var dateTo = $("#date_to").val();

              var date_search = dateFrom+','+dateTo;
              encode_table_search = window.btoa(date_search);
           }
           else
           {
              encode_table_search = table_search;
           }
           if(!encode_table_search){ encode_table_search = '0'; }

           //alert(encode_table_search);
           //var _href = '<?php echo base_url(); ?>'+'View_orders_payment/exportExcel/downloadExcel';

           var newhref = '<?php echo base_url(); ?>'+'View_orders/exportExcel/downloadExcel'+'/'+search_by+'/'+encode_table_search+'/'+is_download;
           //alert(newhref);

           $('#frmsearch').attr('action',newhref);

           //location.reload();
           /*var link = document.createElement('a');
           link.href = newhref;
           document.body.appendChild(link);
           link.click();*/
      }
function send_email(order_id,product_id){
	var result = window.confirm('Are you sure?');
	 if(result == true)
       {
		if(order_id != null){
					$.ajax({
						url: "<?php echo base_url() ?>View_orders/send_mail",
						type: 'POST',
						data: {'order_id':order_id,'product_id':product_id},
						cache: false,
						async: false,
						success: function(response) {
							alert("Email sent to user");
							if(response == 1)
							{
								location.reload();
							}else{
								location.reload();
							}

						}
					});
				  }
	   }else
       {

       }

}
</script>
