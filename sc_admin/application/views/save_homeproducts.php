
<script src="<?php echo base_url(); ?>assets/plugins/sortable/jquery.sortable.min.js"></script>

<section class="content-header">
        <h1>Manage Home page products</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>">Home</a></li>
            <li class="active">Manage Product Size Order</li>
          </ol>
</section>
<form role="form" name="frmtag" id="frmtag" action="<?php echo base_url().'Save_products';?>" method="post">
	<div class="box-body">
		<?php echo $this->session->flashdata('feedback'); ?>
		<div class="row">                             
			   <div class="col-md-6">
				<div class="form-group">
				  <!-- <label for="ProductDesc">Product Description</label> -->
				   <label for="product_delid">Product Ids <span class="text-red">*</span></label>
				   <!-- required="" -->
				  <textarea tabindex="3" name="product_id" id="product_id" placeholder="Product Id" rows="3" class="form-control" required numeric >
				  <?php echo $product_ids ?></textarea>
				  <span class="text-red"><?php echo form_error('product_id'); ?></span>
				</div>	        
			</div>               
		</div>
		<div class="row">   
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Active home page Product ids = <?php echo $product_ids ?>
		</div>	
	</div>
	
	<div class="box-body">
		<?php echo $this->session->flashdata('feedback'); ?>
		<div class="row">                             
			   <div class="col-md-6">
				<div class="form-group">
				  <!-- <label for="ProductDesc">Product Description</label> -->
				   <label for="product_delid">Collection Ids <span class="text-red">*</span></label>
				   <!-- required="" -->
				  <textarea tabindex="3" name="collection_id" id="collection_id" placeholder="Collection Id" rows="3" class="form-control" required numeric ><?php echo $collection_ids ?></textarea>
				  <span class="text-red"><?php echo form_error('collection_id'); ?></span>
				</div>	        
			</div>               
		</div>
		<div class="row">   
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Active home page Collection ids = <?php echo $collection_ids ?>
		</div>	
		<div class="box-footer text-left">
			<button class="btn btn-sm btn-primary" tabindex="30" type="submit" ><i class="fa fa-save"></i> Save </button>             
		</div>
	</div>
</form>	
<style type="text/css">
	ul li{list-style: none;}
	.list-group-item{ background-color: #c6c6c6;cursor: pointer; }
</style>
<script type="text/javascript">
	 $( document ).ready(function() {
    $('.nav-item-banners').addClass("active");
    // $('.nav-banners-list').addClass("active");
	$('.nav-item-list7').addClass("active");
    //$('.datepicker').datepicker();
    });
</script>