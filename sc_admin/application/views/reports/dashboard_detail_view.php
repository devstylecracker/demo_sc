  <?php
  $users_menu=$this->data['users_menu'];
  $pagetitle = $type;
  ?>
  <section class="content-header">
          <h1><?php echo $pagetitle; ?></h1>
            <ol class="breadcrumb">
              <li><a href="<?php echo base_url(); ?>home">Home</a></li>
              <li class="active"><?php echo $pagetitle; ?></li>
            </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">List of <?php echo $pagetitle; ?> <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3>
      <div class="pull-right">
      </div>
      </div><!-- /.box-header -->
    <div class="box-body">
      <?php echo $this->session->flashdata('feedback'); ?>

      <form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>websiteusers" method="post">
        <div class="row">  
          <div class="col-md-5">            
            <div class="pull-right">
              <?php //echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>
      </form>

    <div class="table-responsive">
      <table class="table table-bordered table-striped dataTable" id="example2">
        <thead>         
          <tr>
          <?php if(!empty($users_data['fields'])) 
            { $i = 1;
            foreach($users_data['fields'] as $val)
              {
              ?>
            <th><?php echo $val; ?></th>          
          <?php
              }
            }
          ?>
          </tr>
        </thead>
        <tbody>
          <?php if(!empty($users_data)) { $i = 1;
            foreach($users_data['data'] as $val){
              ?>
              <tr>
                <td><a href="<?php echo base_url(); ?>websiteusers/user_view/<?php echo $val['id']; ?>" target="_blank"><?php echo $val['id']; ?></a></td>
                <td><?php echo $val['user_name']; ?></td>
                <td><?php echo $val['email']; ?></td>
                <td><?php echo $val['first_name']; ?></td>
                <td><?php echo $val['last_name']; ?></td>
                <td><?php //echo $val->created_datetime;
                          $date = new DateTime($val['last_login']);
                          echo $date->format('d-m-Y H:i:s');
                 ?></td>
                <!--<td>
                  <?php if(in_array("15",$users_menu)) { ?>
                    <a href="<?php echo base_url(); ?>websiteusers/user_view/<?php echo $val->id; ?>">
                      <button type="button" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> View</button></a>
                      <?php } ?>
                    </td>-->
                  </tr>
                  <?php $i++; } }else{ ?>
                    <tr>
                      <td colspan="5">Sorry no record found</td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <?php //echo $this->pagination->create_links(); ?>
            </div>
          </div>

        </section>
        <!-- /.content 

        <script type="text/Javascript">
        $( document ).ready(function() {
          $('[data-toggle=confirmation]').confirmation({
            title:'Are you sure?',
            onConfirm : function(){
              var del_id = $(this).closest('td').find('.btn-danger').attr('data-id');
              $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "users/user_delete",
                data: { id: del_id },
                cache:false,
                success:
                function(data){
                  location.reload(); //as a debugging message.
                }

              });
            }
          });
          $('.nav-item-user').addClass("active");
          $('.nav-website-user').addClass("active");
        });
        </script>-->
