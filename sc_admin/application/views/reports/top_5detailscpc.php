<?php $users_menu=$this->data['users_menu']; 
//echo "<pre>";
//print_r($top_5_product_cpc);
$page = $this->uri->segment(3);
$var_array = explode('_', $page);
?>
 <section class="content-header">
          <h1>View <?php echo $page; ?>  Product Click Details</h1>
            <ol class="breadcrumb">
              <li><a href="<?php echo base_url(); ?>home">Home</a></li>
              <li class="active"><?php echo $page; ?>  Details</li>
            </ol>
  </section> 
<!--
<section class="content">
    <form name="myform" id="myform" method="post">
      Date From : <input type="text" name="date_from" id="date_from" value="<?php echo set_value('date_from'); ?>">
      Date To : <input type="text" name="date_to" id="date_to" value="<?php echo set_value('date_to'); ?>">
      <input type="submit" name="submit" id="submit" value="Go" >
    </form>
  -->
	<section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">List of Product Click Details <small class="label pull-right label-info"><?php echo $total_count; ?></small></h3>
      </div>
    <div class="box-body">  
      <form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>view_all_users" method="post">
        <div class="row">        
          <div class="col-md-12">            
            <div class="pull-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>
      </form>
	   <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th>Product ID</th>
              <th>Name</th>
              <th>Brand Name</th>
              <th>Price</th>
             <!--<th>Click Count</th>-->
             <?php 
                if($page == 'Authenticated_CPC' || $page == 'Authenticated_CPA')
                {
             ?>
              <th>User Email</th>
             <?php 
                }
             ?>
              <th><?php echo $var_array[1];  ?></th>
              <th>Created Datetime</th>
            </tr>
          </thead>
          <tbody>
          <?php if(!empty($top_5_product_cpc)) { foreach($top_5_product_cpc as $val){ ?>
            <tr>
                <td><a href="<?php echo base_url(); ?>product_new/product_view/<?php echo $val['product_id']; ?>" target="_blank"><?php echo $val['product_id']; ?></a></td>
                          <td><?php echo $val['name']; ?></td>
                          <td><?php echo $val['company_name']; ?></td>
                          <td>&#8377; <?php echo number_format((float)$val['price'], 2, '.', ''); ?></td>
             <!-- <td><a href="<?php echo base_url(); ?>userCPCClickData_details<?php echo set_value('date_from')!='' ?set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!='' ? set_value('date_to') : '0'; ?>/<?php echo $val['product_id']; ?>"><span class="badge"><?php echo $val['count']; ?></span></a></td>-->
               <?php 
                if($page == 'Authenticated_CPC' || $page == 'Authenticated_CPA')
                {
               ?>
              <td><a href="<?php echo base_url(); ?>websiteusers/user_view/<?php echo $val['id']; ?>" target="_blank"><?php echo $val['email']; ?></a></td>
              <?php 
                  }
               ?>
              <td>&#8377; <?php echo number_format((float)($val['count'] * ($val['price'] * ($val['cpc_percentage']/100))), 2, '.', ''); ?></td>
               
              <td><?php echo $val['datetime']; ?></td>
            </tr>
          <?php } } ?>
          </tbody>
        </table>
      </div>
       <div class="pull-right">
          <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
  </div>
</div>
</section>
<!-- ChartJS 1.0.1 -->
<script src="<?php echo base_url(); ?>assets/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function () { 
  $( "#date_from" ).datepicker({ format: "yyyy-mm-dd" });
  $( "#date_to" ).datepicker({ format: "yyyy-mm-dd" });
  //users_signup_info();
  $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });
    $('.nav-tabs a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
    });
});
</script>