<?php
$users_menu=$this->data['users_menu'];
?>

<section class="content-header">
        <h1>CPC Info (Authenticated & Unauthenticated) <span class="badge"><?php echo count($product_cpc_user_info); ?></span></h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>
          </ol>
</section>
<section class="content">
  <div class="box">
    	 <div class="box-body">
        <div class="table-responsive">
          <table class="table no-margin">
            <thead>
              <tr>
                <th>User Id</th>
                <th>Click Count</th>
                <th>Date time</th>
              </tr>
            </thead>
            <tbody>
           <?php if(!empty($product_cpc_user_info)) { foreach($product_cpc_user_info as $val){ ?>
              <tr>
                <td><?php if($val['user_id'] == 0) { echo $val['user_id'];} else {
                	echo '<a href="'.base_url().'websiteusers/user_view/'.$val['user_id'].'" target="_blank">'.$val['user_id']."</a>";

                	} ?></td>
                <td><?php echo $val['click_count'] == 0 ? 1 : $val['click_count']; ?></td>
                <td><?php echo $val['created_datetime']; ?></td>
              </tr>
            <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>
        </div>
</section>
