<?php
/*echo '<pre>';
print_r($paid_brand_count);
exit();*/
?>
<section class="content-header">
        <h1>Unpaid Brands</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>
            <li><a href="<?php echo base_url(); ?>reports/un_paid_brand_count">Unpaid Brands</a></li>
            <li class="active">Unpaid Brands</li>
          </ol>
</section>
<section class="content">
  <form name="myform" id="myform" method="post">
    <div class="col-md-4">Date From : <input type="text" name="date_from" id="date_from" value="<?php echo set_value('date_from'); ?>"></div>
    <div class="col-md-4">Date To : <input type="text" name="date_to" id="date_to" value="<?php echo set_value('date_to'); ?>"></div>
     <div class="col-md-2"><input type="submit" name="submit" id="submit" value="Go" ></div>
  </form> 
 <div class="row">
          <div class="col-md-12">
            <div class="box">
            <ul class="nav nav-tabs">
              <!-- <li class="active"><a href="#home">Unpaid Brands</a></li> -->
              <li class="active"><a href="#home">Unpaid Brands &nbsp;&nbsp; <small class="label pull-right label-info"><?php echo $paid_brand_brand_count;?></small></a></li>
              <!--<li><a href="#menu1">Brand Product Uploaded</a></li>
              <li><a href="#menu2">Brand Product Click Count</a></li>
              <li><a href="#menu3">Brand Product CPC</a></li> -->                    
            </ul>                
              <div class="tab-content">
              <div class="box box-info tab-pane fade in active" id="home">              
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>S.no</th>                          
                          <!--<th>ID</th>-->
                          <th>Company Id</th> 
                          <th>Company Name</th> 
                          <th>Created Date Time</th> 
                          <th>Modified Date Time</th> 
                          <!-- <th>Product Price</th>
                          <th>Look ID</th>
                          <th>Look Name</th> -->
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(!empty($paid_brand_count)){
                      $i = 1;             
                         foreach($paid_brand_count as $key => $val){                                                     
                      ?>
                        <tr>
                          <td><?php echo $i; ?></td>
                          <!-- <td>
                              <a href="<?php echo base_url();?>/brandusers/brand_view/<?php echo $val['brand_id']; ?>" href="blank" target="_blank"><?php echo $val['brand_id']; ?>
                              </a>
                          </td> -->
                          
                          <!--<td><?php echo $val['id']; ?></td>-->
                          <td>
                            <a href="<?php echo base_url();?>/brandusers/brand_view/<?php echo $val['user_id']; ?>"  target="_blank"><?php echo $val['user_id']; ?></a>
                            <?php //echo $val['user_id']; ?>
                          </td>
                          <td><?php echo $val['company_name']; ?></td> 
                          <td><?php echo $val['created_datetime']; ?></td> 
                          <td><?php echo $val['modified_datetime']; ?></td> 
                          <!-- <td><?php //echo $val['price']; ?></td> 
                          <td><?php //echo $val['Look_ID']; ?></td> 
                          <td><?php //echo $val['lookname']; ?></td>  -->
                          <!-- <td><a href="<?php //echo base_url();?>/reports/brandClickdetails/product/<?php //echo $val['brand_id']; ?>" href="blank" target="_blank"><span class="badge"><?php //echo $val['product_count']; ?></span></a></td>
                          <td><a href="<?php //echo base_url();?>/reports/brandClickdetails/look/<?php //echo $val['brand_id']; ?>" href="blank" target="_blank"><span class="badge"><?php //echo $val['look_count']; ?></span></a></td> -->                             
                          <!--<td><?php //echo $val['cpc_count']; ?></td> 
                          <td><?php //echo $val['cpa_count']; ?></td>-->                         
                        </tr>
                      <?php                                          
                      $i++;
                      } 
                    }else{
                          echo "<tr><td>No Record Found</td></tr>";
                        } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!--div class="box-footer clearfix">
                  <a href="javascript::;" class="btn btn-sm btn-default btn-flat pull-right">View All Brand Details</a>
                </div-->
              </div>
              <div class="box box-info tab-pane fade" id="menu1">              
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>Brand Id</th>
                          <th>Brand Name</th>
                          <th>OnBoarded Date</th> 
                          <th>Product Count</th>                            
                        </tr>
                      </thead>
                      <tbody>
                      <?php //if(!empty($brand_product)) { foreach($brand_product as $val){ ?>
                        <tr>
                         <td><a href="pages/examples/invoice.html" href="blank"><?php //echo $val['brand_id']; ?></a></td>                         
                          <td><?php //echo $val['company_name']; ?></td>
                          <td><?php //echo $val['created_datetime']; ?></td>
                          <td><?php //echo $val['product_count']; ?></td>  
                        </tr>
                      <?php //} }else{
                       // echo "<tr><td>No Record Found</td></tr>";
                      //} ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="box-footer clearfix">
                  <a href="javascript::;" class="btn btn-sm btn-default btn-flat pull-right">View All Brand Product CPC</a>
                </div>
              </div>
              <div class="box box-info tab-pane fade" id="menu2">              
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>User Id</th>
                          <th>Username</th>
                          <th>Email</th>
                          <th>Returning Times</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(!empty($most_returning_users)) { foreach($most_returning_users as $val){ ?>
                        <tr>
                          <td><a href="pages/examples/invoice.html" href="blank"><?php echo $val['user_id']; ?></a></td>
                          <td><?php echo $val['user_name']; ?></td>
                          <td><?php echo $val['email']; ?></td>
                          <td><?php echo $val['count']; ?></td>
                        </tr>
                      <?php } } ?>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="box-footer clearfix">
                  <a href="javascript::;" class="btn btn-sm btn-default btn-flat pull-right">Brand Product Click Count</a>
                </div>
              </div>
              <div class="box box-info tab-pane fade" id="menu3">              
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>User Id</th>
                          <th>Username</th>
                          <th>Email</th>
                          <th>Returning Times</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(!empty($most_returning_users)) { foreach($most_returning_users as $val){ ?>
                        <tr>
                          <td><a href="pages/examples/invoice.html" href="blank"><?php echo $val['user_id']; ?></a></td>
                          <td><?php echo $val['user_name']; ?></td>
                          <td><?php echo $val['email']; ?></td>
                          <td><?php echo $val['count']; ?></td>
                        </tr>
                      <?php } } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="box-footer clearfix">
                  <a href="javascript::;" class="btn btn-sm btn-default btn-flat pull-right">View All Most Returning Users</a>
                </div>
              </div>


              <div class="box box-info tab-pane fade" id="menu4">              
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>User Id</th>
                          <th>Username</th>
                          <th>Email</th>
                          <th>Returning Times</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(!empty($most_returning_users)) { foreach($most_returning_users as $val){ ?>
                        <tr>
                          <td><a href="pages/examples/invoice.html" href="blank"><?php echo $val['user_id']; ?></a></td>
                          <td><?php echo $val['user_name']; ?></td>
                          <td><?php echo $val['email']; ?></td>
                          <td><?php echo $val['count']; ?></td>
                        </tr>
                      <?php } } ?>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="box-footer clearfix">
                  <a href="javascript::;" class="btn btn-sm btn-default btn-flat pull-right">View All Most Returning Users</a>
                </div>
              </div>

              </div>

            </div>
          </div>

        </div>
</section>

<!-- ChartJS 1.0.1 -->
<script src="<?php echo base_url(); ?>assets/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function () {
  $( "#date_from" ).datepicker({ format: "yyyy-mm-dd" });
  $( "#date_to" ).datepicker({ format: "yyyy-mm-dd" });
  //product_info();
  $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });
    $('.nav-tabs a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
    });
});

function product_info(){
  //-------------
  //- PIE CHART -
  //-------------
  // Get context with jQuery - using jQuery's .get() method.
 /* $.ajax({
      url: '<?php echo base_url(); ?>reports/get_brand_product',
      type: 'post',
      data: { 'type' : 'get_brand_product' },
      success: function(data, status) {   
            var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
            var pieChart = new Chart(pieChartCanvas);
            var PieData = <?php echo $signup; ?>;
            var pieOptions = {
              //Boolean - Whether we should show a stroke on each segment
              segmentShowStroke: true,
              //String - The colour of each segment stroke
              segmentStrokeColor: "#fff",
              //Number - The width of each segment stroke
              segmentStrokeWidth: 1,
              //Number - The percentage of the chart that we cut out of the middle
              percentageInnerCutout: 50, // This is 0 for Pie charts
              //Number - Amount of animation steps
              animationSteps: 100,
              //String - Animation easing effect
              animationEasing: "easeOutBounce",
              //Boolean - Whether we animate the rotation of the Doughnut
              animateRotate: true,
              //Boolean - Whether we animate scaling the Doughnut from the centre
              animateScale: false,
              //Boolean - whether to make the chart responsive to window resizing
              responsive: true,
              // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
              maintainAspectRatio: false,
              //String - A legend template
              legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
              //String - A tooltip template
              tooltipTemplate: "<%=value %> <%=label%> users"
            };
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.  
            pieChart.Doughnut(PieData, pieOptions);
            //-----------------
            //- END PIE CHART -
            //-----------------
      },
      error: function(xhr, desc, err) {
        console.log(err);
      }
    }); 
  */
  
}
</script>