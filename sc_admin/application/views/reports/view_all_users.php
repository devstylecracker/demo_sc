<?php $users_menu=$this->data['users_menu'];     
$table_search = $table_search == '0' ? '' : $table_search; 
 ?>
 <section class="content-header">
          <h1>Manage Most Returning Users</h1>
            <ol class="breadcrumb">
              <li><a href="<?php echo base_url(); ?>home">Home</a></li>
              <li class="active">Most Returning Users</li>
            </ol>
  </section> 
	<section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">List of Most Returning Users <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3>
      </div>
    <div class="box-body">  
      <form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>view_all_users" method="post">
        <div class="row">
          <div class="col-md-2">
             <?php $search_by = set_value('search_by')=='' ? $search_by : set_value('search_by'); ?>
            <select name="search_by" id="search_by" class="form-control input-sm" >
              <option value="0">Search by</option>
            <option selected="selected" value="1" <?php echo $search_by==1 ? 'selected' : ''; ?>>Username</option>
              <option value="2" <?php echo $search_by==2 ? 'selected' : ''; ?>>Emailid</option>
              <option value="3" <?php echo $search_by==3 ? 'selected' : ''; ?>>Date</option>
               <option value="4" <?php echo $search_by==4 ? 'selected' : ''; ?>>User ID</option>
            </select>
          </div>
          <div class="col-md-3">
            <div class="input-group">
              <input type="text" name="table_search" value="<?php echo set_value('table_search')=='' && set_value('table_search')==0 ? $table_search : set_value('table_search'); ?>" id="table_search" class="form-control input-sm pull-right" placeholder="Search"  required=""/>
              <div class="input-group-btn">
                <button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <a href="view_all_users" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
          </div>
          <div class="col-md-5">            
            <div class="pull-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>
      </form>
	<div class="box-body">
		  <div class="table-responsive">
      <table class="table table-bordered table-striped dataTable" id="example2">
        <thead>
          <tr>
            <th>Sr.No.</th>
            <th>ID</th>
            <th>Username</th>
            <th>Email</th>
            <th>Returning Times</th>
            <th>Last login time</th>             
          </tr>
        </thead>
        <tbody>
      		 <?php if(!empty($most_returning_users_)) { 
             $i=1;

            foreach($most_returning_users_ as $val){ ?>
      		  <tr>
                <td><?php echo $i; ?></td>
      			  <td><a href="<?php echo base_url(); ?>websiteusers/user_view/<?php echo $val['user_id']; ?>" target="_blank"><?php echo $val['user_id']; ?></a></td>
                <td><?php echo $val['user_name']; ?></td>
                <td><?php echo $val['email']; ?></td>
                <td><?php echo $val['count']; ?></td>
                 <td><?php echo $val['last_visit_time']; ?></td>
      		  </tr>
      		  <?php 
            $i++;} } ?>         
        </tbody>
      </table>
      </div>
		  <div>
          <?php echo $this->pagination->create_links(); ?>
      </div>
	 </div>
  </div>
</div>
</section>
