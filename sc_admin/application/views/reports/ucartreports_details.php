<?php $users_menu=$this->data['users_menu']; ?>
 <section class="content-header">
          <h1><?php echo $page_title; ?><small class="label label-info"><?php echo $total_rows; ?></small></h1>
            <ol class="breadcrumb">
              <li><a href="<?php echo base_url(); ?>home">Home</a></li>
              <li class="active">Details</li>
            </ol>
  </section>
	<section class="content">
    <div class="box">
      <div class="box-header with-border">
        <!--h3 class="box-title"><?php echo $page_title; ?> <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3-->
          <div class="pull-right">
        <!--form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>view_all_users" method="post">
          <?php echo $this->pagination->create_links(); ?>
        </form-->
        </div>
      </div>
    <div class="box-body">

    <?php if($type == 'product_click' || $type == 'notify_me' || $type == 'cart_click'){ ?>
	   <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th>Product ID</th>
              <th>Product Name</th>
              <th>Brand Name</th>
              <th>Price</th>
              <th>User ID</th>
              <th>Created Datetime</th>
            </tr>
          </thead>
          <tbody>
            <?php if(!empty($product_click_list)){ $total_price = 0; $i = 0;
              foreach($product_click_list as $val){ $total_price =  $total_price + $val['price'];
            ?>
            <tr>
              <td><a href="<?php echo base_url(); ?>product_new/product_view/<?php echo $val['id']; ?>" target="_blank"><?php echo $val['id']; ?></a></td>
              <td><?php echo $val['name']; ?></td>
              <td><?php echo $val['company_name']; ?></td>
              <td><i class="fa fa-inr"></i><?php echo $val['price']; ?></td>
              <td><?php echo $val['user_id']; ?></td>
              <td><?php echo $val['created_datetime']; ?></td>
            </tr>
            <?php $i++; } ?>
            <tr>
                <th colspan="3" style="text-align:right;">Total :-</th>
                <td><i class="fa fa-inr"></i><?php echo $total_price; ?></td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
       <!--div class="pull-right">
          <?php echo $this->pagination->create_links(); ?>
        </div-->
    </div>
   


    <?php } else if($type == 'abendant_cart' || $type == 'product_sales' || $type == 'remove_from_cart'){ ?>
         <div class="box-body">
        <div class="table-responsive">
          <table class="table no-margin">
            <thead>
              <tr>
                <th>Product ID</th>
                <th>Product Name</th>
                <th>Brand Name</th>
                <th>User ID</th>
                <th>Price</th>
                <th>Created Datetime</th>
              </tr>
            </thead>
            <tbody>
              <?php if(!empty($product_click_list)){ $total_price = 0; $i = 0;
                foreach($product_click_list as $val){ $total_price =  $total_price + $val['price'];
              ?>
              <tr>
                <td><a href="<?php echo base_url(); ?>product_new/product_view/<?php echo $val['id']; ?>" target="_blank"><?php echo $val['id']; ?></a></td>
                <td><?php echo $val['name']; ?></td>
                <td><?php echo $val['company_name']; ?></td>
                <td><a href="<?php echo base_url(); ?>websiteusers/user_view/<?php echo $val['user_id']; ?>"><?php echo $val['user_id']; ?></td>
                <td><i class="fa fa-inr"></i><?php echo $val['price']; ?></td>
                <td><?php echo $val['created_datetime']; ?></td>
              </tr>
              <?php $i++; } ?>
              <tr>
                <th colspan="3" style="text-align:right;">Total :-</th>
                <td><i class="fa fa-inr"></i><?php echo $total_price; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
         <!--div class="pull-right">
            <?php echo $this->pagination->create_links(); ?>
          </div-->
      </div>
    <?php }else if($type == 'looks'){ ?>
     
    <?php }else if($type == "view_products_used"){ ?>
     
    <?php } ?>

  </div>
</div>
</section>