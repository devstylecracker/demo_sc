
<section class="content-header">
        <h1>User <?php echo $type;?> Clicks</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>
          </ol>
</section>
 
<section class="content">
    <small> <h3 class="box-title">List of Product clicks </small><small class="label label-info"><?php echo @$count_product; ?></small></h3>
     
          
            <div class="box">
               <div class="tab-content">
              <div class="box box-info tab-pane fade in active" id="home">
              
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>Product Id</th>
                          <th>Product Name</th>
                          <th>User Id</th>
                          <th>Datetime</th>
                       </tr>
                      </thead>
                      <tbody>
                      <?php if(!empty($top_5_product_cpc)) { 
                               foreach($top_5_product_cpc as $val){ ?>
                                <tr>
                                  <td><a href="<?php echo base_url(); ?>product_new/product_view/<?php echo $val['id']; ?>" target="_blank"><?php echo $val['id']; ?></a></td>
                                  <td><?php echo $val['name']; ?></td>
                                  <?php 


                                  if($type=='Authenticated')  
                                  {
                                    ?>
                                  <td><a href="<?php echo base_url(); ?>websiteusers/user_view/<?php echo $val['user_id']; ?>" target="_blank"><?php echo $val['user_id']; ?></a></td>
                                  <?php }
                                    else if($type=='UnAuthenticated')
                                   { ?>
                                   <td><?php echo $val['user_id']; ?></td>
                                   <?php } ?>
                                                        
                                    <td><?php echo $val['datetime']; ?></td>
                                  
                                  
                                </tr>
                            </tr>
                      <?php } } ?>
                      </tbody>
                    </table>
                  </div>
                </div>