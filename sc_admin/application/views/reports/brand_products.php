<?php $users_menu=$this->data['users_menu'];
	  $total_rows = $this->data['total_rows'];
	  //echo "<pre>"; print_r($user_click);exit;
    $var = " ";
    if($type == 'Look')
    {
      $var = "Created";
    }elseif($type == 'Product')
    {
       $var = "Uploaded";
    }
?>
<section class="content-header">
        <h1>Brand <?php echo $type." ".$var; ?>  Report</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>
            <li class="active">Brand <?php echo $type; ?> Click Report</li>
          </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">List of Brand <?php echo $type." ".$var; ?> <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3>
							
		</div>
        <div class="box-body">
            <form name="frmsearch" class="form-group" id="frmsearch" action="<?php echo base_url(); ?>category" method="post">
            	<div class="row">
					<!--<div class="col-md-2">
					<select name="search_by" id="search_by" class="form-control input-sm" >
						<option value="0">Search by</option>						
						<option selected value="1" <?php echo set_value('search_by')==1 ? 'selected' : ''; ?>>Category Name</option>
						<option value="2" <?php echo set_value('search_by')==2 ? 'selected' : ''; ?>>Slug</option>
						<option value="3" <?php echo set_value('search_by')==3 ? 'selected' : ''; ?>>Is Active</option>
					</select>
					</div>-->
					<!--<div class="col-md-3">
					<div class="input-group">
						<input type="text" name="table_search" id="table_search" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo set_value('table_search'); ?>" required=""/>
							<div class="input-group-btn">
								<button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
							</div>
					</div>
					</div>
					<div class="col-md-2">
						<a href="reports/brand_product_info" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
					</div>-->
					<div class="col-md-5">						
						<div class="pull-right"><?php echo $this->pagination->create_links(); ?></div>
  					</div>
				</div>
			</form>

       <div class="table-responsive">
          <table class="table table-bordered table-striped dataTable" id="datatable">
            <thead>
            <tr>
              <th>#</th>
              <th>ID</th>
              <th>Name</th>
              <th>Created Datetime</th>                      
            </tr>
            </thead>
            <tbody>
                  <?php

                    if(!empty($user_click))
                    { $i=0;
                        foreach($user_click as $val)
                        { $i++;
                    ?>
                      <tr>
                      <td><?php echo $i; ?></td>
                      <?php 
                        if($type =='Look')
                        {
                      ?>
                       <td><a href="<?php echo base_url(); ?>lookcreator/look_view/<?php echo $val['id']; ?>" target="_blank" ><?php echo $val['id']; ?></a></td>
                      <?php
                        }elseif($type =='Product')
                        {
                       ?>
                       <td><a href="<?php echo base_url(); ?>product_new/product_view/<?php echo $val['id']; ?>" target="_blank"><?php echo $val['id']; ?></a></td>
                       <?php
                        }
                      ?>
                <td><?php echo $val['name']; ?></td> 
                <td><?php echo date($val['created_datetime']); ?></td>                           
              </tr>
            <?php
                }
              }else
              {
            ?>
                <tr>
                  <td >No Records Found</td>
                </tr>
            <?php
              }
            ?>
            </tbody>
          </table>  
           <?php //echo $this->pagination->create_links(); ?>

            </div>
				<?php echo $this->pagination->create_links(); ?>
        </div>
	</div>

</section>

