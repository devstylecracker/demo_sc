<?php $users_menu=$this->data['users_menu'];
//echo "<pre>";print_r($top_5_product_cpc);
 ?>
 <section class="content-header">
    <h1>View Product CPA Details</h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>home">Home</a></li>
        <li class="active">Details</li>
      </ol>
  </section>
	<section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">List of Product CPA Details <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3>
        <div class="pull-right">
        <form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>view_all_users" method="post">
          <?php echo $this->pagination->create_links(); ?>
        </form>
          </div>
      </div>
	   <div class="box-body">
        <div class="table-responsive">
          <table class="table no-margin">
            <thead>
              <tr>
                <th>Product ID</th>
                <th>Name</th>
                <th>Brand Name</th>
                <th>Price</th>
                <th>Click Count</th>
                <th>CPA</th>
              </tr>
            </thead>
            <tbody>
           <?php if(!empty($top_5_product_cpa)) { foreach($top_5_product_cpa as $val){ ?>
              <tr>
                <td><a href="<?php echo base_url(); ?>product_new/product_view/<?php echo $val['product_id']; ?>" target="_blank"><?php echo $val['product_id']; ?></a></td>
                <td><?php echo $val['name']; ?></td>
                <td><?php echo $val['company_name']; ?></td>
               <td>&#8377; <?php echo number_format((float)$val['price'], 2, '.', ''); ?></td>
                          <td><a href="<?php echo base_url(); ?>top_5/top_5_product_cpa/<?php echo set_value('date_from')!='' ?set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!='' ? set_value('date_to') : '0'; ?>/<?php echo $val['product_id']; ?>"><span class="badge"><?php echo $val['count']; ?></span></a></td>
                          <td>&#8377; <?php echo number_format((float)($val['count'] * ($val['price'] * ($val['cpa_percentage']/100))), 2, '.', ''); ?></td>
              </tr>
            <?php } } ?>
            </tbody>
          </table>
        </div>
        <div>
            <div class="pull-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
      </div>
  </div>
</section>
