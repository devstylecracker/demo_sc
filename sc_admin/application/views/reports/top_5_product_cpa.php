<?php
$users_menu=$this->data['users_menu'];
?>

<section class="content-header">
        <h1>CPA Info (Authenticated & Unauthenticated) <span class="badge"><?php echo count($product_cpc_user_info); ?></span></h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>
            <li class="active">CPA Info</li>
          </ol>
</section>

<section class="content">
   <div class="box">
	 <div class="box-body">
        <div class="table-responsive">
          <table class="table no-margin">
            <thead>
              <tr>
                <th>Order Id</th>
                <th>Email</th>
                <th>Date time</th>
              </tr>
            </thead>
            <tbody>
           <?php if(!empty($product_cpc_user_info)) { foreach($product_cpc_user_info as $val){ ?>
              <tr>
                <td><?php echo $val['order_id']; ?></td>
                <td><?php echo $val['email']; ?></td>
                <td><?php echo $val['created_datetime']; ?></td>
              </tr>
            <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>
        </div>
</section>
