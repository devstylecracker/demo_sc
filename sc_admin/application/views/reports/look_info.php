<section class="content-header">
  <h1>Looks Broadcasted <small class="label label-info"><?php echo @$total_rows; ?></small></h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">Looks Broadcasted</li>
  </ol>
</section>
<section class="content">
  <form name="myform" id="myform" class="frm-search" method="post">
    <div class="row">
      <div class="col-md-2">
        <div class="input-group">
          <input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm" id="date_from" value="<?php echo set_value('date_from'); ?>">

        </div>
      </div>
      <div class="col-md-2">
        <div class="input-group">
          <input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="<?php echo set_value('date_to'); ?>">

          <div class="input-group-btn">
            <button type="submit" class="btn btn-sm btn-default" id="submit"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </div>
      <div class="col-md-2">
        <a class="btn btn-default btn-sm" href="<?php echo base_url(); ?>/reports/look_info"><i class="fa fa-refresh"></i> Reset</a>
      </div>
    </div>
  </form>
  <br/>

  <div class="box">
    <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th>Sr. No.</th>
              <th>Bucket ID</th>
              <th>User Count</th>
              <th>Look Count</th>
              <th>Body Shape</th>
              <th>Style</th>

            </tr>
          </thead>
          <tbody>
            <?php if(!empty($look)) {
              $i=1;
              foreach($look as $val){ ?>
                <tr>

                  <td><?php echo $i; ?></td>
                  <td><?php echo $val['bucket']; ?></td>
                  <td><?php echo $val['user_count']; ?></td>
                  <td><?php echo $val['look_count']; ?></td>
                  <td><?php echo $val['body_shape']; ?></td>
                  <td><?php echo $val['style']; ?></td>

                </tr>
                <?php
                $i++;} }else
                {
                  echo '<tr><td colspan="6">No Record Found</td></tr>';
                } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>


    </div>

  </div>
</div>

</div>
</section>

<!-- ChartJS 1.0.1 -->
<script src="<?php echo base_url(); ?>assets/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function () {

  $( "#date_from" ).datepicker({ format: "yyyy-mm-dd" });
  $( "#date_to" ).datepicker({ format: "yyyy-mm-dd" });
  product_info();
  $(".nav-tabs a").click(function(){
    $(this).tab('show');
  });
  $('.nav-tabs a').on('shown.bs.tab', function(event){
    var x = $(event.target).text();         // active tab
    var y = $(event.relatedTarget).text();  // previous tab
    $(".act span").text(x);
    $(".prev span").text(y);
  });
});

function product_info(){
  //-------------
  //- PIE CHART -
  //-------------
  // Get context with jQuery - using jQuery's .get() method.
  /* $.ajax({
  url: '<?php echo base_url(); ?>reports/get_brand_product',
  type: 'post',
  data: { 'type' : 'get_brand_product' },
  success: function(data, status) {
  var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
  var pieChart = new Chart(pieChartCanvas);
  var PieData = <?php echo $signup; ?>;
  var pieOptions = {
  //Boolean - Whether we should show a stroke on each segment
  segmentShowStroke: true,
  //String - The colour of each segment stroke
  segmentStrokeColor: "#fff",
  //Number - The width of each segment stroke
  segmentStrokeWidth: 1,
  //Number - The percentage of the chart that we cut out of the middle
  percentageInnerCutout: 50, // This is 0 for Pie charts
  //Number - Amount of animation steps
  animationSteps: 100,
  //String - Animation easing effect
  animationEasing: "easeOutBounce",
  //Boolean - Whether we animate the rotation of the Doughnut
  animateRotate: true,
  //Boolean - Whether we animate scaling the Doughnut from the centre
  animateScale: false,
  //Boolean - whether to make the chart responsive to window resizing
  responsive: true,
  // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
  maintainAspectRatio: false,
  //String - A legend template
  legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
  //String - A tooltip template
  tooltipTemplate: "<%=value %> <%=label%> users"
};
//Create pie or douhnut chart
// You can switch between pie and douhnut using the method below.
pieChart.Doughnut(PieData, pieOptions);
//-----------------
//- END PIE CHART -
//-----------------
},
error: function(xhr, desc, err) {
console.log(err);
}
});
*/

}
</script>
