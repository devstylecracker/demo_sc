<section class="content-header">
  <h1>Brand Details: <span><?php echo @$brand_name; ?></span> <small><span class="label label-info"><?php //echo @$brand_id; ?></span></small> </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>reports/brand_product_info">Onboarded Brand List</a></li>
    <li class="active">Brand Details</li>
  </ol>
</section>

<section class="content">
  <form name="myform" id="myform" class="frm-search" method="post">
    <div class="row">
      <div class="col-md-2">
        <div class="input-group">
          <input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm" id="date_from" value="<?php echo set_value('date_from'); ?>">

        </div>
      </div>
      <div class="col-md-2">
        <div class="input-group">
          <input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="<?php echo set_value('date_to'); ?>">

          <div class="input-group-btn">
            <button type="submit" class="btn btn-sm btn-default" id="submit"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </div>
      <div class="col-md-2">
        <!--  <a class="btn btn-default btn-sm" href="<?php echo base_url(); ?>reports/brand_product_info"><i class="fa fa-refresh"></i> Reset</a>-->
      </div>
    </div>
  </form>
  <div>
    <small><strong>Note : </strong>Date filter is based on product and look broadcast date</small>
  </div>
  <br/>

  <div class="row">
    <div class="col-md-3">
      <div class="small-box bg-green">
        <div class="inner">
          <h3><?php echo count($product_uploaded); ?></h3>
          <p>Product uploaded</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <span class="small-box-footer"><?php echo set_value('date_from'); ?> to <?php echo set_value('date_to'); ?></span>
      </div>
    </div>
    <div class="col-md-3">
      <div class="small-box bg-green">
        <div class="inner">
          <h3><?php echo count($product_used); ?></h3>
          <p>Products used in look</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <span class="small-box-footer"><?php echo set_value('date_from'); ?> to <?php echo set_value('date_to'); ?></span>
      </div>
    </div>

    <div class="col-md-3">
      <div class="small-box bg-green">
        <div class="inner">
          <h3><?php echo count($product_used); ?></h3>
          <p>Product Clicks (CPC)</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <span class="small-box-footer"><?php echo set_value('date_from'); ?> to <?php echo set_value('date_to'); ?></span>
      </div>
    </div>
    <div class="col-md-3">
      <div class="small-box bg-green">
        <div class="inner">
          <h3><?php echo count($product_used); ?></h3>
          <p>Product Clicks (CPA)</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <span class="small-box-footer"><?php echo set_value('date_from'); ?> to <?php echo set_value('date_to'); ?></span>
      </div>
    </div>
  </div>
  <div class="box">

    <div class="box-body">
      <div class="table-responsive table-report">
        <table class="table no-margin">
          <thead>
            <tr>
              <th>Products uploaded(Product ID)</th>
              <th>Products used in looks</th>
              <th>Products Clicks (CPC) - Product ID/Product Click</th>
              <th>Products Click (CPA) - Product ID</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>

                <?php if(!empty($product_uploaded)) { ?>
                  <ul>
                    <?php  foreach($product_uploaded as $val){ ?>
                      <li><a href="<?php echo base_url(); ?>product_new/product_view/<?php echo $val['id']; ?>" target="_blank"><?php echo $val['id']; ?></a></li>
                      <?php  }
                      ?>
                    </ul>
                    <?php } ?>
                  </td>
                  <td>
                    <?php if(!empty($product_used)) {
                      ?>
                      <ul>
                        <?php
                        foreach($product_used as $val){ ?>
                          <li><a href="<?php echo base_url(); ?>product_new/product_view/<?php echo $val['id']; ?>" target="_blank"><?php echo $val['id']; ?></a></li>
                          <?php  }
                          ?>
                        </ul>
                        <?php } ?>
                      </td>
                      <td>

                        <?php if(!empty($product_click_info)) {
                          ?>
                          <ul>
                            <?php
                            foreach($product_click_info as $val){ ?>
                              <li>
                                <a href="<?php echo base_url(); ?>product_new/product_view/<?php echo $val['id']; ?>" target="_blank"><?php echo $val['id']; ?> / <span class="badge111"><?php echo $val['count']; ?></span></a></li>
                                <?php  }
                                ?>
                              </ul>
                              <?php } ?>
                            </td>
                            <td>


                              <?php if(!empty($product_cpa_info)) {
                                ?>
                                <ul>
                                  <?php
                                  foreach($product_cpa_info as $val){ ?>
                                    <li><a href="<?php echo base_url(); ?>product_new/product_view/<?php echo $val['id']; ?>" target="_blank"><?php echo $val['id']; ?></a></li>
                                    <?php  }
                                    ?>
                                  </ul>
                                  <?php } ?>
                                </td>
                              </tr>
                            </tbody>
                          </table>

                        </div>

                      </div>  </div>


                    </section>
                    <!-- ChartJS 1.0.1 -->
                    <script src="<?php echo base_url(); ?>assets/plugins/chartjs/Chart.min.js" type="text/javascript"></script>

                    <script type="text/javascript">
                    $(function () {
                      $( "#date_from" ).datepicker({ format: "yyyy-mm-dd" });
                      $( "#date_to" ).datepicker({ format: "yyyy-mm-dd" });
                    });
                    </script>
