<?php $users_menu=$this->data['users_menu']; ?>
<section class="content-header">
  <h1>View Brand Click Details</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">Details</li>
  </ol>
</section>
<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">List of Brand Click Details <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3>
      <div class="pull-right">
        <form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>view_all_users" method="post">
          <?php echo $this->pagination->create_links(); ?>
        </form>
      </div>
    </div>
    <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th>Brand ID</th>
              <th>Brand Name</th>
              <th>Click Count</th>              
            </tr>
          </thead>
           <tbody>
               <?php if(!empty($top_5_brands_cpc)) { foreach($top_5_brands_cpc as $val){ ?>
                  <tr>
                    <td><a href="<?php echo base_url(); ?>brandusers/brand_view/<?php echo $val['brand_id']; ?>" target="_blank"><?php echo $val['brand_id']; ?></a></td>
                    <td><?php echo $val['company_name']; ?></td>
                    <td><a href="<?php echo base_url(); ?>top_5/top_5_brands_cpc/<?php echo $this->uri->segment(3)!='' ?$this->uri->segment(3) : '0'; ?>/<?php echo  $this->uri->segment(4)!='' ? $this->uri->segment(4) : '0'; ?>/<?php echo $val['brand_id']; ?>"><span class="badge"><?php echo $val['count']; ?></span></a></td>
                  </tr>
                <?php } } ?>
                </tbody>
          </table>
        </div>
        <div>
          <div class="pull-right">
            <?php echo $this->pagination->create_links(); ?>
          </div>
        </div>
      </div>
    </div>

</section>
