<?php
$total_product_uploaded =0;
$total_product_approved =0;
$total_product_reject =0;
$total_product_pending =0;
$total_product_usedtilltdy =0;
$total_product_uploadtdy =0;
$total_product_usedtdy =0;
$total_product_costprdtdy =0;
$total_tdy_cpc = 0;
$total_tdy_cpa = 0;

?>
<section class="content-header">
  <h1>Onboarded Brand List</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <!--li><a href="<?php echo base_url(); ?>reports/brand_info">Brand Details</a></li-->
    <li class="active">Onboarded Brand List</li>
  </ol>
</section>
<section class="content">
  <form name="myform" id="myform" class="frm-search" method="post">
    <div class="row">
      <div class="col-md-2">
        <div class="input-group">
          <input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm" id="date_from" value="<?php echo set_value('date_from'); ?>">

        </div>
      </div>
      <div class="col-md-2">
        <div class="input-group">
          <input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="<?php echo set_value('date_to'); ?>">

          <div class="input-group-btn">
            <button type="submit" class="btn btn-sm btn-default" id="submit"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </div>
      <div class="col-md-2">
        <a class="btn btn-default btn-sm" href="<?php echo base_url(); ?>reports/brand_product_info"><i class="fa fa-refresh"></i> Reset</a>
      </div>
    </div>
  </form>
  <div>
    <small><strong>Note : </strong>Date filter is based on onboarded date</small>
  </div>
  <br/>

      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#home">Brand Onboarded</a></li>
          <!--<li><a href="#menu1">Brand Product Uploaded</a></li>
          <li><a href="#menu2">Brand Product Click Count</a></li>
          <li><a href="#menu3">Brand Product CPC</a></li> -->
        </ul>
        <div class="tab-content">
          <div class="tab-pane fade in active" id="home">
            <div class="table-responsive">
              <table class="table no-margin table-report">

                <thead>
                  <tr>
                    <th>Sr. No.</th>
                    <th>Brand Id</th>
                    <th>Brand Name</th>
                    <th>OnBoarded Date</th>
                    <th>No. of Products Uploaded till today</th>
                    <th>No. of Products Approved</th>
                    <th>No. of Products Rejected</th>
                    <th>No. of Products Pending</th>
                    <th>No. of products used till today</th>
                    <th>No. of Products Uploaded today</th>
                    <th>No. of products used today</th>
                    <th>Cost of products used today</th>
                    <th>Todays CPC</th>
                    <th>Todays CPA</th>
                    <th>Link</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(!empty($brand)) {
                    $i=1;
                    foreach($brand as $val){ ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><a href="<?php echo base_url();?>brandusers/brand_view/<?php echo $val['brand_id']; ?>" href="blank" target="_blank"><?php echo $val['brand_id']; ?></a></td>
                        <td><?php echo $val['company_name']; ?></td>
                        <td><?php echo $val['created_datetime']; ?></td>
                        <!--td><a href="<?php echo base_url();?>/reports/brandClickdetails/product/<?php echo $val['brand_id']; ?>" href="blank" target="_blank"><span class="badge"><?php echo $val['product_count']; ?></span></a></td-->
                        <td><a href="<?php echo base_url();?>reports/brand_details/Product_Uploaded/<?php echo $val['brand_id']; ?>/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="badge"><?php echo $val['product_count']; $total_product_uploaded = $total_product_uploaded+$val['product_count']; ?></a></td>

                        <td><a href="<?php echo base_url();?>reports/brand_details/Product_Approved/<?php echo $val['brand_id']; ?>/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="badge"><?php echo $val['product_approve']; 
                                  $total_product_approved=$total_product_approved+ $val['product_approve']; ?></a></td>

                        <td><a href="<?php echo base_url();?>reports/brand_details/Product_Reject/<?php echo $val['brand_id']; ?>/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="badge"><?php echo $val['product_reject']; 
                                  $total_product_reject = $total_product_reject+$val['product_reject']; ?></a></td>

                        <td><a href="<?php echo base_url();?>reports/brand_details/Product_Pending/<?php echo $val['brand_id']; ?>/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="badge"><?php echo $val['product_pending']; 
                                $total_product_pending=$total_product_pending+ $val['product_pending'];  ?></a></td>


                        <!--td><a href="<?php echo base_url();?>/reports/brandClickdetails/look/<?php echo $val['brand_id']; ?>" href="blank" target="_blank"><span class="badge"><?php echo $val['look_count']; ?></span></a></td-->

                        <td><a href="<?php echo base_url();?>reports/brand_details/Product_Used/<?php echo $val['brand_id']; ?>/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="badge"><?php echo $val['look_count']; 
                               $total_product_usedtilltdy = $total_product_usedtilltdy + $val['look_count'];  ?></a></td>

                        <td><a href="<?php echo base_url();?>reports/brand_details/Product_Uploaded_Today/<?php echo $val['brand_id']; ?>/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="badge"><?php echo $val['product_count_today']; 
                            $total_product_uploadtdy = $total_product_uploadtdy+$val['product_count_today']; ?></a></td>

                        <td><a href="<?php echo base_url();?>reports/brand_details/Product_Used_Today/<?php echo $val['brand_id']; ?>/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="badge"><?php echo $val['look_count_today']; 
                              $total_product_usedtdy = $total_product_usedtdy+$val['look_count_today']; ?></a></td>

                        <td><span class="badge">&#8377;<?php echo $val['cost_product_count_today']; 
                            $total_product_costprdtdy = $total_product_costprdtdy + $val['cost_product_count_today']; ?></span></td>

                        <td><a href="<?php echo base_url();?>reports/brand_details/Product_CPC/<?php echo $val['brand_id']; ?>/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="badge"><?php echo $val['cpc_count']; 
                            $total_tdy_cpc = $total_tdy_cpc + $val['cpc_count'];  ?></a></td>

                        <td><a href="<?php echo base_url();?>reports/brand_details/Product_CPA/<?php echo $val['brand_id']; ?>/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="badge"><?php echo $val['cpa_count']; 
                              $total_tdy_cpa  = $total_tdy_cpa + $val['cpa_count'];  ?></a></td>

                        <td><a href="<?php echo  base_url(); ?>brand_report_details/index/<?php echo $val['brand_id']; ?>" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View</a></td>

                      </tr>

                      <?php
                      $i++;
                    } 
                      echo "<tr><th colspan='4' class='title'>Total : </th><th>".$total_product_uploaded."</th>
                            <th>".$total_product_approved."</th><th>".$total_product_reject."</th>
                            <th>".$total_product_pending."</th><th>".$total_product_usedtilltdy."</th>
                            <th>".$total_product_uploadtdy."</th><th>".$total_product_usedtdy."</th>
                            <th>".$total_product_costprdtdy."</th><th>".$total_tdy_cpc."</th>
                            <th>".$total_tdy_cpa."</th></tr>";
                    }else
                    {
                      echo "<tr><td>No Record Found</td></tr>";
                    } ?>

                  </tbody>
                </table>
              </div>


              <!--div class="box-footer clearfix">
              <a href="javascript::;" class="btn btn-sm btn-default btn-flat pull-right">View All Brand Details</a>
            </div-->
          </div>

          <div class="tab-pane fade" id="menu1">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                  <tr>
                    <th>Brand Id</th>
                    <th>Brand Name</th>
                    <th>OnBoarded Date</th>
                    <th>Product Count</th>
                  </tr>
                </thead>
                <tbody>
                  <?php //if(!empty($brand_product)) { foreach($brand_product as $val){ ?>
                    <tr>
                      <td><a href="pages/examples/invoice.html" href="blank"><?php //echo $val['brand_id']; ?></a></td>
                      <td><?php //echo $val['company_name']; ?></td>
                      <td><?php //echo $val['created_datetime']; ?></td>
                      <td><?php //echo $val['product_count']; ?></td>
                    </tr>
                    <?php //} }else{
                      // echo "<tr><td>No Record Found</td></tr>";
                      //} ?>
                    </tbody>
                  </table>
                </div>


                <div class="box-footer clearfix">
                  <a href="javascript::;" class="btn btn-sm btn-default btn-flat pull-right">View All Brand Product CPC</a>
                </div>
              </div>

              <div class="tab-pane fade" id="menu2">
                <div class="table-responsive">
                  <table class="table no-margin">
                    <thead>
                      <tr>
                        <th>User Id</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Returning Times</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(!empty($most_returning_users)) { foreach($most_returning_users as $val){ ?>
                        <tr>
                          <td><a href="pages/examples/invoice.html" href="blank"><?php echo $val['user_id']; ?></a></td>
                          <td><?php echo $val['user_name']; ?></td>
                          <td><?php echo $val['email']; ?></td>
                          <td><?php echo $val['count']; ?></td>
                        </tr>
                        <?php } } ?>
                      </tbody>
                    </table>
                  </div>


                  <div class="box-footer clearfix">
                    <a href="javascript::;" class="btn btn-sm btn-default btn-flat pull-right">Brand Product Click Count</a>
                  </div>
                </div>

                <div class="tab-pane fade" id="menu3">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>User Id</th>
                          <th>Username</th>
                          <th>Email</th>
                          <th>Returning Times</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if(!empty($most_returning_users)) { foreach($most_returning_users as $val){ ?>
                          <tr>
                            <td><a href="pages/examples/invoice.html" href="blank"><?php echo $val['user_id']; ?></a></td>
                            <td><?php echo $val['user_name']; ?></td>
                            <td><?php echo $val['email']; ?></td>
                            <td><?php echo $val['count']; ?></td>
                          </tr>
                          <?php } } ?>
                        </tbody>
                      </table>
                    </div>

                    <div class="box-footer clearfix">
                      <a href="javascript::;" class="btn btn-sm btn-default btn-flat pull-right">View All Most Returning Users</a>
                    </div>
                  </div>

                  <div class="tab-pane fade" id="menu4">
                    <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                          <tr>
                            <th>User Id</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Returning Times</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php if(!empty($most_returning_users)) { foreach($most_returning_users as $val){ ?>
                            <tr>
                              <td><a href="pages/examples/invoice.html" href="blank"><?php echo $val['user_id']; ?></a></td>
                              <td><?php echo $val['user_name']; ?></td>
                              <td><?php echo $val['email']; ?></td>
                              <td><?php echo $val['count']; ?></td>
                            </tr>
                            <?php } } ?>
                          </tbody>
                        </table>
                      </div>

                      <div class="box-footer clearfix">
                        <a href="javascript::;" class="btn btn-sm btn-default btn-flat pull-right">View All Most Returning Users</a>
                      </div>
                    </div>



                  </div>
                </div>
            </section>

            <!-- ChartJS 1.0.1 -->
            <script src="<?php echo base_url(); ?>assets/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
            <script type="text/javascript">
            $(function () {
              $( "#date_from" ).datepicker({ format: "yyyy-mm-dd" });
              $( "#date_to" ).datepicker({ format: "yyyy-mm-dd" });
              //product_info();
              $(".nav-tabs a").click(function(){
                $(this).tab('show');
              });
              $('.nav-tabs a').on('shown.bs.tab', function(event){
                var x = $(event.target).text();         // active tab
                var y = $(event.relatedTarget).text();  // previous tab
                $(".act span").text(x);
                $(".prev span").text(y);
              });
            });

            function product_info(){
              //-------------
              //- PIE CHART -
              //-------------
              // Get context with jQuery - using jQuery's .get() method.
              /* $.ajax({
              url: '<?php echo base_url(); ?>reports/get_brand_product',
              type: 'post',
              data: { 'type' : 'get_brand_product' },
              success: function(data, status) {
              var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
              var pieChart = new Chart(pieChartCanvas);
              var PieData = <?php echo $signup; ?>;
              var pieOptions = {
              //Boolean - Whether we should show a stroke on each segment
              segmentShowStroke: true,
              //String - The colour of each segment stroke
              segmentStrokeColor: "#fff",
              //Number - The width of each segment stroke
              segmentStrokeWidth: 1,
              //Number - The percentage of the chart that we cut out of the middle
              percentageInnerCutout: 50, // This is 0 for Pie charts
              //Number - Amount of animation steps
              animationSteps: 100,
              //String - Animation easing effect
              animationEasing: "easeOutBounce",
              //Boolean - Whether we animate the rotation of the Doughnut
              animateRotate: true,
              //Boolean - Whether we animate scaling the Doughnut from the centre
              animateScale: false,
              //Boolean - whether to make the chart responsive to window resizing
              responsive: true,
              // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
              maintainAspectRatio: false,
              //String - A legend template
              legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
              //String - A tooltip template
              tooltipTemplate: "<%=value %> <%=label%> users"
            };
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            pieChart.Doughnut(PieData, pieOptions);
            //-----------------
            //- END PIE CHART -
            //-----------------
          },
          error: function(xhr, desc, err) {
          console.log(err);
        }
      });
      */

    }
    </script>
