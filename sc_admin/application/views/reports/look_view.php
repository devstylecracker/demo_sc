<?php
//echo "<pre>";print_r($product_data); 
$users_menu=$this->data['users_menu']; 
?>
<section class="content-header">
    <h1>Look Details</h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>home">Home</a></li>
      <li><a href="<?php echo base_url(); ?>lookcreator/manage_look">Manage Looks </a></li>
      <li class="active">Look Details</li>
    </ol>
</section>
<section class="content page-look-details">
  <div class="row">
    <div class="col-md-6">
      <div class="box look-details">

        <div class="box-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable table-vth" id="datatable">
                    <tr>
                      <th style="width:25%;">Look ID</th>
                      <td><?php echo $look_data['0']['id']; ?></td>
                    </tr>
                    <tr>
                      <th>Name</th>
                      <td><?php echo $look_data['0']['name']; ?></td>
                    </tr>
                    <tr>
                      <th>Description</th>
                      <td><?php echo $look_data['0']['description']; ?></td>
                    </tr>
                    <tr>
                      <th>Look Type</th>
                      <td><?php if($look_data['0']['look_type'] == 1){ echo 'Stylist'; }else if($look_data['0']['look_type'] == 2){ echo 'Street Style Look'; }else if($look_data['0']['look_type'] == 3){ echo "Promotional Look"; } ?></td>
                    </tr>
                    <tr>
                      <th>Status</th>
                      <td><?php if($look_data['0']['status']==0){ echo "<span class='text-orange'>Pending</span>"; }else if($look_data['0']['status']==1){ echo "<span class='text-green'>Approved</span>"; }else { echo "<span class='text-red'>Rejected</span>"; } ?></td>
                    </tr>
                    <tr><th>Product Images</th>
                      <td class="prod">
                      <ul id="product_list" class="product_imgs">
            						<?php
            						if(!empty($product_data)){
            							foreach($product_data as $val){  ?>
                          <li id='productSelect' style=" display: inline-block; ">
            								<img src="<?php echo base_url(); ?>assets/products/thumb_160/<?php echo $val['image']; ?>" data-price="<?php echo $val['price']; ?>" data-product="<?php echo $val['product_id']; ?>">
            							</li>
                          <?php	}
            							}
            						?>
                        </ul>
            						</td>
            					</tr>
            <?php
                if(in_array("54",$users_menu)){
            ?>
                      <tr>
                        <th>Look Cost</th>
                        <td><div id="final_look_cost" value="0"></div></td> 
                      </tr>
                      <tr>
                      <th>Budget</th>                                                   
                        <td><select name="budget" id="budget"  class="form-control input-sm">
                         <?php if(!empty($budget)) {
                              foreach($budget as $key=>$val){
                                  echo '<option value="'.$key.'">'.$val.'</option>';
                              }
                          } ?>
                        </select>
                        </td> 
                        <td><button class="btn btn-primary btn-sm" id="budgetEdit" name="budgetEdit" type="button" value="">
                        <i class="fa fa fa-thumbs-up"></i> Save </button>
                        <input type="hidden" name="look_id" id="look_id" value="<?php echo $look_data['0']['id']; ?>">
                         <input type="hidden" name="product_ids" id="product_ids" value="<?php echo $look_data['0']['id']; ?>">
                     </td>                                                 
                    </tr>
                    <tr><td colspan="3"><div id="message_display"></div></td></tr>
            <?php
              }
            ?>
          				  <tr><th>Bucket ID</th>
          					  <th>Body Shape</th>
          					  <th>Style</th> 
                      <th>Work Style</th> 
                      <th>Personal Style</th>    
                      <th>Budget</th>
                      <th>Look Cost <i class="fa fa-inr"></i></th>               
                      <td class="prod">					  
            						<?php
									
            						if(!empty($bucket_data)){
            							foreach($bucket_data as $val){  ?>
										<tr>
											<td><?php echo $val['bucket_id'] ?></td>
											<td><?php echo $val['body_shape'] ?></td>
											<td><?php echo $val['style'] ?></td>
                      <td><?php echo $val['work_style'] ?></td>
                      <td><?php echo $val['personal_style'] ?></td>
                      <td><?php echo $val['budget'] ?></td>
                      <td><?php echo $val['look_cost'] ?></td>
										</tr>
											<?php //echo $val['body_shape'].'&nbsp&nbsp&nbsp&nbsp'.$val['style'];
											     // echo "<br>";
												?>
            							<?php	}
            							}
            						?>
            						</td>
            					</tr>
					           <tr><th>User Count</th>
                      <td class="prod">
            						<?php
            						if(!empty($user_count)){
            							foreach($user_count as $val){  ?>
            								<?php echo $val['user_count'];
											      echo "<br>";
												?>
            							<?php	}
            							}
            						?>
            						</td>
            					</tr>			
                    </table>
                  </div>
                </div>
              </div>
</div>
<div class="col-md-6">
  <div class="box">
    <div class="box-body">
      <div class="look-img">
            <?php if($look_data['0']['look_type']== 2 || $look_data['0']['look_type']== 4) { ?>
              <img src="<?php echo base_url(); ?>assets/street_img/<?php echo $look_data['0']['look_image']; ?>" >
          <?php }else if($look_data['0']['look_type']== 3){ ?>
          <img src="<?php echo base_url(); ?>assets/products/thumb/<?php echo $look_data['0']['product_img']; ?>" >
          <?php }else{ ?>
            <img src="<?php echo base_url(); ?>assets/looks/<?php echo $look_data['0']['image']; ?>" >
            <?php } ?>
      </div>
        </div>
        </div>
        </div>
        </div>
</section>
<script type="text/Javascript">

	$( document ).ready(function() {
		$('.nav-item-look').addClass("active");
    $('.nav-manage-looks').addClass("active");

    $( "#budgetEdit" ).click(function() {
      var look_id = $('#look_id').val();      
      var budget = $('#budget').val();
      var lookCost = $('#final_look_cost').text();
      var productIDs = $('#product_ids').val();
     
      if(look_id.trim() == ''){
        $('#message_display').html('Something went wrong');
        }else if(budget.trim() == '' || budget<=0){
          $('#message_display').html('<span class="text-red" >Please select Budget</span>');
          }else{
              $.ajax({
               type: "POST",
               url: "<?php echo base_url(); ?>"+"Lookcreator/look_broadcast_edit/",
               data: { look_id: look_id,budget:budget,lookCost : lookCost, productIds : productIDs },
               cache:false,
               success:
                  function(result){                 
                      
                    if(result == 'Ok')
                    {
                      $('#message_display').html('<span class="text-green" >Successfully Edited the look budget</span>');
                      location.reload();  
                    }else
                    {                      
                      $('#message_display').html('<span class="text-red" >Error</span>');                    
                    }

                  }
              });
            }
    });


  function msg(m){ console.log(m); }

  var arrProductID = [];
  var intProductID =0;
  //##Image Select/Deselect Image
  $('ul.product_imgs li').on('mouseup',function(){

    //##Getting Product ID--------------------
    intProductID = $(this).find('img').attr('data-product');

    if(!$(this).hasClass('selected')){
      $(this).addClass('selected');
    
      var intValue = Number($('#final_look_cost').text());
      $('#final_look_cost').text(intValue+Number($(this).find('img').attr('data-price')));

      arrProductID.push(intProductID);    
      
      var final_look_cost = $('#final_look_cost').text();     
      setBudget(final_look_cost);

      msg("add: "+ intProductID+" :: arrProductID: "+arrProductID);
    } else {

      $(this).removeClass('selected');      


       arrProductID = $.grep(arrProductID, function( a ) {
          return a !== intProductID;
       });

      msg("delete: "+ intProductID+" :: arrProductID: "+arrProductID);


      var intValue = Number($('#final_look_cost').text());
      $('#final_look_cost').text(intValue-Number($(this).find('img').attr('data-price')));

      var final_look_cost = $('#final_look_cost').text();
      setBudget(final_look_cost); 

    }

    $('#product_ids').val(arrProductID);
      
  });

	});

  function setBudget(finallookcost)
  {
    var final_look_cost = finallookcost;
    
    if(final_look_cost >= 50000)
    {
      $('#budget').val(4670);       
    }else
    if(final_look_cost > 15000 && final_look_cost < 50000 )
    {
      $('#budget').val(38);

    }else 
    if(final_look_cost > 5000 && final_look_cost < 15000)
    {
      $('#budget').val(37);
      
    }else 
    if(final_look_cost > 0 && final_look_cost < 5000 )
    {
      $('#budget').val(36);       
    }else 
    if(final_look_cost == 0)
    {
      $('#budget').val(0);
    }
    $('#budget').attr('selected');    
  }

</script>
<style> .selected { border:2px solid green;}</style>