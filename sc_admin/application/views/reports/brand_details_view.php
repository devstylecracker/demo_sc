  <?php
  $users_menu=$this->data['users_menu'];
  $pagetitle = $this->uri->segment(3);
  $titlet = $pagetitle;
  $title = str_replace('_', ' ', $titlet);
  
  
  ?>
  <section class="content-header">
    <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>home">Home</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">List of <?php echo $title; ?> <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3>
      <div class="pull-right">
      </div>
      </div><!-- /.box-header -->
    <div class="box-body">
      <?php echo $this->session->flashdata('feedback'); ?>

      <form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>websiteusers" method="post">
        <div class="row">  
          <div class="col-md-5">            
            <div class="pull-right">
              <?php //echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>
      </form>

    <div class="table-responsive">
      <table class="table table-bordered table-striped dataTable" id="example2">
        <thead>         
          <tr>
            <th>Sr.No.</th>
          <?php if(!empty($brand_data['fields'])) 
            { $i = 1;
            foreach($brand_data['fields'] as $val)
              {
                if( ($pagetitle == 'Product_Used' && $val != 'look_id') || ($pagetitle =='Product_CPC' && $val != 'user_id') || ($pagetitle =='Product_CPA' && $val != 'user_id') ||($pagetitle =='Product_Uploaded') ||($pagetitle =='Product_Uploaded_Today') ||($pagetitle =='Product_Used_Today' && $val != 'look_id' ) ||($pagetitle =='Product_Approved') ||($pagetitle =='Product_Reject') ||($pagetitle =='Product_Pending') )
                {
                  if($val == 'id')
                  {
                ?>
                <th><?php echo 'product_id'; ?></th>          
                <?php

                  }else
                  {
                ?>
                <th><?php echo $val; ?></th>          
                <?php
                  }
             
                }				
              }
            }
          ?>
          </tr>
        </thead>
        <tbody>
          <?php if(!empty($brand_data)) { $i = 1;
            foreach($brand_data['data'] as $val){
              ?>
              <tr>
                <td><?php echo $i; ?></td>
                <td><a href="<?php echo base_url(); ?>product_new/product_view/<?php echo $val['id']; ?>" target="_blank" ><?php echo $val['id']; ?></a></td>
                <td><?php echo $val['name']; ?></td>
                <td>&#8377; <?php echo $val['price']; ?></td>
                <?php
                      if( ($pagetitle == 'Product_Used_Today') ||($pagetitle == 'Product_Used') ||($pagetitle == 'Product_Used_Today') )
                      {
                ?>
                <td><a href="<?php echo base_url(); ?>lookcreator/look_view/<?php echo $val['look_id']; ?>" target="_blank" ><?php echo $val['look_name']; ?></a></td>
                <?php 
                      }else if($pagetitle == 'Product_CPA' || $pagetitle == 'Product_CPC')
					  {
                ?>
					 <td><a href="<?php echo base_url(); ?>websiteusers/user_view/<?php echo $val['user_id']; ?>" target="_blank" ><?php echo $val['user_name']; ?></a></td>
					 <td><?php echo $val['email'];                       
                 ?></td>
				<?php
					  }else if($pagetitle == 'Product_Reject')
					  {
				?>
					 <td><?php echo $val['reason'];                       
                 ?></td>
				<?php
					  }
				?>
                <td><?php echo $val['datetime'];                       
                 ?></td>
                <td><?php echo $val['created_by_stylist'];                       
                 ?></td>
                <td><?php echo $val['Broadcasted_by_stylist'];                       
                 ?></td>
                  </tr>
                  <?php $i++; } }else{ ?>
                    <tr>
                      <td colspan="5">Sorry no record found</td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <?php //echo $this->pagination->create_links(); ?>
            </div>
          </div>

        </section>
        <!-- /.content
        <script type="text/Javascript">
        $( document ).ready(function() {
          $('[data-toggle=confirmation]').confirmation({
            title:'Are you sure?',
            onConfirm : function(){
              var del_id = $(this).closest('td').find('.btn-danger').attr('data-id');
              $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "users/user_delete",
                data: { id: del_id },
                cache:false,
                success:
                function(data){
                  location.reload(); //as a debugging message.
                }

              });
            }
          });
          $('.nav-item-user').addClass("active");
          $('.nav-website-user').addClass("active");
        });
        </script> -->
