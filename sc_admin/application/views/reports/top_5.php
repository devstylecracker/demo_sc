<?php
$users_menu=$this->data['users_menu'];
?>
<section class="content-header">
  <h1>Top 5</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
  </ol>
</section>
<section class="content">
  <form name="myform" id="myform" class="frm-search" method="post">
    <div class="row">
      <div class="col-md-2">
        <div class="input-group">
          <input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm" id="date_from" value="<?php echo set_value('date_from'); ?>">

        </div>
      </div>
      <div class="col-md-2">
        <div class="input-group">
          <input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="<?php echo set_value('date_to'); ?>">

          <div class="input-group-btn">
            <button type="submit" class="btn btn-sm btn-default" id="submit"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </div>
      <div class="col-md-2">
        <a class="btn btn-default btn-sm" href="<?php echo base_url(); ?>home"><i class="fa fa-refresh"></i> Reset</a>
      </div>
    </div>
  </form>
  <br/>
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
                      <li class="active"><a href="#home">Top 5 CPC</a></li>
                      <li><a href="#menu1">Top 5 Looks Clicks</a></li>
                      <li><a href="#menu2">Top 5 CPA</a></li>
                      <li><a href="#menu3">Top 5 Brands Click</a></li>
                      <li><a href="#menu4">Top 5 Buyers</a></li>
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane fade in active" id="home">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>Product ID</th>
                          <th>Name</th>
                          <th>Brand Name</th>
                          <th>Price</th>
                          <th>Click Count</th>
                          <th>CPC</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(!empty($top_5_product_cpc)) { foreach($top_5_product_cpc as $val){ ?>
                        <tr>
                          <td><a href="<?php echo base_url(); ?>product_new/product_view/<?php echo $val['product_id']; ?>" target="_blank"><?php echo $val['product_id']; ?></a></td>
                          <td><?php echo $val['name']; ?></td>
                          <td><?php echo $val['company_name']; ?></td>
                          <td>&#8377; <?php echo number_format((float)$val['price'], 2, '.', ''); ?></td>
                          <td><a href="<?php echo base_url(); ?>top_5/top_5_product_cpc/<?php echo set_value('date_from')!='' ?set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!='' ? set_value('date_to') : '0'; ?>/<?php echo $val['product_id']; ?>" target="_blank"><span class="badge"><?php echo $val['count']; ?></span></a></td>
                          <td>&#8377; <?php echo number_format((float)($val['count'] * ($val['price'] * ($val['cpc_percentage']/100))), 2, '.', ''); ?></td>
                        </tr>
                      <?php } } ?>
                      </tbody>
                    </table>
                  </div>
                <div class="box-footer">
                  <a href="<?php echo base_url(); ?>reports/top_5details/<?php echo set_value('date_from')!='' ?set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!='' ? set_value('date_to') : '0'; ?>" class="btn btn-sm btn-default btn-flat pull-right" target="_blank" >View All Product Clicks</a>
                </div>
              </div>

              <div class="tab-pane fade" id="menu1">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>Look ID</th>
                          <th>Name</th>
                          <th>Click Count</th>
                          <th>Broadcast Datetime </th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(!empty($top_5_look_cpc)) { foreach($top_5_look_cpc as $val){ ?>
                        <tr>
                          <td><a href="<?php echo base_url(); ?>lookcreator/look_view/<?php echo $val['look_id']; ?>" target="_blank"><?php echo $val['look_id']; ?></a></td>
                          <td><?php echo $val['name']; ?></td>
                          <td><a href="<?php echo base_url(); ?>top_5/top_5_looks_click/<?php echo set_value('date_from')!='' ?set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!='' ? set_value('date_to') : '0'; ?>/<?php echo $val['look_id']; ?>" target="_blank"><span class="badge"><?php echo $val['count']; ?></span></a></td>
                          <td><?php echo $val['modified_datetime']; ?></td>
                        </tr>
                      <?php } } ?>
                      </tbody>
                    </table>
                  </div>
                <div class="box-footer">
                  <a href="<?php echo base_url(); ?>reports/look_details/<?php echo set_value('date_from')!='' ?set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!='' ? set_value('date_to') : '0'; ?>" class="btn btn-sm btn-default btn-flat pull-right" target="_blank">View All Looks Click</a>
                </div>
              </div>

              <div class="tab-pane fade" id="menu2">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>Product ID</th>
                          <th>Name</th>
                          <th>Brand Name</th>
                          <th>Price</th>
                          <th>Click Count</th>
                          <th>CPA</th>
                        </tr>
                      </thead>
                      <tbody>
                     <?php if(!empty($top_5_product_cpa)) { foreach($top_5_product_cpa as $val){ ?>
                        <tr>
                          <td><a href="<?php echo base_url(); ?>product_new/product_view/<?php echo $val['product_id']; ?>" target="_blank"><?php echo $val['product_id']; ?></a></td>
                          <td><?php echo $val['name']; ?></td>
                          <td><?php echo $val['company_name']; ?></td>
                          <td>&#8377; <?php echo number_format((float)$val['price'], 2, '.', ''); ?></td>
                          <td><a href="<?php echo base_url(); ?>top_5/top_5_product_cpa/<?php echo set_value('date_from')!='' ?set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!='' ? set_value('date_to') : '0'; ?>/<?php echo $val['product_id']; ?>" target="_blank"><span class="badge"><?php echo $val['count']; ?></span></a></td>
                          <td>&#8377; <?php echo number_format((float)($val['count'] * ($val['price'] * ($val['cpa_percentage']/100))), 2, '.', ''); ?></td>
                        </tr>
                      <?php } } ?>
                      </tbody>
                    </table>
                  </div>

                <div class="box-footer">
                  <a href="<?php echo base_url(); ?>reports/product_cpa_details/<?php echo set_value('date_from')!='' ?set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!='' ? set_value('date_to') : '0'; ?>" class="btn btn-sm btn-default btn-flat pull-right" target="_blank">View All CPA</a>
                </div>
              </div>


                <div class="tab-pane fade" id="menu3">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>Brand ID</th>
                          <th>Brand Name</th>
                          <th>Click Count</th>
                        </tr>
                      </thead>
                      <tbody>
                     <?php if(!empty($top_5_brands_cpc)) { foreach($top_5_brands_cpc as $val){ ?>
                        <tr>
                          <td><a href="<?php echo base_url(); ?>brandusers/brand_view/<?php echo $val['brand_id']; ?>" target="_blank"><?php echo $val['brand_id']; ?></a></td>
                          <td><?php echo $val['company_name']; ?></td>
                          <td><a href="<?php echo base_url(); ?>top_5/top_5_brands_cpc/<?php echo set_value('date_from')!='' ?set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!='' ? set_value('date_to') : '0'; ?>/<?php echo $val['brand_id']; ?>" target="_blank"><span class="badge"><?php echo $val['count']; ?></span></a></td>
                        </tr>
                      <?php } } ?>
                      </tbody>
                    </table>
                  </div>

                  <div class="box-footer">
                  <a href="<?php echo base_url(); ?>reports/top5brandclick_details/<?php echo set_value('date_from')!='' ?set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!='' ? set_value('date_to') : '0'; ?>" class="btn btn-sm btn-default btn-flat pull-right" target="_blank">View All Brand Click</a>
                </div>

              </div>

              <div class="tab-pane fade" id="menu4">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>User_id</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Buycount</th>
                          <th>Total Price</th>
                        </tr>
                      </thead>
                      <tbody>
                     <?php if(!empty($top_5_buyers)) { foreach($top_5_buyers as $val){ ?>
                        <tr>
                          <td><a href="<?php echo base_url(); ?>websiteusers/user_view/<?php echo $val['user_id']; ?>" target="_blank"><?php echo $val['user_id']; ?></a></td>
                          <td><?php echo $val['name']; ?></td>
                          <td><?php echo $val['email']; ?></td>
                          <td><a href="<?php echo base_url(); ?>reports/product_cpa_detail/<?php echo $val['user_id']; ?>" target="_blank" ><span class="badge"><?php echo $val['user_count']; ?></span></a></td>

                          <td>&#8377;<?php echo $val['total_price']; ?></td>
                        </tr>
                      <?php } } ?>
                      </tbody>
                    </table>
                  </div>

              <div class="box-footer">
                  <a href="<?php echo base_url(); ?>reports/get_all_cpa_users/<?php echo set_value('date_from')!='' ?set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!='' ? set_value('date_to') : '0'; ?>" class="btn btn-sm btn-default btn-flat pull-right" target="_blank">View All Users</a>
                </div>
              </div>


              </div>
            </div>
</section>

<!-- ChartJS 1.0.1 -->
<script src="<?php echo base_url(); ?>assets/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function () {
  $( "#date_from" ).datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true });
  $( "#date_to" ).datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true });
  //users_signup_info();
  $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });
    $('.nav-tabs a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
    });
});

function users_signup_info(){
  //-------------
  //- PIE CHART -
  //-------------
  // Get context with jQuery - using jQuery's .get() method.
  $.ajax({
      url: '<?php echo base_url(); ?>reports/get_users_info',
      type: 'post',
      data: { 'type' : 'get_variation' },
      success: function(data, status) {
            var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
            var pieChart = new Chart(pieChartCanvas);
            var PieData = '';
            var pieOptions = {
              //Boolean - Whether we should show a stroke on each segment
              segmentShowStroke: true,
              //String - The colour of each segment stroke
              segmentStrokeColor: "#fff",
              //Number - The width of each segment stroke
              segmentStrokeWidth: 0.0001,
              //Number - The percentage of the chart that we cut out of the middle
              percentageInnerCutout: 0, // This is 0 for Pie charts
              //Number - Amount of animation steps
              animationSteps: 100,
              //String - Animation easing effect
              animationEasing: "easeOutBounce",
              //Boolean - Whether we animate the rotation of the Doughnut
              animateRotate: true,
              //Boolean - Whether we animate scaling the Doughnut from the centre
              animateScale: false,
              //Boolean - whether to make the chart responsive to window resizing
              responsive: true,
              // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
              maintainAspectRatio: false,
              //String - A legend template
              legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
              //String - A tooltip template
              tooltipTemplate: "<%=value %> <%=label%> users"
            };
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            pieChart.Doughnut(PieData, pieOptions);
            //-----------------
            //- END PIE CHART -
            //-----------------
      },
      error: function(xhr, desc, err) {
        console.log(err);
      }
    });


}
</script>
