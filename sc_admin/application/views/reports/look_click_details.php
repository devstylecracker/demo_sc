<?php $users_menu=$this->data['users_menu']; ?>
<section class="content-header">
  <h1>View Look Click Details</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">Details</li>
  </ol>
</section>
<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">List of Look Click Details <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3>
      <div class="pull-right">
        <form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>view_all_users" method="post">
          <?php echo $this->pagination->create_links(); ?>
        </form>
      </div>
    </div>
    <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th>Look ID</th>
              <th>Name</th>
              <th>Click Count</th>
              <th>Broadcast Datetime </th>
            </tr>
          </thead>
          <tbody>
            <?php if(!empty($top_5_look_cpc)) { foreach($top_5_look_cpc as $val){ ?>
              <tr>
                <td><a href="<?php echo base_url(); ?>lookcreator/look_view/<?php echo $val['look_id']; ?>" target="_blank"><?php echo $val['look_id']; ?></a></td>
                <td><?php echo $val['name']; ?></td>
                <td><a href="<?php echo base_url(); ?>top_5/top_5_looks_click/<?php echo set_value('date_from')!='' ?set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!='' ? set_value('date_to') : '0'; ?>/<?php echo $val['look_id']; ?>"><span class="badge"><?php echo $val['count']; ?></span></a></td>
                <td><?php echo $val['modified_datetime']; ?></td>
              </tr>
              <?php } } ?>
            </tbody>
          </table>
        </div>
        <div>
          <div class="pull-right">
            <?php echo $this->pagination->create_links(); ?>
          </div>
        </div>
      </div>
    </div>

</section>
