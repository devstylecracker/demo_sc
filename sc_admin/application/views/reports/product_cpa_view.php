<?php $users_menu=$this->data['users_menu']; ?>
 <section class="content-header">
          <h1>View User CPA Details</h1>
            <ol class="breadcrumb">
              <li><a href="<?php echo base_url(); ?>home">Home</a></li>
              <li class="active">Details</li>
            </ol>
  </section> 
	<section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">List of User CPA Details <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3>
      </div>
    <div class="box-body">  
      <form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>view_all_users" method="post">
        <div class="row">        
          <div class="col-md-12">            
            <div class="pull-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>
      </form>
	   <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
				<th>User Id</th>
				<th>Name</th>
				<th>Email</th>
				<th>product id</th>
				<th>Total Price</th>
            </tr>
          </thead>
          <tbody>
          <?php if(!empty($top_5_buyers)) { foreach($top_5_buyers as $val){ ?>
            <tr>
				<td><a href="<?php echo base_url(); ?>websiteusers/user_view/<?php echo $val['user_id']; ?>" target="_blank"><?php echo $val['user_id']; ?></a></td>
				<td><?php echo $val['name']; ?></td>  
				<td><?php echo $val['email']; ?></td>
				<td><a href="<?php echo base_url(); ?>product_new/product_view/<?php echo $val['product_id']; ?>" target="_blank"><?php echo $val['product_id']; ?></a></td>
				<td>&#8377;<?php echo $val['total_price']; ?></td>
            </tr>
          <?php } } ?>
          </tbody>
        </table>
      </div>
       <div class="pull-right">
          <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
  </div>
</div>
</section>