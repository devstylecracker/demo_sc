
<?php
$users_menu=$this->data['users_menu'];

//echo $shoppingclicks;
//print_r($signup);
//print_r($dashboard);exit;
//echo "Auclick==".$auclick;exit;

?>
<script src="<?php echo base_url(); ?>assets/js/dbreport.js"></script>
  <?php if($this->session->userdata('role_id')!=6){
      ?>
<section class="content-header">
  <h1>Universal Cart</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Dashboard</a></li>
  </ol>
</section>

<section class="content">
  
  <form name="myform" id="myform" class="frm-search" method="post">
    <div class="row">
      <div class="col-md-2">
        <div class="input-group">
          <input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm" id="date_from" value="<?php echo set_value('date_from'); ?>">
        </div>
      </div>
      <div class="col-md-2">
        <div class="input-group">
          <input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="<?php echo set_value('date_to'); ?>">
          <div class="input-group-btn">
            <button type="submit" class="btn btn-sm btn-default" id="submit"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </div>
      <div class="col-md-2">
        <a class="btn btn-default btn-sm" href="<?php echo base_url(); ?>home"><i class="fa fa-refresh"></i> Reset</a>
      </div>
    </div>
  </form>

  <br/>
  <div class="row">

      <div class="col-md-4">
        <div class="box box-report">
          <div class="box-header with-border">
            <h3 class="box-title">Product Click Information <small>(default today)</small></h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <a href="<?php echo base_url(); ?>Ucartreports" target="_blank"><div class="chart-responsive">
                  <canvas id="pieChart" height="150"></canvas>
                </div></a>
              </div>

            </div>
          </div>
          <div class="box-footer no-padding">
            <ul class="nav-report">
             
              <li>
                <i class="fa fa-circle text-light-blue"></i>
                Product Click - <a href="<?php echo base_url(); ?>Ucartreports/product_click/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank">
                  <span class="label"><?php echo $product_click_count; ?> </a>
                  
              </li>

              <li>
                <i class="fa fa-circle text-green"></i>
                Notify me - <a href="<?php echo base_url(); ?>Ucartreports/notify_me/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank">
                <span class="label"><?php echo $notify_count; ?></span></a>
                       
              </li>

               <li>
                <i class="fa fa-circle text-yellow"></i>
                Cart - <a href="<?php echo base_url(); ?>Ucartreports/cart_count/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank">
                <span class="label"><?php echo $cart_count; ?></span></a>
                       
              </li>
                 
                 
                                <li class="title">Total - <span class="label"><?php echo $product_click_count+$notify_count+$cart_count; ?></span></li>
                                
                              </ul>
                            </div>

                          </div>
                        </div>

                       <div class="col-md-4">
        <div class="box box-report">
          <div class="box-header with-border">
            <h3 class="box-title">Cart Info <small>(default today)</small></h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <a href="<?php echo base_url(); ?>reports/top_5" target="_blank"><div class="chart-responsive">
                  <canvas id="pieChart1" height="150"></canvas>
                </div></a>
              </div>

            </div>
          </div>
          <div class="box-footer no-padding">
            <ul class="nav-report">
             
              <li>
                <i class="fa fa-circle text-light-blue"></i>
                Actual sale Product info - <a href="<?php echo base_url(); ?>Ucartreports/product_sales/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank">
                  <span class="label"><?php echo $product_sale_info; ?> </a>
                  
              </li>

              <li>
                <i class="fa fa-circle text-green"></i>
                Abandoned Cart - <a href="<?php echo base_url(); ?>Ucartreports/abandoned_cart/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank">
                <span class="label"><?php echo $abendant_cart; ?></span></a>
                       
              </li>
              <li>
                             <i class="fa fa-circle text-yellow"></i>
                Removed from Cart - <a href="<?php echo base_url(); ?>Ucartreports/remove_from_cart/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank">
                <span class="label"><?php echo $remove_from_cart; ?></span></a>
              </li>
                                <li class="title">Total - <span class="label"><?php echo $product_sale_info+$abendant_cart+$remove_from_cart; ?></span></li>
                                
                              </ul>
                            </div>

                          </div>
                        </div>


      <div class="col-md-4">
        <div class="box box-report">
          <div class="box-header with-border">
            <h3 class="box-title">Cart Platform Info <small>(default today)</small></h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="chart-responsive">
                  <canvas id="pieChart2" height="150"></canvas>
                </div>
              </div>

            </div>
          </div>
          <div class="box-footer no-padding">
            <ul class="nav-report">
              


              <li>
                <i class="fa fa-circle text-light-blue" style="color:#990099 !important"></i>
                App Android - 
                  <a target="_blank" href="<?php echo base_url() ?>Ucartreports/platform_info_details/mobile_android/<?php echo $date_from.'/'.$date_to ?>"><span class="label"><?php echo $platform_count['mobile_android']; ?> </span></a>
              </li>

              <li>
                <i class="fa fa-circle text-green" style="color:#dd4b39 !important"></i>
                App iOS - 
                <a target="_blank" href="<?php echo base_url() ?>Ucartreports/platform_info_details/mobile_ios/<?php echo $date_from.'/'.$date_to ?>">
                  <span class="label"><?php echo $platform_count['mobile_ios']; ?></span>
                </a>
              </li>

              <li>
                <i class="fa fa-circle text-green" style="color:#f39c12 !important"></i>
                Desktop Windows - 
                <a target="_blank" href="<?php echo base_url() ?>Ucartreports/platform_info_details/Windows/<?php echo $date_from.'/'.$date_to ?>">
                  <span class="label"><?php echo $platform_count['desktop_window']; ?></span>
                </a>
              </li>

              <li>
                <i class="fa fa-circle text-green" style="color:#3266CC !important"></i>
                Desktop Linux - 
                <a target="_blank" href="<?php echo base_url() ?>Ucartreports/platform_info_details/desktop_linux/<?php echo $date_from.'/'.$date_to ?>">
                <span class="label"><?php echo $platform_count['desktop_linux']; ?></span>
                </a>
              </li>

    <!--   {value: '.$platform_count['mobile_android'].',color: "#990099",highlight: "#990099",label: "App Android"},
      {value: '.$platform_count['mobile_ios'].',color: "#dd4b39",highlight: "#dd4b39",label: "App iOS"},
      {value: '.$platform_count['desktop_window'].',color: "#f39c12",highlight: "#f39c12",label: "Desktop Window"},
      {value: '.$platform_count['desktop_linux'].',color: "#3266CC",highlight: "#3266CC",label: "Desktop Linux"},

      {value: '.$platform_count['desktop_mac'].',color: "#3C8DBC",highlight: "#3C8DBC",label: "Desktop Mac"},
      {value: '.$platform_count['web_ios'].',color: "#00a65a",highlight: "#00a65a",label: "Web iOS"},
      {value: '.$platform_count['web_android'].',color: "#00c0ef",highlight: "#00c0ef",label: "Web Android"},
      {value: '.$platform_count['others'].',color: "#000000",highlight: "#000000",label: "Others"}]';
     / -->
              <li>
                <i class="fa fa-circle text-green" style="color:#3C8DBC !important"></i>
                Desktop Mac - 
                <a target="_blank" href="<?php echo base_url() ?>Ucartreports/platform_info_details/desktop_mac/<?php echo $date_from.'/'.$date_to ?>">
                  <span class="label"><?php echo $platform_count['desktop_mac']; ?></span>
                </a>
              </li> 

              <li>
                <i class="fa fa-circle text-green" style="color:#00a65a !important"></i>
                Web iOS - 
                <a target="_blank" href="<?php echo base_url() ?>Ucartreports/platform_info_details/web_ios/<?php echo $date_from.'/'.$date_to ?>">
                <span class="label"><?php echo $platform_count['web_ios']; ?></span>
                </a>
              </li>

              <li>
                <i class="fa fa-circle text-green" style="color:#00c0ef !important"></i>
                Web Android - 
                <a target="_blank" href="<?php echo base_url() ?>Ucartreports/platform_info_details/web_android/<?php echo $date_from.'/'.$date_to ?>">
                  <span class="label"><?php echo $platform_count['web_android']; ?></span>
                </a>
              </li>

              <li>
                <i class="fa fa-circle text-green" style="color:#000000 !important"></i>
                Others - 
                <a target="_blank" href="<?php echo base_url() ?>Ucartreports/platform_info_details/others/<?php echo $date_from.'/'.$date_to ?>">
                  <span class="label"><?php echo $platform_count['others']; ?></span>
                </a>
              </li>
          
              <li>
                <strong>Total - </strong>
                <a target="_blank" href="<?php echo base_url() ?>Ucartreports/platform_info_details/total/<?php echo $date_from.'/'.$date_to ?>">
                  <span class="label"><?php echo $platform_count['total']; ?></span>
                </a>
              </li>


                    
              </ul>
            </div>

          </div>
      </div>
                              
                          <?php } ?>
                      </div>
                
                   
                </section>

                            <!-- ChartJS 1.0.1 -->
                            <script src="<?php echo base_url(); ?>assets/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
                            <script type="text/javascript">
                            $(function () {
                              $("#date_from").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true});
                              $("#date_to").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true });
                              users_click_info();
                              platform_piechart();
                              users_click_au();
                              $(".nav-tabs a").click(function(){
                                $(this).tab('show');
                              });
                              $('.nav-tabs a').on('shown.bs.tab', function(event){
                                var x = $(event.target).text();         // active tab
                                var y = $(event.relatedTarget).text();  // previous tab
                                $(".act span").text(x);
                                $(".prev span").text(y);
                              });
                            });

                            function users_click_info(){
                              //-------------
                              //- PIE CHART -
                              //-------------
                              // Get context with jQuery - using jQuery's .get() method.
                              $.ajax({
                                url: '<?php echo base_url(); ?>reports/get_users_info',
                                type: 'post',
                                data: { 'type' : 'get_click_data' },
                                success: function(data, status) {
                                  var pieChartCanvas = $("#pieChart").get(0).getContext("2d");

                                  var pieChart = new Chart(pieChartCanvas);
                                  var PieData = <?php echo $click; ?>;
                                  var pieOptions = {
                                    //Boolean - Whether we should show a stroke on each segment
                                    segmentShowStroke: true,
                                    //String - The colour of each segment stroke
                                    segmentStrokeColor: "#fff",
                                    //Number - The width of each segment stroke
                                    segmentStrokeWidth: 0.0001,
                                    //Number - The percentage of the chart that we cut out of the middle
                                    percentageInnerCutout: 0, // This is 0 for Pie charts
                                    //Number - Amount of animation steps
                                    animationSteps: 100,
                                    //String - Animation easing effect
                                    animationEasing: "easeOutBounce",
                                    //Boolean - Whether we animate the rotation of the Doughnut
                                    animateRotate: true,
                                    //Boolean - Whether we animate scaling the Doughnut from the centre
                                    animateScale: false,
                                    //Boolean - whether to make the chart responsive to window resizing
                                    responsive: true,
                                    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                                    maintainAspectRatio: false,
                                    //String - A legend template
                                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
                                    //String - A tooltip template
                                    tooltipTemplate: "<%=value %> <%=label%> Clicks"
                                  };
                                  //Create pie or douhnut chart
                                  // You can switch between pie and douhnut using the method below.
                                  pieChart.Doughnut(PieData, pieOptions);
                                  //-----------------
                                  //- END PIE CHART -
                                  //-----------------
                                },
                                error: function(xhr, desc, err) {
                                  console.log(err);
                                }
                              });
                            }

                            function users_click_au(){
                              $.ajax({
                                url: '<?php echo base_url(); ?>reports/get_users_info',
                                type: 'post',
                                data: { 'type' : 'get_click_data' },
                                success: function(data, status) {
                                  var pieChartCanvas = $("#pieChart1").get(0).getContext("2d");

                                  var pieChart = new Chart(pieChartCanvas);
                                  var PieData = <?php echo $cart_info; ?>;
                                  var pieOptions = {
                                    //Boolean - Whether we should show a stroke on each segment
                                    segmentShowStroke: true,
                                    //String - The colour of each segment stroke
                                    segmentStrokeColor: "#fff",
                                    //Number - The width of each segment stroke
                                    segmentStrokeWidth: 0.0001,
                                    //Number - The percentage of the chart that we cut out of the middle
                                    percentageInnerCutout: 0, // This is 0 for Pie charts
                                    //Number - Amount of animation steps
                                    animationSteps: 100,
                                    //String - Animation easing effect
                                    animationEasing: "easeOutBounce",
                                    //Boolean - Whether we animate the rotation of the Doughnut
                                    animateRotate: true,
                                    //Boolean - Whether we animate scaling the Doughnut from the centre
                                    animateScale: false,
                                    //Boolean - whether to make the chart responsive to window resizing
                                    responsive: true,
                                    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                                    maintainAspectRatio: false,
                                    //String - A legend template
                                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
                                    //String - A tooltip template
                                    tooltipTemplate: "<%=value %> <%=label%> Clicks"
                                  };
                                  //Create pie or douhnut chart
                                  // You can switch between pie and douhnut using the method below.
                                  pieChart.Doughnut(PieData, pieOptions);
                                  //-----------------
                                  //- END PIE CHART -
                                  //-----------------
                                },
                                error: function(xhr, desc, err) {
                                  console.log(err);
                                }
                              });
                            }

                            function platform_piechart(){
                              $.ajax({
                                url: '<?php echo base_url(); ?>reports/get_users_info',
                                type: 'post',
                                data: { 'type' : 'get_click_data' },
                                success: function(data, status) {
                                  var pieChartCanvas = $("#pieChart2").get(0).getContext("2d");

                                  var pieChart = new Chart(pieChartCanvas);
                                  var PieData = <?php echo $cart_info_platform; ?>;
                                  var pieOptions = {
                                    //Boolean - Whether we should show a stroke on each segment
                                    segmentShowStroke: true,
                                    //String - The colour of each segment stroke
                                    segmentStrokeColor: "#fff",
                                    //Number - The width of each segment stroke
                                    segmentStrokeWidth: 0.0001,
                                    //Number - The percentage of the chart that we cut out of the middle
                                    percentageInnerCutout: 0, // This is 0 for Pie charts
                                    //Number - Amount of animation steps
                                    animationSteps: 100,
                                    //String - Animation easing effect
                                    animationEasing: "easeOutBounce",
                                    //Boolean - Whether we animate the rotation of the Doughnut
                                    animateRotate: true,
                                    //Boolean - Whether we animate scaling the Doughnut from the centre
                                    animateScale: false,
                                    //Boolean - whether to make the chart responsive to window resizing
                                    responsive: true,
                                    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                                    maintainAspectRatio: false,
                                    //String - A legend template
                                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
                                    //String - A tooltip template
                                    tooltipTemplate: "<%=value %> <%=label%> Clicks"
                                  };
                                  //Create pie or douhnut chart
                                  // You can switch between pie and douhnut using the method below.
                                  pieChart.Doughnut(PieData, pieOptions);
                                  //-----------------
                                  //- END PIE CHART -
                                  //-----------------
                                },
                                error: function(xhr, desc, err) {
                                  console.log(err);
                                }
                              });
                            }

                         
                            </script>
