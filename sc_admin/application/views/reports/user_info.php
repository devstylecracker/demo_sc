<?php
$users_menu=$this->data['users_menu'];
?>
<section class="content-header">
        <h1>Users</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>
            <li class="active">User Details</li>
          </ol>
</section>
<section class="content">
  <form name="myform" id="myform" class="frm-search" method="post">
    <div class="row">
      <div class="col-md-2">
        <div class="input-group">
          <input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm" id="date_from" value="<?php echo set_value('date_from'); ?>">

        </div>
      </div>
      <div class="col-md-2">
        <div class="input-group">
          <input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="<?php echo set_value('date_to'); ?>">

          <div class="input-group-btn">
            <button type="submit" class="btn btn-sm btn-default" id="submit"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </div>
      <div class="col-md-2">
        <a class="btn btn-default btn-sm" href="<?php echo base_url(); ?>/reports/user_info"><i class="fa fa-refresh"></i> Reset</a>
      </div>
    </div>
  </form>
  <br/>
 <div class="row">
          <div class="col-md-8">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                      <li class="active"><a href="#home">Most Returning</a></li>
                      <li><a href="#menu1">Least Returning</a></li>
                      <li><a href="#menu2">Most Sharing</a></li>
                      <li><a href="#menu3">Most Favouriteing</a></li>
                      <li><a href="#menu4">Most Wardrobing</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane fade in active" id="home">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>User Id</th>
                          <th>Username</th>
                          <th>Email</th>
                          <th>Returning Times</th>
                          <th>Last login time</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(!empty($most_returning_users)) { foreach($most_returning_users as $val){ ?>
                        <tr>
                          <td><a href="<?php echo base_url(); ?>websiteusers/user_view/<?php echo $val['user_id']; ?>" target="_blank"><?php echo $val['user_id']; ?></a></td>
                          <td><?php echo $val['user_name']; ?></td>
                          <td><?php echo $val['email']; ?></td>
                          <td><?php echo $val['count']; ?></td>
                          <td><?php echo $val['last_visit_time']; ?></td>
                       <!--   <td><?php //echo $val->created_datetime;
                         $date = new DateTime($val->last_visit_time);
                         echo $date->format('d-m-Y H:i:s');
                ?></td> -->
                        </tr>
                      <?php } } ?>
                      </tbody>
                    </table>
                  </div>
                <div class="box-footer">
                  <a href="<?php echo base_url(); ?>View_all_users" class="btn btn-sm btn-primary pull-right" target="_blank" >View All Most Returning Users</a>
                </div>
              </div>
                  <div class="tab-pane fade" id="menu1">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>User Id</th>
                          <th>Username</th>
                          <th>Email</th>
                          <th>Last login time</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(!empty($most_least_runing)) { foreach($most_least_runing as $val){ ?>
                        <tr>
                          <td><a href="<?php echo base_url(); ?>websiteusers/user_view/<?php echo $val['user_id']; ?>" target="_blank"><?php echo $val['user_id']; ?></a></td>
                          <td><?php echo $val['user_name']; ?></td>
                          <td><?php echo $val['email']; ?></td>
                          <td><?php echo $val['last_visit_time']; ?></td>
                        </tr>
                      <?php } } ?>
                      </tbody>
                    </table>
                  </div>
                <div class="box-footer">
                  <a href="<?php echo base_url(); ?>Least_returning" class="btn btn-sm btn-primary pull-right" target="_blank" >View All Least Returning Users</a>
                </div>
              </div>

                <div class="tab-pane fade" id="menu2">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>User Id</th>
                          <th>Username</th>
                          <th>Email</th>
                          <th>Sharing Count</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(!empty($most_sharing)) { foreach($most_sharing as $val){ ?>
                        <tr>
                          <td><a href="<?php echo base_url(); ?>websiteusers/user_view/<?php echo $val['user_id']; ?>" target="_blank"><?php echo $val['user_id']; ?></a></td>
                          <td><?php echo $val['user_name']; ?></td>
                          <td><?php echo $val['email']; ?></td>
                          <td><?php echo $val['count']; ?></td>


                        </tr>
                      <?php } } ?>
                      </tbody>
                    </table>
                  </div>
                <div class="box-footer">
                  <a href="<?php echo base_url(); ?>Most_sharing" class="btn btn-sm btn-primary pull-right" target="_blank" >View All Most Sharing Users</a>
                </div>
              </div>


              <div class="tab-pane fade" id="menu3">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>User Id</th>
                          <th>Username</th>
                          <th>Email</th>
                          <th>Count</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(!empty($most_fav)) { foreach($most_fav as $val){ ?>
                        <tr>
                          <td><a href="<?php echo base_url(); ?>websiteusers/user_view/<?php echo $val['user_id']; ?>" target="_blank"><?php echo $val['user_id']; ?></a></td>
                          <td><?php echo $val['user_name']; ?></td>
                          <td><?php echo $val['email']; ?></td>
                          <td><?php echo $val['count']; ?></td>
                        </tr>
                      <?php } } ?>
                      </tbody>
                    </table>
                  </div>

                <div class="box-footer">
                  <a href="<?php echo base_url(); ?>Most_fav" class="btn btn-sm btn-primary pull-right" target="_blank" >View All Most Favouriting Users</a>
                </div>
              </div>

              <div class="tab-pane fade" id="menu4">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>User Id</th>
                          <th>Username</th>
                          <th>Email</th>
                          <th>Wardrobing Times</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(!empty($most_wardrobe)) { foreach($most_wardrobe as $val){ ?>
                        <tr>
                          <td><a href="<?php echo base_url(); ?>websiteusers/user_view/<?php echo $val['user_id']; ?>" target="_blank"><?php echo $val['user_id']; ?></a></td>
                          <td><?php echo $val['user_name']; ?></td>
                          <td><?php echo $val['email']; ?></td>
                          <td><?php echo $val['count']; ?></td>
                        </tr>
                      <?php } } ?>
                      </tbody>
                    </table>
                  </div>
                <div class="box-footer">
                  <a href="<?php echo base_url(); ?>Most_wardrobe" class="btn btn-sm btn-primary pull-right" target="_blank" >View All Most Wardrobing Users</a>
                </div>
              </div>

              </div>

            </div>
          </div>
          <div class="col-md-4">
            <div class="box box-report">
              <div class="box-header with-border">
                <h3 class="box-title">User Signup </h3>
              </div>
              <div class="box-body">

                    <a href="<?php echo base_url(); ?>websiteusers" target="_blank"><div class="chart-responsive">
                      <canvas id="pieChart" height="150"></canvas>
                    </div></a>
                  </div>
                  <div class="box-footer no-padding">
                    <ul class="nav-report">
                      <li><i class="fa fa-circle text-light-blue"></i> Website 
                      <a href="<?php echo base_url();?>reports/user_details/website/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class=" pull-right"> 
                      <span class="pull-right label"><?php echo $website; ?></span>
                      </a></li>

                      <li><i class="fa fa-circle text-green"></i> Facebook  
                      <a href="<?php echo base_url();?>reports/user_details/facebook/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class=" pull-right">
                      <span class="pull-right label"><?php echo $facebook; ?></span>
                      </a></li>

                      <li><i class="fa fa-circle text-yellow"></i> Google  
                       <a href="<?php echo base_url();?>reports/user_details/google/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class=" pull-right">
                      <span class="pull-right label"><?php echo $google; ?></span>
                      </a></li>

                      <li><i class="fa fa-circle text-aqua"></i> Mobile 
                      <a href="<?php echo base_url();?>reports/user_details/mobile/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="pull-right"> 
                      <span class="pull-right label"><?php echo $mobile; ?></span>
                      </a></li>

                      <li><i class="fa fa-circle text-red"></i> Mobile Facebook  <a href="<?php echo base_url();?>reports/user_details/mobile_facebook/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="pull-right">
                     <span class="label"><?php echo $mobile_facebook; ?></span>
                     </a></li>

                     <!--<li><i class="fa fa-circle text-purple"></i> Mobile Google <a href="<?php echo base_url();?>reports/user_details/mobile_google/<?php echo set_value('date_from')!=''? set_value('date_from') : '0'; ?>/<?php echo set_value('date_to')!=''? set_value('date_to') : '0'; ?>" target="_blank" class="pull-right">
                     <span class="label"><?php echo $mobile_google; ?></span>
                     </a></li> -->


                    <li><b>Total Users </b><span class="pull-right label"><?php echo $website+$facebook+$gmail+$mobile+$mobile_facebook+$mobile_google;?></span></li>

                    </ul>
                    </div>
                </div>
              </div>
            </div>
        </div>
        </div>
</section>

<!-- ChartJS 1.0.1 -->
<script src="<?php echo base_url(); ?>assets/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function () {
  $( "#date_from" ).datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true });
  $( "#date_to" ).datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true });
  users_signup_info();
  $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });
    $('.nav-tabs a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
    });
});

function users_signup_info(){
  //-------------
  //- PIE CHART -
  //-------------
  // Get context with jQuery - using jQuery's .get() method.
  $.ajax({
      url: '<?php echo base_url(); ?>reports/get_users_info',
      type: 'post',
      data: { 'type' : 'get_variation' },
      success: function(data, status) {
            var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
            var pieChart = new Chart(pieChartCanvas);
            var PieData = <?php echo $signup; ?>;
            var pieOptions = {
              //Boolean - Whether we should show a stroke on each segment
              segmentShowStroke: true,
              //String - The colour of each segment stroke
              segmentStrokeColor: "#fff",
              //Number - The width of each segment stroke
              segmentStrokeWidth: 0.0001,
              //Number - The percentage of the chart that we cut out of the middle
              percentageInnerCutout: 0, // This is 0 for Pie charts
              //Number - Amount of animation steps
              animationSteps: 100,
              //String - Animation easing effect
              animationEasing: "easeOutBounce",
              //Boolean - Whether we animate the rotation of the Doughnut
              animateRotate: true,
              //Boolean - Whether we animate scaling the Doughnut from the centre
              animateScale: false,
              //Boolean - whether to make the chart responsive to window resizing
              responsive: true,
              // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
              maintainAspectRatio: false,
              //String - A legend template
              legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
              //String - A tooltip template
              tooltipTemplate: "<%=value %> <%=label%> users"
            };
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            pieChart.Doughnut(PieData, pieOptions);
            //-----------------
            //- END PIE CHART -
            //-----------------
      },
      error: function(xhr, desc, err) {
        console.log(err);
      }
    });


}
</script>
