<?php
/*print_r($d_report);
exit;*/
?>
<?php

function makecomma($input)
{
    // This function is written by some anonymous person - I got it from Google
    if(strlen($input)<=2)
    { return $input; }
    $length=substr($input,0,strlen($input)-2);
    $formatted_input = makecomma($length).",".substr($input,-2);
    return $formatted_input;
}

function formatInIndianStyle($num){
    // This is my function
    $pos = strpos((string)$num, ".");
    if ($pos === false) { $decimalpart="00";}
    else { $decimalpart= substr($num, $pos+1, 2); $num = substr($num,0,$pos); }

    if(strlen($num)>3 & strlen($num) <= 12){
                $last3digits = substr($num, -3 );
                $numexceptlastdigits = substr($num, 0, -3 );
                $formatted = makecomma($numexceptlastdigits);
                $stringtoreturn = $formatted.",".$last3digits.".".$decimalpart ;
    }elseif(strlen($num)<=3){
                $stringtoreturn = $num.".".$decimalpart ;
    }elseif(strlen($num)>12){
                $stringtoreturn = number_format($num, 2);
    }

    if(substr($stringtoreturn,0,2)=="-,"){$stringtoreturn = "-".substr($stringtoreturn,2 );}

    return $stringtoreturn;
}

//$num = 98765432198;
//echo  formatInIndianStyle($num);

?>




<section class="content-header">
  <h1>Daily Report <small class="label label-info"><?php echo @$total_rows; ?></small></h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">Daily Report</li>
  </ol>
</section>

<section class="content">
  <form name="myform" id="myform" class="frm-search" method="post">
    <div class="row">
      <div class="col-md-2">
        <div class="input-group">
          <input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm" id="date_from" value="<?php echo set_value('date_from'); ?>">

        </div>
      </div>
      <div class="col-md-2">
        <div class="input-group">
          <input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="<?php echo set_value('date_to'); ?>">

          <div class="input-group-btn">
            <button type="submit" class="btn btn-sm btn-default" id="submit"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </div>
      <div class="col-md-2">
        <a class="btn btn-default btn-sm" href="<?php echo base_url(); ?>/Dailyreports/current_report"><i class="fa fa-refresh"></i> Reset</a>
      </div>
    </div>
  </form>
  <br/>
<div class="box-body">  
      <form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>view_all_users" method="post">
        <div class="row">        
          <div class="col-md-12">            
            <div class="pull-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>
      </form>
  <div class="box">
    <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th>Sr. No.</th>
              <th v-align="">Company Name</th>
              <th>No. of Products Uploaded <br/> (Till Date)</th>
              <th>No of products used (Till Date)</th>
              <th>No of products used (As per Date)</th>
              <th>Total Value of products used <br/> (As per Date)</th>
              <th>How many Buy Now clicks <br/> (As per Date)</th>
              <th>How many closed transactions <br/> (actually paying)</th>
              <th>How many per product price <br/> (As per Date)</th>
            </tr>
          </thead>
          <tbody>
            <?php if(!empty($d_report)) {              
              $i=1;
              $sum_product = 0;
              $sum_product_date = 0;
              $sum_product_CPA = 0;
              $sum_total_products_used_current_date = 0;
              $sum_buy_total_value_products = 0;              
              $sum_product_buy_now_clicks = 0;
              foreach($d_report as $val){ 
                ?>
                <tr>

                  <td><?php echo $i; ?></td>
                  <td><?php echo $val['brand_name']; ?></td>
                  <td><?php echo $val['no_products_uploaded']; ?></td>
                  <td><?php echo $val['product_count_till_date']; ?></td>
                  <td><?php echo $val['total_products_used_current_date']; ?></td>
 <td><i class="fa fa-inr"></i>&nbsp;<?php if($val['buy_total_value_products'] != NULL){ echo  formatInIndianStyle($val['buy_total_value_products']);  } else { echo "0"; } ?></b></td>
              <!--<td><i class="fa fa-inr"></i>  <?php if($val['buy_total_value_products'] != NULL){ echo $val['buy_total_value_products']; } else { echo "0"; } ?></b></td> -->               
                  <!--<td>Rs. <?php echo $val['buy_total_value_products']; ?></td>-->
                  <td><?php echo $val['product_buy_now_clicks']; ?></td>
                  <td><?php echo $val['product_CPA']; ?></td>
 <td><i class="fa fa-inr"></i>&nbsp; <?php if($val['sc_product_per_price'] != 0){ echo formatInIndianStyle($val['sc_product_per_price']); } else { echo "0"; }?></td>
                </tr>

                <?php
                $sum_product = $sum_product + $val['no_products_uploaded'];
                $sum_product_date = $sum_product_date + $val['product_count_till_date'];
                $sum_total_products_used_current_date = $sum_total_products_used_current_date + $val['total_products_used_current_date'];
                $sum_buy_total_value_products = $sum_buy_total_value_products + $val['buy_total_value_products'];
                $sum_product_buy_now_clicks = $sum_product_buy_now_clicks + $val['product_buy_now_clicks'];
                $sum_product_CPA = $sum_product_CPA + $val['product_CPA'];
                $sum_product_per_price = $sum_product_per_price + $val['sc_product_per_price'];                
                $i++;}
                ?> 
                <tr>

                  <td></td>
                  <td><b>Total : </b></td>
                  <td><b><?php echo $sum_product; ?></b></td>
                  <td><b><?php echo $sum_product_date; ?></b></td>
                  <td><b><?php echo $sum_total_products_used_current_date; ?></b></td>                  
                  <!--<td><b><i class="fa fa-inr"></i> <span class="label"><i class="fa fa-inr"></i> <?php echo $sum_buy_total_value_products; ?></span> </b></td>-->                  
                  <td><b><i class="fa fa-inr"></i>&nbsp;  <?php echo  formatInIndianStyle($sum_buy_total_value_products);?></b></td>
                  <td><b><?php echo $sum_product_buy_now_clicks; ?></b></td>
                  <td><b><?php echo $sum_product_CPA; ?></b></td> 
                  <td><b><i class="fa fa-inr"></i>&nbsp;<?php echo formatInIndianStyle($sum_product_per_price); ?></b></td> 
                </tr>
                <?php
              }else
                {
                  echo '<tr><td colspan="6">No Record Found</td></tr>';
                } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>


    </div>

  </div>
</div>

</div>
</section>
<style>
.table>thead>tr>th{
vertical-align:top;
}
</style>
<!-- ChartJS 1.0.1 -->
<script src="<?php echo base_url(); ?>assets/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function () {

  $( "#date_from" ).datepicker({ format: "yyyy-mm-dd" });
  $( "#date_to" ).datepicker({ format: "yyyy-mm-dd" });
  product_info();
  $(".nav-tabs a").click(function(){
    $(this).tab('show');
  });
  $('.nav-tabs a').on('shown.bs.tab', function(event){
    var x = $(event.target).text();         // active tab
    var y = $(event.relatedTarget).text();  // previous tab
    $(".act span").text(x);
    $(".prev span").text(y);
  });
});

function product_info(){
  //-------------
  //- PIE CHART -
  //-------------
  // Get context with jQuery - using jQuery's .get() method.
  /* $.ajax({
  url: '<?php echo base_url(); ?>reports/get_brand_product',
  type: 'post',
  data: { 'type' : 'get_brand_product' },
  success: function(data, status) {
  var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
  var pieChart = new Chart(pieChartCanvas);
  var PieData = <?php echo $signup; ?>;
  var pieOptions = {
  //Boolean - Whether we should show a stroke on each segment
  segmentShowStroke: true,
  //String - The colour of each segment stroke
  segmentStrokeColor: "#fff",
  //Number - The width of each segment stroke
  segmentStrokeWidth: 1,
  //Number - The percentage of the chart that we cut out of the middle
  percentageInnerCutout: 50, // This is 0 for Pie charts
  //Number - Amount of animation steps
  animationSteps: 100,
  //String - Animation easing effect
  animationEasing: "easeOutBounce",
  //Boolean - Whether we animate the rotation of the Doughnut
  animateRotate: true,
  //Boolean - Whether we animate scaling the Doughnut from the centre
  animateScale: false,
  //Boolean - whether to make the chart responsive to window resizing
  responsive: true,
  // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
  maintainAspectRatio: false,
  //String - A legend template
  legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
  //String - A tooltip template
  tooltipTemplate: "<%=value %> <%=label%> users"
};
//Create pie or douhnut chart
// You can switch between pie and douhnut using the method below.
pieChart.Doughnut(PieData, pieOptions);
//-----------------
//- END PIE CHART -
//-----------------
},
error: function(xhr, desc, err) {
console.log(err);
}
});
*/

}
</script>