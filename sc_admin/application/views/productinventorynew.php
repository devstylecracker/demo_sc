<?php
  if(!empty($this->data['users_menu']!=""))
    {
       $users_menu=$this->data['users_menu'];

    }else
    {
       $users_menu=array();
    }
?>
<section class="content-header">
  <h1>Product Inventory</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">Product Inventory</li>
  </ol>
</section>
<section class="content">
  <div class="box">
  <div class="box-header with-border">
      <h3 class="box-title">List of Products <small class="label label-info" id="total_count"><?php echo @$total_rows; ?></small></h3>
      <div class="pull-right">
	   <a class="small-box-footer" target="_blank" id ="inventory_download" onclick="download_inventory_excel(1);" >
         <button type="button" class="btn btn-primary btn-sm">  Download Inventory Report <i class="fa fa-download"></i></button>
	   </a>
					
        <?php if(in_array("60",@$users_menu))
        {
        ?>
         <a href="<?php echo base_url(); ?>Productinventory_upload"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Upload inventory report</button></a>
        <?php
        }
        ?>
      </div>
 </div>
    <div class="box-body">
	  <div class="frmsearch form-group">
        <div class="row">
            <div class="col-md-2">
              <select class="form-control input-sm" id="search_by" name="search_by" onchange="show_products(1);">
                  <option value="">Select Store</option>
                    <?php foreach($paid_store as $val)
                          {
                    ?>
                      <option value="<?php echo $val['brand_id']; ?>" <?php echo @$store_id==$val['brand_id'] ? 'selected' : ''; ?> ><?php echo $val['company_name']; ?></option>
                  <?php } ?>
              </select>
            </div>
            <div class="col-md-2">             
                <input type="text" placeholder="Product Id" class="form-control input-sm" id="table_search" value="" name="table_search">               
            
            </div>
            <div class="col-md-2">
              <div class="input-group">
                <input type="text" placeholder="Product Sku" class="form-control input-sm pull-right" id="table_search1" value="" name="table_search1">
                <div class="input-group-btn">
                  <button type="button" class="btn btn-sm btn-default" onclick="show_products(1);"><i class="fa fa-search"></i></button>
                </div>
              </div>             
            </div>
            <div class="col-md-3">
              <div class="input-group">                
                  <!-- <span class="info-box-text">Download Product Inventory</span>  -->
               
                 
              </div>
            </div>           
        </div>
    </div>

  <table class="table table-bordered table-striped table-enventory"><thead><tr><th>Sr no.</th><th>Product ID</th><th>Product Name</th><th>Action</th><th>Save</th></tr></thead><tbody id="show_product_list">

  </tbody></table>
  <input type="hidden" name="min_offset" id="min_offset" value="0">

</div>
</div>
</section>

<script type="text/Javascript">
$( document ).ready(function() {
  $('.nav-item-inventory').addClass("active");
  $('.nav-product-inventory').addClass("active");

  $('#load_more').click(function(){
    load_more();

  });
});


function show_products(attr){
    var store_id = $('#search_by').val();
    var product_id = $('#table_search').val();
    var product_sku = $('#table_search1').val();

    if($('#min_offset').length > 0){
    var min_offset = $('#min_offset').val(); }
    else{ var min_offset =0 };
    if($('#max_offset').length > 0){
    var max_offset = $('#max_offset').val(); }
    else{ var max_offset =0 };

    if(attr == 1){ min_offset =0; }

    if( store_id !='' || product_id !=''){
      $('.page-overlay').show(); $('.loader').show();
        $.ajax({
            url: "<?php echo base_url() ?>productinventory_new/get_products",
            type: 'POST',
            data: {'store_id':store_id,'product_id':product_id,'product_sku':product_sku,'min_offset':min_offset,'max_offset':max_offset},
            cache :true,
            async: false,
            success: function(response) {
              if(attr == 1){
                $('#show_product_list').html(response);
              }else{
                $('#show_product_list').append(response);
              }
              $('#min_offset').val(parseInt(min_offset)+1);

              $('#total_count').html($('#max_offset').val());
              $('.page-overlay').hide();
              $('.loader').hide();

            }
        });

      }
}

function load_more(){
      if($('#min_offset').length > 0){
        var min_offset = $('#min_offset').val(); }
      else{ var min_offset =0 };
      if($('#max_offset').length > 0){
        var max_offset = $('#max_offset').val(); }
      else{ var max_offset =0 };

      min_offset_val = parseInt(min_offset)-1;

      $('#remove'+min_offset_val).remove();
      show_products(0);
}

function addnew_row(id,product_id){
  var new_id = parseInt(id) + 1;
  var old_count = $('#row_count'+product_id).val();
  var new_count = parseInt($('#row_count'+product_id).val())+1;
  $('#product_info'+id).append('<div id="row_'+product_id+'_'+new_count+'" class="product-size-qty-wrp"><div class="input-group-label-wrp"><input type="text" name="product_size'+product_id+'_'+new_count+'" id="product_size'+product_id+'_'+new_count+'" placeholder="Size" class="form-control input-xs"></div><div class="input-group-label-wrp"><input pattern="[0-9]" type="text" name="product_qty'+product_id+'_'+new_count+'" id="product_qty'+product_id+'_'+new_count+'" placeholder="Qty" class="form-control input-xs"></div><div class="input-group-label-wrp"><input type="text" name="product_sku'+product_id+'_'+new_count+'" id="product_sku'+product_id+'_'+new_count+'" placeholder="Product SKU" class="form-control input-xs"></div><span onclick="remove_row('+new_count+','+product_id+');" class="btn btn-primary btn-xs btn-delete"><i class="fa fa-minus"></i></span></div>');
  $('#row_count'+product_id).val(new_count);
}

function remove_row(id,product_id){
  $('#row_'+product_id+'_'+id).remove();
}

function save_product_size(product_id){
  var row_count = $('#row_count'+product_id).val();
  var product_size = ''; var product_qty = '';
  var arrProductSizeAndCount = [];
  for(var i = 1;i<=row_count;i++){
    if(($('#product_size'+product_id+'_'+i).val() != null) && ($('#product_qty'+product_id+'_'+i).val() != null)){
      $('#product_size'+product_id+'_'+i).val();
      $('#product_qty'+product_id+'_'+i).val();
      $('#product_sku'+product_id+'_'+i).val();
      arrProductSizeAndCount.push([$('#product_size'+product_id+'_'+i).val(),$('#product_qty'+product_id+'_'+i).val(),$('#product_sku'+product_id+'_'+i).val()]);
    }
  }
  //console.log(arrProductSizeAndCount);
  var myJsonString = JSON.stringify(arrProductSizeAndCount);

  $.ajax({
            url: "<?php echo base_url() ?>productinventory_new/save_product_size",
            type: 'POST',
            data: {'product_id':product_id,'product_info':myJsonString},
            cache :true,
            async: false,
            success: function(response) {
                $('#message'+product_id).html('Save Successfully');
            },
            error: function(xhr) {

            }
  });
}

function download_inventory_excel(attr){
    var store_id = $('#search_by').val();
    var product_id = $('#table_search').val();
    var product_sku = $('#table_search1').val();

    if($('#min_offset').length > 0){
    var min_offset = $('#min_offset').val(); }
    else{ var min_offset =0 };
    if($('#max_offset').length > 0){
    var max_offset = $('#max_offset').val(); }
    else{ var max_offset =0 };

    if(attr == 1){ min_offset =0; }

    if( store_id !='' || product_id !=''){
      $('.page-overlay').show(); $('.loader').show();

        $.ajax({
            url: "<?php echo base_url() ?>productinventory_new/download_Inventory/"+store_id,
            type: 'POST',
            data: {'store_id':store_id,'product_id':product_id,'product_sku':product_sku,'min_offset':min_offset,'max_offset':max_offset},
            cache :true,
            async: false,
            success: function(response) {
             
              $('#inventory_download').attr('href',"<?php echo base_url(); ?>Productinventory_new/download_Inventory/"+store_id);
             
              $('.page-overlay').hide();
              $('.loader').hide();

            }
        });

      }
}


</script>
