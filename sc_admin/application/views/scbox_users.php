  <?php
  $users_menu=$this->data['users_menu'];
  $table_search = $table_search == '0' ? '' : $table_search;  
  ?>
  <section class="content-header">
          <h1>Manage Scbox Users</h1>
            <ol class="breadcrumb">
              <li><a href="<?php echo base_url(); ?>home">Home</a></li>
              <li class="active">Scbox Users</li>
            </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">List of Scbox Users <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3>
      <div class="pull-right">
      </div>
      </div><!-- /.box-header -->
    <div class="box-body">
      <?php echo $this->session->flashdata('feedback'); ?>

      <form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>websiteusers/scbox" method="post">
        <div class="row">
          <div class="col-md-2">
              <?php $search_by = set_value('search_by')=='' ? $search_by : set_value('search_by'); ?>
            <select name="search_by" id="search_by" class="form-control input-sm" >

              <option value="0">Search by</option>
              <!--<option selected="selected" value="1" <?php echo $search_by==1 ? 'selected' : ''; ?>>Username</option>-->
              <option selected="selected"  value="2" <?php echo $search_by==2 ? 'selected' : ''; ?>>Emailid</option>
             <!--<option value="3" <?php echo $search_by==3 ? 'selected' : ''; ?>>Date</option>-->
              <option value="4" <?php echo $search_by==4 ? 'selected' : ''; ?>>User ID</option>
               <option value="5" <?php echo $search_by==5 ? 'selected' : ''; ?>>First Name</option>
               <option value="6" <?php echo $search_by==6 ? 'selected' : ''; ?>>Last Name</option>

               <option value="9" <?php echo $search_by==9 ? 'selected' : ''; ?>>source</option>
               <option value="10" onclick="submitForm(10)" <?php echo $search_by==10 ? 'selected' : ''; ?>>Order Count asc</option>
			   <option value="11" onclick="submitForm(11)" <?php echo $search_by==11 ? 'selected' : ''; ?>>Order Count desc</option>
			   <option value="12" <?php echo $search_by==12 ? 'selected' : ''; ?>>Date Range</option>

            </select>
          </div>
          <div class="col-md-3">
            <div class="input-group">
			<div id ="filter" style="display: block;" >
				<input type="text" name="table_search" value="<?php echo set_value('table_search')=='' && set_value('table_search')==0 ? $table_search : set_value('table_search'); ?>" id="table_search" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo set_value('table_search'); ?>" required="" />
			</div>
				<div id ="daterange" style="display: none;">
				<div class="row">
				  <div class="col-md-5">
					<div class="form-group">
					  <input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm" id="date_from" value="<?php echo set_value('date_from'); ?>">
					</div>
				</div>
				<div class="col-md-7">
				  <div class="form-group">
					<input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="<?php echo set_value('date_to'); ?>">
				  </div>
				 </div>
				</div>
				</div>
                        
              <div class="input-group-btn">
                <button class="btn btn-sm btn-default" type="submit" onclick="submitForm(0);"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <a href="scbox" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
          </div>
          <div class="col-md-5">
			<button title="Download Excel" id="download_excel" class="btn btn-primary btn-sm"  onclick="download_scbox_user_data();"><i class="fa fa-file-excel-o"></i> Download Excel</button>
			<button title="Stylist Extracted Data" id="download_excel2" class="btn btn-primary btn-sm"  onclick="download_stylist_extracted_data();"><i class="fa fa-file-excel-o"></i> Stylist Extracted Data</button>
            <div class="pull-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>
      </form>

    <div class="table-responsive">
      <table class="table table-bordered table-striped dataTable" id="example2">
        <thead>
          <tr>
            <th>User ID</th>
            <!--<th>Username</th>-->
            <th>Email</th>          
            <th>First Name</th>
            <th>Last Name</th>
            <th>Mobile Number</th>
			<th>Total orders &nbsp;&nbsp;<i class="fa fa-sort" aria-hidden="true"></i></th>
            <th>Registered From</th>
			<th>Registration date</th>
            <th>Last visit time</th>
            <th>Action</th>           
            
          </tr>
        </thead>
        <tbody>
          <?php if(!empty($users_data)) { $i = 1;
            foreach($users_data as $val){
              ?>
              <tr>
                <td><?php echo $val->id; ?></td>
                <!--<td><?php echo $val->user_name; ?></td>-->
                <td><?php echo $val->email; ?></td>
                <td><?php echo $val->first_name; ?></td>
                <td><?php echo $val->last_name; ?></td>
                <td><?php echo $val->contact_no; ?></td>
				      <td><?php echo $val->order_count; ?></td>
                <td><?php echo $val->registered_from; ?></td>
				<td><?php $date = new DateTime($val->created_datetime);
                          echo $date->format('d-m-Y H:i:s');
				?></td>
				<td><?php //echo $val->created_datetime;
				  $date = new DateTime($val->modified_datetime);
				  echo $date->format('d-m-Y H:i:s');
				?></td>
                <td>
                  <?php if(in_array("15",$users_menu)) { ?>
                    <a href="<?php echo base_url(); ?>Stylist/user_view/<?php echo $val->id; ?>">
                      <button type="button" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> View</button></a>
					&nbsp;&nbsp;
					<!--<a href="<?php echo base_url(); ?>websiteusers/padata/<?php echo $val->id; ?>">
                      <button type="button" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> PA Data</button></a>
					&nbsp;&nbsp;-->
                    <a href="javascript:void(0)" data-userid="<?php echo $val->id; ?>" data-objectid="<?php echo $val->object_id; ?>" onclick="get_box_remark_popup(this)"><button type="button" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> Add Remarks</button></a>  
                      <?php } ?>
				</td>
                  </tr>
                  <?php $i++; } }else{ ?>
                    <tr>
                      <td colspan="5">Sorry no record found</td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <?php echo $this->pagination->create_links(); ?>
           
   
           </div>
          </div>

        </section>
        <!-- /.content -->

<script type="text/Javascript">
$(function () {
	$("#date_from").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true});
	$("#date_to").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true });
}); 
$( document ).ready(function() {
	$('[data-toggle=confirmation]').confirmation({
		title:'Are you sure?',
		onConfirm : function(){
		  var del_id = $(this).closest('td').find('.btn-danger').attr('data-id');
		  $.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>" + "users/user_delete",
			data: { id: del_id },
			cache:false,
			success:
			function(data){
			  location.reload(); //as a debugging message.
			}
		  });
		}
	});
	$('.nav-item-user').addClass("active");
	$('.nav-scbox-user').addClass("active");
	var show_date_range = '<?php echo $search_by; ?>';
	if(show_date_range == '12'){
		$("#filter").hide();
        $("#daterange").show();
	}
});

function submitForm(search_by){
   if(search_by > 0){
	   $("#search_by").val(search_by);
   }
   if(search_by == 12){
	    var dateFrom = $("#date_from").val();
        var dateTo = $("#date_to").val();
        var date_search = dateFrom+','+dateTo;
		encode_table_search = window.btoa(date_search);
   }
   var newhref = '<?php echo base_url(); ?>'+'websiteusers/scbox/';
   $('#frmsearch').attr('action',newhref);
   $('#frmsearch').submit();
}


/*function download_scbox_user_data(){
	
	var search_by = $('#search_by').val();
	search_by = search_by.trim();
	var table_search = $('#table_search').val();
	table_search = table_search.trim();
	table_search = window.btoa(table_search);
	var is_download = '1';
	if(search_by == 12)
	{
		var date_from_range 	= $('#date_from').val();
		var date_to_range 		= $('#date_to').val();
		if(date_from_range == '' && date_to_range == ''){
			alert('please select date range');
			return false;
		}
	}
	$('#table_search').removeAttr('required');
	var newhref = '<?php echo base_url(); ?>'+'Websiteusers/download_scbox_user_data/'+search_by+'/'+table_search+'/'+is_download;
	$('#frmsearch').attr('action',newhref);
}*/

function download_scbox_user_data(){
	
	var search_by = $('#search_by').val();
	
	search_by = search_by.trim();
	var table_search 		= $('#table_search').val();
	var date_from_range 	= $('#date_from').val();
	var date_to_range 		= $('#date_to').val();
	if(search_by != 0)
	{
			if(search_by == 12)
				{ 
					if(date_from_range == '' && date_to_range == ''){
						alert('please select date range');
						return false;
					}
					else{
						//alert('valid date range');
						valid_download(search_by,table_search);
						return true;
					}
				} 
				else{
					if(table_search != '')
					{
						//alert('valid selection');
						valid_download(search_by,table_search);
						return true;
					}
					else{
						alert('Please enter the value to export the sheet');
						return false;
					}
				} 
	}
	else{
		alert('please select option');
		//return false;
	}
	
	
}

function valid_download(search_by,table_search_data)
{
	var table_search = table_search_data;
	//alert(table_search_data);
	table_search = table_search.trim();
	table_search = window.btoa(table_search);
	var is_download = '1';
	$('#table_search').removeAttr('required');
	var newhref = '<?php echo base_url(); ?>'+'Websiteusers/download_scbox_user_data/'+search_by+'/'+table_search+'/'+is_download;
	$('#frmsearch').attr('action',newhref);
}

function download_stylist_extracted_data(){
	
	var search_by = $('#search_by').val();
	search_by = search_by.trim();
	var table_search 		= $('#table_search').val();
	var date_from_range 	= $('#date_from').val();
	var date_to_range 		= $('#date_to').val();
	if(search_by != 0)
	{
			if(search_by == 12)
				{ 
					if(date_from_range == '' && date_to_range == ''){
						alert('Please select the Date Range to export the sheet.');
						return false;
					}
					else{
						//alert('valid date range');
						valid_download_stylist_extracted_data(search_by,table_search);
						return true;
					}
				} 
				else{
					if(table_search != '')
					{
						//alert('valid selection');
						valid_download_stylist_extracted_data(search_by,table_search);
						return true;
					}
					else{
						alert('Please enter the value to export the sheet');
						return false;
					}
				} 
	}
	else{
		alert('please select option');
		//return false;
	}
	
	
}

function valid_download_stylist_extracted_data(search_by,table_search_data){
	
	//var search_by = $('#search_by').val();
	//search_by = search_by.trim();
	//var table_search = $('#table_search').val();
	var table_search = table_search_data;
	table_search = table_search.trim();
	table_search = window.btoa(table_search);
	var is_download = '1';
	$('#table_search').removeAttr('required');
	var newhref = '<?php echo base_url(); ?>'+'Websiteusers/download_stylist_extracted_data/'+search_by+'/'+table_search+'/'+is_download;
	$('#frmsearch').attr('action',newhref);
}



$("#search_by").change(function(){

    var filterType = $(this).val();
    if(filterType == 12){
        $("#filter").hide();
        $("#daterange").show();
        $("#table_search").removeAttr('required');
        $("#date_from").attr('required','');
		$("#date_to").attr('required','');

    }else{
       $("#filter").show();
      $("#daterange").hide();
    }

});

function get_box_remark_popup(container){
	var user_id = $(container).attr("data-userid");
	var objectid = $(container).attr("data-objectid");
	$.ajax({
		url: "<?php echo base_url(); ?>websiteusers/get_box_remark_popup",
		type: 'post',
		data: { 'user_id' : user_id ,'objectid' : objectid },
		success: function(data, status) {
			$('#box_remark_data').html(data);
			$("#modal-scbox-remark").modal('show');
		}
	});
}

function save_stylist_remark(user_id,objectid){
	
	var remark = $("#stylist_remark_"+user_id).val();
	$.ajax({
		url: "<?php echo base_url(); ?>websiteusers/save_stylist_remark",
		type: 'post',
		data: { 'user_id' : user_id,'remark' : remark,'objectid' : objectid },
		success: function(data, status) {
			alert("remark added.")
			// location.reload();
		}
	});
}

/*function download_stylist_extracted_data(){
	
	var search_by = $('#search_by').val();
	search_by = search_by.trim();
	var table_search = $('#table_search').val();
	table_search = table_search.trim();
	table_search = window.btoa(table_search);
	var is_download = '1';
	$('#table_search').removeAttr('required');
	var newhref = '<?php echo base_url(); ?>'+'Websiteusers/download_stylist_extracted_data/'+search_by+'/'+table_search+'/'+is_download;
	$('#frmsearch').attr('action',newhref);
}*/
</script>
