<?php
/*echo '<pre>';
print_r($unisex_category);
exit;*/
?>


<!-- Content Header (Page header) -->
<?php
if(!isset($error))
{
  $error = "";
}

?>
<section class="content-header">
  <h1>Product Upload</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>product_new">Manage Products</a></li>
    <li class="active">Product Upload</li>
  </ol>
</section>
<section class="content">
  <div class="row">
    <?php if($this->session->userdata('feedback')){echo $this->session->userdata('feedback'); $this->session->unset_userdata('feedback');}?>
    <form role="form" id="product_uploadform" enctype="multipart/form-data" method="POST" action="<?php echo base_url(); ?>quesheet/manage" onsubmit="return validate()">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-upload"></i> Upload Que Sheet <small class="label label-info"></small></h3>
            <div class="pull-right">
            </div>
          </div>
          <div class="box-body  text-center">
            <br/><br/>
            <p>Browse Que Sheet <input  id="product_upload" name="product_upload" type="file" class="file"></p>
            <!--p class="help-block">Max file size: 5MB | Allowed format: .xls, .xlsx </p-->
          <br/>
          <p>
            <button class="btn btn-primary btn-sm" type="submit" name = "Save" id="Upload" value="Save" ><i class="fa fa-upload"></i> Upload</button>
              <a href="<?php echo base_url(); ?>product_new" ><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i> Back</button></a>
          </p>

          </div>
          <span class="text-red"> <?php /*if(@$errorCount!="") { echo "Error Inserting Records: ".@$errorCount."<br/>";}
          if(@$successcount!="") { echo "Success  Inserting Records: ".@$successcount."<br/>"; }
          if(@$imagemsg!="") { echo "<b> Error in Uploading Image :</b> ".@$imagemsg."<br/>"; }*/

          if(!empty($excelmsg))
          {
            echo "<b> Error in CSV :</b> <br/>";
            foreach($excelmsg  as $ex)
            {
              echo @$ex."<br/>";
            }

          }

          ?>
        </span>
      </div>
    </div>
  </form>
  <div class="col-md-12">
    <div class="box quesheet-doc">
      <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-book"></i> Help <small class="label label-info"></small></h3>
        <div class="pull-right">
        </div>
      </div>
      <div class="box-body">
        <p class="text-orange">Download sample Que Sheet  <a href="<?php echo base_url(); ?>quesheet/manage/download_cuesheet" class="btn btn-default btn-xs"><i class="fa fa-download"></i> Download</a></p>
        <div class="seperator"><br></div>

          <div class="doc-box">
            <h4>Category</h4>
            <ul class="list-unstyled">
              <?php
              if(isset($category))
              {
                foreach ($category as $row)
                {
                  echo '<li>'.$row['category_slug'].'</li>';
                }
              }
              ?>
            </ul>
          </div>

        <div class="doc-box">
            <h4>Women Category</h4>
            <ul class="list-unstyled">
              <?php
              if(@$women_category != ""){                
                for($j=0; $j<sizeof($women_category); $j++){
                    echo '<li>'.$women_category[$j]->category_slug.'</li>';                  
                }
              }
              ?>
            </ul>
        </div>


        <div class="doc-box">
            <h4>Men Category</h4>
            <ul class="list-unstyled">
              <?php
              if(@$men_category != ""){                
                for($j=0; $j<sizeof($men_category); $j++){
                    echo '<li>'.$men_category[$j]->category_slug.'</li>';                  
                }
              }
              ?>
            </ul>
        </div>


        <div class="doc-box">
            <h4>Unisex</h4>
            <ul class="list-unstyled">
              <?php              
              if(@$unisex_category != ""){                
                for($j=0; $j<sizeof($unisex_category); $j++){
                    echo '<li>'.$unisex_category[$j]->category_slug.'</li>';                  
                }
              }
              ?>
            </ul>
        </div>

        <div class="doc-box">
          <h4>Image</h4>
          <p>Image path to upload image
          <p>
            <b>Example : </b>http://www.imagegallery.net/images/demos/test1.jpg</p>
          </p>
        </div>

        <div class="doc-box">
          <h4>Store</h4>
          <ul class="list-unstyled">
            <?php
            if(!empty($brand_data))
            {
              foreach($brand_data as $val){
                echo '<li>'.$val["user_name"].'</li>';
              }
            }
            ?>
          </ul>
        </div>

        <div class="doc-box">
          <h4>Tags</h4>
          <ul class="list-unstyled">
            <?php
            if(@$tag_data != "")
            {
              foreach($tag_data as $value)
              {
                echo '<li>'.$value->name.'</li>';

              }
            }
            ?>
          </ul>
        </div>

        <div class="doc-box">
          <h4>Brand</h4>
          <ul class="list-unstyled">
            <?php
            if(@$all_stores != "")
            {
              foreach($all_stores as $value)
              {
                echo '<li>'.$value['brand_store_name'].'</li>';

              }
            }
            ?>
          </ul>
        </div>

        <div class="doc-box">
          <h4>Product Url</h4>
          <p><b>Example : </b>http://www.example.net/images/demos/galleries/abstract/test.jpg</p>
        </div>

        <div class="doc-box">
              <h4>Attribute</h4>
              <ul class="list-unstyled">
            <?php
            if(@$attribute != ""){
                foreach($attribute as $value){
                    echo '<li><b>'.$value['attribute_slug'].'</b>: </li>';
                    foreach ($attribute_child as $value1) {
                      if($value1['attribute_id'] == $value['id'])
                        echo '<li>'.$value1['attr_value_slug'].'</li>';
                    }
                    echo '</ul><ul class="list-unstyled">';
                }
              }
            ?>
      </div>
  </section><!-- /.content -->

  <!-- DATA TABES SCRIPT -->
  <!--<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"  type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>-->


  <script type="text/javascript">

  $('.nav-item-product').addClass("active");

  /*$( document ).ready(function() {
  $('#table-brands').DataTable({
  "bPaginate": true,
  "bLengthChange": true,
  "bFilter": true,
  "bSort": true,
  "bInfo": true,
  "bAutoWidth": false
  });
  });*/

  function validate() {
    var extension;
    var filename=document.getElementById('product_upload').value;
    extension=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();
  
    if(filename == "")
    {
        alert('Browse File');
        return false;

    }else
    {//extension == 'xlsx' || extension == 'xls' || 
      if(extension == 'csv')
      {
        $(':input[type="submit"]').prop('disabled', true);
        return true;
      }else
      {
        alert('This File Is Not CSV');
        return false;
      }

    }
    

  }

  </script>
