<?php //echo $allTags;
//print_r($store);
?>
<!--<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/redmond/jquery-ui.css" />
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>-->

<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/plugins/bootstrap-chosen/custom-chosen.css" />
<script src="<?php echo base_url(); ?>/assets/plugins/bootstrap-chosen/chosen.jquery.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.lazy.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/product_tagging.js" type="text/javascript"></script>
<!-- Tag-it -->
<link href="<?php echo base_url();?>assets/css/jquery.tagit.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>assets/css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
<!-- The real deal -->

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo base_url();?>assets/js/tag-it.js" type="text/javascript" charset="utf-8"></script>
<!--End Of Tag-it -->


<section class="content-header">
  <h1>Products Tagging</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url();?>product_management">Manage Products</a></li>
    <li class="active">Products Tagging</li>
  </ol>
</section>

<section class="content page-products-tagging">
  <div class="row">
    <div class="col-md-7">
      <div class="row">
          <div class="col-md-12">
            <div class="box box-solid">
              <div class="box-header with-border">
                <h3 class="box-title">SC Tags</h3>
                <div class="pull-right">
                    <button type="submit" class="btn btn-primary btn-xs" id="saveTags" name="saveTags" ><i class="fa fa-check-circle"></i> Apply Tags</button>
                </div>
              </div>

              <form>
              <div class="box-body">
                <div class="tags-wrp sc-tags">
                  <ul id="myTags" name="myTags" >
                  </ul>
                </div>
              </div>
              </form>
              <div id="events_container"></div>

            </div>
          </div>
        </div>
      </div>
      <div class="col-md-5">
        <div class="box box-solid products-wrp">
          <div class="box-header with-border">
            <h3 class="box-title">Products <small class="label label-info"></small></h3>
          </div>
          <div class="box-body">
            <div class="box-tools">
              <form method="post" action="" id="frmsearch" class="frmsearch form-group" name="frmsearch">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-inline">
                      <label> Store  </label>
                        <select class="form-control input-sm select-brand" id="store" name="store">
                          <option value="" >Select Store </option>
                          <?php foreach($store as $val)
                              {
                          ?>
                          <option value="<?php echo $val['brand_id']; ?>"  title="<?php echo $val['company_name']; ?>" ><?php echo $val['company_name']; ?></option>
                          <?php } ?>
                        </select>

                    </div>
                  </div>
          <div class="col-md-6">
            <div class="box-tools pull-right">
				<div class="pull-right">
					<div id="show_products" class="btn btn-default btn-sm"><i class="fa fa-check-eye"></i>Show</div>
				</div>
				<div class="form-inline pull-right">
					<label style="position:relative; top:3px;">Page No </label> <select name="records-per-page" class="form-control input-sm pull-right" name="page" id="page">
					<option value="">Select</option>
					</select>
				</div>

            </div>
          </div>
        </div>
      </form>
    </div>

    <div class="image-thumbs">
      <ul id="product_list">

      </ul>
    </div>

  </div>
  <div class="box-footer">
    <button type="submit" name="selectAll" class="product-selectAll btn btn-default btn-xs"><i class="fa fa-check-circle"></i> Select All</button>
    <button type="submit" name="deselectAll" class="product-deselectAll btn btn-default btn-xs"><i class="fa fa-times-circle"></i> Deselect All</button>


    <div class="box-tools pull-right">
      <div class="input-group">
        <button name="productPublish" id="productPublish" class="product-publish btn btn-primary btn-xs" type="submit" onclick="publishProduct();"><i class="fa fa-check-circle"></i> Publish</button>
      </div>
    </div>

  </div>
</div>


</div>
</div>

</section>

<style>
#frmsearch .col-md-6, .frm-search .col-md-6{padding-left: 5px; padding-right: 5px;}
.pending-products-wrp{color:#777;}
.page-products-tagging .tags-wrp{height:420px;}

  .tags-wrp ul.tagit{border: none;}
  .page-products-tagging .frmsearch .form-inline .form-control.select-brand{width: 150px; font-size: 12px;}
  .products-wrp .box-header label{margin-bottom: 0; font-weight: normal;}
  .page-products-tagging .frmsearch #page{width:80px; margin-left: 4px; margin-right: 2px;}

  .chosen-container-single .chosen-single{border: 1px solid #d2d6de; font-size: 12px;}
  .chosen-container .chosen-results li{padding: 2px 6px; line-height: 1.3; font-size: 12px;}

  </style>

<style>

/*pqselect*/
/*
.skin-blue div.pq-select-button{
  box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.1);
  box-sizing: border-box;
  cursor: pointer;
  display: inline-block;
  height: auto;
  line-height: inherit;
  min-height: auto;
  min-width: auto;
  padding: 1px 3px 0;
  position: relative;
  text-align: left;
  white-space: normal;
  width: 100%;
}
.skin-blue .ui-state-default .ui-icon.ui-icon-triangle-1-s{float: right;
  border-left: 4px solid transparent;
  border-right: 4px solid transparent;
  border-top: 4px dashed;
  display: inline-block;
  height: 0;
  margin-left: 2px;
  vertical-align: middle;
  width: 0;
  position: relative;
  top: 10px;
  right: 2px;
}
.skin-blue .pq-select-popup{background: #fff;}
.skin-blue .pq-select-text > .pq-select-item {
  display: inline-block;
  font-size: 100%;
  font-weight: normal;
  line-height: 16px;
  margin: 1px;
  padding: 2px 0 2px 3px;
}
.skin-blue div.pq-select-menu > .pq-select-option-label{min-height: inherit; margin: 0; color: inherit; font-weight: normal; }
.skin-blue .pq-select-item > .ui-icon-close{}
.skin-blue div.pq-select-menu > .pq-select-option-label{font-size: inherit;}
*/
  </style>



<!--
  <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/redmond/jquery-ui111.css" />
  <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
  <link rel="stylesheet" href="http://localhost/liveadmin/assets/plugins/pqselect/pqselect.dev.css" />
  <script src ="http://localhost/liveadmin/assets/plugins/pqselect/pqselect.dev.js"></script>
-->

  <script>
  $(document).ready(function() {
    $('.brand-tags li, .products-wrp .image-thumbs li.selected').hover(function() {

      if($('.brand-tags li .fa, .products-wrp .image-thumbs li.selected .fa').hasClass('fa-check-circle'))
      {
        $(this).find('.fa').removeClass('fa-check-circle');
        $(this).find('.fa').addClass('fa-times-circle');
      }
      else {
        $(this).find('.fa').removeClass('fa-check-circle');
        $(this).find('.fa').addClass('fa-times-circle');
      }
    }, function() {

      if($('.brand-tags li .fa, .products-wrp .image-thumbs li.selected .fa').hasClass('fa-times-circle'))
      {
        $(this).find('.fa').addClass('fa-check-circle');
        $(this).find('.fa').removeClass('fa-times-circle');
      }
      else {
        $(this).find('.fa').addClass('fa-check-circle');
        $(this).find('.fa').removeClass('fa-times-circle');

      }
    });

    //scrollbar
  $('.tags-wrp').slimScroll({
      height: '400px',
      size : '4px'
    });
    $('.products-wrp .image-thumbs').slimScroll({
      height: '330px',
      size : '4px'
    });
    //
    // $('[data-toggle="tooltip"]').tooltip();



    //
    /*$(".select-brand111").pqSelect({
      multiplePlaceholder: 'Select Brand',
      checkbox: true //adds checkbox to options
    }).pqSelect();
*/
  $('.select-brand').chosen();

    //
/* Tag-it Event */

 //$('#myTags').tagit();

 var sampleTags = <?php echo $allTags; ?>;
    console.log(sampleTags);
    InitTags();

    function msg(m) { console.log(m); }

  });

function InitTags(){

 var sampleTags = <?php echo $allTags; ?>;
  msg("InitTags function");
  //msg(sampleTags);

  var eventTags = $('#myTags');

  eventTags.tagit({
          availableTags: sampleTags,
          caseSensitive: false,
          allowDuplicates: false,
          removeConfirmation: true,
          allowSpaces: true,

          beforeTagAdded: function(evt, ui) {
              if (!ui.duringInitialization) {
                 // addEvent('beforeTagAdded: ' + eventTags.tagit('tagLabel', ui.tag));
              }
          },
          afterTagAdded: function(evt, ui) {

              if (!ui.duringInitialization) {
                 // addEvent('afterTagAdded: ' + eventTags.tagit('tagLabel', ui.tag));
              }
          },
          beforeTagRemoved: function(evt, ui) {
              //console.log(ui);
              //addEvent('beforeTagRemoved: ' + eventTags.tagit('tagLabel', ui.tag));
              console.log(ui.tag);
          },
          afterTagRemoved: function(evt, ui) {
            //console.log(ui);
              //addEvent('afterTagRemoved: ' + eventTags.tagit('tagLabel', ui.tag));
          },
          onTagClicked: function(evt, ui) {
              //addEvent('onTagClicked: ' + eventTags.tagit('tagLabel', ui.tag));
          },
          onTagExists: function(evt, ui) {
             // addEvent('onTagExists: ' + eventTags.tagit('tagLabel', ui.existingTag));
             //alert(ui.tag);
          }
      });
}

  </script>
