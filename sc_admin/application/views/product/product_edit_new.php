<script type="text/javascript">
  var baseUrl = '<?php echo base_url(); ?>';
</script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/product.js" type="text/javascript"></script>

<?php $users_menu=$this->data['users_menu']; 
      $imagify_brandid = unserialize(IMAGIFY_BRANDID);      
?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Product Details</h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>home">Home</a></li>
      <li><a href="<?php echo base_url();?>product_management">Manage Products</a></li>
      <li class="active">Edit Product Details</li>
    </ol>
  </section>


<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
<form name="frmproductadd" id="frmproductadd" method="post" enctype="multipart/form-data">
<!-- new -->
  <div class="box ">
        <div class="box-header">
            <h3 class="box-title">Product Basic</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
      
          <div class="box-body">
              <?php echo $this->session->flashdata('feedback'); ?>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="ProductName">Product Name <span class="text-red">*</span></label>
                  <input type="text" name="product_name" tabindex="1" id="product_name" value="<?php if(isset($product_info[0]['name'])){ echo $product_info[0]['name']; } else{echo set_value('product_name');} ?>" placeholder="Enter Product Name" class="form-control" onblur="genarate_slug();">
                  <span class="text-red"> <?php echo form_error('product_name'); ?></span>
                </div>
            </div>
               <div class="col-md-6">
                <div class="form-group">
                  <label for="ProductName">Product Slug <span class="text-red">*</span></label>
                  <input type="text" name="product_slug" tabindex="1" id="product_slug" value="<?php if(isset($product_info[0]['slug'])){ echo $product_info[0]['slug']; } else{echo set_value('product_name');} ?>" placeholder="Enter Product Name" class="form-control">
                  <span class="text-red"> <?php echo form_error('product_slug'); ?></span>
                </div>
            </div>             
            <div class="col-md-6">
                <div class="form-group">
                  <label for="ProductDesc">Product Description</label>
                  <textarea tabindex="3" name="product_desc" id="product_desc" placeholder="Enter Description" rows="3" class="form-control"><?php if(isset($product_info[0]['description'])){ echo $product_info[0]['description']; } else{echo set_value('product_desc'); } ?></textarea>
                </div>
            </div>
           <!--  <div class="col-md-6">
                <div class="form-group">
                  <label for="ProductName">Product URL <span class="text-red">*</span></label>
                  <input type="text" name="product_url" tabindex="1" id="product_url" value="<?php if(isset($product_info[0]['url'])){ echo $product_info[0]['url']; } else{echo set_value('product_url'); } ?>" placeholder="Enter Product Url" class="form-control">
                  <span class="text-red"> <?php echo form_error('product_url'); ?></span>
                </div>
            </div> -->
             <div class="col-md-3">
                    <div class="form-group">
                      <label for="sku_number">Product SKU <span class="text-red">*</span></label>
                      <input type="text" tabindex="11" placeholder="Enter product sku" name="sku_number" id="sku_number" class="form-control" value="<?php if(isset($product_info[0]['sku_number'])){ echo $product_info[0]['sku_number']; } else{ echo set_value('sku_number'); } ?>" > <span class="text-red"> <?php echo form_error('sku_number'); ?></span>

                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="Stock">Product Stock <span class="text-red">*</span></label>
                      <input type="text" tabindex="11" placeholder="Enter Product Stock" name="stock_count" id="stock_count" class="form-control" value="<?php if(isset($product_info[0]['stock_count'])){ echo $product_info[0]['stock_count']; } else{ echo set_value('stock_count'); } ?>" > <span class="text-red"> <?php echo form_error('stock_count'); ?></span>

                    </div>
                  </div>
             <div class="col-md-3">
                    <div class="form-group">
                      <label for="Price">Price <span class="text-red">*</span></label>
                      <input type="text" tabindex="11" placeholder="Enter Price" name="product_price" id="product_price" class="form-control" value="<?php if(isset($product_info[0]['price'])){ echo $product_info[0]['price']; } else{ echo set_value('product_price'); } ?>" > <span class="text-red"> <?php echo form_error('product_price'); ?></span>
                    </div>
                  </div>
				  
		<div class="col-md-3">
				<div class="form-group">
				  <label for="Price">MRP Price</label>
				  <input type="text" tabindex="11" placeholder="Enter Price" name="compare_price" id="compare_price" class="form-control" value="<?php if(isset($product_info[0]['compare_price'])){ echo $product_info[0]['compare_price']; } else{ echo set_value('compare_price'); } ?>" > <span class="text-red"> <?php echo form_error('compare_price'); ?></span>
				</div>
			</div>

        <div class="col-md-3">
        <div class="form-group">
          <label for="Price">Discount Start Date</label>
          <input type="text" tabindex="11" placeholder="Enter Price" name="discount_start_date" id="discount_start_date" class="form-control datetimepicker" value="<?php if(isset($product_info[0]['discount_start_date'])){ echo $product_info[0]['discount_start_date']; } else{ echo set_value('discount_start_date'); } ?>" > <span class="text-red"> <?php echo form_error('discount_start_date'); ?></span>
        </div>
     </div>

      <div class="col-md-3">
        <div class="form-group">
          <label for="Price">Discount End Date</label>
          <input type="text" tabindex="11" placeholder="Enter Price" name="discount_end_date" id="discount_end_date" class="form-control datetimepicker" value="<?php if(isset($product_info[0]['discount_end_date'])){ echo $product_info[0]['discount_end_date']; } else{ echo set_value('discount_end_date'); } ?>" > <span class="text-red"> <?php echo form_error('discount_end_date'); ?></span>
        </div>
     </div>
			
     <div class="col-md-3"> 
          <div class="form-group">                        
                  <label for="Active">Active <span class="text-red">*</span></label>
                  <select name="product_status" id="product_status" class="form-control">
                  <?php if($product_info[0]['status'] == 1){ ?>
                    <option value="1" selected>Yes</option>
                  <?php }else{ ?>
                    <option value="1">Yes</option>
                  <?php } ?>

                  <?php if($product_info[0]['status'] == 0){ ?>
                    <option value="0" selected>No</option>
                  <?php }else{ ?>
                    <option value="0">No</option>
                  <?php } ?>

                  </select>
            </div>
            <span class="text-red"> </span>
      </div>

       <div class="col-md-3"> 
          <div class="form-group">                        
                <label for="parent_sku">Parent SKU<span class="text-red">*</span></label>
                <input type="text" tabindex="11" placeholder="Parent SKU" name="parent_sku" id="parent_sku" class="form-control" value="<?php if(isset($product_info[0]['parent_id'])){ echo $product_info[0]['parent_id']; } else{ echo set_value('parent_sku'); } ?>" > <span class="text-red"> <?php echo form_error('parent_sku'); ?></span>
            </div>
            <span class="text-red"> </span>
      </div>
                 <div class="col-md-6">
 <div class="form-group">
                  <label for="Brand">Store  </label>
                  <select tabindex="5" id="brand1" name="brand" class="form-control chosen-select" tabindex="-1">
                    
                    <?php
                    if(!empty($brand_data)){
                      foreach($brand_data as $val){
                          if($product_info[0]['brand_id'] == $val["id"]){
                          echo '<option value="'.$val["id"].'" selected>'.$val["user_name"].'</option>';
                        }else{
                          echo '<option value="'.$val["id"].'">'.$val["user_name"].'</option>';
                        }
                      }
                    }
                    ?>
                  </select>
                </div>
 </div>
                
                 <div class="col-md-6">
                    <div class="form-group">
                      <label for="ProductAvail">Brand  <span class="text-red">*</span></label>
                      <!-- <select tabindex="27" id="product_avail1" value="" name="product_avail" class="form-control chosen-select" > -->
                      <select tabindex="27" id="product_avail1" value="" name="product_avail[]" class="form-control chosen-select" multiple> 
                        <?php
                        if(!empty($all_stores)){ 
                          foreach($all_stores as $val){
                            if(in_array($val['id'],$product_avalibility)){
                              echo '<option value="'.$val['id'].'" selected>'.$val['brand_store_name'].'</option>';
                            }else{
                              echo '<option value="'.$val['id'].'">'.$val['brand_store_name'].'</option>';
                            }
                          }
                        }
                        ?>
                      </select>
                      <span class="text-red"> <?php echo form_error('product_avail'); ?></span>
                    </div>
                  </div>
                  


                </div>
           
            </div>
          </div>
<!-- -->



               <div class="box">
        <div class="box-header">
            <h3 class="box-title">Product Images</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
         <div class="row">
              <div class="col-md-6">
                   <div class="form-group">
                  <label for="Uploadimage">Upload Product Image<span class="text-red">*</span></label>
                    <div>
                  <input id="product_images" name="product_images" class="file1" type="file" >
                  <span class="help-block">Max file size: 2MB. | Allowed format: .jpg, .jpeg, .png, .gif</span>
                  <span class="text-red"> <?php if(isset($error)) { echo $error[0]; } ?></span>
                </div> </div>
                </div>

                 <div class="col-md-6">
                      <div class="form-group">
                                    <label class="control-label">Upload Product Extra Images</label>
                                      <div>
                                      <input type="file" name="extra_images[]" id="extra_images" multiple="true">
                                      <span class="help-block">Max file size: 2MB. | Allowed format: .jpg, .jpeg, .png, .gif</span>
                                      </div>
                                    </div>
                                  </div>


                  <div class="col-md-6">
                  <?php if(@$product_info[0]['image']) {
                       
                      if(in_array($product_info[0]['brand_id'],$imagify_brandid))
                      {
                        $productUrl = $this->config->item('products_small').$product_info[0]['image'];
                      }else
                      {
                        $productUrl =  base_url().'assets/products/thumb_124/'.$product_info[0]['image'];                          
                      }
        
                      echo '<div class="img-wrp">';
                      echo '<img src="'.$productUrl.'" onerror="this.onerror=null; this.src='.DEFAULT_IMAGE_PATH.';">';
                      echo '</div>';
                    } ?>
                    </div>

                    <div class="col-md-6">
                            <?php if(!empty($get_extra_images)){
                                foreach($get_extra_images as $val){
                                   
                                    if(in_array($product_info[0]['brand_id'],$imagify_brandid))
                                    {
                                      $productUrlthumb = $this->config->item('products_small').$val['product_images'];
                                    }else
                                    {
                                      $productUrlthumb =  base_url().'assets/products_extra/thumb/'.$val['product_images'];                                       
                                    }
                                  ?>
                                  <div class="col-md-2">
                                      <div class="img-wrp">
                                    <img src="<?php echo $productUrlthumb; ?>" onerror="this.onerror=null; this.src='https://i.imgur.com/Qu4zXlp.jpg';">
                                    <a href="javascript:void(0);" class="text-red" onclick="delete_extra_img(<?php echo $val['id']; ?>);"><i class="fa fa-close"></i></a>
                                  </div>
                                    </div>
                                  <?php
                                }
                              } ?>
                </div>

                </div>
              </div>
            </div>

<!-- new -->
  <div class="box ">
        <div class="box-header">
            <h3 class="box-title">Product Grouping</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
      
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                    <div class="form-group">
                      <label for="category">Category  <span class="text-red">*</span></label>
                      <select class="form-control chosen-select" tabindex="7" name="category[]" id="category" multiple="">
                       <option value="-1">None</option>
                               <?php if(!empty($cat_data)){
                              foreach($cat_data as $val){
                                  echo $val;
                              }

                              } ?>
                      </select>
                      <span class="text-red"> <?php echo form_error('category'); ?></span>
                    </div>
                  </div>
                   <div class="col-md-6">
                  <div class="form-group">
                  <label for="productTags">Tags</label>
                  <select tabindex="19" name="product_tags[]" id="product_tags1" class="form-control chosen-select" multiple tabindex="">
                   
                    <?php
                    if(!empty($tag_data)){
                      foreach($tag_data as $val){
                        if(in_array($val->id,$product_tags)){
                          echo '<option value="'.$val->id.'" selected>'.$val->name.'</option>';
                        }else{

                          echo '<option value="'.$val->id.'">'.$val->name.'</option>';
                        }
                      }
                    }
                    ?>
                  </select>
                </div>
                 </div>

                <!--<div class="col-md-6">
                  <div class="form-group">
                  <label for="productTags">Attributes</label>
                  <select tabindex="19" name="attributes[]" id="attributes" class="form-control chosen-select" multiple tabindex="">
                   <option value="">Select Attributes</option>       
                   <?php if(!empty($all_attributes)) { ?>            
                      <?php foreach ($all_attributes as $value) { ?>
                          <option value="<?php echo $value['id']; ?>" disabled><?php echo $value['attribute_name']; ?></option>
                          <?php 
                          $attributes_value = $this->productmanagement_model->attributes_values($value['id']);

                          if(!empty($attributes_value)){
                            foreach($attributes_value as $val_data){ 
                              if(in_array($val_data['id'],$attributes)){ ?>
                              <option value="<?php echo $val_data['id']; ?>" selected><?php echo $val_data['attr_value_name']; ?></option>
                          <?php  }else{ ?>
                              <option value="<?php echo $val_data['id']; ?>"><?php echo $val_data['attr_value_name']; ?></option>
                          <?php
                          } }
                          }

                          ?>
                      <?php } ?>
                   <?php } ?>
                  </select>
                  </div>
                </div>-->
                

                  <!--div class="col-md-6">
                  <div class="form-group">
                  <?php $color =  unserialize(ALL_FILTER_COLOR);  ?>               
                  <label for="productTags">Color</label>
                  <select tabindex="19" name="sc_product_color" id="sc_product_color" class="form-control chosen-select" tabindex="">
                    <?php 
                      if(!empty($color)){
                        foreach($color as $key=>$val){
                          if($product_info[0]['sc_product_color'] == $key){ 
                          ?>
                          <option value="<?php echo $key; ?>" selected><?php echo $val; ?></option>
                          <?php }else{ ?>
                          <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                          <?php }
                        }
                      }
                    ?>

                  </select>
                </div>
                 </div-->

                 <!-- <div class="col-md-6">
                    <div class="form-group">
                      <label for="productTags">Personality</label>
                      <select name="bucket[]" id="bucket" class="form-control chosen-select" tabindex="7" multiple="">
                        <option value="-1">None</option>
                        <?php if(!empty($bucket)){
                            foreach($bucket as $val){ 
                              if(in_array($val['id'], $bucket_data)){ ?>
                              <option value="<?php echo $val['id']; ?>" selected><?php echo $val['body_shape'].' '.$val['style'].' '.$val['work_style'].' '.$val['personal_style']; ?></option>

                              <?php }else{
                              ?>
                              <option value="<?php echo $val['id']; ?>"><?php echo $val['body_shape'].' '.$val['style'].' '.$val['work_style'].' '.$val['personal_style']; ?></option>
                            <?php } }

                        } ?>
                      </select>
                      <p class="help-block">Assign products to the personality (Totally optional).</p>
                    </div>
                  </div>
 -->
                <!--   <div class="col-md-6">
                    <div class="form-group">
                      <label for="productTags">Age</label>
                      <select name="age[]" id="age" class="form-control chosen-select" tabindex="7" multiple="">
                        <option value="-1">None</option>
                        <?php if(!empty($age)){
                            foreach($age as $val){ 
                            if($product_info[0]['age_group'] == $val->answer){ ?>
                              <option value="<?php echo $val['id']; ?>" selected><?php echo $val['answer']; ?></option>
                            <?php }else{ ?>
                              <option value="<?php echo $val['id']; ?>"><?php echo $val['answer']; ?></option>
                            <?php } }

                            } ?>
                      </select>
                      <p class="help-block">Assign products to the age (Totally optional).</p>
                    </div>
                  </div> -->

                  <!-- <div class="col-md-6">
                    <div class="form-group">
                      <label for="productTags">Budget</label>
                      <select name="budget[]" id="budget" class="form-control chosen-select" tabindex="7" multiple="" >
                        <option value="-1">None</option>
                        <?php if(!empty($budget)){
                          foreach($budget as $val){ 
                            if(in_array($val['id'], $budget_data)){
                            ?>
                            <option value="<?php echo $val['id']; ?>" selected><?php echo $val['answer']; ?></option>
                          <?php }else{ ?>
                            <option value="<?php echo $val['id']; ?>"><?php echo $val['answer']; ?></option>
                          <?php } }

                          } ?>
                      </select>
                      <p class="help-block">Assign products to the budget (Totally optional).</p>
                    </div>
                  </div> -->

            </div>
            </div>
            </div>

<!-- new -->
<div class="box ">
    <div class="box-header">
        <h3 class="box-title">Product Attribute Mapping</h3>
    </div><!-- /.box-header -->
    <!-- form start --> 
    <div class="row">
       <div class="col-md-12">
          <div class="form-group">
          <!--<label for="productTags">Attributes - </label><br/>-->                  
           <!--<option value="">Select Attributes</option>-->       
           <?php if(!empty($all_attributes)) { ?>            
              <?php foreach ($all_attributes as $value) { ?>
                <div class="col-md-6">
                <label for="productTags"><?php echo $value['attribute_name'].' :'; ?></label>
                <select tabindex="19" name="attributes[]" id="attributes" class="form-control chosen-select" multiple tabindex="">
                  <!--<option value="<?php echo $value['id']; ?>" disabled><?php echo $value['attribute_name']; ?></option>-->
                  <?php  
                  $attributes_value = $this->productmanagement_model->attributes_values($value['id']);

                  if(!empty($attributes_value)){
                    foreach($attributes_value as $val_data){ 
                      if(in_array($val_data['id'],$attributes)){ ?>
                      <option value="<?php echo $val_data['id']; ?>" selected><?php echo $val_data['attr_value_name']; ?></option>
                  <?php  }else{ ?>
                      <option value="<?php echo $val_data['id']; ?>"><?php echo $val_data['attr_value_name']; ?></option>
                  <?php
                  } }
                  }
                  ?></select>  </div>   
              <?php } ?>
           <?php } ?>
          </select>
          </div>
        </div>
    </div>
    <br/>
</div>

<!--  -->
  <div class="box hide">
        <div class="box-header">
            <h3 class="box-title">Product ...</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
         <div class="row">
         <div class="col-md-4">
                    <div class="form-group">
                      <label for="targetMarket">Target Market</label>
                      <select placeholder="Select Target Market" tabindex="23" id="product_target_market" name="product_target_market" class="form-control chosen-select">
                        <option value="">Select Target Market</option>
                        <?php
                        if(!empty($target_marget)){
                          foreach($target_marget as $val){
                           if($product_info[0]['target_market'] == $val->id){
                            echo '<option value="'.$val->id.'" selected>'.$val->target_market.'</option>';
                          }else{
                            echo '<option value="'.$val->id.'">'.$val->target_market.'</option>';
                          }
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="ConsumeType">Consumer Type</label>
                      <select placeholder="Select Consumer Type" tabindex="25" id="product_consumer_type" name="product_consumer_type" class="form-control chosen-select" >
                        <option value="">Select Consumer Type </option>
                        <?php
                            $mat = ''; $fem=''; $mal=''; $kid=''; $pmal=''; $pfem='';
                            if($product_info[0]['consumer_type'] == "Maternity &amp; Baby"){
                              $mat = "selected";
                            }else if($product_info[0]['consumer_type'] == "Female"){
                              $fem = "selected";
                            }else if($product_info[0]['consumer_type'] == "Male"){
                              $mal = "selected";
                            }else if($product_info[0]['consumer_type'] == "Kidswear"){
                              $kid = "selected";
                            }else if($product_info[0]['consumer_type'] == "Plus Male"){
                              $pmal = "selected";
                            }else if($product_info[0]['consumer_type'] == "Plus Female"){
                              $pfem = "selected";
                            }
                        ?>
                         <option value="Maternity &amp; Baby" <?php echo $mat; ?>>Maternity & Baby</option>
                        <option value="Female" <?php echo $fem; ?>>Female</option>
                        <option value="Male" <?php echo $mal; ?>>Male</option>
                        <option value="Kidswear" <?php echo $kid; ?>>Kidswear</option>
                        <option value="Plus Male" <?php echo $pmal; ?>>Plus Male</option>
                        <option value="Plus Female" <?php echo $pfem; ?>>Plus Female</option>
                      </select>
                    </div>
                  </div>                 
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="Rating">Rating</label>
                      <input type="text" tabindex="10" placeholder="Enter your rating" name="product_rate" id="product_rate" class="form-control" value="<?php if($product_info[0]['rating']){ echo $product_info[0]['rating']; } else{ echo set_Value('product_rate'); } ?>">
                    </div>
                  </div>
                      <div class="col-md-12">
                              
                                  <div class="form-group">
                                    <label class="control-label">Product Inventory</label>
                                    
                               <!--  <div id="product_inventory">
                                <?php $i = 1; if(!empty($product_inventory)){ ?>
                                <?php foreach ($product_inventory as $key => $value) { ?>
                                  <div class="row" id="row<?php echo $i; ?>">
                                    <div class="col-md-3">
                                      <input type="text" name="size<?php echo $i; ?>" id="size<?php echo $i; ?>" placeholder="Size" class="form-control" value="<?php echo $value['size_text']; ?>">
                                    </div>
                                    <div class="col-md-3">
                                      <input type="text" name="qty<?php echo $i; ?>" id="qty<?php echo $i; ?>" placeholder="Qty" class="form-control" value="<?php echo $value['stock_count']; ?>">
                                    </div>
                                    <div class="col-md-3">
                                      <input type="text" name="product_sku<?php echo $i; ?>" id="product_sku<?php echo $i; ?>" placeholder="Product SKU" class="form-control" value="<?php echo $value['product_sku']; ?>">
                                    </div>
                                    <div class="col-md-3">
                                      <span class="btn btn-primary btn-xs btn-delete" onclick="remove_row(<?php echo $i; ?>);"><i class="fa fa-minus"></i></span>
                                    </div>
                                  </div>
                                  <?php $i++; } ?>
                                <?php } ?>

                                   <div class="row" id="row<?php echo $i; ?>">
                                    <div class="col-md-3">
                                      <input type="text" name="size<?php echo $i; ?>" id="size<?php echo $i; ?>" placeholder="Size" class="form-control" value="">
                                    </div>
                                    <div class="col-md-3">
                                      <input type="text" name="qty<?php echo $i; ?>" id="qty<?php echo $i; ?>" placeholder="Qty" class="form-control" value="">
                                    </div>
                                    <div class="col-md-3">
                                      <input type="text" name="product_sku<?php echo $i; ?>" id="product_sku<?php echo $i; ?>" placeholder="Product SKU" class="form-control" value="">
                                    </div>
                                    <div class="col-md-3">
                                      <span class="btn btn-primary btn-xs" onclick="addnew_row(<?php echo $i; ?>);"><i class="fa fa-plus"></i></span>
                                    </div>
                                  </div>

                                </div>
                              <input type="hidden" name="total_rows" id="total_rows" value="<?php echo count($product_inventory) >0 ? count($product_inventory)+1 : $i; ?>">
 -->

                                   </div>
                            </div>
               



                </div>
              </div>
            </div>





                 <div class="box-footer text-right">
          <button class="btn btn-sm btn-primary" tabindex="30" type="submit"><i class="fa fa-save"></i> Save</button>
           <a href="<?php echo base_url(); ?>product_management"><button type="button" class="btn btn-primary  btn-sm btn-rad"><i class="fa fa-reply"></i> Back</button></a>
          <?php if(in_array("43",$users_menu)) { ?>
          <button class="btn btn-sm btn-primary" tabindex="30" type="button" id="approved"><i class="fa fa-save"></i> Approved</button>
          <?php } ?>
           <?php if(in_array("43",$users_menu)) { ?>
          <button class="btn btn-sm btn-primary" tabindex="30" type="button" id="reject"><i class="fa fa-save"></i> Reject</button>
          <?php } ?>
          <!-- <button class="btn btn-primary" type="submit">Edit</button>
          <button class="btn btn-primary" type="submit">Approved/Pending</button> -->
        </div>
        <input type="hidden" name="product_id" id="product_id" value="<?php echo @$product_info[0]['id']; ?>">

<!-- /new-->
</form>
        </div>
      </div>
    </section><!-- /.content -->
    <script type="text/javascript">
      function genarate_slug(){
        var category_name = $('#product_name').val();
        var slug = category_name.replace(/[^a-zA-Z0-9]/g, "-");
        $('#product_slug').val(slug.toLowerCase());
      }

    </script>