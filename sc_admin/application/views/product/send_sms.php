<?php 
$all_sms = unserialize(SMS_ARRAY);$radio_html = '';$message_html = '';
$mobile_no = $this->input->get('mobile_no');
$f_name = $this->input->get('f_name');
$i=0;
foreach($all_sms as $val){
	if($i==0){
		$checked = 'checked';
		$required = 'required';
		$class = '';
	}else{
		$checked = '';
		$required = '';
		$class = 'hide';
	}
	$radio_html = $radio_html.' <input type="radio" name="message_type" value="'.$val['type'].'" onclick="change_smsbox('.$val['type'].',\''.$val['message_id'].'\');" '.$checked.' > '.$val['message_for'].'   ';
	$message_html = $message_html.'<textarea class="form-control '.$class.' " rows="10" name="message_'.$val['type'].'" id="'.$val['message_id'].'" placeholder="Message..." maxlength="160" '.$required.' >'.$val['user_sms'].'</textarea>';
	$i++;
}
?>
<section class="content-header">
  <h1>SEND SMS</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">Send Sms</li>
  </ol>
  <br>
</section>
<!-- /.box-header -->
<div class="box">
  <div class="box-body">
      <form name="sendsms" id="sendsms" class="form-group" action="" method="post" onsubmit="return validate_sms_Form()">
		<div class="row">
			<div class="col-md-5">
				<div class="row">
		         <div class="col-md-8">
		              <div class="form-group">
		                <label class="control-label" for="">Mobile Number</label><span class="text-red">*</span></label>
		                <input type="text" class="form-control" name="mobile_num" id="mobile_num" placeholder="Mobile" pattern="[0-9]+(,[0-9]+)*" value="<?php echo $mobile_no; ?>" required >
		              </div>
		          </div>
		        </div>
				
				<div class="row" id="">
					<div class="col-md-8">
					  <div class="form-group">
						<label class="control-label" for="">Message</label><span class="text-red">*</span></label>
						<!-- <textarea class="form-control" rows="10" name="message" id="message" placeholder="Message..." maxlength="160" required ></textarea> -->
						<?php echo $message_html; ?>
					  </div>
					</div>
				</div> 
				
		         <div class="row">
					<div class="col-md-8">
						<div class="form-group">
						  <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-send"></i> SEND</button>
						</div>   
					</div>
		        </div>     
		        <div class="pull-right">
		          <?php echo $this->pagination->create_links(); ?>
		        </div>
			</div>
			<div class="col-md-7">
			
				<!-- <input type="radio" name="message_type" value="0" onclick="change_smsbox(0);" checked > Other 		
				<input type="radio" name="message_type" value="1" onclick="change_smsbox(1);" > Missed call
				<input type="radio" name="message_type" value="2" onclick="change_smsbox(2);" > Order Confirmed
				<input type="radio" name="message_type" value="3" onclick="change_smsbox(3);" > Order delivered -->
				<?php echo $radio_html; ?>
			</div>
		</div>	
		        
      </form>
  </div>
</div>
<script type="text/Javascript">
var old_message = 0;
var old_message_id = 'other';
function validate_sms_Form(){
	var result = confirm("Want to send SMS?");
	return result;
}


function change_smsbox(type,message_id){
	if(type == '1'){
		$('#'+old_message_id).addClass('hide');
		$('#'+message_id).removeClass('hide');
	}else if(type == '2'){
		$('#'+old_message_id).addClass('hide');
		$('#'+message_id).removeClass('hide');
	}else if(type == '3'){
		$('#'+old_message_id).addClass('hide');
		$('#'+message_id).removeClass('hide');
	}else if(type == '4'){
		$('#'+old_message_id).addClass('hide');
		$('#'+message_id).removeClass('hide');
	}
	old_message = type;
	old_message_id = message_id;
}

</script> 
