<!-- Main content -->
<!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.min11.css" />
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>-->

<!--sumoselect-->
<!--link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/sumoselect/sumoselect.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/sumoselect/jquery.sumoselect.min.js"></script-->

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/redmond/jquery-ui.css" />    
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<!--pqSelect dependencies-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev.js"></script>

<script src="<?php echo base_url(); ?>assets/js/product.js"></script>

<!--pqSelect dependencies-->
<!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev111.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev.js"></script>-->

<!-- product dependencies -->
<script src="<?php echo base_url(); ?>assets/js/product.js" type="text/javascript"></script>
<?php $users_menu=$this->data['users_menu']; ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Product Details</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url();?>product_new">Manage Products</a></li>
    <li class="active">Add Product Details</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box ">
        <div class="box-header">
            <h3 class="box-title">Add Product Details</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" id="product_form" enctype="multipart/form-data" method="POST">
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">

                <div class="form-group">
                  <label for="ProductName">Product Name <span class="text-red">*</span></label>
                  <input type="text" name="product_name" tabindex="1" id="product_name" value="<?php echo set_value('product_name'); ?>" placeholder="Enter Product Name" class="form-control">
                  <span class="text-red"> <?php echo form_error('product_name'); ?></span>
                </div>

                <div class="form-group">
                  <label for="ProductDesc">Product Description</label>
                  <textarea tabindex="3" name="product_desc" id="product_desc" placeholder="Enter Description" rows="3" class="form-control"><?php echo set_value('product_desc'); ?></textarea>
                </div>

                <div class="form-group">
                  <label for="Brand">Store  </label>
                  <select tabindex="5" id="brand" name="brand" class="form-control" tabindex="-1">
                    <option value="">Select Store</option>
                    <?php
                    if(!empty($brand_data)){
                      foreach($brand_data as $val){
                        if(set_value('brand') == $val['id']){
                          echo '<option value="'.$val["id"].'" selected>'.$val["user_name"].'</option>';
                        }else{

                          echo '<option value="'.$val["id"].'">'.$val["user_name"].'</option>';
                        }
                      }
                    }
                    ?>
                  </select>
                </div>
              
                <div class="form-group-product-tags-wrp">
                  <label for="productTags">Tags</label>
                  <select placeholder="Select Tags" tabindex="19" data-placeholder="Choose Tags" name="product_tags[]" id="product_tags" class="form-control multi-select-box chosen-select" multiple tabindex="">
                    <?php
                    if(!empty($tag_data)){
                      foreach($tag_data as $val){
                        if(set_value('product_tags') == $val->id){
                          echo '<option value="'.$val->id.'">'.$val->name.'</option>';
                        }else{

                          echo '<option value="'.$val->id.'">'.$val->name.'</option>';
                        }
                      }
                    }
                    ?>
                  </select>
                </div>

                <div class="form-group">
                  <label for="ProductURL">Product URL </label>
                  <input type="text" tabindex="15" placeholder="Enter Product URL" id="product_url" name="product_url" class="form-control" value="">
                  <span class="text-red"> <?php echo form_error('product_url'); ?></span>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="category">Category  <span class="text-red">*</span></label>
                      <select class="form-control" tabindex="7"  name="category" id="category">
                        <option value="">Select Category</option>
                        <?php
                        if(!empty($category)){
                          foreach($category as $val){
                            echo '<option value="'.$val->id.'">'.$val->name.'</option>';
                          }
                        }
                        ?>
                      </select>
                      <span class="text-red"> <?php echo form_error('category'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="subCategory">Sub Category  <span class="text-red">*</span></label>
                      <select tabindex="9" class="form-control"  name="sub_category" id="sub_category">
                        <option value="">Select Subcategory</option>
                      </select>
                      <span class="text-red"> <?php echo form_error('sub_category'); ?></span>
                    </div>
                  </div>

                  <div class="col-md-12">
                      <div id="variation_type" class="prod-variation-type" ></div>
                  </div>

                  <div class="col-md-6">

                    <!--div class="form-group">
                    <label for="exampleInputEmail1">Tags </label>
                    <input type="text" placeholder="Enter Tags" id="exampleInputEmail1" class="form-control">
                    </div-->
                    <div class="form-group">
                      <label for="budget">Price Range  <span class="text-red">*</span></label>
                      <select placeholder="Select Price Range" tabindex="23" id="product_price_range" name="product_price_range" class="form-control chosen-select">
                        <option value="">Select Price Range</option>
                        <?php
                        if(!empty($budget)){
                          foreach($budget as $val){
                            echo '<option value="'.$val['id'].'">'.$val['answer'].'</option>';
                          }
                        }
                        ?>
                      </select>
                      <span class="text-red"> <?php echo form_error('product_price_range'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="Price">Price <span class="text-red">*</span></label>
                      <input type="text" tabindex="11" placeholder="Enter Price" name="product_price" id="product_price" class="form-control" value="<?php echo set_value('product_price'); ?>" > <span class="text-red"> <?php echo form_error('product_price'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="targetMarket">Target Market</label>
                      <select placeholder="Select Target Market" tabindex="23" id="product_target_market" name="product_target_market" class="form-control chosen-select">
                        <option value="">Select Target Market</option>
                        <?php
                        if(!empty($target_marget)){
                          foreach($target_marget as $val){
                            echo '<option value="'.$val->id.'">'.$val->target_market.'</option>';
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="ConsumeType">Consumer Type</label>
                      <select placeholder="Select Consumer Type" tabindex="25" id="product_consumer_type" name="product_consumer_type" class="form-control" >
                        <option value="">Select Consumer Type </option>
                        <option value="Maternity &amp; Baby" >Maternity & Baby</option>
                        <option value="Female" >Female</option>
                        <option value="Male" >Male</option>
                        <option value="Kidswear" >Kidswear</option>
                        <option value="Plus Male" >Plus Male</option>
                        <option value="Plus Female" >Plus Female</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="ProductAvail">Brand  <span class="text-red">*</span></label>
                      <select placeholder="Select Product Availabilty" tabindex="27" id="product_avail" value="" name="product_avail[]"class="form-control multi-select-box chosen-select" multiple>

                        <?php
                        if(!empty($all_stores)){ $i = 0;
                          foreach($all_stores as $val){
                            if($i==0){
                              echo '<option value="'.$val['id'].'" selected>'.$val['brand_store_name'].'</option>';
                            }else{
                              echo '<option value="'.$val['id'].'">'.$val['brand_store_name'].'</option>';
                            }
                            $i++;
                          }
                        }
                        ?>
                      </select>
                      <span class="text-red"> <?php echo form_error('product_avail'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="Rating">Rating</label>
                      <input type="text" tabindex="10" placeholder="Enter your rating" name="product_rate" id="product_rate" class="form-control" value="<?php echo set_Value('product_rate'); ?>">
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="Uploadimage">Upload Product Image<span class="text-red">*</span></label>

                  <input id="product_images" name="product_images" class="file1" type="file" >
                  <span class="help-block">Max file size: 2MB. | Allowed format: .jpg, .jpeg, .png, .gif</span>
                  <span class="text-red"> <?php if(isset($error)) { echo $error[0]; } ?></span>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="BodyPart">Body Part</label>
                      <select placeholder="Select Body Part" tabindex="6" id="body_part" name="body_part" class="form-control" >
                        <option value="">Select Body Part </option>
                        <option value="Head gear / Hair">Head gear / Hair</option>
                        <option value="Ears">Ears</option>
                        <option value="Eyes">Eyes</option>
                        <option value="Nose">Nose</option>
                        <option value="Neck">Neck</option>
                        <option value="Arms">Arms</option>
                        <option value="Wrist">Wrist</option>
                        <option value="Fingers">Fingers</option>
                        <option value="Torso">Torso</option>
                        <option value="Feet">Feet</option>
                        <option value="Shoes">Shoes</option>
                        <option value="Accessories">Accessories</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="BodyType">Body Type</label>
                      <select name="body_shape" id="body_shape" class="form-control">
                        <?php
                        if(!empty($body_type)){
                          foreach($body_type as $val){
                            echo '<option value="'.$val['id'].'">'.$val['answer'].'</option>';
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="BodyShape">Body Shape</label>
                      <select name="style" id="style" class="form-control">
                        <?php
                        if(!empty($body_shape)){
                          foreach($body_shape as $val){
                            echo '<option value="'.$val['id'].'">'.$val['answer'].'</option>';
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">

                    <div class="form-group">
                      <label for="Personality">Style</label>
                      <select name="style" id="style" class="form-control">
                        <?php
                        if(!empty($style)){
                          foreach($style as $val){
                            echo '<option value="'.$val['id'].'">'.$val['answer'].'</option>';
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">

                    <div class="form-group">
                      <label class="control-label" for="age">Age Group</label>
                      <select name="age" id="age" class="form-control">
                        <?php
                        if(!empty($age)){
                          foreach($age as $val){
                            echo '<option value="'.$val['id'].'">'.$val['answer'].'</option>';
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="control-label">Active<span class="text-red"> * </label>
                            <div>  <label class="radio-inline"> <input type="radio" name="product_status" value="1" checked>Yes</label>
                              <label class="radio-inline"> <input type="radio" name="product_status" value="0" > No</label>
                            </div>
                            <span class="text-red"> <?php echo form_error('product_status'); ?></span>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Promotional<span class="text-red"> * </label>
                              <div>  <label class="radio-inline"> <input type="radio"  name="promotional" value="1">Yes</label>
                                <label class="radio-inline"> <input type="radio" name="promotional" value="0" checked> No</label>
                              </div>
                              <span class="text-red"> <?php echo form_error('promotional'); ?></span>
                            </div>
                          </div>
                        </div>
                      </div>

                         <div class="col-md-12">
                              <div class="row">
                                 <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="control-label">Upload Product Extra Images</label>
                                      <div>
                                      <input type="file" name="extra_images[]" id="extra_images" multiple="true">
                                      <span class="help-block">Max file size: 2MB. | Allowed format: .jpg, .jpeg, .png, .gif</span>
                                      </div>
                                    </div>
                                  </div>
                              </div>
                            </div>
      
                            <div class="col-md-12">
                              
                                  <div class="form-group">
                                    <label class="control-label">Product Inventory</label>
                                      <div id="product_inventory">
                                        <div class="row" id="row1">
                                          <div class="col-md-3">
                                            <input type="text" name="size1" id="size1" placeholder="Size" class="form-control">
                                          </div>
                                          <div class="col-md-3">
                                            <input type="text" name="qty1" id="qty1" placeholder="Qty" class="form-control">
                                          </div>
                                          <div class="col-md-3">
                                            <input type="text" name="product_sku1" id="product_sku1" placeholder="Product SKU" class="form-control">
                                          </div>
                                          <div class="col-md-3">
                                            <span class="btn btn-primary btn-xs" onclick="addnew_row(1);"><i class="fa fa-plus"></i></span>
                                          </div>
                                        </div>
                                      </div>
                                    <input type="hidden" name="total_rows" id="total_rows" value="1">
                                   </div>
                            </div>
                            
                    </div>

                  </div>
                </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                <button class="btn btn-sm btn-primary" tabindex="30" type="submit"><i class="fa fa-save"></i> Save</button>
                 <a href="<?php echo base_url(); ?>product_new"><button type="button" class="btn btn-primary  btn-sm btn-rad"><i class="fa fa-reply"></i> Back</button></a>
                <button class="btn btn-sm btn-default" tabindex="31" type="reset"><i class="fa fa-close"></i> Cancel</button>
                <!-- <button class="btn btn-primary" type="submit">Edit</button>
                <button class="btn btn-primary" type="submit">Approved/Pending</button> -->
              </div>
            </form>
          </div>
        </div>
      </div>
    </section><!-- /.content -->
