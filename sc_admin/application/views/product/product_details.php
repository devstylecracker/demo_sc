<?php
    //echo "<pre>"; print_r($product_info[0]);
   $imagify_brandid = unserialize(IMAGIFY_BRANDID); 
?>
<style type="text/css">
  img
{
display: inline-block;
margin: 0px;
padding: 0px;
width: 100px;
}
</style>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Product Details</h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>home">Home</a></li>
      <li><a href="<?php echo base_url();?>product_management"></i>Manage Products</a></li>
      <li class="active">Product Details</li>
    </ol>
  </section>
  <section class="content page-product-details">
    <div class="row">
      <div class="col-md-6">
        <div class="box product-details">
          <div class="box-body">
            <div class="table-responsive">
              <table class="table table-bordered table-striped dataTable table-vth" id="datatable">
                <tr>
                  <th style="width:30%;">Name </th>
                  <td><?php echo @$product_info[0]['name'];?> </td>
                 </tr>
                <tr>
                  <th>Description</th>
                   <td><?php echo @$product_info[0]['description'];?> </td>
                </tr>
                <tr>
                  <th>Store </th>
                   <td><?php
                   if(!empty($brand_data)){
                      foreach($brand_data as $val){
                        if($product_info[0]['brand_id'] == $val['id']){
                        echo $val["user_name"];
                          }
                        }
                    }

                   ?></td>
                </tr>
               <tr>
                  <th>Category  </th>
                  <td><?php if(!empty($category)){
                    /*foreach($category as $val){
                      if(@$product_info[0]['product_cat_id'] == $val->id){
                        echo $val->name;
                      }
                    }*/
                    echo $category;
                  } ?></td>
                </tr>           

                <tr>
                  <th>Tags</th>
                  <td><?php if(!empty($tag_data)){
                    foreach($tag_data as $val){
                       if(in_array($val->id,$product_tags)){
                         echo $val->name.' ';
                       }
                    }
                    } ?></td>
                </tr>

                 <tr>
                  <th>SKU</th>
                  <td><?php if(!empty(@$product_info[0]['sku_number'])){                    
                    echo $product_info[0]['sku_number'];
                  } ?></td>
                </tr>

                <tr>
                  <th>Stock</th>
                  <td><?php if(!empty(@$product_info[0]['sku_number'])){                    
                    echo $product_info[0]['stock_count'];
                  } ?></td>
                </tr>               

                <tr>
                  <th>Price </th>
                  <td><?php echo $product_info[0]['price']; ?></td>
                </tr>  
                <tr>
                  <th>Brand </th>
                   <td><?php
                        if(!empty($all_stores)){
                              foreach($all_stores as $val){
                                if(in_array($val['id'],$product_avalibility)){
                                  echo $val['brand_store_name'];
                                }

                              }
                        }
                    ?></td>
                </tr>
                <tr>
                  <th>Child Products </th>
                   <td><?php
                        if(!empty($child_products)){
                              foreach($child_products as $val){                                
                                  //echo base_url().'product_management/product_view/'.$val['id'];                               
                                //echo '<a href="'.base_url().'product_management/product_view/'.$val['id'].'" target="_blank">'.$val['id'].'</a> -'.$val['sku_number'].' - '.$val['stock_count'].'<br/>';  
                                 echo '<a href="'.base_url().'product_management/product_edit/'.$val['id'].'" target="_blank">'.$val['id'].'</a> -'.$val['sku_number'].' - '.$val['stock_count'].'<br/>';      

                              }
                        }
                    ?></td>
                </tr>
        </table>
      </div>
    </div>

  </div>
</div>
<div class="col-md-6">
  <div class="box">
    <div class="box-body">
        <div class="table-responsive">
      <table class="table table-bordered table-striped dataTable table-vth" id="datatable">
        <tr>
         <th style="width:30%;">Product Image</th>
         <!--<?php echo  @$productData[0]->image; ?>-->
         <?php                     
            if(in_array($product_info[0]['brand_id'],$imagify_brandid))
            {
               $productUrl = $this->config->item('products_small').$product_info[0]['image'];
            }else
            {
              $productUrl =  base_url().'assets/products/thumb_124/'.$product_info[0]['image'];                
            }

          ?>
          <td>
            <div class="prod-image-wrp">
            <img src="<?php echo @$productUrl;?>" onerror="this.onerror=null; this.src='https://i.imgur.com/Qu4zXlp.jpg';">
          </div>
          </td>
        </tr>
        <tr>
         <th style="width:30%;">Extra Image</th>
         <!--<?php echo  @$productData[0]->image; ?>-->
         <?php                     
            if(in_array($product_info[0]['brand_id'],$imagify_brandid))
            {
               $productUrl = $this->config->item('products_small').$product_info[0]['image'];
            }else
            {
              $productUrl =  base_url().'assets/products/thumb_124/'.$product_info[0]['image'];                
            }

          ?>
          <td>
            <div style="display: block;
                    margin: 0px;
                    padding: 0px;
                    top: 90px;
                    height: auto;
                    max-width: auto;
                    overflow-y: hidden;
                    overflow-x:auto;">
            <img src="<?php echo @$productUrl;?>" onerror="this.onerror=null; this.src='https://i.imgur.com/Qu4zXlp.jpg';">
            <img src="<?php echo @$productUrl;?>" onerror="this.onerror=null; this.src='https://i.imgur.com/Qu4zXlp.jpg';">
            <img src="<?php echo @$productUrl;?>" onerror="this.onerror=null; this.src='https://i.imgur.com/Qu4zXlp.jpg';">
            <img src="<?php echo @$productUrl;?>" onerror="this.onerror=null; this.src='https://i.imgur.com/Qu4zXlp.jpg';">
          </div>
          </td>
        </tr> 
        <tr>
          <th>Active </th>
          <td><?php if(@$product_info[0]['status'] == '1'){ echo "YES"; }else{echo "NO";} ?></td>
        </tr>
        <?php if(@$product_info[0]['approve_reject'] == 'R') { ?>
       <tr>
         <th>Reason </th>
         <td><?php if(@$product_info[0]['reason']){ echo $product_info[0]['reason']; } ?></td>
       </tr>
       <?php } ?>
      </table>
      </div>
        </div>
      </div>
      </div>

</div>
</section>
<script type="text/Javascript">
$( document ).ready(function() {
	$('.nav-item-product').addClass("active");
});
</script>
