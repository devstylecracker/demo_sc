<!-- Main content -->
<!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.min11.css" />
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>-->

<!--sumoselect-->
<!--link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/sumoselect/sumoselect.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/sumoselect/jquery.sumoselect.min.js"></script-->

<!--pqSelect dependencies-->
<!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev111.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev.js"></script>-->

<!-- product dependencies -->

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/redmond/jquery-ui.css" />    
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<!--pqSelect dependencies-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev.js"></script>

<script src="<?php echo base_url(); ?>assets/js/product.js" type="text/javascript"></script>
<script>
$( document ).ready(function() {
  //$("select:not([multiple])").addClass("select-box");
  //$(".select-box").SumoSelect();
  //$(".multi-select-box").SumoSelect({selectAll: true });
});
</script>
<?php $users_menu=$this->data['users_menu']; ?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Product Details</h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>home">Home</a></li>
      <li><a href="<?php echo base_url();?>product_new">Manage Products</a></li>
      <li class="active">Edit Product Details</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content page-product">
  <div class="row">
    <div class="col-md-12">
      <div class="box ">
        <div class="box-header">
          <h3 class="box-title">Edit Product Details</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" id="product_form" enctype="multipart/form-data" method="POST">
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">

                <div class="form-group">
                  <label for="ProductName">Product Name <span class="text-red">*</span></label>
                  <input type="text" name="product_name" tabindex="1" id="product_name" value="<?php if(isset($product_info[0]['name'])){ echo $product_info[0]['name']; } else{echo set_value('product_name');} ?>" placeholder="Enter Product Name" class="form-control">
                  <span class="text-red"> <?php echo form_error('product_name'); ?></span>
                </div>

                <div class="form-group">
                  <label for="ProductDesc">Product Description</label>
                  <textarea tabindex="3" name="product_desc" id="product_desc" placeholder="Enter Description" rows="3" class="form-control"><?php if(isset($product_info[0]['description'])){ echo $product_info[0]['description']; } else{echo set_value('product_desc'); } ?></textarea>
                </div>

                <div class="form-group">
                  <label for="Brand">Store  </label>
                  <select tabindex="5" id="brand" name="brand" class="form-control" tabindex="-1">
                    <option value="">Select Store</option>
                    <?php
						if(!empty($brand_data)){
								foreach($brand_data as $val){
                  if($product_info[0]['brand_id'] == $val['id']){
									echo '<option value="'.$val["id"].'" selected>'.$val["user_name"].'</option>';
                    }else{

                  echo '<option value="'.$val["id"].'">'.$val["user_name"].'</option>';
                    }
								}
						}
                    ?>
                  </select>
                </div>
               

                <div class="form-group-product-tags-wrp">
                  <label for="productTags">Tags</label>
                  <select placeholder="Select Tags" tabindex="19" data-placeholder="Choose Tags" name="product_tags[]" id="product_tags" class="form-control multi-select-box chosen-select" multiple tabindex="">
                    <?php
					if(!empty($tag_data)){
								foreach($tag_data as $val){
                   if(in_array($val->id,$product_tags)){
									   echo '<option value="'.$val->id.'" selected>'.$val->name.'</option>';
                   }else{

                    echo '<option value="'.$val->id.'">'.$val->name.'</option>';
                   }
								}
					}
                    ?>
                    </select>
                </div>

                <div class="form-group">
                  <label for="ProductURL">Product URL </label>
                  <input type="text" tabindex="15" placeholder="Enter Product URL" id="product_url" name="product_url" class="form-control" value="<?php if(isset($product_info[0]['url'])){ echo $product_info[0]['url']; } else{echo set_value('product_url'); } ?>">
                </div>
              <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="category">Category  <span class="text-red">*</span></label>
                  <select class="form-control" tabindex="7"  name="category" id="category">
                    <option value="">Select Category</option>
					<?php
						if(!empty($category)){
									foreach($category as $val){
                    if(@$product_info[0]['product_cat_id'] == $val->id){
										  echo '<option value="'.$val->id.'" selected>'.$val->name.'</option>';
                    }else{
                      echo '<option value="'.$val->id.'">'.$val->name.'</option>';
                    }
									}
						}
                    ?>
                  </select>
                   <span class="text-red"> <?php echo form_error('category'); ?></span>
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                  <label for="subCategory">Sub Category  <span class="text-red">*</span></label>
                  <select tabindex="9" class="form-control"  name="sub_category" id="sub_category" >
                    <option value="">Select Subcategory</option>
                  </select>
                  <span class="text-red"> <?php echo form_error('sub_category'); ?></span>
                </div>
                </div>

                <div class="col-md-12">
                <div class="form-group">
                  <div id="variation_type" class="prod-variation-type" ></div>
                </div>
                </div>

                <div class="col-md-6">

                <!--div class="form-group">
                  <label for="exampleInputEmail1">Tags </label>
                  <input type="text" placeholder="Enter Tags" id="exampleInputEmail1" class="form-control">
                </div-->
                <div class="form-group">
                  <label for="budget">Price Range  <span class="text-red">*</span></label>
                  <select placeholder="Select Price Range" tabindex="23" id="product_price_range" name="product_price_range" class="form-control chosen-select">
					  <option value="">Select Price Range</option>
                  <?php
						if(!empty($budget)){
									foreach($budget as $val){
                    if($product_info[0]['price_range'] == $val['id'] ){
										  echo '<option value="'.$val['id'].'" selected>'.$val['answer'].'</option>';
                    }else{
                      echo '<option value="'.$val['id'].'">'.$val['answer'].'</option>';
                    }
									}
						}
                    ?>
                   </select>
                  <span class="text-red"> <?php echo form_error('product_price_range'); ?></span>
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                  <label for="Price">Price <span class="text-red">*</span></label>
                  <input type="text" tabindex="11" placeholder="Enter Price" name="product_price" id="product_price" class="form-control" value="<?php if(isset($product_info[0]['price'])){ echo $product_info[0]['price']; } else{ echo set_value('product_price'); } ?>" > <span class="text-red"> <?php echo form_error('product_price'); ?></span>
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                  <label for="targetMarket">Target Market</label>
                  <select placeholder="Select Target Market" tabindex="23" id="product_target_market" name="product_target_market" class="form-control chosen-select">
					  <option value="">Select Target Market</option>
                     <?php
						if(!empty($target_marget)){
									foreach($target_marget as $val){
                    if($product_info[0]['target_market'] == $val->id){
										echo '<option value="'.$val->id.'" selected>'.$val->target_market.'</option>';
                  }else{
                     echo '<option value="'.$val->id.'">'.$val->target_market.'</option>';
                  }
									}
						}
                    ?>
                  </select>
                </div>
                </div>
                <div class="col-md-6">
                 <div class="form-group">
                  <label for="ConsumeType">Consumer Type</label>
                  <select placeholder="Select Consumer Type" tabindex="25" id="product_consumer_type" name="product_consumer_type" class="form-control" >
                    <option value="">Select Consumer Type </option>
                    <?php
                      $mat = ''; $fem=''; $mal=''; $kid=''; $pmal=''; $pfem='';
                      if($product_info[0]['consumer_type'] == "Maternity &amp; Baby"){
                        $mat = "selected";
                      }else if($product_info[0]['consumer_type'] == "Female"){
                        $fem = "selected";
                      }else if($product_info[0]['consumer_type'] == "Male"){
                        $mal = "selected";
                      }else if($product_info[0]['consumer_type'] == "Kidswear"){
                        $kid = "selected";
                      }else if($product_info[0]['consumer_type'] == "Plus Male"){
                        $pmal = "selected";
                      }else if($product_info[0]['consumer_type'] == "Plus Female"){
                        $pfem = "selected";
                      }
                    ?>
                    <option value="Maternity &amp; Baby" <?php echo $mat; ?>>Maternity & Baby</option>
                    <option value="Female" <?php echo $fem; ?>>Female</option>
                    <option value="Male" <?php echo $mal; ?>>Male</option>
                    <option value="Kidswear" <?php echo $kid; ?>>Kidswear</option>
                    <option value="Plus Male" <?php echo $pmal; ?>>Plus Male</option>
                    <option value="Plus Female" <?php echo $pfem; ?>>Plus Female</option>
                  </select>
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                  <label for="ProductAvail">Brand  <span class="text-red">*</span></label>
                  <select placeholder="Select Product Availabilty" tabindex="27" id="product_avail" value="" name="product_avail[]"class="form-control  multi-select-box chosen-select" multiple>

                    <?php
						if(!empty($all_stores)){
									foreach($all_stores as $val){
										if(in_array($val['id'],$product_avalibility)){
											echo '<option value="'.$val['id'].'" selected>'.$val['brand_store_name'].'</option>';
										}else{
											echo '<option value="'.$val['id'].'">'.$val['brand_store_name'].'</option>';
											}
										$i++;
									}
						}
                    ?>
                  </select>
                  <span class="text-red"> <?php echo form_error('product_avail'); ?></span>
                </div>

                <div class="form-group">
            <textarea name="product_reject_reason" id="product_reject_reason" style="display:none">  </textarea>
          </div>
          
                </div>
                <div class="col-md-6">
                <div class="form-group">
                  <label for="Rating">Rating</label>
                  <input type="text" tabindex="10" placeholder="Enter your rating" name="product_rate" id="product_rate" class="form-control" value="<?php if($product_info[0]['rating']){ echo $product_info[0]['rating']; } else{ echo set_Value('product_rate'); } ?>">
                </div>
              </div>
            </div>
              </div>

              <div class="col-md-6">
                <div class="form-group prod-image-outer">
                  <label for="Uploadimage">Upload Product Image<span class="text-red">*</span></label>
                  <?php if(@$product_info[0]['image']) {
                      echo '<div class="prod-image-wrp">';
                      echo '<img src="'.base_url().'assets/products/thumb_160/'.$product_info[0]['image'].'">';
                      echo '</div>';
                    } ?>
                  <!--  <div class="btn btn-default btn-file">
                    <input id="product_images" name="product_images" class="file111" type="file" >
                  </div> -->
                    <input id="product_images" name="product_images" type="file" >
                    <span class="help-block">Max file size: 2MB. | Allowed format: .jpg, .jpeg, .png, .gif</span>
                     <span class="text-red"> <?php if(isset($error)) { echo $error[0]; } ?></span>
                </div>

                <div class="form-group prod-image-outer multi-img-wrp">

                  <div class="multi-img-wrp">
                    <label class="control-label">Product Extra Images</label>

                              <div class="row">
                            <?php if(!empty($get_extra_images)){
                                foreach($get_extra_images as $val){
                                  ?>
                                  <div class="col-md-2">
                                      <div class="img-wrp">
                                    <img src="<?php echo base_url(); ?>assets/products_extra/thumb/<?php echo $val['product_images']; ?>">
                                    <a href="javascript:void(0);" class="text-red" onclick="delete_extra_img(<?php echo $val['id']; ?>);"><i class="fa fa-close"></i></a>
                                  </div>
                                    </div>
                                  <?php
                                }
                              } ?>
                            </div>
    </div>
                        <div class="form-group">
                                    <input type="file" name="extra_images[]" id="extra_images" multiple="true">
                                    <span class="help-block">Max file size: 2MB. | Allowed format: .jpg, .jpeg, .png, .gif</span>
                          </div>
  </div>


                <div class="row">
                <div class="col-md-6">
                <div class="form-group">
                 <label for="BodyPart">Body Part</label>
                  <select placeholder="Select Body Part" tabindex="6" id="body_part" name="body_part" class="form-control" >
                    <option value="">Select Body Part </option>
                    <option value="Head gear / Hair"
                    <?php if($product_info[0]['body_part'] == 'Head gear / Hair'){ echo 'selected'; } ?>>Head gear / Hair</option>
                    <option value="Ears" <?php if($product_info[0]['body_part'] == 'Ears'){ echo 'selected'; } ?>>Ears</option>
                    <option value="Eyes" <?php if($product_info[0]['body_part'] == 'Eyes'){ echo 'selected'; } ?>>Eyes</option>
                    <option value="Nose" <?php if($product_info[0]['body_part'] == 'Nose'){ echo 'selected'; } ?>>Nose</option>
                    <option value="Neck" <?php if($product_info[0]['body_part'] == 'Neck'){ echo 'selected'; } ?>>Neck</option>
                    <option value="Arms" <?php if($product_info[0]['body_part'] == 'Arms'){ echo 'selected'; } ?>>Arms</option>
                    <option value="Wrist" <?php if($product_info[0]['body_part'] == 'Wrist'){ echo 'selected'; } ?>>Wrist</option>
                    <option value="Fingers" <?php if($product_info[0]['body_part'] == 'Fingers'){ echo 'selected'; } ?>>Fingers</option>
                    <option value="Torso" <?php if($product_info[0]['body_part'] == 'Torso'){ echo 'selected'; } ?>>Torso</option>
                    <option value="Feet" <?php if($product_info[0]['body_part'] == 'Feet'){ echo 'selected'; } ?>>Feet</option>
                    <option value="Shoes" <?php if($product_info[0]['body_part'] == 'Shoes'){ echo 'selected'; } ?>>Shoes</option>
                    <option value="Accessories" <?php if($product_info[0]['body_part'] == 'Accessories'){ echo 'selected'; } ?>>Accessories</option>
                  </select>
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                 <label for="BodyType">Body Type</label>
                 <select name="body_type" id="body_type" class="form-control">
                   <?php
						if(!empty($body_type)){
									foreach($body_type as $val){
                    if(@$product_info[0]['body_type'] == $val['id']){
										echo '<option value="'.$val['id'].'" selected>'.$val['answer'].'</option>';
                    }else{
                      echo '<option value="'.$val['id'].'">'.$val['answer'].'</option>';

                    }
									}
						}
                    ?>
                  </select>
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                 <label for="BodyShape">Body Shape</label>
                  <select name="body_shape" id="body_shape" class="form-control">
                   <?php
						if(!empty($body_shape)){
									foreach($body_shape as $val){
                    if(@$product_info[0]['body_shape'] == $val['id']){
										echo '<option value="'.$val['id'].'" selected>'.$val['answer'].'</option>';
                    }else{
                    echo '<option value="'.$val['id'].'">'.$val['answer'].'</option>';
                    }
									}
						}
                    ?>
                  </select>
                </div>
                </div>
                <div class="col-md-6">

                <div class="form-group">
                 <label for="Personality">Style</label>
                 <select name="style" id="style" class="form-control">
                   <?php
						if(!empty($style)){
									foreach($style as $val){
                      if(@$product_info[0]['personality'] == $val['id']){
										echo '<option value="'.$val['id'].'" selected>'.$val['answer'].'</option>';
                    }else{
                     echo '<option value="'.$val['id'].'">'.$val['answer'].'</option>';
                    }
									}
						}
                    ?>
                  </select>
                </div>
                </div>
                <div class="col-md-6">

                 <div class="form-group">
                 <label class="control-label" for="age">Age Group</label>
                  <select name="age" id="age" class="form-control">
                  <?php
						if(!empty($age)){
									foreach($age as $val){
                    if(@$product_info[0]['age_group'] == $val['id']){
										echo '<option value="'.$val['id'].'" selected >'.$val['answer'].'</option>';
                    }else{
                      echo '<option value="'.$val['id'].'">'.$val['answer'].'</option>';
                    }
									}
						}
                    ?>
                    </select>
                </div>
                </div>

                <div class="col-md-12">
                    <div class="row">
                    <div class="col-md-6">

					<div class="form-group">
					  <label class="control-label">Active<span class="text-red"> * </label>
					  <div>  <label class="radio-inline"> <input type="radio" name="product_status" value="1" <?php if($product_info[0]['status'] == 1) { echo 'checked'; } ?>>Yes</label>
						<label class="radio-inline"> <input type="radio" name="product_status" value="0" <?php if($product_info[0]['status'] == 0) { echo 'checked'; } ?>> No</label>
					  </div>
					   <span class="text-red"> <?php echo form_error('product_status'); ?></span>
					</div>
        </div>
        <div class="col-md-6">
					 <div class="form-group">
					  <label class="control-label">Promotional<span class="text-red"> * </label>
					  <div>  <label class="radio-inline"> <input type="radio"  name="promotional" value="1" <?php
            if($product_info[0]['is_promotional'] == 1) { echo 'checked'; } ?>>Yes</label>
						<label class="radio-inline"> <input type="radio" name="promotional" value="0" <?php
            if($product_info[0]['is_promotional'] == 0) { echo 'checked'; } ?>> No</label>
					  </div>
					   <span class="text-red"> <?php echo form_error('promotional'); ?></span>
					</div>

           
                </div>
</div>
</div>
                </div>

                <div class="col-md-12">

                            <div class="form-group">
                              <label class="control-label">Product Inventory</label>
                                <div id="product_inventory">
                                <?php $i = 1; if(!empty($product_inventory)){ ?>
                                <?php foreach ($product_inventory as $key => $value) { ?>
                                  <div class="row" id="row<?php echo $i; ?>">
                                    <div class="col-md-3">
                                      <input type="text" name="size<?php echo $i; ?>" id="size<?php echo $i; ?>" placeholder="Size" class="form-control" value="<?php echo $value['size_text']; ?>">
                                    </div>
                                    <div class="col-md-3">
                                      <input type="text" name="qty<?php echo $i; ?>" id="qty<?php echo $i; ?>" placeholder="Qty" class="form-control" value="<?php echo $value['stock_count']; ?>">
                                    </div>
                                    <div class="col-md-3">
                                      <input type="text" name="product_sku<?php echo $i; ?>" id="product_sku<?php echo $i; ?>" placeholder="Product SKU" class="form-control" value="<?php echo $value['product_sku']; ?>">
                                    </div>
                                    <div class="col-md-3">
                                      <span class="btn btn-primary btn-xs btn-delete" onclick="remove_row(<?php echo $i; ?>);"><i class="fa fa-minus"></i></span>
                                    </div>
                                  </div>
                                  <?php $i++; } ?>
                                <?php } ?>

                                   <div class="row" id="row<?php echo $i; ?>">
                                    <div class="col-md-3">
                                      <input type="text" name="size<?php echo $i; ?>" id="size<?php echo $i; ?>" placeholder="Size" class="form-control" value="">
                                    </div>
                                    <div class="col-md-3">
                                      <input type="text" name="qty<?php echo $i; ?>" id="qty<?php echo $i; ?>" placeholder="Qty" class="form-control" value="">
                                    </div>
                                    <div class="col-md-3">
                                      <input type="text" name="product_sku<?php echo $i; ?>" id="product_sku<?php echo $i; ?>" placeholder="Product SKU" class="form-control" value="">
                                    </div>
                                    <div class="col-md-3">
                                      <span class="btn btn-primary btn-xs" onclick="addnew_row(<?php echo $i; ?>);"><i class="fa fa-plus"></i></span>
                                    </div>
                                  </div>

                                </div>
                              <input type="hidden" name="total_rows" id="total_rows" value="<?php echo count($product_inventory) >0 ? count($product_inventory)+1 : $i; ?>">
                             </div>
                      </div>

              </div>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
          <button class="btn btn-sm btn-primary" tabindex="30" type="submit"><i class="fa fa-save"></i> Save</button>
           <a href="<?php echo base_url(); ?>product_new"><button type="button" class="btn btn-primary  btn-sm btn-rad"><i class="fa fa-reply"></i> Back</button></a>
          <?php if(in_array("43",$users_menu)) { ?>
          <button class="btn btn-sm btn-primary" tabindex="30" type="button" id="approved"><i class="fa fa-save"></i> Approved</button>
          <?php } ?>
           <?php if(in_array("43",$users_menu)) { ?>
          <button class="btn btn-sm btn-primary" tabindex="30" type="button" id="reject"><i class="fa fa-save"></i> Reject</button>
          <?php } ?>
          <!-- <button class="btn btn-primary" type="submit">Edit</button>
          <button class="btn btn-primary" type="submit">Approved/Pending</button> -->
        </div>
        <input type="hidden" name="product_id" id="product_id" value="<?php echo @$product_info[0]['id']; ?>">


        </form>
      </div>
    </div>
  </div>
</section><!-- /.content -->
<script type="text/Javascript">
 $(document).ready(function(e){
    get_sub_cat("<?php echo @$product_info[0]['product_cat_id']; ?>","<?php echo @$product_info[0]['product_sub_cat_id']; ?>");
    get_variation_type("<?php echo @$product_info[0]['product_sub_cat_id']; ?>","<?php echo @$product_info[0]['id']; ?>");
 	$('.nav-item-product').addClass("active");
 });


</script>
