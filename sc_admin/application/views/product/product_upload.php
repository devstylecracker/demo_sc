<!-- Content Header (Page header) -->
<?php
    if(!isset($error))
    {
      $error = "";
    }

  // print_r($error);
?>
<section class="content-header">
    <h1>Product Upload</h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>home">Home</a></li>
      <li><a href="#">Manage Products</a></li>
      <li class="active">Product Upload</li>
    </ol>
</section>
<section class="content">

  <div class="row">
  <?php echo $this->session->flashdata('feedback'); ?>
    <form role="form" id="product_uploadform" enctype="multipart/form-data" method="POST" action="<?php echo base_url(); ?>product_upload/product_upload/" onsubmit="return validate()">
      <div class="col-md-6">
        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-upload"></i> Upload Product CueSheet <small class="label label-info"></small></h3>
              <div class="pull-right">
              </div>
              </div>
              <div class="box-body">
                  <p>Browse Que Sheet <input id="product_upload" name="product_upload" class="file1" type="file"></p>
                  <p class="help-block">Max file size: 5MB | Allowed format: .xls, .xlsx</p>
                  <button class="btn btn-primary btn-sm" type="submit" name = "Save"  value="Save"><i class="fa fa-upload"></i> Upload</button>
                  <a href="<?php echo base_url(); ?>product_new" ><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i> Back</button></a>
              </div>
              <span class="text-red"> <?php /*if(@$errorCount!="") { echo "Error Inserting Records: ".@$errorCount."<br/>";}
                                                if(@$successcount!="") { echo "Success  Inserting Records: ".@$successcount."<br/>"; } */
                                                if(@$imagemsg!="") { echo "<b> Error in Uploading Image :</b> ".@$imagemsg."<br/>"; }

                                                if(@$excelmsg!="")
                                                  {
                                                     echo "<b> Error in Excel Sheet :</b> <br/>";
                                                    foreach($excelmsg  as $ex)
                                                    {
                                                      echo @$ex."<br/>";
                                                    }

                                                  }

                                                ?>
              </span>
         </div>
      </div>
      </form>
      <div class="col-md-6">
          <div class="box">
                  <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-book"></i> Help <small class="label label-info"></small></h3>
                    <div class="pull-right">
                      </div>
                    </div>
                      <div class="box-body">
                      <p class="text-orange">Download sample Que Sheet  <a href="<?php echo base_url(); ?>product_upload/download_cuesheet/" class="btn btn-primary btn-sm"><i class="fa fa-download"></i> Download</a></p>
                        <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="Brand">Brand  </label>
                            <select class="form-control" name="brand" id="brand" tabindex="5">
                              <option value="">Select Brand</option>
                              <option value="27">faballey</option><option selected="" value="23">Koovs</option><option value="21">nike</option>                  </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="Brand">Price Range  </label>
                            <select class="form-control" name="brand" id="">
                              <option value="">Select</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="Brand">Consumer Type  </label>
                            <select class="form-control" name="brand" id="">
                              <option value="">Select</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="Brand">Body Part  </label>
                            <select class="form-control" name="brand" id="">
                              <option value="">Select</option>
                              </select>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="Brand">Age Group  </label>
                            <select class="form-control" name="brand" id="">
                              <option value="">Select</option>
                              </select>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="Brand">Image </label>
                            <p>Physical path to upload image</p>
                          </div>
                        </div>

                      </div>
                      </div>



                    </div>
        </div>
      </div>
  </section><!-- /.content -->

<script type="text/Javascript">

    function validate() {
      var extension;
      var filename=document.getElementById('product_upload').value;
      extension=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();

      if(extension == 'xlsx' || extension == 'xls')
      {
        return true;
      }else
      {
        alert('This File Is Not Excel');
        return false;
      }

    }
	$( document ).ready(function() {
$('.nav-item-product').addClass("active");
)};
</script>
