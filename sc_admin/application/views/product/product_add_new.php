<script type="text/javascript">
  var baseUrl = '<?php echo base_url(); ?>';
</script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/product.js"></script>
<?php $users_menu=$this->data['users_menu']; ?>
<section class="content-header">
  <h1>Product Details</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url();?>product_management">Manage Products</a></li>
    <li class="active">Add Product Details</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
<form name="frmproductadd" id="frmproductadd" method="post" enctype="multipart/form-data">
<!-- new -->
  <div class="box ">
        <div class="box-header">
            <h3 class="box-title">Product Basic</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
      
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="ProductName">Product Name <span class="text-red">*</span></label>
                  <input type="text" name="product_name" tabindex="1" id="product_name" value="<?php echo set_value('product_name'); ?>" placeholder="Enter Product Name" class="form-control" onblur="genarate_slug();">
                  <span class="text-red"> <?php echo form_error('product_name'); ?></span>
                </div>
            </div>
               <div class="col-md-6">
                <div class="form-group">
                  <label for="ProductName">Product Slug <span class="text-red">*</span></label>
                  <input type="text" name="product_slug" tabindex="1" id="product_slug" value="<?php echo set_value('product_slug'); ?>" placeholder="Enter Product Name" class="form-control">
                  <span class="text-red"> <?php echo form_error('product_slug'); ?></span>
                </div>
            </div>             
            <div class="col-md-6">
                <div class="form-group">
                  <label for="ProductDesc">Product Description</label>
                  <textarea tabindex="3" name="product_desc" id="product_desc" placeholder="Enter Description" rows="3" class="form-control"><?php echo set_value('product_desc'); ?></textarea>
                </div>
            </div>        
            <div class="col-md-3">
                    <div class="form-group">
                      <label for="Sku">Product SKU <span class="text-red">*</span></label>
                      <input type="text" tabindex="11" placeholder="Enter product sku" name="sku_number" id="sku_number" class="form-control" value="<?php echo set_value('sku_number'); ?>" > <span class="text-red"> <?php echo form_error('sku_number'); ?></span>

                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="Stock">Product Stock <span class="text-red">*</span></label>
                      <input type="text" tabindex="11" placeholder="Enter Product Stock" name="stock_count" id="stock_count" class="form-control" value="<?php echo set_value('stock_count'); ?>" > <span class="text-red"> <?php echo form_error('stock_count'); ?></span>

                    </div>
                  </div>
             <div class="col-md-3">
                    <div class="form-group">
                      <label for="Price">Price <span class="text-red">*</span></label>
                      <input type="text" tabindex="11" placeholder="Enter Price" name="product_price" id="product_price" class="form-control" value="<?php echo set_value('product_price'); ?>" > <span class="text-red"> <?php echo form_error('product_price'); ?></span>
                    </div>
                  </div>
				  
			<div class="col-md-3">
					<div class="form-group">
					  <label for="Price">MRP Price</label>
					  <input type="text" tabindex="11" placeholder="Enter Price" name="compare_price" id="compare_price" class="form-control" value="<?php if(isset($product_info[0]['compare_price'])){ echo $product_info[0]['compare_price']; } else{ echo set_value('compare_price'); } ?>" > <span class="text-red"> <?php echo form_error('compare_price'); ?></span>
					</div>
			</div>

        <div class="col-md-3">
        <div class="form-group">
          <label for="Price">Discount Start Date</label>
          <input type="text" tabindex="11" placeholder="Enter Price" name="discount_start_date" id="discount_start_date" class="form-control datetimepicker" value="<?php if(isset($product_info[0]['discount_start_date'])){ echo $product_info[0]['discount_start_date']; } else{ echo set_value('discount_start_date'); } ?>" > <span class="text-red"> <?php echo form_error('discount_start_date'); ?></span>
        </div>
     </div>

      <div class="col-md-3">
        <div class="form-group">
          <label for="Price">Discount End Date</label>
          <input type="text" tabindex="11" placeholder="Enter Price" name="discount_end_date" id="discount_end_date" class="form-control datetimepicker" value="<?php if(isset($product_info[0]['discount_end_date'])){ echo $product_info[0]['discount_end_date']; } else{ echo set_value('discount_end_date'); } ?>" > <span class="text-red"> <?php echo form_error('discount_end_date'); ?></span>
        </div>
     </div>

               <div class="col-md-3"> 
                    <div class="form-group">                        
                            <label for="Active">Active <span class="text-red">*</span></label>
                            <select name="product_status" id="product_status" class="form-control">
                              <option value="1">Yes</option>
                              <option value="0">No</option>
                            </select>
                      </div>
                      <span class="text-red"> </span>
                </div>
                <div class="col-md-3"> 
                    <div class="form-group">                        
                          <label for="Active">Parent SKU<span class="text-red">*</span></label>
                          <input type="text" tabindex="11" placeholder="Parent SKU" name="parent_sku" id="parent_sku" class="form-control" value="<?php if(isset($product_info[0]['parent_id'])){ echo $product_info[0]['parent_id']; } else{ echo set_value('parent_sku'); } ?>" > <span class="text-red"> <?php echo form_error('parent_sku'); ?></span>
                      </div>
                      <span class="text-red"> </span>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                  <label for="Brand">Store  </label>
                  <select tabindex="5" id="brand1" name="brand" class="form-control chosen-select" tabindex="-1">
                    
                    <?php
                    if(!empty($brand_data)){
                      foreach($brand_data as $val){

                          echo '<option value="'.$val["id"].'">'.$val["user_name"].'</option>';
                        
                      }
                    }
                    ?>
                  </select>
                </div>
              </div>
                
                 <div class="col-md-6">
                    <div class="form-group">
                      <label for="ProductAvail">Brand  <span class="text-red">*</span></label>
                      <!-- <select tabindex="27" id="product_avail1" value="" name="product_avail" class="form-control chosen-select" > -->
                      <select tabindex="27" id="product_avail1" value="" name="product_avail[]" class="form-control chosen-select" multiple>                        
                        <?php
                        if(!empty($all_stores)){ 
                          foreach($all_stores as $val){
                              echo '<option value="'.$val['id'].'">'.$val['brand_store_name'].'</option>';
                          }
                        }
                        ?>
                      </select>
                      <span class="text-red"> <?php echo form_error('product_avail'); ?></span>
                    </div>
                  </div>
                  


                </div>
           
            </div>
          </div>
<!-- -->
<!-- new -->
  <div class="box ">
        <div class="box-header">
            <h3 class="box-title">Product Grouping</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
      
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                    <div class="form-group">
                      <label for="category">Category  <span class="text-red">*</span></label>
                      <select class="form-control chosen-select" tabindex="7"  name="category[]" id="category" multiple="">
                       <option value="-1">None</option>
                           <?php if(!empty($cat_data)){
                              foreach($cat_data as $val){
                                  echo $val;
                              }

                              } ?>
                      </select>
                      <span class="text-red"> <?php echo form_error('category'); ?></span>
                    </div>
                  </div>
                   <div class="col-md-6">
                  <div class="form-group">
                  <label for="productTags">Tags</label>
                  <select tabindex="19" name="product_tags[]" id="product_tags1" class="form-control chosen-select" multiple tabindex="">
                   
                    <?php
                    if(!empty($tag_data)){
                      foreach($tag_data as $val){
                        if(set_value('product_tags') == $val->id){
                          echo '<option value="'.$val->id.'">'.$val->name.'</option>';
                        }else{

                          echo '<option value="'.$val->id.'">'.$val->name.'</option>';
                        }
                      }
                    }
                    ?>
                  </select>
                </div>
                 </div>
            </div>
            </div>
            </div>

<!-- new -->
    <div class="box ">
      <div class="box-header">
          <h3 class="box-title">Product Attribute Mapping</h3>
      </div><!-- /.box-header -->
      <!-- form start --> 
      <div class="row">
        <div class="box-body">        
                <div class="col-md-12">
                  <div class="form-group">
                  <!--<label for="productTags">Attributes -</label><br/>-->
                  
                     <!--<option value="">Select Attributes</option>-->       
                     <?php if(!empty($all_attributes)) { ?>            
                        <?php foreach ($all_attributes as $value) { ?>
                          <div class="col-md-6">
                          <label for="productTags"><?php echo $value['attribute_name'].' :'; ?></label>
                          <select tabindex="19" name="attributes[]" id="attributes" class="form-control chosen-select" multiple tabindex="">                            
                            <?php 
                            $attributes_value = $this->productmanagement_model->attributes_values($value['id']);

                            if(!empty($attributes_value)){
                              foreach($attributes_value as $val_data){ ?>
                                <option value="<?php echo $val_data['id']; ?>"><?php echo $val_data['attr_value_name']; ?></option>
                            <?php  }
                            }
                            ?></select> </div>  
                        <?php } ?>                       
                     <?php } ?>                   
                  </div>
                </div>
          </div>      
        </div> <br/>
    </div>

   <div class="box">
        <div class="box-header">
            <h3 class="box-title">Product Images</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
         <div class="row">
              <div class="col-md-6">
                   <div class="form-group">
                  <label for="Uploadimage">Upload Product Image<span class="text-red">*</span></label>
                    <div>
                  <input id="product_images" name="product_images" class="file1" type="file" >
                  <span class="help-block">Max file size: 2MB. | Allowed format: .jpg, .jpeg, .png, .gif</span>
                  <span class="text-red"> <?php if(isset($error)) { echo $error[0]; } ?></span>
                </div> </div>
                </div>

                 <div class="col-md-6">
                      <div class="form-group">
                                    <label class="control-label">Upload Product Extra Images</label>
                                      <div>
                                      <input type="file" name="extra_images[]" id="extra_images" multiple="true">
                                      <span class="help-block">Max file size: 2MB. | Allowed format: .jpg, .jpeg, .png, .gif</span>
                                      </div>
                                    </div>
                                  </div>


                </div>
              </div>
            </div>

    <div class="box-footer text-right">
                <button class="btn btn-sm btn-primary" tabindex="30" type="submit"><i class="fa fa-save"></i> Save</button>
                 <a href="<?php echo base_url(); ?>product_management"><button type="button" class="btn btn-primary  btn-sm btn-rad"><i class="fa fa-reply"></i> Back</button></a>
                <button class="btn btn-sm btn-default" tabindex="31" type="reset"><i class="fa fa-close"></i> Cancel</button>
               
              </div>
<!-- /new-->
</form>
        </div>
      </div>
    </section><!-- /.content -->
    <script type="text/javascript">
      function genarate_slug(){
        var category_name = $('#product_name').val();
        var slug = category_name.replace(/[^a-zA-Z0-9]/g, "-");
        $('#product_slug').val(slug.toLowerCase());
      }

    </script>
