<script type="text/javascript">
  var baseUrl = '<?php echo base_url(); ?>';
</script>
<script src="<?php echo base_url(); ?>assets/js/product_new.js?v=1.1" type="text/javascript"></script>
<?php
    if(!empty($this->data['users_menu']!=""))
    {
       $users_menu=$this->data['users_menu'];

    }else
    {
       $users_menu=array();
    }

    if(!empty($this->data['total_rows']))
    {
      $total_rows = $this->data['total_rows'];

    }else
    {
       $total_rows="";
    }
    //echo "<pre>"; print_r($product);exit;
    $imagify_brandid = unserialize(IMAGIFY_BRANDID); 
?>
  <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Manage Products</h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>home">Home</a></li>
      <li><a href="product_management">Products</a></li>
      <li class="active">Manage Products</li>
    </ol>
</section>
<section class="content page-manage-products">
        <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">List of Products <small class="label label-info"><?php echo @$total_rows; ?></small></h3>
                	<div class="pull-right">

                    <?php if(in_array("40",@$users_menu)) { ?>

                     <a href="<?php echo base_url(); ?>quesheet/manage"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Upload Product through CueSheet</button></a>

                      <a href="<?php echo base_url(); ?>product_management/product_add"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add Product</button></a>

                      <?php if($this->session->userdata('role_id') !=6 && !empty($_POST)){ ?>
                      <a href="<?php echo base_url(); ?>product_management/product_search_download/<?php echo $this->input->post('search_by'); ?>/<?php echo $this->input->post('table_search'); ?>"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-download"></i> Download Search Result</button></a>

                      <?php } ?>

                    <?php } ?>
                    </div>
                  </div>
                    <div class="box-body">
                         <?php echo $this->session->flashdata('feedback'); ?>
                     <form name="frmsearch" class="form-group" id="frmsearch" action="<?php echo base_url(); ?>product_management" method="post" novalidate>
                      <div class="row">
                        <div class="col-md-2">
                        <?php $search_by = set_value('search_by')=='' ? $search_by : set_value('search_by'); ?>
                            <select name="search_by" id="search_by" class="form-control input-sm">
                                <option value="0">Search by</option>
                              <option selected="selected" value="1" <?php echo $search_by==1 ? 'selected' : ''; ?>>Product Name</option>
                              <!--option value="2" <?php echo $search_by==2 ? 'selected' : ''; ?>>Category</option>
                              <option value="3" <?php echo $search_by==3 ? 'selected' : ''; ?>>Sub-Category</option>-->
                              <!--option value="4" <?php echo set_value('search_by')==4 ? 'selected' : ''; ?>>Created Date On</option-->
                              <?php if($this->session->userdata('role_id') !=6 ){ ?>
                              <option value="5" <?php echo $search_by==5 ? 'selected' : ''; ?>>Status</option>
                              <?php } ?>
                               <option value="6" <?php echo $search_by==6 ? 'selected' : ''; ?>>Product Id</option>
                              <option value="7" <?php echo $search_by==7 ? 'selected' : ''; ?>>Store Name</option>
                              <option value="8" <?php echo $search_by==8 ? 'selected' : ''; ?>>Category</option>
                              <option value="9" <?php echo $search_by==9 ? 'selected' : ''; ?>>SKU</option>
                              <!--<option value="10" <?php echo $search_by==10 ? 'selected' : ''; ?>>Women Search</option>
                              <option value="11" <?php echo $search_by==11 ? 'selected' : ''; ?>>Men Search</option>-->
                            </select>
                        </div>
    					<div class="col-md-5">
    					<div class="input-group">
                          <input type="text" name="table_search" value="<?php echo set_value('table_search')=='' && set_value('table_search')==0 ? urldecode($table_search) : urldecode(set_value('table_search')); ?>" id="table_search" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo set_value('table_search'); ?>" required=""/>
						  
						<!--<div class="side-by-side clearfix" id="store_data" style="">-->
							<select data-placeholder="Choose a Category..." style="width:100%; display:none;" id="table_search1" name="table_search1" class="chzn_table_search chosen-select select2 form-control input-sm pull-right" tabindex="4">
								<option value=""></option>
								<?php 
									 /* if(is_array($brand_store) && count($brand_store)>0)
										{
											foreach($brand_store as $getStore)
											{
												?>
												<option value="<?php echo $getStore['company_name']; ?>"><?php echo $getStore['company_name']; ?></option>
												<?php
											}
										}  */
									
								?>
							</select>
						<!--</div>-->
						  
						  
                            <div class="input-group-btn">
                              <button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        </div>
                        <div class="col-md-2">
        									<a href="product_management" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
        								</div>
        								<div class="col-md-3">
        									<!--div class="records-per-page pull-right">
        										<select name="records-per-page" class="form-control input-sm">
        											<option>Per Page</option>
        											<option value="10" selected="selected">10</option>
        											<option value="25">25</option>
        											<option value="50">50</option>
        										</select>
        									</div-->
    												<div class="pull-right">
                              <?php echo $this->pagination->create_links(); ?>
                          </div>

                        </div>
                        </div>
                    </form>



        <div>
<!-- new design -->
<div class="table-responsive">
<table class="table table-bordered table-striped table-products dataTable table-tr-collapse">
<thead>
                      <tr>
                        <th width="70">PID</th>
                        <th width="70">Image</th>
                        <th>Name</th>
                        <th >SKU</th>
                        <th width="100">Price</th>
                        <?php if($this->session->userdata('role_id') !=6 ){ ?>
						            <th width="70">Status</th>
                        <?php } ?>
                        <th width="140">Store</th>
                        <th width="134">Created Date</th>
                      </tr>
        </thead>
          <tbody>
        <?php
        if(!empty($product))
        {
          foreach($product as $singleproduct) {
        ?>
        <tr>
          <td><?php echo $singleproduct->id; ?></td>
          <td>
          <?php 

            /*if(in_array($singleproduct->brand_id,$imagify_brandid))
            {             
              $productUrl = $this->config->item('products_thumbnail').$singleproduct->image;
            }else
            {
               $productUrl =  base_url().'assets/products/thumb_124/'.$singleproduct->image;               
            }*/
             $productUrl =  base_url().'assets/products/'.$singleproduct->image;
				$extraImages = $this->productmanagement_model->extara_images($singleproduct->id);
			/* echo "<pre>";
			print_r($extraImages);
			echo "</pre>"; */				

          ?>
            	<div class="img-wrp">
					<div class="row">     
							<div class="col-12 col-md-4 col-sm-6">
						  <a title="Image 1" href="#"> 
							<img class="thumbnail img-responsive" id="<?php echo $singleproduct->id; ?>" src="<?php echo base_url().'assets/products/'.$singleproduct->image;?>" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
						  </a>
						</div>
				  </div>    
				</div>
			    <div class="hidden" id="img-repo">
					
					<div class="item" id="<?php echo $singleproduct->id; ?>">
					  <img class="thumbnail img-responsive" style="margin: 0 auto;" title=" <?php echo $singleproduct->name; ?>" src="<?php echo base_url().'assets/products/'.$singleproduct->image;?>" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
					</div>
					<?php 
						 if(is_array($extraImages) && count($extraImages))
						 {
							foreach($extraImages as $exImages)
							{
							//echo $exImages['product_images'];
							?>	
							<div class="item" id="<?php echo $exImages['product_id']; ?>">
							  <img class="thumbnail img-responsive" style="margin: 0 auto;" title=" <?php echo $singleproduct->name; ?>" src="<?php echo base_url().'assets/products_extra/'.$exImages['product_images'];?>" onerror="this.onerror=null; this.src='http://www.adbazar.pk/frontend/images/default-image.jpg';">
							</div>
						<?php
							} 
						 }
					?>
						<!--<div class="item" id="image-1">
						  <img class="thumbnail img-responsive" title="Image 11" src="<?php //echo base_url().'assets/products/thumb_546/'.$singleproduct->image;?>">
						</div>
						<div class="item" id="image-1">
						  <img class="thumbnail img-responsive" title="Image 12" src="<?php //echo base_url().'assets/products/thumb_546/'.$singleproduct->image;?>">
						</div>-->       
				</div>
				
				<div class="modal" id="modal-gallery" role="dialog">
				  <div class="modal-dialog">
					<div class="modal-content">
					  <div class="modal-header">
						  <button class="close" type="button" data-dismiss="modal">×</button>
						  <h3 class="modal-title"></h3>
					  </div>
					  <div class="modal-body">
						  <div id="modal-carousel" class="carousel">
							<div class="carousel-inner"></div>
							<a class="carousel-control left" href="#modal-carousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
							<a class="carousel-control right" href="#modal-carousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
						  </div>
					  </div>
					  <div class="modal-footer">
						  <button class="btn btn-default" data-dismiss="modal">Close</button>
					  </div>
					</div>
				  </div>
				</div>
          </td>
          <td>
          <div class="product-name" id="product_name_<?php echo $singleproduct->id; ?>">
            <?php echo $singleproduct->name; ?>
          </div>
            <div class="prod-btns-wrp-outer">
            <div class="prod-btns-wrp">
              <ul class="btns-list">

                <?php if(in_array("39",$users_menu)) { ?>
                  <li>
                   <a href="<?php echo base_url(); ?>product_management/product_view/<?php echo $singleproduct->id; ?>"> View</a>
                  </li>
                <?php } ?>

                <?php if(in_array("41",$users_menu) || in_array("43",$users_menu)) { ?>
                  <li>
                  <a href="javascrip:void(0)" onclick="get_prod_info(<?php echo $singleproduct->id; ?>);" class="btn-quick-edit"> Quick Edit</a>
                  </li>
                <?php } ?>

                <?php if(in_array("41",$users_menu) || in_array("43",$users_menu)) { ?>
                  <li>
                  <a href="<?php echo base_url(); ?>product_management/product_edit/<?php echo $singleproduct->id; ?>"> Edit</a>
                  </li>
                <?php } ?>

                <?php if(in_array("42",$users_menu)) { ?>
                  <li>
                    <a class="btn1 btn-primary1 btn-xs1 btn-delete deleteProd" data-id="<?php echo $singleproduct->id; ?>" data-toggle="confirmation" > Delete</a>
                    </li>

                <?php } ?>

                <li>
                <?php $short_url = 'https://www.stylecracker.com/product/details/'.$singleproduct->slug.'-'.$singleproduct->id; ?>
                <a href="<?php echo $short_url; ?>">Product URL</a>
                </li>
                </li>
                <li>
                <a href="<?php echo $this->config->item('product_url'); ?><?php echo $singleproduct->image; ?>" download="<?php echo $singleproduct->image; ?>" title="Download"><i class="fa fa-download"></i></a> </li>
                <li>
                <a href="javascript:void(0);" onclick="openwindow('<?php echo base_url(); ?>product_management/upload_product_image/<?php echo $singleproduct->id; ?>');" title="Upload"><i class="fa fa-upload"></i></a>
               </li>

              </ul>
            </div>
          </div>
          </td>
          <td><span > <?php echo $singleproduct->sku_number; ?></span></td>
          <td><i class="fa fa-inr"></i><span id="price_<?php echo $singleproduct->id; ?>"> <?php echo $singleproduct->price; ?></span></td>
          <?php if($this->session->userdata('role_id') !=6 ){ ?>
          <td>    <?php if($singleproduct->approve_reject=='A')
                    {
                      $apr_status = '<span class="text-green">Approved</span>';
                    }else
                    {
                        if($singleproduct->approve_reject=='R')
                        {
                          $apr_status = '<span class="text-red">Reject</span>';
                        }else
                        {
                          $apr_status = '<span class="text-orange">Pending</span>';
                        }
                    }
                ?>
              <?php echo $apr_status; ?>
            </td>
          <?php } ?>
          <td><?php echo $singleproduct->brand; ?></td>
          <td><?php echo $singleproduct->created_datetime; ?></td>
        <tr class="tr-quick-view">
          <td colspan="9">
            <div class="box-quick-view">
              <div class="box box-solid">
                <div class="box-header">
                    <h3 class="box-title">Quick Edit</h3>
                </div><!-- /.box-header -->
                    <div class="box-body">
                    <div class="box-inner">

          <div class="row">
              <div class="col-md-4">
                  <div class="form-inline1">
                <div class="form-group row">
                  <label for="" class="col-md-3">Title</label>
                  <div class="col-md-9">
                    <input type="text" name="product_name<?php echo $singleproduct->id; ?>" id="product_name<?php echo $singleproduct->id; ?>" class="form-control" value="<?php echo $singleproduct->name; ?>" onblur="genarate_slug(<?php echo $singleproduct->id; ?>);">
                    <span class="error"></span>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-md-3">Slug</label>
                    <div class="col-md-9">
                  <input type="text" name="product_slug<?php echo $singleproduct->id; ?>" id="product_slug<?php echo $singleproduct->id; ?>" class="form-control" value="<?php echo $singleproduct->slug; ?>">
                  <span class="error"></span>
                </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-md-3">Price</label>
                    <div class="col-md-9">
                  <input type="text" name="price<?php echo $singleproduct->id; ?>" id="price<?php echo $singleproduct->id; ?>" class="form-control" value="<?php echo $singleproduct->price; ?>">
                  <span class="error"></span>
                </div>
                </div>
				<div class="form-group row" style="display:none">
                  <label for="" class="col-md-3">MRP Price</label>
                    <div class="col-md-9">
                  <input type="text" name="compare_price<?php echo $singleproduct->id; ?>" id="compare_price<?php echo $singleproduct->id; ?>" class="form-control" value="<?php echo $singleproduct->compare_price; ?>">
                  <span class="error"></span>
                </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-md-3">Active(Display on web)</label>
                    <div class="col-md-9">
                  <select class="form-control" name="product_status<?php echo $singleproduct->id; ?>" id="product_status<?php echo $singleproduct->id; ?>">
                  <?php if( $singleproduct->status == '1') { ?>
                    <option value="1" selected>Yes</option>
                  <?php }else{ ?>
                    <option value="1">Yes</option>
                  <?php } ?>

                  <?php if( $singleproduct->status == '0') { ?>
                    <option value="0" selected>No</option>
                  <?php }else{ ?>
                    <option value="0">No</option>
                  <?php } ?>
                  </select>
                  <span class="error"></span>
                </div>
                </div>
                  </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="">Product Categories</label>
                  <?php
                      $category = array();

                    ?>
                  <select class="form-control" tabindex="7"  name="category[]" id="category<?php echo $singleproduct->id; ?>" multiple="">


                      </select>

              </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="">Product Tags</label>


                  <select tabindex="19" name="product_tags[]" id="product_tags<?php echo $singleproduct->id; ?>" class="form-control chosen-select" multiple tabindex="">

                    <?php  $product_tags = array();
                      $product_tags = $this->productmanagement_model->get_product_tags($singleproduct->id);
                    if(!empty($tag_data)){
                      foreach($tag_data as $val){
                        if(in_array($val->id,$product_tags)){
                          echo '<option value="'.$val->id.'" selected>'.$val->name.'</option>';
                        }else{

                          echo '<option value="'.$val->id.'">'.$val->name.'</option>';
                        }
                      }
                    }
                    ?>
                  </select>

                  </select>
                </div>
                  </div>
          </div>
          </div>
                  <div class="text-right">
                <button class="btn btn-primary btn-sm btn-quick-close"><i class="fa fa-close"></i> Cancel</button>
                <button class="btn btn-primary btn-sm" onclick="quick_edit(<?php echo $singleproduct->id; ?>);"><i class="fa fa-save"></i> Update</button>
              </div>

                </div>
                </div>
                </div>
            </div>
        </td>

          </tr>
        <?php
          }
          }else
          {
        ?>
          <tr><td colspan="9">
              No Records Found.
            </td>
          </tr>
        <?php
          }
        ?>
      </tbody>
      </table>
    </div>


<!-- //new design -->


				</div><!-- /.box-body -->
         <div class="pull-right">
            <?php echo $this->pagination->create_links(); ?>
         </div>
      </div>
		 </div>
		</div>
  </section><!-- /.content -->
  <style>
  .chosen-container-single .chosen-single{border-radius:2px !important; height:30px !important; border: 1px solid #00b19c !important;background:none !important;}
  .chosen-container-active.chosen-with-drop .chosen-single{border: 1px solid #00b19c !important;}
  .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td{border: 1px solid #d2d6de; }

  .table > caption + thead > tr:first-child > td, .table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table > thead:first-child > tr:first-child > th{border: 1px solid #d2d6de; }
  .table-bordered { border: 1px solid #ddd; }

  .page-manage-products .prod-btns-wrp-outer{position: relative;}
  .page-manage-products .prod-btns-wrp{display: block; color:#999; font-size: 13px; position: absolute; left:-5px;}
  .page-manage-products .prod-btns-wrp .btns-list li{display: inline-block; padding: 0px 5px;line-height: 1; border-right: 1px solid #999; margin-bottom: 5px;}
  .page-manage-products .prod-btns-wrp .btns-list li:last-child{border-right: none;}
  .page-manage-products .product-name{margin-bottom: 3px; margin-top: -3px;}
    .page-manage-products .img-wrp{ width:50px; height: 50px;}
  .page-manage-products .img-wrp img{width:auto; max-width:50px; height:auto; max-height: 50px;}

/*
.page-manage-products .product-cat-checklist{border: 1px solid #d2d6de; padding: 10px; max-height: 230px;}
.page-manage-products .product-cat-checklist li{list-style: none;}
.page-manage-products .product-cat-checklist label{font-weight: normal;}
.page-manage-products .product-cat-checklist label input{margin-right: 3px;}
.page-manage-products .product-cat-checklist li > ul{margin-left: 15px;}
.page-manage-products .product-cat-checklist li > ul li  > ul{margin-left: 15px;}
*/

.table-striped > tbody > tr:nth-of-type(2n+1){background: #fff;}
.table-striped > tbody > tr:hover{background: #f9f9f9;}
.table-tr-collapse tr.open .prod-btns-wrp{display: block !important;}
  </style>
  
   <script>
  $(document).ready(function() {
        
   /* activate the carousel */
   $("#modal-carousel").carousel({interval:false});

   /* change modal title when slide changes */
   $("#modal-carousel").on("slid.bs.carousel",       function () {
        $(".modal-title")
        .html($(this)
        .find(".active img")
        .attr("title"));
   });

   /* when clicking a thumbnail */
   $(".row .thumbnail").click(function(){
    var content = $(".carousel-inner");
    var title = $(".modal-title");
  
    content.empty();  
    title.empty();
  
    var id = this.id;  
     var repo = $("#img-repo .item");
     var repoCopy = repo.filter("#" + id).clone();
     var active = repoCopy.first();
  
    active.addClass("active");
    title.html(active.find("img").attr("title"));
    content.append(repoCopy);

    // show the modal
    $("#modal-gallery").modal("show");
  });

});



$('document').ready(function(){
	   $('#search_by').on('change', function() {
		 getCategory(this.value);
	}); 
	var selOpt = $( "#search_by option:selected" ).val();
	  if (selOpt == 8)
	  {
		var selOpt1 = $( "#table_search" ).val();
		getCategory(selOpt,selOpt1);
		
	  }

	
  });
	  
	 
	  
	  function getCategory(value, selOpt1)
	  {
		if(value == 8)
		  {
			$.ajax({
				type:'POST',
				url: "<?php echo base_url(); ?>product_management/get_category",
				 //data:{'oid':oid, 'user_id':user_id},
				 data:{'seloption':selOpt1},
				 dataType: "html",
				 cache:false,
				 success:function(data){ 				 
					$('#table_search1').html(data);
					$('#table_search').val('');
					$('#table_search').css('display','none');
					$('#chzn_table_search_chosen').css('width','100% !important');
					$('.chzn_table_search').trigger('chosen:updated');
					 $(".chosen-container").each(function() {
						   $(this).attr('style', 'width: 100%');
					   });
				},
				error: function(data){
					console.log("error");
					console.log(data);
				}
			});
		
		  }
		  else if(this.value != 8){
			 $('#table_search').css('display','block');
			 $('.chzn_table_search').hide();
			  $(".chosen-container").each(function() {
				   $(this).attr('style', 'display: none');
			   });
		  }
	  }
	  
</script>
  

  <script type="text/Javascript">
  function msg(m){ console.log(m);}
	$( document ).ready(function() {
		$('[data-toggle=confirmation]').confirmation({
			title:'Are you sure?',
			onConfirm : function(){

				var del_id = $(this).closest('.btns-list').find('.deleteProd').attr('data-id');
				$.ajax({
					 type: "POST",
					 url: "<?php echo base_url(); ?>"+"product_management/delete_product/",
					 data: { prodID: del_id },
					 cache:false,
					 success:
						  function(data){
							location.reload(); //as a debugging message.
						  }

					});
				}
			});

      	$('.nav-item-product').addClass("active");
/*
        	$('.table-products tr').hover( function(){
            $(this).find('.prod-btns-wrp').show();
          },
          function(){
            $(this).find('.prod-btns-wrp').hide();
          }
        );*/

        $('.btn-quick-edit').click( function(event){
             event.preventDefault();
           $('.table-tr-collapse tr.tr-quick-view .box-quick-view').slideUp(500);
             $('.table-tr-collapse tr').removeClass("open");
           $(this).parents('tr').next('.tr-quick-view').find('.box-quick-view').slideDown(500);
           $(".chosen-select").chosen("destroy");
           $(".chosen-select").chosen({width: "95%"});
           $(this).parents('tr').addClass("open");
        });

        $('.btn-quick-close').click( function(){
          $('.table-tr-collapse tr.tr-quick-view .box-quick-view').slideUp();
          $('.table-tr-collapse tr').removeClass("open");
        });

	});
function openwindow (url) {
   var win = window.open(url, "window1", "width=300,height=300,status=yes,scrollbars=no,resizable=no");
   win.focus();
}
/*	$( document ).ready(function() {
		$('[data-toggle=confirmation]').confirmation({
			title:'Are you sure?',
			onConfirm : function(){
				var p_id = $(this).closest('td').find('.approveRejectProd').attr('data-id');
				$.ajax({
					 type: "POST",
					 url: "<?php echo base_url(); ?>"+"product/approveReject_product/",
					 data: { prodID: p_id },
					 cache:false,
					 success:
						  function(data){
							location.reload(); //as a debugging message.
						  }

					});
				}
			});



	});*/
</script>
 <script type="text/javascript">
      function genarate_slug(id){
        var category_name = $('#product_name'+id).val();
        var slug = category_name.replace(/[^a-zA-Z0-9]/g, "-");
        $('#product_slug'+id).val(slug.toLowerCase());
      }


function get_prod_info(pid){
  $('.page-overlay').show();
  $.ajax({
           type: "POST",
           url: "<?php echo base_url(); ?>"+"product_management/get_product_cat",
           data: { prodID: pid },
           cache:false,
           success:function(data){
              $('#category'+pid).html(data);
              $('#category'+pid).chosen();
              $('.page-overlay').hide();
      }

  });
}


    </script>
