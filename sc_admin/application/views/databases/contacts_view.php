<?php $users_menu=$this->data['users_menu'];
        $total_rows = $this->data['total_rows'];
       // echo "<pre>"; print_r($contact_details);
    ?>
<section class="content-header">
	<h1>Contacts Details</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <!--li><a href="<?php echo base_url(); ?>">Manage Brand Users</a></li-->   
		<li class="active">Contacts Details</li>
	</ol>
</section>
<section class="content page-product-details">
  <div class="row">
    <div class="col-md-12">
      <div class="box contact-details">
         <div class="box-header with-border">
                      <h3 class="box-title">List of Contacts <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3>
                      <div class="pull-right">

                        <?php if(in_array("2",$users_menu)) { ?>
                        <a href="<?php echo base_url(); ?>databases/contacts/contacts_add"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add Contact</button></a>
                        <?php } ?>
                      </div>
                    </div>
                      <div class="box-body">

                           <?php echo $this->session->flashdata('feedback'); ?>

                <form name="frmsearch" class="form-group" id="frmsearch" action="<?php echo base_url(); ?>databases/contacts" method="post">
                    <div class="col-md-2">
                      <select name="search_by" id="search_by" class="form-control input-sm" >
                        <option value="0">Search by</option>
                        <!--option value="1" <?php //echo set_value('search_by')==1 ? 'selected' : ''; ?>>id</option-->
                        <option selected value="1" <?php echo set_value('search_by')==1 ? 'selected' : ''; ?>> Contact Name </option>
                        <option value="2" <?php echo set_value('search_by')==2 ? 'selected' : ''; ?>> Email-Id </option>
                        <option value="3" <?php echo set_value('search_by')==3 ? 'selected' : ''; ?>> Mobile </option>
                        <option value="4" <?php echo set_value('search_by')==4 ? 'selected' : ''; ?>> Phone </option>
                        <option value="5" <?php echo set_value('search_by')==5 ? 'selected' : ''; ?>> Contact Type </option>
                      </select>
                    </div>
                    <div class="col-md-3">
                      <div class="input-group">
                        <input type="text" name="table_search" id="table_search" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo set_value('table_search'); ?>" required=""/>
                        <div class="input-group-btn">
                          <button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <a href="contacts" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
                    </div>                    
                    <div class="col-md-5">
                      <!--div class="records-per-page pull-right">
                        <select name="records-per-page" class="form-control input-sm">
                          <option>Per Page</option>
                          <option value="10" selected="selected">10</option>
                          <option value="25">25</option>
                          <option value="50">50</option>
                        </select>
                      </div-->
                      <div class="pull-right"><?php echo $this->pagination->create_links(); ?></div>
                    </div>
                    </form>
            <!--div class="pull-right">
              <?php if(in_array("58",$users_menu)) { ?>
              <a href="<?php echo base_url(); ?>databases/contacts/contacts_add"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add Contacts</button></a>
              <?php } ?>
            </div-->
        </div>
        <div class="box-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable table-vth" id="datatable">
               <thead>
                  <tr>
                    <th>#</th>
                    <th>Contact Name</th>                    
                    <th>Gender</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Country</th>
                    <th>Email-Id </th>
                    <!--th>Alternate Email </th-->
                    <th>Mobile</th>                    
                    <th>Phone </th> 
                    <th>Pincode</th>                   
                    <th>Company </th>
                    <th>Fax </th>
                    <th>Designation </th>
                    <th>Contact Type </th>
                    <th>Action </th>
                  </tr>
                </thead>
                <tbody>
                <?php
                $i=0;
                foreach($contact_details as $contact)
                {   $i++;             
                ?>
                  <tr>
                    <td><?php echo $i; ?></td> 
                    <td><?php echo $contact['contact_name']; ?></td>                   
                    <td><?php if($contact['gender'] == 'M') { echo "Male";}
                              if($contact['gender'] == 'F') { echo "Female";}  ?></td>
                    <td><?php echo $contact['city'] ?></td>
                    <td><?php echo $contact['state'] ?></td>
                    <td><?php echo $contact['country'] ?></td>
                    <td><?php echo $contact['email']."<br/>".$contact['altemail']; ?></td>
                    <td><?php echo $contact['mobile_one']."<br/>".$contact['mobile_two']; ?></td>
                    <td><?php echo $contact['phone_one']."<br/>".$contact['phone_two']; ?></td>
                    <td><?php echo $contact['pincode'] ?></td>                  
                    <td><?php echo $contact['company'] ?></td> 
                    <td><?php echo $contact['fax'] ?></td>
                    <td><?php echo $contact['designation'] ?></td>
                    <td><?php echo $contact['contacttype'] ?></td>
                    <td>
                      <?php //if(in_array("3",$users_menu)) { ?>
                          <a href="<?php echo base_url(); ?>databases/contacts/contacts_edit/<?php echo $contact['id']; ?>"><button type="button" class="btn btn-xs btn-primary btn-rad editContact"><i class="fa fa-edit"></i> Edit</button></a>
                      <?php// } ?>
                      <?php// if(in_array("4",$users_menu)) { ?>
                          <button type="button" class="btn btn-xs btn-primary btn-delete btn-rad deleteContact"  data-id="<?php echo $contact['id']; ?>" data-toggle="confirmation" ><i class="fa fa-trash-o"></i> Delete</button>
                      <?php// } ?>
                    </td>
                  </tr> 
                <?php
                }
                ?>                 
                </tbody>
            </table>           
          </div>
            <div class="pull-right"><?php echo $this->pagination->create_links(); ?></div>
        </div>       
        </div>			
      </div>        

    </div>
</section>

<script type="text/javascript">

  $( document ).ready(function() {
        $('[data-toggle=confirmation]').confirmation({
          title:'Are you sure?',
          onConfirm : function(){
            var del_id = $(this).closest('td').find('.deleteContact').attr('data-id');

            $.ajax({
               type: "POST",
               url: "<?php echo base_url(); ?>"+"databases/contacts/contacts_delete",
               data: { contactID: del_id },
               cache:false,
               success:
                  function(data){                    
                  location.reload(); //as a debugging message.
                  }

              });
            }
          });

          $('.nav-item-database').addClass("active");
          $('.nav-item-dcontacts').addClass("active");
      });
</script>
