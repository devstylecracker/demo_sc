<link href="<?php echo base_url(); ?>assets/css/data_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/blueimp-gallery.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-image-gallery.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/bootstrap-image-gallery.min.js">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery.blueimp-gallery.min.js">
<section class="content page-image-upload ss-image-catalog">  
    
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>Upload Images for Database</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>
          
            <li class="active">Image Catalog</li>
          </ol>
        </section>

          <!-- Main content -->
          <section id="image-catalog" class="content page-images">
          <form name="frmsearch" id="frmsearch" method="post" action="">
                <div class="box box-solid search-filter-bar">
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-2">
                        <div class="form-group">
                          <select class="form-control input-sm image-categories" name="image_categories" id="image_categories">
                            <option selected="selected">All Categories</option>
                            <?php if(!empty($dbcategory)){ ?>
                              <?php foreach($dbcategory as $val){ ?>
                                  <option value="<?php echo $val['id']; ?>"><?php echo $val['category_name']; ?></option>
                              <?php } ?>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-7">
                          <div class="cat-options-wrp" id="cat_items">

                          </div>
                      </div>  
                 
                    <div class="col-md-3">
                      <div class="filter-search-box-wrp">

                      <div class="input-group pull-left search-box">
                        <input type="text" placeholder="Search by Tags" style="width: 150px;" class="form-control input-sm pull-right" name="tag_search" id="tag_search">
                        <div class="input-group-btn">
                          <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                        </div>
                      </div>
                    </div>
                    </div> </form>
                    <!-- -->
                </div>
              </div>
            </div>
            <div class="box">
              <div class="box-body">
                <div class="box box-solid">
                  <div class="box-body">
                    <div class="image-thumbs">
                      <ul>
                      <?php if(!empty($db_pic)){ foreach($db_pic as $val) { ?>
                        <li>
                          <a href="<?php echo base_url().'assets/databases_images/'.$val['image_name']; ?>" title="" data-gallery>
                             <img src="<?php echo base_url().'assets/databases_images/thumb/'.$val['image_name']; ?>" alt="">
                         </a>
                        </li>
                       <?php } } ?>
                      </ul>
                    </div>
                  </div>
                </div>
                  <div class="pull-right">
                        <?php echo $this->pagination->create_links(); ?>
                      </div>
              </div>
            </div>
          <div>
          </div>

        </section><!-- /.content -->
          
</section>        
<script type="text/Javascript">
$('.nav-item-database').addClass("active");
    $('.nav-item-dbimagecatalog').addClass("active");
   $( document ).ready(function() {
       /* $(document).on('change', '#image_categories', function (event) {
        var cat_id = $('#image_categories').val();
        $.ajax({
           url: '<?php echo base_url(); ?>databases/Dbimageupload/get_search_options',
           type: 'post',
           data: {'type' : 'get_search_options','category_id': cat_id },
           success: function(data, status) {
            $('#cat_items').html(data);
          }
        });
    });*/
  });

</script>