<?php
$users_menu=$this->data['users_menu'];
?>
<section class="content-header">
        <h1>Databases Category</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>          
            <li class="active">Databases Category</li>
          </ol>
</section>
<section class="content">
        <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Category List <small class="label pull-right label-info"><?php //echo $total_rows; ?></small></h3>
                  <div class="pull-right">
                  
                  <a href="<?php echo base_url(); ?>databases/database_category/add_category"><button class="btn btn-sm btn-primary" type="button"><i class="fa fa-plus"></i> Add Category</button></a>
                  
                  </div>
                  </div>
                  <div class="box-body">
                    <?php echo $this->session->flashdata('feedback'); ?>
						<form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>databases/database_category" method="post">
              <div class="row">
        <div class="col-md-2">
									<select name="search_by" id="search_by" class="form-control input-sm" >

										<option value="0">Search by</option>
										<option selected value="1" <?php echo set_value('search_by')==1 ? 'selected' : ''; ?>>Category Name</option>
									</select>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
								<input type="text" name="table_search" id="table_search" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo set_value('table_search'); ?>" required=""/>
									<div class="input-group-btn">
										<button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
									</div>
                </div>
                </div>
                <div class="col-md-2">
                  <a href="users" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
                </div>
                <div class="col-md-5">
                  
                    <div class="pull-right">
                        <?php echo $this->pagination->create_links(); ?>
                      </div>
                </div>
              </div>
						</form>
            <div class="table-responsive">
                <table class="table table-bordered table-striped dataTable" id="datatable">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Category</th>
                        <th>Status</th>
						            <th>Created Date</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
						<?php if(!empty($category_details)) { $i = 1;
						foreach($category_details as $val){
						?>
					  <tr>
                        <td><?php echo $val['id']; ?></td>
                        <td><?php echo $val['category_name']; ?></td>
                        <td><?php echo $val['is_delete']==0 ? 'Active' : 'Deactive'; ?></td>
                        <td><?php //echo $val->created_datetime;
                                  $date = new DateTime($val['created_datetime']);
                                  echo $date->format('d-m-Y H:i:s');
                        ?></td>
                        <td>
                         
                          <a href="<?php echo base_url(); ?>databases/Database_category/view_category/<?php echo $val['id']; ?>">
                          <button type="button" class="btn  btn-xs btn-primary"><i class="fa fa-eye"></i> View</button></a>
                          

							<a href="<?php echo base_url(); ?>databases/Database_category/edit_category/<?php echo $val['id']; ?>">
							<button type="button" class="btn  btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</button></a>

							<a onclick="javascript:void(0)">
							<button type="button" class="btn  btn-xs btn-delete btn-primary" data-id='<?php echo $val['id']; ?>' data-ext-id='<?php echo $val['is_delete']; ?>' data-toggle="confirmation"><i class="fa fa-trash-o"></i> Active/Inactive</button></a>
							

            </td>
                      </tr>
                       <?php $i++; } }else{ ?>
					  <tr>
						<td colspan="5">Sorry no record found</td>
					  </tr>
					   <?php } ?>
                    </tbody>

                  </table>
                </div>

                  <?php echo $this->pagination->create_links(); ?>
                  </div>
                  </div>

                </section>

<script type="text/Javascript">
	$( document ).ready(function() {
		$('[data-toggle=confirmation]').confirmation({
			title:'Are you sure?',
			onConfirm : function(){
				var del_id = $(this).closest('td').find('.btn-delete').attr('data-id');
        var data_ext_id = $(this).closest('td').find('.btn-delete').attr('data-ext-id');
        console.log(data_ext_id);
				$.ajax({
					 type: "POST",
					 url: "<?php echo base_url(); ?>" + "databases/Database_category/delete_category/",
					 data: { id: del_id,data_ext_id:data_ext_id },
					 cache:false,
					 success:
						  function(data){
							location.reload(); //as a debugging message.
						  }

					});
				}
			});
      $('.nav-item-database').addClass("active");
      $('.nav-item-dcat').addClass("active");
	});
</script>
