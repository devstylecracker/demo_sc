<!--link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/Tokenize-2.4.3/jquery.tokenize.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/Tokenize-2.4.3/jquery.tokenize.js"></script-->

<!--script type="text/javascript">
$( document ).ready(function() {
$('#contacttype').tokenize({   
});

});
</script-->

<!-- Main content -->
<section class="content-header">
  <h1>Database Contacts</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>databases/contacts">Database Contacts</a></li>    
    <li class="active">Edit Contacts</li>
  </ol>
</section>
<section class="content">
  <div class="box ">
    <div class="box-header">
      <h3 class="box-title">Edit Contacts</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
         <section id="add-contacts" class="content page-add-contacts">
          <form role="form"  id="db_contact_form" method="POST">
            <div class="box box-solid">
              <div class="row">
                <div class="col-md-6">
                 
                  <div class="box-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="forcontactname">Contact Name  <span class="text-red">*</span></label>
                        <input type="text" placeholder="Enter Contact Name" id="contact_name" name="contact_name" maxlength="150" class="form-control" alphanumeric="true" value="<?php if(isset($contact_type[0]['contact_name'])){ echo $contact_type[0]['contact_name']; }else{ echo set_value('contact_name'); } ?>" >
                        <span class="text-red"> <?php echo form_error('contact_name'); ?></span>
                      </div>
                    </div>                   
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="forgender">Gender  <span class="text-red">*</span></label>
                        <?php 
                        if(isset($contact_type[0]['gender'])){ $gender=$contact_type[0]['gender']; }else{ echo set_value('gender'); } ?>
                        <select class="form-control" id="gender" name="gender">
                          <option value="">Select Gender</option>
                          <option value="M" <?php if($gender=='M'){ echo 'selected'; } ?> >Male</option>
                          <option value="F" <?php if($gender=='F'){ echo 'selected'; } ?> >Female</option>
                        </select>
                        <span class="text-red"> <?php echo form_error('gender'); ?></span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="forcity">City</label>
                        <input type="text" placeholder="Enter City" id="city" name="city" maxlength="150" value="<?php if(isset($contact_type[0]['city'])){ echo $contact_type[0]['city']; }else{ echo set_value('city'); } ?>" class="form-control" >
                         <span class="text-red"> <?php echo form_error('city'); ?></span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="forstate">State</label>
                        <input type="text" placeholder="Enter State" id="state" name="state" maxlength="50" value="<?php if(isset($contact_type[0]['state'])){ echo $contact_type[0]['state']; }else{ echo set_value('state'); } ?>" class="form-control">
                        <span class="text-red"> <?php echo form_error('state'); ?></span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="forcountry">Country</label>
                        <input type="text" placeholder="Enter Country" id="country"  name="country"  maxlength="50" value="<?php  if(isset($contact_type[0]['country'])){ echo $contact_type[0]['country']; }else{ echo set_value('country'); } ?>" class="form-control" >
                         <span class="text-red"> <?php echo form_error('country'); ?></span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="forpincode">Pincode</label>
                        <input type="text" placeholder="Enter Pincode" id="pincode" name="pincode" maxlength="6" value="<?php if(isset($contact_type[0]['pincode'])){ echo $contact_type[0]['pincode']; }else{  echo set_value('pincode'); } ?>" class="form-control">
                        <span class="text-red"> <?php echo form_error('pincode'); ?></span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="forcompany">Company</label>
                        <input type="text" placeholder="Enter Company" id="company" name="company"  maxlength="150" value="<?php if(isset($contact_type[0]['company'])){ echo $contact_type[0]['company']; }else{ echo set_value('company'); } ?>" class="form-control">
                        <span class="text-red"> <?php echo form_error('company'); ?></span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="fordesignation">Designation / Job Title</label>
                        <input type="text" placeholder="Enter Designation" id="designation" name="designation" value="<?php if(isset($contact_type[0]['designation'])){ echo $contact_type[0]['designation']; }else{ echo set_value('designation'); } ?>"  maxlength="150"class="form-control">
                        <span class="text-red"> <?php echo form_error('designation'); ?></span>
                      </div>
                    </div>
                  </div>
                </div>                 
              </div>
                <div class="col-md-6">                 
                  <div class="box-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="forphone1">Phone1</label>
                        <input type="text" placeholder="Enter Phone1" id="phone1"  name="phone1"  maxlength="15" value="<?php if(isset($contact_type[0]['phone_one'])){ echo $contact_type[0]['phone_one']; }else{ echo set_value('phone1'); }?>"  class="form-control">
                        <span class="text-red"> <?php echo form_error('phone1'); ?></span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="forphone2">Phone2</label>
                        <input type="text" placeholder="Enter Phone2" id="phone2"  name="phone2" maxlength="15" value="<?php if(isset($contact_type[0]['phone_two'])){ echo $contact_type[0]['phone_two']; }else{ echo set_value('phone2'); } ?>" class="form-control">
                        <span class="text-red"> <?php echo form_error('phone2'); ?></span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="formobile1">Mobile1 <span class="text-red">*</span></label>
                        <input type="text" placeholder="Enter Mobile1" id="mobile1" name="mobile1" maxlength="10" value="<?php if(isset($contact_type[0]['mobile_one'])){ echo $contact_type[0]['mobile_one']; }else{ echo set_value('mobile1'); }?>" class="form-control">
                         <span class="text-red"> <?php echo form_error('mobile1'); ?></span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="formobile2">Mobile2</label>
                        <input type="text" placeholder="Enter Mobile2" id="mobile2" nmae="mobile2" value="<?php if(isset($contact_type[0]['mobile_two'])){ echo $contact_type[0]['mobile_two']; }else{ echo set_value('mobile2'); }?>" maxlength="10" class="form-control">
                         <span class="text-red"> <?php echo form_error('mobile2'); ?></span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="foremail">Email-Id  <span class="text-red">*</span></label>
                        <input type="text" placeholder="Enter Email-Id" id="email" name="email" maxlength="150" value="<?php if(isset($contact_type[0]['email'])){ echo $contact_type[0]['email']; }else{ echo set_value('email'); }?>" class="form-control">
                         <span class="text-red"> <?php echo form_error('email'); ?></span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="foraltemail">Alternate Email </label>
                        <input type="text" placeholder="Enter Alternate Email" id="altemail" name="altemail" maxlength="150" value="<?php if(isset($contact_type[0]['altemail'])){ echo $contact_type[0]['altemail']; }else{  echo set_value('altemail'); } ?>"  class="form-control">
                        <span class="text-red"> <?php echo form_error('altemail'); ?></span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="forfax">Fax </label>
                        <input type="text" placeholder="Enter Fax" id="fax" name="fax" maxlength="50" value="<?php if(isset($contact_type[0]['fax'])){ echo $contact_type[0]['fax']; }else{ echo set_value('fax'); } ?>" class="form-control">
                      </div>
                    </div>
                     <div class="col-md-6">
                         <div class="form-group">
                          <label for="forcontacttype">Contact Type  <span class="text-red">*</span></label>
                           <input type="text" placeholder="Enter Contact Type" id="fax" name="contacttype" maxlength="50" value="<?php  if(isset($contact_type[0]['contacttype'])){ echo $contact_type[0]['contacttype']; }else{ echo set_value('contacttype'); }?>" class="form-control">
                          <!--select placeholder="Select Contact type" name="contacttype" id="contacttype" maxlength="50" class="form-control tokenize-sample" multiple="false" >
                          <?php
                          if(!empty($contact_type)){
                          foreach($contact_type as $val){
                            if(set_value('contacttype') == $val->contact_type){
                              echo '<option value="'.$val->contact_type.'">'.$val->contact_type.'</option>';
                            }else{

                              echo '<option value="'.$val->contact_type.'">'.$val->contact_type.'</option>';
                            }
                          }
                          }
                          ?>
                          </select-->
                          <span class="text-red"> <?php echo form_error('contacttype'); ?></span>
                        </div>
                     </div>
                    </div>
                    </div>
                  </div>
                  <div class="col-md-offset-5">
                    <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Save </button>
                    <a href="<?php echo base_url()?>databases/contacts"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i> Back</button></a>
                    <button class="btn btn-sm btn-default" tabindex="31" type="reset"><i class="fa fa-close"></i> Cancel</button>
                  </div>
                  <div>
                    
                  </div>

                </div>                  
                </div>
               
              </div>           
           
              </form>

        </section><!-- /.content -->
      </div>
  
</section>
<script type="text/javascript">
$( document ).ready(function() {
$('.nav-item-user').addClass("active");
$('.nav-brand-user').addClass("active");
});

</script>
