<?php

  $catarray = array();

?>

<!-- custom setting for database_image_upload.php page -->

<link href="<?php echo base_url(); ?>assets/css/data_style.css" rel="stylesheet" type="text/css" />

<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />

<!-- search select box implementation -->
<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js" type="text/javascript"></script>

<section class="content-header">
  <h1>Upload Images for Database</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>lookcreator/manage_look">Image Upload</a></li>
    <li class="active">Image Upload</li>
  </ol>
</section>
<section class="content page-image-upload ss-image-upload">

  <div id="image-upload">
    <form role="form" name="frmuploadimg" id="frmuploadimg"  method="post" action="<?php echo base_url(); ?>databases/dbimageupload/upload_images" enctype="multipart/form-data">
      <!--div class="box">
        <div class="box-body">
          <div class="box-drag-drop">
          <div id="preview"></div>
            <input id="file" name="file[]" class="file" type="file" multiple onclick="readURL(this);" >
            <span class="text-red"> </span>
          </div>
        </div>
      </div -->

      <div class="box">
        <div class="box-body">
          <?php echo $this->session->flashdata('feedback'); ?>
          <div class="box-drag-drop">
          <div id="preview_img"></div>  
          <label for="categoryName" class="control-label">Upload Images
                      <span class="text-red">*</span></label>          
            <input type="file" id="avatar"  name="avatar[]" multiple onchange="readURL(this);"/>
             <span class="text-red"> <?php echo form_error('avatar'); ?></span>
          </div>
        </div>
      </div>

         <div class="row">
                <div class="col-md-12">
                  <!-- form start -->
                  <div class="row">
                    <div class="col-md-7">
                      <div class="box">
                        <div class="box-header with-border"><h3 class="box-title">Categories</h3><span class="text-red">*</span></label> </div>
                        <div class="box-body">
                          <div class="form-group">
                            <div class="checkbox-group">
                              <div class="cat-wrp">
                                <div class="row">
                                  <div class="col-md-12">

                                  <?php if(!empty($dbcategory))
                                        { 
                                           foreach($dbcategory as $val)
                                            { 
                                              
                                              $cat = "cat-".strtolower(trim($val['category_name']));
                                              $catclass = str_replace(' ', '-', $cat);
                                              $catarray[] =  $catclass;
                                  ?>
                                    <label class="btn btn-active cat-label <?php echo $catclass; ?>">
                                     
                                       <input type="radio" class="minimal" name="category_id" id="category_id" value="<?php if($val['id']!= "") { echo $val['id']; }else { echo set_value('product_name'); }  ?>" > <?php echo $val['category_name']; ?></label>
                                  <?php
                                            }
                                        }
                                  ?>
                                   <span class="text-red"> <?php echo form_error('category_id'); ?></span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="box">
                        <div class="box-body">
                          <div class="cat-options-wrp" id="cat_items">                         
                       
                          </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-5">
                        <!--div class="box box-solid">
                          <div class="box-body">
                            <!--div class="image-thumbs">
                              <ul>
                                <li>
                                  <img  src="<?php echo base_url(); ?>/assets/street_img/<?php //echo $val['img_name']; ?>" >
                                  <i class="fa fa-times-circle-o" onclick="delete_img(<?php //echo $val['id']; ?>)" ></i>
                                </li>
                               
                              </ul>
                            </div>
                          </div>
                        </div-->
                        <div class="box">
                          <div class="box-header with-border"><h3 class="box-title">Tags</h3></div>
                          <div class="box-body">
                            <div class="form-group">
                              <textarea name="tags" id="tags" placeholder="Tags(separate with comma)" rows="3" class="form-control"></textarea>
                              <span class="text-red"> <?php echo form_error('tags'); ?></span>
                            </div>
                          </div>
                        </div>
                        <button class="btn btn-primary pull-right" type="submit" onclick="return validate_form();" ><i class="fa fa-save"></i> SAVE</button>
                      </div>
              </div>


            </div>
          </div>
      <div class="box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Images <small class="label label-info"></small></h3>
          <div class="pull-right">
            <?php echo $this->pagination->create_links(); ?>
          </div>
        </div>
        <div class="box-body">
              <div class="image-thumbs" id="cat_images">            
              </div>
              <div><?php echo $this->pagination->create_links(); ?></div>
        </div>
      </div>

        </form>
      </div>
    </section><!-- /.content -->
    <link href="<?php echo base_url(); ?>assets/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <script src="<?php echo base_url(); ?>assets/js/fileinput.min.js" type="text/javascript"></script>

    <script type="text/Javascript">

     $( document ).ready(function() {
        $(document).on('change', 'input:radio[name="category_id"]', function (event) {
        var cat_id = $('input:radio[name=category_id]:checked').val();
        $.ajax({
           url: '<?php echo base_url(); ?>databases/Dbimageupload/get_category_details',
           type: 'post',
           data: {'type' : 'get_category_details','category_id': cat_id },
           success: function(data, status) {
            $('#cat_items').html(data);
            show_images(cat_id);
            
          }
        });
      });

      adddatepicker();
      addstyle();
     });


    function delete_img(id){
      var result = confirm("Want to delete?");
     // var del_cat_id = $('#category_id').val();    
      if (result) {
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "databases/Dbimageupload/delete_database_pic",
          data: { cat_del_id: id },
          cache:false,
          success:
          function(data){
            location.reload(); //as a debugging message.
          }

        });
      }
    }

    function adddatepicker()
    { 
       $(".datepicker").datepicker({     
        format: " yyyy", // Notice the Extra space at the beginning
        viewMode: "years", 
        minViewMode: "years"
        });
    }   

   
    function readURL(input) { 
       $('#preview_img').html('');
      for(var i = 0;i <= input.files.length; i++){ 
        if (input.files && input.files[i]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                //$('#preview_img').attr('src', e.target.result);
                $('#preview_img').append('<img id="preview_img" src='+e.target.result+' style="max-width: 130px; max-height: 130px;"/>');
            }

            reader.readAsDataURL(input.files[i]);
        }
      }
    }
 
 /*$("#image-upload .cat-references" ).click(function() {
    $(".cat-options" ).css( "display","none" );
    $("#cat-references" ).css("display","block" );
  });
  */

  function addstyle(){
    $('#image-upload-ref input[type=radio][name=ref-style]').change(function() {  
    var styletype = $('#image-upload-ref input[type=radio][name=ref-style]').val();  
      if (this.value == 'Celeb Style') {
        $(".ref-personality-content" ).css("display","none" );
      }
      else if (this.value == 'Personality') {
        $(".ref-personality-content" ).css("display","block" );
      }
    });
  }

   function show_images(id){     
      
      if (id) {
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "databases/Dbimageupload/show_database_images",
          data: { category_id: id },
          cache:false,
          success:
          function(data){            
             $('#cat_images').html(data);
            //location.reload(); //as a debugging message.
          }

        });
      }
    }

    function validate_form()
    { 
      if($("#category_id").val()=="" && $("#avatar").val()=="")
      {
          alert("Select Category and  Browse Image files to Upload");
          return false;
      }else
      {
          if($("#category_id").val()=="")
          {
            alert("Select Category ");
            return false;
          }

          if($("#avatar").val()=="")
          {
            alert("Browse Image files to Upload");
            return false;
          }
      }
          
      
    }

   $('.nav-item-database').addClass("active");
    $('.nav-item-dimage').addClass("active"); 
  /// page image-upload
  </script>
