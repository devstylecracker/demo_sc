<?php
$users_menu=$this->data['users_menu'];
?>
<section class="content-header">
        <h1>Category</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>
            <li><a href="<?php echo base_url(); ?>databases/database_category">Category</a></li>
            <li class="active"> Edit Catgory</li>
          </ol>
</section>

<section class="content">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title"> Edit Category </h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" name="fromcategory" id="fromcategory" action="" method="post">
                  <div class="box-body">
                    <div class="row">
              <div class="col-md-4">
                    
                    <div class="form-group">
                      <label for="tag_name" class="control-label">Category Name
						          <span class="text-red">*</span></label>
                      <input type="text" placeholder="Enter Category Name" name="category_name" id="category_name" class="form-control" value="<?php echo $cat_details[0]['category_name']; ?>"> <span class="text-red"><?php echo form_error('category_name'); ?></span>
                    </div>

                    <div class="form-group">
                      <label for="season_show" class="control-label">Season</label>
                      <input type="checkbox" name="show_season" id="show_season" value="1" <?php if($cat_details[0]['show_season'] == 1){ echo 'checked'; } ?>> 
                      <label for="" class="control-label">Mutiple</label>
                      <input type="checkbox" name="show_season_multi" id="show_season_multi" value="1" <?php if($cat_details[0]['show_season_multi'] == 1){ echo 'checked'; } ?>> 
                    </div>

                    <div class="form-group">
                      <label for="tag_name" class="control-label">Year</label>
                      <input type="checkbox" name="show_year" id="show_year" value="1" <?php if($cat_details[0]['show_year'] == 1){ echo 'checked'; } ?>> 
                    </div>

                    <div class="form-group">
                     <label for="season_show" class="control-label">Trend</label>
                      <input type="checkbox" name="show_trend" id="show_trend" value="1" <?php if($cat_details[0]['show_trend'] == 1){ echo 'checked'; } ?>>
                    </div>

                    <div class="form-group">
                      <label for="season_show" class="control-label">Events</label>
                      <input type="checkbox" name="show_events" id="show_events" value="1" <?php if($cat_details[0]['show_events'] == 1){ echo 'checked'; } ?>>
                    </div>

                    <div class="form-group">
                      <label for="season_show" class="control-label">Type</label>
                      <input type="checkbox" name="show_type" id="show_type" value="1" <?php if($cat_details[0]['show_type'] == 1){ echo 'checked'; } ?>>
                    </div>

                    <div class="form-group">
                      <label for="season_show" class="control-label">Designer</label>
                      <input type="checkbox" name="show_designer" id="show_designer" value="1" <?php if($cat_details[0]['show_designer'] == 1){ echo 'checked'; } ?>>
                      <label for="" class="control-label">Mutiple</label>
                      <input type="checkbox" name="show_designer_multi" id="show_designer_multi" value="1" <?php if($cat_details[0]['show_designer_multi'] == 1){ echo 'checked'; } ?>>
                    </div>

                    <div class="form-group">
                      <label for="season_show" class="control-label">Reference</label>
                      <input type="checkbox" name="refrence_show" id="refrence_show" value="1" <?php if($cat_details[0]['refrence_show'] == 1){ echo 'checked'; } ?>>
                    </div>

                    </div>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Submit</button>
                    <a href="<?php echo base_url(); ?>databases/database_category"><button type="button" class="btn btn-primary btn-sm btn-rad"><i class="fa fa-reply"></i> Back</button></a>
                    <button class="btn btn-default btn-sm" type="reset"><i class="fa fa-close"></i> Cancel</button>
                  </div>
                </form>
              </div><!-- /.box -->
            </div>
          </div>
</section>
<script type="text/Javascript">
$( document ).ready(function() {
//	$('.nav-item-category').addClass("active");
//  $('.nav-manage-tags').addClass("active");
});


</script>
