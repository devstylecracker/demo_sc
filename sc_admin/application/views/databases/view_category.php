<section class="content-header">
  <h1>Category</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>databases/database_category">Category</a></li>
    <li class="active">Category View</li>
  </ol>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Database Category</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="datatable">
              <tr>
                <th>ID</th>
                <td><?php echo $cat_details[0]['id']; ?></td>
              </tr>
              <tr>
                <th>Category</th>
                <td><?php echo $cat_details[0]['category_name']; ?></td>
              </tr>
              <tr>
                <th>Season</th>
                <td><?php echo $cat_details[0]['show_season']==1 ? 'Yes' : 'No' ; ?></td>
              </tr>
              <tr>
                <th>Season Mutiple</th>
                <td><?php echo $cat_details[0]['show_season_multi']==1 ? 'Yes' : 'No' ; ?></td>
              </tr>
              <tr>
                <th>Year</th>
                <td><?php echo $cat_details[0]['show_year']==1 ? 'Yes' : 'No' ; ?></td>
              </tr>
              <tr>
                <th>Trend</th>
                <td><?php echo $cat_details[0]['show_trend']==1 ? 'Yes' : 'No' ; ?></td>
              </tr>
              <tr>
                <th>Events</th>
                <td><?php echo $cat_details[0]['show_events']==1 ? 'Yes' : 'No' ; ?></td>
              </tr>
              <tr>
                <th>Type</th>
                <td><?php echo $cat_details[0]['show_type']==1 ? 'Yes' : 'No' ; ?></td>
              </tr>
              <tr>
                <th>Designer</th>
                <td><?php echo $cat_details[0]['show_designer']==1 ? 'Yes' : 'No' ; ?></td>
              </tr>
              <tr>
                <th>Designer Multiple</th>
                <td><?php echo $cat_details[0]['show_designer_multi']==1 ? 'Yes' : 'No' ; ?></td>
              </tr>
              <tr>
                <th>Refrence</th>
                <td><?php echo $cat_details[0]['refrence_show']==1 ? 'Yes' : 'No' ; ?></td>
              </tr>
              
            </table>
          </div>

          <div class="box-footer">          
            <a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>databases/database_category"><i class="fa fa-reply"></i> Back</a>
          </div>

        </div>
      </div>
    </div>
  </div>


</section>
<script type="text/Javascript">
$( document ).ready(function() {
 
});
</script>
