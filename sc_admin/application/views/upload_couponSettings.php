<?php
if(!isset($error))
{
  $error = "";
}
?>
<section class="content-header">
  <h1>Coupon Settings Upload</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>CouponSettings">Manage Coupon</a></li>
    <li class="active">Coupon Settings Upload</li>
  </ol>
</section>
<section class="content">

  <div class="row">
    <?php echo $this->session->flashdata('feedback'); ?>
    <!--<form role="form" id="product_uploadform" enctype="multipart/form-data" method="POST" action="<?php echo base_url(); ?>Productinventory_upload/product_upload/" onsubmit="return validate()"> -->
    <form role="form" id="product_uploadform" enctype="multipart/form-data" method="POST" action="<?php echo base_url(); ?>CouponSettings/upload_way_queesheet" onsubmit="return validate()">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-upload"></i> Coupon Settings  Sheet <small class="label label-info"></small></h3>
            <br/><br/>
            <h5><a href="<?php echo base_url(); ?>assets/example/coupon-setting-template.xls">Sample Queue Sheet</a></h5>
            <div class="pull-right">
            </div>
          </div>
          <div class="box-body  text-center">
            <br/><br/>
            <p>Browse Coupon Settings Sheet <input  id="product_upload" name="product_upload" type="file" class="file"></p>
            <!--p class="help-block">Max file size: 5MB | Allowed format: .xls, .xlsx </p-->
          <br/>
          <p>
            <button class="btn btn-primary btn-sm" type="submit" name = "Save"  value="Save" ><i class="fa fa-upload"></i> Upload</button>
              <a href="<?php echo base_url(); ?>CouponSettings" ><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i> Back</button></a>
          </p>
          </div>
          <span class="text-green">            
            <?php            
              if(!empty($meassage['successfully'])){                
                echo '<center>'.'Successfully Entry :- '.$meassage['successfully'] .'</center>';                
              }
            ?>
        </span>
          <span class="text-red">            
            <?php                        
            $meassaged = $meassage['duplicate'];
              if(!empty($meassage['duplicate']) && $meassaged != 0){
                echo '<center>'.'Duplicate Entry :- '.$meassage['duplicate'] .'</center>';
              }                       
            ?>
        </span>
        <span class="text-red">            
            <?php                
                if(!empty($meassage['duplicate_id'])){ 
                  foreach ($meassage['duplicate_id'] as $id => $value){
                    if(!empty($value)){
                      echo '<center>'.'Duplicate ids :- '.$value .'</center>';                                                       
                    }
                    
                  }
                }
            ?>
        </span>
      </div>
    </div>
  </form>
 

<script>
function validate() {
    var extension;
    var filename=document.getElementById('product_upload').value;
    extension=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();
  
    if(filename == "")
    {
        alert('Browse Excel File');
        return false;

    }else
    {
      if(extension == 'xlsx' || extension == 'xls')
      {
        return true;
      }else
      {
        alert('This File Is Not Excel');
        return false;
      }

    }
    

  }

  </script>