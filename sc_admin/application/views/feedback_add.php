<?php
//echo "<pre>"; print_r(@$feedback);

$users_menu=$this->data['users_menu'];
?>
<section class="content-header">
        <h1>Feedback</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>
            <li class="active">Feedback</li>
          </ol>
</section>

<section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Your Feedback</h3>
        </div><!-- /.box-header -->
        <?php echo $this->session->flashdata('feedback'); ?>
        <!-- form start -->
        <form role="form" name="frmtag" id="frmfdb" action="" method="post">
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">

                  <div class="form-group">
                    <label for="username" class="control-label">Name                              
                    <input class="form-control" type="text" placeholder="Enter Your Name" name="username" id="username" value="" >
                  </div>

                  <div class="form-group">
                    <label for="feedback" class="control-label">Give Your Feedback
					          <span class="text-red">*</span></label>
                   <textarea  class="form-control" tabindex="3"  name="feedback" id="feedback" placeholder="Give Your Feddback" rows="5" ></textarea>
                     <span class="text-red"><?php echo form_error('feedback'); ?></span>
                  </div>

              </div>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">

            <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Submit</button>

            <a href="<?php echo base_url(); ?>home"><button type="button" class="btn btn-primary btn-sm btn-rad"><i class="fa fa-reply"></i> Back</button></a>

            <button class="btn btn-default btn-sm" type="reset"><i class="fa fa-close"></i> Cancel</button>

            <?php if(in_array("57",$users_menu)) { ?>
            <a href="<?php echo base_url(); ?>feedback/feedback_list"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> View Feedback</button></a>
            <?php } ?>

          </div>
        </form>
    </div><!-- /.box -->
</section>

<script type="text/Javascript">
$( document ).ready(function() {
  $('.nav-item-feedback').addClass("active");
});
</script>
