<section class="content-header">
  <h1>SEND SMS</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li class="active">Send Sms</li>
  </ol>
  <br>
</section>
<!-- /.box-header -->
<div class="box">
  <div class="box-body">
      <form name="sendsms" id="sendsms" class="form-group" action="" method="post" onsubmit="return validate_sms_Form()">
        <div class="row">
         <div class="col-md-4">
              <div class="form-group">
                <label class="control-label" for="">Mobile Number</label><span class="text-red">*</span></label>
                <input type="text" class="form-control" name="mobile_num" id="mobile_num" placeholder="Mobile" pattern="[0-9]+(,[0-9]+)*" required>
              </div>
          </div>
        </div>
        <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="control-label" for="">Message</label><span class="text-red">*</span></label>
                    <textarea class="form-control" rows="10" name="message" id="message" placeholder="Message..." maxlength="160" required ></textarea>
                  </div>
                </div>
        </div> 
         <div class="row">
			<div class="col-md-4">
				<div class="form-group">
				  <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-send"></i> SEND</button>
				</div>   
			</div>
        </div>     
        <div class="pull-right">
          <?php echo $this->pagination->create_links(); ?>
        </div>
      </form>
  </div>
</div>
<script type="text/Javascript">
function validate_sms_Form(){
	var result = confirm("Want to send SMS?");
	return result;
}
$( document ).ready(function() {
$("body").addClass('sidebar-collapse');
$('.nav-item-sms').addClass("active"); 
});
</script> 
