<?php
$users_menu=$this->data['users_menu'];
// echo "<pre>";print_r($collection_data);exit;
?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<section class="content-header">
        <h1>Collection</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>
            <li><a href="<?php echo base_url(); ?>lookcreator/manage_look">Collection Manage</a></li>
            <li class="active">Edit Collection</li>
          </ol>
</section>
<section class="content">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Collection</h3>
                </div>
                <form role="form" name="frmtag" id="frmtag" action="<?php echo base_url(); ?>collection_new/edit_collection/<?php echo $collection_data['object_id']; ?>" method="post" enctype="multipart/form-data">
                  <div class="box-body">
                    <div class="row">
				<div class="col-md-4">
                    <div class="form-group">
                      <label for="collection_name" class="control-label">Collection Name
					  <span class="text-red">*</span></label>
					  <input type="text" name="collection_name" class="form-control input-sm" id="collection_name" placeholder="Enter Collection name" value="<?php if($collection_data['object_name']!="") echo $collection_data['object_name']; else echo set_value('collection_name');?>" required > 
					   <input type="hidden" name="collection_id" id="collection_id" value="<?php echo $collection_data['object_id'];?>" > 
					  <span class="text-red"><?php echo form_error('collection_name'); ?></span>
                    </div>

                    <div class="form-group">
                      <label for="collection_short_desc" class="control-label">Enter short description
					  <span class="text-red">*</span></label>
					  <input type="text" name="collection_short_desc" class="form-control input-sm" id="collection_short_desc" placeholder="Enter short description" value="<?php if($collection_data['collection_short_desc']!="") echo $collection_data['collection_short_desc']; else echo set_value('collection_short_desc');?>" required > 
					  <span class="text-red"><?php echo form_error('collection_short_desc'); ?></span>
                    </div>
					
					<div class="form-group">
                      <label for="collection_long_desc" class="control-label">Enter long description
					  <span class="text-red">*</span></label>
					  <textarea rows="4" cols="50" name="collection_long_desc" id="collection_long_desc" class="form-control input-sm" placeholder="Enter Collection  description" required><?php if($collection_data['collection_long_desc']!="") echo $collection_data['collection_long_desc']; else echo set_value('collection_long_desc');?></textarea>
					  <!--<input type="text" name="collection_long_desc" class="form-control input-sm" id="collection_long_desc" placeholder="Enter long description" value="<?php if($collection_data['collection_long_desc']!="") echo $collection_data['collection_long_desc']; else echo set_value('collection_long_desc');?>" required > -->
					  <span class="text-red"><?php echo form_error('collection_long_desc'); ?></span>
                    </div>
					
					<div class="form-group">
                      <label for="collection_start_date" class="control-label">Date From
					  <span class="text-red">*</span></label>
					  <input type="text" name="collection_start_date" class="form-control input-sm" id="date_from" placeholder="Start date" value="<?php if($collection_data['collection_start_date']!="") echo $collection_data['collection_start_date']; else echo set_value('collection_start_date');?>" required > 
					  <span class="text-red"><?php echo form_error('collection_start_date'); ?></span>
                    </div>

                    <div class="form-group">
                      <label for="collection_end_date" class="control-label">Date to
					  <span class="text-red">*</span></label>
					  <input type="text" name="collection_end_date" class="form-control input-sm" id="date_to" placeholder="End Date" value="<?php if($collection_data['collection_end_date']!="") echo $collection_data['collection_end_date']; else echo set_value('collection_end_date');?>" required > 
					  <span class="text-red"><?php echo form_error('collection_end_date'); ?></span>
                    </div>
					
					 <div class="form-group">
                      <label for="collection_end_date" class="control-label">Event Image</label>
					  <input class="form-control1" id="collection_img"  name="collection_img" type="file">
                    </div>
					
                  </div>
                    </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                    <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i>  Submit</button>
                    <a href="<?php echo base_url(); ?>lookcreator/manage_look"><button type="button" class="btn btn-sm btn-primary btn-rad"><i class="fa fa-reply"></i> Back</button></a>
                    <button class="btn btn-sm btn-default" type="reset"><i class="fa fa-close"></i> Cancel</button>
                  </div>
                </form>
              </div>
</section>
<script type="text/Javascript">
	$( document ).ready(function() {
		$('.nav-item-event').addClass("active");
		$('.nav-manage-event').addClass("active");
	});
	
	$(function () {
		var date_from =  $("#date_from").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii" , autoclose: true });
		var date_to = $("#date_to").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii", autoclose: true });
	});
</script>
