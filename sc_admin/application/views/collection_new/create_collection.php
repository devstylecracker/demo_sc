<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/redmond/jquery-ui.css" />
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<!--pqSelect dependencies-->
<!--link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev.css" />
<script src ="<?php echo base_url(); ?>assets/plugins/pqselect/pqselect.dev.js"></script-->

<script src="<?php echo base_url(); ?>assets/js/jquery.lazy.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/event_creater.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/fabric.min.js" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet" />

<div class="container">
  <section class="content">
    <div class="look-creater-wrp">
      <div class="row">
        <div class="col-xs-12 col-md-7 col-lg-7">
          <div class="canvas-wrp street-canvas-wrp">
            <!--  <div class="canvas-top">
					</div> -->
            <div class="canvas-middle">
              <div class="row">
                <div class="col-xs-12 col-md-5 col-lg-5">
                  <div class="panel">
                    <div class="panel-recom-prod">
                      <div class="title"> Recommended Products </div>
                      <ul id="recommended_products" class="recom-prods">

                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-md-7 col-lg-7">
                  <div class="panel canvas-outer">
                    <div class="canvas" id="street_canvas">
                      <img src="" alt="" id="street_canvas_img">
                      <input type="hidden" name="street_canvas_img_id" id="street_canvas_img_id" value="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel panel-look-desc-wrp">
                <div class="panel-look-desc">
                  <div>
                    <div class="form-group clearfix">
                      <label for="inputName" class="col-sm-3 control-label">Collection name</label>
                      <div class="col-sm-9">
                        <input type="text" name="collection_name" class="form-control input-sm" id="collection_name" placeholder="Enter Collection name"  onblur="genarate_slug('collection');" required>
                      </div>
                    </div>
                    <div class="form-group clearfix">
                      <label for="inputName" class="col-sm-3 control-label">Cover Description</label>
                      <div class="col-sm-9">
                        <input type="text" name="collection_short_desc" id="collection_short_desc" class="form-control input-sm" placeholder="Enter Collection cover description" required>
                      </div>
                    </div>
                    <div class="form-group clearfix">
                      <label for="inputName" class="col-sm-3 control-label">Description</label>
                      <div class="col-sm-9">
                       <textarea rows="4" cols="50" name="collection_long_desc" id="collection_long_desc" class="form-control input-sm" placeholder="Enter Collection  description" required></textarea> 
						<!--<input type="text" name="collection_long_desc" id="collection_long_desc" class="form-control input-sm" placeholder="Enter Collection  description" required>-->
                      </div>
                    </div>
                    <div class="form-group clearfix">
                      <label for="inputName" class="col-sm-3 control-label">Date From</label>
                      <div class="col-sm-9">
                        <input type="text" name="collection_start_date" placeholder="Date From" class="form-control input-sm date_from" id="date_from" value="<?php echo set_value('event_start_date'); ?>" required>
                      </div>
                    </div>
                    <div class="form-group clearfix">
                      <label for="inputName" class="col-sm-3 control-label">Date To</label>
                      <div class="col-sm-9">
                        <input type="text" name="collection_end_date" placeholder="Date To" class="form-control input-sm date_to" id="date_to" value="<?php echo set_value('collection_date_to'); ?>" required>
                      </div>
                    </div>
					<div class="form-group clearfix hide">
                      <label for="inputName" class="col-sm-3 control-label"></label>
                      <div class="col-sm-9">
						<input type="hidden" name="collection_slug" id="collection_slug">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel-footer">
                  <input type="submit" name="save_look" class="btn btn-primary btn-sm" id="save_look" value="Save" onclick="save_collection();">
                  <div id="error_text" class="error_text pull-right"></div>
                </div>

              </div>

            </div>
          </div>
        </div>

        <div class="col-xs-12 col-md-5 col-lg-5">
          <div class="canvas-data-wrp">
            <div class="canvas-data-selection">
              <div class="steps">
                <ul>
                  <li class="step1 active">Cover Image <span class="caret"></span></li>
                  <li class="step2">Select Products <span class="caret"></span></li>
                </ul>
              </div>
            </div>

            <div class="canvas-data">
              <div class="step-content canvas-template-wrp street-image-wrp">

                <ul>

                  <?php if(!empty($street_data)) {
										foreach($street_data as $val){?>

                    <li><img src="<?php echo base_url(); ?>assets/event_img/<?php echo $val['img_name']; ?>" onclick="put_in_canvas(<?php echo $val['id']; ?>,'<?php echo $val['img_name']; ?>');"></li>
                    <?php }} ?>

                </ul>

              </div>
              <div class="step-content canvas-products-wrp" style="display:block;">
                <div class="panel-step-level2">
                  <div class="row">

                    <div class="col-md-4">
                      <select name="gender" id="gender" class="form-control input-sm inline chosen-select" style="width:100%;" data-placeholder="Gender">
                        <option value="0">Select</option>
                        <?php
									if(!empty($parent_category_list)){
										foreach ($parent_category_list as $key => $value) {
											?>
                          <option value="<?php echo $value['id']; ?>">
                            <?php echo $value['category_name']; ?>
                          </option>
                          <?php
										}
									}
									?>
                      </select>
                    </div>

                    <div class="col-md-4">
                      <select name="cat" id="cat" class="form-control input-sm inline chosen-select" style="width:100%;" data-placeholder="Category">
                        <option value=""></option>
                        <?php
									/*if(!empty($category_list)){
										foreach ($category_list as $key => $value) {
											echo $value;
										}
									}*/
								?>
                      </select>
                    </div>


                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <select id="brand_combo" name="brand_combo" class="form-control input-sm inline chosen-select" style="width:100%;">
                        <option value="0">Select</option>
                        <?php if(!empty($brand_list)){
												foreach($brand_list as $val){
											?>
                          <option value="<?php echo $val['id']; ?>">
                            <?php echo $val['user_name'] .'-'. $val['cpa_percentage'].' %'; ?>
                          </option>
                          <?php } } ?>
                      </select>
                    </div>

                    <div class="col-md-4">
                      <div class="btn btn-primary btn-sm" id="show_products"> Show Products </div>
                    </div>
                  </div>

                  <div class="panel search-container">
                    <span class="btn btn-primary btn-sm backBtn">Back</span>
                    <input type="hidden" name="tag_name" id="tag_name" placeholder="Search by tag" class="form-control input-sm inline" style="width:140px;"></input>

                  </div>


                  <div class="cat-content-wrp panel">
                    <div id="cat-1-content" class="products-container cat-content cat-1-content sub-categories">

                      <ul id="getVariationVal">

                      </ul>
                    </div>

                    <div id="cat-2-content" class="cat-content cat-2-content sub-categories">
                      <div>Footwear</div>
                      <ul>
                        <li class="active">Footwear</li>
                        <li>Footwear 1</li>
                        <li>Footwear 2</li>
                        <li>Footwear 3</li>
                        <li>Footwear 4</li>
                        <li>Footwear 5</li>
                        <li>Footwear 6</li>
                        <li>Footwear 7</li>
                      </ul>
                    </div>

                    <div id="cat-3-content" class="cat-content cat-3-content sub-categories">
                      <div>Accessories</div>
                      <ul>
                        <li class="active">Accessories</li>
                        <li>Accessories 1</li>
                        <li>Accessories 2</li>
                        <li>Accessories 3</li>
                        <li>Accessories 4</li>
                        <li>Accessories 5</li>
                        <li>Accessories 6</li>
                        <li>Accessories 7</li>
                      </ul>
                    </div>

                    <div id="cat-4-content" class="cat-content cat-4-content sub-categories">
                      <div>Bags</div>
                      <ul>
                        <li class="active">Footwear</li>
                        <li>Bags 1</li>
                        <li>Bags 2</li>
                        <li>Bags 3</li>
                        <li>Bags 4</li>
                        <li>Bags 5</li>
                        <li>Bags 6</li>
                        <li>Bags 7</li>
                      </ul>
                    </div>
                  </div>




                </div>
                <div class="step-content canvas-effects-wrp">

                  <div id="cat-1-content" class="cat-content cat-1-content sub-categories11 panel-step-level2">

                    <div>Effects</div>
                    <ul>
                      <li>Bg Color
                        <div id="colorSelector">
                          <div style="background-color: #0000ff"></div>
                        </div>
                      </li>
                      <li class="btn btn-sm add-text-btn">Text</li>
                      <li class="btn btn-sm add-big-image">Bg Image</li>

                    </ul>

                  </div>

                  <div class="cat-content-wrp panel">

                    <div class="product-container cat-content cat-1-content sub-categories bg-container">
                      <ul>
                        <li><img src="<?php echo base_url(); ?>assets/backgrounds/bg-1.jpg" /></li>
                        <li><img src="<?php echo base_url(); ?>assets/backgrounds/bg-2.jpg" /></li>
                        <li><img src="<?php echo base_url(); ?>assets/backgrounds/bg-3.jpg" /></li>
                        <li><img src="<?php echo base_url(); ?>assets/backgrounds/bg-4.jpg" /></li>
                        <li><img src="<?php echo base_url(); ?>assets/backgrounds/bg-5.jpg" /></li>
                      </ul>
                    </div>


                  </div>




                </div>
              </div>

            </div>


          </div>
        </div>
      </div>

    </div>

  </section>
</div>
<script type="text/Javascript">
	function put_in_canvas(id,img){ $('#street_canvas_img_id').val(id); $('#street_canvas_img').attr('src', 'assets/event_img/'+img); } $(function () { var date_from = $("#date_from").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii" , autoclose: true }); var
	date_to = $("#date_to").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii", autoclose: true }); }); $('.nav-item-collection').addClass("active"); $('.nav-collection-creator').addClass("active");
</script>
