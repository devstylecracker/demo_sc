<?php

//print_r(@$category_details);

//echo @$category_details->status;

$users_menu=$this->data['users_menu'];



  if(@$category_details!=NULL)

  {

    $page="Edit";

  }else

  {

    $page="Add";

  }

?>

<section class="content-header">

        <h1>Category </h1>

          <ol class="breadcrumb">

            <li><a href="<?php echo base_url(); ?>home">Home</a></li>

            <li><a href="<?php echo base_url(); ?>category">Categories</a></li>

            <li class="active"><?php echo $page;?> Category</li>

          </ol>

</section>

<section class="content">

              <div class="box">

                <div class="box-header with-border">

                  <h3 class="box-title"><?php echo $page;?> Category</h3>

                </div><!-- /.box-header -->



                <form role="form" name="frmcategory" id="frmcategory" action="" method="post" enctype="multipart/form-data">

                  <div class="box-body">

                    <div class="row">

              <div class="col-md-4">

                    <div class="form-group">

                      <label for="categoryName" class="control-label">Category Name

						          <span class="text-red">*</span></label>

                      <input type="text" placeholder="Enter New Category Name" name="categoryName" id="categoryName" value="<?php if(@$category_details->name!="") echo @$category_details->name; else echo set_value('categoryName');?>" onblur="assignSlug()" class="form-control" > <span class="text-red"><?php echo form_error('categoryName'); ?></span>

                    </div>

                    <div class="form-group">

                      <label for="categorySlug" class="control-label">Slug

						          <span class="text-red">*</span></label>

                      <input type="text" placeholder="Enter Category Slug" name="categorySlug" id="categorySlug" value="<?php if(@$category_details->slug!="") echo @$category_details->slug; else echo set_value('categorySlug');?>"  <?php //if(@$category_details->slug!="") echo 'disabled'; ?> class="form-control"><span class="text-red"><?php echo form_error('categorySlug'); ?></span>

                    </div>

                    <div class="form-group">

                      <label class="control-label">Active</label>

                      <div>

                        <label class="radio-inline">



                        <?php if(isset($category_details->status) && $category_details->status==0) { $check_no =  'checked="true"'; }else { $check_ok = 'checked="true"'; } ?>



                          <input type="radio" value="1" name="active" <?php echo @$check_ok; ?> > Yes</label>

                        <label class="radio-inline"> <input type="radio" value="0" name="active" <?php echo @$check_no; ?> > No</label>

                      </div>

                    </div>

                    <div class="form-group">

                      <label class="control-label">Image</label>

                      <div>

                        <input type="file" name="category_img" id="category_img" >

                      </div>


                      <?php 
                        if(@$category_details!=NULL){
                          if($category_details->id > 0){
                            $images = scandir('../assets/images/products_category/category/'.$category_details->id,true);
                            if(!empty($images)){
                              echo '<img src="'.'../../../assets/images/products_category/category/'.$category_details->id.'/'.$images[0].'" style="width:50%;">';
                            }
                          }
                        }
                      ?>
                    </div>

                      </div>

                        </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">

                    <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Submit</button>

                    <a href="<?php echo base_url(); ?>category"><button type="button" class="btn btn-primary btn-sm btn-rad"><i class="fa fa-reply"></i> Back</button></a>

                    <button class="btn btn-default btn-sm" type="reset"><i class="fa fa-close"></i> Cancel</button>

                  </div>

                </form>

              </div><!-- /.box -->

</section>



<script type="text/Javascript">

$( document ).ready(function() {

	$('.nav-item-category').addClass("active");

  $('.nav-manage-cat').addClass("active");

});



function assignSlug() {



    var x = document.getElementById("categoryName");

    var slug = x.value.toLowerCase();

    var newslug = slug.replace(/ /g,"-");

    $("#categorySlug").val(newslug);



}

</script>

