<section class="content-header">
  <h1>Admin User</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>users">Manage Admin Users</a></li>
    <li class="active">Admin User</li>
  </ol>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">User Info</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="datatable">
              <tr>
                <th>ID</th>
                <td><?php echo $user_data[0]['id']; ?></td>
              </tr>
              <tr>
                <th>Role</th>
                <td><?php echo $user_data[0]['role_name']; ?></td>
              </tr>
              <tr>
                <th>Username</th>
                <td><?php echo $user_data[0]['user_name']; ?></td>
              </tr>
              <tr>
                <th>Email</th>
                <td><?php echo $user_data[0]['email']; ?></td>
              </tr>
              <tr>
                <th>First Name</th>
                <td><?php echo $user_data[0]['first_name']; ?></td>
              </tr>
              <tr>
                <th>Last Name</th>
                <td><?php echo $user_data[0]['last_name']; ?></td>
              </tr>
            </table>
          </div>

          <div class="box-footer">          
            <a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>users"><i class="fa fa-reply"></i> Back</a>
          </div>

        </div>
      </div>
    </div>
  </div>


</section>
<script type="text/Javascript">
$( document ).ready(function() {
  $('.nav-item-user').addClass("active");
  $('.nav-admin-user').addClass("active");
});
</script>
