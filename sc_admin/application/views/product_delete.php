
<script src="<?php echo base_url(); ?>assets/plugins/sortable/jquery.sortable.min.js"></script>

<section class="content-header">
        <h1>Manage Product Size Order</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>">Home</a></li>
            <li class="active">Manage Product Size Order</li>
          </ol>
</section>
<form role="form" name="frmtag" id="frmtag" action="" method="post">
	<div class="box-body">
		   <?php echo $this->session->flashdata('feedback'); ?>
	    <div class="row">                             
	       <div class="col-md-6">
	        <div class="form-group">
	          <!-- <label for="ProductDesc">Product Description</label> -->
	           <label for="product_delid">Product Id <span class="text-red">*</span></label>
	           <!-- required="" -->
	          <textarea tabindex="3" name="product_id" id="product_id" placeholder="Product Id" rows="3" class="form-control" required numeric ><?php echo set_value('product_id'); ?></textarea>
	          <span class="text-red"><?php echo form_error('product_id'); ?></span>
	        </div>	        
	    </div>               
	</div>
	 <div class="box-footer text-left">
		<button class="btn btn-sm btn-primary" tabindex="30" type="submit" onclick="return confirm('Are you sure you want to delete this product?');" ><i class="fa fa-save"></i> Product Delete</button>
		<!-- <a href="<?php //echo base_url(); ?>product_new"><button type="button" class="btn btn-primary  btn-sm btn-rad"><i class="fa fa-reply"></i> Back</button></a>
		<button class="btn btn-sm btn-default" tabindex="31" type="reset"><i class="fa fa-close"></i> Cancel</button> -->               
      </div>
</form>	
<style type="text/css">
	ul li{list-style: none;}
	.list-group-item{ background-color: #c6c6c6;cursor: pointer; }
</style>