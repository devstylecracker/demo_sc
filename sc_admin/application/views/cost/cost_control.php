<script src ="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<style type="text/css">
h5.box-title,h3.box-title{
font-weight: 700;
}
table, th, td {
    border: 1px solid #D3D3D3;
    border-collapse: collapse;
    padding: 5px;
}
.success{
	background-color: darkseagreen;
}
</style>
<section class="content-header">
	<h1>Cost Control</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url(); ?>home">Home</a></li>
		<li><a href="<?php echo base_url(); ?>cost_control">Cost Control</a></li>
		<li class="active">Cost Control</li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-body">
			<div class="row">
				<div class="col-md-3">
					<div class="box-header with-border">
						<h3 class="box-title"> COST FOR Rs.2999 BOX</h3>
					</div><!-- /.box-header -->
					
					<form role="form" name="form_2999" id="form_2999" action="" method="post">
						<div class="form-group">
							<label for="box_cost_2999" class="control-label">Max Product Cost
							<span class="text-red">*</span></label>
							<input type="text" placeholder="Enter Box Cost" name="box_cost_2999" id="box_cost_2999" class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_2999['max_box_cost']; ?>"> 
							<div id="text-red"></div>
							<span class="text-red" id="text-red"><?php echo form_error('box_cost_2999'); ?></span>
						</div>
						<div class="form-group">
							<label for="shipping_cost_2999" class="control-label">Shipping Cost
							<span class="text-red">*</span></label>
							<input type="text" placeholder="Enter Shipping Cost" name="shipping_cost_2999" id="shipping_cost_2999" class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_2999['shipping_cost']; ?>">
							<div id="text-red"></div>
							<span class="text-red"><?php echo form_error('shipping_cost_2999'); ?></span>
						</div>
						<hr>
						<div class="form-group">
							<label for="acquisition_cost_2999" class="control-label">Acquisition Cost
							<span class="text-red">*</span></label>
							<input type="text" placeholder="Enter Acquisition Cost" name="acquisition_cost_2999" id="acquisition_cost_2999"  class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_2999['acquisition_cost']; ?>">
							<div id="text-red" ></div>
							<span class="text-red"><?php echo form_error('acquisition_cost_2999'); ?></span>
						</div> 
						<!--<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								<label>Start Date-Time<span class="text-red">*</span></label>
								<input type="text" name="sdate_time1" id="sdate_time1" placeholder="Enter Start Date" class="form-control datetimepicker" value="<?php echo $form_2999['start_date_time']; ?>">
								<div id="text-red"></div>
								<span class="text-red"> <?php echo form_error('sdate_time1'); ?></span>
								</div>
							</div>
							<div class="col-md-6">  
								<div class="form-group">
								<label>End Date-Time<span class="text-red">*</span></label>
								<input type="text" name="edate_time1" id="edate_time1" placeholder="Enter End Date" class="form-control datetimepicker" value="<?php echo $form_2999['end_date_time']; ?>">
								<div id="text-red"></div>
								<span class="text-red"> <?php echo form_error('edate_time1'); ?></span>
								</div> 
							</div>
						</div> <hr>-->
						<div class="form-group">
							<label for="packaging_cost_2999" class="control-label">Packaging Cost
							<span class="text-red">*</span></label>
							<input type="text" placeholder="Enter Packaging Cost" name="packaging_cost_2999" id="packaging_cost_2999"  class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_2999['package_cost']; ?>">
							<div id="text-red"></div>
							<span class="text-red" ><?php echo form_error('packaging_cost_2999'); ?></span>
						</div>
						<div class="form-group">
							<h5 class="box-title">Transaction Cost</h5>
							<div class="row">
								<div class="col-md-6">
									<label for="transaction_cost_2999_online" class="control-label">Online (in %)
									<span class="text-red">*</span></label>
									<input type="text" placeholder="Enter Online Cost" name="transaction_cost_2999_online" id="transaction_cost_2999_online" class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_2999['transaction_cost_online']; ?>">
									<div id="text-red"></div>
									<span class="text-red"><?php echo form_error('transaction_cost_2999_online'); ?></span>
								</div>
								<div class="col-md-6">
									<label for="transaction_cost_2999_cod" class="control-label">COD
									<span class="text-red">*</span></label>
									<input type="text" placeholder="Enter COD Cost" name="transaction_cost_2999_cod" id="transaction_cost_2999_cod" class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_2999['transaction_cost_cod']; ?>">
									<div id="text-red"></div>
									<span class="text-red"><?php echo form_error('transaction_cost_2999_cod'); ?></span>
								</div>
							</div>
						</div>
						<div class="box-header with-border">
							<!--<h4 class="box-title">Transaction Cost Online : <span id="tco_2999"><?php //echo number_format($packageCostOnline_2999, 2); ?></span></h4>
							<h4 class="box-title">Transaction Cost COD : <span id="tcc_2999"><?php //echo number_format($packageCostCod_2999, 2); ?></span></h4>-->
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label for="" class="control-label">Profit Online</label>
										 <table>
										   
										     <tbody>
										      <tr style="font-size: 12px;font-weight: 700;">
										        <td>2,999.00 - <span id="tco_2999"><?php echo number_format($packageCostOnline_2999, 2); ?></span></td>
										      </tr>
										      <tr class="success" style="font-size: 16px;font-weight: 700;">
										        <td><span id="tco_cal_2999"><i class="fa fa-inr"></i> <?php echo number_format($calculationOnline_2999, 2); ?></span></td>
										      </tr>
										    </tbody>
										  </table>
										
										
									</div>
									<div class="col-md-6">
										<label for="" class="control-label">Profit COD</label>

										 <table>
										   
										     <tbody>
										      <tr style="font-size: 12px;font-weight: 700;">
										        <td>2,999.00 - <span id="tcc_2999"><?php echo number_format($packageCostCod_2999, 2); ?></span></td>
										      </tr>
										      <tr class="success" style="font-size: 16px;font-weight: 700;">
										        <td><span id="tcc_cal_2999"><i class="fa fa-inr"></i> <?php echo number_format($calculationCod_2999, 2); ?></span></td>
										      </tr>
										    </tbody>
										  </table>

										
									</div>
								</div>
							</div>
						</div><!-- /.box-header -->
						<div class="box-footer">
							<button class="btn btn-sm btn-primary" type="submit" name="btn_2999" id="btn_2999" ><i class="fa fa-save"></i>  Submit</button>
							
						</div>
					</form>
				</div>
				
				<div class="col-md-3">
					<div class="box-header with-border">
						<h3 class="box-title"> COST FOR Rs.4999 BOX</h3>
					</div><!-- /.box-header -->
					<form role="form" name="form_4999" id="form_4999" action="" method="post">
						<div class="form-group">
							<label for="box_cost_4999" class="control-label">Max Product Cost
							<span class="text-red">*</span></label>
							<input type="text" placeholder="Enter Box Cost" name="box_cost_4999" id="box_cost_4999" class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_4999['max_box_cost']; ?>"> 
							<div id="text-red"></div>
							<span class="text-red"><?php echo form_error('box_cost_4999'); ?></span>
						</div>
						<div class="form-group">
							<label for="shipping_cost_4999" class="control-label">Shipping Cost
							<span class="text-red">*</span></label>
							<input type="text" placeholder="Enter Shipping Cost" name="shipping_cost_4999" id="shipping_cost_4999" class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_4999['shipping_cost']; ?>">
							<div id="text-red"></div>
							<span class="text-red"><?php echo form_error('shipping_cost_4999'); ?></span>
						</div><hr>
						<div class="form-group">
							<label for="acquisition_cost_4999" class="control-label">Acquisition Cost
							<span class="text-red">*</span></label>
							<input type="text" placeholder="Enter Acquisition Cost" name="acquisition_cost_4999" id="acquisition_cost_4999" class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_4999['acquisition_cost']; ?>">
							<div id="text-red"></div>
							<span class="text-red"><?php echo form_error('acquisition_cost_4999'); ?></span>
						</div> 
						<!--<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Start Date-Time<span class="text-red">*</span></label>
									<input type="text" name="sdate_time2" id="sdate_time2" placeholder="Enter Start Date" class="form-control datetimepicker" value="<?php echo $form_4999['start_date_time']; ?>">
									<div id="text-red"></div>
									<span class="text-red"> <?php echo form_error('sdate_time2'); ?></span>
								</div>
							</div>
							<div class="col-md-6">  
								<div class="form-group">
									<label>End Date-Time<span class="text-red">*</span></label>
									<input type="text" name="edate_time2" id="edate_time2" placeholder="Enter End Date" class="form-control datetimepicker" value="<?php echo $form_4999['end_date_time']; ?>">
									<div id="text-red"></div>
									<span class="text-red"> <?php echo form_error('edate_time2'); ?></span>
								</div> 
							</div>
						</div> <hr>-->
						<div class="form-group">
							<label for="packaging_cost_4999" class="control-label">Packaging Cost
							<span class="text-red">*</span></label>
							<input type="text" placeholder="Enter Packaging Cost" name="packaging_cost_4999" id="packaging_cost_4999"  class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_4999['package_cost']; ?>">
							<div id="text-red"></div>
							<span class="text-red"><?php echo form_error('packaging_cost_4999'); ?></span>
						</div>
						<div class="form-group">
							<h5 class="box-title">Transaction Cost</h5>
							<div class="row">
								<div class="col-md-6">
									<label for="transaction_cost_4999_online" class="control-label">Online (in %)
									<span class="text-red">*</span></label>
									<input type="text" placeholder="Enter Online Cost" name="transaction_cost_4999_online" id="transaction_cost_4999_online" class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_4999['transaction_cost_online']; ?>">
									<div id="text-red"></div>
									<span class="text-red"><?php echo form_error('transaction_cost_4999_online'); ?></span>
								</div>
								<div class="col-md-6">
									<label for="transaction_cost_4999_cod" class="control-label">COD
									<span class="text-red">*</span></label>
									<input type="text" placeholder="Enter COD Cost" name="transaction_cost_4999_cod" id="transaction_cost_4999_cod" class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_4999['transaction_cost_cod']; ?>">
									<div id="text-red"></div>
									<span class="text-red"><?php echo form_error('transaction_cost_4999_cod'); ?></span>
								</div>
							</div>
						</div>
						<div class="box-header with-border">
							<!--<h4 class="box-title">Transaction Cost Online : <span id="tco_4999"><?php echo number_format($packageCostOnline_4999, 2);; ?></span></h4>
							<h4 class="box-title">Transaction Cost COD : <span id="tcc_4999"><?php echo number_format($packageCostCod_4999, 2); ?></span></h4>-->
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label for="" class="control-label">Profit Online</label>
										 <table>
										   
										     <tbody>
										      <tr style="font-size: 12px;font-weight: 700;">
										        <td>4,999.00 - <span id="tco_4999"><?php echo number_format($packageCostOnline_4999, 2); ?></span></td>
										      </tr>
										      <tr class="success" style="font-size: 16px;font-weight: 700;">
										        <td><span id="tco_cal_4999"><i class="fa fa-inr"></i> <?php echo number_format($calculationOnline_4999, 2); ?></span></span></td>
										      </tr>
										    </tbody>
										  </table>

										
									</div>
									<div class="col-md-6">
										<label for="" class="control-label">Profit COD</label>
										<table>
										   
										     <tbody>
										      <tr style="font-size: 12px;font-weight: 700;">
										        <td>4,999.00 - <span id="tcc_4999"><?php echo number_format($packageCostCod_4999, 2); ?></span></td>
										      </tr>
										      <tr class="success" style="font-size: 16px;font-weight: 700;">
										        <td><span id="tcc_cal_4999"><i class="fa fa-inr"></i> <?php echo number_format($calculationCod_4999, 2); ?></span></td>
										      </tr>
										    </tbody>
										  </table>
										
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<button class="btn btn-sm btn-primary" type="submit" name="btn_4999" id="btn_4999" ><i class="fa fa-save"></i>  Submit</button>
							
						</div>
					</form>
				</div><!-- /.box-header -->


				<div class="col-md-3">
					<div class="box-header with-border">
						<h3 class="box-title"> COST FOR Rs.6999 BOX</h3>
					</div><!-- /.box-header -->
					<form role="form" name="form_6999" id="form_6999" action="" method="post">
						<div class="form-group">
							<label for="box_cost_6999" class="control-label">Max Product Cost
							<span class="text-red">*</span></label>
							<input type="text" placeholder="Enter Box Cost" name="box_cost_6999" id="box_cost_6999" class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_6999['max_box_cost']; ?>">
							<div id="text-red"></div>
							<span class="text-red"><?php echo form_error('box_cost_6999'); ?></span>
						</div>
						<div class="form-group">
							<label for="shipping_cost_6999" class="control-label">Shipping Cost
							<span class="text-red">*</span></label>
							<input type="text" placeholder="Enter Shipping Cost" name="shipping_cost_6999" id="shipping_cost_6999" class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_6999['shipping_cost']; ?>">
							<div id="text-red"></div>
							<span class="text-red"><?php echo form_error('shipping_cost_6999'); ?></span>
						</div><hr>
						<div class="form-group">
							<label for="acquisition_cost_6999" class="control-label">Acquisition Cost
							<span class="text-red">*</span></label>
							<input type="text" placeholder="Enter Acquisition Cost" name="acquisition_cost_6999" id="acquisition_cost_6999"  class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_6999['acquisition_cost']; ?>">
							<div id="text-red"></div>
							<span class="text-red"><?php echo form_error('acquisition_cost_6999'); ?></span>
						</div>
						<!--<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								<label>Start Date-Time<span class="text-red">*</span></label>
								<input type="text" name="sdate_time3" id="sdate_time3" placeholder="Enter Start Date" class="form-control datetimepicker" value="<?php echo $form_6999['start_date_time']; ?>">
								<div id="text-red"></div>
								<span class="text-red"> <?php echo form_error('sdate_time3'); ?></span>
								</div>
							</div>
							<div class="col-md-6">  
								<div class="form-group">
								<label>End Date-Time<span class="text-red">*</span></label>
								<input type="text" name="edate_time3" id="edate_time3" placeholder="Enter End Date" class="form-control datetimepicker" value="<?php echo $form_6999['end_date_time']; ?>">
								<div id="text-red"></div>
								<span class="text-red"> <?php echo form_error('edate_time3'); ?></span>
								</div> 
							</div>
						</div> <hr>-->
						<div class="form-group">
							<label for="packaging_cost_6999" class="control-label">Packaging Cost
							<span class="text-red">*</span></label>
							<input type="text" placeholder="Enter Packaging Cost" name="packaging_cost_6999" id="packaging_cost_6999"  class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_6999['package_cost']; ?>">
							<div id="text-red"></div>
							<span class="text-red"><?php echo form_error('packaging_cost_6999'); ?></span>
						</div>
						<div class="form-group">
							<h5 class="box-title">Transaction Cost</h5>
							<div class="row">
								<div class="col-md-6">
									<label for="transaction_cost_6999_online" class="control-label">Online (in %)
									<span class="text-red">*</span></label>
									<input type="text" placeholder="Enter Online Cost" name="transaction_cost_6999_online" id="transaction_cost_6999_online" class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_6999['transaction_cost_online']; ?>">
									<div id="text-red"></div>
									<span class="text-red"><?php echo form_error('transaction_cost_6999_online'); ?></span>
								</div>
								<div class="col-md-6">
									<label for="transaction_cost_6999_cod" class="control-label">COD
									<span class="text-red">*</span></label>
									<input type="text" placeholder="Enter COD Cost" name="transaction_cost_6999_cod" id="transaction_cost_6999_cod" class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_6999['transaction_cost_cod']; ?>">
									<div id="text-red"></div>
									<span class="text-red"><?php echo form_error('transaction_cost_6999_cod'); ?></span>
								</div>
							</div>
						</div>
						<div class="box-header with-border">
							<!--<h4 class="box-title">Transaction Cost Online : <span id="tco_6999"><?php echo number_format($packageCostOnline_6999, 2); ?></span></h4>
							<h4 class="box-title">Transaction Cost COD : <span id="tcc_6999"><?php echo number_format($packageCostCod_6999, 2); ?></span></h4>-->
							
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label for="" class="control-label">Profit Online</label>
										<table>
										   
										     <tbody>
										      <tr style="font-size: 12px;font-weight: 700;">
										        <td>6,999.00 - <span id="tco_6999"><?php echo number_format($packageCostOnline_6999, 2); ?></span></td>
										      </tr>
										      <tr class="success" style="font-size: 16px;font-weight: 700;">
										        <td><span id="tco_cal_6999"><i class="fa fa-inr"></i> <?php echo number_format($calculationOnline_6999, 2); ?></span></td>
										      </tr>
										    </tbody>
										  </table>
										
									</div>
									<div class="col-md-6">
										<label for="" class="control-label">Profit COD</label>
										<table>
										   
										     <tbody>
										      <tr style="font-size: 12px;font-weight: 700;">
										        <td>6,999.00 - <span id="tcc_6999"><?php echo number_format($packageCostCod_6999, 2); ?></span></td>
										      </tr>
										      <tr class="success" style="font-size: 16px;font-weight: 700;">
										        <td><span id="tcc_cal_6999"><i class="fa fa-inr"></i> <?php echo number_format($calculationCod_6999, 2); ?></span></td>
										      </tr>
										    </tbody>
										  </table>
									</div>
								</div>
							</div>
						</div><!-- /.box-header -->
						<div class="box-footer">
							<button class="btn btn-sm btn-primary" type="submit" name="btn_6999" id="btn_6999" ><i class="fa fa-save"></i>  Submit</button>
							
						</div>
					</form>
			</div>

			<div class="col-md-3">
				<div class="box-header with-border">
					<h3 class="box-title"> COST FOR CUSTOM BOX</h3>
				</div><!-- /.box-header -->
				<form role="form" name="form_custom" id="form_custom" action="" method="post">
					<div class="form-group">
						<label for="box_cost_custom" class="control-label">Max Product Cost
						<span class="text-red">*</span></label>
						<input type="text" placeholder="Enter Box Cost" name="box_cost_custom" id="box_cost_custom" class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_custom['max_box_cost']; ?>"> 
						<div id="text-red"></div>
						<span class="text-red"><?php echo form_error('box_cost_custom'); ?></span>
					</div>
					<div class="form-group">
						<label for="shipping_cost_custom" class="control-label">Shipping Cost
						<span class="text-red">*</span></label>
						<input type="text" placeholder="Enter Shipping Cost" name="shipping_cost_custom" id="shipping_cost_custom" class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_custom['shipping_cost']; ?>">
						<div id="text-red"></div>
						<span class="text-red"><?php echo form_error('shipping_cost_custom'); ?></span>
					</div><hr>
					<div class="form-group">
						<label for="acquisition_cost_custom" class="control-label">Acquisition Cost
						<span class="text-red">*</span></label>
						<input type="text" placeholder="Enter Acquisition Cost" name="acquisition_cost_custom" id="acquisition_cost_custom"  class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_custom['acquisition_cost']; ?>">
						<div id="text-red"></div>
						<span class="text-red"><?php echo form_error('acquisition_cost_custom'); ?></span>
					</div> 
					<!--<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Start Date-Time<span class="text-red">*</span></label>
								<input type="text" name="sdate_time4" id="sdate_time4" placeholder="Enter Start Date" class="form-control datetimepicker"  value="<?php echo $form_custom['start_date_time']; ?>">
								<div id="text-red"></div>
								<span class="text-red"> <?php echo form_error('sdate_time4'); ?></span>
							</div>
						</div>
						<div class="col-md-6">  
							<div class="form-group">
								<label>End Date-Time<span class="text-red">*</span></label>
								<input type="text" name="edate_time4" id="edate_time4" placeholder="Enter End Date" class="form-control datetimepicker"  value="<?php echo $form_custom['end_date_time']; ?>">
								<div id="text-red"></div>
								<span class="text-red"> <?php echo form_error('edate_time4'); ?></span>
							</div> 
						</div>
					</div><hr> -->
					<div class="form-group">
						<label for="packaging_cost_custom" class="control-label">Packaging Cost
						<span class="text-red">*</span></label>
						<input type="text" placeholder="Enter Packaging Cost" name="packaging_cost_custom" id="packaging_cost_custom"  class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_custom['package_cost']; ?>">
						<div id="text-red" ></div>
						<span class="text-red"><?php echo form_error('packaging_cost_custom'); ?></span>
					</div>
					<div class="form-group">
						<h5 class="box-title">Transaction Cost</h5>
						<div class="row">
							<div class="col-md-6">
								<label for="transaction_cost_custom_online" class="control-label">Online (in %)
								<span class="text-red">*</span></label>
								<input type="text" placeholder="Enter Online Cost" name="transaction_cost_custom_online" id="transaction_cost_custom_online" class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_custom['transaction_cost_online']; ?>">
								<div id="text-red"></div>
								<span class="text-red"><?php echo form_error('transaction_cost_custom_online'); ?></span>
							</div>
							<div class="col-md-6">
								<label for="transaction_cost_custom_cod" class="control-label">COD
								<span class="text-red">*</span></label>
								<input type="text" placeholder="Enter COD Cost" name="transaction_cost_custom_cod" id="transaction_cost_custom_cod" class="form-control" onkeypress="return onlyNumbersWithDot(event);" value="<?php echo $form_custom['transaction_cost_cod']; ?>">
								<div id="text-red"></div>
								<span class="text-red"><?php echo form_error('transaction_cost_custom_cod'); ?></span>
							</div>
						</div>
					</div>
					<div class="box-header with-border">
						<!--<h4 class="box-title">Transaction Cost Online : <span id="tco_custom"><?php echo number_format($packageCostOnline_custom, 2); ?></span></h4>
						<h4 class="box-title">Transaction Cost COD : <span id="tcc_custom"><?php echo number_format($packageCostCod_custom, 2); ?></span></h4>-->
						
						
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label for="" class="control-label">Profit Online</label>
										<table>
										   
										     <tbody>
										      <tr style="font-size: 12px;font-weight: 700;">
										        <td>8,000.00 - <span id="tco_custom"><?php echo number_format($packageCostOnline_custom, 2); ?></span></td>
										      </tr>
										      <tr class="success" style="font-size: 16px;font-weight: 700;">
										        <td><span id="tco_cal_custom"><i class="fa fa-inr"></i> <?php echo number_format($calculationOnline_custom, 2); ?></span></td>
										      </tr>
										    </tbody>
										  </table>
									</div>
									<div class="col-md-6">
										<label for="" class="control-label">Profit COD</label>
										
										<table>
										   
										     <tbody>
										      <tr style="font-size: 12px;font-weight: 700;">
										        <td>8,000.00 - <span id="tcc_4999"><?php echo number_format($packageCostCod_custom, 2); ?></span></td>
										      </tr>
										      <tr class="success" style="font-size: 16px;font-weight: 700;">
										        <td><span id="tcc_cal_custom"><i class="fa fa-inr"></i> <?php echo number_format($calculationCod_custom, 2); ?></span></td>
										      </tr>
										    </tbody>
										  </table>
									</div>
								</div>
							</div>
					</div><!-- /.box-header -->
					<div class="box-footer">
						<button class="btn btn-sm btn-primary" type="submit" name="btn_custom" id="btn_custom" ><i class="fa fa-save"></i>  Submit</button>
						
					</div>
				</form>
			</div>
			</div><!-- /.box-body -->

		<!--<div class="box-footer">
			<button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i>  Submit</button>
			<button class="btn btn-sm btn-default" type="reset"><i class="fa fa-close"></i> Reset</button>
		</div>-->
		</div><!-- /.box -->
	</div>	
</section>
<style>
.error{color:red;}
</style>
<script src="<?php echo base_url('assets/js/jquery.validate.min.js'); ?>"></script>

<script>
	$(document).ready(function(){
		$("#form_2999").validate({
			ignore: "input[type='text']:hidden",
			rules: {
				box_cost_2999: {
					required:true,
					number: true,
					max:2999
				},
				shipping_cost_2999: {
					required: true,
					number: true,
				},
				acquisition_cost_2999: {
					required: true,
					number: true,
				},
				/*sdate_time1: {
					required: true,
				},
				edate_time1: {
					required: true,
				},*/
				packaging_cost_2999: {
					required: true,
					number: true,
				},
				transaction_cost_2999_online: {
					required: true,
					number: true,
				},
				transaction_cost_2999_cod: {
					required: true,
					number: true,
				}
			},
			messages: {
				box_cost_2999: {
					required: "Please provide box cost",
					number: "Please provide only number",
					max:"Max Product Cost cannot be more than 2999.",
				},
				shipping_cost_2999: {
					required: "Please provide shipping cost",
					number: "Please provide only number",
				},
				acquisition_cost_2999: {
					required: "Please provide acquisition cost",
					number: "Please provide only number",
				},
				/*sdate_time1: {
					required: "Please select start date",
				},
				edate_time1: {
					required: "Please select end date",
				},*/
				packaging_cost_2999: {
					required: "Please provide packaging cost",
					number: "Please provide only number",
				},
				transaction_cost_2999_online: {
					required: "Please provide online transaction cost",
					number: "Please provide only number",
				},
				transaction_cost_2999_cod: {
					required: "Please provide cod transaction cost",
					number: "Please provide only number",
				}
			},
			/*errorPlacement: function(error, element) {
			error.appendTo('#text-red');
			}*/
			submitHandler: function(form) {
				var box_cost_2999 					= $('#box_cost_2999').val();
				var shipping_cost_2999 				= $('#shipping_cost_2999').val();
				var acquisition_cost_2999 			= $('#acquisition_cost_2999').val();
				var sdate_time1 					= ''; //$('#sdate_time1').val();
				var edate_time1 					= ''; //$('#edate_time1').val();
				var packaging_cost_2999 			= $('#packaging_cost_2999').val();
				var transaction_cost_2999_online 	= $('#transaction_cost_2999_online').val();
				var transaction_cost_2999_cod 		= $('#transaction_cost_2999_cod').val();
				$.ajax({
					type:'POST',
					url: "<?php echo base_url(); ?>cost_control/cost_calculation",
					data: {'box':'2999', 'box_cost':box_cost_2999, 'shipping_cost':shipping_cost_2999, 'acquisition_cost':acquisition_cost_2999,'sdate_time':sdate_time1,'edate_time':edate_time1, 'packaging_cost':packaging_cost_2999, 'transaction_cost_online':transaction_cost_2999_online, 'transaction_cost_cod':transaction_cost_2999_cod},
					dataType:'json',
					cache:false,
					success:function(data){ 
					console.log(data);
						$('#tco_2999').html(data.packageCostOnline);
						$('#tcc_2999').html(data.packageCostCod);
						$('#tco_cal_2999').html(data.calculationOnline);
						$('#tcc_cal_2999').html(data.calculationCod);
						//location.reload();
					},
					error: function(data){
						console.log("error");
						console.log(data);
					}
				});
			}
		});
	});
	
	
	$(document).ready(function(){
		$("#form_4999").validate({
			ignore: "input[type='text']:hidden",
			rules: {
				box_cost_4999: {
					required: true,
					number: true,
					max:4999
				},
				shipping_cost_4999: {
					required: true,
					number: true,
				},
				acquisition_cost_4999: {
					required: true,
					number: true,
				},
				/*sdate_time2: {
					required: true,
				},
				edate_time2: {
					required: true,
				},*/
				packaging_cost_4999: {
					required: true,
					number: true,
				},
				transaction_cost_4999_online: {
					required: true,
					number: true,
				},
				transaction_cost_4999_cod: {
					required: true,
					number: true,
				}
			},
			messages: {
				box_cost_4999: {
					required: "Please provide box cost",
					number: "Please provide only number",
					max:"Max Product Cost cannot be more than 4999."
				},
				shipping_cost_4999: {
					required: "Please provide shipping cost",
					number: "Please provide only number",
				},
				acquisition_cost_4999: {
					required: "Please provide acquisition cost",
					number: "Please provide only number",
				},
				/*sdate_time2: {
					required: "Please select start date",
				},
				edate_time2: {
					required: "Please select end date",
				},*/
				packaging_cost_4999: {
					required: "Please provide packaging cost",
					number: "Please provide only number",
				},
				transaction_cost_4999_online: {
					required: "Please provide online transaction cost",
					number: "Please provide only number",
				},
				transaction_cost_4999_cod: {
					required: "Please provide cod transaction cost",
					number: "Please provide only number",
				}
			},
			/*errorPlacement: function(error, element) {
			error.appendTo('#text-red');
			}*/
			submitHandler: function(form) {
				var box_cost_4999 					= $('#box_cost_4999').val();
				var shipping_cost_4999 				= $('#shipping_cost_4999').val();
				var acquisition_cost_4999 			= $('#acquisition_cost_4999').val();
				var sdate_time2 					= ''; //$('#sdate_time2').val();
				var edate_time2 					= ''; //$('#edate_time2').val();
				var packaging_cost_4999 			= $('#packaging_cost_4999').val();
				var transaction_cost_4999_online 	= $('#transaction_cost_4999_online').val();
				var transaction_cost_4999_cod 		= $('#transaction_cost_4999_cod').val();
				 $.ajax({
					type:'POST',
					url: "<?php echo base_url(); ?>cost_control/cost_calculation",
					data: {'box':'4999', 'box_cost':box_cost_4999, 'shipping_cost':shipping_cost_4999, 'acquisition_cost':acquisition_cost_4999, 'sdate_time':sdate_time2, 'edate_time':edate_time2, 'packaging_cost':packaging_cost_4999, 'transaction_cost_online':transaction_cost_4999_online, 'transaction_cost_cod':transaction_cost_4999_cod},
					dataType:'json',
					cache:false,
					success:function(data){  
						$('#tco_4999').html(data.packageCostOnline);
						$('#tcc_4999').html(data.packageCostCod);
						$('#tco_cal_4999').html(data.calculationOnline);
						$('#tcc_cal_4999').html(data.calculationCod);
						//location.reload();
					},
					error: function(data){
						console.log("error");
						console.log(data);
					}
				});
			}
		});
	});

	
	$(document).ready(function(){
		$("#form_6999").validate({
			ignore: "input[type='text']:hidden",
			rules: {
				box_cost_6999: {
					required: true,
					number: true,
					max:6999
				},
				shipping_cost_6999: {
					required: true,
					number: true,
				},
				acquisition_cost_6999: {
					required: true,
					number: true,
				},
				/*sdate_time3: {
					required: true,
				},
				edate_time3: {
					required: true,
				},*/
				packaging_cost_6999: {
					required: true,
					number: true,
				},
				transaction_cost_6999_online: {
					required: true,
					number: true,
				},
				transaction_cost_6999_cod: {
					required: true,
					number: true,
				}
			},
			messages: {
				box_cost_6999: {
					required: "Please provide box cost",
					number: "Please provide only number",
					max:"Max Product Cost cannot be more than 6999."
				},
				shipping_cost_6999: {
					required: "Please provide shipping cost",
					number: "Please provide only number",
				},
				acquisition_cost_6999: {
					required: "Please provide acquisition cost",
					number: "Please provide only number",
				},
				/*sdate_time3: {
					required: "Please select start date",
				},
				edate_time3: {
					required: "Please select end date",
				},*/
				packaging_cost_6999: {
					required: "Please provide packaging cost",
					number: "Please provide only number",
				},
				transaction_cost_6999_online: {
					required: "Please provide online transaction cost",
					number: "Please provide only number",
				},
				transaction_cost_6999_cod: {
					required: "Please provide cod transaction cost",
					number: "Please provide only number",
				}
			},
			/*errorPlacement: function(error, element) {
			error.appendTo('#text-red');
			}*/
			submitHandler: function(form) {
				var box_cost_6999 					= $('#box_cost_6999').val();
				var shipping_cost_6999 				= $('#shipping_cost_6999').val();
				var acquisition_cost_6999 			= $('#acquisition_cost_6999').val();
				var sdate_time3 					= ''; //$('#sdate_time3').val();
				var edate_time3 					= ''; //$('#edate_time3').val();
				var packaging_cost_6999 			= $('#packaging_cost_6999').val();
				var transaction_cost_6999_online 	= $('#transaction_cost_6999_online').val();
				var transaction_cost_6999_cod 		= $('#transaction_cost_6999_cod').val();
				$.ajax({
					type:'POST',
					url: "<?php echo base_url(); ?>cost_control/cost_calculation",
					data: {'box':'6999', 'box_cost':box_cost_6999, 'shipping_cost':shipping_cost_6999, 'acquisition_cost':acquisition_cost_6999, 'sdate_time':sdate_time3, 'edate_time':edate_time3, 'packaging_cost':packaging_cost_6999, 'transaction_cost_online':transaction_cost_6999_online, 'transaction_cost_cod':transaction_cost_6999_cod},
					dataType:'json',
					cache:false,
					success:function(data){  
						$('#tco_6999').html(data.packageCostOnline);
						$('#tcc_6999').html(data.packageCostCod);
						$('#tco_cal_6999').html(data.calculationOnline);
						$('#tcc_cal_6999').html(data.calculationCod);
						//location.reload();
					},
					error: function(data){
						console.log("error");
						console.log(data);
					}
				});
			}
		});
	});
	
	$(document).ready(function(){
		$("#form_custom").validate({
			ignore: "input[type='text']:hidden",
			rules: {
				box_cost_custom: {
					required: true,
					number: true,
				},
				shipping_cost_custom: {
					required: true,
					number: true,
				},
				acquisition_cost_custom: {
					required: true,
					number: true,
				},
				/*sdate_time4: {
					required: true,
				},
				edate_time4: {
					required: true,
				},*/
				packaging_cost_custom: {
					required: true,
					number: true,
				},
				transaction_cost_custom_online: {
					required: true,
					number: true,
				},
				transaction_cost_custom_cod: {
					required: true,
					number: true,
				}
			},
			messages: {
				box_cost_custom: {
					required: "Please provide box cost",
					number: "Please provide only number",
				},
				shipping_cost_custom: {
					required: "Please provide shipping cost",
					number: "Please provide only number",
				},
				acquisition_cost_custom: {
					required: "Please provide acquisition cost",
					number: "Please provide only number",
				},
				/*sdate_time4: {
					required: "Please select start date",
				},
				edate_time4: {
					required: "Please select end date",
				},*/
				packaging_cost_custom: {
					required: "Please provide packaging cost",
					number: "Please provide only number",
				},
				transaction_cost_custom_online: {
					required: "Please provide online transaction cost",
					number: "Please provide only number",
				},
				transaction_cost_custom_cod: {
					required: "Please provide cod transaction cost",
					number: "Please provide only number",
				}
			},
			/*errorPlacement: function(error, element) {
			error.appendTo('#text-red');
			}*/
			submitHandler: function(form) {
				var box_cost_custom 				= $('#box_cost_custom').val();
				var shipping_cost_custom 			= $('#shipping_cost_custom').val();
				var acquisition_cost_custom 		= $('#acquisition_cost_custom').val();
				var sdate_time4 					= ''; //$('#sdate_time4').val();
				var edate_time4 					= ''; //$('#edate_time4').val();
				var packaging_cost_custom 			= $('#packaging_cost_custom').val();
				var transaction_cost_custom_online 	= $('#transaction_cost_custom_online').val();
				var transaction_cost_custom_cod 	= $('#transaction_cost_custom_cod').val();
				$.ajax({
					type:'POST',
					url: "<?php echo base_url(); ?>cost_control/cost_calculation",
				data: {'box':'custom','box_cost':box_cost_custom,'shipping_cost':shipping_cost_custom,'acquisition_cost':acquisition_cost_custom,'sdate_time':sdate_time4,'edate_time':edate_time4,'packaging_cost':packaging_cost_custom,'transaction_cost_online':transaction_cost_custom_online, 'transaction_cost_cod':transaction_cost_custom_cod},
				dataType:'json',
					cache:false,
					success:function(data){  
						$('#tco_custom').html(data.packageCostOnline);
						$('#tcc_custom').html(data.packageCostCod);
						$('#tco_cal_custom').html(data.calculationOnline);
						$('#tcc_cal_custom').html(data.calculationCod);
						//location.reload();
					},
					error: function(data){
						console.log("error");
						console.log(data);
					}
				});
			}
		});
	});

</script>


<script type="text/javascript">
$( document ).ready(function() { 

$("#sdate_time1,#edate_time1,#sdate_time2,#edate_time2,#sdate_time3,#edate_time3,#sdate_time4,#edate_time4").datetimepicker({ dateFormat: "yyyy-mm-dd hh:ii" , autoclose: true });
});

function onlyNumbersWithDot(e) {           
            var charCode;
            if (e.keyCode > 0) {
                charCode = e.which || e.keyCode;
            }
            else if (typeof (e.charCode) != "undefined") {
                charCode = e.which || e.keyCode;
            }
            if (charCode == 46)
                return true
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
</script>