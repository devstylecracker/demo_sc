<script src ="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<style>
table {
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
    font-size: 12px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
<section class="content-header">
  <h1>Sales Report <small class="label label-info"><?php echo $total_rows; ?></small></h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
    <li><a href="<?php echo base_url(); ?>cost_control">Cost Control</a></li>
    <li class="active">Sales Report</li>
  </ol>
</section>
  <section class="content">
    <div class="box">
      <div class="box-body">
       <form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url();?>cost_control/sales_report" method="post">
		<input type="hidden"  name="is_download" id="is_download" value="0" />
    <div class="row">
      <div class="col-md-2">
       
        <select name="search_by" id="search_by" class="form-control input-sm" >
          <option value="">Search by</option>
          <option value="1" <?php echo $search_by==1 ? 'selected' : ''; ?> >Order ID</option>
          <option value="2" <?php echo $search_by==2 ? 'selected' : ''; ?> >Customer Name</option>
         <!--  <option value="5" <?php echo $search_by==5 ? 'selected' : ''; ?> >Status</option> -->
         <!--  <option value="6" <?php echo $search_by==6 ? 'selected' : ''; ?> >Order Unique No.</option> -->
          <option value="8" <?php echo $search_by==8 ? 'selected' : ''; ?> >Date Range</option>
        </select>
      </div>
      <div class="col-md-3">
           <div class="input-group">
         <div id ="filter" style="display: block;" >
          <input type="text" name="table_search" value="<?php echo set_value('table_search')=='' && set_value('table_search')==0 ? $table_search : set_value('table_search'); ?>" id="table_search" class="form-control input-sm pull-right" placeholder="Search"  required=""/>
         </div>
        <div id ="daterange" style="display: none;">
            <div class="row">
              <div class="col-md-5">
                <div class="form-group">
                  <input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm" id="date_from" value="<?php echo set_value('date_from'); ?>">
                </div>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="<?php echo set_value('date_to'); ?>">
              </div>
             </div>
            </div>
        </div>
        <div class="input-group-btn">
          <!--<button class="btn btn-sm btn-default" type="submit" onclick="submitForm();" ><i class="fa fa-search"></i></button>-->
          <button class="btn btn-sm btn-default" type="submit" onclick="submitForm();" ><i class="fa fa-search"></i></button>
        </div>
      </div>
      </div>
      <div class="col-md-1">
        <a href="<?php echo base_url(); ?>Manage_orders" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
      </div>
     
      <div class="col-md-3">
		<button title="Sales Report" id="download_excel" class="btn btn-primary btn-sm"  onclick="downloadExcel();"><i class="fa fa-download"></i> Sales Report</button>
        
      </div>
       <div class="col-md-3">
        <div class="pull-right" style="padding-top: 10px;">    

          <?php echo $this->pagination->create_links(); ?>
        </div>
      </div>
    </div>
</form>
     </div>
      <div class="table-responsive">
        <table >
      <tr>
            <th>Sr. No.</th>
            <th>Date</th>
            <th>Order No.</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Box Type</th>
            <th>Order Total</th>
            <th>CTC Order Total</th>
            <th>First Exchange Cost</th>
            <th>Second Exchange Cost</th>
            <th>Refund</th>
            <th>Gross CTC</th>
            <th>Profit</th>
        </tr>
		<?php
			if(is_array($arrSalaesReport) && count($arrSalaesReport)>0)
			{
				$i=1;
				/* echo "<pre>";
				print_r($arrSalaesReport);
				echo "</pre>";  */
				foreach($arrSalaesReport as $salesReport)
				{
					$productPrice = $this->cost_model->get_product_meta($box_order_products,$salesReport['order_display_no']);
					
					$price 				= array_column($productPrice, 'price');
					$compare_price 		= array_column($productPrice, 'compare_price');
					$totalPrice			= array_sum($price);
					$totalComparePrice	= array_sum($compare_price);
					//$totalProfit		= ($totalPrice - $totalComparePrice);
					$totalProfit		= ($salesReport['product_price'] - $totalComparePrice);
					?>
						<tr>
						  <td><?php echo $i++; ?></td>
						  <td><?php echo $salesReport['created_datetime']; ?></td>
						  <td><?php echo $salesReport['order_display_no']; ?></td>
						  <td><?php echo $salesReport['first_name'].' '.$salesReport['last_name']; ?></td>
						  <td><?php echo ($salesReport['gender'] == 1)?'Female':'Male'; ?></td>
						  <td><?php echo number_format($salesReport['product_price'], 2); ?></td>
						  <td><?php echo number_format($salesReport['order_total'], 2); ?></td>
						  <td><?php echo number_format($totalPrice, 2); ?></td>
						  <td></td>
						  <td></td>
						  <td></td>
						  <td><?php echo number_format($totalComparePrice, 2); ?></td>
						  <td><?php echo number_format($totalProfit, 2); ?></td>
						</tr>
					<?php
				}
			}
		
		?>
       </table>
    </div>
          <?php echo $this->pagination->create_links(); ?>
        </div>
    </div><!-- /.box -->
  </section>
 <script> 
function downloadExcel()
{
    
	 var search_by = '';
     var table_search = '';
     var encode_table_search = '';
	  
     $("#is_download").val('1');
     search_by = $('#search_by').val();
     search_by = search_by.trim();
     table_search = $('#table_search').val();  
	if(search_by !=1 && search_by !=2  && search_by !=8 )
	  {
		  alert('please select date range');
		  return false
	  } 
     $('#table_search').removeAttr('required');
     table_search = table_search.trim();
     var is_download = $("#is_download").val();
	
     if(search_by == 1)
     {
		//alert('by order id'+ table_search);
	   encode_table_search = window.btoa(table_search);
     }
	 if(search_by == 2)
     {
		 //alert('by name'+ table_search);
	   encode_table_search = window.btoa(table_search);       
     }
	 else if(search_by == 8)
     {
		 //alert('by date'+ table_search);
        var dateFrom = $("#date_from").val();
        var dateTo   = $("#date_to").val();

        var date_search = dateFrom+','+dateTo;
        encode_table_search = window.btoa(date_search);
     }
     else
     {
        encode_table_search = table_search;
     }
     if(!encode_table_search){ encode_table_search = '0'; }
    	search_by2 = $('#search_by2').val();
    	//search_by2 = search_by2.trim();
    // alert(encode_table_search);
     //var _href = '<?php echo base_url(); ?>'+'View_orders_payment/exportExcel/downloadExcel';
     var newhref = '<?php echo base_url(); ?>'+'cost_control/exportExcel/downloadExcel'+'/'+search_by+'/'+encode_table_search+'/'+is_download+'/'+search_by2;
     //alert(newhref);

     $('#frmsearch').attr('action',newhref);
	
}
 
 </script>       

<script type="text/Javascript">
$(function () {
      $("#date_from").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true});
      $("#date_to").datepicker({ format: "yyyy-mm-dd", endDate: '+0d', autoclose: true });
    // $('#date').datepicker({format: "yyyy-mm-dd"});
      $(".nav-tabs a").click(function(){
        $(this).tab('show');
      });
      $('.nav-tabs a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
      });
    });

$( document ).ready(function() {
$("body").addClass('sidebar-collapse');

$('.btn-quick-view').click( function(event){

  var orderUniqueId = $(this).attr('id');
     event.preventDefault();


  if(orderUniqueId!='')
  {
    dispalyOrderDetail(orderUniqueId);
  }

//  $(this).parents('tr').next('.tr-quick-view').find('.box-quick-view').slideToggle(500);
  //$(this).parents('tr').toggleClass("open");
  var elem = $(this).parents('tr').next('tr.tr-quick-view');
  elem.slideToggle('10');
  $(this).parents('tr').toggleClass("open");

});


 $('.nav-item-order').addClass("active");
  $('.nav-manage-orders').addClass("active");
});



var filterType = $('#search_by').val();
if(filterType == 8)
{
    $("#filter").hide();
    $("#daterange").show();
    $("#table_search").removeAttr('required');
    $("#date_from").attr('required','');
    $("#box_filter").hide();
}else if(filterType == 10)
{
  $("#status_filter").hide();
  $("#filter").hide();  
  $("#box_filter").show();
  $("#daterange").hide();

}else if(filterType == 5)
{
  $("#filter").hide(); 
  $("#status_filter").show();
  $("#box_filter").hide();
  $("#daterange").hide();
}



 $("#search_by").change(function(){

    var filterType = $(this).val();
    if(filterType == 8)
    {
        $("#filter").hide();
        $("#daterange").show();
        $("#table_search").removeAttr('required');
        $("#date_from").attr('required','');
        $("#box_filter").hide();
        $("#status_filter").hide();
    }else if(filterType == 10)
    {
      $("#status_filter").hide();
      $("#filter").hide();     
      $("#box_filter").show();
      $("#daterange").hide();

    }else if(filterType == 5)
    {
      $("#filter").hide(); 
      $("#status_filter").show();
      $("#box_filter").hide();
      $("#daterange").hide();
    }else
    {
      $("#filter").show();
      $("#daterange").hide();
      $("#box_filter").hide();
      $("#status_filter").hide();
    }
    if(filterType == 6){
        $(".msg-days-lapsed").show();
    }else
    {
        $(".msg-days-lapsed").hide();
    }

  });


function order_close(orderUniqId)
{
  $('.table-tr-collapse tr.tr-quick-view').slideUp(10);
  $('.table-tr-collapse tr').removeClass("open");
}

function submitForm(){
   $("#is_download").val('0');
   var newhref = '<?php echo base_url(); ?>'+'cost_control/sales_report';
   $('#frmsearch').attr('action',newhref);
   $('#frmsearch').submit();
}

</script>