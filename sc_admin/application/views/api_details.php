<?php
//echo "<pre>"; print_r(@$feedback);

$users_menu=$this->data['users_menu'];
?>
<section class="content-header">
        <h1>Api Details</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>home">Home</a></li>            
            <li class="active">Api Detail</li>
          </ol>
</section>

<section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Your API Credential</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" name="frmtag" id="frmfdb" action="" method="post">
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">

                  <div class="form-group">
                    <label for="api_key" class="control-label">API Key :</label>                              
                    <label for="api_key" class="control-label"><?php echo $api_key; ?></label>  
                  </div>

                   <div class="form-group">
                    <label for="api_secret_key" class="control-label">Secret Key : </label>
                    <label for="api_secret_key" class="control-label"><?php echo $api_secret_key; ?></label>
                  </div> 

                 
              </div>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
              <a href="<?php echo base_url(); ?>home"><button type="button" class="btn btn-primary btn-sm btn-rad"><i class="fa fa-reply"></i> Back</button></a>
          </div>
        </form>
    </div><!-- /.box -->        
</section>

<script type="text/Javascript">
$( document ).ready(function() {
  $('.nav-item-api').addClass("active");
});
</script>

