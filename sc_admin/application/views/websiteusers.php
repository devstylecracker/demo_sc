  <?php
  $users_menu=$this->data['users_menu'];
  $table_search = $table_search == '0' ? '' : $table_search;  
  ?>
  <section class="content-header">
          <h1>Manage Website Users</h1>
            <ol class="breadcrumb">
              <li><a href="<?php echo base_url(); ?>home">Home</a></li>
              <li class="active">Website Users</li>
            </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">List of Website Users <small class="label pull-right label-info"><?php echo $total_rows; ?></small></h3>
      <div class="pull-right">
      </div>
      </div><!-- /.box-header -->
    <div class="box-body">
      <?php echo $this->session->flashdata('feedback'); ?>

      <form name="frmsearch" id="frmsearch" class="form-group" action="<?php echo base_url(); ?>websiteusers" method="post">
        <div class="row">
          <div class="col-md-2">
              <?php $search_by = set_value('search_by')=='' ? $search_by : set_value('search_by'); ?>
            <select name="search_by" id="search_by" class="form-control input-sm" >

              <option value="0">Search by</option>
              <option selected="selected" value="1" <?php echo $search_by==1 ? 'selected' : ''; ?>>Username</option>
              <option value="2" <?php echo $search_by==2 ? 'selected' : ''; ?>>Emailid</option>
             <option value="3" <?php echo $search_by==3 ? 'selected' : ''; ?>>Date</option>
              <option value="4" <?php echo $search_by==4 ? 'selected' : ''; ?>>User ID</option>
               <option value="5" <?php echo $search_by==5 ? 'selected' : ''; ?>>First Name</option>
               <option value="6" <?php echo $search_by==6 ? 'selected' : ''; ?>>Last Name</option>

               <option value="9" <?php echo $search_by==9 ? 'selected' : ''; ?>>source</option>
               <!--<option value="11" <?php echo set_value('search_by')==10 ? 'selected' : ''; ?>>gmail</option>
               <option value="12" <?php echo set_value('search_by')==11 ? 'selected' : ''; ?>>stylecracker</option>!-->

            </select>
          </div>
          <div class="col-md-3">
            <div class="input-group">
            <input type="text" name="table_search" value="<?php echo set_value('table_search')=='' && set_value('table_search')==0 ? $table_search : set_value('table_search'); ?>" id="table_search" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo set_value('table_search'); ?>" required=""/>
                        
              <div class="input-group-btn">
                <button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <a href="websiteusers" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Reset</a>
          </div>
          <div class="col-md-5">
            <!--div class="records-per-page pull-right">
              <select name="records-per-page" class="form-control input-sm">
                <option>Per Page</option>
                <option value="10" selected="selected">10</option>
                <option value="25">25</option>
                <option value="50">50</option>
              </select>
            </div-->
            <div class="pull-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>
      </form>

    <div class="table-responsive">
      <table class="table table-bordered table-striped dataTable" id="example2">
        <thead>
          <tr>
            <th>USER ID</th>
            <!--<th>Username</th>-->
            <th>Email</th>          
            <th>First Name</th>
            <th>Last Name</th>
            <th>Mobile Number</th>
            <th>Registered From</th>
            <th>Last visit time</th>
            <th>Action</th>           
            
          </tr>
        </thead>
        <tbody>
          <?php if(!empty($users_data)) { $i = 1;
            foreach($users_data as $val){
              ?>
              <tr>
                <td><?php echo $val->id; ?></td>
                <!--<td><?php echo $val->user_name; ?></td>-->
                <td><?php echo $val->email; ?></td>
                <td><?php echo $val->first_name; ?></td>
                <td><?php echo $val->last_name; ?></td>
                <td><?php echo $val->contact_no; ?></td>
                <td><?php echo $val->registered_from; ?></td>
                 <td><?php //echo $val->created_datetime;
                          $date = new DateTime($val->created_datetime);
                          echo $date->format('d-m-Y H:i:s');
                 ?></td>
                <td>
                  <!--<?php if(in_array("15",$users_menu)) { ?>
                    <a href="<?php echo base_url(); ?>websiteusers/user_view/<?php echo $val->id; ?>">
                      <button type="button" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> View</button></a>                      
                      <?php } ?>-->

                       <?php if(in_array("15",$users_menu)) { ?>
                    <a href="<?php echo base_url(); ?>Stylist/user_view/<?php echo $val->id; ?>">
                      <button type="button" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> View</button></a>                      
                      <?php } ?>
                    </td>
                  </tr>
                  <?php $i++; } }else{ ?>
                    <tr>
                      <td colspan="5">Sorry no record found</td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <?php echo $this->pagination->create_links(); ?>
           
   
           </div>
          </div>

        </section>
        <!-- /.content -->

        <script type="text/Javascript">
        $( document ).ready(function() {
          $('[data-toggle=confirmation]').confirmation({
            title:'Are you sure?',
            onConfirm : function(){
              var del_id = $(this).closest('td').find('.btn-danger').attr('data-id');
              $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "users/user_delete",
                data: { id: del_id },
                cache:false,
                success:
                function(data){
                  location.reload(); //as a debugging message.
                }

              });


            }
          });
          $('.nav-item-user').addClass("active");
          $('.nav-website-user').addClass("active");




        });
        </script>
