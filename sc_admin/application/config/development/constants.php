<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


/*
|--------------------------------------------------------------------------
| (Stylecracker) User-Defined Global Variables
|--------------------------------------------------------------------------|
| 
*/

define('PRODUCT_USE_MAX', 10000); // highest Product Used Count
define('LIVE_SITE_URL', 'https://www.stylecracker.com/sc_admin/'); // highest Product Used Count
define('LOOK_CREATOR_PAGE_OFFSET', 100);

define('SC_SITE_URL', 'https://www.stylecracker.com/');

define('WOMEN_CATEGORY_ID',1);
define('MEN_CATEGORY_ID',12);
define('LIVE_SITE', 'https://www.stylecracker.com/'); 
define('POINT_VAlUE',500);
define('MIN_CART_VALUE',1000);
define('REDEEM_POINTS_CART_TEXT','Redeem');

/*sms gatway*/

define('SMS_USERNAME','stylecracker_com');
// define('SMS_PASSWORD','99903979');
define('SMS_PASSWORD','SC$123*');

/*New Design related CONSTANTS*/

define('PA_BRAND_LIST',serialize(array('1'=>(object) ['brand_id' => '18136','brand_name' => 'Zara'],'2'=>(object) ['brand_id' => '18139','brand_name' => 'Adidas'],'3'=>(object) ['brand_id' => '18148','brand_name' => 'Converse'],'4'=>(object) ['brand_id' => '20835','brand_name' => 'Zooomberg'],'5'=>(object) ['brand_id' => '28296','brand_name' => 'Color Buckket'],'6'=>(object) ['brand_id' => '28188','brand_name' => 'London Bee  Clothing'],'7'=>(object) ['brand_id' => '35820','brand_name' => 'Tuna London'],'8'=>(object) ['brand_id' => '21841','brand_name' => 'Jaded London'],'9'=>(object) ['brand_id' => '18182','brand_name' => 'Reebok'],'10'=>(object) ['brand_id' => '18258','brand_name' => 'Tresmode'])));

/*Added for SCBOX */

define('SCBOX_PACKAGE',serialize(array(
'0'=>(object) ['id' => '1','name' => '3 items','desc' =>  'Apparel, Bag, Jewellery','price'=>'2999','productid' => '190481','packname' => 'package1','numofproducts'=>'3'],
'1'=>(object) ['id' => '2','name' => '4 items','desc' =>  'Apparel, Footwear, Bag, Jewellery, Beauty Products','price'=>'4999','productid' => '190482','packname' => 'package2','numofproducts'=>'4'],
'2'=>(object) ['id' => '3','name' => '5 items','desc' =>  'Apparel, Footwear, Bag, Jewellery, Beauty Products','price'=>'6999','productid' => '190483','packname' => 'package3','numofproducts'=>'5'],
'3'=>(object) ['id' => '4','name' => 'Unlimited items','desc' => 'Customized based on the type and number of products that you are looking for.','price'=>'','productid' => '190484','packname' => 'package4','numofproducts'=>''])));


define('SCBOX_ID',"'1','2','3','4'");
define('SCBOX_PRODUCTID',serialize(array('0'=>'190481','1'=>'190482','2'=>'190483','3'=>'190484')));
define('SCBOX_PRODUCTID_STR',"'190481','190482','190483','190484'");
define('SCBOX_BRAND_ID','46352');
define('SCBOX_CUSTOM_ID','190484');

define('SCBOX_PRODUCT_BRAND_IDS',"'40030','40282','45967','50358','50376','47506','44948','30822','45668','43334','29784','41532','40020','25169','28545','35258','27949
','44491','20936','41624','32166','24156','51140','51144','50744','50725','50720','50688','50624','50486','50408','50330','43334','30701'");



/*
define('SCBOX_SKINTONE_LIST',serialize(array('0'=>(object) ['id' => '#F8D5C2','colorid' => '1','name' => ''],'1'=>(object) ['id' => '#F8D5C3','colorid' => '2','name' => ''],'2'=>(object) ['id' => '#E6AA86','colorid' => '3','name' => ''],'3'=>(object) ['id' => '#E6AA87','colorid' => '4','name' => ''],'4'=>(object) ['id' => '#D2946B','colorid' => '5','name' => ''],'5'=>(object) ['id' => '#C58F63','colorid' => '6','name' => ''],'6'=>(object) ['id' => '#845736','colorid' => '7','name' => ''],'7'=>(object) ['id' => '#825535','colorid' => '8','name' => ''],'8'=>(object) ['id' => '#705031','colorid' => '9','name' => ''],'9'=>(object) ['id' => '#87563b','colorid' => '10','name' => ''])));*/

/* commented on 270317---------------
define('SCBOX_SKINTONE_LIST',serialize(array(	
	'0'=>(object) ['id' => '#F9DBC3','colorid' => '3','name' => ''],
	'1'=>(object) ['id' => '#F5C8A1','colorid' => '4','name' => ''],
	'2'=>(object) ['id' => '#F4B57F','colorid' => '5','name' => ''],
	'3'=>(object) ['id' => '#EDA366','colorid' => '6','name' => ''],
	'4'=>(object) ['id' => '#F6B597','colorid' => '7','name' => ''],
	'5'=>(object) ['id' => '#F29D76','colorid' => '8','name' => ''],
	'6'=>(object) ['id' => '#B65947','colorid' => '10','name' => ''])));*/
	
	
	
define('SCBOX_SKINTONE_LIST',serialize(array(	
	'0'=>(object) ['id' => '#f5d6c1','colorid' => '3','name' => 'Pale'],
	'1'=>(object) ['id' => '#ebbca7','colorid' => '4','name' => 'Rosy Pale'],
	'2'=>(object) ['id' => '#e3ac8a','colorid' => '5','name' => 'Light'],
	'3'=>(object) ['id' => '#cf986d','colorid' => '6','name' => 'Wheatish'],
	'4'=>(object) ['id' => '#ab7343','colorid' => '7','name' => 'Tan'],
	'5'=>(object) ['id' => '#825937','colorid' => '8','name' => 'Medium'],
	'6'=>(object) ['id' => '#6b4f31','colorid' => '10','name' => 'Dark'])));	

/*
define('SCBOX_COLOR_LIST',serialize(array('0'=>(object) ['id' => '1','name' => 'Earthy','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/earthly.png'],'1'=>(object) ['id' => '2','name' => 'Neutral','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/neutral.png'],'2'=>(object) ['id' => '3','name' => 'Bright','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/bright.png'],'3'=>(object) ['id' => '4','name' => 'Black & White','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/black-white.png'],'4'=>(object) ['id' => '5','name' => 'Pastel','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/pastel.png'],'5'=>(object) ['id' => '6','name' => 'Light','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/light.png'])));
*/

define('SCBOX_COLOR_LIST',serialize(array(
'0'=>(object) ['id' => '5','name' => 'Pastel','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/pastel.png'],
'1'=>(object) ['id' => '2','name' => 'Neutral','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/neutral.png'],
'2'=>(object) ['id' => '6','name' => 'METALLIC','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/light.png'],
'3'=>(object) ['id' => '1','name' => 'Earthy','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/earthly.png'],
'4'=>(object) ['id' => '3','name' => 'Bright','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/bright.png'],
'5'=>(object) ['id' => '4','name' => 'Black & White','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/black-white.png'])));

/* commented on 270317
define('SCBOX_PRINT_LIST',serialize(array('0'=>(object) ['id' => '1','name' => 'Houndstooth','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/houndtooth.png'],'1'=>(object) ['id' => '2','name' => 'Plaid','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/plaid.png'],'2'=>(object) ['id' => '3','name' => 'Psychedelic','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/psychedelic.png'],'3'=>(object) ['id' => '4','name' => 'Aztec','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/aztex.png'],'4'=>(object) ['id' => '5','name' => 'Tie-Dye','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/tie-dye.png'],'5'=>(object) ['id' => '6','name' => 'Checkered','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/chcekered.png'],'6'=>(object) ['id' => '7','name' => 'Stripes','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/stripes.png'],'7'=>(object) ['id' => '8','name' => 'Floral','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/floral.png'],'8'=>(object) ['id' => '9','name' => 'Geometric','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/geometric.png'],'9'=>(object) ['id' => '10','name' => 'Abstract','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/abstract.png'],'10'=>(object) ['id' => '11','name' => 'Animal','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/animal.png'],'3'=>(object) ['id' => '4','name' => 'Aztec','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/aztex.png'])));
*/


define('SCBOX_PRINT_LIST',serialize(array(
'0'=>(object) ['id' => '9','name' => 'Geometric','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/geometric.png'],
'1'=>(object) ['id' => '8','name' => 'Floral','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/floral.png'],
'2'=>(object) ['id' => '10','name' => 'Abstract','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/abstract.png'],
'3'=>(object) ['id' => '11','name' => 'Animal','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/animal.png'],
'4'=>(object) ['id' => '12','name' => 'No Prints','imgpath' =>  SC_SITE_URL.'assets/seventeen/images/qa/no_prints.jpg']
)));


define('SCBOX_BUDGET_LIST',serialize(array('0'=>(object) ['id' => '1','name' => '2000'],'1'=>(object) ['id' => '2','name' => '8000'],'2'=>(object) ['id' => '3','name' => '15000'])));

define('SCBOX_MENTOP_LIST',"'XS','S','M','L','XL'");
define('SCBOX_MENBOTTOM_LIST',"'24','26','28','30','32','34','36','38','40','42','44','46','48'");
define('SCBOX_MENFOOT_LIST',"'6','6.5','7','7.5','8','8.5','9','9.5','10','10.5','11','11.5','12'");

define('SCBOX_WOMENTOP_LIST',"'XS','S','M','L','XL','XXL'");
define('SCBOX_WOMENBOTTOM_LIST',"'24','26','27','28','30','32','34','36','38','40'");
define('SCBOX_WOMENFOOT_LIST',"'3','3.5','4','4.5','5','5.5','6','6.5','7','7.5','8'");
define('SCBOX_BANDSIZE_LIST',"'30','32','34','36','38','40','42'");
define('SCBOX_CUPSIZE_LIST',"'A','B','C','D','DD','E','F'");
define('SCBOX_SIZE',serialize(array('0'=>(object) ['id' => '2','name' => 'FREE'])));

/* SCBOX */

// pa brands list
define('PA_BRAND_LIST_MEN',serialize(array(
'1'=>(object) ['brand_name' => 'Reliance Trends','budget_id' => '4706','is_show' => '1','brand_key'=>'1'],
'2'=>(object) ['brand_name' => 'Westside','budget_id' => '4706','is_show' => '1','brand_key'=>'2'],
'3'=>(object) ['brand_name' => 'Maxx','budget_id' => '4706','is_show' => '1','brand_key'=>'3'],
'4'=>(object) ['brand_name' => 'Zara','budget_id' => '4707','is_show' => '1','brand_key'=>'4'],
'5'=>(object) ['brand_name' => 'Jack & Jones','budget_id' => '4707','is_show' => '1','brand_key'=>'5'],
'6'=>(object) ['brand_name' => 'Kenneth Cole','budget_id' => '4708','is_show' => '1','brand_key'=>'6'],
'7'=>(object) ['brand_name' => 'Diesel','budget_id' => '4708','is_show' => '1','brand_key'=>'7'],
'8'=>(object) ['brand_name' => 'Manyavar','budget_id' => '4708','is_show' => '1','brand_key'=>'8'],
'9'=>(object) ['brand_name' => 'Burberry','budget_id' => '4709','is_show' => '1','brand_key'=>'9'],
'10'=>(object) ['brand_name' => 'Rohit Bal','budget_id' => '4709','is_show' => '1','brand_key'=>'10'],
'11'=>(object) ['brand_name' => 'test','budget_id' => '4709','is_show' => '0','brand_key'=>'11'])));

define('PA_BRAND_LIST_WOMEN',serialize(array(
'1'=>(object) ['brand_name' => 'Pantaloons','budget_id' => '4706','is_show' => '1','brand_key'=>'1'],
'2'=>(object) ['brand_name' => 'Westside','budget_id' => '4706','is_show' => '0','brand_key'=>'2'],
'3'=>(object) ['brand_name' => 'Lifestyle','budget_id' => '4706','is_show' => '1','brand_key'=>'3'],
'4'=>(object) ['brand_name' => 'Zara','budget_id' => '4707','is_show' => '1','brand_key'=>'4'],
'5'=>(object) ['brand_name' => 'Forever21','budget_id' => '4707','is_show' => '1','brand_key'=>'5'],
'6'=>(object) ['brand_name' => 'Global Desi','budget_id' => '4707','is_show' => '1','brand_key'=>'6'],
'7'=>(object) ['brand_name' => 'Anita Dongre','budget_id' => '4708','is_show' => '1','brand_key'=>'7'],
'8'=>(object) ['brand_name' => 'BCBG','budget_id' => '4708','is_show' => '1','brand_key'=>'8'],
'9'=>(object) ['brand_name' => 'Michael Kors','budget_id' => '4708','is_show' => '1','brand_key'=>'9'],
'10'=>(object) ['brand_name' => 'Gucci','budget_id' => '4709','is_show' => '1','brand_key'=>'10'],
'11'=>(object) ['brand_name' => 'Fendi','budget_id' => '4709','is_show' => '1','brand_key'=>'11'],
'12'=>(object) ['brand_name' => 'Dolce & Gabbana','budget_id' => '4709','is_show' => '1','brand_key'=>'12'],
'13'=>(object) ['brand_name' => 'test','budget_id' => '4709','is_show' => '0','brand_key'=>'13'])));

define('ATTRIBUTE_SIZE_ID','17');

define('IMAGIFY_BRANDID',serialize(array(
'0'=>'40030',
'1'=>'40282',
'2'=>'47530',
'3'=>'47355',
'4'=>'45967',
'5'=>'25032',
'6'=>'28296',
'7'=>'47508',
'8'=>'23347',
'9'=>'40895',
'10'=>'47506',
'11'=>'27992',
'12'=>'43484',
'13'=>'45670',
'14'=>'44948',
'15'=>'30822',
'16'=>'38634',
'17'=>'28792',
'18'=>'45668',
'19'=>'25454',
'20'=>'39492',
'21'=>'43334',
'22'=>'47400',
'23'=>'41307',
'24'=>'31465',
'25'=>'39495',
'26'=>'45643',
'27'=>'41532',
'28'=>'47378',
'29'=>'40020',
'30'=>'25169',
'31'=>'28545',
'32'=>'47535',
'33'=>'35258',
'34'=>'27949',
'35'=>'36454',
'36'=>'44491',
'37'=>'20936',
'38'=>'41624',
'39'=>'45781',
'40'=>'36415',
'41'=>'45647',
'42'=>'32166',
'43'=>'39646',
'44'=>'47279',
'45'=>'47239',
'46'=>'24156',
'47'=>'20963',
'48'=>'20835',
'49'=>'20541',
'50'=>'20424',
'51'=>'20734')));

define('FEEDBACK_QUESTIONS',serialize(array(
'1'=>(object) ['name' => 'bop','question' => 'How was your StyleCracker Box Experience from start to finish? ','is_show' => '1'],
'2'=>(object) ['name' => 'scs','question' => 'Stylist communication skills','is_show' => '1'],
'3'=>(object) ['name' => 'sa','question' => 'Stylists ability to understand your likes and dislikes','is_show' => '1'],
'4'=>(object) ['name' => 'time','question' => 'Timeliness of box delivery','is_show' => '1'],
'5'=>(object) ['name' => 'package','question' => 'Packaging of the delivered box','is_show' => '1'],
'6'=>(object) ['name' => 'quality','question' => 'Quality of products','is_show' => '1'],
'7'=>(object) ['name' => 'unique','question' => 'Uniqueness of products','is_show' => '1'],
'8'=>(object) ['name' => 'money','question' => 'Value for money','is_show' => '1'],
'9'=>(object) ['name' => 'communicate','question' => 'Communication with StyleCracker team in case of any queries relating to order status and delivery','is_show' => '1'],
'10'=>(object) ['name' => 'returnp','question' => 'Return process (if applicable)','is_show' => '0'],
'11'=>(object) ['name' => 'refundp','question' => 'Refund process (if applicable)','is_show' => '0'],
'12'=>(object) ['name' => 'recommend','question' => 'Would you recommend the SC Box to your friends and family?','is_show' => '0'],
'13'=>(object) ['name' => 'stylistexperience','question' => 'Did the stylist go out of the way to make your experience a little extra pleasurable? If yes, please let us know.','is_show' => '0'],
'14'=>(object) ['name' => 'processexperience','question' => 'Is there anything we could do in the entire process to further enhance your experience?.','is_show' => '0'],
'15'=>(object) ['name' => 'anyfeedback','question' => 'Any other feedback','is_show' => '0'],
'16'=>(object) ['name' => 'orderagain','question' => 'Would you order an SC Box again?','is_show' => '0'],
)));

define('DEFAULT_IMAGE_PATH',"'https://i.imgur.com/Qu4zXlp.jpg'");
