<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Brandpa_model extends MY_Model {
	public $ouptut = array();
    public $depth = 0; public $children = 0;
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }	

    function get_all_paid_brands(){
    	$query = $this->db->query("select user_id, company_name from brand_info where is_paid = 1");
    	$res = $query->result_array();
    	return $res;
    }

    function save_brands_data($data,$brand_id){
            if(!empty($data['category_data'])){ 
                $this->db->delete('category_object_relationship', array('object_id' => $brand_id,'object_type'=>'brand'));

                foreach ($data['category_data'] as $value) {
                    $k = array(
                            'object_id' => $brand_id,
                            'object_type' => 'brand',
                            'category_id' => $value,
                            'modified_by' => $this->session->userdata('user_id')
                        );
                    $this->db->insert('category_object_relationship', $k); 
                    
                 } 
            }
             $this->db->delete('pa_object_relationship', array('object_id' => $brand_id,'pa_type'=>'bucket','object_type'=>'brand'));
        if(!empty($data['bucket_data'])){ 
               

                foreach ($data['bucket_data'] as $value) {
                    $k = array(
                            'object_id' => $brand_id,
                            'pa_type' => 'bucket',
                            'object_type' => 'brand',
                            'object_value' => $value,
                            'created_by' => $this->session->userdata('user_id'),
                            'modified_by' => $this->session->userdata('user_id'),
                            'created_datetime' => date('Y-m-d H:i:s')
                        );
                    $this->db->insert('pa_object_relationship', $k); 
                    
                 } 
            }
            $this->db->delete('pa_object_relationship', array('object_id' => $brand_id,'pa_type'=>'age','object_type'=>'brand'));
            if(!empty($data['age_data'])){ 
                

                foreach ($data['age_data'] as $value) {
                    $k = array(
                            'object_id' => $brand_id,
                            'pa_type' => 'age',
                            'object_type' => 'brand',
                            'object_value' => $value,
                            'created_by' => $this->session->userdata('user_id'),
                            'modified_by' => $this->session->userdata('user_id'),
                            'created_datetime' => date('Y-m-d H:i:s')
                        );
                    $this->db->insert('pa_object_relationship', $k); 
                    
                 } 
            }
            $this->db->delete('pa_object_relationship', array('object_id' => $brand_id,'pa_type'=>'budget','object_type'=>'brand'));
            if(!empty($data['budget_data'])){  
                

                foreach ($data['budget_data'] as $value) {
                    $k = array(
                            'object_id' => $brand_id,
                            'pa_type' => 'budget',
                            'object_type' => 'brand',
                            'object_value' => $value,
                            'created_by' => $this->session->userdata('user_id'),
                            'modified_by' => $this->session->userdata('user_id'),
                            'created_datetime' => date('Y-m-d H:i:s')
                        );
                    $this->db->insert('pa_object_relationship', $k); 
                    
                 } 
            }  
    }

    function get_brands_category($brand_id){
        if($brand_id !=''){
            $query = $this->db->get_where('category_object_relationship', array('object_id' => $brand_id,'object_type'=>'brand'));
            $res = $query->result_array(); 
            $objects = array();
            if(!empty($res)){
                foreach($res as $val){

                    $objects[] = $val['category_id'];
                }
            }

            return $objects;
        }
    }

} ?>