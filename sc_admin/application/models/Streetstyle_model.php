<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Streetstyle_model extends MY_Model {
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	public function save_street_look($look_name,$look_desc,$street_canvas_img_id,$prod_look)
	{	
		$look_slug = str_replace(' ','-',strtolower($look_name)).time().mt_rand(0,10000);
		$data = array('name'=>$look_name,'description'=>$look_desc,'look_type'=>2,'image'=>$street_canvas_img_id,'created_datetime'=>date('Y-m-d H:i:s'),'slug'=>$look_slug,'created_by'=> $this->session->userdata('user_id'));
		$this->db->insert('look', $data);
		$ins_id = $this->db->insert_id();
		$data = array();
		 foreach($prod_look as $val){ 
                        $data[] = array('look_id'=>$ins_id,'product_id'=>$val,'deflag'=>0);
                        }
                $this->db->insert_batch('look_products', $data);

		return true;	
	}
	
}

