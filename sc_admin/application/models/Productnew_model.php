<?php
class Productnew_model extends CI_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
   
	/*---------Product Details functionalities-------*/

	public function get_all_product_details($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL){
		
		
		if($search_by == '1'){ $search_cond = " AND a. name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '2'){ $search_cond = " AND b.name like '%".strip_tags(trim($table_search))."%'"; }
			else if($search_by == '3'){ $search_cond = " AND c.name like '%".strip_tags(trim($table_search))."%'"; }
				//else if($search_by == '4'){ $search_cond = " AND a.slug like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '5'){ 

				$tb_status=strip_tags(trim($table_search));
				/*if($tb_status=="Approved")
				{
					$apr_status = 'A';

				}else
				{
					if($tb_status == "Reject")
					{
						$apr_status = 'R';
					}else
					{
						$apr_status = 'P';
					}
					
				}*/

				if($tb_status=="Approved"){ $search_cond = " AND a.approve_reject = 'A'"; }
				else if($tb_status=="Reject") { $search_cond = " AND a.approve_reject = 'R'"; }
				else{ $search_cond = " AND (a.approve_reject = 'P' || a.approve_reject = 'D')"; }

				//$search_cond = " AND a.approve_reject = '".$apr_status."'";
			 }else if($search_by == '6'){ $search_cond = " AND a.id ='".strip_tags(trim($table_search))."'"; }
				else if($search_by == '7'){ $search_cond = " AND d.company_name like '%".strip_tags(trim($table_search))."%'"; }
				else if($search_by == '8'){ $search_cond = " AND g.product_sku like '%".strip_tags(trim($table_search))."%'"; }					
				else{ $search_cond ='AND 1=1'; }
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}

		if($this->session->userdata('role_id') == 6 ){ 
			$search_cond = $search_cond." and a.created_by=".$this->session->userdata('user_id');
		}
		if($search_by == '8'){
			$query = $this->db->query("select distinct a.id, a.product_sku,a.product_cat_id, a. product_sub_cat_id,d.company_name as brand, a.name, a.slug, a.status,a.is_delete, a.created_datetime, a.modified_datetime,a.image, a.approve_reject, b.id as cat_id, b.name AS cat_name, c.id as subcat_id, c.name AS subcat_name FROM `product_desc` AS a, `product_category` AS b, `product_sub_category` AS c,`brand_info` as d,`product_inventory` as g  WHERE 
		 a.`product_cat_id` = b.`id` 
		AND a.`product_sub_cat_id` = c.`id` AND a.`brand_id`=d.`user_id` AND a.`id` >='3598' and g.product_id = a.id
		AND a.`is_delete` = 0 ".$search_cond." order by a.id desc ".$limit_cond);	
		}else{
		$query = $this->db->query("select a.id, a.product_sku, a.product_cat_id, a. product_sub_cat_id,d.company_name as brand, a.name, a.slug, a.status,a.is_delete, a.created_datetime, a.modified_datetime,a.image, a.approve_reject, b.id as cat_id, b.name AS cat_name, c.id as subcat_id, c.name AS subcat_name FROM `product_desc` AS a, `product_category` AS b, `product_sub_category` AS c,`brand_info` as d  WHERE 
		 a.`product_cat_id` = b.`id` 
		AND a.`product_sub_cat_id` = c.`id` AND a.`brand_id`=d.`user_id` AND a.`id` >='3598'
		AND a.`is_delete` = 0 ".$search_cond." order by a.id desc ".$limit_cond);		
		}
		$res = $query->result();
		
		return $res;
		

	}//this function is used to get the product details added
	
	function get_sub_cat($cat_id){
			$query = $this->db->get_where('product_sub_category', array('status' => 1,'is_delete'=>0,'product_cat_id' =>$cat_id));
			$result = $query->result_array();
			return $result;
		}
	
	function get_var_type($var_type){
			$query = $this->db->query('SELECT a.id,a.name as variation_type,b.name as variation,b.id as nid FROM `product_variation_value` as a,`product_variation_type` as b where a.`product_variation_type_id` = b.`id` and b.`prod_sub_cat_id`='.$var_type." and a.status = 1 and b.status = 1 and a.is_delete = 0 and b.is_delete =0 order by b.id desc");
			$result = $query->result_array();
			return $result;
		}
	
	function all_store(){
			$query = $this->db->get_where('brand_store', array('status' => 1));
			$result = $query->result_array();
			return $result;
		}
	
	function load_questAns($id){
		$query = $this->db->query("SELECT A.id, A.answer FROM answers AS A INNER JOIN question_answer_assoc AS Q ON Q.answer_id=A.id AND (Q.question_id = ".$id.") ORDER BY A.id");
		$result = $query->result_array();
		return $result;
	}

	function add_product($data){
		$this->db->insert('product_desc', $data['product_info']); 
		$product_insert_id = $this->db->insert_id(); 

		/* Add Product Tags*/
		if(!empty($data['product_tags_array'])){
			
			foreach($data['product_tags_array'] as $tag){
				$tags = array();
				$tags = array(
						'product_id'=>$product_insert_id,
						'tag_id' => $tag,
						'status' => 1,
						'created_by' =>$this->session->userdata('user_id'),
						'created_datetime' => date('Y-m-d H:i:s')
					);
				$this->db->insert('product_tag', $tags); 

			}
		}
		/* Add Product Store / Product Availibility */
		if(!empty($data['product_avail_array'])){
			
			foreach($data['product_avail_array'] as $store){
				$stores = array();
				$stores = array(
						'product_id'=>$product_insert_id,
						'store_id' => $store,
						'status' => 1
					);
				$this->db->insert('product_store', $stores); 

			}
		}

		/* Add Product Variations type and value */
		$vari_array =$data['vari_array'] ;
		$variation_val_array = $data['variation_val_array'];

		if(!empty($data['variation_val_array'])){
			foreach($data['variation_val_array'] as $val){
				foreach($val as $key=>$vall){
					$valtype = array();
					$valtype = array(
						'product_id'=>$product_insert_id,
						'product_var_type_id' => $key,
						'product_var_val_id' => $vall,
						'deflag' => 0
					);
					$this->db->insert('product_variations', $valtype); 
				}
			}
			
		}
		/* Product extra images */
		if(!empty($data['extra_images'])){
			foreach ($data['extra_images'] as $key => $value) {
				$extr_img = array(
						'product_id'=>$product_insert_id,
						'product_images' => $value,
						'created_datetime' => date('Y-m-d H:i:s')
					);
				$this->db->insert('extra_images', $extr_img); 
			}
		}
		
		/* Product Size */
		if(!empty($data['qty_size_info'])){
			foreach($data['qty_size_info'] as $val){
				if($val['size']!='' && $val['qty']!=''){
					$size_id = $this->get_size_id($val['size']);

					if($size_id>0 && $product_insert_id>0){
	                    $data = array(
	                        'product_id'=>$product_insert_id,
	                        'size_id'=>$size_id,
	                        'stock_count'=>$val['qty'],
	                        'product_sku' => $val['product_sku']
	                    );
	                    $this->db->insert('product_inventory', $data); 
                	}

				}
			}
		}

		return true;

	}

	function get_product_info($product_id){
		if($product_id !=''){
			$query = $this->db->get_where('product_desc', array('id' => $product_id));
			$res = $query->result_array();
			return $res;
		}
	}

	function get_product_tags($product_id){
		if($product_id !=''){
			$query = $this->db->get_where('product_tag', array('product_id' => $product_id));
			$res = $query->result_array(); 
			$tags_id = array();
			if(!empty($res)){
				foreach($res as $val){

					$tags_id[] = $val['tag_id'];
				}
			}

			return $tags_id;
		}

	}

	function get_product_avalibility($product_id){
		if($product_id !=''){
			$query = $this->db->get_where('product_store', array('product_id' => $product_id));
			$res = $query->result_array(); 
			$store_ids = array();
			if(!empty($res)){
				foreach($res as $val){

					$store_ids[] = $val['store_id'];
				}
			}

			return $store_ids;
		}

	}

	function get_product_variations($product_id){
		if($product_id !=''){
			$query = $this->db->get_where('product_variations', array('product_id' => $product_id));
			$res = $query->result_array(); 
			$varivalue = array();
			if(!empty($res)){
				foreach($res as $val){

					$varivalue[] = $val['product_var_val_id'];
				}
			}

			return $varivalue;
		}

	}
	function get_extra_images($product_id){
		if($product_id !=''){
			$query = $this->db->get_where('extra_images', array('product_id' => $product_id));
			$res = $query->result_array(); 
			
			return $res;
		}

	}

	function edit_product($data,$product_id){
		
		$this->db->where('id', $product_id);
		$this->db->update('product_desc', $data['product_info']); 

		/* Add Product Tags*/
		if(!empty($data['product_tags_array'])){
			
			$this->db->where('product_id', $product_id);
			$this->db->delete('product_tag'); 
			
			foreach($data['product_tags_array'] as $tag){
				$tags = array();
				$tags = array(
						'product_id'=>$product_id,
						'tag_id' => $tag,
						'status' => 1,
						'created_by' =>$this->session->userdata('user_id'),
						'created_datetime' => date('Y-m-d H:i:s')
					);
				$this->db->insert('product_tag', $tags); 

			}
		}
		/* Add Product Store / Product Availibility */
		if(!empty($data['product_avail_array'])){

			$this->db->where('product_id', $product_id);
			$this->db->delete('product_store'); 

			foreach($data['product_avail_array'] as $store){
				$stores = array();
				$stores = array(
						'product_id'=>$product_id,
						'store_id' => $store,
						'status' => 1
					);
				$this->db->insert('product_store', $stores); 

			}
		}

		/* Add Product Variations type and value */
		$vari_array =$data['vari_array'] ;
		$variation_val_array = $data['variation_val_array'];

		if(!empty($data['variation_val_array'])){
			
			$this->db->where('product_id', $product_id);
			$this->db->delete('product_variations'); 

			foreach($data['variation_val_array'] as $val){
				foreach($val as $key=>$vall){
					$valtype = array();
					$valtype = array(
						'product_id'=>$product_id,
						'product_var_type_id' => $key,
						'product_var_val_id' => $vall,
						'deflag' => 0
					);
					$this->db->insert('product_variations', $valtype); 
				}
			}
			
		}

		/* Product extra images */
		if(!empty($data['extra_images'])){
			foreach ($data['extra_images'] as $key => $value) {
				$extr_img = array(
						'product_id'=>$product_id,
						'product_images' => $value,
						'created_datetime' => date('Y-m-d H:i:s')
					);
				$this->db->insert('extra_images', $extr_img); 
			}
		}

		/* Product Size */
		$this->db->delete('product_inventory', array('product_id' => $product_id)); 
		if(!empty($data['qty_size_info'])){

			foreach($data['qty_size_info'] as $val){
				if($val['size']!='' && $val['qty']!=''){
					$size_id = $this->get_size_id($val['size']);

					if($size_id>0 && $product_id>0){
	                    $data = array(
	                        'product_id'=>$product_id,
	                        'size_id'=>$size_id,
	                        'stock_count'=>$val['qty'],
	                        'product_sku' => $val['product_sku']
	                    );
	                    $this->db->insert('product_inventory', $data); 
                	}

				}
			}
		}


		return true;

	}

	function product_approve($product_id){
		if($product_id){
			$this->db->where('id', $product_id);
			if($this->db->update('product_desc', array('approve_reject'=>'A','modified_datetime'=>date('Y-m-d H:i:s'),'modified_by'=>$this->session->userdata('user_id')))){
				return true;
			}else{
				return false;
			}

		}
	}

	function product_reject($product_id,$reason){
		if($product_id!='' && $reason!=''){
			$this->db->where('id', $product_id);
			if($this->db->update('product_desc', array('approve_reject'=>'R','reason'=>$reason,'modified_datetime'=>date('Y-m-d H:i:s'),'modified_by'=>$this->session->userdata('user_id')))){
				return true;
			}else{
				return false;
			}

		}

	}

	function product_delete($product_id){
		if($product_id){
			$this->db->where('id', $product_id);
			if($this->db->update('product_desc', array('is_delete'=>1,'modified_datetime'=>date('Y-m-d H:i:s'),'modified_by'=>$this->session->userdata('user_id')))){
				return true;
			}else{
				return false;
			}
		}
	}

	function upload_product_image($product_img,$product_id){
		if($product_img!='' && $product_id!=''){
			$this->db->where('id', $product_id);
			$this->db->update('product_desc', array('image'=>$product_img,'modified_datetime'=>date('Y-m-d H:i:s'),'modified_by'=>$this->session->userdata('user_id')));
		}
	}

	function product_image_extra($product_id){
		if($product_id>0){
			$query = $this->db->get_where('extra_images', array('product_id' => $product_id));
			return $query->result_array();
		}
	}

	function delete_extra_img($image_id){
		if($image_id>0){
			$query = $this->db->get_where('extra_images', array('id' => $image_id));
			$res = $query->result_array();

			if(!empty($res)){
				$file = $res[0]['product_images'];
				unlink('./assets/products_extra/thumb/'.$file);
				unlink('./assets/products_extra/'.$file);
				$this->db->delete('extra_images', array('id' => $image_id)); 
			}

		}
	}

	function get_size_id($size){
        if($size!=''){
            $query = $this->db->query("select count(*) as cnt,id from product_size where size_text = '".strtoupper($size)."'");
            $result = $query->result_array();
            if($result[0]['cnt'] == 0){
                $data = array(
                    'size_text' => strtoupper($size) ,
                );
                $this->db->insert('product_size', $data); 
                return $this->db->insert_id();
            }else {
                return $result[0]['id']; 
            }
        }
    }

    function product_inventory($product_id){
    	$query = $query = $this->db->query("select a.stock_count,c.`size_text`,a.product_sku from product_inventory as a,product_desc as b,product_size as c where a.`product_id` = b.`id` and a.`size_id` = c.`id` and a.`product_id`=".$product_id);
		$res = $query->result_array();
		return $res;
    }

    function getbrand_code($brand_id){
        if($brand_id > 0){
            $query = $this->db->query("select brand_code from `brand_info` where user_id='".$brand_id."'");
            $result = $query->result_array();
            if(!empty($result)){
                return $result[0]['brand_code'];
            }else{
                return '';
            }
        }else{
            return '';
        }
    }
	
	function get_product_sku($product_id){
			$query = $this->db->query("select pi.product_sku from product_inventory as pi where pi.product_id = $product_id");
			$result = $query->result_array();
			$count = count($result);
			$i = 1;
			$product_sku = '';
			foreach($result as $row){
				if($count == 1){ $product_sku = $row['product_sku']; 
				}else if($i == $count)
				{ $product_sku = $product_sku.' '.$row['product_sku']; }
				else if($i<$count)
				{ $product_sku = $row['product_sku'].' , '.$product_sku; }
				$i++;
			}
			return $product_sku;
	}
	
 } 
?>
