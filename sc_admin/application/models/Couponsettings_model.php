<?php
class Couponsettings_model extends MY_Model {    
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    function get_paid_stores()
    {
        if($this->session->userdata('role_id') == 6){
            $brand_id = $this->session->userdata('user_id');
            $query = $this->db->query("SELECT user_id as brand_id, company_name FROM brand_info  WHERE is_paid = 1 and user_id=".$brand_id." ORDER BY id DESC");
        }else{
            $query = $this->db->query("SELECT user_id as brand_id, company_name FROM brand_info  WHERE is_paid = 1 ORDER BY id DESC");
        }
        $result = $query->result_array();
        return $result;
    }

    function get_store_products($brand_id)
    {
      if($brand_id!='' || $brand_id!=0)
      {
        $query = $this->db->query("SELECT id, name FROM product_desc  WHERE  brand_id=".$brand_id." ORDER BY id DESC ");  
      }else
      {
        return 0;
      }
       
       $result = $query->result_array();
       return $result;
    }

    function saveUpdate_coupon($data,$coupon_id=NULL)
    {     
      if($coupon_id!=NULL)
      {       
        $this->db->where('id',$coupon_id);
        $result = $this->db->update('coupon_settings', $data['coupon_info']);         
          if($result == TRUE)
          {
            return $coupon_id;
          }else
          {
            return FALSE;
          }

      }else
      {
        if(!empty($data))
        {
          /* if(isset($data['brand']) && $data['brand']!='')
           {
              $query = $this->db->query("SELECT * FROM coupon_settings  WHERE  id=".$coupon_id." AND status=1 AND is_delete=0 ORDER BY id DESC ");  
           }*/            
            $result = $this->db->insert('coupon_settings', $data['coupon_info']);
            $coupon_id = $this->db->insert_id();
            //echo $this->db->last_query();exit;
            if($result == TRUE)
            {
              return $coupon_id;
            }else
            {
              return FALSE;
            }
        }  
      }   
     
    }

     function get_coupon($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL,$coupon_id=NULL,$date_from=NULL,$date_to=NULL){

       if($search_by == '1'){
        $search_cond = " AND coupon_code like '%".strip_tags(trim($table_search))."%'"; 
        }elseif($search_by == '2'){
        /*$search_cond = " AND coupon_email_applied like '%".strip_tags(trim($table_search))."%'  or coupon_start_datetime like '%".strip_tags(trim($date_from))."%'or coupon_end_datetime like '%".strip_tags(trim($date_to))."%'";*/
        $search_cond = " AND coupon_email_applied like '%".strip_tags(trim($table_search))."%'";
        }
        elseif($search_by == '3'){
         /*echo $date_from .$date_to;*/
        /*$search_cond = " AND coupon_email_applied like '%".strip_tags(trim($table_search))."%'  or coupon_start_datetime like '%".strip_tags(trim($date_from))."%'or coupon_end_datetime like '%".strip_tags(trim($date_to))."%'";*/
          $search_cond = " AND coupon_start_datetime like '%".strip_tags(trim($date_from))."%'or coupon_end_datetime like '%".strip_tags(trim($date_to))."%'";
        }

      /*else if($search_by == '2'){ $search_cond = " AND slug like '%".strip_tags(trim($table_search))."%'"; }
      else if($search_by == '3'){ 

          $tb_status=strip_tags(trim($table_search));
          if($tb_status=="Yes")
          {
            $status=1;

          }else
          {
            $status=0;
          }

        $search_cond = " AND status =".$status." "; }*/
        else{ $search_cond ='AND 1=1'; }
         
        if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                       $page = $page*$per_page-$per_page;
                    }else{ $page = 0;}
            $limit_cond = "LIMIT ".$page.",".$per_page;
        }else{
          $limit_cond = ''; 
        }


      if($coupon_id!='' || $coupon_id!=0)
      {
        /*$query = $this->db->query("SELECT a.*,b.`company_name` FROM coupon_settings as a, brand_info as b  WHERE  a.`brand_id`= b.`user_id` AND a.`id`=".$coupon_id." AND a.`status`=1 AND a.`is_delete`=0 ORDER BY a.`id` DESC ");  */
        $query = $this->db->query("SELECT a.*,b.`company_name` FROM coupon_settings as a left join brand_info as b  on  a.`brand_id`= b.`user_id` WHERE a.`id`=".$coupon_id." AND a.`status`=1 AND a.`is_delete`=0 ORDER BY a.`id` DESC ");  
      }else
      {
       /* $query = $this->db->query("SELECT a.*,b.`company_name` FROM coupon_settings as a, brand_info as b  WHERE  a.`brand_id`= b.`user_id`  AND a.`status`=1 AND a.`is_delete`=0 ".$search_cond." ORDER BY a.`id` DESC ".$limit_cond); */
        
        $query = $this->db->query("SELECT a.*,b.`company_name` FROM coupon_settings as a left join brand_info as b  on  a.`brand_id`= b.`user_id` WHERE a.`status`=1 AND a.`is_delete`=0 ".$search_cond." ORDER BY a.`id` DESC ".$limit_cond);
      }
       
       /*echo $this->db->last_query();*/
       $result = $query->result();
       return $result;
    }


    function download_coupon_sheet($search_by=NULL,$date_from=NULL,$date_to=NULL,$table_search=NULL)
    { 
      if($search_by == '1'){ 
        /*$search_cond = " AND coupon_code like '%".strip_tags(trim($table_search))."%' or coupon_start_datetime like '%".strip_tags(trim($date_from))."%'or coupon_end_datetime like '%".strip_tags(trim($date_to))."%'"; */
        $search_cond = " AND coupon_code like '%".strip_tags(trim($table_search))."%'";
        //echo $search_by.'+++++++++++++++'.$date_from.'+++++++++++++++'.$date_to .'==========='.$table_search;
        //exit;
        //$search_cond = " AND coupon_code like '%".strip_tags(trim($table_search))."%' or coupon_email_applied like '%".strip_tags(trim($table_search))."%'"; 
      }elseif($search_by == '2'){
        /*$search_cond = " AND coupon_email_applied like '%".strip_tags(trim($table_search))."%'  or coupon_start_datetime like '%".strip_tags(trim($date_from))."%'or coupon_end_datetime like '%".strip_tags(trim($date_to))."%'";*/
        $search_cond = " AND coupon_email_applied like '%".strip_tags(trim(@$table_search))."%'";
        }
        elseif($search_by == '3'){
         /*echo $date_from .$date_to;*/
        /*$search_cond = " AND coupon_email_applied like '%".strip_tags(trim($table_search))."%'  or coupon_start_datetime like '%".strip_tags(trim($date_from))."%'or coupon_end_datetime like '%".strip_tags(trim($date_to))."%'";*/
          $search_cond = " AND coupon_start_datetime like '%".strip_tags(trim($date_from))."%'or coupon_end_datetime like '%".strip_tags(trim($date_to))."%'";          
        }     
      /*if($search_by == '3' || $date_from != 0 || $date_to != 0){ 

        //$search_cond = " AND coupon_code like '%".strip_tags(trim($table_search))."%' or coupon_start_datetime like '%".$date_from."%'or coupon_end_datetime like '%".$date_to."%'"; 
        //$search_cond = " AND coupon_code like '%".strip_tags(trim($table_search))."%' or coupon_email_applied like '%".strip_tags(trim($table_search))."%'"; 
        $search_cond = " AND coupon_start_datetime like '%".strip_tags(trim($date_from))."%'or coupon_end_datetime like '%".strip_tags(trim($date_to))."%'";
      }elseif($search_by == '3' || $date_from != 0 || $date_to != 0){
        //$search_cond = " AND coupon_email_applied like '%".strip_tags(trim($table_search))."%'  or coupon_start_datetime like '%".$date_from."%'or coupon_end_datetime like '%".$date_to."%'";
        $search_cond = " AND coupon_start_datetime like '%".strip_tags(trim($date_from))."%'or coupon_end_datetime like '%".strip_tags(trim($date_to))."%'";
      }*/

      /*else if($search_by == '2'){ $search_cond = " AND slug like '%".strip_tags(trim($table_search))."%'"; }
      else if($search_by == '3'){ 

          $tb_status=strip_tags(trim($table_search));
          if($tb_status=="Yes")
          {
            $status=1;

          }else
          {
            $status=0;
          }

        $search_cond = " AND status =".$status." "; }*/
        else{ $search_cond ='AND 1=1'; }
         //echo $search_cond;exit;
        /*if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                       $page = $page*$per_page-$per_page;
                    }else{ $page = 0;}
            $limit_cond = "LIMIT ".$page.",".$per_page;
        }else{
          $limit_cond = ''; 
        }*/
      $coupon_id=0;
      if($coupon_id!='' || $coupon_id!=0){

        /*$query = $this->db->query("SELECT a.*,b.`company_name` FROM coupon_settings as a, brand_info as b  WHERE  a.`brand_id`= b.`user_id` AND a.`id`=".$coupon_id." AND a.`status`=1 AND a.`is_delete`=0 ORDER BY a.`id` DESC ");  */
      $query = $this->db->query("SELECT a.*,b.`company_name` FROM coupon_settings as a left join brand_info as b  on  a.`brand_id`= b.`user_id` WHERE a.`id`=".$coupon_id." AND a.`status`=1 AND a.`is_delete`=0 ORDER BY a.`id` DESC ");  
      }else{
        /*echo 'asdfasdf'.$search_cond; 
        exit;*/
       /* $query = $this->db->query("SELECT a.*,b.`company_name` FROM coupon_settings as a, brand_info as b  WHERE  a.`brand_id`= b.`user_id`  AND a.`status`=1 AND a.`is_delete`=0 ".$search_cond." ORDER BY a.`id` DESC ".$limit_cond); */       
        $query = $this->db->query("SELECT a.*,b.`company_name` FROM coupon_settings as a left join brand_info as b  on  a.`brand_id`= b.`user_id` WHERE a.`status`=1 AND a.`is_delete`=0 ".$search_cond." ORDER BY a.`id` DESC ");
      }
       $result = $query->result_array();
       /*echo "<pre>";
       print_r($result);
       exit;*/
       return $result;
    }


    public function delete_coupon($couponID)
    {    
        $this->db->where('id', $couponID);
        $deleted = $this->db->update('coupon_settings' ,array('is_delete'=>'1'));
        
        if($deleted){          
          return TRUE;          
        }
        else
        {          
          return false; 
        }
    }



    function Coupon_uploadqueesheet($data_post){          
          $i = 0;      
          $k = 0;
          $duplicate_id = array();
              if(!empty($data_post)){                
                  foreach($data_post as $val){
                    $coupon_code_check = $val['coupon_code'];
                    $query = $this->db->query('SELECT count(coupon_code) as count_coupon FROM coupon_settings WHERE coupon_code = "'.$coupon_code_check.'"');                    
                    $result = $query->result_array();
                    $duplicate_coupon = $result[0]['count_coupon'];

                    $query1 = $this->db->query('SELECT coupon_code as count_id FROM coupon_settings WHERE coupon_code = "'.$coupon_code_check.'"');                    
                    $result1 = $query1->result_array();                    
                    @$duplicate_id[] = $result1[0]['count_id'];

                    if($duplicate_coupon == 1){
                       $k++;
                    }
                  if($duplicate_coupon != 1){  
                        $i++;
                        $data = array(
                                'coupon_code' => $val['coupon_code'],
                                'coupon_desc' => $val['coupon_description'],
                                'discount_type' => $val['discount_type'],
                                'coupon_amount' => $val['coupon_amount'],
                                /*'allow_free_ship'=>$val['allow_free_ship'],*/
                                'coupon_start_datetime' => $val['start_date'],
                                'coupon_end_datetime' => $val['end_date'],
                                'min_spend' => $val['minimum_spend'],
                                'max_spend' => $val['maximum_spend'],
                                'individual_use_only' => $val['individual_use_only'],
                                'exclude_sale_items' => $val['exclude_sale_items'],
                                'usage_limit_per_coupon' => $val['usage_limit_per_coupon'],
                                'usage_limit_to_user' => $val['usage_limit_to_user'],
                                'coupon_email_applied' => $val['email_Id_applicable'],
                                'stylecrackerDiscount' => $val['stylecracker_discount'],
                                'status' => 1,
                                'showEmailPopUp' => $val['show_email_popup'],
                                'created_datetime'=>date('Y-m-d H:i:s'),            
                                'created_by'=>$this->session->userdata('user_id'),
                                'modified_by' => $this->session->userdata('user_id')
                              );                                                        
                              $result = $this->db->insert('coupon_settings', $data);                
                        }
                    }
                }
                //exit;
                $var_array =array('duplicate' => $k, 'successfully' => $i, 'duplicate_id' => $duplicate_id);                
                 if($duplicate_coupon == 1 || $result == 1){
                    return $var_array;
                  }else{
                    return FALSE;
                 }                        
              }

 } 
?>
