<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Api_details_model extends MY_Model {

	

	function __construct()

    {

        // Call the Model constructor

        parent::__construct();

    }



    function get_api_details(){

    	$user_id = $this->session->userdata('user_id');

    	if($user_id!=''){

    		$query = $this->db->get_where('user_login', array('id' =>$user_id));

    		$res = $query->result_array();

    		return $res;

    	}

    }



    function authorized_api_user($api_key,$secret_key){

    	if($api_key!='' && $secret_key!=''){ 

    		$query = $this->db->get_where('user_login', array('email' =>trim($api_key),'password'=>trim($secret_key),'status' => 1));

    		$res = $query->result_array();

    		return 0;

    		

    	}else{

    		return 1;

    	}

    }



    function get_cat_id($cat_name){

    	if($cat_name){

    		$query = $this->db->query("select id from product_category where lower(name) =lower('".$cat_name."')");

    		$res = $query->result_array();

    		if(!empty($res)){ return $res[0]['id']; }else{ return false; }

    	}

    }



    function get_subcat_id($cat_id, $sub_cat_name){

    	if($sub_cat_name){

    		$query = $this->db->query("select id from product_sub_category where product_cat_id = ".$cat_id." and lower(name) =lower('".$sub_cat_name."')");

    		$res = $query->result_array();

    		

    		if(!empty($res)){ return $res[0]['id']; }else{ return false; }

    	}

    }

    function get_price_range($price_range,$id){

    		$query = $this->db->query("SELECT A.id, A.answer FROM answers AS A INNER JOIN question_answer_assoc AS Q ON Q.answer_id=A.id AND (Q.question_id = ".$id.") and upper(A.answer) = upper('".$price_range."') ORDER BY A.answer");

			$res = $query->result_array(); 

			//echo $this->db->last_query();

			if(!empty($res)){ return $res[0]['id']; }else{ return false; }

    	}



    function get_store_id($store_name){

        $store_id = array();

        if(!empty($store_name)){

        foreach($store_name as $val){

            $query = $this->db->query("select id from brand_store where lower(brand_store_name) =lower('".$val."')");

            $res = $query->result_array();

           // echo $this->db->last_query();

            if(!empty($res)){

                $store_id[] = $res[0]['id'];

            }

        }

        }



        return $store_id;



    }



    function get_brand_id($brand_name,$api_key,$secret_key){

        if($brand_name == ''){ 

            return $this->get_brand_info($api_key,$secret_key); 

        }else{

            $query = $this->db->query("select id from user_login where lower(user_name) =lower('".$brand_name."')");

            $res = $query->result_array();

            if(!empty($res)){

                return $res[0]['id'];

            }else{

                return $this->get_brand_info($api_key,$secret_key); 

            }

        }



    }



    function get_brand_info($api_key,$secret_key){

        $query = $this->db->get_where('user_login', array('email' =>trim($api_key),'password'=>trim($secret_key),'status' => 1));

        $res = $query->result_array();

        if(!empty($res)){

        $brand_id = $res[0]['id'];

            return $brand_id;

        }else{

            return false;

        } 

    }

    

    function get_target_market_id($target_market){

        $query = $this->db->query("select id from target_market where lower(target_market) =lower('".$target_market."')");

        $res = $query->result_array();

        if(!empty($res)){ return $res[0]['id']; }else{ return 0; }

    }

    

	function add_products($data,$login_id){

        $this->db->insert('product_desc', $data['product_info']); 

        $product_insert_id = $this->db->insert_id(); 



        /* Add Product Tags*/

        if(!empty($data['product_tags_array'])){

            

            foreach($data['product_tags_array'] as $tag){

                $tags = array(

                        'product_id'=>$product_insert_id,

                        'tag_id' => $tag,

                        'status' => 1,

                        'created_by' =>$login_id,

                        'created_datetime' => date('Y-m-d H:i:s')

                    );

                $this->db->insert('product_tag', $tags); 



            }

        }

        /* Add Product Store / Product Availibility */

       if(!empty($data['stores'])){

            

            foreach($data['stores'] as $store){

                $stores = array();

                $stores = array(

                        'product_id'=>$product_insert_id,

                        'store_id' => $store,

                        'status' => 1

                    );

                $this->db->insert('product_store', $stores); 



            }

        }



        /* Add Product Variations type and value */

       // $vari_array =$data['vari_array'] ;

       // $variation_val_array = $data['variation_val_array'];

        

        if(!empty($data['variation_val_array']['variation_type'])){

            foreach($data['variation_val_array']['variation_type'] as $key1=>$val){  

                foreach($val as $key=>$vall){

                    $valtype = array();

                    $valtype = array(

                        'product_id'=>$product_insert_id,

                        'product_var_type_id' => $key1,

                        'product_var_val_id' => $vall,

                        'deflag' => 0

                    );

                    $this->db->insert('product_variations', $valtype); 

                    //echo $this->db->last_query();

                }

            }

            

        }



         if(!empty($data['variation_val_array']['variation_type'])){

            foreach($data['variation_val_array']['variation_type'] as $key1=>$val){  



            }  

         }

        return true;



    }



    function get_tags_id($tags_name){

        $tag_id = array();

        foreach($tags_name as $val){

            $query = $this->db->query("select id from tag where lower(name) =lower('".$val."')");

            $res = $query->result_array();

           // echo $this->db->last_query();

            if(!empty($res)){

                $tag_id[] = $res[0]['id'];

            }



        }

        return $tag_id;

    }



    function get_variations_id($variation_values){

        $variation_type = array();

        $variation_values1 = array();

        $data = array();

        foreach($variation_values as $val){

            $query = $this->db->query("select id,product_variation_type_id from product_variation_value where lower(name) =lower('".$val."')");

            $res = $query->result_array();

           // echo $this->db->last_query();

            if(!empty($res)){

                $variation_type[$res[0]['product_variation_type_id']][] = $res[0]['id'];

                

            }



        }

       

        $data['variation_type'] =   $variation_type;

        return $data;

    }



    function unique_product_name($product_name){

        $query = $this->db->query("select id from product_desc where name ='".$product_name."'");

        $res = $query->result_array();

        if(!empty($res)){

            return true;

        }else{

            return false;

        }

    }



    function product_cpa_track($product_id,$price){

        if($product_id>=0){

            $data = array('product_id'=>$product_id,'sc_product_price'=>$price);

            if($this->db->insert('product_cpa_track', $data)){ return true; }else{ return false; }

        }

    }

}