<?php
class Stylist_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_allusers(){ 
    	$limit_cond = '';		
		
		$query = $this->db->query("select a.id,a.user_name, a.bucket, a.created_datetime, a.email ,c.`first_name`,c.`last_name`,c.`registered_from`,c.`gender`,c.`contact_no`, d.`role_name` ,z.last_visit_time,c.registered_from from user_login as a,user_info as c, user_role_mapping as b ,role_master as d ,
		user_visit_history as z WHERE a.`status` =1 and a.`test_user`!=1 and  
		 a.`id` = b.`user_id` and b.`role_id`=2 and c.`user_id` = b.`user_id` and z.`id`=a.`id` and b.`role_id` = d.`id`   
		 ".$search_cond." order by a.id desc ".$limit_cond);
		$res = $query->result();
	//echo $this->db->last_query();
		return $res;
		echo $res;
		
	}

	function getUserCart($userid)
	{  
		if($userid!='')
		{
			$query = $this->db->query('SELECT a.*,(a.`product_qty`*b.`price`) as price,CASE WHEN DATE_FORMAT(b.`discount_start_date`,"%Y-%m-%d  %h:%i") <= DATE_FORMAT(now(),"%Y-%m-%d %h:%i") and DATE_FORMAT(b.`discount_end_date`,"%Y-%m-%d  %h:%i") >= DATE_FORMAT(now(),"%Y-%m-%d %h:%i") then (a.`product_qty`*b.`compare_price`) ELSE 0 END AS compare_price,b.`name` as product_name,b.`brand_id`,(a.`product_qty`*b.`price`) as price,b.`price` as product_price,b.`image` as product_image,c.`first_name`,c.`last_name`,c.`gender`,c.`registered_from`,c.`contact_no`,d.`company_name`,e.`email`,f.`id` as size_id,f.`size_text`,d.`cod_available`,d.`shipping_exc_location`,d.`cod_exc_location`,b.`slug`,d.`return_policy`,d.`company_name`,d.`user_id`,d.`min_del_days`,d.`max_del_days`,d.`store_tax`,d.`is_shipping`,d.`shipping_charges`,d.`shipping_min_values`,d.`shipping_max_values`,d.`is_cod`,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,b.`id` as pid,e.`email`,a.`created_datetime`,DATEDIFF(now(),a.`created_datetime`) as datediff  FROM `cart_info` as a, `product_desc` as b,`user_info` as c,`brand_info` as d, `user_login` as e,`product_size` as f WHERE a.`product_id`=b.`id` and a.`user_id`=c.`user_id` and b.`brand_id`=d.`user_id` and e.`id`=c.`user_id` and
				a.`user_id`=e.`id` and a.`product_size`=f.`id` and a.`order_id` is NULL  and ( a.`user_id`="'.$userid.'") order by b.`brand_id` ');	 

           /* $query = $this->db->query('SELECT a.*,(a.`product_qty`*b.`price`) as price,CASE WHEN DATE_FORMAT(b.`discount_start_date`,"%Y-%m-%d  %h:%i") <= DATE_FORMAT(now(),"%Y-%m-%d %h:%i") and DATE_FORMAT(b.`discount_end_date`,"%Y-%m-%d  %h:%i") >= DATE_FORMAT(now(),"%Y-%m-%d %h:%i") then (a.`product_qty`*b.`compare_price`) ELSE 0 END AS compare_price,b.`name` as product_name,b.`brand_id`,(a.`product_qty`*b.`price`) as price,b.`price` as product_price,b.`image` as product_image,c.`first_name`,c.`last_name`,c.`gender`,c.`registered_from`,c.`contact_no`,d.`company_name`,e.`email`,f.`id` as size_id,f.`attr_value_name` as size_text,d.`cod_available`,d.`shipping_exc_location`,d.`cod_exc_location`,b.`slug`,d.`return_policy`,d.`company_name`,d.`user_id`,d.`min_del_days`,d.`max_del_days`,d.`store_tax`,d.`is_shipping`,d.`shipping_charges`,d.`shipping_min_values`,d.`shipping_max_values`,d.`is_cod`,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,b.`id` as pid,e.`email`,a.`created_datetime`,DATEDIFF(now(),a.`created_datetime`) as datediff  FROM `cart_info` as a, `product_desc` as b,`user_info` as c,`brand_info` as d, `user_login` as e,`attribute_values_relationship` as f WHERE a.`product_id`=b.`id` and a.`user_id`=c.`user_id` and b.`brand_id`=d.`user_id` and e.`id`=c.`user_id` and
            a.`user_id`=e.`id` and a.`product_size`=f.`id` and a.`order_id` is NULL  and ( a.`user_id`="'.$userid.'") order by b.`brand_id` ');*/
                         
			$result = $query->result_array();
			return $result;
		}else
		{
			return 0;
		}		

	}

    function getUserCartData($userid)
    {
        if($userid!='')
        {
            $res = $this->getUserCart($userid);
           
            $product_data = array();
             if(!empty($res)){ $i = 0;
                foreach ($res as $key => $value) {
                    $product_data[$i]['id'] = $value['id'];
                    $product_data[$i]['product_id'] = $value['product_id'];
                    $product_data[$i]['cod_available'] = $value['cod_available'];
                    //$product_data[$i]['shipping_exc_location'] = $value['shipping_exc_location'];
                    //$product_data[$i]['cod_exc_location'] = $value['cod_exc_location'];
                    $product_data[$i]['slug'] = $value['slug'];
                    $product_data[$i]['product_qty'] = $value['product_qty'];
                    $product_data[$i]['return_policy'] = $value['return_policy'];
                    $product_data[$i]['product_name'] =$value['product_name'];
                    $product_data[$i]['mrpprice'] = $value['compare_price'] > $value['product_price'] ?  $value['compare_price'] : $value['product_price'];
                    $product_data[$i]['price'] =$value['price'];
                    $product_data[$i]['product_price'] =$value['product_price'];
                    $product_data[$i]['product_image'] =$value['product_image'];
                    $product_data[$i]['size_text'] =$value['size_text'];
                    $product_data[$i]['company_name'] =$value['company_name'];
                    $product_data[$i]['user_id'] =$value['user_id'];
                    $product_data[$i]['min_del_days'] =$value['min_del_days'];
                    $product_data[$i]['max_del_days'] =$value['max_del_days'];
                    $product_data[$i]['store_tax'] =$value['store_tax'];
                    $product_data[$i]['is_shipping'] =$value['is_shipping'];
                    $product_data[$i]['shipping_charges'] =$value['shipping_charges'];
                    $product_data[$i]['shipping_min_values'] =$value['shipping_min_values'];
                    $product_data[$i]['shipping_max_values'] =$value['shipping_max_values'];
                    $product_data[$i]['is_cod'] =$value['is_cod'];
                    $product_data[$i]['cod_charges'] =$value['cod_charges'];
                    $product_data[$i]['cod_min_value'] =$value['cod_min_value'];
                    $product_data[$i]['code_max_value'] =$value['code_max_value'];
                    $product_data[$i]['product_size'] =$value['product_size'];
                    $product_data[$i]['compare_price']= $value['compare_price'];

                    $discount_percent = $this->get_discount_percentage($value['user_id'],$value['pid']);
                    $product_data[$i]['discount_price'] =round($value['product_qty']*($value['product_price']-($value['product_price']*$discount_percent)/100));
                    $product_data[$i]['discount_percent'] = $this->calculate_discount_percent($product_data[$i]['mrpprice'],$product_data[$i]['discount_price'],$value['price']);

                    $product_data[$i]['discount_type'] =  $this->get_discount_id($value['user_id'],$value['pid'])['discount_type_id'];

                    if($product_data[$i]['discount_type'] == 1){
                     $product_data[$i]['discount_id'] =  $this->get_discount_id($value['user_id'],0)['id'];
                    }else if($product_data[$i]['discount_type'] == 2){
                        $product_data[$i]['discount_id'] =  $this->get_discount_id(0,$value['pid'])['id'];
                    }else{
                         $product_data[$i]['discount_id'] =0;
                         $product_data[$i]['discount_type'] =0;
                    }
                    $product_data[$i]['discount_max_amount'] = $this->get_discount_maxvalue($value['user_id'],$value['pid']);
                    $product_data[$i]['product_all_size'] = $this->get_size($value['product_id']);                    
                    $product_data[$i]['brand_unique_name'] =@$this->get_user_info($value['user_id'])[0]['user_name'];
                    $product_data[$i]['in_stock'] = $this->get_product_availablility($value['product_id'],$value['size_id']);
                    $product_data[$i]['brand_id'] =$value['brand_id'];
                    $product_data[$i]['useremail'] =$value['email'];
                    $product_data[$i]['dateadded'] =$value['created_datetime'];
                    $product_data[$i]['datediff'] =$value['datediff'];
                    /*$product_data[$i]['coupon_amount'] = $this->getCouponData()*/

                    $i++;
                }
               /* $product_data['coupon_amount'] = $this->getCouponData($coupon_code)['coupon_amount'];*/
               /*echo "<pre>";print_r($product_data);*/
            }
        return $product_data;
        }
    }

	function getUserCartByStylist($userid,$stylistid)
	{
		if($userid!='' && $stylistid!='')
		{
			$query = $this->db->query('SELECT a.*,b.`name` as product_name,b.`brand_id`,b.`price` as product_price,b.`image` as product_image,c.`first_name`,c.`last_name`,c.`gender`,c.`registered_from`,c.`contact_no`,d.`company_name`,e.`email`,f.`size_text`  FROM `cart_info` as a, `product_desc` as b,`user_info` as c,`brand_info` as d, `user_login` as e,`product_size` as f WHERE a.`product_id`=b.`id` and a.`user_id`=c.`user_id` and b.`brand_id`=d.`user_id` and 
				a.`user_id`=e.`id` and a.`product_size`=f.`id` and a.`order_id` is NULL  and a.`user_id`="'.$userid.'" and 
				a.`created_by`="'.$stylistid.'" order by a.`user_id` ');
				/*echo $this->db->last_query();exit;	*/		
			$result = $query->result_array();
			return $result;
		}else
		{
			return 0;
		}		

	}

	function scremovecart($cartid){
        if($cartid > 0){
            $this->db->delete('cart_info', array('id' => $cartid)); 
        }
    }

    function getProductSize($product_id)
    {
        /*$query=$this->db->query("Select distinct a.size_id,b.size_text from product_inventory as a,product_size as b where a.stock_count>0 and a.size_id=b.id and a.product_id = '".$product_id."' ");
        $result = $query->result_array();
        return $result;*/
        $res_array = array();
        if($product_id!='')
        {
            //Size = 17
            // For Parent-child relationship 
            /* $query = $this->db->query("select CONCAT(c.attribute_id,'@',b.attr_value_name) as size from attribute_object_relationship as c,attribute_values_relationship as b where b.id = c.attribute_id  and c.object_type = 'product' and b.attribute_id = '".ATTRIBUTE_SIZE_ID."' and c.object_id=".$product_id." ");
            $res = $query->result_array();

            $i=0;
             foreach($res as $val) 
             {
                $res_arr = explode('@',@$val['size']);            
                $res_array[$i]['size_id'] = $res_arr[0];
                $res_array[$i]['size_text'] = $res_arr[1];
                $i++;
             }     */
             // End For Parent-child relationship 

             $query=$this->db->query("Select distinct a.size_id,b.size_text from product_inventory as a,product_size as b where a.stock_count>0 and a.size_id=b.id and a.product_id = '".$product_id."' ");
             $res = $query->result_array();
             $i=0;
             foreach($res as $val) 
             {  
                $res_array[$i]['size_id'] = $val['size_id'];
                $res_array[$i]['size_text'] = $val['size_text'];
                $i++;
             } 
            
            return $res_array;
        }
    }

    function get_products($product_ids){
        $query = $this->db->query("select id,name,description,image,slug,price,compare_price from product_desc where id in($product_ids) ");
        $result = $query->result_array();
        return $result;
    }

    function getUserOrders($user_id){
    	$result = array();
        if(!empty($user_id))
        {
         $query = $this->db->query('select a.`shipping_amount`,a.`id`, a.`user_id`, a.`brand_id`,a.`order_unique_no`, sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total , sum(a.`order_total`) as order_total, a.`pay_mode`, a.`created_datetime`, a.`modified_datetime`, a.`order_status`, a.`first_name`, a.`last_name`, a.`email_id`, a.`mobile_no`, a.`shipping_address`, a.`pincode`, a.`city_name`, a.`state_name`,c.`state_name`,b.`company_name`,a.`total_discount_percent`,a.coupon_code,a.coupon_amount,a.order_display_no,a.billing_address from order_info as a,brand_info as b,states as c where a.`brand_id` = b.`user_id` and a.`state_name` = c.`id` and a.`user_id` = "'.$user_id.'" AND a.`order_status`=1 group by a.`order_unique_no` order by a.`id` desc');
         $result = $query->result_array();       	  
        }
        return $result;
        
    }

    function getUserProductsOrders($user_id){
        $result = array();
        if(!empty($user_id))
        {
         $query = $this->db->query('select a.`shipping_amount`,b.`order_status`,e.`company_name`,a.`order_unique_no`,a.`order_display_no`,a.`id`,c.`name`,c.`price`,b.product_qty,b.product_size,d.size_text as size_text,(Select sum(i.product_price) from order_product_info as i  group by i.order_id having i.order_id = a.id ) as OrderIdProductSum,(Select sum(h.product_price) from order_info as g,order_product_info as h where g.id = h.order_id and g.order_unique_no = a.order_unique_no group by g.order_unique_no ) as OrderProductTotal,b.product_price, c.`image`,a.shipping_address,b.discount_percent,b.coupon_code,c.`id` as product_id from `order_info` as a,`order_product_info` as b,`product_desc` as c,`product_size` as d,brand_info as e where 1 and a.`id` = b.`order_id` and b.`product_id` = c.`id` and a.`user_id` = "'.$user_id.'" and d.`id` = b.product_size and e.user_id = c.`brand_id` order by a.`id` desc');
        $result = $query->result_array();
    	}
        return $result;
    }

    function display_wardrobe($user_id){
       
        $query = $this->db->query("select id,image_name from users_wardrobe where user_id='".$user_id."' and is_active = 1 order by id desc");
        $result = $query->result_array();
		return $result;
        // $wordrobehtml = '';        
        // if(!empty($result)){
            // foreach($result as $val){            	
                // $wordrobehtml =$wordrobehtml.'<div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 item-wrp"><div class="item item-hover"><div class="item-img">
                  // <a href="'.$this->config->item('wordrobe_path_url').$val['image_name'].'" data-gallery><img src="'.$this->config->item('wordrobe_thumb_path_url').$val['image_name'].'" /></a></div></div></div>';
            // }
        // }else
        // {
        	// $wordrobehtml = "No Items into the Wardrobe";
        // }
        // echo $wordrobehtml;
    }

    function getUserEmail($userid){
    	$email = '';
    	if($userid!='')
    	{
	        $query = $this->db->query('select email from user_login where id ="'.$userid.'"');
	        $username_email = $query->result_array();
	        $email = $username_email[0]['email'];
    	}
    	return $email;    	       
    }

    function update_cart($uniquecookie,$productid,$userid,$size,$agent,$platform_info,$look_id){

        $login_id = $this->session->userdata('user_id'); if(!$userid){ $userid = 0; }

        $time = new DateTime(date('Y-m-d H:i:s'));
        $time->add(new DateInterval('PT330M'));
        $dateM = $time->format('Y-m-d H:i:s');


        if($productid!='' && $size!='' && $userid!=''){
        $query = $this->db->query("select * from cart_info where (`SCUniqueID` = '".$uniquecookie."' and user_id='".$userid."' and `product_id` = '".$productid."' and `product_size` = '".$size."' and `order_id` is null )");
        $res = $query->result_array();
        if(empty($res)){
            $data = array(
               'product_id' => $productid,
               'user_id' => $userid,
               'created_datetime' => $dateM,
               'product_size' => $size,
               'product_qty' => '1',
               'SCUniqueID' => $uniquecookie,
               'agent' =>$agent,
               'platform_info'=>$platform_info,
               'look_id'=>$look_id,
               'created_by' => $login_id
            );
            $this->db->insert('cart_info', $data);
        }else{
            if($res[0]['product_qty']<5){
                $data = array(
                   'product_id' => $productid ,
                   'user_id' => $userid,
                   'created_datetime' => $dateM,
                   'product_size' => $size,
                   'product_qty' => $res[0]['product_qty']+1,
                   'SCUniqueID' => $uniquecookie,
                   'agent' =>$agent,
                   'platform_info'=>$platform_info,
                   'modified_by' => $login_id
                );

                //$this->db->where(array('id'=>$res[0]['id'],'product_id' => $productid,'product_size' => $size,'SCUniqueID' => $uniquecookie));
                 $this->db->where(array('id'=>$res[0]['id'],'product_id' => $productid,'SCUniqueID' => $uniquecookie));
                $this->db->update('cart_info', $data);
                }
            }
           
        }
    }

    function updatecartproductsize($cartid,$productid,$productsize,$userid)
    {

        $login_id = $this->session->userdata('user_id'); if(!$userid){ $userid = 0; }

        $time = new DateTime(date('Y-m-d H:i:s'));
        $time->add(new DateInterval('PT330M'));
        $dateM = $time->format('Y-m-d H:i:s');
        if($cartid!='' && $productid!='' && $productsize!='' && $userid!='' )
        {

            //$query = $this->db->query("select * from cart_info where ( user_id='".$userid."' and `product_id` = '".$productid."' and `product_size` = '".$productsize."' and `order_id` is null )");
            $query = $this->db->query("select * from cart_info where ( id='".$cartid."' and `product_id` = '".$productid."' and `user_id` = '".$userid."' and `order_id` is null )");
            $res = $query->result_array();
            if(!empty($res)){
                $data = array(                   
                       'modified_datetime' => $dateM,
                       'product_size' => $productsize,                   
                       'modified_by' => $login_id
                    );
                $this->db->where(array('id'=>$cartid,'product_id' => $productid,'user_id' => $userid));
                $this->db->update('cart_info', $data);               
                return 1;
            }else
            {
                return 0;
            }
        }
    }

    function get_discount_percentage($brand_id,$product_id){
        if($brand_id > 0){
            $query = $this->db->query("select discount_percent from discount_availed as da where (da.brand_id = '".$brand_id."' || da.product_id = '".$product_id."') AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1");
           /* echo $this->db->last_query();*/
            $result = $query->result_array();
            if(!empty($result)){
                return $result[0]['discount_percent'];
            }else{
                return 0;
            }
        }else{ return 0; }
    }

    function calculate_discount_percent($compare_price,$discount_price,$product_price=null){
        // echo "comming".$compare_price.'----'.$discount_price;exit;
        $discount_percent = 0;
        if($compare_price > 0 && $discount_price > 0){
            $discount_percent = round((1-($discount_price/$compare_price))*100);
        }else if($compare_price > 0 && $product_price > 0){
            $discount_percent = round((1-($product_price/$compare_price))*100);
        }
        
        return $discount_percent;
    }

    /*
        Discount Type 
        1 : Cart Discount
        2 : Cart % Discount
        3 : Product Discount
        4 : Product % Discount
    */

    function check_coupon_exist($coupon=null,$useremail=null,$userid=null){
        $user_email_id = $useremail;
        $cart_product_cost = 0; $discount_amount = 0; $response = array(); $cart_brand = array();
        $query = $this->db->query("SELECT `id`, `coupon_code`, `coupon_desc`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `individual_use_only`, `exclude_sale_items`, `brand_id`, `products`, `exclude_products`, `prd_category`, `exclude_prd_category`, `email_restriction`, `usage_limit_per_coupon`, `usage_limit_to_items`, `usage_limit_to_user`, `coupon_used_count`, `coupon_email_applied`, `stylecrackerDiscount`, `status`, `is_delete`, `created_datetime`, `created_by`, `modified_datetime`, `modified_by` FROM `coupon_settings` WHERE 1 and LOWER(coupon_code) = '".$coupon."' and coupon_start_datetime <= now() and coupon_end_datetime >= now() and status = 1 and is_delete = 0 and usage_limit_per_coupon!=coupon_used_count limit 0,1");
        $res = $query->result_array();
        //echo $this->db->last_query();
        if(!empty($res)){
            $products_cost = $this->getUserCartData($userid);

            $coupon_amt = 0; $coupon_per = 0; 

            $individual_use_only = $res[0]['individual_use_only'];
            $coupon_email_applied = $res[0]['coupon_email_applied'];
            $brand_id = $res[0]['brand_id'];
            $products = $res[0]['products'];
            $discount_type = $res[0]['discount_type'];
            $coupon_amount = $res[0]['coupon_amount'];
            $min_spend = $res[0]['min_spend'];
            $max_spend = $res[0]['max_spend'];
            $stylecrackerDiscount  = $res[0]['stylecrackerDiscount'];


            if($discount_type == '1' || $discount_type == '3'){
                $coupon_amt = $coupon_amount;
            }else{
                $coupon_per = $coupon_amount;
            }

            $cart_users = array();
            $cart_users = explode(',',$coupon_email_applied);

            if($individual_use_only == 1 && $coupon_email_applied!='' && $user_email_id=='' && $stylecrackerDiscount!=1){
                //setcookie('discountcoupon_stylecracker', '', -1, "/"); 
                $response['msg'] = 'Coupon Not Applicable';
                $response['msg_type'] = '0';
                $response['coupon_code'] = '';
                return $response;
            }else if($individual_use_only == 1 && $coupon_email_applied!='' && $user_email_id!='' && in_array($user_email_id,$cart_users) == 1){
                /* Abondant Cart Discount Userwise*/
               
                if(!empty($products_cost) && !empty($cart_users)){
                    foreach ($products_cost as $value) {
                        $cart_product_cost = $cart_product_cost + $value['discount_price'];
                    }
                    if($min_spend == 0 && $max_spend==0){
                        $cart_product_cost = $cart_product_cost;   
                    }else if($min_spend <= $cart_product_cost && $max_spend >= $cart_product_cost){
                        $cart_product_cost = $cart_product_cost;  
                    }else{
                        $cart_product_cost = 0;
                    }
                }

                if($cart_product_cost > 0){
                    if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                    }
                }
                if($discount_amount > 0){ 
                    //setcookie('discountcoupon_stylecracker', strtoupper($coupon), time() + (86400 * 30), "/"); 
                    $response['msg'] = 'Coupon Applied'; $response['brand_id'] = 0; $response['msg_type'] = '1'; 
                    $response['coupon_code'] = strtoupper($coupon);
                     } else{
                    //setcookie('discountcoupon_stylecracker', '', -1, "/"); 
                    $response['msg'] = 'Userwise Discount'; $response['msg_type'] = '1';
                    $response['coupon_code'] = strtoupper($coupon);
                }
                $response['coupon_discount_amount'] = $discount_amount;
                return $response;

            }else if($brand_id == 0 && $products == 0 && $stylecrackerDiscount==1){
                /* StyleCracker Discount */
                
                if(!empty($products_cost)){
                    foreach ($products_cost as $value) {
                        $cart_product_cost = $cart_product_cost + $value['discount_price'];
                    }
                    if($min_spend == 0 && $max_spend==0){
                        $cart_product_cost = $cart_product_cost;   
                    }else if($min_spend <= $cart_product_cost && $max_spend >= $cart_product_cost){
                        $cart_product_cost = $cart_product_cost;  
                    }else{
                        $cart_product_cost = 0;
                    }
                }

                if($cart_product_cost > 0){ 
                    if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $coupon_amount = $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                        $coupon_amount = $coupon_amount/100; 
                    }
                }
                if($discount_amount > 0){ 
                    //setcookie('discountcoupon_stylecracker', strtoupper($coupon), time() + (86400 * 30), "/"); 
                    $response['msg'] = 'Coupon Applied';$response['brand_id'] = 0;$response['msg_type'] = '1'; 
                    $response['coupon_code'] = strtoupper($coupon);
                     } else{
                    //setcookie('discountcoupon_stylecracker', '', -1, "/"); 
                    $response['msg'] = 'Coupon will apply if you spend minimum <span class="fa fa-inr"></span>'.$min_spend.' on StyleCracker';
                    $response['msg_type'] = '0'; 
                    $response['coupon_code'] = '';
                }
                $response['coupon_discount_amount'] = $discount_amount;
                return $response;
            }else if($brand_id > 0 && $products == 0){
                /* Brands Discount */
               
                if(!empty($products_cost)){
                    foreach ($products_cost as $value) {
                        if($value['brand_id'] == $brand_id){
                            $cart_product_cost = $cart_product_cost + $value['discount_price'];
                        }
                    }
                    if($min_spend == 0 && $max_spend==0){
                        $cart_product_cost = $cart_product_cost;   
                    }else if($min_spend <= $cart_product_cost && $max_spend >= $cart_product_cost){
                        $cart_product_cost = $cart_product_cost;  
                    }else{
                        $cart_product_cost = 0;
                    }
                }

                if($cart_product_cost > 0 ){
                    if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                    }
                }
                if($discount_amount > 0){ 
                    setcookie('discountcoupon_stylecracker', strtoupper($coupon), time() + (86400 * 30), "/"); 
                    $response['msg'] = 'Coupon Applied'; $response['brand_id'] = $brand_id; $response['msg_type'] = '1'; 
                    $response['coupon_code'] = strtoupper($coupon);
                    } else{
                    setcookie('discountcoupon_stylecracker', '', -1, "/"); 
                    $response['msg'] = 'Coupon will apply if you spend minimum <span class="fa fa-inr"></span>'.$min_spend.' on '.$this->getBrandExtraInfo($brand_id)[0]['company_name'].'s products';
                     $response['msg_type'] = '0';
                     $response['coupon_code'] = ''; 
                }
                $response['coupon_discount_amount'] = $discount_amount;
                return $response;
            }else if($brand_id > 0 && $products > 0){
                /* Productwise Discount*/
               
                $cart_products = array();
                $cart_products = explode(',',$products);
                
                  if(!empty($products_cost) && !empty($cart_products)){
                    foreach ($products_cost as $value) {
                        if(in_array($value['product_id'],$cart_products) == true){
                            $cart_product_cost = $cart_product_cost + $value['discount_price'];
                        }
                    }
                    if($min_spend == 0 && $max_spend==0){
                        $cart_product_cost = $cart_product_cost;   
                    }else if($min_spend <= $cart_product_cost && $max_spend >= $cart_product_cost){
                        $cart_product_cost = $cart_product_cost;  
                    }else{
                        $cart_product_cost = 0;
                    }
                }

                if($cart_product_cost > 0 ){
                    if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                    }
                } 
                if($discount_amount > 0){ 
                    setcookie('discountcoupon_stylecracker', strtoupper($coupon), time() + (86400 * 30), "/"); 
                    $response['msg'] = 'Coupon Applied'; $response['brand_id'] = $brand_id; $response['msg_type'] = '1'; 
                    $response['coupon_code'] = strtoupper($coupon);
                } else{
                    setcookie('discountcoupon_stylecracker', '', -1, "/"); 
                    $response['msg'] = 'Coupon code will be applied for specific products'; $response['msg_type'] = '0';
                    $response['coupon_code'] = '';
                }
                $response['coupon_discount_amount'] = $discount_amount;

                
                return $response;
            }

        }else{
            setcookie('discountcoupon_stylecracker', '', -1, "/");  
            $response['msg'] = 'No Coupon Found or Expired';
            $response['msg_type'] = '0';
            $response['coupon_code'] = '';
            $response['coupon_discount_amount'] = 0;
            return $response;
        }
    }

    function get_discount_id($brand_id,$product_id)
    {
        if($brand_id > 0 && $product_id > 0){
           $query = $this->db->query("select id, discount_type_id from discount_availed as da where (da.brand_id = '".$brand_id."' OR da.product_id = '".$product_id."' ) AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1 ");
       
            $result = $query->result_array();

            if(!empty($result)){
                return $result[0];
            }else{
                return 0;
            }
        }else if($brand_id > 0){
            $query = $this->db->query("select id, discount_type_id from discount_availed as da where (da.brand_id = '".$brand_id."' ) AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1 ");
       
            $result = $query->result_array();
            if(!empty($result)){
                return $result[0];
            }else{
                return 0;
            }

        }else{

            if($product_id > 0){
                $query = $this->db->query("select da.id, da.discount_type_id from discount_availed as da where (da.brand_id = 0 AND da.product_id = '".$product_id."' ) AND da.status=1");
             
                $result = $query->result_array();
                if(!empty($result)){
                    return $result[0];
                }else{
                    return 0;
                }
            }else
            {
                $query = $this->db->query("select da.id, da.discount_type_id from discount_availed as da where (da.brand_id = '".$brand_id."' AND da.product_id = 0 ) AND da.status=1");
              
                $result = $query->result_array();
                if(!empty($result)){
                    return $result[0];
                }else{
                    return 0;
                }
            }
         }
    }

    function get_discount_maxvalue($brand_id,$product_id){
        if($brand_id > 0){
            
            $query = $this->db->query("select max_amount from discount_availed as da where (da.brand_id = '".$brand_id."') AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1");
            /*echo $this->db->last_query();exit;*/
            $result = $query->result_array();
            if(!empty($result)){
                return $result[0]['max_amount'];
            }else{
                return 0;
            }
        }else{ return 0; }
    }

    function get_size($product_id){
        
       /* $query = $this->db->query("select distinct b.`product_id`,c.`size_text`,b.stock_count,c.`id` from `product_inventory` as b,`product_size` as c where b.`size_id` = c.`id` and b.stock_count > 0 and b.`product_id`='".$product_id."'");
        $res = $query->result_array();
        return $res;*/
        if($product_id!='')
        {
            //Size = 17
             $query = $this->db->query("select CONCAT(c.attribute_id,'@',b.attr_value_name) from attribute_object_relationship as c,attribute_values_relationship as b where b.id = c.attribute_id  and c.object_type = 'product' and b.attribute_id = '".ATTRIBUTE_SIZE_ID."' and c.object_id=".$product_id." ");
            $res = $query->result_array();
            return $res;
        }
    }


    function get_user_info($user_id){
        $query = $this->db->get_where('user_login', array('id' => $user_id));
        return $query->result_array();
    }

     function get_product_availablility($product_id,$size_id){
        $query = $this->db->query("select sum(stock_count) as stock_count from `product_inventory` where product_id = '".$product_id."' and size_id = '".$size_id."'");
       /* $query = $this->db->query("select sum(stock_count) as stock_count from `product_desc` where parent_id = '".$product_id."' ");*/
        $res = $query->result_array();
        if(!empty($res)){
            if($res[0]['stock_count'] > 0){
                return 0;
            }else{
                return 1;
            }
        }
    }

    function getBrandExtraInfo($brand_id){
        $query = $this->db->get_where('brand_info', array('user_id' => $brand_id));
        $res = $query->result_array();
        if(!empty($res)){
            return $res;
        }else{
            return '';
        }
    }  
	
	function upload_wardrobe_images($image_name,$user_id){
		
		$data = array(
               'user_id' => $user_id,
               'image_name' => $image_name,
               'is_active' => '1',
               'created_by' => $user_id,
               'modified_by' =>$user_id,
               'created_datetime'=>date('Y-m-d H:i:s')
            );
		$this->db->insert('users_wardrobe', $data);
			
		return true;
	}

    public function getImage($arrData = array())
    {
        $this->db->select('image_name');
        $this->db->from('users_wardrobe');
        $this->db->where('id', $arrData[0]);
        $this->db->where('user_id', $arrData[1]);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->row_array();
        }
        return false;
    }
    
    public function deleteImage($arrData = array())
    {
        $this->db->where('id', $arrData[0]);
        $this->db->where('user_id', $arrData[1]);
        $this->db->delete('users_wardrobe');
        return true;
    } 

    public function addProduct($arrData = array())
    {
        $this->db->insert('product_desc', $arrData);
        $pid = $this->db->insert_id();
        return  $pid;
    }
    
    public function addObjectMeta($arrData = array())
    {
        $this->db->insert('object_meta', $arrData);
        $omid = $this->db->insert_id();
        return  $omid;
    }
    
    public function chkObjectMeta($oid = '')
    {
        $this->db->select('id,object_id,object_meta_key,object_meta_value');
        $this->db->from('object_meta');
        $this->db->where('object_meta_key', $oid);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        return false;
    }
    
    
    public function updateObjectMeta($oid, $arrUpdate = array())
    {
        $this->db->where('object_meta_key', $oid);
        $this->db->update('object_meta', $arrUpdate);
        return true;
    }
    
    public function getMetaValue($objectMetaKey = '', $objectId= '')
    {
        $this->db->select('object_meta_value');
        $this->db->from('object_meta');
        $this->db->where('object_meta_key', $objectMetaKey);
        $this->db->where('object_id',$objectId);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->row_array();
        }
        return false;
    }
    
    public function getMetaValueData($objectId= '')
    {
        $this->db->select('id,product_cat_id,brand_id,name,description,price,image,approve_reject');
        $this->db->from('product_desc');
        $this->db->where('id', $objectId);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->row_array();
        }
        return false;
    }    
   
    public function getSereializeProductDetail($objectMetaKey = '', $oid='', $uid='', $objectId= '')
    {   
        $this->db->select('object_meta_value');
        $this->db->from('object_meta');
        $this->db->where('object_meta_key', $objectMetaKey);
        //$this->db->where('created_by', $uid);
        //$this->db->where('object_id IN ('.$objectId.')');
        $this->db->where('object_id',$objectId);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->row_array();
        }
        return false;
    }

	public function get_all_tag($tag ='')
	{
		$this->db->select('id,tag_type_id,name,slug,is_delete');
		$this->db->from('tag');
		$this->db->where('is_delete',0);
		if(!empty($tag) && count($tag)>0)
		{
			$this->db->like('name',$tag , 'both');
		}
		$query = $this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}

    public function update_inventory($key ='', $sku='')
    {
        $this->db->set('stock_count', 'stock_count-1', false);
        $this->db->where('id' , $key);
        $this->db->where('sku_number', $sku);
        $this->db->limit('1');
        $this->db->update('product_desc');
        //$this->db->update('product_inventory');
    }
	
	public function get_order_display_number($orderUniqueId)
	{
		$this->db->select('order_display_no');
		$this->db->from('order_info');
		$this->db->where('order_unique_no',$orderUniqueId);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		return false;
	}
	
	public function get_curated_data($objectId='',$order_display_no='')
	{
		$this->db->select('id,object_id,object_meta_key,object_meta_value');
		$this->db->from('object_meta');
		$this->db->where('object_id', $objectId);
		$this->db->where('object_meta_key', $order_display_no);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		return false;
	}

    public function restore_inventory($productid ='')
    {
        $this->db->set('stock_count', 'stock_count+1', false);
        $this->db->where('id' , $productid);   
        $this->db->limit('1');
        $this->db->update('product_desc');       
    }

     public function deduct_inventory($productid ='')
    {
        $this->db->set('stock_count', 'stock_count-1', false);
        $this->db->where('id' , $productid);
        $this->db->where('stock_count >' ,'0');
        $this->db->limit('1');
        $this->db->update('product_desc');
    }

     public function get_inventory($productid ='')
    {
        $this->db->select('stock_count');
        $this->db->from('product_desc'); 
        $this->db->where('id' , $productid);
        $this->db->limit('1');
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $result = $query->row_array();
            return $result['stock_count'];
        }
        return false;     
    }

 } 
?>
