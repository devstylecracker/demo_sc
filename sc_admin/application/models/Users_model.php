<?php
class Users_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function index()
	{	
		echo 'Users_model';exit;
	}
    
    function get_allusers($search_by,$table_search,$page,$per_page){  
		if($search_by == '1'){ $search_cond = " AND a.user_name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '2'){ $search_cond = " AND a.email like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '3'){ $search_cond = " AND a.created_datetime like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '4'){ $search_cond = " AND a.id = ".$table_search; }
		else{ $search_cond =''; }
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}
		
		$query = $this->db->query("select a.id,a.user_name, a.bucket, a.created_datetime, a.email,c.`role_name` from user_login as a, user_role_mapping as b,role_master as c WHERE a.`status` =1 and a.`id` = b.`user_id` and b.`role_id`<>2 and b.`role_id`<>6 and c.`id` = b.role_id ".$search_cond." order by a.id desc ".$limit_cond);
		$res = $query->result();
		//echo $this->db->last_query();
		return $res;
	}
	
	function get_all_modules(){
		$query = $this->db->query("SELECT `id`, `module_name`, `status` FROM `module_master` WHERE `status` =1");
		$res = $query->result();
		return $res;	
	}
	
	function add_permissions($all_per,$user_id,$login_user_id){
		$data = array();
		$this->db->delete('user_module_permission', array('user_id' => $user_id)); 
		foreach($all_per as $val){
			$data[] = array('user_id'=>$user_id,'module_id'=>$val,'created_by'=>$login_user_id,'created_datetime'=>date('Y-m-d H:i:s'),'modified_by'=>$login_user_id);
			}
		$this->db->insert_batch('user_module_permission', $data); 
		return true;
		}
		
	function get_roles(){
		$query = $this->db->query("SELECT `id`, `role_name`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`, `description` FROM `role_master` WHERE 1");
		$res = $query->result();
		return $res;
		}
		
	function add_backend_user($user_type,$first_name,$last_name,$username,$email,$password,$login_user_id){
		$data = array("bucket"=>1,"user_name"=>$username,"password"=>$password,"email"=>$email,"ip_address"=>$_SERVER['REMOTE_ADDR'],"status"=>1,"created_by"=>$login_user_id,"created_datetime"=>date('Y-m-d H:i:s'));
		
		$this->db->insert('user_login', $data);
		$last_ins_id = $this->db->insert_id(); 
		
		$user_role_map_data = array("user_id"=>$last_ins_id,"role_id"=>$user_type,"created_by"=>$login_user_id,"created_datetime"=>date('Y-m-d H:i:s'));
		
		$this->db->insert('user_role_mapping', $user_role_map_data);
		
		$user_extra_info = array("user_id"=>$last_ins_id,"first_name"=>$first_name,"last_name"=>$last_name,"created_by"=>$login_user_id,"created_datetime"=>date('Y-m-d H:i:s'));
		
		$this->db->insert('user_info', $user_extra_info);

		$brand_info = array("user_id"=>$last_ins_id,"company_name"=>$first_name." ".$last_name,"created_by"=>$login_user_id,"created_datetime"=>date('Y-m-d H:i:s'));
		$brand_contact_info = array("brand_id"=>$last_ins_id,"created_by"=>$login_user_id,"created_datetime"=>date('Y-m-d H:i:s'));

		if($user_type == '6')
		{
			$this->db->insert('brand_contact_info', $brand_contact_info);
			$this->db->insert('brand_info', $brand_info);
		}
		
		$dpartner_info = array("delivery_partner_name"=>$username,"user_id"=>$last_ins_id,"status"=>1,"created_by"=>$login_user_id,"created_datetime"=>date('Y-m-d H:i:s'));
		if($user_type == '7')
		{
			$this->db->insert('delivery_partner', $dpartner_info);			
		}

		$query = $this->db->get_where('role_permission_mappings', array('role_id' => $user_type));
		$res = $query->result_array();

			if(!empty($res)){
				foreach ($res as $value) {
					$data = array('user_id'=>$last_ins_id,'module_id'=>$value['module_id'],'created_by'=>$login_user_id,'created_datetime'=>date('Y-m-d H:i:s'));
					$this->db->insert('user_module_permission', $data);
					//echo $this->db->insert_id(); 
				}

			}
			
			return true;
		
		}
		
	function get_user_data($id){
		/* $query = $this->db->query("select a.id,a.user_name, a.email,b.`first_name`,b.`last_name`,b.`contact_no`,c.`role_id`,d.`role_name`,b.`gender`, b.`registered_from`,b.`facebook_id`,b.`profile_pic`,b.`profile_cover_pic`,b.fb_data,b.instagram_data from user_login as a,user_info as b,user_role_mapping as c,role_master as d where a.`id`=b.`user_id` and b.`user_id` = c.`user_id` and c.`role_id` = d.`id` and a.`id`=".$id);
		$res = $query->result_array();
		return $res; */
		
		$this->db->select('a.id,a.user_name, a.email,b.`first_name`,b.`last_name`,b.`contact_no`,c.`role_id`,d.`role_name`,b.`gender`, b.`registered_from`,b.`facebook_id`,b.`profile_pic`,b.`profile_cover_pic`,b.fb_data,b.instagram_data');
		$this->db->from('user_login as a');
		$this->db->join('user_info as b', 'a.id=b.user_id', 'left');
		$this->db->join('user_role_mapping as c', 'b.user_id = c.user_id', 'left');
		$this->db->join('role_master as d', 'c.role_id = d.id', 'left');
		$this->db->where('a.id', $id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}


	function sc_box_mobile_number($user_id){	
		$res = array();	
		//$user_id = 37394;
		//$query = $this->db->query("select a.meta_value from user_metadata as a  where a.`user_id` =".$user_id);
		//$res = $query->result_array();
		//echo $this->db->last_query();
		//exit;
		return $res;
		
	}
	
	function edit_backend_user($user_type,$first_name,$last_name,$user_id,$password,$login_user_id){

		$data = array("bucket"=>1,"ip_address"=>$_SERVER['REMOTE_ADDR'],"status"=>1,"modified_by"=>$login_user_id,"modified_datetime"=>date('Y-m-d H:i:s'));
		if(trim($password)!=''){ $data = array("bucket"=>1,"password"=>$password,"ip_address"=>$_SERVER['REMOTE_ADDR'],"status"=>1,"modified_by"=>$login_user_id,"modified_datetime"=>date('Y-m-d H:i:s'));}
		$this->db->where('id', $user_id);
		$this->db->update('user_login', $data);
		$last_ins_id = $user_id; 
		
		$user_role_map_data = array("user_id"=>$last_ins_id,"role_id"=>$user_type,"modified_by"=>$login_user_id,"modified_datetime"=>date('Y-m-d H:i:s'));
		$this->db->where('user_id', $user_id);
		$this->db->update('user_role_mapping', $user_role_map_data);
		
		$user_extra_info = array("user_id"=>$last_ins_id,"first_name"=>$first_name,"last_name"=>$last_name,"modified_datetime"=>$login_user_id,"created_datetime"=>date('Y-m-d H:i:s'));
		$this->db->where('user_id', $user_id);

		if($this->db->update('user_info', $user_extra_info)){

			$res1= $this->db->get_where('user_module_permission',array('user_id'=>$user_id));
			$res1 = $res1->result(); 
			if(count($res1)>0)
			{
				$this->db->delete('user_module_permission', array('user_id'=>$user_id));
				$query = $this->db->get_where('role_permission_mappings', array('role_id' => $user_type));
				$res = $query->result_array();

				if(!empty($res)){
					foreach ($res as $value) {
						$data = array('user_id'=>$last_ins_id,'module_id'=>$value['module_id'],'created_by'=>$login_user_id,'created_datetime'=>date('Y-m-d H:i:s'));
						$this->db->insert('user_module_permission', $data);
						//echo $this->db->insert_id(); 
					}
				}
			}else
			{
				$query = $this->db->get_where('role_permission_mappings', array('role_id' => $user_type));
				$res = $query->result_array();

				if(!empty($res)){
					foreach ($res as $value) {
						$data = array('user_id'=>$last_ins_id,'module_id'=>$value['module_id'],'created_by'=>$login_user_id,'created_datetime'=>date('Y-m-d H:i:s'));
						$this->db->insert('user_module_permission', $data);
						//echo $this->db->insert_id(); 
					}
				}

			}


			return true;
			}else{ 
				return false;
				}
		
		}
		
	function delete_user($id,$login_user_id){
		if($id){
			$data = array('status'=>0,"modified_by"=>$login_user_id,"modified_datetime"=>date('Y-m-d H:i:s'));
			$this->db->where('id', $id);
			$this->db->update('user_login', $data);
		}
	}

	 function get_user_pa($user_id){
		if($user_id != "")
		{
			$res = array();
		/*$query = $this->db->query('SELECT a.object_meta_key,b.`answer`,(select c.object_meta_value from object_meta as c where c.object_meta_key = "user_brand_list" AND c.`created_by`= '.$user_id.' limit 0,1) as brand_ids FROM `object_meta` a, `answers` AS b WHERE b.`id` = a.`object_meta_value` AND a.`created_by`= '.$user_id.' and a.object_meta_key in ("user_body_shape","user_style_p1","user_style_p2","user_style_p3")');
		$res = $query->result_array();*/
		return $res; 
		}
		else{
			return false;
		}
		/* if($user_id != "")
		{
			$this->db->select('a.object_meta_key,b.`answer`,(select c.object_meta_value from object_meta as c where c.object_meta_key = "user_brand_list" AND c.`created_by`= '.$user_id.' limit 0,1) as brand_ids');
			$this->db->from('object_meta as a');
			$this->db->join('answers as b', 'b.id = a.object_meta_value', 'left');
			$this->db->where_in('a.object_meta_key', array('user_body_shape','user_style_p1','user_style_p2','user_style_p3'));
			$this->db->where('a.created_by', $user_id);
			echo $this->db->last_query();
			$query = $this->db->get();
			if($query->num_rows()>0)
			{
				return $query->result_array();
			}
			return false;
		}
		else{
			return false;
		} */
	} 
	
	function get_user_pa_extras($user_id){
		/*if($user_id != "")
		{
			$res = array();
		$query = $this->db->query('SELECT a.`question_id`,b.`answer` FROM `users_answer` AS a, `answers` AS b WHERE 1 AND b.`id` = a.`answer_id` and a.`user_id`= '.$user_id);
		$res = $query->result_array();
		return $res;
		}
		else{return false;}*/
		return false;
	}
	
	function get_wordrobe_image($user_id){
		if($user_id != "")
		{
		$query = $this->db->query('SELECT `id`, `user_id`, `caption`, `description`, `image_name`, `is_active`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`, `referred_count` FROM `users_wardrobe` WHERE 1 and is_active=1 and user_id='.$user_id);
		$res = $query->result_array();
		return $res;
		}else{ return false;}
	}

	function extra_info($user_id){
		//$query = $this->db->query("SELECT `id`, `user_id`, `visit_count`, `first_visit` FROM `user_history_info` WHERE 1 and `user_id`='".$user_id."'");
		if($user_id != "")
		{
			$query = $this->db->query("SELECT login_time FROM `user_login_attempts` WHERE `user_id`='".$user_id."' ORDER BY `user_login_attempts`.`login_time` ASC limit 0,1");
			$res = $query->result_array();
			return $res;
		}else{ return false;}
	}

	function visit_count($user_id){
		//$query = $this->db->query("SELECT `id`, `user_id`, `visit_count`, `first_visit` FROM `user_history_info` WHERE 1 and `user_id`='".$user_id."'");
		if($user_id != "")
		{
			$query = $this->db->query("SELECT count(id) as visit_count FROM `user_login_attempts` WHERE `user_id`='".$user_id."' ORDER BY `user_login_attempts`.`login_time`");
			$res = $query->result_array();
			return $res;
		}else{ return false;}
	}

	function last_login($user_id){
		//$query = $this->db->query("SELECT current_visit_time FROM `user_visit_history` WHERE 1 and `user_id`='".$user_id."' order by id desc limit 0,1");
		if($user_id != "")
		{
			$query = $this->db->query("SELECT login_time  FROM `user_login_attempts` WHERE `user_id` = '".$user_id."' ORDER BY `user_login_attempts`.`login_time` DESC limit 0,1");
			$res = $query->result_array();
			return $res;
		}else{ return false;}
	}

	function total_look_click_count($user_id){
		/*if($user_id != "")
		{
			$res = array();
			$query = $this->db->query("SELECT count(id) as count FROM `users_look_click_track` WHERE 1 and `user_id`='".$user_id."'");
			$res = $query->result_array();
			return $res;
		}else{ return false;}*/
		return false;
	}

	function total_product_click_count($user_id){
		if($user_id != "")
		{
			$res = array();
			/*$query = $this->db->query("SELECT count(id) as count FROM `users_product_click_track` WHERE 1 and `user_id`='".$user_id."'");*/
			//$res = $query->result_array();
			return $res;
		}else{ return false;}
	}

	function fav_look_count($user_id){
		/*if($user_id != "")
		{
			$res = array();
			$query = $this->db->query("SELECT count(id) as count FROM `look_fav` WHERE 1 and `user_id`='".$user_id."'");
			$res = $query->result_array();
			return $res;
		}else{ return false;}*/
		return false;
	}

	function fav_look_detail($user_id){
		/*if($user_id != "")
		{
			$query = $this->db->query("SELECT b.id ,b.name , a.modified_datetime as datetime  FROM `look_fav` as a, look as b  WHERE a.look_id = b.id and `user_id`='".$user_id."'");
			$res = $query->result_array();
			return $res;
		}else{ return false;}*/
		 return false;
	}

	function user_look_click_id($user_id){
		/*if($user_id != "")
		{
			$query = $this->db->query("SELECT a.`look_id` as id,b.`name`,a.`created_datetime` as datetime  FROM `users_look_click_track` as a, `look` as b WHERE  a.`look_id` = b.`id` and a.`user_id`='".$user_id."'");
			//$this->db->last_query();
			$res = $query->result_array();
			return $res;
		}else{ return false;}*/
		 return false;
	}

	function user_product_click_id($user_id){
		if($user_id != "")
		{
			$query = $this->db->query("SELECT a.`product_id` as id, b.`name`, a.`created_datetime` as datetime FROM `users_product_click_track` as a, `product_desc` as b  WHERE  a.`product_id` = b.`id` and a.`user_id`='".$user_id."'");
			//echo $this->db->last_query();;exit;
			$res = $query->result_array();
			return $res;
		}else{ return false;}
	}

	function emailUnique($emaild){		
        $query = $this->db->query('select email from user_login where email ="'.$emaild.'"');		
        $username_unique = $query->result_array();		
        return $username_unique;		
    }

    function stylist_assigned_pa($data)
    {
    	$result = 0;
    	if(!empty($data))
    	{
    		$obj = $this->check_object_exist($data['stylist_id'],'Stylist');
    		if(!empty($obj))
    		{
		    	$last_ins_id = $obj[0]['object_id'];
		    	$result = 2;
			}else
			{
				$data_object = array("object_id"=>$data['stylist_id'],"object_name"=>$data['stylist_name'],"object_type"=>'Stylist',"object_created"=>date('Y-m-d H:i:s'),"object_creator"=>$data['login_user_id']);		
				$this->db->insert('object', $data_object);
				$last_ins_id = $this->db->insert_id();				
			}

			$obj_meta = $this->check_objectmeta_exist($last_ins_id,'_assigned_pa');
			if(!empty($obj_meta))
			{
				$data_meta = array("object_id"=>$data['stylist_id'],"object_meta_key"=>"_assigned_pa","object_meta_value"=>$data['assigned_pa'],"modified_by"=>$data['login_user_id']);
				$this->db->update('object_meta', $data_meta);
				$result = 2;
			}else
			{				
				$data_meta = array("object_id"=>$data['stylist_id'],"object_meta_key"=>"_assigned_pa","object_meta_value"=>$data['assigned_pa'],"created_datetime"=>date('Y-m-d H:i:s'),"created_by"=>$data['login_user_id']);
				$this->db->insert('object_meta', $data_meta);
				$result = 1;
			}
		}
		return $result;
    }

    function check_object_exist($objectid,$objecttype)
    {
    	if($objectid!='' && $objecttype!='')
    	{
    		$query = $this->db->query('select * from object where object_id ="'.$objectid.'" and  object_type ="'.$objecttype.'" ');
        	$object_data = $query->result_array();		
        	return $object_data;
    	}    			
    }

    function check_objectmeta_exist($objectid,$objecttype)
    {
    	if($objectid!='' && $objecttype!='')
    	{
    		$query = $this->db->query('select * from object_meta where object_id ="'.$objectid.'" and  object_meta_key ="'.$objecttype.'" ');
        	$object_data = $query->result_array();		
        	return $object_data;
    	}    			
    }

     function get_state_byid($id){        
        $query = $this->db->get_where('states', array('id' => $id));
        $res = $query->result_array();
        return $res;
    }

 } 
?>
