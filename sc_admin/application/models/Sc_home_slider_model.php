<?php
class Sc_home_slider_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->model('category_model');
    }
    
    function get_all(){ 
		$query = $this->db->query("select a.* from sc_home_slider as a where a.id >= 1 AND a.id <= 5 order by a.banner_position ");
		$res = $query->result_array();
		return $res;
	}
	
	function get_event_based_sliders(){ 
		$query = $this->db->query("select a.* from sc_home_slider as a where banner_type = 1 AND is_active = 1 order by a.end_date ");
		$res = $query->result_array();
		return $res;
	}
	
	function get_active_banners(){ 
		$time = new DateTime(date('Y-m-d H:i:s'));
        $time->add(new DateInterval('PT330M'));
        $dateM = $time->format('Y-m-d H:i:s');
		$query = $this->db->query("select a.* from sc_home_slider as a where a.start_date <= '".$dateM."' AND a.end_date >= '".$dateM."' AND banner_type = 1  AND is_active = 1 order by a.end_date ");
		$res = $query->result_array();
		return $res;
	}
	
	function get_upcomming_banners(){ 
		$time = new DateTime(date('Y-m-d H:i:s'));
        $time->add(new DateInterval('PT330M'));
        $dateM = $time->format('Y-m-d H:i:s');
		$query = $this->db->query("select a.* from sc_home_slider as a where banner_type = 1  AND is_active = 1 AND a.start_date >= '".$dateM."' order by a.end_date ");
		$res = $query->result_array();
		return $res;
	}
	
	function save_banner($line1,$line2,$button_text,$url,$status,$fromdate,$todate,$banner_image,$order){
			// $fromdate->add(new DateInterval('PT330M'));
			// $date_from = $fromdate->format('Y-m-d H:i:s');
			// $todate->add(new DateInterval('PT330M'));
			// $date_to = $todate->format('Y-m-d H:i:s');
		 // echo $status;exit;
		$data = array(
               'caption_line_1' => $line1 ,
               'caption_line_2' => $line2,
               //'product_size' => $button_text,
               'lable_link' => $url,
			   'is_active' =>$status,
               'start_date'=>$fromdate,
			   'end_date'=>$todate,
			   'slide_url'=>$banner_image,
			   'banner_position'=>$order,
            );
            $this->db->insert('sc_home_slider', $data);	
			// echo $this->db->last_query();exit;
	}
	
	function update_slider_img($filename,$url){
            $data = array('profile_pic'=>$filename);
            $this->db->update('sc_home_slider', $data, array('lable_link' => $url));
            return true;    
    }
	
	function check_availability($fromdate,$todate,$order){
		if($fromdate!='' && $todate!=''){
			$query = $this->db->query(" select a.* from sc_home_slider as a where a.start_date <= '$fromdate' AND a.end_date >= '$todate' AND a.banner_position = $order AND a.banner_type = 1  AND is_active = 1");
			$res = $query->result();
			$count = count($res);
			if($count > 0)
			return $count;
			else return 0;
		}

	}
	
	function remove_banner($id){
		$this->db->where('id', $id);
		$this->db->delete('sc_home_slider'); 
		return true;
	}
	
	function update_banner_data($id,$line1,$line2,$button_text,$url,$status,$fromdate,$todate,$order){
		$this->db->where('id', $id);
		$data = array(
					'caption_line_1' =>$line1 ,
					'caption_line_2' =>$line2,
					'banner_url' => $button_text,
					'lable_link' =>$url,
					'is_active' =>$status,
					'start_date'=>$fromdate,
					'end_date'=>$todate,
					'banner_position'=>$order,
					);
              
                $this->db->where(array('id'=>$id));
                $this->db->update('sc_home_slider', $data); 
				// echo $this->db->last_query();exit;
	}
} 
?>
