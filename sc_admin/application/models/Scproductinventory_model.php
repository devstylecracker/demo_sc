<?php
class SCProductinventory_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_brand_id($api_key,$api_secret_key){ 
        $query = $this->db->get_where('user_login', array('email' => trim($api_key),'password'=>trim($api_secret_key),'status'=>1));
        $result = $query->result_array();
        if(!empty($result)){
            return $result[0]['id'];
        }else{
            return 0;
        }
    }

    function get_product_id($product_color,$product_url){

        $query = $this->db->query("select id from product_desc where `product_color`='".$product_color."' AND `url`='".$product_url."'");
        $result = $query->result_array();
        if(!empty($result)){
            return $result[0]['id'];
        }else{
            return 0;
        }
    }

    function getbrand_code($brand_id){
        if($brand_id > 0){
            $query = $this->db->query("select brand_code from `brand_info` where user_id='".$brand_id."'");
            $result = $query->result_array();
            if(!empty($result)){
                return $result[0]['brand_code'];
            }else{
                return '';
            }
        }else{
            return '';
        }
    }
    
    function get_categoryID($category_name){
        if(trim($category_name)!=''){
            $query = $this->db->query("select id from product_category where lower(name) =lower('".$category_name."')");
            $res = $query->result_array();
            if(!empty($res)){ return $res[0]['id']; }else{ return 1; }
        }else{
            return 1;
        }
    }

    function get_subcategoryID($category_name,$cat_id = null){
        if(trim($category_name)!=''){
            $where_cond = '';
            if($cat_id != null) { $where_cond = ' and product_cat_id='.$cat_id; }
            $query = $this->db->query("select id from product_sub_category where lower(name) =lower('".$category_name."')".$where_cond);
            $res = $query->result_array();
            if(!empty($res)){ return $res[0]['id']; }else{ return 1; }
        }else{
            return 1;
        }
    }

    function add_product($data){
        if(!empty($data)){
                if(!empty($data['product_desc'])){
                    $this->db->insert('product_desc', $data['product_desc']); 

                    $product_insert_id = $this->db->insert_id(); 

                    /* Product Inventory */
                    if(!empty($data['product_stock']['size'])){
                       $this->db->where('product_id', $product_insert_id);
                       $this->db->delete('product_inventory'); 

                        if(!empty($data['product_stock']['size'])){  $o = 0;  
                            foreach($data['product_stock']['size'] as $val){ 
                                $size = $data['product_stock']['size'][$o]; 
                                $qty = $data['product_stock']['stock'][$o]; 
                                $product_sku = $data['product_stock']['product_sku'][$o]; 

                                $size_id = $this->get_size_id($size);
                                if($size_id>0 && $product_insert_id>0){
                                    $data1 = array(
                                        'product_id'=>$product_insert_id,
                                        'size_id'=>$size_id,
                                        'stock_count'=>$qty,
                                        'product_sku'=>$product_sku
                                    );
                                    $this->db->insert('product_inventory', $data1); 
                                }
                             $o++;
                            }
                            
                        }
                }
                /* Product Inventory end*/

                /* Product Extra image starts*/
                    if(!empty($data['product_extra_images'])){
                        $this->db->where('product_id', $product_insert_id);
                        $this->db->delete('extra_images'); 
                        foreach($data['product_extra_images'] as $val){
                            $image_info = array(
                                'product_id'=>$product_insert_id,
                                'product_images'=>$val,
                                'created_datetime'=>date('Y-m-d H:i:s')
                                );
                             $this->db->insert('extra_images', $image_info); 
                        }
                    }
                    
                /* Product Extra image end*/

                /* Products tags start*/
                     if(!empty($data['product_tags'])){
                       
                       foreach($data['product_tags'] as $val){
                            $tag_id = $this->get_tag_id($val);
                            $this->add_tag_to_product($tag_id,$product_insert_id,$data['product_desc']['brand_id']);
                        }
                    }
                   
                /* Products tags end*/

                /* Products tags start*/
                     if(!empty($data['product_brands'])){
                       
                       foreach($data['product_brands'] as $val){
                            $tag_id = $this->get_brand_avail_id($val);
                            $this->add_brands_to_product($tag_id,$product_insert_id);
                        }
                    }
                   
                /* Products tags end*/


                        if(!empty($data['variations']['var'])){

                            $vartypeid = $data['variations']['vartype'];

                            $variation = $data['variations']['var'];
                            for($i = 0; $i < sizeof($variation); $i++){

                                

                                    $valtype = array();

                                    $valtype = array(

                                        'product_id'=>$product_insert_id,

                                        'product_var_type_id' => $vartypeid[$i],

                                        'product_var_val_id' => $variation[$i],

                                        'deflag' => 0

                                    );

                                    $this->db->insert('product_variations', $valtype); 

                                

                            }

                            

                        }

                          /* Product Extra image starts*/
                    if(!empty($data['product_extra_images'])){
                        $this->db->where('product_id', $product_insert_id);
                        $this->db->delete('extra_images'); 
                        foreach($data['product_extra_images'] as $val){
                            $image_info = array(
                                'product_id'=>$product_insert_id,
                                'product_images'=>$val,
                                'created_datetime'=>date('Y-m-d H:i:s')
                                );
                             $this->db->insert('extra_images', $image_info); 
                        }
                    }
                    
                /* Product Extra image end*/

                  /* Product Inventory */

                    if(!empty($data['product_stock']['size']) && $product_insert_id>0){ 
                       $this->db->where('product_id', $product_insert_id);
                       $this->db->delete('product_inventory'); 

                        if(!empty($data['product_stock']['size'])){  $o = 0;  
                            foreach($data['product_stock']['size'] as $val){ 
                                $size = $data['product_stock']['size'][$o]; 
                                $qty = $data['product_stock']['stock'][$o]; 
                                $product_sku = $data['product_stock']['product_sku'][$o]; 

                                $size_id = $this->get_size_id($size);
                                if($size_id>0 && $product_insert_id>0){
                                    $data1 = array(
                                        'product_id'=>trim($product_insert_id),
                                        'size_id'=>$size_id,
                                        'stock_count'=>$qty,
                                        'product_sku'=>$product_sku,
                                    ); 
                                    $this->db->insert('product_inventory', $data1); 
                                   
                                }
                             $o++;
                            }
                            
                        }
                }
                /* Product Inventory end*/

            }
        } 
    }

    function update_product($data){
        if(!empty($data)){
            if($data['product_id'] > 0){
               $product_id = $data['product_id']; 
                if(!empty($data['product_desc'])){
                    $this->db->where('id', $product_id);
                    $this->db->update('product_desc', $data['product_udesc']); 
                }

                /* Product Inventory starts*/
                if(!empty($data['product_stock']['size'])){
                       $this->db->where('product_id', $product_id);
                       $this->db->delete('product_inventory'); 

                        if(!empty($data['product_stock']['size'])){  $o = 0;  
                            foreach($data['product_stock']['size'] as $val){ 
                                $size = $data['product_stock']['size'][$o]; 
                                $qty = $data['product_stock']['stock'][$o]; 
                                $product_sku = $data['product_stock']['product_sku'][$o]; 

                                $size_id = $this->get_size_id($size);
                                if($size_id>0 && $product_id>0){
                                    $data1 = array(
                                        'product_id'=>$product_id,
                                        'size_id'=>$size_id,
                                        'stock_count'=>$qty,
                                        'product_sku'=>$product_sku
                                    );
                                    $this->db->insert('product_inventory', $data1); 
                                }
                             $o++;
                            }
                            
                        }
                }
                /* Product Inventory end*/

                /* Product Extra image starts*/
                  /*  if(!empty($data['product_extra_images'])){
                        $this->db->where('product_id', $product_id);
                        $this->db->delete('extra_images'); 
                        foreach($data['product_extra_images'] as $val){
                            $image_info = array(
                                'product_id'=>$product_id,
                                'product_images'=>$val,
                                'created_datetime'=>date('Y-m-d H:i:s')
                                );
                             $this->db->insert('extra_images', $image_info); 
                        }
                    }
                    */
                /* Product Extra image end*/
            }
        }
    }


      function get_size_id($size){
        if($size!=''){
            $query = $this->db->query("select count(*) as cnt,id from product_size where size_text = '".strtoupper($size)."'");
            $result = $query->result_array();
            if($result[0]['cnt'] == 0){
                $data = array(
                    'size_text' => strtoupper($size) ,
                );
                $this->db->insert('product_size', $data); 
                return $this->db->insert_id();
            }else {
                return $result[0]['id']; 
            }
        }
    }

      function get_tag_id($tag){
        if($tag!=''){
            $query = $this->db->query("select count(*) as cnt,id from tag where name = '".strtoupper(trim($tag))."'");
            $result = $query->result_array();
            if($result[0]['cnt'] == 0){
                $data = array(
                    'name' => strtolower(trim($tag)) ,
                    'status' => 1 ,
                    'is_delete' => 0 ,
                    'created_datetime' => date('Y-m-d H:i:s') ,
                    'slug' => str_replace(' ','-',strtolower($tag)).'-'.time().mt_rand(),
                );
                $this->db->insert('tag', $data); 
                return $this->db->insert_id();
            }else {
                return $result[0]['id']; 
            }
        }
    }

    function add_tag_to_product($tag_id,$product_insert_id,$brand_id){
        if($tag_id > 0 && $product_insert_id>0){
            $tags = array();
            $tags = array(
                        'product_id'=>$product_insert_id,
                        'tag_id' => $tag_id,
                        'status' => 1,
                        'created_by' =>$brand_id,
                        'created_datetime' => date('Y-m-d H:i:s')
                    );
            $this->db->insert('product_tag', $tags); 
        }
    }

    function get_store_id($store){
        $query = $this->db->query("select user_id from brand_info where UPPER(company_name) = '".strtoupper($store)."'");
        $result = $query->result_array();
        if(!empty($result)){
            return $result[0]['user_id'];
        }else{
            return 0;
        }
    }

    function get_store_id_name($store){
        $query = $this->db->query("select id from user_login where UPPER(user_name) = '".strtoupper($store)."'");
        $result = $query->result_array();
        if(!empty($result)){
            return $result[0]['id'];
        }else{
            return 0;
        }
    }

    function get_brand_avail_id($brand){
        $query = $this->db->query("select id from brand_store where UPPER(brand_store_name) = '".strtoupper($brand)."'");
        $result = $query->result_array();
        if(!empty($result)){
            return $result[0]['id'];
        }else{
            return 0;
        }
    }

    function add_brands_to_product($store_id,$product_id){
        if($product_id>0 && $store_id >0){
            $brands_to_prod = array();
            $brands_to_prod = array(
                        'product_id'=>$product_id,
                        'store_id' => $store_id,
                        'status' => 0
                        
                    );
            $this->db->insert('product_store', $brands_to_prod); 
           
        }
        
    }

    function get_pricerangeID($price){
        if($price > 50000){ return 4670; }
        else if($price < 50000 && $price > 15000){ return 38; }
        else if($price < 15000 && $price > 5000){ return 37; }
        else { return 36; }

    }

    function get_product_id_by_sku($product_sku){

        //$query = $this->db->query("select id from product_desc where `product_color`='".$product_color."' AND `sku_number`='".$product_sku."'");
        $query = $this->db->query("select id from product_desc where `sku_number`='".$product_sku."'");
        $result = $query->result_array();
        if(!empty($result)){
            return $result[0]['id'];
        }else{
            return 0;
        }
    }
}

?>