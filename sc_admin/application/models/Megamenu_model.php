<?php
class Megamenu_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
    }
	
    function get_variationvalue($id,$cat_id){
 		$query = $this->db->query("SELECT a.`id`, a.`product_variation_type_id`, a.`name`, a.`slug`, a.`status`, a.`is_delete`, a.`created_by`, a.`modified_by`, a.`created_datetime`, a.`modified_datetime` FROM `product_variation_value` as a,`product_variation_type` as b,product_sub_category as c WHERE  a.`is_delete` = 0 and a.`status` = 1 AND c.`product_cat_id` = $cat_id AND c.`id` = b.`prod_sub_cat_id` AND a.`product_variation_type_id` = b.`id` ");
 		$res = $query->result_array();		
		return $res;
 	}
    
	function get_all_variationtype($id,$cat_id){

 		// $query = $this->db->query("SELECT `id`, `prod_sub_cat_id`, `name`, `slug`, `status`, `is_delete`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `product_variation_type` WHERE 1 and `is_delete` = 0 and `status` = 1");		
		$query = $this->db->query("SELECT DISTINCT a.`id`, a.`prod_sub_cat_id`, a.`name`, a.`slug`, a.`status`, a.`is_delete`, a.`created_by`, a.`modified_by`, a.`created_datetime`, a.`modified_datetime` FROM `product_variation_type` as a,product_sub_category as c WHERE 1 and a.`is_delete` = 0 and a.`status` = 1 AND c.product_cat_id = $cat_id AND c.id = a.prod_sub_cat_id ");	
		$res = $query->result_array();		
		return $res;

 	}

    function saveMegamenuHtml($mmHtml,$option)
    {
        $this->db->where('option_name', $option);
        $this->db->delete('sc_options');         
        $this->db->insert('sc_options', array('option_name'=>$option,'option_value'=>$mmHtml,'created_by'=>$this->session->userdata('user_id')));
        $c_id = $this->db->insert_id(); 
        return TRUE;
    }

    function get_megamenu_women_html()
    {
        $megamenu =array();
        $res =  $this->db->get_where('sc_options', array('option_name' => 'mega_menu_women_html'));        
        $result_megamenu = $res->result_array();  
        if(!empty($result_megamenu))
        {
          $megamenu = $result_megamenu[0]['option_value'];    
        }  
        return $megamenu;
    }

     function get_megamenu_men_html()
    {
        $megamenu =array();
        $res =  $this->db->get_where('sc_options', array('option_name' => 'mega_menu_men_html'));        
        $result_megamenu = $res->result_array();  
        if(!empty($result_megamenu))
        {
          $megamenu = $result_megamenu[0]['option_value'];    
        }  
        return $megamenu;
    }

    function save_MMcategory($mmcategory,$option)
    {
        $error_msg = array(); $final_cat = array();                
        $final_cat = serialize($mmcategory);
        $option_name = $option;   
       
        $result = $this->add_sc_options($option_name,$final_cat);
        if($result)
        {
            $error_msg = "Save"; 
        }else
        {
            $error_msg = "Error"; 
        }
              
        return json_encode($error_msg);
    }
	
	function get_category_chailds_parents($category_slug){

 		$query = $this->db->query("select id from category where category_slug = '$category_slug' limit 1");		
		$result = $query->result_array();		
		$id=$result[0]['id'];
		$slug_url = '/'.$category_slug.'-'.$id;
		return $slug_url;

 	}

}
?>