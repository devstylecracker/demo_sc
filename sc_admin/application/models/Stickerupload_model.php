<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stickerupload_model extends MY_Model {
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	public function street_pic($img_name,$user_id)
	{	
		$data = array('img_name'=>$img_name,'created_by'=>$user_id,'created_datetime'=>date('Y-m-d H:i:s'));
		$this->db->insert('sticker', $data);
		return true;	
	}
	
	public function get_street_pic($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL)
	{	
		
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}		
		$query = $this->db->query('select * from sticker where status=1 order by id desc '.$limit_cond);
		return $query->result_array();
		
	}
	
	public function delete_street_pic($id,$user_id){
		$data = array(
               'status' => 0,
               'modified_by' =>$user_id
            );

		$this->db->where('id', $id);
		$this->db->update('sticker', $data); 
		return true;
		}
	
	
		
/*---------End of Common functionalities-------*/  
}

