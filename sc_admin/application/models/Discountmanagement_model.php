<?php
class Discountmanagement_model extends MY_Model {    
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    function get_paid_brands(){
       if($this->session->userdata('role_id') == 6){
          $query = $this->db->order_by('company_name', 'ASC')->get_where('brand_info', array('user_id' => $this->session->userdata('user_id')));
        }else{
          $query = $this->db->order_by('company_name', 'ASC')->get_where('brand_info', array('is_paid' => 1));
        }
       $res = $query->result_array();
       return $res;
    }

    function get_discount($brand_id){
      
      $query = $this->db->query("select id,name,(Select GROUP_CONCAT(pi.product_sku) from `product_inventory` as pi where pi.`product_id`= product_desc.id) as product_sku,price,compare_price,discount_start_date,discount_end_date,(SELECT `discount_percent` FROM `discount_availed` WHERE product_id = product_desc.id) as discount_percent,(SELECT `start_date` FROM `discount_availed` WHERE product_id = product_desc.id) as start_date,(SELECT `end_date` FROM `discount_availed` WHERE product_id = product_desc.id) as end_date from product_desc where brand_id = ".$brand_id." and status = 1 ");
      $res = $query->result_array();
      return $res;

    }

    function upload_discount($product_data){
      $i = 0; $k = 0;
      if(!empty($product_data)){
        if($product_data['product_id']>0 && $product_data['price']>=0 && $product_data['comp_price']>=0 ){
          $query = $this->db->query("update product_desc set price = '".$product_data['comp_price']."', compare_price = '".$product_data['price']."', discount_start_date = '".$product_data['brand_start_datetime']."' ,discount_end_date = '".$product_data['brand_end_datetime']."' where id = '".$product_data['product_id']."'");
          $i++;
        }
        if($this->session->userdata('role_id') != 6){
        if($product_data['product_id']>0 && $product_data['percentage']<=100 ){

          $query = $this->db->query('select id from discount_availed where product_id ="'.$product_data['product_id'].'"');
          $res = $query->result_array();

          if(empty($res)){
            $data = array();
            $data = array('discount_type_id'=>2,'brand_id'=>0,'product_id'=>$product_data['product_id'],'discount_percent'=>$product_data['percentage'],'discount_price'=>0,'max_amount'=>0,'referal_code'=>'','start_date'=>$product_data['sc_start_datetime'],'end_date'=>$product_data['sc_end_datetime'],'status'=>1,'is_delete'=>0,'created_datetime'=>date('Y-m-d H:i:s'),'created_by'=>$this->session->userdata('user_id'),'modified_by'=>$this->session->userdata('user_id'));
            $this->db->insert('discount_availed',$data);
            $k++;
          }else{
             $query = $this->db->query("update discount_availed set discount_percent='".$product_data['percentage']."', start_date='".$product_data['sc_start_datetime']."', end_date ='".$product_data['sc_end_datetime']."',modified_by = '".$this->session->userdata('user_id')."' where product_id = '".$product_data['product_id']."'");
             $k++;
          }

        }
      }
    }
    }
 } 
?>