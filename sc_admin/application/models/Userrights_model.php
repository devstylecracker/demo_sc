<?php
class Userrights_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
    }
    
    function has_rights($module_id){
		if($this->session->userdata('user_id')){
			$user_id = $this->session->userdata('user_id');
			
			$query = $this->db->get_where('user_module_permission', array('user_id' => $user_id,'module_id'=>$module_id));
			$result = $query->result();
			if(!empty($result)){ return true; }else{ return false; }
		}else{
			return false;
		}
	}
	
	function pass_permission(){
		if($this->session->userdata('user_id')){
			$user_id = $this->session->userdata('user_id');
			$query = $this->db->get_where('user_module_permission', array('user_id' => $user_id));
			$result = $query->result();
			return $result;
			
		}
	}

	
	function user_pass_permission($user_id){
		if($user_id){
			$query = $this->db->get_where('user_module_permission', array('user_id' => $user_id));
			$result = $query->result();
			return $result;
			
		}
	}
 } 
?>
