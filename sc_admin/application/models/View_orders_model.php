<?php
class View_orders_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function getOrders($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL){   

        if($search_by == '1'){ $search_cond = " AND a.`id` = '".strip_tags(trim($table_search))."'"; }
		else if($search_by == '2'){ $search_cond = " AND a.`first_name` like '%".strip_tags(trim($table_search))."%'"; }
			else if($search_by == '3'){ $search_cond = " AND b.`company_name` like '%".strip_tags(trim($table_search))."%'"; }
			else if($search_by == '4'){ $search_cond = " AND a.`created_datetime` like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '5'){ 

				$tb_status=strtolower(strip_tags(trim($table_search)));
				if($tb_status=="processing")
				{
					$apr_status = 1;

				}else if($tb_status == "confirmed")
				{					
					$apr_status = 2;
				}else if($tb_status == "dispatched")
				{
					$apr_status = 3;

				}else if($tb_status == "delivered")
				{
					$apr_status = 4;

				}else if($tb_status == "cancelled")
				{
					$apr_status = 5;

				}else if($tb_status == "return")
				{
					$apr_status = 6;

				}else if($tb_status == "fake")
				{
					$apr_status = 7;
				}
				else
				{
					$apr_status = 0;
				}

				if($apr_status!=0)
				{
					$search_cond = " AND d.`order_status` = '".$apr_status."'";
				}
				
			 }/*else if($search_by == '6'){ $search_cond = " AND a.id ='".strip_tags(trim($table_search))."'"; }
				else if($search_by == '7'){ $search_cond = " AND d.company_name like '%".strip_tags(trim($table_search))."%'"; }	*/
				else if($search_by == '8'){

					$date_from = $this->input->post('date_from');
					$date_to = $this->input->post('date_to');
					if($date_from=='' && $date_to == '' )
					{
						$tb_arr = explode(',',$table_search);
						$date_from = $tb_arr[0];
						$date_to = $tb_arr[1];
					}

					$search_cond = " AND a.`created_datetime` >= '".$date_from." 00:00:00'  and a.`created_datetime` <= '".$date_to." 23:59:59'";

				}			
				else{ $search_cond ='AND 1=1'; }
				

				//$search_cond="";
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}

		if($this->session->userdata('role_id') == 6 ){ 
			$search_cond = $search_cond." and a.`brand_id`=".$this->session->userdata('user_id');
		}
      	
      	$query = $this->db->query("select (select product_sku from `product_inventory` where  `product_id` = d.`product_id` and `size_id`=d.`product_size`  limit 0,1) as product_sku, (select look_id as look from cart_info where order_id=d.`order_id` and product_id = d.`product_id` and product_size=f.id) as look_id,(select platform_info as look from cart_info where order_id=d.`order_id` and product_id = d.`product_id` and product_size=f.id) as platform_info,(Select sum(h.product_price) from order_info as g,order_product_info as h where g.id = h.order_id and g.order_unique_no = a.order_unique_no group by g.order_unique_no ) as OrderProductTotal,
      		(Select sum(i.product_price) from order_product_info as i  group by i.order_id having i.order_id = d.order_id ) as OrderIdProductSum,f.size_text,a.`id` as order_id,d.id as order_prd_id, a.`user_id`, a.`brand_id`, a.`order_tax_amount`, a.`cod_amount`, a.`order_sub_total`, a.`order_total`,d.product_id,d.send_email, a.`pay_mode`, a.`created_datetime`, d.`modified_datetime`, a.`order_status`, a.`first_name`, a.`last_name`, a.`email_id`, a.`mobile_no`, a.`shipping_address`, a.`pincode`, a.`city_name`, a.`state_name`,c.`state_name`,b.`company_name`, d.`product_id`, d.`product_qty`,d.`product_size`,e.`name` as product_name, d.`product_price`,a.`order_unique_no`, d.`order_status`,d.`discount_percent`,e.image,d.coupon_code from order_info as a,brand_info as b,states as c, order_product_info as d, product_desc as e,product_size as f where  a.`brand_id` = b.`user_id` and a.`state_name` = c.`id` and d.`order_id` = a.`id` and e.`id` = d.`product_id` and d.`product_size` =f.id  and a.`order_status`!=9 ".$search_cond."  order by a.`created_datetime` desc, a.`id` desc  ".$limit_cond);
      	/*echo $this->db->last_query();*/
        $result = $query->result_array();
        return $result;
    }

    public function updateOrder($orderPrdId,$order_status)
	{	$send_email = $order_status;
		if($order_status == 5) $send_email = 0;
		$this->db->where('id', $orderPrdId);
		$result = $this->db->update('order_product_info', array('order_status'=>$order_status)); 
		
		if($result)
		{
			return true;
		}else
		{
			return false;
		}

	}

	public function order_status($order_status)
	{
		$query = $this->db->query("SELECT a.user_id,b.order_id,a.first_name,a.last_name,b.order_status,b.product_id,c.name,(b.`product_qty`*c.price) as price,a.created_datetime  FROM `order_info` a,order_product_info b,product_desc c where b.order_status = $order_status AND b.order_id = a.id AND b.product_id = c.id ");
		$result = $query->result_array();
		// echo "<pre>";
		// print_r($result);exit;
        return $result;
	}

	function getOrdersPay($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL){   

    	$search_cond = '';
    	$search_cond_have = '';$result = array();
        if($search_by == '1'){ $search_cond = " AND a.`id` = '".strip_tags(trim($table_search))."'"; }
		else if($search_by == '2'){ $search_cond = " AND a.`first_name` like '%".strip_tags(trim($table_search))."%'"; }
			else if($search_by == '3'){ $search_cond = " AND b.`company_name` like '%".strip_tags(trim($table_search))."%'"; }
			else if($search_by == '4'){ $search_cond = " AND a.`created_datetime` like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '5'){ 

				$tb_status1=strtolower(strip_tags(trim($table_search)));
				if($tb_status1 == "processing")
				{
					$apr_status = 1;

				}else if($tb_status1 == "confirmed")
				{					
					$apr_status = 2;
				}else if($tb_status1 == "curated")
				{					
					$apr_status = 8;
				}else if($tb_status1 == "dispatched")
				{
					$apr_status = 3;

				}else if($tb_status1 == "delivered")
				{
					$apr_status = 4;

				}else if($tb_status1 == "cancelled")
				{
					$apr_status = 5;

				}else if($tb_status1 == "return")
				{
					$apr_status = 6;
					
				}else if($tb_status1 == "fake")
				{
					$apr_status = 7;
				}
				else
				{
					$apr_status = '';
				}

				if($tb_status1!='')
				{
					$search_cond = " AND d.`order_status` = '".$apr_status."'";
				}
				
			 }else if($search_by == '6'){ $search_cond_have = "having daysLapsed ".trim($table_search)." "; }
				else if($search_by == '7'){ 
					//echo $table_search;
					$pay_status ="";
				$tb_status2=strtolower(strip_tags(trim($table_search)));

				if($tb_status2=="create invoice" )
				{
					$pay_status = 1;

				}else if($tb_status2 == "cleared" )
				{					
					$pay_status = 2;

				}else if($tb_status2=="pending" )
				{
					$pay_status = 3;
				}
				else
				{
					$pay_status = "";
				}
				
				if($pay_status!="")
				{
					$search_cond = " AND d.payment_status=".$pay_status." "; 
				}else 
				{
					$search_cond = '';
				}		
									
				
				}else if($search_by == '8'){

					$date_from = $this->input->post('date_from');
					$date_to = $this->input->post('date_to');
					
					if($date_from=='' && $date_to == '' )
					{
						$tb_arr = explode(',',$table_search);
						$date_from = $tb_arr[0];
						$date_to = $tb_arr[1];
					}

					$search_cond = "AND a.`created_datetime` >= '".$date_from." 00:00:00'  and a.`created_datetime` <= '".$date_to." 23:59:59'";

				}else{ $search_cond ='';
					  $search_cond_have = "";
					}
				

				//$search_cond="";
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}

		if($this->session->userdata('role_id') == 6 ){ 
			$search_cond = $search_cond." and a.`brand_id`=".$this->session->userdata('user_id');
		}
		
		$query_sql = "select distinct a.order_unique_no, @type:='0' as look_id,(select platform_info as look from cart_info where order_id=d.`order_id` and product_id = d.`product_id` and product_size=f.id) as platform_info, f.size_text,a.`id` as order_id,d.id as order_prd_id, a.`user_id`, a.`brand_id`, a.`order_tax_amount`, a.`cod_amount`, a.`shipping_amount`, a.`order_sub_total`, a.`order_total`, a.`pay_mode`,a.`mihpayid` as tranid, a.`created_datetime`, d.`modified_datetime`, a.`order_status`, a.`first_name`, a.`last_name`, a.`email_id`, a.`mobile_no`, a.`shipping_address`, a.`pincode`, a.`city_name`, a.`state_name`,c.`state_name`,b.`company_name`,b.`brand_code`, d.`product_id`, d.`product_qty`,d.`product_size`,e.`name` as product_name,e.image, d.`product_price`, (select (sum(oif.order_total)-sum(oif.`order_tax_amount`)-sum(oif.`cod_amount`)-sum(oif.`shipping_amount`)) from order_info oif where oif.order_unique_no = a.order_unique_no) total_order_amt, a.order_unique_no, d.`order_status`, DATEDIFF(CURDATE(),DATE(a.`created_datetime`)) as daysLapsed, payment_status, g.cpa_percentage as commission_rate, b.payment_gateway_charges, b.service_tax, b.integration_charges, b.cpa_percentage, g.commission_cat, (select cs.coupon_amount from coupon_settings cs where a.coupon_code = cs.coupon_code limit 1) coupon_amount, (select cs.discount_type from coupon_settings cs where a.coupon_code = cs.coupon_code limit 1) discount_type, (select cs.coupon_code from coupon_settings cs where a.coupon_code = cs.coupon_code limit 1) coupon_code,iod.points,d.compare_price
			 from order_info as a
			 left join order_product_info as d on d.`order_id` = a.`id`
			 left join referral_info as iod on iod.`invite_order_id` = a.`order_unique_no`
			 left join brand_info as b on a.`brand_id` = b.`user_id`
			 left join states as c on a.`state_name` = c.`id`
			 left join product_desc as e on e.`id` = d.`product_id`
			 left join product_size as f on d.`product_size` = f.id
			 left join brand_store as g on g.`brand_id` = b.user_id 
			 left join product_store as h on h.`store_id` = g.`id` 
			 where 1 and  a.`order_status`!=9 and  d.`order_status`!=7 AND h.`product_id` = e.`id` ".$search_cond." ".$search_cond_have." order by a.`created_datetime` desc ".$limit_cond;
         $query = $this->db->query($query_sql);
        
        $result = $query->result_array();
        return $result;
    }

    public function updatepayOrder($orderPrdId,$orderpay_status)
	{
		$this->db->where('id', $orderPrdId);
		$result = $this->db->update('order_product_info', array('payment_status'=>$orderpay_status)); 
		
		if($result)
		{
			return true;
		}else
		{
			return false;
		}
	}
	
	public function send_mail($order_id)
	{
		 $query = $this->db->query("select order_id,product_id,user_id,cart_id,product_price,brand_id,order_tax_amount,cod_amount from order_product_info as a,order_info as b where a.order_id = $order_id ");
        $result = $query->result_array();
        if(!empty($result)) return $result; else return null;
	}
	
	public function get_order_unique_no($order_id){
		$query = $this->db->query("select b.order_unique_no,b.user_id,b.first_name,b.last_name from order_product_info as a,order_info as b where a.order_id = $order_id AND a.order_id = b.id AND a.user_id = b.user_id");
        $result = $query->result_array();
        if(!empty($result)) return $result; else return null;
	}
	
	function su_get_order_info($order_id,$product_order_id,$product_id,$order_status=null){
        $query = $this->db->query('select id,user_id from `order_info` where order_unique_no = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

            $order_ids = implode(",",$order_info);

            $query = $this->db->query('select a.`order_id`, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`, a.`product_price`,b.`name`,b.`image`,b.`brand_id`,c.size_text,d.company_name from order_product_info as a,product_desc as b,product_size as c,`brand_info` as d where a.`product_id` = b.`id` and a.`order_id` in ('.$product_order_id.') and a.`user_id`='.$user_id.' AND a.`product_id` = '.$product_id.' and a.product_size = c.`id` and d.`user_id` = b.`brand_id` AND a.order_status = '.$order_status.' ' );

            $query_res = $query->result_array();
			// echo $this->db->last_query();exit;
            return $query_res;

        }
    }
	
	function su_get_order_price_info($order_id,$product_order_id){
        $query = $this->db->query('select id,user_id from `order_info` where order_unique_no = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info);

        $query = $this->db->query('select sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no, a.shipping_address,a.pincode,a.city_name,s.state_name,a.pay_mode,a.country_name,c.product_price from order_info as a,states as s,order_product_info as c where a.`state_name` = s.`id` AND a.id = '.$product_order_id.' and a.id in ('.$order_ids.') AND a.id = c.order_id ' );

            $query_res = $query->result_array();
            return $query_res;
        }
    }
	
	function get_user_info($user_id){
        $query = $this->db->get_where('user_login', array('id' => $user_id));
        return $query->result_array();
    }
	
	function get_order_count($product_order_id){
        $query = $this->db->query("select * from order_product_info where order_id = $product_order_id");
        $query_res = $query->result_array();
        return $query_res;
    }
	
	function update_email($order_id,$product_id,$send_email=null){
	   if($send_email == 5) $send_email = 1;
		
       if($order_id!='' && $product_id!=''){
        
            $data = array(
               'product_id' => $product_id ,
               'order_id' => $order_id,
			   'send_email' => $send_email
            );
			 $this->db->where(array('product_id'=>$product_id,'order_id' => $order_id));
            $this->db->update('order_product_info', $data);
       
		}
	}

	function getViewOrdersExcel($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL){   

    	$search_cond = '';
    	$search_cond_have = '';
        if($search_by == '1'){ $search_cond = " AND a.`id` = '".strip_tags(trim($table_search))."'"; }
		else if($search_by == '2'){ $search_cond = " AND a.`first_name` like '%".strip_tags(trim($table_search))."%'"; }
			else if($search_by == '3'){ $search_cond = " AND b.`company_name` like '%".strip_tags(trim($table_search))."%'"; }
			else if($search_by == '4'){ $search_cond = " AND a.`created_datetime` like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '5'){ 

				$tb_status1=strtolower(strip_tags(trim($table_search)));
				if($tb_status1 == "processing")
				{
					$apr_status = 1;

				}else if($tb_status1 == "confirmed")
				{					
					$apr_status = 2;
				}else if($tb_status1 == "dispatched")
				{
					$apr_status = 3;

				}else if($tb_status1 == "delivered")
				{
					$apr_status = 4;

				}else if($tb_status1 == "cancelled")
				{
					$apr_status = 5;

				}else if($tb_status1 == "return")
				{
					$apr_status = 6;
				}
				else if($tb_status1 == "fake")
				{
					$apr_status = 7;
				}
				else
				{
					$apr_status = '';
				}

				if($tb_status1!='')
				{
					$search_cond = " AND d.`order_status` = '".$apr_status."'";
				}
				
			 }else if($search_by == '6'){ $search_cond_have = "having daysLapsed ".trim($table_search)." "; }
				else if($search_by == '7'){ 
					//echo $table_search;
					$pay_status ="";
				$tb_status2=strtolower(strip_tags(trim($table_search)));

				if($tb_status2=="create invoice" )
				{
					$pay_status = 1;

				}else if($tb_status2 == "cleared" )
				{					
					$pay_status = 2;

				}else if($tb_status2=="pending" )
				{
					$pay_status = 3;
				}
				else
				{
					$pay_status = "";
				}
				
				if($pay_status!="")
				{
					$search_cond = " AND d.payment_status=".$pay_status." "; 
				}else 
				{
					$search_cond = '';
				}		
									
				
				}else if($search_by == '8'){

					$date_from = $this->input->post('date_from');
					$date_to = $this->input->post('date_to');
					if($date_from=='' && $date_to == '' )
					{
						$tb_arr = explode(',',$table_search);
						$date_from = $tb_arr[0];
						$date_to = $tb_arr[1];
					}

					$search_cond = " AND a.`created_datetime` >= '".$date_from." 00:00:00'  and a.`created_datetime` <= '".$date_to." 23:59:59'";

				}else{ $search_cond ='';
					  $search_cond_have = "";
					}
				

				//$search_cond="";
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}

		if($this->session->userdata('role_id') == 6 ){ 
			$search_cond = $search_cond." and a.`brand_id`=".$this->session->userdata('user_id');
		}

		
         $query = $this->db->query("select e.id,(select product_sku from `product_inventory` where  `product_id` = d.`product_id` and `size_id`=d.`product_size` limit 0,1 ) as product_sku,(select name from `product_category` where id =e.product_cat_id) as product_cat_id,(select name from `product_sub_category` where id =e.product_sub_cat_id) as product_sub_cat_id,(select look_id as look from cart_info where order_id=d.`order_id` and product_id = d.`product_id` and product_size=f.id) as look_id,(select platform_info as look from cart_info where order_id=d.`order_id` and product_id = d.`product_id` and product_size=f.id) as platform_info ,(Select sum(h.product_price) from order_info as g,order_product_info as h where g.id = h.order_id and g.order_unique_no = a.order_unique_no group by g.order_unique_no ) as OrderProductTotal,
      		(Select sum(i.product_price) from order_product_info as i  group by i.order_id having i.order_id = d.order_id ) as OrderIdProductSum, f.size_text,a.`id` as order_id,d.id as order_prd_id, a.`user_id`, a.`brand_id`, a.`order_tax_amount`, a.`cod_amount`, a.`shipping_amount`, a.`order_sub_total`, a.`order_total`, a.`pay_mode`,a.`mihpayid` as tranid, a.`created_datetime`, a.`modified_datetime`, a.`order_status`, a.`first_name`, a.`last_name`, a.`email_id`, a.`mobile_no`, a.`shipping_address`, a.`pincode`, a.`city_name`, a.`state_name`,c.`state_name`,b.`company_name`,b.`brand_code`, d.`product_id`, d.`product_qty`,d.`product_size`,e.`name` as product_name,e.image, d.`product_price`, d.`order_status`, DATEDIFF(CURDATE(),DATE(a.`created_datetime`))  as daysLapsed, payment_status, b.cpa_percentage as commission_rate,d.coupon_code,a.order_unique_no from order_info as a,brand_info as b,states as c, order_product_info as d, product_desc as e,product_size as f where a.`brand_id` = b.`user_id` and a.`state_name` = c.`id` and d.`order_id` = a.`id` and e.`id` = d.`product_id` and d.`product_size` =f.id and a.`order_status`!=9".$search_cond." ".$search_cond_have." order by a.`created_datetime` desc ".$limit_cond);
      	/*echo $this->db->last_query();exit;*/
        $result = $query->result_array();
        return $result;
    }

    function product_variations($product_id){
    	$query = $this->db->query("select b.name from product_variations as a,`product_variation_type` as b where a.	product_var_type_id = b.`id` and a.`product_id`=".$product_id);
    	$result = $query->result_array();
    	return $result;
    }

    function product_variations_value($product_id){
    	$query = $this->db->query("select b.name from product_variations as a,`product_variation_value` as b where a.	product_var_val_id = b.`id` and a.`product_id`=".$product_id);
    	$result = $query->result_array();
    	return $result;
    }

     function getUserCoupon($couponCode,$orderDate, $CbrandId = "")
    {

      if($CbrandId!='')
      {
       /* $brand_cond =' AND `brand_id`='.$CbrandId;*/
       $brand_cond =' AND (brand_id = "'.$CbrandId.'" || brand_id=0) ';
      }else
      {
        $brand_cond = '';
      }
        $query = $this->db->query("SELECT `coupon_code`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `brand_id`, `products`, `exclude_products`,`coupon_used_count`,`usage_limit_per_coupon`,`stylecrackerDiscount`,`individual_use_only`,`coupon_email_applied` FROM `coupon_settings` WHERE '".$orderDate."' BETWEEN `coupon_start_datetime` AND `coupon_end_datetime` AND`status`=1 AND `is_delete`=0  AND `coupon_code`='".$couponCode."' ".$brand_cond."; "); 
            //return $this->db->last_query();
            $result = $query->result_array();
           
            if(!empty($result))
            {
              /* $coupon_info['coupon_products'] =  $coupon_products.$result[0]['products'];*/
               $coupon_info['coupon_amount'] = $result[0]['coupon_amount']; 
               $coupon_info['brand_id'] = $result[0]['brand_id']; 
               $coupon_info['coupon_products'] = $result[0]['products'];
               $coupon_info['coupon_min_spend'] = $result[0]['min_spend'];
               $coupon_info['coupon_max_spend'] = $result[0]['max_spend'];
               $coupon_info['coupon_discount_type'] = $result[0]['discount_type'];
               $coupon_info['coupon_used_count'] = $result[0]['coupon_used_count'];
               $coupon_info['usage_limit_per_coupon'] = $result[0]['usage_limit_per_coupon'];
               $coupon_info['stylecrackerDiscount'] = $result[0]['stylecrackerDiscount'];
               $coupon_info['individual_use_only'] = $result[0]['individual_use_only'];
               $coupon_info['coupon_email_applied'] = $result[0]['coupon_email_applied'];
            }  

            if(!empty($coupon_info))
            {
              return $coupon_info;
            }else
            {
                return 0;
            }               
    }

     function calculateCouponAmount($cartproduct=array())
  	{   
      if(!empty($cartproduct))
      {
        
        foreach($cartproduct as $val)
        {

          /*if(@$val['discount_percent']!='' || @$val['discount_percent']!=0)
          {
             $price = $val['discount_price'];            
          }else
          {
             $price = $val['product_price'];            
          }*/
          $price = $val['product_price']; 

          if(isset($val['coupon_code']) && $val['coupon_code']!='')
          {
            $coupon_info = $this->getUserCoupon($val['coupon_code'],$val['created_datetime'],$val['brand_id']);            
            $coupon_brand = $coupon_info['brand_id'];
            $coupon_products = $coupon_info['coupon_products'];
            if($coupon_products!='' || $coupon_products!=0)
            {
              $coupon_products_arr =explode(',',$coupon_products);
            }
            $coupon_min_spend = $coupon_info['coupon_min_spend'];
            $coupon_max_spend = $coupon_info['coupon_max_spend'];
            $coupon_discount_type = $coupon_info['coupon_discount_type']; 
            $coupon_stylecracker =  $coupon_info['stylecrackerDiscount'];
            $individual_use_only =  $coupon_info['individual_use_only'];   
            $data['coupon_code'] = $val['coupon_code'];
            $apply_email =  $coupon_info['coupon_email_applied'];
            $coupon_product_price = 0;

             if($coupon_brand==$val['brand_id'] && $coupon_brand!=0 )
              {        
                if($coupon_products!=''&& $coupon_products!=0)
                {
                   //coupon_discount_type =3 (Product discount)
                  if($coupon_discount_type==3)
                  {
                    if(in_array($val['product_id'], $coupon_products_arr))
                    {              
                      if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                      {                        
                        $data['coupon_discount'] = $coupon_info['coupon_amount'];  
                        $data['coupon_code'] = $val['coupon_code'];                   
                      } 
                    }
                  } 
                   //coupon_discount_type =4 (Product % discount)
                  if($coupon_discount_type==4)
                  {
                    if(in_array($val['product_id'], $coupon_products_arr))
                    {              
                      if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                      {
                        $coupon_percent = $coupon_info['coupon_amount'];
                        $data['coupon_discount'] = $price*($coupon_percent/100); 
                        $data['coupon_code'] = $val['coupon_code'];                      
                      } 
                    }
                  }                 

                }else
                { 
                   //coupon_discount_type =1 (cart discount)
                  if($coupon_discount_type==1)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $data['coupon_discount'] = $coupon_info['coupon_amount']; 
                      $data['coupon_code'] = $val['coupon_code'];                   
                    }
                  }
                   //coupon_discount_type =2 (cart % discount)
                  if($coupon_discount_type==2)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $coupon_percent = $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];                      
                    }
                  }
                }          
              }

            /* if($coupon_brand==0 && strtolower($coupon_code)=='sccart20')
	        { 
	            $coupon_product_price = $coupon_product_price+$price;
	            if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                {
                  $coupon_percent = $coupon_info['coupon_amount'];  
                  $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                  $data['coupon_code'] = $val['coupon_code'];                      
                }
	        } */

	        if(($coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only!=1  )|| ( $coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only==1 ))
	        { 
	            //coupon_discount_type =1 (cart discount)
                  if($coupon_discount_type==1)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $data['coupon_discount'] = $coupon_info['coupon_amount']; 
                      $data['coupon_code'] = $val['coupon_code'];                   
                    }
                  }
                   //coupon_discount_type =2 (cart % discount)
                  if($coupon_discount_type==2)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $coupon_percent = $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];                      
                    }
                  }
	        }                           

          }else
          {
            $data['coupon_discount']=0;
            $data['coupon_code'] = '';
          }         
        }      
      } 
      return $data;      
  	}

  	function multi_array_search($search_for, $search_in) {
	    foreach ($search_in as $element) {
	        if ( ($element === $search_for) || (is_array($element) && multi_array_search($search_for, $element)) ){
	            return true;
	        }
	    }
	    return false;
	}

	function calculate_product_discountAmt($product_price,$totalproductprice,$discountType,$discountAmt)
    { /*echo $product_price.'--'.$totalproductprice.'--'.$discountType.'--'.$discountAmt.'<br>';*/
      if($product_price!='' && $totalproductprice!=0 && $discountAmt!='')
      {  

        if($discountType==1)/*1-Cart-Discount*/
        {
            $percent = ($product_price*100)/$totalproductprice;
            $DiscountAmt = ($discountAmt*$percent)/100;            
            return $DiscountAmt;
          /*  $productDiscountAmt = $product_price-$DiscountAmt;         
            return $productDiscountAmt;*/
            

        }else if($discountType==2) /*2-Cart-%-Discount*/ 
        {     
            $percent = $discountAmt;  
            $DiscountAmt = ($product_price*$percent)/100;
            return $DiscountAmt;
            /*$productDiscountAmt = $product_price-$DiscountAmt;  
            return $productDiscountAmt;*/
            
        }   
       
      }else
      {
        return 0;
      }
    }

	
}

?>