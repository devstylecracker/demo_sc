<?php
class Homepage_settings_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function look_exist($look,$type){
    	$error_msg = array(); $final_array = array();
    	if(!empty($look)){
    		foreach($look as $val){
    			 $query = $this->db->get_where('look', array('status' => '1','id'=>$val));
    			 $res = $query->result_array();
    			 if(empty($res)){
    			 	$error_msg[] = "Look does not exist otherwise in pending state ";
    			 }else{
    			 	$error_msg[] = "";
    			 	$final_array[] = $val;
    			 }
    		}
    	}
    	if(sizeof($final_array) == 3){ 
    		$final_looks = serialize($final_array);
    		$option_name = 'top_looks_'.$type;
    		$this->add_sc_options($option_name,$final_looks);
    	}
    	return json_encode($error_msg);
    }

    function get_options($option_name){
    	return unserialize($this->get_sc_options($option_name));
    }

    function get_look_img($lookId){
    	if($lookId > 0){
		$query = $this->db->query('select z.id,z.new_img as product_img,z.look_type,z.image,d.img_name as look_image,name,z.created_datetime,z.description,z.status,z.deflag from 
(SELECT a.id,a.image,a.look_type,b.image as new_img,a.name,a.created_datetime,a.description,a.status,a.deflag FROM `look` as a left join `product_desc` as b on a.image = b.id and a.look_type=3 where 1 and a.deflag=0 and a.id = '.$lookId.') as z left join street_style_look_img as d on z.image = d.id and (z.look_type = 2 OR z.look_type = 4 OR z.look_type = 6) ');
			$res = $query->result_array();
			return $res;
		}else{
		return false;
		}
    }

    /*function get_category($cat_id,$preselected){
    	$cat_table_name = ''; $cathtml = '';
    	if($cat_id == 1){ $cat_table_name = 'product_category'; }
    	else if($cat_id == 2){ $cat_table_name = 'product_sub_category'; }
    	else if($cat_id == 3){ $cat_table_name = 'product_variation_type'; }
    	else if($cat_id == 4){ $cat_table_name = 'product_variation_value'; }

    	if($cat_table_name !=''){
    		$query = $this->db->get_where($cat_table_name, array('status' => '1'));
    		$res = $query->result_array();
    		if(!empty($res)){
    			foreach ($res as $val) {
    				if(is_array($preselected) == true && in_array($val['id'],$preselected)){
    					$cathtml.= '<option value="'.$val['id'].'" selected>'.$val['name'].'-'.$val['id'].'</option>';
    				}else if($preselected == $val['id']){
    					$cathtml.= '<option value="'.$val['id'].'" selected>'.$val['name'].'-'.$val['id'].'</option>';
    				}else{
    					$cathtml.= '<option value="'.$val['id'].'">'.$val['name'].'-'.$val['id'].'</option>';
    				}
    			}
    		}
    	}

    	return $cathtml;
    }*/

        function get_category($cat_id,$preselected){
        $cat_table_name = ''; $cathtml = '';
        if($cat_id == 1){ $cat_table_name = 'category'; }
        /*else if($cat_id == 2){ $cat_table_name = 'product_sub_category'; }
        else if($cat_id == 3){ $cat_table_name = 'product_variation_type'; }
        else if($cat_id == 4){ $cat_table_name = 'product_variation_value'; }*/

        if($cat_table_name !=''){
            $query = $this->db->get_where($cat_table_name);
            $res = $query->result_array();
            if(!empty($res)){
                foreach ($res as $val) {
                    if(is_array($preselected) == true && in_array($val['id'],$preselected)){
                        $cathtml.= '<option value="'.$val['id'].'" selected>'.$val['category_name'].'-'.$val['id'].'</option>';
                    }else if($preselected == $val['id']){
                        $cathtml.= '<option value="'.$val['id'].'" selected>'.$val['category_name'].'-'.$val['id'].'</option>';
                    }else{
                        $cathtml.= '<option value="'.$val['id'].'">'.$val['category_name'].'-'.$val['id'].'</option>';
                    }
                }
            }
        }

        return $cathtml;
    }

    function cat_save($cat,$type){
        $error_msg = array(); $final_cat = array();
        if(sizeof($cat) == 3){ 
            $final_cat = serialize($cat);
            $option_name = 'top_cat_'.$type;
            $this->add_sc_options($option_name,$final_cat);
            $error_msg[] = "";
        }else{
            $error_msg[] = "Error";
        }
        return json_encode($error_msg);
    }

    function save_view_cat($all_cat){
        $option_name = 'sc_view_all_cat';
        $this->add_sc_options($option_name,serialize($all_cat));
        return true;
    }
    /*function cat_save($cat,$type){
    	$error_msg = array(); $final_cat = array();
    	if(sizeof($cat) == 3){ 
    		$final_cat = serialize($cat);
    		$option_name = 'top_cat_'.$type;
    		$this->add_sc_options($option_name,$final_cat);
    		$error_msg[] = "";
    	}else{
    		$error_msg[] = "Error";
    	}
    	return json_encode($error_msg);
    }

    function save_view_cat($all_cat){
        print_r( $all_cat);
    	$option_name = 'sc_view_all_cat';
    	$this->add_sc_options($option_name,serialize($all_cat));
    	return true;
    }*/

    function get_paid_brands($preselected){
        $brand_table_name = ''; $brandhtml = '';
        $brand_table_name = 'brand_info';
        if($brand_table_name !=''){
            $query = $this->db->get_where($brand_table_name, array('is_paid' => '1'));
            $res = $query->result_array();
            if(!empty($res)){
                foreach ($res as $val) {
                    if(is_array($preselected) == true && in_array($val['user_id'],$preselected)){
                        $brandhtml.= '<option value="'.$val['user_id'].'" selected>'.$val['company_name'].'</option>';
                    }else if($preselected == $val['user_id']){
                        $brandhtml.= '<option value="'.$val['user_id'].'" selected>'.$val['company_name'].'</option>';
                    }else{
                        $brandhtml.= '<option value="'.$val['user_id'].'">'.$val['company_name'].'</option>';
                    }
                }
            }
        }

        return $brandhtml;
    }

    function save_trending_brands($cat,$type){
        $error_msg = array(); $final_cat = array();
        if(sizeof($cat) == 3){ 
            $final_cat = serialize($cat);
            $option_name = 'trending_brand_'.$type;
            $this->add_sc_options($option_name,$final_cat);
            $error_msg[] = "";
        }else{
            $error_msg[] = "Error";
        }
        return json_encode($error_msg);
    }

    function save_top_brand($brands){
        $option_name = 'sc_top_brands';
        $this->add_sc_options($option_name,serialize($brands));
        return true;
    }

    function get_all_categories()
    {
        $query = $this->db->query("select id,name,slug from product_category WHERE `is_delete` =0 order by id desc ");       
        $res = $query->result();        
        return $res;
    }
    
    function get_categoryLevel($cat=NULL)
    {
        if($cat!='')
        {
           $query = $this->db->query("SELECT a.`id`,a.`name` as variation_value,b.`name` as variation_type,b.`id` as vartype_id , c.`name` as subcat_name, c.`id` as subcat_id FROM `product_variation_value` as a,`product_variation_type` as b, `product_sub_category` as c  where a.`product_variation_type_id` = b.`id` and b.`prod_sub_cat_id`=c.`id` and c.`product_cat_id` = ".$cat." ");
        }else
        {
            $query = $this->db->query('SELECT a.`id`,a.`name` as variation_value,b.`name` as variation_type,b.`id` as vartype_id , c.`name` as subcat_name, c.`id` as subcat_id FROM `product_variation_value` as a,`product_variation_type` as b, `product_sub_category` as c  where a.`product_variation_type_id` = b.`id` and b.`prod_sub_cat_id`=c.`id` ');
        }       
        $result = $query->result_array();
        return $result;
    }

    function save_MMcategory($mmcategory,$option)
    {
        $error_msg = array(); $final_cat = array();                
        $final_cat = serialize($mmcategory);
        $option_name = $option;   
       
        $result = $this->add_sc_options($option_name,$final_cat);
        if($result)
        {
            $error_msg = "Save"; 
        }else
        {
            $error_msg = "Error"; 
        }
              
        return json_encode($error_msg);
    }
    
    
} 
?>
