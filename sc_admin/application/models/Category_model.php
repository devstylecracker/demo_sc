<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category_model extends MY_Model {
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }	

/*---------Category submodule functionalities-------*/

	public function get_all_categories($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL){
		
		/*
		if($search_by == '1'){ $search_cond = " AND name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '2'){ $search_cond = " AND slug like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '3'){ 

				$tb_status=strip_tags(trim($table_search));
				if($tb_status=="Yes")
				{
					$status=1;

				}else
				{
					$status=0;
				}

			$search_cond = " AND status =".$status." "; }
		else{ $search_cond ='AND 1=1'; }
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}


		$query = $this->db->query("select id,name, slug, status,is_delete, created_datetime from product_category WHERE `is_delete` =0 ".$search_cond." order by id desc ".$limit_cond);		
		$res = $query->result();*/
		$res = array();
		return $res;
		

	}//this function is used to get the list  category added

	/*open men categories*/
    function get_men_categories(){
        //$query = $this->db->query("select a.`category_name`, a.`category_slug` from category as a WHERE `category_parent` = '-1' and `category_name` = 'women' order by id desc ");
        $query = $this->db->query("select  id, category_name, category_parent,category_slug from (select * from category order by category_parent, id) 
        	products_sorted,(select @pv := '3') initialisation where find_in_set(category_parent, @pv) > 0 and @pv := concat(@pv, ',', id)");
        $res = $query->result();        
        return $res;        
    }/*End men categories*/

	/*open women categories*/
	function get_women_categories(){
        //$query = $this->db->query("select a.`category_name`, a.`category_slug` from category as a WHERE `category_parent` = '-1' and `category_name` = 'women' order by id desc ");
        $query = $this->db->query("select  id, category_name, category_parent,category_slug from (select * from category order by category_parent, id) 
        	products_sorted,(select @pv := '1') initialisation where find_in_set(category_parent, @pv) > 0 and @pv := concat(@pv, ',', id)");
        $res = $query->result();        
        return $res;        
    }/*End women categories*/



    /*open women categories*/
	function get_unisex_categories(){
        //$query = $this->db->query("select a.`category_name`, a.`category_slug` from category as a WHERE `category_parent` = '-1' and `category_name` = 'women' order by id desc ");
        $query = $this->db->query("select  id, category_name, category_parent,category_slug from (select * from category order by category_parent, id) 
        	products_sorted,(select @pv := '2') initialisation where find_in_set(category_parent, @pv) > 0 and @pv := concat(@pv, ',', id)");
        //echo $this->db->last_query();

        $res = $query->result();        
        return $res;        
    }/*End women categories*/

	
	
	public function checkMultilpeCategroy($name)
	{
		$catQuery = "select count(name) as catCount from product_category where name LIKE '".$name."' ";
		$countRes = $this->db->query($catQuery);		
		$rowRes = $countRes->row();	
		return $rowRes;
	}//this function checks for same category while adding new category 
	
	public function new_category($data)
	{	 
		
		$this->db->insert('product_category', $data);
		$c_id = $this->db->insert_id();		
		return $c_id;
		
	}

	public function checkMultilpeCategroyEdit($catId,$name)
	{
		$catQuery = "select count(name) as catCount from product_category where id!=".$catId." and name LIKE '".$name."' ";
		$countRes = $this->db->query($catQuery);
		$rowRes = $countRes->row();
		return $rowRes;
	}//this function checks for same category while editing category

	public function get_category_by_ID($catID){
		
		$this->db->select('*');
		$this->db->from('product_category');
		$this->db->where('id', $catID);
		
		$category = $this->db->get(); 	
		
		if($category){
			
			return $category->row();
			
		}
	}

	public function update_category($catID,$data){
		
		$this->db->where('id', $catID);
 		$updated = $this->db->update('product_category' ,$data);
		
		if($updated){
			
			return TRUE;
			
		}
		else
		{
			
			return false;	
		}
	}


	public function delete_category($catID){
		
		$this->db->where('id', $catID);
 		$deleted = $this->db->update('product_category' ,array('is_delete'=>'1'));
		
		if($deleted){
			
			return TRUE;
			
		}
		else
		{
			
			return false;	
		}
	}
 /*---------End of SubCategory submodule functionalities-------*/

 /*---------SubCategory submodule functionalities-------*/
	public function checkMultilpeSubCategroy($category,$name)
	{
		$catCount = "select count(name) as subcatCount from product_sub_category where product_cat_id=".$category." and name LIKE '".$name."' and is_delete=0";
		$countRes = $this->db->query($catCount);
		$countRow = $countRes->row();
		return $countRow;
	}//this function checks for same subcategory while adding new subcategory 
	
	public function checkMultilpeSubCategroyEdit($subCatId,$category,$name)
	{
		$catCount = "select count(name) as subcatCount from product_sub_category where id!=".$subCatId." and product_cat_id=".$category." and name LIKE '".$name."' and is_delete=0";
		$countRes = $this->db->query($catCount);
		$countRow = $countRes->row();
		return $countRow;
	}//this function checks for same subcategory while editing subcategory
	

	public function get_all_sub_categories($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL){

		if($search_by == '1'){ $search_cond = " AND psc.name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '2'){ $search_cond = " AND psc.slug like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '3'){ $search_cond = " AND pc.name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '4'){ 

				$tb_status=strip_tags(trim($table_search));
				if($tb_status=="Yes")
				{
					$status=1;

				}else
				{
					$status=0;
				}

			$search_cond = " AND psc.status =".$status." "; }
		else{ $search_cond ='AND 1=1'; }
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0)
			{
                   $page = $page*$per_page-$per_page;

            }else
            	{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}


		$query = $this->db->query("select psc.id, psc.name, psc.slug, psc.status, psc.product_cat_id, psc.is_delete, psc.created_datetime,pc.name as prod_cat_name from product_category as pc inner join product_sub_category as psc  on pc.id = psc.product_cat_id WHERE psc.is_delete =0 ".$search_cond." order by psc.modified_datetime desc ".$limit_cond);		
		$res = $query->result();		
		return $res;		
		
	}

	public function get_sub_categories($cat_id=NULL){
		
		$this->db->select('id,name');
		$this->db->from('product_sub_category');
		if($cat_id!=NULL)
		$this->db->where_in('product_cat_id',$cat_id);
		$this->db->where('status',1);
		$this->db->where('is_delete',0);		
		return $this->db->get()->result_array();
	} //to get sub_category according category
	
	public function get_sub_category_by_ID($sub_cat_ID){
		
		$this->db->select('*');
		$this->db->from('product_sub_category');
		$this->db->where('id', $sub_cat_ID);
		$category = $this->db->get(); 	
		
		if($category){
			
			return $category->row();
			
		}
	}
	
	public function update_sub_category($sub_cat_ID,$data){
		
		$this->db->where('id', $sub_cat_ID);
 		$updated = $this->db->update('product_sub_category' ,$data);
		
		if($updated){

			return TRUE;
			
		}
		else
		{			
			return false;	
		}
	}
		
	
	public function delete_subcategory($catID){
		
		$this->db->where('id', $catID);
 		$deleted = $this->db->update('product_sub_category' ,array('is_delete'=>'1'));	
		
		if($deleted){
			
			return true;
			
		}
		else
		{
			
			return false;	
		}
	}
	
	public function get_all_active_categories()
	{
	    $this->db->select('*');
	    $this->db->from('prod_cat');
	    $this->db->where('is_active',1);
	    $res=$this->db->get();
	    return $res->result_array();
	}

	public function get_subcat_by_category()
	{
	    $this->db->select('*');
	    $this->db->from('product_sub_category');
	    $this->db->where('prod_cat_id',$this->input->post('cat_id'));
	    $this->db->where('is_active',1);
	    $this->db->where('is_delete',0);
	    $res=$this->db->get();
	    return $res->result_array();
	}
 /*---------End of SubCategory submodule functionalities-------*/

 /*---------Variation-Type submodule functionalities-------*/

 	public function checkMultilpeVariationtype($subcat,$name)
	{
		$varCount = "select count(name) as vartypeCount from product_variation_type where prod_sub_cat_id=".$subcat." and name LIKE '".$name."' ";
		$countRes = $this->db->query($varCount);
		$countRow = $countRes->row();
		return $countRow;
	}//this function checks for same subcategory while adding new variation-type 
	
	public function checkMultilpeVariationtypeEdit($varId,$subcat,$name)
	{
		$varCount = "select count(name) as vartypeCount from product_variation_type where id!=".$varId." and prod_sub_cat_id=".$subcat." and name LIKE '".$name."' ";
		$countRes = $this->db->query($varCount);
		$countRow = $countRes->row();
		return $countRow;
	}//this function checks for same subcategory while editing variation-type
	

	public function get_all_variationtype($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL){

		if($search_by == '1'){ $search_cond = " AND pvt.name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '2'){ $search_cond = " AND pvt.slug like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '3'){ $search_cond = " AND psc.name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '4'){ 

				$tb_status=strip_tags(trim($table_search));
				if($tb_status=="Yes")
				{
					$status=1;

				}else
				{
					$status=0;
				}

			$search_cond = " AND pvt.status =".$status." "; }
		else{ $search_cond ='AND 1=1'; }
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0)
			{
                   $page = $page*$per_page-$per_page;

            }else
            	{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}


		$query = $this->db->query("select pvt.id, pvt.name, pvt.slug, pvt.status, pvt.prod_sub_cat_id, pvt.is_delete, pvt.created_datetime,psc.name as prod_subcat_name, pc.name as prod_cat_name,pvt.show_in_filter from product_variation_type as pvt inner join product_sub_category as psc inner join product_category as pc  on psc.id = pvt.prod_sub_cat_id and pc.id = psc.product_cat_id WHERE pvt.is_delete =0 ".$search_cond." order by pvt.modified_datetime desc ".$limit_cond);		
		$res = $query->result();		
		return $res;		
		
	}

	public function get_variationtype($id=NULL){
		
		$this->db->select('id,name');
		$this->db->from('product_variation_type');
		if($id!=NULL)
		$this->db->where_in('prod_sub_cat_id',$id);		
		return $this->db->get()->result_array();
	} //to get sub_category according category
	
	public function get_variationtype_by_id($vt_id)
	{
		
		/*$this->db->select('*');
		$this->db->from('product_variation_type');
		$this->db->where('id', $vt_id);
		$this->db->where('status',1);
		$this->db->where('is_delete', 0);
		$variationtype = $this->db->get(); 	
		
		if($variationtype){
			
			return $variationtype->row();
			
		}*/
		$query = $this->db->query("SELECT a.name, a.id ,a.is_delete,a.modified_datetime,a.status,a.slug, b.id as subcat_id, b.name AS subcat_type, c.id as cat_id, c.name AS cat_name,a.`size_guide`,a.show_in_filter FROM product_variation_type AS a, `product_sub_category` AS b, `product_category` AS c WHERE 
			 a.`prod_sub_cat_id` = b.`id` 
			AND b.`product_cat_id` = c.`id`
			AND a.`id` = ".$vt_id."; ");	
		$res = $query->result();		
		return $res;
	}

	public function get_variationtype_bySubCatId($sub_cat_id)
	{
		$this->db->select('id,name');
		$this->db->from('product_variation_type');
		$this->db->where_in('prod_sub_cat_id',$sub_cat_id);
		$this->db->where('status',1);
		$this->db->where('is_delete',0);
		return $this->db->get()->result_array();
	}
	
	
	public function update_variationtype($vt_id,$data){
		
		$this->db->where('id', $vt_id);
 		$updated = $this->db->update('product_variation_type' ,$data);
		
		if($updated)
		{
			return true;			
		}
		else
		{
			return false;	
		}
	}
		
	
	public function delete_variationtype($vt_id){
		
		$this->db->where('id', $vt_id);
 		$deleted = $this->db->update('product_variation_type' ,array('is_delete'=>'1'));	
		
		if($deleted)
		{			
			return true;			
		}
		else
		{			
			return false;	
		}
	}
	
	public function get_all_product_variations_types(){
		
		$this->db->select('pvt.*,psc.name as sub_cat_name');
        $this->db->from('product_variation_type as pvt');        
		$this->db->join('product_sub_category as psc','pvt.prod_sub_cat_id = psc.id');
		$result = $this->db->get(); 	
		
		if($result){
			
			return $result->result_array();
			
		}
		
	}

 /*---------End of Variation-Type submodule functionalities-------*/   

 /*---------Variation submodule functionalities-------*/

 	public function checkMultilpeVariationvalue($vartype,$name)
	{
		$varCount = "select count(name) as varCount from product_variation_value where product_variation_type_id=".$vartype." and name LIKE '".$name."' ";
		$countRes = $this->db->query($varCount);
		$countRow = $countRes->row();
		return $countRow;
	}//this function checks for same subcategory while adding new variation-type 
	
	public function checkMultilpeVariationvalueEdit($varId,$vartype,$name)
	{
		$varCount = "select count(name) as varCount from product_variation_value where id=".$varId." and product_variation_type_id=".$vartype." and name LIKE '".$name."' and status = 1 ";
		$countRes = $this->db->query($varCount);
		$countRow = $countRes->row();
		return $countRow;
	}//this function checks for same subcategory while editing variation-type
	

	public function get_all_variationvalue($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL){

		if($search_by == '1'){ $search_cond = " AND a.name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '2'){ $search_cond = " AND a.slug like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '3'){ $search_cond = " AND b.name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '4'){ $search_cond = " AND c.name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '5'){ $search_cond = " AND d.name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '6'){ 

				$tb_status=strip_tags(trim($table_search));
				if($tb_status=="Yes")
				{
					$status=1;

				}else
				{
					$status=0;
				}

			$search_cond = " AND a.status =".$status." "; }
		else{ $search_cond ='AND 1=1'; }
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0)
			{
                   $page = $page*$per_page-$per_page;

            }else
            	{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}


		/*$query = $this->db->query("select pv.id, pv.name, pv.slug, pv.status, pv.product_variation_type_id, pv.is_delete, pv.created_datetime,pvt.name as prod_subcat_name from product_variation_value as pv inner join product_variation_type as pvt  on pvt.id = pv.product_variation_type_id WHERE pv.is_delete =0 ".$search_cond." order by pv.modified_datetime desc ".$limit_cond);*/

		$query = $this->db->query("SELECT a.name, a.id AS variation_value,a.is_delete,a.modified_datetime,a.status,a.slug, b.id, b.name AS variation_type, c.id, c.name AS sub_cat, d.id, d.name AS cat_name,a.show_in_filter FROM product_variation_value AS a, product_variation_type AS b, `product_sub_category` AS c, `product_category` AS d WHERE a.`product_variation_type_id` = b.`id` 
			AND b.`prod_sub_cat_id` = c.`id` 
			AND c.`product_cat_id` = d.`id` ".$search_cond."
			AND a.is_delete = 0  order by a.modified_datetime desc ".$limit_cond);
		
		$res = $query->result();		
		return $res;		

	}

	public function get_variationvalue($id=NULL){
		
		$this->db->select('id,name');
		$this->db->from('product_variation_value');
		if($id!=NULL)
		$this->db->where_in('product_variation_type_id',$id);
		$this->db->where('status',1);
		$this->db->where('is_delete',0);		
		return $this->db->get()->result_array();
	} //to get sub_category according category
	
	public function get_variationvalue_by_id($v_id)
	{
		$query = $this->db->query("SELECT a.name, a.id AS variation_value,a.is_delete,a.modified_datetime,a.status,a.slug, b.id as vart_id, b.name AS variation_type, c.id as sub_cat_id, c.name AS sub_cat, d.id as cat_id, d.name AS cat_name,a.show_in_filter FROM product_variation_value AS a, product_variation_type AS b, `product_sub_category` AS c, `product_category` AS d WHERE a.`product_variation_type_id` = b.`id` 
			AND b.`prod_sub_cat_id` = c.`id` 
			AND c.`product_cat_id` = d.`id`
			AND a.`id` = ".$v_id."; ");	
		$res = $query->result();		
		return $res;
		/*$this->db->select('*');
		$this->db->from('product_variation_value');
		$this->db->where('id', $v_id);
		$this->db->where('status',1);_
		$this->db->where('is_delete', 0);
		$variationvalue = $this->db->get(); 	
		
		if($variationvalue){
			
			return $variationvalue->row();
			
		}*/
	}

	public function get_variationvalue_byVarType($vartype_id)
	{
		$this->db->select('id,name');
		$this->db->from('product_variation_value');
		$this->db->where_in('product_variation_type_id',$vartype_id);
		$this->db->where('status',1);
		$this->db->where('is_delete',0);
		return $this->db->get()->result_array();
	}
	
	
	public function update_variationvalue($v_id,$data){
		
		$this->db->where('id', $v_id);
 		$updated = $this->db->update('product_variation_value' ,$data);
		
		if($updated)
		{
			return true;			
		}
		else
		{
			return false;	
		}
	}
		
	
	public function delete_variationvalue($v_id){
		
		$this->db->where('id', $v_id);
 		$deleted = $this->db->update('product_variation_value' ,array('is_delete'=>'1'));	
		
		if($deleted)
		{			
			return true;			
		}
		else
		{			
			return false;	
		}
	}
	
	public function get_all_product_variations(){
		
		$this->db->select('pv.*,psc.name as sub_cat_name');
        $this->db->from('product_variation_value as pv');        
		$this->db->join('product_variation_type as pvt','pv.product_variation_type_id = pvt.id');
		$result = $this->db->get(); 	
		
		if($result){
			
			return $result->result_array();
			
		}
		
	}

 /*---------End of Variation submodule functionalities-------*/   


/*---------Common functionalities-------*/  
	public function new_record($table,$data){
		
		$this->db->insert($table, $data);
		$c_id = $this->db->insert_id();
		return $c_id;
	}
/*---------End of Common functionalities-------*/  
}

