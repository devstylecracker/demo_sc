<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Delivery_process_model extends MY_Model {
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function getDpProduct($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL)
    {
    	$search_cond = "";
    	$search_cond_have = "";
    	$limit_cond = "";

        if($search_by == '1'){ $search_cond = " AND a.`id` = '".strip_tags(trim($table_search))."'"; }
        else if($search_by == '2'){
          $search_cond_have = " having `awp_no` = '".strip_tags(trim($table_search))."'";        	 
        }else if($search_by == '3'){

        	$tb_status1=strtolower(strip_tags(trim($table_search)));
				 if($tb_status1 == "confirmed")
				{					
					$apr_status = 2;
				}else if($tb_status1 == "dispatched")
				{
					$apr_status = 3;

				}else if($tb_status1 == "delivered")
				{
					$apr_status = 4;

				}else if($tb_status1 == "return")
				{
					$apr_status = 6;
					
				}else
				{
					$apr_status = '';
				}
          $search_cond = " AND d.`order_status` = '".$apr_status."'";        	 
        }


        if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}

    	if($this->session->userdata("role_id") != 7)
		{		  
		  $query = $this->db->query("select distinct a.`id` as order_id, (Select id from `order_delivery_process` where order_prd_id =d.id) as doid,(Select awp_no from `order_delivery_process` where order_prd_id =d.id) as awp_no,(Select dpCharges from `order_delivery_process` where order_prd_id =d.id) as dpCharges,d.`id` as order_prd_id, a.`user_id`, a.`brand_id`, a.`order_tax_amount`, a.`cod_amount`, a.`order_sub_total`, a.`order_total`, d.`product_id`, d.`send_email`, a.`pay_mode`, a.`created_datetime`, d.`modified_datetime`, a.`first_name`, a.`last_name`, a.`email_id`, a.`mobile_no`, a.`shipping_address`, a.`pincode`, a.`city_name`, a.`state_name`,c.`state_name`,b.`company_name`, d.`product_id`, g.`company_address`, g.`city` as company_city,g.`state` as company_state,g.`country` as company_country, d.`product_qty`,d.`product_size`,e.`name` as product_name, d.`product_price`,a.`order_unique_no`, d.`order_status`,e.`image`,d.`dp_active` from `order_info` as a,`brand_info` as b,`states` as c, `order_product_info` as d, `product_desc` as e,`product_size` as f,`brand_contact_info` as g,`brand_delivery_partner_mapping` as h where  a.`brand_id` = b.`user_id` and a.`state_name` = c.`id` and d.`order_id` = a.`id` and e.`id` = d.`product_id` and d.`product_size` =f.id and a.`brand_id` = g.`brand_id` and ((a.`brand_id` = h.`brand_id`) OR (d.`dp_active` = 1)) and d.`order_status`NOT IN(9,7,5,1)  ".$search_cond."  ".$search_cond_have." order by d.`modified_datetime` desc, a.`id` desc ".$limit_cond); 

		}else
		{
			$d_userid = $this->session->userdata("user_id");
		  	$d_id = $this->get_dpid($d_userid);
		  	if($search_by == '1'){ $search_cond = " and i.dp_id='".$d_id."'"; }
        	else if($search_by == '2'){
		  		$search_cond = " AND a.`awp_no` = '".strip_tags(trim($table_search))."'";
		  	}else if($search_by == '3'){

        		$tb_status1=strtolower(strip_tags(trim($table_search)));
				 if($tb_status1 == "confirmed")
				{					
					$apr_status = 2;
				}else if($tb_status1 == "dispatched")
				{
					$apr_status = 3;

				}else if($tb_status1 == "delivered")
				{
					$apr_status = 4;

				}else if($tb_status1 == "return")
				{
					$apr_status = 6;
					
				}else
				{
					$apr_status = '';
				}
		          $search_cond = " AND b.`order_status` = '".$apr_status."'";        	 
		        }
		  	$query = $this->db->query("SELECT a.`order_no` as order_id,a.`id` as doid,a.`product_id`,a.`brand_id`,a.`dp_id`,a.`order_unq_no` as order_unique_no,
		  		a.`pickup_address` ,a.`shipping_address`,a.`awp_no`,a.`dp_status`,b.`id`as order_prd_id, b.`order_status`, (Select name from product_desc where id=a.`product_id`) as product_name,(Select `company_name` from `brand_info` where `user_id` = a.`brand_id`) as company_name,c.`delivery_partner_name` as dp_name FROM `order_delivery_process` as a,order_product_info as b,delivery_partner as c where b.`id` = a.`order_prd_id` and b.`product_id` = a.`product_id` and a.`dp_id`= c.`id` and a.`dp_id`='".$d_id."' ".$search_cond." order by a.`created_datetime` desc, a.`order_no` desc ".$limit_cond);
		  	//echo $this->db->last_query();  
		  	//(d.order_id=i.order_prd_id and d.product_id =i.product_id)
		}
    	//echo $this->db->last_query(); 
    	//a.`order_status`!=9 and a.`order_status`!=7

    	/*$query = $this->db->query("select distinct d.`id` as order_prd_id, (Select id from `order_delivery_process` where order_prd_id =d.id) as doid,a.`id` as order_id, a.`user_id`, a.`brand_id`, a.`order_tax_amount`, a.`cod_amount`, a.`order_sub_total`, a.`order_total`, d.`product_id`, d.`send_email`, a.`pay_mode`, a.`created_datetime`, d.`modified_datetime`, a.`order_status`, a.`first_name`, a.`last_name`, a.`email_id`, a.`mobile_no`, a.`shipping_address`, a.`pincode`, a.`city_name`, a.`state_name`,c.`state_name`,b.`company_name`, d.`product_id`, g.`company_address`, g.`city` as company_city,g.`state` as company_state,g.`country` as company_country, d.`product_qty`,d.`product_size`,e.`name` as product_name, d.`product_price`,a.`order_unique_no`, d.`order_status`,e.`image`,d.`dp_active` from `order_info` as a,`brand_info` as b,`states` as c, `order_product_info` as d, `product_desc` as e,`product_size` as f,`brand_contact_info` as g,`brand_delivery_partner_mapping` as h where  a.`brand_id` = b.`user_id` and a.`state_name` = c.`id` and d.`order_id` = a.`id` and e.`id` = d.`product_id` and d.`product_size` =f.id and a.`brand_id` = g.`brand_id` and ((a.`brand_id` = h.`brand_id`) OR (d.`dp_active` = 1)) and (d.`order_status`IN(2,3,4,6))  ".$search_cond."  order by a.`created_datetime` desc, a.`id` desc ".$limit_cond);*/

      	/*echo $this->db->last_query();exit;*/
        $result = $query->result_array();
        //echo '<pre>';print_r($result);
        return $result;


    }

    public function getDelPartnerOrder()
	{			
		$query = $this->db->query("SELECT * FROM `order_delivery_process` order by created_datetime desc");
		$res = $query->result_array();				
		return $res;
	}

	public function getDelPartners()
	{	
		$query = $this->db->query("select id, delivery_partner_name from delivery_partner where status = 1 order by created_datetime desc ");
		$res = $query->result_array();				
		return $res;
	}


	 public function get_paid_stores($search_by,$search_value)
    {
    	$search_cond = "";
    	if($search_by!='' && $search_value!='')
    	{
    		if($search_by==1)
    		{
    			$search_cond = "AND  company_name like '%".$search_value."%'";
    		}
    	}
        if($this->session->userdata('role_id') == 6){
            $brand_id = $this->session->userdata('user_id');
            $query = $this->db->query("SELECT user_id as brand_id, company_name FROM brand_info  WHERE is_paid = 1 and user_id=".$brand_id."  ".$search_cond." ORDER BY id DESC");
        }else{
            $query = $this->db->query("SELECT user_id as brand_id, company_name FROM brand_info  WHERE is_paid = 1  ".$search_cond." ORDER BY id DESC");
        }
        $result = $query->result_array();
        return $result;
    }

    public function get_alldpmIds()
    { 	
    	$query = $this->db->query("select dpm.brand_id,	dpm.dp_id,b.company_name from brand_delivery_partner_mapping as dpm, brand_info as b where dpm.brand_id = b.user_id ");    	

		$result = $query->result_array();
		return $result;		
    }

    public function dp_save($dpids_str,$brand_id)
	{	
		if($brand_id>0)
		{
			$this->db->from('brand_delivery_partner_mapping');
			$this->db->where('brand_id',$brand_id);
			$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				if($dpids_str>0 && $brand_id>0)
				{
					$data = array(
						'brand_id'=>$brand_id,
						'dp_id'=>$dpids_str,
						'modified_by' =>$this->session->userdata('user_id'),
						'created_datetime' => date('Y-m-d H:i:s')
						);
					$this->db->where('brand_id', $brand_id);
 					$updated = $this->db->update('brand_delivery_partner_mapping' ,$data);
 				}else
 				{
 					$this->db->where('brand_id', $brand_id);
      				$updated = $this->db->delete('brand_delivery_partner_mapping'); 
 				}
 				if($updated)
 				{
 					return True;
 				}else
 				{
 					return False;
 				}
			}else
			{				
				if($dpids_str!='' && $brand_id!='')
				{
					$data = array(
						'brand_id'=>$brand_id,
						'dp_id'=>$dpids_str,
						'created_by' =>$this->session->userdata('user_id'),
						'created_datetime' => date('Y-m-d H:i:s')
						);
					$this->db->insert('brand_delivery_partner_mapping', $data);
					$f_id = $this->db->insert_id();						
					return $f_id;		
				}	
			}
		}		
	}

	public function dp_order_save($data_post,$doid=null)
	{	
		if(!empty($data_post))
		{
			if($doid!='')
			{
				$this->db->where('id',$doid);
				$this->db->update('order_delivery_process',$data_post);


				if($data_post['dp_status']==2 )//Dispatched
				{
					//$this->db->where('id',$data_post['order_prd_id']);
					$this->db->where(array('id' => $data_post['order_prd_id'], 'order_status !=' => 6));
					$this->db->update('order_product_info',array('order_status'=>3,'modified_by'=>$this->session->userdata('user_id')));//In order_product_info Dispatched=3
					return 2;
				}else if($data_post['dp_status']==3)//Delivered
				{
					//$this->db->where('id',$data_post['order_prd_id']);
					$this->db->where(array('id' => $data_post['order_prd_id'], 'order_status !=' => 6));
					$this->db->update('order_product_info',array('order_status'=>4,'modified_by'=>$this->session->userdata('user_id')));//In order_product_info Delivered=4
					return 2;
				}else if($data_post['dp_status']==5)//Confirmed
				{
					//$this->db->where('id',$data_post['order_prd_id']);
					$this->db->where(array('id' => $data_post['order_prd_id'], 'order_status !=' => 6));
					$this->db->update('order_product_info',array('order_status'=>2,'modified_by'=>$this->session->userdata('user_id')));//In order_product_info Confirmed=2
					return 2;
				}
				return 1;

			}else
			{
				$this->db->insert('order_delivery_process',$data_post);
				/*echo $this->db->last_query();exit;*/
				$rdo_id = $this->db->insert_id();						
				return 1;
			}
		}
	}

	public function get_dpid($dp_userid)
	{
		if($dp_userid!="" && $dp_userid>0)
		{
			$query = $this->db->query("select `id` from delivery_partner where `status` = 1 and `user_id`='".$dp_userid."'");
			$res = $query->result_array();				
			return $res[0]['id'];
		}

	}

	public function getDpUserId($dpartner)
	{
		if($dpartner!="" && $dpartner>0)
		{
			$query = $this->db->query("select `user_id` from delivery_partner where `status` = 1 and `id`='".$dpartner."'");
			$res = $query->result_array();				
			return $res[0]['user_id'];
		}
	}

	public function getOrderStatus($ordprdid)
	{
		if($dpartner!="" && $dpartner>0)
		{
			$query = $this->db->query("select `order_status` from `order_product_info` where `id`='".$ordprdid."'");
			$res = $query->result_array();				
			return $res[0]['order_status'];
		}
	}

	public function getDPOrdersExcel($search_by,$search_cond)
	{
		//$d_userid = $this->session->userdata("user_id");
		  	//$d_id = $this->get_dpid($d_userid);
		  	if($search_by == '1'){ $search_cond = " and b.order_id = $search_cond "; }
        	else if($search_by == '2'){
		  		$search_cond = " AND a.`awp_no` = '".strip_tags(trim($search_cond))."'";
		  	}else if($search_by == '3'){

        		$tb_status1=strtolower(strip_tags(trim($search_cond)));
				 if($tb_status1 == "confirmed")
				{					
					$apr_status = 2;
				}else if($tb_status1 == "dispatched")
				{
					$apr_status = 3;

				}else if($tb_status1 == "delivered")
				{
					$apr_status = 4;

				}else if($tb_status1 == "return")
				{
					$apr_status = 6;
					
				}else
				{
					$apr_status = '';
				}
		          $search_cond = " AND b.`order_status` = '".$apr_status."'";        	 
		      }else
		      {
		      	$search_cond = "AND 1=1";
		      }
		  	$query = $this->db->query("SELECT a.`order_no` as order_id,a.`id` as doid,a.`product_id`,a.`brand_id`,a.`dp_id`,a.`order_unq_no` as order_unique_no,
		  		a.`pickup_address` ,a.`shipping_address`,a.`awp_no`,a.`dpCharges`,a.`dp_status`,b.`id`as order_prd_id, b.`order_status`, (Select name from product_desc where id=a.`product_id`) as product_name,(Select `company_name` from `brand_info` where `user_id` = a.`brand_id`) as company_name,c.`delivery_partner_name` as dp_name,IF(d.`pay_mode` = 1,b.`product_price`,'0') as product_price,IF(d.`pay_mode` = 1,'COD','Online') as pay_mode FROM `order_delivery_process` as a,order_product_info as b,delivery_partner as c,order_info as d where b.`id` = a.`order_prd_id` and b.`product_id` = a.`product_id` and a.`dp_id`= c.`id` AND d.`id` = b.`order_id`  ".$search_cond." order by a.`created_datetime` desc, a.`order_no` desc");
		  	//a.`dp_id`='".$d_id."'
		  	// echo $this->db->last_query();exit;
        $result = $query->result_array();
        //echo '<pre>';print_r($result);
        return $result;

	}

	function getBrandDelPart($brandId)
	{
		$dpartner = array();
		$querybrand = $this->db->query("select dp_id from brand_delivery_partner_mapping where brand_id =".$brandId." order by created_datetime desc ");
		$resultbrand = $querybrand->result_array();	
		if(!empty($resultbrand))
		{		
			@$dpbexplode = explode(',',$resultbrand[0][dp_id]);		

			$query = $this->db->query("select id, delivery_partner_name from delivery_partner where status = 1 order by created_datetime desc ");
			$res = $query->result_array();		

			foreach($res as $val)
			{
				if(in_array($val['id'], $dpbexplode))
				{
					$dpartner[] = array('id'=>$val['id'],'delivery_partner_name'=>$val['delivery_partner_name']);
				}
			}		
		}			
		return $dpartner;
	}


	
/*---------End of Common functionalities-------*/ 
public function getTrackData($awbno='')
{
	$this->db->select('tracking_data');
	$this->db->from('order_delivery_process');
	$this->db->where('awp_no', $awbno);
	$query = $this->db->get();
	if($query->num_rows()>0)
	{
		return $query->row_array();
	}
	return false;
}
 
}

