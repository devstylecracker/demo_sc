<?php
class Product_cron_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function product_import_cron($type){
        $prdate = strtotime(date('Y-m-d 00:50:00') . ' -1 day');
        if($type == 'add')
        {            
			$query = $this->db->query('select b.company_name,a.brand_id,count(a.id) as product_added from product_desc as a,brand_info as b where a.brand_id = b.user_id and  a.approve_reject IN ("A","D","P") and a.is_delete=0 and a.status=1 and b.is_paid= 1 and a.created_datetime Like  "%'.DATE('Y-m-d',$prdate).'%" GROUP BY a.brand_id order by a.id desc ');

        }else if($type == 'update')
        {
			$query = $this->db->query('select b.company_name,a.brand_id,count(a.id) as product_updated from product_desc as a,brand_info as b where a.brand_id = b.user_id and  a.approve_reject IN ("A","D","P") and a.is_delete=0 and a.status=1 and b.is_paid= 1 and a.modified_datetime Like  "%'.DATE('Y-m-d',$prdate).'%" GROUP BY a.brand_id order by a.id desc ');
        }
		
        $result = $query->result_array();        
        return $result;
    }
	
	function user_data(){
		
		$prdate = strtotime(date('Y-m-d 00:50:00') . ' -1 day');
		$query = $this->db->query('select a.id,a.bucket,a.email,b.registered_from from user_login as a,user_info as b where a.id = b.user_id and a.created_datetime Like  "%'.DATE('Y-m-d',$prdate).'%" ');
		$result = $query->result_array();        
        return $result;
	}
	
	function get_cart_data(){
		
		$prdate = strtotime(date('Y-m-d 00:50:00') . ' -1 day');
		$query = $this->db->query('select a.id,a.user_id,a.platform_info,b.name,b.price,c.company_name from cart_info as a,product_desc as b,brand_info as c where a.product_id = b.id and b.brand_id = c.user_id AND a.created_datetime Like  "%'.DATE('Y-m-d',$prdate).'%" ');
		$result = $query->result_array();        
        return $result;
		
	}
	
	function get_order_data(){
		// echo date('Y-m-d 00:01:00');exit;
		// echo date('d.m.Y',strtotime("-1 days"));exit;
		$prdate = strtotime(date('Y-m-d 00:50:00') . ' -1 day');
		$query = $this->db->query('select DISTINCT a.id,a.user_id,a.order_display_no,b.platform_info,a.order_total from order_info as a,cart_info as b where a.id = b.order_id AND a.created_datetime Like  "%'.DATE('Y-m-d',$prdate).'%"');
		$result = $query->result_array();        
        return $result;
		
	}

	function getdeletelook()
	{
		$query = $this->db->query('Select DISTINCT lp.look_id as look_ids from look_products as lp, product_inventory as pi,look as l where pi.product_id=lp.product_id and l.id=lp.look_id and l.deflag=0  group by lp.look_id having sum(pi.stock_count)=0 order by lp.look_id ');
		$result = $query->result_array();
		$delids ='';
		
		foreach($result as $val)
		{
			if($delids=='')
			{
				$delids = $val['look_ids'];
			}else
			{
				$delids = $delids.','.$val['look_ids'];
			}
			
		}		

		if(!empty($result))
		{
			$q = $this->db->query('Update look set deflag=1, modified_datetime = NOW() where deflag=0 and id IN ('.$delids.') ');
			$del_res = $q->result_array();
		}        
        return $delids;
	}
	
	function remove_collection(){
		$query = $this->db->query('select GROUP_CONCAT(a.object_id) as collection_ids,b.object_meta_key,b.object_meta_value,now() from object as a,object_meta as b where a.object_id = b.object_id AND a.object_status = 3 AND b.object_meta_value < now() AND b.object_meta_key = "collection_end_date"  order by a.object_id desc');
		$result = $query->result_array();
		// echo "<pre>";print_r($result);exit;
		if(@$result[0]['collection_ids'] != ''){
			$query = $this->db->query('update object SET `object_status` = 0 where `object_id` in ('.@$result[0]['collection_ids'].')');
		}
		return true;
	}
	
	function get_box_order_data($days){
		
		$prdate = strtotime(date('Y-m-d 00:00:00') . $days);
		$query = $this->db->query('select DISTINCT a.order_unique_no,a.user_id,a.email_id,a.brand_id,a.first_name from order_info as a where a.modified_datetime Like  "%'.DATE('Y-m-d',$prdate).'%" AND a.brand_id = '.SCBOX_BRAND_ID.' AND a.order_status != 9 AND is_completed = 1 ');
		$result = $query->result_array();
		// echo $this->db->last_query();
        return $result;
		
	}
	
}