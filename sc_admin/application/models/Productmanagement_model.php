<?php
class Productmanagement_model extends CI_Model {

    public $ouptut = array();
    public $depth = 0; public $children = 0;
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
   
	/*---------Product Details functionalities-------*/

	public function get_all_product_details($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL){
		
		
		if($search_by == '1'){ $search_cond = " AND a. name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '2'){ $search_cond = " AND b.name like '%".strip_tags(trim($table_search))."%'"; }
			else if($search_by == '3'){ $search_cond = " AND c.name like '%".strip_tags(trim($table_search))."%'"; }
				//else if($search_by == '4'){ $search_cond = " AND a.slug like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '5'){ 

				$tb_status=strip_tags(trim($table_search));
				

				if($tb_status=="Approved"){ $search_cond = " AND a.approve_reject = 'A'"; }
				else if($tb_status=="Reject") { $search_cond = " AND a.approve_reject = 'R'"; }
				else{ $search_cond = " AND (a.approve_reject = 'P' || a.approve_reject = 'D')"; }

				
			 }else if($search_by == '6'){ $search_cond = " AND a.id ='".strip_tags(trim($table_search))."'"; }
				else if($search_by == '7'){ $search_cond = " AND b.company_name like '%".strip_tags(trim($table_search))."%'"; }
				//else if($search_by == '8'){ $search_cond = " AND g.product_sku like '%".strip_tags(trim($table_search))."%'"; }					
				else{ $search_cond ='AND 1=1'; $search_cond =''; }
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}

		if($this->session->userdata('role_id') == 6 ){ 
			$search_cond = $search_cond." and a.brand_id=".$this->session->userdata('user_id');
		}
		

		if($search_by == '8'){

			//$query1 = $this->db->query("select id from category where LOWER(category_name) = '".strtolower(trim($table_search))."' limit 0,1");
			$query1 = $this->db->query("select id from category where id = '".strtolower(trim($table_search))."' limit 0,1");
			$res1 = $query1->result_array();
			
			if(!empty($res1)){
				$cat_id = $res1[0]['id'];

				$get_pro_id = $this->db->query("select distinct object_id from category_object_relationship where object_type='product' and category_id IN ('".$cat_id."') order by object_id desc");
            	$result = $get_pro_id->result_array();

	            if(!empty($result)){
	                foreach($result as $val){
	                    $product_ids[] = $val['object_id'];
	                }

	                $product_id = implode(',', $product_ids);
	                $search_cond = $search_cond." and a.id IN (".$product_id.")";
	            }
			}

		}

		if($search_by == '9'){

			//$get_pro_id = $this->db->query("select product_id from product_inventory where product_sku = '".trim($table_search)."'");
			$get_pro_id = $this->db->query("select id as product_id from product_desc where sku_number = '".trim($table_search)."'");
            	$result = $get_pro_id->result_array();

	            if(!empty($result)){
	                foreach($result as $val){
	                    $product_ids[] = $val['product_id'];
	                }

	                $product_id = implode(',', $product_ids);
	                $search_cond = $search_cond." and a.id IN (".$product_id.")";
	            }
		

		}


		if($search_by == '10'){

			$query1 = $this->db->query("select id from (select * from category order by category_parent, id) products_sorted,(select @pv := '1') initialisation where find_in_set(category_parent, @pv) > 0 and @pv := concat(@pv, ',', id)");
			$res1 = $query1->result_array();
			$cat_id = ''; $i = 0;
			if(!empty($res1)){
				foreach($res1 as $val){
					if($i!=0){ $cat_id = $cat_id.','; }
					$cat_id = $cat_id.$val['id'];
					$i++;
				}
			}


			if($cat_id!=''){
				$get_pro_id = $this->db->query("select distinct object_id from category_object_relationship where object_type='product' and category_id IN ('".$cat_id."') order by object_id desc");
            	$result = $get_pro_id->result_array();

	            if(!empty($result)){
	                foreach($result as $val){
	                    $product_ids[] = $val['object_id'];
	                }

	                $product_id = implode(',', $product_ids);
	                $search_cond = $search_cond." and a.id IN (".$product_id.")";
	                $search_cond = $search_cond." AND a.name like '%".strip_tags(trim($table_search))."%'";
	            }
	        }

		}

		if($search_by == '11'){

			$query1 = $this->db->query("select id from (select * from category order by category_parent, id) products_sorted,(select @pv := '3') initialisation where find_in_set(category_parent, @pv) > 0 and @pv := concat(@pv, ',', id)");
			$res1 = $query1->result_array();
			$cat_id = ''; $i = 0;
			if(!empty($res1)){
				foreach($res1 as $val){
					if($i!=0){ $cat_id = $cat_id.','; }
					$cat_id = $cat_id.$val['id'];
					$i++;
				}
			}


			if($cat_id!=''){
				$get_pro_id = $this->db->query("select distinct object_id from category_object_relationship where object_type='product' and category_id IN ('".$cat_id."') order by object_id desc");
            	$result = $get_pro_id->result_array();

	            if(!empty($result)){
	                foreach($result as $val){
	                    $product_ids[] = $val['object_id'];
	                }

	                $product_id = implode(',', $product_ids);
	                $search_cond = $search_cond." and a.id IN (".$product_id.")";
	                $search_cond = $search_cond." AND a.name like '%".strip_tags(trim($table_search))."%'";
	            }
	        }

		}

		if($search_by!='' && $search_by!='0')
		{
			$query = $this->db->query("select a.id,a.name,a.slug,a.price,a.created_datetime,a.image,b.company_name as brand,a.approve_reject,a.status,a.compare_price,a.brand_id,a.store_id,a.sku_number,a.stock_count,a.parent_id from product_desc as a,brand_info as b where a.brand_id = b.user_id and a.is_delete=0 and a.`modified_datetime` >= '2018-01-16 00:00:00' ".$search_cond." order by a.id desc ".$limit_cond);
		}else
		{
			$query = $this->db->query("select a.id,a.name,a.slug,a.price,a.created_datetime,a.image,b.company_name as brand,a.approve_reject,a.status,a.compare_price,a.brand_id,a.store_id,a.sku_number,a.stock_count,a.parent_id from product_desc as a,brand_info as b where a.brand_id = b.user_id and a.is_delete=0 and a.parent_id='0' and a.`modified_datetime` >= '2018-01-16 00:00:00' ".$search_cond." order by a.id desc ".$limit_cond);
		}

		
		//and a.`modified_datetime` >= '2018-01-16 00:00:00'
		if(@$_GET['test']==1)
		{			
			//echo $this->db->last_query();	
		}
		
		
		$res = $query->result();
		return $res;
		

	}//this function is used to get the product details added
	
	function get_sub_cat($cat_id){
			/*$query = $this->db->get_where('product_sub_category', array('status' => 1,'is_delete'=>0,'product_cat_id' =>$cat_id));
			$result = $query->result_array();
			return $result;*/
			return true;
		}
	
	function get_var_type($var_type){
			$query = $this->db->query('SELECT a.id,a.name as variation_type,b.name as variation,b.id as nid FROM `product_variation_value` as a,`product_variation_type` as b where a.`product_variation_type_id` = b.`id` and b.`prod_sub_cat_id`='.$var_type." and a.status = 1 and b.status = 1 and a.is_delete = 0 and b.is_delete =0 order by b.id desc");
			$result = $query->result_array();
			return $result;
		}
	
	function all_store(){
			$query = $this->db->get_where('brand_store', array('status' => 1));
			$result = $query->result_array();
			return $result;
		}
	
	function load_questAns($id){
		/*$query = $this->db->query("SELECT A.id, A.answer FROM answers AS A INNER JOIN question_answer_assoc AS Q ON Q.answer_id=A.id AND (Q.question_id = ".$id.") ORDER BY A.answer");
		$result = $query->result_array();
		return $result;*/
		return true;
	}

	function add_product($data){
		$this->db->insert('product_desc', $data['product_info']); 
		$product_insert_id = $this->db->insert_id(); 

		/* Add Product Tags*/
		if(!empty($data['product_tags_array'])){
			
			foreach($data['product_tags_array'] as $tag){
				$tags = array();
				$tags = array(
						'product_id'=>$product_insert_id,
						'tag_id' => $tag,
						'status' => 1,
						'created_by' =>$this->session->userdata('user_id'),
						'created_datetime' => date('Y-m-d H:i:s')
					);
				$this->db->insert('product_tag', $tags); 

			}
		}
		/* Add Product Store / Product Availibility */
		if(!empty($data['product_avail_array'])){
			
			foreach($data['product_avail_array'] as $store){
				$stores = array();
				$stores = array(
						'product_id'=>$product_insert_id,
						'store_id' => $store,
						'status' => 1
					);
				$this->db->insert('product_store', $stores); 

			}
		}

		/* category mapping */
		if(!empty($data['category'])){
			foreach ($data['category'] as $value) {
				$cat_data = array(
						'object_id'=>$product_insert_id,
						'category_id' => $value,
						'object_type' => 'product',
						'modified_by'=>$this->session->userdata('user_id')
				);
				$this->db->insert('category_object_relationship', $cat_data);
				$this->update_category_product_count($value);
			}
		}

		/* Attributes mapping*/
		if(!empty($data['attributes'])){
			foreach ($data['attributes'] as $value) {
				$cat_data = array(
						'object_id'=>$product_insert_id,
						'attribute_id' => $value,
						'object_type' => 'product',
						'modified_by'=>$this->session->userdata('user_id')
				);
				$this->db->insert('attribute_object_relationship', $cat_data);
				$this->update_attribute_product_count($value);
			}
		}


		/* Product extra images */
		if(!empty($data['extra_images'])){
			foreach ($data['extra_images'] as $key => $value) {
				$extr_img = array(
						'product_id'=>$product_insert_id,
						'product_images' => $value,
						'created_datetime' => date('Y-m-d H:i:s')
					);
				$this->db->insert('extra_images', $extr_img); 
			}
		}		
		
		return true;

	}

	function get_product_info($product_id){
		if($product_id !=''){
			$query = $this->db->get_where('product_desc', array('id' => $product_id));
			$res = $query->result_array();
			return $res;
		}
	}

	function get_product_tags($product_id){
		if($product_id !=''){
			$query = $this->db->get_where('product_tag', array('product_id' => $product_id));
			$res = $query->result_array(); 
			$tags_id = array();
			if(!empty($res)){
				foreach($res as $val){

					$tags_id[] = $val['tag_id'];
				}
			}

			return $tags_id;
		}

	}

	function get_product_avalibility($product_id){
		if($product_id !=''){
			$query = $this->db->get_where('product_store', array('product_id' => $product_id));
			$res = $query->result_array(); 
			$store_ids = array();
			if(!empty($res)){
				foreach($res as $val){

					$store_ids[] = $val['store_id'];
				}
			}

			return $store_ids;
		}

	}

	function get_product_variations($product_id){
		/*if($product_id !=''){
			$query = $this->db->get_where('product_variations', array('product_id' => $product_id));
			$res = $query->result_array(); 
			$varivalue = array();
			if(!empty($res)){
				foreach($res as $val){

					$varivalue[] = $val['product_var_val_id'];
				}
			}

			return $varivalue;
		}*/
		return true;

	}
	function get_extra_images($product_id){
		if($product_id !=''){
			$query = $this->db->get_where('extra_images', array('product_id' => $product_id));
			$res = $query->result_array(); 
			
			return $res;
		}

	}

	function edit_product($data,$product_id){
		
		$this->db->where('id', $product_id);
		$this->db->update('product_desc', $data['product_info']); 

		/* Add Product Tags*/
		if(!empty($data['product_tags_array'])){
			
			$this->db->where('product_id', $product_id);
			$this->db->delete('product_tag'); 
			
			foreach($data['product_tags_array'] as $tag){
				$tags = array();
				$tags = array(
						'product_id'=>$product_id,
						'tag_id' => $tag,
						'status' => 1,
						'created_by' =>$this->session->userdata('user_id'),
						'created_datetime' => date('Y-m-d H:i:s')
					);
				$this->db->insert('product_tag', $tags); 

			}
		}
		/* Add Product Store / Product Availibility */
		if(!empty($data['product_avail_array'])){

			$this->db->where('product_id', $product_id);
			$this->db->delete('product_store'); 

			foreach($data['product_avail_array'] as $store){
				$stores = array();
				$stores = array(
						'product_id'=>$product_id,
						'store_id' => $store,
						'status' => 1
					);
				$this->db->insert('product_store', $stores); 

			}
		}

		/* Add Product Store / Product Availibility */
		if(!empty($data['category'])){

			$this->db->where(array('object_id'=>$product_id,'object_type' => 'product'));
			$this->db->delete('category_object_relationship'); 

			
			foreach ($data['category'] as $value) {
				$cat_data = array(
						'object_id'=>$product_id,
						'category_id' => $value,
						'object_type' => 'product',
						'modified_by'=>$this->session->userdata('user_id')
				);
				$this->db->insert('category_object_relationship', $cat_data);
				$this->update_category_product_count($value);
			}
		
		}

		if(!empty($data['attributes'])){

			$this->db->where(array('object_id'=>$product_id,'object_type' => 'product'));
			$this->db->delete('attribute_object_relationship'); 

			
			foreach ($data['attributes'] as $value) {
				$cat_data = array(
						'object_id'=>$product_id,
						'attribute_id' => $value,
						'object_type' => 'product',
						'modified_by'=>$this->session->userdata('user_id')
				);
				$this->db->insert('attribute_object_relationship', $cat_data);
				$this->update_attribute_product_count($value);
			}
		
		}		

		/* Product extra images */
		if(!empty($data['extra_images'])){
			foreach ($data['extra_images'] as $key => $value) {
				$extr_img = array(
						'product_id'=>$product_id,
						'product_images' => $value,
						'created_datetime' => date('Y-m-d H:i:s')
					);
				$this->db->insert('extra_images', $extr_img); 
			}
		}           

		return true;

	}

	function product_approve($product_id){
		if($product_id){
			$this->db->where('id', $product_id);
			if($this->db->update('product_desc', array('approve_reject'=>'A','modified_datetime'=>date('Y-m-d H:i:s'),'modified_by'=>$this->session->userdata('user_id')))){
				return true;
			}else{
				return false;
			}

		}
	}

	function product_reject($product_id,$reason){
		if($product_id!='' && $reason!=''){
			$this->db->where('id', $product_id);
			if($this->db->update('product_desc', array('approve_reject'=>'R','reason'=>$reason,'modified_datetime'=>date('Y-m-d H:i:s'),'modified_by'=>$this->session->userdata('user_id')))){
				return true;
			}else{
				return false;
			}

		}

	}

	function product_delete($product_id){
		if($product_id){
			$this->db->where('id', $product_id);
			if($this->db->update('product_desc', array('is_delete'=>1,'modified_datetime'=>date('Y-m-d H:i:s'),'modified_by'=>$this->session->userdata('user_id')))){
				return true;
			}else{
				return false;
			}
		}
	}

	function upload_product_image($product_img,$product_id){
		if($product_img!='' && $product_id!=''){
			$this->db->where('id', $product_id);
			$this->db->update('product_desc', array('image'=>$product_img,'modified_datetime'=>date('Y-m-d H:i:s'),'modified_by'=>$this->session->userdata('user_id')));
		}
	}

	function product_image_extra($product_id){
		if($product_id>0){
			$query = $this->db->get_where('extra_images', array('product_id' => $product_id));
			return $query->result_array();
		}
	}

	function delete_extra_img($image_id){
		if($image_id>0){
			$query = $this->db->get_where('extra_images', array('id' => $image_id));
			$res = $query->result_array();

			if(!empty($res)){
				$file = $res[0]['product_images'];
				unlink('./assets/products_extra/thumb/'.$file);
				unlink('./assets/products_extra/'.$file);
				$this->db->delete('extra_images', array('id' => $image_id)); 
			}

		}
	}

	function get_size_id($size){
        if($size!=''){
            $query = $this->db->query("select count(*) as cnt,id from product_size where size_text = '".strtoupper($size)."'");
            $result = $query->result_array();
            if($result[0]['cnt'] == 0){
                $data = array(
                    'size_text' => strtoupper($size) ,
                );
                $this->db->insert('product_size', $data); 
                return $this->db->insert_id();
            }else {
                return $result[0]['id']; 
            }
        }
    }

    function product_inventory($product_id){
    	$query = $query = $this->db->query("select a.stock_count,c.`size_text`,a.product_sku from product_inventory as a,product_desc as b,product_size as c where a.`product_id` = b.`id` and a.`size_id` = c.`id` and a.`product_id`=".$product_id);
		$res = $query->result_array();
		return $res;
    }

    function getbrand_code($brand_id){
        if($brand_id > 0){
            $query = $this->db->query("select brand_code from `brand_info` where user_id='".$brand_id."'");
            $result = $query->result_array();
            if(!empty($result)){
                return $result[0]['brand_code'];
            }else{
                return '';
            }
        }else{
            return '';
        }
    }
	
	function get_product_sku($product_id){
			$query = $this->db->query("select pi.product_sku from product_inventory as pi where pi.product_id = $product_id");
			$result = $query->result_array();
			$count = count($result);
			$i = 1;
			$product_sku = '';
			foreach($result as $row){
				if($count == 1){ $product_sku = $row['product_sku']; 
				}else if($i == $count)
				{ $product_sku = $product_sku.' '.$row['product_sku']; }
				else if($i<$count)
				{ $product_sku = $row['product_sku'].' , '.$product_sku; }
				$i++;
			}
			return $product_sku;
	}

	function all_attributes(){
		$query = $this->db->get_where('attributes');
		$res = $query->result_array();
		return $res;
	}

	function attributes_values($id){
		$query = $this->db->get_where('attribute_values_relationship', array('attribute_id' => $id));
		$res = $query->result_array();
		return $res;
	}

	function get_products_categories($id){
		$this->db->select('category_id'); 
		$query = $this->db->get_where('category_object_relationship', array('object_id' => $id,'object_type' => 'product'));
		$res = $query->result_array();
		$category = array();
		if(!empty($res)){
			foreach ($res as $value) {
				$category[] = $value['category_id'];
			}
		}
		return $category;
	}

	function get_products_attributes($id){
		$this->db->select('attribute_id'); 
		$query = $this->db->get_where('attribute_object_relationship', array('object_id' => $id,'object_type' => 'product'));
		$res = $query->result_array();
		$attributes = array();
		if(!empty($res)){
			foreach ($res as $value) {
				$attributes[] = $value['attribute_id'];
			}
		}
		return $attributes;
	}


	function quick_edit($data,$product_id){
		$this->db->where('id', $product_id);
		$this->db->update('product_desc', $data['product_info']); 

		if(!empty($data['category'])){

			$this->db->where(array('object_id'=>$product_id,'object_type' => 'product'));
			$this->db->delete('category_object_relationship'); 

			
			foreach ($data['category'] as $value) {
				$cat_data = array(
						'object_id'=>$product_id,
						'category_id' => $value,
						'object_type' => 'product',
						'modified_by'=>$this->session->userdata('user_id')
				);
				$this->db->insert('category_object_relationship', $cat_data);
				$this->update_category_product_count($value);
			}
		
		}

			if(!empty($data['product_tags_array'])){
			
			$this->db->where('product_id', $product_id);
			$this->db->delete('product_tag'); 
			
			foreach($data['product_tags_array'] as $tag){
				$tags = array();
				$tags = array(
						'product_id'=>$product_id,
						'tag_id' => $tag,
						'status' => 1,
						'created_by' =>$this->session->userdata('user_id'),
						'created_datetime' => date('Y-m-d H:i:s')
					);
				$this->db->insert('product_tag', $tags); 

			}
		}
	}

	 function get_cat_data($id = null){
      $query = $this->db->query("SELECT `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `category` WHERE 1");
        $res = $query->result_array();
        $tree = $this->buildTree($res);

        $res1 = $this->printAllValues1($tree,$this->ouptut,$id);
        return $res1;

    }

    function printAllValues1(array $array,$ouptut,$id=null) {

        foreach($array as $value){
            if($value['category_parent'] == -1){ $this->depth = 0; }
            $nbsp = '';
            $i = $this->depth;
            /*while($i>=0){
                $nbsp .='&nbsp;&nbsp;';
                $i--;
            }*/
            if(in_array($value['id'], $id)){
            /*$this->ouptut[$value['id']] = '<option value="'.$value['id'].'" class="level-'.$i.'" selected>'.$nbsp.$value['category_name'].'</option>';*/
            $this->ouptut[$value['id']] = '<option value="'.$value['id'].'" class="level-'.$i.'" selected>'.$nbsp.$value['category_name'].'->'.$this->get_slug($value['category_parent']).'</option>'; $this->slug ='';
            }else{
            /*$this->ouptut[$value['id']] = '<option value="'.$value['id'].'" class="level-'.$i.'">'.$nbsp.$value['category_name'].'</option>';*/
            $this->ouptut[$value['id']] = '<option value="'.$value['id'].'" class="level-'.$i.'">'.$nbsp.$value['category_name'].'->'.$this->get_slug($value['category_parent']).'</option>'; $this->slug ='';
            }
            
            if(isset($value['children'])){
                $this->depth = $this->depth+1;
                $this->printAllValues1($value['children'],$ouptut,$id);
            }else{
                $this->children = 0;
            }
        
        }
        return $this->ouptut;
    }


       function buildTree(array $elements, $parentId = '-1') {
    $branch = array();

    foreach ($elements as $element) {
        if ($element['category_parent'] == $parentId) {
            $children = $this->buildTree($elements, $element['id']);
            if ($children) {
                $element['children'] = $children;
            }
            $branch[] = $element;
        }
    }

    return $branch;
    }

    function update_category_product_count($cat_id){
    	$query1 = $this->db->query("select count(id) as cnt from category_object_relationship where object_type = 'product' and category_id = '".$cat_id."'");
			$res1 = $query1->result_array();	

			if(!empty($res1)){

				$query11 = $this->db->query("update category set count='".$res1[0]['cnt']."' where id = '".$cat_id."'");
			}
    }

    function update_attribute_product_count($cat_id){
    	$query1 = $this->db->query("select count(id) as cnt from attribute_object_relationship where object_type = 'product' and attribute_id = '".$cat_id."'");
			$res1 = $query1->result_array();	

			if(!empty($res1)){

				$query11 = $this->db->query("update attribute_values_relationship set count='".$res1[0]['cnt']."' where id = '".$cat_id."'");
			}
    }

    function get_slug($id){
        
        if($id!='-1'){
        $query = $this->db->query('select category_name,category_parent from category where id = "'.$id.'"');
        $res = $query->result_array();
        if(!empty($res)){
            foreach($res as $val){

                $this->get_slug($val['category_parent']);

                $this->slug = '->'.$val['category_name'].$this->slug; 

            }
            return  $this->slug ;
        }
        }
        
    }
	
	
	public function extara_images($proId='')
	{
		$this->db->select('product_id,product_images');
		$this->db->from('extra_images');
		$this->db->where('product_id', $proId);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}

	public function product_children($product_sku){ 
		if(!empty($product_sku)){
			$this->db->select('a.id,a.sku_number,a.stock_count');
			$this->db->from('product_desc as a');
			$this->db->like('a.parent_id',$product_sku,'both');		
			$query = $this->db->get();			
			if($query->num_rows()>0)
			{
				return $query->result_array();
			}			
		}
		return false;
	}
 } 
?>
