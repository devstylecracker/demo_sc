<?php
class Manage_orders_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }    

    public function updateOrder($orderPrdId,$order_status)
	{	$send_email = $order_status;
		if($order_status == 5) $send_email = 0;
		$this->db->where('id', $orderPrdId);
		$result = $this->db->update('order_product_info', array('order_status'=>$order_status)); 
				
		if($result)
		{
			return true;
		}else
		{
			return false;
		}

	}

	public function order_status($order_status)
	{
		$query = $this->db->query("SELECT a.user_id,b.order_id,a.first_name,a.last_name,b.order_status,b.product_id,c.name,(b.`product_qty`*c.price) as price,a.created_datetime  FROM `order_info` a,order_product_info b,product_desc c where b.order_status = $order_status AND b.order_id = a.id AND b.product_id = c.id ");
		$result = $query->result_array();
		// echo "<pre>";
		// print_r($result);exit;
        return $result;
	}

	function getOrdersPay($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL){   

    	$search_cond = '';
    	$search_cond_have = '';
        if($search_by == '1'){ $search_cond = " AND a.`id` = '".strip_tags(trim($table_search))."'"; }
		else if($search_by == '2'){ $search_cond = " AND a.`first_name` like '%".strip_tags(trim($table_search))."%'"; }
			else if($search_by == '3'){ $search_cond = " AND b.`company_name` like '%".strip_tags(trim($table_search))."%'"; }
			else if($search_by == '4'){ $search_cond = " AND a.`created_datetime` like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '5'){ 

				$tb_status1=strtolower(strip_tags(trim($table_search)));
				if($tb_status1 == "processing")
				{
					$apr_status = 1;

				}else if($tb_status1 == "confirmed")
				{					
					$apr_status = 2;
				}else if($tb_status1 == "dispatched")
				{
					$apr_status = 3;

				}else if($tb_status1 == "delivered")
				{
					$apr_status = 4;

				}else if($tb_status1 == "cancelled")
				{
					$apr_status = 5;

				}else if($tb_status1 == "return")
				{
					$apr_status = 6;
					
				}else if($tb_status1 == "fake")
				{
					$apr_status = 7;

				}else if($tb_status1 == "curated")
				{
					$apr_status = 8;
				}
				else
				{
					$apr_status = '';
				}

				if($tb_status1!='')
				{
					$search_cond = " AND d.`order_status` = '".$apr_status."'";
				}
				
			 }else if($search_by == '6'){ $search_cond_have = "having daysLapsed ".trim($table_search)." "; }
				else if($search_by == '7'){ 
					//echo $table_search;
					$pay_status ="";
				$tb_status2=strtolower(strip_tags(trim($table_search)));

				if($tb_status2=="create invoice" )
				{
					$pay_status = 1;

				}else if($tb_status2 == "cleared" )
				{					
					$pay_status = 2;

				}else if($tb_status2=="pending" )
				{
					$pay_status = 3;
				}
				else
				{
					$pay_status = "";
				}
				
				if($pay_status!="")
				{
					$search_cond = " AND d.payment_status=".$pay_status." "; 
				}else 
				{
					$search_cond = '';
				}		
									
				
				}else if($search_by == '8'){

					$date_from = $this->input->post('date_from');
					$date_to = $this->input->post('date_to');
					
					if($date_from=='' && $date_to == '' )
					{
						$tb_arr = explode(',',$table_search);
						$date_from = $tb_arr[0];
						$date_to = $tb_arr[1];
					}

					$search_cond = "AND a.`created_datetime` >= '".$date_from." 00:00:00'  and a.`created_datetime` <= '".$date_to." 23:59:59'";

				}else{ $search_cond ='';
					  $search_cond_have = "";
					}
				

				//$search_cond="";
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}

		if($this->session->userdata('role_id') == 6 ){ 
			$search_cond = $search_cond." and a.`brand_id`=".$this->session->userdata('user_id');
		}

		
         $query = $this->db->query("select distinct a.order_unique_no, (select look_id as look from cart_info where order_id=d.`order_id` and product_id = d.`product_id` and product_size=f.id) as look_id,(select platform_info as look from cart_info where order_id=d.`order_id` and product_id = d.`product_id` and product_size=f.id) as platform_info, f.size_text,a.`id` as order_id,d.id as order_prd_id, a.`user_id`, a.`brand_id`, a.`order_tax_amount`, a.`cod_amount`, a.`shipping_amount`, a.`order_sub_total`, a.`order_total`, a.`pay_mode`,a.`mihpayid` as tranid, a.`created_datetime`, d.`modified_datetime`, a.`order_status`, a.`first_name`, a.`last_name`, a.`email_id`, a.`mobile_no`, a.`shipping_address`, a.`pincode`, a.`city_name`, a.`state_name`,c.`state_name`,b.`company_name`,b.`brand_code`, d.`product_id`, d.`product_qty`,d.`product_size`,e.`name` as product_name,e.image, d.`product_price`, (select (sum(oif.order_total)-sum(oif.`order_tax_amount`)-sum(oif.`cod_amount`)-sum(oif.`shipping_amount`)) from order_info oif where oif.order_unique_no = a.order_unique_no) total_order_amt, a.order_unique_no, d.`order_status`, DATEDIFF(CURDATE(),DATE(a.`created_datetime`)) as daysLapsed, payment_status, b.cpa_percentage as commission_rate, b.payment_gateway_charges, b.service_tax, b.integration_charges, b.cpa_percentage, b.commission_cat, (select cs.coupon_amount from coupon_settings cs where a.coupon_code = cs.coupon_code limit 1) coupon_amount, (select cs.discount_type from coupon_settings cs where a.coupon_code = cs.coupon_code limit 1) discount_type, (select cs.discount_type from coupon_settings cs where a.coupon_code = cs.coupon_code limit 1) coupon_code
			 from order_info as a
			 left join order_product_info as d on d.`order_id` = a.`id`
			 left join brand_info as b on a.`brand_id` = b.`user_id`
			 left join states as c on a.`state_name` = c.`id`
			 left join product_desc as e on e.`id` = d.`product_id`
			 left join product_size as f on d.`product_size` = f.id
			 where 1 and  a.`order_status`!=9 and  d.`order_status`!=7 ".$search_cond." ".$search_cond_have." order by a.`created_datetime` desc ".$limit_cond);
         // /d.`order_status`!=5  and 
      	//echo $this->db->last_query();exit;
        $result = $query->result_array();
        return $result;
    }

    public function updatepayOrder($orderPrdId,$orderpay_status)
	{
		$this->db->where('id', $orderPrdId);
		$result = $this->db->update('order_product_info', array('payment_status'=>$orderpay_status)); 
		
		if($result)
		{
			return true;
		}else
		{
			return false;
		}
	}
	
	public function send_mail($order_id)
	{
		 $query = $this->db->query("select order_id,product_id,user_id,cart_id,product_price,brand_id,order_tax_amount,cod_amount from order_product_info as a,order_info as b where a.order_id = $order_id ");
        $result = $query->result_array();
        if(!empty($result)) return $result; else return null;
	}
	
	public function get_order_unique_no($order_id){
		$query = $this->db->query("select b.order_unique_no,b.user_id,b.first_name,b.last_name,b.order_display_no,b.mobile_no,b.email_id,a.product_id from order_product_info as a,order_info as b where a.order_id = $order_id AND a.order_id = b.id AND a.user_id = b.user_id");
        $result = $query->result_array();
        if(!empty($result)) return $result; else return null;
	}
	
	
	function su_get_order_info($order_id,$product_order_id,$product_id,$order_status=null){
        $query = $this->db->query('select id,user_id from `order_info` where order_unique_no = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

            $order_ids = implode(",",$order_info);          

            $query = $this->db->query('select a.`order_id`, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`,  a.`product_price`,a.`created_datetime`,b.`name`,b.`image`,b.`brand_id`,c.size_text,d.company_name,d.min_del_days,d.max_del_days,e.order_tax_amount, e.cod_amount, e.shipping_amount,e.order_total,(select a.`product_qty`*(b.price-((b.price*discount_percent)/100)) from discount_availed as da where da.brand_id = b.brand_id AND NOW() >= da.start_date AND NOW() <= da.end_date AND (da.discount_type_id=1 ) AND da.status = 1 AND da.is_delete=0 )as discount_price,a.coupon_code,e.bms_discount  from order_product_info as a,product_desc as b,product_size as c,`brand_info` as d,order_info as e where a.`product_id` = b.`id` and a.`order_id` in ('.$order_ids.') and a.`user_id`='.$user_id.'  and a.product_size = c.`id` and d.`user_id` = b.`brand_id` and a.`order_id`=e.`id` ' );            
            $query_res = $query->result_array();
			 /*echo $this->db->last_query();exit;*/
            return $query_res;

        }
    }
    
	function su_get_order_price_info($order_id,$product_order_id){
        $query = $this->db->query('select id,user_id from `order_info` where order_unique_no = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info);

        $query = $this->db->query('select sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no, a.shipping_address,a.pincode,a.city_name,s.state_name,a.pay_mode,a.country_name,c.product_price from order_info as a,states as s,order_product_info as c where a.`state_name` = s.`id` AND a.id = '.$product_order_id.' and a.id in ('.$order_ids.') AND a.id = c.order_id ');

            $query_res = $query->result_array();
            return $query_res;
        }
    }
	
	function get_user_info($user_id){
        $query = $this->db->get_where('user_login', array('id' => $user_id));
        return $query->result_array();
    }

    function getBrands_info($user_id){
        $query = $this->db->get_where('brand_info', array('user_id' => $user_id));
        return $query->result_array();
    }
	
	function get_order_count($product_order_id){
        $query = $this->db->query("select * from order_product_info where order_id = $product_order_id");
        $query_res = $query->result_array();
        return $query_res;
    }
	
	function update_email($order_id,$product_id,$send_email=null){
	   if($send_email == 5) $send_email = 1;
		
       if($order_id!='' && $product_id!=''){
        
            $data = array(
               'product_id' => $product_id ,
               'order_id' => $order_id,
			   'send_email' => $send_email
            );
			 $this->db->where(array('product_id'=>$product_id,'order_id' => $order_id));
            $this->db->update('order_product_info', $data);
       
		}
	}

	function getViewOrdersExcel($search_by=NULL,$table_search=NULL,$page=NULL ,$per_page=NULL ,$search_by2=NULL ,$pagetype=NULL ,$utm_medium=NULL ,$is_stylist_download=''){   

    	$search_cond = '';
    	$search_cond_have = '';
        if($search_by == '1'){ $search_cond = " AND a.`id` = '".strip_tags(trim($table_search))."'"; }
		else if($search_by == '2')
			{
				
				/* if(!empty($table_search[1]))
				{
					$search_cond = " AND CONCAT(a.first_name,' ',a.last_name) as first_name like '%".$table_search[0]." ".$table_search[1]."%'";
				}
				else{ */
					$search_cond = " AND a.`first_name` like '%".$table_search[0]."%'";
				/* } */

				/*if(is_array($table_search)){ $search_cond = " AND a.`first_name` like '%".strip_tags(trim(@$table_search[0]))."%'"; }else{ $search_cond = " AND a.`first_name` like '%".strip_tags(trim(@$table_search))."%'"; }*/
			}
			else if($search_by == '3'){ $search_cond = " AND b.`company_name` like '%".strip_tags(trim($table_search))."%'"; }
			else if($search_by == '4'){ $search_cond = " AND a.`created_datetime` like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '5'){ 

				$tb_status1=strtolower(strip_tags(trim($table_search)));

				if($tb_status1 == "processing")
				{
					$apr_status = 1;

				}else if($tb_status1 == "confirmed")
				{					
					$apr_status = 2;
				}else if($tb_status1 == "dispatched")
				{
					$apr_status = 3;

				}else if($tb_status1 == "delivered")
				{
					$apr_status = 4;

				}else if($tb_status1 == "cancelled")
				{
					$apr_status = 5;

				}else if($tb_status1 == "return")
				{
					$apr_status = 6;
				}
				else if($tb_status1 == "fake")
				{
					$apr_status = 7;

				}else if($tb_status1 == "curated")
				{
					$apr_status = 8;
				}
				else
				{
					$apr_status = '';
				}

				if($tb_status1!='')
				{
					$search_cond = " AND d.`order_status` = '".$apr_status."'";
				}
				
			 }else if($search_by == '6'){ $search_cond_have = "having daysLapsed ".trim($table_search)." "; }
				else if($search_by == '7'){ 
					//echo $table_search;
					$pay_status ="";
				$tb_status2=strtolower(strip_tags(trim($table_search)));

				if($tb_status2=="create invoice" )
				{
					$pay_status = 1;

				}else if($tb_status2 == "cleared" )
				{					
					$pay_status = 2;

				}else if($tb_status2=="pending" )
				{
					$pay_status = 3;
				}
				else
				{
					$pay_status = "";
				}
				
				if($pay_status!="")
				{
					$search_cond = " AND d.payment_status=".$pay_status." "; 
				}else 
				{
					$search_cond = '';
				}		
									
				
				}else if($search_by == '8'){

					$date_from = $this->input->post('date_from');
					$date_to = $this->input->post('date_to');
					if($date_from=='' && $date_to == '' )
					{
						$tb_arr = explode(',',$table_search);
						$date_from = $tb_arr[0];
						$date_to = $tb_arr[1];
					}

					$search_cond = " AND a.`created_datetime` >= '".$date_from." 00:00:00'  and a.`created_datetime` <= '".$date_to." 23:59:59'";

				}else if($search_by == '9'){
					$search_cond = " AND a.`order_display_no` = '".strip_tags(trim($table_search))."'";
				}else{ $search_cond ='';
					  $search_cond_have = "";
					}
				

				//$search_cond="";
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}

		if($this->session->userdata('role_id') == 6 ){ 
			$search_cond = $search_cond." and a.`brand_id`=".$this->session->userdata('user_id');
		}

		if($search_by2 == '1'){
				$search_cond = $search_cond." AND a.brand_id != ".SCBOX_BRAND_ID." ";
		}else if($search_by2 == '2'){
				$search_cond = $search_cond." AND a.brand_id = ".SCBOX_BRAND_ID." ";
		}
		
		if($pagetype=='affiliate')
		{ 
			$search_cond_have = "having sc_medium ='".trim($utm_medium)."' ";
			
		}
		

        // $query = $this->db->query("select e.id,e.slug,(select product_sku from `product_inventory` where  `product_id` = d.`product_id` and `size_id`=d.`product_size` limit 0,1 ) as product_sku,(select name from `product_category` where id =e.product_cat_id limit 0,1) as product_cat_id,(select name from `product_sub_category` where id =e.product_sub_cat_id limit 0,1) as product_sub_cat_id,(select look_id as look from cart_info where order_id=d.`order_id` and product_id = d.`product_id` and product_size=f.id limit 0,1) as look_id,(select platform_info as look from cart_info where order_id=d.`order_id` and product_id = d.`product_id` and product_size=f.id limit 0,1) as platform_info ,(Select sum(h.product_price) from order_info as g,order_product_info as h where g.id = h.order_id and g.order_unique_no = a.order_unique_no group by g.order_unique_no limit 0,1) as OrderProductTotal,
      		// (Select sum(i.product_price) from order_product_info as i  group by i.order_id having i.order_id = d.order_id limit 0,1) as OrderIdProductSum, f.size_text,a.`id` as order_id,d.id as order_prd_id, a.`user_id`, a.`brand_id`, a.`order_tax_amount`, a.`cod_amount`, a.`shipping_amount`, a.`order_sub_total`, (Select SUM(`order_total`) from order_info where order_unique_no =a.order_unique_no group by order_unique_no limit 0,1) as order_total , a.`pay_mode`,a.`mihpayid` as tranid, a.`created_datetime`, a.`modified_datetime`, a.`order_status`, a.`first_name`, a.`last_name`, a.`email_id`, a.`mobile_no`, a.`shipping_address`, a.`pincode`, a.`city_name`,a.`order_display_no`, a.`state_name`,c.`state_name`,b.`company_name`,b.`brand_code`, d.`product_id`, d.`product_qty`,d.`product_size`,e.`name` as product_name,e.image, d.`product_price`, d.`order_status`, DATEDIFF(CURDATE(),DATE(a.`created_datetime`))  as daysLapsed, payment_status, b.cpa_percentage as commission_rate,d.coupon_code,a.order_unique_no,e.`price` as actual_product_price,(select sc_source from cart_info as ci where ci.order_id=a.`id` limit 0,1) as sc_source,(select sc_medium from cart_info as ci where ci.order_id=a.`id` limit 0,1) as sc_medium,(select sc_campaign from cart_info as ci where ci.order_id=a.`id` limit 0,1) as sc_campaign,a.is_paymentdone,(select an.answer from users_answer as ua,answers as an where a.user_id = ua.user_id AND (ua.question_id = 8 OR ua.question_id  = 17 ) AND ua.answer_id = an.id limit 0,1) as user_age,if(ui.gender=1,'female','male') as gender from order_info as a,brand_info as b,states as c, order_product_info as d, product_desc as e,product_size as f,user_info as ui where a.`brand_id` = b.`user_id` and a.`state_name` = c.`id` and d.`order_id` = a.`id` and e.`id` = d.`product_id` and d.`product_size` =f.id and a.`order_status`!=9 AND a.user_id = ui.user_id ".$search_cond." ".$search_cond_have." order by a.`created_datetime` desc ".$limit_cond);

          $query = $this->db->query("select e.id,e.slug,(select product_sku from `product_inventory` where  `product_id` = d.`product_id` and `size_id`=d.`product_size` limit 0,1 ) as product_sku,(select look_id as look from cart_info where order_id=d.`order_id` and product_id = d.`product_id` and product_size=f.id limit 0,1) as look_id,(select platform_info as look from cart_info where order_id=d.`order_id` and product_id = d.`product_id` and product_size=f.id limit 0,1) as platform_info ,(Select sum(h.product_price) from order_info as g,order_product_info as h where g.id = h.order_id and g.order_unique_no = a.order_unique_no group by g.order_unique_no limit 0,1) as OrderProductTotal,
      		(Select sum(i.product_price) from order_product_info as i  group by i.order_id having i.order_id = d.order_id limit 0,1) as OrderIdProductSum, f.size_text,a.`id` as order_id,d.id as order_prd_id, a.`user_id`, a.`brand_id`, a.`order_tax_amount`, a.`cod_amount`, a.`shipping_amount`, a.`order_sub_total`, (Select SUM(`order_total`) from order_info where order_unique_no =a.order_unique_no group by order_unique_no limit 0,1) as order_total , a.`pay_mode`,a.`mihpayid` as tranid, a.`created_datetime`, a.`modified_datetime`, a.`order_status`, a.`first_name`, a.`last_name`, a.`email_id`, a.`mobile_no`, a.`shipping_address`, a.`pincode`, a.`city_name`,a.`order_display_no`, a.`state_name`,c.`state_name`,b.`company_name`,b.`brand_code`, d.`product_id`, d.`product_qty`,d.`product_size`,e.`name` as product_name,e.image, d.`product_price`, d.`order_status`, DATEDIFF(CURDATE(),DATE(a.`created_datetime`))  as daysLapsed, payment_status, b.cpa_percentage as commission_rate,d.coupon_code,a.order_unique_no,e.`price` as actual_product_price,(select sc_source from cart_info as ci where ci.order_id=a.`id` limit 0,1) as sc_source,(select sc_medium from cart_info as ci where ci.order_id=a.`id` limit 0,1) as sc_medium,(select sc_campaign from cart_info as ci where ci.order_id=a.`id` limit 0,1) as sc_campaign,a.is_paymentdone,ui.birth_date,if(ui.gender=1,'female','male') as gender from order_info as a,brand_info as b,states as c, order_product_info as d, product_desc as e,product_size as f,user_info as ui where a.`brand_id` = b.`user_id` and a.`state_name` = c.`id` and d.`order_id` = a.`id` and e.`id` = d.`product_id` and d.`product_size` =f.id and a.`order_status`!=9 AND a.user_id = ui.user_id ".$search_cond." ".$search_cond_have." order by a.`created_datetime` desc ".$limit_cond);

        $result = $query->result_array();
        //echo $this->db->last_query();exit;
		//echo "<pre>";print_r($result);echo "</pre>";exit();
		if($is_stylist_download == '1'){
			$user_data = array();$i=0;
			if(!empty($result)){
				foreach($result as $val){
					$user_data[$i] = $this->Scbox_model->get_scbox_userdata($val['user_id']);
					$i++;
				}
			}
			return $user_data;
		}
        return $result;
    }

    function product_variations($product_id){
    	$query = $this->db->query("select b.name from product_variations as a,`product_variation_type` as b where a.product_var_type_id = b.`id` and a.`product_id`=".$product_id);
    	$result = $query->result_array();
    	return $result;
    }

    function product_variations_value($product_id){
    	$query = $this->db->query("select b.name from product_variations as a,`product_variation_value` as b where a.	product_var_val_id = b.`id` and a.`product_id`=".$product_id);
    	$result = $query->result_array();
    	return $result;
    }

     function getUserCoupon($couponCode,$orderDate, $CbrandId = "")
    {

      if($CbrandId!='')
      {
       /* $brand_cond =' AND `brand_id`='.$CbrandId;*/
       $brand_cond =' AND (brand_id = "'.$CbrandId.'" || brand_id=0) ';
      }else
      {
        $brand_cond = '';
      }
        /*$query = $this->db->query("SELECT `coupon_code`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `brand_id`, `products`, `exclude_products`,`coupon_used_count`,`usage_limit_per_coupon`,`stylecrackerDiscount`,`individual_use_only`,`coupon_email_applied` FROM `coupon_settings` WHERE '".$orderDate."' BETWEEN `coupon_start_datetime` AND `coupon_end_datetime` AND`status`=1 AND `is_delete`=0  AND `coupon_code`='".$couponCode."' ".$brand_cond."; "); */
        $query = $this->db->query("SELECT `coupon_code`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `brand_id`, `products`, `exclude_products`,`coupon_used_count`,`usage_limit_per_coupon`,`stylecrackerDiscount`,`individual_use_only`,`coupon_email_applied` FROM `coupon_settings` WHERE `status`=1 AND `is_delete`=0  AND `coupon_code`='".$couponCode."' ".$brand_cond."; "); 
            //return $this->db->last_query();
            $result = $query->result_array();
           
            if(!empty($result))
            {
              /* $coupon_info['coupon_products'] =  $coupon_products.$result[0]['products'];*/
               $coupon_info['coupon_amount'] = $result[0]['coupon_amount']; 
               $coupon_info['brand_id'] = $result[0]['brand_id']; 
               $coupon_info['coupon_products'] = $result[0]['products'];
               $coupon_info['coupon_min_spend'] = $result[0]['min_spend'];
               $coupon_info['coupon_max_spend'] = $result[0]['max_spend'];
               $coupon_info['coupon_discount_type'] = $result[0]['discount_type'];
               $coupon_info['coupon_used_count'] = $result[0]['coupon_used_count'];
               $coupon_info['usage_limit_per_coupon'] = $result[0]['usage_limit_per_coupon'];
               $coupon_info['stylecrackerDiscount'] = $result[0]['stylecrackerDiscount'];
               $coupon_info['individual_use_only'] = $result[0]['individual_use_only'];
               $coupon_info['coupon_email_applied'] = $result[0]['coupon_email_applied'];
            }  

            if(!empty($coupon_info))
            {
              return $coupon_info;
            }else
            {
                return 0;
            }               
    }

     function calculateCouponAmount($cartproduct=array(),$totalProductPrice=null)
  	{   
  		$data = array();  		
      if(!empty($cartproduct))
      {
        $coupon_product_price = 0;
        foreach($cartproduct as $val)
        {

          /*if(@$val['discount_percent']!='' || @$val['discount_percent']!=0)
          {
             $price = $val['discount_price'];            
          }else
          {
             $price = $val['product_price'];            
          }*/
          $price = $val['product_price']; 

          if(isset($val['coupon_code']) && $val['coupon_code']!='')
          {
            $coupon_info = $this->getUserCoupon($val['coupon_code'],$val['created_datetime'],$val['brand_id']);            
            $coupon_brand = $coupon_info['brand_id'];
            $coupon_products = $coupon_info['coupon_products'];
            if($coupon_products!='' || $coupon_products!=0)
            {
              $coupon_products_arr =explode(',',$coupon_products);
            }
            $coupon_min_spend = $coupon_info['coupon_min_spend'];
            $coupon_max_spend = $coupon_info['coupon_max_spend'];
            $coupon_discount_type = $coupon_info['coupon_discount_type']; 
            $coupon_stylecracker =  $coupon_info['stylecrackerDiscount'];
            $individual_use_only =  $coupon_info['individual_use_only'];   
            $data['coupon_code'] = $val['coupon_code'];
            $apply_email =  $coupon_info['coupon_email_applied'];
            

             if($coupon_brand==$val['brand_id'] && $coupon_brand!=0 )
              {        
                if($coupon_products!=''&& $coupon_products!=0)
                {
                   //coupon_discount_type =3 (Product discount)
                  if($coupon_discount_type==3)
                  {
                    if(in_array($val['product_id'], $coupon_products_arr))
                    {              
                      if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                      {                        
                        $data['coupon_discount'] = $coupon_info['coupon_amount'];  
                        $data['coupon_code'] = $val['coupon_code'];  
                        $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                        $data['coupon_amount'] =  $coupon_info['coupon_amount']; 
                        $data['coupon_brand'] = $coupon_brand;                               
                      } 
                    }
                  } 
                   //coupon_discount_type =4 (Product % discount)
                  if($coupon_discount_type==4)
                  {
                    if(in_array($val['product_id'], $coupon_products_arr))
                    {              
                      if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                      {
                        $coupon_percent = $coupon_info['coupon_amount'];
                        $data['coupon_discount'] = $price*($coupon_percent/100); 
                        $data['coupon_code'] = $val['coupon_code']; 
                        $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                        $data['coupon_amount'] =  $coupon_info['coupon_amount']; 
                        $data['coupon_brand'] = $coupon_brand;                        
                      } 
                    }
                  }                 

                }else
                { 
                   //coupon_discount_type =1 (cart discount)
                  if($coupon_discount_type==1)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $data['coupon_discount'] = $coupon_info['coupon_amount']; 
                      $data['coupon_code'] = $val['coupon_code']; 
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount']; 
                      $data['coupon_brand'] = $coupon_brand;                     
                    }
                  }
                   //coupon_discount_type =2 (cart % discount)
                  if($coupon_discount_type==2)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $coupon_percent = $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount']; 
                      $data['coupon_brand'] = $coupon_brand;                         
                    }
                  }
                }          
              }

            /* if($coupon_brand==0 && strtolower($coupon_code)=='sccart20')
	        { 
	            $coupon_product_price = $coupon_product_price+$price;
	            if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                {
                  $coupon_percent = $coupon_info['coupon_amount'];  
                  $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                  $data['coupon_code'] = $val['coupon_code'];                      
                }
	        } */

	        if(($coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only!=1  )|| ( $coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only==1 ))
	        { 
	        	$coupon_product_price = $totalProductPrice;
	            //coupon_discount_type =1 (cart discount)
                  if($coupon_discount_type==1)
                  {
                    //$coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $data['coupon_discount'] = $coupon_info['coupon_amount']; 
                      $data['coupon_code'] = $val['coupon_code'];
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount']; 
                      $data['coupon_brand'] = $coupon_brand;                      
                    }
                  }
                   //coupon_discount_type =2 (cart % discount)
                  if($coupon_discount_type==2)
                  {
                    //$coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $coupon_percent = $coupon_info['coupon_amount'];  
                      $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type']; 
                      $data['coupon_amount'] =  $coupon_info['coupon_amount']; 
                      $data['coupon_brand'] = $coupon_brand;                       
                    }
                  }
	        }                           

          }else
          {
            //$data['coupon_discount']=0;
            //$data['coupon_code'] = '';
          }         
        }      
      } 
      return $data;      
  	}

  	function multi_array_search($search_for, $search_in) {
	    foreach ($search_in as $element) {
	        if ( ($element === $search_for) || (is_array($element) && multi_array_search($search_for, $element)) ){
	            return true;
	        }
	    }
	    return false;
	}

	function calculate_product_discountAmt($product_price,$totalproductprice,$discountType,$discountAmt)
    { /*echo $product_price.'--'.$totalproductprice.'--'.$discountType.'--'.$discountAmt.'<br>';*/
      if($product_price!='' && $totalproductprice!=0 && $discountAmt!='')
      {  

        if($discountType==1)/*1-Cart-Discount*/
        {
            $percent = ($product_price*100)/$totalproductprice;
            $DiscountAmt = ($discountAmt*$percent)/100;            
          /*  return $DiscountAmt;*/
            $productDiscountAmt = $product_price-$DiscountAmt;         
            return $productDiscountAmt;
            

        }else if($discountType==2) /*2-Cart-%-Discount*/ 
        {     
            $percent = $discountAmt;  
            $DiscountAmt = ($product_price*$percent)/100;
            return $DiscountAmt;
            /*$productDiscountAmt = $product_price-$DiscountAmt;  
            return $productDiscountAmt;*/
            
        }   
       
      }else
      {
        return 0;
      }
    }

    function su_get_order_price_info_res($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where order_unique_no = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info);

        $query = $this->db->query('select sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no,a.brand_id, a.coupon_code,a.coupon_amount,a.shipping_address,a.pincode,a.city_name,s.state_name,a.pay_mode,a.order_status,a.user_id,a.state_name as state_id,a.it_state,a.country_name,a.confirm_mobile_no,a.created_datetime,a.order_display_no,a.bms_discount from order_info as a,states as s where a.`state_name` = s.`id` and a.id in ('.$order_ids.') group by a.brand_id' );

            $query_res = $query->result_array();
            return $query_res;
        }
    }


    function getOrders($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL,$search_by2=NULL){

    	$search_cond = '';
        if($search_by == '1'){ $search_cond = " AND a.`id` = '".strip_tags(trim($table_search))."'"; }
        else if($search_by == '9'){ $search_cond = " AND a.`order_display_no` = '".strip_tags(trim($table_search))."'"; }
		else if($search_by == '2'){ $search_cond = " AND CONCAT_WS(' ',a.`first_name`,a.`last_name`) like '%".strip_tags(trim($table_search))."%'"; }
			else if($search_by == '3'){ $search_cond = " AND b.`company_name` like '%".strip_tags(trim($table_search))."%'"; }
			else if($search_by == '4'){ $search_cond = " AND a.`created_datetime` like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '5'){ 

				$tb_status=strtolower(strip_tags(trim($table_search)));
				if($tb_status=="processing")
				{
					$apr_status = 1;

				}else if($tb_status == "confirmed")
				{					
					$apr_status = 2;
				}else if($tb_status == "dispatched")
				{
					$apr_status = 3;

				}else if($tb_status == "delivered")
				{
					$apr_status = 4;

				}else if($tb_status == "cancelled")
				{
					$apr_status = 5;

				}else if($tb_status == "return")
				{
					$apr_status = 6;

				}else if($tb_status == "fake")
				{
					$apr_status = 7;
				}else if($tb_status == "curated")
				{
					$apr_status = 8;
				}
				else
				{
					$apr_status = 0;
				}

				if($apr_status!=0)
				{

					$query = $this->db->query("select GROUP_CONCAT(order_id) as order_id from order_product_info where order_status = ".$apr_status." and order_status !=9 and 	created_datetime >= DATE_SUB(CURDATE(), INTERVAL 50 DAY) order by id desc  ");
					$res = $query->result_array();
					if(!empty($res[0]['order_id'])){
						$search_cond = " AND a.`id` IN (".rtrim($res[0]['order_id'],',').")";
					}else
					{
						$search_cond = '';
					}
				}
				
			 }else if($search_by == '6'){ $search_cond = " AND a.order_unique_no ='".strip_tags(trim($table_search))."'"; }
				/*else if($search_by == '7'){ $search_cond = " AND d.company_name like '%".strip_tags(trim($table_search))."%'"; }	*/
				else if($search_by == '8'){
					if(!empty($_POST))
					{
					$date_from = $this->input->post('date_from');
					$date_to = $this->input->post('date_to');
					if($date_from=='' && $date_to == '' )
					{
						$tb_arr = explode(',',$table_search);
						$date_from = @$tb_arr[0];
						$date_to = @$tb_arr[1];
					}
					}
					else
					{
							$table_search = explode(',',base64_decode($table_search));
							$date_from = $table_search[0];
							$date_to = $table_search[1];
					}

					$search_cond = " AND a.`created_datetime` >= '".$date_from." 00:00:00'  and a.`created_datetime` <= '".$date_to." 23:59:59'";

				}else if($search_by == '10'){ 

					$box_attribute = strtolower(strip_tags(trim($table_search)));

					if($box_attribute=='gift')
					{
						$scbox_gift_object = $this->Scbox_model->get_object('scbox_gift')[0]['object_id'];
						$this->db->select('object_meta_key');
						$this->db->from('object_meta');
						$this->db->where('object_id',$scbox_gift_object);
						$this->db->order_by('created_datetime','DESC');
						$this->db->limit('50');
						$query = $this->db->get();
						if($query->num_rows()>0)
						{
							$res = $query->result_array();
							$res_orderIds = array_column($res, 'object_meta_key');
							$orderDisplayNo = "'" . implode ( "', '", $res_orderIds ) . "'";
						}
						if(!empty($res)){
							$search_cond = " AND a.order_display_no IN (".strip_tags(trim($orderDisplayNo)).")  "; 
						}
					}else if($box_attribute=='curated')
					{
						$box_order_products = $this->Scbox_model->get_object('box_order_products')[0]['object_id'];
						//$query = $this->db->query("select GROUP_CONCAT(object_meta_key) as orderDisplayNo from object_meta where object_id = ".$scbox_gift_object." ");
						//$query = $this->db->query("select  GROUP_CONCAT(QUOTE(object_meta_key)) as orderDisplayNo from object_meta where object_id = ".$box_order_products." ");
						
						$this->db->select('object_meta_key');
						$this->db->from('object_meta');
						$this->db->where('object_id',$box_order_products);
						$this->db->order_by('created_datetime','DESC');
						$query = $this->db->get();
						if($query->num_rows()>0)
						{
							$res = $query->result_array();
							$res_orderIds = array_column($res, 'object_meta_key');
							$orderDisplayNo = "'" . implode ( "', '", $res_orderIds ) . "'";
						}
						//$res = $query->result_array();
						if(!empty($res)){

							//$search_cond = " AND a.order_display_no IN (".strip_tags(trim($res[0]['orderDisplayNo'])).")"; 
							$search_cond = " AND a.order_display_no IN (".strip_tags(trim($orderDisplayNo)).")"; 
						}
					}else
					{
						$search_cond ='';
					}
					

				}else{ $search_cond ='AND 1=1'; }
				

				//$search_cond="";
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}

		if($this->session->userdata('role_id') == 6 ){
			$search_cond = $search_cond." and (a.confirm_mobile_no = 1 or a.`pay_mode`=2) and a.`brand_id`=".$this->session->userdata('user_id');
		}  
		
		if($search_by2 == '1'){
				$search_cond = $search_cond." AND a.brand_id != ".SCBOX_BRAND_ID." ";
		}else if($search_by2 == '2'){
				$search_cond = $search_cond." AND a.brand_id = ".SCBOX_BRAND_ID." ";
		}

      	/*$query = $this->db->query("select (select platform_info from cart_info as ci where ci.order_id=a.`id` limit 0,1) as platform_info, (sum(d.product_price) +(SUM(a.cod_amount)+SUM(a.shipping_amount)+SUM(a.order_tax_amount))) as OrderTotal ,(a.order_total-(SUM(a.cod_amount)+SUM(a.shipping_amount)+SUM(a.order_tax_amount))) as OrderProductTotal , a.`user_id`, a.`order_sub_total`, a.`order_total`, a.`pay_mode`, a.`created_datetime`, a.`modified_datetime`, a.`order_status`, a.`first_name`, a.`last_name`, a.`email_id`, a.`mobile_no`, a.`shipping_address`, a.`pincode`, a.`city_name`, a.`state_name`,c.`state_name`,a.`order_unique_no`, a.`order_status`,a.coupon_code,a.confirm_mobile_no from order_info as a,brand_info as b,states as c,order_product_info as d where a.`brand_id` = b.`user_id` and a.`state_name` = c.`id` and a.`order_status`!=9 ".$search_cond."  AND 1=1 AND d.order_id = a.id group by a.order_unique_no order by a.`created_datetime` desc, a.`id` desc  ".$limit_cond);*/
// ,order_product_info as d,cart_info as ci    AND d.order_id = a.id AND d.cart_id = ci.id    ,ci.sc_source,ci.sc_medium,ci.sc_campaign
//echo $search_cond;
      	if($search_cond!='')
      	{	
      		// $query = $this->db->query("select (select id from cart_info as ci where ci.order_id=a.`id` limit 0,1) as cart_id,(select platform_info from cart_info as ci where ci.order_id=a.`id` limit 0,1) as platform_info,(select product_id from cart_info as ci where ci.order_id=a.`id` limit 0,1) as product_id,(SUM(a.order_sub_total)) as product_sum,(SUM(a.order_sub_total) - (SUM(a.cod_amount)+SUM(a.shipping_amount)+SUM(a.order_tax_amount))) as OrderTotal ,(a.order_total-(SUM(a.cod_amount)+SUM(a.shipping_amount)+SUM(a.order_tax_amount))) as OrderProductTotal , a.`user_id`, a.`order_sub_total`, a.`order_total`, a.`pay_mode`, a.`created_datetime`, a.`modified_datetime`, a.`order_status`, a.`first_name`, a.`last_name`, a.`email_id`, a.`mobile_no`, a.`shipping_address`, a.`pincode`, a.`city_name`, a.`state_name` as state_id,c.`state_name`,a.`order_unique_no`, a.`order_status`,a.coupon_code,a.confirm_mobile_no,a.order_display_no,a.is_completed,(select sc_source from cart_info as ci where ci.order_id=a.`id` limit 0,1) as sc_source,(select sc_medium from cart_info as ci where ci.order_id=a.`id` limit 0,1) as sc_medium,(select sc_campaign from cart_info as ci where ci.order_id=a.`id` limit 0,1) as sc_campaign,a.is_paymentdone,a.order_tax_amount from order_info as a,brand_info as b,states as c where a.`brand_id` = b.`user_id` and a.`state_name` = c.`id` and a.`order_status`!=9 ".$search_cond."  AND 1=1  group by a.order_unique_no order by a.`created_datetime` desc, a.`id` desc  ".$limit_cond);

      		$query = $this->db->query("select (select id from cart_info as ci where ci.order_id=a.`id` limit 0,1) as cart_id,(select platform_info from cart_info as ci where ci.order_id=a.`id` limit 0,1) as platform_info,(select product_id from cart_info as ci where ci.order_id=a.`id` limit 0,1) as product_id,(SUM(a.order_sub_total)) as product_sum,(SUM(a.order_sub_total) - (SUM(a.cod_amount)+SUM(a.shipping_amount)+SUM(a.order_tax_amount))) as OrderTotal ,(a.order_total-(SUM(a.cod_amount)+SUM(a.shipping_amount)+SUM(a.order_tax_amount))) as OrderProductTotal , a.`user_id`, a.`order_sub_total`, a.`order_total`, a.`pay_mode`, a.`created_datetime`, a.`modified_datetime`, (Select order_status from order_product_info where a.id = order_id limit 0,1) as order_prd_status,(Select id from order_product_info where a.id = order_id limit 0,1) as order_prd_id, a.`first_name`, a.`last_name`, a.`email_id`, a.`mobile_no`, a.`shipping_address`, a.`pincode`, a.`city_name`, a.`state_name` as state_id,c.`state_name`,a.`order_unique_no`,a.coupon_code,a.confirm_mobile_no,a.order_display_no,a.is_completed,(select sc_source from cart_info as ci where ci.order_id=a.`id` limit 0,1) as sc_source,(select sc_medium from cart_info as ci where ci.order_id=a.`id` limit 0,1) as sc_medium,(select sc_campaign from cart_info as ci where ci.order_id=a.`id` limit 0,1) as sc_campaign,a.is_paymentdone,a.order_tax_amount,a.coupon_amount from order_info as a,brand_info as b,states as c where a.`brand_id` = b.`user_id` and a.`state_name` = c.`id` and a.`order_status`!=9 ".$search_cond."  AND 1=1  group by a.order_unique_no order by a.`created_datetime` desc, a.`id` desc  ".$limit_cond);

      	//SELECT CONVERT_TZ('2004-01-01 20:00:00','+00:00','+05:30')
      	// echo $this->db->last_query();
        	$result = $query->result_array();
    	}else
    	{
    		$result = false;
    	}
		// echo "<pre>";print_r($result);exit;
        return $result;
    }


    function getOrderProductInfo($orderUniqueNo)
    {
    	/*echo $orderUniqueNo;exit;*/
    	$search_cond = '';
    	$limit_cond = '';

    	if($this->session->userdata('role_id') == 6 ){ 
			$search_cond = $search_cond." and a.`brand_id`=".$this->session->userdata('user_id');
		} 

    	if($orderUniqueNo!="")
    	{

	    	$query = $this->db->query("select (select product_sku from `product_inventory` where  `product_id` = d.`product_id` and `size_id`=d.`product_size`  limit 0,1) as product_sku, (select look_id as look from cart_info where order_id=d.`order_id` and product_id = d.`product_id` limit 0,1) as look_id,(select distinct platform_info as look from cart_info where order_id=d.`order_id` and product_id = d.`product_id` ) as platform_info,(Select sum(h.product_price) from order_info as g,order_product_info as h where g.id = h.order_id and g.order_unique_no = '".$orderUniqueNo."' and h.coupon_code!='' group by g.order_unique_no ) as OrderProductTotal,
      		(Select sum(g.order_total) from order_info as g where  g.order_unique_no = '".$orderUniqueNo."' group by g.order_unique_no ) as OrderGrandTotal,
      		(Select sum(i.product_price) from order_product_info as i  group by i.order_id having i.order_id = d.order_id ) as OrderIdProductSum,
      		(Select f.size_text from product_size as f where f.`id` =d.`product_size`) as size_text,
      		a.`id` as order_id,d.id as order_prd_id, a.`user_id`, a.`brand_id`, a.`order_tax_amount`, a.`cod_amount`,a.`shipping_amount`, a.`order_sub_total`, a.`order_total`,a.`bms_discount`,d.product_id,d.send_email, a.`pay_mode`, a.`created_datetime`, d.`modified_datetime`, a.`order_status`, a.`first_name`, a.`last_name`, a.`email_id`, a.`mobile_no`, a.`shipping_address`,a.`billing_address`, a.`pincode`, a.`city_name`, a.`state_name`,c.`state_name`,b.`company_name`, d.`product_id`, d.`product_qty`,e.`name` as product_name, d.`product_price`,a.`order_unique_no`, d.`order_status`,d.`discount_percent`,e.`image`,d.`coupon_code`,d.`remark`,e.`price` as actual_product_price,e.`slug`,d.`dp_active`,a.txnid,a.mihpayid,a.order_display_no,d.sc_discount as online_pay_discount,a.coupon_amount from order_info as a,brand_info as b,states as c, order_product_info as d, product_desc as e where  a.`brand_id` = b.`user_id` and a.`state_name` = c.`id` and d.`order_id` = a.`id` and e.`id` = d.`product_id` and a.`order_status`!=9 and  a.order_unique_no = '".$orderUniqueNo."' ".$search_cond."  order by a.`created_datetime` desc, a.`id` desc  ".$limit_cond);
	      	/*echo $this->db->last_query();*/
	        $result = $query->result_array();
	        return $result;
    	}else
    	{
    		return 0;
    	}
    }

     function getProductSku($productId,$productSize)
    {
    	$result=array();
        if($productId!="" && $productSize!="")
        {
          $query = $this->db->query("select product_sku from product_inventory where product_id='".$productId."' AND size_id='".$productSize."'; ");
           /* echo $this->db->last_query();*/
            $result = $query->result_array();           
        }   

        if($result)
        {
          return @$result[0]['product_sku'];
        }else
        {
          return '';
        }     
          
    }

    function calculate_product_discountAmt_order($product_price,$totalproductprice,$discountType,$discountAmt)
    { 
      if($product_price!='' && $totalproductprice!=0 && $discountAmt!='')
      {  

        if($discountType==1)
        {
            $percent = ($product_price*100)/$totalproductprice;
            $DiscountAmt = ($discountAmt*$percent)/100;
            $productDiscountAmt = $product_price-$DiscountAmt;
            return $productDiscountAmt;

        }else if($discountType==2)   
        {     
            $percent = $discountAmt;  
            $DiscountAmt = ($product_price*$percent)/100;
            $productDiscountAmt = $product_price-$DiscountAmt;  
            return $productDiscountAmt;
        }   
       
      }else
      {
        return 0;
      }
    }

    function get_refre_amt($order_id){
        $query = $this->db->query('select points from referral_info where invite_order_id ="'.$order_id.'"');
        $res = $query->result_array();
        return @$res[0]['points'];
    }

    function getOrderStatus($orderUniqueId)
    {        	
	    $orderStatusList = array('1'=>'Processing', '2'=>'Confirmed', '3'=>'Dispatched', '4'=>'Delivered','5'=>'Cancelled','6'=>'Return','7'=>'Fake','8'=>'Curated');
	    $orderId_array = explode('_',$orderUniqueId);	
	    $final_status = '';    
	    $statusFlag = 0; $status_array = []; $all_order_status = [1,2,3,4,5,6,7,8];
	    for($i=1; $i<=(sizeof($orderId_array)-1);$i++)
	    {
	    	$query = $this->db->query('select order_status from `order_product_info` where order_id ="'.$orderId_array[$i].'"');
        	$res = $query->result_array();
        	$j=1;
        	foreach($res as $v) 
        	{   	
	        	//$order_status = $res[0]['order_status'];
        		$order_status = $v['order_status'];
        		$status_array[] = $v['order_status'];
	        	if($j == 1)
	        	{
	        		$final_status = $order_status;        		
	        	}
	        	
	        	if($final_status != $order_status)
	        	{
	        		$statusFlag = 1;
	        	}
	        	$j++;
	       	}

	    }
	   
	    /*if($statusFlag == 1)
	    {
	    	return '<span class="text-blue">Processing</span>';
	    }else
	    {
	    	if($final_status == 2)
	    	{
	    		return '<span class="text-green">Confirmed</span>';
	    	}else if($final_status == 3)
	    	{
	    		return '<span class="text-green">Dispatched</span>';
	    	}else if($final_status == 4)
	    	{
	    		return '<span class="text-green">Delivered</span>';
	    	}else if($final_status == 5)
	    	{
	    		return '<span class="text-red">Cancelled</span>';
	    	}else if($final_status == 6)
	    	{
	    		return '<span class="text-orange">Return</span>';
	    	}else if($final_status == 7)
	    	{
	    		return '<span class="text-red">Fake</span>';
	    	}else
	    	{
	    		return '<span class="text-blue">Processing</span>';
	    	}
	    }*/
	    if(!empty($status_array)){
		    if(count(array_unique($status_array)) == 1){
		    	
		    	if(!empty($status_array) && (in_array(1,$status_array) || in_array(2,$status_array) || in_array(3,$status_array) )){
		    		return '<span class="text-blue">'.$orderStatusList[$status_array[0]].'</span>';
		    	}else{
		    		return '<span class="text-orange">'.$orderStatusList[$status_array[0]].'</span> / <span class="text-red">Closed</span>';
		    	}

		    }else if(count(array_unique($status_array)) != 1){
		    	$arr = array_unique(@array_intersect($status_array,array(4,5,6)));
		    	if((in_array(1,$status_array) || in_array(2,$status_array) || in_array(3,$status_array) )){
		    		return '<span class="text-blue">Processing</span>';
		    	}else{
		    		return '<span class="text-red">Closed</span>';
		    	}
		    }else{
		    	return '<span class="text-blue">Processing</span>';
		    }
		}
	}

	 public function updateRemark($orderPrdId,$remark)
	{	
		if($remark!='' && $orderPrdId!='')
		{	
			$orderProductRemark = trim($remark);	
			$this->db->where('id', $orderPrdId);
			$result = $this->db->update('order_product_info', array('remark'=>$orderProductRemark)); 
				
			if($result)
			{
				return true;
			}else
			{
				return false;
			}
		}
		return false;
	}

	public function change_order_status($order_id,$order_status,$first_name,$last_name,$user_id){
		if($order_id!=''){

		if($order_status == 2){
			$this->db->where('order_unique_no', $order_id);
			$this->db->update('order_info', array('order_status'=>7));

			$new_order_id = explode('_', $order_id);
			array_shift($new_order_id);

			if(!empty($new_order_id)){
				foreach($new_order_id as $val){
					$this->db->where('order_id', $val);
					$this->db->update('order_product_info', array('order_status'=>7));
				}
			}

			$this->db->where('order_unique_no', $order_id);
			$this->db->update('order_info', array('confirm_mobile_no'=>1));
		}else if($order_status == 1){

			$this->db->where('order_unique_no', $order_id);
			$this->db->update('order_info', array('order_status'=>1));

			$new_order_id = explode('_', $order_id);
			array_shift($new_order_id);

			if(!empty($new_order_id)){
				foreach($new_order_id as $val){
					$this->db->where('order_id', $val);
					$this->db->update('order_product_info', array('order_status'=>1));
				}
			}

			
			$this->load->library('curl');
			$url = LIVE_SITE."Cartnew/orderPlaceMessage/".$user_id."/".$order_id."/".$first_name."/".$last_name; 
			$output = $this->curl->simple_post($url);
			$url = LIVE_SITE."Cartnew/orderPlaceMessage_sc/".$user_id."/".$order_id."/".$first_name."/".$last_name; 
			$output = $this->curl->simple_post($url);
			$url = LIVE_SITE."Cartnew/orderPlaceMessagetoBrand/".$user_id."/".$order_id."/".$first_name."/".$last_name; 
			$output = $this->curl->simple_post($url);

			$this->db->where('order_unique_no', $order_id);
			$this->db->update('order_info', array('confirm_mobile_no'=>1));
		}
		}	
	}

	function get_order_price_ref($orderUniqueNo){
        $query = $this->db->query('select id,user_id from `order_info` where order_unique_no = "'.$orderUniqueNo.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info);

        $query = $this->db->query('SELECT SUM(product_price) as product_price FROM `order_product_info` WHERE order_id IN ('.$order_ids.')' );

            $query_res = $query->result_array();
            if(!empty($query_res)){
                return $query_res[0]['product_price'];
            }else{
                return 0;
            }
        }
    }

    function getOrderDp($brand_id)
    {
    	$query1 =$this->db->query("SELECT dp_id FROM `brand_delivery_partner_mapping` where brand_id = '".$brand_id."'");
    	$result1 = $query1->result_array();
    	if(!empty($result1))
    	{
	    	$query2 = $this->db->query("SELECT GROUP_CONCAT(delivery_partner_name) as dpartner FROM `delivery_partner` where id in(".$result1[0]['dp_id'].")");
	    	$result2 = $query2->result_array();	    	
    	}
    	return @$result2[0]['dpartner'];    	
    }

    function assign_order_dp($orderIds)
    {
    	if(!empty($orderIds))
    	{
    		foreach($orderIds as $val)
    		{
    			$this->db->where('id',$val);
				$this->db->update('order_product_info', array('dp_active'=>1));
    		}
    	}
    } 
	
	function update_notification_data($order_id,$product_id,$message=''){
		$result = array();
		$query = $this->db->query("SELECT a.id as order_product_id,a.user_id,b.order_unique_no as order_id,a.product_id,a.order_status from order_product_info as a,order_info as b WHERE a.product_id = $product_id AND a.order_id = $order_id AND b.id = a.order_id ");
		$result = $query->result_array();
		$object_meta_data['user_id'] = $result[0]['user_id'];
		$object_meta_data['order_id'] = $result[0]['order_id'];
		$object_meta_data['product_id'] = $result[0]['product_id'];
		$object_meta_data['order_status'] = $result[0]['order_status'];
		$object_meta_data['message'] = $message;
		$object_meta_data['order_product_id'] = $result[0]['order_product_id'];
		$object_meta_data['order_notification_read'] = '0';
		$object_meta_keys = array_keys($object_meta_data);$i=0;
		// print_r($result);exit;
		$query = $this->db->query('SELECT a.object_id from object as a where a.object_name = '.$result[0]['order_product_id'].' ');
		$res = $query->result_array();
		if(!empty($res)){
			foreach($object_meta_data as $val){
				
				$data = array('object_meta_key'=>$object_meta_keys[$i],'object_meta_value'=>$val,'created_datetime'=>date('Y-m-d H:i:s'),'created_by'=> $this->session->userdata('user_id'),'modified_by'=> $this->session->userdata('user_id'));
				
				$this->db->update('object_meta', $data, array('object_id' => $res[0]['object_id'],'object_meta_key'=>$object_meta_keys[$i]));
				
				$i++;
			}
		}else{
			$this->db->insert('object',array('object_name'=>$result[0]['order_product_id'],'object_description'=>'order_product_id','object_type'=>'notification','object_created'=>date('Y-m-d H:i:s'),'object_creator'=> $this->session->userdata('user_id'),'object_modifier'=> $this->session->userdata('user_id'),'object_image'=>''));
			$object_id = $this->db->insert_id();
			
			foreach($object_meta_data as $val){
				$this->db->insert('object_meta',array('object_id'=>$object_id,'object_meta_key'=>$object_meta_keys[$i],'object_meta_value'=>$val,'created_datetime'=>date('Y-m-d H:i:s'),'created_by'=> $this->session->userdata('user_id'),'modified_by'=> $this->session->userdata('user_id')));
				$i++;
			}
		}
		
	}
	
	function change_final_order_status($order_id,$order_status,$user_id){
		$query = $this->db->query('UPDATE order_info as a SET a.is_completed = '.$order_status.' WHERE a.order_unique_no = "'.$order_id.'" AND a.user_id = '.$user_id.' ');
		return true;
	}
	
	function get_product_info($product_id){
		$query = $this->db->query('SELECT a.id,a.name,b.company_name,CONCAT("http://www.stylecracker.com/sc_admin/assets/user_profiles/",a.image) as product_img from product_desc as a,brand_info as b where a.brand_id = b.user_id AND a.id = '.$product_id.' ');
		$res = $query->result_array();
		return $res[0];
	}
	
	function update_tax_id($order_id,$txnid,$payu_id,$paid_price,$payment_date,$is_paymentdone){
		
		$query = $this->db->query('UPDATE order_info as a SET a.txnid = "'.$txnid.'",a.mihpayid = "'.$payu_id.'",a.paid_price = "'.$paid_price.'",a.payment_date = "'.$payment_date.'",a.is_paymentdone = "'.$is_paymentdone.'" WHERE a.order_unique_no = "'.$order_id.'" ');
		echo $this->db->last_query();
		return true;
	}
	
	function get_order_info($order_id){
		$query = $this->db->query('select a.order_unique_no,a.txnid,a.mihpayid,a.paid_price,a.payment_date,a.is_paymentdone,a.pay_mode from order_info as a  WHERE a.order_unique_no = "'.$order_id.'" ');
		$res = $query->result_array();
		return $res;
		
	}	
	
	function check_coupon_exist($coupon,$user_email_id,$cart_product_cost){
		
        $discount_amount = 0; $response = array(); $cart_brand = array();
        //$query = $this->db->query("SELECT `id`, `coupon_code`, `coupon_desc`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `individual_use_only`, `exclude_sale_items`, `brand_id`, `products`, `exclude_products`, `prd_category`, `exclude_prd_category`, `email_restriction`, `usage_limit_per_coupon`, `usage_limit_to_items`, `usage_limit_to_user`, `coupon_used_count`, `coupon_email_applied`, `stylecrackerDiscount`, `status`, `is_delete`, `created_datetime`, `created_by`, `modified_datetime`, `modified_by`,bin_no FROM `coupon_settings` WHERE 1 and LOWER(coupon_code) = '".$coupon."' and status = 1 and is_delete = 0 and usage_limit_per_coupon!=coupon_used_count limit 0,1");
		
        $query = $this->db->query("SELECT `id`, `coupon_code`, `coupon_desc`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `individual_use_only`, `exclude_sale_items`, `brand_id`, `products`, `exclude_products`, `prd_category`, `exclude_prd_category`, `email_restriction`, `usage_limit_per_coupon`, `usage_limit_to_items`, `usage_limit_to_user`, `coupon_used_count`, `coupon_email_applied`, `stylecrackerDiscount`, `status`, `is_delete`, `created_datetime`, `created_by`, `modified_datetime`, `modified_by`,bin_no FROM `coupon_settings` WHERE 1 and LOWER(coupon_code) = '".$coupon."' and status = 1 and is_delete = 0 limit 0,1");
        //and coupon_start_datetime <= now() and coupon_end_datetime >= now()
        $res = $query->result_array();
		// print_r($res);exit;
        if(!empty($res)){
			
            $coupon_amt 			= 0; $coupon_per = 0; 
            $individual_use_only 	= $res[0]['individual_use_only'];
            $coupon_email_applied 	= $res[0]['coupon_email_applied'];
            $brand_id 				= $res[0]['brand_id'];
            $products 				= $res[0]['products'];
            $discount_type 			= $res[0]['discount_type'];
            $coupon_amount 			= $res[0]['coupon_amount'];
            $min_spend 				= $res[0]['min_spend'];
            $max_spend 				= $res[0]['max_spend'];
            $stylecrackerDiscount  	= $res[0]['stylecrackerDiscount'];
			$db_bin_no  			= $res[0]['bin_no'];

            if($discount_type == '1' || $discount_type == '3'){
                $coupon_amt = $coupon_amount;
            }else{
                $coupon_per = $coupon_amount;
            }

            $cart_users = array();
            $cart_users = explode(',',$coupon_email_applied);
			$coupon = strtoupper($coupon);
            if($individual_use_only == 1 && $coupon_email_applied!='' && $user_email_id=='' && $stylecrackerDiscount!=1){
                $response['msg'] = 'Please login to apply coupon code';
                $response['msg_type'] = '0';
                $response['coupon_code'] = '';
                return $response;
            }else if($individual_use_only == 1 && $coupon_email_applied!='' && $user_email_id!='' && in_array($user_email_id,$cart_users) == 1){
                /* Abendant Cart Discount Userwise*/
               
                if($cart_product_cost > 0 && !empty($cart_users)){
					
                    if($min_spend == 0 && $max_spend==0){
                        $cart_product_cost = $cart_product_cost;   
                    }else if($min_spend <= $cart_product_cost && $max_spend >= $cart_product_cost){
                        $cart_product_cost = $cart_product_cost;  
                    }else{
                        $cart_product_cost = 0;
                    }
                }
				
                if($cart_product_cost > 0){
                    if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                    }
                }
                if($discount_amount > 0){ 
					$response['msg'] = 'Coupon Applied'; $response['brand_id'] = 0; $response['msg_type'] = '1'; 
					$response['coupon_code'] = strtoupper($coupon);
				} else{
                    
                    $response['msg'] = 'Userwise Discount'; $response['msg_type'] = '0';
                    $response['coupon_code'] = strtoupper($coupon);
                }
                $response['coupon_discount_amount'] = $discount_amount;
                return $response;

            }else if($brand_id == 0 && $products == 0 && $stylecrackerDiscount==1){
                /* StyleCracker Discount */
                
                if($cart_product_cost > 0){
					
                    if($min_spend == 0 && $max_spend==0){
                        $cart_product_cost = $cart_product_cost;   
                    }else if($min_spend <= $cart_product_cost && $max_spend >= $cart_product_cost){
                        $cart_product_cost = $cart_product_cost;  
                    }else{
                        $cart_product_cost = 0;
                    }
                }

                if($cart_product_cost > 0){ 
                    if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $coupon_amount = $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                        $coupon_amount = $coupon_amount/100; 
                    }
                }
                if($discount_amount > 0){ 
                    
                    $response['msg'] = 'Coupon Applied';$response['brand_id'] = 0;$response['msg_type'] = '1'; 
                    $response['coupon_code'] = strtoupper($coupon);
                     } else{
                    
                    $response['msg'] = 'Coupon will apply if you spend minimum '.$min_spend.' on StyleCracker';
                    $response['msg_type'] = '0'; 
                    $response['coupon_code'] = '';
                }
                $response['coupon_discount_amount'] = $discount_amount;
                return $response;
            }else if($brand_id > 0 && $products == 0){
                /* Brands Discount */
               
                if($cart_product_cost > 0){
					
                    if($min_spend == 0 && $max_spend==0){
                        $cart_product_cost = $cart_product_cost;   
                    }else if($min_spend <= $cart_product_cost && $max_spend >= $cart_product_cost){
                        $cart_product_cost = $cart_product_cost;  
                    }else{
                        $cart_product_cost = 0;
                    }
                }

                if($cart_product_cost > 0 ){
                    if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                    }
                }
                if($discount_amount > 0){
					
					$response['msg'] = 'Coupon Applied'; $response['brand_id'] = $brand_id; $response['msg_type'] = '1'; 
					$response['coupon_code'] = strtoupper($coupon);
				} else{
					
					$response['msg'] = 'Coupon will apply if you spend minimum '.$min_spend.' on '.$this->getBrandExtraInfo($brand_id)[0]['company_name'].'s products';
					$response['msg_type'] = '0';
					$response['coupon_code'] = ''; 
                }
                $response['coupon_discount_amount'] = $discount_amount;
                return $response;
            }else if($brand_id > 0 && $products > 0){
               
                $cart_products = array();
                $cart_products = explode(',',$products);
                
                  if($cart_product_cost > 0){
					
                    if($min_spend == 0 && $max_spend==0){
                        $cart_product_cost = $cart_product_cost;   
                    }else if($min_spend <= $cart_product_cost && $max_spend >= $cart_product_cost){
                        $cart_product_cost = $cart_product_cost;  
                    }else{
                        $cart_product_cost = 0;
                    }
                }

                if($cart_product_cost > 0 ){
                    if($coupon_amt > 0){
                        $discount_amount = $cart_product_cost - $coupon_amount;
                        $discount_amount = $coupon_amount;
                    }else{
                        $discount_amount = $cart_product_cost * ($coupon_amount/100);
                    }
                } 
                if($discount_amount > 0){
                    $response['msg'] = 'Coupon Applied'; $response['brand_id'] = $brand_id; $response['msg_type'] = '1'; 
                    $response['coupon_code'] = strtoupper($coupon);
                } else{
                    $response['msg'] = 'Coupon code will be applied for specific products'; $response['msg_type'] = '0';
                    $response['coupon_code'] = '';
                }
                $response['coupon_discount_amount'] = $discount_amount;

                
                return $response;
            }

        }else{
            $response['msg'] = 'No Coupon Found or Expired';
            $response['msg_type'] = '0';
            $response['coupon_code'] = '';
            $response['coupon_discount_amount'] = '0';
            return $response;
        }
    }

    function getOrders_affiliate($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL,$search_by2=NULL,$pagetype=NULL,$utm_medium=NULL){   

        if($search_by == '1'){ $search_cond = " AND a.`id` = '".strip_tags(trim($table_search))."'"; }
        else if($search_by == '9'){ $search_cond = " AND a.`order_display_no` = '".strip_tags(trim($table_search))."'"; }
		else if($search_by == '2'){ $search_cond = " AND CONCAT_WS(' ',a.`first_name`,a.`last_name`) like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '3'){ $search_cond = " AND b.`company_name` like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '4'){ $search_cond = " AND a.`created_datetime` like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '5'){ 

				$tb_status=strtolower(strip_tags(trim($table_search)));
				if($tb_status=="processing")
				{
					$apr_status = 1;

				}else if($tb_status == "confirmed")
				{					
					$apr_status = 2;
				}else if($tb_status == "dispatched")
				{
					$apr_status = 3;

				}else if($tb_status == "delivered")
				{
					$apr_status = 4;

				}else if($tb_status == "cancelled")
				{
					$apr_status = 5;

				}else if($tb_status == "return")
				{
					$apr_status = 6;

				}else if($tb_status == "fake")
				{
					$apr_status = 7;
				}else if($tb_status == "curated")
				{
					$apr_status = 8;
				}
				else
				{
					$apr_status = 0;
				}

				if($apr_status!=0)
				{

					$query = $this->db->query("select GROUP_CONCAT(order_id) as order_id from order_product_info where order_status = ".$apr_status." and order_status !=9");
					$res = $query->result_array();
					if(!empty($res)){
						$search_cond = " AND a.`id` IN (".$res[0]['order_id'].")";
					}
				}
				
			 }else if($search_by == '6'){ $search_cond = " AND a.order_unique_no ='".strip_tags(trim($table_search))."'"; }
				/*else if($search_by == '7'){ $search_cond = " AND d.company_name like '%".strip_tags(trim($table_search))."%'"; }	*/
				else if($search_by == '8'){

				$date_from = $this->input->post('date_from');
				$date_to = $this->input->post('date_to');
				if($date_from=='' && $date_to == '' )
				{
					$tb_arr = explode(',',$table_search);
					$date_from = @$tb_arr[0];
					$date_to = @$tb_arr[1];
				}

				$search_cond = " AND a.`created_datetime` >= '".$date_from." 00:00:00'  and a.`created_datetime` <= '".$date_to." 23:59:59'";

				}else{ $search_cond ='AND 1=1'; }				

				//$search_cond="";
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}

		if($this->session->userdata('role_id') == 6 ){
			$search_cond = $search_cond." and (a.confirm_mobile_no = 1 or a.`pay_mode`=2) and a.`brand_id`=".$this->session->userdata('user_id');
		}  
		
		if($search_by2 == '1'){
				$search_cond = $search_cond." AND a.brand_id != ".SCBOX_BRAND_ID." ";
		}else if($search_by2 == '2'){
				$search_cond = $search_cond." AND a.brand_id = ".SCBOX_BRAND_ID." ";
		}

		if($pagetype=='affiliate')
		{
			$search_cond = $search_cond." AND sc_medium = '".$utm_medium."' ";
		}      	

      	$query = $this->db->query("select (select platform_info from cart_info as ci where ci.order_id=a.`id` limit 0,1) as platform_info,(select product_id from cart_info as ci where ci.order_id=a.`id` limit 0,1) as product_id,(SUM(a.order_sub_total)) as product_sum,(SUM(a.order_sub_total) - (SUM(a.cod_amount)+SUM(a.shipping_amount)+SUM(a.order_tax_amount))) as OrderTotal ,(a.order_total-(SUM(a.cod_amount)+SUM(a.shipping_amount)+SUM(a.order_tax_amount))) as OrderProductTotal , a.`user_id`, a.`order_sub_total`, a.`order_total`, a.`pay_mode`, a.`created_datetime`, a.`modified_datetime`, a.`order_status`, a.`first_name`, a.`last_name`, a.`email_id`, a.`mobile_no`, a.`shipping_address`, a.`pincode`, a.`city_name`, a.`state_name`,c.`state_name`,a.`order_unique_no`, a.`order_status`,a.coupon_code,a.confirm_mobile_no,a.order_display_no,a.is_completed, ci.sc_source,ci.sc_medium, ci.sc_campaign,a.is_paymentdone from order_info as a,brand_info as b,states as c, cart_info as ci where ci.order_id=a.`id` and a.`brand_id` = b.`user_id` and a.`state_name` = c.`id` and a.`order_status`!=9 ".$search_cond."  AND 1=1  group by a.order_unique_no order by a.`created_datetime` desc, a.`id` desc  ".$limit_cond);
      	
      	// echo $this->db->last_query();
        $result = $query->result_array();
		// echo "<pre>";print_r($result);exit;
        return $result;
    }	
	
	public function exportStylistData($data=array(), $PaObjectID)
	{
		$this->db->select('om.object_id,om.object_meta_key,om.object_meta_value,om.created_by,oi.order_unique_no,oi.order_display_no,oi.brand_id,oi.order_tax_amount,oi.order_sub_total,oi.order_total,oi.pay_mode,oi.first_name,oi.last_name,oi.email_id,oi.mobile_no,oi.shipping_address,oi.pincode,oi.city_name,oi.state_name,oi.sc_discount_applied,oi.coupon_code,oi.coupon_amount,oi.billing_address, ui.gender as forme_gender');
		$this->db->from('object_meta as om');
		$this->db->join('order_info as oi', 'oi.order_display_no = om.object_meta_key','INNER');
		$this->db->join('user_info as ui', 'ui.user_id = om.created_by','INNER');
		
		if($data['search_by'] == 9)
		{ 
			if(isset($data['table_search_encode']) && $data['table_search_encode'] != '')
			{
				$this->db->where('om.object_meta_key',$data['table_search_encode']);
			}
		} 
		if($data['search_by'] == 8)
		{ 
			$dateRange = explode(',', $data['table_search_encode']);
			if(isset($data['table_search_encode']) && $data['table_search_encode'] != '')
			{
				$this->db->where('oi.created_datetime >=',$dateRange[0].' 00:00:00');
				$this->db->where('oi.created_datetime <=',$dateRange[1].' 23:59:59');
				$this->db->where('om.object_meta_key !=','pa_selection_after');
			}
		} 
		$this->db->where_in('om.object_id',$this->db->escape_str($PaObjectID), NULL, false);
		if($data['search_by'] == 2)
		{
			if(isset($data['table_search_encode'][0]) && trim($data['table_search_encode'][0])!='')
			{
				$this->db->like('oi.first_name',$this->db->escape_str($data['table_search_encode'][0]),'both');
			} 
		}
		if($data['search_by'] == 4)
		{
			if(isset($data['table_search_encode']) && trim($data['table_search_encode'])!='')
			{
				$this->db->like('oi.created_datetime',$data['table_search_encode']);
			} 
		}
		if($data['search_by'] == 6)
		{
			if(isset($data['table_search_encode']) && trim($data['table_search_encode'])!='')
			{
				$this->db->like('oi.order_unique_no',$data['table_search_encode']);
			} 
		}
		$this->db->where('oi.order_status !=', 9);
		$query = $this->db->get();
		//echo $this->db->last_query();exit();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		return false;
	}
	
	
	public function getPaObject()
	{
		$objectSlug = array('pa_selection_before','pa_selection_after','category_selection','scbox_gift');
		$this->db->select('object_id');
		$this->db->from('object');
		$this->db->where_in('object_slug',$objectSlug);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		return false;
	}

	function get_state($id){
        $query = $this->db->query("select * from `states` as a where a.`id` = '".$id."' ");
        $res = $query->result_array();       
        return $res[0]['state_name'];
    }

    public function getBoxProductDetail($objectMetaKey = '', $oid='', $uid='', $objectId= '')
	{	
		$this->db->select('object_meta_value');
		$this->db->from('object_meta');
		$this->db->where('object_meta_key', $objectMetaKey);
		//$this->db->where('created_by', $uid);
		//$this->db->where('object_id IN ('.$objectId.')');
		$this->db->where('object_id',$objectId);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		return false;
	}

	public function getCartId($order_display_no,$user_id)
	{
		//$orders = explode('_',$order_unique_no);
		$this->db->select('ci.id as cart_id');
		$this->db->from('cart_info as ci');
		$this->db->join('order_info as oi', 'ci.order_id = oi.id','INNER');
		$this->db->where('oi.order_display_no', trim($order_display_no));		
		$this->db->where('ci.user_id',$user_id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		return false;
	}
	
	public function addDeliveryProcess($arrData = array())
	{
		$this->db->select('id');
		$this->db->from('order_delivery_process');
		$this->db->where('order_unq_no',$arrData['order_unq_no']);
		$this->db->where('order_prd_id',$arrData['order_prd_id']);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$this->db->set('modified_datetime', date('Y-m-d H:i:s'));
			$this->db->where('order_unq_no',$arrData['order_unq_no']);
			$this->db->where('order_prd_id',$arrData['order_prd_id']);
			$this->db->update('order_delivery_process');
			$dpId = $query->row_array();
			return $dpId['id'];
		}
		else
		{
			$this->db->insert('order_delivery_process', $arrData);
			$dpId = $this->db->insert_id();
			return $dpId;
		}
	}
	
	public function getAwbNosData($dpId='', $order_unq_no='')
	{
		$this->db->from('order_delivery_process');
		$this->db->where('id',$dpId);
		$this->db->where('order_unq_no',$order_unq_no);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		return false;
	}

	function getOrderProductStatus($orderUniqueId,$product_id)
    {        	
	    $orderStatusList = array('1'=>'Processing', '2'=>'Confirmed', '3'=>'Dispatched', '4'=>'Delivered','5'=>'Cancelled','6'=>'Return','7'=>'Fake','8'=>'Curated');
	    $orderId_array = explode('_',$orderUniqueId);	
	    $final_status = '';    
	    $statusFlag = 0; $status_array = []; $all_order_status = [1,2,3,4,5,6,7,8];
	    for($i=1; $i<=(sizeof($orderId_array)-1);$i++)
	    {
	    	$query = $this->db->query('select order_status from `order_product_info` where order_id ="'.$orderId_array[$i].'" AND product_id ="'.$product_id.'"');
        	$res = $query->result_array();
        	$j=1;
        	$order_status = $res[0]['order_status'];
        	return $order_status;
	    }
	   
	}
	
	public function update_trackdata_by_awbno($arrData=array(), $awbno='')
	{
		$this->db->where('awp_no',$awbno);
		$this->db->update('order_delivery_process',$arrData);
		if($this->db->affected_rows() > 0)
		{
			return true;
		}
		return false;
	}
	
	public function get_cartid_for_awbnumber($orderId='')
	{
		$this->db->select('id,product_id,user_id,product_size,product_qty,SCUniqueID,order_id');
		$this->db->from('cart_info');
		$this->db->where('order_id',$orderId);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		return false;
	}
	
	public function add_cart_meta($arrData = array())
	{
		$this->db->insert('cart_meta', $arrData);
		$id = $this->db->insert_id();
		return $id;
	}
	
	public function get_cart_meta_data($cartid='')
	{
		$this->db->from('cart_meta');
		$this->db->where('cart_id', $cartid);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}
	
	public function get_del_partner($delid='')
	{
		$this->db->select('id,delivery_partner_name,tracking_link');
		$this->db->from('delivery_partner');
		$this->db->where('user_id', $delid);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		return false;
	}

	public function get_morder_pa($cartid)
	{
		$this->db->from('category_object_relationship');
		$this->db->where('object_id', $cartid);
		$this->db->where('object_type','order');
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return true;
		}
		return false;
	}

	public function get_orderproductinfo_data($id='')
	{
		$this->db->select('opi.id,opi.product_id,opi.order_id,oi.order_unique_no,oi.order_display_no,oi.user_id ');
		$this->db->from('order_product_info as opi');
		$this->db->join('order_info as oi', 'opi.order_id = oi.id','INNER');
		$this->db->where('opi.id', trim($id));			
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		return false;		
	}

	public function get_order_user_data($orderUniqueId='')
	{
		$this->db->select('id,user_id,order_unique_no,order_display_no,first_name,last_name,email_id,mobile_no,shipping_address,pincode,city_name,state_name');
		$this->db->from('order_info');
		$this->db->where('order_unique_no',$orderUniqueId);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		return false;
	}
	
	public function get_all_state()
	{
		$this->db->from('states');
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}
	
	
	public function update_user_order_data($arrData = array(),$unique_no='',$display_no='')
	{
		$this->db->where('order_unique_no',$unique_no);
		$this->db->where('order_display_no',$display_no);
		$this->db->update('order_info',$arrData);
		return true;
	}
	
	
	public function get_cart_metavalue_bykey($cartid,$key)
	{
		$this->db->select('cart_meta_value');
		$this->db->from('cart_meta');
		$this->db->where('cart_id', $cartid);
		$this->db->where('cart_meta_key', $key);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$keyValue = $query->row_array();
			return $keyValue['cart_meta_value'];
		}
		return false;
	}
	
	public function update_gift_datail($arrData = array(),$object_id = '',$display_no = '')
	{
		$this->db->where('object_id',$object_id);
		$this->db->where('object_meta_key',$display_no);
		$this->db->update('object_meta',$arrData);
		return true;
	}

	function get_scdiscount_data($order_id,$userid){
		$query = $this->db->query('select a.sc_discount from order_product_info as a  WHERE a.order_id = "'.$order_id.'" and a.user_id = "'.$userid.'" ');
		//echo $this->db->last_query();exit;
		$res = $query->result_array();
		return $res[0]['sc_discount'];		
	}

	function get_scboxproduct_data($order_id,$userid){
		$query = $this->db->query('select a.product_price from order_product_info as a  WHERE a.order_id = "'.$order_id.'" and a.user_id = "'.$userid.'" ');
		//echo $this->db->last_query();exit;
		$res = $query->result_array();
		return $res[0]['product_price'];		
	}	
	
	
}

?>