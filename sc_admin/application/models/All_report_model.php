<?php
ini_set('max_execution_time',3600);
class All_report_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function abendant_cart_list($date_from,$date_to){
        if($date_from!='' && $date_to!=''){
            $query = $this->db->query("select b.`id`,(select first_name from user_info where 
user_id = a.user_id) as user_name,(select mobile_no from user_address 
where user_id = a.user_id limit 0,1) as 
mobile_no,a.`user_id`,IF( a.`user_id` = 0, '', (SELECT ul.email FROM user_login ul where a.`user_id` = ul.id) ) as email_id,b.name,a.created_datetime,b.price,c.`company_name`,b.`price` from `cart_info` as a,`product_desc` as b,`brand_info` as c where  a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' and a.`order_id` is null and a.`product_id` =b.`id` and b.`brand_id` = c.`user_id`");

        }else if($date_from=='' && $date_to==''){
            $query = $this->db->query("select b.`id`,(select first_name from user_info where 
user_id = a.user_id) as user_name,(select mobile_no from user_address 
where user_id = a.user_id limit 0,1) as 
mobile_no,a.`user_id`,IF( a.`user_id` = 0, '', (SELECT ul.email FROM user_login ul where a.`user_id` = ul.id) ) as email_id,b.name,a.created_datetime,b.price,c.`company_name`,b.`price` from `cart_info` as a,`product_desc` as b,`brand_info` as c where a.`created_datetime` like '%".date('Y-m-d')."%' and a.`order_id` is null and a.`product_id` =b.`id` and b.`brand_id` = c.`user_id`");

        }else if($date_from!='' && $date_to==''){
            $query = $this->db->query("select b.`id`,(select first_name from user_info where 
user_id = a.user_id) as user_name,(select mobile_no from user_address 
where user_id = a.user_id limit 0,1) as 
mobile_no,a.`user_id`,IF( a.`user_id` = 0, '', (SELECT ul.email FROM user_login ul where a.`user_id` = ul.id) ) as email_id,b.name,a.created_datetime,b.price,c.`company_name`,b.`price` from `cart_info` as a,`product_desc` as b,`brand_info` as c where a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.`order_id` is null and a.`product_id` =b.`id` and b.`brand_id` = c.`user_id`");

        }else if($date_from=='' && $date_to!=''){
            $query = $this->db->query("select b.`id`,(select first_name from user_info where 
user_id = a.user_id) as user_name,(select mobile_no from user_address 
where user_id = a.user_id limit 0,1) as 
mobile_no,a.`user_id`,IF( a.`user_id` = 0, '', (SELECT ul.email FROM user_login ul where a.`user_id` = ul.id) ) as email_id,b.name,a.created_datetime,b.price,c.`company_name`,b.`price` from `cart_info` as a,`product_desc` as b,`brand_info` as c where a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.`order_id` is null and a.`product_id` =b.`id` and b.`brand_id` = c.`user_id`");
        }
        //echo $this->db->last_query();exit;
        $res = $query->result();
        return $res;
    }
	
	public function failed_order_list($date_from,$date_to){
        if($date_from!='' && $date_to!=''){
            $query = $this->db->query("select a.*,d.name as product_name,d.price as prduct_price,b.product_qty,c.company_name as brand_name,e.size_text as product_size from fail_order as a,cart_info b,brand_info as c,product_desc as d,product_size as e where  a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' and a.status = 0 AND a.cart_id = b.id AND b.product_id = d.id AND d.brand_id = c.user_id AND b.product_size = e.id");

        }else if($date_from=='' && $date_to==''){
            $query = $this->db->query("select a.*,d.name as product_name,d.price as prduct_price,b.product_qty,c.company_name as brand_name,e.size_text as product_size from fail_order as a,cart_info b,brand_info as c,product_desc as d,product_size as e where a.`created_datetime` like '%".date('Y-m-d')."%' and a.status = 0 AND a.cart_id = b.id AND b.product_id = d.id AND d.brand_id = c.user_id AND b.product_size = e.id ");

        }else if($date_from!='' && $date_to==''){
            $query = $this->db->query("select a.*,d.name as product_name,d.price as prduct_price,b.product_qty,c.company_name as brand_name,e.size_text as product_size from fail_order as a,cart_info b,brand_info as c,product_desc as d,product_size as e where a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.status = 0 AND a.cart_id = b.id AND b.product_id = d.id AND d.brand_id = c.user_id AND b.product_size = e.id ");

        }else if($date_from=='' && $date_to!=''){
            $query = $this->db->query("select a.*,d.name as product_name,d.price as prduct_price,b.product_qty,c.company_name as brand_name,e.size_text as product_size from fail_order as a,cart_info b,brand_info as c,product_desc as d,product_size as e where a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.status = 0 AND a.cart_id = b.id AND b.product_id = d.id AND d.brand_id = c.user_id AND b.product_size = e.id ");
        }
        // echo $this->db->last_query();exit;
        $res = $query->result();
        return $res;
    }

    function google_ads(){
        $query = $this->db->query("select distinct a.id,c.company_name,a.brand_id,a.name,a.description,a.image,a.slug,a.price,a.approve_reject,a.status,a.is_delete from product_desc as a,product_inventory as b,brand_info as c where a.id = b.product_id and b.stock_count >= 1 and c.user_id = a.brand_id and c.is_paid = 1 and a.status = 1 and is_delete = 0 and (a.approve_reject = 'A' || a.approve_reject = 'D') order by a.id desc");
        $res = $query->result_array();
        return $res;
    }

       function facebook_ads(){
        $query = $this->db->query("select distinct a.id,c.company_name,a.brand_id,a.name,a.description,a.image,a.slug,a.price,CASE WHEN  DATE_FORMAT(a.discount_start_date,'%Y-%m-%d  %h:%i') <= DATE_FORMAT(now(),'%Y-%m-%d %h:%i') and DATE_FORMAT(a.discount_end_date,'%Y-%m-%d  %h:%i') >= DATE_FORMAT(now(),'%Y-%m-%d %h:%i') then compare_price ELSE 0 END AS compare_price,a.approve_reject,a.status,a.is_delete,a.discount_start_date from product_desc as a,product_inventory as b,brand_info as c where a.id = b.product_id and b.stock_count >= 1 and c.user_id = a.brand_id and c.is_paid = 1 and a.status = 1 and is_delete = 0 and (a.approve_reject = 'A' || a.approve_reject = 'D') order by a.id desc");
        $res = $query->result_array();
        return $res;
    }

    function get_gender($product_id){
        $gender = '';
        $query = $this->db->query("select category_id from category_object_relationship where object_id=".$product_id." and object_type='product' and category_id IN(1,3,2)");
        $res = $query->result_array();

        if(!empty($res)){
            if($res[0]['category_id'] == 1){ $gender = 'Women'; }
            else if($res[0]['category_id'] == 3){ $gender = 'Men'; }
            else if($res[0]['category_id'] == 2){ $gender = 'Unisex'; }
        }

        return $gender;
    }

    function get_categories($product_id){
        $query = $this->db->query("select a.category_id,b.category_name from category_object_relationship as a,category as b where a.category_id = b.id and a.object_type = 'product' and a.object_id =$product_id and a.category_id NOT IN (1,2,3) order by a.category_id asc");
        $res = $query->result_array();
        $product_breadcrumps = '';

        if(!empty($res)){ $i = 0; 
            foreach ($res as $value) {
               if($i!=0){ $product_breadcrumps = $product_breadcrumps.'>'; }
               $product_breadcrumps = $product_breadcrumps.$value['category_name'];
               $i++;
            }
        }
        return $product_breadcrumps;
    }

    public function failed_response_payu_order($date_from,$date_to){
        if($date_from!='' && $date_to!=''){

            $query = $this->db->query(" SELECT * FROM `order_info` as a WHERE `pay_mode` = 2 AND `order_status` = 9 AND  a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' ORDER BY `id` DESC");

        }else if($date_from=='' && $date_to==''){

            $query = $this->db->query("SELECT * FROM `order_info` as a WHERE `pay_mode` = 2 AND `order_status` = 9 AND  a.`created_datetime` like '%".date('Y-m-d')."%' ORDER BY `id` DESC");

        }else if($date_from!='' && $date_to==''){

            $query = $this->db->query("SELECT * FROM `order_info` as a WHERE `pay_mode` = 2 AND `order_status` = 9 AND  a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' ORDER BY `id` DESC ");

        }else if($date_from=='' && $date_to!=''){

            $query = $this->db->query(" SELECT * FROM `order_info` as a WHERE `pay_mode` = 2 AND `order_status` = 9 AND  a.`created_datetime` >= '".$date_from."' and a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' ORDER BY `id` DESC");
        }
        // echo $this->db->last_query();exit;
        $res = $query->result();
        return $res;
    }
} 
?>
