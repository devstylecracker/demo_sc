<?php

class Saveproducts_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function save_homeproducts($product_ids,$key){
		$data = array('object_meta_value'=>$product_ids,'created_datetime'=>date('Y-m-d H:i:s'),'created_by'=> $this->session->userdata('user_id'),'modified_by'=> $this->session->userdata('user_id'));

		$this->db->where('object_meta_key', $key);
		$this->db->update('object_meta', $data); 
		return true;
	}
	
	function get_old_products($key){
		$query = $this->db->query('select object_meta_value from object_meta where object_meta_key = "'.$key.'" ');
		$result = $query->result_array();
		return @$result[0]['object_meta_value'];
	}
	
}