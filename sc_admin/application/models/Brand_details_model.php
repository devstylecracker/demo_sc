<?php
class Brand_details_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function add_brand_user($user_type,$first_name,$last_name,$username,$email,$password,$login_user_id,$store_type,$company_name){
		$data = array("bucket"=>1,"user_name"=>$username,"password"=>$password,"email"=>$email,"ip_address"=>$_SERVER['REMOTE_ADDR'],"status"=>1,"created_by"=>$login_user_id,"created_datetime"=>date('Y-m-d H:i:s'));
		
		$this->db->insert('user_login', $data);
		$last_ins_id = $this->db->insert_id(); 
		
		$user_role_map_data = array("user_id"=>$last_ins_id,"role_id"=>$user_type,"created_by"=>$login_user_id,"created_datetime"=>date('Y-m-d H:i:s'));
		
		$this->db->insert('user_role_mapping', $user_role_map_data);
		
		$user_extra_info = array("user_id"=>$last_ins_id,"first_name"=>$first_name,"last_name"=>$last_name,"created_by"=>$login_user_id,"created_datetime"=>date('Y-m-d H:i:s'));
		
		$this->db->insert('user_info', $user_extra_info);

		$brand_info = array("user_id"=>$last_ins_id,"store_type"=>$store_type,"company_name"=>$company_name,"created_by"=>$login_user_id,"created_datetime"=>date('Y-m-d H:i:s'));
		$brand_contact_info = array("brand_id"=>$last_ins_id,"created_by"=>$login_user_id,"created_datetime"=>date('Y-m-d H:i:s'));

		if($user_type == '6')
		{
			$this->db->insert('brand_contact_info', $brand_contact_info);
			$this->db->insert('brand_info', $brand_info);
		}
		
		$query = $this->db->get_where('role_permission_mappings', array('role_id' => $user_type));
		$res = $query->result_array();

			if(!empty($res)){
				foreach ($res as $value) {
					$data = array('user_id'=>$last_ins_id,'module_id'=>$value['module_id'],'created_by'=>$login_user_id,'created_datetime'=>date('Y-m-d H:i:s'));
					$this->db->insert('user_module_permission', $data);
					//echo $this->db->insert_id(); 
				}

			}
			if($store_type == 1){
				$data = array(
				   'brand_id' => $last_ins_id,
				   'brand_store_name' => $company_name ,
				   'created_datetime' => date('Y-m-d H:i:s'),
				);
				$this->db->insert('brand_store', $data);
			}
			return true;
		
		}
		
	function forgot_password($Emailid)
    {
        $query = $this->db->query("Select email,password from user_login where email='".$Emailid."'");
        $result = $query->result_array();

        if(sizeof($result)>0)
        {
            $scpassword =$this->randomPassword();
            $salt = substr(md5(uniqid(rand(), true)), 0, 10);
            $password   =$salt . substr(sha1($salt . $scpassword), 0, -10);
            $data=array('password'=>$password);
            $this->db->update('user_login', $data, array('email' => $result[0]['email'] ));

            return  $scpassword;
        }else
        {
            return false;
        }
    }
	
	public function randomPassword() {
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
    }

    public function contact_info($post = null){
    	$data = array();
    	$user_id = $this->session->userdata('user_id');

    	if(!empty($post)){
    		
    		$data = array(
    				'company_address' => trim($post['company_address']),
    				'city' => $post['city'],
    				'state' => $post['state'],
    				'pincode' => $post['pincode'],
    				'country' => $post['country'],
    				'contact_person' => $post['contact_person'],
    				'phone_one' => $post['phone_one'],
    				'emailid' => $post['emailid'],
    				'contact_person_two' => $post['contact_person_two'],
    				'phone_two' => $post['phone_two'],
    				'alt_emailid' => $post['alt_emailid'],
    				'contact_person_three' => $post['contact_person_three'],
    				'phone_three' => $post['phone_three'],
    				'alt_emailid_three' => $post['alt_emailid_three'],
					'warehouse_address' => trim($post['warehouse_address']),
    				'warehouse_city' => $post['warehouse_city'],
    				'warehouse_state' => $post['warehouse_state'],
    				'warehouse_pincode' => $post['warehouse_pincode'],
    				'warehouse_country' => $post['warehouse_country'],
    			);

    		$this->db->where('brand_id', $user_id);
			$this->db->update('brand_contact_info', $data);
			$data2 = array(
    				'step_completed'=>'5',
					);
			$this->db->where('user_id', $user_id);
			$this->db->update('brand_info', $data2);
			
    	}


    	$this->db->from('brand_contact_info');
        $this->db->where('brand_id',$user_id);
        $query = $this->db->get();
        $data['contact_info'] = $query->result_array();
        return $data;
    }
	
	public function update_brand_data($data) {
		
		$brand_id = $this->session->userdata('user_id');
		$this->db->where('user_id', $brand_id);
		$this->db->update('brand_info', $data);
		return true;

	}
	public function get_brand_info($brand_id) {
		
		$res = $this->db->get_where('brand_info', array('user_id' => $brand_id));
		return $res->result_array();
		
	}
	
	public function get_brand_list(){
		$query = $this->db->query("Select * from brand_store where status = 1 AND `brand_id` IS NOT NULL ORDER BY `brand_id` ASC");
        $result = $query->result_array();
		return $result;
	}
	
	public function save_brand_store($brand_name,$logo,$cover,$brand_store_id){
		
		$brand_id = $this->session->userdata('user_id');
		
		$query1 = $this->db->query('Select id,brand_store_name,logo,cover,brand_id from brand_store where status = 1 AND `brand_id` = '.$brand_id.' AND `brand_store_name` = "'.$brand_name.'" ORDER BY `brand_id` ASC ');
        $result1 = $query1->result_array();
		if((empty($result1) && $brand_name != '') && $brand_store_id<=0){
			$data = array(
				   'brand_id' => $brand_id,
				   'brand_store_name' => $brand_name ,
				   'logo' => $logo,
				   'cover' => $cover,
				   'created_datetime' => date('Y-m-d H:i:s'),
				);
			$this->db->insert('brand_store', $data);
			
		}else {
			
				$data = array(
				   'brand_id' => $brand_id,
				   'brand_store_name' => $brand_name ,
				   'logo' => $logo,
				   'cover' => $cover,
				   'created_datetime' => date('Y-m-d H:i:s'),
				);
			 $this->db->where(array('id'=>$brand_store_id,'brand_id' => $brand_id));
			 $this->db->update('brand_store', $data);
		}
		
			$data2 = array(
							'step_completed'=>'2',
						  );
			$this->db->where(array('user_id' => $brand_id));
			$this->db->update('brand_info', $data2);
	}
	
	public function get_store_data($brand_id){
		$query = $this->db->query("Select id,brand_store_name,logo,cover,brand_id from brand_store where status = 1 AND `brand_id` = '$brand_id' ORDER BY `brand_id` ASC ");
        $result = $query->result_array();
		return $result;
	}
	
	public function remove_row($brand_name,$brand_id){
		$query = $this->db->query('delete from `brand_store` where `brand_id` = '.$brand_id.' AND `brand_store_name` = "'.$brand_name.'" ');
		return true;
		
	}
	
	public function update_business_data($data){
		$res= $this->db->get_where('brand_info',array('user_id'=>$data['brand_info1']['user_id']));
		$res = $res->result(); 
			if(count($res)>0){
				$this->db->where('user_id', $data['brand_info1']['user_id']);
				$result = $this->db->update('brand_info', $data['brand_info1']);

				if($this->db->affected_rows())
				{
					$this->db->where('user_id', $data['brand_info1']['user_id']);
					$result = $this->db->update('brand_info', array('approve_reject' => 'P'));
				}

			}else{
					$this->db->insert('brand_info', $data['brand_info1']);
			} 
			
			$this->db->delete('brand_target_market_assoc', array('brand_id' => $data['brand_info1']['user_id'])); 
			// echo"<pre>";print_r($data['target_market']);exit;
			if(!empty($data['target_market'])){
				
				foreach($data['target_market'] as $val){
					$target_market[] = array('brand_id'=>$data['brand_info1']['user_id'],'target_market_id'=>$val,'status'=>1, 'created_by'=>$this->session->userdata('user_id'),'modified_by'=>$this->session->userdata('user_id'),'created_datetime'=>date('Y-m-d H:i:s'));
				}
				
				$this->db->insert_batch('brand_target_market_assoc', $target_market);
			
			}
				// echo"<pre>";print_r($target_market);exit;
		  
		 	
			return true;
	}
	
	function target_market(){
		$res = $this->db->get_where('target_market', array('status' => 1));
		return $res->result();
	}	
		
	function target_market_by_brand($brand_id){
		$res = $this->db->query('select target_market_id from brand_target_market_assoc where brand_id='.$brand_id." and status = 1");
		return $res->result_array();
	}
		
	function update_brand_images($cheque,$pancard,$brand_id,$is_confirm){
		$data = array('cheque' => $cheque,
					  'pancard' => $pancard,
					  'is_form_completed' => 1,
					  'step_completed'=>'6',
					  'is_confirm' => $is_confirm,
					  );
		$result = $this->db->update('brand_info', $data,array('user_id' => $brand_id));
		return true;
	}
	
	function check_brand_name($company_name){
		$brand_id = $this->session->userdata('user_id');
		$query = $this->db->query('select id from brand_info where company_name = "'.$company_name.'" AND user_id != '.$brand_id.' ');
		$result = $query->result_array();
		
		if(!empty($result)){
			return true;
		}else return false;
	}
	
	function get_pincode_list($pincodes){
		$query = $this->db->query("select pincode from pincode where pincode NOT IN (".$pincodes.") " );
		$result = $query->result_array();
		if(!empty($result)){
			// echo "<pre>";print_r($result);
			$pincodes = $out = implode(",",array_map(function($a) {return implode(",",$a);},$result)); 
			return $pincodes;
		}else return '';
	}
	
}

?>