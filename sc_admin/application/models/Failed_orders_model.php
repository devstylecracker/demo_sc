<?php
class Failed_orders_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
    function get_failed_orders($date_from,$date_to){
    	  if($date_from!='' && $date_to!=''){
            $query = $this->db->query("select a.*,d.name as product_name,d.price as prduct_price,b.product_qty,c.company_name as brand_name,e.size_text as product_size from fail_order as a,cart_info b,brand_info as c,product_desc as d,product_size as e where  a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' and a.status = 0 AND a.cart_id = b.id AND b.product_id = d.id AND d.brand_id = c.user_id AND b.product_size = e.id ORDER BY a.cart_id DESC");

        }else if($date_from=='' && $date_to==''){
            $query = $this->db->query("select a.*,d.name as product_name,d.price as prduct_price,b.product_qty,c.company_name as brand_name,e.size_text as product_size from fail_order as a,cart_info b,brand_info as c,product_desc as d,product_size as e where a.`created_datetime` like '%".date('Y-m-d')."%' and a.status = 0 AND a.cart_id = b.id AND b.product_id = d.id AND d.brand_id = c.user_id AND b.product_size = e.id ORDER BY a.cart_id DESC");

        }else if($date_from!='' && $date_to==''){
            $query = $this->db->query("select a.*,d.name as product_name,d.price as prduct_price,b.product_qty,c.company_name as brand_name,e.size_text as product_size from fail_order as a,cart_info b,brand_info as c,product_desc as d,product_size as e where a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.status = 0 AND a.cart_id = b.id AND b.product_id = d.id AND d.brand_id = c.user_id AND b.product_size = e.id  ORDER BY a.cart_id DESC");

        }else if($date_from=='' && $date_to!=''){
            $query = $this->db->query("select a.*,d.name as product_name,d.price as prduct_price,b.product_qty,c.company_name as brand_name,e.size_text as product_size from fail_order as a,cart_info b,brand_info as c,product_desc as d,product_size as e where a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.status = 0 AND a.cart_id = b.id AND b.product_id = d.id AND d.brand_id = c.user_id AND b.product_size = e.id  ORDER BY a.cart_id DESC");
        }
        // echo $this->db->last_query();exit;
        $res = $query->result();
        return $res;
    }
	
	function get_state_id($pincode){
		$query = $this->db->query("select a.state_id from city as a,pincode as b where b.pincode = $pincode AND b.city_id = a.id");
		$res = $query->result_array();
		// print_r($res);exit;
		if(!empty($res)){
        return $res[0]['state_id'];
		}else return null;
	}
	
	public function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
  }
  
	function sc_uc_order($data){
        $this->load->model('Schome_model');
        $user_id = 0;
        $SCUniqueID = $data['SCUniqueID'];
        $productPrice = 0; $final_cod_charges =0;
        $order_list = array(); $product_total = 0;
        $new_store_shipping_charges = 0;
            $old_store_shipping_charges = 0;
                $final_shipping_charges = 0;
				
		if($data['paymod'] == 'cod'){
			$payent_method = '1';
		}else $payent_method = '2';

        $time = new DateTime(date('Y-m-d H:i:s'));
        $time->add(new DateInterval('PT330M'));
        $dateM = $time->format('Y-m-d H:i:s');

        $cart_data = $this->get_cart($SCUniqueID,$data['cart_id'],$data['product_qty']);
		// print_r($cart_data);exit;
		// echo $this->db->last_query();exit;
        if(!empty($cart_data)){

            if($user_id == 0){
                $user_id = $this->check_user_exist_or_not($data['uc_email_id']);
            }
            $new_brand = ''; $old_brand = ''; $new_store_tax = 0; $old_store_tax = 0; $new_store_cod_charges = 0; $old_store_cod_charges = 0;
            $brand_total = 0; $order_info = ''; $total_discount_percent=0;
            $product_info = array(); $i = 0;

            if($user_id == 0 ){
                $scusername = $this->Schome_model->randomPassword();
                $scpassword = $this->Schome_model->randomPassword();
                $ip = $this->input->ip_address();
                $this->Schome_model->registerUser('',$scusername,$data['uc_email_id'],$scpassword,$ip,2,'');
                $user_id = $this->check_user_exist_or_not($data['uc_email_id']);
            }

            foreach($cart_data as $val){
				// print_r($val);exit;
                $new_brand = $val['user_id'];
                $new_store_tax = $val['store_tax'];

                $new_store_cod = $val['is_cod'];
                $new_store_cod_charges = $val['cod_charges'];
                $discount_id = $val['discount_id'];
                $productPrice =0;
                if($new_brand != $old_brand){

                    if($brand_total!=0){
                        $tax = $brand_total*($old_store_tax/100);
                        $order_amount = $brand_total+($brand_total*($old_store_tax/100));

                      /* COD CALCULAION START */
                        if($this->getBrandExtraInfo($old_brand,'is_cod')[0]['is_cod'] == 1 && $data['paymod'] == 'cod'){
                            if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0){
                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                                $order_amount = $order_amount + $final_cod_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] >= 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] > 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']<=$product_total && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']>=$product_total) {


                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                                $order_amount = $order_amount + $final_cod_charges;
                            }else { $old_store_cod_charges = 0; }
                        }else{ $old_store_cod_charges = 0; }
                        /* COD CALCULAION END */

                        /* SHIPPING CHARGES START*/
                        if($this->getBrandExtraInfo($old_brand,'is_shipping')[0]['is_shipping'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] == 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] == 0){


                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] >= 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] > 0 && $this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values']<=$product_total && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values']>=$product_total) {

                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;

                            }else { $old_store_shipping_charges = 0; }
                            }else{ $old_store_shipping_charges = 0; }
                        /* SHIPPING CHARGES END*/
                        /* Brand Total Discount */
                        if($val['discount_type']==1 || $val['discount_type']==2 )
                        {
                            $total_discount_percent = $val['discount_percent'];
                        }else if($val['discount_type']==3)
                        {
                            $total_discount_percent = $val['discount_percent'];
                        }
                        /* Brand Total Discount End*/
						echo "1st order amount".$order_amount;exit;
                        $order_info = array(
                                'user_id' => $user_id,
                                'brand_id' => $old_brand,
                                'order_tax_amount' => $tax,
                                'order_sub_total' => $order_amount,
                                'order_total' => $order_amount,
                                'pay_mode' => $data['paymod']=='cod' ? '1' : '2',
                                'created_datetime' =>$dateM,
                                'first_name' => $data['uc_first_name'],
                                'last_name' => $data['uc_last_name'],
                                'email_id' => $data['uc_email_id'],
                                'mobile_no' => $data['uc_mobile'],
                                'shipping_address' => $data['uc_address'],
                                'pincode' => $data['uc_pincode'],
                                'city_name' => $data['uc_city'],
                                'state_name' => $data['uc_state'],
                                'mihpayid' => $data['mihpayid'],
                                'txnid' => $data['txnid'],
                                'order_status' => '1',
                                'cod_amount' => $final_cod_charges,
                                'shipping_amount' => $final_shipping_charges, 
                                'total_discount_percent' => $total_discount_percent
                            );
                        $order_id = $this->sc_place_order($order_info); $product_total =0;
                        $order_list[] = $order_id;
                        $final_cod_charges = 0;
                        $final_shipping_charges = 0;
                        if(!empty($product_info)){
                            foreach($product_info as $value){
                                  $pro_data = array();
                                 $discount_type =  $this->get_discount_id($old_brand,$value['product_id'])['discount_type_id'];         
                                    if( $discount_type == 1){       
                                     $discount_id =  $this->get_discount_id($old_brand,0)['id'];                            
                                    }else if( $discount_type == 2){     
                                        $discount_id =  $this->get_discount_id(0,$value['product_id'])['id'];       
                                    }else{      
                                         $discount_id =0;       
                                         $discount_type =0;     
                                    }       
                                    $discount_percent = $this->get_discount_percentage($old_brand,$value['product_id']);
                                    $productPrice = round($value['price']-($value['price']*$discount_percent/100));
                              
                                $pro_data = array('order_id'=>$order_id,'product_id'=>$value['product_id'],'user_id'=>$user_id,'created_datetime'=>$dateM,'product_size'=>$value['product_size'],'product_qty'=>$data['product_qty'],'product_price'=>$productPrice,'cart_id'=>$value['cart_id'],'discount_percent'=>$discount_percent,'discount_availed_id' =>$discount_id);
                                $sc_o_id = $this->sc_orders_product($pro_data);
                                $this->update_order_cart_info($value['cart_id'],$order_id,$user_id,$data['product_qty']);
                            }
                        }

                        $product_info = [];
                    }
                    //$brand_total = 0;
                    $old_brand = $new_brand;
                    $old_store_tax = $new_store_tax;
                    //$brand_total = $brand_total+$val['price'];

                    $product_info[$i]['product_id'] = $val['product_id'];
                    $product_info[$i]['user_id'] = $user_id;
                    $product_info[$i]['created_datetime'] = $dateM;
                    $product_info[$i]['product_size'] = $val['product_size'];
                    $product_info[$i]['product_qty'] = $data['product_qty'];
                    $product_info[$i]['price'] = $val['price'];
                    $product_info[$i]['cart_id'] = $val['id'];
                    //$product_total = $product_total+$val['price'];
                    /*Added For Discount by Sudha*/
                    if($val['discount_price']!='' || $val['discount_price']!=0)
                    {
                        $product_total = $product_total+$val['discount_price'];
                    }else
                    {
                        $product_total = $product_total+$val['price'];
                    }                    
                    /*End Added For Discount by Sudha*/
                    $brand_total = $product_total;
                }else{
                    //$brand_total = $brand_total+$productPrice;
                    $old_brand = $new_brand;
                    $old_store_tax = $new_store_tax;

                    $product_info[$i]['product_id'] = $val['product_id'];
                    $product_info[$i]['user_id'] = $user_id;
                    $product_info[$i]['created_datetime'] = $dateM;
                    $product_info[$i]['product_size'] = $val['product_size'];
                    $product_info[$i]['product_qty'] = $data['product_qty'];
                    $product_info[$i]['price'] = $val['price'];
                    $product_info[$i]['cart_id'] = $val['id'];
                   // $product_total = $product_total+$val['price'];
                     /*Added For Discount by Sudha*/
                    if($val['discount_price']!='' || $val['discount_price']!=0)
                    {
                        $product_total = $product_total+$val['discount_price'];
                    }else
                    {
                        $product_total = $product_total+$val['price'];
                    }                    
                    /*End Added For Discount by Sudha*/
					
                    $brand_total = $product_total;

                }
                $i++;
            }
            if($brand_total!=0){
                $tax = $brand_total*($old_store_tax/100);
                $order_amount = $brand_total+($brand_total*($old_store_tax/100));
                /* COD CALCULAION START */

                        if($this->getBrandExtraInfo($old_brand,'is_cod')[0]['is_cod'] == 1 && $data['paymod'] == 'cod'){
                            if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] == 0){
                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                                $order_amount = $order_amount + $final_cod_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] >= 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value'] > 0 && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']<=$product_total && $this->getBrandExtraInfo($old_brand,'cod_min_value')[0]['cod_min_value']>=$product_total) {


                                $old_store_cod_charges = $this->getBrandExtraInfo($old_brand,'cod_charges')[0]['cod_charges'];
                                $final_cod_charges = $final_cod_charges + $old_store_cod_charges ;
                                $order_amount = $order_amount + $final_cod_charges;
                            }else { $old_store_cod_charges = 0;  }
                        }else{ $old_store_cod_charges = 0; }
                        /* COD CALCULAION END */

                                 /* SHIPPING CHARGES START*/
                        if($this->getBrandExtraInfo($old_brand,'is_shipping')[0]['is_shipping'] == 1){
                            if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] == 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] == 0){


                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;
                            }else if($this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values'] >= 0 && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values'] > 0 && $this->getBrandExtraInfo($old_brand,'shipping_min_values')[0]['shipping_min_values']<=$product_total && $this->getBrandExtraInfo($old_brand,'shipping_max_values')[0]['shipping_max_values']>=$product_total) {

                                $old_store_shipping_charges = $this->getBrandExtraInfo($old_brand,'shipping_charges')[0]['shipping_charges'];
                                $final_shipping_charges = $final_shipping_charges+ $old_store_shipping_charges;
                                $order_amount = $order_amount + $final_shipping_charges;

                            }else { $old_store_shipping_charges = 0;  }
                            }else{ $old_store_shipping_charges = 0;  }
                        /* SHIPPING CHARGES END*/
						
                $order_info = array(
                                'user_id' => $user_id,
                                'brand_id' => $old_brand,
                                'order_tax_amount' => $tax,
                                'order_sub_total' => $order_amount,
                                'order_total' => $order_amount,
                                'pay_mode' => $data['paymod']=='cod' ? '1' : '2',
                                'created_datetime' =>$dateM,
                                'first_name' => $data['uc_first_name'],
                                'last_name' => $data['uc_last_name'],
                                'email_id' => $data['uc_email_id'],
                                'mobile_no' => $data['uc_mobile'],
                                'shipping_address' => $data['uc_address'],
                                'pincode' => $data['uc_pincode'],
                                'city_name' => $data['uc_city'],
                                'state_name' => $data['uc_state'],
                                'mihpayid' => $data['mihpayid'],
                                'txnid' => $data['txnid'],
                                'order_status' => '1',
                                'cod_amount' => $final_cod_charges,
                                'shipping_amount' => $final_shipping_charges,
                            );
                        $order_id = $this->sc_place_order($order_info); $product_total =0;
                        $order_list[] = $order_id;
                        $final_cod_charges = 0;
                        $final_shipping_charges = 0;
                        if(!empty($product_info)){
                            foreach($product_info as $value){
                                $pro_data = array();
                                 $discount_type =  $this->get_discount_id($old_brand,$value['product_id'])['discount_type_id']; 
                   
                                    if( $discount_type == 1){
                                     $discount_id =  $this->get_discount_id($old_brand,0)['id'];                     
                                    }else if( $discount_type == 2){
                                        $discount_id =  $this->get_discount_id(0,$value['product_id'])['id'];
                                    }else{
                                         $discount_id =0;
                                         $discount_type =0;
                                    }
                                $discount_percent = $this->get_discount_percentage($old_brand,$value['product_id']);
                                $productPrice = round($value['price']-($value['price']*$discount_percent/100));
                                $pro_data = array('order_id'=>$order_id,'product_id'=>$value['product_id'],'user_id'=>$user_id,'created_datetime'=>$dateM,'product_size'=>$value['product_size'],'product_qty'=>$data['product_qty'],'product_price'=>$productPrice,'cart_id'=>$value['cart_id'],'discount_percent'=>$discount_percent,'discount_availed_id' =>$discount_id);
                                $sc_o_id = $this->sc_orders_product($pro_data);
                                $this->update_order_cart_info($value['cart_id'],$order_id,$user_id,$data['product_qty']);
                            }
                        }
                        $product_info = [];



            }
            if(!empty($order_list)){
				$this->update_fail_order_status($SCUniqueID,$cart_data,$data['payment_mode']);
                $this->update_user_address($data,$user_id);
                return $this->update_order_unique_no($order_list,$user_id);
            }
        }else{
           // return 'Your cart is empty';
            return '{"order" : "fail", "msg" : "Your cart is empty"}';
        }

    }
	
	function update_order_unique_no($order_list,$user_id){
        $order_info = '';
        if(!empty($order_list)){
            $i = 0;
            foreach ($order_list as $key => $value) {
                if($i!=0){ $order_info = $order_info.'_'; }
                $order_info = $order_info.$value;
                $i++;
            }

            $data = array(
                'order_unique_no' => $user_id.'_'.$order_info
            );

            $this->db->where_in('id', $order_list);
            $this->db->update('order_info', $data);
            return '{"order" : "success", "msg" : "'.$user_id.'_'.$order_info.'","user_info" : "'.$user_id.'"}';
        }

    }
	
	function update_user_address($data,$user_id){
        //$user_id = $this->session->userdata('user_id');
        $query = $this->db->get_where('user_address', array('user_id' => $user_id,'shipping_address'=>$data['uc_address'],'pincode'=>$data['uc_pincode'],'city_name'=>$data['uc_city'],'state_name'=>$data['uc_state'],'first_name'=>$data['uc_first_name'],'last_name'=>$data['uc_last_name'],'mobile_no'=>$data['uc_mobile']));
        $res = $query->result_array();
        if(empty($res)){
            $add_data = array(
                    'user_id'=>$user_id,
                    'shipping_address'=>$data['uc_address'],
                    'pincode'=>$data['uc_pincode'],
                    'city_name'=>$data['uc_city'],
                    'state_name'=>$data['uc_state'],
                    'first_name'=>$data['uc_first_name'],
                    'last_name'=>$data['uc_last_name'],
                    'mobile_no'=>$data['uc_mobile'],
                );
            $this->db->insert('user_address', $add_data);

        }else{
            $add_data = array(
                    'user_id'=>$user_id,
                    'shipping_address'=>$data['uc_address'],
                    'pincode'=>$data['uc_pincode'],
                    'city_name'=>$data['uc_city'],
                    'state_name'=>$data['uc_state'],
                    'first_name'=>$data['uc_first_name'],
                    'last_name'=>$data['uc_last_name'],
                    'mobile_no'=>$data['uc_mobile'],
                );
            $this->db->where(array('id'=>$data['existing_shipping_add'],'user_id'=>$user_id));
            $this->db->update('user_address', $add_data);
        }
    }
	
	function update_order_cart_info($cart_id,$order_id,$user_id,$product_qty){
        if($cart_id!='' && $order_id!=''){
            $data = array(
               'order_id' => $order_id,
               'user_id' => $user_id,
			   'product_qty' => $product_qty
            );

            $this->db->where('id', $cart_id);
            $this->db->update('cart_info', $data);
        }
    }
	function update_fail_order_status($SCUniqueID,$cart_data,$payent_method){
		$count = count($cart_data);
		$cart_id_in = '';
		  if(!empty($cart_data)){
            $i = 0;
            foreach ($cart_data as $result) {
				if($i != 0){ $cart_id_in = $cart_id_in .',';  } 
                $cart_id_in = $cart_id_in . $cart_data[$i]['id'];
                $i++;
				
		  }}
		 $query = $this->db->query("update fail_order SET `status` = 1 where `SCUniqueID` = $SCUniqueID AND paymod = $payent_method AND `cart_Id` in ($cart_id_in)");
	}
	 function get_cart($uniquecookie,$cart_id,$product_qty){
       $user_id = $this->session->userdata('user_id'); if(!$user_id){ $user_id = ''; }
       /*$query = $this->db->query('select a.`id`,a.`product_id`,d.`return_policy`,a.`product_qty`,b.`name`,(a.`product_qty`*b.price) as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size` from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and (a.`SCUniqueID` = "'.$uniquecookie.'") order by b.`brand_id`');*/
        $query = $this->db->query('select a.`id`,a.`product_id`,d.`return_policy`,a.`product_qty`,b.`name`,b.price as price,b.image,c.`size_text`,d.`company_name`,d.`user_id`,d.min_del_days,d.max_del_days,d.store_tax,d.is_shipping,d.shipping_charges,d.shipping_min_values,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value`,a.`product_size`,b.`id` as pid,b.`price` as product_price from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id`  and (a.`SCUniqueID` = "'.$uniquecookie.'") AND a.id = "'.$cart_id.'" order by b.`brand_id`');
        //$res = $query->result_array();
        //return $res;
        $res = $query->result_array();
        $product_data = array();
            if(!empty($res)){ $i = 0;
                foreach ($res as $key => $value) {
                    $product_data[$i]['id'] = $value['id'];
                    $product_data[$i]['product_id'] = $value['product_id'];
                    $product_data[$i]['product_qty'] = $value['product_qty'];
                    $product_data[$i]['return_policy'] = $value['return_policy'];
                    $product_data[$i]['name'] =$value['name'];
                    $product_data[$i]['price'] = $value['price'] * $product_qty; 
                    $product_data[$i]['image'] =$value['image'];
                    $product_data[$i]['size_text'] =$value['size_text'];   
                    $product_data[$i]['company_name'] =$value['company_name'];                 
                    $product_data[$i]['user_id'] =$value['user_id'];
                    $product_data[$i]['min_del_days'] =$value['min_del_days'];
                    $product_data[$i]['max_del_days'] =$value['max_del_days'];
                    $product_data[$i]['store_tax'] =$value['store_tax'];
                    $product_data[$i]['is_shipping'] =$value['is_shipping'];
                    $product_data[$i]['shipping_charges'] =$value['shipping_charges'];
                    $product_data[$i]['shipping_min_values'] =$value['shipping_min_values'];
                    $product_data[$i]['shipping_max_values'] =$value['shipping_max_values'];
                    $product_data[$i]['is_cod'] =$value['is_cod'];
                    $product_data[$i]['cod_charges'] =$value['cod_charges'];
                    $product_data[$i]['cod_min_value'] =$value['cod_min_value'];
                    $product_data[$i]['code_max_value'] =$value['code_max_value'];
                    $product_data[$i]['product_size'] =$value['product_size'];
                    $product_data[$i]['discount_percent'] = $this->get_discount_percentage($value['user_id'],$value['pid']);
                    $product_data[$i]['discount_price'] =round($product_qty * ($value['product_price']-($value['product_price']*$product_data[$i]['discount_percent'])/100)); 
                    
                    $product_data[$i]['discount_type'] =  $this->get_discount_id($value['user_id'],$value['pid'])['discount_type_id']; 
                   
                    if($product_data[$i]['discount_type'] == 1){
                     $product_data[$i]['discount_id'] =  $this->get_discount_id($value['user_id'],0)['id'];                     
                    }else if($product_data[$i]['discount_type'] == 2){
                        $product_data[$i]['discount_id'] =  $this->get_discount_id(0,$value['pid'])['id'];
                    }else{
                         $product_data[$i]['discount_id'] =0;
                         $product_data[$i]['discount_type'] =0;
                    }               
                    $product_data[$i]['discount_max_amount'] = $this->get_discount_maxvalue($value['user_id'],$value['pid']);                             
                    
                    $i++;
                }
            }            
        return $product_data;
    }//end of get_cart
	
	function check_user_exist_or_not($email_id){
        if($email_id!=''){
            $check_email = $this->emailUnique($email_id);
			// print_r($check_email);exit;
                if(!empty($check_email)){
                   return $check_email[0]['id'];
                }else{
                    return 0;
                }

        }
    }
	
	function get_discount_percentage($brand_id,$product_id){
        if($brand_id > 0){
            $query = $this->db->query("select discount_percent from discount_availed as da where (da.brand_id = '".$brand_id."' || da.product_id = '".$product_id."') AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1");
           /* echo $this->db->last_query();*/
            $result = $query->result_array();
            if(!empty($result)){
                return $result[0]['discount_percent'];
            }else{
                return 0;
            }
        }else{ return 0; }
    }
	 function get_discount_id($brand_id,$product_id)
    { 
        if($brand_id > 0 && $product_id > 0){
           $query = $this->db->query("select id, discount_type_id from discount_availed as da where (da.brand_id = '".$brand_id."' OR da.product_id = '".$product_id."' ) AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1 ");
          /*   $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = '".$brand_id."' AND da.discount_type_id='".$discount_type_id."' ) ");*/          
            $result = $query->result_array();

            if(!empty($result)){
                return $result[0];
            }else{
                return 0;
            }
        }else if($brand_id > 0){
            $query = $this->db->query("select id, discount_type_id from discount_availed as da where (da.brand_id = '".$brand_id."' ) AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1 ");
          /*   $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = '".$brand_id."' AND da.discount_type_id='".$discount_type_id."' ) ");*/          
            $result = $query->result_array();
            if(!empty($result)){
                return $result[0];
            }else{
                return 0;
            }

        }else{ 

            if($product_id > 0){
                $query = $this->db->query("select da.id, da.discount_type_id from discount_availed as da where (da.brand_id = 0 AND da.product_id = '".$product_id."' ) AND da.status=1");
                 /*  $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = '".$product_id."' AND da.discount_type_id='".$discount_type_id."' ) ");*/
                $result = $query->result_array();
                if(!empty($result)){
                    return $result[0];
                }else{
                    return 0;
                }            
            }else
            {
                $query = $this->db->query("select da.id, da.discount_type_id from discount_availed as da where (da.brand_id = '".$brand_id."' AND da.product_id = 0 ) AND da.status=1");
                 /*  $query = $this->db->query("select id  from discount_availed as da where (da.brand_id = '".$product_id."' AND da.discount_type_id='".$discount_type_id."' ) ");*/
                $result = $query->result_array();
                if(!empty($result)){
                    return $result[0];
                }else{
                    return 0;
                }          
            }
         }  
    }
	
	function get_discount_maxvalue($brand_id,$product_id){
        if($brand_id > 0){
            /*$query = $this->db->query("select max_amount, from discount_availed as da where (da.brand_id = '".$brand_id."' || da.product_id = '".$product_id."') AND NOW() >= da.start_date AND NOW() <= da.end_date");*/
            $query = $this->db->query("select max_amount from discount_availed as da where (da.brand_id = '".$brand_id."') AND NOW() >= da.start_date AND NOW() <= da.end_date AND da.status=1");
            /*echo $this->db->last_query();exit;*/
            $result = $query->result_array();
            if(!empty($result)){
                return $result[0]['max_amount'];
            }else{
                return 0;
            }
        }else{ return 0; }
    }
	
	function getBrandExtraInfo($brand_id){
        $query = $this->db->get_where('brand_info', array('user_id' => $brand_id));
        $res = $query->result_array();
        if(!empty($res)){
            return $res;
        }else{
            return '';
        }
    }
	
	function sc_place_order($data){
        if(!empty($data)){
            $this->db->insert('order_info', $data);
            return $this->db->insert_id();
        }
    }

    function sc_orders_product($data){
        if(!empty($data)){
            $this->db->insert('order_product_info', $data);
            return $this->db->insert_id();
        }
    }

    function emailUnique($emaild){
        $query = $this->db->query('select id,email from user_login where email ="'.$emaild.'"');
        $username_unique = $query->result_array();
        return $username_unique;
    }
	
	//added for mail data
	
	function br_get_order_info($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where id = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

            $order_ids = implode(",",$order_info);

           /* $query = $this->db->query('select a.`order_id`, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`, a.`product_price`,b.`name`,b.`image`,b.`brand_id`,c.size_text,d.company_name from order_product_info as a,product_desc as b,product_size as c,`brand_info` as d where a.`product_id` = b.`id` and a.`order_id` in ('.$order_ids.') and a.`user_id`='.$user_id.' and a.product_size = c.`id` and d.`user_id` = b.`brand_id`' );*/
            $query = $this->db->query('select b.`product_sku`,a.`order_id`, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`, a.`product_price`,b.`name`,b.`image`,b.`brand_id`,c.size_text,d.company_name, (a.product_price-a.product_price*a.discount_percent/100) as discount_price from order_product_info as a,product_desc as b,product_size as c,`brand_info` as d where a.`product_id` = b.`id` and a.`order_id` in ('.$order_ids.') and a.`user_id`='.$user_id.' and a.product_size = c.`id` and d.`user_id` = b.`brand_id`');

            $query_res = $query->result_array();
            return $query_res;

        }
    }
	
	function br_get_order_price_info($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where id = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info);

        $query = $this->db->query('select a.id,a.created_datetime,sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no, a.shipping_address,a.pincode,a.city_name,s.state_name,a.brand_id,a.pay_mode from order_info as a,states as s where a.`state_name` = s.`id` and a.id in ('.$order_ids.')' );

            $query_res = $query->result_array();
            return $query_res;
        }
    }
	
	function su_get_order_price_info($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where order_unique_no = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

        $order_ids = implode(",",$order_info);

        $query = $this->db->query('select sum(a.`order_tax_amount`) as order_tax_amount, sum(a.`cod_amount`) as cod_amount, sum(a.`order_sub_total`) as order_sub_total, sum(a.`order_total`) as order_total,sum(a.shipping_amount) as shipping_amount,a.first_name, a.last_name, a.email_id, a.mobile_no, a.shipping_address,a.pincode,a.city_name,s.state_name,a.pay_mode,a.order_status,a.user_id from order_info as a,states as s where a.`state_name` = s.`id` and a.id in ('.$order_ids.')' );

            $query_res = $query->result_array();
            return $query_res;
        }
    }
	
	function su_get_order_info($order_id){
        $query = $this->db->query('select id,user_id from `order_info` where order_unique_no = "'.$order_id.'"');
        $order_id = $query->result_array();

        if(!empty($order_id)){ $order_onfo = array();
            foreach($order_id as $val){
                $order_info[] = $val['id'];
                $user_id = $val['user_id'];
             }

            $order_ids = implode(",",$order_info);

            /*$query = $this->db->query('select a.`order_id`,b.price, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`, a.`product_price`,b.`name`,b.`image`,b.`brand_id`,c.size_text,d.company_name,d.min_del_days,d.max_del_days from order_product_info as a,product_desc as b,product_size as c,`brand_info` as d where a.`product_id` = b.`id` and a.`order_id` in ('.$order_ids.') and a.`user_id`='.$user_id.' and a.product_size = c.`id` and d.`user_id` = b.`brand_id`' );*/
            $query = $this->db->query('select a.`order_id`, a.`product_id`, a.`user_id`, a.`product_size`, a.`product_qty`, a.`cart_id`, a.`product_price`,b.`name`,b.`image`,b.`brand_id`,c.size_text,d.company_name,d.min_del_days,d.max_del_days,(select a.`product_qty`*(b.price-((b.price*discount_percent)/100)) from discount_availed as da where da.brand_id = b.brand_id AND NOW() >= da.start_date AND NOW() <= da.end_date AND (da.discount_type_id=1 ) AND da.status = 1 AND da.is_delete=0 )as discount_price,a.coupon_code  from order_product_info as a,product_desc as b,product_size as c,`brand_info` as d where a.`product_id` = b.`id` and a.`order_id` in ('.$order_ids.') and a.`user_id`='.$user_id.' and a.product_size = c.`id` and d.`user_id` = b.`brand_id`' );

            $query_res = $query->result_array();
            return $query_res;

        }
    }
	
	function get_user_info($user_id){
        $query = $this->db->get_where('user_login', array('id' => $user_id));
        return $query->result_array();
    }
	
	function getUserCoupon($couponCode)
    {
        $query = $this->db->query("SELECT `coupon_code`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `brand_id`, `products`, `exclude_products`,`coupon_used_count`,`usage_limit_per_coupon` FROM `coupon_settings` WHERE NOW() BETWEEN `coupon_start_datetime` AND `coupon_end_datetime` AND`status`=1 AND `is_delete`=0  AND `coupon_code`='".$couponCode."' "); 
            //return $this->db->last_query();
            $result = $query->result_array();
           
            if(!empty($result))
            {
              /* $coupon_info['coupon_products'] =  $coupon_products.$result[0]['products'];*/
               $coupon_info['coupon_amount'] = $result[0]['coupon_amount']; 
               $coupon_info['brand_id'] = $result[0]['brand_id']; 
               $coupon_info['coupon_products'] = $result[0]['products'];
               $coupon_info['coupon_min_spend'] = $result[0]['min_spend'];
               $coupon_info['coupon_max_spend'] = $result[0]['max_spend'];
               $coupon_info['coupon_discount_type'] = $result[0]['discount_type'];
               $coupon_info['coupon_used_count'] = $result[0]['coupon_used_count'];
               $coupon_info['usage_limit_per_coupon'] = $result[0]['usage_limit_per_coupon'];
            }  

            if(!empty($coupon_info))
            {
              return $coupon_info;
            }else
            {
                return 0;
            }               
    }
    
}
?>