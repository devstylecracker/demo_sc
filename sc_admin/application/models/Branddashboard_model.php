<?php
class Branddashboard_model extends MY_Model {    
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    function basic_details($date_from,$date_to){

    	$data['total_sales'] = $this->total_sales($date_from,$date_to);
    	$data['total_products'] = $this->total_products($date_from,$date_to);
    	$data['approved_products'] = $this->total_products_approved($date_from,$date_to);
    	$data['looks_created'] = $this->looks_created($date_from,$date_to);
    	$data['product_used_in_looks'] = $this->product_used_in_looks($date_from,$date_to);
    	$data['cpc_track'] = $this->cpc_track($date_from,$date_to);
    	$data['cpa_price_track'] = $this->cpa_price_track($date_from,$date_to);
    	$data['cpc'] = $this->calculate_cpc($date_from,$date_to);
    	$data['total_orders'] = $this->total_orders($date_from,$date_to);
    	$data['total_orders_info_pending'] = $this->total_orders_info_pending($date_from,$date_to);
        $data['total_orders_info_confirmed'] = $this->total_orders_info_confirmed($date_from,$date_to);
        $data['total_orders_info_dispatched'] = $this->total_orders_info_dispatched($date_from,$date_to);
        $data['total_orders_info_delivered'] = $this->total_orders_info_delivered($date_from,$date_to);
        $data['total_orders_info_cancelled'] = $this->total_orders_info_cancelled($date_from,$date_to);
        $data['total_orders_info_returned'] = $this->total_orders_info_returned($date_from,$date_to);
    	
    	return $data;
    }

    /* Total brandwise sales*/
    function total_sales($date_from,$date_to){
    	$brand_id = $this->session->userdata('user_id');

         if($this->session->userdata('role_id')==6)
        {
            $condition = "b.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        }   

    	if($date_from!='' && $date_to!=''){

    		$query = $this->db->query("select sum(a.product_price) as total_sales from order_product_info as a,product_desc as b,order_info as c where c.id = a.order_id and a.`product_id`=b.`id` and c.order_status!=9 and ".$condition." and a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."'");

    	}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("select sum(a.product_price) as total_sales from order_product_info as a,product_desc as b,order_info as c where c.id = a.order_id and a.`product_id`=b.`id` and c.order_status!=9  and ".$condition);

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("select sum(a.product_price) as total_sales from order_product_info as a,product_desc as b,order_info as c where c.id = a.order_id and a.`product_id`=b.`id` and c.order_status!=9 and  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%'");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("select sum(a.product_price) as total_sales from order_product_info as a,product_desc as b,order_info as c where c.id = a.order_id and a.`product_id`=b.`id` and c.order_status!=9 and  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%'");
    	}

    	$res = $query->result_array();
    	//echo $this->db->last_query();
    	return $res[0]['total_sales'];
    }

    function total_products($date_from,$date_to){
    	$brand_id = $this->session->userdata('user_id');

         if($this->session->userdata('role_id')==6)
        {
            $condition = "a.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        }  
    	
    	if($date_from!='' && $date_to!=''){

    		$query = $this->db->query("select count(a.id) as total_products from product_desc as a where ".$condition." and a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."'");

    	}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("select count(a.id) as total_products from product_desc as a where ".$condition);

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("select count(a.id) as total_products from product_desc as a where ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%'");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("select count(a.id) as total_products from product_desc as a where ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%'");
    	}

    	$res = $query->result_array();
    	//echo $this->db->last_query();
    	return $res[0]['total_products'];
    }

    function total_products_approved($date_from,$date_to){
    	$brand_id = $this->session->userdata('user_id');
    	
         if($this->session->userdata('role_id')==6)
        {
            $condition = "a.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        }  

    	if($date_from!='' && $date_to!=''){

    		$query = $this->db->query("select count(a.id) as total_products from product_desc as a where  ".$condition." and a.`modified_datetime` >= '".$date_from."' and a.`modified_datetime` <= '".$date_to."' and a.`approve_reject` = 'A'");

    	}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("select count(a.id) as total_products from product_desc as a where  ".$condition." and a.`approve_reject` = 'A'");

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("select count(a.id) as total_products from product_desc as a where ".$condition." and a.`modified_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.`approve_reject` = 'A'");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("select count(a.id) as total_products from product_desc as a where  ".$condition." and a.`modified_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.`approve_reject` = 'A'");
    	}

    	$res = $query->result_array();
    	//echo $this->db->last_query();
    	return $res[0]['total_products'];
    }

      function looks_created($date_from,$date_to){
    	$brand_id = $this->session->userdata('user_id');

         if($this->session->userdata('role_id')==6)
        {
            $condition = "c.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        }  
    	
    	if($date_from!='' && $date_to!=''){

    		$query = $this->db->query("SELECT DISTINCT a.id from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and a.`status` = 1 and ".$condition." and a.`modified_datetime` >= '".$date_from."' and a.`modified_datetime` <= '".$date_to."' and a.`status` = 1");

    	}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("SELECT DISTINCT a.id from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and a.`status` = 1 and ".$condition);

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("SELECT DISTINCT a.id from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and a.`status` = 1 and ".$condition." and a.`modified_datetime` like '%".str_replace('00:00:00','',$date_from)."%'");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("SELECT DISTINCT a.id from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and a.`status` = 1 and ".$condition." and a.`modified_datetime` like '%".str_replace('00:00:00','',$date_to)."%'");
    	}

    	$res = $query->result_array();
    	//echo $this->db->last_query();
    	return count($res);
    }

    function product_used_in_looks($date_from,$date_to){
    	$brand_id = $this->session->userdata('user_id');
    	
         if($this->session->userdata('role_id')==6)
        {
            $condition = "c.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        }  

    	if($date_from!='' && $date_to!=''){

    		$query = $this->db->query("SELECT b.product_id as looks from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and ".$condition." and a.`modified_datetime` >= '".$date_from."' and a.`modified_datetime` <= '".$date_to."' and a.`status` = 1");

    	}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("SELECT b.product_id as looks from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and a.`status` = 1 and ".$condition);

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("SELECT b.product_id as looks from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and a.`status` = 1 and ".$condition." and a.`modified_datetime` like '%".str_replace('00:00:00','',$date_from)."%'");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("SELECT b.product_id as looks from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and a.`status` = 1 and ".$condition." and a.`modified_datetime` like '%".str_replace('00:00:00','',$date_to)."%'");
    	}

    	$res = $query->result_array();
    	//echo $this->db->last_query();
    	return count($res);
    }

    function cpc_track($date_from,$date_to){
    	$brand_id = $this->session->userdata('user_id');

         if($this->session->userdata('role_id')==6)
        {
            $condition = "a.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        }  
    	
    	if($date_from!='' && $date_to!=''){

    		$query = $this->db->query("select sum(IF (b.click_count >0, b.click_count, 1) ) as count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and ".$condition." and a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."'");

    	}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("select sum(IF (b.click_count >0, b.click_count, 1)) as count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and  ".$condition);

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("select sum(IF (b.click_count >0, b.click_count, 1)) as count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%'");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("select sum(IF (b.click_count >0, b.click_count, 1)) as count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%'");
    	}

    	$res = $query->result_array();
    	//echo $this->db->last_query();
    	return $res[0]['count'];
    }

    function cpa_price_track($date_from,$date_to){
    	$brand_id = $this->session->userdata('user_id');

         if($this->session->userdata('role_id')==6)
        {
            $condition = "a.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        }  
    	
    	if($date_from!='' && $date_to!=''){

    		$query = $this->db->query("select sum(a.price*IF (b.click_count >0, b.click_count, 1)) as count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and  ".$condition." and a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."'");

    	}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("select sum(a.price*IF (b.click_count >0, b.click_count, 1)) as count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and  ".$condition);

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("select sum(a.price*IF (b.click_count >0, b.click_count, 1)) as count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%'");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("select sum(a.price*IF (b.click_count >0, b.click_count, 1)) as count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%'");
    	}

    	$res = $query->result_array();
    	//echo $this->db->last_query();
    	return $res[0]['count'];
    }

    function calculate_cpc($date_from,$date_to){

    	$brand_id = $this->session->userdata('user_id');
    	
    	$query = $this->db->get_where('brand_info', array('user_id' => $brand_id));
    	$res = $query->result_array();
    	
    	$cpa_price_track = $this->cpa_price_track($date_from,$date_to);
       
        if(!empty($res[0]['cpc_percentage']))
        {
            $cpc_percentage = $res[0]['cpc_percentage'];
        }else
        {
            $cpc_percentage = 0;
        }
    	

    	$cpc = $cpa_price_track*($cpc_percentage/100);
    	return $cpc;
    }

    function total_orders($date_from,$date_to){
    	$brand_id = $this->session->userdata('user_id');

         if($this->session->userdata('role_id')==6)
        {
            $condition = " and a.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = " and 1=1";
        }   

    	if($date_from!='' && $date_to!=''){

    		$query = $this->db->query("select count(a.id) as count from order_info as a,order_product_info as b where a.id = b.order_id and b.order_status != 9 ".$condition." and a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."'");

    	}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("select count(a.id) as count from order_info as a,order_product_info as b where a.id = b.order_id and b.order_status != 9  ".$condition);

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("select count(a.id) as count from order_info as a,order_product_info as b where a.id = b.order_id and b.order_status != 9  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%'");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("select count(a.id) as count from order_info as a,order_product_info as b where a.id = b.order_id and b.order_status != 9 ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%'");
    	}

    	$res = $query->result_array();
    	//echo $this->db->last_query();
    	return $res[0]['count'];

    }

     function total_orders_info_pending($date_from,$date_to){
    	$brand_id = $this->session->userdata('user_id');

        if($this->session->userdata('role_id')==6)
        {
            $condition = "b.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        } 

    	if($date_from!='' && $date_to!=''){

    		$query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and ".$condition." and a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' and a.`product_id` = b.`id` and a.`order_status`=1");

    	}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and  ".$condition." and a.`product_id` = b.`id` and a.`order_status`=1 ");

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.`product_id` = b.`id` and a.`order_status`=1");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.`product_id` = b.`id` and a.`order_status`=1");
    	}

    	$res = $query->result_array();
    	
    	//echo $this->db->last_query();
    	return $res[0]['count'];

    }

    function total_orders_info_confirmed($date_from,$date_to){
        $brand_id = $this->session->userdata('user_id');

        if($this->session->userdata('role_id')==6)
        {
            $condition = "b.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        } 

        if($date_from!='' && $date_to!=''){

            $query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and  ".$condition." and a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' and a.`product_id` = b.`id` and a.`order_status`=2");

        }else if($date_from=='' && $date_to==''){
            $query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and  ".$condition." and a.`product_id` = b.`id` and a.`order_status`=2 ");

        }else if($date_from!='' && $date_to==''){
            $query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.`product_id` = b.`id` and a.`order_status`=2");

        }else if($date_from=='' && $date_to!=''){
            $query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.`product_id` = b.`id` and a.`order_status`=2");
        }

        $res = $query->result_array();
        
        //echo $this->db->last_query();
        return $res[0]['count'];

    }

        function total_orders_info_dispatched($date_from,$date_to){
        $brand_id = $this->session->userdata('user_id');

         if($this->session->userdata('role_id')==6)
        {
            $condition = "b.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        } 

        if($date_from!='' && $date_to!=''){

            $query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and  ".$condition." and a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' and a.`product_id` = b.`id` and a.`order_status`=3");

        }else if($date_from=='' && $date_to==''){
            $query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and  ".$condition." and a.`product_id` = b.`id` and a.`order_status`=3 ");

        }else if($date_from!='' && $date_to==''){
            $query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.`product_id` = b.`id` and a.`order_status`=3");

        }else if($date_from=='' && $date_to!=''){
            $query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.`product_id` = b.`id` and a.`order_status`=3");
        }

        $res = $query->result_array();
        
        //echo $this->db->last_query();
        return $res[0]['count'];

    }

         function total_orders_info_delivered($date_from,$date_to){
        $brand_id = $this->session->userdata('user_id');

         if($this->session->userdata('role_id')==6)
        {
            $condition = "b.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        } 


        if($date_from!='' && $date_to!=''){

            $query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and  ".$condition." and a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' and a.`product_id` = b.`id` and a.`order_status`=4");

        }else if($date_from=='' && $date_to==''){
            $query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and  ".$condition." and a.`product_id` = b.`id` and a.`order_status`=4 ");

        }else if($date_from!='' && $date_to==''){
            $query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.`product_id` = b.`id` and a.`order_status`=4");

        }else if($date_from=='' && $date_to!=''){
            $query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.`product_id` = b.`id` and a.`order_status`=4");
        }

        $res = $query->result_array();
        
        //echo $this->db->last_query();
        return $res[0]['count'];

    }


         function total_orders_info_cancelled($date_from,$date_to){
        $brand_id = $this->session->userdata('user_id');

         if($this->session->userdata('role_id')==6)
        {
            $condition = "b.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        } 

        if($date_from!='' && $date_to!=''){

            $query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and  ".$condition." and a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' and a.`product_id` = b.`id` and a.`order_status`=5");

        }else if($date_from=='' && $date_to==''){
            $query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and  ".$condition." and a.`product_id` = b.`id` and a.`order_status`=5 ");

        }else if($date_from!='' && $date_to==''){
            $query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.`product_id` = b.`id` and a.`order_status`=5");

        }else if($date_from=='' && $date_to!=''){
            $query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.`product_id` = b.`id` and a.`order_status`=5");
        }

        $res = $query->result_array();
        
        //echo $this->db->last_query();
        return $res[0]['count'];

    }

    function total_orders_info_returned($date_from,$date_to){
        $brand_id = $this->session->userdata('user_id');

        if($this->session->userdata('role_id')==6)
        {
            $condition = "b.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        } 

        if($date_from!='' && $date_to!=''){

            $query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and ".$condition." and a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' and a.`product_id` = b.`id` and a.`order_status`=6");

        }else if($date_from=='' && $date_to==''){
            $query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and ".$condition." and a.`product_id` = b.`id` and a.`order_status`=6 ");

        }else if($date_from!='' && $date_to==''){
            $query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where c.id = a.order_id and c.order_status!=9 and ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.`product_id` = b.`id` and a.`order_status`=6");

        }else if($date_from=='' && $date_to!=''){
            $query = $this->db->query("select count(a.id) as count,a.order_status from order_product_info as a,`product_desc`as b,order_info as c where  c.id = a.order_id and c.order_status!=9 and ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.`product_id` = b.`id` and a.`order_status`=6");
        }

        $res = $query->result_array();
        
        //echo $this->db->last_query();
        return $res[0]['count'];

    }

    function total_sales_details($date_from,$date_to){
    	$brand_id = $this->session->userdata('user_id');

        if($this->session->userdata('role_id')==6)
        {
            $condition = "b.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        } 

    	if($date_from!='' && $date_to!=''){

    		$query = $this->db->query("select a.order_id,a.product_id,b.`name`,a.created_datetime,a.product_price from order_product_info as a,product_desc as b where a.`product_id`=b.`id` and ".$condition." and a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."'");

    	}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("select a.order_id,a.product_id,b.`name`,a.created_datetime,a.product_price from order_product_info as a,product_desc as b where a.`product_id`=b.`id` and ".$condition);

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("select a.order_id,a.product_id,b.`name`,a.created_datetime,a.product_price from order_product_info as a,product_desc as b where a.`product_id`=b.`id` and ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%'");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("select a.order_id,a.product_id,b.`name`,a.created_datetime,a.product_price from order_product_info as a,product_desc as b where a.`product_id`=b.`id` and ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%'");
    	}

    	$res = $query->result_array();
    	return $res;
    }

    function view_products($date_from,$date_to){
    	$brand_id = $this->session->userdata('user_id');

        if($this->session->userdata('role_id')==6)
        {
            $condition = "a.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        } 
    	
    	if($date_from!='' && $date_to!=''){

    		$query = $this->db->query("select a.id,a.name,a.created_datetime,a.price from product_desc as a where ".$condition." and a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' order by a.id desc");

    	}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("select a.id,a.name,a.created_datetime,a.price from product_desc as a where ".$condition." order by a.id desc");

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("select a.id,a.name,a.created_datetime,a.price from product_desc as a where  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' order by a.id desc");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("select a.id,a.name,a.created_datetime,a.price from product_desc as a where ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' order by a.id desc");
    	}

    	$res = $query->result_array();
    	return $res;
    }

    function approved_products($date_from,$date_to){
    	$brand_id = $this->session->userdata('user_id');
    	
         if($this->session->userdata('role_id')==6)
        {
            $condition = "a.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        } 

    	if($date_from!='' && $date_to!=''){

    		$query = $this->db->query("select a.id,a.name,a.created_datetime,a.price from product_desc as a where  ".$condition." and a.`modified_datetime` >= '".$date_from."' and a.`modified_datetime` <= '".$date_to."' and a.`approve_reject` = 'A' order by a.id desc");

    	}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("select a.id,a.name,a.created_datetime,a.price from product_desc as a where  ".$condition." and a.`approve_reject` = 'A' order by a.id desc");

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("select a.id,a.name,a.created_datetime,a.price from product_desc as a where  ".$condition." and a.`modified_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.`approve_reject` = 'A' order by a.id desc");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("select a.id,a.name,a.created_datetime,a.price from product_desc as a where ".$condition." and a.`modified_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.`approve_reject` = 'A' order by a.id desc");
    	}

    	$res = $query->result_array();
    	return $res;
    }

    function looks_details($date_from,$date_to){
    	$brand_id = $this->session->userdata('user_id');

         if($this->session->userdata('role_id')==6)
        {
            $condition = "c.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        } 
    	
    	if($date_from!='' && $date_to!=''){

    		$query = $this->db->query("SELECT DISTINCT a.id,a.name,a.modified_datetime,a.status,a.created_datetime from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and a.`status` = 1 and ".$condition." and a.`modified_datetime` >= '".$date_from."' and a.`modified_datetime` <= '".$date_to."' and a.`status` = 1 order by a.id desc");

    	}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("SELECT DISTINCT a.id,a.name,a.modified_datetime,a.status,a.created_datetime from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and a.`status` = 1 and ".$condition." order by a.id desc");

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("SELECT DISTINCT a.id,a.name,a.modified_datetime,a.status,a.created_datetime from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and a.`status` = 1 and ".$condition." and a.`modified_datetime` like '%".str_replace('00:00:00','',$date_from)."%' order by a.id desc");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("SELECT DISTINCT a.id,a.name,a.modified_datetime,a.status,a.created_datetime from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and a.`status` = 1 and  ".$condition." and a.`modified_datetime` like '%".str_replace('00:00:00','',$date_to)."%' order by a.id desc");
    	}

    	$res = $query->result_array();
    	return $res;
    }

    function view_products_used($date_from,$date_to){
    	$brand_id = $this->session->userdata('user_id');

        if($this->session->userdata('role_id')==6)
        {
            $condition = "c.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        } 
    	
    	if($date_from!='' && $date_to!=''){

    		$query = $this->db->query("SELECT a.id,a.name,a.modified_datetime,a.status,a.created_datetime,b.`product_id`,c.name as product_name from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and ".$condition." and a.`modified_datetime` >= '".$date_from."' and a.`modified_datetime` <= '".$date_to."' and a.`status` = 1 order by a.id desc");

    	}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("SELECT a.id,a.name,a.modified_datetime,a.status,a.created_datetime,b.`product_id`,c.name as product_name from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and a.`status` = 1 and  ".$condition." order by a.id desc");

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("SELECT a.id,a.name,a.modified_datetime,a.status,a.created_datetime,b.`product_id`,c.name as product_name from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and a.`status` = 1 and  ".$condition." and a.`modified_datetime` like '%".str_replace('00:00:00','',$date_from)."%' order by a.id desc");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("SELECT a.id,a.name,a.modified_datetime,a.status,a.created_datetime,b.`product_id`,c.name as product_name from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and a.`status` = 1 and ".$condition." and a.`modified_datetime` like '%".str_replace('00:00:00','',$date_to)."%' order by a.id desc");
    	}

    	$res = $query->result_array();
    	return $res;
    }

    function view_products_click($date_from,$date_to){
    	$brand_id = $this->session->userdata('user_id');

         if($this->session->userdata('role_id')==6)
        {
            $condition = "a.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        } 
    	
    	if($date_from!='' && $date_to!=''){

    		$query = $this->db->query("select a.id,a.name,b.created_datetime,a.price,b.user_id,IF (b.click_count >0, b.click_count, 1) as click_count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and  ".$condition." and a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."'");

    	}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("select a.id,a.name,b.created_datetime,a.price,b.user_id,IF(b.click_count >0, b.click_count, 1) as click_count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and  ".$condition);

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("select a.id,a.name,b.created_datetime,a.price,b.user_id,IF(b.click_count >0, b.click_count, 1) as click_count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%'");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("select a.id,a.name,b.created_datetime,a.price,b.user_id,IF(b.click_count >0, b.click_count, 1) as click_count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%'");
    	}

    	$res = $query->result_array();
    	return $res;
    }

    function top_five($date_from,$date_to){
    		
    	$data['top_five_product_buy']  = $this->top_five_product_buy($date_from,$date_to);
    	$data['top_five_product_click'] = $this->top_five_product_click($date_from,$date_to);
    	return $data;

    }

    function top_five_product_buy($date_from,$date_to){
    	$brand_id = $this->session->userdata('user_id');

          if($this->session->userdata('role_id')==6)
        {
            $condition = "b.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        } 
    	
    	if($date_from!='' && $date_to!=''){

    		$query = $this->db->query("select count(a.product_id) as count_product_id,a.product_id,b.`name`,a.created_datetime,a.product_price from order_product_info as a,product_desc as b where a.`product_id`=b.`id` and  ".$condition." and a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' group by a.product_id order by a.created_datetime desc limit 0,5");

    	}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("select count(a.product_id) as count_product_id,a.product_id,b.`name`,a.created_datetime,a.product_price from order_product_info as a,product_desc as b where a.`product_id`=b.`id` and  ".$condition." group by a.product_id order by a.created_datetime desc limit 0,5");

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("select count(a.product_id) as count_product_id,a.product_id,b.`name`,a.created_datetime,a.product_price from order_product_info as a,product_desc as b where a.`product_id`=b.`id` and  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' group by a.product_id order by a.created_datetime desc limit 0,5");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("select count(a.product_id) as count_product_id,a.product_id,b.`name`,a.created_datetime,a.product_price from order_product_info as a,product_desc as b where a.`product_id`=b.`id` and  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' group by a.product_id order by a.created_datetime desc limit 0,5");
    	}

    	$res = $query->result_array();
    	return $res;
    }

    function top_five_product_click($date_from,$date_to){
    	$brand_id = $this->session->userdata('user_id');

          if($this->session->userdata('role_id')==6)
        {
            $condition = "a.`brand_id` = ".$brand_id." ";
        }else
        {
            $condition = "1=1";
        } 
    	
    	if($date_from!='' && $date_to!=''){

    		$query = $this->db->query("select a.id,a.name,b.created_datetime,a.price,b.user_id,IF (b.click_count >0, b.click_count, 1) as click_count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and ".$condition." and a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' group by b.`product_id` order by click_count desc  limit 0,5");

    	}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("select a.id,a.name,b.created_datetime,a.price,b.user_id,IF(b.click_count >0, b.click_count, 1) as click_count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and  ".$condition." group by b.`product_id` order by click_count desc limit 0,5");

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("select a.id,a.name,b.created_datetime,a.price,b.user_id,IF(b.click_count >0, b.click_count, 1) as click_count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and  ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' group by b.`product_id` order by click_count desc  limit 0,5");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("select a.id,a.name,b.created_datetime,a.price,b.user_id,IF(b.click_count >0, b.click_count, 1) as click_count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and ".$condition." and a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' group by b.`product_id` order by click_count desc  limit 0,5");
    	}

    	$res = $query->result_array();
    	return $res;
    }
	
	public function get_brand_info($brand_id) {
		
		$res = $this->db->get_where('brand_info', array('user_id' => $brand_id));
		return $res->result_array();
		
	}
}
?>