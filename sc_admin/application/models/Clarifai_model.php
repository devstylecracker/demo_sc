<?php
class Clarifai_model extends MY_Model {
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    function show_gender_specific_personalisation($gender){
    	$where_cond='';
       
    	if($gender==1){ $where_cond=$where_cond.' and a.id<=36';}
    	if($gender==3){ $where_cond=$where_cond.' and a.id>36';}
    	$query = $this->db->query("select a.id,(select answer from answers where id =a.body_shape) as body_shape,(select answer from answers where id = a.style) as style,(select answer from answers where id = a.work_style) as work_style,(select answer from answers where id = a.personal_style) as personal_style from bucket as a where 1 ".$where_cond);
        $res = $query->result_array();
        return $res;
    }

    function show_category_specific_personalisation($show_category_specific_personalisation){
    	$query = $this->db->query("select a.object_id,b.category_name from pa_object_relationship as a,category as b where a.object_type='category' and a.pa_type='bucket' and a.object_id=b.id and a.object_value=".$show_category_specific_personalisation."");
        $res = $query->result_array();
        return $res;
    }

    function get_all_attributes(){
        $this->db->from('attributes');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    function product_personalisation_logic($offset){
        $length=50;
        $offset=$offset*$length;
        $query = $this->db->query("SELECT `id`, (select category_name from category where id=`object_id`) as cat_name,`object_type`, `pa_type`, `object_value`, `attribute_value_ids` FROM `pa_object_relationship` WHERE 1 and pa_type='bucket' and object_type='category' and attribute_value_ids!='' order by `modified_datetime` desc limit ".$offset.",".$length);
        $res = $query->result_array();
        return $res;
    }

    function get_bucket_name($bucket){
        $query = $this->db->query("select a.id,(select answer from answers where id =a.body_shape) as body_shape,(select answer from answers where id = a.style) as style,(select answer from answers where id = a.work_style) as work_style,(select answer from answers where id = a.personal_style) as personal_style from bucket as a where 1 and a.id=".$bucket);
        $res = $query->result_array();
        return strip_tags($res[0]['body_shape'].''.$res[0]['style'].''.$res[0]['work_style'].''.$res[0]['personal_style']);
    }

    function attribute_value_data($attr){
        if($attr!=''){ $dt = array(); $ht_data = '';
            $logic = unserialize($attr);
            if(!empty($logic)){
                $attr_val_ids = implode(',', $logic);

                $query = $this->db->query("select a.attr_value_name,b.attribute_name from attribute_values_relationship as a,attributes as b where a.attribute_id=b.id and a.id IN (".$attr_val_ids.") order by a.attribute_id");
                $res = $query->result_array();
                if(!empty($res)){ $i=0;
                    foreach ($res as $value) {
                        $dt[$value['attribute_name']][$i] = $value['attr_value_name'];
                        $i++;
                    }
                }
                unset($i);
            }

            if(!empty($dt)){ $i=0;
                foreach ($dt as $key=>$val){
                    if($i!=0){ $ht_data = $ht_data. '<br>'; }
                    $ht_data = $ht_data. $key.' => ';
                    $ht_data = $ht_data. $vv = implode(', ', $val);
                    $i++;
                }
            }

        }
        return $ht_data;
    }
    

    function existing_attr_values($personality,$category){
        $query = $this->db->query("select attribute_value_ids from pa_object_relationship where object_type='category' and pa_type='bucket' and object_id='".$category."' and object_value='".$personality."' and attribute_value_ids!='' limit 0,1");
        $res = $query->result_array();
        if(!empty($res)){
            return unserialize($res[0]['attribute_value_ids']);
        }else{
            return '';
        }
    }
   
    function show_gender_specific_categories($gender){
        $gender_id='1';
        if($gender==1){ $gender_id='1'; }
        if($gender==3){ $gender_id='3'; }

        $query = $this->db->query("select id,category_name,category_parent from (select * from category order by category_parent, id) products_sorted,(select @pv := '".$gender_id."') initialisation where find_in_set(category_parent, @pv) > 0 and @pv := concat(@pv, ',', id)");
        $res = $query->result_array();
        return $res;
    }

    function get_options($option_name){
        return unserialize($this->get_sc_options($option_name));
    }   

    function product_availability($product_ids){
        $pro_id = '';
        if($product_ids!=''){
                $query11 = $this->db->query("select id from product_desc where status=1 and approve_reject IN('A','D') and id in(0".$product_ids.")");
                $res11 = $query11->result_array();

                if(!empty($res11)){
                    foreach($res11 as $val){
                        $pro_id = $pro_id.','.$val['id'];
                    }
                }
             return $pro_id;
        }else{
            return $pro_id;
        }
    }

    function in_stock_check($prod_id){
        $query = $this->db->query("select distinct product_id from product_inventory where stock_count>0 and product_id IN (0".$prod_id.")");
        $res1 = $query->result_array();
        $pro_id = ''; $product_arr = array();
        if(!empty($res1)){ $i=0;
            foreach ($res1 as $key => $value) {
               if($i!=0){ $pro_id = $pro_id.','; }
               $pro_id = $pro_id.$value['product_id'];
               $i++;
            }
        }
        return $pro_id;
    }   

    function recommended_products_new($bucket,$product_id){
        $prod_id = explode(',', $product_id);
        $this->get_category_id($prod_id);

    }

    function get_category_id($prod_id){
        $cat_ids = array();
        if(!empty($prod_id)){
            //foreach($prod_id as $val){

                $query = $this->db->query("select a.category_id,b.category_name from category_object_relationship as a,category as b where a.category_id = b.id and a.object_type='product' and a.`object_id`=".$prod_id." and b.status = 1 order by a.category_id desc limit 0,1");                
                $res = $query->result_array();
                $cat_ids['cat_id'] = $res[0]['category_id'];
                $cat_ids['cat_name'] = $res[0]['category_name'];
            //}
        }
        return $cat_ids;
    }

     function get_attribute_id($prod_id){ 
        //$prod_id='13739';
        //$attr_ids = array();
        $attr_ids = '';
        if(!empty($prod_id)){
            //foreach($prod_id as $val){

                $query11 = $this->db->query("select object_id ,GROUP_CONCAT(attribute_id) as attr from attribute_object_relationship where object_id = ".$prod_id." group by object_id");
                $res11 = $query11->result_array();                
                //$attr_ids[] = $res11[0]['attr'];
                if(!empty($attr_ids))
                {                    
                 $attr_ids = $res11[0]['attr'];
                }

           // }
        }
        return $attr_ids;
    }

    function get_products()
    {
        $pro_id = '';
        $query11 = $this->db->query("select id from product_desc where status=1 and is_delete=0 and approve_reject IN('A','D') and brand_id in(27947)");
        $res11 = $query11->result_array();
       // echo $this->db->last_query();echo '<br/>';
        if(!empty($res11)){
            foreach($res11 as $val){
                $pro_id = $pro_id.','.$val['id'];
            }
        }
        //echo $pro_id;
        $productids = $this->in_stock_check($pro_id);

         $query = $this->db->query("select id,name,image,(Select company_name from brand_info where brand_id=user_id ) as brand,price from product_desc where status=1 and is_delete=0 and id in (".$productids.") and brand_id IN (select user_id from brand_info where is_paid=1 and user_id in (27947)) order by id desc limit 5,7");
         $res = $query->result_array();
         //echo $this->db->last_query();exit;
         return $res;
         //,22178,20512
    }

    function get_parent_cat($cat_id)
    {
        
        $query = $this->db->query("SELECT Concat(T2.category_slug,-T2.id) as slug,T2.category_name as cat_name,T2.id as category_id FROM (SELECT @r AS _id, (SELECT @r := category_parent FROM category WHERE id = _id) AS category_parent, @l := @l + 1 AS lvl FROM (SELECT @r := $cat_id) vars, category m WHERE @r <> 0) T1 JOIN category T2 ON T1._id = T2.id ORDER BY T1.lvl ASC");
        
        // $query = $this->db->query("SELECT Concat(T2.category_slug,-T2.id) as slug,T2.category_name as cat_name,T2.id as category_id FROM (SELECT @r AS _id, (SELECT @r := category_parent FROM category WHERE id = _id) AS category_parent, @l := @l + 1 AS lvl FROM (SELECT @r := $cat_id) vars, category m WHERE @r <> 0) T1 JOIN category T2 ON T1._id = T2.id WHERE T2.id IN(1,2,3)  ORDER BY T1.lvl ASC ");
        $res = $query->result_array();
        return $res;
    }

    function get_bucketByCategory($cat_id)
    {
        $query = $this->db->query("select por.id,por.object_value,(select (select answer from answers where id =a.body_shape) as body_shape  from bucket as a where a.id =por.object_value ) as body_shape,(select (select answer from answers where id =a.style) as style from bucket as a where a.id = por.object_value ) as style, por.object_id,por.attribute_value_ids from pa_object_relationship as por where object_type='category' and pa_type='bucket' and object_id =".$cat_id." ");
        $res = $query->result_array();
        return $res;

        /*
            (select a.id,(select answer from answers where id =a.body_shape) as body_shape,(select answer from answers where id = a.style) as style,(select answer from answers where id = a.work_style) as work_style,(select answer from answers where id = a.personal_style) as personal_style from bucket as a where a.id =".$cat_id." )
        */
    }

 }
?>	