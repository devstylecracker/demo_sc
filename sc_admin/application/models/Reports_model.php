<?php
ini_set('max_execution_time',3600);
class Reports_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


    function get_signup_graphdata($date_from,$date_to){
		if($date_from!='' && $date_to!=''  && $date_from!=$date_to){
		$query = $this->db->query("select count(a.id) as count_user, b.registered_from from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id  and c.role_id = 2 and a.test_user!=1 and a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <= '".$date_to." 23:59:59' group by b.registered_from ");
		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
			$query = $this->db->query("select count(a.id) as count_user,b.registered_from from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id  and c.role_id = 2 and a.test_user!=1 and a.created_datetime like '%".$date_from."%' group by b.registered_from");
		}else{
		$query = $this->db->query("select count(a.id) as count_user,b.registered_from from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id  and c.role_id = 2 and a.test_user!=1 and a.created_datetime like '%".date('Y-m-d')."%' group by b.registered_from ");
		}
		$res = $query->result_array();
		return $res;	
	}


	 function get_signup_graphDetails($type,$date_from,$date_to,$page,$per_page){

		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
				}else{ $page = 0;}
					$limit_cond = "LIMIT ".$page.",".$per_page;
				}else{
					$limit_cond = '';	
				}

		if($date_from!='' && $date_to!=''  && $date_from!=$date_to && $date_to!=0){
		$query = $this->db->query("select a.id,a.user_name, a.email, b.first_name, b.last_name, b.modified_datetime as last_login from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = '".$type."' and a.test_user!=1 and c.role_id = 2 and a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <= '".$date_to." 23:59:59' order by b.modified_datetime desc ");

		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to && $date_from!=0 && $date_to!=0 ) || ($date_from!='' && $date_to!='' && $date_from!=0 ))
		{

			$query = $this->db->query("select a.id,a.user_name, a.email, b.first_name, b.last_name, b.modified_datetime as last_login from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = '".$type."' and a.test_user!=1 and c.role_id = 2 and a.created_datetime like '%".$date_from."%' order by b.modified_datetime desc ");
		}else{

		$query = $this->db->query("select a.id,a.user_name, a.email, b.first_name, b.last_name, b.modified_datetime as last_login from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = '".$type."' and a.test_user!=1 and c.role_id = 2 and a.created_datetime like '%".date('Y-m-d')."%' order by b.modified_datetime desc ");
		
		}
		//echo $this->db->last_query();
		$result['data'] = $query->result_array();
		$result['fields'] = $query->list_fields(); 
		return $result;	
	}
	
	/*function get_signup_website_info($date_from,$date_to){
		if($date_from!='' && $date_to!=''  && $date_from!=$date_to){
		$query = $this->db->query("select count(a.id) as count_user from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'website' and c.role_id = 2 and a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <= '".$date_to." 23:59:59' ");
		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
			$query = $this->db->query("select count(a.id) as count_user from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'website' and c.role_id = 2 and a.created_datetime like '%".$date_from."%' ");
		}else{
		$query = $this->db->query("select count(a.id) as count_user from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'website' and c.role_id = 2 and a.created_datetime like '%".date('Y-m-d')."%' ");
		}
		$res = $query->result();
		return $res;	
	}

	function get_signup_website_details($date_from,$date_to,$page,$per_page){
			
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
				}else{ $page = 0;}
					$limit_cond = "LIMIT ".$page.",".$per_page;
				}else{
					$limit_cond = '';	
				}

		if($date_from!='' && $date_to!=''  && $date_from!=$date_to && $date_to!=0){
		$query = $this->db->query("select a.id,a.user_name, a.email, b.first_name, b.last_name, b.modified_datetime as last_login from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'website' and c.role_id = 2 and a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <= '".$date_to." 23:59:59' order by b.modified_datetime desc");

		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to && $date_from!=0 && $date_to!=0 ) || ($date_from!='' && $date_to!='' && $date_from!=0 ))
		{

			$query = $this->db->query("select a.id,a.user_name, a.email, b.first_name, b.last_name, b.modified_datetime as last_login from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'website' and c.role_id = 2 and a.created_datetime like '%".$date_from."%' order by b.modified_datetime desc");
		}else{

		$query = $this->db->query("select a.id,a.user_name, a.email, b.first_name, b.last_name, b.modified_datetime as last_login from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'website' and c.role_id = 2 and a.created_datetime like '%".date('Y-m-d')."%' order by b.modified_datetime desc");
		
		}		
		$result['data'] = $query->result_array();
		$result['fields'] = $query->list_fields(); 
		return $result;	
	}

	function get_signup_facebook_info($date_from,$date_to){
		if($date_from!='' && $date_to!='' && $date_from!=$date_to){
			$query = $this->db->query("select count(a.id) as count_user from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'facebook' and c.role_id = 2 and a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <= '".$date_to." 23:59:59'");
		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!='')){
			$query = $this->db->query("select count(a.id) as count_user from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'facebook' and c.role_id = 2 and a.created_datetime like '%".$date_from."%' ");
		}else{
		$query = $this->db->query("select count(a.id) as count_user from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'facebook' and c.role_id = 2 and a.created_datetime like '%".date('Y-m-d')."%' ");
		}
		$res = $query->result();
		return $res;	
	}

	function get_signup_facebook_details($date_from,$date_to,$page,$per_page){
			if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
				}else{ $page = 0;}
					$limit_cond = "LIMIT ".$page.",".$per_page;
				}else{
					$limit_cond = '';	
				}
		if($date_from!='' && $date_to!='' && $date_from!=$date_to && $date_to!=0){
			$query = $this->db->query("select a.id,a.user_name, a.email, b.first_name, b.last_name, b.modified_datetime as last_login from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'facebook' and c.role_id = 2 and a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <='".$date_to." 23:59:59' order by b.modified_datetime desc");
			//if(($date_from!='' && $date_to!='' && $date_from==$date_to && $date_from!=0) || ($date_from!='' || $date_to!='' && $date_to==0))
		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to && $date_from!=0 && $date_to!=0 ) || ($date_from!='' && $date_to!='' && $date_from!=0 ))
		{
			$query = $this->db->query("select a.id,a.user_name, a.email, b.first_name, b.last_name, b.modified_datetime as last_login from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'facebook' and c.role_id = 2 and a.created_datetime like '%".$date_from."%' order by b.modified_datetime desc");
		}else{
		
		$query = $this->db->query("select a.id,a.user_name, a.email, b.first_name, b.last_name, b.modified_datetime as last_login from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'facebook' and c.role_id = 2 and a.created_datetime like '%".date('Y-m-d')."%' order by b.modified_datetime desc");
		}
		//$res = $query->result();
		$result['data'] = $query->result_array();
		$result['fields'] = $query->list_fields(); 
		return $result;	
	}	

	function get_signup_gmail_info($date_from,$date_to){
		if($date_from!='' && $date_to!='' && $date_from!=$date_to){
			$query = $this->db->query("select count(a.id) as count_user from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'google' and c.role_id = 2 and a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <='".$date_to." 23:59:59'");
		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
			$query = $this->db->query("select count(a.id) as count_user from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'google' and c.role_id = 2 and a.created_datetime like '%".$date_from."%' ");	
		}else{
		$query = $this->db->query("select count(a.id) as count_user from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'google' and c.role_id = 2 and a.created_datetime like '%".date('Y-m-d')."%' ");
		}
		$res = $query->result();
		return $res;	
	}

	function get_signup_gmail_details($date_from,$date_to,$page,$per_page){

		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
				}else{ $page = 0;}
					$limit_cond = "LIMIT ".$page.",".$per_page;
				}else{
					$limit_cond = '';	
				}

		if($date_from!='' && $date_to!='' && $date_from!=$date_to && $date_to!=0){
			$query = $this->db->query("select a.id,a.user_name, a.email, b.first_name, b.last_name, b.modified_datetime as last_login from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'google' and c.role_id = 2 and a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <='".$date_to." 23:59:59' order by b.modified_datetime desc");
		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to && $date_from!=0 && $date_to!=0 ) || ($date_from!='' && $date_to!='' && $date_from!=0 ))
		{
			$query = $this->db->query("select a.id,a.user_name, a.email, b.first_name, b.last_name, b.modified_datetime as last_login from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'google' and c.role_id = 2 and a.created_datetime like '%".$date_from."%' order by b.modified_datetime desc");	
		}else{
		$query = $this->db->query("select a.id,a.user_name, a.email, b.first_name, b.last_name, b.modified_datetime as last_login from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'google' and c.role_id = 2 and a.created_datetime like '%".date('Y-m-d')."%' order by b.modified_datetime desc");
		}
		//$res = $query->result();
		$result['data'] = $query->result_array();
		$result['fields'] = $query->list_fields(); 
		return $result;		
	}

	function get_signup_mobile_info($date_from,$date_to){
		if($date_from!='' && $date_to!='' && $date_from!=$date_to){
			$query = $this->db->query("select count(a.id) as count_user from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'mobile' and c.role_id = 2 and a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <='".$date_to." 23:59:59' ");
			
		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!='')) 
		{
			$query = $this->db->query("select count(a.id) as count_user from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'mobile' and c.role_id = 2 and a.created_datetime like '%".$date_from."%' ");
		}else{
			$query = $this->db->query("select count(a.id) as count_user from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'mobile' and c.role_id = 2 and a.created_datetime like '%".date('Y-m-d')."%' ");
		}
		$res = $query->result();
		return $res;	
	}

	function get_signup_mobile_details($date_from,$date_to,$page,$per_page){

		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
				}else{ $page = 0;}
					$limit_cond = "LIMIT ".$page.",".$per_page;
				}else{
					$limit_cond = '';	
				}

		if($date_from!='' && $date_to!='' && $date_from!=$date_to && $date_to!=0){
			$query = $this->db->query("select a.id,a.user_name, a.email, b.first_name, b.last_name, b.modified_datetime as last_login from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'mobile' and c.role_id = 2 and a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <='".$date_to." 23:59:59' order by b.modified_datetime desc");
			
		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to && $date_from!=0 && $date_to!=0 ) || ($date_from!='' && $date_to!='' && $date_from!=0 )) 
		{
			$query = $this->db->query("select a.id,a.user_name, a.email, b.first_name, b.last_name, b.modified_datetime as last_login from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'mobile' and c.role_id = 2 and a.created_datetime like '%".$date_from."%' order by b.modified_datetime desc");
		}else{
			$query = $this->db->query("select a.id,a.user_name, a.email, b.first_name, b.last_name, b.modified_datetime as last_login from user_login as a,user_info as b,user_role_mapping as c where a.status = 1 and a.id = b.user_id and c.user_id = a.id and b.registered_from = 'mobile' and c.role_id = 2 and a.created_datetime like '%".date('Y-m-d')."%' order by b.modified_datetime desc");
		}
		$result['data'] = $query->result_array();
		$result['fields'] = $query->list_fields(); 
		return $result;		
	}*/
	
	function most_returning_users($date_from,$date_to){
		$res = array();
		/*if($date_from!='' && $date_to!='' && $date_from!=$date_to){		

			$query = $this->db->query("select a.`id` as user_id, a.`user_name`, a.`email` as email, s.`last_visit_time`, b.`visit_count` as count from `user_login` as a,`user_history_info` as b,`user_visit_history`as s,`user_role_mapping` as c where
			 a.`id` = b.`user_id` and c.`user_id` = a.`id` and a.`id`=s.`id` and c.`role_id` = 2 and a.test_user!=1 and b.first_visit >= '".$date_from." 00:00:00' and b.first_visit <='".$date_to." 23:59:59' order by visit_count desc limit 0,5");

		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{			
			$query = $this->db->query("select a.`id` as user_id, a.`user_name`, a.`email` as email, s.`last_visit_time`, b.`visit_count` as count from `user_login` as a,`user_history_info` as b,`user_visit_history`as s,`user_role_mapping` as c where
			 a.`id` = b.`user_id` and c.`user_id` = a.`id` and a.`id`=s.`id` and c.`role_id` = 2 and a.test_user!=1 and b.first_visit >= '".$date_from." 00:00:00' and b.first_visit <= '".$date_to." 23:59:59' order by visit_count desc limit 0,5");

		}else{
		$query = $this->db->query("select a.`id` as user_id, a.`user_name`, a.`email` as email, s.`last_visit_time`, b.`visit_count` as count from `user_login` as a,`user_history_info` as b,`user_visit_history`as s,`user_role_mapping` as c where
			 a.`id` = b.`user_id` and c.`user_id` = a.`id` and a.`id`=s.`id` and c.`role_id` = 2 and a.test_user!=1 and b.first_visit like '%".date('Y-m-d')."%' order by visit_count desc limit 0,5");
		}
		$res = $query->result_array();
		$this->db->last_query();*/
		return $res;	
	}

	function most_least_runing($date_from,$date_to){
		if($date_from!='' && $date_to!='' && $date_from!=$date_to){		
		$query = $this->db->query("select a.`id` as user_id, a.`user_name`, a.`email` as email, a.`modified_datetime` as last_visit_time from `user_login` as a where a.`created_datetime` = a.`modified_datetime` and a.test_user!=1 and a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <= '".$date_to." 23:59:59' order by a.created_datetime desc limit 0,5");
		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
			$query = $this->db->query("select a.`id` as user_id, a.`user_name`, a.`email` as email, a.`modified_datetime` as last_visit_time from `user_login` as a where a.`created_datetime` = a.`modified_datetime` and a.test_user!=1 and a.created_datetime between '".$date_from."' and '".$date_to."' order by a.created_datetime desc limit 0,5");
		}else{
		$query = $this->db->query("select a.`id` as user_id, a.`user_name`, a.`email` as email, a.`modified_datetime` as last_visit_time from `user_login` as a where a.`created_datetime` = a.`modified_datetime` and a.test_user!=1 and a.created_datetime between '".$date_from."' and '".$date_to."' order by a.created_datetime desc limit 0,5");
		}
		$res = $query->result_array();
		return $res;
	}

	function most_sharing($date_from,$date_to){
		$res = array();	
		/*if($date_from!='' && $date_to!='' && $date_from!=$date_to){
		$query = $this->db->query("SELECT count(a.id) as count,a.user_id,b.user_name,b.email FROM `user_share` as a,`user_login` as b where a.`user_id`=b.`id` and b.`test_user`!=1 and a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <= '".$date_to." 23:59:59'  group by a.user_id order by count desc  limit 0,5");
		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
			$query = $this->db->query("SELECT count(a.id) as count,a.user_id,b.user_name,b.email FROM `user_share` as a,`user_login` as b where a.`user_id`=b.`id` and b.`test_user`!=1 and a.created_datetime like '%".$date_from."%'  group by a.user_id order by count desc  limit 0,5");
		}else{
		$query = $this->db->query("SELECT count(a.id) as count,a.user_id,b.user_name,b.email FROM `user_share` as a,`user_login` as b where a.`user_id`=b.`id` and b.`test_user`!=1 and a.created_datetime like '%".date('Y-m-d')."%'  group by a.user_id order by count desc  limit 0,5");
		}
		$res = $query->result_array();*/
		return $res;
	}

	function most_fav($date_from,$date_to){
		$res = array();
		/*if($date_from!='' && $date_to!='' && $date_from!=$date_to){
		$query = $this->db->query("SELECT count(a.id) as count,a.user_id,b.user_name,b.email FROM `look_fav` as a,`user_login` as b where a.`user_id`=b.`id` and b.`test_user`!=1 and a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <= '".$date_to." 23:59:59'  group by a.user_id order by count desc  limit 0,5");
		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
			$query = $this->db->query("SELECT count(a.id) as count,a.user_id,b.user_name,b.email FROM `look_fav` as a,`user_login` as b where a.`user_id`=b.`id` and b.`test_user`!=1 and a.created_datetime like '%".$date_from."%'  group by a.user_id order by count desc  limit 0,5");
		}else{
		$query = $this->db->query("SELECT count(a.id) as count,a.user_id,b.user_name,b.email FROM `look_fav` as a,`user_login` as b where a.`user_id`=b.`id` and b.`test_user`!=1 and a.created_datetime like '%".date('Y-m-d')."%'  group by a.user_id order by count desc  limit 0,5");
		}
		$res = $query->result_array();*/
		return $res;
	}

	function most_wardrobe($date_from,$date_to){
		if($date_from!='' && $date_to!='' && $date_from!=$date_to){
		$query = $this->db->query("SELECT count(a.id) as count,a.user_id,b.user_name,b.email FROM `users_wardrobe` as a,`user_login` as b where a.`user_id`=b.`id` and b.`test_user`!=1 and a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <='".$date_to." 23:59:59'  group by a.user_id order by count desc  limit 0,5");
		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
			$query = $this->db->query("SELECT count(a.id) as count,a.user_id,b.user_name,b.email FROM `users_wardrobe` as a,`user_login` as b where a.`user_id`=b.`id` and b.`test_user`!=1 and a.created_datetime like '%".$date_from."%'  group by a.user_id order by count desc  limit 0,5");
		}else{
		$query = $this->db->query("SELECT count(a.id) as count,a.user_id,b.user_name,b.email FROM `users_wardrobe` as a,`user_login` as b where a.`user_id`=b.`id` and b.`test_user`!=1 and a.created_datetime like '%".date('Y-m-d')."%' group by a.user_id order by count desc  limit 0,5");
		}
		$res = $query->result_array();
		return $res;
	}


	function get_brand_onboard($date_from,$date_to)
	{
		/*if($date_from!='' && $date_to!='')
		{
			$query = $this->db->query("SELECT a.`user_id` as brand_id, a.`company_name`, a.`cpc_percentage`, a.`created_datetime`, a.`modified_datetime`, a.`cpa_percentage` FROM `brand_info` as a WHERE 1 and a.`is_paid`=1 and a.`created_datetime`<='".$date_to."' order by a.created_datetime");
		}else
		{
			$query = $this->db->query("SELECT a.`user_id` as brand_id, a.`company_name`, a.`cpc_percentage`, a.`created_datetime`, a.`modified_datetime`, a.`cpa_percentage` FROM `brand_info` as a WHERE 1 and  a.`is_paid`=1 order by a.created_datetime");	
		}*/

		if($date_from!='' && $date_to!='' && $date_from!=$date_to)
		{
			$query = $this->db->query("SELECT a.`user_id` as brand_id, a.`company_name`, a.`cpc_percentage`, a.`created_datetime`, a.`onboarded_date`, a.`cpa_percentage` FROM `brand_info` as a WHERE 1 and a.`is_paid`=1 and a.`onboarded_date` >= '".$date_from." 00:00:00' and a.`onboarded_date` <=  '".$date_to." 23:59:59' order by a.onboarded_date desc");

		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
			$query = $this->db->query("SELECT a.`user_id` as brand_id, a.`company_name`, a.`cpc_percentage`, a.`created_datetime`, a.`onboarded_date`, a.`cpa_percentage` FROM `brand_info` as a WHERE 1 and a.`is_paid`=1 and a.`onboarded_date` like'".$date_from."' order by a.onboarded_date desc");
		}else
		{
			$query = $this->db->query("SELECT a.`user_id` as brand_id, a.`company_name`, a.`cpc_percentage`, a.`created_datetime`, a.`onboarded_date`, a.`cpa_percentage` FROM `brand_info` as a WHERE 1 and  a.`is_paid`=1 and a.`onboarded_date` like '%".date('Y-m-d')."%' order by a.onboarded_date desc");	
		}

		$res = $query->result_array();
		//echo $this->db->last_query();
		$brand_info = "";
		if(!empty($res)){ $i = 0;
			foreach ($res as $val) {
				
				$brand_info[$i]['company_name'] =  $val['company_name'];
				$brand_info[$i]['brand_id'] =  $val['brand_id'];
				$brand_info[$i]['created_datetime'] =  $val['onboarded_date'];
				//$brand_info[$i]['product_count'] =  $this->product_used_count($val['brand_id'],$date_from,$date_to)[0]['count'];
				$brand_info[$i]['product_count'] =  $this->product_used_count_till_today($val['brand_id'])[0]['count'];
				$brand_info[$i]['product_approve'] =  $this->product_approve($val['brand_id'])[0]['count'];
				$brand_info[$i]['product_reject'] =  $this->product_reject($val['brand_id'])[0]['count'];
				$brand_info[$i]['product_pending'] =  $this->product_pending($val['brand_id'])[0]['count'];
				/*$brand_info[$i]['look_count'] = $this->product_look_used_count($val['brand_id'],$date_from,$date_to)[0]['count'];*/
				$brand_info[$i]['look_count'] = $this->product_look_used_count_till_today($val['brand_id'])[0]['count'];
				$brand_info[$i]['cpc_count'] = $this->product_cpc_count_today($val['brand_id'])[0]['count'];
				$brand_info[$i]['cpa_count'] = $this->product_cpa_count_today($val['brand_id'])[0]['count'];
				$brand_info[$i]['product_count_today'] =  $this->product_used_count($val['brand_id'],date('Y-m-d'),'')[0]['count'];
				/*$brand_info[$i]['look_count_today'] = $this->product_look_used_count($val['brand_id'],date('Y-m-d H:i:s'),'')[0]['count'];*/
				$brand_info[$i]['look_count_today'] = $this->product_look_used_count($val['brand_id'],date('Y-m-d'),'')[0]['count'];				
				$brand_info[$i]['cost_product_count_today'] =  $this->cost_product_used_count($val['brand_id'],date('Y-m-d H:i:s'),'')[0]['count']!='' ? $this->my_money_format($this->cost_product_used_count($val['brand_id'],date('Y-m-d H:i:s'),'')[0]['count']) : $this->my_money_format(0);

				//echo "<pre>";print_r($this->product_look_used_count($val['brand_id'],$date_from,$date_to));exit;
				$i++;
			}
		}


		return $brand_info;
	}

	function product_look_used_count($brand_id,$date_from,$date_to)
	{
		if($date_from!='' && $date_to!='' && $date_from!=$date_to)
		{
			$query = $this->db->query("SELECT distinct count(c.id) as count  from look as a,`look_products` as b,product_desc  as c where a.`id` = b.`look_id` and b.`product_id` = c.`id` and c.`brand_id` =  '".$brand_id."' and a.`status` = 1 and a.`modified_datetime` >= '".$date_from." 00:00:00' and a.`modified_datetime` <='".$date_to." 23:59:59'");
			
		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
			$query = $this->db->query("SELECT distinct count(c.id) as count from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and c.`brand_id` =  '".$brand_id."' and a.`status` = 1 and a.`modified_datetime` like '%".$date_from."%'  ");

		}else{
			
			$query = $this->db->query("SELECT distinct count(c.id) as count from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and c.`brand_id` =  '".$brand_id."' and a.`status` = 1 and a.`modified_datetime` like '%".date('Y-m-d')."%'  ");
		}

		$res = $query->result_array();	
		//echo $this->db->last_query();
		return $res;
	}

	function product_look_used_details($brand_id,$date_from,$date_to)
	{
		if($date_from!='' && $date_to!='' && $date_from!=$date_to)
		{
			$query = $this->db->query("SELECT c.`id` as id, c.`name`,c.`price`, a.`id` as look_id, a.`name` as look_name, c.`created_datetime` as datetime ,(select d.email as stylist_name from user_login as d where a.created_by = d.id) as created_by_stylist,(select d.email as stylist_name from user_login as d where a.modified_by = d.id) as Broadcasted_by_stylistfrom look as a,`look_products` as b,product_desc  as c where a.`id` = b.`look_id` and b.`product_id` = c.`id` and c.`brand_id` =  '".$brand_id."' and a.`status` = 1 and a.`modified_datetime` >= '".$date_from." 00:00:00' and a.`modified_datetime` <='".$date_to." 23:59:59'");
			
		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
			$query = $this->db->query("SELECT c.`id` as id, c.`name`,c.`price`, a.`id` as look_id, a.`name` as look_name, c.`created_datetime` as datetime ,(select d.email as stylist_name from user_login as d where a.created_by = d.id) as created_by_stylist,(select d.email as stylist_name from user_login as d where a.modified_by = d.id) as Broadcasted_by_stylist from look as a,`look_products` as b,product_desc  as c where a.`id` = b.`look_id` and b.`product_id` = c.`id` and c.`brand_id` =  '".$brand_id."' and a.`status` = 1 and a.`modified_datetime` like '%".$date_from."%'  ");

		}else{
			
			$query = $this->db->query("SELECT distinct c.`id` as id, c.`name`,c.`price`, a.`id` as look_id, a.`name` as look_name, c.`created_datetime` as datetime ,(select d.email as stylist_name from user_login as d where a.created_by = d.id) as created_by_stylist,(select d.email as stylist_name from user_login as d where a.modified_by = d.id) as Broadcasted_by_stylist from look as a,`look_products` as b,product_desc  as c where a.`id` = b.`look_id` and b.`product_id` = c.`id` and c.`brand_id` =  '".$brand_id."' and a.`status` = 1 and a.`modified_datetime` like '%".date('Y-m-d')."%'  ");
		}
		$result = $query->result_array();	
		//echo $this->db->last_query();
		$result['data'] = $query->result_array();
		$result['fields'] = $query->list_fields(); 
		return $result;
	}


	function product_look_used_count_till_today($brand_id){
		$query = $this->db->query("SELECT distinct count(c.id) as count from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and c.`brand_id` =  '".$brand_id."' and a.`status` = 1 ");
		$res = $query->result_array();		
		return $res;
	}

		function product_look_used_count_till_today_details($brand_id){
		//$query = $this->db->query("SELECT c.`id` as id, c.`name`,c.`price`, a.`id` as look_id, a.`name` as look_name, c.`created_datetime` as datetime  from look as a, `look_products` as b,`product_desc`  as c where a.`id` = b.`look_id` and b.`product_id` = c.`id` and c.`brand_id` = '".$brand_id."' and a.`status` = 1 ");
		$query = $this->db->query("SELECT c.`id` as id, c.`name`,c.`price`, a.`id` as look_id, a.`name` as look_name, c.`created_datetime` as datetime ,(select d.email as stylist_name from user_login as d where a.created_by = d.id) as created_by_stylist,(select d.email as stylist_name from user_login as d where a.modified_by = d.id) as Broadcasted_by_stylist from look as a, `look_products` as b,`product_desc`  as c where a.`id` = b.`look_id` and b.`product_id` = c.`id` and c.`brand_id` = '".$brand_id."' and a.`status` = 1 order by a.created_datetime desc");
		$result = $query->result_array();		
		$result['data'] = $query->result_array();
		$result['fields'] = $query->list_fields(); 
		// echo '<pre>';
		 // print_r($result);exit;
		return $result;	
	}

	function product_used_count($brand_id,$date_from,$date_to){
		if($date_from!='' && $date_to!='' && $date_from!=$date_to)
		{
			$query = $this->db->query("select count(id) as count from product_desc where `brand_id` = '".$brand_id."' and created_datetime >= '".$date_from." 00:00:00' and created_datetime <= '".$date_to." 23:59:59'");
		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
			$query = $this->db->query("select count(id) as count from product_desc where `brand_id` = '".$brand_id."' and created_datetime like '%".$date_from."%'");
		}else{
			$query = $this->db->query("select count(id) as count from product_desc where `brand_id` = '".$brand_id."' and created_datetime like '%".date('Y-m-d')."%' ");
		}
		$res = $query->result_array();
		return $res;
	}

	function product_used_count_details($type,$brand_id,$date_from,$date_to){
		if($date_from!='' && $date_to!='' && $date_from!=$date_to)
		{
			$query = $this->db->query("select id, name,price, created_datetime as datetime from product_desc where `brand_id` = '".$brand_id."' and created_datetime >= '".$date_from." 00:00:00' and created_datetime <= '".$date_to." 23:59:59' order by created_datetime desc");

		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to && $date_from!=0 && $date_to!=0 ))
		{
			$query = $this->db->query("select id, name,price, created_datetime as datetime from product_desc where `brand_id` = '".$brand_id."' and created_datetime like '%".$date_from."%' order by created_datetime desc");
		}else{
			$query = $this->db->query("select id, name,price, created_datetime as datetime from product_desc where `brand_id` = '".$brand_id."' and created_datetime like '%".date('Y-m-d')."%' order by created_datetime desc");
		}
		$result = $query->result_array();
		//echo $this->db->last_query();
		$result['data'] = $query->result_array();
		$result['fields'] = $query->list_fields(); 
		return $result;	
	}

	function cost_product_used_count($brand_id,$date_from,$date_to){
	
		$query = $this->db->query("SELECT distinct count(c.id), sum(c.price) as count from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and c.`brand_id` =  '".$brand_id."' and a.`status` = 1 and a.modified_datetime like '%".date('Y-m-d')."%'");
		
		$res = $query->result_array();
		return $res;
	}


	function product_used_count_till_today($brand_id){
		$query = $this->db->query("select count(id) as count from product_desc where `brand_id` = '".$brand_id."' ");
		$res = $query->result_array();
		return $res;
	}

	function product_used_count_till_today_detail($brand_id,$date_from,$date_to){
		$query = $this->db->query("select id,name, price, created_datetime as datetime from product_desc where `brand_id` = '".$brand_id."' order by created_datetime desc ");
		$result = $query->result_array();		
		$result['data'] = $query->result_array();
		$result['fields'] = $query->list_fields(); 
		return $result;	
	}
	
	function product_approve($brand_id){
		$query = $this->db->query("select count(id) as count from product_desc where `approve_reject`='A' and `is_delete`=0 and `brand_id` = '".$brand_id."' order by created_datetime desc ");
		$res = $query->result_array();
		return $res;
	}
	
	function product_approve_details($brand_id){
		$query = $this->db->query("select id,name,price,modified_datetime as datetime from product_desc where `approve_reject`='A' and `is_delete`=0 and `brand_id` = '".$brand_id."' order by modified_datetime desc ");
		$result = $query->result_array();
		$result['data'] = $query->result_array();
		$result['fields'] = $query->list_fields(); 
		return $result;	
	}
	
	function product_reject($brand_id){
		$query = $this->db->query("select count(id) as count from product_desc where `approve_reject`='R' and `is_delete`=0 and `brand_id` = '".$brand_id."' order by created_datetime desc ");
		$res = $query->result_array();
		return $res;
	}
	
	function product_reject_details($brand_id){
		$query = $this->db->query("select id,name,price,reason,modified_datetime as datetime from product_desc where `approve_reject`='R' and `is_delete`=0 and `brand_id` = '".$brand_id."' order by modified_datetime desc  ");
		$result = $query->result_array();
		$result['data'] = $query->result_array();
		$result['fields'] = $query->list_fields(); 
		return $result;
	}
	
	function product_pending($brand_id){
		$query = $this->db->query("select count(id) as count from product_desc where `approve_reject`='P' and `is_delete`=0 and `brand_id` = '".$brand_id."' order by created_datetime desc ");
		$res = $query->result_array();
		return $res;
	}
	
	function product_pending_details($brand_id){
		$query = $this->db->query("select id,name,price,modified_datetime as datetime from product_desc where `approve_reject`='P' and `is_delete`=0 and `brand_id` = '".$brand_id."' order by modified_datetime desc ");
		$result = $query->result_array();
		$result['data'] = $query->result_array();
		$result['fields'] = $query->list_fields(); 
		return $result;
	}
	
	function brand_products($brand_id,$date_from,$date_to){		
		if($date_from!='' && $date_to!='' && $date_from!=$date_to)
		{
			$query = $this->db->query("select id,name, created_datetime from product_desc where `brand_id` = '".$brand_id."' and created_datetime >= '".$date_from." 00:00:00' and created_datetime <= '".$date_to." 23:59:59' order by created_datetime desc ");
		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
			$query = $this->db->query("select  id,name, created_datetime from product_desc where `brand_id` = '".$brand_id."' and created_datetime like '%".$date_from."%' order by created_datetime desc ");
		}else{
			$query = $this->db->query("select id,name, created_datetime from product_desc where `brand_id` = '".$brand_id."' and created_datetime like '%".date('Y-m-d')."%' order by created_datetime desc ");
		}
		$res = $query->result_array();
		return $res;
	}

	function brand_look_products($brand_id,$date_from,$date_to)
	{
		if($date_from!='' && $date_to!='' && $date_from!=$date_to)
		{
			$query = $this->db->query("SELECT  c.`id`, c.`name`,a.`modified_datetime` as `created_datetime` from look as a,`look_products` as b,product_desc  as c where a.`id` = b.`look_id` and b.`product_id` = c.`id` and c.`brand_id` =  '".$brand_id."' and a.`status` = 1 and a.`modified_datetime` >= '".$date_from." 00:00:00' and a.`modified_datetime` <= '".$date_to." 23:59:59'");
			
		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
			$query = $this->db->query("SELECT  c.`id`, c.`name`,a.`modified_datetime` as `created_datetime` from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and c.`brand_id` =  '".$brand_id."' and a.`status` = 1 and a.`modified_datetime` like '%".$date_from."%'  ");

		}else{
			
			$query = $this->db->query("SELECT  c.`id`, c.`name`,a.`modified_datetime` as `created_datetime` from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and c.`brand_id` =  '".$brand_id."' and a.`status` = 1 and a.`modified_datetime` like '%".date('Y-m-d')."%'  ");
		}

		$res = $query->result_array();		
		return $res;
	}

	function top_5_product_cpc($date_from,$date_to){
        if($date_from!='' && $date_to!='' && $date_from!=$date_to){
        $query = $this->db->query("SELECT a.product_id, count(a.`id`) as count,b.name,b.price,b.brand_id,c.company_name,c.cpc_percentage,c.cpa_percentage FROM `users_product_click_track` as a,product_desc as b,brand_info as c  where a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <='".$date_to." 23:59:59' and a.product_id = b.`id` and b.`brand_id` = c.user_id group by a.`product_id` ORDER BY `count` DESC limit 0,5");

        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!='')){
        $query = $this->db->query("SELECT a.product_id, count(a.`id`) as count,b.name,b.price,b.brand_id,c.company_name,c.cpc_percentage,c.cpa_percentage FROM `users_product_click_track` as a,product_desc as b,brand_info as c  where a.created_datetime like '%".$date_from."%' and a.product_id = b.`id` and b.`brand_id` = c.user_id group by a.`product_id` ORDER BY `count` DESC limit 0,5");

        }else{
        $query = $this->db->query("SELECT a.product_id, count(a.`id`) as count,b.name,b.price,b.brand_id,c.company_name,c.cpc_percentage,c.cpa_percentage FROM `users_product_click_track` as a,product_desc as b,brand_info as c  where a.product_id = b.`id` and b.`brand_id` = c.user_id and a.created_datetime like '%".date('Y-m-d')."%' group by a.`product_id` ORDER BY `count` DESC limit 0,5");
        }
        $res = $query->result_array();
        return $res;
    }

    function top_5_brands_cpc($date_from,$date_to){
    	  if($date_from!='' && $date_to!='' && $date_from!=$date_to){
        $query = $this->db->query("SELECT a.product_id, count(a.`id`) as count,b.name,b.price,b.brand_id,c.company_name,c.cpc_percentage,c.cpa_percentage FROM `users_product_click_track` as a,product_desc as b,brand_info as c  where a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <= '".$date_to." 23:59:59' and a.product_id = b.`id` and b.`brand_id` = c.user_id group by b.brand_id ORDER BY `count` DESC limit 0,5");

        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!='')){
        $query = $this->db->query("SELECT a.product_id, count(a.`id`) as count,b.name,b.price,b.brand_id,c.company_name,c.cpc_percentage,c.cpa_percentage FROM `users_product_click_track` as a,product_desc as b,brand_info as c  where a.created_datetime like '%".$date_from."%' and a.product_id = b.`id` and b.`brand_id` = c.user_id group by b.brand_id ORDER BY `count` DESC limit 0,5");

        }else{
        $query = $this->db->query("SELECT a.product_id, count(a.`id`) as count,b.name,b.price,b.brand_id,c.company_name,c.cpc_percentage,c.cpa_percentage FROM `users_product_click_track` as a,product_desc as b,brand_info as c  where a.product_id = b.`id` and b.`brand_id` = c.user_id and a.created_datetime like '%".date('Y-m-d')."%' group by b.brand_id ORDER BY `count` DESC limit 0,5");
        }
        $res = $query->result_array();
        return $res;
    }

    function top_5_look_cpc($date_from,$date_to){
        if($date_from!='' && $date_to!='' && $date_from!=$date_to){
        $query = $this->db->query("select count(a.id) as count,b.name,a.look_id, b.modified_datetime  from  `users_look_click_track` as a,look as b where b.id = a.`look_id` and a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <='".$date_to." 23:59:59' group by a.`look_id` ORDER BY `count` DESC limit 0,5");
        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!='')){
        $query = $this->db->query("select count(a.id) as count,b.name,a.look_id,b.modified_datetime from `users_look_click_track` as a,look as b where b.id = a.`look_id` and a.created_datetime like '%".$date_from."%' group by a.`look_id` ORDER BY `count` DESC limit 0,5");
        }else{
        $query = $this->db->query("select count(a.id) as count,b.name,a.look_id,b.modified_datetime from `users_look_click_track` as a,look as b where b.id = a.`look_id` and a.created_datetime like '%".date('Y-m-d')."%' group by a.`look_id` ORDER BY `count` DESC limit 0,5");
        }
        $res = $query->result_array();
        return $res;
    }

    function top_5_product_cpa($date_from,$date_to){
        if($date_from!='' && $date_to!='' ){
        $query = $this->db->query("SELECT a.product_id, count(a.`id`) as count,b.name,b.price,b.brand_id,c.company_name,c.cpc_percentage,c.cpa_percentage FROM `product_cpa_track` as a,product_desc as b,brand_info as c  where a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <= '".$date_to." 23:59:59' and a.product_id = b.`id` and b.`brand_id` = c.user_id group by a.`product_id` ORDER BY `count` DESC limit 0,5");
        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
        {
        	$query = $this->db->query("SELECT a.product_id, count(a.`id`) as count,b.name,b.price,b.brand_id,c.company_name,c.cpc_percentage,c.cpa_percentage FROM `product_cpa_track` as a,product_desc as b,brand_info as c  where a.created_datetime like '%".$date_from."%' and a.product_id = b.`id` and b.`brand_id` = c.user_id group by a.`product_id` ORDER BY `count` DESC limit 0,5");
        }else{
        	$query = $this->db->query("SELECT a.product_id, count(a.`id`) as count,b.name,b.price,b.brand_id,c.company_name,c.cpc_percentage,c.cpa_percentage FROM `product_cpa_track` as a,product_desc as b,brand_info as c  where a.created_datetime like '%".date('Y-m-d')."%' and a.product_id = b.`id` and b.`brand_id` = c.user_id group by a.`product_id` ORDER BY `count` DESC limit 0,5");
        }
        
        $res = $query->result_array();
        return $res;
    }


	function top_5_buyers_cpa($date_from,$date_to){
		
		if($date_from!='' && $date_to!='' ){
			
		$query = $this->db->query("select a.sc_user_id as user_id,a.product_id,b.user_name as name,b.email as email,sum(total_price) as total_price,count(a.id) as user_count FROM product_cpa_track as a, user_login as b where a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <= '".$date_to." 23:59:59' AND a.sc_user_id = b.id AND a.sc_user_id != 0 group by sc_user_id order by user_count DESC limit 0,5 ");
	   
        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
        {    
        	$query = $this->db->query("select a.sc_user_id as user_id,a.product_id,b.user_name as name,b.email as email,sum(total_price) as total_price,count(a.id) as user_count FROM product_cpa_track as a, user_login as b where a.created_datetime like '%".$date_from."%' AND a.sc_user_id = b.id AND a.sc_user_id != 0 group by sc_user_id order by user_count DESC limit 0,5 ");
			
        }else{
        	$query = $this->db->query("select a.sc_user_id as user_id,a.product_id,b.user_name as name,b.email as email,sum(total_price) as total_price,count(a.id) as user_count FROM product_cpa_track as a, user_login as b where a.created_datetime like '%".date('Y-m-d')."%'  AND a.sc_user_id = b.id AND a.sc_user_id != 0 group by sc_user_id order by user_count DESC limit 0,5 ");
        }
	 
        $res = $query->result_array();		
        return $res;
    }

	
	function all_buyers_cpa(){
		
		$query = $this->db->query("select a.sc_user_id as user_id,a.product_id,b.user_name as name,b.email as email,sum(c.price) as total_price,count(a.id) as user_count FROM product_cpa_track as a, user_login as b, product_desc as c  where a.product_id = c.id AND a.sc_user_id = b.id AND a.sc_user_id != 0 group by sc_user_id order by user_count DESC");
        $res = $query->result_array();		
        return $res;
    }
	
	function product_cpa_details($user_id){
		
		$query = $this->db->query("select a.sc_user_id as user_id,a.product_id,b.user_name as name,b.email as email,c.price as total_price, d.company_name FROM product_cpa_track as a, user_login as b, product_desc as c, brand_info as d where a.product_id = c.id AND  d.user_id = c.brand_id AND a.sc_user_id = $user_id AND b.id = $user_id");
        $res = $query->result_array();
        return $res;
    }
	

    function most_returning_users_all($search_by,$table_search,$page,$per_page){
		//echo $search_by.'_'.$table_search.'_'.$page.'_'.$per_page;
		//exit;
		$res = array();
		if($search_by == '1'){ $search_cond = " AND a.user_name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '2'){ $search_cond = " AND a.email like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '3'){ $search_cond = " AND a.created_datetime like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '4'){ $search_cond = " AND a.id =".$table_search; }
		else{ $search_cond =''; }
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}
	
		/*	$query = $this->db->query("select a.`id` as user_id, a.`user_name`, a.`email` as email, s.`last_visit_time`, b.`visit_count` as count from `user_login` as a,`user_history_info` as b,`user_visit_history`as s,`user_role_mapping` as c where
			 a.`id` = b.`user_id` and c.`user_id` = a.`id` and a.`id`=s.`id` and c.`role_id` = 2 and a.test_user!=1 ".$search_cond." order by visit_count desc ".$limit_cond);
		
		$res = $query->result_array();
		*/
		return $res;	
	}

	function most_least_runing_all($search_by,$table_search,$page,$per_page){ 
		$res = array();
		if($search_by == '1'){ $search_cond = " AND a.user_name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '2'){ $search_cond = " AND a.email like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '3'){ $search_cond = " AND a.created_datetime like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '4'){ $search_cond = " AND a.id =".$table_search; }
		else{ $search_cond =''; }
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}
		
		/*$query = $this->db->query("select a.`id` as user_id, a.`user_name`, a.`email` as email, a.`modified_datetime` as last_visit_time from `user_login` as a where a.`created_datetime` = a.`modified_datetime` and a.test_user!=1  ".$search_cond." order by a.created_datetime desc ".$limit_cond);
		$res = $query->result_array();*/
		return $res;
	}
	
	function most_sharing_all($search_by,$table_search,$page,$per_page){
		$res = array();	
		if($search_by == '1'){ $search_cond = " AND b.user_name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '2'){ $search_cond = " AND b.email like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '3'){ $search_cond = " AND b.created_datetime like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '4'){ $search_cond = " AND b.id =".$table_search; }
		else{ $search_cond =''; }
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}
		/*
		$query = $this->db->query("SELECT count(a.`id`) as count,a.`user_id`,b.`user_name`,b.`email` FROM `user_share` as a,`user_login` as b where a.`user_id`=b.`id` and b.`test_user`!=1 ".$search_cond." group by a.user_id order by count desc ".$limit_cond);		
		$res = $query->result_array();*/
		return $res;
	}

	function most_fav_all($search_by,$table_search,$page,$per_page){
		$res = array();
		if($search_by == '1'){ $search_cond = " AND b.user_name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '2'){ $search_cond = " AND b.email like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '3'){ $search_cond = " AND b.created_datetime like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '4'){ $search_cond = " AND b.id =".$table_search; }
		else{ $search_cond =''; }
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}
		
		/*$query = $this->db->query("SELECT count(a.id) as count,a.user_id,b.user_name,b.email FROM `look_fav` as a,`user_login` as b where a.`user_id`=b.`id` and b.`test_user`!=1".$search_cond." group by a.user_id order by count desc  ".$limit_cond);
		
		$res = $query->result_array();*/
		return $res;
	}

	function most_wardrobe_all($search_by,$table_search,$page,$per_page){
		
		if($search_by == '1'){ $search_cond = " AND b.user_name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '2'){ $search_cond = " AND b.email like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '3'){ $search_cond = " AND b.created_datetime like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '4'){ $search_cond = " AND b.id =".$table_search; }
		else{ $search_cond =''; }
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}
			
		$query = $this->db->query("SELECT count(a.id) as count,a.user_id,b.user_name,b.email FROM `users_wardrobe` as a,`user_login` as b where a.`user_id`=b.`id` and b.`test_user`!=1 ".$search_cond." group by a.user_id order by count desc ".$limit_cond);
		$res = $query->result_array();
		return $res;
	}

	function top_5_product_cpc_all($date_from,$date_to,$page,$per_page){		
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0)
		{
               $page = $page*$per_page-$per_page;
        }else{ $page = 0;}
			$limit_cond = "LIMIT ".$page.",".$per_page;
		}else{
			$limit_cond = '';	
		}

        if($date_from!='' && $date_to!='' && $date_from!=$date_to && $date_from!=0 && $date_to!=0 ){
       		 $query = $this->db->query("SELECT a.`product_id`, count(a.`id`) as count,b.`name`,b.`price`,b.`brand_id`,c.`company_name`,c.`cpc_percentage`,c.`cpa_percentage` FROM `users_product_click_track` as a,product_desc as b,brand_info as c  where a.`created_datetime` >= '".$date_from." 00:00:00' and a.`created_datetime` <='".$date_to." 23:59:59' and a.product_id = b.`id` and b.`brand_id` = c.user_id group by a.`product_id` ORDER BY `count` DESC ".$limit_cond);
        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to && $date_from!=0 ) || ($date_from!=0 ))
        {
        	$query = $this->db->query("SELECT a.`product_id`, count(a.`id`) as count,b.`name`,b.`price`,b.`brand_id`,c.`company_name`,c.`cpc_percentage`,c.`cpa_percentage` FROM `users_product_click_track` as a,product_desc as b,brand_info as c  where a.`created_datetime` like '%".$date_from."%' and a.product_id = b.`id` and b.`brand_id` = c.user_id group by a.`product_id` ORDER BY `count` DESC ".$limit_cond);
        }else
        {
        $query = $this->db->query("SELECT a.`product_id`, count(a.`id`) as count,b.`name`,b.`price`,b.`brand_id`,c.`company_name`,c.`cpc_percentage`,c.`cpa_percentage` FROM `users_product_click_track` as a,product_desc as b,brand_info as c  where a.`created_datetime` like '%".date('Y-m-d')."%' and a.`product_id` = b.`id` and b.`brand_id` = c.`user_id` group by a.`product_id` ORDER BY `count` DESC  ".$limit_cond);
        }
        $res = $query->result_array();
        //echo $this->db->last_query();
        return $res;
    }

    function top_5_look_cpc_all($date_from,$date_to,$page,$per_page){

    	if($page!='' || $per_page!=''){ if($page != 1 && $page !=0)
		{
               $page = $page*$per_page-$per_page;
        }else{ $page = 0;}
			$limit_cond = "LIMIT ".$page.",".$per_page;
		}else{
			$limit_cond = '';	
		}

        /*if($date_from!='' && $date_to!='' && $date_from!=$date_to){*/
        if($date_from!='' && $date_to!='' && $date_from!=$date_to && $date_from!=0 && $date_to!=0 ){
        $query = $this->db->query("select count(a.id) as count,b.name,a.look_id,b.modified_datetime from `users_look_click_track` as a,look as b where b.id = a.`look_id` and a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <= '".$date_to." 23:59:59' group by a.`look_id` ORDER BY `count` DESC ".$limit_cond);
        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to && $date_from!=0 ) || ($date_from!=0 ))
        {
        	$query = $this->db->query("select count(a.id) as count,b.name,a.look_id,b.modified_datetime from `users_look_click_track` as a,look as b where b.id = a.`look_id` and a.created_datetime like '%".$date_from."%' group by a.`look_id` ORDER BY `count` DESC ".$limit_cond);
        }else{
        $query = $this->db->query("select count(a.id) as count,b.name,a.look_id,b.modified_datetime from `users_look_click_track` as a,look as b where b.id = a.`look_id` and a.created_datetime like '%".date('Y-m-d')."%' group by a.`look_id` ORDER BY `count` DESC ".$limit_cond);
        }
        //echo $this->db->last_query();
        $res = $query->result_array();
        return $res;
    }

    function top_5_product_cpa_all($date_from,$date_to,$page,$per_page)
    {
    	if($date_from!='' && $date_to!='' && $date_from!=$date_to && $date_from!=0 && $date_to!=0 ){
        $query = $this->db->query("SELECT a.`product_id`, b.`name`, count(a.`id`) as count,b.`price`,b.`brand_id`,c.`company_name`,c.`cpc_percentage`,c.`cpa_percentage` FROM `product_cpa_track` as a,product_desc as b,brand_info as c  where a.`created_datetime` >= '".$date_from." 00:00:00' and a.`created_datetime` <= '".$date_to." 23:59:59' and a.product_id = b.`id` and b.`brand_id` = c.user_id group by a.`product_id` ORDER BY `count` DESC ");
        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to && $date_from!=0 ) || ($date_from!=0 ))
        {
        	$query = $this->db->query("SELECT a.`product_id`, b.`name`, count(a.`id`) as count,b.`price`,b.`brand_id`,c.`company_name`,c.`cpc_percentage`,c.`cpa_percentage` FROM `product_cpa_track` as a,product_desc as b,brand_info as c  where a.`created_datetime` like '%".$date_from."%' and a.product_id = b.`id` and b.`brand_id` = c.user_id group by a.`product_id` ORDER BY `count` DESC ");
        }else{
        	$query = $this->db->query("SELECT a.`product_id`, b.`name`, count(a.`id`) as count,b.`price`,b.`brand_id`,c.`company_name`,c.`cpc_percentage`,c.`cpa_percentage` FROM `product_cpa_track` as a,product_desc as b,brand_info as c  where a.`created_datetime` like '%".date('Y-m-d')."%' and a.`product_id` = b.`id` and b.`brand_id` = c.`user_id` group by a.`product_id` ORDER BY `count` DESC ");
        }
        $res = $query->result_array();
        return $res;
    	
    }

    function top_5_brand_click_all($date_from,$date_to)
    {
    	 if($date_from!='' && $date_to!='' && $date_from!=$date_to && $date_from!=0 && $date_to!=0 ){
        $query = $this->db->query("SELECT a.product_id, count(a.`id`) as count,b.name,b.price,b.brand_id,c.company_name,c.cpc_percentage,c.cpa_percentage FROM `users_product_click_track` as a,product_desc as b,brand_info as c  where a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <= '".$date_to." 23:59:59' and a.product_id = b.`id` and b.`brand_id` = c.user_id group by b.brand_id ORDER BY `count` DESC");

        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to && $date_from!=0 ) || ($date_from!=0 )){
        $query = $this->db->query("SELECT a.product_id, count(a.`id`) as count,b.name,b.price,b.brand_id,c.company_name,c.cpc_percentage,c.cpa_percentage FROM `users_product_click_track` as a,product_desc as b,brand_info as c  where a.created_datetime like '%".$date_from."%' and a.product_id = b.`id` and b.`brand_id` = c.user_id group by b.brand_id ORDER BY `count` DESC");

        }else{
        $query = $this->db->query("SELECT a.product_id, count(a.`id`) as count,b.name,b.price,b.brand_id,c.company_name,c.cpc_percentage,c.cpa_percentage FROM `users_product_click_track` as a,product_desc as b,brand_info as c  where a.product_id = b.`id` and b.`brand_id` = c.user_id and a.created_datetime like '%".date('Y-m-d')."%' group by b.brand_id ORDER BY `count` DESC");
        }
        $res = $query->result_array();
        //echo $this->db->last_query();
        return $res;
    }

    function top_5_product_cpc_details($type,$date_from,$date_to) 
    {	
    	if($type == 'Authenticated_CPC')
    	{
    		if($date_from!='' && $date_to!=''  && $date_from!=$date_to &&  $date_from!=0 &&  $date_to!=0 )
    		{
	        /*$query = $this->db->query("select  b.click_count as count, a.`id` as product_id,a.`name`,c.`company_name`,a.`price`,c.`cpa_percentage`, b.created_datetime as datetime from product_desc as a,`users_product_click_track` as b,brand_info as c where a.id = b. product_id   and b.`user_id`!=0 and a.`brand_id` = c.user_id and b.`created_datetime` between '".$date_from."' and '".$date_to."'  ORDER BY a.`created_datetime` desc ");*/
	       $query = $this->db->query("select b.`created_datetime` as datetime  ,d.id,case b.`click_count`  when '0' then '1' else `click_count` end as count, a.`id` as product_id,a.`name`,c.`company_name`,a.`price`,c.`cpc_percentage`,d.`email` from product_desc as a,`users_product_click_track` as b,brand_info as c, user_login as d where a.id = b. product_id and d.`id`=b.`user_id`  and b.`user_id`!=0 and a.`brand_id` = c.user_id and d.test_user=0 and b.`created_datetime` >= '".$date_from." 00:00:00' and b.`created_datetime` <='".$date_to." 23:59:59'  ORDER BY b.`created_datetime` desc ");
	      	        }
	        else  if(($date_from!='' && $date_to!='' && $date_from==$date_to  && $date_from!=0) || ( $date_from!='' && $date_to!='' && $date_from!='0' ))
	        {	        	
	        	/*$query = $this->db->query("select b.click_count as count, a.`id` as product_id ,a.`name`,c.`company_name`,a.`price`,c.`cpa_percentage`, b.created_datetime as datetime from product_desc as a,`users_product_click_track` as b,brand_info as c where a.id = b. product_id   and b.`user_id`!=0 and a.`brand_id` = c.user_id and b.`created_datetime` like '%".$date_from."%'  ORDER BY a.`created_datetime` desc ");*/
	        	$query = $this->db->query("select b.`created_datetime` as datetime,d.id,case b.`click_count`  when '0' then '1' else `click_count` end as count, a.`id` as product_id,a.`name`,c.`company_name`,a.`price`,c.`cpc_percentage`,d.`email` from product_desc as a,`users_product_click_track` as b,brand_info as c, user_login as d where a.id = b. product_id and d.`id`=b.`user_id`  and b.`user_id`!=0 and d.test_user=0  and a.`brand_id` = c.user_id and b.`created_datetime` like '%".$date_from."%'  ORDER BY b.`created_datetime` desc ");
	        }else
	        {	       
	       		/*$query = $this->db->query("select b.click_count as count, a.`id` as product_id,a.`name`,c.`company_name`,a.`price`,c.`cpa_percentage`, b.created_datetime as datetime from product_desc as a,`users_product_click_track` as b,brand_info as c where a.id = b. product_id   and b.`user_id`!=0 and a.`brand_id` = c.user_id and b.`created_datetime`like '%".date('Y-m-d')."%' and b.`user_id`!=0 ORDER BY a.`created_datetime` desc");*/
	       			       $query = $this->db->query("select b.`created_datetime` as datetime  ,d.id, case b.`click_count`  when '0' then '1' else `click_count` end as count, a.`id` as product_id,a.`name`,c.`company_name`,a.`price`,c.`cpc_percentage`,d.`email` from product_desc as a,`users_product_click_track` as b,brand_info as c, user_login as d where a.id = b. product_id and d.`id`=b.`user_id`  and b.`user_id`!=0 and a.`brand_id` = c.user_id and d.test_user=0  and b.`created_datetime`like '%".date('Y-m-d')."%' and b.`user_id`!=0 ORDER BY b.`created_datetime` desc");

	        }

    	}else if($type == 'Authenticated_CPA')
    	{    		

    		if($date_from!='' && $date_to!=''  && $date_from!=$date_to &&  $date_from!=0 &&  $date_to!=0)
    		{	        
	        	/*$query = $this->db->query("select a.product_id,b.name,c.company_name,b.price,count(a.`id`) as count,c.`cpa_percentage` from product_cpa_track as a,product_desc as b,brand_info as c where a.`created_datetime` between '".$date_from."' and '".$date_to."'  and a.`sc_user_id` != 0 and a.product_id = b.id and b.brand_id = c.user_id group by a.product_id");*/
	        		        $query = $this->db->query("select a.`created_datetime` AS datetime, a.product_id,d.id,b.name,c.company_name,b.price,d.`email`,c.`cpa_percentage` as cpc_percentage from product_cpa_track as a,product_desc as b,brand_info as c, user_login as d  where a.`created_datetime` >= '".$date_from." 00:00:00' and a.`created_datetime` <='".$date_to." 23:59:59' and  a.`sc_user_id` != 0 and a.product_id = b.id and d.`id`=a.`sc_user_id` and d.test_user=0  and b.brand_id = c.user_id ORDER BY a.`created_datetime` DESC");

	        }
	        else if(($date_from!='' && $date_to!='' && $date_from==$date_to && $date_from!=0 ) || ( $date_from!='' && $date_to!='' && $date_from!=0 ))
	        {	        	
	        	/* $query = $this->db->query("select a.product_id,b.name,c.company_name,b.price,count(a.`id`) as count,c.`cpa_percentage` from product_cpa_track as a,product_desc as b,brand_info as c where a.`created_datetime` like '%".$date_from."%'  and a.`sc_user_id` != 0 and a.product_id = b.`id` and  b.`brand_id` = c.user_id group by a.`product_id` ORDER BY `count` DESC ");*/
	        		        	 $query = $this->db->query("select a.`created_datetime` AS datetime,d.id, a.product_id,b.name,c.company_name,b.price,d.`email`,c.`cpa_percentage` as cpc_percentage from product_cpa_track as a,product_desc as b,brand_info as c, user_login as d  where a.`created_datetime` like '%".$date_from."%'and a.product_id = b.id and d.`id`=a.`sc_user_id` and b.brand_id = c.user_id and d.test_user=0  ORDER BY a.`created_datetime` DESC");
	        }else{
	        	
	        	 /*$query = $this->db->query("select a.product_id,b.name,c.company_name,b.price,count(a.`id`) as count,c.`cpa_percentage` from product_cpa_track as a,product_desc as b,brand_info as c where a.`created_datetime` like '%".date('Y-m-d')."%'  and a.`sc_user_id` != 0 and a.`product_id` = b.`id`  and b.`brand_id` = c.`user_id` group by a.`product_id` ORDER BY `count` DESC ");*/
	        	 	        	 $query = $this->db->query("select a.`created_datetime` AS datetime,d.id, a.product_id,b.name,c.company_name,b.price,d.`email`,c.`cpa_percentage` as cpc_percentage from product_cpa_track as a,product_desc as b,brand_info as c, user_login as d  where a.`created_datetime`  like '%".date('Y-m-d')."%'and a.product_id = b.id and d.`id`=a.`sc_user_id` and b.brand_id = c.user_id and d.test_user=0 ORDER BY a.`created_datetime` DESC");	        	

	        }   

    		
		}	
    	else if($type == 'Unauthenticated_CPC')
    	{
    		if($date_from!='' && $date_to!=''  && $date_from!=$date_to &&  $date_from!=0 &&  $date_to!=0 ){
	        $query = $this->db->query("select case b.`click_count`  when '0' then '1' else `click_count` end as count,  a.`id` as product_id,a.`name`,c.`company_name`,a.`price`,c.`cpc_percentage`, b.created_datetime as datetime from product_desc as a,`users_product_click_track` as b,brand_info as c where a.id = b. product_id   and b.`user_id`=0 and a.`brand_id` = c.user_id and b.`created_datetime` >= '".$date_from." 00:00:00' and b.`created_datetime` <= '".$date_to." 23:59:59'  ORDER BY b.`created_datetime` desc ");
	        }else  if(($date_from!='' && $date_to!='' && $date_from==$date_to  && $date_from!=0) || ( $date_from!='' && $date_to!='' && $date_from!='0' ))
	        {
	        	$query = $this->db->query("select case b.`click_count`  when '0' then '1' else `click_count` end as count,a.`id` as  product_id,a.`name`,c.`company_name`,a.`price`,c.`cpc_percentage`, b.created_datetime as datetime from product_desc as a,`users_product_click_track` as b,brand_info as c where a.id = b. product_id  and b.`user_id`=0 and a.`brand_id` = c.user_id and  b.`created_datetime` like '%".$date_from."%'  ORDER BY b.`created_datetime` desc ");
	        }else
	        {
	        $query = $this->db->query("select case b.`click_count`  when '0' then '1' else `click_count` end as count, a.`id`as product_id,a.`name`,c.`company_name`,a.`price`,c.`cpc_percentage`, b.created_datetime as datetime from product_desc as a,`users_product_click_track` as b ,brand_info as c where a.id = b. product_id and a.`brand_id` = c.user_id and b.`user_id`=0 and b.`created_datetime` like '%".date('Y-m-d')."%' ORDER BY b.`created_datetime` desc");
	        }     

    	}else if($type == 'Unauthenticated_CPA')
    	{

    		if($date_from!='' && $date_to!=''  && $date_from!=$date_to &&  $date_from!=0 &&  $date_to!=0)
    		{
	        $query = $this->db->query("select a.product_id,b.name,c.company_name,b.price,count(a.`id`) as count,c.`cpa_percentage` as cpc_percentage from product_cpa_track as a,product_desc as b,brand_info as c where a.`created_datetime` >= '".$date_from." 00:00:00' and  a.`created_datetime` <='".$date_to." 23:59:59' and a.product_id = b.id   and b.brand_id = c.user_id group by a.product_id ORDER BY `count` DESC");
	        }
	        else if(($date_from!='' && $date_to!='' && $date_from==$date_to && $date_from!=0 ) || ($date_from!=0 || $date_to!=0))
	        {
	        	$query = $this->db->query("select a.product_id,b.name,c.company_name,b.price,count(a.`id`) as count,c.`cpa_percentage` as cpc_percentage from product_cpa_track as a,product_desc as b,brand_info as c where a.`created_datetime` like '%".$date_from."%' and a.product_id = b.`id`  and b.`brand_id` = c.user_id group by a.`product_id` ORDER BY `count` DESC ");        	
	        }
	        else
	        {
	        	$query = $this->db->query("select a.product_id,b.name,c.company_name,b.price,count(a.`id`) as count,c.`cpa_percentage` as cpc_percentage from product_cpa_track as a,product_desc as b,brand_info as c where a.`created_datetime` like '%".date('Y-m-d')."%' and a.`product_id` = b.`id`  and b.`brand_id` = c.`user_id` group by a.`product_id` ORDER BY `count` DESC ");	        	
	        }
    	}   
			
		$res = $query->result_array();
		return $res;
	
    }

    function brand_cpc_count_count($brand_id,$date_from,$date_to){
		if($date_from!='' && $date_to!=''){
        $query = $this->db->query("SELECT sum(a.`click_count`) as count,c.`cpc_percentage` as cpc_percentage FROM `users_product_click_track` as a,product_desc as b,brand_info as c  where a.created_datetime >= '".$date_from." 00:00:00' and  a.created_datetime <= '".$date_to." 23:59:59' and b.`brand_id`='".$brand_id."' and a.product_id = b.`id` and b.`brand_id` = c.user_id ORDER BY `count`");
        }else{
        $query = $this->db->query("SELECT sum(a.`click_count`) as count,c.cpc_percentage as cpc_percentage FROM `users_product_click_track` as a,product_desc as b,brand_info as c  where a.product_id = b.`id` and b.`brand_id` = c.user_id and b.`brand_id`='".$brand_id."' ORDER BY `count`");
        }
        $res = $query->result_array(); 
        return $res;
	}

	function brand_cpa_count_count($brand_id,$date_from,$date_to){
		if($date_from!='' && $date_to!=''){
        $query = $this->db->query("SELECT sum(a.`click_count`) as count FROM `users_product_click_track` as a,product_desc as b,brand_info as c  where a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <='".$date_to." 23:59:59' and b.`brand_id`='".$brand_id."' and a.product_id = b.`id` and b.`brand_id` = c.user_id ORDER BY `count`");
        }else{
        $query = $this->db->query("SELECT sum(a.`click_count`) as count FROM `users_product_click_track` as a,product_desc as b,brand_info as c  where a.product_id = b.`id` and b.`brand_id` = c.user_id and b.`brand_id`='".$brand_id."' ORDER BY `count`");
        }
        $res = $query->result_array();
        return $res;
	}

	 function onboardedbrand($date_from,$date_to)
    {
    	if($date_from!='' && $date_to!='' && $date_from!=$date_to && $date_from!=0 && $date_to!=0){
        	$query = $this->db->query("SELECT a.`user_id` as brand_id, a.`company_name`, a.`cpc_percentage`, a.`created_datetime`, a.`onboarded_date`, a.`cpa_percentage` FROM `brand_info` as a WHERE 1 and a.`is_paid`=1 and a.`onboarded_date` >= '".$date_from." 00:00:00' and a.`onboarded_date` <='".$date_to." 23:59:59' order by a.onboarded_date desc ");

        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
        {
        	$query = $this->db->query("SELECT a.`user_id` as brand_id, a.`company_name`, a.`cpc_percentage`, a.`created_datetime`, a.`onboarded_date`, a.`cpa_percentage` FROM `brand_info` as a WHERE 1 and a.`is_paid`=1 and a.`onboarded_date` like'".$date_from."' order by a.onboarded_date desc");
        }else
        {
        	$query = $this->db->query("SELECT a.`user_id` as brand_id, a.`company_name`, a.`cpc_percentage`, a.`created_datetime`, a.`onboarded_date`, a.`cpa_percentage` FROM `brand_info` as a WHERE 1 and  a.`is_paid`=1 and a.`onboarded_date` like '%".date('Y-m-d')."%' order by a.onboarded_date desc ");
        }
        $res = $query->result_array();
        return $res;
    }

    function onboardedproducts($date_from,$date_to)
    {
    	if($date_from!='' && $date_to!='' && $date_from!=$date_to && $date_from!=0 && $date_to!=0)
    	{

        $query = $this->db->query("SELECT a.`id` as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`  FROM `product_desc` as a,`brand_info` as b where a.`brand_id`= b.`user_id` and b.`is_paid`=1 and  a.`created_datetime` >= '".$date_from." 00:00:00' and a.`created_datetime` <= '".$date_to." 23:59:59'  ORDER BY a.`created_datetime` desc");

        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
        {
        	$query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`  FROM `product_desc` as a,`brand_info` as b where a.`brand_id`= b.`user_id` and b.`is_paid`=1 and  a.`created_datetime` like '%".$date_from."%'  ORDER BY a.`created_datetime` desc ");
        }
        else{
        $query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`  FROM `product_desc` as a,`brand_info` as b where a.`brand_id`= b.`user_id` and b.`is_paid`=1 and a.`created_datetime` like  '%".date('Y-m-d')."%' ORDER BY a.`created_datetime` desc");
        }

        $res = $query->result_array();
        return $res;
    }

    function onboardedproductsUsed($date_from,$date_to)
    { 
    	$res = array();
       /* if($date_from!=0 && $date_to!=0 && $date_from!=$date_to){
        	 $query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`, c.`name` as lookname FROM `product_desc` as a,`brand_info` as b, look as c, look_products as d where a.`brand_id`= b.`user_id` and a.`id`= d.`product_id` and c.`id` = d.`look_id` and b.`is_paid`=1 and c.`status`=1 and  c.`modified_datetime` >= '".$date_from." 00:00:00' and c.`modified_datetime` <= '".$date_to." 23:59:59'  ORDER BY c.`modified_datetime` desc");
        }else if($date_from!=0 && $date_to==0){
        	$query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`, c.`name` as lookname FROM `product_desc` as a,`brand_info` as b, look as c, look_products as d where a.`brand_id`= b.`user_id` and a.`id`= d.`product_id` and c.`id` = d.`look_id` and b.`is_paid`=1 and c.`status`=1 and  c.`modified_datetime` like '%".$date_from."%'   ORDER BY c.`modified_datetime` desc");
        }else if($date_from==$date_to && $date_from!=0 && $date_to!=0){
        	$query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`, c.`name` as lookname FROM `product_desc` as a,`brand_info` as b, look as c, look_products as d where a.`brand_id`= b.`user_id` and a.`id`= d.`product_id` and c.`id` = d.`look_id` and b.`is_paid`=1 and c.`status`=1 and  c.`modified_datetime` like '%".$date_from."%'   ORDER BY c.`modified_datetime` desc ");
        }
        else{
        	$query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`, c.`name` as lookname FROM `product_desc` as a,`brand_info` as b, look as c, look_products as d where a.`brand_id`= b.`user_id` and a.`id`= d.`product_id` and c.`id` = d.`look_id` and b.`is_paid`=1 and c.`status`=1 and  c.`modified_datetime` like '%".date('Y-m-d')."%' ORDER BY c.`modified_datetime` desc ");
        }
       
        $res = $query->result_array();*/
        
        return $res;
    }

     function unpaidbrand($date_from,$date_to)
    {
    	if($date_from!='' && $date_to!=''  && $date_from!=$date_to){
        $query = $this->db->query("SELECT `id`,`company_name`,`created_datetime`  FROM `brand_info` where `created_datetime` >= '".$date_from." 00:00:00' and  `created_datetime` <= '".$date_to." 23:59:59' and is_paid=0  ORDER BY `created_datetime` desc ");
        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
			$query = $this->db->query("SELECT `id`,`company_name`,`created_datetime`  FROM `brand_info` where `created_datetime` like '%".$date_from."%' and is_paid=0  ORDER BY `created_datetime` desc ");
		}else{
        	$query = $this->db->query("SELECT `id`,`company_name`,`created_datetime`  FROM `brand_info` where `created_datetime`  like '%".date('Y-m-d')."%'  and is_paid=0  ORDER BY `created_datetime` desc ");
        }
        $res = $query->result_array();
        return $res;
    }

     function unpaidbrandproducts($date_from,$date_to)
    {
    	if($date_from!='' && $date_to!=''  && $date_from!=$date_to){
        $query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`  FROM `product_desc` as a,`brand_info` as b where a.`brand_id`= b.`user_id` and b.`is_paid`=0 and  a.`created_datetime` >= '".$date_from." 00:00:00' and a.`created_datetime` <='".$date_to." 23:59:59'  ORDER BY a.`created_datetime` desc ");

        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
        {
        	$query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`  FROM `product_desc` as a,`brand_info` as b where a.`brand_id`= b.`user_id` and b.`is_paid`=0 and  a.`created_datetime` like '%".$date_from."%'  ORDER BY a.`created_datetime` desc");
        }else
        {
        $query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`  FROM `product_desc` as a,`brand_info` as b where a.`brand_id`= b.`user_id`and a.`created_datetime` like '%".date('Y-m-d')."%' and b.`is_paid`=0  ORDER BY a.`created_datetime` desc");
        }

        $res = $query->result_array();
        return $res;
    }

     function userCPCClickData($date_from,$date_to)
    {
    	$res = array();
    	/*SELECT count(a.`click_count`) as `count` FROM `users_product_click_track` as a left join `user_login` as b on a.`user_id` = b.`id` where a.`user_id` != 0 and b.`test_user`!=1*/

    	/*
			select count(b.id) as count,sum(a.price*(c.cpc_percentage/100)) as value from product_desc as a,`users_product_click_track` as b, brand_info as c where a.id = b. product_id  and  a.brand_id = c.user_id and b.`created_datetime` between '".$date_from."' and '".$date_to."'  ORDER BY a.`created_datetime`
    	*/

    	/*if($date_from!='' && $date_to!=''  && $date_from!=$date_to){
      
       $query = $this->db->query("SELECT count(a.`id`) as count,a.click_count as `click_count`,a.`user_id`,b.`test_user`,c.`brand_id`,d.`cpc_percentage`,sum(c.price*(d.`cpc_percentage`/100)) as value  FROM product_desc as c, `brand_info` as d ,`users_product_click_track` as a left join `user_login` as b on a.user_id = b.`id` where a.`created_datetime` >= '".$date_from." 00:00:00' and a.`created_datetime` <= '".$date_to." 23:59:59' and c.`id` = a.`product_id` and c.`brand_id` = d.`user_id` and (b.`test_user` = 0 OR b.`test_user` IS NULL) ORDER BY a.`created_datetime` desc");

        }else  if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
        {
        	

        	$query = $this->db->query("SELECT count(a.`id`) as count,a.click_count as `click_count`,a.`user_id`,b.`test_user`,c.`brand_id`,d.`cpc_percentage`,sum(c.price*(d.`cpc_percentage`/100)) as value  FROM product_desc as c, `brand_info` as d ,`users_product_click_track` as a left join `user_login` as b on a.user_id = b.`id` where a.`created_datetime` like '%".$date_from."%' and c.`id` = a.`product_id` and c.`brand_id` = d.`user_id` and (b.`test_user` = 0 OR b.`test_user` IS NULL) ORDER BY a.`created_datetime`");

        }else
        {
        
        $query = $this->db->query("SELECT count(a.`id`) as count,a.click_count as `click_count`,a.`user_id`,b.`test_user`,c.`brand_id`,d.`cpc_percentage`,sum(c.price*(d.`cpc_percentage`/100)) as value  FROM product_desc as c, `brand_info` as d ,`users_product_click_track` as a left join `user_login` as b on a.user_id = b.`id` where a.`created_datetime` like '%".date('Y-m-d')."%' and c.`id` = a.`product_id` and c.`brand_id` = d.`user_id` and (b.`test_user` = 0 OR b.`test_user` IS NULL) ORDER BY a.`created_datetime`");

        }
		//echo $this->db->last_query();//exit;
        $res = $query->result_array();  */
        
        return $res;
    }
    
    function authCPCClickData($date_from,$date_to)
    {
    	$res = array();

    	/*if($date_from!='' && $date_to!=''  && $date_from!=$date_to){
        $query = $this->db->query("SELECT count(a.`id`) as count,a.click_count as `click_count`,a.`user_id`,b.`test_user`,c.`brand_id`,d.`cpc_percentage`,sum(c.price*(d.`cpc_percentage`/100)) as value  FROM product_desc as c, `brand_info` as d ,`users_product_click_track` as a left join `user_login` as b on a.user_id = b.`id` where a.`created_datetime` >= '".$date_from." 00:00:00' and a.`created_datetime` <= '".$date_to." 23:59:59' and c.`id` = a.`product_id` and c.`brand_id` = d.`user_id` and (b.`test_user` = 0 OR b.`test_user` IS NULL) and a.`user_id`!=0 ORDER BY a.`created_datetime` ");
        }else  if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
        {
        	$query = $this->db->query("SELECT count(a.`id`) as count,a.click_count as `click_count`,a.`user_id`,b.`test_user`,c.`brand_id`,d.`cpc_percentage`,sum(c.price*(d.`cpc_percentage`/100)) as value  FROM product_desc as c, `brand_info` as d ,`users_product_click_track` as a left join `user_login` as b on a.user_id = b.`id` where a.`created_datetime` like '%".$date_from."%' and c.`id` = a.`product_id` and c.`brand_id` = d.`user_id` and (b.`test_user` = 0 OR b.`test_user` IS NULL) and a.`user_id`!=0 ORDER BY a.`created_datetime`");
        }else
        {
        $query = $this->db->query("SELECT count(a.`id`) as count,a.click_count as `click_count`,a.`user_id`,b.`test_user`,c.`brand_id`,d.`cpc_percentage`,sum(c.price*(d.`cpc_percentage`/100)) as value  FROM product_desc as c, `brand_info` as d ,`users_product_click_track` as a left join `user_login` as b on a.user_id = b.`id` where a.`created_datetime` like '%".date('Y-m-d')."%' and c.`id` = a.`product_id` and c.`brand_id` = d.`user_id` and (b.`test_user` = 0 OR b.`test_user` IS NULL) and a.`user_id`!=0 ORDER BY a.`created_datetime`");
        }

        $res = $query->result_array();       */ 
        return $res;
    }


    function authCPCClickData_details($type,$date_from,$date_to)
    { 
    	if($type=='Authenticated')
    	{

    		if($date_from!='' && $date_to!=''  && $date_from!=$date_to && $date_from!=0 && $date_to!=0){
	        $query = $this->db->query("select a.`id`, a.`name`, b.`user_id`,b.`created_datetime` as datetime  from product_desc as a,`users_product_click_track` as b where a.id = b. product_id   and b.`user_id`!=0 and  b.`created_datetime` >= '".$date_from." 00:00:00' and  b.`created_datetime` <= '".$date_to." 23:59:59'  ORDER BY a.`created_datetime` DESC");
	        }else  if(($date_from!='' && $date_to!='' && $date_from==$date_to && $date_from!=0 ) || ($date_from!='' && $date_to!='' && $date_from!=0 ) )
	        {
	        	$query = $this->db->query("select a.`id`, a.`name`, b.`user_id`,b.`created_datetime` as datetime from product_desc as a,`users_product_click_track` as b where a.id = b. product_id  and b.`user_id`!=0 and  b.`created_datetime` like '%".$date_from."%'  ORDER BY a.`created_datetime` DESC");
	        }else
	        {
	        $query = $this->db->query("select a.`id`, a.`name`, b.`user_id`,b.`created_datetime` as datetime from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and b.`created_datetime` like '%".date('Y-m-d')."%' and b.`user_id`!=0 ORDER BY a.`created_datetime` desc");
	        }

    	}else if($type=='UnAuthenticated')
    	{
    		if($date_from!='' && $date_to!=''  && $date_from!=$date_to && $date_from!=0 && $date_to!=0){
        	$query = $this->db->query("select a.`id`, a.`name`, b.`user_id`,b.`created_datetime` as datetime  from product_desc as a,`users_product_click_track` as b where a.id = b. product_id  and b.`user_id`=0 and  b.`created_datetime` >= '".$date_from." 00:00:00' and b.`created_datetime` <=  '".$date_to." 23:59:59'  ORDER BY a.`created_datetime` DESC");
        }else  if(($date_from!='' && $date_to!='' && $date_from==$date_to && $date_from!=0) || ($date_from!='' && $date_to!='' && $date_from!=0 ))
        {
        	$query = $this->db->query("select a.`id`, a.`name`, b.`user_id`,b.`created_datetime` as datetime  from product_desc as a,`users_product_click_track` as b where a.id = b.product_id and b.`user_id`=0 and  b.`created_datetime` like '%".$date_from."%'  ORDER BY a.`created_datetime` DESC");
        }else
        {
        $query = $this->db->query("select a.`id`, a.`name`, b.`user_id`,b.`created_datetime` as datetime  from product_desc as a,`users_product_click_track` as b where a.id = b.product_id and b.`created_datetime` like '%".date('Y-m-d')."%' and b.`user_id`=0 ORDER BY a.`created_datetime` DESC");
        }
    	}   	

        $res = $query->result_array();
        return $res;
    }


   function unauthCPCClickData($date_from,$date_to)
    {
    	$res = array();
    	/*if($date_from!='' && $date_to!=''  && $date_from!=$date_to){
        $query = $this->db->query("select count(b.id) as count,sum(a.price*(c.cpc_percentage/100)) as value from product_desc as a,`users_product_click_track` as b, brand_info as c  where a.id = b. product_id and  a.brand_id = c.user_id and b.`user_id`=0 and  b.`created_datetime` >= '".$date_from." 00:00:00' and b.`created_datetime` <='".$date_to." 23:59:59'  ORDER BY a.`created_datetime` desc");
        }else  if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
        {
        	$query = $this->db->query("select count(b.id) as count,sum(a.price*(c.cpc_percentage/100)) as value from product_desc as a,`users_product_click_track` as b, brand_info as c  where a.id = b.product_id and  a.brand_id = c.user_id and b.`user_id`=0 and  b.`created_datetime` like '%".$date_from."%'  ORDER BY a.`created_datetime` desc");
        }else
        {
        $query = $this->db->query("select count(b.id) as count,sum(a.price*(c.cpc_percentage/100)) as value from product_desc as a,`users_product_click_track` as b, brand_info as c  where a.id = b.product_id and  a.brand_id = c.user_id and  b.`created_datetime` like '%".date('Y-m-d')."%' and b.`user_id`=0 ORDER BY a.`created_datetime` desc");
        }

        $res = $query->result_array();*/
        return $res;
    }

    function userCPAClickData($date_from,$date_to)
    {
    	$res = array();
    	/*if($date_from!='' && $date_to!='' && $date_from!=$date_to){
        $query = $this->db->query("select count(b.id) as count,sum(a.price*(c.cpa_percentage/100)) as value from product_desc as a,`product_cpa_track` as b, brand_info as c where a.id = b. product_id and  a.brand_id = c.user_id and b.created_datetime  >= '".$date_from." 00:00:00' and  b.created_datetime  <=  '".$date_to." 23:59:59'  ORDER BY a.`created_datetime` desc");
        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!='')){
        	 $query = $this->db->query("select count(b.id) as count,sum(a.price*(c.cpa_percentage/100)) as value from product_desc as a,`product_cpa_track` as b, brand_info as c where a.id = b. product_id and  a.brand_id = c.user_id and b.created_datetime  like '%".$date_from."%'  ORDER BY a.`created_datetime` desc");
        }else{
        $query = $this->db->query("select count(b.id) as count,sum(a.price*(c.cpa_percentage/100)) as value from product_desc as a,`product_cpa_track` as b, brand_info as c where a.id = b. product_id and  a.brand_id = c.user_id and b.`created_datetime` like '%".date('Y-m-d')."%' ORDER BY a.`created_datetime` desc");
        }
        $res = $query->result_array();*/
        return $res;
    }

     function authCPAClickData($date_from,$date_to)
    {
    	$res = array();
    	/*if($date_from!='' && $date_to!='' && $date_from!=$date_to){
        $query = $this->db->query("select count(b.id) as count,sum(a.price*(c.cpa_percentage/100)) as value from product_desc as a,`product_cpa_track` as b, brand_info as c where a.id = b. product_id and  a.brand_id = c.user_id and b.`sc_user_id` != 0 and b.created_datetime >= '".$date_from." 00:00:00' and b.created_datetime <='".$date_to." 23:59:59'  ORDER BY a.`created_datetime` desc");
        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!='')){
        	 $query = $this->db->query("select count(b.id) as count,sum(a.price*(c.cpa_percentage/100)) as value from product_desc as a,`product_cpa_track` as b, brand_info as c where a.id = b. product_id and  a.brand_id = c.user_id and b.`sc_user_id` != 0 and b.created_datetime like '%".$date_from."%'  ORDER BY a.`created_datetime` desc");
        }else{
        $query = $this->db->query("select count(b.id) as count,sum(a.price*(c.cpa_percentage/100)) as value from product_desc as a,`product_cpa_track` as b, brand_info as c where a.id = b. product_id and  a.brand_id = c.user_id and b.`sc_user_id` != 0 and b.`created_datetime` like '%".date('Y-m-d')."%' ORDER BY a.`created_datetime` desc");
        }

        $res = $query->result_array();*/
        return $res;
    }
	
	function unauthCPAClickData($date_from,$date_to)
    {
    	$res = array();
    	/*if($date_from!='' && $date_to!='' && $date_from!=$date_to){
        $query = $this->db->query("select count(b.id) as count,sum(a.price*(c.cpa_percentage/100)) as value from product_desc as a,`product_cpa_track` as b, brand_info as c where a.id = b. product_id and  a.brand_id = c.user_id and b.`sc_user_id` = 0 and b.created_datetime >= '".$date_from." 00:00:00' and  b.created_datetime <= '".$date_to." 23:59:59'  ORDER BY a.`created_datetime` desc");
        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!='')){
        	 $query = $this->db->query("select count(b.id) as count,sum(a.price*(c.cpa_percentage/100)) as value from product_desc as a,`product_cpa_track` as b, brand_info as c where a.id = b. product_id and  a.brand_id = c.user_id and b.`sc_user_id` = 0 and b.created_datetime like '%".$date_from."%'  ORDER BY a.`created_datetime` desc");
        }else{
        $query = $this->db->query("select count(b.id) as count,sum(a.price*(c.cpa_percentage/100)) as value from product_desc as a,`product_cpa_track` as b, brand_info as c where a.id = b. product_id and  a.brand_id = c.user_id and b.`sc_user_id` = 0 and b.`created_datetime` like '%".date('Y-m-d')."%' ORDER BY a.`created_datetime` desc ");
        }

        $res = $query->result_array();*/
        return $res;
    }

    function AuthenClickData($date_from,$date_to)
    {
    	$res = array();
    	/*if($date_from!='' && $date_to!=''  && $date_from!=$date_to){
        $query = $this->db->query("select count(b.id) as count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and b.created_datetime >= '".$date_from." 00:00:00' and b.created_datetime <='".$date_to." 23:59:59' and b.`user_id`!=0 ORDER BY a.`created_datetime` desc");
        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!='')){
        $query = $this->db->query("select count(b.id) as count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and b.created_datetime like '%".$date_from."%' and b.`user_id`!=0 ORDER BY a.`created_datetime` desc");
        }else{
        $query = $this->db->query("select count(b.id) as count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and b.`user_id`!=0 and b.`created_datetime` like '%".date('Y-m-d')."%' ORDER BY a.`created_datetime` desc");
        }

        $res = $query->result_array();*/
        return $res;
    }

    function UnAuthenClickData($date_from,$date_to)
    {
    	$res = array();
    /*	if($date_from!='' && $date_to!='' && $date_from!=$date_to){
        $query = $this->db->query("select count(b.id) as count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and b.created_datetime >= '".$date_from." 00:00:00' and b.created_datetime <='".$date_to." 23:59:59' and b.`user_id`=0 ORDER BY a.`created_datetime` desc");
        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
        {
        	$query = $this->db->query("select count(b.id) as count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and b.created_datetime like '%".$date_from."%' and b.`user_id`=0 ORDER BY a.`created_datetime` desc");
        }else{
        $query = $this->db->query("select count(b.id) as count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and b.`user_id`=0 and b.`created_datetime` like '%".date('Y-m-d')."%' ORDER BY a.`created_datetime` desc");
        }

        $res = $query->result_array();*/
        return $res;
    }


    function product_cpc_count($brand_id,$date_from,$date_to){
    	if($date_from!='' && $date_to!='' && $date_from!=$date_to){
        	$query = $this->db->query("select count(b.id) as count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and a.brand_id = '".$brand_id."' and b.created_datetime >= '".$date_from." 00:00:00' and b.created_datetime <=  '".$date_to." 23:59:59' ");

        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
			$query = $this->db->query("select count(b.id) as count from product_desc as a,`users_product_click_track` as b where a.id = b. product_id and a.brand_id = '".$brand_id."' and b.created_datetime like '%".$date_from."%' ");
			
		}else{

        	$query = $this->db->query("select count(b.id) as count from product_desc as a,`users_product_click_track` as b where a.id = b.product_id and a.brand_id ='".$brand_id."' and b.created_datetime like '%".date('Y-m-d')."%' ");
        }

        $res = $query->result_array(); 
        return $res;
    }
    function product_cpc_count_today($brand_id){
    	$query = $this->db->query("select count(b.id) as count from product_desc as a,`users_product_click_track` as b where a.id = b.product_id and a.brand_id ='".$brand_id."' and b.created_datetime like '%".date('Y-m-d')."%' ");
    	$res = $query->result_array(); 
        return $res;
    }
	
	 function product_cpc_count_today_details($brand_id){
    	$query = $this->db->query("select a.id, a.name, a.price, c.id as user_id,c.user_name, c.email, b.created_datetime as datetime from product_desc as a,`users_product_click_track` as b,`user_login` as c where a.id = b.product_id and b.product_id = c.id and a.brand_id ='".$brand_id."' and b.created_datetime like '%".date('Y-m-d')."%' order by  b.created_datetime desc");
    	$result = $query->result_array(); 
        $result['data'] = $query->result_array();
		$result['fields'] = $query->list_fields(); 
		return $result;
    }
	
	
     function product_cpa_count_today($brand_id){
    	$query = $this->db->query("select count(a.id) as count from product_desc as a,`product_cpa_track` as b where a.id = b. product_id and a.brand_id ='".$brand_id."' and  b.created_datetime like '%".date('Y-m-d')."%' ");
    	$res = $query->result_array(); 
        return $res;
    }
	
	function product_cpa_count_today_details($brand_id){
    	$query = $this->db->query("select a.id, a.name, a.price, b.sc_user_id as user_id,b.sc_user_id as user_name, b.email, b.created_datetime as datetime from product_desc as a,`product_cpa_track` as b where a.id = b.product_id and a.brand_id ='".$brand_id."' and  b.created_datetime like '%".date('Y-m-d')."%' order by  b.created_datetime desc");
    	$result = $query->result_array(); 
        $result['data'] = $query->result_array();
		$result['fields'] = $query->list_fields(); 
		return $result;
    }
	
    function product_cpa_count($brand_id,$date_from,$date_to){
    	if($date_from!='' && $date_to!='' && $date_from!=$date_to){

        $query = $this->db->query("select count(a.id) as count from product_desc as a,`product_cpa_track` as b where a.id = b. product_id and a.brand_id ='".$brand_id."' and b.created_datetime >= '".$date_from." 00:00:00' and b.created_datetime <= '".$date_to." 23:59:59'");

        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
			$query = $this->db->query("select count(a.id) as count from product_desc as a,`product_cpa_track` as b where a.id = b. product_id and a.brand_id ='".$brand_id."' and b.created_datetime like '%".$date_from."%' ");

		}else{

        $query = $this->db->query("select count(a.id) as count from product_desc as a,`product_cpa_track` as b where a.id = b. product_id and a.brand_id ='".$brand_id."' and  b.created_datetime like '%".date('Y-m-d')."%' ");
        }

        $res = $query->result_array();
        return $res;
    }

	function get_look_info($date_from,$date_to,$page,$per_page){
		// echo $date_from;
		// echo $date_to;
		// exit;
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
				}else{ $page = 0;}
					$limit_cond = "LIMIT ".$page.",".$per_page;
				}else{
					$limit_cond = '';	
				}
				
		if($date_from!='' && $date_to!=''  && $date_from!=$date_to){
			$sql ="select count(a.id) as user_count,a.bucket,(select answer from answers where id = b.body_shape) as body_shape,(select answer from answers where id = b.style) as style,(select count(id) from look_bucket_mapping where bucket_id = a.bucket AND look_bucket_mapping.modified_datetime >= '".$date_from." 00:00:00' and look_bucket_mapping.modified_datetime <='".$date_to." 23:59:59' ) as look_count from user_login as a,`bucket` as b where a.bucket = b.id and a.modified_datetime >= '".$date_from." 00:00:00' and a.modified_datetime <='".$date_to." 23:59:59' group by bucket ";

		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
			$sql ="select count(a.id) as user_count,a.bucket,(select answer from answers where id = b.body_shape) as body_shape,(select answer from answers where id = b.style) as style,(select count(id) from look_bucket_mapping where bucket_id = a.bucket AND look_bucket_mapping.modified_datetime like '%".$date_from."%' ) as look_count from user_login as a,`bucket` as b where a.bucket = b.id AND a.modified_datetime like '%".$date_from."%' group by bucket ";
		}
		else{ $sql ="select count(a.id) as user_count,a.bucket,(select answer from answers where id = b.body_shape) as body_shape,(select answer from answers where id = b.style) as style,(select count(id) from look_bucket_mapping where bucket_id = a.bucket AND look_bucket_mapping.modified_datetime like '%".date('Y-m-d')."%' ) as look_count from user_login as a,`bucket` as b where a.bucket = b.id  AND a.modified_datetime like '%".date('Y-m-d')."%' group by bucket ";
		}
			
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
		//echo "<pre>";
        //print_r($res);
	    //exit;
		
		
	}
	
	function get_look_details($date_from,$date_to,$page,$per_page){
		$user_role_id = $this->session->userdata('role_id');
		if($user_role_id == 1 || $user_role_id == 3)
	    	{
	    		$querycond = "AND 1=1";
	    	}else
	    	{
	    		$querycond = "AND a.created_by = ".$user_id." ";
	    	}
			
			if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
				}else{ $page = 0;}
					$limit_cond = "LIMIT ".$page.",".$per_page;
				}else{
					$limit_cond = '';	
				}
			
			
			if($date_from!='' && $date_to!='' && $date_from!=$date_to){
				$sql = "SELECT a.id,a.name,a.modified_datetime,(select COUNT(b.user_id) from users_look_click_track b where b.look_id = a.id AND b.created_datetime between '".$date_from."' and '".$date_to."') as view_cpc,a.created_by FROM look a WHERE a.deflag = 0 AND a.status = 1 AND a.modified_datetime >= '".$date_from." 00:00:00' and  a.modified_datetime <='".$date_to." 23:59:59' ".$querycond." ".$limit_cond;
			}else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
			{
				$sql = "SELECT a.id,a.name,a.modified_datetime,(select COUNT(b.user_id) from users_look_click_track b where b.look_id = a.id AND b.created_datetime like '%".$date_from."%') as view_cpc,a.created_by FROM look a WHERE a.deflag = 0 AND a.status = 1 AND a.modified_datetime like '%".$date_from."%' ".$querycond." ".$limit_cond;
			}else
			{
				$sql = "SELECT a.id,a.name,a.modified_datetime,(select COUNT(b.user_id) from users_look_click_track b where b.look_id = a.id) as view_cpc,a.created_by FROM look a WHERE a.deflag = 0 AND a.status = 1 AND a.modified_datetime like '%".date('Y-m-d')."%' ".$querycond." ".$limit_cond;
			}
			
	        $query = $this->db->query($sql);
			
			$res = $query->result_array();
			return $res;
	}

	
	function look_bucket_details($id){
		
		//$sql ="select count(a.id) as user_count,a.bucket,(select count(id) from look_bucket_mapping where bucket_id = a.bucket) as look_count from user_login as a,`bucket` as b where a.bucket = b.id group by bucket";
		
		$sql =" select a.bucket_id as bucket_id,(select answer from answers where id = b.body_shape)  body_shape,(select answer from answers where id = b.style) as style,(select answer from answers where id = b.work_style) as work_style,(select answer from answers where id = b.personal_style) as personal_style from look_bucket_mapping as a INNER JOIN bucket as b ON a.bucket_id = b.id AND  a.look_id = $id";
		
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}
	
	function look_user_count($id){
		$sql =" select COUNT(u.id) as user_count from user_login as u JOIN look_bucket_mapping as l ON u.bucket = l.bucket_id AND l.look_id = $id";
		//$sql =" select COUNT(users_look_click_track.user_id) as user_count from users_look_click_track WHERE look_id = $id";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		// print_r($res);
		// exit;
		return $res;
		
	}

	/* Report Brand Click details  
  public function brand_details($offset=null)
  {
    if(!$this->session->userdata('user_id')){
      $this->load->view('login');
    }else{ 
      $data = array();
      
      if($this->has_rights(15) == 1){

         $date_from = '';  $date_to = '';

         if($this->input->post()){
             $date_from = $this->input->post('date_from');
             $date_to = $this->input->post('date_to');

          }else if($date_from)

          $type = $this->uri->segment(3);
          $brand_id = $this->uri->segment(4);

          $config['base_url'] = base_url().'reports/brand_details/';
          
         // $config['per_page'] = 20;
          $config['per_page'] = '';
          $config['uri_segment'] = 4;
          $config['use_page_numbers'] = TRUE;



        if($type == 'Product_Uploaded')
        {
          $brand_data = $this->reports_model->product_used_count_till_today_detail($brand_id,$date_from,$date_to); 
          $config['total_rows'] = count($brand_data['data']);
          //echo count($brand_data['data']);exit;
          //echo "<br/>";
          //echo "<pre>"; print_r($users_data);exit;

        }else if($type == 'Product_Used')
        {
          $brand_data = $this->reports_model->product_look_used_count_till_today_details($brand_id); 
          $config['total_rows'] = count($brand_data['data']);

        }else if($type == 'Product_Uploaded_Today')
        {
           $brand_data =$this->reports_model->product_used_count_details($brand_id,date('Y-m-d'),'');  
           $config['total_rows'] = count($brand_data['data']);

        }else if($type == 'Product_Used_Today')
        {
            $brand_data = $this->reports_model->product_look_used_details($brand_id,date('Y-m-d'),'');   
            $config['total_rows'] = count($brand_data['data']);

        }

          $config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
          $config['full_tag_close'] = '</ul></div>';

          $config['first_link'] = '&laquo; First';
          $config['first_tag_open'] = '<li>';
          $config['first_tag_close'] = '</li>';

          $config['last_link'] = 'Last &raquo;';
          $config['last_tag_open'] = '<li>';
          $config['last_tag_close'] = '</li>';

          $config['next_link'] = 'Next &rarr;';
          $config['next_tag_open'] = '<li>';
          $config['next_tag_close'] = '</li>';

          $config['prev_link'] = '&larr; Previous';
          $config['prev_tag_open'] = '<li>';
          $config['prev_tag_close'] = '</li>';

          $config['cur_tag_open'] = '<li><a href="">';
          $config['cur_tag_close'] = '</a></li>';

          $config['num_tag_open'] = '<li>';
          $config['num_tag_close'] = '</li>';
          $data['brand_data'] = $brand_data;
          $data['total_rows'] = $config['total_rows'];
          $this->pagination->initialize($config);         

        
        $this->load->view('common/header',$data);
        $this->load->view('reports/brand_details_view');
        $this->load->view('common/footer');
      }else{
        $this->load->view('common/header',$data);
        $this->load->view('not_permission');
        $this->load->view('common/footer');
      }
    }

  }*/
  /* End Report Brand Click details */
	
	/*Ankit Onboarded Brand Product Uploaded*/
	/*function brand_product_uploaded($date_from,$date_to){	
		if($date_from!=0 && $date_to!=0 ){   
        	$query = $this->db->query("SELECT a.`id` as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`  FROM `product_desc` as a,`brand_info` as b where a.`brand_id`= b.`user_id` and b.`is_paid`=1 and  a.`created_datetime` between '".$date_from."' and '".$date_to."'  ORDER BY a.`created_datetime` ");
        }else if(($date_from!=0 && $date_to!=0 && $date_from==$date_to) || ($date_from!=0 || $date_to!=0)){
        	$query = $this->db->query("SELECT a.`id` as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`  FROM `product_desc` as a,`brand_info` as b where a.`brand_id`= b.`user_id` and b.`is_paid`=1 and a.`created_datetime` like '%".date('Y-m-d')."%' ORDER BY a.`created_datetime` ");        
        }else{
        	$query = $this->db->query("SELECT a.`id` as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`  FROM `product_desc` as a,`brand_info` as b where a.`brand_id`= b.`user_id` and b.`is_paid`=1 and a.`created_datetime` like '%".date('Y-m-d')."%' ORDER BY a.`created_datetime` ");
        }
        $res = $query->result_array();        
        return $res;	    
	}*/

	function brand_product_uploaded($date_from,$date_to){		

		if($date_from!='' && $date_to!='' && $date_from != '0' && $date_to != '0' ){   			
        	$query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`  FROM `product_desc` as a,`brand_info` as b where a.`brand_id`= b.`user_id` and b.`is_paid`=1 and  a.`created_datetime` >= '".$date_from." 00:00:00' and  a.`created_datetime` <= '".$date_to." 23:59:59'  ORDER BY a.`created_datetime` desc ");
        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to && $date_to!=0) || ($date_from!='' && $date_to!='' && $date_from!=0 )){           
        	$query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`  FROM `product_desc` as a,`brand_info` as b where a.`brand_id`= b.`user_id` and b.`is_paid`=1 and  a.`created_datetime` like '%".$date_from."%'  ORDER BY a.`created_datetime` desc");
        }else{        	
        	$query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`  FROM `product_desc` as a,`brand_info` as b where a.`brand_id`= b.`user_id` and b.`is_paid`=1 and a.`created_datetime` like  '%".date('Y-m-d')."%' ORDER BY a.`created_datetime` desc");
        }
        $res = $query->result_array();                              
        return $res;				    
	}

	/*function dashborad_onboarded_brand_product($date_from,$date_to){					
	   if($date_from!=0 && $date_to!=0){
        	$query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`, c.`name` as lookname , c.`id` as Look_ID FROM `product_desc` as a,`brand_info` as b, look as c, look_products as d where a.`brand_id`= b.`user_id` and a.`id`= d.`product_id` and c.`id` = d.`look_id` and b.`is_paid`=1 and c.`status`=1 and  c.`modified_datetime` between '".$date_from."' and '".$date_to."'  ORDER BY c.`modified_datetime` ");
        }else if(($date_from!=0 && $date_to!=0 && $date_from==$date_to) || ($date_from!='' && $date_to!='' && $date_from!=0 )){
        	$query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`, c.`name` as lookname , c.`id` as Look_ID FROM `product_desc` as a,`brand_info` as b, look as c, look_products as d where a.`brand_id`= b.`user_id` and a.`id`= d.`product_id` and c.`id` = d.`look_id` and b.`is_paid`=1 and c.`status`=1 and a.`created_datetime` like '%".date('Y-m-d')."%'  ORDER BY c.`created_datetime` ");        
        }else{
        	$query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`, c.`name` as lookname , c.`id` as Look_ID FROM `product_desc` as a,`brand_info` as b, look as c, look_products as d where a.`brand_id`= b.`user_id` and a.`id`= d.`product_id` and c.`id` = d.`look_id` and b.`is_paid`=1 and c.`status`=1 and a.`created_datetime` like '%".date('Y-m-d')."%' ORDER BY c.`created_datetime` ");        
        }
        $res = $query->result_array();
        //echo $this->db->last_query();                
        return $res;	   
	}*/

	function dashborad_onboarded_brand_product($date_from,$date_to){        
	   if($date_from!='' && $date_to!='' && $date_from != $date_to && $date_from !=0 &&   $date_to!=0  ){          
	    $query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`, c.`name` as lookname, c.`id` as ID FROM `product_desc` as a,`brand_info` as b, look as c, look_products as d where a.`brand_id`= b.`user_id` and a.`id`= d.`product_id` and c.`id` = d.`look_id` and b.`is_paid`=1 and c.`status`=1 and  c.`modified_datetime` >= '".$date_from." 00:00:00' and c.`modified_datetime` <= '".$date_to." 23:59:59'  ORDER BY c.`modified_datetime` desc");

	    }else if(($date_from!='' && $date_to!='' && $date_from==$date_to && $date_from!=0) || ($date_from!='' && $date_to!='' && $date_from!=0 )){

	        $query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`, c.`name` as lookname, c.`id` as ID FROM `product_desc` as a,`brand_info` as b, look as c, look_products as d where a.`brand_id`= b.`user_id` and a.`id`= d.`product_id` and c.`id` = d.`look_id` and b.`is_paid`=1 and c.`status`=1 and  c.`modified_datetime` like '%".$date_from."%'   ORDER BY c.`modified_datetime` desc");
	    }else{
	        $query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`, c.`name` as lookname, c.`id` as ID FROM `product_desc` as a,`brand_info` as b, look as c, look_products as d where a.`brand_id`= b.`user_id` and a.`id`= d.`product_id` and c.`id` = d.`look_id` and b.`is_paid`=1 and c.`status`=1 and  c.`modified_datetime` like '%".date('Y-m-d')."%' ORDER BY c.`created_datetime` desc");            
	    }
	    $res = $query->result_array();
	   // echo $this->db->last_query();
	    return $res;       
	}

	/*function dashborad_paid_brand_count($date_from,$date_to){					
		if($date_from!='' && $date_to!=''){
        	//$query = $this->db->query("SELECT `id`,`company_name`,`created_datetime`  FROM `brand_info` where
        	//$`created_datetime` between '".$date_from."' and '".$date_to."' and is_paid=0  ORDER BY `created_datetime`
        	//$");
        	$query = $this->db->query("SELECT  * FROM `brand_info` where `created_datetime` between '".$date_from."' and '".$date_to."' and is_paid=0  ORDER BY `created_datetime` ");
        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!='')){
        	$query = $this->db->query("SELECT *  FROM `brand_info` where  is_paid=0 and `created_datetime` like '%".date('Y-m-d')."%'  ORDER BY `created_datetime` ");
        }else{
        	$query = $this->db->query("SELECT *  FROM `brand_info` where  is_paid=0 and `created_datetime` like '%".date('Y-m-d')."%' ORDER BY `created_datetime` ");
        }
	        $res = $query->result_array();

	        return $res;
	}*/

	function dashborad_paid_brand_count($date_from,$date_to){					
		if($date_from!='' && $date_to!='' && $date_from != '0' && $date_to != '0' ){
        	$query = $this->db->query("SELECT `user_id`,`company_name`,`created_datetime`,`modified_datetime` FROM `brand_info` where `created_datetime` >= '".$date_from." 00:00:00' and `created_datetime` <= '".$date_to." 23:59:59' and is_paid=0  ORDER BY `created_datetime` desc ");
        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to && $date_to!=0) || ($date_from!='' && $date_to!='' && $date_from!=0 )){
        	$query = $this->db->query("SELECT `user_id`,`company_name`,`created_datetime`,`modified_datetime`  FROM `brand_info` where `created_datetime` like '%".$date_from."%' and is_paid=0  ORDER BY `created_datetime` desc ");
        }else{
        	$query = $this->db->query("SELECT `user_id`,`company_name`,`created_datetime`,`modified_datetime`  FROM `brand_info` where `created_datetime`  like '%".date('Y-m-d')."%'  and is_paid=0  ORDER BY `created_datetime` desc");
        }
	        $res = $query->result_array();

	        return $res;
	}

	function dashborad_brand_prd_count($date_from,$date_to){

		if($date_from!='' && $date_to!='' && $date_from!=0 && $date_to!=0){
        	$query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`  FROM `product_desc` as a,`brand_info` as b where a.`brand_id`= b.`user_id` and b.`is_paid`=0 and  a.`created_datetime` >= '".$date_from." 00:00:00' and a.`created_datetime` <= '".$date_to." 23:59:59'  ORDER BY a.`created_datetime` desc");
        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to && $date_from!=0) || ($date_from!='' && $date_to!='' && $date_from!=0 ))
        {
        	$query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`  FROM `product_desc` as a,`brand_info` as b where a.`brand_id`= b.`user_id` and b.`is_paid`=0 and a.`created_datetime` like '%".$date_from."%' ORDER BY a.`created_datetime` desc");
        }else {
        	$query = $this->db->query("SELECT a.`id`as productid, a.`name`,a.`brand_id`, a.`price`, b.`company_name`,a.`created_datetime`  FROM `product_desc` as a,`brand_info` as b where a.`brand_id`= b.`user_id` and b.`is_paid`=0 and a.`created_datetime` like '%".date('Y-m-d')."%'  ORDER BY a.`created_datetime` desc");
        }
        $res = $query->result_array();        
        return $res;
	}	
 } 
?>
