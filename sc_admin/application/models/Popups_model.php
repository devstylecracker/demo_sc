<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Popups_model extends MY_Model {
	 function __construct()
    {
        // Call the Model constructor
        
        parent::__construct();
    }	

    function save_popup($data){
    	if(!empty($data)){
            $cat_data = array();
    		$popup_name = $this->input->post('popup_name');
    		$popup_content = $this->input->post('popup_content');
    		$target_link = $this->input->post('target_link');
    		$page_link = $this->input->post('page_link');
            $popup_interval_chosen = $this->input->post('popup_interval');
            $start_datetime = $this->input->post('start_datetime');
            $end_datetime = $this->input->post('end_datetime');
            $status_chosen = $this->input->post('status');

    		if($popup_name!=''){
                $cat_data['popup_name'] = $popup_name;
                $cat_data['popup_content'] = $popup_content;
                $cat_data['target_link'] = $target_link;
                $cat_data['page_link'] = $page_link;
                $cat_data['popup_interval_chosen'] = $popup_interval_chosen;
                $cat_data['start_datetime'] = $start_datetime;
                $cat_data['end_datetime'] = $end_datetime;
                $cat_data['status'] = $status_chosen;
                $cat_data['created_by'] = $this->session->userdata('user_id');
                $cat_data['modified_by'] = $this->session->userdata('user_id');
                $cat_data['created_datetime'] = date('Y-m-d H:i:s');
                
    			$cat_id = $this->add_popups($cat_data);
                
                if($_FILES["desktop_img"]["name"] !=''){
                    $target_dir = "../assets/images/popups/desktop/".$cat_id.'.jpg';
                    if(move_uploaded_file($_FILES["desktop_img"]["tmp_name"], $target_dir)){
                        $this->update_popup_image($cat_id,'desktop');
                    }
                }

                if($_FILES["mobile_img"]["name"] !=''){
                    $target_dir = "../assets/images/popups/mobile/".$cat_id.'.jpg';
                    if(move_uploaded_file($_FILES["mobile_img"]["tmp_name"], $target_dir)){
                        $this->update_popup_image($cat_id,'mobile');
                    }
                }
    		}
    	}
    }

    function add_popups($data){
        if(!empty($data)){
            $this->db->insert('popups', $data); 
            return $this->db->insert_id(); 
        }else{
            return 0;
        }
    }

    function get_popup(){
        $this->db->select('id, popup_name, popup_content, target_link, page_link, popup_interval_chosen, start_datetime, end_datetime,status,desktop_img,mobile_img,created_by,modified_by,created_datetime,modifided_datetime');
        $this->db->from('popups');
        $this->db->order_by("id", "desc");
        $query = $this->db->get(); 
        return $query->result_array();
    }

    function get_popup_data($cat_id){
        $this->db->from('popups');
        $this->db->where('id',$cat_id);
        $query = $this->db->get(); 
        return $query->result_array();
    }

    function get_filtered_category($search){
        if($search!=''){
            $query = $this->db->query("select distinct `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`,`status` from category where category_name like '%".$search."%' order by category_parent asc");  
            $res = $query->result_array();

            return $res;
        }
    }

    function update_popup_image($cat_id=null,$type){
        $data = array();
        if($cat_id!=null && $cat_id >0){
            if($type=='desktop'){
                $data = array(
                   'desktop_img' => $cat_id.'.jpg'
                );
            }

            if($type=='mobile'){
                $data = array(
                   'mobile_img' => $cat_id.'.jpg'
                );
            }

            if(!empty($data)){
                $this->db->where('id', $cat_id);
                $this->db->update('popups', $data); 
            }
        }
    }

    function edit_popup($data,$cat_id=null){
        if($cat_id!=null && $cat_id >0){
          
            $data1 = array(
               'popup_name' => $data['popup_name'],
               'popup_content' => $data['popup_content'],
               'target_link' => $data['target_link'],
               'page_link' => $data['page_link'],
               'popup_interval_chosen' => $data['popup_interval'],
               'start_datetime' => $data['start_datetime'],
               'end_datetime' => $data['end_datetime'],
               'status' => $data['status'],
            );

            $this->db->where('id', $cat_id);
            $this->db->update('popups', $data1); 

            if($_FILES["desktop_img"]["name"] !=''){
                $target_dir = "../assets/images/popups/desktop/".$cat_id.'.jpg';
                if(move_uploaded_file($_FILES["desktop_img"]["tmp_name"], $target_dir)){
                    $this->update_popup_image($cat_id,'desktop');
                }
            }

            if($_FILES["mobile_img"]["name"] !=''){
                $target_dir = "../assets/images/popups/mobile/".$cat_id.'.jpg';
                if(move_uploaded_file($_FILES["mobile_img"]["tmp_name"], $target_dir)){
                    $this->update_popup_image($cat_id,'mobile');
                }
            }
        }
    }

    function delete_popups($id){
        if($id > 0){
            $this->db->delete('popups', array('id' => $id)); 
            return 1;
        }else{
            return 2;
        }
    }

    function get_cat($id = null){
      $query = $this->db->query("SELECT `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`,`status`,sort_order FROM `category` WHERE 1");
        $res = $query->result_array();
        $tree = $this->buildTree($res);

        $res1 = $this->printAllValues($tree,$this->ouptut);
        return $res1;

    }

    function get_cat_data($id = null){
      $query = $this->db->query("SELECT `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`,`status`,sort_order FROM `category` WHERE 1");
        $res = $query->result_array();
        $tree = $this->buildTree($res);

        $res1 = $this->printAllValues1($tree,$this->ouptut,$id);
        return $res1;

    }

    function buildTree(array $elements, $parentId = '-1') {
    $branch = array();

    foreach ($elements as $element) {
        if ($element['category_parent'] == $parentId) {
            $children = $this->buildTree($elements, $element['id']);
            if ($children) {
                $element['children'] = $children;
            }
            $branch[] = $element;
        }
    }

    return $branch;
    }

    function printAllValues(array $array,$ouptut) {

        foreach($array as $value){
            if($value['category_parent'] == -1){ $this->depth = 0; }
            $nbsp = '';
            $i = $this->depth;
           
            while($i>=0){
                //$nbsp .='-';
                $nbsp='';
                $i--;
            }
            $status = '';
            /*$this->ouptut[$value['id']] = '<option value="'.$value['id'].'" class="level-'.$i.'">'.$nbsp.$value['category_name'].'</option>';*/
            if($value['status'] == 0){ $status = '<span class="text-orange">(Hide)</span>'; }else{ $status = ''; }
            $this->ouptut[$value['id']] = "<tr><td>".$value['id']."</td><td><input type='checkbox' name='cat_show[]'
             value='".$value['id']."' class='checkbox'/></td><td>".$nbsp.$value['category_name'].$status."</td><td>".$value['category_slug']."</td><td>".$value['count']."</td><td><a href='".base_url()."categories/categories_edit/".$value['id']."'><button class='btn btn-primary btn-xs editProd' type='button' ><i class='fa fa-edit'></i> Edit</button></a> <button class='btn btn-xs btn-primary btn-delete' type='button' onclick='delete_category(".$value['id'].");'><i class='fa fa-trash-o'></i> Delete</button></td></tr>";
            
            if(isset($value['children'])){
                $this->depth = $this->depth+1;
                $this->printAllValues($value['children'],$ouptut);
            }else{
                $this->children = 0;
            }
        
        }
        return $this->ouptut;
    }


    function printAllValues1(array $array,$ouptut,$id=null) {

        foreach($array as $value){
            if($value['category_parent'] == -1){ $this->depth = 0; }
            $nbsp = '';
            $i = $this->depth;
            /*while($i>=0){
                $nbsp .='&nbsp;&nbsp;';
                $i--;
            }*/
            if($id!=null && $value['id'] == $id){
            $this->ouptut[$value['id']] = '<option value="'.$value['id'].'" class="level-'.$i.'" selected>'.$nbsp.$value['category_name'].'->'.$this->get_slug($value['category_parent']).'</option>'; $this->slug ='';
            }else{
            $this->ouptut[$value['id']] = '<option value="'.$value['id'].'" class="level-'.$i.'">'.$nbsp.$value['category_name'].'->'.$this->get_slug($value['category_parent']).'</option>'; $this->slug ='';
            }
            
            if(isset($value['children'])){
                $this->depth = $this->depth+1;
                $this->printAllValues1($value['children'],$ouptut,$id);
            }else{
                $this->children = 0;
            }
        
        }
        return $this->ouptut;
    }

    function get_all_buckets(){
        $query = $this->db->query("select a.id,(select answer from answers where id =a.body_shape) as body_shape,(select answer from answers where id = a.style) as style,(select answer from answers where id = a.work_style) as work_style,(select answer from answers where id = a.personal_style) as personal_style from bucket as a");
        $res = $query->result_array();
        return $res;
    }

    function get_question_answers($id){
        if($id){
            $query = $this->db->query('SELECT a.id,a.answer,a.description FROM `answers` as a, `question_answer_assoc`  as b where a.`id` = b.`answer_id` AND a.`status` =1 AND b.`question_id` IN ('.$id.')');

            $result = $query->result_array();
            return $result;
        }

    }

    function get_pa_data($id,$type,$pa){
        if($id !=''){
            $query = $this->db->get_where('pa_object_relationship', array('object_id' => $id,'object_type'=>$type,'pa_type'=>$pa));
            $res = $query->result_array(); 
            $objects = array();
            if(!empty($res)){
                foreach($res as $val){

                    $objects[] = $val['object_value'];
                }
            }

            return $objects;
        }
    }

    function get_slug($id){
        
        if($id!='-1'){
        $query = $this->db->query('select category_name,category_parent from category where id = "'.$id.'"');
        $res = $query->result_array();
        if(!empty($res)){
            foreach($res as $val){

                $this->get_slug($val['category_parent']);

                $this->slug = '->'.$val['category_name'].$this->slug; 

            }
            return  $this->slug ;
        }
        }
        
    }

    function hide_show_cat($cat_hide_show,$cat){
        if(!empty($cat)){
            foreach ($cat as $value) {
                $data=array('status'=>$cat_hide_show);
                $this->db->where('id',$value);
                $this->db->update('category',$data);
            }
        }
    }

}
?>