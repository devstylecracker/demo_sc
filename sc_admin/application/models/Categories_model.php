<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories_model extends MY_Model {
	public $ouptut = array();
    public $depth = 0; public $children = 0;
	 function __construct()
    {
        // Call the Model constructor
        $this->slug = '';
        parent::__construct();
    }	

    function save_category($data){
    	if(!empty($data)){
            $cat_data = array();
    		$cat_name = $this->input->post('category_name');
    		$cat_slug = $this->input->post('category_slug');
    		$cat_parent = $this->input->post('category_parent');
    		$cat_desc = $this->input->post('category_description');
            $size_guide = $this->input->post('size_guide');
            $sort_order = $this->input->post('sort_order');

    		if($cat_name!=''){
                $cat_data['category_name'] = $cat_name;
                $cat_data['category_content'] = $cat_desc;
                if($this->get_cat_slug($cat_parent)!=''){
                    $cat_data['category_slug'] = $this->get_cat_slug($cat_parent).'-'.strtolower($cat_slug);
                    //$cat_data['category_slug'] = strtolower($cat_slug);
                }else{
                    $cat_data['category_slug'] = strtolower($cat_slug);
                }
                $cat_data['category_parent'] = $cat_parent;
                $cat_data['created_datetime'] = date('Y-m-d H:i:s');
                $cat_data['size_guide'] = $size_guide;
                 $cat_data['sort_order'] = $sort_order;
    			$cat_id = $this->add_category($cat_data);
                
                if($_FILES["category_image"]["name"] !=''){
                    $target_dir = "../assets/images/products_category/".$cat_id.'.jpg';
                    if(move_uploaded_file($_FILES["category_image"]["tmp_name"], $target_dir)){
                        $cat_id = $this->update_cat_image($cat_id);
                    }
                }
    		}
    	}
    }

    function add_category($data){
        if(!empty($data)){
            $this->db->insert('category', $data); 
            return $this->db->insert_id(); 
        }else{
            return 0;
        }
    }

    function get_all_categories($cat_id = '-1'){
        $this->db->from('category');
        $this->db->order_by("category_parent", "asc");
        $this->db->where('category_parent',$cat_id);
        $query = $this->db->get(); 
        return $query->result_array();
    }

    function get_all(){
        $this->db->select('id, category_name,  category_slug,category_parent');
        $this->db->from('category');
        $this->db->order_by("category_parent", "asc");
        $query = $this->db->get(); 
        return $query->result_array();
    }

    function get_all_parent(){/*
        $this->db->select('category_parent');
        $this->db->from('category');
        $this->db->order_by("category_parent", "asc");
        $query = $this->db->get();*/ //select id, category_name,  category_slug,category_parent from category where id in
        $query = $this->db->query("(select distinct category_parent as id from category) order by id asc"); 

        return $query->result_array();
    }

    function get_categories($cat_id){
        $this->db->from('category');
        $this->db->order_by("category_parent", "asc");
        $this->db->where('id',$cat_id);
        $query = $this->db->get(); 
        return $query->result_array();
    }

    function get_filtered_category($search){
        if($search!=''){
            $query = $this->db->query("select distinct `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`,`status` from category where category_name like '%".$search."%' order by category_parent asc");  
            $res = $query->result_array();

            return $res;
        }
    }

    function get_cat_slug($cat_id=null){
        if($cat_id!='-1'){
            $this->db->from('category');
            $this->db->where('id',$cat_id);
            $query = $this->db->get(); 
            $res = $query->result_array(); 
            if(!empty($res)){
                return $res[0]['category_slug'];
            }
        }else{ return ''; }
    }

    function update_cat_image($cat_id=null){
        if($cat_id!=null && $cat_id >0){
            $data = array(
               'category_image' => $cat_id.'.jpg'
            );

            $this->db->where('id', $cat_id);
            $this->db->update('category', $data); 
        }
    }

    function edit_category($data,$cat_id=null){
        if($cat_id!=null && $cat_id >0){
            $category_slug = '';
            if($this->get_cat_slug($data['category_parent'])!=''){
                    $category_slug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $data['category_name'])); 
                    $category_slug = $this->get_cat_slug($data['category_parent']).'-'.strtolower($category_slug);
                   // $category_slug = strtolower($category_slug);
            }else{
                    $category_slug = strtolower($data['category_slug']);
            }


            $data1 = array(
               'category_name' => $data['category_name'],
               'category_content' => $data['category_description'],
               'category_parent' => $data['category_parent'],
              // 'category_slug' => $category_slug,
               'size_guide' => $data['size_guide'],
               'sort_order' => $data['sort_order'],
               'comp_cat' => $data['comp_cat'],
            );

            $this->db->where('id', $cat_id);
            $this->db->update('category', $data1); 

            if($_FILES["category_image"]["name"] !=''){
                $target_dir = "../assets/images/products_category/".$cat_id.'.jpg';
                if(move_uploaded_file($_FILES["category_image"]["tmp_name"], $target_dir)){
                    $cat_id = $this->update_cat_image($cat_id);
                }
            }
            $this->db->delete('pa_object_relationship', array('object_id' => $cat_id,'pa_type'=>'bucket','object_type'=>'category'));
            if(!empty($data['bucket'])){ 
                

                foreach ($data['bucket'] as $value) {
                    $k = array(
                            'object_id' => $cat_id,
                            'pa_type' => 'bucket',
                            'object_type' => 'category',
                            'object_value' => $value,
                            'created_by' => $this->session->userdata('user_id'),
                            'modified_by' => $this->session->userdata('user_id'),
                            'created_datetime' => date('Y-m-d H:i:s')
                        );
                    $this->db->insert('pa_object_relationship', $k); 
                    
                 } 
            }
            $this->db->delete('pa_object_relationship', array('object_id' => $cat_id,'pa_type'=>'age','object_type'=>'category'));
            if(!empty($data['age'])){ 
                

                foreach ($data['age'] as $value) {
                    $k = array(
                            'object_id' => $cat_id,
                            'pa_type' => 'age',
                            'object_type' => 'category',
                            'object_value' => $value,
                            'created_by' => $this->session->userdata('user_id'),
                            'modified_by' => $this->session->userdata('user_id'),
                            'created_datetime' => date('Y-m-d H:i:s')
                        );
                    $this->db->insert('pa_object_relationship', $k); 
                    
                 } 
            }
             $this->db->delete('pa_object_relationship', array('object_id' => $cat_id,'pa_type'=>'budget','object_type'=>'category'));
            if(!empty($data['budget'])){  
               

                foreach ($data['budget'] as $value) {
                    $k = array(
                            'object_id' => $cat_id,
                            'pa_type' => 'budget',
                            'object_type' => 'category',
                            'object_value' => $value,
                            'created_by' => $this->session->userdata('user_id'),
                            'modified_by' => $this->session->userdata('user_id'),
                            'created_datetime' => date('Y-m-d H:i:s')
                        );
                    $this->db->insert('pa_object_relationship', $k); 
                    
                 } 
            }
        }
    }

    function delete_category($id){
        if($id > 0){
            $this->db->delete('category', array('id' => $id)); 
            return 1;
        }else{
            return 2;
        }
    }

    function get_cat($id = null){
      $query = $this->db->query("SELECT `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`,`status`,sort_order FROM `category` WHERE 1");
        $res = $query->result_array();
        $tree = $this->buildTree($res);

        $res1 = $this->printAllValues($tree,$this->ouptut);
        return $res1;

    }

    function get_cat_data($id = null){
      $query = $this->db->query("SELECT `id`, `category_name`, `category_content`, `category_image`, `category_parent`, `category_type`, `category_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`,`status`,sort_order FROM `category` WHERE 1");
        $res = $query->result_array();
        $tree = $this->buildTree($res);

        $res1 = $this->printAllValues1($tree,$this->ouptut,$id);
        return $res1;

    }

    function buildTree(array $elements, $parentId = '-1') {
    $branch = array();

    foreach ($elements as $element) {
        if ($element['category_parent'] == $parentId) {
            $children = $this->buildTree($elements, $element['id']);
            if ($children) {
                $element['children'] = $children;
            }
            $branch[] = $element;
        }
    }

    return $branch;
    }

    function printAllValues(array $array,$ouptut) {

        foreach($array as $value){
            if($value['category_parent'] == -1){ $this->depth = 0; }
            $nbsp = '';
            $i = $this->depth;
           
            while($i>=0){
                //$nbsp .='-';
                $nbsp='';
                $i--;
            }

            /*$this->ouptut[$value['id']] = '<option value="'.$value['id'].'" class="level-'.$i.'">'.$nbsp.$value['category_name'].'</option>';*/

            if($value['status'] == 0){ $status = '<span class="text-orange">(Hide)</span>'; }else{ $status = ''; }
            $this->ouptut[$value['id']] = "<tr><td>".$value['id']."</td><td><input type='checkbox' name='cat_show[]'
             value='".$value['id']."' class='checkbox'/></td><td>".$nbsp.$value['category_name'].$status."</td><td>".$value['category_slug']."</td><td>".$value['count']."</td><td><a href='".base_url()."categories/categories_edit/".$value['id']."'><button class='btn btn-primary btn-xs editProd' type='button' ><i class='fa fa-edit'></i> Edit</button></a> <button class='btn btn-xs btn-primary btn-delete' type='button' onclick='delete_category(".$value['id'].");'><i class='fa fa-trash-o'></i> Delete</button></td></tr>";
            
            if(isset($value['children'])){
                $this->depth = $this->depth+1;
                $this->printAllValues($value['children'],$ouptut);
            }else{
                $this->children = 0;
            }
        
        }
        return $this->ouptut;
    }


    function printAllValues1(array $array,$ouptut,$id=null) {

        foreach($array as $value){
            if($value['category_parent'] == -1){ $this->depth = 0; }
            $nbsp = '';
            $i = $this->depth;
            /*while($i>=0){
                $nbsp .='&nbsp;&nbsp;';
                $i--;
            }*/
            if($id!=null && $value['id'] == $id){
            $this->ouptut[$value['id']] = '<option value="'.$value['id'].'" class="level-'.$i.'" selected>'.$nbsp.$value['category_name'].'->'.$this->get_slug($value['category_parent']).'</option>'; $this->slug ='';
            }else{
            $this->ouptut[$value['id']] = '<option value="'.$value['id'].'" class="level-'.$i.'">'.$nbsp.$value['category_name'].'->'.$this->get_slug($value['category_parent']).'</option>'; $this->slug ='';
            }
            
            if(isset($value['children'])){
                $this->depth = $this->depth+1;
                $this->printAllValues1($value['children'],$ouptut,$id);
            }else{
                $this->children = 0;
            }
        
        }
        return $this->ouptut;
    }

    function get_all_buckets(){

       /* $query = $this->db->query("select a.id,(select answer from answers where id =a.body_shape) as body_shape,(select answer from answers where id = a.style) as style,(select answer from answers where id = a.work_style) as work_style,(select answer from answers where id = a.personal_style) as personal_style from bucket as a");
        $res = $query->result_array();
        return $res;*/
        return true;
    }

    function get_question_answers($id){
       /* if($id){
            $query = $this->db->query('SELECT a.id,a.answer,a.description FROM `answers` as a, `question_answer_assoc`  as b where a.`id` = b.`answer_id` AND a.`status` =1 AND b.`question_id` IN ('.$id.')');


            $result = $query->result_array();
            return $result;
        }
        */
        return true;
    }

    function get_pa_data($id,$type,$pa){
        if($id !=''){
            $query = $this->db->get_where('pa_object_relationship', array('object_id' => $id,'object_type'=>$type,'pa_type'=>$pa));
            $res = $query->result_array(); 
            $objects = array();
            if(!empty($res)){
                foreach($res as $val){

                    $objects[] = $val['object_value'];
                }
            }

            return $objects;
        }
    }

    function get_slug($id){
        
        if($id!='-1'){
        $query = $this->db->query('select category_name,category_parent from category where id = "'.$id.'"');
        $res = $query->result_array();
        if(!empty($res)){
            foreach($res as $val){

                $this->get_slug($val['category_parent']);

                $this->slug = '->'.$val['category_name'].$this->slug; 

            }
            return  $this->slug ;
        }
        }
        
    }

    function hide_show_cat($cat_hide_show,$cat){
        if(!empty($cat)){
            foreach ($cat as $value) {
                $data=array('status'=>$cat_hide_show);
                $this->db->where('id',$value);
                $this->db->update('category',$data);
            }
        }
    }
	
	public function get_brands_store()
	{
		$this->db->select('id,company_name,slug');
		$this->db->from('brand_info');
		$this->db->where('status', '1');
		$this->db->order_by('company_name', 'ASC');
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}

    public function get_storeBrandData()
    {
        $this->db->select('bi.id,bi.user_id as store_id,bs.brand_id as store_id,bi.company_name as store_name,bi.slug,bs.brand_store_name');
        $this->db->from('brand_info as bi');
        $this->db->join('brand_store as bs', 'bs.brand_id = bi.user_id','INNER');
        $this->db->where('bi.status', '1');
        $this->db->where('bs.status', '1');
        $this->db->order_by('bi.user_id', 'DESC');
        $query = $this->db->get();
        if($query->num_rows()>0)
        {            
            return $query->result_array();
        }
        return false;
    }

}
?>