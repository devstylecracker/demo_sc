<?php
class Product_model extends CI_Model {
    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
   
	/*---------Product Details functionalities-------*/

	public function get_all_product_details($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL){
		
		
		if($search_by == '1'){ $search_cond = " AND a. name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '2'){ $search_cond = " AND b.name like '%".strip_tags(trim($table_search))."%'"; }
			else if($search_by == '3'){ $search_cond = " AND c.name like '%".strip_tags(trim($table_search))."%'"; }
				//else if($search_by == '4'){ $search_cond = " AND a.slug like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '5'){ 

				$tb_status=strip_tags(trim($table_search));
				/*if($tb_status=="Approved")
				{
					$apr_status = 'A';

				}else
				{
					if($tb_status == "Reject")
					{
						$apr_status = 'R';
					}else
					{
						$apr_status = 'P';
					}
					
				}*/

				if($tb_status=="Approved"){ $search_cond = " AND a.approve_reject = 'A'"; }
				else if($tb_status=="Reject") { $search_cond = " AND a.approve_reject = 'R'"; }
				else{ $search_cond = " AND (a.approve_reject = 'P' || a.approve_reject = 'D')"; }

			}
		else{ $search_cond ='AND 1=1'; }
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}

		if($this->session->userdata('role_id') == 6 ){ 
			$search_cond = " and a.created_by=".$this->session->userdata('user_id');
		}
		$query = $this->db->query("select a.id, a.product_cat_id, a. product_sub_cat_id, a.name, a.slug, a.status,a.is_delete, a.created_datetime, a.modified_datetime,a.image, a.approve_reject, b.id as cat_id, b.name AS cat_name, c.id as subcat_id, c.name AS subcat_name FROM `product_desc` AS a, `product_category` AS b, `product_sub_category` AS c  WHERE 
		 a.`product_cat_id` = b.`id` 
		AND a.`product_sub_cat_id` = c.`id`
		AND a.`is_delete` = 0 ".$search_cond." order by a.id desc ".$limit_cond);		
		$res = $query->result();
		
		return $res;
		

	}//this function is used to get the product details added
	

	
	function insert_product($formFileds, $imageField){
		//echo "<pre>";print_r($formFileds);
		//echo "<br/>";
		$rand_string = substr(str_shuffle(MD5(microtime())), 0, 10);
		$slug = $this->input->post('product_name').$rand_string;
		$data_post = array(
			'name'=>$this->input->post('product_name'),
			'brand_id'=>$this->input->post('brand'),
			'product_cat_id'=>$this->input->post('category'),
			'product_sub_cat_id'=>$this->input->post('sub_category'),
			'price'=>$this->input->post('product_price'),
			'url'=>$this->input->post('product_url'),
			//'product_code' => $this->input->post('product_code'),
			'price_range' => $this->input->post('budget'),
			'description' => $this->input->post('product_desc'),	
			'rating' => $this->input->post('product_rate'),	
			'slug' => $slug,
			'body_part' => $this->input->post('body_part'),
			'body_type' => $this->input->post('body_type'),
			'body_shape' => $this->input->post('body_shape'),
			'personality' => $this->input->post('personality'),
			'age_group' => $this->input->post('age'),
			'image' => $imageField,
			'price' => $this->input->post('product_price'),
			'target_market' => $this->input->post('product_target_market'),			
			'consumer_type' => $this->input->post('product_consumer_type'),
			'status'  => $this->input->post('active'),
			'is_promotional' => $this->input->post('promotional'),
			'created_by'=>$this->session->userdata('user_id'),
			'modified_by'=>$this->session->userdata('user_id'),
			'created_datetime'=>date('Y-m-d H:i:s')			
		);

		$productRes = $this->db->insert('product_desc',$data_post);
	
		$insert_id = $this->db->insert_id();

		if($insert_id!="")
		{
			
			$data_post1 = array();
			$data_post2 = array();
			$data_post3 = array();
			$prodVarRes = "";
			$tagRes = "";
			$storeRes = "";
			$variation_value ="";
			$variation_type = $this->input->post('variation_type');
			
			$counvartype = sizeof($variation_type);			

			//$variation_type = $this->input->post('variation_type');
			$variation_type = $formFileds['variation_type'];			
			
			if(!empty($variation_type))
			{
				for($i=0; $i<$counvartype; $i++)
				{
					//$variation_value = $this->input->post('variation_value_'.$i);
					$variation_value = $formFileds['variation_value_'.$i];	
					foreach($variation_value as $varval)
					{
						$data_post1[] = array(
						'product_var_type_id' => $variation_type[$i],
						'product_id' => $insert_id ,
						'product_var_val_id' => $varval							
						); 
					}						

				}
				
				$prodVarRes = $this->db->insert_batch('product_variations',$data_post1);
				//echo $this->db->last_query();exit;
							
			}
				

			if($prodVarRes)
			{
				$tagval = $this->input->post('product_tags');	
				if(!empty($tagval))
				{
					foreach($tagval as $singletag)
					{
						$data_post2[] = array(					
							'product_id' => $insert_id,
							'tag_id' => $singletag,
							'status'  => $this->input->post('active'),
							'created_by'=>$this->session->userdata('user_id'),
							'modified_by'=>$this->session->userdata('user_id'),
							'created_datetime'=>date('Y-m-d H:i:s')						
						); 
					}	
					$tagRes = $this->db->insert_batch('product_tag',$data_post2);
				}
				
				if($tagRes)
				{	
					$store= $this->input->post('product_avail');						
					if(!empty($store))
					{
						foreach($store as $singlestore)
						{
							$data_post3[] = array(					
								'product_id' => $insert_id,
								'store_id' => $singlestore,							
								'status'=>$this->input->post('active')													
							); 
						}
						$storeRes = $this->db->insert_batch('product_store',$data_post3);	
					}				

				}
				
				if($storeRes)
				{
					return true;

				}else
				{
					return false;
				}				

			}
			
		}

	}


	// to load question answers
	function load_questAns($id){
		$query = $this->db->query("SELECT A.id, A.answer FROM answers AS A INNER JOIN question_answer_assoc AS Q ON Q.answer_id=A.id AND (Q.question_id = ".$id.") ORDER BY A.answer");
		//$result_shape_array = $query->row_array();
		$query_result[0] = 'Select';
		foreach ($query->result() as $row){
			$query_result[$row->id] = $row->answer;
			//$query_result['id'][] = $row->id;
		}
		return($query_result);
	}

	
	public function get_all_product_details_id($prod_id)
	{
		/*$query = $this->db->query("select a.id, a.name, a.slug, a.status,a.is_delete, a.created_datetime, a.modified_datetime,a.image,b.id as cat_id, b.name AS cat_name, c.id as subcat_id, c.name AS subcat_name FROM `product_desc` AS a, `product_category` AS b, `product_sub_category` AS c  WHERE 
		 a.`product_cat_id` = b.`id` 
		AND a.`product_sub_cat_id` = c.`id`
		AND a.`is_delete` =0 AND a.id = ".$prod_id." ");*/
		$query = $this->db->query("select a.*,b.id as cat_id, b.name AS cat_name, c.id as subcat_id, c.name AS subcat_name FROM `product_desc` AS a, `product_category` AS b, `product_sub_category` AS c WHERE 
		 a.`product_cat_id` = b.`id` 
		AND a.`product_sub_cat_id` = c.`id`
		AND a.`is_delete` =0 AND a.id = ".$prod_id." ");		
		$res = $query->result();
		
		return $res;		
		
	}
	
	
	public function delete_product($prodID){
		
		$this->db->where('id', $prodID);
 		$deleted = $this->db->update('product_desc' ,array('is_delete'=>1));
		//echo $deleted ;exit;
		if($deleted)
		{			
			return true;			
		}
		else
		{			
			return false;	
		}
	}
	
	
	public function approve_product($prodID)
	{
		$this->db->where('id', $prodID);
		$result = $this->db->update('product_desc' ,array('approve_reject'=>'A'));		
		
		if($result)
		{			
			return true;			
		}
		else
		{			
			return false;	
		}
	}
	

	public function reject_product($prodID,$reason)
	{
		$this->db->where('id', $prodID);
		$result = $this->db->update('product_desc' ,array('approve_reject'=>'R','reason'=>$reason));	
				
		
		if($result)
		{			
			return true;			
		}
		else
		{			
			return false;	
		}
	}

	function get_promotional_products(){
		$query = $this->db->get_where('product_desc', array('is_promotional' => 1,'is_delete'=>0));
		return $query->result_array();		
	}

	/* get tag by productid */
	public function get_tag_byprdid($prd_id)
	{	

		$query = $this->db->get_where('product_tag', array('product_id' => $prd_id));
		return $query->result_array();	
		
	}

	/* get store by productid */
	public function get_store_byprdid($prd_id)
	{	

		$query = $this->db->get_where('product_store', array('product_id' => $prd_id));
		return $query->result_array();	

		
	}

	function get_store_info(){

		$this->db->select('*');
		$this->db->from('brand_store');			
		
		$store = $this->db->get(); 
		
		if($store)
		{			
			return $store->result();
			
		}
	}

	/* get store by productid */
	public function get_productvar_byprdid($prd_id)
	{	
		$query = $this->db->get_where('product_variations', array('product_id' => $prd_id));
		return $query->result_array();	
		
	}

	public function update_product($formFileds, $imageField,$id){
		//print_r($formFileds);exit;
		$rand_string = substr(str_shuffle(MD5(microtime())), 0, 10);
		$slug = $this->input->post('product_name').$rand_string;
		$data_post = array(
			'name'=>$this->input->post('product_name'),
			'brand_id'=>$this->input->post('brand'),
			'product_cat_id'=>$this->input->post('category'),
			'product_sub_cat_id'=>$this->input->post('sub_category'),
			'price'=>$this->input->post('product_price'),
			'url'=>$this->input->post('product_url'),
			//'product_code' => $this->input->post('product_code'),
			'price_range' => $this->input->post('product_price_range'),
			'description' => $this->input->post('product_desc'),	
			'rating' => $this->input->post('product_rate'),	
			'slug' => $slug,
			'body_part' => $this->input->post('body_part'),
			'body_type' => $this->input->post('body_type'),
			'body_shape' => $this->input->post('body_shape'),
			'personality' => $this->input->post('personality'),
			'age_group' => $this->input->post('age'),
			'image' => $imageField,
			'price' => $this->input->post('product_price'),
			'target_market' => $this->input->post('product_target_market'),			
			'consumer_type' => $this->input->post('product_consumer_type'),
			'status'  => $this->input->post('active'),
			'is_promotional' => $this->input->post('promotional'),
			'created_by'=>$this->session->userdata('user_id'),
			'modified_by'=>$this->session->userdata('user_id'),
			'created_datetime'=>date('Y-m-d H:i:s')
			//need to add the following values.
			// 'status'  => $this->input->post('body_shape'),
			// 'approve_reject' => $this->input->post('body_shape'),
			// 'image' => $this->input->post('body_shape'),
			// 'age_group' => $this->input->post('body_shape'),
			//
		);
		//print_r($data_post);exit;
		$this->db->where('id', $id);
		$productRes = $this->db->update('product_desc',$data_post);
		//$insert_id = $this->db->insert_id();
		if($id!="")
		{		


			$data_post1 = array();
			$data_post2 = array();
			$data_post3 = array();
			$prodVarRes = "";
			$tagRes = "";
			$storeRes = "";

			$this->db->where('product_id', $id);
			$delvarRes = $this->db->delete('product_variations'); 

			if($delvarRes)
			{
				$variation_type = $this->input->post('variation_type');
				
				$counvartype = sizeof($variation_type);			

				$variation_type = $this->input->post('variation_type');
				//$variation_value = $this->input->post('variation_value');
				
				if(!empty($variation_type))
				{
					for($i=0; $i<$counvartype; $i++)
					{
						$variation_value = $this->input->post('variation_value_'.$i);
							
						foreach($variation_value as $varval)
						{
							$data_post1[] = array(
							'product_var_type_id' => $variation_type[$i],
							'product_id' => $id ,
							'product_var_val_id' => $varval							
							); 
						}						

					}
					
					$prodVarRes = $this->db->insert_batch('product_variations',$data_post1);
					//echo $this->db->last_query(); exit;
				}
			}	

			$this->db->where('product_id', $id);
			$deltagRes = $this->db->delete('product_tag'); 

			if($deltagRes)
			{

				if($prodVarRes)
				{
					$tagval = $this->input->post('product_tags');	
					if(!empty($tagval))
					{
						foreach($tagval as $singletag)
						{
							$data_post2[] = array(					
								'product_id' => $id,
								'tag_id' => $singletag,
								'status'  => $this->input->post('active'),
								'created_by'=>$this->session->userdata('user_id'),
								'modified_by'=>$this->session->userdata('user_id'),
								'created_datetime'=>date('Y-m-d H:i:s')						
							); 
						}	
						$tagRes = $this->db->insert_batch('product_tag',$data_post2);
					}
					

					
					
					/*if($delstoreRes)
					{ */
							
							$store= $this->input->post('product_avail');						
							if(!empty($store))
							{
								foreach($store as $singlestore)
								{
									$data_post3[] = array(					
										'product_id' => $id,
										'store_id' => $singlestore,							
										'status'=>$this->input->post('active')													
									); 
								}
								$this->db->where('product_id', $id);
								$delstoreRes = $this->db->delete('product_store'); 

								$storeRes = $this->db->insert_batch('product_store',$data_post3);	
								
							}				

						
					/*}-*/
					
					if($storeRes)
					{
						return true;

					}else
					{
						return false;
					}				

				}
			}
			
		}

	}
	public function id_wise_delete($k){						
		foreach ($k as $deleted_id_key) {					
			$this->db->where('product_id', $deleted_id_key);
			$delvarRes = $this->db->delete('extra_images'); 
		}
		foreach ($k as $deleted_id_key) {					
			$this->db->where('product_id', $deleted_id_key);
			$delvarRes = $this->db->delete('product_tag'); 
		}
		foreach ($k as $deleted_id_key) {					
			$this->db->where('product_id', $deleted_id_key);
			$delvarRes = $this->db->delete('product_inventory'); 
		}
		foreach ($k as $deleted_id_key) {					
			$this->db->where('id', $deleted_id_key);
			$delvarRes = $this->db->delete('product_desc'); 
		}		
		return $delvarRes;
	}	
 } 
?>
