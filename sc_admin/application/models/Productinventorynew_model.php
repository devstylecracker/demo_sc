<?php
class productinventorynew_model extends MY_Model {    
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    function get_paid_stores()
    {
        if($this->session->userdata('role_id') == 6){
            $brand_id = $this->session->userdata('user_id');
            $query = $this->db->query("SELECT user_id as brand_id, company_name FROM brand_info  WHERE is_paid = 1 and user_id=".$brand_id." ORDER BY company_name ASC");
        }else{
            $query = $this->db->query("SELECT user_id as brand_id, company_name FROM brand_info  WHERE is_paid = 1 ORDER BY company_name ASC");
        }
        $result = $query->result_array();
        return $result;
    }

    function get_products($store_id,$product_id,$product_sku,$min_offset,$max_offset){
    	 if($min_offset > 0){ $min_offset = $min_offset*50; }
          $limit_cond =" ";
         if($product_sku!='')
         {
           /* $query_sku = $this->db->query("SELECT distinct product_id  FROM product_inventory  WHERE product_sku LIKE '%".$product_sku."%' ORDER BY id DESC");*/            
            /*$product_str = implode(",",array_map(function($a) {return implode(",",$a);},$result_sku));
            $product_id = $product_str;   */     
            $query_sku = $this->db->query("SELECT product_id  FROM product_inventory  WHERE product_sku LIKE '%".$product_sku."%' ORDER BY id DESC");
            $result_sku = $query_sku->result_array();              
           
            $product_id = $result_sku[0]['product_id'];
         }

        if($store_id!='' && $product_id!=''){            
                $cond = "pd.`brand_id`= '".$store_id."' and pd.id ='".$product_id."' or pi.product_sku ='".$product_id."' and pd.is_delete=0 ";
                $limit_cond='';
            /*if (is_numeric($product_id)) {
                $cond = "pd.`brand_id`= '".$store_id."' and pd.id ='".$product_id."' and pd.is_delete=0 ";
                $limit_cond='';
            }else{
                $cond = "pd.`brand_id`= '".$store_id."' and pi.product_sku ='".$product_id."'  and pd.is_delete=0 ";
                $limit_cond='';
            }*/            
            //exit;            
        }else if($store_id!='' && $product_id==''){
            $cond = "pd.`brand_id`= '".$store_id."' and pd.is_delete=0 ";
            $limit_cond =" limit ".$min_offset.",".$max_offset;
        }else if($store_id=='' && $product_id!=''){
            $cond = "pd.id = '".$product_id."'  and pd.is_delete=0";
            $limit_cond='';
        }else{
            $cond = ' pd.is_delete=0 ';
                      
        }
        
        if($min_offset!='' && $max_offset!='')
        {
           $limit_cond =" limit ".$min_offset.",".$max_offset;
            
        }else{
            $limit_cond =" ";
        }  
       
        $query = $this->db->query("SELECT pi.`id`, pd.`id` as product_id, GROUP_CONCAT(pi.`size_id`) as size_id , GROUP_CONCAT(ps.`size_text`) as size_text,  GROUP_CONCAT(pi.`stock_count`) as stock_count ,GROUP_CONCAT(pi.`product_sku`) as product_sku,  pd.`name`,pd.`brand_id`,pd.`price`,pd.`compare_price`,pd.`discount_start_date`,pd.`discount_end_date`,pd.status FROM product_desc as pd LEFT OUTER JOIN product_inventory as pi  ON pd.`id` = pi.`product_id` LEFT OUTER Join `product_size` as ps ON ps.`id` = pi.`size_id` WHERE ".$cond." group by pd.`id`, pd.`brand_id` order by pd.`id` ".$limit_cond );  
        $res = $query->result_array(); 
      	//echo $this->db->last_query(); 
        return $res; 
    }

    function get_products_count($store_id,$product_id){

        if($store_id!='' && $product_id!=''){
            $cond = " and pd.`brand_id`= '".$store_id."' and pd.id='".$product_id."'";
        }else if($store_id!='' && $product_id==''){
            $cond = " and pd.`brand_id`= '".$store_id."'";
        }else if($store_id=='' && $product_id!=''){
            $cond = " and pd.id='".$product_id."'";
        }else{
            $cond = '';
        }

       /* $query = $this->db->query("SELECT count(pi.`id`) as product_id FROM product_desc as pd LEFT OUTER JOIN product_inventory as pi  ON pd.`id` = pi.`product_id` LEFT OUTER Join `product_size` as ps ON ps.`id` = pi.`size_id` WHERE ".$cond." group by pd.`id`, pd.`brand_id` order by pd.`id` desc");  */

        $query = $this->db->query("SELECT count(pd.`id`) as product_id from product_desc as pd where 1 ".$cond);

        $res = $query->result_array();  
        return $res['0']['product_id']; 
    }

    function save_product_size($product_id,$product_info){
        
        $this->db->where('product_id', $product_id);
        $this->db->delete('product_inventory'); 

        if(!empty($product_info)){
            foreach($product_info as $val){
                $size = $val[0]; $qty = $val[1]; 
                $psku  = $val[2]; 
                $size_id = $this->get_size_id($size);
                
                if($size_id>0 && $product_id>0){
                    $data = array(
                        'product_id'=>$product_id,
                        'size_id'=>$size_id,
                        'stock_count'=>$qty,
                        'product_sku'=> $psku
                    );
                    $this->db->insert('product_inventory', $data); 
                }
            }
        }
    }

    function get_size_id($size){
        if($size!=''){
            $query = $this->db->query("select count(*) as cnt,id from product_size where size_text = '".strtoupper($size)."'");
            $result = $query->result_array();
            if($result[0]['cnt'] == 0){
                $data = array(
                    'size_text' => strtoupper($size) ,
                );
                $this->db->insert('product_size', $data); 
                return $this->db->insert_id();
            }else {
                return $result[0]['id']; 
            }
        }
    }


 } 
?>
