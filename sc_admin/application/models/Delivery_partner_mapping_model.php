<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Delivery_partner_mapping_model extends MY_Model {
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	

	public function getDelPartners()
	{	
		$query = $this->db->query("select id,delivery_partner_name,user_id from delivery_partner where status = 1 order by created_datetime desc ");
		$res = $query->result_array();				
		return $res;
	}


	 public function get_paid_stores($search_by,$search_value)
    {
    	$search_cond = "";
    	if($search_by!='' && $search_value!='')
    	{
    		if($search_by==1)
    		{
    			$search_cond = "AND  company_name like '%".$search_value."%'";
    		}
    	}
        if($this->session->userdata('role_id') == 6){
            $brand_id = $this->session->userdata('user_id');
            $query = $this->db->query("SELECT user_id as brand_id, company_name FROM brand_info  WHERE is_paid = 1 and user_id=".$brand_id."  ".$search_cond." ORDER BY id DESC");
        }else{
            $query = $this->db->query("SELECT user_id as brand_id, company_name FROM brand_info  WHERE is_paid = 1  ".$search_cond." ORDER BY id DESC");
        }
        $result = $query->result_array();
        return $result;
    }

    public function get_alldpmIds()
    { 	
    	$query = $this->db->query("select dpm.brand_id,	dpm.dp_id,b.company_name from brand_delivery_partner_mapping as dpm, brand_info as b where dpm.brand_id = b.user_id ");    	

		$result = $query->result_array();
		return $result;		
    }

    public function dp_save($dpids_str,$brand_id)
	{	
		if($brand_id>0)
		{
			$this->db->from('brand_delivery_partner_mapping');
			$this->db->where('brand_id',$brand_id);
			$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				if($dpids_str>0 && $brand_id>0)
				{
					$data = array(
						'brand_id'=>$brand_id,
						'dp_id'=>$dpids_str,
						'modified_by' =>$this->session->userdata('user_id'),
						'created_datetime' => date('Y-m-d H:i:s')
						);
					$this->db->where('brand_id', $brand_id);
 					$updated = $this->db->update('brand_delivery_partner_mapping' ,$data);
 				}else
 				{
 					$this->db->where('brand_id', $brand_id);
      				$updated = $this->db->delete('brand_delivery_partner_mapping'); 
 				}
 				if($updated)
 				{
 					return True;
 				}else
 				{
 					return False;
 				}
			}else
			{				
				if($dpids_str!='' && $brand_id!='')
				{
					$data = array(
						'brand_id'=>$brand_id,
						'dp_id'=>$dpids_str,
						'created_by' =>$this->session->userdata('user_id'),
						'created_datetime' => date('Y-m-d H:i:s')
						);
					$this->db->insert('brand_delivery_partner_mapping', $data);
					$f_id = $this->db->insert_id();						
					return $f_id;		
				}	
			}
		}
		
	}
	
/*---------End of Common functionalities-------*/  
}

