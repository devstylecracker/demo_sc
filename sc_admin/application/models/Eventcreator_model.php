<?php
class Eventcreator_model extends MY_Model {

    
    public $ouptut = array();
    public $depth = 0; public $children = 0;
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
    }
	
	public function event_pic($img_name,$user_id)
	{	
		$data = array('object_name'=>'event_img','object_description'=>'event_img','object_type'=>'event_img','object_created'=>date('Y-m-d H:i:s'),'object_creator'=>$user_id,'object_parent'=>'0','object_status'=>'1','object_image'=>$img_name);
		$this->db->insert('object', $data);
		return true;	
	}
	
	public function get_event_pic($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL)
	{	
		
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}		
		$query = $this->db->query("select object_id as id,object_image as img_name from object where object_status = 1 order by id desc ".$limit_cond);
		return $query->result_array();
		
	}
	
	public function delete_event_pic($id,$user_id){
		$data = array(
               'object_status' => 0,
               'object_modifier' =>$user_id
            );

		$this->db->where('object_id', $id);
		$this->db->update('object', $data); 
		return true;
	}
	
	public function get_event_image($img_id){	
	
		$query = $this->db->query("select object_id as id,object_image as img_name from object where object_status = 1 AND object_id = ".$img_id." order by id desc ");
		$result = $query->result_array();
		if(!empty($result)){ return $result[0]['img_name']; }else return '';
	}
	
	function save_object_data($object_data,$object_meta_data,$type=null){
			if($type == 'event'){
				$this->db->insert('object',array('object_name'=>$object_data['event_name'],'object_slug'=>$object_data['event_slug'],'object_description'=>'event_name','object_type'=>'event','object_created'=>date('Y-m-d H:i:s'),'object_creator'=> $this->session->userdata('user_id'),'object_modifier'=> $this->session->userdata('user_id'),'object_image'=>$object_data['event_img']));
			}else if($type == 'collection'){
				$this->db->insert('object',array('object_name'=>$object_data['collection_name'],'object_slug'=>$object_data['collection_slug'],'object_description'=>'collection_name','object_type'=>'collection','object_created'=>date('Y-m-d H:i:s'),'object_creator'=> $this->session->userdata('user_id'),'object_modifier'=> $this->session->userdata('user_id'),'object_image'=>$object_data['collection_img']));
			}
			
			$object_id = $this->db->insert_id();
			$object_meta_keys = array_keys($object_meta_data);$i=0;
			foreach($object_meta_data as $val){
				$this->db->insert('object_meta',array('object_id'=>$object_id,'object_meta_key'=>$object_meta_keys[$i],'object_meta_value'=>$val,'created_datetime'=>date('Y-m-d H:i:s'),'created_by'=> $this->session->userdata('user_id'),'modified_by'=> $this->session->userdata('user_id')));
				$i++;
			}
			return $object_id;
	}
	
	function get_all_values($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL,$type=NULL){

		if($search_by == '1'){ $search_cond = " AND a.object_id = '".strip_tags(trim($table_search))."'"; }
		else if($search_by == '2'){ $search_cond = " AND a.object_name LIKE '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '3'){ 
			$table_search = trim(strtolower($table_search));
			if($table_search == 'broadcast'){
				$search_cond = " AND a.object_status = '3' ";
			}else if($table_search == 'pending'){
				$search_cond = " AND a.object_status != '3' ";
			}
		}
        else{ $search_cond =''; }


		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
                                $limit_cond = " LIMIT ".$page.",".$per_page;
                        }else{
                                $limit_cond = '';
                        }

		$query = $this->db->query("select a.object_id as id,a.object_name as name,a.object_image as image,a.object_created as created_datetime,a.object_modified as modified_datetime,b.object_meta_key,b.object_meta_value,a.object_status as status  from object as a,object_meta as b where a.object_id = b.object_id AND a.object_type = '".$type."' AND b.object_status != 0 ".$search_cond." GROUP BY a.object_id order by a.object_id desc ".$limit_cond);
		$result = $query->result_array();
		return $result;
	}
	
	function get_event($object_id,$type=null){
		$product_array = array();
		$query = $this->db->query("select a.object_id as id,a.object_name as name,a.object_slug as obj_slug,a.object_image as image,a.object_created as created_datetime,a.object_modified as modified_datetime,b.object_meta_key,b.object_meta_value  from object as a,object_meta as b where a.object_id = b.object_id AND b.object_status != 0 AND b.object_id = ".$object_id." order by a.object_id desc ");
		$result = $query->result_array();
		$product_array['object_id'] = @$result[0]['id'];
		$product_array['object_name'] = @$result[0]['name'];
		$product_array['object_image'] = @$result[0]['image'];
		$product_array['obj_slug'] = @$result[0]['obj_slug'];
		foreach($result as $val){
			$product_array[$val['object_meta_key']] = $val['object_meta_value'];
			if($val['object_meta_key'] == 'event_products' || $val['object_meta_key'] == 'collection_products'){
				$products = unserialize($val['object_meta_value']);
				$product_array[$val['object_meta_key']] = $this->get_products($products);
			} 
		}
		// echo "<pre>";print_r($product_array);exit;
		return $product_array;
		
	}
	
	function get_products($product_ids){
		$product_ids = implode(",",$product_ids);
		$query = $this->db->query("select distinct a.id as product_id,a.name,a.image,a.url,a.price,a.brand_id from product_desc as a where a.`id`>='3598' and a.approve_reject IN ('A','D') AND a.id IN(".$product_ids.") order by a.id desc ");
		$result = $query->result_array();
		
		$product_array = array();$i=0;$imagify_brandid = unserialize(IMAGIFY_BRANDID); 
		foreach($result as $val){
			$product_array[$i]['product_id'] = $val['product_id'];
			$product_array[$i]['name'] = $val['name'];
			$product_array[$i]['url'] = $val['url'];
			$product_array[$i]['price'] = $val['price'];
			// added for new product url  
			$productUrl = '';
			if(in_array($val['brand_id'],$imagify_brandid)){
				$productUrl = $this->config->item('products_tiny').$val['image'];
            }else{
				$productUrl =  base_url().'assets/products/thumb_124/'.$val['image'];                
            }
			// added for new product url
			$product_array[$i]['image'] = $productUrl;
			$i++;
		}
		
		return $product_array;
	}
	
	function delete_object($id){
		
			$data = array('object_status'=>0,"modified_by"=>$this->session->userdata('user_id'));
			$this->db->where('object_id', $id);
			$this->db->update('object_meta', $data);
			return true;
	}
	
	function update_object($object_data,$object_meta_data,$object_id){
		
		if(@$object_data['object_img'] != ''){
			
			$data = array('object_name'=>$object_data['object_name'],'object_created'=>date('Y-m-d H:i:s'),'object_creator'=> $this->session->userdata('user_id'),'object_modifier'=> $this->session->userdata('user_id'),'object_image'=>$object_data['object_img']);
			$this->db->update('object', $data, array('object_id' => $object_id ));
		}else if(@$object_data['object_name'] != ''){
			
			$data = array('object_name'=>$object_data['object_name'],'object_created'=>date('Y-m-d H:i:s'),'object_creator'=> $this->session->userdata('user_id'),'object_modifier'=> $this->session->userdata('user_id'));
			$this->db->update('object', $data, array('object_id' => $object_id ));
		}
		
		$object_meta_keys = array_keys($object_meta_data);$i=0;
		// echo "<pre>";print_r($object_meta_data);exit;
		foreach($object_meta_data as $val){
			if($this->key_exist($object_meta_keys[$i],$object_id) > 0){
				$data = array('object_meta_key'=>$object_meta_keys[$i],'object_meta_value'=>$val,'created_datetime'=>date('Y-m-d H:i:s'),'created_by'=> $this->session->userdata('user_id'),'modified_by'=> $this->session->userdata('user_id'));
				// echo $this->db->last_query();
				$this->db->update('object_meta', $data, array('object_id' => $object_id	,'object_meta_key'=>$object_meta_keys[$i]));
				if($object_meta_keys[$i] == 'pa_body_shape' && $val != ''){
					$data = array('object_status'=>'3');
					$this->db->update('object', $data, array('object_id' => $object_id ));
				}
			}else{
				
				$this->db->insert('object_meta',array('object_id'=>$object_id,'object_meta_key'=>$object_meta_keys[$i],'object_meta_value'=>$val,'created_datetime'=>date('Y-m-d H:i:s'),'created_by'=> $this->session->userdata('user_id'),'modified_by'=> $this->session->userdata('user_id')));
			}	
			
			$i++;
		}
		return true;
	}
	
	function key_exist($object_meta_key,$object_id){
		
		$query = $this->db->query('select * from object_meta where object_id = '.$object_id.' AND object_meta_key = \''.$object_meta_key.'\' ');
		$result = $query->result_array();
		return count($result);
		
	}
	
	function get_bucket_ids($body_shape,$pref1){
		
		$query = $this->db->query('select id from bucket where body_shape = '.$body_shape.' AND style = '.$pref1.' ');
		$result = $query->result_array();
		
		if(!empty($result)){
			return trim($result[0]['id']);
		}else return 0;
		
	}
	
}