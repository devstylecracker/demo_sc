<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Collection_img_model extends MY_Model {
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	public function collection_pic($img_name,$user_id)
	{	
		$data = array('object_name'=>'collection_img','object_slug'=>'collection_img','object_description'=>'collection_img','object_type'=>'collection_img','object_created'=>date('Y-m-d H:i:s'),'object_creator'=>$user_id,'object_parent'=>'0','object_status'=>'1','object_image'=>$img_name);
		$this->db->insert('object', $data);
		return true;	
	}
	
	public function get_collection_pic($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL)
	{	
		
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}		
		$query = $this->db->query("select object_id as id,object_image as img_name from object where object_status = 1 order by id desc ".$limit_cond);
		return $query->result_array();
		
	}
	
	public function delete_collection_pic($id,$user_id){
		$data = array(
               'object_status' => 0,
               'object_modifier' =>$user_id
            );

		$this->db->where('object_id', $id);
		$this->db->update('object', $data); 
		return true;
	}
	
	
		
/*---------End of Common functionalities-------*/  
}

