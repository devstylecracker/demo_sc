<?php
class Brand_details_info_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function product_uploaded_info($brand_id,$date_from,$date_to){

    	if($date_from!='' && $date_to!='' && $date_from!=$date_to)
		{
			$query = $this->db->query("select id from product_desc where `brand_id` = '".$brand_id."' and created_datetime between '".$date_from."' and '".$date_to."'");
		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
			$query = $this->db->query("select id from product_desc where `brand_id` = '".$brand_id."' and created_datetime like '%".date_format(date_create($date_from),'Y-m-d')."%'");
		}else{
			$query = $this->db->query("select id from product_desc where `brand_id` = '".$brand_id."' and created_datetime like '%".date('Y-m-d')."%' ");
		}

		$res = $query->result_array();		
		return $res;
    }

    function product_used_info($brand_id,$date_from,$date_to){
    	if($date_from!='' && $date_to!='' && $date_from!=$date_to)
		{
			$query = $this->db->query("SELECT distinct c.id as id  from look as a,`look_products` as b,product_desc  as c where a.`id` = b.`look_id` and b.`product_id` = c.`id` and c.`brand_id` =  '".$brand_id."' and a.`status` = 1 and a.`modified_datetime` between '".$date_from."' and '".$date_to."'");
			
		}else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
			$query = $this->db->query("SELECT distinct c.id as id from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and c.`brand_id` =  '".$brand_id."' and a.`status` = 1 and a.`modified_datetime` like '%".date_format(date_create($date_from),'Y-m-d')."%'  ");

		}else{
			
			$query = $this->db->query("SELECT distinct c.id as id from look as a,`look_products` as b,product_desc  as c where a.id = b.`look_id` and b.`product_id` = c.`id` and c.`brand_id` =  '".$brand_id."' and a.`status` = 1 and a.`modified_datetime` like '%".date('Y-m-d')."%'  ");
		}
		
		$res = $query->result_array();	

		return $res;
    }

    function product_click_info($brand_id,$date_from,$date_to){
    	if($date_from!='' && $date_to!='' && $date_from!=$date_to)
		{
        $query = $this->db->query("SELECT b.id,sum(a.`click_count`) as count,c.cpc_percentage as cpc_percentage FROM `users_product_click_track` as a,product_desc as b,brand_info as c  where a.created_datetime between '".$date_from."' and '".$date_to."' and b.`brand_id`='".$brand_id."' and a.product_id = b.`id` and b.`brand_id` = c.user_id group by b.id");
        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
        $query = $this->db->query("SELECT b.id,sum(a.`click_count`) as count,c.cpc_percentage as cpc_percentage FROM `users_product_click_track` as a,product_desc as b,brand_info as c  where a.product_id = b.`id` and b.`brand_id` = c.user_id and b.`brand_id`='".$brand_id."' and a.`created_datetime` like '%".date_format(date_create($date_from),'Y-m-d')."%' group by b.id");
        }else{
        	$query = $this->db->query("SELECT b.id,sum(a.`click_count`) as count,c.cpc_percentage as cpc_percentage FROM `users_product_click_track` as a,product_desc as b,brand_info as c  where a.product_id = b.`id` and b.`brand_id` = c.user_id and b.`brand_id`='".$brand_id."'and  a.`created_datetime` like '%".date('Y-m-d')."%' group by b.id");
        }
        $res = $query->result_array(); 
        return $res;
    }

    function product_cpa_info($brand_id,$date_from,$date_to){
    	if($date_from!='' && $date_to!='' && $date_from!=$date_to)
		{
       $query = $this->db->query("select a.id as id,count(a.id) as count from product_desc as a,`product_cpa_track` 
as b where a.id = b. product_id and a.brand_id = '".$brand_id."' and b.created_datetime between '".$date_from."' and '".$date_to."'");

        }else if(($date_from!='' && $date_to!='' && $date_from==$date_to) || ($date_from!='' || $date_to!=''))
		{
        $query = $this->db->query("select a.id as id,count(a.id) as count from product_desc as a,`product_cpa_track` 
as b where a.id = b. product_id and a.brand_id = '".$brand_id."' and b.`created_datetime` like '%".date_format(date_create($date_from),'Y-m-d')."%'");

        }else{
        	$query = $this->db->query("select a.id as id,count(a.id) as count from product_desc as a,`product_cpa_track` 
as b where a.id = b. product_id and a.brand_id = '".$brand_id."' and b.`created_datetime` like '%".date('Y-m-d')."%'");
        }
 	
        $res = $query->result_array();
        return $res;

    }

    function brand_name($id){
    	$query = $this->db->query("SELECT company_name from brand_info where user_id = '".$id."'");
    	$res = $query->result_array();	

		return $res[0]['company_name'];
    }
}
?>