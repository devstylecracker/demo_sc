<?php
class Category_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function add_new_category($data)
	{		
		if(!empty($data))
		{			
			if($this->db->insert('database_category', $data['category_info']))
			{
				$contact_id = $this->db->insert_id();
				return true;	 	
			}else
			{
				return false;
			}			
		}
		
	}
	
	function get_category($search_by,$table_search,$page,$per_page)
	{
		$search_cond = '';
		if($search_by == '1'){ $search_cond = " AND category_name like '%".strip_tags(trim($table_search))."%'"; }
		
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0)
		{
            $page = $page*$per_page-$per_page;

        }else{ $page = 0;}
			$limit_cond = "LIMIT ".$page.",".$per_page;
		}else{
			$limit_cond = '';	
		}
		
		$query = $this->db->query("select * from database_category where 1=1 ".$search_cond." order by id desc ".$limit_cond); 

		$res = $query->result_array();
		
		return $res;
		
	}

	function get_contacttype(){
			//$query = $this->db->get_where('databases_contact_type', array('is_delete' => 0));
			$query = $this->db->query("select distinct(contacttype) as contact_type  from databases_contacts ");	
			//print_r($query->result());
			return $query->result();
		}
	
	function get_category_info($contact_id=NULL)
	{		
		$query = $this->db->get_where('database_category', array('id' => $contact_id));
		return $query->result_array();
	}
  

  	function edit_category($contact_info,$contact_id){
		
			$this->db->where('id', $contact_id);

			if($this->db->update('database_category', $contact_info))
			{
				return true;
			}else
			{
				return false;
			}
			
			
		}

 
	function delete_user($contact_id,$user_id,$data_ext_id){		
		if($contact_id){
			if($data_ext_id ==0 ){ $del_info = 1; }else{ $del_info = 0; }
			$data_post = array('is_delete'=>$del_info,"modified_by"=>$user_id,"modified_datetime"=>date('Y-m-d H:i:s'));
			$this->db->where('id', $contact_id);
			if($this->db->update('database_category', $data_post))
			{
				return true;
			}else
			{
				return false;
			}
		}
	
	}

 } 
?>
