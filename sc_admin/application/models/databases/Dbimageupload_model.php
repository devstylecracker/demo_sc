<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dbimageupload_model extends MY_Model {
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


    public function get_dbCategories()
	{			
		$query = $this->db->query('select * from database_category where is_delete=0 order by id desc ');
		return $query->result_array();		
	}

	public function get_dbSeason()
	{			
		$query = $this->db->query('select id,season_name from databases_season  order by id desc ');
		return $query->result_array();		
	}
	
	public function get_dbdesigner()
	{			
		$query = $this->db->query('select id,contact_name from databases_contacts where contacttype = "designer" order by id desc ');
		return $query->result_array();		
	}


	public function get_dbCategoriesbyname($name)
	{			
		$query = $this->db->query("select * from database_category where category_name = '".$name."' order by id desc ");
		return $query->result_array();		
	}

	
	public function insert_dbcat_data($data)
	{
		if($this->db->insert('database_category_data', $data['data']))
		{
			$db_cat_id = $this->db->insert_id();

			return $db_cat_id;

		}else
		{
			return false;	
		}
	}

	public function insert_image_data($db_cat_id,$data)
	{	
		if($db_cat_id)
		{
			$data['data_images']['db_catdata_id'] = $db_cat_id;
			if($this->db->insert('databases_category_images', $data['data_images']))
			{
				return true;	

			}else{

				return false;
			}
		}else
		{
			return false;
		}
	}
	
	public function get_database_pic($cat_id=NULL,$search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL)
	{	
		
		if($cat_id!="")
		{
			$cond = "AND cat_id = ".$cat_id;
		}else
		{
			$cond = "AND 1=1";
		}
		
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}

		$query = $this->db->query('select dci.id, dci.db_catdata_id, dci.image_name from databases_category_images as dci inner join database_category_data dcd  on dci.db_catdata_id = dcd.id   where dci.is_delete = 0 '.$cond.' order by id desc '.$limit_cond);
		return $query->result_array();
		
	}
	
	public function delete_database_pic($id,$user_id){
		$data = array(
               'is_delete' => 1,
               'modified_by' =>$user_id
            );

		$this->db->where('id', $id);
		$this->db->update('databases_category_images', $data); 
		return true;
		}
	

	public function get_database_img($data=NULL,$page=NULL,$per_page=NULL)
	{	
		
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}		

			$search_cond = '';
	
				    
				    	
				    

		if(!empty($data)){ 
			
			if($data['image_categories']!=''){
				$search_cond = $search_cond.' and dcd.`category_id` ="'.$data['image_categories'].'"';
			}
			if($data['season']!=''){
				$search_cond = $search_cond.' and dcd.`season` ="'.$data['season'].'"';
			}
			if($data['year']!=''){
				$search_cond = $search_cond.' and dcd.`year` ="'.$data['year'].'"';
			}
			if($data['trendname']!=''){
				$search_cond = $search_cond.' and dcd.`trend_name` ="'.$data['trendname'].'"';
			}
			if($data['eventname']!=''){
				$search_cond = $search_cond.' and dcd.`event_name` ="'.$data['eventname'].'"';
			}
			if($data['lookbooks_prod']!=''){
				//$search_cond = $search_cond.' and ds.`` ="'.$data['lookbooks_prod'].'"';
			}
			if($data['designer']!=''){
				$search_cond = $search_cond.' and dcd.`designer` ="'.$data['designer'].'"';
			}
			if($data['ref_style']!=''){
				$search_cond = $search_cond.' and dcd.`reference` ="'.$data['ref_style'].'"';
			}
			if($data['style']!=''){
				$search_cond = $search_cond.' and dcd.`personality_style` ="'.$data['style'].'"';
			}
			if($data['tag_search']!=''){
				$search_cond = $search_cond.' and dcd.`tags` ="'.$data['tag_search'].'"';
			}



		}
		$query = $this->db->query('select dci.id, dci.db_catdata_id, dci.image_name from databases_category_images as dci inner join database_category_data dcd  on dci.db_catdata_id = dcd.id   where dci.is_delete = 0 '.$search_cond.' order by dci.id desc '.$limit_cond);

		//echo 'select dci.id, dci.db_catdata_id, dci.image_name from databases_category_images as dci inner join database_category_data dcd  on dci.db_catdata_id = dcd.id   where dci.is_delete = 0 '.$search_cond.' order by id desc '.$limit_cond;
		return $query->result_array();
		
	}
	
	public function get_year(){
		$query = $this->db->query("select distinct year from database_category_data ");
		return $query->result_array();	
	}
	
		
/*---------End of Common functionalities-------*/  
}

