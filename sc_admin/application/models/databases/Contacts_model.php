<?php
class Contacts_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function add_new_contact($contact_info)
	{		
		if(!empty($contact_info))
		{			
			if($this->db->insert('databases_contacts', $contact_info))
			{
				$contact_id = $this->db->insert_id();
				return true;	 	
			}else
			{
				return false;
			}			
		}
		
	}
	
	function get_contact($search_by,$table_search,$page,$per_page)
	{

		if($search_by == '1'){ $search_cond = " AND contact_name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '2'){ $search_cond = " AND email like '%".strip_tags(trim($table_search))."%' OR altemail like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '3'){ 
		$search_cond = " AND mobile_one like '%".strip_tags(trim($table_search))."%' OR mobile_two like '%".strip_tags(trim($table_search))."%'";
		}else if($search_by == '4'){ 
		$search_cond = " AND phone_one like '%".strip_tags(trim($table_search))."%' OR phone_two like '%".strip_tags(trim($table_search))."%'";
		}else if($search_by == '5'){ 
		$search_cond = " AND contacttype like '%".strip_tags(trim($table_search))."%'";
		}else{ $search_cond =' AND 1=1'; }

		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0)
		{
            $page = $page*$per_page-$per_page;

        }else{ $page = 0;}
			$limit_cond = "LIMIT ".$page.",".$per_page;
		}else{
			$limit_cond = '';	
		}
		
		$query = $this->db->query("select * from databases_contacts where is_delete = 0 ".$search_cond." order by id desc ".$limit_cond); 

		$res = $query->result_array();
		
		return $res;
		
	}

	function get_contacttype(){
			//$query = $this->db->get_where('databases_contact_type', array('is_delete' => 0));
			$query = $this->db->query("select distinct(contacttype) as contact_type  from databases_contacts ");	
			//print_r($query->result());
			return $query->result();
		}
	
	function get_contact_info($contact_id=NULL)
	{		
		$query = $this->db->get_where('databases_contacts', array('id' => $contact_id));
		return $query->result_array();
	}
  

  	function edit_contact($contact_info,$contact_id){
		
			$this->db->where('id', $contact_id);

			if($this->db->update('databases_contacts', $contact_info))
			{
				return true;
			}else
			{
				return false;
			}
			
			
		}

 
	function delete_contact($contact_id,$user_id){		
		if($contact_id){
			$data_post = array('is_delete'=>1,"modified_by"=>$user_id,"modified_datetime"=>date('Y-m-d H:i:s'));
			$this->db->where('id', $contact_id);
			if($this->db->update('databases_contacts', $data_post))
			{
				return true;
			}else
			{
				return false;
			}
		}
	
	}

 } 
?>
