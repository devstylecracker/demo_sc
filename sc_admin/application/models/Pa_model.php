<?php
class Pa_model extends MY_Model {

    
    public $ouptut = array();
    public $depth = 0; public $children = 0;
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
    }
    
    function get_question_ans($gender,$question_id){
		return 1;
	}
	
	function load_questAns($id){
		$query = $this->db->query("SELECT A.id, A.answer FROM answers AS A INNER JOIN question_answer_assoc AS Q ON Q.answer_id=A.id AND (Q.question_id = ".$id.") ORDER BY A.answer");
		$query_result[0] = 'Select';
		foreach ($query->result() as $row){
			$query_result[$row->id] = $row->answer;
		}
		return($query_result);
	}
	
	function get_answer($answer_ids,$question){
		if(!empty($answer_ids)){
			$query = $this->db->query("SELECT a.id, a.answer FROM answers as a where a.id IN (".$answer_ids.") ");
			$res = $query->result_array();
			$pa_html = '<label>'.$question.'</label> : ';$i=1;
			foreach($res as $val){
				if($i==1){
					$pa_html = $pa_html. $val['answer'];
				}else {
					$pa_html = $pa_html.' , '.$val['answer'];
				}
				$i++;
			}
			return $pa_html;
		}else return null;
	}
	
	function get_size_data($type){
        $filter='';       
        if(!empty($type))
        {
            if($type=='womentopsize')
            {
                $filter = '('.SCBOX_WOMENTOP_LIST.')';
            }else if($type=='womenbottomsize')
            {
                $filter = '('.SCBOX_WOMENBOTTOM_LIST.')';
            }else if($type=='womenfootsize')
            {
                $filter = '('.SCBOX_WOMENFOOT_LIST.')';
            }else if($type=='womenbandsize')
            {
                $filter = '('.SCBOX_BANDSIZE_LIST.')';
            }else if($type=='womencupsize')
            {
                $filter = '('.SCBOX_CUPSIZE_LIST.')';
            }else if($type=='mentopsize')
            {
                $filter = '('.SCBOX_MENTOP_LIST.')';
            }else if($type=='menbottomsize')
            {
                $filter = '('.SCBOX_MENBOTTOM_LIST.')';
            }else if($type=='menfootsize')
            {
                $filter = '('.SCBOX_MENFOOT_LIST.')';
            }
			
            $query = $this->db->query('select a.id,a.size_text as answer from product_size as a where a.size_text IN '.$filter.' order by a.order ');            
            $result = $query->result_array();
            return $result;
        }
    }

     function get_order_cartid($orderid)
    {
        $query = $this->db->get_where('cart_info', array('order_id' => $orderid ));
        $result = $query->result_array();
        return $result;
    }

    function user_pa_data($type,$category,$attribute,$cartid)
    {
        
        if(!empty($category))
        {
            $this->db->where('object_id', $cartid);
            $this->db->where('object_type', 'order');
            $this->db->delete('category_object_relationship');
            $this->db->insert_batch('category_object_relationship',$category);
        }

        if(!empty($attribute))
        {
            $this->db->where('object_id', $cartid);
            $this->db->where('object_type', 'order');
            $this->db->delete('attribute_object_relationship');
            $this->db->insert_batch('attribute_object_relationship',$attribute);
        }

    }
}