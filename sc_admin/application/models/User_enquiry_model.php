<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Enquiry_model extends MY_Model {
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }	
	/*Open Get and Search All Data*/
	public function get_all_User_enquiry($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL){
		
		if($search_by == '1'){ $search_cond = " AND name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '2'){ $search_cond = " AND email like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '3'){ $search_cond = " AND contact_no like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '4'){ $search_cond = " AND message like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '5'){ $search_cond = " AND enquiry_date_time like '%".strip_tags(trim($table_search))."%'"; }	
		else if($search_by == '6'){
				$date_from = $this->input->post('date_from');
				$date_to = $this->input->post('date_to');
				if($date_from=='' && $date_to == '' )
				{
				$tb_arr = explode(',',$table_search);
				$date_from = $tb_arr[0];
				$date_to = $tb_arr[1];
				}

				$search_cond = "AND `enquiry_date_time` >= '".$date_from." 00:00:00'  and `enquiry_date_time` <= '".$date_to." 23:59:59'";

				}		
		else{ $search_cond ='AND 1=1'; }		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}				
		$query = $this->db->query("select id, name, email, contact_no, message, enquiry_date_time, subject, created_datetime from user_enquiry where 1=1 " .$search_cond." order by id desc ".$limit_cond);				
		$res = $query->result();				
		return $res;
	}
	/*End Get and Search All Data*/
		
/*---------End of Common functionalities-------*/  
}

