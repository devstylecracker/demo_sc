<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attrsettings_model extends MY_Model {
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }	

    function save_attribues($data){
    	if(!empty($data)){
            $cat_data = array();
    		$attr_value_name = $this->input->post('attr_value_name');
    		$attr_value_slug = $this->input->post('attr_value_slug');
    		$attr_value_content = $this->input->post('attr_value_content');
            $id = $this->input->post('attribute_id');
             $display = $this->input->post('display');
    		

    		if($attr_value_name!=''){
                $cat_data['attr_value_name'] = $attr_value_name;
                $cat_data['attribute_id'] = $id;
                $cat_data['count'] = 0;
                $cat_data['attr_value_content'] = $attr_value_content;
                $cat_data['attr_value_slug'] = strtolower($attr_value_slug);
                $cat_data['created_datetime'] = date('Y-m-d H:i:s');
                $cat_data['display'] = $display;
    			$cat_id = $this->add_category($cat_data);
                
    		}
    	}
    }

    function add_category($data){
        if(!empty($data)){
            $this->db->insert('attribute_values_relationship', $data); 
            return $this->db->insert_id(); 
        }else{
            return 0;
        }
    }

    function get_all_categories($cat_id = '-1'){
        $this->db->from('category');
        $this->db->order_by("category_parent", "asc");
        $this->db->where('category_parent',$cat_id);
        $query = $this->db->get(); 
        return $query->result_array();
    }

    function get_attr_value($cat_id){
        $this->db->from('attribute_values_relationship');
        $this->db->where('attribute_id',$cat_id);
        $query = $this->db->get(); 
        return $query->result_array();
    }

    function get_attr_value_data($cat_id){
        $this->db->from('attribute_values_relationship');
        $this->db->where('id',$cat_id);
        $query = $this->db->get(); 
        return $query->result_array();
    }

    function get_attributes($cat_id){
        $this->db->from('attribute_values_relationship');
        $this->db->where('id',$cat_id);
        $query = $this->db->get(); 
        return $query->result_array();
    }

    function get_filtered_category($search,$id){
        if($search!=''){
            $query = $this->db->query("SELECT `id`, `attribute_id`, `attr_value_name`, `attr_value_content`, `attr_value_slug`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `attribute_values_relationship` WHERE 1  and attr_value_name like '%".$search."%' and attribute_id = ".$id); 
            return $query->result_array();
        }
    }

    function get_cat_slug($cat_id=null){
        if($cat_id!='-1'){
            $this->db->from('attributes');
            $this->db->where('id',$cat_id);
            $query = $this->db->get(); 
            $res = $query->result_array(); 
            if(!empty($res)){
                return $res[0]['attribute_slug'];
            }
        }else{ return ''; }
    }

    function update_cat_image($cat_id=null){
        if($cat_id!=null && $cat_id >0){
            $data = array(
               'category_image' => $cat_id.'.jpg'
            );

            $this->db->where('id', $cat_id);
            $this->db->update('category', $data); 
        }
    }

    function edit_attribute($data,$cat_id=null){
        if($cat_id!=null && $cat_id >0){
            
            $data = array(
               'attr_value_name' => $data['attr_value_name'],
               'attr_value_content' => $data['attr_value_content'],
               'attr_value_slug' => $data['attr_value_slug'],
               'display' => $data['display'],
			   'attr_hashtag' => $data['attr_hashtag'],
            );

            $this->db->where('id', $cat_id);
            $this->db->update('attribute_values_relationship', $data); 

            
        }
    }

    function delete_category($id){
        if($id > 0){
            $this->db->delete('attribute_values_relationship', array('id' => $id)); 
            return 1;
        }else{
            return 2;
        }
    }

    function get_attr_info($id){
            $this->db->from('attributes');
            $this->db->where('id',$id);
            $query = $this->db->get(); 
            $res = $query->result_array(); 
            if(!empty($res)){
                return $res[0]['attribute_name'];
            }
    }

    function update_display($id,$display_val){
        $data = array(
               'display' => $display_val
        );
        $this->db->where('id', $id);
        $this->db->update('attribute_values_relationship', $data); 
    }

}
?>