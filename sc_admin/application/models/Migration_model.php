<?php
ini_set('memory_limit', '-1');
class Migration_model extends MY_Model {

function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

function category_migration1(){

	/* Truncate Table */
	$this->db->truncate('category');
	/* Level First */
	$query = $this->db->query("SELECT `id`, `name`, `slug`, `status`, `is_delete`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `product_category` WHERE 1 and is_delete = 0");
	$res = $query->result_array();
	if(!empty($res)){
	$data = array();
	foreach($res as $val){
	$cat_slug = strtolower($val['name']);
	$cat_slug = preg_replace("/[^a-zA-Z0-9]+/", "-", $cat_slug);
	$data[] = array(
	'category_name' => $val['name'],
	'category_content' => $val['name'],
	'category_image' => $val['id'].'.jpg',
	'category_parent' => '-1',
	'category_type' => 'product',
	'category_slug' => $cat_slug,
	'count' => 0,
	'created_by' => $val['created_by'],
	'modified_by' => $val['modified_by'],
	'created_datetime' =>$val['created_datetime'],
	'modified_datetime'=>$val['modified_datetime'],
	'old_id' => $val['id'],
	'level' => 1
	);
	}
	if(!empty($data)){
	if($this->db->insert_batch('category', $data)){ echo 'Done'; } else{ echo 'Not Done'; }
	}
	}

	/* Second Level*/
	$query = $this->db->query("SELECT `id`, `product_cat_id`, `name`, `slug`, `status`, `is_delete`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `product_sub_category` WHERE 1 and is_delete = 0");
	$res = $query->result_array();
	if(!empty($res)){
	$data = array();
	foreach($res as $val){
	$cat_slug = strtolower($val['name']);
	$cat_slug = preg_replace("/[^a-zA-Z0-9]+/", "-", $cat_slug);
	$parent_ids = $this->db->query("SELECT `id`,`old_id`,'level' FROM `category` WHERE old_id = '".$val['product_cat_id']."' and level = 1");
	$parent_id = $parent_ids->result_array();


	$data[] = array(
	'category_name' => $val['name'],
	'category_content' => $val['name'],
	'category_image' => $val['id'].'.jpg',
	'category_parent' => $parent_id[0]['id'],
	'category_type' => 'product',
	'category_slug' => $cat_slug,
	'count' => 0,
	'created_by' => $val['created_by'],
	'modified_by' => $val['modified_by'],
	'created_datetime' =>$val['created_datetime'],
	'modified_datetime'=>$val['modified_datetime'],
	'old_id' => $val['id'],
	'level' => 2
	);
	}
	if(!empty($data)){
	if($this->db->insert_batch('category', $data)){ echo 'Done'; } else{ echo 'Not Done'; }
	}
	}


	/* Third Level*/
	$query = $this->db->query("SELECT `id`, `prod_sub_cat_id`, `name`, `slug`, `status`, `is_delete`, `size_guide`, `show_in_filter`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `product_variation_type` WHERE 1 and is_delete = 0");
	$res = $query->result_array();
	if(!empty($res)){
	$data = array();
	foreach($res as $val){
	$cat_slug = strtolower($val['name']);
	$cat_slug = preg_replace("/[^a-zA-Z0-9]+/", "-", $cat_slug);
	$parent_ids = $this->db->query("SELECT `id`,`old_id`,'level' FROM `category` WHERE old_id = '".$val['prod_sub_cat_id']."' and level = 2");
	$parent_id = $parent_ids->result_array();

	if(isset($parent_id[0]['id'])){ $parent_id=$parent_id[0]['id']; }else{ $parent_id = 1; }

	$data[] = array(
	'category_name' => $val['name'],
	'category_content' => $val['name'],
	'category_image' => $val['id'].'.jpg',
	'category_parent' => $parent_id,
	'category_type' => 'product',
	'category_slug' => $cat_slug,
	'count' => 0,
	'created_by' => $val['created_by'],
	'modified_by' => $val['modified_by'],
	'created_datetime' =>$val['created_datetime'],
	'modified_datetime'=>$val['modified_datetime'],
	'old_id' => $val['id'],
	'level' => 3
	);
	}
	if(!empty($data)){
	if($this->db->insert_batch('category', $data)){ echo 'Done'; } else{ echo 'Not Done'; }
	}
	}

	/* Forth Level*/
	$query = $this->db->query("SELECT `id`, `product_variation_type_id`, `name`, `slug`, `status`, `is_delete`, `show_in_filter`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `product_variation_value` WHERE 1 and is_delete = 0");
	$res = $query->result_array();
	if(!empty($res)){
	$data = array();
	foreach($res as $val){
	$cat_slug = strtolower($val['name']);
	$cat_slug = preg_replace("/[^a-zA-Z0-9]+/", "-", $cat_slug);
	$parent_ids = $this->db->query("SELECT `id`,`old_id`,'level' FROM `category` WHERE old_id = '".$val['product_variation_type_id']."' and level = 3");
	$parent_id = $parent_ids->result_array();

	if(isset($parent_id[0]['id'])){ $parent_id=$parent_id[0]['id']; }else{ $parent_id = 1; }

	$data[] = array(
	'category_name' => $val['name'],
	'category_content' => $val['name'],
	'category_image' => $val['id'].'.jpg',
	'category_parent' => $parent_id,
	'category_type' => 'product',
	'category_slug' => $cat_slug,
	'count' => 0,
	'created_by' => $val['created_by'],
	'modified_by' => $val['modified_by'],
	'created_datetime' =>$val['created_datetime'],
	'modified_datetime'=>$val['modified_datetime'],
	'old_id' => $val['id'],
	'level' => 4
	);
	}
	if(!empty($data)){
	if($this->db->insert_batch('category', $data)){ echo 'Done'; } else{ echo 'Not Done'; }
	}
	}

}

function product_migration(){
	//$this->db->truncate('category_object_relationship');
	$query = $this->db->query("select id,product_cat_id,product_sub_cat_id from product_desc limit 500,500000000");
	$res = $query->result_array();
	$data = array();

	if(!empty($res)){
	foreach ($res as $value) {

	$product_id = $value['id'];
	$cat_id = $value['product_cat_id'];
	$sub_cat_id = $value['product_sub_cat_id'];

	$query1 = $this->db->query("select id from category where old_id='".$cat_id."' and level=1");
	$res1 = $query1->result_array();

	foreach($res1 as $val){
	$data[] = array(
	'object_id' => $product_id,
	'category_id' => $val['id'],
	'object_type' => 'product'
	);
	}
	
	$query11 = $this->db->query("select id from category where old_id='".$sub_cat_id."' and level=2");
	$res11 = $query11->result_array();

	foreach($res11 as $val1){
	$data[] = array(
	'object_id' => $product_id,
	'category_id' => $val1['id'],
	'object_type' => 'product'
	);
	}
	

	}
	}

	if(!empty($data)){
	if($this->db->insert_batch('category_object_relationship', $data)){ echo 'Done'; } else{ echo 'Not Done'; }
	}
}

function delete_products(){

	/* Required Category */
	$this->db->where('product_cat_id', '2');
	$this->db->delete('product_desc');

	/* Required Rainboot */
	$this->db->where('product_cat_id', '4');
	$this->db->delete('product_desc');

	/* Required Kids Wear */
	$this->db->where('product_cat_id', '9');
	$this->db->delete('product_desc');

	echo 'Category Delete Done';

	/* Sub - Category  */
	$sub_cat_ids = array(7,11,12,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,35,36,37,38,39,40,41,42,43,44,45,49,53,54,55,59,61);
	$this->db->where_in('product_sub_cat_id', $sub_cat_ids);
	$this->db->delete('product_desc');

	echo 'Sub Category Delete Done - 227 ';
	$i  = 0;
	/*$data = array();
	$query = $this->db->query("select * from category where level = 3");
	$res = $query->result_array();
	if(!empty($res)){
	$data = array();
	foreach($res as $val){

	$old_variation_type = $val['old_id'];

	$query1 = $this->db->query("select product_id from product_variations where product_var_type_id = '".$old_variation_type."'");
	$res1 = $query1->result_array();

	foreach($res1 as $val1){
	$data = array();
	$data = array(
	'object_id' => $val1['product_id'],
	'category_id' => $val['id'],
	'object_type' => 'product'
	);

	$query11 = $this->db->query("select * from category_object_relationship where object_id = '". $val1['product_id']."' and category_id = '".$val['id']."' and object_type = 'product'");
	$res11 = $query11->result_array();

	if(empty($res11)){
	$i++;
	$this->db->insert('category_object_relationship', $data);
	}
	}

	}


	}*/

	echo 'i == '.$i;



	$data = array();
	$query = $this->db->query("select * from category where level = 4");
	$res = $query->result_array();
	if(!empty($res)){
	$data = array();
	foreach($res as $val){

	$old_variation_type = $val['old_id'];

	$query1 = $this->db->query("select product_id from product_variations where product_var_val_id = '".$old_variation_type."'");
	$res1 = $query1->result_array();

	foreach($res1 as $val1){
	$data = array();
	$data = array(
	'object_id' => $val1['product_id'],
	'category_id' => $val['id'],
	'object_type' => 'product'
	);

	$query11 = $this->db->query("select * from category_object_relationship where object_id = '". $val1['product_id']."' and category_id = '".$val['id']."' and object_type = 'product'");
	$res11 = $query11->result_array();

	if(empty($res11)){
	$i++; echo $i.'---';
	$this->db->insert('category_object_relationship', $data);
	}
	}

	}


	}

}

function attribute_category_mapping(){
$this->db->truncate('attribute_values_relationship');
$data=array();
/*Material - 1*/
$material = array(1532,1577,2092,1599,320,3882,560,321,1770,3664,319,1831,1793,336,325,333,330,2992,608,328,2134,557,2008,324,332,978,1509,329,1530,665,338,327,1547,561,1709,3868,562,3883,1542,322,334,3221,318,337,335,1535,3535,339,3919,564,563,3572,340,1694);

if(!empty($material)){
		foreach ($material as $value) {
			$query = $this->db->query("SELECT id,name,product_variation_type_id,slug,created_by,modified_by FROM `product_variation_value` WHERE id =".$value);
			$res = $query->result_array();
			if(!empty($res)){
				
			//echo $res[0]['name'].'<br>';

			$data[] = array(
					'attribute_id' => 1,
					'attr_value_name' => $res[0]['name'],
					'attr_value_content' => $res[0]['name'],
					'attr_value_slug' => $this->clean($res[0]['name']),
					'old_id' => $res[0]['id']
				);

			}
	}
		
}


/*Colour - 2*/

$color = array(499,273,284,287,277,285,276,1955,290,509,275,281,282,286,280,274,283);

if(!empty($color)){
		foreach ($color as $value) {
			$query = $this->db->query("SELECT id,name,product_variation_type_id,slug,created_by,modified_by FROM `product_variation_value` WHERE id =".$value);
			$res = $query->result_array();
			if(!empty($res)){
				
			//echo $res[0]['name'].'<br>';

			$data[] = array(
					'attribute_id' => 2,
					'attr_value_name' => $res[0]['name'],
					'attr_value_content' => $res[0]['name'],
					'attr_value_slug' => $this->clean($res[0]['name']),
					'old_id' => $res[0]['id']
				);

			}
	}
		
}

/*Details - 3*/

$color = array(499,273,284,287,277,285,276,1955,290,509,275,281,282,286,280,274,283);

if(!empty($color)){
		foreach ($color as $value) {
			$query = $this->db->query("SELECT id,name,product_variation_type_id,slug,created_by,modified_by FROM `product_variation_value` WHERE id =".$value);
			$res = $query->result_array();
			if(!empty($res)){
				
			//echo $res[0]['name'].'<br>';

			$data[] = array(
					'attribute_id' => 3,
					'attr_value_name' => $res[0]['name'],
					'attr_value_content' => $res[0]['name'],
					'attr_value_slug' => $this->clean($res[0]['name']),
					'old_id' => $res[0]['id']
				);

			}
	}
		
}

/*Prints - 4*/

$print = array(301,554,1685,312,3529,1548,3563,311,2049,603,607,608,305,306,602,2035,2439,1887,304,1419,303,302,1753,3524);

if(!empty($print)){
		foreach ($print as $value) {
			$query = $this->db->query("SELECT id,name,product_variation_type_id,slug,created_by,modified_by FROM `product_variation_value` WHERE id =".$value);
			$res = $query->result_array();
			if(!empty($res)){
				
			//echo $res[0]['name'].'<br>';

			$data[] = array(
					'attribute_id' => 4,
					'attr_value_name' => $res[0]['name'],
					'attr_value_content' => $res[0]['name'],
					'attr_value_slug' => $this->clean($res[0]['name']),
					'old_id' => $res[0]['id']
				);

		}
	}
		
}

/*Neck - 5*/

$Neck = array(400,567,401,3286,408,404,2046,398,566,402,399);

if(!empty($Neck)){
		foreach ($Neck as $value) {
			$query = $this->db->query("SELECT id,name,product_variation_type_id,slug,created_by,modified_by FROM `product_variation_value` WHERE id =".$value);
			$res = $query->result_array();
			if(!empty($res)){
				
			//echo $res[0]['name'].'<br>';

			$data[] = array(
					'attribute_id' => 5,
					'attr_value_name' => $res[0]['name'],
					'attr_value_content' => $res[0]['name'],
					'attr_value_slug' => $this->clean($res[0]['name']),
					'old_id' => $res[0]['id']
				);

		}
	}
		
}

/*Silhoutte - 6*/

$Silhoutte = array(552,546,2058,2070,549,547);

if(!empty($Silhoutte)){
		foreach ($Silhoutte as $value) {
			$query = $this->db->query("SELECT id,name,product_variation_type_id,slug,created_by,modified_by FROM `product_variation_value` WHERE id =".$value);
			$res = $query->result_array();
			if(!empty($res)){
				
			//echo $res[0]['name'].'<br>';

			$data[] = array(
					'attribute_id' => 6,
					'attr_value_name' => $res[0]['name'],
					'attr_value_content' => $res[0]['name'],
					'attr_value_slug' => $this->clean($res[0]['name']),
					'old_id' => $res[0]['id']
				);

		}
	}
		
}

/*Sleeve - 7*/

$Sleeve = array(375,376,374,1874,372);

if(!empty($Sleeve)){
		foreach ($Sleeve as $value) {
			$query = $this->db->query("SELECT id,name,product_variation_type_id,slug,created_by,modified_by FROM `product_variation_value` WHERE id =".$value);
			$res = $query->result_array();
			if(!empty($res)){
				
			//echo $res[0]['name'].'<br>';

			$data[] = array(
					'attribute_id' => 7,
					'attr_value_name' => $res[0]['name'],
					'attr_value_content' => $res[0]['name'],
					'attr_value_slug' => $this->clean($res[0]['name']),
					'old_id' => $res[0]['id']
				);

		}
	}
		
}


/*Length - 8*/

$Length = array(1854,365,364,2235);

if(!empty($Length)){
		foreach ($Length as $value) {
			$query = $this->db->query("SELECT id,name,product_variation_type_id,slug,created_by,modified_by FROM `product_variation_value` WHERE id =".$value);
			$res = $query->result_array();
			if(!empty($res)){
				
			//echo $res[0]['name'].'<br>';

			$data[] = array(
					'attribute_id' => 8,
					'attr_value_name' => $res[0]['name'],
					'attr_value_content' => $res[0]['name'],
					'attr_value_slug' => $this->clean($res[0]['name']),
					'old_id' => $res[0]['id']
				);

		}
	}
		
}


/*Collar - 9*/


$Collar = array(3608,2366,387,388,3600);

if(!empty($Collar)){
		foreach ($Collar as $value) {
			$query = $this->db->query("SELECT id,name,product_variation_type_id,slug,created_by,modified_by FROM `product_variation_value` WHERE id =".$value);
			$res = $query->result_array();
			if(!empty($res)){
				
			//echo $res[0]['name'].'<br>';

			$data[] = array(
					'attribute_id' => 9,
					'attr_value_name' => $res[0]['name'],
					'attr_value_content' => $res[0]['name'],
					'attr_value_slug' => $this->clean($res[0]['name']),
					'old_id' => $res[0]['id']
				);

		}
	}
		
}

/*Waist - 10*/

$Waist = array(397,395,396);

if(!empty($Waist)){
		foreach ($Waist as $value) {
			$query = $this->db->query("SELECT id,name,product_variation_type_id,slug,created_by,modified_by FROM `product_variation_value` WHERE id =".$value);
			$res = $query->result_array();
			if(!empty($res)){
				
			//echo $res[0]['name'].'<br>';

			$data[] = array(
					'attribute_id' => 10,
					'attr_value_name' => $res[0]['name'],
					'attr_value_content' => $res[0]['name'],
					'attr_value_slug' => $this->clean($res[0]['name']),
					'old_id' => $res[0]['id']
				);

		}
	}
		
}

if(!empty($data)){
		if($this->db->insert_batch('attribute_values_relationship', $data)){ echo 'Done'; } else{ echo 'Not Done'; }
	}

}

function cat_level_4(){

$cat_level_3 = array(3719,3147,3713,3732,3296,3669,3753,3723,3751,3306,3687,3772,3678,3293,3739,3806,3298,3105,3314,3727,3754,3818,3104,3688,3692,3300,3309,3823,3292,3313,3303,3287,3828,3698,121,2929,3921,3922,3712,3702,3318,3942,3700,3899,3307,3316,3726,3330,3740,3813,3901,3731,3736,3294,3310,3824,3670,3671,3728,122,3771,3816,3327,3892,3822,3846,3730,3691,3683,3686,3895,3297,3694,3897,3145,3716,3894,3685,3745,3738,3769,3676,3714,3750,2928,3724,3833,2931,3665,3681,3709,3746,3791,3793,3794,3795,3796,3797,3799,3800,3801,3803,3804,3808,3809,3812,3815,3853,3902,3913,3914,3896,3889,3699,3755,3323,3758,3900,3708,3729,3317,3311,3322,3324,3821,3302,3312,3325,3734,3831,3718,3315,3725,1707,3748,3819,3697,3733,3826,3696,3149,3328,3717,3305,3915,3934,3756,3673,3675,3742,3744,3852,3770,3682,3749,3757,3711,3825,3811,3737,2240,3301,3893,3850,3304,3695,3326,3693,3705,3299,3028,3848,3917,3710,3851,3845,3116,3689,3308,3684,3092,3680,3701,3890,3832,3898,3830,3849,3706,3820,3068,3690,3715,3817,3722,3720,3735,3805,3679);
	
	if(!empty($cat_level_3)){
		foreach ($cat_level_3 as $value) {
			$query = $this->db->query("SELECT id,name,product_variation_type_id,slug,created_by,modified_by FROM `product_variation_value` WHERE id =".$value);
			$res = $query->result_array();
			if(!empty($res)){
				
				$query1 = $this->db->query("select id,category_slug from category where old_id=".$res[0]['product_variation_type_id']." and level = 3 limit 0,1");
				$res1 = $query1->result_array();
				
				if(!empty($res1)){
				$data[] = array(
					'category_name' => $res[0]['name'],
					'category_content' => $res[0]['name'],
					'category_image' => '',
					'category_parent' => $res1[0]['id'],
					'category_type' => 'product',
					'category_slug' => $res1[0]['category_slug'].'-'.$this->clean($res[0]['slug']),
					'count' => 0,
					'created_by' => $res[0]['created_by'],
					'modified_by' => $res[0]['modified_by'],
					'created_datetime' =>date('Y-m-d H:i:s'),
					'modified_datetime'=>date('Y-m-d H:i:s'),
					'old_id' => $res[0]['id'],
					'level' => 4
				);
				}

			}
		}
		
	}

	if(!empty($data)){
		if($this->db->insert_batch('category', $data)){ echo 'Done'; } else{ echo 'Not Done'; }
	}

}

function clean($string) {
   $string = str_replace(' ', '-', strtolower($string)); // Replaces all spaces with hyphens.

   return preg_replace('/[^a-z\-]/', '', $string); // Removes special chars.
}

function attributes_product_mapping(){
	$query = $this->db->query("select * from attribute_values_relationship");
	$res = $query->result_array();
	$i=0;
	if(!empty($res)){
		foreach($res as $val){
			$old_id=$val['old_id'];

			
			$query1 = $this->db->query("select distinct product_id from product_variations where product_var_val_id = '".$old_id."'");
			$res1 = $query1->result_array();
			$data = array();
			foreach($res1 as $val1){
				
				$data[] = array(
				'object_id' => $val1['product_id'],
				'attribute_id' => $val['id'],
				'object_type' => 'product'
				);
				$i++; echo $i.'----';
		}

		if(!empty($data)){
			if($this->db->insert_batch('attribute_object_relationship', $data)){ echo 'Done'; } else{ echo 'Not Done'; }
		}
	 }
   }
}


function attributes_product_extra_mapping(){
	$extra_array = array(375 => '1255, 3413, 3509, 3597,2215',
		606 => '936,937,938,939,940,942,1381,3392,2192,3232,3282,2254',
		1532 => '1869,1580,1721,1797,2230,3442',
		300 => '514,795,796,797,798,799,1357,2056,2938,3376,3552,3585,1613,2182,2445,2246,314,876,877,878,879,880,881,3558,1645,1818,2091,2136',
		362 => '1202,1203,1204,1205,1206,1207,1506,3505',
		317 => '894,895,896,897,898,899,1377,2322,3389',
		3713 => '3716',
		499 => '1591,1690,2361,2926,3195,3656',
		273 => '670,671,672,673,674,1327,1674,1786, 2358,2359,2374,2374,2399,2407,2914,3611,3862,3935',
		2092 => '2111,2318,2422,3939,1717,2303',
		600 => '900,901,902,903,904,905,1379,3390,1607,3534,3560,505,1611',
		284 => '500,528,718,718,719,720,1679,2957,3627,3628,3858,3865,3929,2196,1786,2041,536,651,751,752,1728,2150,1776,2395',
		400 => '1050,3365,3603',
		353 => '1123,1124,1125,1126,1127,1128,1450,3495',
		320 => '966,967,968,969,970,971,1361,23493483',
		287 => '503,531,730,731,732,733,1340,2093,3633,3634,3861,3872,3928',
		348 => '1078,1079,1080,1081,1082,1083,1084,1440',
		604 => '924,925,926,927,928,929,3551',
		3882 => '1543,1868,3882',
		560 => '634,1410,1466,1467,1468,1469,1470,1471,3460',
		313 => '870,871,872,873,874,875,1375,3303,3386,3546,3564',
		2353 => '3438,3968,3408,1632',
		321 => '972,973,974,975,976,977,1364,3482',
		2366 => '1827,3666,3374',
		319 => '960,961,962,963,964,965,1360,1696,2255,2301,2415,2978,3484,3570,3923',
		277 => '492,2076,3275,3876,3887,2885,3659,3003',
		1831 => '2033,3444,1576,3336,3449,3583,1720',
		567 => '1093,3396,3660',
		1806 => '1589,381,1300,1806,1812,3419,3537,3580,1208,1209,1210,1211,1212,1213,1498,3503,2067,590,1208,1208,1210,1211,1213,1498,3503,2067',
		593 => '1227,1228,1229,1230,1231,1232',
		351 => '1111,1112,1113,1114,1115,1116,1446,3493',
		336 => '1278,1279,1280,1281,1282,1283,1284,1285,1286,1287,1719,3579,1785',
		301 => '515,800,801,802,803,804,2321,3401,3662',
		345 => '1059,1060,1061,1062,1063,1064,1435,3488',
		2164 => '1009,1010,1032,1033,1034,1035,1036,1037,1427,3468,2348',
		2118 => '1066,1067,1068,1069,1070,7010,1436,2118,1685,1691,2993,2995,2997,3000,3397,3489,3519,3520,3521,3559,346',
		585 => '1166,1167,1168,1169,1170,1171',
		312 => '864,865,866,867,868,869,1373,3385,3544,2995,3520',
		586 => '1172,1173,1174,1175,1176,1177,1489,3500',
		1871 => '2103,3253,3538,3589,1908,1872',
		376 => '1256,1605,2224,3414,3593',
		1548 => '3567,1673,2342,3222,3241,3403,3569',
		330 => '1233,1234,1235,1236,1237,1238,1239,1387,1416,2221,2345,3443,3454',
		2320 => '3510',
		311 => '858,859,860,861,862,863,1372,3384,3661',
		285 => '501,529,721,722,723,724,1338,1346,2923,3629,3630,3866,39297,494,698,3654,3857',
		276 => '491,519,520,683,684,685,686,1823,2408,2414,3647,3648,3864',
		401 => '1051,2266,2388',
		2332 => '3511,2239',
		2049 => '3196,2251',
		408 => '1089,3366',
		603 => '918,919,920,921,922,923',
		607 => '941,943,944,945,946,947,1384,1737,3393',
		2992 => '3428',
		608 => '948,949,950,951,952,953,1385,1801,1802,3394',
		3271 => '3522',
		328 => '1017,1018,1019,1020,1021,1022,1383,2419,3477,3575',
		343 => '1038,1039,1040,1042,1044,1046,1047,1535,1533',
		557 => '1407,1452,1453,1457,1458,1459,3462',
		2008 => '632,631,1925,2429,3237,3357,3433,3584,633',
		324 => '997,998,999,1000,1001,1002,1378',
		332 => '1249,1250,1251,1252,1253,1259,1261,1262,1263,1264,1265,333,1390,1391,1529,1639,1769,1840,1958,2292,3472,3473,3578,3591,3878,3879,3941',
		978 => '979,980,981,982,983,984,1385,3481,3571,3924',
		1509 => '1510,1510,1511,1512,1514,1822,3451,1862',
		329 => '1024,1026,1027,1028,1029,1030,1386,3476,3582',
		1955 => '295,296,511,512,539,540,762,763,764,765,766,767,768,769,770,1348,1349,1598,1713,1817,2052,2210,1754,2286,1992,3874,2373,2920,2921,3223,3639,3640,3641,3642',
		2934 => '3517,2985',
		290 => '506,1596,1610,1618,1829,2363,2370,2925,3193,3649,3650,3877,3936,331,1240,1241,1242,1243,1244,1245,1389,3474',
		509 => '1614,1875,2100,3239,2172,3215',
		665 => '666,667,1398,3469',
		275 => '2108,2445,490,518,679,680,681,682,1329,1680,3615,3616,3873,1687,3645,3646',
		338 => '1309,1310,1311,1312,1313,1314,1402,2418,3467,3672',
		404 => '1085,2245',
		281 => '496,525,705,706,707,708,1334,2409,3621,3622,3870,3930',
		327 => '1011,1012,1013,1014,1016,1382,1417,2394,3453',
		602 => '3548,912,913,914,915,916,917,1418,3395,2347,3404',
		282 => '497,709,710,711,712,1335,1675,1729,2354,2356,2381,2396,2405,3623,3624,3871,3931,2040,2355,3281,3340',
		589 => '1196,1197,1198,1199,1200,1201,1501,3504',
		354 => '359,1129,1130,1131,1132,1133,1134,1451,1481,3496',
		350 => '358,1105,1107,1108,1109,1110,1445,1456,3492,1106',
		1547 => '1938,3881',
		1590 => '2003,344,1053,1054,1055,1056,1058,1065,1428,3487',
		347 => '1072,1073,1074,1075,1076,1077,1437,3490',
		1887 => '309,846,847,848,849,850,851,1371,2068,3383,3653',
		3821 => '3285',
		561 => '1411,1472,1473,1474,1475,1476,1477,1478,2416,3459,3592',
		1709 => '1716,1953,3886,1670',
		3868 => '1918,1941',
		286 => '502,530,725,727,728,729,1339,1677,2412,3631,3632,3932,534,655,743,745,1343,3637,3638',
		562 => '1412,1479,1480,1482,1483,1484,1845,2421,3458,3590',
		280 => '495,524,701,702,703,704,1333,1676,2360,2884,3919,3620,3827,3867,3937,2936,2937,3343',
		398 => '1048,1523,1554,3363,3283,3542,3601',
		1542 => '840,841,842,843,844,845,1370,1531,1643,3382,3880',
		355 => '1135,1136,1137,1138,1139,1140,1454,3452',
		361 => '1184,1185,1186,1187,1188,1189,1496,3502',
		322 => '985,986,987,988,989,990,1366,1416,2305,3434,3480',
		566 => '1092,2267',
		334 => '1266,1267,1268,1269,1270,1271',
		318 => '954,955,956,957,958,959,1359,1838,1860,2234,2296,2303,2304,2339,2369,3437,3441,3485,3573,3926',
		584 => '1160,1161,1162,1163,1164,1165,1487,3499',
		337 => '1289,1291,1293,1295,1297,1299,1401,2417,3468',
		304 => '816,817,818,819,820,821,1363,2015,3304,3379,3528,3545',
		335 => '1272,1273,1274,1275,1276,1277,1394,2102,3470,3588',
		402 => '1052',
		1535 => '1536,1537,1538,1539,1540,3536,3576,3885,3918',
		1795 => '2229',
		303 => '810,811,812,813,814,815,1362,3378,3561',
		302 => '805,806,807,808,809,1358,3377',
		587 => '1178,1179,1180,1181,1182,1183,1493,3501',
		1753 => '3550,2276,2324',
		357 => '1147,1148,1149,1150,1151,1152',
		339 => '1315,1316,1317,1318,1319,1320,1403,3466,3587',
		564 => '1414,1497,1499,1500,1502,1503,1504,1505,3456,3888',
		563 => '1413,1488,1490,1491,1492,1494,1495,3457',
		399 => '1049,3284,3364,3543,3602',
		3572 => '2238,3440,3912,3925',
		274 => '489,661,675,676,677,678,794,1328,1826,2210,2411,2960,3613,3614,3863',
		340 => '1321,1322,1323,1324,1325,1326,1404,3465,3577',
		283 => '498,713,714,715,716,1336,1678,2357,2413,3625,3626,3869,3933',
		349 => '1096,1098,1100,1102,1103,1104,1442,3491,2047,2135');
	$i = 0;
		if(!empty($extra_array)){
			foreach($extra_array as $key => $val){
					
				$query = $this->db->query("select * from attribute_values_relationship where old_id = '".$key."'");
				$res = $query->result_array();

				if(!empty($res)){
					$attribute_id = $res[0]['attribute_id'];				

					$query1 = $this->db->query("select distinct product_id from product_variations where product_var_val_id IN (".trim($val).")");
					$res1 = $query1->result_array();
					$data = array();
					foreach($res1 as $val1){
						$data = array();
						$data = array(
						'object_id' => $val1['product_id'],
						'attribute_id' => $attribute_id,
						'object_type' => 'product'
						);

							$query11 = $this->db->query("select * from attribute_object_relationship where object_id = '". $val1['product_id']."' and attribute_id = '".$attribute_id."' and object_type = 'product'");
							$res11 = $query11->result_array();

							if(empty($res11)){
							$i++; echo $i.'----';
							$this->db->insert('attribute_object_relationship', $data);
							}


					}

					

				}


			}
		}
		echo $i;
}

function category_count_update(){
	$query = $this->db->query("select * from category");
	$res = $query->result_array();

	if(!empty($res)){
		foreach ($res as $value) {
			$data = array();		
			$cat_id = $value['id'];
		
			$query1 = $this->db->query("select count(id) as cnt from category_object_relationship where object_type = 'product' and category_id = '".$cat_id."'");
			$res1 = $query1->result_array();	

			if(!empty($res1)){

				$query11 = $this->db->query("update category set count='".$res1[0]['cnt']."' where id = '".$cat_id."'");
			}
		}
	}
}

function attribute_count_update(){
	$query = $this->db->query("select * from attribute_values_relationship");
	$res = $query->result_array();

	if(!empty($res)){
		foreach ($res as $value) {
			$data = array();		
			$cat_id = $value['id'];
		
			$query1 = $this->db->query("select count(id) as cnt from attribute_object_relationship where object_type = 'product' and attribute_id = '".$cat_id."'");
			$res1 = $query1->result_array();	

			if(!empty($res1)){

				$query11 = $this->db->query("update attribute_values_relationship set count='".$res1[0]['cnt']."' where id = '".$cat_id."'");
			}
		}
	}
}


function lifestyle_product_mapping(){

	$query = $this->db->query("select object_id from category_object_relationship where category_id = 62");
	$res = $query->result_array();
	$data = array();
	if(!empty($res)){
		foreach($res as $value) {
			$data[] = array(
			'object_id' => $value['object_id'],
			'category_id' => 2,
			'object_type' => 'product'
			);

			$data[] = array(
			'object_id' => $value['object_id'],
			'category_id' => 14,
			'object_type' => 'product'
			);
		}
	}

	if(!empty($data)){
	if($this->db->insert_batch('category_object_relationship', $data)){ echo 'Done'; } else{ echo 'Not Done'; }
	}
}



function update_print(){

	$print = array(301,554,1685,312,3529,1548,3563,311,2049,603,607,608,305,306,602,2035,2439,1887,304,1419,303,302,1753,3524);

	if(!empty($print)){
			foreach ($print as $value) {
				$query = $this->db->query("SELECT id,name,product_variation_type_id,slug,created_by,modified_by FROM `product_variation_value` WHERE id =".$value);
				$res = $query->result_array();
				if(!empty($res)){
					
				

				$data[] = array(
						'attribute_id' => 4,
						'attr_value_name' => $res[0]['name'],
						'attr_value_content' => $res[0]['name'],
						'attr_value_slug' => $this->clean($res[0]['name']),
						'old_id' => $res[0]['id']
					);

			}
		}
			
	}

	if(!empty($data)){
		if($this->db->insert_batch('attribute_values_relationship', $data)){ echo 'Done'; } else{ echo 'Not Done'; }
	}

	}

	function go_live_categorisation(){
			/*$query = $this->db->query("select id,product_cat_id,product_sub_cat_id from product_desc where id >= 110707 ");
			$res = $query->result_array();
			$data = array();
			if(!empty($res)){
				foreach ($res as $value) {

				$product_id = $value['id'];
				$cat_id = $value['product_cat_id'];
				$sub_cat_id = $value['product_sub_cat_id'];

				$query1 = $this->db->query("select id from category where old_id='".$cat_id."' and level=1");
				$res1 = $query1->result_array();

				foreach($res1 as $val){
					$data[] = array(
					'object_id' => $product_id,
					'category_id' => $val['id'],
					'object_type' => 'product'
					);
					}

				$query11 = $this->db->query("select id from category where old_id='".$sub_cat_id."' and level=2");
				$res11 = $query11->result_array();

					foreach($res11 as $val1){
					$data[] = array(
					'object_id' => $product_id,
					'category_id' => $val1['id'],
					'object_type' => 'product'
					);
					}
				}
		}

		if(!empty($data)){
			echo "<pre>"; print_r($data);
			if($this->db->insert_batch('category_object_relationship', $data)){ echo 'Done'; } else{ echo 'Not Done'; }
		}*/


		/*$data = array(); $i=0; 
		$query = $this->db->query("select * from category where level = 3 ");
		$res = $query->result_array();
		if(!empty($res)){
			$data = array();
			foreach($res as $val){

			$old_variation_type = $val['old_id'];

			$query1 = $this->db->query("select product_id from product_variations where product_var_type_id = '".$old_variation_type."' and product_id >= 110707");
			$res1 = $query1->result_array();

			foreach($res1 as $val1){
				$data = array();
				$data = array(
				'object_id' => $val1['product_id'],
				'category_id' => $val['id'],
				'object_type' => 'product'
				);

				$query11 = $this->db->query("select * from category_object_relationship where object_id = '". $val1['product_id']."' and category_id = '".$val['id']."' and object_type = 'product'");
				$res11 = $query11->result_array();

				if(empty($res11)){
					$i++;
					echo "<pre>"; print_r($data);
					$this->db->insert('category_object_relationship', $data);
					echo $i;
				}
			}

			}
		}*/


			/*$data = array(); $i=0; 
			$query = $this->db->query("select * from category where level = 4");
			$res = $query->result_array();
			if(!empty($res)){
				$data = array();
				foreach($res as $val){

				$old_variation_type = $val['old_id'];

				$query1 = $this->db->query("select product_id from product_variations where product_var_val_id = '".$old_variation_type."' and product_id >= 110707");
				$res1 = $query1->result_array();

				foreach($res1 as $val1){
				$data = array();
				$data = array(
				'object_id' => $val1['product_id'],
				'category_id' => $val['id'],
				'object_type' => 'product'
				);

				$query11 = $this->db->query("select * from category_object_relationship where object_id = '". $val1['product_id']."' and category_id = '".$val['id']."' and object_type = 'product'");
				$res11 = $query11->result_array();

				if(empty($res11)){
				$i++; echo $i.'---';
					$this->db->insert('category_object_relationship', $data);
					}
				}

				}


			}*/
	}


		function perosnality_attribute_mapping1(){

		/* All attributes added the None attribute values */
		/*$attributes = $this->db->query("select id from attributes");
		$attributes_data = $attributes->result_array();
		$data = array();
		if(!empty($attributes_data)){
			foreach($attributes_data as $val ){
				$data[] = array(
					'attribute_id' => $val['id'],
					'attr_value_name' => 'None',
					'attr_value_content' => 'None',
					'attr_value_slug' => $this->clean('none')
				);
			}
		}
		
		if(!empty($data)){
			if($this->db->insert_batch('attribute_values_relationship', $data)){ echo 'All attributes added the None attribute value'; } else{ echo 'Not Done'; }
		}*/

		/* Pear/ Athleisure */
		$pa_data = $this->db->query("SELECT `attribute_value_ids`,object_id FROM `pa_object_relationship` where object_type ='category' and pa_type='bucket' and `attribute_value_ids`!='' and object_value IN(37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117)");
		$pa_data = $pa_data->result_array();
    	 
    
		if(!empty($pa_data)){ 
			$object_id = $pa_data[0]['object_id'];
			$pa_data_array = $pa_data[0]['attribute_value_ids'];
			$pa_data_array = unserialize($pa_data_array);
			$atr_dt = implode(',', $pa_data_array);
//			print_r($atr_dt);
       
			$get_parent_attr = $this->db->query("select GROUP_CONCAT(DISTINCT attribute_id) as atdata from attribute_values_relationship where id IN(".$atr_dt.")");
			$get_parent_attr_d = $get_parent_attr->result_array();
			
			$pa_attr = explode(',',$get_parent_attr_d[0]['atdata']);
     
		
			$product_data = $this->db->query("SELECT object_id FROM `category_object_relationship` where category_id ='".$object_id."' and object_type='product'");

			$product_data_array = $product_data->result_array();
       
			$product_id = array();
			if(!empty($product_data_array)){
				foreach($product_data_array as $value){
					$product_id[] = $value['object_id'];

				}
			}
	
			if(!empty($product_id)){

				foreach($product_id as $p_id){

					$attribute_obj_data = $this->db->query("SELECT GROUP_CONCAT(attribute_id) as attr,object_id FROM `attribute_object_relationship` as a where object_id =".$p_id." and object_type='product' group by object_id");
					$attribute_obj_data_array = $attribute_obj_data->result_array();
					if(!empty($attribute_obj_data_array)){
						foreach($attribute_obj_data_array as $v){
							$pa_attr_dt = array(); $result =array();
							$get_parent_attr = $this->db->query("select GROUP_CONCAT(DISTINCT attribute_id) as atdata from attribute_values_relationship where id IN(".$v['attr'].")");
							$get_parent_attr_d = $get_parent_attr->result_array();

							$pa_attr_dt = explode(',',$get_parent_attr_d[0]['atdata']);
							
							$result = array_diff($pa_attr, $pa_attr_dt);
							
							if(!empty($result)){

								foreach($result as $rr){
									$data_att = array();
									$attribute_obj_data1 = $this->db->query("select id from `attribute_values_relationship` where attribute_id = ".$rr." and attr_value_name = 'None' limit 0,1");
									$ids = $attribute_obj_data1->result_array();
									
									
									$data_att = array(
										'object_id' =>$v['object_id'],
										'attribute_id' => $ids[0]['id'],
										'object_type' =>'product',
									);
									
							echo "<pre>"; print_r($data_att);

									$this->db->insert('attribute_object_relationship', $data_att);
								}

							}

						}
					}
				}
			
			}
		}

	
	}

	function attribute_sheet_mapping1(){

		$file = fopen("assets/Mens_Bucket1_with-pipe.csv","r");
		$i=0;
		while(!feof($file))
		{ 
		  $data = array();

		  $data = fgetcsv($file);

		  $bucket = $data[0];
		  $atrributes = $data[2];
		  $cat = $data[5];
		 

		  if($bucket!='' && $atrributes!='' && $cat!='' && $i!=0){
		  		
		  		$atrributes = str_replace('|', ',', $atrributes);
		  		$atrributes = str_replace(' ', ',', $atrributes);
		  		$atrributes = preg_replace('/[^0-9,]/', '', $atrributes);
		  		$attr = explode(',', $atrributes);
		  		$attr_s = array();

		  		if(!empty($attr)){
		  			foreach ($attr as $value) {
		  				if($value!=''){
		  					$attr_s[] = trim($value);
		  				}
		  			}
		  		}
		  		$attr_s = array_unique($attr_s);
         		$ww = '';
          		if(!empty($attr_s)){ $z=0;
                	foreach($attr_s as $hh){
                    if($z!=0){ $ww = $ww.','; }
                    $ww = $ww.$hh;
                    $z++;
                    }
                }
          
          		if($ww!=''){
                	$attribute_obj_data1122 = $this->db->query("select attribute_id from attribute_values_relationship where id IN(".$ww.")");
                	$attribute_obj_data1122333 = $attribute_obj_data1122->result_array();
                	
                	if(!empty($attribute_obj_data1122333)){
                    	foreach($attribute_obj_data1122333 as $ee){
                        	$attribute_obj_data15555 = $this->db->query("select id from `attribute_values_relationship` where attribute_id = ".$ee['attribute_id']." and attr_value_name = 'None' limit 0,1");
							$ids = $attribute_obj_data15555->result_array();
                            if(@$ids[0]['id']!=''){
                        	$attr_s[] = trim(@$ids[0]['id']);
                        }
                    }
                    }
                }
          
		  		//serialize($attr_s);
		  		
		  		//echo $bucket."-----".$cat."----".serialize($attr_s);
		  		//echo "<br>";
				$attr_s = array_unique($attr_s);
				//echo "<pre>"; print_r($attr_s);
				$cat_array = explode(',', $cat);
				if(!empty($cat_array)){
					foreach($cat_array as $cc){
				$attribute_obj_data11 = $this->db->query("select * from pa_object_relationship where pa_type='bucket' and object_type='category' and object_value = '".$bucket."' and object_id='".$cc."'");		  		
				$attribute_obj_data111 = $attribute_obj_data11->result_array();
				if(!empty($attribute_obj_data111)){
					echo '--------------------'; echo "<pre>"; print_r($attribute_obj_data111);
					$attribute_obj_data_1 = $this->db->query("update pa_object_relationship set `attribute_value_ids`='".serialize($attr_s)."' where pa_type='bucket' and object_type='category' and object_value = '".$bucket."' and object_id='".$cc."'");
		  		}else{

		  			$data_att11111 = array(
										'object_value' =>$bucket,
										'object_type' => 'category',
										'pa_type' =>'bucket',
										'object_id'=>$cc,
										'attribute_value_ids' => serialize($attr_s)
									);

		  			echo "<pre>"; print_r($data_att11111);
					$this->db->insert('pa_object_relationship', $data_att11111);

		  		}
		  		}
		  		}
		  }

		  $i++;
		}

		fclose($file);

	}

	function add_order_display_no1111(){

		$query = $this->db->query("select distinct order_unique_no from order_info where order_display_no=''");
		$res = $query->result_array();

		if(!empty($res)){ $i=1;
			foreach($res as $val){
				echo $order_display_no = 'SC'.sprintf("%010d",$i);
				echo '<br>';
				$query = $this->db->query("update order_info set order_display_no = '".$order_display_no."',modified_datetime=modified_datetime where order_unique_no='".$val['order_unique_no']."'");
				$i++;
			}
		}

	}
}
?>