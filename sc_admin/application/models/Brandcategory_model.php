<?php
class BrandCategory_model extends MY_Model {    
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    /*---------Category of brand submodule functionalities-------*/

  public function get_all_categories($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL){    
    
    if($search_by == '1'){ $search_cond = " AND category_name like '%".strip_tags(trim($table_search))."%'"; }
    else if($search_by == '2'){ $search_cond = " AND category_slug like '%".strip_tags(trim($table_search))."%'"; }
    /*else if($search_by == '3'){ 

        $tb_status=strip_tags(trim($table_search));
        if($tb_status=="Yes")
        {
          $status=1;

        }else
        {
          $status=0;
        }

      $search_cond = " AND status =".$status." "; }*/
    else{ $search_cond ='AND 1=1'; }
     
    if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
        $limit_cond = "LIMIT ".$page.",".$per_page;
      }else{
        $limit_cond = ''; 
      }


    $query = $this->db->query("select id,category_name, category_slug, status,is_delete, created_datetime,category_image from brand_category WHERE `is_delete` =0 ".$search_cond." order by id,parent asc ".$limit_cond);    
    $res = $query->result();    
    return $res;    

  }//this function is used to get the list of brand category added

  public function new_category($data)
  { 
    $this->db->insert('brand_category', $data);
    $c_id = $this->db->insert_id();   
    return $c_id;    
  }

  public function get_brand_parent_category($brand_id=NULL){  
   
    $query = $this->db->query("select id,category_name,category_image from brand_category WHERE `is_delete` =0  order by id,parent asc ");    
    $res = $query->result();    
    return $res;    

  } //to get brand_parent_category 

  public function get_brand_categories($brand_id=NULL){  
   
      $cathtml = '';
      $brand_cat_id = $this->get_brand_selectedcat($brand_id); 
      $query = $this->db->get_where('brand_category' , array('is_delete' =>0 ));
      $res = $query->result_array();
      if(!empty($res)){
        foreach($res as $val){
          if(is_array($brand_cat_id) == true && in_array($val['id'],$brand_cat_id)){
            $cathtml.= "<option value='".$val['id']."'' selected>".$val['category_name']."</option>";
          }else{
            $cathtml.= "<option value='".$val['id']."''>".$val['category_name']."</option>";
          }
        }
      }

      return $cathtml;
  } //to get brand_category 

  public function get_brand_selectedcat($brand_id=NULL){  
  
    $cat_id = array();
    $this->db->select('category_id');
    $query = $this->db->get_where('category_brand_relationship', array('brand_id' =>$brand_id ));
    $res = $query->result_array();
    if(!empty($res)){
      foreach ($res as $val) {
        $cat_id[] = $val['category_id'];
      }
    }
    return $cat_id;

  } 

  public function checkMultilpeCategroy($name,$parent)
  {
    if($name!='' && $parent!='')
    {
        $catQuery = "select count(category_name) as catCount from brand_category where category_name LIKE '".$name."' and parent='".$parent."' AND is_delete=0";        
        $countRes = $this->db->query($catQuery);    
        $rowRes = $countRes->row(); 
        return $rowRes;
    }else
    {
       return NULL;
    }
    
  }//this function checks for same category while adding new category 

  public function update_category($catID,$data){    
    $this->db->where('id', $catID);
    $updated = $this->db->update('brand_category' ,$data);
    
    if($updated){      
      return TRUE;     
    }
    else
    {     
      return false; 
    }
  }

  public function checkMultilpeCategroyEdit($catId,$name,$parent)
  {
    $catQuery = "select count(category_name) as catCount from brand_category where id!=".$catId." and category_name LIKE '".$name."' AND parent='".$parent."' AND is_delete=0 ";
    $countRes = $this->db->query($catQuery);
    $rowRes = $countRes->row();
    return $rowRes;
  }//this function checks for same category while editing category

  public function get_category_by_ID($catID){
    
    $this->db->select('*');
    $this->db->from('brand_category');
    $this->db->where('id', $catID);    
    $category = $this->db->get();   
    
    if($category){      
      return $category->row();      
    }
  }

  public function delete_category($catID)
  {    
      $this->db->where('id', $catID);
      $deleted = $this->db->update('brand_category' ,array('is_delete'=>'1'));
      
      if($deleted){      
        return TRUE;      
      }
      else
      {      
        return false; 
      }
  }

  public function get_paid_stores()
  {
      if($this->session->userdata('role_id') == 6){
          $brand_id = $this->session->userdata('user_id');
          $query = $this->db->query("SELECT user_id as brand_id, company_name FROM brand_info  WHERE is_paid = 1 and user_id=".$brand_id." ORDER BY company_name");
      }else{
          $query = $this->db->query("SELECT user_id as brand_id, company_name FROM brand_info  WHERE is_paid = 1 ORDER BY company_name");         
      }
      $result = $query->result();
      return $result;
  }

   public function get_store_products($brand_id)
    {
      if($brand_id!='' || $brand_id!=0)
      {
        $query = $this->db->query("SELECT id, name FROM product_desc  WHERE  brand_id=".$brand_id." ORDER BY id DESC ");  
      }else
      {
        return 0;
      }
       
       $result = $query->result_array();
       return $result;
    }

    function saveBrandCat($brand, $cat_arr)
    {
       if($brand!='')
       {
          $result_count = $this->db->query("SELECT count(id) FROM `category_brand_relationship`  WHERE  brand_id='".$brand."'  "); 
       }    
        
        $this->db->delete('category_brand_relationship', array('brand_id' => $brand)); 
        if(!empty($cat_arr))
        {  
          foreach($cat_arr as $val)
          {
            $category = array();
            $category = array('brand_id'=>$brand,
                              'category_id'=>$val,
                              'object_type'=>'brand_cat');
            $result = $this->db->insert('category_brand_relationship', $category);
          } 
          
          if($result == TRUE)
          {
            return TRUE;
          }else
          {
            return FALSE;
          }  
        }
       
        /*$insert_result = $this->db->insert('category_brand_relationship', $data['category']);
        $coupon_id = $this->db->insert_id();*/
        
    }

   /* function saveUpdate_coupon($data,$coupon_id=NULL)
    {     
      if($coupon_id!=NULL)
      {       
        $this->db->where('id',$coupon_id);
        $result = $this->db->update('coupon_settings', $data['coupon_info']);         
          if($result == TRUE)
          {
            return $coupon_id;
          }else
          {
            return FALSE;
          }

      }else
      {
        if(!empty($data))
        {
           if(isset($data['brand']) && $data['brand']!='')
           {
              $query = $this->db->query("SELECT * FROM coupon_settings  WHERE  id=".$coupon_id." AND status=1 AND is_delete=0 ORDER BY id DESC ");  
           }            
            $result = $this->db->insert('coupon_settings', $data['coupon_info']);
            $coupon_id = $this->db->insert_id();
            if($result == TRUE)
            {
              return $coupon_id;
            }else
            {
              return FALSE;
            }
        }  
      }   
     
    }
*/
   /*  function get_coupon($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL,$coupon_id=NULL)
    {

      if($search_by == '1'){ $search_cond = " AND coupon_code like '%".strip_tags(trim($table_search))."%'"; }     
        else{ $search_cond ='AND 1=1'; }
         
        if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                       $page = $page*$per_page-$per_page;
                    }else{ $page = 0;}
            $limit_cond = "LIMIT ".$page.",".$per_page;
        }else{
          $limit_cond = ''; 
        }

      if($coupon_id!='' || $coupon_id!=0)
      {
        $query = $this->db->query("SELECT a.*,b.`company_name` FROM coupon_settings as a, brand_info as b  WHERE  a.`brand_id`= b.`user_id` AND a.`id`=".$coupon_id." AND a.`status`=1 AND a.`is_delete`=0 ORDER BY a.`id` DESC ");  
      }else
      {
        $query = $this->db->query("SELECT a.*,b.`company_name` FROM coupon_settings as a, brand_info as b  WHERE  a.`brand_id`= b.`user_id`  AND a.`status`=1 AND a.`is_delete`=0 ".$search_cond." ORDER BY a.`id` DESC ".$limit_cond); 
      }
       
       //echo $this->db->last_query();
       $result = $query->result();
       return $result;
    }
*/

   

   

 } 
?>
