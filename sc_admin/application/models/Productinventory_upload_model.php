<?php

    /*
        1. Data from controller to model.
        2. Product is exist in our database or not : if exist then fetch product_id,name else not do nothing leave it
        3. product_size :- if size exist in our size master table then fetch the size_id else insert the new record and then fetch last inserted as size id.
        4. qty :- if number ( float ) then valid.
        5. product_inventory :- if product_id and size_id exist in product_inventory table or not
                                if exist update the qty
                                else insert new records.

    */

class Productinventory_upload_model extends CI_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /*function product_upload($data_post)
    {
        $upload_error = ''; $upload_success = '';
    	if(!empty($data_post))
    	{
        	$a = count($data_post);
            
        	for($i=0;$i<$a;$i++){
            	$product_id=$data_post[$i]['product_id'];
            	$Product_Name=$data_post[$i]['Product_Name'];
            	$Product_Size=$data_post[$i]['Product_Size'];
            	$Quantity=$data_post[$i]['Quantity'];
    	
                $query= $this->db->get_where('product_desc',array('id'=>$product_id,'name'=>$Product_Name));
                $res = $query->result_array();
    
                $count_array=count($res);

                if($count_array!=0)
                {
                        $query1= $this->db->get_where('product_size',array('size_text'=>$Product_Size));
                        $res1 = $query1->result_array();
                        $count_array1=count($res1);
                        
                            if($count_array1 == 0 )
                            {
                                $query = $this->db->query("INSERT INTO product_size (size_text)VALUES('".$Product_Size."')");
                                $size_id=$this->db->insert_id();
                            }else{
                                $size_id=$res1[0]['id'];
                                $query=$this->db->query("UPDATE Product_Size SET size_text='".$Product_Size."' WHERE id='".$size_id."'");
                           }
                          
                         $product_inventory_query= $this->db->get_where('product_inventory',array('product_id'=>$product_id,'size_id'=>$size_id));
                         $res = $product_inventory_query->result_array();
                            
                         $count_array=count($res);
                         if($count_array==0)
                         {
                            $query= $this->db->query("INSERT INTO product_inventory (product_id,size_id,stock_count)VALUES('".$product_id."','".$size_id."','".$Quantity."')");
                             $upload_success++;
                         }else{
                         $query=$this->db->query("UPDATE product_inventory SET size_id='".$size_id."',stock_count='".$Quantity."' WHERE id='".$res[0]['id']."'");
                            $upload_success++;

                         }

                    }else{
                            $upload_error = $upload_error .$i .' Product not exist'."<br>" ; 

                    }
    }
    }else{

        $upload_error  = $upload_error .' Cue sheet is empty ';

    }
    return '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>'.$upload_error . '</div>'.'<div class="alert alert-success alert-dismissable"><h5></i><strong>Successfully uploaded</strong></h5>  '.$upload_success.'Successfully uploaded</div>';
    }*/

    function product_upload($data_post){
        $upload_error = ''; $upload_success = 0; $product_info = array();
        
        if(!empty($data_post)){ $k = 2; 
            foreach($data_post as $val){
               
                $product_id = $val['product_id'];
                $product_name = trim($val['product_name']);
                $product_size = $val['product_size'];
                $quantity = $val['quantity'];
                $sku_code = $val['sku_code'];
                $price = $val['price'];
                $description = $val['description'];
				$is_active = $val['is_active'];
				
                $new_product_id = $this->check_product_exist($product_id,$product_name);
                if($new_product_id > 0){
                    $product_info[$product_id][] = array('size_id'=> $product_size, 'stock_count'=> $quantity,'product_sku'=>$sku_code);
                   /* $this->save_product_size($new_product_id,$product_info,$sku_code);*/
                    $this->save_product_extra_info($new_product_id,$price,$description,$is_active);
                    $upload_success++;
                   // var_dump($product_info);
                }else{
                    $upload_error = $upload_error .$k .' Product not exist'."<br>" ; 
                }
                $k++;
            }
           
           if(!empty($product_info))
           {
               foreach($product_info as $key=>$value)
               {
                    $this->save_product_size($new_product_id,$value,$key);
               }
           }
           
           
        }else{
            $upload_error  = $upload_error .' Cue sheet is empty ';
        }
        return $upload_error.' Successfully Update '.$upload_success;
    }

    function check_product_exist($product_id,$product_name){
        if($product_id>0){
            $query= $this->db->query('select id from product_desc where id = "'.$product_id.'"');
        }else{
            $query= $this->db->query('select id from product_desc where id = "'.$product_id.'" OR name = "'.$product_name.'"');
        }
        $res = $query->result_array();
        if(!empty($res)){ return $res[0]['id']; } else { return 0; }
    }

    function save_product_size($product_id,$product_info,$product_id){
        
        $this->db->where('product_id', $product_id);
        $this->db->delete('product_inventory'); 

        if(!empty($product_info)){
            foreach($product_info as $val){ 
                $size = $val['size_id']; $qty = $val['stock_count']; $sku_code = $val['product_sku'];

                $size_id = $this->get_size_id($size);
               
                if($size_id>0 && $product_id>0){
                    $data = array(
                        'product_id'=>$product_id,
                        'size_id'=>$size_id,
                        'stock_count'=>$qty,
                        'product_sku'=>$sku_code
                    );
                    $this->db->insert('product_inventory', $data); 
                }
            }
        }
    }

    function save_product_extra_info($new_product_id,$price,$description,$is_active){
        if($price!="" || $price!=0)
        {
            $data = array(              
                   'price' => $price
                );

            $this->db->where('id', $new_product_id);
            $this->db->update('product_desc', $data); 
        }
        if($description!=''){
            $data = array(
                   'description' => $description
            );

            $this->db->where('id', $new_product_id);
            $this->db->update('product_desc', $data); 
        }
		$data = array('status' => $is_active);
		$this->db->where('id', $new_product_id);
		$this->db->update('product_desc', $data); 
    }

    function get_size_id($size){
        if($size!=''){
            $query = $this->db->query("select count(*) as cnt,id from product_size where size_text = '".strtoupper($size)."'");
            $result = $query->result_array();
            if($result[0]['cnt'] == 0){
                $data = array(
                    'size_text' => strtoupper($size) ,
                );
                $this->db->insert('product_size', $data); 
                return $this->db->insert_id();
            }else {
                return $result[0]['id']; 
            }
        }
    }

}
