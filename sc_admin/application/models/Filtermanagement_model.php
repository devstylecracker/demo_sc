<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Filtermanagement_model extends MY_Model {
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }	

    function get_data($table_name){
    	if($table_name!=''){
    		$query = $this->db->query("select * from ".$table_name." where is_delete = 0 and status = 1 order by id desc");
    		return $query->result_array();
    	}
    }

    function save_data($table_name,$selected){
    	if($table_name!='' && !empty($selected)){
    		$res = $this->get_data($table_name);
    		foreach($res as $val){
    			if (in_array($val['id'], $selected)) {
    				$query = $this->db->query("update ".$table_name." set show_in_filter=1 WHERE id=".$val['id']);
    			}else{
    				$query = $this->db->query("update ".$table_name." set show_in_filter=0 WHERE id=".$val['id']);
    			}
    		}
    	}
    }

 }
 ?>