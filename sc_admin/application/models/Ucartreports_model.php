<?php
ini_set('max_execution_time',3600);
class Ucartreports_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function product_click_count($date_from,$date_to){

    	if($date_from!='' && $date_to!=''){

    		$query = $this->db->query("select count(id) as count from `users_product_click_track` where  `created_datetime` >= '".$date_from."' and `created_datetime` <= '".$date_to."' and `click_source` = 2");

    	}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("select count(a.id) as count from `users_product_click_track` as a where a.`created_datetime` like '%".date('Y-m-d')."%' and a.`click_source` = 2");

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("select count(a.id) as count from `users_product_click_track` as a where a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.`click_source` = 2");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("select count(a.id) as count from `users_product_click_track` as a where a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.`click_source` = 2");
    	}

    	$res = $query->result_array();
    	//echo $this->db->last_query();
    	return $res[0]['count'];
    }

    function get_cart_platform_info($date_from="", $date_to=""){
        if($date_from!='' && $date_to!=''){
            $query = $this->db->query("select count(id) count, platform_info from cart_info where date(created_datetime) >= '$date_from' and date(created_datetime) <= '$date_to' group by platform_info");
        }else if($date_from=='' && $date_to==''){
            $query = $this->db->query("select count(id) count, platform_info from cart_info where date(`created_datetime`) like '%".date('Y-m-d')."%' group by platform_info");

           // $query = $this->db->query("select count(id) count, platform_info from cart_info group by platform_info");

        }else if($date_from!='' && $date_to==''){
            $query = $this->db->query("select count(id) count, platform_info from cart_info where date(`created_datetime`) like '%$date_from%' group by platform_info");

        }else if($date_from=='' && $date_to!=''){
            $query = $this->db->query("select count(id) count, platform_info from cart_info where `created_datetime` like '%$date_to%' group by platform_info");
        }

        $res = $query->result_array();
        //echo $this->db->last_query();
   /*     echo "<pre>";
        print_r($res);
        exit;*/
        return $res;
    }

    function get_cart_platform_info_data($date_from="", $date_to="", $type_name){

         if(substr($type_name, 0, 7) == 'Windows'){
          $platform = "platform_info like '%$type_name%'";
        }else if($type_name == 'iOS'){
          $platform = "platform_info = '$type_name'";
        }else if($type_name == 'mobile_android'){
          $platform = "platform_info = '$type_name'";
        }else if($type_name == 'mobile_ios'){
          $platform ="platform_info = '$type_name'";
        }else if($type_name == 'Android'){
          $platform = "platform_info = '$type_name'";
        }else if($type_name == 'Linux'){
          $platform = "platform_info = '$type_name'";
        }else if($type_name == 'Mac OS X'){
          $platform = "platform_info = '$type_name'";
        }else if($type_name == 'total'){
          $platform = "platform_info != '$type_name'";
        }else{
          $platform = "platform_info not like '%Windows%' and platform_info != 'iOS' and platform_info != 'mobile_android' and platform_info != 'mobile_ios' 
                       and platform_info != 'Android' and platform_info != 'Linux' and platform_info != 'Mac OS X'";
        }

        if($date_from!='' && $date_to!=''){
            $query = $this->db->query("select ci.user_id,  ci.platform_info, bi.company_name,pd.`name`,ci.product_id id, 
                                        pd.price, ci.created_datetime from cart_info ci 
                                        left join product_desc pd on pd.id = ci.product_id
                                        left join brand_info bi on bi.id = pd.brand_id
                                        where $platform 
                                        and ci.created_datetime >= '$date_from' 
                                        and ci.created_datetime <= '$date_to';");
        }else if($date_from=='' && $date_to==''){
            $query = $this->db->query("select ci.user_id,  ci.platform_info, bi.company_name,pd.`name`,ci.product_id id, 
                                        pd.price, ci.created_datetime from cart_info ci 
                                        left join product_desc pd on pd.id = ci.product_id
                                        left join brand_info bi on bi.id = pd.brand_id
                                        where $platform ");

           // $query = $this->db->query("select count(id) count, platform_info from cart_info group by platform_info");

        }else if($date_from!='' && $date_to==''){
            $query = $this->db->query("select ci.user_id,  ci.platform_info, bi.company_name,pd.`name`,ci.product_id id, 
                                        pd.price, ci.created_datetime from cart_info ci 
                                        left join product_desc pd on pd.id = ci.product_id
                                        left join brand_info bi on bi.id = pd.brand_id
                                        where $platform 
                                        and ci.created_datetime like '%$date_from%';");

        }else if($date_from=='' && $date_to!=''){
            $query = $this->db->query("select ci.user_id,  ci.platform_info, bi.company_name,pd.`name`,ci.product_id id, 
                                        pd.price, ci.created_datetime from cart_info ci 
                                        left join product_desc pd on pd.id = ci.product_id
                                        left join brand_info bi on bi.id = pd.brand_id
                                        where $platform 
                                        and ci.created_datetime like '%$date_to%';");
        }

        $res = $query->result_array();
        //echo $this->db->last_query();
   /*     echo "<pre>";
        print_r($res);
        exit;*/
        return $res;
    }

     function notify_count($date_from,$date_to){

    	if($date_from!='' && $date_to!=''){

            $query = $this->db->query("select count(a.id) as count from `users_product_click_track` as a where  a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' and a.`click_source` = 3");

        }else if($date_from=='' && $date_to==''){
            $query = $this->db->query("select count(a.id) as count from `users_product_click_track` as a where a.`created_datetime` like '%".date('Y-m-d')."%' and a.`click_source` = 3");

        }else if($date_from!='' && $date_to==''){
            $query = $this->db->query("select count(a.id) as count from `users_product_click_track` as a where a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.`click_source` = 3");

        }else if($date_from=='' && $date_to!=''){
            $query = $this->db->query("select count(a.id) as count from `users_product_click_track` as a where a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.`click_source` = 3");
        }

        $res = $query->result_array();
        //echo $this->db->last_query();
        return $res[0]['count'];
    }

    function cart_count($date_from,$date_to){
    	if($date_from!='' && $date_to!=''){

            $query = $this->db->query("select count(a.id) as count from `users_product_click_track` as a where  a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' and a.`click_source` = 1");

        }else if($date_from=='' && $date_to==''){
            $query = $this->db->query("select count(a.id) as count from `users_product_click_track` as a where a.`created_datetime` like '%".date('Y-m-d')."%' and a.`click_source` = 1");

        }else if($date_from!='' && $date_to==''){
            $query = $this->db->query("select count(a.id) as count from `users_product_click_track` as a where a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.`click_source` = 1");

        }else if($date_from=='' && $date_to!=''){
            $query = $this->db->query("select count(a.id) as count from `users_product_click_track` as a where a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.`click_source` = 1");
        }

        $res = $query->result_array();
        //echo $this->db->last_query();
        return $res[0]['count'];
    }

    function product_sale_info($date_from,$date_to){
    	if($date_from!='' && $date_to!=''){
    		$query = $this->db->query("select count(a.id) as count from `order_product_info` as a where  a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."'");

    	}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("select count(a.id) as count from `order_product_info` as a where a.`created_datetime` like '%".date('Y-m-d')."%' ");

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("select count(a.id) as count from `order_product_info` as a where a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%'");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("select count(a.id) as count from `order_product_info` as a where a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%'");
    	}

    	$res = $query->result_array();
    	//echo $this->db->last_query();
    	return $res[0]['count'];
    }

    function abendant_cart($date_from,$date_to){
    	if($date_from!='' && $date_to!=''){
    		$query = $this->db->query("select count(a.id) as count from `cart_info` as a where  a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' and a.`order_id` is null");

    	}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("select count(a.id) as count from `cart_info` as a where a.`created_datetime` like '%".date('Y-m-d')."%' and a.`order_id` is null ");

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("select count(a.id) as count from `cart_info` as a where a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.`order_id` is null");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("select count(a.id) as count from `cart_info` as a where a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.`order_id` is null");
    	}

    	$res = $query->result_array();
    	//echo $this->db->last_query();
    	return $res[0]['count'];
    }

   /* function remove_from_cart($date_from,$date_to){
    	if($date_from!='' && $date_to!=''){
            $query = $this->db->query("SELECT (select count(a.id) as count from `users_product_click_track` as a where a.`created_datetime` 
                >='".$date_from."' and a.`created_datetime` <= '".$date_to."' ) - (select count(a.id) as count from `cart_info` as a where 
                a.`created_datetime`>= '".$date_from."'  and a.`created_datetime` <= '".$date_to."') AS Difference");
    		/*$query = $this->db->query("select count(a.id) as count from `cart_info` as a where  a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."'");*/

    	/*}else if($date_from=='' && $date_to==''){
    		$query = $this->db->query("select count(a.id) as count from `cart_info` as a where a.`created_datetime` like '%".date('Y-m-d')."%' and a.`order_id` is null ");

    	}else if($date_from!='' && $date_to==''){
    		$query = $this->db->query("select count(a.id) as count from `cart_info` as a where a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.`order_id` is null");

    	}else if($date_from=='' && $date_to!=''){
    		$query = $this->db->query("select count(a.id) as count from `cart_info` as a where a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.`order_id` is null");
    	}

    	$res = $query->result_array();
       // $count = count($res );
    // echo  $count;
    	echo $this->db->last_query();
    	return $res;
       
    }*/
public function remove_from_cart($date_from,$date_to){
         if($date_from!='' && $date_to!=''){
            $query = $this->db->query("select count(c.id) as cnt,c.name,k.user_id,k.product_id,k.created_datetime, (select `company_name` from brand_info where user_id =c.brand_id) as company_name,c.price from (SELECT a.product_id,a.created_datetime,a.unique_id,b.SCUniqueID,a.`user_id`
    FROM users_product_click_track as a
        LEFT JOIN cart_info as b
            ON a.product_id = b.product_id
    WHERE b.id IS NULL and a.`created_datetime`>= '".$date_from."' and a.`created_datetime` 
                <= '".$date_to."')as k,product_desc as c where k.`product_id` = c.id ");

         

        }else if($date_from=='' && $date_to==''){
           
               $query = $this->db->query("select count(c.id) as cnt,c.name,k.user_id,k.product_id,k.created_datetime,(select `company_name` from brand_info where user_id =c.brand_id) as company_name,c.price from (SELECT a.product_id,a.created_datetime,a.unique_id,b.SCUniqueID,a.`user_id`
    FROM users_product_click_track as a
        LEFT JOIN cart_info as b
            ON a.product_id = b.product_id
    WHERE b.id IS NULL and a.created_datetime like '%".date('Y-m-d')."%')as k,product_desc as c where k.`product_id` = c.id");

        }else if($date_from!='' && $date_to==''){
            $query = $this->db->query("select count(c.id) as cnt,c.name,k.user_id,k.product_id,k.created_datetime,(select `company_name` from brand_info where user_id =c.brand_id) as company_name,c.price from (SELECT a.product_id,a.created_datetime,a.unique_id,b.SCUniqueID,a.`user_id`
    FROM users_product_click_track as a
        LEFT JOIN cart_info as b
            ON a.product_id = b.product_id
    WHERE b.id IS NULL and a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%')as k,product_desc as c where k.`product_id` = c.id
");


        }else if($date_from=='' && $date_to!=''){
 

           $query = $this->db->query("select count(c.id) as cnt,c.name,k.user_id,k.product_id,k.created_datetime,(select `company_name` from brand_info where user_id =c.brand_id) as company_name,c.price from (SELECT a.product_id,a.created_datetime,a.unique_id,b.SCUniqueID,a.`user_id`
    FROM users_product_click_track as a
        LEFT JOIN cart_info as b
            ON a.product_id = b.product_id
    WHERE b.id IS NULL and a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%')as k,product_desc as c where k.`product_id` = c.id
");
        }
               // echo $this->db->last_query();
        $res = $query->result_array();
        // $count = count($res );
         //echo  $count;exit;

        return $res[0]['cnt'];
    }
    function product_click_list($date_from,$date_to){
        if($date_from!='' && $date_to!=''){

            $query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,c.`company_name`,b.`price` from `users_product_click_track` as a,`product_desc` as b,brand_info as c where  a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' and a.`click_source` = 2 and a.`product_id` =b.`id` and b.`brand_id`=c.`user_id`");

        }else if($date_from=='' && $date_to==''){
            $query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,c.`company_name`,b.`price` from `users_product_click_track` as a,`product_desc` as b,brand_info as c where a.`created_datetime` like '%".date('Y-m-d')."%' and a.`click_source` = 2 and a.`product_id` =b.`id` and b.`brand_id`=c.`user_id`");

        }else if($date_from!='' && $date_to==''){
            $query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,c.`company_name`,b.`price` from `users_product_click_track` as a,`product_desc` as b,brand_info as c where a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.`click_source` = 2 and a.`product_id` =b.`id` and b.`brand_id`=c.`user_id`");

        }else if($date_from=='' && $date_to!=''){
            $query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,c.`company_name`,b.`price` from `users_product_click_track` as a,`product_desc` as b,brand_info as c where a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.`click_source` = 2 and a.`product_id` =b.`id` and b.`brand_id`=c.`user_id`");
        }

        $res = $query->result_array();
        return $res;
    }

    function notify_count_list($date_from,$date_to){
        if($date_from!='' && $date_to!=''){

            $query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,c.`company_name`,b.`price` from `users_product_click_track` as a,`product_desc` as b,brand_info as c where  a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' and a.`click_source` = 3 and a.`product_id` =b.`id` and b.`brand_id`=c.`user_id`");

        }else if($date_from=='' && $date_to==''){
            $query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,c.`company_name`,b.`price` from `users_product_click_track` as a,`product_desc` as b,brand_info as c where a.`created_datetime` like '%".date('Y-m-d')."%' and a.`click_source` = 3 and a.`product_id` =b.`id` and b.`brand_id`=c.`user_id`");

        }else if($date_from!='' && $date_to==''){   
            $query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,c.`company_name`,b.`price` from `users_product_click_track` as a,`product_desc` as b,brand_info as c where a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.`click_source` = 3 and a.`product_id` =b.`id` and b.`brand_id`=c.`user_id`");

        }else if($date_from=='' && $date_to!=''){
            $query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,c.`company_name`,b.`price` from `users_product_click_track` as a,`product_desc` as b,brand_info as c where a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.`click_source` = 3 and a.`product_id` =b.`id` and b.`brand_id`=c.`user_id`");
        }

        $res = $query->result_array();
        return $res;
    }

    function cart_count_list($date_from,$date_to){
           if($date_from!='' && $date_to!=''){

            $query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,c.`company_name`,b.`price` from `users_product_click_track` as a,`product_desc` as b,brand_info as c  where  a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' and a.`click_source` = 1 and a.`product_id` =b.`id` and b.`brand_id`=c.`user_id`");

        }else if($date_from=='' && $date_to==''){
            $query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,c.`company_name`,b.`price` from `users_product_click_track` as a,`product_desc` as b,brand_info as c  where a.`created_datetime` like '%".date('Y-m-d')."%' and a.`click_source` = 1 and a.`product_id` =b.`id` and b.`brand_id`=c.`user_id`");

        }else if($date_from!='' && $date_to==''){
            $query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,c.`company_name`,b.`price` from `users_product_click_track` as a,`product_desc` as b,brand_info as c  where a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.`click_source` = 1 and a.`product_id` =b.`id` and b.`brand_id`=c.`user_id`");

        }else if($date_from=='' && $date_to!=''){
            $query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,c.`company_name`,b.`price` from `users_product_click_track` as a,`product_desc` as b,brand_info as c  where a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.`click_source` = 1 and a.`product_id` =b.`id` and b.`brand_id`=c.`user_id`");
        }

        $res = $query->result_array();
        return $res;
    }

    public function abendant_cart_list($date_from,$date_to){
        if($date_from!='' && $date_to!=''){
            $query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,b.price,c.`company_name`,b.`price` from `cart_info` as a,`product_desc` as b,`brand_info` as c where  a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' and a.`order_id` is null and a.`product_id` =b.`id` and b.`brand_id` = c.`user_id`");

        }else if($date_from=='' && $date_to==''){
            $query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,b.price,c.`company_name`,b.`price` from `cart_info` as a,`product_desc` as b,`brand_info` as c where a.`created_datetime` like '%".date('Y-m-d')."%' and a.`order_id` is null and a.`product_id` =b.`id` and b.`brand_id` = c.`user_id`");

        }else if($date_from!='' && $date_to==''){
            $query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,b.price,c.`company_name`,b.`price` from `cart_info` as a,`product_desc` as b,`brand_info` as c where a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.`order_id` is null and a.`product_id` =b.`id` and b.`brand_id` = c.`user_id`");

        }else if($date_from=='' && $date_to!=''){
            $query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,b.price,c.`company_name`,b.`price` from `cart_info` as a,`product_desc` as b,`brand_info` as c where a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.`order_id` is null and a.`product_id` =b.`id` and b.`brand_id` = c.`user_id`");
        }
        //echo $this->db->last_query();
        $res = $query->result_array();
        return $res;
    }

    public function product_sale_info_list($date_from,$date_to){
        if($date_from!='' && $date_to!=''){
            $query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,a.product_price as price,c.`company_name` from `order_product_info` as a,`product_desc` as b,`brand_info` as c where  a.`created_datetime` >= '".$date_from."' and a.`created_datetime` <= '".$date_to."' and a.`product_id` =b.`id` and b.`brand_id` = c.`user_id`");

        }else if($date_from=='' && $date_to==''){
             $query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,a.product_price as price,c.`company_name` from `order_product_info` as a,`product_desc` as b,`brand_info` as c where  a.`created_datetime` like '%".date('Y-m-d')."%' and a.`product_id` =b.`id` and b.`brand_id` = c.`user_id` ");
            /*$query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,a.product_price as price,c.`company_name` from `order_product_info` as a,`product_desc` as b,`brand_info` as c where a.`created_datetime` like '%".date('Y-m-d')."%' and a.`product_id` =b.`id` and b.`brand_id` = c.`user_id`");
*/
        }else if($date_from!='' && $date_to==''){
            $query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,a.product_price as price,c.`company_name` from `order_product_info` as a,`product_desc` as b,`brand_info` as c where a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%' and a.`product_id` =b.`id` and b.`brand_id` = c.`user_id`");

        }else if($date_from=='' && $date_to!=''){
            $query = $this->db->query("select b.`id`,a.`user_id`,b.name,a.created_datetime,a.product_price as price,c.`company_name` from `order_product_info` as a,`product_desc` as b,`brand_info` as c where a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%' and a.`product_id` =b.`id` and b.`brand_id` = c.`user_id`");
        }
// echo $this->db->last_query();
        $res = $query->result_array();
        return $res;
    }
    
    public function remove_from_cart_list($date_from,$date_to){
        if($date_from!='' && $date_to!=''){
            $query = $this->db->query("select count(c.id) cnt,c.name,k.user_id,k.product_id,k.created_datetime,(select `company_name` from brand_info where user_id =c.brand_id) as company_name,c.price from (SELECT a.product_id,a.created_datetime,a.unique_id,b.SCUniqueID,a.`user_id`
    FROM users_product_click_track as a
        LEFT JOIN cart_info as b
            ON a.product_id = b.product_id
    WHERE b.id IS NULL and a.`created_datetime`>= '".$date_from."' and a.`created_datetime` 
                <= '".$date_to."')as k,product_desc as c where k.`product_id` = c.id ");

         

        }else if($date_from=='' && $date_to==''){
           
               $query = $this->db->query("select count(c.id) cnt,c.name,k.user_id,k.product_id,k.created_datetime,(select `company_name` from brand_info where user_id =c.brand_id) as company_name,c.price from (SELECT a.product_id,a.created_datetime,a.unique_id,b.SCUniqueID,a.`user_id`
    FROM users_product_click_track as a
        LEFT JOIN cart_info as b
            ON a.product_id = b.product_id
    WHERE b.id IS NULL and a.created_datetime like '%".date('Y-m-d')."%')as k,product_desc as c where k.`product_id` = c.id");

        }else if($date_from!='' && $date_to==''){
            $query = $this->db->query("select count(c.id) cnt,c.name,k.user_id,k.product_id,k.created_datetime,(select `company_name` from brand_info where user_id =c.brand_id) as company_name,c.price from (SELECT a.product_id,a.created_datetime,a.unique_id,b.SCUniqueID,a.`user_id`
    FROM users_product_click_track as a
        LEFT JOIN cart_info as b
            ON a.product_id = b.product_id
    WHERE b.id IS NULL and a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%')as k,product_desc as c where k.`product_id` = c.id
");


        }else if($date_from=='' && $date_to!=''){
 

           $query = $this->db->query("select count(c.id) cnt,c.name,k.user_id,k.product_id,k.created_datetime,(select `company_name` from brand_info where user_id =c.brand_id) as company_name,c.price from (SELECT a.product_id,a.created_datetime,a.unique_id,b.SCUniqueID,a.`user_id`
    FROM users_product_click_track as a
        LEFT JOIN cart_info as b
            ON a.product_id = b.product_id
    WHERE b.id IS NULL and a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%')as k,product_desc as c where k.`product_id` = c.id
");
        }
                //echo $this->db->last_query();
        $res = $query->result_array();
         //$count = count($res );
         //echo  $count;exit;

        return $res[0]['cnt'];
    }

    public function remove_from_cart_list_data($date_from,$date_to){
        if($date_from!='' && $date_to!=''){
            $query = $this->db->query("select (c.id) ,c.name,k.user_id,k.product_id,k.created_datetime,(select `company_name` from brand_info where user_id =c.brand_id) as company_name,c.price from (SELECT a.product_id,a.created_datetime,a.unique_id,b.SCUniqueID,a.`user_id`
    FROM users_product_click_track as a
        LEFT JOIN cart_info as b
            ON a.product_id = b.product_id
    WHERE b.id IS NULL and a.`created_datetime`>= '".$date_from."' and a.`created_datetime` 
                <= '".$date_to."')as k,product_desc as c where k.`product_id` = c.id ");

         

        }else if($date_from=='' && $date_to==''){
           
               $query = $this->db->query("select (c.id) ,c.name,k.user_id,k.product_id,k.created_datetime,(select `company_name` from brand_info where user_id =c.brand_id) as company_name,c.price from (SELECT a.product_id,a.created_datetime,a.unique_id,b.SCUniqueID,a.`user_id`
    FROM users_product_click_track as a
        LEFT JOIN cart_info as b
            ON a.product_id = b.product_id
    WHERE b.id IS NULL and a.created_datetime like '%".date('Y-m-d')."%')as k,product_desc as c where k.`product_id` = c.id");

        }else if($date_from!='' && $date_to==''){
            $query = $this->db->query("select (c.id) ,c.name,k.user_id,k.product_id,k.created_datetime,(select `company_name` from brand_info where user_id =c.brand_id) as company_name,c.price from (SELECT a.product_id,a.created_datetime,a.unique_id,b.SCUniqueID,a.`user_id`
    FROM users_product_click_track as a
        LEFT JOIN cart_info as b
            ON a.product_id = b.product_id
    WHERE b.id IS NULL and a.`created_datetime` like '%".str_replace('00:00:00','',$date_from)."%')as k,product_desc as c where k.`product_id` = c.id
");


        }else if($date_from=='' && $date_to!=''){
 

           $query = $this->db->query("select (c.id) ,c.name,k.user_id,k.product_id,k.created_datetime,(select `company_name` from brand_info where user_id =c.brand_id) as company_name,c.price from (SELECT a.product_id,a.created_datetime,a.unique_id,b.SCUniqueID,a.`user_id`
    FROM users_product_click_track as a
        LEFT JOIN cart_info as b
            ON a.product_id = b.product_id
    WHERE b.id IS NULL and a.`created_datetime` like '%".str_replace('00:00:00','',$date_to)."%')as k,product_desc as c where k.`product_id` = c.id
");
        }
                //echo $this->db->last_query();
        $res = $query->result_array();
         //$count = count($res );
         //echo  $count;exit;

        return $res;//[0]['cnt'];
    }
 } 
?>
