<?php
class View_orders_payment_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function getOrders($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL){   

        if($search_by == '1'){ $search_cond = " AND a.`id` = '".strip_tags(trim($table_search))."'"; }
		else if($search_by == '2'){ $search_cond = " AND a.`first_name` like '%".strip_tags(trim($table_search))."%'"; }
			else if($search_by == '3'){ $search_cond = " AND b.`company_name` like '%".strip_tags(trim($table_search))."%'"; }
			else if($search_by == '4'){ $search_cond = " AND a.`created_datetime` like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '5'){ 

				$tb_status=strtolower(strip_tags(trim($table_search)));
				if($tb_status=="pending")
				{
					$apr_status = 1;

				}else if($tb_status == "processing")
				{					
					$apr_status = 2;
				}else if($tb_status == "delivered")
				{
					$apr_status = 3;

				}else if($tb_status == "cancel")
				{
					$apr_status = 4;
				}
				$search_cond = " AND d.`order_status` = '".$apr_status."'";
			 }/*else if($search_by == '6'){ $search_cond = " AND a.id ='".strip_tags(trim($table_search))."'"; }
				else if($search_by == '7'){ $search_cond = " AND d.company_name like '%".strip_tags(trim($table_search))."%'"; }	*/			
				else{ $search_cond ='AND 1=1'; }
				

				//$search_cond="";
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}

		if($this->session->userdata('role_id') == 6 ){ 
			$search_cond = $search_cond." and a.`brand_id`=".$this->session->userdata('user_id');
		}


         $query = $this->db->query("select a.`id` as order_id,d.id as order_prd_id, a.`user_id`, a.`brand_id`, a.`order_tax_amount`, a.`cod_amount`, a.`order_sub_total`, a.`order_total`, a.`pay_mode`, a.`created_datetime`, a.`modified_datetime`, a.`order_status`, a.`first_name`, a.`last_name`, a.`email_id`, a.`mobile_no`, a.`shipping_address`, a.`pincode`, a.`city_name`, a.`state_name`,c.`state_name`,b.`company_name`, d.`product_id`, d.`product_qty`,d.`product_size`,e.`name` as product_name, d.`product_price`, d.`order_status` from order_info as a,brand_info as b,states as c, order_product_info as d, product_desc as e where a.`brand_id` = b.`user_id` and a.`state_name` = c.`id` and d.`order_id` = a.`id` and e.`id` = d.`product_id` ".$search_cond." order by a.`id` desc ".$limit_cond);
      
        $result = $query->result_array();
        return $result;
    }

    public function updateOrder($orderPrdId,$order_status)
	{
		$this->db->where('id', $orderPrdId);
		$result = $this->db->update('order_product_info', array('order_status'=>$order_status)); 
		
		if($result)
		{
			return true;
		}else
		{
			return false;
		}

	}

	public function order_status($order_status)
	{
		$query = $this->db->query("SELECT a.user_id,b.order_id,a.first_name,a.last_name,b.order_status,b.product_id,c.name,(b.`product_qty`*c.price) as price,a.created_datetime  FROM `order_info` a,order_product_info b,product_desc c where b.order_status = $order_status AND b.order_id = a.id AND b.product_id = c.id ");
		$result = $query->result_array();
		// echo "<pre>";
		// print_r($result);exit;
        return $result;
	}
}

?>