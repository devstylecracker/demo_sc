<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feedback_model extends MY_Model {
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	public function new_feedback($data)
	{	
		$this->db->insert('system_feedback', $data);
		$f_id = $this->db->insert_id();		
		return $f_id;			
	}

	
	
	/*Get All Data*/
	public function get_all_feedback($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL){
		
		if($search_by == '1'){ $search_cond = " username like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '2'){ $search_cond = " created_datetime like '%".strip_tags(trim($table_search))."%'"; }		
		else{ 
				
				$search_cond =' 1=1';
				
			}		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}		
		$query = $this->db->query("select id, username, feedback, created_by, created_datetime from system_feedback where ".$search_cond." order by created_datetime desc ".$limit_cond);				
		$res = $query->result();				
		return $res;
	}
	
	/*Get Id form datatable*/
	public function get_feedback_by_ID($id){		
		$this->db->select('*');
		$this->db->from('system_feedback');
		$this->db->where('id', $id);
				
		$feedback_result = $this->db->get();		
		
		if($feedback_result){			
			
			return $feedback_result->row();			
			
		}
	}
		
	
	/*This function update tag*/
	public function update_feedback($id,$data){
		
		$this->db->where('id', $id);
 		$updated = $this->db->update('system_feedback' ,$data);
		
		if($updated){
			
			return TRUE;
			
		}
		else
		{
			
			return FALSE;	
		}
	}
	
		
/*---------End of Common functionalities-------*/  
}

