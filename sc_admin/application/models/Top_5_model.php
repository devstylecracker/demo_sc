<?php
class Top_5_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

	function top_5_product_cpc($date_from,$date_to,$product_id){

        if($date_from==0 && $date_to==0 ){
        $query = $this->db->query("select user_id,created_datetime,click_count from users_product_click_track where product_id='".$product_id."' and created_datetime like '%".date('Y-m-d')."%' order by created_datetime desc");
        }else if($date_from!=0 && $date_to==0){
        $query = $this->db->query("select user_id,created_datetime,click_count from users_product_click_track where product_id='".$product_id."' and created_datetime like '%".$date_from."%' order by created_datetime desc"); 
        }else if($date_from!=0 && $date_to!=0){
        $query = $this->db->query("select user_id,created_datetime,click_count from users_product_click_track where product_id='".$product_id."' and created_datetime >= '".$date_from." 00:00:00' and created_datetime <='".$date_to." 23:59:59' order by created_datetime desc");    
        }
        $res = $query->result_array();
        return $res;
	}

    function top_5_looks_click($date_from,$date_to,$look_id){
           if($date_from==0 && $date_to==0 ){
        $query = $this->db->query("select user_id,created_datetime,click_count from users_look_click_track where look_id='".$look_id."' and created_datetime like '%".date('Y-m-d')."%' order by created_datetime desc");
        }else if($date_from!=0 && $date_to==0){
        $query = $this->db->query("select user_id,created_datetime,click_count from users_look_click_track where look_id='".$look_id."' and created_datetime like '%".$date_from."%' order by created_datetime desc"); 
        }else if($date_from!=0 && $date_to!=0){
        $query = $this->db->query("select user_id,created_datetime,click_count from users_look_click_track where look_id='".$look_id."' and created_datetime >= '".$date_from." 00:00:00' and created_datetime <= '".$date_to." 23:59:59' order by created_datetime desc");    
        }
        $res = $query->result_array();
        return $res;
    }

    function top_5_product_cpa($date_from,$date_to,$product_id){
        if($date_from==0 && $date_to==0 ){
        $query = $this->db->query("select order_id,email,created_datetime from product_cpa_track where product_id='".$product_id."' and created_datetime like '%".date('Y-m-d')."%' order by created_datetime desc");
        }else if($date_from!=0 && $date_to==0){
        $query = $this->db->query("select order_id,email,created_datetime from product_cpa_track where product_id='".$product_id."' and created_datetime like '%".$date_from."%' order by created_datetime desc"); 
        }else if($date_from!=0 && $date_to!=0){
        $query = $this->db->query("select order_id,email,created_datetime from product_cpa_track where product_id='".$product_id."' and created_datetime >= '".$date_from." 00:00:00' and created_datetime <='".$date_to." 23:59:59' order by created_datetime desc");    
        }
        $res = $query->result_array();
        return $res;
    }

    function top_5_brands_cpc($date_from,$date_to,$brand_id){
        if($date_from==0 && $date_to==0 ){
        $query = $this->db->query("select a.user_id,a.created_datetime,a.click_count,a.product_id from users_product_click_track as a,`product_desc` as b where a.product_id=b.id and a.created_datetime like '%".date('Y-m-d')."%' and b.`brand_id` = '".$brand_id."' order by a.created_datetime desc");
        }else if($date_from!=0 && $date_to==0){
        $query = $this->db->query("select a.user_id,a.created_datetime,a.click_count,a.product_id from users_product_click_track as a,`product_desc` as b where a.product_id=b.id and a.created_datetime like '%".$date_from."%' and b.`brand_id` = '".$brand_id."' order by a.created_datetime desc"); 
        }else if($date_from!=0 && $date_to!=0){
        $query = $this->db->query("select a.user_id,a.created_datetime,a.click_count,a.product_id from users_product_click_track as a,`product_desc` as b where a.product_id=b.id and a.created_datetime >= '".$date_from." 00:00:00' and a.created_datetime <='".$date_to." 23:59:59' and b.`brand_id` = '".$brand_id."' order by a.created_datetime desc");    
        }
        $res = $query->result_array();
        //echo $this->db->last_query();
        return $res;
    } 
 } 
?>
