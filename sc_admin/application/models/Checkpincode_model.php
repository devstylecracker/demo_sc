<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Checkpincode_model extends MY_Model {
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	public function getpincode()
	{	
		$this->db->select('pincode');

		$query = $this->db->get('pincode');
		return $query->result_array();

	}

	

}

