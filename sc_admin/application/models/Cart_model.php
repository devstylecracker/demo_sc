<?php
class Cart_model extends MY_Model {
	
	private $brandwise_cost = array();
	private $coupon_code = '';
    function __construct(){
        parent::__construct();
    }	
	
	function update_cart($uniquecookie,$product_id,$user_id,$productsize,$agent,$platform_info,$product_price,$scbox_order_id,$paymod,$is_scbox){

        $time = new DateTime(date('Y-m-d H:i:s'));
        $time->add(new DateInterval('PT330M'));
        $dateM = $time->format('Y-m-d H:i:s');


        if($product_id!='' && $productsize!='' && $user_id!=''){
			$query = $this->db->query("select * from cart_info where (`SCUniqueID` = '".$uniquecookie."' and user_id='".$user_id."' and `product_id` = '".$product_id."' and `product_size` = '".$productsize."' and `order_id` is null )");
			$res = $query->result_array();
			if(empty($res)){
				$data = array(
				   'product_id' => $product_id,
				   'user_id' => $user_id,
				   'created_datetime' => $dateM,
				   'product_size' => $productsize,
				   'product_qty' => '1',
				   'SCUniqueID' => $uniquecookie,
				   'agent' =>$agent,
				   'platform_info'=>$platform_info,
				   'product_price'=>$product_price,
				   'created_by' => $user_id,
				   'scbox_order_id' => $scbox_order_id
				);
				$this->db->insert('cart_info', $data);
			}else{
				if($res[0]['product_qty']<5){
					$data = array(
						'product_id' => $product_id ,
						'user_id' => $user_id,
						'created_datetime' => $dateM,
						'product_size' => $productsize,
						'product_qty' => $res[0]['product_qty']+1,
						'SCUniqueID' => $uniquecookie,
						'agent' =>$agent,
						'product_price'=>$product_price,
						'modified_by' => $user_id,
						'scbox_order_id' => $scbox_order_id
					);

					$this->db->where(array('id'=>$res[0]['id'],'product_id' => $product_id,'SCUniqueID' => $uniquecookie));
					$this->db->update('cart_info', $data);
				}
			}
           
        }
	}
	
	function get_cart($uniquecookie,$user_id,$paymod,$coupon_code,$is_scbox,$sc_credit,$scbox_order_id=''){
		$cart_array = array();$scbox_product_condtion = ' ';
		//added for scbox products
		if($is_scbox == '1'){
			$scbox_product_condtion = $scbox_product_condtion.' AND a.`product_id` IN ('.SCBOX_PRODUCTID_STR.') ';  
		}else{
			$scbox_product_condtion = $scbox_product_condtion.' AND a.`product_id` NOT IN ('.SCBOX_PRODUCTID_STR.') ';  
		}
		if($scbox_order_id != ''){
			$scbox_product_condtion = $scbox_product_condtion.' AND a.scbox_order_id = "'.$scbox_order_id.'" ';
		}
		//added for scbox products
		if($user_id > 0){
			$cart_condtion = ' a.`user_id`="'.$user_id.'" ' ;
		}else{
			$cart_condtion = ' a.`user_id`="'.$user_id.'" OR a.SCUniqueID = "'.$uniquecookie.'" ' ;
		}
		$query = $this->db->query('select a.`id` as cart_id,a.`product_id`,a.`product_qty`,a.`product_size` ,a.SCUniqueID ,a.created_datetime,b.`name` ,(a.`product_qty`*a.product_price) as price ,b.image ,b.brand_id ,c.`size_text`,c.id as size_id,d.store_tax ,d.shipping_charges ,d.shipping_min_values ,d.shipping_max_values,d.is_cod,d.`cod_charges`,d.`cod_min_value`,d.`code_max_value` ,d.company_name as  seller from `cart_info` as a,`product_desc` as b,`product_size` as c,`brand_info` as d where a.`order_id` is null and a.`product_id` = b.`id` and c.`id`= a.`product_size` and b.`brand_id` = d.`user_id` and ('.$cart_condtion.') '.$scbox_product_condtion.' order by b.`brand_id` DESC');
        $cart_items = $query->result_array();
		$cart_array['product_object'] = $this->cart_product_object($cart_items);
		$cart_array['total_summary'] = $this->total_summary($cart_array['product_object'],$user_id,$paymod,$coupon_code,$sc_credit,$is_scbox);
		// echo "<pre>";print_r($cart_array);exit;
		return $cart_array;
		
	}
	
	function cart_product_object($cart_items){
		$product_array = array();$i=0;$imagify_brandid = unserialize(IMAGIFY_BRANDID);$productUrl = '';
		foreach($cart_items as $val){
			$product_sizes = $this->product_sizes($val['product_id']);
			if(count($product_sizes) > 0){ $is_available = '1'; }else{ $is_available = '0'; }
			$product_array[$i]['cart_id'] = $val['cart_id'];
			$product_array[$i]['product_id'] = $val['product_id'];
			$product_array[$i]['product_name'] = $val['name'];
			$product_array[$i]['product_qty'] = $val['product_qty'];
			// added for new product url  
			// if(in_array($val['brand_id'],$imagify_brandid)){
				// $productUrl = $this->config->item('products_tiny').$val['image'];
			// }else{
				// $productUrl = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_124/'.$val['image'];                
			// }
			$productUrl = $this->config->item('products_tiny').$val['image'];
			if(!file_exists($productUrl)){
				$productUrl = $this->config->item('products_zoom').$val['image'];
			}
			// added for new product url
			$product_array[$i]['image'] = $productUrl;
			$product_array[$i]['price'] = $val['price'];
			$product_array[$i]['size_text'] = $val['size_text'];
			$product_array[$i]['store_tax'] = $val['store_tax'];
			$product_array[$i]['shipping_charges'] = $val['shipping_charges'];
			$product_array[$i]['shipping_min_values'] = $val['shipping_min_values'];
			$product_array[$i]['shipping_max_values'] = $val['shipping_max_values'];
			$product_array[$i]['is_cod'] = $val['is_cod'];
			$product_array[$i]['cod_charges'] = $val['cod_charges'];
			$product_array[$i]['cod_min_value'] = $val['cod_min_value'];
			$product_array[$i]['code_max_value'] = $val['code_max_value'];
			$product_array[$i]['product_sizes'] = $product_sizes;
			$product_array[$i]['is_available'] = $is_available;
			$brand_discount = $this->brand_discount($val['brand_id']);
			$product_discount = $this->product_discount($val['product_id']);
			if(!empty($brand_discount)){
				$product_array[$i]['discount_percent'] = $brand_discount['discount_percent'];
				$product_array[$i]['discount_price'] = (string)($val['price'] - ($val['price'] * ($brand_discount['discount_percent']/100)));
			}else if(!empty($product_discount)){
				$product_array[$i]['discount_percent'] = $product_discount['discount_percent'];
				$product_array[$i]['discount_price'] = (string)($val['price'] - ($val['price'] * ($product_discount['discount_percent']/100)));
			}else {
				$product_array[$i]['product_discount'] = '0';
				$product_array[$i]['discount_price'] = $val['price'];
			}
			$product_array[$i]['brand_id'] = $val['brand_id'];
			$product_array[$i]['seller'] = $val['seller'];
			$product_array[$i]['size_id'] = $val['size_id'];
			$product_array[$i]['dateadded'] = $val['created_datetime'];
			$this->brandwise_cost[$val['brand_id']][] = $product_array[$i]['discount_price'];
			$i++;
		}
		// echo "<pre>";print_r($product_array);
		return $product_array;
	}
	
	function product_sizes($product_id){
		$query = $this->db->query('SELECT a.product_id,b.size_text,b.id,a.stock_count FROM product_inventory AS a, product_size b WHERE a.product_id = '.$product_id.' AND a.size_id = b.id AND a.stock_count > 0 ');
		$res = $query->result_array();
		return $res;
	}
	
	function brand_discount($brand_id){
		$query = $this->db->query(" select IF(a.discount_percent is null, '0', a.discount_percent) as discount_percent FROM discount_availed AS a WHERE a.brand_id = $brand_id AND a.discount_type_id = 1 AND NOW() >= a.start_date AND NOW() <= a.end_date AND a.status=1 ");
		$res = $query->result_array();
		if (!empty($res)) return $res[0]; else return null;
	}
	
	function product_discount($product_id){
		$query = $this->db->query("SELECT  IF(sum(a.discount_percent) is null, '0', sum(a.discount_percent)) as discount_percent FROM discount_availed AS a WHERE a.product_id = $product_id AND a.discount_type_id = 2 AND NOW() >= a.start_date AND NOW() <= a.end_date AND a.status=1 ");
		$res = $query->result_array();
		if (!empty($res)) return $res[0]; else return null;
	}
	
	function total_summary($cart_items,$userid,$paymod,$coupon_code,$sc_credit,$is_scbox){
		$total_summary = array();
		// echo "<pre>";print_r($this->brandwise_cost);exit;
		$coupon_discount = $this->get_coupon_discount($coupon_code,$cart_items,$is_scbox);
		$cod_charges = 0;$shipping_charges = 0;$tax_charges = 0;$total_product_price = 0;
		$brandwise_cost = $this->brandwise_cost;
		if(!empty($brandwise_cost)){
			foreach($brandwise_cost as $key=>$v){
				$brand_extra_info = $this->getBrandExtraInfo($key);
				// echo "<pre>";print_r($brand_extra_info);exit;
				$brand_product_cost =  array_sum($brandwise_cost[$key]);
				/* COD calculation start */
				if($paymod == 'cod'){
					if($brand_extra_info[0]['cod_min_value'] == 0 && $brand_extra_info[0]['code_max_value'] == 0){
						$cod_charges = $cod_charges + $brand_extra_info[0]['cod_charges'];
					}else if($brand_extra_info[0]['cod_min_value'] > 0 && $brand_extra_info[0]['code_max_value'] > 0 && $brand_product_cost > $brand_extra_info[0]['cod_min_value'] && $brand_product_cost < $brand_extra_info[0]['code_max_value']){
						$cod_charges = $cod_charges + $brand_extra_info[0]['cod_charges'];
					}
				}
				/* COD calculation end */
				
				/* shipping calculation start */
				if($paymod == 'cod' || $paymod == 'online'){
					if($brand_extra_info[0]['shipping_min_values'] < 0){
						$shipping_charges = $shipping_charges + $brand_extra_info[0]['shipping_charges'];
					}else if($brand_extra_info[0]['shipping_min_values'] > 0 && $brand_extra_info[0]['shipping_max_values'] > 0 && $brand_product_cost > $brand_extra_info[0]['shipping_min_values'] && $brand_product_cost < $brand_extra_info[0]['shipping_max_values'] ){
						$shipping_charges = $shipping_charges + $brand_extra_info[0]['shipping_charges'];
					}
				}
				/* shipping calculation end */
				/* tax calculation start */
				if($key == SCBOX_BRAND_ID){
					$tax_payble_amount = $brand_product_cost - $coupon_discount;
					$tax_charges = ($brand_extra_info[0]['store_tax'] / 100) * $tax_payble_amount;
				}else{
					$store_tax = ($brand_extra_info[0]['store_tax'] / 100) * $brand_product_cost;
					$tax_charges = $tax_charges + $store_tax;
				}
				/* tax calculation end */
				$total_product_price = $total_product_price + $brand_product_cost;
			}	
		}
		$order_total = $total_product_price + $cod_charges + $shipping_charges + $tax_charges - $coupon_discount;
		$total_summary['cod_charges'] = $cod_charges;
		$total_summary['shipping_charges'] = $shipping_charges;
		$total_summary['tax_charges'] = $tax_charges;
		$total_summary['coupon_discount'] = $coupon_discount;
		$total_summary['order_total'] = $order_total;
		return $total_summary;
	}
	
	function getBrandExtraInfo($brand_id){
        $query = $this->db->get_where('brand_info', array('user_id' => $brand_id));
        $res = $query->result_array();
        if(!empty($res)){
            return $res;
        }else{
            return '';
        }
    }
	
	// get the value of coupon discount
	function get_coupon_discount($coupon_code,$cart_items,$is_scbox){
		$coupon_data = $this->getCouponData($coupon_code);
		// echo "<pre>";print_r($coupon_data);exit;
		$coupon_brand = $coupon_data['brand_id'];
		$coupon_min_spend = $coupon_data['coupon_min_spend'];
		$coupon_max_spend = $coupon_data['coupon_max_spend'];
		$coupon_products = $coupon_data['coupon_products']; 
		$coupon_discount_type = $coupon_data['coupon_discount_type'];
		$is_special_code = $coupon_data['is_special_code'];
		if($is_scbox == '1' && $is_special_code != '1'){
			return '0';
		}else if($is_scbox == '2' && $is_special_code == '1'){
			return '0';
		}
		$cart_items = (array)$cart_items;
		if($coupon_products!='' || $coupon_products!=0){
			$coupon_products_arr = explode(',',$coupon_products);
		}
		$coupon_brandsSel = '';
		$coupon_productSel = '';
		$coupon_discount = 0;
		$coupon_charges_text = '';
		$coupon_product_price = 0;
		foreach($cart_items as $value){
			// echo "<pre>";print_r($value);exit;
			$new_brand_id = $value['brand_id'];
			$new_brand_name = $value['seller'];
			if($coupon_brand==$new_brand_id || $coupon_brand==0 )
			{        
			  if($coupon_products!=''&& $coupon_products!=0)
			  {
				/* coupon_discount_type =3 (Product discount)*/
				if($coupon_discount_type==3)
				{
				  if(in_array($value['product_id'], $coupon_products_arr))
				  {              
					if($coupon_min_spend<=$value['price'] && $value['price']<=$coupon_max_spend)
					{
					  $coupon_discount = $this->getCouponData($coupon_code,$value['brand_id'])['coupon_amount'];
					  $coupon_charges_text =  $new_brand_name;
					} 
				  }
				} 
				/* coupon_discount_type =4 (Product % discount)*/
				if($coupon_discount_type==4)
				{
				  if(in_array($value['product_id'], $coupon_products_arr))
				  {              
					if($coupon_min_spend<=$value['price'] && $value['price']<=$coupon_max_spend)
					{
					  $coupon_percent = $this->getCouponData($coupon_code,$value['brand_id'])['coupon_amount'];
					  $coupon_discount = $value['price']*($coupon_percent/100);
					  $coupon_charges_text =  $new_brand_name;
					} 
				  }
				}                 

			  }else
			  { 
				/* coupon_discount_type =1 (cart discount)*/
				if($coupon_discount_type==1)
				{
				  $coupon_product_price = $coupon_product_price+$value['price'];
				  if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
				  {
					$coupon_discount = $this->getCouponData($coupon_code,$value['brand_id'])['coupon_amount'];
					$coupon_charges_text =  $new_brand_name;
				  }
				}
				/* coupon_discount_type =2 (cart % discount)*/
				if($coupon_discount_type==2)
				{
				  $coupon_product_price = $coupon_product_price+$value['price'];
				  if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
				  {
					$coupon_percent = $this->getCouponData($coupon_code,$value['brand_id'])['coupon_amount'];
					$coupon_discount = $coupon_product_price*($coupon_percent/100);
					$coupon_charges_text =  $new_brand_name;
				  }
				}
			  }          
			}//end of if
		}//end foreach
		
		return $coupon_discount;
	}
	
	function getCouponData($couponCode,$CbrandId=NULL){
		
		if($CbrandId!='') {
          $brand_cond =' AND (brand_id = "'.$CbrandId.'" || brand_id=0) ';
        }else{
          $brand_cond = '';
        }
		
        $query = $this->db->query("SELECT `coupon_code`, `discount_type`, `coupon_amount`, `allow_free_ship`, `coupon_start_datetime`, `coupon_end_datetime`, `min_spend`, `max_spend`, `brand_id`, `products`, `exclude_products`,`coupon_used_count`,`usage_limit_per_coupon`,is_special_code FROM `coupon_settings` WHERE NOW() BETWEEN `coupon_start_datetime` AND `coupon_end_datetime` AND`status`=1 AND `is_delete`=0 AND `usage_limit_per_coupon`>`coupon_used_count` AND `coupon_code`='".$couponCode."' ".$brand_cond." "); 
		// return $this->db->last_query();
		$result = $query->result_array();
	   
		if(!empty($result))
		{
		   $coupon_info['coupon_amount'] = $result[0]['coupon_amount']; 
		   $coupon_info['brand_id'] = $result[0]['brand_id']; 
		   $coupon_info['coupon_products'] = $result[0]['products'];
		   $coupon_info['coupon_min_spend'] = $result[0]['min_spend'];
		   $coupon_info['coupon_max_spend'] = $result[0]['max_spend'];
		   $coupon_info['coupon_discount_type'] = $result[0]['discount_type'];
		   $coupon_info['coupon_used_count'] = $result[0]['coupon_used_count'];
		   $coupon_info['usage_limit_per_coupon'] = $result[0]['usage_limit_per_coupon'];
		   $coupon_info['is_special_code'] = $result[0]['is_special_code'];
		}  

		if(!empty($coupon_info))
		{
		  return $coupon_info;
		}else
		{
			return 0;
		}               
    }
	
	function scremovecart($cartid,$user_id){
        if($cartid > 0){
            $this->db->delete('cart_info', array('id' => $cartid,'user_id' => $user_id)); 
        }
    }

    function get_orderDetailBycart($cartid){

	 	$this->db->select('ci.id,ci.product_id,ci.user_id,ci.product_price,oi.order_unique_no,oi.order_display_no');
	 	$this->db->from('cart_info as ci');
	 	$this->db->join('order_info as oi', 'ci.order_id = oi.id');	 
	 	$this->db->where('ci.id',$cartid);
	 	$this->db->order_by('ci.id','DESC');
	 	$query = $this->db->get();		
		if($query->num_rows()>0)
		{
			//echo $this->db->last_query();exit;
			//echo '<pre>';print_r($query->result_array());exit;
			return $query->result_array();
		}
		return false;
	}
	
}