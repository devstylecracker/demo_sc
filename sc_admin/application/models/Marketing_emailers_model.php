<?php
class Marketing_emailers_model extends MY_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }  

    function get_deliveredOrders_spandays($order_display_no=null,$span=null)
    {
        $result = array();
        if($order_display_no!='')
        {
          $cond = " and a.order_display_no = '".$order_display_no."'";  
        }else
        {
          $cond = '';
        }

        if($span!='')
        {
          $spancond = " and DATEDIFF(NOW(),b.modified_datetime) = ".$span." ";  
        }else
        {
          $spancond = '';
        }
       
        if($spancond!='')
        {
            $query = $this->db->query("Select a.user_id,a.order_unique_no,a.order_display_no,a.order_total,a.created_datetime,b.order_status,a.coupon_code,a.coupon_amount,a.first_name,a.last_name,a.email_id,a.mobile_no,a.shipping_address,a.pincode,a.city_name,b.product_price,b.modified_datetime,DATEDIFF(NOW(),b.modified_datetime) as datediff from order_info as a, order_product_info as b where a.id = b.order_id and (b.order_status=4) ".$spancond."  and a.created_datetime > '2017-12-1 00:00:00' and a.brand_id IN (".SCBOX_BRAND_ID.") ".$cond." ");
            //echo $this->db->last_query();exit;
            $result = $query->result_array();
        }        
        return $result;
    }
   
}

?>