<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products_import_model extends MY_Model {
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	public function get_product_id($product_url)
	{	
		$query = $this->db->get_where('product_desc', array('url' => $product_url));
		$res = $query->result_array();
		if(!empty($res)){ return $res[0]['id']; }else{ return 0; }

	}

	public function add_product($data){
		
		$this->db->insert('product_desc', $data); 
	}

	public function update_price($product_url,$price){
		$data = array(
               'price' => $price
            );

		$this->db->where('url', $product_url);
		$this->db->update('product_desc', $data); 
	}

	public function update_description($product_url,$description){
		$data = array(
               'description' => $description
            );

		$this->db->where('url', $product_url);
		$this->db->update('product_desc', $data); 

	}

	public function update_product_sku($product_url,$product_sku){
		$data = array(
               'product_sku' => $product_sku
            );

		$this->db->where('url', $product_url);
		$this->db->update('product_desc', $data); 

	}

	public function update_quantity($product_url,$quantity,$size){
		$product_id = $this->get_product_id($product_url);
		$sizes = explode('|', $size);
		if(!empty($sizes) && $size >0 ){
			foreach($sizes as $val){
				if($quantity == 0){ $quantity =1; }
				$this->check_set_size($product_id,$val,$quantity);
			}
		}else{
			$this->check_set_size($product_id,'',$quantity);
		}

	}

	public function check_set_size($product_id,$size,$quantity){
		if($size == ''){ $size = 'Free'; }
		$size_id = $this->get_size_id($size);
		$query = $this->db->get_where('product_inventory', array('product_id' => $product_id));
		$res = $query->result_array();
		if(!empty($res)){ 

			$inv_id = $res[0]['id']; 
		}

		else{ 
			$data = array(
				'product_id' => $product_id,
				'size_id' => $size_id,
				'stock_count' =>$quantity
				);

			$this->db->insert('product_inventory', $data); 
//			echo $this->db->last_query(); echo '<br>';
		}
	}

	public function get_size_id($size){
        if($size!=''){
            $query = $this->db->query("select count(*) as cnt,id from product_size where size_text = '".strtoupper($size)."'");
            $result = $query->result_array();
            if($result[0]['cnt'] == 0){
                $data = array(
                    'size_text' => strtoupper($size) ,
                );
                $this->db->insert('product_size', $data); 
                return $this->db->insert_id();
            }else {
                return $result[0]['id']; 
            }
        }
    }

}

