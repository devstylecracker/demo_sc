<?php
class Product_delete_model extends CI_Model {
    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
   
   	public function getproduct_todelete()
   	{
   		$size_id = '1,2,12,13,44,118,119,122,124,126,127,128,129,130,141,168,228,308,310,587,589,817,960,1073,1077,1086,1091,1095,1136,1137,1138,1170,1238,1245,1246,1247,1249,1252,1255,1262,1263,1266,1513,1514,1515,1517,1518,1519,1520,1521,1522,1533,1563,1564,1565,1566,1584,1585,1587';
   		$not_delete_brand = '1,2,12,13,44,118,119,122,124,126,127,128,129,130,141,168,228,308';

   		// $query = $this->db->query('SELECT a.`id`,a.`brand_id`,(Select `company_name` from `brand_info` where `user_id` =a.`brand_id`) as brand_name FROM `product_desc` as a WHERE a.`is_delete`=1 and a.`brand_id` NOT IN ('.$not_delete_brand.')  and a.`id` IN(Select distinct(product_id) from `product_inventory` where `size_id` IN (1138,1170,1238,1245,1246,1247,1249,1252,1255,1262,1263,1266,1513,1514,1515,1517,1518,1519,1520,1521,1522,1533,1563,1564,1565,1566,1584,1585,1587) ) order by a.`id`,a.`brand_id`');
   		$query = $this->db->query('SELECT a.`id`,a.`brand_id`,(Select `company_name` from `brand_info` where `user_id` =a.`brand_id`) as brand_name FROM `product_desc` as a WHERE a.`status`=0 and a.`brand_id` NOT IN ('.$not_delete_brand.')  and a.`id` IN(Select distinct(product_id) from `product_inventory` where `size_id` IN (1) ) order by a.`id`,a.`brand_id` limit 0,2');
   		$result = $query->result_array();
   		return $result;
   	}

   	public function getproduct_todelete_status()
   	{   		
   		/*$not_delete_brand = '20734,20933,20936,20956,20963,21441,21792,23744,25221,25314,25450,27140,28188,28363,28374,29784,33592,34640,34917,35678,43183,43319,43334';*/
   		$brand_id = '29784';		

   		//$query = $this->db->query('SELECT a.`id`,a.`brand_id` FROM `product_desc` as a WHERE a.`status`=0 and a.`brand_id` IN ('.$brand_id.') order by a.`id`,a.`brand_id` ');
		$query = $this->db->query('SELECT a.`id`,a.`brand_id` FROM `product_desc` as a WHERE a.`brand_id` IN ('.$brand_id.') order by a.`id`,a.`brand_id` ');
   		/*$product_id = '188672,188671';
   		$query = $this->db->query('SELECT a.`id`,a.`brand_id` FROM `product_desc` as a WHERE a.`status`=0 and a.`id` IN ('.$product_id.') order by a.`id`,a.`brand_id` ');*/

   		
   		$result = $query->result_array();
   		return $result;
   	}

	public function delete_productforever($delete_id){						
		
		$this->db->where('product_id', $delete_id);
		$delvarRes = $this->db->delete('extra_images'); 
			
		$this->db->where('product_id', $delete_id);
		$delvarRes = $this->db->delete('product_tag'); 
				
		$this->db->where('product_id', $delete_id);
		$delvarRes = $this->db->delete('product_inventory'); 

		$this->db->where('object_id', $delete_id);
		$delvarRes = $this->db->delete('category_object_relationship');
				
		$this->db->where('object_id', $delete_id);
		$delvarRes = $this->db->delete('attribute_object_relationship'); 
			
		$this->db->where('id', $delete_id);
		$delvarRes = $this->db->delete('product_desc');
		return $delvarRes; 		
		
	}

	public function check_productinorder($pid)
	{

		$this->db->where('product_id',$pid);
	    $query = $this->db->get('order_product_info');
	    if ($query->num_rows() > 0){
	        return true;
	    }
	    else{
	        return false;
	    }		
	}	

	public function getproduct_image($pid)
	{
		$query = $this->db->query('SELECT a.`image` FROM `product_desc` as a WHERE  a.`id`='.$pid.';');   		
   		$result = $query->result_array();
	    if ($result){
	        return $result[0]['image'];
	    }
	    else{
	        return false;
	    }		
	}

	public function getproduct_extraimage($pid)
	{
		$query = $this->db->query('SELECT a.`product_images` FROM `extra_images` as a WHERE  a.`product_id`='.$pid.';');   		
   		$result = $query->result_array();
	    if ($result){
	        return $result;
	    }
	    else{
	        return false;
	    }		
	}

	function checkImageInDB($image_name)
   	{   	
   		$this->db->select('id');
		$this->db->from('product_desc');		
		$this->db->where_in('image', $image_name);		
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if($query->num_rows() > 0)
        {
            $result = $query->row_array();
            //echo '<pre>';print_r($result);exit;
            return $result['id'];
        }
        return false;		
   	}

   	function checkExtraImageInDB($image_name)
   	{   	
   		$this->db->select('product_id');
		$this->db->from('extra_images');		
		$this->db->where_in('product_images', $image_name);		
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if($query->num_rows() > 0)
        {
            $result = $query->row_array();
            //echo '<pre>';print_r($result);exit;
            return $result['product_id'];
        }
        return false;		
   	}						
 } 
?>
