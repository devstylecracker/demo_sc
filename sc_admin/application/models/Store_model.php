<?php
class Store_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function add_new_store($store_info){
		
			$this->db->insert('brand_store', $store_info['store_info']);
			$store_id = $this->db->insert_id();
			if(!empty($store_info['target_market'])){
				foreach($store_info['target_market'] as $val){
						if(!empty($val)){
						foreach($val as $vall){
							$target_market[] = array('store_id'=>$store_id,'target_market_id'=>$vall,'status'=>1);
						}
						}
					}
			 	if(!empty($target_market)){ 
				$this->db->insert_batch('store_target_market_assoc', $target_market); 
				}
			}
			
			if(!empty($store_info['brand'])){
				$this->db->delete('store_brand_mapping', array('store_id' => $store_id)); 
				$target_market  = array(); 
				foreach($store_info['brand'] as $val){  
						foreach($val as $vall){
							$target_market[] = array('store_id'=>$store_id,'brand_id'=>$vall);
						}
					}
			  
				$this->db->insert_batch('store_brand_mapping', $target_market); 
			}
		}
	
	function get_store_data($search_by,$table_search,$page,$per_page,$brand_id){
		
		if($search_by == '1'){ $search_cond = " AND brand_store_name like '%".strip_tags(trim($table_search))."%'"; }
		
		else{ $search_cond =''; }
		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}
		
		
			$query = $this->db->query("select * from brand_store where brand_id=".$brand_id." and status =1 ".$search_cond." order by id desc ".$limit_cond); 
			return $query->result_array();
		}
	
	function get_store_info($store_id=NULL){

		/*

		$this->db->select('*');
		$this->db->from('brand_store');
		if($store_id!="")
		{
			$this->db->where('id', $catID);

		}else
		{

		}		
		
		$store = $this->db->get(); 
		
		if($store)
		{			
			return $store->result();
			
		}*/
			$query = $this->db->get_where('brand_store', array('id' => $store_id));
			return $query->result_array();
		}
  
    function edit_new_store($store_edit_info,$store_id){
		
			$this->db->where('id', $store_id);
			$this->db->update('brand_store', $store_edit_info['store_edit_info']); 
			/*if(!empty($store_edit_info['target_market'])){
				$this->db->delete('store_target_market_assoc', array('store_id' => $store_id)); 
				$target_market  = array(); 
				if(!empty($store_edit_info['target_market'])){
				foreach($store_edit_info['target_market'] as $val){  
					if(!empty($val)){
						foreach($val as $vall){
							$target_market[] = array('store_id'=>$store_id,'target_market_id'=>$vall,'status'=>1);
						}
					}
					}
			  
				$this->db->insert_batch('store_target_market_assoc', $target_market); 
			} }*/
			
			if(!empty($store_edit_info['brand'])){
			$this->db->delete('store_brand_mapping', array('store_id' => $store_id)); 
			$target_market  = array(); 
			if(!empty($store_edit_info['brand'])){
			foreach($store_edit_info['brand'] as $val){  
				if(!empty($val)){
					foreach($val as $vall){
						$target_market[] = array('store_id'=>$store_id,'brand_id'=>$vall);
					}
				}
				}
			}
		  
				$this->db->insert_batch('store_brand_mapping', $target_market); 
			}
			
			
			return true;
		}
		
	function delete_store($store_id,$user_id){
			if($store_id){
				$data = array('status'=>0,"modified_by"=>$user_id,"modified_datetime"=>date('Y-m-d H:i:s'));
				$this->db->where('id', $store_id);
				$this->db->update('brand_store', $data);
			}
		
		}
		
	function store_target_market_assoc($store_id){
			/*$query = $this->db->get_where('store_target_market_assoc', array('id' => $store_id));
			$this->db->select('*');
			$this->db->from('store_target_market_assoc');
			$this->db->join('target_market', 'store_target_market_assoc.target_market_id = target_market.id and store_target_market_assoc.store_id ='.$store_id);
			$query = $this->db->get();
			return $query->result_array();*/
			return true;
		}
	
	function all_brands(){
			//$query = $this->db->get_where('brand_master', array('is_delete' => 0));
			$user_id = $this->session->userdata('user_id');
			if($this->session->userdata('role_id') == 6){
				$query = $this->db->query("select a.id,a.user_name, a.bucket, a.created_datetime, a.email,c.company_name from user_login as a, user_role_mapping as b,brand_info as c WHERE c.`status` =1 and a.`id` = b.`user_id` and b.`role_id`=6 and c.user_id = a.id and a.id = '".$user_id."' and c.user_id = b.user_id order by a.id desc");
			}else{
				$query = $this->db->query("select a.id,a.user_name, a.bucket, a.created_datetime,a.email,c.company_name from user_login as a, user_role_mapping as b,brand_info as c WHERE c.`status` =1 and a.`id` = b.`user_id` and b.`role_id`=6 and c.user_id = a.id  and c.user_id = b.user_id  order by a.id desc");
			}
			/*$query = $this->db->query("select a.id,a.user_name, a.bucket, a.created_datetime, a.email from user_login as a, user_role_mapping as b WHERE a.`status` =1 and a.`id` = b.`user_id` and b.`role_id`=6 order by a.id desc");*/
			return $query->result_array();
		}
		
	function all_storewise_brands($store_id){
			$query = $this->db->get_where('store_brand_mapping', array('store_id' => $store_id));
			return $query->result_array();
		}

 } 
?>
