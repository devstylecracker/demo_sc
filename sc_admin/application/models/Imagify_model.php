<?php

class Imagify_model extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
   

	/*-- Uploading Product Data through Excel Sheet to the database --*/	

	function get_image_names($product_id){
		$query = $this->db->query('select a.product_images,a.product_id from extra_images as a where a.product_id = '.$product_id.' ');
		$result = $query->result_array();
		return $result;
	}
	
	function get_product_data($brand_ids){
		// this will get product_images 
		$query = $this->db->query('select DISTINCT a.image,a.brand_id,a.id,@type:="product_img" as img_type from product_desc as a where a.brand_id IN ('.$brand_ids.') ');
		$result = $query->result_array();
		
		// this will get product extra images 
		$query2 = $this->db->query('select DISTINCT b.product_images as image,a.brand_id,a.id,@type:="product_extra_img" as img_type from product_desc as a,extra_images as b where a.id = b.product_id AND a.brand_id IN ('.$brand_ids.') ');
		$result2 = $query2->result_array();
		
		$data = array_merge($result,$result2);
		
		return $data;
	}
	
	function get_product_data2($brand_ids){
		// this will get product_images 
		$query = $this->db->query('select concat("https://www.stylecracker.com/sc_admin/assets/products/",a.image) as image,a.brand_id,a.id,@type:="product_img" as img_type from product_desc as a where a.brand_id IN ('.$brand_ids.') ');
		$result = $query->result_array();
		
		// this will get product extra images 
		$query2 = $this->db->query('select concat("https://www.stylecracker.com/sc_admin/assets/products_extra/",b.product_images) as image,a.brand_id,a.id,@type:="product_extra_img" as img_type from product_desc as a,extra_images as b where a.id = b.product_id AND a.brand_id IN ('.$brand_ids.') ');
		$result2 = $query2->result_array();
		
		$data = array_merge($result,$result2);
		
		return $data;
	}
	
}	