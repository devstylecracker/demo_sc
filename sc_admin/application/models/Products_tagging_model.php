<?php
class Products_tagging_model extends CI_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
   
	/*---------Product Details functionalities-------*/

	public function get_products($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL){

		if($search_by == 'store'){ $search_cond = " AND a.`brand_id` = '".$table_search."'"; }				
				else{ $search_cond ='AND 1=1'; }
		
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0)
			{
                   $page = $page*$per_page-$per_page;
            }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}			

		if($this->session->userdata('role_id') == 6 ){ 
			$search_cond = $search_cond." and a.created_by=".$this->session->userdata('user_id');
		}

		$query = $this->db->query("select a.id as product_id, a.url, a.product_cat_id, a. product_sub_cat_id,d.company_name as brand, a.name, a.slug, a.status,a.is_delete, a.created_datetime, a.modified_datetime,a.image, a.approve_reject, a.price,a.brand_id FROM `product_desc` AS a,`brand_info` as d  WHERE a.`brand_id`=d.`user_id` AND a.`id` >='3598' AND (a.approve_reject='P' OR a.approve_reject='A' OR a.approve_reject='D' OR a.approve_reject='B') AND a.`is_delete` = 0 ".$search_cond." order by a.id desc ".$limit_cond);	
		$res = $query->result_array();		
		return $res;	

	}//this function is used to get the product details added


	function get_stores(){
        $query = $this->db->query("SELECT a.user_id as brand_id, a.company_name FROM brand_info as a,`user_login` as b where a.user_id = b.id AND a.store_type != 3 AND a.`show_in brand_list` = '1' ORDER BY a.company_name");
        $result = $query->result_array();
        return $result;
    }


	function get_product_tags($product_id){
	
		if($product_id !=''){
		
		$query = $this->db->query(" Select distinct(pt.tag_id) as tag_id,t.name as tagname from product_tag as pt, tag as t where pt.tag_id= t.id and t.status=1 and pt.product_id in (".$product_id.")");
		
			$res = $query->result_array(); 

			$tags_id = array();

			if(!empty($res)){
				foreach($res as $val){
					
					$tags_id[$val['tag_id']] = $val['tagname'];
					
				}
			}		
			
			return $tags_id;
		}

	}

	function get_all_tags(){		
		
		$query = $this->db->query(" Select id as tag_id,name as tagname from tag where status=1  and is_delete=0 ");
		
			$res = $query->result_array(); 

			$tags_id = array();
			
			// if(!empty($res)){
			// 	foreach($res as $val){
					
			// 		$tags_id[$val['tag_id']] = $val['tagname'];
					
			// 	}
			// }	

			if(!empty($res)){
				foreach($res as $val){
					
					$tags_id[] = $val['tagname'];
					
				}
			}				

			return $tags_id;		
	}


	function saveTags($tags,$products)
	{
		$tags_arr = array();
		$prd_tags = array();
		$loggedInUser=$this->session->userdata('user_id');

		if(!empty($products))
		{
			foreach($products as $prdid)
			{
				$resultprd = $this->deleteproducttag($prdid);
				if(!empty($tags))
				{
					foreach($tags as $val)
					{
						$tags_id = $this->getTagId($val);
						$tags_arr[] = $tags_id;
						$prd_tags[] = array(
								'product_id' => $prdid,
								 'tag_id' => $tags_id,
								 'status' => 1,
								 'created_by'=> $loggedInUser,
								 'created_datetime'=> date("Y-m-d H:i:s"),                                
								 'modified_by'=> $loggedInUser
								);
						
					}
					//print_r($prd_tags);exit;
					$resultinsert = $this->insertproducttag($prd_tags);
									
				}

			}
				
		}
		
	}

	function getTagId($tag)
	{
		$tag_id = '';
		$loggedInUser=$this->session->userdata('user_id');

		$query = $this->db->query(" Select id as tag_id from tag where status=1  and is_delete=0 and name = '".$tag."'");
		
			$res = $query->result_array();

			if(!empty($res))
			{
				$tag_id = $res[0]['tag_id'];
				
			}else
			{
				$tagnew = strtolower(trim($tag));
				$tag_slug = str_replace(' ', '-', $tagnew);
				$data_post = array( 
								'tag_type_id' => 0,
								'name' => $tagnew,
								'slug' => $tag_slug,
								'status' => 1,
								'created_by'=> $loggedInUser,
							    'created_datetime'=> date("Y-m-d H:i:s"),                                
							    'modified_by'=> $loggedInUser
					);
				$productRes = $this->db->insert('tag',$data_post);	
				$tag_id = $this->db->insert_id();

			}			

			return $tag_id;
	}

	function deleteproducttag($prdid)
	{
		$this->db->where('product_id', $prdid);
		$delvarRes = $this->db->delete('product_tag'); 
		return $delvarRes;
	}
	
	function insertproducttag($prd_tags)
	{
		$prodRes = $this->db->insert_batch('product_tag',$prd_tags);
		//echo $this->db->last_query();
		return $prodRes;
	}

	function publishProducts($products)
	{	
		$loggedInUser=$this->session->userdata('user_id');	
		if(!empty($products))
		{
			foreach($products as $val)
			{
				$this->db->where('id', $val);
 				$result = $this->db->update('product_desc' ,array('approve_reject'=>'A', 'modified_by' => $loggedInUser));
			}
		}

		return $result;
	}
	
	function check_tag_exists($tag_id,$product_id){
		$query = $this->db->query('select a.* from product_tag as a where a.tag_id = '.$tag_id.' AND a.product_id = '.$product_id.' ');
		$result = $query->result_array();
		if(count($result) > 1){
			return true;
		}else {
			return false;
		}
	}

 } 
?>
