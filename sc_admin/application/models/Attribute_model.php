<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attribute_model extends MY_Model {
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }	

    function save_attribues($data){
    	if(!empty($data)){
            $cat_data = array();
    		$cat_name = $this->input->post('attribute_name');
    		$cat_slug = $this->input->post('attribute_slug');
    		$cat_parent = $this->input->post('attribute_parent');
    		$cat_desc = $this->input->post('attribute_content');
            $attribute_type = $this->input->post('attribute_type');
            $show_on_web = $this->input->post('show_on_web');
            $order = $this->input->post('order');
            $sort = $this->input->post('sort');

            $i = 0; $cat_parent1 = '';
            if(!empty($cat_parent)){
                foreach ($cat_parent as $value) {
                    if($i != 0){  $cat_parent1 = $cat_parent1.','; }
                    $cat_parent1 = $cat_parent1.$value;
                    $i++;
                }
            }

    		if($cat_name!=''){
                $cat_data['attribute_name'] = $cat_name;
                $cat_data['attribute_content'] = $cat_desc;
                $cat_data['attribute_slug'] = strtolower($cat_slug);
                $cat_data['attribute_parent'] = $cat_parent1;
                $cat_data['attribute_type'] = $attribute_type;
                $cat_data['show_on_web'] = $show_on_web;
                $cat_data['order'] = $order;
                $cat_data['count'] = 0;
                $cat_data['created_datetime'] = date('Y-m-d H:i:s');
                $cat_data['sort'] = $sort;
    			$cat_id = $this->add_category($cat_data);
                
                
    		}
    	}
    }

    function add_category($data){
        if(!empty($data)){
            $this->db->insert('attributes', $data); 
            return $this->db->insert_id(); 
        }else{
            return 0;
        }
    }

    function get_all_categories($cat_id = '-1'){
        $this->db->from('category');
        $this->db->order_by("category_parent", "asc");
        $this->db->where('category_parent',$cat_id);
        $query = $this->db->get(); 
        return $query->result_array();
    }

    function get_all_attributes(){
        $this->db->from('attributes');
        $query = $this->db->get(); 
        return $query->result_array();
    }

    function get_attributes($cat_id){
        $this->db->from('attributes');
        $this->db->where('id',$cat_id);
        $query = $this->db->get(); 
        return $query->result_array();
    }

     function get_parent_attributes(){
        $this->db->from('attributes');
        //$this->db->where('attribute_parent',-1);
        $query = $this->db->get(); 
        return $query->result_array();
    }

    function get_chaild_attributes(){
        $this->db->from('attribute_values_relationship');
        //$this->db->where('attribute_parent!=',-1);
        $query = $this->db->get(); 
        return $query->result_array();
    }
    
    function get_filtered_category($search){
        if($search!=''){
            $query = $this->db->query("SELECT `id`, `attribute_name`, `attribute_content`, `sort`,`attribute_parent`, `attribute_type`, `attribute_slug`, `show_on_web`, `order`, `count`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime` FROM `attributes` WHERE 1  and attribute_name like '%".$search."%' ");  
            
            return $query->result_array();
        }
    }

    function get_cat_slug($cat_id=null){
        if($cat_id!='-1'){
            $this->db->from('attributes');
            $this->db->where('id',$cat_id);
            $query = $this->db->get(); 
            $res = $query->result_array(); 
            if(!empty($res)){
                return $res[0]['attribute_slug'];
            }
        }else{ return ''; }
    }

    function update_cat_image($cat_id=null){
        if($cat_id!=null && $cat_id >0){
            $data = array(
               'category_image' => $cat_id.'.jpg'
            );

            $this->db->where('id', $cat_id);
            $this->db->update('category', $data); 
        }
    }

    function edit_attribute($data,$cat_id=null){
        if($cat_id!=null && $cat_id >0){
            $category_slug = '';
            $category_slug = strtolower($data['attribute_slug']);

            $i = 0; $cat_parent1 = '';
            if(!empty($data['attribute_parent'])){
                foreach ($data['attribute_parent'] as $value) {
                    if($i != 0){  $cat_parent1 = $cat_parent1.','; }
                    $cat_parent1 = $cat_parent1.$value;
                    $i++;
                }
            }
            
            $data = array(
               'attribute_name' => $data['attribute_name'],
               'attribute_content' => $data['attribute_content'],
               'attribute_parent' => $cat_parent1,
               'attribute_slug' => $category_slug,
               'attribute_type' => $data['attribute_type'],
               'show_on_web' => $data['show_on_web'],
               'order' => $data['order'],
               'sort' => $data['sort']
            );

            $this->db->where('id', $cat_id);
            $this->db->update('attributes', $data); 

            
        }
    }

    function delete_category($id){
        if($id > 0){
            $this->db->delete('attributes', array('id' => $id)); 
            return 1;
        }else{
            return 2;
        }
    }


//----For attribute_category_relationship migration---------
   /* function migrate_acr()
    {  echo 'migrate_acr';exit;
        $query = $this->db->query("SELECT `id`,`attribute_name`,`attribute_parent` FROM `attributes` WHERE `attribute_parent`!='-1'  "); 
        $result = $query->result_array();
        //echo $result[0]['attribute_parent'];
        //echo '<pre>';print_r($result);exit;
        if(!empty($result))
        {
            foreach($result as $value)
            {
                if($value['attribute_parent']!='')
                {
                     $attr_ids = explode(',',$value['attribute_parent']);
                    //echo '<pre>';print_r($attr_ids);exit;
                    foreach($attr_ids as $val)
                    {
                        $data['attribute_id'] = $value['id'] ;
                        $data['category_id'] = $val;
                        $this->db->insert('attribute_category_relationship',$data); 
                        $this->db->insert_id();
                    }
                }
                   
            }            
        }
        
    }*/


}
?>