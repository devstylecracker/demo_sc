<?php
class Cost_model extends MY_Model {
	
	public function add_scx_option($arrData = array())
	{
		$this->db->insert('scx_options', $arrData);
		$optionId = $this->db->insert_id();
		return $optionId;
	}

	public function add_scx_option_meta($arrData = array(), $scx_option_id='')
	{
		$this->db->from('scx_options_meta');
		$this->db->where('scx_options_id',$scx_option_id);
		$this->db->where('option_key',$arrData['option_key']);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$this->db->where('scx_options_id',$scx_option_id);
			$this->db->where('option_key',$arrData['option_key']);
			$this->db->update('scx_options_meta',$arrData);
		}
		else
		{
			$this->db->insert('scx_options_meta', $arrData);
			$optionId = $this->db->insert_id();
			//return $optionId;
		}
	}

	public function get_scx_option_box_type($box = '')
	{
		$this->db->select('id');
		$this->db->from('scx_options');
		$this->db->where('option_value', $box);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$optionId = $query->row_array();;
			return $optionId['id'];
		}
		return false;
	}
	
	public function get_scx_option_meta($scx_options_id = '')
	{
		$this->db->from('scx_options_meta');
		$this->db->where('scx_options_id',$scx_options_id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}
	
	public function get_sales_report($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL,$search_by2=NULL,$object_id='')
	{
		if($page!='' || $per_page!='')
		{ if($page != 1 && $page !=0)
			{ $page = $page*$per_page-$per_page; }
			else{ $page = 0;}	
		}
		$this->db->select('oi.id,oi.user_id,oi.order_unique_no,oi.order_display_no,oi.first_name,oi.last_name,oi.email_id,oi.mobile_no,oi.brand_id,oi.order_tax_amount,oi.cod_amount,oi.order_sub_total,oi.order_total,oi.pay_mode,oi.created_datetime,oi.order_status,oi.shipping_address,oi.pincode,oi.city_name,oi.state_name,oi.total_discount_percent,oi.sc_discount_applied,oi.coupon_code,oi.coupon_amount,oi.is_paymentdone,opi.product_id,opi.product_size,opi.product_qty,opi.cart_id,opi.product_price,opi.compare_price,ui.gender');
		$this->db->from('order_info as oi');
		$this->db->join('order_product_info as opi', 'opi.order_id = oi.id', 'INNER');
		$this->db->join('user_info as ui', 'ui.user_id = oi.user_id', 'INNER');
		$this->db->join('object_meta as om', 'oi.order_display_no = om.object_meta_key', 'INNER');
		$this->db->where('om.object_id',$object_id);
		$this->db->where('oi.order_display_no !=','');
		if($search_by == 1 && $search_by != '')
		{
			$this->db->where('oi.order_display_no',$table_search);
		}
		if($search_by == 2 && $search_by != '')
		{
			$this->db->where("CONCAT(oi.first_name,' ',oi.last_name) LIKE '%".trim($table_search)."%'", NULL, FALSE);
		}
		if($search_by == 8 && $search_by != '')
		{
			$date_from 	= $this->input->post('date_from');
			$date_to 	= $this->input->post('date_to');
			$this->db->where('oi.created_datetime >=',$date_from.' 00:00:00');
			$this->db->where('oi.created_datetime <=',$date_to.' 23:59:59');
		}
		$this->db->order_by('oi.created_datetime','DESC');
		$this->db->limit($per_page,$page);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}
	
	
	
	public function get_product_meta($objectId='', $OrderDisplayNo='')
	{
		$this->db->select('object_meta_value');
		$this->db->from('object_meta');
		$this->db->where('object_id', $objectId);
		$this->db->where('object_meta_key', $OrderDisplayNo);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$rsponse = $query->row_array();
			$responseData = unserialize($rsponse['object_meta_value']);
			
			if(is_array($responseData) && count($responseData)>0)
			{
				foreach($responseData as $keyData => $responseValue)
				{
					 $responseValueData[] = $this->get_product_desc($keyData); 
				}
			}
			return $responseValueData;
		}
		return false;
	}
	
	public function get_product_desc($id='')
	{
		//$this->db->select('pd.id,pd.brand_id,pd.designer_id,pd.name,pd.url,pd.description,pd.slug,pd.price,pd.compare_price,pd.price_unit,pd.price_range,pd.consumer_type,pd.target_market,pd.product_sku,pd.product_discount,pd.discount_percent,pd.sku_number,pd.stock_count,pd.grouping_products');
		$this->db->select('pd.id,pd.price,pd.compare_price,pd.price_unit,pd.price_range,pd.product_discount,pd.discount_percent');
		$this->db->from('product_desc as pd');
		$this->db->where('pd.id', $id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		return false;
	}

	public function addUpdate_cart_meta($arrData = array(), $cart_id='')
	{
		$session_userid = $this->session->userdata('user_id');
		$arrData['cart_meta_key'] = 'boxcuration_maxamount';
		$this->db->from('cart_meta');
		$this->db->where('cart_id',$cart_id);
		$this->db->where('cart_meta_key',$arrData['cart_meta_key']);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$arrData['modified_by'] = $session_userid;
			$this->db->where('cart_id',$cart_id);
			$this->db->where('cart_meta_key',$arrData['cart_meta_key']);
			$this->db->update('cart_meta',$arrData);
			//echo $this->db->last_query();exit;
			return true;
		}else
		{
			$arrData['cart_id'] = $cart_id;			
			$arrData['created_by'] = $session_userid;
			$arrData['created_datetime'] = date('Y-m-d h:i:s');			
			$this->db->insert('cart_meta', $arrData);
			$optionId = $this->db->insert_id();
			return true;
		}
		return false;
	}

	public function get_cart_meta_value($cart_id,$metakey)
	{
		$this->db->from('cart_meta');
		$this->db->where('cart_id',$cart_id);
		$this->db->where('cart_meta_key',$metakey);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$result = $query->result_array();
			return $result[0]['cart_meta_value'];
		}
		return false;
	}
	
	
	public function get_categories_by_id($id = '')
	{
		$this->db->select('c.id,c.category_name,cor.id,cor.object_id,cor.category_id,cor.object_type,pd.price,pd.compare_price');
		$this->db->from('category as c');
		$this->db->join('category_object_relationship as cor','c.id = cor.category_id', 'INNER');
		$this->db->join('product_desc as pd','pd.id = cor.object_id', 'INNER');
		$this->db->where('c.level', '2');
		$this->db->where_in('cor.object_id', $id);
		$this->db->where('cor.object_type', 'product');
		$this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		return false;
	}
	
	
	public function get_scx_options()
	{
		$this->db->select('sco.id,sco.option_key,sco.option_value,sco.scx_options_id');
		$this->db->from('scx_options_meta as sco');
		$this->db->order_by('sco.scx_options_id','DESC');
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$scxOptions = $query->result_array();
			$option = array();
			foreach($scxOptions as $options)
			{
				$option[$options['scx_options_id']][$options['option_key']] = $options['option_value'];
			}
			return $option;
		}
		return false;
		
	}
	
	public function get_coupon_setting($couponCode = '')
	{
		$this->db->select('cs.coupon_amount');
		$this->db->from('coupon_settings as cs');
		$this->db->where('coupon_code',$couponCode);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		return false;
	}
}