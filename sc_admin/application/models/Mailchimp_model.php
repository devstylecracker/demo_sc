<?php
ini_set('memory_limit', '-1');
class Mailchimp_model extends MY_Model {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function getUserData()
    {
    	$query = $this->db->query("Select a.id as UserId,a.email, a.bucket, a.user_name, b.first_name, b.last_name, b.registered_from as Platform, b.gender,(Select role_name from role_master where id=c.role_id) as role from user_login as a, user_info as b, user_role_mapping as c where a.id=b.user_id and a.id = c.user_id limit 0,3");
		$result = $query->result_array();
		return $result;
    }

    function get_user_pa($user_id){
		$query = $this->db->query('SELECT a.`question_id`,b.`answer` FROM `users_answer` AS a, `answers` AS b WHERE 1 AND b.`id` = a.`answer_id` and a.`user_id`= '.$user_id);

		$pa_data = $query->result_array();

		$question_ans = array();
             if(!empty($pa_data)){
                foreach($pa_data as $val){
                     $question_ans[$val['question_id']] = $val['answer'];
                }
             }    
		return $question_ans;
	}

	function getLoginUserData($user_id)
	{
		$query = $this->db->query("select a.id as userid,a.email,a.bucket,a.question_comp as pa_complete,(select first_name from user_info where user_id = a.id) as first_name,(select last_name from user_info where user_id = a.id) as last_name,(select if(gender=1,'Female','Male') from user_info where user_id = a.id) as gender,(select registered_from from user_info where user_id = a.id) as platform,(select birth_date from user_info where user_id = a.id) as birth_date,(select contact_no from user_info where user_id = a.id) as contact_no,(select GROUP_CONCAT(w.answer) from users_answer as q,answers as w where q.user_id = a.id and q.answer_id=w.id and q.answer_id IN(45,46,47,48,49,50,51,52) group by q.user_id) as size,(select GROUP_CONCAT(w.answer) from users_answer as q,answers as w where q.user_id = a.id and q.answer_id=w.id and q.answer_id IN(39,40,41,42,43,44,4683,4684,4685,4686,4687) group by q.user_id) as age,(select GROUP_CONCAT(w.answer) from users_answer as q,answers as w where q.user_id = a.id and q.answer_id=w.id and q.answer_id IN(36,37,38,4651,4670,4688,4689,4690,4691) group by q.user_id) as budget,(select answer from answers WHERE id IN(select body_shape as bucket from bucket where id=a.bucket)) as first,(select answer from answers WHERE id IN(select style as bucket from bucket where id=a.bucket)) as second,(select answer from answers WHERE id IN(select work_style as bucket from bucket where id=a.bucket)) as third,(select answer from answers WHERE id IN(select personal_style as bucket from bucket where id=a.bucket)) as forth from user_login as a where a.id = '".$user_id."' order by id desc");

		$loginData = $query->result_array();
		/*echo $this->db->last_query();*/
		return $loginData;
	}	

	function get_user_login($user_id){
		if($user_id!='')
		{
			$query = $this->db->query('SELECT a.`login_time` FROM `user_login_attempts` AS a WHERE a.`user_id`= '.$user_id.' ORDER BY a.`login_time` desc LIMIT 0,1');

			$login_data = $query->result_array();

			if($login_data)
			{
				return $login_data[0]['login_time'];
			}else
			{
				return 0;
			}
			
		}
	}

	function getset_scoption($option,$optionvalue,$action){

		$result = array();
		if($action=='Get_data')
		{
			$query = $this->db->query('SELECT `option_value` FROM `sc_options`  WHERE `option_name`="'.$option.'"');
			$result = $query->result_array();

		}else if($action=='Update_data')
		{
			$query = $this->db->query('UPDATE `sc_options` SET `option_value` = NOW()  WHERE `option_name`="'.$option.'"');
			if($query)
			{
				$result = true;	
			}else
			{
				$result = false;
			}
			
		}		
		return $result;
	}

	function get_cart($cartid,$userid=Null)
	{
		if($cartid==0)
		{
			$query = $this->db->query('SELECT a.*,b.`name` as product_name,b.`brand_id`,b.`price` as product_price,b.`image` as product_image,c.`first_name`,c.`last_name`,c.`gender`,c.`registered_from`,c.`contact_no`,d.`company_name`,e.`email`,f.`size_text`  FROM `cart_info` as a, `product_desc` as b,`user_info` as c,`brand_info` as d, `user_login` as e,`product_size` as f WHERE a.`product_id`=b.`id` and a.`user_id`=c.`user_id` and b.`brand_id`=d.`user_id` and 
				a.`user_id`=e.`id` and a.`product_size`=f.`id`and a.`order_id` is NULL order by a.`user_id` ');		
			$result = $query->result_array();
		}else
		{
			$query = $this->db->query('SELECT a.*,b.`name` as product_name,b.`brand_id`,b.`price` as product_price,b.`image` as product_image,c.`first_name`,c.`last_name`,c.`gender`,c.`registered_from`,c.`contact_no`,d.`company_name`,e.`email`,f.`size_text`  FROM `cart_info` as a, `product_desc` as b,`user_info` as c,`brand_info` as d, `user_login` as e,`product_size` as f WHERE a.`product_id`=b.`id` and a.`user_id`=c.`user_id` and b.`brand_id`=d.`user_id` and 
				a.`user_id`=e.`id` and a.`product_size`=f.`id` and a.`order_id` is NULL  and a.`user_id`="'.$userid.'" order by a.`user_id` ');
			$result = $query->result_array();

		}
		return $result;

	}

	function get_Uniquecart($cartid)
	{
		if($cartid!='')
		{
			$query = $this->db->query('SELECT a.*,b.`name` as product_name,b.`brand_id`,b.`price` as product_price,b.`image` as product_image,c.`first_name`,c.`last_name`,c.`gender`,c.`registered_from`,c.`contact_no`,d.`company_name`,e.`email`,f.`size_text`  FROM `cart_info` as a, `product_desc` as b,`user_info` as c,`brand_info` as d, `user_login` as e,`product_size` as f WHERE a.`product_id`=b.`id` and a.`user_id`=c.`user_id` and b.`brand_id`=d.`user_id` and a.`user_id`=e.`id` and a.`product_size`=f.`id` and a.`order_id` is NULL and a.`id`="'.$cartid.'" group by a.`user_id` order by a.`user_id` ');
			
			$result = $query->result_array();
		}else
		{
			$query = $this->db->query('SELECT a.*,b.`name` as product_name,b.`brand_id`,b.`price` as product_price,b.`image` as product_image,c.`first_name`,c.`last_name`,c.`gender`,c.`registered_from`,c.`contact_no`,d.`company_name`,e.`email`,f.`size_text`  FROM `cart_info` as a, `product_desc` as b,`user_info` as c,`brand_info` as d, `user_login` as e,`product_size` as f WHERE a.`product_id`=b.`id` and a.`user_id`=c.`user_id` and b.`brand_id`=d.`user_id` and a.`user_id`=e.`id` and a.`product_size`=f.`id` and a.`order_id` is NULL  and a.`id`="'.$cartid.'" group by a.`user_id` order by a.`user_id` ');
			$result = $query->result_array();

		}
		return $result;

	}

	function get_Product($mcstart_datetime)
	{
		if($mcstart_datetime==0)
		{
			$query = $this->db->query('SELECT a.*,b.`name` as product_name,b.`brand_id`,b.`price` as product_price,b.`image` as product_image,c.`first_name`,c.`last_name`,c.`gender`,c.`registered_from`,c.`contact_no`,d.`company_name`,e.`email`,f.`size_text`  FROM `cart_info` as a, `product_desc` as b,`user_info` as c,`brand_info` as d, `user_login` as e,`product_size` as f WHERE a.`product_id`=b.`id` and a.`user_id`=c.`user_id` and b.`brand_id`=d.`user_id` and 
				a.`user_id`=e.`id` and a.`product_size`=f.`id`and a.`order_id` is NULL order by a.`product_id` ');
			
			$result = $query->result_array();
		}else
		{
			$query = $this->db->query('SELECT a.*,b.`name` as product_name,b.`brand_id`,b.`price` as product_price,b.`image` as product_image,c.`first_name`,c.`last_name`,c.`gender`,c.`registered_from`,c.`contact_no`,d.`company_name`,e.`email`,f.`size_text`  FROM `cart_info` as a, `product_desc` as b,`user_info` as c,`brand_info` as d, `user_login` as e,`product_size` as f WHERE a.`product_id`=b.`id` and a.`user_id`=c.`user_id` and b.`brand_id`=d.`user_id` and 
				a.`user_id`=e.`id` and a.`product_size`=f.`id` and a.`order_id` is NULL  and a.`created_datetime`="'.$mcstart_datetime.'" order by a.`product_id` ');
			$result = $query->result_array();

		}
		return $result;

	}

	function get_UniqueProduct($mcstart_datetime)
	{

		if($mcstart_datetime==0)
		{
			$query = $this->db->query('SELECT a.*,b.`name` as product_name,b.`brand_id`,b.`price` as product_price,b.`image` as product_image,c.`first_name`,c.`last_name`,c.`gender`,c.`registered_from`,c.`contact_no`,d.`company_name`,e.`email`,f.`size_text`  FROM `cart_info` as a, `product_desc` as b,`user_info` as c,`brand_info` as d, `user_login` as e,`product_size` as f WHERE a.`product_id`=b.`id` and a.`user_id`=c.`user_id` and b.`brand_id`=d.`user_id` and a.`user_id`=e.`id` and a.`product_size`=f.`id` and a.`order_id` is NULL group by a.`product_id` order by a.`product_id` ');
			
			$result = $query->result_array();
		}else
		{
			$query = $this->db->query('SELECT a.*,b.`name` as product_name,b.`brand_id`,b.`price` as product_price,b.`image` as product_image,c.`first_name`,c.`last_name`,c.`gender`,c.`registered_from`,c.`contact_no`,d.`company_name`,e.`email`,f.`size_text`  FROM `cart_info` as a, `product_desc` as b,`user_info` as c,`brand_info` as d, `user_login` as e WHERE a.`product_id`=b.`id` and a.`user_id`=c.`user_id` and b.`brand_id`=d.`user_id` and a.`user_id`=e.`id` and a.`product_size`=f.`id` and a.`order_id` is NULL  and a.`created_datetime`="'.$mcstart_datetime.'" group by a.`product_id` order by a.`product_id` ');
			$result = $query->result_array();

		}
		return $result;

	}

	function product_import($productids=Null){
		$query = $this->db->query('select a.id,a.name,a.description,a.slug,a.price,a.compare_price,b.company_name,a.image,a.created_datetime,a.modified_datetime from product_desc as a,brand_info as b where a.brand_id = b.user_id and  a.approve_reject IN ("A","D","P") and a.id IN ('.$productids.') order by a.id desc ');
		//limit 9000,10		
		$result = $query->result_array();
		return $result;
	}

	function product_variants_import($product_id){
		/*$query = $this->db->query('select a.product_id,a.product_sku,a.size_id,a.stock_count,b.size_text from product_inventory as a,product_size as b where a.size_id=b.id and a.product_id = '.$product_id);*/
		$query = $this->db->query('select DISTINCT CAST(b.`id` as UNSIGNED) as `size_id`,a.`product_id`,a.`stock_count`,b.`size_text`,(Select product_sku from product_inventory where product_id='.$product_id.' and size_id = b.id and product_sku IS NOT NULL limit 0,1 )as product_sku from product_inventory as a,product_size as b where a.size_id=b.id and a.product_id = '.$product_id); 
		$result = $query->result_array();
		return $result;
	}

	function mailchimp_update_cart($cartid)
	{
		echo 'mailchimp--'.$cartid;
    	exit;

	}

	public function get_nonscdomain($email)
    {
        $allowed = array('stylecracker.com');

        $explodedEmail = explode('@', $email);
        $domain = array_pop($explodedEmail);
        $domain1 = strtolower($domain);
        if (! in_array($domain1, $allowed))
        {
            return true;
        }else
        {
            return false;
        }       
    }


    public function get_orderDetails($orderid=NULL)
    {
    	$query = $this->db->query('SELECT a.*,a.`first_name`,a.`last_name`,a.`shipping_address`,a.`pincode`,a.`city_name`,(Select state_name from `states` as s where s.`id`= a.`state_name`)as statename,a.`country_name`,a.`mobile_no`,a.`pay_mode`,a.`cod_amount`,a.`user_id` as cust_id,SUM(a.`order_total`) as order_total,SUM(a.`order_tax_amount`) as tax_total,SUM(a.`shipping_amount`) as shipping_total,SUM(a.`cod_amount`) as cod_total,a.`created_datetime`,a.`modified_datetime`,GROUP_CONCAT(brand_id) as brand_id FROM `order_info` as a WHERE order_unique_no ="'.$orderid.'" group by a.`order_display_no`  order by a.`id` desc ');
    	$result = $query->result_array();
    	return $result;
    }

    public function get_orderLine($orderid=NULL)
    {
    	$orders = array();
    	if($orderid!='')
    	{
    		$order_array = explode('_',$orderid);
	    	//echo '<pre>';print_r($order_array);

	    	$result_count = count($order_array);

	    	for($i=1;$i<$result_count;$i++)
	    	{
	    		$query = $this->db->query('SELECT a.`id`,a.`product_id`, b.`name` as product_title,a.`product_size` as product_variant_id,c.`size_text` as product_variant_title,a.`product_qty` as quantity,a.`product_price` as price,a.`order_status` FROM `order_product_info` as a,product_desc as b,product_size as c WHERE a.`product_id`=b.`id` AND a.`product_size`=c.`id` AND a.`order_status`!=7 AND a.`order_status`!=9 AND a.`order_id`='.$order_array[$i].' order by a.`id` desc ');	
	    		//echo $this->db->last_query();exit;
	    		$result = $query->result_array();
	    		foreach($result as $val)
	    		{
	    			$orders[] = $val;
	    			
	    		}	   	
	    	}
    		
    	}else
    	{
    		/*$query = $this->db->query('SELECT a.* FROM `order_info` as a order by a.`id` desc ');
    		$result = $query->result_array();*/			
    	}
    	//echo '<pre>';print_r($orders);  exit;  	
    	return $orders;    	
    }

    public function get_ordernos()
    {
    	$query = $this->db->query('SELECT DISTINCT a.`order_unique_no`, a.`order_display_no` FROM `order_info` as a where a.`order_status`!=9 order by a.`id` desc ');
    	$result = $query->result_array();
    	return $result;
    }

    public function get_orderCount($userid)
    {
    	 $query_order = $this->db->query('select count(*) as ordcnt,SUM(order_total) as OrderTotal,order_unique_no,user_id from order_info group by order_unique_no having user_id = "'.$userid.'" ');
        $order_result = $query_order->result_array(); 
        if(!empty($order_result)) 
        {
        	$order_count = count($order_result);
        	return $order_count;
        }else
        {
        	return 0;
        }      
       
    }

    public function get_tdyOrders()
    {
    	$date = date('Y-m-d');
    	$prdate = strtotime(date('Y-m-d 00:00:00') . ' -1 day');
    	//$date = '2016-02-21';
    	$query = $this->db->query('SELECT DISTINCT a.`order_unique_no`, a.`order_display_no` FROM `order_info` as a WHERE a.`order_status`!=9 and a.`created_datetime` Like "%'.DATE('Y-m-d',$prdate).'%" order by a.`id` desc ');
    	$result = $query->result_array();
    	//echo $this->db->last_query();exit;
    	return $result;
    }

    public function check_fakeOrders($orderid)
    {
    	if($orderid!='')
    	{
    		$order_flag = 0;
    		$order_array = explode('_',$orderid);    	

	    	$result_count = count($order_array);

	    	for($i=1;$i<$result_count;$i++)
	    	{
	    		$query = $this->db->query('SELECT a.`order_id` FROM `order_product_info` as a WHERE  a.`order_status`!=7 AND a.`order_status`!=9 AND a.`order_id`='.$order_array[$i].' order by a.`id` desc ');	
	    		//echo $this->db->last_query();exit;
	    		$result = $query->result_array();
	    		if(!empty($result))
	    		{
	    			$order_flag = 1;
	    		}

	    	}

	    	return $order_flag;
	   	}
    }

    function product_import_cron($type,$typeid=null){
    	$prdate = strtotime(date('Y-m-d 00:00:00') . ' -1 day');
		if($type == 'add')
		{			
			$query = $this->db->query('select a.id,a.name,a.description,a.slug,a.price,a.compare_price,b.company_name,a.image,a.created_datetime,a.modified_datetime from product_desc as a,brand_info as b where a.brand_id = b.user_id and  a.approve_reject IN ("A","D","P") and a.is_delete=0 and a.status=1 and b.is_paid= 1 and a.created_datetime Like  "%'.DATE('Y-m-d',$prdate).'%" order by a.id desc ');

		}else if($type == 'update')
		{
			$query = $this->db->query('select a.id,a.name,a.description,a.slug,a.price,a.compare_price,b.company_name,a.image,a.created_datetime,a.modified_datetime from product_desc as a,brand_info as b where a.brand_id = b.user_id and  a.approve_reject IN ("A","D","P") and a.is_delete=0 and a.status=1 and b.is_paid= 1 and a.modified_datetime Like  "%'.DATE('Y-m-d',$prdate).'%" order by a.id desc ');
		}else if($type == 'brand')
		{ 
			if($typeid!='')
			{
				$query = $this->db->query('select a.id,a.name,a.description,a.slug,a.price,a.compare_price,b.company_name,a.image,a.created_datetime,a.modified_datetime from product_desc as a,brand_info as b where a.brand_id = b.user_id and  a.approve_reject IN ("A","D","P") and a.is_delete=0 and a.status=1 and b.is_paid= 1  and a.brand_id="'.$typeid.'" order by a.id desc ');
			}
		}else if($type == 'allproducts')
		{ 
			
			$query = $this->db->query('select a.id,a.name,a.description,a.slug,a.price,a.compare_price,b.company_name,a.image,a.created_datetime,a.modified_datetime from product_desc as a,brand_info as b where a.brand_id = b.user_id and  a.approve_reject IN ("A","D","P") and a.is_delete=0 and a.status=1  and b.is_paid= 1 order by a.id asc  ');
			
		}
		//echo $this->db->last_query();exit;
		//limit 9000,10		
		$result = $query->result_array();		
		return $result;
	}

    function get_products_categories($id){
		/*$this->db->select('category_id'); 
		$query = $this->db->get_where('category_object_relationship', array('object_id' => $id,'object_type' => 'product'));*/
		$query = $this->db->query("Select a.category_id,b.category_name from category_object_relationship as a, category as b where a.category_id=b.id and a.object_id='".$id."' and a.object_type = 'product' ");
		$res = $query->result_array();
		$category = array();
		if(!empty($res)){
			$i=0;
			foreach ($res as $value) {
				//$category[$i]['id'] = $value['category_id'];
				//$category[$i]['name'] = $value['category_name'];
				$category[] = $value['category_name'];
				$i++;
			}
		}
		return $category;
	}

	function get_products_attributes($id){
		/*$this->db->select('attribute_id'); 
		$query = $this->db->get_where('attribute_object_relationship', array('object_id' => $id,'object_type' => 'product'));*/
		$query = $this->db->query("Select a.attribute_id,b.attribute_name from attribute_object_relationship as a, attributes as b where a.attribute_id=b.id and a.object_id='".$id."' and a.object_type = 'product' ");
		$res = $query->result_array();
		$attributes = array();
		if(!empty($res)){
			$i=0;
			foreach ($res as $value) {
				//$attributes[$i]['id'] = $value['attribute_id'];
				//$attributes[$i]['name'] = $value['attribute_name'];
				$attributes[] = $value['attribute_name'];
				$i++;
			}
		}
		return $attributes;
	}

	function get_cart_user()
	{
		$prdate = strtotime(date('Y-m-d 00:00:00') . ' -1 day');	
		$query = $this->db->query('SELECT  distinct(a.`user_id`) as userid, a.`id` as cartid FROM `cart_info` as a WHERE a.`user_id`!=0 and a.`order_id` is NULL and a.`created_datetime` LIKE "%'.date('Y-m-d',$prdate).'%"  group by a.`user_id`  order by a.`id` desc ');	
		//and a.`created_datetime` LIKE "%'.date('Y-m-d').'%"					
		
		$result = $query->result_array();
		return $result;

	}

}