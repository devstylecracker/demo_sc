<?php
class Home_model extends MY_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function total_no_products(){

			$data = array(); 
			$date = date('Y-m-d');
			$user_id = $this->session->userdata('user_id');
			$user_role_id = $this->session->userdata('role_id');
			//$query = $this->db->get_where('product_desc',array('created_by'=>$user_id));
			
			$user_login = $this->db->get_where('user_login',array('status'=>1));		

			$stylist_users = $this->db->get_where('user_role_mapping',array('role_id'=>3));

			$intern_users = $this->db->get_where('user_role_mapping',array('role_id'=>4));

			$dataentry_users = $this->db->get_where('user_role_mapping',array('role_id'=>5));			

			$brand_users = $this->db->get_where('user_role_mapping',array('role_id'=>6));

			//$signup_data = $this->get_signup_data();

			/* Get Overall Users signup for today*/ 			
			$sql = "SELECT ul.id,ui.registered_from FROM user_login as ul INNER JOIN user_info as ui on ul.id = ui.	user_id WHERE ui.registered_from IN('google','facebook','website') AND ul.created_datetime like '%".$date."%' ";
	        $query = $this->db->query($sql);	      
	    	$signup_data['current_date'] = sizeof($query->result_array());	

	    	 /* Get Users Count from Google for today */			
			 $sql = "SELECT ul.id,ui.registered_from FROM user_login AS ul INNER JOIN user_info AS ui ON ul.id = ui.user_id WHERE ui.registered_from = 'google' AND ul.created_datetime like '%".$date."%' ";
	         $query = $this->db->query($sql);	         
			 $signup_data['current_date_google'] = sizeof($query->result_array());	

			 /* Get Users Count from facebook  for today */			
			 $sql = "SELECT ul.id,ui.registered_from FROM user_login AS ul INNER JOIN user_info AS ui ON ul.id = ui.user_id WHERE ui.registered_from = 'facebook'  AND ul.created_datetime like '%".$date."%' ";
	         $query = $this->db->query($sql);
			 $signup_data['current_date_facebook'] = sizeof($query->result_array());

			  /* Get Users Count from Website for today	*/		 
			 $sql = "SELECT ul.id,ui.registered_from FROM user_login AS ul INNER JOIN user_info AS ui ON ul.id = ui.user_id WHERE ui.registered_from = 'website' AND ul.created_datetime like '%".$date."%' ";
	         $query = $this->db->query($sql);		         
			 $signup_data['current_date_website'] = sizeof($query->result_array());			

			$product_data = $this->get_product_data();

			/*Get Overall Product uploaded  */
	    	if($user_role_id != 6)
			{
				$sql = "SELECT id FROM product_desc  WHERE is_delete = 0  ";
	        	$query = $this->db->query($sql);	       	      
	    		$product_data = sizeof($query->result_array());	
			}else
			{
				$sql = "SELECT id FROM product_desc  WHERE is_delete = 0 AND `created_by` = ".$user_id." ";
	        	$query = $this->db->query($sql);	       	      
	    		$product_data = sizeof($query->result_array());

			}


	    	/* Get Overall Look created */  
	    	$look_data = array();
	    	$brand_data = array();
	    	$look_data = array();
	    	$cpa_track = array();
	    	/*$sql = "SELECT id,name FROM look  WHERE deflag = 0 ";
	        $query = $this->db->query($sql);	      	      
	    	$look_data = sizeof($query->result_array());*/

	    	 /* Get Overall Brand onboarded */	    		
			 $sql = "SELECT ul.id,urm.role_id FROM user_login AS ul INNER JOIN user_role_mapping AS urm ON ul.id = urm.	user_id WHERE ul.status = 1 AND urm.role_id = 6  ";
	         $query = $this->db->query($sql);	         
			 $brand_data = sizeof($query->result_array());	

			//$look_data = $this->get_look_data();

			//$brand_data = $this->get_brand_data();

			// $click_data = $this->click_report();

			//$cpa_track = $this->track_cpa();			

			if($user_role_id == 1)
			{
				//$data['stylist_users'] = count($stylist_users->result_array());
				//$data['intern_users'] = count($intern_users->result_array());
				//$data['dataentry_users'] = count($dataentry_users->result_array());
				//$data['brand_users'] = count($brand_users->result_array());			
				//$data['product_count'] = count($query->result_array());
				//$data['users_count'] = count($user_login->result_array());
				$data['signup_data'] = $signup_data;
			}
			
			$data['product_data'] = $product_data;
			//$data['look_data'] = $look_data;
			//$data['brand_data'] = $brand_data;
			//$data['cpa_track'] = $cpa_track;
			return $data;
		}


		/* Get Users info */
		public function get_signup_data()
	    {	        
	    	 $date = date('Y-m-d');
	    	 $user_data = array();

	    	/* Get Overall Users signup for today*/ 			
			$sql = "SELECT ul.id,ui.registered_from FROM user_login as ul INNER JOIN user_info as ui on ul.id = ui.	user_id WHERE ui.registered_from IN('google','facebook','website') AND ul.created_datetime like '%".$date."%' ";
	        $query = $this->db->query($sql);	      
	    	$signup_data['current_date'] = sizeof($query->result_array());				
		
			 /* Get Users Count from Google for today */			
			 $sql = "SELECT ul.id,ui.registered_from FROM user_login AS ul INNER JOIN user_info AS ui ON ul.id = ui.	user_id WHERE ui.registered_from = 'google' AND ul.created_datetime like '%".$date."%' ";
	         $query = $this->db->query($sql);	         
			 $signup_data['current_date_google'] = sizeof($query->result_array());	

			 /* Get Users Count from facebook  for today */			
			 $sql = "SELECT ul.id,ui.registered_from FROM user_login AS ul INNER JOIN user_info AS ui ON ul.id = ui.	user_id WHERE ui.registered_from = 'facebook'  AND ul.created_datetime like '%".$date."%' ";
	         $query = $this->db->query($sql);
			 $signup_data['current_date_facebook'] = sizeof($query->result_array());

			  /* Get Users Count from Website for today	*/		 
			 $sql = "SELECT ul.id,ui.registered_from FROM user_login AS ul INNER JOIN user_info AS ui ON ul.id = ui.	user_id WHERE ui.registered_from = 'website' AND ul.created_datetime like '%".$date."%' ";
	         $query = $this->db->query($sql);		         
			 $signup_data['current_date_website'] = sizeof($query->result_array());	
				

  			 /* Get Users Count from Current Month */			
			 $sql = "SELECT ul.id,ui.registered_from FROM user_login AS ul INNER JOIN user_info AS ui ON ul.id = ui.	user_id WHERE ui.registered_from IN('google','facebook','website') AND MONTH(ul.created_datetime) = MONTH(CURRENT_DATE) AND YEAR(ul.created_datetime) = YEAR(CURRENT_DATE)";
      		 $query = $this->db->query($sql);
			 $signup_data['current_month'] = sizeof($query->result_array());  

			 /* Get Users Count from Google for Current Month */	
			 $sql = "SELECT ul.id,ui.registered_from FROM user_login AS ul INNER JOIN user_info AS ui ON ul.id = ui.	user_id WHERE ui.registered_from = 'google' AND MONTH(ul.created_datetime) = MONTH(CURRENT_DATE) AND YEAR(ul.created_datetime) = YEAR(CURRENT_DATE)";
      		 $query = $this->db->query($sql);
			 $signup_data['current_month_google'] = sizeof($query->result_array());

			 /* Get Users Count from facebook for Current Month */
			 $sql = "SELECT ul.id,ui.registered_from FROM user_login AS ul INNER JOIN user_info AS ui ON ul.id = ui.	user_id WHERE ui.registered_from = 'facebook' AND MONTH(ul.created_datetime) = MONTH(CURRENT_DATE) AND YEAR(ul.created_datetime) = YEAR(CURRENT_DATE)";
      		 $query = $this->db->query($sql);
			 $signup_data['current_month_facebook'] = sizeof($query->result_array());	

			 /* Get Users Count from website for Current Month */
			 $sql = "SELECT ul.id,ui.registered_from FROM user_login AS ul INNER JOIN user_info AS ui ON ul.id = ui.	user_id WHERE ui.registered_from = 'website' AND MONTH(ul.created_datetime) = MONTH(CURRENT_DATE) AND YEAR(ul.created_datetime) = YEAR(CURRENT_DATE)";
      		 $query = $this->db->query($sql);
			 $signup_data['current_month_website'] = sizeof($query->result_array());		

			 /* Get Users Count from Last Month */			 
			 $sql = "SELECT ul.id,ui.registered_from FROM user_login AS ul INNER JOIN user_info AS ui ON ul.id = ui.	user_id WHERE ui.registered_from IN('google','facebook','website') AND YEAR(ul.created_datetime) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(ul.created_datetime) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) ";
			 $query = $this->db->query($sql);			 
			 $signup_data['last_month'] = sizeof($query->result_array());	

			 /* Get Users Count from google for Last Month */
			 $sql = "SELECT ul.id,ui.registered_from FROM user_login AS ul INNER JOIN user_info AS ui ON ul.id = ui.	user_id WHERE ui.registered_from = 'google' AND YEAR(ul.created_datetime) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(ul.created_datetime) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)";
      		 $query = $this->db->query($sql);
			 $signup_data['last_month_google'] = sizeof($query->result_array());	

			  /* Get Users Count from facebook for Last Month */
			 $sql = "SELECT ul.id,ui.registered_from FROM user_login AS ul INNER JOIN user_info AS ui ON ul.id = ui.	user_id WHERE ui.registered_from = 'facebook' AND YEAR(ul.created_datetime) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(ul.created_datetime) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)";
      		 $query = $this->db->query($sql);
			 $signup_data['last_month_facebook'] = sizeof($query->result_array());	

			  /* Get Users Count from website for Last Month */
			 $sql = "SELECT ul.id,ui.registered_from FROM user_login AS ul INNER JOIN user_info AS ui ON ul.id = ui.	user_id WHERE ui.registered_from = 'website' AND YEAR(ul.created_datetime) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(ul.created_datetime) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)";
      		 $query = $this->db->query($sql);
			 $signup_data['last_month_website'] = sizeof($query->result_array()); 

			 /* Get Overall Users Count	 */		
			 $sql = "SELECT ul.id,ui.registered_from FROM user_login AS ul INNER JOIN user_info AS ui ON ul.id = ui.	user_id WHERE ui.registered_from IN('google','facebook','website') ";
	         $query = $this->db->query($sql);	         
			 $signup_data['overall_signup'] = sizeof($query->result_array());				  

			 /* Get Users Count from Google */			
			 $sql = "SELECT ul.id,ui.registered_from FROM user_login AS ul INNER JOIN user_info AS ui ON ul.id = ui.	user_id WHERE ui.registered_from = 'google' ";
	         $query = $this->db->query($sql);	         
			 $signup_data['google'] = sizeof($query->result_array());	

			 /* Get Users Count from facebook */	
			 $sql = "SELECT ul.id,ui.registered_from FROM user_login AS ul INNER JOIN user_info AS ui ON ul.id = ui.	user_id WHERE ui.registered_from = 'facebook' ";
	         $query = $this->db->query($sql);
			 $signup_data['facebook'] = sizeof($query->result_array());	

			 /* Get Users Count from Website */		
			 $sql = "SELECT ul.id,ui.registered_from FROM user_login AS ul INNER JOIN user_info AS ui ON ul.id = ui.	user_id WHERE ui.registered_from = 'website' ";
	         $query = $this->db->query($sql);		         
			 $signup_data['website'] = sizeof($query->result_array());	
			
	        return $signup_data;
	    }

	    /* Get Product Data */
	    public function get_product_data()
	    {	    	
	    	$date = date('Y-m-d');
	    	$product_data = array();
	    	$user_id = $this->session->userdata('user_id');
	    	$user_role_id = $this->session->userdata('role_id');
	    	$querycond = "";

	    	if($user_role_id == 1)
	    	{
	    		$querycond = "AND 1=1";
	    	}else
	    	{
	    		$querycond = "AND `created_by` = ".$user_id." ";
	    	}

	    	/* Get Overall Product uploaded  
	    	$sql = "SELECT id FROM product_desc  WHERE is_delete = 0 ".$querycond." ";
	        $query = $this->db->query($sql);	       	      
	    	$product_data['product_count'] = sizeof($query->result_array());*/ 	

	    	/* Get Overall Product uploaded today	*/		
			$sql = "SELECT id FROM product_desc  WHERE is_delete = 0 AND created_datetime like '%".$date."%' ".$querycond." ";
	        $query = $this->db->query($sql);	      
	    	$product_data['current_date_product'] = sizeof($query->result_array()); 				
		
			/* Get Overall Product uploaded current month */  			
			$sql = "SELECT id FROM product_desc  WHERE is_delete = 0 AND MONTH(created_datetime) = MONTH(CURRENT_DATE) AND YEAR(created_datetime) = YEAR(CURRENT_DATE) ".$querycond." ";
      		$query = $this->db->query($sql);      		
			$product_data['current_month_product'] = sizeof($query->result_array());

			/* Get Overall Product uploaded last month	*/  		
			$sql = "SELECT id FROM product_desc  WHERE is_delete = 0 AND YEAR(created_datetime) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(created_datetime) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) ".$querycond." ";
      		$query = $this->db->query($sql);      		
			$product_data['last_month_product'] = sizeof($query->result_array());
			
			return $product_data;
	    }


	    /* Get Look Data */
	    public function get_look_data()
	    {	    	
	    	$date = date('Y-m-d');
	    	$yersterday = date('Y-m-d', strtotime(' -1 day'));
	    	$look_data = array();
	    	$user_id = $this->session->userdata('user_id');
	    	$user_role_id = $this->session->userdata('role_id');
	    	
	    	$querycond = "";

	    	if($user_role_id == 1 || $user_role_id == 3)
	    	{
	    		$querycond = "AND 1=1";
	    	}else
	    	{
	    		$querycond = "AND 'created_by' = ".$user_id." ";
	    	}

	    	/*-----------Overall Look Queries-------------------------------*/

	    	/* Get Overall Look created  
	    	$sql = "SELECT id,name FROM look  WHERE deflag = 0  ".$querycond." ";
	        $query = $this->db->query($sql);	      	      
	    	$look_data['total_look_count'] = sizeof($query->result_array());*/ 	

	    	/* Get  Look created today 			
			$sql = "SELECT id,name FROM look  WHERE deflag = 0 AND created_datetime like '%".$date."%' ".$querycond." ";
	        $query = $this->db->query($sql);	           
	    	$look_data['current_look_count'] = sizeof($query->result_array());*/				
		
	    	/* Get  Look created Yesterday 			
			$sql = "SELECT id,name FROM look  WHERE deflag = 0 AND created_datetime like '%".$yersterday."%' ".$querycond." ";
	        $query = $this->db->query($sql);	           
	    	$look_data['yesterday_look_count'] = sizeof($query->result_array());*/		

			/* Get  Look created current month  	
			$sql = "SELECT id,name FROM look  WHERE deflag = 0 AND MONTH(created_datetime) = MONTH(CURRENT_DATE) AND YEAR(created_datetime) = YEAR(CURRENT_DATE) ".$querycond." ";
      		$query = $this->db->query($sql);      		
			$look_data['current_month_look_count'] = sizeof($query->result_array());*/		

			/* Get Look created last month  		
			$sql = "SELECT id,name  FROM look  WHERE deflag = 0 AND  YEAR(created_datetime) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(created_datetime) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) ".$querycond." ";
      		$query = $this->db->query($sql);      		
			$look_data['last_month_look_count'] = sizeof($query->result_array());*/	

			/*-----------Look Broadcasted Queries-------------------------------*/

			/* Get Overall Look broadcasted status = 1(broadcasted)	    	
	    	$sql = "SELECT id,name FROM look  WHERE deflag = 0 AND status = 1  ".$querycond." ";
	        $query = $this->db->query($sql);	      	      
	    	$look_data['total_look_broadcast'] = sizeof($query->result_array());*/

			/* Get  Look broadcasted today status = 1(broadcasted) 	*/		
			/*$sql = "SELECT id,name FROM look  WHERE deflag = 0 AND status = 1 AND modified_datetime like '%".$date."%' ";
	        $query = $this->db->query($sql);	      
	    	$look_data['current_look_broadcast'] = sizeof($query->result_array());	*/

	    	/* Get  Look broadcasted yersterday status = 1(broadcasted) 	*/		
			/*$sql = "SELECT id,name FROM look  WHERE deflag = 0 AND status = 1 AND modified_datetime like '%".$yersterday."%' ";
	        $query = $this->db->query($sql);	      
	    	$look_data['yersterday_look_broadcast'] = sizeof($query->result_array());				*/
		
			/* Get  Look broadcasted current month 			
			$sql = "SELECT id,name FROM look  WHERE deflag = 0 AND status = 1 AND MONTH(created_datetime) = MONTH(CURRENT_DATE) AND YEAR(created_datetime) = YEAR(CURRENT_DATE) ".$querycond." ";
      		$query = $this->db->query($sql);      		
			$look_data['current_month_look_broadcast'] = sizeof($query->result_array());*/ 

			/* Get Look broadcasted last month 			
			$sql = "SELECT id,name  FROM look  WHERE deflag = 0 AND status = 1 AND YEAR(created_datetime) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(created_datetime) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) ".$querycond." ";
      		$query = $this->db->query($sql);      		
			$look_data['last_month_look_broadcast'] = sizeof($query->result_array());*/ 

			/*-----------Look Pending Queries-------------------------------*/

			/* Get Overall Look pending status = 0(pending)    	
	    	$sql = "SELECT id,name FROM look  WHERE deflag = 0 AND status = 0 ".$querycond." ";
	        $query = $this->db->query($sql);	      	      
	    	$look_data['total_look_pending'] = sizeof($query->result_array());*/	

			/* Get Look pending today status = 0(pending) 			
			$sql = "SELECT id,name FROM look  WHERE deflag = 0 AND status = 0 AND created_datetime like '%".$date."%' ";
	        $query = $this->db->query($sql);	      
	    	$look_data['current_look_pending'] = sizeof($query->result_array());*/				
		
			/* Get Look pending current month status = 0(pending) 			
			$sql = "SELECT id,name FROM look  WHERE deflag = 0 AND status = 0 AND MONTH(created_datetime) = MONTH(CURRENT_DATE) AND YEAR(created_datetime) = YEAR(CURRENT_DATE) ".$querycond." ";
      		$query = $this->db->query($sql);      		
			$look_data['current_month_look_pending'] = sizeof($query->result_array());*/ 

			/* Get Look pending last month status = 0(pending)  		
			$sql = "SELECT id,name  FROM look  WHERE deflag = 0 AND status = 0 AND YEAR(created_datetime) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(created_datetime) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) ".$querycond." ";
      		$query = $this->db->query($sql);      		
			$look_data['last_month_look_pending'] = sizeof($query->result_array());*/	

			/*-----------Look Rejected Queries-------------------------------*/

			/* Get Overall Look rejected status = 2(rejected)  	
	    	$sql = "SELECT id,name FROM look  WHERE deflag = 0 AND status = 2 ".$querycond." ";
	        $query = $this->db->query($sql);	      	      
	    	$look_data['total_look_rejected'] = sizeof($query->result_array());*/	  

			/* Get Look rejected today status = 2(rejected) 		
			$sql = "SELECT id,name FROM look  WHERE deflag = 0 AND status = 2 AND created_datetime like '%".$date."%' ";
	        $query = $this->db->query($sql);	      
	    	$look_data['current_look_rejected'] = sizeof($query->result_array());*/					
		
			/* Get Look rejected current month status = 2(rejected) 			
			$sql = "SELECT id,name FROM look  WHERE deflag = 0 AND status = 2 AND MONTH(created_datetime) = MONTH(CURRENT_DATE) AND YEAR(created_datetime) = YEAR(CURRENT_DATE) ".$querycond." ";
      		$query = $this->db->query($sql);      		
			$look_data['current_month_look_rejected'] = sizeof($query->result_array());*/ 

			/* Get Look rejected last month status = 2(rejected) 			
			$sql = "SELECT id,name  FROM look  WHERE deflag = 0 AND status = 2 AND YEAR(created_datetime) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(created_datetime) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) ".$querycond." ";
      		$query = $this->db->query($sql);      		
			$look_data['last_month_look_rejected'] = sizeof($query->result_array());*/ 

			/* Get Look Bucket Count for Today 			
			$sql = "Select count(a.look_id) as look_count,a.bucket_id,(select answer from answers where id = b.body_shape) as body_shape,(select answer from answers where id = b.style) as style from `look_bucket_mapping` as a,`bucket` as b where a.`bucket_id` = b.`id` and a.`modified_datetime` like '%".$date."%' group by a.bucket_id order by look_count desc";
			$query = $this->db->query($sql);			   		
			$look_data['look_bucket'] = $query->result_array();

			$sql ="select count(a.id) as user_count,a.bucket,(select answer from answers where id = b.body_shape) as body_shape,(select answer from answers where id = b.style) as style from user_login as a,`bucket` as b where a.bucket = b.id group by bucket";
			$query = $this->db->query($sql);			   		
			$look_bucket_user = $query->result_array();

			$user_bucket_count =array();

			if(!empty($look_bucket_user))
			{
				foreach($look_bucket_user as $val)
				{
					$user_bucket_count[$val['bucket']] = $val['user_count'];
				}
			}
			$look_data['look_bucket_user'] = $user_bucket_count;*/ 
			
			return $look_data;
	    }


	     /* Get Brand Data */
	    public function get_brand_data()
	    {	    	
	    	$date = date('Y-m-d');
	    	$brand_data = array();
	    	$user_id = $this->session->userdata('user_id');
	    	$user_role_id = $this->session->userdata('role_id');
	    	$querycond = "";

	    	if($user_role_id == 1)
	    	{
	    		$querycond = "AND 1=1";
	    	}else
	    	{
	    		$querycond = "AND 'ul.brand_id' = ".$user_id." ";
	    	}

	    	/*-----------Brand Queries-------------------------------*/
	    	 /* Get Overall Brand onboarded */	    		
			 $sql = "SELECT ul.id,urm.role_id FROM user_login AS ul INNER JOIN user_role_mapping AS urm ON ul.id = urm.	user_id WHERE ul.status = 1 AND urm.role_id = 6 ".$querycond." ";
	         $query = $this->db->query($sql);	         
			 $brand_data['overall_brands'] = sizeof($query->result_array());				  

			 /* Get Brands Count for today */			
			 $sql = "SELECT ul.id,urm.role_id FROM user_login AS ul INNER JOIN user_role_mapping AS urm ON ul.id = urm.	user_id WHERE ul.status = 1 AND ul.created_datetime AND urm.role_id = 6 like '%".$date."%' ".$querycond."  ";
	         $query = $this->db->query($sql);	         
			 $brand_data['current_date_brand'] = sizeof($query->result_array());	

			 /* Get Brands Count for Current Month */			
			 $sql = "SELECT ul.id,urm.role_id FROM user_login AS ul INNER JOIN user_role_mapping AS urm ON ul.id = urm.	user_id WHERE ul.status = 1 AND urm.role_id = 6 AND MONTH(ul.created_datetime) = MONTH(CURRENT_DATE) AND YEAR(ul.created_datetime) = YEAR(CURRENT_DATE) ".$querycond."  ";
	         $query = $this->db->query($sql);	         
			 $brand_data['current_month_brand'] = sizeof($query->result_array());				

			 /* Get Brands Count for Last Month */			
			 $sql = "SELECT ul.id,urm.role_id FROM user_login AS ul INNER JOIN user_role_mapping AS urm ON ul.id = urm.	user_id WHERE ul.status = 1 AND urm.role_id = 6 AND YEAR(ul.created_datetime) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(ul.created_datetime) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) ".$querycond."  ";
	         $query = $this->db->query($sql);	         
			 $brand_data['last_month_brand'] = sizeof($query->result_array());	



			 /*-----------Store Queries-------------------------------*/
			 if($user_role_id == 1)
	    	{
	    		$querycond1 = "AND 1=1";
	    	}else
	    	{
	    		$querycond1 = "AND 'bs.brand_id' = ".$user_id." ";
	    	}
			  /* Get Overall Store onboarded 	    		
			 $sql = "SELECT bs.id,bs.brand_id,urm.role_id FROM brand_store AS bs INNER JOIN user_role_mapping AS urm ON bs.brand_id = urm.user_id WHERE bs.status = 1  ".$querycond." ";
	         $query = $this->db->query($sql);	             
			 $brand_data['overall_store'] = sizeof($query->result_array());	*/			  

			 /* Get Store Count for today 			
			 $sql = "SELECT bs.id,bs.brand_id,urm.role_id FROM brand_store AS bs INNER JOIN user_role_mapping AS urm ON bs.brand_id = urm.user_id WHERE bs.status = 1 AND urm.role_id = 6 AND bs.created_datetime like '%".$date."%'   ".$querycond." ";
	         $query = $this->db->query($sql);	         
			 $brand_data['current_date_store'] = sizeof($query->result_array());*/	

			 /* Get Store Count for Current Month 			
			 $sql = "SELECT bs.id,bs.brand_id,urm.role_id FROM brand_store AS bs INNER JOIN user_role_mapping AS urm ON bs.brand_id = urm.user_id WHERE bs.status = 1 AND urm.role_id = 6 AND  MONTH(bs.created_datetime) = MONTH(CURRENT_DATE) AND YEAR(bs.created_datetime) = YEAR(CURRENT_DATE)  ".$querycond." ";
	         $query = $this->db->query($sql);	         
			 $brand_data['current_month_store'] = sizeof($query->result_array());*/	
			

			 /* Get Store Count for Last Month		
			 $sql = "SELECT bs.id,bs.brand_id,urm.role_id FROM brand_store AS bs INNER JOIN user_role_mapping AS urm ON bs.brand_id = urm.user_id WHERE bs.status = 1 AND urm.role_id = 6 AND YEAR(bs.created_datetime) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(bs.created_datetime) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)  ".$querycond." ";
	         $query = $this->db->query($sql);	         
			 $brand_data['last_month_store'] = sizeof($query->result_array()); */	


			/* if($user_role_id == 1)
	    	{
	    		$querycond1 = "AND 1=1";
	    	}else
	    	{
	    		$querycond1 = "AND b.`brand_id` = ".$user_id." ";
	    	}*/
			 /* Get Brand Product Click Count Overall		
			 $sql = " SELECT c.`company_name` AS brand_name,c.`user_id` as brand_id, b.`name` AS product_name,b.`price`, sum(a.`click_count`) as click_count, a.`user_id`, a.`created_datetime` FROM users_product_click_track AS a, product_desc AS b LEFT JOIN brand_info AS c ON b.`brand_id` = c.`user_id` WHERE a.`product_id` = b.`id` ".$querycond." GROUP BY c.`user_id` DESC  ";
			 $query = $this->db->query($sql);*/	
			  

			/* $sql = "SELECT c.`company_name` AS brand_name,c.`user_id` as brand_id, b.`name` AS product_name,b.`price`, a.`click_count` as click_count, a.`user_id`, a.`created_datetime` FROM users_product_click_track AS a, product_desc AS b LEFT JOIN brand_info AS c ON b.`brand_id` = c.`user_id` WHERE a.`product_id` = b.`id` ".$querycond1." ORDER BY c.`user_id` DESC  ";
	         $query = $this->db->query($sql);	        
	         $result = $query->result_array();	             
	         $click_array = array();
	         $price_array = array();
	         $cost_array = array();
	         foreach($result as $val)
	         {
	         	if($val['brand_name']!="" && $val['brand_id']!="")
	         	{
	         		$click_array[] = $val['click_count'];
	         		$price_array[] = $val['price'];
	         		$cost_array[] = $val['click_count'] * $val['price'] * 0.001;
 	         	}else
 	         	{
 	         		$click_array[] = $val['click_count'];
	         		$price_array[] = $val['price'];
	         		$cost_array[] = $val['click_count'] * $val['price'] * 0.001;
 	         	}
	         	
	         }
	        
	         $brand_product_click_count = array_sum($click_array);
	         $brand_cpc = array_sum($cost_array);     	         
			 $brand_data['brand_product_click_count'] = $brand_product_click_count;	
			 $brand_data['brand_cpc'] =  $brand_cpc;*/	

			return $brand_data;
	    }


	    public function click_report($brand_id)
	    {	    	
	    	$date = date('Y-m-d');
	    	$brand_data = array();
	    	$user_id = $this->session->userdata('user_id');
	    	$user_role_id = $this->session->userdata('role_id');
	    	$querycond = "";

	    	 if($user_role_id == 1)
	    	{
	    		$querycond1 = "AND 1=1";
	    		if($brand_id!="")
	    		{
	    			$querycond1 = "AND b.`brand_id` = ".$brand_id." ";
	    		}
	    	}else
	    	{
	    		$querycond1 = "AND b.`brand_id` = ".$user_id." ";
	    	}
			 
			  
			 /* Get Brand Product Click Count Overall*/
			$sql = "SELECT c.`company_name` AS brand_name,c.`user_id` as brand_id, b.`name` AS product_name,b.`price`, a.`click_count` as click_count, a.`user_id`, a.`created_datetime` FROM users_product_click_track AS a, product_desc AS b LEFT JOIN brand_info AS c ON b.`brand_id` = c.`user_id` WHERE a.`product_id` = b.`id` ".$querycond1." ORDER BY c.`user_id` DESC  ";
	         $query = $this->db->query($sql);	        
	         $result = $query->result_array();	             
	         $click_array = array();
	         $price_array = array();
	         $cost_array = array();
	         foreach($result as $val)
	         {
	         	if($val['brand_name']!="" && $val['brand_id']!="")
	         	{
	         		$click_array[] = $val['click_count'];
	         		$price_array[] = $val['price'];
	         		$cost_array[] = $val['click_count'] * $val['price'] * 0.001;
 	         	}else
 	         	{
 	         		$click_array[] = $val['click_count'];
	         		$price_array[] = $val['price'];
	         		$cost_array[] = $val['click_count'] * $val['price'] * 0.001;
 	         	}
	         	
	         }
	        
	         $brand_product_click_count = array_sum($click_array);
	         $brand_cpc = array_sum($cost_array);     	         
			 $brand_data['brand_product_click_count'] = $brand_product_click_count;	
			 $brand_data['brand_cpc'] =  $brand_cpc;
			return $brand_data;
	    }

	 
	   public function track_cpa(){
	        //if(isset($this->session->userdata('role_id')) $role_id = ;
	       /* 
	        if($this->session->userdata('role_id') != 6){
		    	$sql = "select count(product_id) as product_id  from product_cpa_track ";
		        $query = $this->db->query($sql);	        
		        $result = $query->result_array(); 
		        return $result[0]['product_id'];	      
				}
				else{
					$sql = "select count(a.product_id) as product_id from product_cpa_track as a, product_desc as b where a.product_id = b.id AND b.brand_id='".$this->session->userdata('user_id')."'";
				        $query = $this->db->query($sql);	        
				        $result = $query->result_array(); 
				        return $result[0]['product_id'];	
				}    */
	    }

	    public function topfivereports()
	    {
	    	$brand_data = array();
	    	$user_id = $this->session->userdata('user_id');
	    	$user_role_id = $this->session->userdata('role_id');
	    	$querycond = "";

	    	 if($user_role_id == 1)
	    	{
	    		$querycond1 = "AND 1=1";
	    	}else
	    	{
	    		$querycond1 = "AND b.`brand_id` = ".$user_id." ";
	    	}

	    	 /* Get Top Five Product Click Count Overall*/
			 $sql = " SELECT c.`company_name` AS brand_name,c.`user_id` as brand_id, b.`name` AS product_name,b.`price`, sum(a.`click_count`) as click_count, a.`user_id`, a.`created_datetime` FROM users_product_click_track AS a, product_desc AS b LEFT JOIN brand_info AS c ON b.`brand_id` = c.`user_id` WHERE a.`product_id` = b.`id` and a.`user_id`!=0 ".$querycond." GROUP BY `brand_name` order by click_count DESC  Limit 0,5";
			 $query = $this->db->query($sql);
			 $brand_data['product_click'] = $query->result_array();	
			 $i=0;

			 foreach($brand_data['product_click'] as $val)
			 {
			 	$prd_click=$this->click_report($val['brand_id']);		 	

			 	$brand_data['product_click'][$i]['brand_product_click_count']=$prd_click['brand_product_click_count'];
			 	$brand_data['product_click'][$i]['brand_cpc']=$prd_click['brand_cpc'];
			 	$i++;			 	
			 }

			 return $brand_data['product_click'];
	    }

	   
 } 
?>
