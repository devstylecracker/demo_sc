<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lookcreator_mobile_model extends MY_Model {
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_categories(){
    	$query = $this->db->get_where('product_sub_category', array('is_delete' =>0));
    	$res = $query->result_array();
    	return $res;
    }

    function get_subcategories($cat_id){
    	$query = $this->db->get_where('product_variation_type', array('is_delete' =>0,'prod_sub_cat_id'=>$cat_id));
    	$res = $query->result_array();
    	return $res;
    }

    function get_variation_value($cat_id){
    	$query = $this->db->get_where('product_variation_value', array('is_delete' =>0,'product_variation_type_id'=>$cat_id));
    	$res = $query->result_array();
    	return $res;
    }

    function get_products($variation_value){
    	$query = $this->db->query('SELECT distinct a.`product_id` , b.`name` , b.`image` FROM `product_variations` AS a, `product_desc` AS b,`product_tag` as c WHERE a.`product_id` = b.`id` AND b.status =1 and c.`product_id` = b.`id` and a.`product_var_val_id`='.$variation_value);
		$res = $query->result_array();
    	return $res;
    }

    function get_allcategories()
    {
      
        $data = array();
        $sub_cat = $this->get_categories();
        //print_r($sub_cat);
        foreach($sub_cat as $val){
            $vari_type = array();

            $variation_cat =  $this->get_subcategories($val['id']);

            foreach($variation_cat as $key => $value) {

                $variation_val =  $this->get_variation_value($value['id']);
                $vari_val = array();

                foreach($variation_val as $ke => $valu) {
                    $vari_val[] = array("id"=>$valu['id'],"name"=>$valu['name']);
                }

                $vari_type[] = array("id"=>$value['id'],"name"=>$value['name'],"variations_value"=>$vari_val);
            }

            
            $data[] = array("id"=>$val['id'],"name"=>$val['name'],"variations"=>$vari_type);

        }
        
        return $data;

    }
       

     function get_allproducts()
    {
      
         $data = array();
        $sub_cat = $this->get_categories();
        //print_r($sub_cat);
         foreach($sub_cat as $val){
             $vari_type = array();

            $variation_cat =  $this->get_subcategories($val['id']);

            foreach($variation_cat as $key => $value) {

                $variation_val =  $this->get_variation_value($value['id']);
                $vari_val = array();

                foreach($variation_val as $ke => $valu) {
                    $vari_val[] = array("id"=>$valu['id'],"name"=>$valu['name']);

                    $product =  $this->get_products($valu['id']);
                    $prd_val = array();

                    foreach($product as $k => $v) {                                              
                        $prd_val[] = array("id"=>$v['product_id'],"name"=>$v['name'],"imagename"=>$v['image']);
                    }
                }

               // $data[] = array("id"=>$value['id'],"name"=>$value['name'],"product"=>$prd_val);
        $data[] = $prd_val;
            }

            //$imagepath = base_url().'assets/products/thumb/';  
           // $data[] = array("id"=>$val['id'],"name"=>$val['name'],"variations"=>$vari_type);

        }
        
        return $data;

    }


}