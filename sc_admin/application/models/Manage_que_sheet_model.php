<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_que_sheet_model extends MY_Model {
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function add_que_sheet($file_name,$numbers_of_products_list,$status,$uploaded_by,$brands,$progress){
		$sql = "INSERT INTO `que_sheet_file_log`
							(`file_name`,
							`numbers_of_products_list`,
							`status`,
							`uploaded_by`,
							`brands`,
							`progress`)
							VALUES
							('$file_name',
							'$numbers_of_products_list',
							'$status',
							'$uploaded_by',
							'$brands',
							'$progress');";
		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	function update_que_sheet($product_in_draft,$product_in_error,$product_in_published,$status,$progress,$file_name){
		$sql = "update `que_sheet_file_log`
							set 
							`status`='$status',
							`progress`='$progress',
							`product_in_draft`='$product_in_draft',
							`product_in_error`='$product_in_error',
							`product_in_published`='$product_in_published',
							`end_time`=now() where file_name = '$file_name'";

		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

}

