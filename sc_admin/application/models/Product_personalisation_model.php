<?php

class Product_personalisation_model extends MY_Model {
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    function show_gender_specific_personalisation($gender){
    	$where_cond='';
    	if($gender==1){ $where_cond=$where_cond.' and a.id<=36';}
    	if($gender==3){ $where_cond=$where_cond.' and a.id>36';}
    	$query = $this->db->query("select a.id,(select answer from answers where id =a.body_shape) as body_shape,(select answer from answers where id = a.style) as style,(select answer from answers where id = a.work_style) as work_style,(select answer from answers where id = a.personal_style) as personal_style from bucket as a where 1 ".$where_cond);
        $res = $query->result_array();
        return $res;
    }

    function show_category_specific_personalisation($show_category_specific_personalisation){
    	$query = $this->db->query("select a.object_id,b.category_name from pa_object_relationship as a,category as b where a.object_type='category' and a.pa_type='bucket' and a.object_id=b.id and a.object_value=".$show_category_specific_personalisation."");
        $res = $query->result_array();
        return $res;
    }

    function get_all_attributes(){
        $this->db->from('attributes');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    function product_personalisation_logic($offset){
        $length=50;
        $offset=$offset*$length;
        $query = $this->db->query("SELECT `id`, (select category_name from category where id=`object_id`) as cat_name,`object_type`, `pa_type`, `object_value`, `attribute_value_ids` FROM `pa_object_relationship` WHERE 1 and pa_type='bucket' and object_type='category' and attribute_value_ids!='' order by `modified_datetime` desc limit ".$offset.",".$length);
        $res = $query->result_array();
        return $res;
    }

    function get_bucket_name($bucket){
        $query = $this->db->query("select a.id,(select answer from answers where id =a.body_shape) as body_shape,(select answer from answers where id = a.style) as style,(select answer from answers where id = a.work_style) as work_style,(select answer from answers where id = a.personal_style) as personal_style from bucket as a where 1 and a.id=".$bucket);
        $res = $query->result_array();
        return strip_tags($res[0]['body_shape'].''.$res[0]['style'].''.$res[0]['work_style'].''.$res[0]['personal_style']);
    }

    function attribute_value_data($attr){
        if($attr!=''){ $dt = array(); $ht_data = '';
            $logic = unserialize($attr);
            if(!empty($logic)){
                $attr_val_ids = implode(',', $logic);

                $query = $this->db->query("select a.attr_value_name,b.attribute_name from attribute_values_relationship as a,attributes as b where a.attribute_id=b.id and a.id IN (".$attr_val_ids.") order by a.attribute_id");
                $res = $query->result_array();
                if(!empty($res)){ $i=0;
                    foreach ($res as $value) {
                        $dt[$value['attribute_name']][$i] = $value['attr_value_name'];
                        $i++;
                    }
                }
                unset($i);

            }

            if(!empty($dt)){ $i=0;
                foreach ($dt as $key=>$val){
                    if($i!=0){ $ht_data = $ht_data. '<br>'; }
                    $ht_data = $ht_data. $key.' => ';
                    $ht_data = $ht_data. $vv = implode(', ', $val);
                    $i++;
                }
            }

        }
        return $ht_data;
    }

    function delete_pa($id){
        $this->db->where('id', $id);
        $this->db->update('pa_object_relationship', array('attribute_value_ids'=>''));
    }

    function existing_attr_values($personality,$category){
        $query = $this->db->query("select attribute_value_ids from pa_object_relationship where object_type='category' and pa_type='bucket' and object_id='".$category."' and object_value='".$personality."' and attribute_value_ids!='' limit 0,1");
        $res = $query->result_array();
        if(!empty($res)){
            return unserialize($res[0]['attribute_value_ids']);
        }else{
            return '';
        }
    }

    function save_data($personality,$category,$attributes){
        if($personality>0 && $category>0 and $attributes!=''){
            $attributes = explode(',',$attributes);
            $attr = serialize($attributes);

            $data = array('attribute_value_ids'=>$attr);

            $this->db->where(array('object_id'=>$category,'object_type'=>'category','object_value'=>$personality,'pa_type'=>'bucket'));
            $this->db->update('pa_object_relationship',$data);

        }
    }

    function show_gender_specific_categories($gender){
        $gender_id='1';
        if($gender==1){ $gender_id='1'; }
        if($gender==3){ $gender_id='3'; }

        $query = $this->db->query("select id,category_name,category_parent from (select * from category order by category_parent, id) products_sorted,(select @pv := '".$gender_id."') initialisation where find_in_set(category_parent, @pv) > 0 and @pv := concat(@pv, ',', id)");
        $res = $query->result_array();
        return $res;
    }

    function save_data_logic($gender,$category){
        $option_name = 'women_upselling_cat';
        if($gender == 3){ $option_name = 'men_upselling_cat'; }
        $category = serialize($category);
        $this->add_sc_options($option_name,$category);
    }

    function get_options($option_name){
        return unserialize($this->get_sc_options($option_name));
    }

    function show_results($order_id){
        $product_id = '';
        $query = $this->db->query("select GROUP_CONCAT(id) as id from order_info where order_unique_no='".$order_id."' group by order_unique_no");
        $res = $query->result_array();
        if(!empty($res)){

            $order = explode('_', $order_id);
            $user_id = $order[0];

            $query = $this->db->query("select product_id from order_product_info where order_id IN (".$res[0]['id'].")");
            $res = $query->result_array();

            if(!empty($res)){ $i=0;
                foreach($res as $val){
                    if($i!=0){ $product_id = $product_id.','; }
                    $product_id = $product_id.$val['product_id'];
                    $i++;
                }

                if($product_id!='' && $user_id!=''){
                    $p_ids='';
                    $bucket = $this->get_users_bucket($user_id);

                    if($bucket>0){
                        $p_id = $this->recommended_products($bucket,$product_id);
                        $p_ids = $this->in_stock_check($p_id);
                    }else{
                        $p_id = $this->show_upselling_cat_products();
                        $p_ids = $this->in_stock_check($p_id);
                    }

                    if($p_ids!=''){

                        $query = $this->db->query("select name,image from product_desc where id IN(".$p_ids.") and id NOT IN(".$product_id.") and brand_id IN (select user_id from brand_info where is_paid=1) order by RAND() limit 0,10");
                        $res = $query->result_array();
                        if(!empty($res)){
                            foreach($res as $val){
                                echo '<span class="img-wrp"><img heigth="20px" title="'.$val['name'].'" src="https://www.stylecracker.com/sc_admin/assets/products/thumb_124/'.$val['image'].'"></span>';
                            }
                        }

                    }else{
                        return 'No products recommended for you';
                    }


                }else{
                    return 'No products found';
                }

            }else{
                return 'Sorry products from these order is not delivered yet';
            }

        }else{
            return 'Please enter valid Order Id';
        }
    }

    function get_users_bucket($user_id){
        $query = $this->db->query("select bucket from user_login where id=".$user_id);
        $res = $query->result_array();
        return $res[0]['bucket'];
    }

    function recommended_products($bucket){
        $product_id = ''; 
        if($this->session->userdata('user_id')>0){
            $query = $this->db->query("select a.attribute_value_ids from (select object_id,attribute_value_ids from pa_object_relationship where object_type='category' and pa_type='bucket' and object_value =".$bucket." and attribute_value_ids!='') as a ");
            $res = $query->result_array($query);
           
            if(!empty($res)){ $i=0;$cat_id = '';
            foreach($res as $val){
                if($val['attribute_value_ids']!=''){
                    if($i!=0){ $cat_id = $cat_id.','; }

                    $dt = unserialize($val['attribute_value_ids']);
                    $cat_ids = implode(',', $dt);
                    $cat_id = $cat_id.$cat_ids;
                    $i++;
                }
            $w_c3='';
            if($cat_id==''){ $cat_id = '0'; }
             $get_pro_id1 = $this->db->query("select GROUP_CONCAT(id) as att from attribute_values_relationship where id IN (".$cat_id.") group by attribute_id");
             $resul1 = $get_pro_id1->result_array();
             $att = array();

             
             if(!empty($resul1)){ $f = 0;
                $w_c3 = ''; $w_c3 = $w_c3.' Having (';
                foreach($resul1 as $vall){ 
                    $att = explode(',', $vall['att']);
                    if($f!=0){ $w_c3 = $w_c3.') and ('; }
                    $k=0;
                    foreach($att as $val){
                        if($k!=0){ $w_c3 = $w_c3.' or '; }
                        $w_c3 = $w_c3.' Find_In_Set('.$val.',attr)';
                        $k++;
                    }
                    $f++;

                }

                $w_c3 = $w_c3.')'; 

            if($w_c3!=''){
               $query11 = $this->db->query("select object_id ,GROUP_CONCAT(attribute_id) as attr from attribute_object_relationship group by object_id".$w_c3);
                $res11 = $query11->result_array();
            
                if(!empty($res11)){
                    foreach($res11 as $val1){
                        $product_id = $product_id.','.$val1['object_id'];
                       
                    }
                }
            }
        }
            }
        }
        }
        $product_id = $this->product_availability($product_id);
        if($product_id==''){
            $product_id1 = $this->recommended_products_via_category($bucket);
            $product_id = $this->product_availability($product_id1);
        }
        //echo $product_id;
        return $product_id;
    
    }

    function product_availability($product_ids){
        $pro_id = '';
        
        $product_ids = rtrim($product_ids,",");
        if($product_ids!=''){
                $query11 = $this->db->query("select id from product_desc where status=1 and is_delete=0 and approve_reject IN('A','D') and id in(0".$product_ids.")");
                $res11 = $query11->result_array();

                if(!empty($res11)){
                    foreach($res11 as $val){

                        $query12 = $this->db->query("select SUM(stock_count) as stock from `product_inventory` where product_id =".$val['id']." group by product_id having stock>0 ");

                        $res12 = $query12->result_array();
                       
                        if($res12)
                        {
                            if($pro_id!='')
                            {
                                 $pro_id = $pro_id.','.$val['id'];  
                            }else
                            {
                                 $pro_id = $val['id'];  
                            }
                         
                        }
                        
                    }
                }
                
             return $pro_id;
        }else{
            return $pro_id;
        }
    }

    function recommended_products_via_category($bucket){
            $pro_ids = '';
            if($bucket!=''){
    
            $query = $this->db->query("select object_id from pa_object_relationship where object_type='category' and pa_type='bucket' and object_value =".$bucket." ");

            $res = $query->result_array();

            $cat_id =''; $product_id = '';
            if(!empty($res)){ 
                foreach($res as $val){
                    $cat_id = $cat_id.','.$val['object_id'];
                }
            

                $query1 = $this->db->query("select distinct object_id from category_object_relationship where category_id IN(-1".$cat_id.") and object_type='product'");
                $res1 = $query1->result_array();
                if(!empty($res1)){
                    foreach($res1 as $val){
                        $product_id = $product_id.','.$val['object_id'];
                    }
                }
                
                return $product_id; 

            }else{
                return '';    
            }
            }
    }

    function in_stock_check($prod_id){
        $query = $this->db->query("select distinct product_id from product_inventory where stock_count>0 and product_id IN (0".$prod_id.")");
        $res1 = $query->result_array();
        $pro_id = '';
        if(!empty($res1)){ $i=0;
            foreach ($res1 as $key => $value) {
               if($i!=0){ $pro_id = $pro_id.','; }
               $pro_id = $pro_id.$value['product_id'];
               $i++;
            }
        }
        return $pro_id;
    }

    function show_upselling_cat_products(){
        $option_name = 'women_upselling_cat';
        $option_name = 'men_upselling_cat'; 

        $women_upselling_cat = $this->get_options('women_upselling_cat');
        $men_upselling_cat = $this->get_options('men_upselling_cat');

        $final_cat = array_merge($women_upselling_cat,$men_upselling_cat);

        if(!empty($final_cat)){ 
            $final_cat_ids = '';
            $final_cat_ids = implode(',', $final_cat);
            $product_id = '';

            $query = $this->db->query("select object_id from category_object_relationship where category_id IN (".$final_cat_ids.") and object_type='product' order by object_id desc");
            $res1 = $query->result_array();
            if(!empty($res1)){
                    foreach($res1 as $val){
                        $product_id = $product_id.','.$val['object_id'];
                    }
                }
                
            return $product_id; 

        }
    }

    function recommended_products_new($bucket,$product_id){
        $prod_id = explode(',', $product_id);

        $this->get_category_id($prod_id);

    }

    function get_category_id($prod_id){
        $cat_ids = array();
        if(!empty($prod_id)){
            foreach($prod_id as $val){

                $query = $this->db->query("select a.category_id from category_object_relationship as a,category as b where a.category_id = b.id and a.object_type='product' and a.`object_id`=$val and b.status = 1 order by a.category_id desc limit 0,1");
                
                $res = $query->result_array();
                $cat_ids[] = $res[0]['category_id'];

            }
        }
        return $cat_ids;
    }

    function show_complementary_results($order_id,$order_prdid){
        $product_id = '';
        $recom_array = array();

        /*$query = $this->db->query("select GROUP_CONCAT(id) as id from order_info where order_unique_no='".$order_id."' group by order_unique_no");
        $res = $query->result_array();*/
        $query = $this->db->query("select id,user_id from order_product_info where id='".$order_prdid."' ");
        $res = $query->result_array();       
        
        if(!empty($res)){

            /*$order = explode('_', $order_id);
            $user_id = $order[0];*/
            $user_id = $res[0]['user_id'];

            /*$query = $this->db->query("select product_id from order_product_info where order_id IN (".$res[0]['id'].")");
            $res = $query->result_array();*/
            $query = $this->db->query("select product_id from order_product_info where id IN (".$res[0]['id'].")");
            $res = $query->result_array();

            if(!empty($res)){ $i=0;
                foreach($res as $val){
                    if($i!=0){ $product_id = $product_id.','; }
                    $product_id = $product_id.$val['product_id'];
                    $i++;
                }
                $product_arr[]=$product_id;
                $product_cat = $this->get_category_id($product_arr);

                if($product_id!='' && $user_id!=''){
                    $p_ids='';
                    $bucket = $this->get_users_bucket($user_id);
                    $recom_array['user']['email']=$this->get_user_info($user_id)[0]['email'];                    
                    $product_arr[]=$product_id;
                    $recom_array['bought'] = $this->getProductData($product_id);
                    
                    $product_cat = $this->get_category_id($product_arr);                    
                    $product_parentd = $this->get_parent_cat_info($product_cat[0]);                                   
                    $prd_cat = explode('-',$product_parentd[0]['category_slug'])[0];
                    $prd_parentcat = $product_parentd[0]['category_parent'];
                    //echo '<pre>';print_r($prd_cat);exit;
                   /* echo $prd_cat;exit;
                    echo '<pre>';print_r($product_parent);
                    echo '<br/>';*/
                    if($bucket>0){
                                                
                        $p_id = $this->recommended_comp_products($bucket,$product_cat);                       
                        if($prd_cat == 'men' && $bucket<36 )
                        {
                            $p_id = $this->get_parentcat_products($prd_parentcat);

                        }else if( ($prd_cat == 'women' || $prd_cat == 'womens') && $bucket>36)
                        {
                            $p_id = $this->get_parentcat_products($prd_parentcat);
                        }
                        $p_ids = $this->in_stock_check($p_id);
                    }else{
                       
                        $product_ids = $this->in_stock_check($product_id);
                        $comp_cat = $this->get_comp_category_id($product_ids,$product_cat); 
                        
                        if($comp_cat[0]==-1)
                        {
                            $product_parentd = $this->get_parent_cat_info($product_cat[0]);
                            $comp_cat[0] = $product_parentd[0]['category_parent'];
                        }                        
                      
                        $p_id = $this->show_upselling_comp_cat_products($comp_cat);
                         
                        $p_ids = $this->in_stock_check($p_id);
                    }
                   
                    if($p_ids!=''){

                       /* $query = $this->db->query("select name,image from product_desc where id IN(".$p_ids.") and id NOT IN(".$product_id.") and brand_id IN (select user_id from brand_info where is_paid=1) order by RAND() limit 0,10");*/

                        $query = $this->db->query("select id,name,image,price,slug from product_desc where id IN(".$p_ids.") and id NOT IN(".$product_id.") and brand_id IN (select user_id from brand_info where is_paid=1) order by modified_datetime,RAND() limit 1,100");
                        $res = $query->result_array();
                        shuffle($res);
                        $i=0;
                        if(!empty($res)){
                            foreach($res as $val){
                                if($i<3)
                                {
                                echo '<span class="img-wrp"><img heigth="20px" title="'.$val['name'].'" src="https://www.stylecracker.com/sc_admin/assets/products/thumb_124/'.$val['image'].'"></span>';
                                    $recom_array['recomm'][$i]['name'] = $val['name'];
                                    $recom_array['recomm'][$i]['image'] = $val['image'];
                                    $recom_array['recomm'][$i]['price'] = $val['price'];
                                    $recom_array['recomm'][$i]['prd_link'] = 'https://www.stylecracker.com/product/details/'.$val['slug'].'-'.$val['id'];
                                }
                                $i++;
                            }
                        }
                        return $recom_array;

                    }else{
                        return 'No products recommended for you';
                    }


                }else{
                    return 'No products found';
                }

            }else{
                return 'Sorry products from these order is not delivered yet';
            }

        }else{
            return 'Please enter valid Order Id';
        }
    }

    function recommended_comp_products($bucket,$product_cat){
        $product_id = '';
        $product_catstr= implode(',',$product_cat);
        if($product_catstr==''){
            $product_id1 = $this->recommended_comp_products_via_category($bucket); 
            $product_id = $this->product_availability($product_id1);           
        }else
        {
            $product_id1 = $this->recommended_comp_products_via_category($bucket,$product_catstr); 
            $product_id = $this->product_availability($product_id1);
        }
        return $product_id;
    
    }

    function recommended_comp_products_via_category($bucket,$product_cat=NULL){
            $pro_ids = '';
            //echo $bucket.'--,--'.$product_cat;
            
            $prod_cat = "";                
            if($product_cat!='')
            {
                $prod_cat = "and `object_id` NOT IN(".$product_cat.")";
            }
           
            if($bucket!=''){                
    
            $query = $this->db->query("select distinct object_id from pa_object_relationship where object_type='category' and pa_type='bucket' and object_value =".$bucket."  ".$prod_cat." order by RAND(),object_id desc ");           

            $res = $query->result_array();
           // echo '<pre>';print_r($res);exit;
            $cat_id =''; $product_id = '';$productId = '';$product_count = 0;$productArray=array();$productIdnew = '';
            /*if(!empty($res)){ 
                foreach($res as $val){
                    $cat_id = $cat_id.','.$val['object_id'];
                }*/
           
                if(!empty($res))
                {
                    for($i=0; ($i<count($res)&& $product_count<3); $i++)
                    { $product_id = '';
                        
                       /* $query1 = $this->db->query("select distinct object_id,category_id from category_object_relationship where category_id IN(-1".$cat_id.") and object_type='product'");*/
                            $query1 = $this->db->query("select distinct object_id from category_object_relationship where category_id IN(".$res[$i]['object_id'].") and object_type='product' order by RAND() ");
                            $res1 = $query1->result_array();

                            if(!empty($res1))
                            { 
                                foreach($res1 as $val){
                                    if($product_id!='')
                                    {
                                        if($val['object_id']!='')
                                        {
                                            $product_id = $product_id.','.$val['object_id'];
                                        }
                                       
                                    }else
                                    {
                                         if($val['object_id']!='')
                                        {
                                             $product_id = $val['object_id'];
                                        }
                                       
                                    }
                                    
                                }
                                if($product_id!='')
                                {
                                    $productId = $this->product_availability($product_id);                        
                                    $productArray = explode(',', $productId);
                                }
                            }
                              $productArray = array_unique($productArray);                  
                            if(!empty($productArray))
                            {
                                if($productIdnew!='' && $productArray[0]!='')
                                {
                                    $productIdnew = $productIdnew.','.@$productArray[0];   
                                }else
                                {
                                    if($productArray[0]!='')
                                    {
                                        $productIdnew = $productArray[0];
                                    }
                                    
                                }
                               
                               $product_count++;                        
                            }                                   
                                                  
                    }                                
                
               /* echo $this->db->last_query();*/
                /*if(!empty($res1)){
                    foreach($res1 as $val){
                        $product_id = $product_id.','.$val['object_id'];
                    }
                }*/
               
                return $productIdnew; 

            }else{
                return '';    
            }
            }
    }

    function show_upselling_comp_cat_products($compcat){
        $final_cat = $compcat;
       
        if(!empty($final_cat)){ 
            $final_cat_ids = '';           
            $product_id = '';
            
            foreach($final_cat as $val)
            {
                $query = $this->db->query("select object_id from category_object_relationship where category_id IN (".$val.") and object_type='product' order by object_id desc");
                $res1 = $query->result_array();
               /* echo '<pre>';print_r($res1);exit;*/
                if(!empty($res1)){                       

                        foreach($res1 as $v)
                        {
                            if($product_id!='')
                           {
                               $product_id = $product_id.','.$v['object_id']; 
                           }else
                           {
                                $product_id = $v['object_id']; 
                           }
                            
                        }
                        
                    }
            }
                
            $p_ids = $this->in_stock_check($product_id);
            return $p_ids; 

        }
    }

    function get_comp_category_id($prodstr,$selProductcat){        
        $prod_id = explode(',', $prodstr);   
        $selProductcatStr = implode(',',$selProductcat);    
        $cat_ids = array();
        if(!empty($prod_id)){
            foreach($prod_id as $val){

                $query = $this->db->query("select b.comp_cat from category_object_relationship as a,category as b where a.category_id = b.id and a.object_type='product' and b.status = 1 and a.object_id=".$val." and a.`category_id` NOT IN ($selProductcatStr) order by a.category_id desc limit 0,1");
                $res = $query->result_array();
              /*  echo $this->db->last_query();exit;*/
               if($res[0]['comp_cat']!='')
               {
                 $cat_ids[] = $res[0]['comp_cat'];
               }               

            }
        }       
        return $cat_ids;
    }

    function get_parent_categoryids($prodstr,$selProductcat){        
        $prod_id = explode(',', $prodstr);   
        $selProductcatStr = implode(',',$selProductcat);    
        $cat_ids = array();
        if(!empty($prod_id)){
            foreach($prod_id as $val){

                $query = $this->db->query("select b.category_slug,b.category_parent from category_object_relationship as a,category as b where a.category_id = b.id and a.object_type='product' and b.status = 1 and a.object_id=".$val." and a.`category_id` NOT IN ($selProductcatStr) order by a.category_id desc limit 0,1");
                $res = $query->result_array();
               /* echo $this->db->last_query();exit;*/
               if($res[0]['category_parent']!='')
               {
                 $cat_ids[] = $res[0]['category_parent'];
               }                

            }
        }        
        return $cat_ids;
    }

     function get_parent_cat_info($id){

        if($id!=''){
            $query = $this->db->query("select id,category_parent,category_slug from category where id=".$id." ");
            //and category_parent not in(1,3,2)
            $res = $query->result_array();           
            if(!empty($res)){ return $res; }else{ return ''; }
        }
    }

    function get_parentcat_products($p_cat)
    {
        $product_id='';$productId='';
        if($p_cat!='')
        {
            $query1 = $this->db->query("select distinct object_id from category_object_relationship where category_id IN(".$p_cat.") and object_type='product' order by RAND() ");            
            $res = $query1->result_array();
             if(!empty($res))
            { 
                foreach($res as $val){
                    if($product_id!='')
                    {
                        if($val['object_id']!='')
                        {
                            $product_id = $product_id.','.$val['object_id'];
                        }
                       
                    }else
                    {
                         if($val['object_id']!='')
                        {
                             $product_id = $val['object_id'];
                        }
                       
                    }
                    
                }
                if($product_id!='')
                {
                    $productId = $this->product_availability($product_id);                        
                    //$productArray = explode(',', $productId);
                }
            }
            return $productId;
        }
    }

    function getProductData($p_ids)
    {
        $query = $this->db->query("select id,name,image,price,slug from product_desc where id IN(".$p_ids.") and brand_id IN (select user_id from brand_info where is_paid=1) order by modified_datetime ");
        $res = $query->result_array();
        return $res;
    }

    function get_user_info($user_id){
        $query = $this->db->get_where('user_login', array('id' => $user_id));
        return $query->result_array();
    }

    function get_deliver_order()
    {
        //$query = $this->db->query("select id,product_id from order_product_info where order_status = 4 and modified_datetime Like date('Y-m-d')");
        $query = $this->db->query("select id,product_id,order_status,DATE_ADD(modified_datetime,INTERVAL 7 DAY) AS OrderRecomDate from order_product_info group by modified_datetime having order_status = 4 and OrderRecomDate Like '".date('Y-m-d')."%' ");
        //echo $this->db->last_query();exit;
        $res = $query->result_array();
        return $res;
    }

 }
?>