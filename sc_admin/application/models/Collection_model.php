<?php
class Collection_model extends MY_Model {

    
    public $ouptut = array();
    public $depth = 0; public $children = 0;
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
    }
    
    function parent_catgory_list(){
		$query = $this->db->get_where('category', array('category_parent' => '-1'));
		$res = $query->result_array();
		return $res;
	}
	
	function get_brands_list(){
		// $query = $this->db->query("select a.id,a.user_name, a.bucket, a.created_datetime, a.email,c.cpa_percentage from user_login as a, user_role_mapping as b,brand_info as c WHERE a.`status` =1 and a.`id` = b.`user_id` and b.`role_id`=6 and a.`id` = c.`user_id` and c.is_paid=1 order by a.user_name asc ");
		$query = $this->db->query("select a.id,a.user_name, a.bucket, a.created_datetime, a.email,c.cpa_percentage,c.company_name from user_login as a, user_role_mapping as b,brand_info as c WHERE a.`status` =1 and a.`id` = b.`user_id` and b.`role_id`=6 and a.`id` = c.`user_id` and c.is_paid=1 AND c.`show_in brand_list` = 1 order by a.user_name asc ");
		$res = $query->result_array();
		return $res;
	}
	
	function save_collection($object_data,$object_meta_data){
			$this->db->insert('object',array('object_name'=>$object_data['collection_name'],'object_description'=>'collection_name','object_type'=>'collection','object_created'=>date('Y-m-d H:i:s'),'object_creator'=> $this->session->userdata('user_id'),'object_modifier'=> $this->session->userdata('user_id'),'object_image'=>$object_meta_data['collection_img']));
			$object_id = $this->db->insert_id();
			$object_meta_keys = array_keys($object_meta_data);$i=0;
			foreach($object_meta_data as $val){
				$this->db->insert('object_meta',array('object_id'=>$object_id,'object_meta_key'=>$object_meta_keys[$i],'object_meta_value'=>$val,'created_datetime'=>date('Y-m-d H:i:s'),'created_by'=> $this->session->userdata('user_id'),'modified_by'=> $this->session->userdata('user_id')));
				$i++;
			}
			return $object_id;
	}
	
	function get_all_collection($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL){

		if($search_by == '1'){ $search_cond = " AND a.object_id = '".strip_tags(trim($table_search))."'"; }
		else if($search_by == '2'){ $search_cond = " AND a.object_name LIKE '%".strip_tags(trim($table_search))."%'"; }
		// else if($search_by == '3'){ $search_cond = " AND a.look_type = 1"; }
        else{ $search_cond =''; }


		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
                                $limit_cond = " LIMIT ".$page.",".$per_page;
                        }else{
                                $limit_cond = '';
                        }

		$query = $this->db->query("select a.object_id as id,a.object_name as name,a.object_image as image,a.object_created as created_datetime,a.object_modified as modified_datetime,b.object_meta_key,b.object_meta_value  from object as a,object_meta as b where a.object_id = b.object_id AND a.object_type = 'collection' AND b.object_status != 0 ".$search_cond." GROUP BY a.object_id order by a.object_id desc ".$limit_cond);
		$result = $query->result_array();
		// echo "<pre>";print_r($result);exit;
		return $result;
	}
	
	function delete_collection($id){
		
		$data = array('object_status'=>0,"modified_by"=>$this->session->userdata('user_id'));
		$this->db->where('object_id', $id);
		$this->db->update('object_meta', $data);
		return true;
	}
	
	function get_single_product($product_id){
		$query = $this->db->query("select id,name,description,image,slug,brand_id,stock_count,sku_number,price,compare_price from product_desc where id = $product_id and stock_count > 0 "); 
		$result = $query->result_array();
		$product_array = array();
		$i=0;
		$imagify_brandid = unserialize(IMAGIFY_BRANDID); 
		foreach($result as $val){
			$product_array[$i]['id'] = $val['id'];
			$product_array[$i]['name'] = $val['name'];
			$product_array[$i]['description'] = $val['description'];
			$product_array[$i]['slug'] = $val['slug'];
			$product_array[$i]['image_name'] = $val['image'];
			$product_array[$i]['stock_count'] = $val['stock_count'];
			$product_array[$i]['sku_number'] = $val['sku_number'];
			$product_array[$i]['price'] = $val['price'];
			$product_array[$i]['compare_price'] = $val['compare_price'];
			$product_array[$i]['product_category_id'] = @$this->get_product_category_level2($val['id'])[0]['id'];
			$product_array[$i]['product_category_name'] = @$this->get_product_category_level2($val['id'])[0]['category_name'];
			// added for new product url  
			$productUrl = '';
			if(in_array($val['brand_id'],$imagify_brandid)){
				$productUrl = $this->config->item('products_tiny').$val['image'];
			}else{
				$productUrl = base_url().'assets/products/thumb_124/'.$val['image'];                
			}
			// added for new product url
			$product_array[$i]['image'] = $productUrl;
			$product_array[$i]['brand_id'] = $val['brand_id'];
			$i++;
		}
		
		return $product_array;
	}
	
	function get_products($product_ids){
		$query = $this->db->query("select id,name,description,image,slug from product_desc where id in($product_ids) ");
		$result = $query->result_array();
		return $result;
	}
	
	function get_product_list($brand,$cat,$attributes_chosen,$category_specific_attr,$lookoffset,$popup_tags=null){
    	$lookoffset = $lookoffset*LOOK_CREATOR_PAGE_OFFSET; $where_cond = '';
    	if($this->session->userdata('user_id')){
    		$brand_cond = '';
    		$brand_cond = $brand_cond." and a.brand_id IN(".$this->get_paid_brands().")";
    		if($brand!='' && $cat!='' && $attributes_chosen =='' && $category_specific_attr == ''){
				
    			if($brand >0){
    				$brand_cond = "and a.brand_id = ".$brand;
    			}
				if($popup_tags != ''){
    				$where_cond = "and e.tag_id IN(".$popup_tags.") ";
    			}
				/*$query = $this->db->query("select distinct a.id as product_id,a.name,a.image,a.product_used_count,a.url,a.price,trim(c.company_name) brand_name from product_desc as a,category_object_relationship as b,brand_info as c,product_tag as e where a.id = b.object_id and b.object_type='product' and a.`id`>='3598' ".$brand_cond." and b.category_id = ".$cat." ".$where_cond ." and a.approve_reject = 'A' AND a.id = a.parent_id AND a.brand_id = c.user_id AND e.product_id = a.id AND a.stock_count > 0 order by a.id desc limit ".$lookoffset.",".LOOK_CREATOR_PAGE_OFFSET);*/

				$query = $this->db->query("select distinct a.id as product_id,a.name,a.image,a.product_used_count,a.url,a.price,trim(c.company_name) brand_name,a.brand_id,g.brand_store_name from product_desc as a,category_object_relationship as b,brand_info as c,product_tag as e,product_store as f,brand_store as g where a.id = b.object_id and b.object_type='product' and a.`id`>='3598' ".$brand_cond." and b.category_id = ".$cat." ".$where_cond ." AND f.product_id = a.id AND g.id = f.store_id  AND a.brand_id = c.user_id AND e.product_id = a.id AND a.stock_count > 0  order by a.id desc limit ".$lookoffset.",".LOOK_CREATOR_PAGE_OFFSET);
				
    		}else{
    			$brand_cond = '';
    			if($attributes_chosen!=''){
    				$where_cond = $where_cond . "and c.attribute_id IN (".$attributes_chosen.")";
    			}

    			if($category_specific_attr!=''){
    				$where_cond = $where_cond . "and c.attribute_id =".$category_specific_attr."";
    			}
    			if($brand >0){
    				$brand_cond = "and a.brand_id = ".$brand;
    			}
				if($popup_tags != ''){
    				$where_cond = "and e.tag_id IN(".$popup_tags.") ";
    			}
				/*$query = $this->db->query("select distinct a.id as product_id,a.name,a.image,a.product_used_count,a.url,a.price,trim(d.company_name) brand_name from product_desc as a,category_object_relationship as b,attribute_object_relationship as c,brand_info as d,product_tag as e where a.id = b.object_id and a.id = c.object_id and b.object_type='product' and a.`id`>='3598' ".$brand_cond." and b.category_id = ".$cat." ".$where_cond ." and a.approve_reject = 'A' AND a.id = a.parent_id AND a.brand_id = d.user_id AND e.product_id = a.id AND a.stock_count > 0  order by a.id desc limit ".$lookoffset.",".LOOK_CREATOR_PAGE_OFFSET);*/
				$query = $this->db->query("select distinct a.id as product_id,a.name,a.image,a.product_used_count,a.url,a.price,trim(d.company_name) brand_name,a.brand_id,g.brand_store_name from product_desc as a,category_object_relationship as b,attribute_object_relationship as c,brand_info as d,product_tag as e,product_store as f,brand_store as g where a.id = b.object_id and a.id = c.object_id and b.object_type='product' and a.`id`>='3598' ".$brand_cond." and b.category_id = ".$cat." ".$where_cond." AND f.product_id = a.id AND g.id = f.store_id  AND a.brand_id = d.user_id AND e.product_id = a.id AND  AND a.stock_count > 0 order by a.id desc limit ".$lookoffset.",".LOOK_CREATOR_PAGE_OFFSET);
    		}
			// echo $this->db->last_query();exit;	
			
    		$result = $query->result_array();
			$product_array = array();$i=0;$imagify_brandid = unserialize(IMAGIFY_BRANDID); 
			foreach($result as $val){
				$product_array[$i]['product_id'] = $val['product_id'];
				$product_array[$i]['name'] = $val['name'];
				$product_array[$i]['product_used_count'] = $val['product_used_count'];
				$product_array[$i]['url'] = $val['url'];
				$product_array[$i]['price'] = $val['price'];
				$product_array[$i]['brand_name'] = $val['brand_name'];
				$product_array[$i]['brand_store_name'] = $val['brand_store_name'];
				
				// added for new product url  
				$productUrl = '';
				if(in_array($val['brand_id'],$imagify_brandid)){
					$productUrl = $this->config->item('products_tiny').$val['image'];
				}else{
					$productUrl = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_124/'.$val['image'];                
				}
				// added for new product url
				$product_array[$i]['image'] = $productUrl;
				$i++;
			}
			// echo "<pre>";print_r($product_array);
			return $product_array;
    	}
	}
	
	function get_paid_brands(){
 		$brands = $this->get_brands_list(); $brand_html = '';
 		if(!empty($brands)){ $i = 0; 
 			foreach ($brands as $data) {
 				if($i!=0) { $brand_html = $brand_html.','; }
 				$brand_html = $brand_html.$data['id'];
 				$i++;
 			}
 		}
 		return $brand_html;
 	}
	
	function get_collection($object_id){
		$product_array = array();
		$query = $this->db->query("select a.object_id as id,a.object_name as name,a.object_image as image,a.object_created as created_datetime,a.object_modified as modified_datetime,b.object_meta_key,b.object_meta_value  from object as a,object_meta as b where a.object_id = b.object_id AND a.object_type = 'collection' AND b.object_status != 0 AND b.object_id = ".$object_id." order by a.object_id desc ");
		$result = $query->result_array();
		// echo "<pre>";print_r($result);
		$product_array['collection_id'] = @$result[0]['id'];
		$product_array['collection_name'] = @$result[0]['name'];
		foreach($result as $val){
			$product_array[$val['object_meta_key']] = $val['object_meta_value'];
			if($val['object_meta_key'] == 'collection_products'){
				$products = unserialize($val['object_meta_value']);
				$product_array[$val['object_meta_key']] = $this->get_collection_products($products);
			} 
		}
		return $product_array;
		
	}
	
	function get_collection_products($product_ids){
		$product_ids = implode(",",$product_ids);
		$query = $this->db->query("select distinct a.id as product_id,a.name,a.image,a.url,a.price from product_desc as a where a.`id`>='3598' and a.approve_reject = 'A' AND a.id = a.parent_id AND a.id IN(".$product_ids.") order by a.id desc ");
		$result = $query->result_array();
		// echo "<pre>";print_r($result);
		return $result;
	}
	
	function edit_collection($object_data,$object_meta_data,$object_id){
		
		if($object_meta_data['collection_img'] != ''){
			
			$data = array('object_name'=>$object_data['collection_name'],'object_description'=>'collection_name','object_type'=>'collection','object_created'=>date('Y-m-d H:i:s'),'object_creator'=> $this->session->userdata('user_id'),'object_modifier'=> $this->session->userdata('user_id'),'object_image'=>$object_meta_data['collection_img']);
		}else{
			
			$data = array('object_name'=>$object_data['collection_name'],'object_description'=>'collection_name','object_type'=>'collection','object_created'=>date('Y-m-d H:i:s'),'object_creator'=> $this->session->userdata('user_id'),'object_modifier'=> $this->session->userdata('user_id'));
		}
		$this->db->update('object', $data, array('object_id' => $object_id ));
		// echo $this->db->last_query();exit;
			$object_meta_keys = array_keys($object_meta_data);$i=0;
			// print_r($object_meta_keys);exit;
			foreach($object_meta_data as $val){
				
				$data = array('object_meta_key'=>$object_meta_keys[$i],'object_meta_value'=>$val,'created_datetime'=>date('Y-m-d H:i:s'),'created_by'=> $this->session->userdata('user_id'),'modified_by'=> $this->session->userdata('user_id'));
				
				$this->db->update('object_meta', $data, array('object_id' => $object_id	,'object_meta_key'=>$object_meta_keys[$i]));
				// echo $this->db->last_query();
				$i++;
			}
			return true;
	}

	function getbrandProducts($brandcstr)
	{
		$brand_cond = " and a.brand_id IN(".$brandcstr.")";
		/*$query = $this->db->query("select distinct a.id as product_id,a.name,a.image,a.product_used_count,a.url,a.price,trim(d.company_name) brand_name from product_desc as a,category_object_relationship as b,attribute_object_relationship as c,brand_info as d,product_tag as e where a.id = b.object_id and a.id = c.object_id and b.object_type='product' and a.`id`>='3598' ".$brand_cond."  and a.approve_reject = 'A' AND a.id = a.parent_id AND a.brand_id = d.user_id AND e.product_id = a.id AND a.stock_count > 0  order by a.id desc ");*/
		$query = $this->db->query("select distinct a.id as product_id from product_desc as a where  a.`id`>='3598' ".$brand_cond."  and a.status = '1' and is_delete=0 order by a.id desc ");
		echo $this->db->last_query();
		return $result = $query->result_array();
	}
	
	function get_tags($popup_tags){
		$tags = explode(' ',$popup_tags);
		$tag_name = implode('\',\'',$tags);
		$query = $this->db->query('SELECT GROUP_CONCAT(DISTINCT a.id) as tag_ids FROM `tag` as a WHERE a.name in(\''.$tag_name.'\') ');
		$res = $query->result_array();
		if(!empty($res)){
			return $res[0]['tag_ids'];
		}else return null;
	}
	
	public function product_inventory($pid='')
	{
		$this->db->select('pi.product_sku,pi.stock_count, ps.size_text');
		$this->db->from('product_inventory as pi');
		$this->db->join('product_size as ps', 'ps.id = pi.size_id', 'inner');
		$this->db->where('product_id', $pid);
		$query = $this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}

	public function get_product_attributes($product_id)
	{
		$this->db->select('aor.attribute_id,a.attr_value_name,a.attribute_id as attribute_parent,a.attr_value_slug,b.attribute_name as attribute_parent_name');
		$this->db->from('attribute_object_relationship as aor');
		$this->db->join('attribute_values_relationship as a', 'a.id = ABS(aor.attribute_id)','LEFT');
		$this->db->join('attributes as b', 'b.id = a.attribute_id','INNER');
		$this->db->where('aor.object_type','product');		
		$this->db->where('aor.object_id',$product_id);
		$query = $this->db->get();		
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}

	function get_product_sku($product_id)
	{
		if($product_id!='')
		{
			$this->db->select('pd.sku_number');
			$this->db->from('product_desc as pd');		
			$this->db->where('pd.id',$product_id);	
			$this->db->where('pd.is_delete','0');			
			$query = $this->db->get();		
			if($query->num_rows()>0)
			{
				$result = $query->result_array();
				return $result[0]['sku_number'];				
			}
		}		
		return false;
	}

	function product_child_sku($product_id,$parent_sku)
	{
		if($product_id!='' && $parent_sku!='')
		{
			$this->db->select('pd.id,pd.sku_number');
			$this->db->from('product_desc as pd');		
			//$this->db->where('pd.parent_id',$parent_sku);
			$this->db->where("(pd.parent_id LIKE '%".$parent_sku."%' OR pd.sku_number LIKE '%".$parent_sku."%' )", NULL, FALSE);	
			$this->db->where('pd.is_delete','0');
			$this->db->where('pd.stock_count >','0');			
			$query = $this->db->get();		
			if($query->num_rows()>0)
			{
				return $query->result_array();
			}
		}		
		return false;
	}

	function get_order_category($cartid)
	{
		$this->db->select('cor.category_id,c.category_name,c.category_parent,c.category_slug');
		$this->db->from('category_object_relationship as cor');
		$this->db->join('category as c', 'c.id = ABS(cor.category_id)','LEFT');
		$this->db->where('cor.object_type','order');		
		$this->db->where('cor.object_id',$cartid);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}

	function get_order_attributes($cartid)
	{
		$this->db->select('aor.attribute_id,a.attr_value_name,a.attribute_id as attribute_parent,a.attr_value_slug');
		$this->db->from('attribute_object_relationship as aor');
		$this->db->join('attribute_values_relationship as a', 'a.id = ABS(aor.attribute_id)','LEFT');
		$this->db->where('aor.object_type','order');		
		$this->db->where('aor.object_id',$cartid);
		$query = $this->db->get();		
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return false;
	}

	 function get_product_category_level2($productid){

	 	$this->db->select('c.id,c.category_name,c.category_slug,c.level');
	 	$this->db->from('category as c');
	 	$this->db->join('category_object_relationship as cor', 'c.id = cor.category_id');
	 	$this->db->where('c.status="1" AND c.level=2 ');
	 	$this->db->where('cor.object_type','product');
	 	$this->db->where('cor.object_id',$productid);
	 	$this->db->order_by('cor.category_id','ASC');
	 	$query = $this->db->get();		
		if($query->num_rows()>0)
		{
			//echo $this->db->last_query();exit;
			//echo '<pre>';print_r($query->result_array());exit;
			return $query->result_array();
		}
		return false;
	}
	
}