<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tag_model extends MY_Model {
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	public function new_tag($data)
	{	
		$this->db->insert('tag', $data);
		$c_id = $this->db->insert_id();		
		return $c_id;			
	}

	/*check Multilpe tag*/
	public function checkMultilpetag($name)
	{
		$catQuery = "select count(name) as catCount from tag where name LIKE '".$name."' ";
		$countRes = $this->db->query($catQuery);		
		$rowRes = $countRes->row();	
		return $rowRes;
	}
	
	
	/*Get All Data*/
	public function get_all_tag($search_by=NULL,$table_search=NULL,$page=NULL,$per_page=NULL){
		
		if($search_by == '1'){ $search_cond = " AND name like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '2'){ $search_cond = " AND slug like '%".strip_tags(trim($table_search))."%'"; }
		else if($search_by == '3'){ 
				$tb_status=strip_tags(trim($table_search));
				if($tb_status=="Yes")
				{
					$status=1;

				}else
				{
					$status=0;
				}
			$search_cond = " AND status =".$status." "; }
		else if($search_by == '4'){ $search_cond = " AND id IN (".$table_search.") "; }
		else{ $search_cond ='AND 1=1'; }		 
		if($page!='' || $per_page!=''){ if($page != 1 && $page !=0){
                   $page = $page*$per_page-$per_page;
                }else{ $page = 0;}
				$limit_cond = "LIMIT ".$page.",".$per_page;
			}else{
				$limit_cond = '';	
			}		
		$query = $this->db->query("select id, name, slug, status, is_delete, created_datetime from tag where is_delete=0 ".$search_cond." order by id desc ".$limit_cond);				
		$res = $query->result();
		return $res;
	}
	
	/*Get Id form datatable*/
	public function get_tag_by_ID($catID){		
		$this->db->select('*');
		$this->db->from('tag');
		$this->db->where('id', $catID);
				
		$category = $this->db->get();		
		
		if($category){			
			
			return $category->row();			
			
		}
	}
	
	/*This function checks for same tag while editing tag*/
	public function checkMultilpetagEdit($catId,$name)
	{	
		$catQuery = "select count(name) as catCount from tag where id!=".$catId." and name LIKE '".$name."' ";
		$countRes = $this->db->query($catQuery);		
		$rowRes = $countRes->row();
		return $rowRes;
	}	
	
	/*This function update tag*/
	public function update_tag($catID,$data){
		
		$this->db->where('id', $catID);
 		$updated = $this->db->update('tag' ,$data);
		
		if($updated){
			
			return TRUE;
			
		}
		else
		{
			
			return FALSE;	
		}
	}
	
	/*delete Code Here tag*/
	public function delete_tag($catID){
		
		$this->db->where('id', $catID);
 		$deleted = $this->db->update('tag' ,array('is_delete'=>'1'));
		
		if($deleted){

			return TRUE;			
		}
		else
		{			
			return FALSE;	
		}
	}
	
	/* get get_tag_type */
	public function get_tag_type()
	{	
		$this->db->select('*');
		$this->db->from('tag_type');				
		$tag_type = $this->db->get();		
		
		if($tag_type){			
			
			return $tag_type->result();		
			
		}
	}
	
		
/*---------End of Common functionalities-------*/  
}

