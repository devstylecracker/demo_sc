<?php
class Discountsetting_model extends MY_Model {    
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    function get_discount_type()
    {
        if($this->session->userdata('role_id') == 6){
            $brand_id = $this->session->userdata('user_id');
            $query = $this->db->query("SELECT id as discount_id, discount_name FROM discount_type  WHERE status = 1  ORDER BY id DESC");
        }else{
            $query = $this->db->query("SELECT id as discount_id, discount_name FROM discount_type  WHERE status = 1 ORDER BY id DESC");
        }
        $result = $query->result_array();
        return $result;
    }

    function get_products($store_id,$product_id,$min_offset,$max_offset){
    	 if($min_offset > 0){ $min_offset = $min_offset*50; }
        if($store_id!='' && $product_id!=''){
            $cond = "pd.`brand_id`= '".$store_id."' and pd.id='".$product_id."' and pd.is_delete=0 ";
            $limit_cond='';
        }else if($store_id!='' && $product_id==''){
            $cond = "pd.`brand_id`= '".$store_id."' and pd.is_delete=0 ";
            $limit_cond =" limit ".$min_offset.",".$max_offset;
        }else if($store_id=='' && $product_id!=''){
            $cond = "pd.id='".$product_id."' and pd.is_delete=0 ";
            $limit_cond='';
        }else{
            $cond = ' pd.is_delete=0';
            $limit_cond =" limit ".$min_offset.",".$max_offset;
        }
       
        $query = $this->db->query("SELECT pi.`id`, pd.`id` as product_id, GROUP_CONCAT(pi.`size_id`) as size_id , GROUP_CONCAT(ps.`size_text`) as size_text,  GROUP_CONCAT(pi.`stock_count`) as stock_count , pd.`name`,pd.`brand_id` FROM product_desc as pd LEFT OUTER JOIN product_inventory as pi  ON pd.`id` = pi.`product_id` LEFT OUTER Join `product_size` as ps ON ps.`id` = pi.`size_id` WHERE ".$cond." group by pd.`id`, pd.`brand_id` order by pd.`id` desc".$limit_cond );  
        $res = $query->result_array();
        return $res; 
    }
    
    function get_brandsetting_details()
    {

    }

	function get_brand_discount_ontotal($store_id,$discountType)
  {
		$query = $this->db->query("select date(start_date) as date_from,date(end_date) as date_to,discount_percent,max_amount from discount_availed where discount_type_id = $discountType AND brand_id = $store_id ");  
        $res = $query->result_array();       	
        if(!empty($res)) return $res[0]; else return null;
	}

	function save_brand_discount_ontotal($brand_id,$date_from,$date_to,$max_amount,$discount_percent,$discountType)
    {	
    		$query = $this->db->query("select * from discount_availed where (`brand_id` = '".$brand_id."' AND `discount_type_id` = '".$discountType."')");
            $res = $query->result_array(); 
            if(empty($res)){
    		$data = array(
    		   'discount_type_id' => $discountType ,
    		   'brand_id' => $brand_id ,
    		   'start_date' => $date_from,
    		   'end_date' => $date_to,
    		   'max_amount' =>$max_amount,
    		   'discount_percent'=>$discount_percent,
    		   'created_datetime' => date('Y-m-d H:i:s'),
    		);
    		$result = $this->db->insert('discount_availed', $data);
		}else{
    			$data = array(
    		   'discount_type_id' => $discountType ,
    		   'brand_id' => $brand_id ,
    		   'start_date' => $date_from,
    		   'end_date' => $date_to,
    		   'max_amount' =>$max_amount,
    		   'discount_percent'=>$discount_percent,
    		   'created_datetime' => date('Y-m-d H:i:s'),
    		);
    		$this->db->where(array('brand_id'=>$brand_id,'discount_type_id' => $discountType));
            $result = $this->db->update('discount_availed', $data); 
    		}
    		if($result){
    			return 1;
		}else return 2;
    }
	
	function get_product_discount($store_id,$discountType,$product_id){
		$query = $this->db->query("select start_date as date_from,end_date as date_to,discount_percent,max_amount from discount_availed where discount_type_id = $discountType AND product_id = $product_id ");  
        $res = $query->result_array();       	
        if(!empty($res)) return $res[0]; else return null;
	}
	
	function save_product_discount($brand_id,$date_from,$date_to,$product_id,$discount_percent,$discountType)
  {
  		$query = $this->db->query("select * from discount_availed where ( `brand_id`=0 AND `discount_type_id`=2  AND product_id =  '".$product_id."') OR ( `brand_id`='".$brand_id."' AND `discount_type_id`=1  AND product_id = 0 )");
     /* echo $this->db->last_query();exit;*/
      $res = $query->result_array(); 
      if(empty($res))
      {
      		$data = array(
      		   'discount_type_id' => $discountType ,
      		   'brand_id' => 0 ,
      		   'start_date' => $date_from,
      		   'end_date' => $date_to,
      		   'product_id' =>$product_id,
      		   'discount_percent'=>$discount_percent,
             'created_by' => $this->session->userdata('user_id'),
      		   'created_datetime' => date('Y-m-d H:i:s'),
             'status' => 1
      		);
  		  $result = $this->db->insert('discount_availed', $data);
  		}else{
      			$data = array(
      		   'discount_type_id' => $discountType ,
      		   'brand_id' => 0 ,
      		   'start_date' => $date_from,
      		   'end_date' => $date_to,
      		   'product_id' =>$product_id,
      		   'discount_percent'=>$discount_percent,
      		   'modified_by' => $this->session->userdata('user_id'),
             'modified_datetime' => date('Y-m-d H:i:s'),              
             'status' => 1
      		);
  		    $this->db->where(array('product_id'=>$product_id,'discount_type_id' => $discountType));
          $result = $this->db->update('discount_availed', $data); 
  		}
  		if($result){
  			return 1;
  		}else return 2;
  }

    function save_stylecracker_discount($brand_id,$date_from,$date_to,$max_amount,$discount_percent,$discountType)
    {  
       $count=count($brand_id); 
       $query = $this->db->query("select date(start_date) as date_from,date(end_date) as date_to,discount_percent,max_amount from discount_availed where discount_type_id = $discountType AND date(end_date) > date('".$date_from."') AND brand_id = 0 AND product_id = 0");               
        $res = $query->result_array();         
         if(empty($res)){ 
        /*foreach($brand_id as $val){

            $query = $this->db->query("select date(start_date) as date_from,date(end_date) as date_to,discount_percent,max_amount from discount_availed where discount_type_id = $discountType AND date(end_date) > date('".$date_from."') AND brand_id ='".$val."'"); */
           /*  echo $this ->db->last_query();exit;*/
                  /*$res = $query->result_array();    
                  if(empty($res)){*/
                      $data = array(
                         'discount_type_id' => $discountType ,
                         'brand_id' => 0,
                         'product_id' => 0,
                         'start_date' => $date_from,
                         'end_date' => $date_to,
                         'max_amount' =>$max_amount,
                         'discount_percent'=>$discount_percent,
                         'created_by' => $this->session->userdata('user_id'),
                         'created_datetime' => date('Y-m-d H:i:s'),
                         'status' => 1
                      );
                  $result = $this->db->insert('discount_availed', $data);

                /*}else
                {
                  return 3;
                }
              }*/
        }else{
        /*for($i=0;$i<=$count;$i++){*/
            $data = array(
               'discount_type_id' => $discountType ,
               'brand_id' => 0,
               'product_id' => 0,
               'start_date' =>$date_from,
               'end_date' => $date_to,
               'max_amount' =>$max_amount,
               'discount_percent'=>$discount_percent, 
               'modified_by' => $this->session->userdata('user_id'),              
               'status' => 1
            );
	        $this->db->where(array('brand_id'=>0,'product_id'=>0,'discount_type_id' => $discountType));
	        $result = $this->db->update('discount_availed', $data);
        //} 
        }
    
        if($result){
            return 1;
        }else return 2;
    }

    function save_brand_discount($brand_id,$date_from,$date_to,$discount_percent,$discountType)
    {          
         if(empty($res)){ 

            $query = $this->db->query("select id from discount_availed where discount_type_id = $discountType AND date(end_date) > date('".$date_from."') AND brand_id ='".$brand_id."' AND  discount_type_id = '".$discountType."'"); 
             /*echo $this ->db->last_query();exit;*/
                  $res1 = $query->result_array();    
                  if(empty($res1)){
                      $data = array(
                         'discount_type_id' => $discountType ,
                         'brand_id' => $brand_id,
                         'start_date' => $date_from,
                         'end_date' => $date_to,                        
                         'discount_percent'=>$discount_percent,
                         'created_by' => $this->session->userdata('user_id'),
                         'created_datetime' => date('Y-m-d H:i:s'),
                         'status' => 1
                      );
                  $result = $this->db->insert('discount_availed', $data);
                   return 1;

                }else
                {
                  return 3;
                }
              
        }else{	        
	            $data = array(
	               'discount_type_id' => $discountType ,
	               'brand_id' => $brand_id,
	               'start_date' =>$date_from,
	               'end_date' => $date_to,             
	               'discount_percent'=>$discount_percent, 
	               'modified_by' => $this->session->userdata('user_id'),              
	                'status' => 1
	            );
		        $this->db->where(array('brand_id'=>$brand_id,'discount_type_id' => $discountType));
		        $result1 = $this->db->update('discount_availed', $data);
	       
        }
    
        if($result){
            return 1;
        }else return 2;
    }

   function getDiscounts($page=Null,$per_page=Null,$discount_type=Null,$min_offset=null,$max_offset=null)
   {   
        if($min_offset > 0){ $min_offset = $min_offset*50; }
        if($min_offset ==0){
            $limit_cond='limit 0,50';
        }else if($min_offset>=0){
            $limit_cond =" limit ".$min_offset.",".$max_offset;
        }else{
            $limit_cond =" limit ".$min_offset.",".$max_offset;
        }
    
      if($discount_type!=2 && $discount_type!=4)
      {
        $query = $this->db->query("SELECT (select name from product_desc where a.`product_id`=product_desc.`id` ) as product_name, a.`id`, a.`discount_type_id`, a.`brand_id`, a.`product_id`, a.`discount_percent`, a.`max_amount`, a.`discount_price`, a.`start_date`, a.`end_date`,b.`user_id`,b. `company_name`,c.`discount_name`,c.`id` FROM `discount_availed` as a,brand_info as b,discount_type as c WHERE a.`brand_id`=b.`user_id` AND c.`id`=a.`discount_type_id`  AND a.`discount_type_id`= '".$discount_type."' ".$limit_cond);

      }else if($discount_type==2)
      {
        $query = $this->db->query("SELECT (select name from product_desc where a.`product_id`=product_desc.`id` ) as product_name, a.`id`, a.`discount_type_id`, a.`brand_id`, a.`product_id`, a.`discount_percent`, a.`max_amount`, a.`discount_price`, a.`start_date`, a.`end_date`,(select bi.`company_name` from brand_info as bi, product_desc as pd WHERE pd.`brand_id`=bi.`user_id` AND a.`product_id` = pd.`id` ) as `company_name`,c.`discount_name`,c.`id` FROM `discount_availed` as a,discount_type as c WHERE a.`brand_id`=0 AND c.`id`=a.`discount_type_id`  AND a.`discount_type_id`= '".$discount_type."' ".$limit_cond);

      }else if($discount_type==4)    
      {   
        $query = $this->db->query("SELECT a.`id`, a.`discount_type_id`, a.`brand_id`, a.`product_id`, a.`discount_percent`, a.`max_amount`, a.`discount_price`, a.`start_date`, a.`end_date`,c.`discount_name`,c.`id`,'' as company_name, '' as product_name FROM `discount_availed` as a,discount_type as c WHERE a.`brand_id`=0 AND a.`product_id`=0 AND c.`id`=a.`discount_type_id`  AND a.`discount_type_id`= '".$discount_type."' ".$limit_cond);
      }
                
          $result = $query->result_array();
         // echo $this->db->last_query();
         //print_r(expression)
          return $result;
    }
 } 
?>
