<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class View_orders_payment extends MY_Controller {

	function __construct(){
       parent::__construct();
       $this->load->model('View_orders_model');
   	}

	public function index($param=NULL)
	{

		if(!$this->session->userdata('user_id'))
		{
			$this->load->view('login');

		}else
		{
			 $data_post = array();

			 if($this->has_rights(63) == 1)
			{

				$search_by = 0;
				$table_search = 0;

				$data_post['orderStatusList'] = array('1'=>'Processing', '2'=>'Confirmed', '3'=>'Dispatched', '4'=>'Delivered','5'=>'Cancelled','6'=>'Return');
				
				$data_post['orderPayStatusList'] = array('3'=>'Pending','1'=>'Success', '2'=>'Cleared');
				//echo "<pre>"; print_r($this->uri); 
            	$search_by = $this->input->post('search_by')!='' ? $this->input->post('search_by') : $this->uri->segment(3);
                $table_search = $this->input->post('table_search')!='' ? $this->input->post('table_search') : $this->uri->segment(4);
                
                if($search_by!='0' && $table_search!='0' && $search_by!='' && $table_search!=''){ 
                	$paginationUrl = 'view_orders_payment/index/'.$search_by.'/'.$table_search;
                	$uri_segment = 5;
            	}else{
            		$paginationUrl = 'view_orders_payment/index/0/0';
            		$uri_segment = 5;
            	}
            	
               	$config['per_page'] = 20;
                $offset = $this->uri->segment(5)!='' ? $this->uri->segment(5) : 0;
                $config['total_rows'] = count($this->View_orders_model->getOrdersPay($search_by,$table_search,'',''));
                $total_rows = $config['total_rows'];
                $get_data = $this->View_orders_model->getOrdersPay($search_by,$table_search,$offset,$config['per_page']);              

                $count = $config['total_rows'];
                $data_post['orders'] = $get_data;
                $data_post['search_by'] = $search_by;
                $data_post['table_search'] = $table_search;
                $data_post['sr_offset'] = $offset;
               // $this->customPagination($paginationUrl,$count,$uri_segment,$get_data,$total_rows);

				$config['base_url'] = base_url().$paginationUrl;		
				$config['total_rows'] = $count;
				
				$config['uri_segment'] = $uri_segment;
				$config['use_page_numbers'] = TRUE;
				$config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
				$config['full_tag_close'] = '</ul></div>';

				$config['first_link'] = '&laquo; First';
				$config['first_tag_open'] = '<li>';
				$config['first_tag_close'] = '</li>';

				$config['last_link'] = 'Last &raquo;';
				$config['last_tag_open'] = '<li>';
				$config['last_tag_close'] = '</li>';

				$config['next_link'] = 'Next &rarr;';
				$config['next_tag_open'] = '<li>';
				$config['next_tag_close'] = '</li>';

				$config['prev_link'] = '&larr; Previous';
				$config['prev_tag_open'] = '<li>';
				$config['prev_tag_close'] = '</li>';

				$config['cur_tag_open'] = '<li class="active" ><a href="" >';
				$config['cur_tag_close'] = '</a></li>';

				$config['num_tag_open'] = '<li>';
				$config['num_tag_close'] = '</li>';
				
				$this->pagination->initialize($config);	
				$data_post['total_rows'] = $total_rows;


				$this->load->view('common/header');
				$this->load->view('orders/view_orders_payment',$data_post);
				$this->load->view('common/footer');
			}else
			{
				$this->load->view('common/header');
                $this->load->view('not_permission');
                $this->load->view('common/footer');

			}
		}
	}	
	
	public function updatepayOrder()
	{
		if(!$this->input->is_ajax_request())
		{
			exit("No direct script access allowed");
		}else
		{
			$orderPrdId = $this->input->post('id');
			$orderpayStatus = $this->input->post('orderpay_Status');
			$result = $this->View_orders_model->updatepayOrder($orderPrdId,$orderpayStatus);

			if($result)
			{
				$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Product Order Status Updated Successfully</div>');				
				return true;
			}else
			{
				$this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Error!</strong> </h5>Error</div>');				
				return false;
			}
		}
		

	}

	public function orderstatus($orderStatus)
	{	//echo $orderStatus;exit;
		$data['status'] = $orderStatus;
		$data['result'] = $this->View_orders_model->order_status($orderStatus);
		$this->load->view('common/header');
		$this->load->view('orders/order_status_view',$data);
		$this->load->view('common/footer');
	}

	/*public function exportExcel($param,$search_by=NULL,$table_search=NULL,$is_download=NULL)
	{	$is_download = $this->uri->segment(6); 

		if($search_by == 6 )
		{
			$table_search_encode = base64_decode($table_search);
		}else if($search_by == 8)
		{
			$table_search_encode = base64_decode($table_search);
		}else
		{
			$table_search_encode = $table_search;
		}		
		//$is_download = $this->input->post('param');
        $fieldName = array('Date','Order ID','Product Name','Brand Name'
                            ,'Payment Type','Unique Reference Number','Transaction Id','Quantity'
                            ,'Product Amount','Brand Tax Amount','Shipping Charges','COD Charges'
                            ,'Grand Total','Rate Of Commission(%)','Commission Amount','Service Tax Amount(14.0%)','Swach Bharat Abhiyan(0.5%)'
                            ,'Total Commission Invoice','PG cost @3.3%'
                            ,'PG cost Service Tax Amount(14.0%)','PG cost Swach Bharat Abhiyan(0.5%)','Total PG cost @3.3%','TDS deduction by Brand on commission'
                            ,'Net Income receivable from Brand','Brand Income','Days Lapsed'
                            ,'Payment Status','Username','Phone Number','Email'
                            ,'Shipping Address','City','State','Pincode');//Brand Payout 
		$get_data = $this->View_orders_model->getOrdersPay($search_by,$table_search_encode);
		
		$result_data = array();
		$i =0;
        $old_order_id=-1;
		foreach($get_data as $val) {   
            
            $result_data[$i][] = $val['created_datetime'];            
            $result_data[$i][] = $val['order_id'];
            $result_data[$i][] = $val['product_name'];
            $result_data[$i][] = $val['company_name'];
            $brand_code ='';
            $product_price = 0;
            $commission_rate = 0;
            if($val['pay_mode']==1)
            {
            	$result_data[$i][] = 'COD';
            }else
            {
            	$result_data[$i][] = 'Online';
            }
            $date_ref_no = explode('-',$val['created_datetime']);
            
            if($val['brand_code']!='')
            {
            	$brand_code = $val['brand_code'];
            }

            $result_data[$i][] = $brand_code.'/'.$date_ref_no[1].'/'.$date_ref_no[0].'/'.$val['order_id'];
            if(isset($val['tranid']))
            {
            	$result_data[$i][] = $val['tranid'];
            }
            
            if(isset($val['product_qty']))
            {
            	$result_data[$i][] = $val['product_qty'];
            }
           
            $result_data[$i][] = $val['product_price'];
            
            if($old_order_id != $val['order_id']){
                $result_data[$i][] = $val['order_tax_amount'];
                $result_data[$i][] = $val['shipping_amount'];
                $result_data[$i][] = $val['cod_amount'];
            }else{
                $result_data[$i][] = 0;
                $result_data[$i][] = 0;
                $result_data[$i][] = 0;
            }
            $old_order_id = $val['order_id'];
            
            $result_data[$i][] = $val['order_total'];
            $result_data[$i][] = $val['commission_rate'];
            if(@$val['product_price']!='')
            {
            	$product_price = $val['product_price'];
            }

            if(@$val['commission_rate']!='')
            {
            	$commission_rate = $val['commission_rate'];
            }
            $comm_amt =  $product_price*($commission_rate/100);
            @$comm_amt_tax =  $comm_amt*(14/100);
            @$comm_amt_sb_tax =  $comm_amt*(0.5/100);
            $result_data[$i][] = $comm_amt;
            $result_data[$i][] = $comm_amt_tax;
            $result_data[$i][] = $comm_amt_sb_tax;
            
            @$total_comm_invoice = $comm_amt+$comm_amt_tax+$comm_amt_sb_tax;
            $result_data[$i][] = round($total_comm_invoice,2);

            $pgCost=0;
            if($val['pay_mode']!=1)
            {
            	$pgCost = $val['order_total'] * (3.3/100);
            }
           
            $result_data[$i][] = round($pgCost,2);

            $service_tax = $comm_amt+$pgCost*(14.00/100);
            $result_data[$i][] = round($service_tax,2);

            $swachba = ($comm_amt+$pgCost)*(0.50/100);
            $result_data[$i][] = round($swachba,2);
            
            $total_pg_cost = $swachba+$service_tax+$pgCost;
            $result_data[$i][] = round($total_pg_cost,2);


            $tds_deduction = ($comm_amt+$pgCost)*(10/100);
            $result_data[$i][] =  round($tds_deduction,2);

            $netIncome_rec_brand =  $total_comm_invoice-$tds_deduction;
            $result_data[$i][] = round($netIncome_rec_brand,2);

            $brand_income = $val['order_total']-$total_comm_invoice;
            $result_data[$i][] = round($brand_income,2);

            //$brand_payout =  $tds_deduction+$brand_income;
            //$result_data[$i][] = round($brand_payout,2);

            $result_data[$i][] = $val['daysLapsed'];
            if($val['payment_status'] == 1)
            {
            	$pay_status = 'Success';
            }else if($val['payment_status'] == 2)
            {
            	$pay_status = 'Cleared';
            }else
            {
            	$pay_status = 'Pending';
            }
            $result_data[$i][] = $pay_status;
            $result_data[$i][] = $val['first_name']." ".$val['last_name'];
            $result_data[$i][] = $val['mobile_no'];
            $result_data[$i][] = $val['email_id'];
            $result_data[$i][] = trim($val['shipping_address']);
            $result_data[$i][] = trim($val['city_name']);
            $result_data[$i][] = trim($val['state_name']);
            $result_data[$i][] = $val['pincode'];
            
            $i++;
        }
        //echo "<pre>";print_r($result_data);exit;
       // echo $param.' '.$is_download;
		if($param == 'downloadExcel' && $is_download == 1)
        {       	
    	  //========================Excel Download==============================
	        //$excel_result = $this->writeDataintoCSV($fieldName,$get_data,'view_order_payment');
	       $this->writeDataintoCSV($fieldName,$result_data,'view_order_payment');
	       //========================End Excel Download==========================

           
        }
              
	}*/
public function exportExcel($param,$search_by=NULL,$table_search=NULL,$is_download=NULL)
    {   $is_download = $this->uri->segment(6); 

        if($search_by == 6 )
        {
            $table_search_encode = urldecode(base64_decode($table_search));
        }else if($search_by == 8)
        {
            $table_search_encode = urldecode(base64_decode($table_search));
        }else
        {
            $table_search_encode = urldecode($table_search);
        }       
        //$is_download = $this->input->post('param');
        $fieldName = array('Date','Order ID','Product Name','Brand Name'
                            ,'Payment Type','Unique Reference Number','Transaction Id','Quantity'
                            ,'Product Amount','Brand Tax Amount','Shipping Charges','COD Charges'
                            ,'Grand Total','Rate Of Commission(%)','Commission Amount','Service Tax Amount(14.0%)','Swach Bharat Abhiyan(0.5%)'
                            ,'Total Commission Invoice','PG cost @3.3%'
                            ,'PG cost Service Tax Amount(14.0%)','PG cost Swach Bharat Abhiyan(0.5%)','Total PG cost @3.3%','TDS deduction by Brand on commission'
                            ,'Net Income receivable from Brand','Brand Income','Days Lapsed'
                            ,'Payment Status','Username','Phone Number','Email'
                            ,'Shipping Address','City','State','Pincode','Discount', 'Coupon Code', 'Coupon Amount', 'Unique No', 'Order Status'
                            , 'Referral Amount Used','Commission KKC(0.5%)','PG cost KKC(0.5%)','MRP');//Brand Payout 
        $get_data = $this->View_orders_model->getOrdersPay($search_by,$table_search_encode);
        
        $result_data = array();
        $i =0;
        $old_order_id=-1;
        foreach($get_data as $val) {   
            
            $result_data[$i][] = $val['created_datetime'];            
            $result_data[$i][] = $val['order_id'];
            $result_data[$i][] = $val['product_name'];
            $result_data[$i][] = $val['company_name'];
            $brand_code ='';
            $product_price = 0;
            $commission_rate = 0;
            if($val['pay_mode']==1)
            {
                $result_data[$i][] = 'COD';
            }else
            {
                $result_data[$i][] = 'Online';
            }
            $date_ref_no = explode('-',$val['created_datetime']);
            
            if($val['brand_code']!='')
            {
                $brand_code = $val['brand_code'];
            }

            $result_data[$i][] = $brand_code.'/'.$date_ref_no[1].'/'.$date_ref_no[0].'/'.$val['order_id'];
            @$result_data[$i][] = $val['tranid'];
            
            
            if(isset($val['product_qty']))
            {
                $result_data[$i][] = $val['product_qty'];
            }
           
            $result_data[$i][] = $val['product_price'];
            
            if($old_order_id != $val['order_id']){
                $result_data[$i][] = $val['order_tax_amount'];
                $result_data[$i][] = $val['shipping_amount'];
                $result_data[$i][] = $val['cod_amount'];
            }else{
                $result_data[$i][] = 0;
                $result_data[$i][] = 0;
                $result_data[$i][] = 0;
            }
            $old_order_id = $val['order_id'];
            
            $result_data[$i][] = "=ROUND((SUM(I".($i+2).":L".($i+2).")-AI".($i+2)."-AN".($i+2)."),2)";//-AI".($i+2)."-AN".($i+2);//$val['order_total'];
            $result_data[$i][] = $val['commission_rate'];
            if(@$val['product_price']!='')
            {
                $product_price = $val['product_price'];
            }

            /*if(@$val['commission_rate']!='')
            {
                $commission_rate = $val['commission_rate'];
            }*/
            //$comm_amt =  $product_price*($commission_rate/100);
            //@$comm_amt_tax =  $comm_amt*(14/100);
            //@$comm_amt_sb_tax =  $comm_amt*(0.5/100);

            if($val['service_tax'] > 0){
                if($val['commission_cat'] == 3){
                    $result_data[$i][] = "=ROUND((AQ".($i+2)."*N".($i+2)."%)-S".($i+2).",2)";//$comm_amt;
                }
                else
                    $result_data[$i][] = "=ROUND(AQ".($i+2)."*N".($i+2)."%,2)";//$comm_amt;

                $result_data[$i][] = "=ROUND(O".($i+2)."*14%,2)";//$comm_amt_tax;
                $result_data[$i][] = "=ROUND(O".($i+2)."*0.5%,2)";//$comm_amt_sb_tax;
                $com_kkc = "=ROUND(O".($i+2)."*0.5%,2)";//$comm_amt_sb_tax;
                
                //@$total_comm_invoice = $comm_amt+$comm_amt_tax+$comm_amt_sb_tax;
                $result_data[$i][] = "=ROUND(SUM(O".($i+2).":Q".($i+2).")+AO".($i+2).",2)";//round($total_comm_invoice,2);
                
            }else{
                $result_data[$i][] = "=ROUND(((R".($i+2)."*100)/115),2)";//$comm_amt;
                $result_data[$i][] = "=ROUND(((O".($i+2).")*14%),2)";//$comm_amt_tax;
                $result_data[$i][] = "=ROUND(((O".($i+2).")*0.5%),2)";//$comm_amt_sb_tax;
                $com_kkc = "=ROUND(((O".($i+2).")*0.5%),2)";//$comm_amt_sb_tax;
                
                //@$total_comm_invoice = $comm_amt+$comm_amt_tax+$comm_amt_sb_tax;
                $result_data[$i][] = "=ROUND(((AQ".($i+2)."/100)*N".($i+2)."-V".($i+2)."),2)";//round($total_comm_invoice,2);
            }
            $pgCost=0;
            if($val['pay_mode']!=1)
            {
                //$pgCost = $val['order_total'] * (3.3/100);
                $result_data[$i][] ="=ROUND(M".($i+2)."*3.3%,2)";

                //$service_tax = $comm_amt+$pgCost*(14.00/100);
                $result_data[$i][] = "=ROUND(S".($i+2)."*14%,2)";

               // $swachba = ($comm_amt+$pgCost)*(0.50/100);
                $result_data[$i][] = "=ROUND(S".($i+2)."*0.5%,2)";
                $pg_kkc = "=ROUND(S".($i+2)."*0.5%,2)";
                
                //$total_pg_cost = $swachba+$service_tax+$pgCost;
                $result_data[$i][] = "=ROUND(SUM(S".($i+2).":U".($i+2).")+AP".($i+2).",2)";

            }else{
                $pgCost = 0;
                $result_data[$i][] = round($pgCost,2);

                $service_tax = 0;
                $result_data[$i][] = round($service_tax,2);

                $swachba = 0;
                $pg_kkc = 0;
                $result_data[$i][] = round($swachba,2);
                
                $total_pg_cost = 0;
                $result_data[$i][] = round($total_pg_cost,2);
            }


            //$tds_deduction = ($comm_amt+$pgCost)*(10/100);
            $result_data[$i][] = "=ROUND(O".($i+2)."*10%,2)";// round($tds_deduction,2);

           // $netIncome_rec_brand =  $total_comm_invoice-$tds_deduction;
            $result_data[$i][] = "=ROUND(R".($i+2)."-W".($i+2).",2)";//round($netIncome_rec_brand,2);

           // $brand_income = $val['order_total']-$total_comm_invoice;
            $result_data[$i][] = "=ROUND(M".($i+2)."-R".($i+2)."-V".($i+2).",2)";//round($brand_income,2);

            //$brand_payout =  $tds_deduction+$brand_income;
            //$result_data[$i][] = round($brand_payout,2);

            $result_data[$i][] = $val['daysLapsed'];
            if($val['payment_status'] == 1)
            {
                $pay_status = 'Success';
            }else if($val['payment_status'] == 2)
            {
                $pay_status = 'Cleared';
            }else
            {
                $pay_status = 'Pending';
            }
            $result_data[$i][] = $pay_status;
            $result_data[$i][] = $val['first_name']." ".$val['last_name'];
            $result_data[$i][] = $val['mobile_no'];
            $result_data[$i][] = $val['email_id'];
            $result_data[$i][] = trim($val['shipping_address']);
            $result_data[$i][] = trim($val['city_name']);
            $result_data[$i][] = trim($val['state_name']);
            $result_data[$i][] = $val['pincode'];
            
            $result_data[$i][] = round($this->calculate_product_discountAmt($val['product_price'],$val['total_order_amt'],$val['discount_type'],$val['coupon_amount']),2);
            $result_data[$i][] = $val['coupon_code'];
            $result_data[$i][] = round($this->calculate_product_discountAmt($val['product_price'],$val['total_order_amt'],$val['discount_type'],$val['coupon_amount']),2);//$val['coupon_amount'];
            $result_data[$i][] = $val['order_unique_no'];

            $tb_status1 =  $val['order_status'];
            if($tb_status1 == 1)
            {
                $apr_status = "processing";

            }else if($tb_status1 == 2)
            {                   
                $apr_status = "confirmed";
            }else if($tb_status1 == 3)
            {
                $apr_status = "dispatched";

            }else if($tb_status1 == 4)
            {
                $apr_status = "delivered";

            }else if($tb_status1 == 5)
            {
                $apr_status = "cancelled";

            }else if($tb_status1 == 6)
            {
                $apr_status = "return";
                
            }else if($tb_status1 == 7)
            {
                $apr_status = "fake";
            }
            else
            {
                $apr_status = 'Unknown';
            }
            $result_data[$i][] = $apr_status;
            $result_data[$i][] = round($this->calculate_product_discountAmt($val['product_price'],$val['total_order_amt'],1,(int)$val['points']),2);
            $result_data[$i][] = $com_kkc;
            $result_data[$i][] = $pg_kkc;
            if($val['compare_price'] > $val['product_price'])
                $result_data[$i][] = $val['compare_price'];
            else
                $result_data[$i][] = $val['product_price'];
            $i++;
        }
        //echo "<pre>";print_r($result_data);exit;
       // echo $param.' '.$is_download;
        if($param == 'downloadExcel' && $is_download == 1)
        {           
          //========================Excel Download==============================
            //$excel_result = $this->writeDataintoCSV($fieldName,$get_data,'view_order_payment');
           $this->writeDataintoCSV($fieldName,$result_data,'view_order_payment');
           //========================End Excel Download==========================

           
        }
              
    }

    function calculate_product_discountAmt($product_price,$totalproductprice,$discountType,$discountAmt)
    { //echo $product_price.'--'.$totalproductprice.'--'.$discountType.'--'.$discountAmt.'<br>';
      if($product_price!='' && $totalproductprice!=0 && $discountAmt!='')
      { 

        if($discountType==1 || $discountType==3)
        {
            $percent = ($product_price*100)/$totalproductprice;
            return $DiscountAmt = ($discountAmt*$percent)/100;
            /*$productDiscountAmt = $product_price-$DiscountAmt;
            return $productDiscountAmt;*/

        }else if($discountType==2 || $discountType==4)  
        {    
            $percent = $discountAmt; 
            return $DiscountAmt = ($product_price*$percent)/100;
           /* $productDiscountAmt = $product_price-$DiscountAmt; 
            return $productDiscountAmt;*/
        } 
      
      }else
      {
        return 0;
      }
    }

   public function exportExcelgst($param,$search_by=NULL,$table_search=NULL,$is_download=NULL)
    {   
        $is_download = $this->uri->segment(6); 
        if($search_by == 6 )
        {
            $table_search_encode = urldecode(base64_decode($table_search));
        }else if($search_by == 8)
        {
            $table_search_encode = urldecode(base64_decode($table_search));
        }else
        {
            $table_search_encode = urldecode($table_search);
        }       
        //$is_download = $this->input->post('param');
        $fieldName = array('Date','Order ID','Product Name','Brand Name','Payment Type','Unique Reference Number','Transaction Id','Quantity','Product Amount','Brand Tax Amount(IGST)','Brand Tax Amount(SGST)','Brand Tax Amount(CGST)','Shipping Charges','COD Charges','Discount','Referral Amount Used','Grand Total','MRP','PG cost @3.3%','PG cost IGST(18.0%)','Total PG cost @3.3%','Rate Of Commission(%)','Commission Amount','Commission IGST(18.0%)','Commission SGST(9%)','Commission CGST(9%)','Total Commission Invoice','TDS deduction by Brand on commission'
                            ,'Net Income receivable from Brand','Brand Income','Days Lapsed'
                            ,'Payment Status','Username','Phone Number','Email'
                            ,'Shipping Address','City','State','Pincode', 'Coupon Code', 'Coupon Amount', 'Unique No', 'Order Status'
                            );//Brand Payout 
        $get_data = $this->View_orders_model->getOrdersPay($search_by,$table_search_encode);
        
        $result_data = array();
        $i =0;
        $old_order_id=-1;
        foreach($get_data as $val) {   
            
            $result_data[$i][] = $val['created_datetime'];            
            $result_data[$i][] = $val['order_id'];
            $result_data[$i][] = $val['product_name'];
            $result_data[$i][] = $val['company_name'];
            $brand_code ='';
            $product_price = 0;
            $commission_rate = 0;
            if($val['pay_mode']==1)
            {
                $result_data[$i][] = 'COD';
            }else
            {
                $result_data[$i][] = 'Online';
            }
            $date_ref_no = explode('-',$val['created_datetime']);
            
            if($val['brand_code']!='')
            {
                $brand_code = $val['brand_code'];
            }

            $result_data[$i][] = $brand_code.'/'.$date_ref_no[1].'/'.$date_ref_no[0].'/'.$val['order_id'];
            @$result_data[$i][] = $val['tranid'];
            
            
            if(isset($val['product_qty']))
            {
                $result_data[$i][] = $val['product_qty'];
            }
           
            $result_data[$i][] = $val['product_price'];
            
            if($old_order_id != $val['order_id']){
                if(trim($val['state_name'])=='Maharashtra')
                {
                    $result_data[$i][] = 0;//IGST 18%
                    $result_data[$i][] = round($val['order_tax_amount']/2);//SGST 9%
                    $result_data[$i][] = round($val['order_tax_amount']/2);//CGST 9%
                }else
                {
                    $result_data[$i][] = $val['order_tax_amount'];//IGST 18%
                    $result_data[$i][] = 0;//SGST 9%
                    $result_data[$i][] = 0;//CGST 9%
                }
                
                $result_data[$i][] = $val['shipping_amount'];
                $result_data[$i][] = $val['cod_amount'];
            }else{
                $result_data[$i][] = 0;
                $result_data[$i][] = 0;
                $result_data[$i][] = 0;
                $result_data[$i][] = 0;
                $result_data[$i][] = 0;
            }
            $old_order_id = $val['order_id'];

            $result_data[$i][] = round($this->calculate_product_discountAmt($val['product_price'],$val['total_order_amt'],$val['discount_type'],$val['coupon_amount']),2);//Discount
            $result_data[$i][] = round($this->calculate_product_discountAmt($val['product_price'],$val['total_order_amt'],1,(int)$val['points']),2);//Referral Amount
            
            $result_data[$i][] = "=ROUND((SUM(I".($i+2).":N".($i+2).")-O".($i+2)."-P".($i+2)."),2)";//Grand Total

            //MRP
             if($val['compare_price'] > $val['product_price'])
                $result_data[$i][] = $val['compare_price'];
            else
                $result_data[$i][] = $val['product_price'];
            
            if(@$val['product_price']!='')
            {
                $product_price = $val['product_price'];
            }

            $pgCost=0;
            if($val['pay_mode']!=1)
            {
                //$pgCost = $val['order_total'] * (3.3/100);
                $result_data[$i][] ="=ROUND(Q".($i+2)."*3.3%,2)";

                //$service_tax = $pgCost*(18.00/100);
                $result_data[$i][] = "=ROUND(S".($i+2)."*18%,2)";//PG cost IGST(18%)
              
                //$total_pg_cost = $service_tax+$pgCost;
                $result_data[$i][] = "=ROUND(SUM(S".($i+2)."+T".($i+2)."),2)";

            }else{
                $pgCost = 0;
                $result_data[$i][] = round($pgCost,2);

                $service_tax = 0;
                $result_data[$i][] = round($service_tax,2);
                
                $total_pg_cost = 0;
                $result_data[$i][] = round($total_pg_cost,2);
            }

            /*if(@$val['commission_rate']!='')
            {
                $commission_rate = $val['commission_rate'];
            }*/
            //$comm_amt =  $product_price*($commission_rate/100);
            //@$comm_amt_tax =  $comm_amt*(14/100);
            //@$comm_amt_sb_tax =  $comm_amt*(0.5/100);

            $result_data[$i][] = $val['commission_rate'];
            if($val['service_tax'] > 0){
                if($val['commission_cat'] == 3){
                    $result_data[$i][] = "=ROUND((R".($i+2)."*V".($i+2)."%)-U".($i+2).",2)";//$comm_amt;
                }
                else
                    $result_data[$i][] = "=ROUND(R".($i+2)."*V".($i+2)."%,2)";//$comm_amt;

                if(trim($val['state_name'])=='Maharashtra')
                {
                    $result_data[$i][] = 0;//$comm_amt_tax_igst;
                    $result_data[$i][] = "=ROUND(W".($i+2)."*9%,2)";//$comm_amt_tax_sgst;
                    $result_data[$i][] = "=ROUND(W".($i+2)."*9%,2)";//$comm_amt_tax_cgst;
                }else
                {
                    $result_data[$i][] = "=ROUND(W".($i+2)."*18%,2)";//$comm_amt_tax_igst;
                    $result_data[$i][] = 0;//$comm_amt_tax_sgst;
                    $result_data[$i][] = 0;//$comm_amt_tax_cgst;
                }
                
                //@$total_comm_invoice = $comm_amt+$comm_amt_tax
                $result_data[$i][] = "=ROUND(SUM(W".($i+2).":Z".($i+2)."),2)";//round($total_comm_invoice,2);                
                
            }else{
                //$result_data[$i][] = "=ROUND(((R".($i+2)."*100)/118),2)";//$comm_amt;
               $result_data[$i][] = "=ROUND(((R".($i+2)."*V".($i+2)."%*100)/118),2)";//$comm_amt;

                if(trim($val['state_name'])=='Maharashtra')
                {
                    $result_data[$i][] = 0;//$comm_amt_tax_igst;
                    $result_data[$i][] = "=ROUND(W".($i+2)."*9%,2)";//$comm_amt_tax_sgst;
                    $result_data[$i][] = "=ROUND(W".($i+2)."*9%,2)";//$comm_amt_tax_cgst;
                }else
                {
                    $result_data[$i][] = "=ROUND(W".($i+2)."*18%,2)";//$comm_amt_tax_igst;
                    $result_data[$i][] = 0;//$comm_amt_tax_sgst;
                    $result_data[$i][] = 0;//$comm_amt_tax_cgst;
                }             
                
                //@$total_comm_invoice = $comm_amt+$comm_amt_tax;
                $result_data[$i][] = "=ROUND(SUM(W".($i+2).":Z".($i+2)."),2)";//round($total_comm_invoice,2);
            }
            

            //$tds_deduction = ($comm_amt+$pgCost)*(10/100);
            $result_data[$i][] = "=ROUND(W".($i+2)."*10%,2)";// round($tds_deduction,2);

           // $netIncome_rec_brand =  $total_comm_invoice-$tds_deduction;
            $result_data[$i][] = "=ROUND(AA".($i+2)."-AB".($i+2).",2)";//round($netIncome_rec_brand,2);

           // $brand_income = $val['order_total']-$total_comm_invoice;
            $result_data[$i][] = "=ROUND(Q".($i+2)."-AA".($i+2)."-U".($i+2).",2)";//round($brand_income,2);

            //$brand_payout =  $tds_deduction+$brand_income;
            //$result_data[$i][] = round($brand_payout,2);

            $result_data[$i][] = $val['daysLapsed'];
            if($val['payment_status'] == 1)
            {
                $pay_status = 'Success';
            }else if($val['payment_status'] == 2)
            {
                $pay_status = 'Cleared';
            }else
            {
                $pay_status = 'Pending';
            }
            $result_data[$i][] = $pay_status;
            $result_data[$i][] = $val['first_name']." ".$val['last_name'];
            $result_data[$i][] = $val['mobile_no'];
            $result_data[$i][] = $val['email_id'];
            $result_data[$i][] = trim($val['shipping_address']);
            $result_data[$i][] = trim($val['city_name']);
            $result_data[$i][] = trim($val['state_name']);
            $result_data[$i][] = $val['pincode'];
            
           
            $result_data[$i][] = $val['coupon_code'];
            $result_data[$i][] = round($this->calculate_product_discountAmt($val['product_price'],$val['total_order_amt'],$val['discount_type'],$val['coupon_amount']),2);//$val['coupon_amount'];
            $result_data[$i][] = $val['order_unique_no'];

            $tb_status1 =  $val['order_status'];
            if($tb_status1 == 1)
            {
                $apr_status = "processing";

            }else if($tb_status1 == 2)
            {                   
                $apr_status = "confirmed";
            }else if($tb_status1 == 3)
            {
                $apr_status = "dispatched";

            }else if($tb_status1 == 4)
            {
                $apr_status = "delivered";

            }else if($tb_status1 == 5)
            {
                $apr_status = "cancelled";

            }else if($tb_status1 == 6)
            {
                $apr_status = "return";
                
            }else if($tb_status1 == 7)
            {
                $apr_status = "fake";
            }
            else
            {
                $apr_status = 'Unknown';
            }
            $result_data[$i][] = $apr_status;
            
            /*$result_data[$i][] = $com_kkc;
            $result_data[$i][] = $pg_kkc;*/
           
            $i++;
        }
        //echo "<pre>";print_r($result_data);exit;
       // echo $param.' '.$is_download;
        if($param == 'downloadExcelgst' && $is_download == 1)
        {           
          //========================Excel Download==============================
            //$excel_result = $this->writeDataintoCSV($fieldName,$get_data,'view_order_payment');
           $this->writeDataintoCSV($fieldName,$result_data,'view_order_payment_gst');
           //========================End Excel Download==========================

           
        }
              
    }




}

?>