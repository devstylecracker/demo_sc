<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productinventory_new extends MY_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
       parent::__construct();
       $this->load->model('productinventorynew_model');
            
   }    
	public function index($offset = null)
	{
	 	if(!$this->session->userdata('user_id')){
			$this->load->view('login');
			}else{ 
				$data = array();			
				if($this->has_rights(61) == 1){	

					$data['paid_store'] = $this->productinventorynew_model->get_paid_stores();

					$this->load->view('common/header');
					$this->load->view('productinventorynew',$data);
					$this->load->view('common/footer');
				}else{
					$this->load->view('common/header');
					$this->load->view('not_permission');
					$this->load->view('common/footer');
			}
		}
	}	

	public function get_products(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$store_id = $this->input->post('store_id');
			$product_id = $this->input->post('product_id');
			$product_sku = $this->input->post('product_sku');
			$min_offset = $this->input->post('min_offset');
			$max_offset = $this->input->post('max_offset');

			$pcount = $this->productinventorynew_model->get_products_count($store_id,$product_id);
			$result = $this->productinventorynew_model->get_products($store_id,$product_id,$product_sku,$min_offset,50);

			$producthtml = '';$i=1;
			//$producthtml = $producthtml.'<table class="table table-bordered table-striped table-enventory"><thead><tr><th>Sr. No.</th><th>Product ID</th><th>Product Name</th><th>Action</th><th>Save</th></tr></thead><tbody>';
			if(!empty($result)){ if($min_offset >0) { $i = ($min_offset*50)+1; } else{ $i=1; } $k = 1;
				foreach($result as $val){ 

					$size_text = $val['size_text']!='' ? explode(",",$val['size_text']) : array();
					$stock_count = $val['stock_count']!='' ? explode(",",$val['stock_count']) : array();
					//echo $val['size_text'] .''.$val['stock_count'].'***********';
					//echo count($size_text) .'=='. count($stock_count).'<br>';
					$product_sku = $val['product_sku']!='' ? explode(",",$val['product_sku']) : array();
					
					$producthtml = $producthtml.'<tr>';
					$producthtml = $producthtml.'<td>'.$i.'</td>';
					$producthtml = $producthtml.'<td>'.$val['product_id'].'</td>';
					$producthtml = $producthtml.'<td>'.$val['name'].'</td>';
					$producthtml = $producthtml.'<td><div id="product_info'.$i.'">';  
					if((count($size_text) == count($stock_count)) && count($stock_count)>0){ $p = 0;
						for($k = 1; $k<=(count($size_text)); $k++){ 
					$producthtml = $producthtml.'<div id="row_'.$val['product_id'].'_'.$k.'" class="product-size-qty-wrp"><div class="input-group-label-wrp"><input type="text" name="product_size'.$val['product_id'].'_'.$k.'" id="product_size'.$val['product_id'].'_'.$k.'" placeholder="Size" value="'.$size_text[$p].'" class="form-control input-xs"></div><div class="input-group-label-wrp"><input type="text" pattern="[0-9]" name="product_qty'.$val['product_id'].'_'.$k.'" id="product_qty'.$val['product_id'].'_'.$k.'" placeholder="Qty" value="'.$stock_count[$p].'" class="form-control input-xs"></div><div class="input-group-label-wrp"><input type="text" name="product_sku'.$val['product_id'].'_'.$k.'" id="product_sku'.$val['product_id'].'_'.$k.'" placeholder="SKU" value="'.@$product_sku[$p].'" class="form-control input-xs"></div><span onclick="remove_row('.$k.','.$val['product_id'].');" class="btn btn-primary btn-xs btn-delete"><i class="fa fa-minus"></i></span></div>';
						$p++; }
					}

					$producthtml = $producthtml.'<div id="row_'.$val['product_id'].'_'.(count($size_text)+1).'" class="product-size-qty-wrp"><div class="input-group-label-wrp"><input type="text" name="product_size'.$val['product_id'].'_'.(count($size_text)+1).'" id="product_size'.$val['product_id'].'_'.(count($size_text)+1).'" placeholder="Size" class="form-control input-xs"></div><div class="input-group-label-wrp"><input type="text" pattern="[0-9]" name="product_qty'.$val['product_id'].'_'.$k.'" id="product_qty'.$val['product_id'].'_'.(count($size_text)+1).'" placeholder="Qty" class="form-control input-xs"></div><div class="input-group-label-wrp"><input type="text" name="product_sku'.$val['product_id'].'_'.(count($size_text)+1).'" id="product_sku'.$val['product_id'].'_'.(count($size_text)+1).'" placeholder="SKU"  class="form-control input-xs"></div><span onclick="addnew_row('.$i.','.$val['product_id'].');" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i></span></div>';

					$producthtml = $producthtml.'</div><input type="hidden" name="row_count'.$val['product_id'].'" id="row_count'.$val['product_id'].'" value="'.(count($size_text)+1).'"></div><div style="color:green;" id="message'.$val['product_id'].'" class="success"></div>
					</td>';
					$producthtml = $producthtml.'<td><input type="button" name="save_size" id="save_size" value="Save" onclick="save_product_size('.$val['product_id'].');"></td>';
					$producthtml = $producthtml.'</tr>';
					$i++;
				}			
			}else{
				$producthtml = $producthtml.'<tr>';
				$producthtml = $producthtml.'<td colpan="6">Sorry no records found</td>';
				$producthtml = $producthtml.'</tr>';
			}
			
			if($i <= $pcount){
			$producthtml = $producthtml.'<tr id="remove'.$min_offset.'"><td colspan="5"><div id="load_more" onclick="load_more();" class="btn btn-primary btn-xs">Load More</div></td></tr>';
			}
			$producthtml = $producthtml.'<input type="hidden" name="max_offset" id="max_offset" value="'.$pcount.'">';
			echo $producthtml;
		}
	}	

	public function save_product_size(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$product_id = $this->input->post('product_id');
			$product_info = json_decode($this->input->post('product_info'));

			if($product_id!=''){
				$this->productinventorynew_model->save_product_size($product_id,$product_info);
			}
		}
	}

	public function download_inventory($store){
		
		$is_download = 1;
		$fieldName = array('product_id','product_name','product_size','quantity','sku_code','price','is_active');	
		if($this->input->post('store_id')!='')
		{
			$store_id = $this->input->post('store_id');
		}else
		{
			$store_id = $store;
		}	
		
		$product_id = $this->input->post('product_id');
		$product_sku = $this->input->post('product_sku');
		$min_offset = $this->input->post('min_offset');
		$max_offset = $this->input->post('max_offset');		
		$i =0;
		$result = $this->productinventorynew_model->get_products($store_id,$product_id,$product_sku,$min_offset,$max_offset);
		$result_data = array();
		
		foreach($result as $val) {   
			
			$result_data[$i][] = $val['product_id'];
			$result_data[$i][] = $val['name'];	
			$size_array = explode(',',$val['size_text']);
			$stock_array = explode(',',$val['stock_count']);
			$sku_array = explode(',',$val['product_sku']);
			for($j=0;$j<count($size_array);$j++)
			{	
				if($j!=0)
				{
					$i++;
					$result_data[$i][] = $val['product_id'];
					$result_data[$i][] = $val['name'];						
					$result_data[$i][] = $size_array[$j];
					$result_data[$i][] = $stock_array[$j];
					$result_data[$i][] = @$sku_array[$j];
				}else
				{
					$result_data[$i][] = $size_array[$j];
					$result_data[$i][] = $stock_array[$j];
					$result_data[$i][] = $sku_array[$j];					
				}

			}
			$result_data[$i][] = $val['price'];
			$result_data[$i][] = $val['status'];
			
			$i++;
		}
		
		if($is_download == 1)
		{       	
		  //========================Excel Download==============================
			//$excel_result = $this->writeDataintoCSV($fieldName,$get_data,'view_order_payment');
		   $this->writeDataintoCSV($fieldName,$result_data,'Product_Inventory_');
		   //========================End Excel Download==========================

		   
		}
	}			
}
