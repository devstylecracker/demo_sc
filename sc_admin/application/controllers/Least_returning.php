<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Least_returning extends MY_Controller {
	
	function __construct(){
       parent::__construct();
       $this->load->model('websiteusers_model'); 
       $this->load->model('users_model');    
	   $this->load->model('reports_model'); 
      
   } 
  
        public function index($offset = null){
			    //Least_returning_view.php
				/*$search_by = $this->input->post('search_by');
				$table_search = $this->input->post('table_search');*/
				$search_by = 0;
				$table_search = 0;
				//echo "<pre>"; print_r($this->uri); 
                $search_by = $this->input->post('search_by')!='' ? $this->input->post('search_by') : $this->uri->segment(3);
                $table_search = $this->input->post('table_search')!='' ? $this->input->post('table_search') : $this->uri->segment(4);
                
                if($search_by!='0' && $table_search!='0' && $search_by!='' && $table_search!=''){ 
                	$paginationUrl = 'Least_returning/index/'.$search_by.'/'.$table_search;
                	$uri_segment = 5;
            	}else{
            		$paginationUrl = 'Least_returning/index/0/0';
            		$uri_segment = 5;
            	}
            	 $config['per_page'] = 20;
                $offset = $this->uri->segment(5)!='' ? $this->uri->segment(5) : 0;
               $config['total_rows'] = count($this->reports_model->most_least_runing_all($search_by,$table_search,'',''));
                $total_rows = $config['total_rows'];
                $get_data =  $this->reports_model->most_least_runing_all($search_by,$table_search,$offset,$config['per_page']);
                $count = $config['total_rows'];
                $data_post['most_least_runing'] = $get_data;
                $data_post['search_by'] = $search_by;
                $data_post['table_search'] = $table_search;


				$config['base_url'] = base_url().$paginationUrl;		
				$config['total_rows'] = $count;
				$config['per_page'] = 20;
				$config['uri_segment'] = $uri_segment;
				$config['use_page_numbers'] = TRUE;
				$config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
				$config['full_tag_close'] = '</ul></div>';

				$config['first_link'] = '&laquo; First';
				$config['first_tag_open'] = '<li>';
				$config['first_tag_close'] = '</li>';

				$config['last_link'] = 'Last &raquo;';
				$config['last_tag_open'] = '<li>';
				$config['last_tag_close'] = '</li>';

				$config['next_link'] = 'Next &rarr;';
				$config['next_tag_open'] = '<li>';
				$config['next_tag_close'] = '</li>';

				$config['prev_link'] = '&larr; Previous';
				$config['prev_tag_open'] = '<li>';
				$config['prev_tag_close'] = '</li>';

				$config['cur_tag_open'] = '<li><a href="" style="background-color:#7CCDEF;">';
				$config['cur_tag_close'] = '</a></li>';

				$config['num_tag_open'] = '<li>';
				$config['num_tag_close'] = '</li>';

				
				$this->pagination->initialize($config);	
				$this->data['total_rows'] = $total_rows;

				if($this->session->userdata('user_id')){
					$data1 = array();
					$date_from = '';  $date_to = '';

					   if($this->input->post()){
						  $date_from = $this->input->post('date_from');
						  $date_to = $this->input->post('date_to');
					   }
				  //$data1['most_least_runing'] = $this->reports_model->most_least_runing_all($search_by,$table_search,$offset,$config['per_page']);
				   //$data1['most_least_runing'] = $this->reports_model->most_least_runing($date_from,$date_to);					
					$data['total_rows'] = count($this->reports_model->most_least_runing_all($search_by,$table_search,'',''));
					$this->load->view('common/header');
					$this->load->view('reports/Least_returning_view',$data_post);
					$this->load->view('common/footer');
				}
		}
		
		public function user_view(){
			 $data = array();
		
			 $data['roles'] = $this->users_model->get_roles(); 
			 $user_id =  $this->uri->segment(3);
			 $data['user_data'] = $this->users_model->get_user_data($user_id);
			 $data['pa_data'] = $this->users_model->get_user_pa($user_id); 
			 $data['wordrobe'] = $this->users_model->get_wordrobe_image($user_id); 
			if($this->has_rights(15) == 1){
				
				$this->load->view('common/header',$data);
				$this->load->view('View_all_users_view');
				$this->load->view('common/footer');
				
			}else{
				
				$this->load->view('common/header',$data);
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			 }
			
			}
}
