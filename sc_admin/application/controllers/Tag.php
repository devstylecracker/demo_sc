<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tag extends MY_Controller {    

    function __construct(){
        parent::__construct();
        $this->load->model('Tag_model');       

    }  
 /*---------Category submodule functionalities----------*/

    public function index($offset = null)
    {   
      
        if(!$this->session->userdata('user_id')){

            $this->load->view('login');                    

        }else{ 
			
			/*All Data Show*/			
			$data_post = array();                       
           if($this->has_rights(33) == 1){	 
			    $search_by = $this->input->post('search_by');
                $table_search = $this->input->post('table_search');                 
			    $paginationUrl = 'Tag/index/';
                $uri_segment = 3;
                $config['per_page'] = 20;
                $config['total_rows'] = count($this->Tag_model->get_all_tag($search_by,$table_search,'',''));                
                $total_rows = $config['total_rows'];                                                    
                $get_users = $this->Tag_model->get_all_tag($search_by,$table_search,$offset,$config['per_page']);
                //echo '<pre>'; print_r($get_users); echo '</pre>';
                $count = $config['total_rows']; 
                $data_post['categories'] = $get_users;
                
                // $this->customPagination($paginationUrl,$count,$uri_segment,$get_users,$total_rows); 
				$config['base_url'] = base_url().$paginationUrl;		
				$config['total_rows'] = $count;
				$config['per_page'] = 20;
				$config['uri_segment'] = $uri_segment;
				$config['use_page_numbers'] = TRUE;
				$config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
				$config['full_tag_close'] = '</ul></div>';

				$config['first_link'] = '&laquo; First';
				$config['first_tag_open'] = '<li>';
				$config['first_tag_close'] = '</li>';

				$config['last_link'] = 'Last &raquo;';
				$config['last_tag_open'] = '<li>';
				$config['last_tag_close'] = '</li>';

				$config['next_link'] = 'Next &rarr;';
				$config['next_tag_open'] = '<li>';
				$config['next_tag_close'] = '</li>';

				$config['prev_link'] = '&larr; Previous';
				$config['prev_tag_open'] = '<li>';
				$config['prev_tag_close'] = '</li>';

				$config['cur_tag_open'] = '<li><a href="" style="background-color:#7CCDEF;">';
				$config['cur_tag_close'] = '</a></li>';

				$config['num_tag_open'] = '<li>';
				$config['num_tag_close'] = '</li>';

				
				$this->pagination->initialize($config);				
			    $this->load->view('common/header');
				$this->load->view('tag_view',$data_post);
				$this->load->view('common/footer');
			}else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			}        
		}
	}
	
	/*Add and edit tag function here*/
	public function tag_addedit($id=NULL)
    {
		$data_post = array();        
        $loggedInUser=$this->session->userdata('user_id');
        $data_post['tagtype'] = $this->Tag_model->get_tag_type();     


        if($id==NULL){	
			if($this->has_rights(34) == 1)
			{	
				if($this->input->post()){
					
					$this->form_validation->set_rules('tag_name', 'Tag Name', 'required|is_unique[tag.name]'); 					
					$this->form_validation->set_rules('tag_slug', 'Tag Slug', 'required|is_unique[tag.slug]');
					$this->form_validation->set_rules('tagtype', 'Tag Type', 'required');
										
					if ($this->form_validation->run() == FALSE)
                    {
										
						$this->load->view('common/header');
                        $this->load->view('add_tag',$data_post);
                        $this->load->view('common/footer');						
					}else
					{
						$tag_name = $this->input->post('tag_name');
						$tag_slug = $this->input->post('tag_slug');
						$tagtype  = $this->input->post('tagtype');
						$tag_slug_space = str_replace(' ', '-', $tag_slug);												
						$active = $this->input->post('active');	
						$catCount = $this->Tag_model->checkMultilpetag($this->input->post('tag_name'));	
						if($catCount->catCount > 0){
							$this->session->set_flashdata('feedback', '<i class="fa fa-check sign"></i><strong>Error!</strong> Tag already exits!');
						}else{						
							$data_post = array(
							   'name'=>$tag_name,
							   'slug'=>$tag_slug_space,                           
							   'status'=> $active,
							   'tag_type_id' =>  $tagtype,                               
							   'created_by'=> $loggedInUser,
							   'created_datetime'=> date("Y-m-d H:i:s"),                                
							   'modified_by'=> $loggedInUser,
							   'modified_datetime'=> date("Y-m-d H:i:s")                                
							);
							$created = $this->Tag_model->new_tag($data_post);                
							   if($created){                    
								  $this->session->set_flashdata('feedback', '<div class="callout callout-success alert-dismissable"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Category added successfully!</div>');
								  redirect('Tag'); 
								}else{                                          
								  $this->session->set_flashdata('feedback', '<i class="fa fa-times-circle sign"></i><strong>Error!</strong> The server is not responding, try again later.');
								  redirect('Tag'); 
								} 
                         }                          				
					}					                   
				}else{					
					$this->load->view('common/header');
					$this->load->view('add_tag',$data_post);
					$this->load->view('common/footer');
				}					
			}else{
			$this->load->view('common/header');
            $this->load->view('not_permission');
            $this->load->view('common/footer');
			}
			
		}else{
			/*Edit Code Here tag*/
			if($this->has_rights(35) == 1){	
			     $data_post['tag'] = $this->Tag_model->get_tag_by_ID($id);
			     
			     if($this->input->post()){
					 $this->form_validation->set_rules('tag_name', 'Tag Name', 'required');
					 if ($this->form_validation->run() == FALSE)
                        {
                           
                           $this->load->view('common/header');
                           $this->load->view('add_tag');
                           $this->load->view('common/footer');
                        
                        }else{
							
							$tag_name = $this->input->post('tag_name');
							$tag_slug = $this->input->post('tag_slug');
							$tagtype  = $this->input->post('tagtype');
							$tag_slug_space = str_replace(' ', '-', $tag_slug);
							$active = $this->input->post('active');
							
							$catCount = $this->Tag_model->checkMultilpetagEdit($id,$tag_name);
							
							if($catCount->catCount > 0){
								
								$this->session->set_flashdata('feedback', '<div class="callout callout-warning"><h5><i class="icon fa fa-warning"></i><strong>Error!</strong></h5> Tag already exits!</div>');
							
							}else{
								$data_post = array(
												'name'=>$tag_name,  
												'slug'=>$tag_slug_space,
												'tag_type_id' =>  $tagtype,                                                   
                                                'status'=> $active, 
                                                'modified_by'=> $loggedInUser,
                                                'modified_datetime'=> date("Y-m-d H:i:s")                                
                                                );
                                 $update = $this->Tag_model->update_tag($id,$data_post); 
                                 
                                 if($update){
									 
									 $this->session->set_flashdata('feedback', '<div class="callout callout-success alert-dismissable"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Changes has been Updated successfully</div>');
                                     redirect('/Tag');
									 
								 }else{
									 
									 $this->session->set_flashdata('feedback', '<div class="callout callout-warning"><h5><i class="icon fa fa-warning"></i><strong>Error!</strong></h5> The server is not responding, try again later.</div>');
                                     redirect('/Tag');
									 
								 }
                                                                  
							}							
						}
				 }
			     $this->load->view('common/header');
                 $this->load->view('add_tag',$data_post);
                 $this->load->view('common/footer');
			    
			}else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			}			
		}        
	}
	
	/*delete Code Here tag*/
	function delete_tag()
    {   		
        $catID = $this->input->post('catID');                
        $deleted = $this->Tag_model->delete_tag($catID);
        
        if($deleted)
        {
            $this->session->set_flashdata('feedback', '<div class="callout callout-success alert-dismissable"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Record has been deleted successfully</div>');
            redirect('/Tag');
        }
        else
        {           
            $this->session->set_flashdata('feedback', '<div class="callout callout-danger"><h5><i class="icon fa fa-ban"></i><strong>Error!</strong></h5>The server is not responding, try again later.</div>');  
            redirect('/Tag');    
        }
    }
	
	
	    
/*----------End of Common functions------------------------------------*/	
}
