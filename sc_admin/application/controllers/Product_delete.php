<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product_delete extends MY_Controller {

	function __construct(){
       parent::__construct();
       $this->load->model('Product_model');
       $this->load->model('Product_delete_model');
       $this->load->library('email');
   	}

    function index(){ 
      /*$data = array();
      $data['sizes'] = $this->Size_model->get_product_sizes();*/

      if(!$this->session->userdata('user_id'))
      {
         $this->load->view('login');

      }else{ 

           if($this->has_rights(39) == 1){ 

              $product_deleted = '';
              $product_notdeleted = '';
                  
              $deletedcnt = 0;          

               if($this->input->post()){                  
                  if($this->input->post('product_id')!='')
                  {                      
                     $k = array();
                     $productdelid = $_POST['product_id']; 
                     $res= $this->numeric_wcomma($productdelid);
                     if($res)
                     {
                        $products = explode(",", $productdelid);
                        //$delvarReslut = $this->Product_model->id_wise_delete($k);
                        $prdcnt = sizeof($products);    
                        foreach($products as $val)
                        {
                          $prd = $this->Product_delete_model->check_productinorder($val);          
                          if(!$prd)
                          {
                            $delvarReslut = $this->Product_delete_model->delete_productforever($val);
                            if($delvarReslut)
                            {
                              $product_deleted = $product_deleted.','.$val;
                            }            
                            $deletedcnt++;
                          }else
                          {
                            $product_notdeleted = $product_notdeleted.','.$val;
                          }          
                        }
                        $message = 'Product Count: '.$prdcnt.'<br/> Deleted Count:'.$deletedcnt.' <br/> Product Deleted: '.$product_deleted.'<br/> Product NotDeleted: '.$product_notdeleted; 
                        
                        //data-dismiss="alert" class="close"  
                        if($delvarReslut)
                        {
                            $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable">
                              <button aria-hidden="true"   type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Records deleted successfully!<br/>Product Count: '.$prdcnt.'<br/> Deleted Count:'.$deletedcnt.' <br/> Product Deleted: '.$product_deleted.'<br/> Product NotDeleted: '.$product_notdeleted.'</div>');
                              redirect('/Product_delete','refresh');
                        }else
                        {
                            $this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5> Error.</div>');
                           redirect('/Product_delete','refresh');
                        }
                     }else
                     {
                        $this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5> Enter Numeric ProductIds.</div>');
                           redirect('/Product_delete','refresh');
                     }
                  }
                  
                  }
            }
         }
      $this->load->view('common/header');      
         $this->load->view('product_delete',$data);
         $this->load->view('common/footer');

    }

      function numeric_wcomma ($str)
      {
          return preg_match('/^[0-9,]+$/', $str);
      }

   	function update_sort_order(){   
        if(!$this->session->userdata('user_id'))
      {
         $this->load->view('login');

      }else{ 

           if($this->has_rights(39) == 1){ 
            echo '<pre>';
            print_r($this->input->post());

               if($this->input->post()){
                  $this->form_validation->set_rules('product_id','product id','required');     
                  $k = array();
               	$productdelid = $_POST['product_id']; 
                  $k = explode(",", $productdelid);
                  $delvarReslut = $this->Product_model->id_wise_delete($k);
                  echo '<pre>';
                  print_r($delvarReslut);
                  exit;
                  }
            }
      	}
      }

      function productDeleteCron()
      {exit;
        $products = $this->Product_delete_model->getproduct_todelete();
       // echo '<pre>';print_r($products);
        $product_deleted = '';
        $product_notdeleted = '';
        $prdcnt = sizeof($products);
        $deletedcnt = 0;
        $message = '';
        $new_msg = '';
        foreach($products as $val)
        {
          $prd = $this->Product_delete_model->check_productinorder($val['id']);          
          if(!$prd)
          {
            $dres = $this->Product_delete_model->delete_productforever($val['id']);
            if($dres)
            {
              $product_deleted = $product_deleted.','.$val['id'];
            }
            $deletedcnt++;
          }else
          {
            $product_notdeleted = $product_notdeleted.','.$val['id'];
          }          
        }
          $message = 'Product Count: '.$prdcnt.'<br/> Deleted Count:'.$deletedcnt.' <br/> Product Deleted: '.$product_deleted.'<br/> Product NotDeleted: '.$product_notdeleted;          
          
            $config['protocol'] = 'smtp';
          //      $config['mailpath'] = '/usr/sbin/sendmail';
              $config['charset'] = 'iso-8859-1';
              $config['mailtype'] = 'html';
              $config['wordwrap'] = TRUE;
              $config['smtp_host'] = $this->config->item('smtp_host');
              $config['smtp_user'] = $this->config->item('smtp_user');
              $config['smtp_pass'] = $this->config->item('smtp_pass');
              $config['smtp_port'] = $this->config->item('smtp_port');

              $this->email->initialize($config);

              $this->email->from($this->config->item('from_email'), 'Stylecracker-ProductDeleteCron');
              $this->email->to('sudha@stylecracker.com');
              //$this->email->cc(array('arun@stylecracker.com'));
              $this->email->subject('ProductDeleteCron');
              $new_msg = $message;
              $this->email->message($new_msg);

              $this->email->send();
      }
}