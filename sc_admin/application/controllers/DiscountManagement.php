<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DiscountManagement extends MY_Controller {

	function __construct(){
       parent::__construct();
       $this->load->library('excel');
       $this->load->model('discountmanagement_model');
   }

	public function index($offset = null)
	{
	 	if(!$this->session->userdata('user_id')){
			$this->load->view('login');
			}else{
				$data = array();
				if($this->has_rights(64) == 1){

					$data['paid_brands'] = $this->discountmanagement_model->get_paid_brands();
					$this->load->view('common/header');
					$this->load->view('discount/product_discount_management',$data);
					$this->load->view('common/footer');
				}else{
					$this->load->view('common/header');
					$this->load->view('not_permission');
					$this->load->view('common/footer');
			}
		}
	}

	public function download_cuesheet($brand_id){
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
			}else{
				$data = array();
				if($this->has_rights(64) == 1){

					$is_download = 1;
					if($this->session->userdata('role_id') != 6){
						$fieldName = array('Product Id','MRP','Discount Price','Brand Discount Start Date','Brand Discount End Date','SC Discount 	Start Date','SC Discount End Date','SC Discount Percentage','Product Name','Product SKU');
					}else{
						$fieldName = array('Product Id','MRP','Discount Price','Brand Discount Start Date','Brand Discount End Date','Product Name','Product SKU');
					}
					$result = $this->discountmanagement_model->get_discount($brand_id);
					$result_data = array();
					$i =0;
					
					foreach($result as $val) {   
						
						$result_data[$i][] = $val['id'];
						$result_data[$i][] = $val['compare_price'] == 0 ? $val['price'] : $val['compare_price'];
						$result_data[$i][] = $val['price'];
						$result_data[$i][] = $val['discount_start_date'];
						$result_data[$i][] = $val['discount_end_date'];
						if($this->session->userdata('role_id') != 6){
						$result_data[$i][] = $val['start_date'];
						$result_data[$i][] = $val['end_date'];
						$result_data[$i][] = $val['discount_percent'];
						$result_data[$i][] = $val['name'];
						$result_data[$i][] = $val['product_sku'];
						}
						
						$i++;
					}
					
					if($is_download == 1)
					{       	
					  //========================Excel Download==============================
						//$excel_result = $this->writeDataintoCSV($fieldName,$get_data,'view_order_payment');
					   $this->writeDataintoCSV($fieldName,$result_data,'Product_Discount');
					   //========================End Excel Download==========================
	  
					}

				}else{
					$this->load->view('common/header');
					$this->load->view('not_permission');
					$this->load->view('common/footer');
			}
		}
	}

	function upload_discount(){
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
			}else{
				$data = array();
				if($this->has_rights(64) == 1){

					if($_FILES['product_upload']['tmp_name'] != "")
				 	{
				 		$inputFileName = $_FILES['product_upload']['tmp_name'];	

				 		$handle = fopen($inputFileName, "r");
						try{
							//read file from path
							$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
						}catch(Exception $e) {
							die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
						}

						$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

						$arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet		

						for($i=2;$i<=$arrayCount;$i++)
						{
							$product_data = array();
							$product_id = trim($allDataInSheet[$i]["A"]);
							$price = trim($allDataInSheet[$i]["B"]);
							$comp_price = trim($allDataInSheet[$i]["C"]);
							$brand_start_datetime = trim($allDataInSheet[$i]["D"]);
							$brand_end_datetime = trim($allDataInSheet[$i]["E"]);
							$sc_start_datetime = trim($allDataInSheet[$i]["F"]);
							$sc_end_datetime = trim($allDataInSheet[$i]["G"]);
							$percentage = trim($allDataInSheet[$i]["H"]);

							$product_data['product_id'] = $product_id;
							$product_data['price'] = $price;
							if($comp_price == 0){ $product_data['comp_price'] = $price;  } else{
							$product_data['comp_price'] = $comp_price; }
							$product_data['brand_start_datetime'] = $brand_start_datetime;
							$product_data['brand_end_datetime'] = $brand_end_datetime;
							$product_data['sc_start_datetime'] = $sc_start_datetime;
							$product_data['sc_end_datetime'] = $sc_end_datetime;
							$product_data['percentage'] = $percentage;

							$this->discountmanagement_model->upload_discount($product_data);
							unset($product_data);
						}
						$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>'.$i.' - Discount Uploaded Successfully</div>');
						redirect('DiscountManagement');
				 	}else{
				 		echo '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>Something goes wrong Please try again</div>';
				 	}
			}else{
					$this->load->view('common/header');
					$this->load->view('not_permission');
					$this->load->view('common/footer');
			}
		}
	}
}
