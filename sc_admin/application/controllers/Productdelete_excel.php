<?php
ini_set('max_execution_time', 0);
ini_set('memory_limit','512M');
error_reporting(E_ALL);
ini_set('display_errors', 1);
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Productdelete_excel extends MY_Controller {

    function __construct()
    {
    	 parent::__construct();   
        $this->load->library('excel');
        $this->load->model('Product_delete_model');        
	}

	function index(){
		//$filename = 'unwanted-products-for-datateam1.csv';
		$filename = 'product_delete_temp.csv';
		//$product_data1 = $this->getdata("assets/product_delete/".$filename);
		//$this->delete_products_excel($filename);	
		$this->delete_products();		
	}

	function getdata($csvFile){
		ini_set('auto_detect_line_endings',TRUE);
	    $file_handle = fopen($csvFile, 'r');
	    while (!feof($file_handle) ) {
	    	//print_r(fgetcsv($file_handle, 0, ',', '"', '"'));
	    	$line_of_text[] = fgetcsv($file_handle, 0, ',', '"', '"');
	    }
	    fclose($file_handle);
	    return $line_of_text;
	}
	

	function delete_products_excel($file_path){

		$product_data1 = $this->getdata("assets/product_delete/".$file_path);
		$product_data1 = array_filter($product_data1);
		$deletedcnt=0;		
		$product_deleted = '';
		$product_notdeleted = '';
		$i=0;
		echo "<pre>";
		print_r($product_data1);
		exit;
		if($product_data1[0][0] == 'Product_Ids')
		{		
			unset($product_data1[0]);
			/*echo "<pre>";
			print_r($product_data1);*/
			$rowcount = count($product_data1);			
			
				/*foreach ($product_data1 as $product_data) 
				{
						$i++;
						$brand_product_id = $product_data[0];		
						
					if($brand_product_id!='')
					{
						  $prd = $this->Product_delete_model->check_productinorder($brand_product_id);          
				          if(!$prd)
				          {	echo $i.'--';
				          	echo $product_image = $this->Product_delete_model->getproduct_image($brand_product_id);
				            // $dres = $this->Product_delete_model->delete_productforever($brand_product_id);
				            // if($dres)
				            // {
				            //   $product_deleted = $product_deleted.','.$brand_product_id;
				            //   unset($product_data1[0]);
				            // }
				             $product_deleted = $product_deleted.','.$brand_product_id;
				              unset($product_data1[0]);
				            $deletedcnt++;
				          }else
				          {
				            $product_notdeleted = $product_notdeleted.','.$brand_product_id;
				          } 
			        } 

			        echo '<br/>';
				}*/
			for($k=1;$k<2;$k++)
			{
				$i++;
				$brand_product_id = $product_data1[$k][0];		
				//echo '<pre>';print_r($product_data1);exit;	
				if($brand_product_id!='')
				{
					  $prd = $this->Product_delete_model->check_productinorder($brand_product_id);          
			          if(!$prd)
			          {	
			          	$product_image = $this->Product_delete_model->getproduct_image($brand_product_id);
			          	if($product_image!='')
			          	{
			          		$this->unlinkProductImage($product_image);
			          		$this->unlinkProductExtraImages($product_image);
			          	}
			            $dres = $this->Product_delete_model->delete_productforever($brand_product_id);
			            if($dres)
			            {
			              $product_deleted = $product_deleted.','.$brand_product_id;
			              unset($product_data1[$k]);
			            }
			             $product_deleted = $product_deleted.','.$brand_product_id;
			              //unset($product_data1[0]);
			            $deletedcnt++;
			          }else
			          {
			            $product_notdeleted = $product_notdeleted.','.$brand_product_id;
			          } 
		        } 
		        echo '<pre>';print_r($product_data1);
		        echo '<br/>';
			}	

		}     

	}

	function delete_products(){		echo 'delete_products';exit;
		$product_data1 = $this->Product_delete_model->getproduct_todelete_status();
		// echo "<pre>";
		// print_r($product_data1);exit;
		
		$deletedcnt=0;		
		$product_deleted = '';
		$product_notdeleted = '';
		$i=0;
		/*echo "<pre>";
		print_r($product_data1);
		exit;*/		
		$rowcount = count($product_data1);			
			
		foreach ($product_data1 as $product_data) 
		{					
			$brand_product_id = $product_data['id'];	
			echo $brand_product_id;	
				
			if($brand_product_id!='')
			{
				  $prd = $this->Product_delete_model->check_productinorder($brand_product_id);   
				  //echo '<pre>Products In Order==';print_r($prd);exit;       
		          if(!$prd)
		          {	
		          	$product_image = $this->Product_delete_model->getproduct_image($brand_product_id);
		          	//echo '<pre>Products Images==';print_r($product_image);exit;       
		          	if($product_image!='')
		          	{
		          		$this->unlinkProductImage($product_image);
		          	}

	          		$productextra_image = $this->Product_delete_model->getproduct_extraimage($brand_product_id);
	          		//echo '<pre>Products Extra Images==';print_r($productextra_image);exit;
	          		foreach ($productextra_image as $key => $value) {		          			
	          			if($value['product_images']!='')
	          			{
	          				$this->unlinkProductExtraImages($value['product_images']);
	          			}
	          			
	          		}		          		
		          	
		            $dres = $this->Product_delete_model->delete_productforever($brand_product_id);
		            if($dres)
		            {
		              $product_deleted = $product_deleted.','.$brand_product_id;
		              unset($product_data1[$i]);
		            }			           
		            $deletedcnt++;
		            $i++;
		          }else
		          {
		            $product_notdeleted = $product_notdeleted.','.$brand_product_id;
		          } 
	        } 
	        echo '<br/>';
		}	

		echo 'Product Not Deleted=='.$product_notdeleted.'<br/>';	
		echo 'Product Deleted=='.$product_deleted.'<br/>';		

	}

	public function unlinkProductImage($pimg){ 	
	        unlink('./assets/products/'.$pimg);
	        unlink('./assets/products/thumb/'.$pimg);  
	        unlink('./assets/products/thumb_124/'.$pimg);
	        unlink('./assets/products/thumb_160/'.$pimg);
	        unlink('./assets/products/thumb_310/'.$pimg);
	        unlink('./assets/products/thumb_340/'.$pimg);
	        unlink('./assets/products/thumb_546/'.$pimg);
	        unlink('./assets/product_images/zoom/'.$pimg);	        
	}

    public function unlinkProductExtraImages($pimg){
    	 	unlink('./assets/products_extra/'.$pimg);
    	 	unlink('./assets/products_extra/thumb/'.$pimg);  
    	 	unlink('./assets/products_extra/thumb_124/'.$pimg);
	        unlink('./assets/products_extra/thumb_160/'.$pimg);
	        unlink('./assets/products_extra/thumb_310/'.$pimg);
	        unlink('./assets/products_extra/thumb_340/'.$pimg);
	        unlink('./assets/products_extra/thumb_546/'.$pimg);    	 
    }

}