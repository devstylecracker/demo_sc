<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class View_orders extends MY_Controller {

	function __construct(){
       parent::__construct();
       $this->load->model('View_orders_model');
   	}

	public function index()
	{

		if(!$this->session->userdata('user_id'))
		{
			$this->load->view('login');

		}else
		{
			 $data_post = array();

			 if($this->has_rights(62) == 1)
			{

				$search_by = 0;
				$table_search = 0;

				$data_post['orderStatusList'] = array('1'=>'Processing', '2'=>'Confirmed', '3'=>'Dispatched', '4'=>'Delivered','5'=>'Cancelled','6'=>'Return','7'=>'Fake');
				//echo "<pre>"; print_r($this->uri); 
                $search_by = $this->input->post('search_by')!='' ? $this->input->post('search_by') : $this->uri->segment(3);
                $table_search = $this->input->post('table_search')!='' ? $this->input->post('table_search') : $this->uri->segment(4);
                
                if($search_by!='0' && $table_search!='0' && $search_by!='' && $table_search!=''){ 
                	$paginationUrl = 'view_orders/index/'.$search_by.'/'.$table_search;
                	$uri_segment = 5;
            	}else{
            		$paginationUrl = 'view_orders/index/0/0';
            		$uri_segment = 5;
            	}
            	
               	$config['per_page'] = 20;
                $offset = $this->uri->segment(5)!='' ? $this->uri->segment(5) : 0;
                $config['total_rows'] = $this->View_orders_model->getOrdersCount($search_by,$table_search,'','');
                $total_rows = $config['total_rows'];
                $get_data = $this->View_orders_model->getOrders($search_by,$table_search,$offset,$config['per_page']);
                $count = $config['total_rows'];
                $data_post['orders'] = $get_data;
                $data_post['search_by'] = $search_by;
                $data_post['table_search'] = $table_search;
                $data_post['sr_offset'] = $offset;
               // $this->customPagination($paginationUrl,$count,$uri_segment,$get_data,$total_rows);

				$config['base_url'] = base_url().$paginationUrl;		
				$config['total_rows'] = $count;
				
				$config['uri_segment'] = $uri_segment;
				$config['use_page_numbers'] = TRUE;
				$config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
				$config['full_tag_close'] = '</ul></div>';

				$config['first_link'] = '&laquo; First';
				$config['first_tag_open'] = '<li>';
				$config['first_tag_close'] = '</li>';

				$config['last_link'] = 'Last &raquo;';
				$config['last_tag_open'] = '<li>';
				$config['last_tag_close'] = '</li>';

				$config['next_link'] = 'Next &rarr;';
				$config['next_tag_open'] = '<li>';
				$config['next_tag_close'] = '</li>';

				$config['prev_link'] = '&larr; Previous';
				$config['prev_tag_open'] = '<li>';
				$config['prev_tag_close'] = '</li>';

				$config['cur_tag_open'] = '<li class="active" ><a href="" >';
				$config['cur_tag_close'] = '</a></li>';

				$config['num_tag_open'] = '<li>';
				$config['num_tag_close'] = '</li>';
				
				$this->pagination->initialize($config);	
				$data_post['total_rows'] = $total_rows;


				$this->load->view('common/header');
				$this->load->view('orders/view_orders',$data_post);
				$this->load->view('common/footer');
			}else
			{
				$this->load->view('common/header');
                $this->load->view('not_permission');
                $this->load->view('common/footer');

			}
		}
	}

	public function updateOrder()
	{
		if(!$this->input->is_ajax_request())
		{
			exit("No direct script access allowed");
		}else
		{
			$orderPrdId = $this->input->post('id');
			$orderStatus = $this->input->post('orderStatus');
			$result = $this->View_orders_model->updateOrder($orderPrdId,$orderStatus);

			if($result)
			{
				$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Product Order Status Updated Successfully</div>');				
				return true;
			}else
			{
				$this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Error!</strong> </h5>Error</div>');				
				return false;
			}
		}
		

	}

	public function orderstatus($orderStatus)
	{	//echo $orderStatus;exit;
		$data['status'] = $orderStatus;
		$data['result'] = $this->View_orders_model->order_status($orderStatus);
		$this->load->view('common/header');
		$this->load->view('orders/order_status_view',$data);
		$this->load->view('common/footer');
	}

	public function send_mail(){
		if(!$this->input->is_ajax_request())
		{
			exit("No direct script access allowed");
		}else
		{
			$order_id = $this->input->post('order_id');
			$product_id = $this->input->post('product_id');
			$order_status = '5';
			$data = $this->View_orders_model->update_email($order_id,$product_id,$order_status);
			$data = $this->View_orders_model->get_order_unique_no($order_id);
			//print_r($data);exit;
			if(!empty($data)){
			$this->ordercancelMessage($data[0]['order_unique_no'],$data[0]['user_id'],$data[0]['first_name'],$data[0]['last_name'],$order_id,$product_id,$order_status);
			}
		}
	}
	
	function getEmailAddress($user_id){
      $res = $this->View_orders_model->get_user_info($user_id);
      if(!empty($res)){
        return $res[0]['email'];
      }else{ return ''; }
	}
  
	/*function ordercancelMessage($order_id,$user_id,$first_name,$last_last,$product_order_id,$product_id,$order_status){
    if($this->getEmailAddress($user_id)!=''){
      $config['protocol'] = 'smtp';
//      $config['mailpath'] = '/usr/sbin/sendmail';
      $config['charset'] = 'iso-8859-1';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');
		
    $this->email->initialize($config);
	  $order_count = $this->View_orders_model->get_order_count($product_order_id);
	  $count_product = count($order_count);
	  $explode_order = (explode("_",$order_id));
	  $count_brand = count($explode_order);
	  //echo "count_product ".$count_product."count_brand ".$count_brand;exit;
      $res = $this->View_orders_model->su_get_order_info($order_id,$product_order_id,$product_id,$order_status);
	  //print_r($res);exit;
      $brand_price = $this->View_orders_model->su_get_order_price_info($order_id,$product_order_id);
      $productInfohtml = '';
      $pay_mode = '';
                $i = 0;
                $productInfohtml = '';

                  if(!empty($res)){
					foreach($res as $val){
                    $i++;
                   $productInfohtml =$productInfohtml.' <tr>
                      <td><img src="'. $this->config->item('sc_promotional_look_image').$val['image'].'" width="50" style="border:1px solid #cccccc; padding:2px; width:auto; height:auto; max-width:50px; max-height:50px;"></td>
                      <td>'. $val['name'].' </td>
                      <td>'. $val['product_qty'].' </td>
                      <td>'. $val['size_text'].'</td>
                      <td>'. $val['company_name'].' </td>
                      <td style="text-align:right; width:60px;">&#8377; '. $val['product_price'].'</td></tr>';

                  }
                }
                  $cod_amount = $brand_price[0]['cod_amount']>0 ? $brand_price[0]['cod_amount'] : 0 ;
                $shipping_amount = $brand_price[0]['shipping_amount']>0 ? $brand_price[0]['shipping_amount'] : 0 ;
                 
                 if($brand_price[0]['order_tax_amount'] > 0) {
                   $productInfohtml =$productInfohtml.' <tr>
                   <td colspan="5" style="padding:2px 8px; text-align:right;">Tax :</td>
                    <td style="padding:2px 8px; text-align:right;">
                      &#8377;  '.$brand_price[0]['order_tax_amount'].'
                    </td>
                  </tr>';
                  }
                  if($cod_amount > 0) {
                  $productInfohtml =$productInfohtml.'<tr>

                    <td colspan="5" style="padding:2px 8px; text-align:right;">COD Charges:</td>
                    
                    <td style="padding:2px 8px; text-align:right;">
                      &#8377;  '.$cod_amount.'
                    </td>
                  </tr>';
                }
                if($shipping_amount > 0){
                  $productInfohtml =$productInfohtml.'<tr>

                    <td colspan="5" style="padding:2px 8px; text-align:right;">Shipping Charges:
                    </td>
                    <td style="padding:2px 8px; text-align:right;">
                      &#8377;  '.$shipping_amount.'
                    </td>
                  </tr>';
                }
                if($brand_price[0]['pay_mode'] == 1){
                $pay_mode = '<p style="margin-bottom:20px;">
                  Payment Mode : <img src="https://www.stylecracker.com/assets/images/icons/ic-cod-1.png" style="vertical-align:middle;"> Cash on Delivery
                </p>';
                }

                if($brand_price[0]['pay_mode'] == 2){
                $pay_mode = '<p style="margin-bottom:20px;">
                  Payment Mode : <img src="https://www.stylecracker.com/assets/images/icons/ic-online-payment-1.png" style="vertical-align:middle;"> Online Payment
                </p>';
                }
              
			if($count_product >= 2 && $count_brand == 2){
				$total_amount = $val['product_price'];
			} else $total_amount = $brand_price[0]['order_total'];
 $productInfohtml =$productInfohtml.'</tbody>
      </table>
      <div style="text-align:right; background:#f3f3f3; padding:5px 30px; font-size:18px; text-transform:uppercase; margin-top:20px; border-bottom:2px solid #cccccc;">
        Total Amount  : &#8377; '.$total_amount.'
      </div>
    </td>
  </tr>';
           $productInfohtml =$productInfohtml.' <tr>
    <td style="padding:0px 30px;">
        <div style=" border-bottom:1px solid #ccc; padding-bottom:30px;">
        Thank you for shopping with <strong>StyleCracker</strong>.<br/>
For any query or assistance, feel free to <a href="https://www.stylecracker.com/schome/aboutus" style="color:#00b19c; text-decoration:none;">Contact Us</a>
      </div>
    </td>
  </tr>
  <tr>
    <td style="vertical-align:middle; padding:20px 30px;">
      <span style="color: #888; font-weight: bold; line-height: 1.3; position: relative;top: -4px; vertical-align:top;">  Follow Us:  </span>

      <a target="_blank" href="https://www.facebook.com/StyleCracker" target="_blank"  title="Facebook"><img src="https://www.stylecracker.com/assets/images/social-media/facebook.png" alt="Facebook" style="width:24px;"></a>
      <a target="_blank" href="https://twitter.com/Style_Cracker" target="_blank"  title="Twitter"><img src="https://www.stylecracker.com/assets/images/social-media/twitter.png" alt="Twitter" style="width:24px;"></a>
      <a target="_blank" href="https://www.youtube.com/user/StyleCrackerTV" target="_blank"  title="Youtube"><img src="https://www.stylecracker.com/assets/images/social-media/youtube.png"  alt="Youtube" style="width:24px;"></a>
      <a href="https://instagram.com/stylecracker" target="_blank" title="Instagram"><img src="https://www.stylecracker.com/assets/images/social-media/instagram.png"  alt="Instagram" style="width:24px;"></a>
      <a target="_blank" href="https://www.pinterest.com/stylecracker" target="_blank"  title="Pintrest"><img src="https://www.stylecracker.com/assets/images/social-media/pintrest.png" alt="Pintrest" style="width:24px;"></a>

          </td>
        </tr>
      </table>


          <table width="600" cellpadding="0" cellspacing="0" align="center" border="0" style="width:600px; background:#fff; border:1px solid #cccccc; margin-bottom:20px;">
        <tr>
          <td align="left" style="padding:20px 0 0 20px; width:160px;">
              <img src="https://www.stylecracker.com/assets/images/logo.png" style="width:150px; margin-bottom:5px;">
          <div>
            <a href="https://www.stylecracker.com" style="color:#00b19c; text-decoration:none;">www.stylecracker.com</a>
          </div>
          </td>
            <td align="left"  style="padding:20px 0 0; vertical-align:bottom;">
              <img src="https://www.stylecracker.com/assets/images/icons/ic-download-app-1.png">
            <td>

            </td>
          <td align="right"  style="padding:20px 20px  0 0; text-align:center; ">
            <div style="font-size:18px; margin-bottom:10px;">
              StyleCracker App is India\'s only
personalised styling platform.
            </div>

             <a href="https://play.google.com/store/apps/details?id=com.stylecracker.android" target="_blank" style="color:#333; text-decoration:none;">
                  <img src="https://www.stylecracker.com/assets/images/icons/en_generic_rgb_wo_45.png" style="height:30px;" target="_blank"></a>

            <a href="https://itunes.apple.com/us/app/stylecracker/id1061463812?mt=8&uo=4" target="_blank" style="color:#333; text-decoration:none; ">
              <img src="https://www.stylecracker.com/assets/images/icons/apple_store.png" style="height:30px;" target="_blank"></a>

               
          </td>
        </tr>
      </table>

    </body>
    </html>';


      $this->email->from('tech_support@stylecracker.com', 'StyleCracker: Order Cancelled');
      $this->email->to($this->getEmailAddress($user_id));
	  $cc_email = array('order@stylecracker.com',$this->getEmailAddress($val['brand_id']));
      $this->email->cc($cc_email);
	 // $this->email->bcc($this->getEmailAddress($val['brand_id']));
      $this->email->subject('StyleCracker: Order Cancelled');

      $this->email->message('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Order Summary</title>

</head>
<body style="background:#f3f3f3; color:#333333; text-align:left; font-family:Arial,Helvetica,sans-serif;
font-weight: normal;
line-height: 1.5;
font-size: 14px; padding:0; margin:0 auto;">
<table width="600" cellpadding="0" cellspacing="0" align="center" border="0" style="width:600px; background:#fff; margin-bottom:20px;">
  <tr>
          <td style="text-align:left; border-bottom:3px solid #00b19c; padding:20px;">

            <table cellpadding="0" cellspacing="0" align="center" border="0" style="width:100%; ">
              <tr>
                      <td style="text-align:left;">

            <img src="https://www.stylecracker.com/assets/images/logo.png" style="height:34px;">
  </td>
  <td style="text-align:right; vertical-align:middle; font-size:14px; color:#00b19c;">

  <a href="https://www.stylecracker.com/myOrders" style="color:#00b19c; text-decoration:none;">My Orders</a> 


          </td>
        </tr>
      </table>
      </td>
    </tr>

  <tr><td style="padding:30px;">

      <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
        <tr>
          <td align="center" style="padding:15px 0 30px; vertical-align:middle;">
             <span style="padding:8px 0; border-bottom:1px solid #cccccc; font-size:18px; text-transform:uppercase;">
               <img src="https://www.stylecracker.com/assets/images/icons/ic-order-processing-1.png" style="vertical-align:middle;"> Order Cancelled</span>
          </td>
        </tr>
        <tr>
          <td style="font-size:15px; line-height:1.7; ">
            <p style="margin-bottom:20px;">
            Hi <strong>'.$first_name.' '.$last_last.'</strong>,<br/><br/>
            We regret to inform you that the following orders have been cancelled by <strong style="color:#00B19C">'. $val['company_name'].'</strong>  because the product seems to be out of stock. <br><br/>
			We apologize for the inconvenience caused and hope to improve your experience in the future. If you have paid for the order online you will recieve your refund within 7-9 days from the day it is processed.<br/><br/>
			Please find below, the summary of your cancelled order.<br/>
            </p>
           
          </td>
        </tr>
      </table>
      <br/>
      <div style="text-transform:uppercase; font-weight:bold; line-height:1; font-size:16px; margin-bottom:20px;">
        Order Details:
      </div>

      <table width="100%" cellpadding="8" cellspacing="0" align="center" border="0" style="font-size:12px;">
        <tbody>
          <tr>
            <th style="background-color:#f3f3f3; font-size:14px;">Image</th>
            <th style="background-color:#f3f3f3; font-size:14px;">Product Details</th>
            <th style="background-color:#f3f3f3; font-size:14px;">Qty</th>
            <th style="background-color:#f3f3f3; font-size:14px;">Size</th>
            <th style="background-color:#f3f3f3; font-size:14px;">Seller</th>
            <th style="background-color:#f3f3f3; font-size:14px; width:60px; text-align:right;">Price</th>
          </tr>'.$productInfohtml);


      $this->email->send();
    }
  }
*/

public function exportExcel($param,$search_by=NULL,$table_search=NULL,$is_download=NULL)
  { $is_download = $this->uri->segment(6); 

    if($search_by == 6 )
    {
      $table_search_encode = base64_decode($table_search);
    }else if($search_by == 8)
    {
      $table_search_encode = base64_decode($table_search);
    }else
    {
      $table_search_encode = $table_search;
    }   
    //$is_download = $this->input->post('param');
        $fieldName = array('Date','Order Unique No','Order ID','Product SKU','Product Name','Company Name','Product Price','Payment Type','Order Status','Size','Quantity', 'Grand Total','Coupon Code','Username','Phone Number','Email','Shipping Address','City','State','Pincode','Look id','Platform','Category','Sub-Category','Variations','Variations Value');
    /*$fieldName = array('Date','Order ID','Product Name','Brand Name','Payment Type','Unique Reference Number','Transaction Id','Quantity','Product Amount','Brand Tax Amount','Shipping Charges','COD Charges','Grand Total','Rate Of Commission(%)','Commission Amount','PG cost @3.35%','Service Tax Amount(14.0%)','Swach Bharat Abhiyan(0.5%)','Total Commission Invoice','TDS deduction by Brand on commission','Net Income receivable from Brand','Brand Income','Brand Payout','Days Lapsed','Payment Status','Username','Phone Number','Email','Shipping Address','City','State','Pincode');*/
    $get_data = $this->View_orders_model->getViewOrdersExcel($search_by,$table_search_encode);
    /*echo '<pre>';print_r($get_data);*/
    $result_data = array();
    $i =0;
    $new_order_id = '';$old_order_id = '';

    foreach($get_data as $val) {   
            $result_data[$i][] = $val['created_datetime'];  
            $result_data[$i][] = $val['order_unique_no'];            
            $result_data[$i][] = $val['order_id'];
            $result_data[$i][] = $val['product_sku'];
            $result_data[$i][] = $val['product_name'];
            $result_data[$i][] = $val['company_name'];
            $result_data[$i][] = $val['product_price'];
            $brand_code ='';
            $product_price = 0;
            $commission_rate = 0;
            if($val['pay_mode']==1)
            {
              $result_data[$i][] = 'COD';
            }else
            {
              $result_data[$i][] = 'Online';
            }
            $date_ref_no = explode('-',$val['created_datetime']);
            
            if($val['brand_code']!='')
            {
              $brand_code = $val['brand_code'];
            }

            //$result_data[$i][] = $val['daysLapsed'];
            $order_status = "";
            if($val['order_status'] == 1)
            {
                $order_status = 'Processing';
            }else if($val['order_status'] == 2)
            {
                $order_status = 'Confirmed';
            }else if($val['order_status'] == 3)
            {
                $order_status = 'Dispatched';
            }else if($val['order_status'] == 4)
            {
                $order_status = 'Delivered';
            }else if($val['order_status'] == 5)
            {
                $order_status = 'Cancelled';
            }else if($val['order_status'] == 6)
            {
                $order_status = 'Return';
            }else if($val['order_status'] == 7)
            {
                $order_status = 'Fake';
            }

            $result_data[$i][] = $order_status;
          /*  if($val['product_size'] == 1){
                $result_data[$i][] = 'MEDIUM';
            }else if($val['product_size'] == 2){
                $result_data[$i][] = 'LARGE';
            }else if($val['product_size'] == 3){
                $result_data[$i][] = 'SMALL';
            }else if($val['product_size'] == 4){
                $result_data[$i][] = 'X-LARGE';
            }else if($val['product_size'] == 5){
                $result_data[$i][] = 'X-SMALL';
            }else if($val['product_size'] == 6){
                $result_data[$i][] = 'ONE SIZE';
            }else if($val['product_size'] == 7){
                $result_data[$i][] = 'M';
            }else if($val['product_size'] == 8){
                $result_data[$i][] = 'FREE';
            }else{
                $result_data[$i][] = "";
            }*/

            $result_data[$i][] = $val['size_text'];
            //$result_data[$i][] = $brand_code.'/'.$date_ref_no[1].'/'.$date_ref_no[0];
            if(isset($val['tranid']))
            {
              //$result_data[$i][] = $val['tranid'];
            }
            
            @$result_data[$i][] = $val['product_qty'];
            
            //$result_data[$i][] = $val['product_price'];
            //$result_data[$i][] = $val['order_tax_amount'];
            //$result_data[$i][] = $val['shipping_amount'];
            //$result_data[$i][] = $val['cod_amount'];
            /* Coupon Setting Page */   
                   
              if(isset($val['coupon_code']) && $val['coupon_code']!='')
              {
                $coupon_info = $this->View_orders_model->getUserCoupon($val['coupon_code'],$val['created_datetime'],$val['brand_id']);

                /*$data['coupon_discount'] = $coupon_info['coupon_amount'];
                $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];*/
				$data['coupon_discount'] = $coupon_info['coupon_amount'];
                $coupon_brand = $coupon_info['brand_id'];
                $coupon_products = $coupon_info['coupon_products'];
                if($coupon_products!='' || $coupon_products!=0)
                {
                  $coupon_products_arr =explode(',',$coupon_products);
                }
                $coupon_min_spend = $coupon_info['coupon_min_spend'];
                $coupon_max_spend = $coupon_info['coupon_max_spend'];
                $coupon_discount_type = $coupon_info['coupon_discount_type'];  
                $coupon_stylecracker =  $coupon_info['stylecrackerDiscount'];
                $individual_use_only =  $coupon_info['individual_use_only'];   
                $data['coupon_code'] = $val['coupon_code'];
                $coupon_product_price = 0;
                $price = $val['product_price'];

                 if($coupon_brand==$val['brand_id'] && $coupon_brand!=0)
                  {        
                    if($coupon_products!=''&& $coupon_products!=0)
                    {
                       //coupon_discount_type =3 (Product discount)
                      if($coupon_discount_type==3)
                      {
                        if(in_array($val['product_id'], $coupon_products_arr))
                        {              
                          if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                          {                        
                            $data['coupon_discount'] = $coupon_info['coupon_amount'];  
                            $data['coupon_code'] = $val['coupon_code'];                   
                          } 
                        }
                      } 
                       //coupon_discount_type =4 (Product % discount)
                      if($coupon_discount_type==4)
                      {
                        if(in_array($val['product_id'], $coupon_products_arr))
                        {              
                          if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                          {
                            $coupon_percent = $coupon_info['coupon_amount'];
                            $data['coupon_discount'] = $price*($coupon_percent/100); 
                            $data['coupon_code'] = $val['coupon_code'];                      
                          } 
                        }
                      }                 

                    }else
                    { 
                       //coupon_discount_type =1 (cart discount)
                      if($coupon_discount_type==1)
                      {
                        $coupon_product_price = $coupon_product_price+$price;
                        if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                        {
                          $data['coupon_discount'] = $coupon_info['coupon_amount']; 
                          $data['coupon_code'] = $val['coupon_code'];                   
                        }
                      }
                       //coupon_discount_type =2 (cart % discount)
                      if($coupon_discount_type==2)
                      {
                        $coupon_product_price = $coupon_product_price+$price;
                        if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                        {
                          $coupon_percent = $coupon_info['coupon_amount'];  
                          $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                          $data['coupon_code'] = $val['coupon_code'];                      
                        }
                      }
                    }          
                  }

                if(($coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only!=1  )|| ( $coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only==1 ))
                { 
                    //coupon_discount_type =1 (cart discount)
                     $coupDiscountPrdPrice = $this->View_orders_model->calculate_product_discountAmt($val['OrderIdProductSum'],$val['OrderProductTotal'],$coupon_discount_type,$coupon_info['coupon_amount']);
                    
                        if($coupon_discount_type==1)
                        {
                         /* $coupon_product_price = $coupon_product_price+$price;*/
                          $coupon_product_price = $val['OrderProductTotal'];                         
                          if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                          {    
                            $data['coupon_discount'] = round($coupDiscountPrdPrice) ;
                            $data['coupon_code'] = $val['coupon_code'];                   
                          }
                        }
                      //coupon_discount_type =2 (cart % discount)
                        if($coupon_discount_type==2)
                        {
                          //$coupon_product_price = $coupon_product_price+$price;
                          $coupon_product_price = $val['OrderProductTotal'];
                          if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                          {
                            $coupon_percent = $coupon_info['coupon_amount'];  
                            $data['coupon_discount'] = round($coupDiscountPrdPrice);
                            $data['coupon_code'] = $val['coupon_code'];                      
                          }
                        }
                }                            

              }else
              {
                $data['coupon_discount']=0;
                $data['coupon_code'] = '';
              }         
                   
           
/* Coupon Setting Page End */
            if($old_order_id != $val['order_id'])
            {
              $result_data[$i][] = $val['order_total']-$data['coupon_discount'];
            }else
            {
              $result_data[$i][] = '';
            }
            $old_order_id = $val['order_id'];
            //$result_data[$i][] = $val['commission_rate'];
            $result_data[$i][] = $val['coupon_code'];  
            if(@$val['product_price']!='')
            {
              //$product_price = $val['product_price'];
            }

            if(@$val['commission_rate']!='')
            {
              $commission_rate = $val['commission_rate'];
            }
            $comm_amt =  $product_price*($commission_rate/100);
            //$result_data[$i][] = $comm_amt;
            $pgCost=0;
            if($val['pay_mode']!=1)
            {
              $pgCost = $comm_amt * (3.35/100);
            }
           
            //$result_data[$i][] = round($pgCost,2);

            $service_tax = $comm_amt+$pgCost*(14.00/100);
            //$result_data[$i][] = round($service_tax,2);

            $swachba = ($comm_amt+$pgCost)*(0.50/100);
            //$result_data[$i][] = round($swachba,2);

            $total_comm_invoice = $comm_amt+$service_tax+$swachba;
            //$result_data[$i][] = round($total_comm_invoice,2);

            $tds_deduction = ($comm_amt+$pgCost)*(10/100);
            //$result_data[$i][] =  round($tds_deduction,2);

            $netIncome_rec_brand =  $total_comm_invoice-$tds_deduction;
            //$result_data[$i][] = round($netIncome_rec_brand,2);

            $brand_income = $val['order_total']-$total_comm_invoice;
            //$result_data[$i][] = round($brand_income,2);

            $brand_payout =  $tds_deduction+$brand_income;
            //$result_data[$i][] = round($brand_payout,2);

            //$result_data[$i][] = $val['daysLapsed'];
            if($val['payment_status'] == 1)
            {
              $pay_status = 'Success';
            }else if($val['payment_status'] == 2)
            {
              $pay_status = 'Cleared';
            }else
            {
              $pay_status = 'Pending';
            }
            //$result_data[$i][] = $pay_status;


            $result_data[$i][] = $val['first_name']." ".$val['last_name'];
            $result_data[$i][] = $val['mobile_no'];
            $result_data[$i][] = $val['email_id'];
            $result_data[$i][] = trim($val['shipping_address']);
            $result_data[$i][] = trim($val['city_name']);
            $result_data[$i][] = trim($val['state_name']);
            $result_data[$i][] = $val['pincode'];
            $result_data[$i][] = $val['look_id'];   
            $result_data[$i][] = $val['platform_info'];
            $result_data[$i][] = $val['product_cat_id'];
            $result_data[$i][] = $val['product_sub_cat_id'];
            $result_data[$i][] = $this->product_variations($val['id']);
            $result_data[$i][] = $this->product_variations_value($val['id']);
            
            $i++;
        }
        //echo "<pre>";print_r($result_data);exit;
       // echo $param.' '.$is_download;
    if($param == 'downloadExcel' && $is_download == 1)
        {         
        //========================Excel Download==============================
          //$excel_result = $this->writeDataintoCSV($fieldName,$get_data,'view_order_payment');
         $this->writeDataintoCSV($fieldName,$result_data,'view_order_payment');
         //========================End Excel Download==========================

           
        }
              
  }

  public function product_variations($product_id){
      $product_variations = $this->View_orders_model->product_variations($product_id);
      $product_variations_html = ''; $i = 0;
      if(!empty($product_variations)){
        foreach($product_variations as $val){
          if($i != 0) { $product_variations_html = $product_variations_html.','; } 
          $product_variations_html = $product_variations_html.$val['name'];
        $i++;
        }
      }
      return $product_variations_html;
  }

    public function product_variations_value($product_id){
      $product_variations = $this->View_orders_model->product_variations_value($product_id);
      $product_variations_html = ''; $i = 0;
      if(!empty($product_variations)){
        foreach($product_variations as $val){
          if($i != 0) { $product_variations_html = $product_variations_html.','; } 
          $product_variations_html = $product_variations_html.$val['name'];
        $i++;
        }
      }
      return $product_variations_html;
  } 

}

?>