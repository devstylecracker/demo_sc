<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacts extends MY_Controller {	
	
	function __construct(){
       parent::__construct();
       $this->load->model('databases/contacts_model','contacts_model'); 
      
   } 
   
	public function index($offset = null)
	{ 
		$data_post = array();
		if(!$this->session->userdata('user_id'))
		{
			$this->load->view('login');

		}else{ 			
		
			$data_post = array();
			$search_by = $this->input->post('search_by');
            $table_search = $this->input->post('table_search');
            $paginationUrl = 'databases/Contacts/index/';
            $uri_segment = 4;
            $config['per_page'] = 20;
            $config['total_rows'] = count($this->contacts_model->get_contact($search_by,$table_search,'',''));
            $total_rows = $config['total_rows'];
            $contact_details = $this->contacts_model->get_contact($search_by,$table_search,$offset,$config['per_page']);            
            $count = $config['total_rows'];
            $data_post['contact_details'] = $contact_details;

            $this->customPagination($paginationUrl,$count,$uri_segment,$contact_details,$total_rows);

			$this->load->view('common/header');
			$this->load->view('databases/contacts_view',$data_post);
			$this->load->view('common/footer');
		}
	}
	
	
	public function contacts_add(){	
		
		if($this->has_rights(58) == 1){

			$data_post = array();
			
			$data_post['contact_type'] = $this->contacts_model->get_contacttype(); 

			if($this->input->post())
			{
				
				$this->form_validation->set_rules('contact_name', 'Contact Name', 'required|prep_for_form');				
				$this->form_validation->set_rules('gender', 'Gender', 'required|prep_for_form');
				$this->form_validation->set_rules('city', 'City', 'prep_for_form');
				$this->form_validation->set_rules('state', 'State', 'prep_for_form');				
				$this->form_validation->set_rules('country', 'Country', 'prep_for_form');
				$this->form_validation->set_rules('pincode', 'Pincode', 'is_natural|exact_length[6]');
				$this->form_validation->set_rules('company', 'Company');
				$this->form_validation->set_rules('mobile1', 'Mobile1', 'required|is_natural|exact_length[10]');
				$this->form_validation->set_rules('mobile2', 'Mobile2', 'is_natural|exact_length[10]');
				$this->form_validation->set_rules('phone1', 'Phone1', 'is_natural|max_length[15]');
				$this->form_validation->set_rules('phone2', 'Phone2', 'is_natural|max_length[15]');
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
				$this->form_validation->set_rules('altemail', 'Alternate Email', 'valid_email');				
				$this->form_validation->set_rules('fax', 'Fax', 'is_natural|prep_for_form');
				$this->form_validation->set_rules('contacttype', 'Contact Type', 'required|prep_for_form');
									
				
				if ($this->form_validation->run() == FALSE)
				{ 
					$this->load->view('common/header');
					$this->load->view('databases/contacts_add');
					$this->load->view('common/footer');

				}else
				{
									
						$contact_name= $this->input->post('contact_name');
						$gender= $this->input->post('gender');
						$city= $this->input->post('city');
						$state= $this->input->post('state');
						$country= $this->input->post('country');
						$pincode= $this->input->post('pincode');
						$company= $this->input->post('company');
						$mobile_one= $this->input->post('mobile1');
						$designation= $this->input->post('designation');
						$mobile_two= $this->input->post('mobile2');
						$phone_one= $this->input->post('phone1');
						$phone_two= $this->input->post('phone2');						
						$email= $this->input->post('email');
						$altemail= $this->input->post('altemail');
						$fax= $this->input->post('fax');						
						$contacttype= $this->input->post('contacttype');
						
					
					$data_post['contact_info'] = array(
							'contact_name'=>addslashes(strip_tags(trim($contact_name))),
							'gender'=>$gender,
							'city'=>addslashes(strip_tags(trim($city))),
							'state'=>addslashes(strip_tags(trim($state))),
							'country'=>addslashes(strip_tags(trim($country))),
							'pincode'=>addslashes(strip_tags(trim($pincode))),
							'company'=>addslashes(strip_tags(trim($company))),
							'designation'=>addslashes(strip_tags(trim($designation))),
							'mobile_one'=>$mobile_one,
							'mobile_two'=>$mobile_two,
							'phone_one'=>$phone_one,
							'phone_two'=>$phone_two,
							'email'=>addslashes(strip_tags(trim($email))),
							'altemail'=>addslashes(strip_tags(trim($altemail))),
							'fax'=>$fax,
							'contacttype'=>addslashes(strip_tags(trim($contacttype))),
							'created_by'=>$this->session->userdata('user_id'),
							'modified_by'=>$this->session->userdata('user_id'),
							'created_datetime'=>date('Y-m-d H:i:s')						
						);
						
						
					$result = $this->contacts_model->add_new_contact($data_post['contact_info']); 
					
					if($result)
					{

						 $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Contact added successfully!</div>');
						redirect('databases/contacts/');

					}else
					{
						$this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5> The server is not responding, try again later.</div>');
						redirect('databases/contacts/contacts_add');
					}					
					
				}

			}else{
			
				$this->load->view('common/header',$data_post);
				$this->load->view('databases/contacts_add');
				$this->load->view('common/footer');
			}
		}else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
				
		}
	
	}


	public function contacts_edit($contactId){	
		
		if($this->has_rights(58) == 1){

			$data_post = array();
			
			$contactId = trim($contactId);

			$data_post['contact_type'] = $this->contacts_model->get_contact_info($contactId); 
			
			if($this->input->post())
			{				
				$this->form_validation->set_rules('contact_name', 'Contact Name', 'required|prep_for_form');				
				$this->form_validation->set_rules('gender', 'Gender', 'required|prep_for_form');
				$this->form_validation->set_rules('city', 'City', 'prep_for_form');
				$this->form_validation->set_rules('state', 'State', 'prep_for_form');				
				$this->form_validation->set_rules('country', 'Country', 'prep_for_form');
				$this->form_validation->set_rules('pincode', 'Pincode', 'is_natural|exact_length[6]');
				$this->form_validation->set_rules('company', 'Company');
				$this->form_validation->set_rules('mobile1', 'Mobile1', 'required|is_natural|exact_length[10]');
				$this->form_validation->set_rules('mobile2', 'Mobile2', 'is_natural|exact_length[10]');
				$this->form_validation->set_rules('phone1', 'Phone1', 'is_natural|max_length[15]');
				$this->form_validation->set_rules('phone2', 'Phone2', 'is_natural|max_length[15]');
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
				$this->form_validation->set_rules('altemail', 'Alternate Email', 'valid_email');				
				$this->form_validation->set_rules('fax', 'Fax', 'is_natural|prep_for_form');
				$this->form_validation->set_rules('contacttype', 'Contact Type', 'required|prep_for_form');
									
				
				if ($this->form_validation->run() == FALSE)
				{ 
					$this->load->view('common/header');
					$this->load->view('databases/contacts_add');
					$this->load->view('common/footer');

				}else
				{									
					$contact_name= $this->input->post('contact_name');
					$gender= $this->input->post('gender');
					$city= $this->input->post('city');
					$state= $this->input->post('state');
					$country= $this->input->post('country');
					$pincode= $this->input->post('pincode');
					$company= $this->input->post('company');
					$designation= $this->input->post('designation');
					$mobile_one= $this->input->post('mobile1');
					$mobile_two= $this->input->post('mobile2');
					$phone_one= $this->input->post('phone1');
					$phone_two= $this->input->post('phone2');						
					$email= $this->input->post('email');
					$altemail= $this->input->post('altemail');
					$fax= $this->input->post('fax');						
					$contacttype= $this->input->post('contacttype');
						
					
					$data_post['contact_info'] = array(
							'contact_name'=>addslashes(strip_tags(trim($contact_name))),
							'gender'=>$gender,
							'city'=>addslashes(strip_tags(trim($city))),
							'state'=>addslashes(strip_tags(trim($state))),
							'country'=>addslashes(strip_tags(trim($country))),
							'pincode'=>addslashes(strip_tags(trim($pincode))),
							'company'=>addslashes(strip_tags(trim($company))),
							'designation'=>addslashes(strip_tags(trim($designation))),
							'mobile_one'=>$mobile_one,
							'mobile_two'=>$mobile_two,
							'phone_one'=>$phone_one,
							'phone_two'=>$phone_two,
							'email'=>addslashes(strip_tags(trim($email))),
							'altemail'=>addslashes(strip_tags(trim($altemail))),
							'fax'=>$fax,
							'contacttype'=>addslashes(strip_tags(trim($contacttype))),
							'created_by'=>$this->session->userdata('user_id'),
							'modified_by'=>$this->session->userdata('user_id'),
							'created_datetime'=>date('Y-m-d H:i:s')						
						);
						
						
					$result = $this->contacts_model->edit_contact($data_post['contact_info'],$contactId); 
					
					if($result)
					{
						 $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Contact updated successfully!</div>');
						redirect('databases/contacts/');

					}else
					{
						$this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5> The server is not responding, try again later.</div>');
						redirect('databases/contacts/contacts_add');
					}					
					
				}

			}else{
			
				$this->load->view('common/header',$data_post);
				$this->load->view('databases/contacts_edit');
				$this->load->view('common/footer');
			}

		}else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');				
		}
	
	}
		
	
	 function contacts_delete(){	   
			
			$contact_id = $this->input->post('contactID');

			$result = $this->contacts_model->delete_contact($contact_id,$this->session->userdata('user_id'));			

			 if($result)
	        {
	            $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Record has been deleted successfully</div>');
	            redirect('/databases/contacts');
	        }
	        else
	        {
	            $this->session->set_flashdata('feedback', '<div class="alert alert-danger alert-dismissable"><h5><i class="icon fa fa-ban"></i><strong>Error!</strong></h5>The server is not responding, try again later.</div>');
	            redirect('/databases/contacts');
	        }
			
		}

	
}
