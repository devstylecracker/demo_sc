<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dbimageupload extends MY_Controller {    

    function __construct(){
        parent::__construct();
        $this->load->model('databases/Dbimageupload_model','Dbimageupload_model'); 
        $this->load->model('productnew_model');      
        $this->load->model('databases/Category_model','Category_model');      

    }  
 /*---------Category submodule functionalities----------*/

    public function index($offset = null)
    {   
      
        if(!$this->session->userdata('user_id')){

            $this->load->view('login');                    

        }else{ 
			
				/*All Data Show*/			
				$data_post = array();                       
	            if($this->has_rights(58) == 1){	
				    
				    $data_post['dbcategory'] =  $this->Dbimageupload_model->get_dbCategories(); 
				    $data_post['dbseason'] =  $this->Dbimageupload_model->get_dbSeason();
				    $data_post['dbdesigner'] =  $this->Dbimageupload_model->get_dbdesigner();
				    $data_post['style'] = $this->productnew_model->load_questAns(2);
				    $data_post['database_pic'] =  $this->Dbimageupload_model->get_database_pic();  
				    
				   /* $paginationUrl = 'databases/DbImageupload/index/';
	                $uri_segment = 3;
	                $config['per_page'] = 20;
	                $config['total_rows'] = count($this->Imageupload_model->get_street_pic('','','',''));                
	                $total_rows = $config['total_rows'];                                                    
	                $data['street_pic'] = $this->Imageupload_model->get_street_pic('','',$offset,$config['per_page']);

				    $this->customPagination($paginationUrl,$config['total_rows'],$uri_segment,$data['street_pic'],$total_rows);*/
				    
				    $this->load->view('common/header');
					$this->load->view('databases/database_image_upload',$data_post);
					$this->load->view('common/footer');
				}else{
					$this->load->view('common/header');
					$this->load->view('not_permission');
					$this->load->view('common/footer');
				}        
			}
	}	

	public function upload_images(){
		if(!$this->session->userdata('user_id')){
			
            $this->load->view('login');                    

        }else{ 
			 if($this->has_rights(58) == 1){	

			 	$data_post = array();
			 	$category_id = $this->input->post('category_id');
			 	$season = '';
			 	$year = '';
			 	$trend_name = '';
			 	$event_name = '';
			 	$type = '';
			 	$designer = '';
			 	$reference = '';
			 	$personality_style = '';
			 	$tags = '';
			 	$db_catdata_id = '';
			 	$resultimg = '';

			 	$cat_info = $this->Category_model->get_category_info($category_id);			 	
			 	$files = $_FILES;			 
			 	$dataarray = array();			 	
			 	$postdata = $this->input->post();
			 	
			 	//$dbcat= $this->Dbimageupload_model->get_dbCategoriesbyname($postdata['category']);
			 	//$this->form_validation->set_rules('avatar','','callback_is_image|prep_for_form');
				$this->form_validation->set_rules('category_id','Category','required');
				//$this->form_validation->set_rules('season','Season','required');
				//$this->form_validation->set_rules('year','Year','required');
				//$this->form_validation->set_rules('designer','Designer','required');
				//$this->form_validation->set_rules('lookbooks-prod','Type','required');
				//$this->form_validation->set_rules('trendname','Trend','required');
				//$this->form_validation->set_rules('tags','Tags','required');


				if ($this->form_validation->run() == FALSE)
				{
					$data_post['dbcategory'] =  $this->Dbimageupload_model->get_dbCategories(); 
				    $data_post['dbseason'] =  $this->Dbimageupload_model->get_dbSeason();
				    $data_post['dbdesigner'] =  $this->Dbimageupload_model->get_dbdesigner();
				    $data_post['style'] = $this->productnew_model->load_questAns(2);

					$this->load->view('common/header');
					$this->load->view('databases/database_image_upload',$data_post);
					$this->load->view('common/footer');
			        $ok =0;

			     }else
			     {			    
			 	 
				 	if($category_id)
				 	{
				 		if($cat_info[0]['show_season'] == 1 && $cat_info[0]['show_season_multi'] == 1)
				 		{				 			
				 			$season = $postdata['season'];
				 		}

				 		if($cat_info[0]['show_year'] == 1)
				 		{				 			
				 			$year = $postdata['year'];
				 		}

				 		if($cat_info[0]['show_trend'] == 1){
				 			
				 			$trend_name= $postdata['trendname'];
				 		}

				 		if($cat_info[0]['show_events'] == 1){
				 			
				 			$event_name = $postdata['eventname'];
				 		}

				 		if($cat_info[0]['show_type'] == 1){
				 			
				 			$type = $postdata['lookbooks-prod'];
				 		}

				 		if($cat_info[0]['show_designer'] == 1  && $cat_info[0]['show_designer_multi'] == 1 ){
				 			
				 			$designer = $postdata['designer'];
				 		}

				 		if($cat_info[0]['refrence_show'] == 1 ){
				 			
				 			$reference = $postdata['ref-style'];
				 			if($postdata['ref-style'] == 'Personality')
				 			{
				 				$personality_style = $postdata['style'];
				 			}

				 		}

				 		$tags = $postdata['tags'];

				 		$data_post['data'] = array(
													'category_id' => $category_id,
													'season' => $season,
													'year' => $year,
													'trend_name' => $trend_name,
													'type' => $type,
													'event_name' => $event_name,
													'designer' => $designer,
													'reference' => $reference,
													'personality_style' => $personality_style,
													'tags' => $tags,
													'created_by' => $this->session->userdata('user_id'),
													'created_datetime'=> date("Y-m-d H:i:s")   
									);

				 		$db_catdata_id = $this->Dbimageupload_model->insert_dbcat_data($data_post); 

				 	}
				 	
				
					$files = $_FILES;
					
					$count = count($_FILES['avatar']['name']);
					$output = [];
					for($i=0; $i<$count; $i++)
					{ 
						$_FILES['avatar']['name']= $files['avatar']['name'][$i];
						$_FILES['avatar']['type']= $files['avatar']['type'][$i];
						$_FILES['avatar']['tmp_name']= $files['avatar']['tmp_name'][$i];
						$_FILES['avatar']['error']= $files['avatar']['error'][$i];
						$_FILES['avatar']['size']= $files['avatar']['size'][$i];    
						$ext = pathinfo($files['avatar']['name'][$i], PATHINFO_EXTENSION);
						$img_name = time().mt_rand(0,10000).'.'.$ext;
						$pathAndName = "assets/databases_images/".$img_name;
						// Run the move_uploaded_file() function here
						$moveResult = move_uploaded_file($files['avatar']['tmp_name'][$i], $pathAndName);

						//$image_name= $this->upload->data('file_name');

						$config['image_library'] = 'gd2';
						$config['source_image'] = './assets/databases_images/'.$img_name;
						$config['new_image'] = './assets/databases_images/thumb/';
						$config['create_thumb'] = TRUE;
						$config['thumb_marker'] = '';
						$config['maintain_ratio'] = TRUE;
						$config['width'] = 200;
						$config['height'] = 200;

						$this->image_lib->initialize($config);

						if (!$this->image_lib->resize())
						{
							$this->image_lib->display_errors();
							$ok =0;

						}else{

							$ok =1;
						}

						$data_post['data_images'] = array(
								//'db_catdata_id' => $category_id,
								'cat_id' => $category_id,
								'image_name' => $img_name,												
								'created_by' => $this->session->userdata('user_id'),
								'created_datetime'=> date("Y-m-d H:i:s")   
							);
							
						$resultimg = $this->Dbimageupload_model->insert_image_data($db_catdata_id,$data_post); 	

	                 }

                  //echo json_encode($output);
	                if($resultimg){
						$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Images Uploaded Successfully</div>');
						redirect('/Databases/Dbimageupload/');

					}else{

						$this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>Something goes wrong Please try again</div>');
						redirect('/Databases/Dbimageupload/');

					}
                }

			 }else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			 }   
		}
	}

	public function Dbimagecatalog($offset=NULL)
	{
		//echo "Image search controller";

        if(!$this->session->userdata('user_id')){

            $this->load->view('login');                    

        }else{ 
			
				/*All Data Show*/			
				$data_post = array();                       
	            if($this->has_rights(58) == 1){	
				    $data = array(); $dataa = array();

				    $image_categories = '';
				    $season = '';
				    $year = '';
				    $trendname = '';
				    $eventname = '';
				    $lookbooks_prod = '';
				    $designer = '';
				    $ref_style = '';
				    $style = '';
				    $tag_search = '';

				    $data_post['dbcategory'] =  $this->Dbimageupload_model->get_dbCategories();  
				    $data_post['dbseason'] =  $this->Dbimageupload_model->get_dbSeason();
				    $data_post['dbdesigner'] =  $this->Dbimageupload_model->get_dbdesigner();
				    $data_post['style'] = $this->productnew_model->load_questAns(2);

				   
				    $data['db_pic'] =  $this->Dbimageupload_model->get_database_img('','','');  

				    if($this->input->post()){ 
				    	$image_categories = $this->input->post('image_categories');
				    	$season = $this->input->post('season');
				    	$year = $this->input->post('year');
				    	$trendname = $this->input->post('trendname');
				    	$eventname = $this->input->post('eventname');
				    	$lookbooks_prod = $this->input->post('lookbooks-prod');
				    	$designer = $this->input->post('designer');
				    	$ref_style = $this->input->post('ref-style');
				    	$style = $this->input->post('style');
				    	$tag_search = $this->input->post('tag_search');

				    	$dataa['image_categories'] =  $image_categories; 
				    	$dataa['season'] =  $season; 
				    	$dataa['year'] =  $year; 
				    	$dataa['trendname'] =  $trendname; 
				    	$dataa['eventname'] =  $eventname; 
				    	$dataa['lookbooks_prod'] =  $lookbooks_prod; 
				    	$dataa['designer'] =  $designer; 
				    	$dataa['ref_style'] =  $ref_style; 
				    	$dataa['style'] =  $style; 
				    	$dataa['tag_search'] =  $tag_search; 

				    }

				    
				    
				    $paginationUrl = 'databases/Dbimageupload/Dbimagecatalog/';
	                $uri_segment = 4;
	                $config['per_page'] = 20;
	                $config['total_rows'] = count($this->Dbimageupload_model->get_database_img('','',''));                
	              	$total_rows = count($this->Dbimageupload_model->get_database_img('','',''));                                                
	                $data_post['db_pic'] = $this->Dbimageupload_model->get_database_img($dataa,$offset,$config['per_page']);

				    $this->customPagination($paginationUrl,$total_rows,$uri_segment,$data['db_pic'],$total_rows);
				    
				    $this->load->view('common/header');
					$this->load->view('databases/database_image_catalog',$data_post);
					$this->load->view('common/footer');
				}else{
					$this->load->view('common/header');
					$this->load->view('not_permission');
					$this->load->view('common/footer');
				}        
			}
	}
	
	  private function set_upload_options()
	    {   

	        $config = array();
	        $config['upload_path'] = base_url().'assets/street_img/';
	        $config['allowed_types'] = 'gif|jpg|png|jepg';
	        $config['overwrite']     = FALSE;
	        return $config;
	    }

	 public function delete_database_pic(){
			$del_id = $this->input->post('cat_del_id');			
			$this->Dbimageupload_model->delete_database_pic($del_id,$this->session->userdata('user_id'));  
		 }

	public function get_category_details(){
		$category_id = $this->input->post('category_id');
		$cathtml = ''; $seasonhtml = ''; $designerhtml = ''; $stylehtml = '';
		if($category_id){
			$cat_info = $this->Category_model->get_category_info($category_id);
			$dbseason =  $this->Dbimageupload_model->get_dbSeason();
			$dbdesigner = $this->Dbimageupload_model->get_dbdesigner();
			$style = $this->productnew_model->load_questAns(2);

			if(!empty($dbseason)){
				$seasonhtml = $seasonhtml."<option value=''>Select</option>";
				foreach($dbseason as $val){
					$seasonhtml = $seasonhtml."<option value='".$val['id']."'>".$val['season_name']."</option>";
				}
			}

			if(!empty($dbdesigner)){
				$designerhtml = $designerhtml."<option value=''>Select</option>";
				foreach($dbdesigner as $val){
					$designerhtml = $designerhtml."<option value='".$val['id']."'>".$val['contact_name']."</option>";
				}
			}

			if(!empty($style)){
				$stylehtml = $stylehtml."<option value=''>Select</option>";
				foreach($style as $val){
					$stylehtml = $stylehtml."<option value='".$val['id']."'>".$val['answer']."</option>";
				}
			}

			$cathtml = $cathtml."<div class='form-group'><div class='checkbox-group'><div class='cat-wrp'>";
			$cathtml = $cathtml."<div class='form-group'><div class='checkbox-group'><div class='cat-wrp'>";
			$cathtml = $cathtml."<h4>".$cat_info[0]['category_name']."</h4> <div class='row'>";
			

			if($cat_info[0]['show_season'] == 1 && $cat_info[0]['show_season_multi'] == 1){
			$cathtml = $cathtml."<div class='col-md-6'>
            <div class='form-group'>
                <label class='' for='exampleInputEmail1'>Seasons</label><div class='input-group'>
                    <select class='form-control' name='season' id='season' mutiple>
                    ".$seasonhtml."
                    </select>
                     <span class='text-red'> ".form_error('category_id')." </span>
                    </div>
			</div></div>";
			}			

			if($cat_info[0]['show_year'] == 1){
			$cathtml = $cathtml."<div class='col-md-6'>
                                  <div class='form-group'>
                                    <label for='exampleYear'>Year</label>
                                      <input type='text' class='form-control datepicker' name='year' id='year' readonly id='datepicker' onclick='adddatepicker();''>
                                  </div>
                                </div>";
			}

			if($cat_info[0]['show_trend'] == 1){
			$cathtml = $cathtml." <div class='col-md-6'>
                                    <div class='form-group'>
                                      <label for='exampletrendname'>Trend Name</label>
                                      <input type='text' class='form-control' id='trendname' name='trendname' placeholder='Enter Trend Name'>
                                      <span class='text-red'> ".form_error('trendname')." </span>
                                    </div>
                                   </div>";
			}

			if($cat_info[0]['show_events'] == 1){
			$cathtml = $cathtml." <div class='col-md-6'>
                                        <div class='form-group'>
                                          <label for='exampleeventname'>Event Name</label>
                                          <input type='text' name='eventname' placeholder='Enter Event Name' id='eventname' class='form-control'>
                                          <span class='text-red'> ".form_error('eventname')." </span>
                                        </div>
                                      </div>";
			}


			if($cat_info[0]['show_type'] == 1){
			$cathtml = $cathtml." <div class='col-md-6'>
                                    <label for='exampleType'>Type</label>
                                    <div class='row'>
                                      <div class='col-md-6'>
                                        <div class='checkbox'>
                                          <label>
                                            <input type='radio' name='lookbooks-prod' value='accessories' checked> Accessories
                                          </label>
                                        </div>
                                      </div>
                                      <div class='col-md-6'>
                                        <div class='checkbox'>
                                          <label>
                                            <input type='radio' name='lookbooks-prod' id='lookbooks-prod' value='clothing' > Clothing
                                          </label>
                                        </div>
                                      </div>
                                    </div>
                                    <span class='text-red'> ".form_error('lookbooks-prod')." </span>
                                  </div>";
			}

			if($cat_info[0]['show_designer'] == 1  && $cat_info[0]['show_designer_multi'] == 1 ){
			$cathtml = $cathtml."<div class='col-md-6'>
						            <div class='form-group'>
						                <label class='' for='exampleInputEmail1'>Designer</label><div class='input-group'>
						                    <select class='form-control'  id='designer' name='designer' mutiple>
						                    ".$designerhtml."
						                    </select>
						                    <span class='text-red'> ".form_error('designer')." </span>
						                    </div>
									</div></div>";
			}

			if($cat_info[0]['refrence_show'] == 1 ){
			$cathtml = $cathtml."<div class='col-md-6' id='image-upload-ref'>
                                  <!--label for='exampleReferences'>References</label-->
                                  <div class='row'>
                                    <div class='col-md-6'>
                                      <div class='checkbox'>
                                        <label>
                                          <input type='radio'  name='ref-style'  class='ref-celebstyle' value='Celeb Style' checked> Celeb Style
                                        </label>
                                      </div>
                                    </div>
                                    <div class='col-md-6'>
                                      <div class='checkbox'>
                                        <label>
                                          <input type='radio' name='ref-style' class='ref-personality' id='ref-personality' onchange='addstyle();' value='Personality' > Personality
                                        </label>                                        
                                      </div>
                                    </div>
                                  </div> 
                                  <div class='ref-personality-content'>
                                    <div class='row'>
                                      <div class='col-md-6'>
                                        <div class='form-group'>
                                          <label for='examplePersonalityStyle'> Personality Style</label>
                                          <select name='style' id='style' class='form-control'>
                                           ".$stylehtml."                             
                                          </select>
                                          <span class='text-red'> ".form_error('style')." </span>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                              </div>";
			}
			
			$cathtml = $cathtml."</div></div></div></div>";				
		}


		echo $cathtml;

	}

	public function show_database_images()
	{
		$category_id = $this->input->post('category_id');
		$imghtml = ''; $imagedata = '';
		$database_pic =  $this->Dbimageupload_model->get_database_pic($category_id); 

		 if(!empty($database_pic)) 
		 { 

             foreach($database_pic as $val) 
             { 
             	 $imagedata = $imagedata."<li>
	                <img src='".base_url()."/assets/databases_images/thumb/".$val['image_name']."' >
	                <i class='fa fa-times-circle-o' onclick='delete_img(".$val['id'].")'></i>
	              </li>";
             } 
		 } 

		$imghtml  =" <ul>".$imagedata."</ul>";

		echo $imghtml;
		
	}

	public function get_search_options(){
		$category_id = $this->input->post('category_id');
		$cathtml = ''; $seasonhtml = ''; $designerhtml = ''; $stylehtml = ''; $yearhtml = '';
		if($category_id){
			$cat_info = $this->Category_model->get_category_info($category_id);
			$dbseason =  $this->Dbimageupload_model->get_dbSeason();
			$dbdesigner = $this->Dbimageupload_model->get_dbdesigner();
			$dbyear = $this->Dbimageupload_model->get_year();
			$style = $this->productnew_model->load_questAns(2);

			if(!empty($dbseason)){
				$seasonhtml = $seasonhtml."<option value=''>Season</option>";
				foreach($dbseason as $val){
					$seasonhtml = $seasonhtml."<option value='".$val['id']."'>".$val['season_name']."</option>";
				}
			}

			if(!empty($dbdesigner)){
				$designerhtml = $designerhtml."<option value=''>Designer</option>";
				foreach($dbdesigner as $val){
					$designerhtml = $designerhtml."<option value='".$val['id']."'>".$val['contact_name']."</option>";
				}
			}

			if(!empty($style)){
				$stylehtml = $stylehtml."<option value=''>Style</option>";
				foreach($style as $val){
					$stylehtml = $stylehtml."<option value='".$val['id']."'>".$val['answer']."</option>";
				}
			}

			if(!empty($dbyear)){
				$yearhtml = $yearhtml."<option value=''>Year</option>";
				foreach($dbyear as $val){
					$yearhtml = $yearhtml."<option value='".$val['year']."'>".$val['year']."</option>";
				}
			}

			$cathtml = $cathtml."<div class='form-group'><div class='checkbox-group'><div class='cat-wrp'>";
			$cathtml = $cathtml."<div class='form-group'><div class='checkbox-group'><div class='cat-wrp'>";
			$cathtml = $cathtml."<div class='row'>";
			

			if($cat_info[0]['show_season'] == 1){
			$cathtml = $cathtml."<div class='col-md-6'>
            <div class='form-group'>
                <div class='input-group'>
                    <select class='form-control' name='season' id='season'>
                    ".$seasonhtml."
                    </select>
                     <span class='text-red'> ".form_error('category_id')." </span>
                    </div>
			</div></div>";
			}			

			if($cat_info[0]['show_year'] == 1){
			$cathtml = $cathtml."<div class='col-md-6'>
                                  <div class='form-group'>
                                   <select class='form-control' name='year' id='year'>
                    					".$yearhtml."
                    				</select>   
                                  </div>
                                </div>";
			}

			if($cat_info[0]['show_trend'] == 1){
			$cathtml = $cathtml." <div class='col-md-6'>
                                    <div class='form-group'>
                                      <input type='text' class='form-control' id='trendname' name='trendname' placeholder='Enter Trend Name'>
                                      <span class='text-red'> ".form_error('trendname')." </span>
                                    </div>
                                   </div>";
			}

			if($cat_info[0]['show_events'] == 1){
			$cathtml = $cathtml." <div class='col-md-6'>
                                        <div class='form-group'>
                                          <input type='text' name='eventname' placeholder='Enter Event Name' id='eventname' class='form-control'>
                                          <span class='text-red'> ".form_error('eventname')." </span>
                                        </div>
                                      </div>";
			}


			if($cat_info[0]['show_type'] == 1){
			$cathtml = $cathtml." <div class='col-md-6'>
                                    <label for='exampleType'>Type</label>
                                    <div class='row'>
                                      <div class='col-md-6'>
                                        <select class='form-control' name='lookbooks-prod' id='lookbooks-prod'>
                                       		<option value=''>Select</option>
                                       		<option value='accessories'>Accessories</option>
                                       		<option value='clothing'>Clothing</option>
                                       </select>
                                      </div>
                                    </div>
                                    <span class='text-red'> ".form_error('lookbooks-prod')." </span>
                                  </div>";
			}

			if($cat_info[0]['show_designer'] == 1){
			$cathtml = $cathtml."<div class='col-md-6'>
						            <div class='form-group'>
						                <div class='input-group'>
						                    <select class='form-control'  id='designer' name='designer' mutiple>
						                    ".$designerhtml."
						                    </select>
						                    <span class='text-red'> ".form_error('designer')." </span>
						                    </div>
									</div></div>";
			}

			if($cat_info[0]['refrence_show'] == 1 ){
			$cathtml = $cathtml."<div class='col-md-6' id='image-upload-ref'>
                                  <!--label for='exampleReferences'>References</label-->
                                  <div class='row'>
                                    <div class='col-md-6'>
                                     <select class='form-control' name='ref-style' id='ref-style'>
                                       		<option value=''>Select</option>
                                       		<option value='Celeb Style'>Celeb Style</option>
                                       		<option value='Personality'>Personality</option>
                                       </select>
                                    </div>
                                  </div> 
                                  <div class='ref-personality-content'>
                                    <div class='row'>
                                      <div class='col-md-6'>
                                        <div class='form-group'>
                                          <select name='style' id='style' class='form-control'>
                                           ".$stylehtml."                             
                                          </select>
                                          <span class='text-red'> ".form_error('style')." </span>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                              </div>";
			}
			
			$cathtml = $cathtml."</div></div></div></div>";				
		}


		echo $cathtml;
	}
	    
/*----------End of Common functions------------------------------------*/	
}


                            
                            

                              