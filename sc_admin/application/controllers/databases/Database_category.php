<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Database_category extends MY_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
       parent::__construct();
 	   $this->load->model('databases/category_model','category_model'); 
   } 
   
	public function index($offset = null)
	{  
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{ 
			$search_by ='';
			$table_search ='';

			$data_post = array();
			if($this->input->post()){
				$search_by = $this->input->post('search_by');
            	$table_search = $this->input->post('table_search');
        	}
            $paginationUrl = 'databases/Database_category/index/';
            $uri_segment = 4;
            $config['per_page'] = 20;
            $config['total_rows'] = count($this->category_model->get_category($search_by,$table_search,'',''));
            $total_rows = $config['total_rows'];
            $category_details = $this->category_model->get_category($search_by,$table_search,$offset,$config['per_page']);            
            $count = $config['total_rows'];
            $data_post['category_details'] = $category_details;

            $this->customPagination($paginationUrl,$count,$uri_segment,$category_details,$total_rows);

			$this->load->view('common/header');
			$this->load->view('databases/category',$data_post);
			$this->load->view('common/footer');

		}	
	}

	public function add_category(){
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{ 


			if($this->input->post()){
					
					$this->form_validation->set_rules('category_name', 'Category Name', 'required|is_unique[database_category.category_name]');

					if ($this->form_validation->run() == FALSE)
                    {				
						$this->load->view('common/header');
						$this->load->view('databases/add_category');
						$this->load->view('common/footer');				
					}else
					{
						$category_name = $this->input->post('category_name');
						$show_season = $this->input->post('show_season');
						$show_season_multi = $this->input->post('show_season_multi');
						$show_year = $this->input->post('show_year');
						$show_trend = $this->input->post('show_trend');
						$show_events = $this->input->post('show_events');
						$show_type = $this->input->post('show_type');
						$show_designer = $this->input->post('show_designer');
						$show_designer_multi = $this->input->post('show_designer_multi');
						$refrence_show = $this->input->post('refrence_show');

						$data = array();
						$data['category_info'] = array(
									'category_name' => $category_name,
									'show_season' => $show_season,
									'show_season_multi' => $show_season_multi,
									'show_year' => $show_year,
									'show_trend' => $show_trend,
									'show_events' => $show_events,
									'show_type' => $show_type,
									'show_designer' => $show_designer,
									'show_designer_multi' => $show_designer_multi,
									'refrence_show' => $refrence_show,
									'created_by' => $this->session->userdata('user_id'),
									'created_datetime' => date('Y-m-d H:i:s')
							); 

						$result = $this->category_model->add_new_category($data);
						if($result){
							$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Contact added successfully!</div>');
							redirect('databases/Database_category/');
						}else{
							$this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5> The server is not responding, try again later.</div>');
							redirect('databases/Database_category');
						}
					} 		
			}else{

			$this->load->view('common/header');
			$this->load->view('databases/add_category');
			$this->load->view('common/footer');
			}
		}	
	}
	public function edit_category(){
		
		if(!$this->session->userdata('user_id'))
		{
			$this->load->view('login');

		}else{
			$data = array();
			$cat_id = $this->uri->segment(4);
			$cat_details = $this->category_model->get_category_info($cat_id);
			$data['cat_details'] = $cat_details;

			if($this->input->post()){
			$this->form_validation->set_rules('category_name', 'Category Name', 'required');

					if ($this->form_validation->run() == FALSE)
                    {				
						$this->load->view('common/header');
						$this->load->view('databases/add_category');
						$this->load->view('common/footer');				
					}else
					{
						$category_name = $this->input->post('category_name');
						$show_season = $this->input->post('show_season');
						$show_season_multi = $this->input->post('show_season_multi');
						$show_year = $this->input->post('show_year');
						$show_trend = $this->input->post('show_trend');
						$show_events = $this->input->post('show_events');
						$show_type = $this->input->post('show_type');
						$show_designer = $this->input->post('show_designer');
						$show_designer_multi = $this->input->post('show_designer_multi');
						$refrence_show = $this->input->post('refrence_show');

						$data = array();
						$data['category_info'] = array(
									'category_name' => $category_name,
									'show_season' => $show_season,
									'show_season_multi' => $show_season_multi,
									'show_year' => $show_year,
									'show_trend' => $show_trend,
									'show_events' => $show_events,
									'show_type' => $show_type,
									'show_designer' => $show_designer,
									'show_designer_multi' => $show_designer_multi,
									'refrence_show' => $refrence_show,
									'created_by' => $this->session->userdata('user_id'),
									'created_datetime' => date('Y-m-d H:i:s')
							); 
						$data['cat_id'] = $cat_id;
						$result = $this->category_model->edit_category($data['category_info'],$data['cat_id']);
						if($result){
							$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Contact added successfully!</div>');
							redirect('databases/Database_category/');
						}else{
							$this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5> The server is not responding, try again later.</div>');
							redirect('databases/Database_category');
						}
					} 
				}
			$this->load->view('common/header');
			$this->load->view('databases/edit_category',$data);
			$this->load->view('common/footer');

		}
	}

	public function view_category(){
		if(!$this->session->userdata('user_id'))
		{
			$this->load->view('login');

		}else{
			$data = array();
			$cat_id = $this->uri->segment(4);
			$cat_details = $this->category_model->get_category_info($cat_id);
			$data['cat_details'] = $cat_details;
			$this->load->view('common/header');
			$this->load->view('databases/view_category',$data);
			$this->load->view('common/footer');
		}
	}

	public function delete_category(){
		$data = array();
	    $user_id = $this->input->post('id');
	    $data_ext_id = $this->input->post('data_ext_id');
	    
		$this->category_model->delete_user($user_id,$this->session->userdata('user_id'),$data_ext_id);
		return true;
	}
}
