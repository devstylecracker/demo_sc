<?php

class Product_upload_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
   

	/*-- Uploading Product Data through Excel Sheet to the database --*/	

	function product_upload($data_post,$table)
	{	
		if(!empty($data_post))
		{
			//$this->db->insert_batch('product_desc', $data_post)
			try
			{
				if($this->db->insert('product_desc', $data_post)){

					$product_insert_id = $this->db->insert_id();

						/* Add Product Tags*/
						if(!empty($table['tags'])){						

							foreach($table['tags'] as $tag){
								$tags = array();
								$tags = array(
										'product_id'=>$product_insert_id,
										'tag_id' => $tag,
										'status' => 1,
										'created_by' =>$this->session->userdata('user_id'),
										'created_datetime' => date('Y-m-d H:i:s')
									);
								$this->db->insert('product_tag', $tags); 
							}

						}

						/* Add Product Store / Product Availibility */

						if(!empty($table['store'])){							

							foreach($table['store'] as $store){
								$stores = array();
								$stores = array(
										'product_id'=>$product_insert_id,
										'store_id' => $store,
										'status' => 1
									);
								$this->db->insert('product_store', $stores);
							}
						}

						/* Add Product Variations type and value */
						$vartypeid = $table['vartype'];
						$variation = $table['var'];						

						if(!empty($table['var'])){

							for($i = 0; $i < sizeof($variation); $i++){	
									$valtype = array();
									$valtype = array(
										'product_id'=>$product_insert_id,
										'product_var_type_id' => $vartypeid[$i],
										'product_var_val_id' => $variation[$i],
										'deflag' => 0
									);
									$this->db->insert('product_variations', $valtype); 	
							}				

						}
					return true;

				}else{			

					return false;
					throw new Exception("Error Processing Request", 1);					

				}
				//return false;
			}catch (Exception $e)
        	{	
        		print_r($e->getMessage());         		

        	}	
		}
 	} 	

 	function getall_subcategories()
 	{
		$query = $this->db->get_where('product_sub_category', array('status' => 1,'is_delete' => 0));
		$result = $query->result_array();
		return $result;
	}

	function getall_variation()
 	{
			$query = $this->db->get_where('product_variation_value', array('status' => 1, 'is_delete' => 0));
			$result = $query->result_array();
			return $result;
	}
	/* Added by saylee */
	function getall_cat_rel_variation($cat_id,$sub_cat_id){
			$query = "SELECT a.`name`, a.`id`,a.`product_variation_type_id` FROM product_variation_value AS a, product_variation_type AS b, `product_sub_category` AS c, `product_category` AS d WHERE a.`product_variation_type_id` = b.`id` 
			AND b.`prod_sub_cat_id` = c.`id` 
			AND c.`product_cat_id` = d.`id` and LOWER(d.`name`)='".trim(strtolower($cat_id))."' and LOWER(c.`name`)='".trim(strtolower($sub_cat_id))."' and a.is_delete = 0 and b.is_delete = 0";
			 
			$countRes = $this->db->query($query);
			$result = $countRes->result_array();
			return $result;
	}
	/* End by saylee */

	public function checkMultilpeProduct($name)
	{
		$catQuery = "select count(name) as count from product_desc where name LIKE '".$name."' and is_delete = 0";
		$countRes = $this->db->query($catQuery);
		$rowRes = $countRes->row();	
		return $rowRes;

	}//this function checks for same product_name while adding new category 

	
	function get_subcatbycat($catid)
 	{
		$query = $this->db->get_where('product_sub_category', array('status' => 1,'is_delete' => 0,'product_cat_id' =>$catid ));
		$result = $query->result_array();			
		return $result;
	}

	function get_parentCatId($cat)
 	{
		$query = $this->db->get_where('category', array('category_name' => $cat,'category_parent' => -1 ));
		$result = $query->result_array();			
		return $result[0]['id'];
	}

	function get_childCatId($catId,$childCat)
	{
		//$query = $this->db->get_where('category', array('category_name' => $childCat ));
		if($childCat!='')
		{
			$cat_query = $this->db->query("select id  from category where category_name ='".$childCat."'");
			$result = $cat_query->result_array();				
			
			foreach ($result as $key => $value) {
				$query_hier = $this->db->query("select GetCat(".$value['id'].") as category_hierarchy");
				$result_hier = $query_hier->result_array();				
				$category_array = explode(',',$result_hier[0]['category_hierarchy']);				
				if(in_array($catId, $category_array))
				{
					$chilCatId = $value['id'];
				}
			}

			return $chilCatId;
		}else
		{
			return 0;
		}
	}

	function get_attributeValueId($attrName)
	{
		$attrName = trim($attrName);
		if($attrName!='')
		{
			$attr_query = $this->db->query("select id from attribute_values_relationship where attr_value_name ='".$attrName."'");
			$result = $attr_query->result_array();		
		}
		
		if(!empty($result))
		{
			return $result[0]['id'];
		}else
		{
			return '';
		}
	}

	function get_allAttributeValue()
	{		
		$attr_query = $this->db->query("select id,attr_value_name from attribute_values_relationship ");
		$result = $attr_query->result_array();	
		return $result[0]['id'];
	}

	function update_product($data){

		if(!empty($data['pro_update'])){
            $this->db->update_batch('product_desc', $data['pro_update'],'id'); 
        }
        if(isset($data['sql_stock_inventory_update']) && $data['sql_stock_inventory_update'] != ""){
        	foreach ($data['sql_stock_inventory_update'] as $value) {
        		# code...
        		$this->db->query(substr($value, 0,-2));
        	}
        }
        //$this->db->last_query();
	}
	
    function add_product($data){
		$this->db->trans_start();
	        $this->db->insert_batch('product_desc', $data['pro_add']); 
	        $this->db->trans_complete();

	        if(isset($data['sql_product_category_add']) && $data['sql_product_category_add'] != ""){
	        	foreach ($data['sql_product_category_add'] as $value) {
	        		# code...
	        		$this->db->query(substr($value, 0,-2));
	        	}
	        }
	        if(isset($data['sql_extra_img']) && $data['sql_extra_img'] != ""){
	        	foreach ($data['sql_extra_img'] as $value) {
	        		# code...
	        		$this->db->query(substr($value, 0,-2));
	        	}
	        }

	        
	        if(isset($data['sql_stock_inventory_add']) && $data['sql_stock_inventory_add'] != ""){
	        	foreach ($data['sql_stock_inventory_add'] as $value) {
	        		# code...
	        		$this->db->query(substr($value, 0,-2));
	        	}
	        }
	        	print_r($data);

			if(isset($data['sql_product_tag_update']) && $data['sql_product_tag_update'] != ""){
	        	foreach ($data['sql_product_tag_update'] as $value) {
	        		# code...
	        		$this->db->query(substr($value, 0,-2));
	        	}
	        }


			if(isset($data['pa_query']) && $data['pa_query'] != ""){
	        	foreach ($data['pa_query'] as $value) {
	        		# code...
	        		$this->db->query(substr($value, 0,-2));
        		}
	        }



			if(isset($data['attribute_query']) && $data['attribute_query'] != ""){
	        	foreach ($data['attribute_query'] as $value) {
	        		# code...
	        		$this->db->query(substr($value, 0,-2));
	        	}
	        }
	}
	
 } 

?>