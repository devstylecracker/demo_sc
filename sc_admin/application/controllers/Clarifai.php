<?php
ini_set('memory_limit', '-1');
defined('BASEPATH') OR exit('No direct script access allowed');

class Clarifai extends MY_Controller {

	function __construct(){
       parent::__construct();
       //$this->load->model('Mailchimp_model');
       $this->load->library('email');
       $this->config->load('clarifai');
       $this->load->model('Clarifai_model');
       //$this->load->model('Product_personalisation_model');
       
   	}

   	function index(){ 

   		//$result = $this->Product_personalisation_model->show_gender_specific_personalisation(1);
   		//echo '<pre>';print_r($result);
   		echo 'Clarifai'; exit;
   	}

   	function getAccessToken() 
   	{   
   		$apiKey = '';		
	    //ac--scmailchimp01
		$client_id = $this->config->item('clarifai_client_id');
	    $client_secret = $this->config->item('clarifai_client_secret');
	    $authorization = $this->config->item('clarifai_authorization');
	    //  https://{client_id}:{client_secret}@api.clarifai.com/v1/token/
	   // $url = 'https://'.$client_id.':'.$client_secret.'@api.clarifai.com/v1/token/' . $memberId;   
	    $url = 'https://api.clarifai.com/v1/token/';
	
		$header = array(					    
					    'Content-Type: application/x-www-form-urlencoded'
					    );

	    $json = json_encode([
			        'client_id' => $this->config->item('clarifai_client_id'),
			        'client_secret' => $this->config->item('clarifai_client_secret'),	      
			        'grant_type'=>'client_credentials'
			    ]);    
	  
	     //'grant_type'=>'client_credentials'
	    $ch = curl_init($url);


	    //curl_setopt($ch, CURLOPT_USERPWD, '5L5G0O1qxawunTtG_nAIF_ch6TPVmawcA3fJU4U0:3j4B83fG3WNkIHvI7NSyPFFklCtFQ7LDJgOGQKBc');
	     //curl_setopt($ch, CURLOPT_USERPWD, 'sudhags147@gmail.com: sudha123');
	    //curl_setopt($ch, CURLOPT_HTTPAUTH, '5L5G0O1qxawunTtG_nAIF_ch6TPVmawcA3fJU4U0:3j4B83fG3WNkIHvI7NSyPFFklCtFQ7LDJgOGQKBc');
	    //curl_setopt($ch, CURLOPT_HTTPHEADER, 'Content-Type: application/x-www-form-urlencoded');
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	    //curl_setopt($ch, CURLOPT_POST, true);	  
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);                                                                    

	    $result = curl_exec($ch);
	    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    if(curl_exec($ch) === false)
	    {
	        echo 'Curl error: ' . curl_error($ch);
	    }else
	    {
	        echo 'Operation completed without any errors';
	        echo '<pre>';print_r($result);
	    }
	    curl_close($ch);

	    //return $httpCode;
	}

	function sendImage()
	{
		$apiKey = '';$data = ''; $productImgUrl = '';	
	    //ac--scmailchimp01
		$client_id = $this->config->item('clarifai_client_id');
	    $client_secret = $this->config->item('clarifai_client_secret');
	    $authorization = $this->config->item('clarifai_authorization');	  
	    $url = 'https://api.clarifai.com/v2/inputs';
	    //https://api.clarifai.com/v2/models/{model_id}/inputs

	    $header = array(
					     $this->config->item('clarifai_authorization'),
					    'Content-Type: application/json'
					    );

	  $concepts_bodyshape = array();
	   $metadata = array();$concepts_style = array();
	   $bucketCat_array = array();$attr_array = array();
	   

	   $productImg_url = $this->config->item('product_url');
	   $product_result = $this->Clarifai_model->get_products();  
	   //$this->config->item('product_url')
	   //echo '<pre>';print_r($product_result);
	   $count = sizeof($product_result);
	   foreach($product_result as $val)
	   {
	   	  $concepts = array();
	   	 $concepts3 = array();
	   	 $personality_result = array();
	   	 $metadata['product_id'] = $val['id'];
	   	 $metadata['image_url'] = $val['image'];
	   	 $metadata['product_name'] = $val['name'];
	   	 $metadata['brand_name'] = $val['brand'];
	   	 //$metadata['price'] = $val['price'];

	   	 $productImgUrl = $this->config->item('product_url').$val['image'];	
	   	 
	   	 $attributes = $this->Clarifai_model->get_attribute_id($val['id']);
	   	 $attr_exp = explode(',',$attributes);
	   	 $category_result = $this->Clarifai_model->get_category_id($val['id']);
	   	  //echo '<pre>';print_r($category_result);
	   	  $categoryparent_result = $this->Clarifai_model->get_parent_cat($category_result['cat_id']);
	   	 //echo '<pre>';print_r($categoryparent_result);exit;	   
	   	  $i=0;

		   	foreach($categoryparent_result as $val)
		   	{
		   		//echo $val['category_id'];
		   		$bucketCat_array = $this->Clarifai_model->get_bucketByCategory($val['category_id']);
		   		// echo '<pre>';print_r($bucketCat_array);
		   		if(!empty($bucketCat_array))
		   		{
		   			foreach($bucketCat_array as $val)
		   			{
		   				if($val['attribute_value_ids']!='')
		   				{
			   				$attr_array = unserialize($val['attribute_value_ids']);
			   				//echo '<pre>';print_r($attr_array);
			   				foreach($attr_exp as $v)
			   				{
			   					if(in_array($v, @$attr_array))
			   					{
			   						echo $val['object_value'];
			   						$personality_result[$i]['bucket'] = $val['object_value'];
			   						$personality_result[$i]['body_shape'] = $val['body_shape'];
			   						$personality_result[$i]['style'] = $val['style'];
			   						$personality_result[$i]['category_id'] = $val['object_id'];
			   					}else
			   					{
			   						echo 'Not Found';
			   					}
			   				}
			   				$i++;
			   			}
		   				
		   				//echo '<pre>';print_r($attr_array);exit;
		   			}
		   			break;
		   		}
		   	}
		   	//echo '<pre>';print_r($personality_result);exit;
		   	if(empty($personality_result))
		   	{
		   		$personality_result = $this->Clarifai_model->get_bucketByCategory($category_result['cat_id']);
		   	}
		   	//$bucketCat_array = $this->Clarifai_model->get_bucketByCategory($category_result['cat_id']);
		   	//echo '<pre>bucket==';print_r($bucketCat_array);
		   	//$personality_result = $this->Clarifai_model->show_gender_specific_personalisation(1);
		   	//$personality_result1 = array_map("unserialize", array_unique(array_map("serialize", $personality_result)));
			  //echo '<pre>';print_r($personality_result);exit;
		    foreach($personality_result as $val)
		    {
		   		//$concepts_bodyshape[] = array("id"=>$val['style'],"value"=>true);
		   		$concepts_style[] = array("id"=>$val['style'],"value"=>true);	   		
		    }	
		    $concepts_bs = array_map("unserialize", array_unique(array_map("serialize", $concepts_bodyshape)));
		    $concepts_s = array_map("unserialize", array_unique(array_map("serialize", $concepts_style)));
		     //echo '<pre>';print_r($concepts_bodyshape); 
		    //  foreach($concepts_bs as $val)
		    // {
		   	// 	$concepts3[] = array("id"=>$val['id'],"value"=>true);		   			
		    // }
		     foreach($concepts_s as $val)
		    {
		   		$concepts3[] = array("id"=>$val['id'],"value"=>true);		   			
		    }
		    //echo '<pre>';print_r($concepts3);		    
		   
		   	$metadata_json = json_encode($metadata);
			   //echo $metadata_json;
		   //echo '<pre>';print_r($product_result);exit; 
		   //echo '<pre>';print_r($concepts);exit;

		   $concepts_json = json_encode($concepts3); 
		  
		   //"allow_duplicate_url": true
		   if($concepts_json!='')
		   {
		   		 $data = '{
				    "inputs": [		      
				      {
				        "data": {
				          "image": {
							        "url": "'.$productImgUrl.'"
							       },
				          "concepts":'.$concepts_json.',
				          "metadata":'.$metadata_json.'				         
				        }
				      }
				    ]		   
				  }';
		   }else
		   {
		   		$data ='';
		   }		  

			
			//echo $data;

			if(!empty($data))
		  	{
			     //'grant_type'=>'client_credentials'	  	
			    $ch = curl_init($url);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			    //curl_setopt($ch, CURLOPT_POST, true);	  
			    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);                                                                    

			    $result = curl_exec($ch);
			    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			    if(curl_exec($ch) === false)
			    {
			        echo 'Curl error: ' . curl_error($ch);
			    }
			    else
			    {
			        echo 'Operation completed without any errors';
			        echo '<pre>';print_r($result);
			    }
			    curl_close($ch);
			    //return $httpCode;
			}else
			{
				 echo 'Operation not complete since data is empty';
			}
	   }	  	
	}

	function sendWomenBodyShape()
	{
		$apiKey = '';$data = ''; $productImgUrl = '';
		$client_id = $this->config->item('clarifai_client_id');
	    $client_secret = $this->config->item('clarifai_client_secret');
	    $authorization = $this->config->item('clarifai_authorization');
	    
	    //$url = 'https://api.clarifai.com/v2/models/'.$this->config->item('womenbodyshape_modelid').'/inputs';
	     $url = 'https://api.clarifai.com/v2/inputs';
	    $header = array(
					     $this->config->item('clarifai_authorization'),
					    'Content-Type: application/json'
					    );  

	   $concepts = array();$concepts_bodyshape = array();
	   $metadata = array();$concepts_style = array();
	   $bucketCat_array = array();
	   $concepts3 = array();

	   $productImg_url = $this->config->item('product_url');
	   $product_result = $this->Clarifai_model->get_products();  
	   echo '<pre>';print_r($product_result);exit;	   
	   foreach($product_result as $val)
	   {
	   	 $metadata['product_id'] = $val['id'];
	   	 $metadata['image_name'] = $val['image'];
	   	 $metadata['product_name'] = $val['name'];
	   	 $metadata['brand_name'] = $val['brand'];
	   	 //$metadata['price'] = $val['price'];

	   	 $productImgUrl = $this->config->item('product_url').$val['image'];	

	   	 $category_result = $this->Clarifai_model->get_category_id($val['id']);
	   	  //echo '<pre>';print_r($category_result);	   
	   	  $categoryparent_result = $this->Clarifai_model->get_parent_cat($category_result['cat_id']);
	   	 //echo '<pre>';print_r($categoryparent_result);
		   	foreach($categoryparent_result as $val)
		   	{
		   		$bucketCat_array = $this->Clarifai_model->get_bucketByCategory($val['category_id']);
		   		if(!empty($bucketCat_array))
		   		{
		   			break;
		   		}
		   	}

		   	$bucketCat_array = $this->Clarifai_model->get_bucketByCategory($category_result['cat_id']);
		   	echo '<pre>bucket==';print_r($bucketCat_array);
		   	$personality_result = $this->Clarifai_model->show_gender_specific_personalisation(1);
		   	//$personality_result1 = array_map("unserialize", array_unique(array_map("serialize", $personality_result)));
			 echo '<pre>';print_r($personality_result1);exit;
		    foreach($personality_result as $val)
		    {
		   		$concepts_bodyshape[] = array("id"=>$val['body_shape'],"value"=>true);
		   		$concepts_style[] = array("id"=>$val['style'],"value"=>true);	   		
		    }	
		    $concepts_bs = array_map("unserialize", array_unique(array_map("serialize", $concepts_bodyshape)));    
		     
		     foreach($concepts_bs as $val)
		    {
		   		$concepts3[] = array("id"=>$val['id'],"value"=>true);		   			
		    }
		    //$concepts_s = array_map("unserialize", array_unique(array_map("serialize", $concepts_style)));
		    //  foreach($concepts_s as $val)
		    // {
		   	// 	$concepts3[] = array("id"=>$val['id'],"value"=>true);		   			
		    // }
		    //echo '<pre>';print_r($concepts3);
		    
		   $metadata_json = json_encode($metadata);
			 
		   $concepts_json = json_encode($concepts3); 
		   //echo $concepts_json; 
		  
		   //"allow_duplicate_url": true
		   if($concepts_json!='')
		   {
		   		 $data = '{
				    "inputs": [		      
				      {
				        "data": {
				          "image": {
							        "url": "'.$productImgUrl.'"
							       },
				          "concepts":'.$concepts_json.',
				          "metadata":'.$metadata_json.'				         
				        }
				      }
				    ]		   
				  }';
		   }else
		   {
		   		$data ='';
		   }
		  
		   echo $data;

			if(!empty($data))
		  	{
			    
			    $ch = curl_init($url);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			    //curl_setopt($ch, CURLOPT_POST, true);	  
			    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);                                                                    

			    $result = curl_exec($ch);
			    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			    if(curl_exec($ch) === false)
			    {
			        echo 'Curl error: ' . curl_error($ch);
			    }
			    else
			    {
			        echo 'Operation completed without any errors';
			        echo '<pre>';print_r($result);
			    }
			    curl_close($ch);
			    //return $httpCode;
			}else
			{
				 echo 'Operation not complete since data is empty';
			}
	   }	  	
	}

	function removeConcepts()
	{
		$apiKey = '';$data = ''; $productImgUrl = '';
		$client_id = $this->config->item('clarifai_client_id');
	    $client_secret = $this->config->item('clarifai_client_secret');
	    $authorization = $this->config->item('clarifai_authorization');
	    
	    //$url = 'https://api.clarifai.com/v2/models/'.$this->config->item('womenbodyshape_modelid').'/inputs';
	     $url = 'https://api.clarifai.com/v2/models/';
	    $header = array(
					     $this->config->item('clarifai_authorization'),
					    'Content-Type: application/json'
					    );  
		 $data = '{
				    "models": [
				      {
				        "id": "{model_id}",
				        "output_info": {
				          "data": {
				            "concepts": [
				              {
				                "id": "dogs"
				              }
				            ]
				          }
				        }
				      }
				    ],
				    "action": "merge"
				  }';
		  
		  
		   echo $data;

			if(!empty($data))
		  	{
			    
			    $ch = curl_init($url);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			    //curl_setopt($ch, CURLOPT_POST, true);	  
			    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);                                                                    

			    $result = curl_exec($ch);
			    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			    if(curl_exec($ch) === false)
			    {
			        echo 'Curl error: ' . curl_error($ch);
			    }
			    else
			    {
			        echo 'Operation completed without any errors';
			        echo '<pre>';print_r($result);
			    }
			    curl_close($ch);
			    //return $httpCode;
			}else
			{
				 echo 'Operation not complete since data is empty';
			}
	   }	

	function predictConcepts()
	{
		$apiKey = '';$data = ''; $productImgUrl = '';
		$client_id = $this->config->item('clarifai_client_id');
	    $client_secret = $this->config->item('clarifai_client_secret');
	    $authorization = $this->config->item('clarifai_authorization');
	    
	    //$url = 'https://api.clarifai.com/v2/models/'.$this->config->item('womenbodyshape_modelid').'/inputs';
	     $url = 'https://api.clarifai.com/v2/models/sc-model/outputs';
	    $header = array(
					     $this->config->item('clarifai_authorization'),
					    'Content-Type: application/json'
					    );  
		 $data = '{
				    "inputs": [
				      {
				        "data": {
				          "image": {
				            "url": "https://www.stylecracker.com/sc_admin/assets/products/14714388287175.png"
				          }
				        }
				      }
				    ]
				  }';
		  
		  
		   echo $data;

			if(!empty($data))
		  	{
			    
			    $ch = curl_init($url);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			    //curl_setopt($ch, CURLOPT_POST, true);	  
			    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);                                                                    

			    $result = curl_exec($ch);
			    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			    if(curl_exec($ch) === false)
			    {
			        echo 'Curl error: ' . curl_error($ch);
			    }
			    else
			    {
			        echo 'Operation completed without any errors';
			        echo '<pre>';print_r($result);
			    }
			    curl_close($ch);
			    //return $httpCode;
			}else
			{
				 echo 'Operation not complete since data is empty';
			}
	   }	  	  	

}

 ?>