<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
       parent::__construct();
       $this->load->model('users_model');    
      
   } 
   
	public function index($offset = null)
	{ 
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{ 
			$data = array();
			
			if($this->has_rights(11) == 1){
				$search_by = $this->input->post('search_by');
				$table_search = $this->input->post('table_search');
				
				$config['base_url'] = base_url().'users/index/';
				$config['total_rows'] = count($this->users_model->get_allusers($search_by,$table_search,'',''));
				$config['per_page'] = 20;
				$config['uri_segment'] = 3;
				$config['use_page_numbers'] = TRUE;
				$config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
				$config['full_tag_close'] = '</ul></div>';

				$config['first_link'] = '&laquo; First';
				$config['first_tag_open'] = '<li>';
				$config['first_tag_close'] = '</li>';

				$config['last_link'] = 'Last &raquo;';
				$config['last_tag_open'] = '<li>';
				$config['last_tag_close'] = '</li>';

				$config['next_link'] = 'Next &rarr;';
				$config['next_tag_open'] = '<li>';
				$config['next_tag_close'] = '</li>';

				$config['prev_link'] = '&larr; Previous';
				$config['prev_tag_open'] = '<li>';
				$config['prev_tag_close'] = '</li>';

				$config['cur_tag_open'] = '<li><a href="">';
				$config['cur_tag_close'] = '</a></li>';

				$config['num_tag_open'] = '<li>';
				$config['num_tag_close'] = '</li>';

				
				$this->pagination->initialize($config); 
				$get_users = $this->users_model->get_allusers($search_by,$table_search,$offset,$config['per_page']);
				$data['users_data'] = $get_users;
				$data['total_rows'] = $config['total_rows'];
				$this->load->view('common/header',$data);
				$this->load->view('users');
				$this->load->view('common/footer');
			}else{
				$this->load->view('common/header',$data);
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			}
		}
	}
	
	public function assign_permission(){
			$data = array();
		
			if($this->has_rights(11) == 1){
				$user_id =  $this->uri->segment(3);
				$data['menu_permissionss'] = $this->user_pass_permission($user_id);
				$data['modules_data'] = $this->users_model->get_all_modules();
				$data['edit_user'] = $user_id;
				
				$this->load->view('common/header',$data);
				$this->load->view('assign_permissions');
				$this->load->view('common/footer');
				
			}else{
				$this->load->view('common/header',$data);
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			}
		}
	public function add_permissions(){
		if($this->has_rights(10) == 1){
			$this->form_validation->set_rules('user_id', 'User Id', 'required');
			if ($this->form_validation->run() == FALSE)
			{
				$this->assign_permission();
			}
			else
			{
				$all_per = $this->input->post('assign_per');
				$user_id = $this->input->post('user_id');
				$login_user_id = $this->session->userdata('user_id');
				if($this->users_model->add_permissions($all_per,$user_id,$login_user_id)){
					$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Successfully permissions assigned to the user </div>');
					redirect('/users');
				}else{
					$this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>Sorry something went wrong</div>');
					redirect('/users');
				}
				 
			}
		}
	}
	
	public function user_add(){
		 $data = array();
	    
	     $data['roles'] = $this->users_model->get_roles(); 
		if($this->has_rights(12) == 1){
			
			if($this->input->post()){
				$this->form_validation->set_rules('user_type', 'User type', 'required');
				$this->form_validation->set_rules('first_name', 'First name', 'required');
				if($this->input->post('user_type') != 6) $this->form_validation->set_rules('last_name', 'Last name', 'required');
				$this->form_validation->set_rules('username', 'Username', 'required|is_unique[user_login.user_name]');
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[user_login.email]');
				$this->form_validation->set_rules('password', 'Password', 'required|matches[confirm_password]|min_length[8]|max_length[20]');
				$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|min_length[8]|max_length[20]');
				
				if ($this->form_validation->run() == FALSE)
				{
					$this->load->view('common/header',$data);
					$this->load->view('user_add');
					$this->load->view('common/footer');
				}else{
					
					$user_type = $this->input->post('user_type');
					$first_name = $this->input->post('first_name');
					$last_name = $this->input->post('last_name');
					$username = $this->input->post('username');
					$email = $this->input->post('email');
					$password = $this->input->post('password');
					
					$salt = substr(md5(uniqid(rand(), true)), 0, 10);
					$password   =$salt . substr(sha1($salt . $password), 0, -10);
					
					if($this->users_model->add_backend_user($user_type,$first_name,$last_name,$username,$email,$password,$this->session->userdata('user_id'))){
						$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Users added successfully </div>');
						redirect('/users');
					}else{
						$this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>Something Wrong</div>');
						redirect('/users');
					}
					
				}
			}else{
			
				$this->load->view('common/header',$data);
				$this->load->view('user_add');
				$this->load->view('common/footer');
			}
		}else{
			$this->load->view('common/header',$data);
			$this->load->view('not_permission');
			$this->load->view('common/footer');
		}
	}
	
	public function user_edit(){
		 $data = array();
	   
	     $data['roles'] = $this->users_model->get_roles(); 
	     $user_id =  $this->uri->segment(3);
	     $data['user_data'] = $this->users_model->get_user_data($user_id); 
			if($this->has_rights(13) == 1){
				
				if($this->input->post() && $this->input->post('user_id')){
				$this->form_validation->set_rules('user_type', 'User type', 'required');
				$this->form_validation->set_rules('first_name', 'First name', 'required');
				if($this->input->post('user_type') != 6) $this->form_validation->set_rules('last_name', 'Last name', 'required');
				//$this->form_validation->set_rules('username', 'Username', 'required|is_unique[user_login.user_name]');
				//$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[user_login.email]');
				$this->form_validation->set_rules('password', 'Password', 'matches[confirm_password]|min_length[8]|max_length[20]');
				$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'min_length[8]|max_length[20]');
				
				if ($this->form_validation->run() == FALSE)
				{
					$this->load->view('common/header',$data);
					$this->load->view('user_add');
					$this->load->view('common/footer');
				}else{
					
					$user_type = $this->input->post('user_type');
					$first_name = $this->input->post('first_name');
					$last_name = $this->input->post('last_name');
					$user_id = $this->input->post('user_id');
					$email = $this->input->post('email');
					$password = $this->input->post('password');
				
					$salt = substr(md5(uniqid(rand(), true)), 0, 10);
					$password   =$salt . substr(sha1($salt . $password), 0, -10);
					
					if($this->users_model->edit_backend_user($user_type,$first_name,$last_name,$user_id,$password,$this->session->userdata('user_id'))){
						$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Users edited successfully</div>');
						redirect('/users');
					}else{
						$this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>Something Wrong</div>');
						redirect('/users');
					}
					
				}
				}else{
					
					$this->load->view('common/header',$data);
					$this->load->view('user_add');
					$this->load->view('common/footer');
				}
			}else{
				
			$this->load->view('common/header',$data);
			$this->load->view('not_permission');
			$this->load->view('common/footer');
				}
		
		}
		
		function user_delete(){
			$data = array();
		   
			if($this->has_rights(14) == 1){
				$user_id = $this->input->post('id');
				$this->users_model->delete_user($user_id,$this->session->userdata('user_id'));
			}
			
		}
		
		function user_view(){
			 $data = array();
		
			 $data['roles'] = $this->users_model->get_roles(); 
			 $user_id =  $this->uri->segment(3);
			 $data['user_data'] = $this->users_model->get_user_data($user_id); 
			if($this->has_rights(11) == 1){
				
				$this->load->view('common/header',$data);
				$this->load->view('user_view');
				$this->load->view('common/footer');
				
			}else{
				
				$this->load->view('common/header',$data);
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			 }
			
			}
			
	function check_email(){
		$email = $this->input->post('email');
		$res = $this->users_model->emailUnique($email);
		$count = count($res);
		// echo $count;exit;
		$html = '';
		if($count > 0)
		{	$html = 'Email id already exists';
			echo $html;	
		}
		else echo $html;
  }
}