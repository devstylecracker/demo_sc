<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products_tagging extends MY_Controller {

	function __construct(){
       parent::__construct();
       $this->load->model('products_tagging_model');
   	}

	public function index($offset = null)
	{
		if(!$this->session->userdata('user_id'))
		{
			$this->load->view('login');

		}else
		{
			 $data_post = array();

			 $data_post['store'] = $this->products_tagging_model->get_stores();

			 $all_tags = $this->products_tagging_model->get_all_tags();

			 $data_post['allTags'] = json_encode($all_tags);

			 if($this->has_rights(39) == 1)
			{

				$this->load->view('common/header');
				$this->load->view('product/products_tagging',$data_post);
				$this->load->view('common/footer');

			}else
			{
				$this->load->view('common/header');
                $this->load->view('not_permission');
                $this->load->view('common/footer');

			}

		}
	}


	public function getproduct($store_id=NULL,$page=NULL)
	{
		$store ='';
		if($store_id!=NULL)
		{
			$store ='store';
		}

		if($page==NULL)
		{
			$page = 1;
		}

		$per_page = 30;
		$imagify_brandid = unserialize(IMAGIFY_BRANDID); 
		$data_post['products']=$this->products_tagging_model->get_products($store,$store_id,$page,$per_page);
		$count = count($this->products_tagging_model->get_products($store,$store_id,'',''));

				$countvar = $count;
				$pagenos = 0;
				$remain = 0;
				if($count !=0)
				{
					$remain = $count/30;
				}
				$pagenos =  ceil($remain);
				$data_post['pagenos'] = $pagenos;
				$product_html = '';
				$approveClass ='';
				if(!empty($data_post['products']))
				{
					foreach($data_post['products'] as $val)
					{
						if($val['approve_reject'] == 'A')
						{
							$approveClass="border: 2px solid #00ff00;";
						}else
						{
							$approveClass="";
						}

						
				           if(in_array($val['brand_id'],$imagify_brandid))
				          {
				             $productUrl = $this->config->item('products_tiny').$val['image'];
				          }else
				          {
				            $productUrl = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_160/'.$val['image'];
				             
				          }

						$product_html = $product_html.'<li style="'.$approveClass.'" ><img data-img-link="'.$productUrl.'" id="'.$val['product_id'].'" src="'.$productUrl.'" alt="'.$val['name'].'" onerror="this.onerror=null; this.src='.DEFAULT_IMAGE_PATH.';">
							<i class="fa fa-check-circle"></i>
						</li>

						';
					}
				}else
				{
					$product_html = $product_html.'<li class="cat">No record found </li>';
				}
				echo $product_html;

	}

	public function getproductTags()
	{
		$ajax_productids = $this->input->post('product_ids');
		$prod =array();
		$prod = json_decode($ajax_productids);
		$prod_new = trim($ajax_productids, "[]");
		$product_ids_arr = array();
		$data_post['products_tags'] = array();

		$tags =array();
		$tags1 =array();

		$tags_new= $this->products_tagging_model->get_product_tags($prod_new);

		$count = count($data_post['products_tags']);

		$product_tags = '';
		$j = 0;
		if(!empty($tags_new))
		{
			foreach($tags_new as $key=>$val)
			{
				$product_tags = $product_tags.'<li class="selected" id="'.$key.'" >'.$val.'</li>';

				$j++;
			}

		}else
		{

		}

		echo $product_tags;

	}

	public function getpage($store_id)
	{
		$count = count($this->products_tagging_model->get_products('store',$store_id,'',''));
		$pagenos = 0;
		if($count !=0)
		{
			$remain = $count/30;
		}else{
			$remain = 0;
		}

		$pagenos =  ceil($remain);
		$data_post['pagenos'] = $pagenos;

		$page_html = '';

		if(!empty($data_post['pagenos']))
		{
            for($i=1;$i<=$pagenos;$i++)
            {

     			$page_html = $page_html.'<option value="'.$i.'" >'.$i.'</option>';

            }

		}else
		{

		}
		echo $page_html;
	}

	public function saveproductTags()
	{

		 $tags = $this->input->post('tags');
		 $products = $this->input->post('products');
		 $result = $this->products_tagging_model->saveTags($tags,$products);
		 if($result)
		 {
			return true;
		 }else
		 {
			return false;
		 }
	}

	public function publishproduct()
	{
		$products = $this->input->post('products');
		$result = $this->products_tagging_model->publishProducts($products);
		if($result)
		{
			return true;
		}else
		{
			return false;
		}

	}


}
?>
