<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand_image extends MY_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
       parent::__construct();
       $this->load->model('brandusers_model'); 
   } 
   
   public function upload_images(){
   		if($this->session->userdata('user_id')) { 
   		$data = array(); $show_msg = '';
   		$brand_id = $this->uri->segment(3);
   		$data['paid_brands'] = $this->brandusers_model->get_paid_brands();
   		$data['images'] = $this->brandusers_model->get_brands_image($brand_id);
   		$data['brand_id'] = $brand_id;
   		if($this->input->post()){
	   		if ($this->form_validation->run() == FALSE){

	   			if($_FILES['brands_logo']['name']!=''){

	   				$config['upload_path'] = 'assets/brand_logo/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']	= '1000';
					$config['max_width']  = '530';
					$config['max_height']  = '530';
					$config['overwrite'] = TRUE;
					$config['file_name'] = $data['images'][0]['logo']!='' ? $data['images'][0]['logo'] : $brand_id;
					$this->upload->initialize($config);
					if(!($this->upload->do_upload('brands_logo'))){ print_r($this->upload->display_errors()); }else{
						$this->brandusers_model->update_brand_img($brand_id,'logo',$this->upload->data('file_name'));
						$show_msg = $show_msg."Brand Logo uploaded successfully <br>";
					}

	   			}

	   			if($_FILES['cover_img_']['name']!=''){ 
	   				$config1 = array();
	   				$config1['upload_path'] = '../assets/images/brands/';
					$config1['allowed_types'] = 'gif|jpg|png|jpeg';
					$config1['max_size']	= '100';
					$config1['max_width']  = '1366';
					$config1['max_height']  = '370';
					$config1['overwrite'] = TRUE;
					$config1['file_name'] = $data['images'][0]['cover_image']!='' ? $data['images'][0]['cover_image'] : $brand_id;
					$this->upload->initialize($config1);
					if(!($this->upload->do_upload('cover_img_'))){ print_r($this->upload->display_errors());  }else{
						$filename = $this->upload->data('file_name');
						$this->brandusers_model->update_brand_img($brand_id,'cover_image',$filename);
						$show_msg = $show_msg."Brand Cover Image uploaded successfully <br>";
					}
	   			}
				
				if($_FILES['cover_img_mobile']['name']!=''){
	   				$config1 = array();
	   				$config1['upload_path'] = '../assets/images/brands/mobile_cover_img/';
					$config1['allowed_types'] = 'gif|jpg|png|jpeg';
					$config1['max_size']	= '50';
					$config1['max_width']  = '375';
					$config1['max_height']  = '200';
					$config1['overwrite'] = TRUE;
					$config1['file_name'] = $data['images'][0]['cover_image']!='' ? $data['images'][0]['cover_image'] : $brand_id;
					$this->upload->initialize($config1);
					if(!($this->upload->do_upload('cover_img_mobile'))){ print_r($this->upload->display_errors());  }else{
						$filename = $this->upload->data('file_name');
						$this->brandusers_model->update_brand_img($brand_id,'cover_image',$filename);
						$show_msg = $show_msg."Brand Mobile Cover Image uploaded successfully <br>";
					}
	   			}

	   			if($_FILES['home_page_promotion']['name']!=''){
	   				$config1 = array();
	   				$config1['upload_path'] = '../assets/images/brands/homepage/';
					$config1['allowed_types'] = 'gif|jpg|png|jpeg';
					$config1['max_size']	= '50';
					$config1['max_width']  = '455';
					$config1['max_height']  = '300';
					$config1['overwrite'] = TRUE;
					$config1['file_name'] = $data['images'][0]['home_page_promotion_img']!='' ? $data['images'][0]['home_page_promotion_img'] : $brand_id;
					$this->upload->initialize($config1);
					if(!($this->upload->do_upload('home_page_promotion'))){ print_r($this->upload->display_errors());  }else{
						$this->brandusers_model->update_brand_img($brand_id,'home_page_promotion_img',$this->upload->data('file_name'));
						$show_msg = $show_msg."Brand homepage Image uploaded successfully <br>";
					}
	   			}

	   			if($_FILES['brand_list_img']['name']!=''){
	   				
	   				$config2 = array();
	   				$config2['upload_path'] = '../assets/images/brands/list/';
					$config2['allowed_types'] = 'gif|jpg|png|jpeg';
					$config2['max_size']	= '1000';
					$config2['max_width']  = '530';
					$config2['max_height']  = '530';
					$config2['overwrite'] = TRUE;
					$config2['file_name'] = $data['images'][0]['listing_page_img']!='' ? $data['images'][0]['listing_page_img'] : $brand_id;
					$this->upload->initialize($config2);
					if(!($this->upload->do_upload('brand_list_img'))){ print_r($this->upload->display_errors());  }else{
						$filename = $this->upload->data('file_name');
						$this->brandusers_model->update_brand_img($brand_id,'listing_page_img',$filename);
						$show_msg = $show_msg."Brand Listing Page Image uploaded successfully <br>";
					}
	   			}


	   			if($_FILES['top_img']['name']!=''){
	   				
	   				$config2 = array();
	   				if(!is_dir('../assets/images/size_guide/brands/'.$brand_id)) 
                    	{
                        	mkdir('../assets/images/size_guide/brands/'.$brand_id,0755,TRUE);
                        } else{
                        	
                        }
	   				$config2['upload_path'] = '../assets/images/size_guide/brands/'.$brand_id;
					$config2['allowed_types'] = 'jpg';
					//$config2['max_size']	= '1000';
					//$config2['max_width']  = '500';
					//$config2['max_height']  = '500';
					$config2['overwrite'] = TRUE;
					$config2['file_name'] = 'top';
					$this->upload->initialize($config2);
					if(!($this->upload->do_upload('top_img'))){ print_r($this->upload->display_errors()); }else{
						$filename = $this->upload->data('file_name');
						$show_msg = $show_msg."Top Size guide Image uploaded successfully <br>";
					}
	   			}

	   			if($_FILES['bottom_img']['name']!=''){
	   				
	   				$config2 = array();
	   				if(!is_dir('../assets/images/size_guide/brands/'.$brand_id)) 
                    	{
                        	mkdir('../assets/images/size_guide/brands/'.$brand_id,0755,TRUE);
                        } else{
                        	
                        }
	   				$config2['upload_path'] = '../assets/images/size_guide/brands/'.$brand_id;
					$config2['allowed_types'] = 'jpg';
					//$config2['max_size']	= '1000';
					//$config2['max_width']  = '500';
					//$config2['max_height']  = '500';
					$config2['overwrite'] = TRUE;
					$config2['file_name'] = 'bottom';
					$this->upload->initialize($config2);
					if(!($this->upload->do_upload('bottom_img'))){ print_r($this->upload->display_errors()); }else{
						$filename = $this->upload->data('file_name');
						$show_msg = $show_msg."Bottom Size guide Image uploaded successfully <br>";
					}
	   			}

	   			if($_FILES['footwear_img']['name']!=''){
	   				
	   				$config2 = array();
	   				if(!is_dir('../assets/images/size_guide/brands/'.$brand_id)) 
                    	{
                        	mkdir('../assets/images/size_guide/brands/'.$brand_id,0755,TRUE);
                        } else{
                        	
                        }
	   				$config2['upload_path'] = '../assets/images/size_guide/brands/'.$brand_id;
					$config2['allowed_types'] = 'jpg';
					//$config2['max_size']	= '1000';
					//$config2['max_width']  = '500';
					//$config2['max_height']  = '500';
					$config2['overwrite'] = TRUE;
					$config2['file_name'] = 'footwear';
					$this->upload->initialize($config2);
					if(!($this->upload->do_upload('footwear_img'))){ print_r($this->upload->display_errors()); }else{
						$filename = $this->upload->data('file_name');
						$show_msg = $show_msg."Footwear Size guide Image uploaded successfully <br>";
					}
	   			}

	   			if($_FILES['top_bottom_img']['name']!=''){
	   				
	   				$config2 = array();
	   				if(!is_dir('../assets/images/size_guide/brands/'.$brand_id)) 
                    	{
                        	mkdir('../assets/images/size_guide/brands/'.$brand_id,0755,TRUE);
                        } else{
                        	
                        }
	   				$config2['upload_path'] = '../assets/images/size_guide/brands/'.$brand_id;
					$config2['allowed_types'] = 'jpg';
					//$config2['max_size']	= '1000';
					//$config2['max_width']  = '500';
					//$config2['max_height']  = '500';
					$config2['overwrite'] = TRUE;
					$config2['file_name'] = 'top_bottom';
					$this->upload->initialize($config2);
					if(!($this->upload->do_upload('top_bottom_img'))){ print_r($this->upload->display_errors()); }else{
						$filename = $this->upload->data('file_name');
						$show_msg = $show_msg."Top Bottom Size guide Image uploaded successfully <br>";
					}
	   			}
				/* added for men size guide */
				if($_FILES['top_img_men']['name']!=''){
	   				
	   				$config2 = array();
	   				if(!is_dir('../assets/images/size_guide/brands/'.$brand_id)) 
                    	{
                        	mkdir('../assets/images/size_guide/brands/'.$brand_id,0755,TRUE);
                        } else{
                        	
                        }
	   				$config2['upload_path'] = '../assets/images/size_guide/brands/'.$brand_id;
					$config2['allowed_types'] = 'jpg';
					//$config2['max_size']	= '1000';
					//$config2['max_width']  = '500';
					//$config2['max_height']  = '500';
					$config2['overwrite'] = TRUE;
					$config2['file_name'] = 'top_men';
					$this->upload->initialize($config2);
					if(!($this->upload->do_upload('top_img_men'))){ print_r($this->upload->display_errors()); }else{
						$filename = $this->upload->data('file_name');
						$show_msg = $show_msg."Top Size guide Image uploaded successfully <br>";
					}
	   			}
				
				if($_FILES['bottom_img_men']['name']!=''){
	   				
	   				$config2 = array();
	   				if(!is_dir('../assets/images/size_guide/brands/'.$brand_id)) 
                    	{
                        	mkdir('../assets/images/size_guide/brands/'.$brand_id,0755,TRUE);
                        } else{
                        	
                        }
	   				$config2['upload_path'] = '../assets/images/size_guide/brands/'.$brand_id;
					$config2['allowed_types'] = 'jpg';
					//$config2['max_size']	= '1000';
					//$config2['max_width']  = '500';
					//$config2['max_height']  = '500';
					$config2['overwrite'] = TRUE;
					$config2['file_name'] = 'bottom_men';
					$this->upload->initialize($config2);
					if(!($this->upload->do_upload('bottom_img_men'))){ print_r($this->upload->display_errors()); }else{
						$filename = $this->upload->data('file_name');
						$show_msg = $show_msg."Bottom Size guide Image uploaded successfully <br>";
					}
	   			}

	   			if($_FILES['footwear_img_men']['name']!=''){
	   				
	   				$config2 = array();
	   				if(!is_dir('../assets/images/size_guide/brands/'.$brand_id)) 
                    	{
                        	mkdir('../assets/images/size_guide/brands/'.$brand_id,0755,TRUE);
                        } else{
                        	
                        }
	   				$config2['upload_path'] = '../assets/images/size_guide/brands/'.$brand_id;
					$config2['allowed_types'] = 'jpg';
					//$config2['max_size']	= '1000';
					//$config2['max_width']  = '500';
					//$config2['max_height']  = '500';
					$config2['overwrite'] = TRUE;
					$config2['file_name'] = 'footwear_men';
					$this->upload->initialize($config2);
					if(!($this->upload->do_upload('footwear_img_men'))){ print_r($this->upload->display_errors()); }else{
						$filename = $this->upload->data('file_name');
						$show_msg = $show_msg."Footwear Size guide Image uploaded successfully <br>";
					}
	   			}

	   			if($_FILES['top_bottom_img_men']['name']!=''){
	   				
	   				$config2 = array();
	   				if(!is_dir('../assets/images/size_guide/brands/'.$brand_id)) 
                    	{
                        	mkdir('../assets/images/size_guide/brands/'.$brand_id,0755,TRUE);
                        } else{
                        	
                        }
	   				$config2['upload_path'] = '../assets/images/size_guide/brands/'.$brand_id;
					$config2['allowed_types'] = 'jpg';
					//$config2['max_size']	= '1000';
					//$config2['max_width']  = '500';
					//$config2['max_height']  = '500';
					$config2['overwrite'] = TRUE;
					$config2['file_name'] = 'top_bottom_men';
					$this->upload->initialize($config2);
					if(!($this->upload->do_upload('top_bottom_img_men'))){ print_r($this->upload->display_errors()); }else{
						$filename = $this->upload->data('file_name');
						$show_msg = $show_msg."Top Bottom Size guide Image uploaded successfully <br>";
					}
	   			}
				/* added for men size guide */
	   		}
   		}

   		$data['show_msg'] = $show_msg;

   		$this->load->view('common/header');
		$this->load->view('brand/brand_image',$data);
		$this->load->view('common/footer');
		}else{
			echo "Login Again";
		}
   }
}
