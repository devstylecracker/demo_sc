<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand_step5 extends CI_Controller {

	function __construct(){
       parent::__construct();
       $this->load->model('Brand_details_model');
   	}
	
	public function index()
	{
		if(!$this->session->userdata('user_id')){
		$this->load->view('login');
		}else{
			$brand_id = $this->session->userdata('user_id');
			if($this->input->post()){
				
				$config['upload_path'] = 'assets/cheque/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size']	= '13000';
				$config['max_width']  = '1500';
				$config['max_height']  = '1500';
				$config['file_name'] = time();
				$ok =0;
						
				if(!empty($_FILES)){
					if(@$_FILES['cheque']['name']!=''){
						$this->upload->initialize($config);
						if(!($this->upload->do_upload('cheque'))){
								$data['error'] = array($this->upload->display_errors()); 
								$this->load->view('common/header',$data);
								$this->load->view('brand_onboarding/brand_step5');
								$this->load->view('common/footer');
								$ok=0;
						}else{
						 $cheque = $this->upload->data('file_name'); 
						 $ok=1;
						}
					}else {
							$error = array('please upload image');
							$data['error'] = $error; 

					}
				}
						
				$config2['upload_path'] = 'assets/pancard/';
				$config2['allowed_types'] = 'gif|jpg|png|jpeg';
				$config2['max_size']	= '13000';
				$config2['max_width']  = '1500';
				$config2['max_height']  = '1500';
				$config2['file_name'] = time();
				$ok2 =0;
						
				if(!empty($_FILES)){
					if(@$_FILES['pancard']['name']!=''){
						$this->upload->initialize($config2);
						if(!($this->upload->do_upload('pancard'))){
								$this->load->view('common/header',$data);
								$this->load->view('brand_onboarding/brand_step5');
								$this->load->view('common/footer');
								$ok2 = 0;
							}else{
							 $pancard = $this->upload->data('file_name'); 
							 $ok2 = 1;
							}
						}else {
							$error = array('please upload image');
							$data['error'] = $error; 

						}
				}
						
						
				if($ok == 1 && $ok2 == 1){		
					$data = $this->Brand_details_model->update_brand_images($cheque,$pancard,$brand_id);
					redirect('brand_onboarding/brand_step6');	
				}else 
				{
					
					$this->load->view('common/header1');
					$this->load->view('brand_onboarding/brand_step5',$data);
					$this->load->view('common/footer1');
				}
			}else{
					
				$this->load->view('common/header1');
				$this->load->view('brand_onboarding/brand_step5');
				$this->load->view('common/footer1');
			}
		}
	}
}
