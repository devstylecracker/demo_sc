<?php
ini_set( 'memory_limit', '-1' );
defined('BASEPATH') OR exit('No direct script access allowed');

class Store_registration extends CI_Controller {

	function __construct(){
       parent::__construct();
	   
	   $this->load->library('excel');
       $this->load->model('Brand_details_model');
	   
   	}
	
	public function index()
	{
		$data = array();
		$brand_id = $this->session->userdata('user_id');
		$data['brand_info_table'] =  $this->Brand_details_model->get_brand_info($brand_id);
		if(isset($data['brand_info_table'][0])){
		if($data['brand_info_table'][0]['step_completed'] > 0 && $data['brand_info_table'][0]['is_form_completed'] != 1){
			
			redirect('brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed'].'','refresh');
			// header('Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed'].'');
		}else if($data['brand_info_table'][0]['is_form_completed'] == 1){
			
			redirect('brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed'].'','refresh');
			// header('Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed'].'');
		}}else if($this->input->post()){
				$user_type = 6;
				$this->form_validation->set_rules('store_type', 'Store Type', 'required');
				$this->form_validation->set_rules('company_name', 'Brand Name', 'required|is_unique[brand_info.company_name]');
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[user_login.email]');
				$this->form_validation->set_rules('password', 'Password', 'required|matches[confirm_password]|min_length[8]|max_length[20]');
				$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|min_length[8]|max_length[20]');
				
				if ($this->form_validation->run() == FALSE)
				{
					$this->load->view('brand_onboarding/store_registration',$data);
				}else{
					// save data
					$user_type = 6;
					$store_type = $this->input->post('store_type');
					$first_name = $this->input->post('email');
					$last_name = $this->input->post('company_name');
					$username = preg_replace('/\s+/', '-', trim(strtolower($this->input->post('company_name'))));
					$company_name = $this->input->post('company_name');
					$email = $this->input->post('email');
					$password = $this->input->post('password');
					
					$salt = substr(md5(uniqid(rand(), true)), 0, 10);
					$new_password   =$salt . substr(sha1($salt . $password), 0, -10);
					if($this->Brand_details_model->add_brand_user($user_type,$first_name,$last_name,$username,$email,$new_password,0,$store_type,$company_name)){
						$this->send_email($email,$password);
						$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong></br></br> Hello!</br>
						Your Store Login ID has been successfully updated with StyleCracker.</br>
						You can now continue to register with us! </div>');
						redirect('/');
						// header('Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/');
					}else{
						$this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>Something Wrong</div>');
						redirect('/');
						// header('Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/');
					}
					
				}
			
	}else {
			$this->load->view('brand_onboarding/store_registration');
		 }//end else
		
	}//end of function 

	function forgot_password(){
		$this->load->view('brand_onboarding/forgot_password');
	}
	
	function send_email($email,$password){
		
		$config['protocol'] = 'smtp';
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;
		$config['smtp_host'] = $this->config->item('smtp_host');
		$config['smtp_user'] = $this->config->item('smtp_user');
		$config['smtp_pass'] = $this->config->item('smtp_pass');
		$config['smtp_port'] = $this->config->item('smtp_port');

		$email_id2 = array('arun@stylecracker.com');
		$this->email->initialize($config);

		$this->email->from($this->config->item('from_email'), 'Stylecracker');

		if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){	       
	        $this->email->to($this->config->item('sc_test_emaild'));
			$this->email->bcc($this->config->item('sc_testcc_emaild'));
	    }else{
			$this->email->to($email);
			$this->email->bcc($email_id2);
		}
		$this->email->subject('Welcome to StyleCracker');
		$this->email->message("Hello,</br></br>Thank you for registering with us on StyleCracker!</br></br>The following are your login credentials:</br></br><b>User ID : ".$email."</b></br><b>Password : ".$password."</b></br></br>We will soon get back to you on the way forward with integrating the products and taking the brand live.</br></br>Thanks!</br></br>Warm Regards,</br>Team StyleCracker");

		if($this->email->send())
		{
			return "Email Sent";
		}else
		{
			return "Email Error";
		}
	}
	
	function send_forgotemail(){
		$Emailid = $this->input->post('email');
		if(trim($Emailid) == ''){
            echo "Please enter your email id";

          }else{
            $result = $this->Brand_details_model->forgot_password($Emailid);
            if($result != false){
               
               if($this->send_forgotpwd_email($Emailid,$result)) 
               {
                  echo 'New Password has been Send to your email-id '.$Emailid;
               }else
               {
                  echo "Email Error";
               }

            }else{
              echo 'Invalid Email-id ';

            }

          }  
	}
	
	public function send_forgotpwd_email($email_id,$new_password){
		
	  
      $config['protocol'] = 'smtp';
//      $config['mailpath'] = '/usr/sbin/sendmail';
 //     $config['charset'] = 'iso-8859-1';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = TRUE;
      $config['smtp_host'] = $this->config->item('smtp_host');
      $config['smtp_user'] = $this->config->item('smtp_user');
      $config['smtp_pass'] = $this->config->item('smtp_pass');
      $config['smtp_port'] = $this->config->item('smtp_port');
	  
	  $email_id2 = array('arun@stylecracker.com');
      $this->email->initialize($config);

      $this->email->from($this->config->item('from_email'), 'Stylecracker');

      if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){	       
	        $this->email->to($this->config->item('sc_test_emaild'));
			$this->email->bcc($this->config->item('sc_testcc_emaild'));
	  }else{
	      $this->email->to($email_id);
		  $this->email->bcc($email_id2);
	  }
      $this->email->subject('Your new password');
      $this->email->message("Your login details are <br> Email : ".$email_id."<br><br>Your new password is:".$new_password."<br><br>You can change your password at any time by logging into your account. <br><br>Warm Regards,<br>StyleCracker");

      if($this->email->send())
      {
        return "Email Sent";
      }else
      {
        return "Email Error";
      }
      
  }
	/*
	public function brand_step1(){
		$data = array();
		$this->load->view('common/header1');
		$this->load->view('brand_onboarding/brand_step1',$data);
		$this->load->view('common/footer1');
	}*/
	
	
	public function brand_step4(){
  		$data = array();
		$brand_id = $this->session->userdata('user_id');
  		$data['brand_info_table'] =  $this->Brand_details_model->get_brand_info($brand_id);
		
		if(isset($_REQUEST['test'])){
			$test = $_REQUEST['test'];
			$data['brand_info_table'][0]['step_completed'] = 4;
		}else $test = 0;
		
			if(!$this->session->userdata('user_id')){
				
				$this->load->view('login');
				
			}else if($data['brand_info_table'][0]['is_form_completed'] == 1){
				
				redirect('brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed'].'','refresh');
				// header('Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed'].'');
				
			}else if($data['brand_info_table'][0]['step_completed'] <= 4 && $data['brand_info_table'][0]['is_form_completed'] != 1 && $data['brand_info_table'][0]['step_completed'] != 4 ){
				
			redirect('brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed'].'','refresh');
			// header('Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed'].'');
			
			}else if($this->input->post()){
  			
  			$this->form_validation->set_rules('company_address','Company Name','required|trim');
  			$this->form_validation->set_rules('city','City','required|trim');
  			$this->form_validation->set_rules('state','State','required|trim');
  			$this->form_validation->set_rules('pincode','Pincode','required|trim');
  			$this->form_validation->set_rules('country','country','required|trim');
  			$this->form_validation->set_rules('contact_person','Contact Person','required|trim');
  			$this->form_validation->set_rules('phone_one','Phone Number','required|trim|min_length[10]|max_length[13]');
  			$this->form_validation->set_rules('emailid','Email Id','required|trim|valid_email');
  			$this->form_validation->set_rules('contact_person_two','Phone Number','required|trim');
  			$this->form_validation->set_rules('phone_two','Phone Number','required|trim|min_length[10]|max_length[13]');
  			$this->form_validation->set_rules('alt_emailid','Email Id','required|trim|valid_email');
  			$this->form_validation->set_rules('contact_person_three','Phone Number','required|trim');
  			$this->form_validation->set_rules('phone_three','Phone Number','required|trim|min_length[10]|max_length[13]');
  			$this->form_validation->set_rules('alt_emailid_three','Email Id','required|trim|valid_email');
                
            if($this->form_validation->run() == FALSE){  
				$this->load->view('common/header1');
				$this->load->view('brand_onboarding/brand_step4',$data);
				$this->load->view('common/footer1');
            }else{ 
			
            	$data = $this->Brand_details_model->contact_info($_POST);
					if($data){
						redirect('brand_onboarding/store_registration/brand_step5'.'','refresh');	
						// header('Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/brand_step5');
					}else {
						$this->load->view('common/header1');
						$this->load->view('brand_onboarding/brand_step4',$data);
						$this->load->view('common/footer1');	
					}
            }

  		}else{
			
			$data = $this->Brand_details_model->contact_info();
			$this->load->view('common/header1');
			$this->load->view('brand_onboarding/brand_step4',$data);
			$this->load->view('common/footer1');
  		}
  		
  }
  
  public function brand_step3(){
  		$data = array();
		$brand_id = $this->session->userdata('user_id');
		$data['brand_info'] = $this->Brand_details_model->get_brand_info($brand_id);
		
		if(isset($_REQUEST['test'])){
			$test = $_REQUEST['test'];
			$data['brand_info'][0]['step_completed'] = 3;
		}else $test = 0;
		
		$data['brand_target_market'] = $this->Brand_details_model->target_market_by_brand($brand_id);
		if(!empty($data['brand_target_market'])){ $data['t_market']=array(); 
		foreach($data['brand_target_market'] as $val){ $data['t_market'][]= $val['target_market_id']; } }
		$data['target_market'] = $this->Brand_details_model->target_market(); 
		
	
	if(!$this->session->userdata('user_id')){
		
		$this->load->view('login');
		
	}else if($data['brand_info'][0]['is_form_completed'] == 1){
			redirect('brand_onboarding/Store_registration/brand_step'.$data['brand_info'][0]['step_completed'].'','refresh');
			// header('Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/brand_step'.$data['brand_info'][0]['step_completed'].'');
			
	}else if($data['brand_info'][0]['step_completed'] <= 3 && $data['brand_info'][0]['is_form_completed'] != 1 && $data['brand_info'][0]['step_completed'] != 3 ){
		redirect('brand_onboarding/Store_registration/brand_step'.$data['brand_info'][0]['step_completed'].'','refresh');
		// header('Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/brand_step'.$data['brand_info'][0]['step_completed'].'');
		
	}else if($this->input->post()){
			$company_name = $this->input->post('company_name');
  			// $this->form_validation->set_rules('registration_no','Registered Company ID','required');
  			$this->form_validation->set_rules('company_name','Company Name','required|trim');
  			$this->form_validation->set_rules('registered_address','Registered Address','required|trim');
  			$this->form_validation->set_rules('long_description','Long Description','required|trim');
  			// $this->form_validation->set_rules('look_book_url','Look Book Url','required|trim');
			if($this->input->post('is_male') != 1 && $this->input->post('is_female') != 1 && $this->input->post('is_children') != 1)$this->form_validation->set_rules('is_male','Demographic','required');
			if(empty($this->input->post('target_market'))){ $this->form_validation->set_rules('target_market[]','Target Market','required'); }
  			$this->form_validation->set_rules('age_group_from','Age Group From','required|trim');
  			$this->form_validation->set_rules('age_group_to','Age Group To','required|trim');
  			$this->form_validation->set_rules('price_range_from','Price Range From','required|trim');
  			$this->form_validation->set_rules('price_range_to','Price Range To','required|trim');
  			if($this->input->post('is_online')==1)$this->form_validation->set_rules('platform_name','Platform Name','required|trim');
			
            if($this->form_validation->run() == FALSE){ 
				// echo "comming in fail";print_r($data);
				$this->load->view('common/header1');
				$this->load->view('brand_onboarding/brand_step3',$data);
				$this->load->view('common/footer1');  
            }else if($this->Brand_details_model->check_brand_name($company_name)){
					$data['company_name'] = 'The Company Name field must contain a unique value.';
					$this->load->view('common/header1');
					$this->load->view('brand_onboarding/brand_step3',$data);
					$this->load->view('common/footer1');  
				}else{ 
			
				$registration_no = $this->input->post('registration_no');
				$company_name = $this->input->post('company_name');
				$registered_address = $this->input->post('registered_address');
				$long_description = $this->input->post('long_description');
				$look_book_file = $this->input->post('look_book_file');
				$look_book_url = $this->input->post('look_book_url');
				$is_male = $this->input->post('is_male');
				$is_female = $this->input->post('is_female');
				$is_children = $this->input->post('is_children');
				$target_market = $this->input->post('target_market');
				$age_group_from = $this->input->post('age_group_from');
				$age_group_to = $this->input->post('age_group_to');
				$price_range_from = $this->input->post('price_range_from');
				$price_range_to = $this->input->post('price_range_to');
				$is_online = $this->input->post('is_online');
				$platform_name = $this->input->post('platform_name');
				// echo "<pre>";print_r($_POST);exit;
				if($is_online !=1 ) $is_online = 0;
				$config['upload_path'] = 'assets/look_book/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
				$config['max_size']	= '13000';
				$config['max_width']  = '1500';
				$config['max_height']  = '1500';
				$config['file_name'] = time();
				$ok =1;
				
				if(!empty($_FILES)){
				if(@$_FILES['look_book_file']['name']!=''){
					$this->upload->initialize($config);
					if(!($this->upload->do_upload('look_book_file'))){
							$data['error'] = array($this->upload->display_errors()); 
							$this->load->view('common/header1',$data);
							$this->load->view('brand_onboarding/brand_step3');
							$this->load->view('common/footer1');
							$ok =0;
						}else{
						 $look_file_name = $this->upload->data('file_name'); 
						 $test3 = getimagesize('assets/look_book/'.$look_file_name);
							$width = $test3[0];
							$height = $test3[1];
							if (($width != 1366 && $height != 371) || 1==1){
								// $error = array('Image Height And Width Should Be 1366 X 371');
								// $data['error'] = $error; 
								// $this->load->view('common/header1',$data);
								// $this->load->view('brand_onboarding/brand_step1');
								// $this->load->view('common/footer1');
								$ok =0; 
							}
						}
					}
				}
				$data['brand_info1'] = array(
							'user_id' => $brand_id,
							'company_name'=>$company_name,
							'address'=>$registered_address,
							'long_description'=>$long_description,
							'age_group_from'=>$age_group_from,
							'age_group_to'=>$age_group_to,
							'price_range_from'=>$price_range_from,
							'price_range_to'=>$price_range_to,
							'is_online'=>$is_online,
							'is_male'=>$is_male,
							'is_female'=>$is_female,
							'is_children'=>$is_children,
							'look_book_url'=>$look_book_url,
							'look_book_file'=>$look_book_file,
							'platform_name'=>$platform_name,
							'registration_no'=>$registration_no,
							'step_completed'=>'4',
							);
				$data['target_market'] = $target_market;
				$brand_id = $this->Brand_details_model->update_business_data($data);
				
			
			if($brand_id){
				redirect('brand_onboarding/Store_registration/brand_step4'.'','refresh');	
				// header("Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/brand_step4");
			}else {
				$this->load->view('common/header1');
				$this->load->view('brand_onboarding/brand_step3',$data);
				$this->load->view('common/footer1');	
			}
			
			
            }//end of else 

  		}else{
			
			$this->load->view('common/header1');
			$this->load->view('brand_onboarding/brand_step3',$data);
			$this->load->view('common/footer1');
			
  		}
		
  }
  
  public function brand_step2(){
	$data = array();
	$data['brand_info'] = $this->Brand_details_model->get_brand_info($this->session->userdata('user_id'));
	
	if(isset($_REQUEST['test'])){
		$test = $_REQUEST['test'];
		$data['brand_info'][0]['step_completed'] = 2;
	}else $test = 0;
	
	if(!$this->session->userdata('user_id')){
		$this->load->view('login');
	}else if($data['brand_info'][0]['is_form_completed'] == 1){
			redirect('brand_onboarding/Store_registration/brand_step'.$data['brand_info'][0]['step_completed'].'','refresh');
			// header('Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/brand_step'.$data['brand_info'][0]['step_completed'].'');
	}else if($data['brand_info'][0]['step_completed'] <= 2 && $data['brand_info'][0]['is_form_completed'] != 1 && $data['brand_info'][0]['step_completed'] != 2 ){
		redirect('brand_onboarding/Store_registration/brand_step'.$data['brand_info'][0]['step_completed'].'','refresh');
		// header('Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/brand_step'.$data['brand_info'][0]['step_completed'].'');
	}else{ 
	
		if($this->input->post()){
			
			$user_type = 6;
			// $this->form_validation->set_rules('vat_tin_number', 'VAT TIN Number', 'required');
			// $this->form_validation->set_rules('cst_no', 'CST Number', 'required');
			// $this->form_validation->set_rules('brand_tax', 'Brand Tax', 'required');
			$this->form_validation->set_rules('cod', 'Cod', 'required');
			// if($this->input->post('cod') == 1) $this->form_validation->set_rules('cod_pin', 'COD Pincode', 'required');
			$this->form_validation->set_rules('shipping', 'Shipping', 'required');
			// if($this->input->post('shipping') == 1) $this->form_validation->set_rules('shipping_pin', 'Shipping Pincode', 'required');
			// $this->form_validation->set_rules('return_policy', 'Return Policy', 'required');
			
			
			// echo "<pre>";if(isset($_POST))print_r($_POST);exit;
			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('common/header1');
				$this->load->view('brand_onboarding/brand_step2',$data);
				$this->load->view('common/footer1');
			}else{
				// echo "<pre>";if(isset($_POST))print_r($_POST);exit;
				// save data
				$user_type = 6;
				$vat_tin_number = $this->input->post('vat_tin_number');
				$brand_tax = $this->input->post('brand_tax');
				$cst_no = $this->input->post('cst_no');
				$cod = $this->input->post('cod');
				$shipping = $this->input->post('shipping');
				$return_policy = trim($this->input->post('return_policy'));
				$brand_id = $this->session->userdata('user_id');
				$cod_pincodes = '';
				$shipping_pin = '';
				$fieldorder_cod = 0;
				$fieldorder_shipping = 0;
				$arrayCount = 0;
				// echo "<pre>";print_r($_FILES);exit;
				if($_FILES)
				{	
					if($_FILES['cod_pin']['tmp_name'] != "" && $_FILES['cod_pin']['error'] != 4)
				 	{			
					/* cod pincodes start*/
						if($_FILES['cod_pin']['tmp_name'] != "")
						{	
							$inputFileName = $_FILES['cod_pin']['tmp_name'];	

						}
						
						$handle = fopen($inputFileName, "r");
						
						try{
								//read file from path

								$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);



						}catch(Exception $e) {

							die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());

						}

						$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

						$arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet					

						$excelfields_cod =array('cod_pincode');
					
					
					if(trim($allDataInSheet[1]["A"]) != $excelfields_cod[0])
					{ $fieldorder_cod = 1; }
				
					if($fieldorder_cod == 1)
						{
							//$imagemsg = '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>Please Insert in the order of the fields uploaded.</div>';

							$data['excelmsg_cod'][0] = "Please Insert in the order of the fields uploaded.cod file.";
						}else
						{
							for($i=2;$i<=$arrayCount;$i++)
							{
								if($i==2 && $allDataInSheet[$i]["A"] != ''){
									$cod_pincodes = trim($allDataInSheet[$i]["A"]);	
								}else {
									if($allDataInSheet[$i]["A"] != '') $cod_pincodes = $cod_pincodes.','.trim($allDataInSheet[$i]["A"]);	
								}
										
							}
						}
						/* cod pincodes end*/
					}//end of check if
					unset($allDataInSheet);unset($objPHPExcel);unset($handle);unset($_FILES['cod_pin']);
					/* shipping pincodes start*/
					if($_FILES['shipping_pin']['tmp_name'] != "")
				 	{	
						if($_FILES['shipping_pin']['tmp_name'] != "")
						{	
							$inputFileName2 = $_FILES['shipping_pin']['tmp_name'];	

						}
						
						$handle = fopen($inputFileName2, "r");
						
						try{
								//read file from path

								$objPHPExcel = PHPExcel_IOFactory::load($inputFileName2);
								


						}catch(Exception $e) {
							
							die('Error loading file "'.pathinfo($inputFileName2,PATHINFO_BASENAME).'": '.$e->getMessage());

						}
						
						$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
						
						$arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet					
						
						$excelfields_shipping =array('shipping_pincode');
					
					
					if(trim($allDataInSheet[1]["A"]) != $excelfields_shipping[0])
					{  $fieldorder_shipping = 1; }
				
						if($fieldorder_shipping == 1)
						{
							$data['excelmsg_shipping'][0] = "Please Insert in the order of the fields uploaded. shipping file";
						}else
						{
							for($i=2;$i<=$arrayCount;$i++)
							{
								if($i==2 && $allDataInSheet[$i]["A"] != ''){
									$shipping_pin = trim($allDataInSheet[$i]["A"]);	
								}else {
									if($allDataInSheet[$i]["A"] != '') $shipping_pin = $shipping_pin.','.trim($allDataInSheet[$i]["A"]);	
								}
										
							}
						}
						/* shipping pincodes end*/
					}//end of check if name
				
				}
				
				if($cod_pincodes!='') $cod_pincodes = $this->Brand_details_model->get_pincode_list($cod_pincodes);
				if($shipping_pin!='') $shipping_pin = $this->Brand_details_model->get_pincode_list($shipping_pin);
				
				if($cod_pincodes != '' && $shipping_pin != ''){
					$update_data = array(
									'vat_tin_number' => $vat_tin_number, 
									'store_tax' => $brand_tax,
									'cst_number' => $cst_no,
									'is_cod' => $cod,
									'cod_exc_location' => $cod_pincodes,
									'is_shipping'=> $shipping,
									'shipping_exc_location' => $shipping_pin,
									'return_policy' => $return_policy,
									'step_completed'=>'3',
									);
				}else if($cod_pincodes == '' && $shipping_pin != ''){
					$update_data = array(
									'vat_tin_number' => $vat_tin_number, 
									'store_tax' => $brand_tax,
									'cst_number' => $cst_no,
									'is_cod' => $cod,
									'is_shipping'=> $shipping,
									'shipping_exc_location' => $shipping_pin,
									'return_policy' => $return_policy,
									'step_completed'=>'3',
									);
				}else if($shipping_pin == '' && $cod_pincodes != ''){
					$update_data = array(
									'vat_tin_number' => $vat_tin_number, 
									'store_tax' => $brand_tax,
									'cst_number' => $cst_no,
									'is_cod' => $cod,
									'cod_exc_location' => $cod_pincodes,
									'is_shipping'=> $shipping,
									'return_policy' => $return_policy,
									'step_completed'=>'3',
									);
				}else if($cod_pincodes == '' && $shipping_pin == ''){
					$update_data = array(
									'vat_tin_number' => $vat_tin_number, 
									'store_tax' => $brand_tax,
									'cst_number' => $cst_no,
									'is_cod' => $cod,
									'is_shipping'=> $shipping,
									'return_policy' => $return_policy,
									'step_completed'=>'3',
									);
				}
				
				
				if($this->Brand_details_model->update_brand_data($update_data) && $fieldorder_shipping != 1 && $fieldorder_cod != 1){
						redirect('brand_onboarding/Store_registration/brand_step3'.'','refresh');
						// header("Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/brand_step3");
				}else{
						$this->load->view('common/header1');
						$this->load->view('brand_onboarding/brand_step2',$data);
						$this->load->view('common/footer1');
				}
				
			}
		
		}else {
			$data['brand_info'] = $this->Brand_details_model->get_brand_info($this->session->userdata('user_id'));
			$this->load->view('common/header1');
			$this->load->view('brand_onboarding/brand_step2',$data);
			$this->load->view('common/footer1');
		 }//end else
	}//end of big else
  }
	
	/*
	function Brand_step1(){
		$data = array();
		$brand_id = $this->session->userdata('user_id');
		$data['brand_info_table'] =  $this->Brand_details_model->get_brand_info($brand_id);
		$data['brand_list'] = $this->Brand_details_model->get_brand_list();
		$data['brand_info'] = $this->Brand_details_model->get_store_data($brand_id);
		if(isset($_REQUEST['test'])){
			$test = $_REQUEST['test'];
			$data['brand_info_table'][0]['step_completed'] = 1;
		}else $test = 0;
		
			if(!$this->session->userdata('user_id')){
				
				$this->load->view('login');
				
			}else if($data['brand_info_table'][0]['is_form_completed'] == 1){
				
				redirect('brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed']);
				// header('Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed']);
				
			}else if($data['brand_info_table'][0]['step_completed'] <= 1 && $data['brand_info_table'][0]['is_form_completed'] != 1 && $data['brand_info_table'][0]['step_completed'] != 1 ){
				
				redirect('brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed']);
				// header('Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed']);
				
			}else{ if($this->input->post()){
				// echo "comming in if";exit;
				$counter = $this->input->post('submit');
				
				if($this->input->post('brand_name_'.$counter) == '') $this->form_validation->set_rules('select_brand_'.$counter, 'Brand Name', 'required');
				if($this->input->post('select_brand_'.$counter) == '') $this->form_validation->set_rules('brand_name_'.$counter, 'Brand Name', 'required');
				if ($this->form_validation->run() == FALSE)
				{
					$this->load->view('common/header1',$data);
					$this->load->view('brand_onboarding/brand_step1');
					$this->load->view('common/footer1');
				}else{
						
					$brand_id = $this->input->post('select_brand_'.$counter);
					$brand_name = $this->input->post('brand_name_'.$counter);
					$logo_img = $this->input->post('logo_name_'.$counter);
					$cover_img = $this->input->post('cover_name_'.$counter);
					$brand_store_id = $this->input->post('brand_store_id_'.$counter);
					$logo='';$cover='';
					// echo "counter=".$counter."--brand_id=".$brand_id."--brand_name==".$brand_name."--logo_img==".$logo_img."--cover_img==".$cover_img;
					if($brand_id != ''){ $brand_name = $brand_id; }
					$config['upload_path'] = 'assets/brand_logo/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']	= '13000';
					$config['max_width']  = '1500';
					$config['max_height']  = '1500';
					$config['file_name'] = time();
					$ok =1;
					
					if(!empty($_FILES)){
						
					if(@$_FILES['logo_img_'.$counter]['name']!=''){
						// echo "comming in name";echo "<pre>";print_r($_FILES);exit;
						$this->upload->initialize($config);
						$file_name = 'logo_img_'.$counter;
						if(!($this->upload->do_upload($file_name))){
							
								$data['error_logo_'.$counter] = array($this->upload->display_errors());
								$ok =0;
								// echo "error in image";echo "<pre>";print_r($data['error']);exit;
								$this->load->view('common/header1',$data);
								$this->load->view('brand_onboarding/brand_step1');
								$this->load->view('common/footer1');
							
							}else{
								// echo "image uploaded";exit;
							 $logo = $this->upload->data('file_name'); 
							 $test2 = getimagesize('assets/brand_logo/' . $logo);
								$width = $test2[0];
								$height = $test2[1];
								if (($width != 530 && $height != 530)){
									$ok =0;
									$error = array('Logo Image Height And Width Should Be 530 X 530');
									$data['error_logo_'.$counter] = $error; 
									$this->load->view('common/header1',$data);
									$this->load->view('brand_onboarding/brand_step1');
									$this->load->view('common/footer1');
									
								}
							}
						}
					}
					
					$config2['upload_path'] = 'assets/brand_cover/';
					$config2['allowed_types'] = 'gif|jpg|png|jpeg';
					$config2['max_size']	= '13000';
					$config2['max_width']  = '1500';
					$config2['max_height']  = '1500';
					$config2['file_name'] = time();
					
					if(!empty($_FILES) && $ok!=0){
					if(@$_FILES['cover_img_'.$counter]['name']!=''){
						$this->upload->initialize($config2);
						if(!($this->upload->do_upload('cover_img_'.$counter))){
								$ok =0;
								$data['error_cover_'.$counter] = array($this->upload->display_errors()); 
								$this->load->view('common/header',$data);
								$this->load->view('brand_onboarding/brand_step1');
								$this->load->view('common/footer');
								
							}else{
							 $cover= $this->upload->data('file_name'); 
							 $test3 = getimagesize('assets/brand_cover/' . $cover);
								$width = $test3[0];
								$height = $test3[1];
								if (($width != 1366 && $height != 371)){
									$ok =0;
									$error = array('Cover Image Height And Width Should Be 1366 X 371');
									$data['error_cover_'.$counter] = $error; 
									$this->load->view('common/header1',$data);
									$this->load->view('brand_onboarding/brand_step1');
									$this->load->view('common/footer1');
									 
								}
							}
						}
					}
					
					if($logo == ''){ $logo = $logo_img; }
					if($cover == ''){ $cover = $cover_img; }
					//code for saving brand_Store data 
					if($ok!=0){
						$brand_id = $this->session->userdata('user_id');
						$data['save_brand'] = $this->Brand_details_model->save_brand_store($brand_name,$logo,$cover,$brand_store_id);
						$data['brand_info'] = $this->Brand_details_model->get_store_data($brand_id);
						// echo $brand_id;echo "<pre>";print_r($data['brand_info']);exit;
						$this->load->view('common/header1');
						$this->load->view('brand_onboarding/brand_step1',$data);
						$this->load->view('common/footer1');
					}
				}
			}else {
				$brand_id = $this->session->userdata('user_id');
				$data['brand_info'] = $this->Brand_details_model->get_store_data($brand_id);
				// echo $brand_id;echo "<pre>";print_r($data['brand_info']);exit;
				$this->load->view('common/header1');
				$this->load->view('brand_onboarding/brand_step1',$data);
				$this->load->view('common/footer1');
			}
			
		}
	}*/
	
	function brand_step1(){
		$data = array();
		$brand_id = $this->session->userdata('user_id');
		$data['brand_info_table'] =  $this->Brand_details_model->get_brand_info($brand_id);
		$data['brand_info'] = $this->Brand_details_model->get_store_data($brand_id);
		if(isset($_REQUEST['test'])){
			$test = $_REQUEST['test'];
			$data['brand_info_table'][0]['step_completed'] = 1;
		}else $test = 0;
		
			if(!$this->session->userdata('user_id')){
				
				$this->load->view('login');
				
			}else if($data['brand_info_table'][0]['is_form_completed'] == 1){
				
				redirect('brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed'].'','refresh');
				// header('Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed'].'');
				
			}else if($data['brand_info_table'][0]['step_completed'] <= 1 && $data['brand_info_table'][0]['is_form_completed'] != 1 && $data['brand_info_table'][0]['step_completed'] != 1 ){
				
				redirect('brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed'].'','refresh');
				// header('Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed'].'');
				
			}else{ if($this->input->post()){
				// echo "comming in if";exit;
				$counter = $this->input->post('counter');
				$brand_store_id = $this->input->post('brand_store_id');
				$j=$counter;
				$this->form_validation->set_rules('brand_name_1', 'Brand Name', 'required');
				
				if ($this->form_validation->run() == FALSE)
				{
					$this->load->view('common/header1',$data);
					$this->load->view('brand_onboarding/brand_step1');
					$this->load->view('common/footer1');
				}else{
					$brand_data = array();
					for($i=1;$i<=$counter;$i++){
						if($this->input->post('brand_name_'.$i) != ''){
							$brand_data[$i]['brand_name'] = $this->input->post('brand_name_'.$i);
							$data['save_brand'] = $this->Brand_details_model->save_brand_store($brand_data[$i]['brand_name'],'','',$brand_store_id);
							$j--;
						}
					}
					
					
					
					//code for saving brand_Store data 
					
						$brand_id = $this->session->userdata('user_id');
						$data['brand_info'] = $this->Brand_details_model->get_store_data($brand_id);
						// $this->load->view('common/header1');
						// $this->load->view('brand_onboarding/brand_step2',$data);
						// $this->load->view('common/footer1');
						redirect('brand_onboarding/Store_registration/brand_step2'.'','refresh');
			
				}
			}else {
				$brand_id = $this->session->userdata('user_id');
				$data['brand_info'] = $this->Brand_details_model->get_store_data($brand_id);
				// echo $brand_id;echo "<pre>";print_r($data['brand_info']);exit;
				$this->load->view('common/header1');
				$this->load->view('brand_onboarding/brand_step1',$data);
				$this->load->view('common/footer1');
			}
			
		}
		
	}
	
	function add_row(){
	
		$counter = $this->input->post('counter');
		
		$brand_list = $this->Brand_details_model->get_brand_list();
		$brand_list_option = '';
			if(!empty($brand_list)){
				foreach($brand_list as $val){
					
				$brand_list_option = $brand_list_option.'<option value="'.$val['brand_store_name'].'">'.$val['brand_store_name'].'</option>';
				
				}
			}
			
		$logo = "'logo_img'";
		$cover = "'cover_img'";
		$error_cover_ = '';
		$error_logo_ = '';
		// echo $counter;exit;
		$html = '<div id="new_row_'.$counter.'" class="panel panel-default">
				<form role="form" id="brand_form" enctype="multipart/form-data" method="POST">
				<div class="panel-body">
				<div id="row_'.$counter.'">
				 <div class="form-group row">
						<label class="control-label col-sm-3">Brand Name (Display on Web)<span class="text-red">*</span></label>
						<div class="col-sm-3">
						  <select class="form-control" name="select_brand_'.$counter.'">
							<option disabled="" selected="" value="select_brand">Select Band</option>
						   '.$brand_list_option.'
						  </select>
						  <span class="error text-red"><?php echo form_error("select_brand_'.$counter.'"); ?></span>
						</div>
						<label class="control-label col-sm-1" style="text-align:center;">OR</label>
						<div class="col-sm-3">
						  <input type="text" name="brand_name_'.$counter.'" id="brand_name_'.$counter.'" class="form-control" placeholder="Brand Name"/>
						   <span class="error text-red"><?php echo form_error("brand_name_'.$counter.'"); ?></span>
						</div>
						<div class="col-sm-2">
						<!-- <a class="btn btn-secondary btn-sm" onclick="add_row('.$counter.')">+ Add New</a>&nbsp&nbsp
						<a class="btn btn-secondary btn-sm" onclick="remove_row('.$counter.')">- Remove</a> -->
						</div>
					  </div>
					  <div class="form-group row">
						<label class="control-label col-sm-3">Upload Logo<span class="text-red">*</span></label>
						<div class="col-sm-3"><input required type="file" name="logo_img_'.$counter.'" id="logo_img_'.$counter.'" class="" placeholder="Upload Logo" onchange="change_img('.$counter.','.$logo.')"/>
						<!-- <div class="upload-logo" id="upload_logo_'.$counter.'" >Upload Logo<span class="text-red">*</span></div>-->
						<span class="help-block">WD/HT: 530px/530px<br>Allowed format: .jpg, .jpeg, .png, .gif</span>
						</div> 
						<div class="error text-red"><?php if(isset($error_logo_.$counter))echo '.$error_logo_.$counter.'[0]; ?></div>
						
						<div class="col-sm-6" id="show_logo_'.$counter.'" style="display:none;"><img class="brand-logo" name ="brand_logo_'.$counter.'" id="previewing_logo_'.$counter.'" src="" value="" /></div>
					  </div>
					  <div class="form-group row">
						<label class="control-label col-sm-3">Upload Cover<span class="text-red">*</span></label>
						<div class="col-sm-3"><input required type="file" name="cover_img_'.$counter.'" id="cover_img_'.$counter.'" class="" placeholder="Upload Cover" onchange="change_img('.$counter.','.$cover.')" />
						<!-- <div class="upload-cover" id="upload_cover_'.$counter.'" >Upload Cover<span class="text-red">*</span></div> -->
						<span class="help-block">WD/HT: 1366px/370px<br>Allowed format: .jpg, .jpeg, .png, .gif</span>
						</div>
						<div class="error text-red"><?php if(isset($error_cover_.$counter))echo '.$error_cover_.$counter.'[0]; ?></div>
						
						<div class="col-sm-6" id="show_cover_'.$counter.'" style="display:none;" ><img class="brand-cover" name ="brand_cover_'.$counter.'" id="previewing_cover_'.$counter.'" src="" value="" /></div>
						
					  </div>
					</div>
				</div><div class="panel-footer text-right">
						<a class="btn btn-secondary btn-sm" onclick="add_row('.$counter.')">+ Add New</a>&nbsp&nbsp
						<a class="btn btn-secondary btn-sm" onclick="remove_row('.$counter.')">- Remove</a>
							<button class="btn btn-primary btn-sm" tabindex="30" name="submit" value="'.$counter.'" type="submit"><i class="fa fa-save"></i> Save</button>
						   </div></form></div>';
				echo $html;
	}
  
	function remove_row(){
		$brand_name = $this->input->post('brand_name');
		$brand_id = $this->session->userdata('user_id');
		$brand_list = $this->Brand_details_model->remove_row($brand_name,$brand_id);
		echo "data deleted successfully";
	}
	
	function add_row_new(){
	
		$counter = $this->input->post('counter');
		
		$html = ' <div id="new_row_'.$counter.'" class="panel panel-default" style="border:none;">
						<div class="form-group row" >
						<label class="control-label col-sm-3"></label>
						<div class="col-sm-3">
						  <input type="text" name="brand_name_'.$counter.'" id="brand_name_'.$counter.'" class="form-control" placeholder="Brand Name"/>
						  <span class="error text-red"><?php echo form_error("brand_name_'.$counter.'"); ?></span>
						</div>

						<div class="col-sm-2">
							<span onclick="remove_row('.$counter.');" class="btn btn-primary btn-xs btn-delete"><i class="fa fa-minus"></i></span>
						</div>
					  </div>
					  </div>';
		echo $html;
	}
	
	public function download_cod_sheet(){


			$this->load->helper('download');

			$data = @file_get_contents(base_url()."assets/cod_sheet/sample_cod_sheet.xlsx"); // Read the file's contents			

			$name = 'sample_cod_sheet.xlsx';

			$result = force_download($name, $data);



			if($result)
			{

				// $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Sample CueSheet downloaded successfully!</div>');

			}else
			{				

				$msg = '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5> Downloading Sample Cue Sheet</div>';

				$this->session->set_flashdata('feedback',$msg);

			}

		}
		
		public function download_shipping_sheet(){


			$this->load->helper('download');

			$data = file_get_contents(base_url()."assets/shipping_sheet/sample_shipping_sheet.xls"); // Read the file's contents			

			$name = 'sample_shipping_sheet.xls';

			$result = force_download($name, $data);



			if($result)
			{

				// $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Sample CueSheet downloaded successfully!</div>');

			}else
			{				

				$msg = '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5> Downloading Sample Cue Sheet</div>';

				$this->session->set_flashdata('feedback',$msg);

			}

		}
		
	public function brand_step5(){
		$brand_id = $this->session->userdata('user_id');
		$data['brand_info_table'] =  $this->Brand_details_model->get_brand_info($brand_id);
		
		if(isset($_REQUEST['test'])){
			$test = $_REQUEST['test'];
			$data['brand_info_table'][0]['step_completed'] = 5;
		}else $test = 0;
		
			if(!$this->session->userdata('user_id')){
				$this->load->view('login');
			}else if($data['brand_info_table'][0]['is_form_completed'] == 1){
				redirect('brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed'].'','refresh');
				// header('Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed'].'');
			}else if($data['brand_info_table'][0]['step_completed'] <= 5 && $data['brand_info_table'][0]['is_form_completed'] != 1 && $data['brand_info_table'][0]['step_completed'] != 5 ){
				redirect('brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed'].'','refresh');
				// header('Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed'].'');
			}else{
			$brand_id = $this->session->userdata('user_id');
			if($this->input->post()){
				$this->form_validation->set_rules('is_confirm', 'Confirm Information', 'required');
				
					if ($this->form_validation->run() == FALSE)
					{
						$this->load->view('common/header1',$data);
						$this->load->view('brand_onboarding/brand_step5');
						$this->load->view('common/footer1');
					}else {
							$config['upload_path'] = 'assets/cheque/';
							$config['allowed_types'] = 'gif|jpg|png|jpeg';
							$config['max_size']	= '2048';
							//$config['max_width']  = '1500';
							//$config['max_height']  = '1500';
							$config['file_name'] = time();
							$ok =0;
									
							if(!empty($_FILES)){
								if(@$_FILES['cheque']['name']!=''){
									$this->upload->initialize($config);
									if(!($this->upload->do_upload('cheque'))){
											$data['error'] = array($this->upload->display_errors()); 
											$this->load->view('common/header1',$data);
											$this->load->view('brand_onboarding/brand_step5');
											$this->load->view('common/footer1');
											$ok=0;
									}else{
									 $cheque = $this->upload->data('file_name'); 
									 $ok=1;
									}
								}else {
										$error = array('please upload image');
										$data['error'] = $error; 

								}
							}
									
							$config2['upload_path'] = 'assets/pancard/';
							$config2['allowed_types'] = 'gif|jpg|png|jpeg';
							$config2['max_size']	= '2048';
							//$config2['max_width']  = '1500';
							//$config2['max_height']  = '1500';
							$config2['file_name'] = time();
							$ok2 =0;
									
							if(!empty($_FILES)){
								if(@$_FILES['pancard']['name']!=''){
									$this->upload->initialize($config2);
									if(!($this->upload->do_upload('pancard'))){
											$this->load->view('common/header1',$data);
											$this->load->view('brand_onboarding/brand_step5');
											$this->load->view('common/footer1');
											$ok2 = 0;
										}else{
										 $pancard = $this->upload->data('file_name'); 
										 $ok2 = 1;
										}
									}else {
										$error = array('please upload image');
										$data['error'] = $error; 

									}
							}
							$is_confirm = $this->input->post('is_confirm');		
									
							if($ok == 1 && $ok2 == 1){
								$this->send_mail_to_alliances();								
								$data = $this->Brand_details_model->update_brand_images($cheque,$pancard,$brand_id,$is_confirm);
								redirect('brand_onboarding/Store_registration/brand_step6'.'','refresh');	
								// header("Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/brand_step6");
							}else 
							{
								
								$this->load->view('common/header1');
								$this->load->view('brand_onboarding/brand_step5',$data);
								$this->load->view('common/footer1');
							}
					}
			}else{
					
				$this->load->view('common/header1');
				$this->load->view('brand_onboarding/brand_step5',$data);
				$this->load->view('common/footer1');
			}
		}
	}
	
	public function brand_step6(){
		
		$brand_id = $this->session->userdata('user_id');
		$data['brand_info_table'] =  $this->Brand_details_model->get_brand_info($brand_id);
		
		if($data['brand_info_table'][0]['step_completed'] > 0 && $data['brand_info_table'][0]['is_form_completed'] != 1 && $data['brand_info_table'][0]['step_completed'] != 6){
			
			redirect('brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed'].'','refresh');
			// header('Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed'].'');
		}else if($data['brand_info_table'][0]['is_form_completed'] == 1 && $data['brand_info_table'][0]['step_completed'] != 6){
			
			redirect('brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed'].'','refresh');
			// header('Location: http://www.scnest.in/sc_admin/brand_onboarding/Store_registration/brand_step'.$data['brand_info_table'][0]['step_completed'].'');
		}else if(!$this->session->userdata('user_id')){
			
			$this->load->view('login');
			
		}else{
			$this->load->view('common/header1');
			$this->load->view('brand_onboarding/brand_step6',$data);
			$this->load->view('common/footer1');
		}
	}
	
	
	public function download_contract(){

			$brand_id = $this->session->userdata('user_id');
			$brand_info_table =  $this->Brand_details_model->get_brand_info($brand_id);
			
			$this->load->helper('download');

			$data = file_get_contents(base_url().'assets/brand_contract/'.$brand_info_table[0]['contract_name']); // Read the file's contents			

			$name = 'brand_contract.pdf';

			$result = force_download($name, $data);



			if($result)
			{

				// $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Sample CueSheet downloaded successfully!</div>');

			}else
			{				

				$msg = '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5> Downloading Sample Cue Sheet</div>';

				$this->session->set_flashdata('feedback',$msg);

			}

		}
		
	function send_mail_to_alliances(){
		$first_name = $this->session->userdata('first_name');
		$config['protocol'] = 'smtp';
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;
		$config['smtp_host'] = $this->config->item('smtp_host');
		$config['smtp_user'] = $this->config->item('smtp_user');
		$config['smtp_pass'] = $this->config->item('smtp_pass');
		$config['smtp_port'] = $this->config->item('smtp_port');

		$email_id2 = array('arun@stylecracker.com');
		$this->email->initialize($config);
		$email = 'alliances@stylecracker.com';
		$this->email->from($this->config->item('from_email'), 'Stylecracker');
		if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){	       
	        $this->email->to($this->config->item('sc_test_emaild'));
			$this->email->bcc($this->config->item('sc_testcc_emaild'));
	    }else{
			$this->email->to($email);
			$this->email->bcc($email_id2);
		}
		$this->email->subject('Brand Notification');
		$this->email->message('Hi Team,</br><br> '.$first_name.' has completed their registration process with StyleCracker.</br>
Please have a look to follow through with the verification process.</br></br>Thank you!');

		if($this->email->send())
		{
			return "Email Sent";
		}else
		{
			return "Email Error";
		}
	}

}
