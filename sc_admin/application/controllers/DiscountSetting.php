<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DiscountSetting extends MY_Controller {

	function __construct(){
       parent::__construct();
       $this->load->model('productinventorynew_model');
       $this->load->model('Discountsetting_model');
	   $this->load->library('form_validation');

   }

	public function index($offset = null)
	{
	 	if(!$this->session->userdata('user_id')){
			$this->load->view('login');
			}else{
				$data = array();
				if($this->has_rights(64) == 1){

					$data['paid_store'] = $this->productinventorynew_model->get_paid_stores();
					$data['discount_type'] = $this->Discountsetting_model->get_discount_type();
					 //print_r($data['paid_store']);exit;

					$this->load->view('common/header');
					$this->load->view('discount_setting_view',$data);
					$this->load->view('common/footer');
				}else{
					$this->load->view('common/header');
					$this->load->view('not_permission');
					$this->load->view('common/footer');
			}
		}
	}

	public function get_products(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$store_id = $this->input->post('store_id');
			$product_id = $this->input->post('product_id');
			$min_offset = $this->input->post('min_offset');
			$max_offset = $this->input->post('max_offset');
			$discountType = $this->input->post('discountType');
			$pcount = $this->productinventorynew_model->get_products_count($store_id,$product_id);
			$result = $this->productinventorynew_model->get_products($store_id,$product_id,'',$min_offset,50);

			$producthtml = '';$i=1;
			if(!empty($result)){ if($min_offset >0) { $i = ($min_offset*50)+1; } else{ $i=1; } $k = 1;
				foreach($result as $val){					
					$data = $this->Discountsetting_model->get_product_discount($store_id,$discountType,$val['product_id']);
					$size_text = $val['size_text']!='' ? explode(",",$val['size_text']) : array();
					$stock_count = $val['stock_count']!='' ? explode(",",$val['stock_count']) : array();
					$producthtml = $producthtml.'<tr>';
					$producthtml = $producthtml.'<td>'.$i.'</td>';
					$producthtml = $producthtml.'<td>'.$val['product_id'].'</td>';
					$producthtml = $producthtml.'<td>'.$val['product_sku'].'</td>';
					$producthtml = $producthtml.'<td>'.$val['name'].'</td>';	
					if($val['compare_price'] == '0'){$producthtml = $producthtml.'<td>'.$val['price'].'</td>';}else{$producthtml = $producthtml.'<td>'.$val['compare_price'].'</td>';
					}
					$producthtml = $producthtml.'<td><strong>'.$val['price'].'</strong><br>'.'('.$val['discount_start_date'].'<br>-'.$val['discount_end_date'].')</td>';				

				  $producthtml = $producthtml.'<form name="myform" id="myform" class="frm-search" method="post">
					<td>
						<input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm datetimepicker" id="date_from'.$i.'" value="'.$data['date_from'].'">
					</td>
					<td>
						<input type="text" name="date_to" placeholder="Date To" class="form-control input-sm datetimepicker" id="date_to'.$i.'" value="'.$data['date_to'].'">
					</td>
				  ';
					$producthtml = $producthtml.'<td><input type="text" name="discount'.$i.'" id="product_discount'.$i.'" value="'.$data['discount_percent'].'"></td>';
					$producthtml = $producthtml.'<td><input type="button" name="save_product_discount" id="save_product_discount" value="Save" onclick="save_product_discount('.$val['product_id'].','.$i.');"></td>';
					$producthtml = $producthtml.'</tr>';
					$i++;
				}
			}else{
				$producthtml = $producthtml.'<tr>';
				$producthtml = $producthtml.'<td colpan="6">Sorry no records found</td>';
				$producthtml = $producthtml.'</tr>';
			}
			if($i <= $pcount){
			$producthtml = $producthtml.'<tr id="remove'.$min_offset.'"><td colspan="5"><div id="load_more" onclick="load_more();" class="btn btn-primary btn-xs">Load More</div></td></tr>';
			}
			$producthtml = $producthtml.'<input type="hidden" name="max_offset" id="max_offset" value="'.$pcount.'">';

			echo $producthtml;
		}
	}

	public function save_product_discount(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$store_id = $this->input->post('store_id');
			$date_from1 = $this->input->post('date_from');
			$date_to2 = $this->input->post('date_to');
			$product_id = $this->input->post('product_id');
			$discount_percent = $this->input->post('discount');
			$discountType = $this->input->post('discountType');
			$date_from = strtotime($date_from1);
			$date_from = date('Y-m-d H:i:s',$date_from);

			$date_to = strtotime($date_to2);
			$date_to = date('Y-m-d H:i:s',$date_to);
			$res = $this->Discountsetting_model->save_product_discount($store_id,$date_from,$date_to,$product_id,$discount_percent,$discountType);
			if($res == 1){
				//echo '<div class="alert alert-success alert-dismissable"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> discount added successfully!</div>';
				$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>discount Added Successfully</div>');
				echo  1;

			}else
			{  $this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>Something goes wrong Please try again</div>');

                echo 2;
			}

		}

	}

	public function get_brandsetting()
	{
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$store_id = $this->input->post('store_id');
			$producthtml = '';
			$i=1;

			$producthtml = $producthtml.'
				<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<div class="input-group">
						<label>Discount %</label>
    					<input type="text" name="discount" id="discount" placeholder="Discount %" class="form-control input-xs">
						</div>
					</div>
				</div>
  				<div class="col-md-2">
					<div class="form-group"><label>Date From</label>
						<input type="text" name="date_from" placeholder="Date From" class="form-control input-sm datetimepicker" id="date_from" value="">
					</div>

				</div>
			<div class="col-md-2">
					<div class="input-group">
					<label>Date To</label>
					<input type="text" name="date_to" placeholder="Date To" class="form-control input-sm datetimepicker" id="date_to" value="">
					</div>
				</div>
				<div class="col-md-2">
					<div class="input-group"><label>&nbsp;</label><br/>
					<input type="button" name="save" id="save" value="Save" class="btn btn-primary btn-sm" onclick="save_brand_discount('.$store_id.');">
		  		      </div> 
		  	    </div>  </div> </div>';

    		echo $producthtml;
		}

	}


	public function get_brand_discount_ontotal(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$store_id = $this->input->post('store_id');
			$discountType = $this->input->post('discountType');
			$data = $this->Discountsetting_model->get_brand_discount_ontotal($store_id,$discountType);
			//echo "<pre>";print_r($data);exit;
			$producthtml = '';
			$i=1;
		 $producthtml = $producthtml.'<form name="myform" id="myform" class="frm-search" method="post">
			<div class="row">
			  <div class="col-md-2">
				<div class="input-group"><label>date From</label>
				  <input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm datetimepicker" id="date_from" value="'.$data['date_from'].'">
				</div>
			  </div>
			  <div class="col-md-2">
				<div class="input-group"><label>date To</label>
				  <input type="text" name="date_to" placeholder="Date To" class="form-control input-sm" id="date_to" value="'.$data['date_to'].'">
				  <div class="input-group-btn">
				  </div>
				</div>
			  </div>';
			$producthtml = $producthtml.'<div class="col-md-2"><div class="input-group"><label>Max Amount</label>
    					<input type="number" name="max_amount" id="max_amount" value="'.$data['max_amount'].'" placeholder="Max Amount"  ></div></div>';
			$producthtml = $producthtml.'<div class="col-md-2"><div class="input-group"><label>Discount %</label>
    					<input type="number" name="discount" id="discount" value="'.$data['discount_percent'].'" placeholder="Discount %"  ></div></div>';
			$producthtml = $producthtml.'<div class="col-md-2"><div class="input-group"><label>&nbsp;</label><br/><input type="button" class="btn btn-primary btn-sm" name="save" id="save" value="Save" onclick="save_brand_discount_ontotal('.$store_id.');"></div></div></div></form>';
						/*$producthtml = $producthtml.' <script type="text/javascript">
                            $(function () {
                              $("#date_from").datepicker({ format: "yyyy-mm-dd", endDate: "+0d", autoclose: true});
                              $("#date_to").datepicker({ format: "yyyy-mm-dd", endDate: "+0d", autoclose: true });  });</script>';*/


    		echo $producthtml;
		}
	}


	public function save_brand_discount_ontotal(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{
			$store_id = $this->input->post('store_id');
			$date_from = $this->input->post('date_from');
			$date_to = $this->input->post('date_to');
			$max_amount = $this->input->post('max_amount');
			$discount_percent = $this->input->post('discount');
			$discountType = $this->input->post('discountType');

			$res = $this->Discountsetting_model->save_brand_discount_ontotal($store_id,$date_from,$date_to,$max_amount,$discount_percent,$discountType);
			//print_r($res);exit;
			if($res == 1){
				//echo '<div class="alert alert-success alert-dismissable"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> discount added successfully!</div>';
				$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>discount Added Successfully</div>');
				echo  1;

			}else
			{  $this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>Something goes wrong Please try again</div>');

                echo 2;
			}

		}

	}

	public function get_stylecracker_discount(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{


			$discountType = $this->input->post('discountType');
			$store_id = $this->input->post('store_id');
			$date_from = $this->input->post('date_from');
			$date_to = $this->input->post('date_to');
			//$data = $this->discountSetting_model->get_stylecracker_discount($discountType,$store_id,$date_to,$date_from);
			$producthtml = '';
			$i=1;
		 $producthtml = $producthtml.'<form name="myform" id="myform" class="frm-search" method="post">
			<div class="row" id="dateDiv">
			  <div class="col-md-2">
				<div class="input-group"><label>Date From</label>
				  <input type="text" name="date_from"  placeholder="Date From" class="form-control input-sm datetimepicker" id="date_from" value="">
				</div>
			  </div>
			  <div class="col-md-2">
				<div class="input-group"><label>Date To</label>
				  <input type="text" name="date_to" placeholder="Date To" class="form-control input-sm datetimepicker" id="date_to" value="">
				  <div class="input-group-btn"></div>
				</div>
			  </div>';
			$producthtml = $producthtml.'<div class="col-md-2"><div class="input-group"><label>Max Amount</label>
    					<input type="number" name="max_amount" id="max_amount" value="max_amount" placeholder="Max Amount"  ></div></div>';
			$producthtml = $producthtml.'<div class="col-md-2"><div class="input-group"><label>Discount %</label>
    					<input type="number" name="discount" id="discount" value="discount"  placeholder="Discount %"  ></div></div>';
			$producthtml = $producthtml.'<div class="col-md-2"><div class="input-group"><label>&nbsp;</label><br/><input type="button" class="btn btn-primary btn-sm" name="save" id="save" value="Save" onclick="save_stylecracker_discount('.$store_id.');"></div></div></div></form>';
						$producthtml = $producthtml.' ';


    		echo $producthtml;
		}
	}

	public function save_stylecracker_discount(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{

			$store_id = $this->input->post('store_id');
			$date_from = $this->input->post('date_from');
			$date_to = $this->input->post('date_to');
			$max_amount = $this->input->post('max_amount');
			$discount_percent = $this->input->post('discount');
			$discountType = $this->input->post('discountType');

			$res = $this->Discountsetting_model->save_stylecracker_discount($store_id,$date_from,$date_to,$max_amount,$discount_percent,$discountType);


			if($res == 1)
			{

				 $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>discount Added Successfully</div>');

				echo  1;

			}
			else
			{  $this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>Something goes wrong Please try again</div>');

                echo 2;
			}

		}

	}

	public function save_brand_discount(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{

			$store_id = $this->input->post('store_id');
			$date_from = $this->input->post('date_from');
			$date_to = $this->input->post('date_to');
			$discount_percent = $this->input->post('discount');
			$discountType = $this->input->post('discountType');

			$res = $this->Discountsetting_model->save_brand_discount($store_id,$date_from,$date_to,$discount_percent,$discountType);


			if($res == 1)
			{
				echo  1;

			}if($res == 3)
			{
				/* $this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>Discount already set for the store</div>');*/
				echo 3;
			}else if($res == 2)
			{  /*$this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>Something goes wrong Please try again</div>');*/
				/*echo '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>Something goes wrong Please try again</div>';*/
                echo 2;
			}

		}

	}

	public function discount()
	{
		$data_post['paid_store'] = $this->productinventorynew_model->get_paid_stores();
		$data_post['discount_type'] = $this->Discountsetting_model->get_discount_type();

		//$config['total_rows'] = count($this->Discountsetting_model->getDiscounts());
        //$total_rows = $config['total_rows'];
        $get_data = $this->Discountsetting_model->getDiscounts();
      //$count = $config['total_rows'];
        $data_post['discount'] = $get_data;

        /*$i = 1;
        $discount_html=
             //echo "<pre>";print_r($get_data);
       if(!empty($get_data)) {
           	 	foreach($get_data as $val){
            	  $discount_html= $discount_html.'<tr>
            <td>"'.$val['id'].'"</td>
            <td>"'.$val['discount_name'].'"</td>
            <td>"'.$val['company_name'].'"</td>
            <td"'.$val['discount_percent'].'" </td>
            <td>"'.$val['max_amount'].'"</td>
            <td></td>
            <td>"'.$val['start_date'].'"</td>
            <td>"'.$val['end_date'].'"</td>
             <td></td></tr>';
        		$i++; }
        	}

				else{
				$discount_html = $discount_html.'<tr>';
				$discount_html = $discount_html.'<td colpan="6">Sorry no records found</td>';
				$discount_html = $discount_html.'</tr>';
			}
         	echo $discount_html;*/

			$this->load->view('common/header');
			$this->load->view('discount_setting',$data_post);
			$this->load->view('common/footer');

	}

	public function display_records()
	{	$discount_type = $this->input->post('discountType');
		$min_offset = $this->input->post('min_offset');
		$max_offset = $this->input->post('max_offset');
		//echo $discount_type;exit;
		$get_data = $this->Discountsetting_model->getDiscounts('','',$discount_type,$min_offset,50);
		$pcount = count($get_data);
        $i = 1;
        $discount_html= '';
             //echo "<pre>";print_r($get_data);
       if(!empty($get_data)) { if($min_offset >0) { $i = ($min_offset*50)+1; } else{ $i=1; } $k = 1;
           	 	foreach($get_data as $val){
            $discount_html= $discount_html.'<tr>
            <td>'.$i.'</td>
            <td>'.$val['discount_name'].'</td>
			<td>'.$val['company_name'].'</td>
			<td>'.$val['product_name'].'</td>
             <td>'.$val['product_id'].'</td>
			 <td>'.$val['discount_percent'].' </td>
          	 <td>'.$val['max_amount'].'</td>
            <td>'.$val['start_date'].'</td>
            <td>'.$val['end_date'].'</td>


             </tr>';
        		$i++; }
        	}

				else{
				$discount_html = $discount_html.'<tr>';
				$discount_html = $discount_html.'<td colpan="6">Sorry no records found</td>';
				$discount_html = $discount_html.'</tr>';
			}
			if($pcount>0){
				$discount_html = $discount_html.'<tr id="remove'.$min_offset.'"><td colspan="5"><div id="load_more" onclick="load_more();" class="btn btn-primary 	btn-xs">Load More</div></td></tr>';
			}	
		if($i <= $pcount){
			$discount_html = $discount_html.'<tr id="remove'.$min_offset.'"><td colspan="5"><div id="load_more" onclick="load_more();" class="btn btn-primary btn-xs">Load More</div></td></tr>';
			}
			$discount_html = $discount_html.'<input type="hidden" name="max_offset" id="max_offset" value="'.$pcount.'">';
        echo $discount_html;
	}

}
