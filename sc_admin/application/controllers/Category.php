<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Category_model');

    }

 /*---------Category submodule functionalities----------*/

    public function index($offset = null)
    {

        if(!$this->session->userdata('user_id')){

            $this->load->view('login');

        }else{

            $data_post = array();

            //$data['menu_permissions'] = $this->data['users_menu'];

            if($this->has_rights(1) == 1){

                $search_by = $this->input->post('search_by');
                $table_search = $this->input->post('table_search');
                $paginationUrl = 'category/index/';
                $uri_segment = 3;
                $config['per_page'] = 1000;
                $config['total_rows'] = count($this->Category_model->get_all_categories($search_by,$table_search,'',''));
                $total_rows = $config['total_rows'];
                $get_users = $this->Category_model->get_all_categories($search_by,$table_search,$offset,$config['per_page']);
                $count = $config['total_rows'];
                $data_post['categories'] = $get_users;

                $this->customPagination($paginationUrl,$count,$uri_segment,$get_users,$total_rows);
                $this->load->view('common/header',$data_post);
                $this->load->view('category_view');
                $this->load->view('common/footer');

            }else{

                $this->load->view('common/header');
                $this->load->view('not_permission');
                $this->load->view('common/footer');
            }
         }


    }

    public function category_addedit($id=NULL)
    {

        $data_post = array();
        $loggedInUser=$this->session->userdata('user_id');

         if($id==NULL)
        {
            if($this->has_rights(2) == 1)
            {
                    if($this->input->post())
                    {

                        $this->form_validation->set_rules('categoryName', 'Category Name', 'required|is_unique[product_category.name]');
                        $this->form_validation->set_rules('categorySlug', 'Category Slug', 'required|is_unique[product_category.slug]');

                        if ($this->form_validation->run() == FALSE)
                        {
                            $this->load->view('common/header');
                            $this->load->view('category');
                            $this->load->view('common/footer');

                        }else{

                                $categoryName = $this->input->post('categoryName');
                                $categorySlug = $this->input->post('categorySlug');
                                $active = $this->input->post('active');

                                $catCount = $this->Category_model->checkMultilpeCategroy($this->input->post('categoryName'));


                                if($catCount->catCount > 0)
                                {

                                    $this->session->set_flashdata('feedback', '<i class="fa fa-check sign"></i><strong>Error!</strong> Category already exits!');
                                }
                                else
                                {
                                    $data_post = array(
                                        'name'=>$categoryName,
                                        'slug' => strtolower($categorySlug),
                                        'status'=> $active,
                                        'created_by'=> $loggedInUser,
                                        'created_datetime'=> date("Y-m-d H:i:s"),
                                        'modified_by'=> $loggedInUser,
                                        'modified_datetime'=> date("Y-m-d H:i:s")
                                        );

                                    $created = $this->Category_model->new_category($data_post);
                                     if(!is_dir('../assets/images/products_category/category/'.$created)) 
                                        {
                                          mkdir('../assets/images/products_category/category/'.$created,0755,TRUE);
                                        } 
                                    if($_FILES['category_img']['name']!='')
                                    {
                                        $config['upload_path'] = '../assets/images/products_category/category/'.$created;
                                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                                        $config['max_size'] = '530'; 
                                        $config['max_width']  = '530';
                                        $config['file_name'] = $created;
                                        $config['overwrite'] = TRUE;
                                        $this->upload->initialize($config);
                                        if(!$this->upload->do_upload('category_img')){  }
                                    }

                                    if($created){

                                            $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Category added successfully!</div>');
                                            redirect('/category');
                                    }
                                    else
                                    {
                                         $this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5> The server is not responding, try again later.</div>');
                                            redirect('/category');
                                    }
                                }
                            }
                        }else
                        {
                            $this->load->view('common/header');
                            $this->load->view('category',$data_post);
                            $this->load->view('common/footer');
                        }
                }else
                {
                    $this->load->view('common/header');
                    $this->load->view('not_permission');
                    $this->load->view('common/footer');
                }

            }else
            {
                    if($this->has_rights(3) == 1)
                    {
                        $data_post['category_details'] = $this->Category_model->get_category_by_ID($id);
                        if($this->input->post())
                        {

                            $this->form_validation->set_rules('categoryName', 'Category Name', 'required');
                            $this->form_validation->set_rules('categorySlug', 'Category Slug', 'required');

                            if ($this->form_validation->run() == FALSE)
                            {
                                $this->load->view('common/header');
                                $this->load->view('category');
                                $this->load->view('common/footer');

                            }else{
                                    $categoryName = $this->input->post('categoryName');
                                    $categorySlug = $this->input->post('categorySlug');
                                    $active = $this->input->post('active');

                                    $catCount = $this->Category_model->checkMultilpeCategroyEdit($id,$categoryName);
                                    if($catCount->catCount > 0)
                                    {
                                        $this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-warning"></i><strong>Error!</strong></h5> Category already exits!</div>');
                                    }
                                    else
                                    {
                                        $data_post = array(
                                                'name'=>$categoryName,
                                                'slug' => strtolower($categorySlug),
                                                'status'=> $active,
                                                'modified_by'=> $loggedInUser,
                                                'modified_datetime'=> date("Y-m-d H:i:s")
                                                );

                                        $update = $this->Category_model->update_category($id,$data_post);

                                             if(!is_dir('../assets/images/products_category/category/'.$id)) 
                                                {
                                                  mkdir('../assets/images/products_category/category/'.$id,0755,TRUE);
                                                } 
                                         $config['upload_path'] = '../assets/images/products_category/category/'.$id;
                                         $config['allowed_types'] = 'gif|jpg|png|jpeg';
                                         $config['max_size'] = '530'; 
                                         $config['max_width']  = '530';
                                         $config['file_name'] = $id;
                                         $config['overwrite'] = TRUE;
                                         if(is_dir('../assets/images/products_category/category/'.$id)){ 
                                            delete_files('../assets/images/products_category/category/'.$id);
                                         }
                                         $this->upload->initialize($config);
                                         if(!$this->upload->do_upload('category_img')){  }

                                        if($update)
                                        {

                                            $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable">
                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                            <h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Changes has been Updated successfully</div>');
                                            redirect('/category');
                                        }
                                        else
                                        {
                                            $this->session->set_flashdata('feedback', '<div class="alert alert-warning alert-dismissable "><h5><i class="icon fa fa-warning"></i><strong>Error!</strong></h5> The server is not responding, try again later.</div>');
                                            redirect('/category');
                                        }
                                    }
                                }
                            }else
                            {

                                $this->load->view('common/header');
                                $this->load->view('category',$data_post);
                                $this->load->view('common/footer');
                            }
                    }else
                    {
                        $this->load->view('common/header');
                        $this->load->view('not_permission');
                        $this->load->view('common/footer');
                    }

             }

    }



    function delete_category()
    {
        $catID = $this->input->post('catID');
        $deleted = $this->Category_model->delete_category($catID);

        if($deleted)
        {
            $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Record has been deleted successfully</div>');
            redirect('/category');
        }
        else
        {
            $this->session->set_flashdata('feedback', '<div class="alert alert-danger alert-dismissable"><h5><i class="icon fa fa-ban"></i><strong>Error!</strong></h5>The server is not responding, try again later.</div>');
            redirect('/category');
        }
    }

/*---------End of Category submodule functionalities-------*/

/*---------SubCategory submodule functionalities--------------*/
    public function subcategory($offset = null)
    {
        $data_post=array();
        $data_post['subcategories'] = $this->Category_model->get_all_sub_categories();

        if(!$this->session->userdata('user_id')){

            $this->load->view('login');

        }else{

                $data['menu_permissions'] = $this->data['users_menu'];

                if($this->has_rights(5) == 1){
                $search_by = $this->input->post('search_by');
                $table_search = $this->input->post('table_search');
                $paginationUrl = 'category/subcategory/';
                $uri_segment = 3;
                $config['per_page'] = 1000;
                $config['total_rows'] = count($this->Category_model->get_all_sub_categories($search_by,$table_search,'',''));
                $total_rows = $config['total_rows'];
                $get_data = $this->Category_model->get_all_sub_categories($search_by,$table_search,$offset,$config['per_page']);
                $count = $config['total_rows'];
                $data_post['subcategories'] = $get_data;
                $this->customPagination($paginationUrl,$count,$uri_segment,$get_data,$total_rows);
                $this->load->view('common/header');
                $this->load->view('subcategory_view',$data_post);
                $this->load->view('common/footer');

                }else
                {

                    $this->load->view('common/header');
                    $this->load->view('not_permission');
                    $this->load->view('common/footer');
                }
         }

    }

    public function subcategory_addedit($id=NULL)
    {

         $data_post=array();

         $data_post['categories'] = $this->Category_model->get_all_categories();
         $loggedInUser=$this->session->userdata('user_id');

         if($id==NULL)
        {
            if($this->has_rights(6) == 1)
            {
                    if($this->input->post())
                    {
                        //|is_unique[product_sub_category.name]
                        //|is_unique[product_sub_category.slug]
                        $this->form_validation->set_rules('subcategoryName', 'Sub-Category Name', 'required');
                        $this->form_validation->set_rules('subcategorySlug', 'Sub-Category Slug', 'required');

                        if ($this->form_validation->run() == FALSE)
                        {
                            $this->load->view('common/header');
                            $this->load->view('subcategory');
                            $this->load->view('common/footer');

                        }else{

                                $category = $this->input->post('category');
                                $subcategoryName = $this->input->post('subcategoryName');
                                $subcategorySlug = $this->input->post('subcategorySlug');
                                $active = $this->input->post('active');

                               // $catCount = $this->Category_model->checkMultilpeCategroy($this->input->post('categoryName'));
                                $Count = $this->Category_model->checkMultilpeSubCategroy($category,$subcategoryName);
                                //echo $Count->subcatCount;exit;
                                if($Count->subcatCount > 0)
                                {
                                    $this->session->set_flashdata('feedback', '<div class="callout callout-warning"><h5><i class="icon fa fa-warning"></i><strong>Error!</strong></h5></i><strong>Error!</strong> Sub-Category already exits!</div>');
                                     redirect('/category/subcategory');
                                }
                                else
                                {
                                    $data_post = array(
                                        'product_cat_id' => $category,
                                        'name' => $subcategoryName,
                                        'slug' => strtolower($subcategorySlug),
                                        'status' => $active,
                                        'created_by' => $loggedInUser,
                                        'created_datetime' => date("Y-m-d H:i:s"),
                                        'modified_by' => $loggedInUser,
                                        'modified_datetime' => date("Y-m-d H:i:s")
                                        );

                                    $created = $this->Category_model->new_record('product_sub_category',$data_post);

                                  
                                     if(!is_dir('../assets/images/products_category/sub_category/'.$created)) 
                                        {
                                          mkdir('../assets/images/products_category/sub_category/'.$created,0755,TRUE);
                                        } 
                                    if($_FILES['category_img']['name']!='')
                                    {
                                        $config['upload_path'] = '../assets/images/products_category/sub_category/'.$created;
                                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                                        $config['max_size'] = '530'; 
                                        $config['max_width']  = '530';
                                        $config['file_name'] = $created;
                                        $config['overwrite'] = TRUE;
                                        $this->upload->initialize($config);
                                        if(!$this->upload->do_upload('category_img')){  }
                                    }

                                    if($created){

                                            $this->session->set_flashdata('feedback', '<div class="callout callout-success"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Sub-Category added successfully!</div>');
                                            redirect('/category/subcategory/');
                                    }
                                    else
                                    {

                                         $this->session->set_flashdata('feedback', '<div class="callout callout-danger"><h5><i class="icon fa fa-ban"></i><strong>Error!</strong></h5>The server is not responding, try again later.</div>');
                                            redirect('/category/subcategory/');
                                    }
                                }
                            }
                        }else
                        {

                            $this->load->view('common/header');
                            $this->load->view('subcategory',$data_post);
                            $this->load->view('common/footer');
                        }
                    }else
                    {
                        $this->load->view('common/header');
                        $this->load->view('not_permission');
                        $this->load->view('common/footer');
                    }

            }else
            {
                $data_post['subcategory_details'] = $this->Category_model->get_sub_category_by_ID($id);

                if($this->input->post())
                {
                        $this->form_validation->set_rules('category', 'Category Name', 'required');
                        $this->form_validation->set_rules('subcategoryName', 'Sub-Category Name', 'required');
                        $this->form_validation->set_rules('subcategorySlug', 'Sub-Category Slug', 'required');

                        if ($this->form_validation->run() == FALSE)
                        {
                            $this->load->view('common/header');
                            $this->load->view('subcategory');
                            $this->load->view('common/footer');

                        }else{

                              $category = $this->input->post('category');
                              $subcategoryName = $this->input->post('subcategoryName');
                              $subcategorySlug = $this->input->post('subcategorySlug');
                              $active = $this->input->post('active');

                               // $catCount = $this->Category_model->checkMultilpeSubCategroy($category,$subcategoryName);
                              
                              $catCount = $this->Category_model->checkMultilpeSubCategroyEdit($id,$category,$subcategoryName);

                                if($catCount->subcatCount > 0)
                                {

                                    $this->session->set_flashdata('feedback', '<div class="callout callout-warning"><h5><i class="icon fa fa-warning"></i><strong>Error!</strong></h5> Sub-Category already exits!</div>');
                                    redirect('/category/subcategory/');
                                }
                                else
                                {
                                    $data_post = array(
                                            'product_cat_id' => $category,
                                            'name' => $subcategoryName,
                                            'slug' => strtolower($subcategorySlug),
                                            'status' => $active,
                                            'modified_by' => $loggedInUser,
                                            'modified_datetime' => date("Y-m-d H:i:s")
                                            );

                                    $update = $this->Category_model->update_sub_category($id,$data_post);

                                     if(!is_dir('../assets/images/products_category/sub_category/'.$id)) 
                                                {
                                                  mkdir('../assets/images/products_category/sub_category/'.$id,0755,TRUE);
                                                } 
                                         $config['upload_path'] = '../assets/images/products_category/sub_category/'.$id;
                                         $config['allowed_types'] = 'gif|jpg|png|jpeg';
                                         $config['max_size'] = '530'; 
                                         $config['max_width']  = '530';
                                         $config['file_name'] = $id;
                                         $config['overwrite'] = TRUE;
                                         if(is_dir('../assets/images/products_category/sub_category/'.$id)){ 
                                            delete_files('../assets/images/products_category/sub_category/'.$id);
                                         }
                                         $this->upload->initialize($config);
                                         if(!$this->upload->do_upload('category_img')){  }


                                    if($update)
                                    {
                                        $this->session->set_flashdata('feedback', '<div class="callout callout-success"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Changes has been updated successfully!</div>');
                                        redirect('/category/subcategory/');
                                    }
                                    else
                                    {
                                        $this->session->set_flashdata('feedback', '<div class="callout callout-danger"><h5><i class="icon fa fa-ban"></i><strong>Error!</strong></h5>The server is not responding, try again later.</div>');
                                        redirect('/category/subcategory/');
                                    }
                                }
                            }

                    }else
                    {

                        $this->load->view('common/header');
                        $this->load->view('subcategory',$data_post);
                        $this->load->view('common/footer');
                    }


             }
    }

     function delete_subcategory()
    {

        $catID = $this->input->post('catID');
        $deleted = $this->Category_model->delete_subcategory($catID);

        if($deleted)
        {

            $this->session->set_flashdata('feedback', '<div class="callout callout-success"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Record has been deleted successfully</div>');
            redirect('/category/subcategory');
        }
        else
        {

            $this->session->set_flashdata('feedback', '<div class="callout callout-danger"><h5><i class="icon fa fa-ban"></i><strong>Error!</strong></h5>The server is not responding, try again later.</div>');
            redirect('/category/subcategory/');
        }
    }

    /*---------End of SubCategory submodule functionalities-------*/

    /*---------Variation-Type submodule functionalities-------*/


    public function variationtype($offset = null)
    {
        $data_post=array();

        $data_post['variations_type'] = $this->Category_model->get_all_variationtype();

        if(!$this->session->userdata('user_id')){

            $this->load->view('login');

        }else{

                if($this->has_rights(24) == 1){
                $search_by = $this->input->post('search_by');
                $table_search = $this->input->post('table_search');
                $paginationUrl = 'category/variationtype/';
                $uri_segment = 3;
                $config['per_page'] = 1000;
                $config['total_rows'] = count($this->Category_model->get_all_variationtype($search_by,$table_search,'',''));
                $total_rows = $config['total_rows'];
                $get_data = $this->Category_model->get_all_variationtype($search_by,$table_search,$offset,$config['per_page']);
                $count = $config['total_rows'];
                $data_post['variationtype'] = $get_data;
                $this->customPagination($paginationUrl,$count,$uri_segment,$get_data,$total_rows);
                $this->load->view('common/header');
                $this->load->view('variationtype_view',$data_post);
                $this->load->view('common/footer');

                }else
                {

                    $this->load->view('common/header');
                    $this->load->view('not_permission');
                    $this->load->view('common/footer');
                }
         }

    }

    public function variationtype_addedit($id=NULL)
    {

         $data_post=array();
         $data_post['categories'] = $this->Category_model->get_all_categories();
         $data_post['variationtype'] = $this->Category_model->get_all_variationtype();
         $loggedInUser=$this->session->userdata('user_id');

        if($id==NULL)
        {
            if($this->has_rights(25) == 1)
            {
                    if($this->input->post())
                    {
                        $this->form_validation->set_rules('category', 'Category Name', 'required');
                        $this->form_validation->set_rules('sub_category', 'Sub-Category Name', 'required');
                        $this->form_validation->set_rules('variationtypeName', 'Variation-Type Name', 'required');
                        $this->form_validation->set_rules('variationtypeSlug', 'Variation-Type Slug', 'required');

                        if ($this->form_validation->run() == FALSE)
                        {
                            $this->load->view('common/header');
                            $this->load->view('variationtype');
                            $this->load->view('common/footer');

                        }else{

                            $category = $this->input->post('category');
                            $subcategory = $this->input->post('sub_category');
                            $variationtypeName = $this->input->post('variationtypeName');
                            $variationtypeSlug = $this->input->post('variationtypeSlug');
                            $active = $this->input->post('active');
                            $size_guide = $this->input->post('size_guide');

                            $varCount = $this->Category_model->checkMultilpeVariationtype($subcategory,$variationtypeName);

                            if($varCount->vartypeCount > 0)
                            {
                                $this->session->set_flashdata('feedback', '<div class="callout callout-warning"><h5><i class="icon fa fa-warning"></i><strong>Error!</strong></h5></i><strong>Error!</strong> Variation-Type already exits!</div>');
                            }
                            else
                            {
                                $data_post = array(
                                    'prod_sub_cat_id' => $subcategory,
                                    'name' => $variationtypeName,
                                    'slug' => strtolower($variationtypeSlug),
                                    'status' => $active,
                                    'created_by' => $loggedInUser,
                                    'created_datetime' => date("Y-m-d H:i:s"),
                                    'modified_by' => $loggedInUser,
                                    'modified_datetime' => date("Y-m-d H:i:s"),
                                    'size_guide' =>$size_guide
                                    );

                                $created = $this->Category_model->new_record('product_variation_type',$data_post);

                                 if(!is_dir('../assets/images/products_category/variation_types/'.$created)) 
                                        {
                                          mkdir('../assets/images/products_category/variation_types/'.$created,0755,TRUE);
                                        } 
                                    if($_FILES['category_img']['name']!='')
                                    {
                                        $config['upload_path'] = '../assets/images/products_category/variation_types/'.$created;
                                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                                        $config['max_size'] = '530'; 
                                        $config['max_width']  = '530';
                                        $config['file_name'] = $created;
                                        $config['overwrite'] = TRUE;
                                        $this->upload->initialize($config);
                                        if(!$this->upload->do_upload('category_img')){  }
                                    }

                                if($created){

                                    $this->session->set_flashdata('feedback', '<div class="callout callout-success"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Sub-Category added successfully!</div>');
                                    redirect('/category/variationtype/');
                                }
                                else
                                {
                                     $this->session->set_flashdata('feedback', '<div class="callout callout-danger"><h5><i class="icon fa fa-ban"></i><strong>Error!</strong></h5>The server is not responding, try again later.</div>');
                                        redirect('/category/variationtype/');
                                }
                            }
                        }
                    }else
                    {

                        $this->load->view('common/header');
                        $this->load->view('variationtype',$data_post);
                        $this->load->view('common/footer');
                    }

                }else
                {
                    $this->load->view('common/header');
                    $this->load->view('not_permission');
                    $this->load->view('common/footer');
                }

        }else
        {
            $data_post['categories'] = $this->Category_model->get_all_categories();
            $data_post['variationtype_details'] = $this->Category_model->get_variationtype_by_id($id);

            if($this->has_rights(26) == 1)
            {
                    if($this->input->post())
                    {
                        $category = $this->input->post('category');
                        $subcategory = $this->input->post('sub_category');
                        $variationtypeName = $this->input->post('variationtypeName');
                        $variationtypeSlug = $this->input->post('variationtypeSlug');
                        $active = $this->input->post('active');
                        $size_guide = $this->input->post('size_guide');

                        $this->form_validation->set_rules('category', 'Category Name', 'required');
                        $this->form_validation->set_rules('sub_category', 'Sub-Category Name', 'required');
                        $this->form_validation->set_rules('variationtypeName', 'Variation-Type Name', 'required');
                        $this->form_validation->set_rules('variationtypeSlug', 'Variation-Type Slug', 'required');

                        if ($this->form_validation->run() == FALSE)
                        {
                            $this->load->view('common/header');
                            $this->load->view('variationtype');
                            $this->load->view('common/footer');

                        }else{
                                  $category = $this->input->post('category');
                                  $subcategoryName = $this->input->post('subcategoryName');
                                  $subcategorySlug = $this->input->post('subcategorySlug');
                                  $active = $this->input->post('active');

                                    $catCount = $this->Category_model->checkMultilpeVariationtypeEdit($id,$category,$subcategoryName);
                                    if($catCount->vartypeCount > 1)
                                    {

                                        $this->session->set_flashdata('feedback', '<div class="callout callout-warning"><h5><i class="icon fa fa-warning"></i><strong>Error!</strong></h5> Variation-Type already exits!</div>');
                                        redirect('/category/variationtype/');
                                    }
                                    else
                                    {

                                         $data_post = array(
                                            'prod_sub_cat_id' => $subcategory,
                                            'name' => $variationtypeName,
                                            'slug' => strtolower($variationtypeSlug),
                                            'status' => $active,
                                            'created_by' => $loggedInUser,
                                            'created_datetime' => date("Y-m-d H:i:s"),
                                            'modified_by' => $loggedInUser,
                                            'modified_datetime' => date("Y-m-d H:i:s"),
                                            'size_guide' =>$size_guide
                                            );

                                        $update = $this->Category_model->update_variationtype($id,$data_post);

                                        if(!is_dir('../assets/images/products_category/variation_types/'.$id)) 
                                                {
                                                  mkdir('../assets/images/products_category/variation_types/'.$id,0755,TRUE);
                                                } 
                                         $config['upload_path'] = '../assets/images/products_category/variation_types/'.$id;
                                         $config['allowed_types'] = 'gif|jpg|png|jpeg';
                                         $config['max_size'] = '530'; 
                                         $config['max_width']  = '530';
                                         $config['file_name'] = $id;
                                         $config['overwrite'] = TRUE;
                                         if(is_dir('../assets/images/products_category/variation_types/'.$id)){ 
                                            delete_files('../assets/images/products_category/variation_types/'.$id);
                                         }
                                         $this->upload->initialize($config);
                                         if(!$this->upload->do_upload('category_img')){  }


                                        if($update)
                                        {
                                            $this->session->set_flashdata('feedback', '<div class="callout callout-success"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Changes has been updated successfully!</div>');
                                            redirect('/category/variationtype/');
                                        }
                                        else
                                        {
                                            $this->session->set_flashdata('feedback', '<div class="callout callout-danger"><h5><i class="icon fa fa-ban"></i><strong>Error!</strong></h5>The server is not responding, try again later.</div>');
                                            redirect('/category/variationtype/');
                                        }
                                    }
                            }


                    }else
                    {

                        $this->load->view('common/header');
                        $this->load->view('variationtype',$data_post);
                        $this->load->view('common/footer');
                    }


                }else
                {

                    $this->load->view('common/header');
                    $this->load->view('not_permission');
                    $this->load->view('common/footer');
                }

            }
    }

     function delete_variationtype()
    {
        $del_id = $this->input->post('del_id');
        $deleted = $this->Category_model->delete_variationtype($del_id);

        if($deleted)
        {

            $this->session->set_flashdata('feedback', '<div class="callout callout-success"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Record has been deleted successfully</div>');
            redirect('/category/subcategory');
        }
        else
        {

            $this->session->set_flashdata('feedback', '<div class="callout callout-danger"><h5><i class="icon fa fa-ban"></i><strong>Error!</strong></h5>The server is not responding, try again later.</div>');
            redirect('/category/subcategory/');
        }
    }


    function get_sub_categories(){

        $data = $this->Category_model->get_sub_categories($this->input->post('cat_id'));
        return $this->ajaxCombo($data,$this->input->post('selected_id'));

    }//to get sub_category according category

    function get_variation_type_byId()//this function return only vartype
    {
        $data = $this->Category_model->get_variationtype_bySubCatId($this->input->post('sub_cat_id'));
        return $this->ajaxCombo($data,$this->input->post('selected_id'));
    }
/*---------End of Variation-Type submodule functionalities-------*/


/*---------Variation-Value submodule functionalities-------*/


    public function variationvalue($offset = null)
    {
        $data_post=array();

        $data_post['variationvalue'] = $this->Category_model->get_all_variationvalue();

        if(!$this->session->userdata('user_id')){

            $this->load->view('login');

        }else{


            if($this->has_rights(28) == 1){
            $search_by = $this->input->post('search_by');
            $table_search = $this->input->post('table_search');
            $paginationUrl = 'category/variationvalue/';
            $uri_segment = 3;
            $config['per_page'] = 1000;
            $config['total_rows'] = count($this->Category_model->get_all_variationvalue($search_by,$table_search,'',''));
            $total_rows = $config['total_rows'];
            $get_data = $this->Category_model->get_all_variationvalue($search_by,$table_search,$offset,$config['per_page']);
            $count = $config['total_rows'];
            $data_post['variationvalue'] = $get_data;
            $this->customPagination($paginationUrl,$count,$uri_segment,$get_data,$total_rows);
            $this->load->view('common/header');
            $this->load->view('variationvalue_view',$data_post);
            $this->load->view('common/footer');

            }else
            {
                $this->load->view('common/header');
                $this->load->view('not_permission');
                $this->load->view('common/footer');
            }
         }

    }

    public function variationvalue_addedit($id=NULL)
    {

         $data_post=array();
         $data_post['categories'] = $this->Category_model->get_all_categories();
         $data_post['variationtype'] = $this->Category_model->get_all_variationtype();
         $loggedInUser=$this->session->userdata('user_id');

        if($id==NULL)
        {
            if($this->has_rights(29) == 1)
            {
                    if($this->input->post())
                    {
                        $this->form_validation->set_rules('category', 'Category Name', 'required');
                        $this->form_validation->set_rules('subcategory', 'Sub-Category Name', 'required');
                        $this->form_validation->set_rules('variationtype', 'Variation-Type Name', 'required');
                        $this->form_validation->set_rules('variationName', 'Variation Name', 'required');
                        $this->form_validation->set_rules('variationSlug', 'Variation Slug', 'required');

                        if ($this->form_validation->run() == FALSE)
                        {
                            $this->load->view('common/header');
                            $this->load->view('variationvalue');
                            $this->load->view('common/footer');

                        }else{

                            $category = $this->input->post('category');
                            $subcategory = $this->input->post('subcategory');
                            $variationtype = $this->input->post('variationtype');
                            $variationName = $this->input->post('variationName');
                            $variationSlug = $this->input->post('variationSlug');
                            $active = $this->input->post('active');

                            $varCount = $this->Category_model->checkMultilpeVariationvalue($variationtype,$variationName);

                            if($varCount->varCount > 0)
                            {
                                $this->session->set_flashdata('feedback', '<div class="callout callout-warning"><h5><i class="icon fa fa-warning"></i><strong>Error!</strong></h5></i><strong>Error!</strong> Variation already exits! </div>');
                            }
                            else
                            {
                                $data_post = array(
                                    'product_variation_type_id' => $variationtype,
                                    'name' => $variationName,
                                    'slug' => strtolower($variationSlug),
                                    'status' => $active,
                                    'created_by' => $loggedInUser,
                                    'created_datetime' => date("Y-m-d H:i:s"),
                                    'modified_by' => $loggedInUser,
                                    'modified_datetime' => date("Y-m-d H:i:s")
                                    );

                                $created = $this->Category_model->new_record('product_variation_value',$data_post);

                                 if(!is_dir('../assets/images/products_category/variation_values/'.$created)) 
                                        {
                                          mkdir('../assets/images/products_category/variation_values/'.$created,0755,TRUE);
                                        } 
                                    if($_FILES['category_img']['name']!='')
                                    {
                                        $config['upload_path'] = '../assets/images/products_category/variation_values/'.$created;
                                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                                        $config['max_size'] = '530'; 
                                        $config['max_width']  = '530';
                                        $config['file_name'] = $created;
                                        $config['overwrite'] = TRUE;
                                        $this->upload->initialize($config);
                                        if(!$this->upload->do_upload('category_img')){  }
                                    }

                                if($created){

                                    $this->session->set_flashdata('feedback', '<div class="callout callout-success"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Variation added successfully!</div>');
                                    redirect('/category/variationvalue/');
                                }
                                else
                                {
                                     $this->session->set_flashdata('feedback', '<div class="callout callout-danger"><h5><i class="icon fa fa-ban"></i><strong>Error!</strong></h5>The server is not responding, try again later.</div>');
                                        redirect('/category/variationvalue/');
                                }
                            }
                        }
                    }else
                    {

                        $this->load->view('common/header');
                        $this->load->view('variationvalue',$data_post);
                        $this->load->view('common/footer');
                    }

                }else
                {
                    $this->load->view('common/header');
                    $this->load->view('not_permission');
                    $this->load->view('common/footer');
                }

        }else
        {
            $data_post['categories'] = $this->Category_model->get_all_categories();
            $data_post['variationvalue_details'] = $this->Category_model->get_variationvalue_by_id($id);

            if($this->has_rights(30) == 1)
            {
                    if($this->input->post())
                    {

                        $category = $this->input->post('category');
                        $subcategory = $this->input->post('subcategory');
                        $variationtype = $this->input->post('variationtype');
                        $variationName = $this->input->post('variationName');
                        $variationSlug = $this->input->post('variationSlug');
                        $active = $this->input->post('active');

                        $this->form_validation->set_rules('category', 'Category Name', 'required');
                        $this->form_validation->set_rules('subcategory', 'Sub-Category Name', 'required');
                        $this->form_validation->set_rules('variationtype', 'Variation-Type Name', 'required');
                        $this->form_validation->set_rules('variationName', 'Variation Name', 'required');
                        $this->form_validation->set_rules('variationSlug', 'Variation Slug', 'required');

                        if ($this->form_validation->run() == FALSE)
                        {

                             redirect('/category/variationvalue_addedit/'.$id);

                        }else{

                                $category = $this->input->post('category');
                                $subcategory = $this->input->post('subcategory');
                                $variationtype = $this->input->post('variationtype');
                                $variationName = $this->input->post('variationName');
                                $variationSlug = $this->input->post('variationSlug');
                                $active = $this->input->post('active');

                                    $varCount = $this->Category_model->checkMultilpeVariationvalueEdit($id,$variationtype,$variationName);

                                    if($varCount->varCount > 1)
                                    {
                                        $this->session->set_flashdata('feedback', '<div class="callout callout-warning"><h5><i class="icon fa fa-warning"></i><strong>Error!</strong></h5> Variation already exits!</div>');
                                        redirect('/category/variationvalue/');
                                    }
                                    else
                                    {

                                        $data_post = array(
                                        'product_variation_type_id' => $variationtype,
                                        'name' => $variationName,
                                        'slug' => strtolower($variationSlug),
                                        'status' => $active,
                                        'modified_by' => $loggedInUser,
                                        'modified_datetime' => date("Y-m-d H:i:s")
                                        );

                                        $update = $this->Category_model->update_variationvalue($id,$data_post);

                                         if(!is_dir('../assets/images/products_category/variation_values/'.$id)) 
                                                {
                                                  mkdir('../assets/images/products_category/variation_values/'.$id,0755,TRUE);
                                                } 
                                         $config['upload_path'] = '../assets/images/products_category/variation_values/'.$id;
                                         $config['allowed_types'] = 'gif|jpg|png|jpeg';
                                         $config['max_size'] = '530'; 
                                         $config['max_width']  = '530';
                                         $config['file_name'] = $id;
                                         $config['overwrite'] = TRUE;
                                         if(is_dir('../assets/images/products_category/variation_values/'.$id)){ 
                                            delete_files('../assets/images/products_category/variation_values/'.$id);
                                         }
                                         $this->upload->initialize($config);
                                         if(!$this->upload->do_upload('category_img')){  }


                                        if($update)
                                        {
                                            $this->session->set_flashdata('feedback', '<div class="callout callout-success"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Changes has been updated successfully!</div>');
                                            redirect('/category/variationvalue/');
                                        }
                                        else
                                        {
                                            $this->session->set_flashdata('feedback', '<div class="callout callout-danger"><h5><i class="icon fa fa-ban"></i><strong>Error!</strong></h5>The server is not responding, try again later.</div>');
                                            redirect('/category/variationvalue/');
                                        }
                                    }
                            }


                    }else
                    {
                        $this->load->view('common/header');
                        $this->load->view('variationvalue',$data_post);
                        $this->load->view('common/footer');
                    }


                }else
                {
                    $this->load->view('common/header');
                    $this->load->view('not_permission');
                    $this->load->view('common/footer');
                }

            }
    }

     function delete_variationvalue()
    {
        $del_id = $this->input->post('del_id');
        $deleted = $this->Category_model->delete_variationvalue($del_id);

        if($deleted)
        {
            $this->session->set_flashdata('feedback', '<div class="callout callout-success"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Record has been deleted successfully</div>');
            redirect('/category/variationvalue_view');
        }
        else
        {

            $this->session->set_flashdata('feedback', '<div class="callout callout-danger"><h5><i class="icon fa fa-ban"></i><strong>Error!</strong></h5>The server is not responding, try again later.</div>');
            redirect('/category/variationvalue_view/');
        }
    }

/*---------End of Variation-Value submodule functionalities-------*/



/*----------Common functions------------------------------------*/

    //--- this function is to create option in dropdown based on ajax response
    function ajaxCombo($res,$sel){
        $str ="<option value=''>Please Select</option>";
        foreach($res as $row){
            if($sel == $row['id']){
                 $str.="<option value='".$row['id']."' selected>".$row['name']."</option>";
            }else{
                 $str.="<option value='".$row['id']."'>".$row['name']."</option>";
        }
        }
        print $str;
    }//---

/*----------End of Common functions------------------------------------*/
}
