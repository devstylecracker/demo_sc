<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Streetlook_v2 extends MY_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
       parent::__construct();
        $this->load->model('users_model');    
        $this->load->model('product_model');  
        $this->load->model('Imageupload_model'); 
        $this->load->model('category_model');    
		$this->load->model('Streetstyle_model');
		$this->load->model('lookcreator_model');   
      
   } 
   
	/*public function index($offset = null)
	{ 
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{ 
			if($this->has_rights(50) == 1){
				
				$data_post = array();
			
			    $data['category_list'] = $this->lookcreator_model->catgory_list(); 
				$data['body_shape'] = $this->product_model->load_questAns(1);
				$data['age'] = $this->product_model->load_questAns(8);
				$data['style'] = $this->product_model->load_questAns(2);
				$data['size'] = $this->product_model->load_questAns(9);
				$data['budget'] = $this->product_model->load_questAns(7);
				$data['colors_list'] = $this->lookcreator_model->get_tags_by_tag_type(1); 
				$data['brand_list'] = $this->lookcreator_model->get_brands_list(); 

				$data['street_data'] = $this->Imageupload_model->get_street_pic('','','','');
				$this->load->view('common/header',$data);
				$this->load->view('look/streetstyle_look_creator');
				$this->load->view('common/footer');
			}
		}
	}*/

		public function index($offset = null)
	{ 
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{ 
			if($this->has_rights(50) == 1){
				
				$data_post = array();
			
			    $data['parent_category_list'] = $this->lookcreator_model->parent_catgory_list(); 
				$data['category_list'] = $this->lookcreator_model->catgory_list();
				$data['body_shape'] = $this->product_model->load_questAns(1);
				$data['age'] = $this->product_model->load_questAns(8);
				$data['style'] = $this->product_model->load_questAns(2);
				$data['size'] = $this->product_model->load_questAns(9);
				$data['budget'] = $this->product_model->load_questAns(7);
				$data['colors_list'] = $this->lookcreator_model->get_tags_by_tag_type(1); 
				$data['brand_list'] = $this->lookcreator_model->get_brands_list(); 

				$data['street_data'] = $this->Imageupload_model->get_street_pic('','','','');
				$this->load->view('common/header',$data);
				$this->load->view('look/streetstyle_look_creator_v2');
				$this->load->view('common/footer');
			}
		}
	}

	
	
	
}
