<?php
ini_set('max_execution_time', 900); // in seconds 
defined('BASEPATH') OR exit('No direct script access allowed');
class Remove_images extends MY_Controller {

	function __construct(){
       parent::__construct();       
       $this->load->library('image_lib');
       $this->load->model('Product_delete_model');
       $this->load->library('email');
   	}

   	function cronttest()
   	{
   		echo 'cronttest';exit;
   	}

   	function getFolderImages()
   	{ echo 'getFolderImages';exit;
   		$dir = './assets/products/';  
   		//$dir = './assets/products_extra/';
   		//$dir = './assets/product_images/zoom/'; 		
		$dh  = scandir($dir);
		$count = sizeof($dh);
		//echo $count;exit;
		$file = fopen("imagedelete.txt","w");
		//$file = fopen("imagedelete_extra.txt","w");
		$write =  fwrite($file,'Image Count In Folder-'+$count.'\n');
		foreach( scandir($dir) as $val){			
				echo $row = $val."\r\n";
			    echo '<br/>';
			    $write =  fwrite($file,$row);			   			    
		}
		fclose($file);
		exit;
   	}

   	function deleteImageFile($image_name=NULL)
   	{  //echo 'deleteImageFile';exit;
		$image_content =file("imagedelete.txt");
		//$image_content =file("imagedelete_extra.txt");
		$i=0;$message='';$productids_iamgenotdeleted = '';
		$deleted_images = '';
		foreach($image_content as $image_name)
		{
			//echo $image_name;
			/*if($i==10)
			{
				break;
			}*/			
			$image_name = trim($image_name, "\r\n");
			$prdid = $this->Product_delete_model->checkImageInDB($image_name);
			//$prdid = $this->Product_delete_model->checkExtraImageInDB($image_name);
			if($prdid!='' && $prdid>0)
			{
				$productids_iamgenotdeleted = $productids_iamgenotdeleted.','.$prdid;
			}else
			{				
				$pimg = $this->unlinkProductImage($image_name);
				//$pimg = $this->unlinkProductExtraImages($image_name);
				$deleted_images = $deleted_images.','.$pimg;
			}
			$i++;

		}
		$message.='Data Report==<br/>';
		$message.= 'productids_image_notdeleted=== '.$productids_iamgenotdeleted;
		$message.= '<br/><br/>';
		$message.= 'deleted_images== '.$deleted_images;
		$message.= '<br/>';		
		$this->emailDataSent($message);
		echo 'cron ended';exit;
   	}
   	

   	function unlinkProductImage($pimg){ 
	        unlink('./assets/products/'.$pimg);
	        unlink('./assets/products/thumb/'.$pimg);  
	        unlink('./assets/products/thumb_124/'.$pimg);
	        unlink('./assets/products/thumb_160/'.$pimg);
	        unlink('./assets/products/thumb_310/'.$pimg);
	        unlink('./assets/products/thumb_340/'.$pimg);
	        unlink('./assets/products/thumb_546/'.$pimg);
	        unlink('./assets/product_images/zoom/'.$pimg);	 
	        return $pimg;       
	}

    function unlinkProductExtraImages($pimg){
    	 	unlink('./assets/products_extra/'.$pimg);
    	 	unlink('./assets/products_extra/thumb/'.$pimg);  
    	 	unlink('./assets/products_extra/thumb_124/'.$pimg);
	        unlink('./assets/products_extra/thumb_160/'.$pimg);
	        unlink('./assets/products_extra/thumb_310/'.$pimg);
	        unlink('./assets/products_extra/thumb_340/'.$pimg);
	        unlink('./assets/products_extra/thumb_546/'.$pimg);  
	        return $pimg;     	 
    }

    function emailDataSent($message)
	{
	  	  $config['protocol'] = 'smtp';	     
		  $config['charset'] = 'iso-8859-1';
	      $config['mailtype'] = 'html';
	      $config['wordwrap'] = TRUE;
	      $config['smtp_host'] = $this->config->item('smtp_host');
	      $config['smtp_user'] = $this->config->item('smtp_user');
	      $config['smtp_pass'] = $this->config->item('smtp_pass');
	      $config['smtp_port'] = $this->config->item('smtp_port');
	      $this->email->initialize($config);
	    
	      $this->email->from($this->config->item('from_email'), 'StyleCracker');
	      $this->email->to('sudha@stylecracker.com');    
	      $this->email->cc('arun@stylecracker.com');  		      	  
	           
	      $this->email->subject('StyleCracker:  Product Image Delete Cron');	
	      $this->email->message($message);      
	      
	  	  $sentmsg = $this->email->send();
	  	  if($sentmsg)
	  	  {
	  	  	return 'sent';
	  	  }else
	  	  {
	  	  	return 'notsent';
	  	  }
	}
}




?>