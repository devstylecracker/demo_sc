<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_curation extends MY_Controller {
	
	function __construct(){
        parent::__construct();    
        $this->load->model('Cart_model');    
        $this->load->model('Cost_model'); 
        $this->load->model('Stylist_model');    
    }  

	public function index()
	{
		echo 'Product_curation index';exit;
		$cart_id = $this->uri->segment(2);		
		$this->load->view('common/header2');
		$this->load->view('scbox/product_curation_view');
		$this->load->view('common/footer2');	
	}

	public function order_cart()
	{
		$data = array();
		$cart_id = $this->uri->segment(3);
		$data['cart_id'] = $cart_id;
		if($cart_id!='' && $cart_id>0)
		{
			$order_data = $this->Cart_model->get_orderDetailBycart($cart_id);		
			//echo '<pre>';print_r($order_data);exit;
			if(!empty($order_data))
			{
				$data['order_data']['cart_id'] = $order_data[0]['id'];
				$data['order_data']['product_id'] = $order_data[0]['product_id'];
				$data['order_data']['user_id'] = $order_data[0]['user_id'];
				$data['order_data']['product_price'] = $order_data[0]['product_price'];
				$data['order_data']['order_unique_no'] = $order_data[0]['order_unique_no'];
				$data['order_data']['order_display_no'] = $order_data[0]['order_display_no'];
				if($order_data[0]['product_price']!='2999' || $order_data[0]['product_price']!='4999' || $order_data[0]['product_price']!='6999')
				{
					$box_type = (int)$order_data[0]['product_price'];
				}else
				{
					$box_type = 'custom';
				}			
				$package_objectid = $this->Cost_model->get_scx_option_box_type($box_type);
				$package_cost = $this->Cost_model->get_scx_option_meta($package_objectid);
				//echo '<pre>';print_r($package_cost);exit;
				$i=0;
				if(!empty($package_cost))
				{					
					foreach($package_cost as $val)
					{					
						$data['package_cost'][@$val['option_key']] = @$val['option_value'];	
						$i++;	
					}
					$curation_maxcost = $this->Cost_model->get_cart_meta_value($cart_id,'boxcuration_maxamount');
					//echo '<pre>';print_r($curation_maxcost);exit;
					if(!empty($curation_maxcost) && $curation_maxcost>0 )
					{
						$data['package_cost']['max_box_cost_overide'] = $curation_maxcost;
					}else
					{
						$data['package_cost']['max_box_cost_overide'] = $data['package_cost']['max_box_cost'];
					}

				}
				$data['getAllTag'] 	= $this->Stylist_model->get_all_tag();	
				//echo '<pre>';print_r($data['package_cost']);exit;
			}
		}
		//echo '<pre>';print_r($data['order_data']);exit;
		$this->load->view('common/header2');
		$this->load->view('scbox/product_curation_view',$data);
		$this->load->view('common/footer2');	
	}

	function overideCurationAmt()
	{
		$password = $this->input->post('password');
		$overideAmt = $this->input->post('overideAmt');
		$cart_id = $this->input->post('cart_id');
		$arrData = array();

		if($password!='' && $overideAmt!='' && $overideAmt>0 && $cart_id!='')
		{			
			$arrData['cart_meta_value'] = $overideAmt;			
			$response = $this->Cost_model->addUpdate_cart_meta($arrData,$cart_id);
			if($response){
				echo 'success';
			}else{
				echo 'fail';
			}
		}else
		{
			echo 'Parameters missing';
		}
	}
	
}
?>