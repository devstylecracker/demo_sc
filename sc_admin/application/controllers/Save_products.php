<?php
ini_set('memory_limit', '-1');
defined('BASEPATH') OR exit('No direct script access allowed');
class Save_products extends MY_Controller {

	function __construct()
    {
        // Call the Model constructor

        parent::__construct();
        $this->load->model('Saveproducts_model');

    }

	public function index()
	{
		
		if(!empty($_POST)){
			// echo "<pre>";print_r($_POST);exit;
			$product_ids = $this->input->post('product_id');
			$collection_ids = $this->input->post('collection_id');
			if($product_ids != '')$data = $this->Saveproducts_model->save_homeproducts($product_ids,$key='home_page_products');
			if($collection_ids != '')$data = $this->Saveproducts_model->save_homeproducts($collection_ids,$key='home_page_collections');
			redirect('/','refresh');
		}else{
			$data['product_ids'] = $this->Saveproducts_model->get_old_products($key='home_page_products');
			$data['collection_ids'] = $this->Saveproducts_model->get_old_products($key='home_page_collections');
			$this->load->view('common/header2');      
			$this->load->view('save_homeproducts',$data);
			$this->load->view('common/footer2');
		}
		
	}
	
}