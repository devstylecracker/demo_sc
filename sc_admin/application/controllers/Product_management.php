<?php
ini_set('memory_limit', '-1');
defined('BASEPATH') OR exit('No direct script access allowed');
class Product_management extends MY_Controller {

	function __construct()
    {
        // Call the Model constructor

        parent::__construct();
        $this->load->model('productmanagement_model');
        $this->load->model('product_model');
        $this->load->model('Categories_model');
        $this->load->model('brandusers_model');
        $this->load->model('store_model');
        $this->load->model('brand_model');
        $this->load->model('tag_model');
        $this->load->library('curl');

    }

	public function index($offset = null)
	{
		if(!$this->session->userdata('user_id'))
		{
			$this->load->view('login');

		}else
		{
			 $data_post = array();

			 if($this->has_rights(39) == 1)
			{
				
                $search_by = 0;
				$table_search = 0;
				//echo "<pre>"; print_r($this->uri); 
                $search_by = $this->input->post('search_by')!='' ? $this->input->post('search_by') : $this->uri->segment(3);
               // $table_search = $this->input->post('table_search')!='' ? $this->input->post('table_search') : $this->uri->segment(4);
               if($search_by == 8)
				{
					$table_search = $this->input->post('table_search1')!='' ? $this->input->post('table_search1') : $this->uri->segment(4);
				}
				else{
					$table_search = $this->input->post('table_search')!='' ? $this->input->post('table_search') : $this->uri->segment(4);
				}
                if($search_by!='0' && $table_search!='0' && $search_by!='' && $table_search!=''){ 
                	$paginationUrl = 'product_management/index/'.$search_by.'/'.$table_search;
                	$uri_segment = 5;
            	}else{
            		$paginationUrl = 'product_management/index/0/0';
            		$uri_segment = 5;
            	}

                $config['per_page'] = 20;
                $offset = $this->uri->segment(5)!='' ? $this->uri->segment(5) : 0;
                $config['total_rows'] = count($this->productmanagement_model->get_all_product_details($search_by,$table_search,'',''));
                $total_rows = $config['total_rows'];
                $get_data = $this->productmanagement_model->get_all_product_details($search_by,$table_search,$offset,$config['per_page']);
				//echo $this->db->last_query();
                $count = $config['total_rows'];
                $data_post['product'] = $get_data;
                $data_post['search_by'] = $search_by;
                $data_post['table_search'] = $table_search;
               // $this->customPagination($paginationUrl,$count,$uri_segment,$get_data,$total_rows);


				$config['base_url'] = base_url().$paginationUrl;		
				$config['total_rows'] = $count;
				$config['per_page'] = 20;
				$config['uri_segment'] = $uri_segment;
				$config['use_page_numbers'] = TRUE;
				$config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
				$config['full_tag_close'] = '</ul></div>';

				$config['first_link'] = '&laquo; First';
				$config['first_tag_open'] = '<li>';
				$config['first_tag_close'] = '</li>';

				$config['last_link'] = 'Last &raquo;';
				$config['last_tag_open'] = '<li>';
				$config['last_tag_close'] = '</li>';

				$config['next_link'] = 'Next &rarr;';
				$config['next_tag_open'] = '<li>';
				$config['next_tag_close'] = '</li>';

				$config['prev_link'] = '&larr; Previous';
				$config['prev_tag_open'] = '<li>';
				$config['prev_tag_close'] = '</li>';

				$config['cur_tag_open'] = '<li><a href="" style="background-color:#7CCDEF;">';
				$config['cur_tag_close'] = '</a></li>';

				$config['num_tag_open'] = '<li>';
				$config['num_tag_close'] = '</li>';

				
				$this->pagination->initialize($config);	
				$this->data['total_rows'] = $total_rows;
				$data_post['cat_data'] = $this->Categories_model->get_all_categories();
				$data_post['tag_data'] = $this->tag_model->get_all_tag('','','','');
				//$data_post['brand_store'] = $this->Categories_model->get_brands_store();

				$data_post['brand_store'] = $this->Categories_model->get_storeBrandData();

				$this->load->view('common/header');
				$this->load->view('product/products_manage_view',$data_post);
				$this->load->view('common/footer');

			}else
			{
				$this->load->view('common/header');
                $this->load->view('not_permission');
                $this->load->view('common/footer');

			}

		}
	}




	public function product_add(){

		if(!$this->session->userdata('user_id'))
		{
			$this->load->view('login');

		}else{
			 $data = array();
			 $data['brand_data'] = $this->store_model->all_brands();
			 $data['tag_data'] = $this->tag_model->get_all_tag('','','','');
			 $data['cat_data'] = $this->Categories_model->get_cat_data(); 			 
			 $data['all_stores'] = $this->productmanagement_model->all_store();
			 $data['all_attributes'] = $this->productmanagement_model->all_attributes();

			 if($this->has_rights(39) == 1){

				if($this->input->post()){
					$ok =0;
					$this->form_validation->set_rules('product_name','Product Name','required|trim|min_length[2]|max_length[75]');
					$this->form_validation->set_rules('product_slug','Product Slug','required|is_unique[product_desc.slug]');
					$this->form_validation->set_rules('product_price','Product Price','required|is_natural');
					$this->form_validation->set_rules('sku_number','Product sku','required');
					$this->form_validation->set_rules('parent_sku','Parent sku','required');

					$config['upload_path'] = 'assets/products/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']	= '2048'; // 2048 KB = 2MB = 2048000 B
					$config['max_width']  = '1400';
					//$config['max_height']  = '500';
					$config['file_name'] = time().mt_rand(0,10000);
					$this->upload->initialize($config);
					if ($this->form_validation->run() == FALSE){

						$this->load->view('common/header',$data);
						$this->load->view('product/product_add_new');
						$this->load->view('common/footer');
				        $ok =0;
					}else if(!($this->upload->do_upload('product_images'))){
							$data['error'] = array($this->upload->display_errors());
							print_r($data['error']);
							$this->load->view('common/header',$data);
							$this->load->view('product/product_add_new');
							$this->load->view('common/footer');

							$ok =0;
					}else{
						
						    
							$company_logo= $this->upload->data('file_name');
							$sizeImg = getimagesize('./assets/products/'.$company_logo);

							$config['image_library'] = 'gd2';
							$config['source_image'] = './assets/products/'.$company_logo;
							$config['new_image'] = './assets/products/thumb/';
							$config['create_thumb'] = TRUE;
							$config['thumb_marker'] = '';
							$config['maintain_ratio'] = TRUE;
							$config['width'] = 160;
							//$config['height'] = 200;

							$this->image_lib->initialize($config);

							if (!$this->image_lib->resize())
							{
								$this->image_lib->display_errors();
								$ok =0;

							}else{

								$ok =1;
							}


							$config['image_library'] = 'gd2';
							$config['source_image'] = './assets/products/'.$company_logo;
							$config['new_image'] = './assets/products/thumb_160/';
							$config['create_thumb'] = TRUE;
							$config['thumb_marker'] = '';
							$config['maintain_ratio'] = TRUE;
							$config['width'] = 160;
							//$config['height'] = 200;

							$this->image_lib->initialize($config);

							if (!$this->image_lib->resize())
							{
								$this->image_lib->display_errors();
								$ok =0;

							}else{

								$ok =1;
							}


							$config['image_library'] = 'gd2';
							$config['source_image'] = './assets/products/'.$company_logo;
							$config['new_image'] = './assets/products/thumb_310/';
							$config['create_thumb'] = TRUE;
							$config['thumb_marker'] = '';
							$config['maintain_ratio'] = TRUE;
							$config['width'] = 310;
							//$config['height'] = 200;

							$this->image_lib->initialize($config);

							if (!$this->image_lib->resize())
							{
								$this->image_lib->display_errors();
								$ok =0;

							}else{

								$ok =1;
							}

							/* Thumb for 274x340  */
							if(!empty($sizeImg))
							{
								if($sizeImg[1]<=340)
								{
									$config['image_library'] = 'gd2';
									$config['source_image'] = './assets/products/'.$company_logo;
									$config['new_image'] = './assets/products/thumb_340/';
									$config['create_thumb'] = TRUE;
									$config['thumb_marker'] = '';
									$config['maintain_ratio'] = TRUE;
									//$config['width'] = 310;
									$config['height'] = $sizeImg[1];

									$this->image_lib->initialize($config);

									if (!$this->image_lib->resize())
									{
										$this->image_lib->display_errors();
										$ok =0;

									}else{

										$ok =1;
									}
								}else
								{
									$config['image_library'] = 'gd2';
									$config['source_image'] = './assets/products/'.$company_logo;
									$config['new_image'] = './assets/products/thumb_340/';
									$config['create_thumb'] = TRUE;
									$config['thumb_marker'] = '';
									$config['maintain_ratio'] = TRUE;
									//$config['width'] = 310;
									$config['height'] = 340;

									$this->image_lib->initialize($config);

									if (!$this->image_lib->resize())
									{
										$this->image_lib->display_errors();
										$ok =0;

									}else{

										$ok =1;
									}
								}
							}

							/* Thumb for 440x546   */
							if(!empty($sizeImg))
							{
								if($sizeImg[1]<=546)
								{
									$config['image_library'] = 'gd2';
									$config['source_image'] = './assets/products/'.$company_logo;
									$config['new_image'] = './assets/products/thumb_546/';
									$config['create_thumb'] = TRUE;
									$config['thumb_marker'] = '';
									$config['maintain_ratio'] = TRUE;
									//$config['width'] = 310;
									$config['height'] = $sizeImg[1];

									$this->image_lib->initialize($config);

									if (!$this->image_lib->resize())
									{
										$this->image_lib->display_errors();
										$ok =0;

									}else{

										$ok =1;
									}
								}else
								{
									$config['image_library'] = 'gd2';
									$config['source_image'] = './assets/products/'.$company_logo;
									$config['new_image'] = './assets/products/thumb_546/';
									$config['create_thumb'] = TRUE;
									$config['thumb_marker'] = '';
									$config['maintain_ratio'] = TRUE;
									//$config['width'] = 310;
									$config['height'] = 546;

									$this->image_lib->initialize($config);

									if (!$this->image_lib->resize())
									{
										$this->image_lib->display_errors();
										$ok =0;

									}else{

										$ok =1;
									}
								}
							}

							/* Thumb for 100x124    */
							if(!empty($sizeImg))
							{
								if($sizeImg[1]<=124)
								{
									$config['image_library'] = 'gd2';
									$config['source_image'] = './assets/products/'.$company_logo;
									$config['new_image'] = './assets/products/thumb_124/';
									$config['create_thumb'] = TRUE;
									$config['thumb_marker'] = '';
									$config['maintain_ratio'] = TRUE;
									//$config['width'] = 310;
									$config['height'] = $sizeImg[1];

									$this->image_lib->initialize($config);

									if (!$this->image_lib->resize())
									{
										$this->image_lib->display_errors();
										$ok =0;

									}else{

										$ok =1;
									}
								}else
								{
									$config['image_library'] = 'gd2';
									$config['source_image'] = './assets/products/'.$company_logo;
									$config['new_image'] = './assets/products/thumb_124/';
									$config['create_thumb'] = TRUE;
									$config['thumb_marker'] = '';
									$config['maintain_ratio'] = TRUE;
									//$config['width'] = 310;
									$config['height'] = 124;

									$this->image_lib->initialize($config);

									if (!$this->image_lib->resize())
									{
										$this->image_lib->display_errors();
										$ok =0;

									}else{

										$ok =1;
									}
								}
							}



						}

					if($ok == 1){
							$files = $_FILES; $extra_images =array();
							if(!empty($_FILES['extra_images'])){
							$cpt = count($_FILES['extra_images']['name']);
				    		
						    for($g=0; $g<$cpt; $g++)
						    {

						       		$ext = pathinfo($files['extra_images']['name'][$g], PATHINFO_EXTENSION);
						       		if($ext == 'JPG' || $ext == 'jpg' || $ext == 'png' || $ext == 'jpeg' || $ext == 'PNG' || $ext == 'JPEG'){
										$img_name = time().mt_rand(0,10000).'_sc_'.$g.'.'.$ext;
										$extra_images[] = $img_name; 
										$pathAndName = './assets/products_extra/'.$img_name;
										$moveResult = move_uploaded_file($files['extra_images']['tmp_name'][$g], $pathAndName);
										//$this->createThumbs( $pathAndName, $this->config->item('wordrobe_thumb_path'), 100 ,$img_name);
										$sizeImg = getimagesize($pathAndName);

										$config['image_library'] = 'gd2';
										$config['source_image'] = $pathAndName;
										$config['new_image'] =  './assets/products_extra/thumb/';
										$config['create_thumb'] = TRUE;
										$config['thumb_marker'] = '';
										$config['width'] = 180;
										//$config['height'] = 180;

										//$this->load->library('image_lib', $config);
										$this->image_lib->initialize($config);

										if ( ! $this->image_lib->resize())
										{
				    						echo $this->image_lib->display_errors();
										}

										$config['image_library'] = 'gd2';
										$config['source_image'] = $pathAndName;
										$config['new_image'] =  './assets/products_extra/thumb_160/';
										$config['create_thumb'] = TRUE;
										$config['thumb_marker'] = '';
										$config['width'] = 160;
										//$config['height'] = 180;

										//$this->load->library('image_lib', $config);
										$this->image_lib->initialize($config);

										if ( ! $this->image_lib->resize())
										{
				    						echo $this->image_lib->display_errors();
										}

										$config['image_library'] = 'gd2';
										$config['source_image'] = $pathAndName;
										$config['new_image'] =  './assets/products_extra/thumb_310/';
										$config['create_thumb'] = TRUE;
										$config['thumb_marker'] = '';
										$config['width'] = 310;
										//$config['height'] = 180;

										//$this->load->library('image_lib', $config);
										$this->image_lib->initialize($config);

										if ( ! $this->image_lib->resize())
										{
				    						echo $this->image_lib->display_errors();
										}

										/*thumb for 274x340 */
										if(!empty($sizeImg))
										{
											if($sizeImg[1]<=340)
											{
												$config['image_library'] = 'gd2';
												$config['source_image'] = $pathAndName;
												$config['new_image'] =  './assets/products_extra/thumb_340/';
												$config['create_thumb'] = TRUE;
												$config['thumb_marker'] = '';
												//$config['width'] = 310;
												$config['height'] = $sizeImg[1];
												//$this->load->library('image_lib', $config);
												$this->image_lib->initialize($config);

												if ( ! $this->image_lib->resize())
												{
						    						echo $this->image_lib->display_errors();
												}
											}else
											{
												$config['image_library'] = 'gd2';
												$config['source_image'] = $pathAndName;
												$config['new_image'] =  './assets/products_extra/thumb_340/';
												$config['create_thumb'] = TRUE;
												$config['thumb_marker'] = '';
												//$config['width'] = 310;
												$config['height'] = 340;

												//$this->load->library('image_lib', $config);
												$this->image_lib->initialize($config);

												if ( ! $this->image_lib->resize())
												{
						    						echo $this->image_lib->display_errors();
												}
											}
										}

										/*thumb for 440x546  */
										if(!empty($sizeImg))
										{
											if($sizeImg[1]<=546)
											{
												$config['image_library'] = 'gd2';
												$config['source_image'] = $pathAndName;
												$config['new_image'] =  './assets/products_extra/thumb_546/';
												$config['create_thumb'] = TRUE;
												$config['thumb_marker'] = '';
												//$config['width'] = 310;
												$config['height'] = $sizeImg[1];

												//$this->load->library('image_lib', $config);
												$this->image_lib->initialize($config);

												if ( ! $this->image_lib->resize())
												{
						    						echo $this->image_lib->display_errors();
												}
											}else
											{
												$config['image_library'] = 'gd2';
												$config['source_image'] = $pathAndName;
												$config['new_image'] =  './assets/products_extra/thumb_546/';
												$config['create_thumb'] = TRUE;
												$config['thumb_marker'] = '';
												//$config['width'] = 310;
												$config['height'] = 546;

												//$this->load->library('image_lib', $config);
												$this->image_lib->initialize($config);

												if ( ! $this->image_lib->resize())
												{
						    						echo $this->image_lib->display_errors();
												}
											}
										}

										/*thumb for 100x124  */
										if(!empty($sizeImg))
										{
											if($sizeImg[1]<=546)
											{
												$config['image_library'] = 'gd2';
												$config['source_image'] = $pathAndName;
												$config['new_image'] =  './assets/products_extra/thumb_124/';
												$config['create_thumb'] = TRUE;
												$config['thumb_marker'] = '';
												//$config['width'] = 310;
												$config['height'] = $sizeImg[1];

												//$this->load->library('image_lib', $config);
												$this->image_lib->initialize($config);

												if ( ! $this->image_lib->resize())
												{
						    						echo $this->image_lib->display_errors();
												}
											}else
											{
												$config['image_library'] = 'gd2';
												$config['source_image'] = $pathAndName;
												$config['new_image'] =  './assets/products_extra/thumb_124/';
												$config['create_thumb'] = TRUE;
												$config['thumb_marker'] = '';
												//$config['width'] = 310;
												$config['height'] = 124;

												//$this->load->library('image_lib', $config);
												$this->image_lib->initialize($config);

												if ( ! $this->image_lib->resize())
												{
						    						echo $this->image_lib->display_errors();
												}
											}
										}
									}
							}
							}

							$product_name = $this->input->post('product_name');
							$product_slug = $this->input->post('product_slug');
							$product_desc = $this->input->post('product_desc');
							$brand = $this->input->post('brand');
							$product_tags_array = $this->input->post('product_tags');							
							$category = $this->input->post('category');							
							$product_price = $this->input->post('product_price');						
							$sku_number = $this->input->post('sku_number');
							$stock_count = $this->input->post('stock_count');
							$product_avail_array = $this->input->post('product_avail');						
							$attributes = $this->input->post('attributes');
							$compare_price = $this->input->post('compare_price');
							$discount_start_date = $this->input->post('discount_start_date');
							$discount_end_date = $this->input->post('discount_end_date');	
							$product_status = $this->input->post('product_status');	
							$parent_sku = $this->input->post('parent_sku');						
							
							$brand_code = $this->productmanagement_model->getbrand_code($brand);						

							$data['product_info'] = array(
									'name'=> $product_name,
									'description' => $product_desc,
									'brand_id' =>$brand,									
									'price' =>$product_price,									
									'sku_number' =>$sku_number,
									'stock_count' =>$stock_count,
									'status' =>$product_status,
									'created_by' => $this->session->userdata('user_id'),
									'created_datetime'=>date('Y-m-d H:i:s'),
									'image'=>$company_logo,
									'slug'=>$product_slug,
									'compare_price'=>$compare_price,
									'discount_start_date'=>$discount_start_date,
									'discount_end_date'=>$discount_end_date,
									'store_id'=>$product_avail_array[0],
									'parent_id'=>$parent_sku

								);

							$data['product_tags_array'] =  $product_tags_array;							
							$data['category'] = $category;
							$data['attributes'] = $attributes;
							$data['product_avail_array'] =  $product_avail_array;
							$data['extra_images'] = $extra_images;							

							if($this->productmanagement_model->add_product($data)){

								$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Product Added Successfully</div>');
								redirect('/Product_management');
							}else{
								echo '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>Something goes wrong Please try again</div>';

							}
						}



					}else{

					$this->load->view('common/header',$data);
					$this->load->view('product/product_add_new');
					$this->load->view('common/footer');

				}
			 }else{

				$this->load->view('common/header');
                $this->load->view('not_permission');
                $this->load->view('common/footer');
			}
		}


		}

	public function product_edit(){

		if(!$this->session->userdata('user_id'))
		{
			$this->load->view('login');

		}else if($this->has_rights(41) == 1){

			$product_id = $this->uri->segment(3);
			$data['product_info'] = $this->productmanagement_model->get_product_info($product_id);
			$data['product_tags'] = $this->productmanagement_model->get_product_tags($product_id);
			$data['product_avalibility'] = $this->productmanagement_model->get_product_avalibility($product_id);			
			$data['get_extra_images'] = $this->productmanagement_model->get_extra_images($product_id);			
			$data['category'] = $this->productmanagement_model->get_products_categories($product_id);
			$data['attributes'] = $this->productmanagement_model->get_products_attributes($product_id);

			//echo '<pre>attr';print_r($data['attributes']);exit;
			$data['brand_data'] = $this->store_model->all_brands();
			$data['tag_data'] = $this->tag_model->get_all_tag('','','','');			
			//$data['target_marget'] = $this->brandusers_model->target_market();
			$data['all_stores'] = $this->productmanagement_model->all_store();			
			$data['all_attributes'] = $this->productmanagement_model->all_attributes();
			$data['cat_data'] = $this->productmanagement_model->get_cat_data($data['category']); 			

			if($this->input->post() && $product_id!=''){
				$ok =1;

					$this->form_validation->set_rules('product_name','Product Name','required|trim|min_length[2]|max_length[75]');
					$this->form_validation->set_rules('product_price','Product Price','required|is_natural');
					//$this->form_validation->set_rules('product_url','Product url','required');
					$this->form_validation->set_rules('sku_number','Product SKU','required');
					$this->form_validation->set_rules('parent_sku','Parent SKU','required');

			}

			if ($this->form_validation->run() == FALSE){

						$this->load->view('common/header',$data);
						$this->load->view('product/product_edit_new');
						$this->load->view('common/footer');
				        $ok =0;
			}else{
				$ok =1;

					if(isset($_FILES['product_images']['name']) && $_FILES['product_images']['name']!=''){

						$config['upload_path'] = 'assets/products/';
						$config['allowed_types'] = 'gif|jpg|png|jpeg';
						$config['max_size']	= '2048';
						$config['max_width']  = '1400';
						$config['overwrite'] = TRUE; //overwrite user avatar
						//$config['max_height']  = '500';
						$config['file_name'] = $data['product_info'][0]['image'];
						$this->upload->initialize($config);

						if(!($this->upload->do_upload('product_images'))){
							$data['error'] = array($this->upload->display_errors());
							/*$this->load->view('common/header',$data);
							$this->load->view('product/product_add');
							$this->load->view('common/footer');*/

							$ok =0;
							}else{

									$sizeImg = getimagesize('./assets/products/'.$data['product_info'][0]['image']);

									$config['image_library'] = 'gd2';
									$config['source_image'] = './assets/products/'.$data['product_info'][0]['image'];
									$config['new_image'] = './assets/products/thumb/';
									$config['create_thumb'] = TRUE;
									$config['thumb_marker'] = '';
									$config['maintain_ratio'] = TRUE;
									$config['overwrite'] = TRUE; //overwrite user avatar
									$config['width'] = 200;
									//$config['height'] = 200;

									$this->image_lib->initialize($config);

									if (!$this->image_lib->resize())
									{
										$this->image_lib->display_errors();
										$ok =0;

									}else{

										$ok =1;
									}

									$config['image_library'] = 'gd2';
									$config['source_image'] = './assets/products/'.$data['product_info'][0]['image'];
									$config['new_image'] = './assets/products/thumb_160/';
									$config['create_thumb'] = TRUE;
									$config['thumb_marker'] = '';
									$config['maintain_ratio'] = TRUE;
									$config['overwrite'] = TRUE; //overwrite user avatar
									$config['width'] = 160;
									//$config['height'] = 200;

									$this->image_lib->initialize($config);

									if (!$this->image_lib->resize())
									{
										$this->image_lib->display_errors();
										$ok =0;

									}else{

										$ok =1;
									}


									$config['image_library'] = 'gd2';
									$config['source_image'] = './assets/products/'.$data['product_info'][0]['image'];
									$config['new_image'] = './assets/products/thumb_310/';
									$config['create_thumb'] = TRUE;
									$config['thumb_marker'] = '';
									$config['maintain_ratio'] = TRUE;
									$config['overwrite'] = TRUE; //overwrite user avatar
									$config['width'] = 310;
									//$config['height'] = 200;

									$this->image_lib->initialize($config);

									if (!$this->image_lib->resize())
									{
										$this->image_lib->display_errors();
										$ok =0;

									}else{

										$ok =1;
									}

									/* Thumb for 274x340  */
									if(!empty($sizeImg))
									{
										if($sizeImg[1]<=340)
										{
											$config['image_library'] = 'gd2';
											$config['source_image'] = './assets/products/'.$data['product_info'][0]['image'];
											$config['new_image'] = './assets/products/thumb_340/';
											$config['create_thumb'] = TRUE;
											$config['thumb_marker'] = '';
											$config['maintain_ratio'] = TRUE;
											$config['overwrite'] = TRUE; //overwrite user avatar
											//$config['width'] = 310;
											$config['height'] = $sizeImg[1];

											$this->image_lib->initialize($config);

											if (!$this->image_lib->resize())
											{
												$this->image_lib->display_errors();
												$ok =0;

											}else{

												$ok =1;
											}
										}else
										{
											$config['image_library'] = 'gd2';
											$config['source_image'] = './assets/products/'.$data['product_info'][0]['image'];
											$config['new_image'] = './assets/products/thumb_340/';
											$config['create_thumb'] = TRUE;
											$config['thumb_marker'] = '';
											$config['maintain_ratio'] = TRUE;
											$config['overwrite'] = TRUE; //overwrite user avatar
											//$config['width'] = 310;
											$config['height'] = 340;

											$this->image_lib->initialize($config);

											if (!$this->image_lib->resize())
											{
												$this->image_lib->display_errors();
												$ok =0;

											}else{

												$ok =1;
											}
										}
									}

									/* Thumb for 440x546   */
									if(!empty($sizeImg))
									{
										if($sizeImg[1]<=546)
										{
											$config['image_library'] = 'gd2';
											$config['source_image'] = './assets/products/'.$data['product_info'][0]['image'];
											$config['new_image'] = './assets/products/thumb_546/';
											$config['create_thumb'] = TRUE;
											$config['thumb_marker'] = '';
											$config['maintain_ratio'] = TRUE;
											$config['overwrite'] = TRUE; //overwrite user avatar
											//$config['width'] = 310;
											$config['height'] = $sizeImg[1];

											$this->image_lib->initialize($config);

											if (!$this->image_lib->resize())
											{
												$this->image_lib->display_errors();
												$ok =0;

											}else{

												$ok =1;
											}
										}else
										{
											$config['image_library'] = 'gd2';
											$config['source_image'] = './assets/products/'.$data['product_info'][0]['image'];
											$config['new_image'] = './assets/products/thumb_546/';
											$config['create_thumb'] = TRUE;
											$config['thumb_marker'] = '';
											$config['maintain_ratio'] = TRUE;
											$config['overwrite'] = TRUE; //overwrite user avatar
											//$config['width'] = 310;
											$config['height'] = 546;

											$this->image_lib->initialize($config);

											if (!$this->image_lib->resize())
											{
												$this->image_lib->display_errors();
												$ok =0;

											}else{

												$ok =1;
											}
										}
									}

									/* Thumb for 100x124    */
									if(!empty($sizeImg))
									{
										if($sizeImg[1]<=124)
										{
											$config['image_library'] = 'gd2';
											$config['source_image'] = './assets/products/'.$data['product_info'][0]['image'];
											$config['new_image'] = './assets/products/thumb_124/';
											$config['create_thumb'] = TRUE;
											$config['thumb_marker'] = '';
											$config['maintain_ratio'] = TRUE;
											$config['overwrite'] = TRUE; //overwrite user avatar
											//$config['width'] = 310;
											$config['height'] = $sizeImg[1];

											$this->image_lib->initialize($config);

											if (!$this->image_lib->resize())
											{
												$this->image_lib->display_errors();
												$ok =0;

											}else{

												$ok =1;
											}
										}else
										{
											$config['image_library'] = 'gd2';
											$config['source_image'] = './assets/products/'.$data['product_info'][0]['image'];
											$config['new_image'] = './assets/products/thumb_124/';
											$config['create_thumb'] = TRUE;
											$config['thumb_marker'] = '';
											$config['maintain_ratio'] = TRUE;
											$config['overwrite'] = TRUE; //overwrite user avatar
											//$config['width'] = 310;
											$config['height'] = 124;

											$this->image_lib->initialize($config);

											if (!$this->image_lib->resize())
											{
												$this->image_lib->display_errors();
												$ok =0;

											}else{

												$ok =1;
											}
										}
									}

								}
					}


					$files = $_FILES; $extra_images =array();
							if(!empty($_FILES['extra_images'])){
							$cpt = count($_FILES['extra_images']['name']);
				    		
						    for($g=0; $g<$cpt; $g++)
						    {

						       		$ext = pathinfo($files['extra_images']['name'][$g], PATHINFO_EXTENSION);
						       		if($ext == 'JPG' || $ext == 'jpg' || $ext == 'png' || $ext == 'jpeg' || $ext == 'PNG' || $ext == 'JPEG'){
										$img_name = time().mt_rand(0,10000).'_sc_'.$g.'.'.$ext;
										$extra_images[] = $img_name; 
										$pathAndName = './assets/products_extra/'.$img_name;
										$moveResult = move_uploaded_file($files['extra_images']['tmp_name'][$g], $pathAndName);
										//$this->createThumbs( $pathAndName, $this->config->item('wordrobe_thumb_path'), 100 ,$img_name);
										$sizeImg = getimagesize('./assets/products_extra/'.$img_name);

										$config['image_library'] = 'gd2';
										$config['source_image'] = $pathAndName;
										$config['new_image'] =  './assets/products_extra/thumb/';
										$config['create_thumb'] = TRUE;
										$config['thumb_marker'] = '';
										$config['width'] = 180;
										$config['height'] = 180;

										//$this->load->library('image_lib', $config);
										$this->image_lib->initialize($config);

										if ( ! $this->image_lib->resize())
										{
				    						echo $this->image_lib->display_errors();
										}

										$config['image_library'] = 'gd2';
										$config['source_image'] = $pathAndName;
										$config['new_image'] =  './assets/products_extra/thumb/';
										$config['create_thumb'] = TRUE;
										$config['thumb_marker'] = '';
										$config['width'] = 200;
										//$config['height'] = 180;

										//$this->load->library('image_lib', $config);
										$this->image_lib->initialize($config);

										if ( ! $this->image_lib->resize())
										{
				    						echo $this->image_lib->display_errors();
										}

										$config['image_library'] = 'gd2';
										$config['source_image'] = $pathAndName;
										$config['new_image'] =  './assets/products_extra/thumb_160/';
										$config['create_thumb'] = TRUE;
										$config['thumb_marker'] = '';
										$config['width'] = 160;
										//$config['height'] = 180;

										//$this->load->library('image_lib', $config);
										$this->image_lib->initialize($config);

										if ( ! $this->image_lib->resize())
										{
				    						echo $this->image_lib->display_errors();
										}

										$config['image_library'] = 'gd2';
										$config['source_image'] = $pathAndName;
										$config['new_image'] =  './assets/products_extra/thumb_310/';
										$config['create_thumb'] = TRUE;
										$config['thumb_marker'] = '';
										$config['width'] = 310;
										//$config['height'] = 180;

										//$this->load->library('image_lib', $config);
										$this->image_lib->initialize($config);

										if ( ! $this->image_lib->resize())
										{
				    						echo $this->image_lib->display_errors();
										}

										/*thumb for 274x340 */
										if(!empty($sizeImg))
										{
											if($sizeImg[1]<=340)
											{
												$config['image_library'] = 'gd2';
												$config['source_image'] = $pathAndName;
												$config['new_image'] =  './assets/products_extra/thumb_340/';
												$config['create_thumb'] = TRUE;
												$config['thumb_marker'] = '';
												//$config['width'] = 310;
												$config['height'] = $sizeImg[1];

												//$this->load->library('image_lib', $config);
												$this->image_lib->initialize($config);

												if ( ! $this->image_lib->resize())
												{
						    						echo $this->image_lib->display_errors();
												}
											}else
											{
												$config['image_library'] = 'gd2';
												$config['source_image'] = $pathAndName;
												$config['new_image'] =  './assets/products_extra/thumb_340/';
												$config['create_thumb'] = TRUE;
												$config['thumb_marker'] = '';
												//$config['width'] = 310;
												$config['height'] = 340;

												//$this->load->library('image_lib', $config);
												$this->image_lib->initialize($config);

												if ( ! $this->image_lib->resize())
												{
						    						echo $this->image_lib->display_errors();
												}
											}
										}

										/*thumb for 440x546  */
										if(!empty($sizeImg))
										{
											if($sizeImg[1]<=546)
											{
												$config['image_library'] = 'gd2';
												$config['source_image'] = $pathAndName;
												$config['new_image'] =  './assets/products_extra/thumb_546/';
												$config['create_thumb'] = TRUE;
												$config['thumb_marker'] = '';
												//$config['width'] = 310;
												$config['height'] = $sizeImg[1];

												//$this->load->library('image_lib', $config);
												$this->image_lib->initialize($config);

												if ( ! $this->image_lib->resize())
												{
						    						echo $this->image_lib->display_errors();
												}
											}else
											{
												$config['image_library'] = 'gd2';
												$config['source_image'] = $pathAndName;
												$config['new_image'] =  './assets/products_extra/thumb_546/';
												$config['create_thumb'] = TRUE;
												$config['thumb_marker'] = '';
												//$config['width'] = 310;
												$config['height'] = 546;

												//$this->load->library('image_lib', $config);
												$this->image_lib->initialize($config);

												if ( ! $this->image_lib->resize())
												{
						    						echo $this->image_lib->display_errors();
												}
											}
										}

										/*thumb for 100x124  */
										if(!empty($sizeImg))
										{
											if($sizeImg[1]<=124)
											{
												$config['image_library'] = 'gd2';
												$config['source_image'] = $pathAndName;
												$config['new_image'] =  './assets/products_extra/thumb_124/';
												$config['create_thumb'] = TRUE;
												$config['thumb_marker'] = '';
												//$config['width'] = 310;
												$config['height'] = $sizeImg[1];

												//$this->load->library('image_lib', $config);
												$this->image_lib->initialize($config);

												if ( ! $this->image_lib->resize())
												{
						    						echo $this->image_lib->display_errors();
												}
											}else
											{
												$config['image_library'] = 'gd2';
												$config['source_image'] = $pathAndName;
												$config['new_image'] =  './assets/products_extra/thumb_124/';
												$config['create_thumb'] = TRUE;
												$config['thumb_marker'] = '';
												//$config['width'] = 310;
												$config['height'] = 124;

												//$this->load->library('image_lib', $config);
												$this->image_lib->initialize($config);

												if ( ! $this->image_lib->resize())
												{
						    						echo $this->image_lib->display_errors();
												}
											}
										}
									}
							}
							}

					if($ok==1){ 
							$product_name = $this->input->post('product_name');
							$product_slug = $this->input->post('product_slug');
							$product_desc = $this->input->post('product_desc');
							$brand = $this->input->post('brand');
							$product_tags_array = $this->input->post('product_tags');
							//$product_url = $this->input->post('product_url');
							$category = $this->input->post('category');
							$attributes = $this->input->post('attributes');
							$sku_number = $this->input->post('sku_number');
							$stock_count = $this->input->post('stock_count');
							$product_price = $this->input->post('product_price');							
							$product_avail_array = $this->input->post('product_avail');							
							$product_status = $this->input->post('product_status');
							$parent_sku = $this->input->post('parent_sku');

							$compare_price = $this->input->post('compare_price');
							$discount_start_date = $this->input->post('discount_start_date');
							$discount_end_date = $this->input->post('discount_end_date');
														
							$slug = $product_slug;

							$qty_size_info = array();
							
							$data['product_info'] = array(
									'name'=> $product_name,
									'description' => $product_desc,
									'brand_id' =>$brand,
									'price' =>$product_price,									
									'status' =>$product_status,
									'sku_number' => $sku_number,
									'stock_count' => $stock_count,
									'modified_by' => $this->session->userdata('user_id'),
									'modified_datetime'=>date('Y-m-d H:i:s'),
									'slug'=>$slug,									
									'compare_price'=>$compare_price,
									'discount_start_date'=>$discount_start_date,
									'discount_end_date'=>$discount_end_date,
									'store_id'=>$product_avail_array[0],
									'parent_id'=>$parent_sku,

								);

							$data['product_tags_array'] =  $product_tags_array;
							$data['category'] = $category;
							$data['attributes'] = $attributes;
							
							$data['product_avail_array'] =  $product_avail_array;
							$data['extra_images'] = $extra_images;
							if($this->productmanagement_model->edit_product($data,$product_id)){

								$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Product Edited Successfully</div>');
								//redirect('/Product_management');
							}else{
								echo '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>Something goes wrong Please try again</div>';

							}
						}
				$this->load->view('common/header',$data);
				$this->load->view('product/product_edit_new');
				$this->load->view('common/footer');
				redirect($this->uri->uri_string());						
			}
		}else{

			$this->load->view('common/header');
            $this->load->view('not_permission');
            $this->load->view('common/footer');
		}


	}
	public function get_sub_cat(){
			$cat_info = array();
			$type = $this->input->post('type');
			$cat_id = $this->input->post('cat_id');
			$sel_id =$this->input->post('sel_id');
			$cat_info = $this->productmanagement_model->get_sub_cat($cat_id);
			$html_cat = '';
			if(!empty($cat_info)){
				$html_cat = $html_cat.'<option value="0">Select Subcategory</option>';
				foreach($cat_info as $val){
					if($val['id'] == $sel_id){
						$html_cat = $html_cat.'<option value="'.$val['id'].'" selected>'.$val['name'].'</option>';
						}else{
						$html_cat = $html_cat.'<option value="'.$val['id'].'">'.$val['name'].'</option>';
						}

					}
				}
			echo $html_cat;

		}


	public function get_var_value_type(){
			$cat_info = array();
			$type = $this->input->post('type');
			$cat_id = $this->input->post('sub_cat_id');
			$sel_id =$this->input->post('sel_id');
			$cat_info = $this->productmanagement_model->get_var_type($cat_id);
			$html_cat = '<div class="form-group">';
			if(!empty($cat_info)){
				$new_name = '';
				$old_name = '';
				$product_variation_values = $this->productmanagement_model->get_product_variations($sel_id);
				foreach($cat_info as $val){
					$new_name = $val['variation'];
					if( $new_name != $old_name) {
						$html_cat = $html_cat.'</div><div class="form-group"><label>'.$val['variation'].'</label><br>
						<input type="hidden" name="vari[]" id="vari" value="'.$val['nid'].'">
						';
						}
						if(!empty($product_variation_values)){
						if(in_array($val['id'], $product_variation_values)){
						$html_cat = $html_cat.'<label class="radio-inline"><input type="radio" name="variation_val[]['.$val['nid'].']" value="'.$val['id'].'" checked>'.$val['variation_type'].'</label>';
							}else{
						$html_cat = $html_cat.'<label class="radio-inline"><input type="radio" name="variation_val[]['.$val['nid'].']" value="'.$val['id'].'">'.$val['variation_type'].'</label>';
							}
						}else{
							$html_cat = $html_cat.'<label class="radio-inline"><input type="radio" name="variation_val[]['.$val['nid'].']" value="'.$val['id'].'">'.$val['variation_type'].'</label>';

						}
						$old_name = $new_name;
					}
				}
			echo $html_cat;

		}

		function product_approve(){
			$product_id = $this->input->post('product_id');
			$product_appr = $this->productmanagement_model->product_approve($product_id);
			if($product_appr){
				return true;
			}else{
				return false;
			}
		}

		function product_reject(){
			$product_id = $this->input->post('product_id');
			$reason = $this->input->post('reason');
			$product_appr = $this->productmanagement_model->product_reject($product_id,$reason);
			if($product_appr){
				return true;
			}else{
				return false;
			}
		}

		function delete_product(){
			$product_id = $this->input->post('prodID');
			$product_appr = $this->productmanagement_model->product_delete($product_id);
			if($product_appr){
				$this->curl->simple_post(BACKENDURL.'/Mailchimp/delete_product/'.$product_id, false, array(CURLOPT_USERAGENT => true)); 
				return true;
			}else{
				return false;
			}
		}

		function delete_extra_img(){
			if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
			}else{
			$type = $this->input->post('delete_extra_img');
			$img_id = $this->input->post('image_id');
			$product_appr = $this->productmanagement_model->delete_extra_img($img_id);
			if($product_appr){
				return true;
			}
			}
		}

		function product_view(){
			if($this->has_rights(39) == 1){
				$product_id = $this->uri->segment(3);
				$data['product_info'] = $this->productmanagement_model->get_product_info($product_id);
				$data['product_tags'] = $this->productmanagement_model->get_product_tags($product_id);
				$data['product_avalibility'] = $this->productmanagement_model->get_product_avalibility($product_id);
				//$data['get_product_variations'] = $this->productmanagement_model->get_product_variations($product_id);
				//$data['product_sub_cat'] = $this->productmanagement_model->get_sub_cat($data['product_info'][0]['product_cat_id']);

				$data['brand_data'] = $this->store_model->all_brands();
				$data['tag_data'] = $this->tag_model->get_all_tag('','','','');
				$data['category'] = $this->category_model->get_all_categories('','','','');				
				$data['all_stores'] = $this->productmanagement_model->all_store();
				
				$data['product_image_extra_info'] = $this->productmanagement_model->product_image_extra($product_id);
				$data['child_products'] = $this->productmanagement_model->product_children(trim($data['product_info'][0]['sku_number']));	
				
				$this->load->view('common/header');
           		$this->load->view('product/product_details',$data);
           		$this->load->view('common/footer');
			}else{

				$this->load->view('common/header');
           		$this->load->view('not_permission');
           		$this->load->view('common/footer');
			}

		}

		function upload_product_image(){
			if($this->has_rights(41) == 1){
				$data = array();
				$product_id = $this->uri->segment(3);

				$data['product_id'] = $product_id;


				if($this->input->post()){

					$config['upload_path'] = 'assets/products/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']	= '2048'; // 2048 KB = 2MB = 2048000 B
					$config['max_width']  = '1400';
					//$config['max_height']  = '500';
					$config['file_name'] = time().mt_rand(0,10000);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload('product_image')){
						$data['error'] = array($this->upload->display_errors());

						$this->load->view('common/header');
						$this->load->view('product/product_img_upload',$data);
						$this->load->view('common/footer');
				       
					}else{

						$product_image= $this->upload->data('file_name');

						$config['image_library'] = 'gd2';
						$config['source_image'] = './assets/products/'.$product_image;
						$config['new_image'] = './assets/products/thumb_160/';
						$config['create_thumb'] = TRUE;
						$config['thumb_marker'] = '';
						$config['maintain_ratio'] = TRUE;
						$config['width'] = 160;
						//$config['height'] = 200;

						$this->image_lib->initialize($config);

						if (!$this->image_lib->resize())
						{
							$this->image_lib->display_errors();
							
							$data['error'] = array($this->upload->display_errors());

							$this->load->view('common/header');
							$this->load->view('product/product_img_upload',$data);
							$this->load->view('common/footer');

						}else{


							$config['image_library'] = 'gd2';
							$config['source_image'] = './assets/products/'.$product_image;
							$config['new_image'] = './assets/products/thumb_310/';
							$config['create_thumb'] = TRUE;
							$config['thumb_marker'] = '';
							$config['maintain_ratio'] = TRUE;
							$config['width'] = 310;
							//$config['height'] = 200;

							$this->image_lib->initialize($config);
							$this->image_lib->resize();

							$config['image_library'] = 'gd2';
							$config['source_image'] = './assets/products/'.$product_image;
							$config['new_image'] = './assets/products/thumb_124/';
							$config['create_thumb'] = TRUE;
							$config['thumb_marker'] = '';
							$config['maintain_ratio'] = TRUE;
							$config['height'] = 124;
							//$config['height'] = 200;
							$this->image_lib->initialize($config);
							$this->image_lib->resize();

							$config['image_library'] = 'gd2';
							$config['source_image'] = './assets/products/'.$product_image;
							$config['new_image'] = './assets/products/thumb_340/';
							$config['create_thumb'] = TRUE;
							$config['thumb_marker'] = '';
							$config['maintain_ratio'] = TRUE;
							$config['height'] = 340;
							//$config['height'] = 200;
							$this->image_lib->initialize($config);
							$this->image_lib->resize();

							$config['image_library'] = 'gd2';
							$config['source_image'] = './assets/products/'.$product_image;
							$config['new_image'] = './assets/products/thumb_546/';
							$config['create_thumb'] = TRUE;
							$config['thumb_marker'] = '';
							$config['maintain_ratio'] = TRUE;
							$config['height'] = 546;
							//$config['height'] = 200;

							$this->image_lib->initialize($config);
							$this->image_lib->resize();

							/*thumb for 274x340 */
								$config['image_library'] = 'gd2';
								$config['source_image'] = $pathAndName;
								$config['new_image'] =  './assets/products_extra/thumb_340/';
								$config['create_thumb'] = TRUE;
								$config['thumb_marker'] = '';
								//$config['width'] = 310;
								$config['height'] = 340;

								//$this->load->library('image_lib', $config);
								$this->image_lib->initialize($config);

								if ( ! $this->image_lib->resize())
								{
									echo $this->image_lib->display_errors();
								}

								/*thumb for 440x546  */
								$config['image_library'] = 'gd2';
								$config['source_image'] = $pathAndName;
								$config['new_image'] =  './assets/products_extra/thumb_546/';
								$config['create_thumb'] = TRUE;
								$config['thumb_marker'] = '';
								//$config['width'] = 310;
								$config['height'] = 546;

								//$this->load->library('image_lib', $config);
								$this->image_lib->initialize($config);

								if ( ! $this->image_lib->resize())
								{
									echo $this->image_lib->display_errors();
								}

								/*thumb for 100x124  */
								$config['image_library'] = 'gd2';
								$config['source_image'] = $pathAndName;
								$config['new_image'] =  './assets/products_extra/thumb_124/';
								$config['create_thumb'] = TRUE;
								$config['thumb_marker'] = '';
								//$config['width'] = 310;
								$config['height'] = 124;

								//$this->load->library('image_lib', $config);
								$this->image_lib->initialize($config);

								if ( ! $this->image_lib->resize())
								{
									echo $this->image_lib->display_errors();
								}

							$this->productmanagement_model->upload_product_image($product_image,$product_id);
							$data['success'] = "Uploaded Successfully";
							$this->load->view('common/header');
							$this->load->view('product/product_img_upload',$data);
							$this->load->view('common/footer');
						}

					}
				}else{

				$this->load->view('common/header');
           		$this->load->view('product/product_img_upload',$data);
           		$this->load->view('common/footer');

           		}
			}
		}


		function quick_edit(){
			$product_name=$this->input->post('product_name');
			$product_slug=$this->input->post('product_slug');
			$price=$this->input->post('price');
			$product_status=$this->input->post('product_status');
			$product_id=$this->input->post('id');
			$category=$this->input->post('category');
			$product_tags=$this->input->post('product_tags');
			$compare_price = $this->input->post('compare_price');
			if($price >= 0 && $price < 5000){
								$product_price_range = 36;
							}else if($price >= 5000 && $price < 15000){
								$product_price_range = 37;
							}else if($price >= 15000 && $price < 50000){
								$product_price_range = 38;
							}else{
								$product_price_range = 4670;
							}

			$data= array();
			$data['product_info'] = array(
					'name' => $product_name,
					'slug' => $product_slug,
					'price' => $price,
					'status' => $product_status,
					'price_range'=>$product_price_range,
					'compare_price'=>$compare_price,
				);
			$data['category'] = $category;
			$data['product_tags_array'] = $product_tags;
			$this->productmanagement_model->quick_edit($data,$product_id);
		}

		function get_product_cat(){
			$prodID=$this->input->post('prodID');
			$category =  $this->productmanagement_model->get_products_categories($prodID);
			$cat_data = $this->productmanagement_model->get_cat_data($category);

			$html = '';
			if(!empty($cat_data)){
                foreach($cat_data as $val){
                    $html = $html.$val;
                }
            }

            echo $html;
		}

		
		function product_search_download(){
			$is_download = 1;
			$fieldName = array('ID','PRODUCT NAME','BRAND','STATUS');
			$result_data = array();
			$i =0;
			
			$search_by = 0;
			$table_search = 0;
			
            $search_by = $this->uri->segment(3);
            $table_search = $this->uri->segment(4);
                
            $result =  $this->productmanagement_model->get_all_product_details($search_by,$table_search,'','');
            
            foreach($result as $val) {   
            	$result_data[$i][] = $val->id;
            	$result_data[$i][] = $val->name;
            	$result_data[$i][] = $val->brand;
            	if($val->approve_reject=='A'){
            		$result_data[$i][] = "Approved";
            	}else if($val->approve_reject=='P'){
            		$result_data[$i][] = "Pending";
            	}else if($val->approve_reject=='R'){
            		$result_data[$i][] = "Rejected";
            	}else if($val->approve_reject=='D'){
            		$result_data[$i][] = "Pending";
            	}
            	$i++;
            }

            if($is_download == 1)
			{       	
		  	//========================Excel Download==============================
			//$excel_result = $this->writeDataintoCSV($fieldName,$get_data,'view_order_payment');
		   	$this->writeDataintoCSV($fieldName,$result_data,'product_export');
		   	//========================End Excel Download==========================

		   
			}
               
		}
		
		
		public function get_category()
			{
				$seloption = $this->input->post('seloption');
				$arrCategory = $this->Categories_model->get_cat_data($seloption);
				if(is_array($arrCategory) && count($arrCategory)>0)
				{
					foreach($arrCategory as $categoryData)
					{
						echo $categoryData;
					}
				}
			}
	}
?>
