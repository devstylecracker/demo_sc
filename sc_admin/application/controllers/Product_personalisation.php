<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_personalisation extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
        parent::__construct();
        $this->load->model('product_personalisation_model');
        $this->load->model('productmanagement_model');
    }

	public function index()
	{
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{
			if($this->has_rights(70) == 1)
			{	
				$data=array();
				$data['existing_data']=$this->product_personalisation_model->product_personalisation_logic(0);
				$this->load->view('common/header');
				$this->load->view('product_personalisation/product_personalisation',$data);
				$this->load->view('common/footer');
			}else{
				$this->load->view('common/header');
        		$this->load->view('not_permission');
        		$this->load->view('common/footer');
			}
		}
	}

	public function show_gender_specific_personalisation(){
		$data=array();
		$gender=$this->input->post('gender');
		$data['perosonalisation']=$this->product_personalisation_model->show_gender_specific_personalisation($gender);
		echo json_encode($data);
	}

	public function show_category_specific_personalisation(){
		$data=array();
		$personality=$this->input->post('personality');
		$data['category']=$this->product_personalisation_model->show_category_specific_personalisation($personality);
		echo json_encode($data);
	}

	public function load_more_data(){
		$id=$this->input->post('id');
		$html_data = '';
		$data['existing_data']=$this->product_personalisation_model->product_personalisation_logic($id);

		if(!empty($data['existing_data'])) {  $i=$id*50+1;
            foreach($data['existing_data'] as $val){

            	$object_value=$this->product_personalisation_model->get_bucket_name($val['object_value']);
            	$attribute_value_ids=$this->product_personalisation_model->attribute_value_data($val['attribute_value_ids']);
                $html_data =$html_data.'<tr>
                <td width="40%">'.$object_value.'</td>
                <td>'.$val['cat_name'].'</td>
                <td width="40%">'.$attribute_value_ids.'</td>
                <td><button onclick="" type="button" class="btn btn-xs btn-primary btn-delete deleteProd" data-id="'.$val['id'].'" data-toggle="confirmation"><i class="fa fa-trash-o"></i> Delete</button></td>
                </tr>'; $i++; }}else{ $html_data =$html_data.'<tr><td colspan="3">No Records Found</td></tr>';
            }
        echo $html_data;
	}

	public function delete_pa(){
		$id=$this->input->post('prodID');
		$this->product_personalisation_model->delete_pa($id);
		return true;
	}

	public function show_attributes(){
		$personality=$this->input->post('personality');
		$category=$this->input->post('category');
		$html_data = ''; $existing_attr_values=array();

		$data['attributes']=$this->product_personalisation_model->get_all_attributes();
		$existing_attr_values = $this->product_personalisation_model->existing_attr_values($personality,$category);

		if(!empty($data['attributes'])){
			foreach($data['attributes'] as $val){
				$attributes_value = $this->productmanagement_model->attributes_values($val['id']);
				  $html_data = $html_data.'<div class="col-md-6">
                    <div class="form-group">'.$val['attribute_name'].'</label>';

                    if(!empty($attributes_value)){
                    	$html_data = $html_data.'<select name="attributes[]" id="attributes" class="form-control chosen-select say_attributes" multiple="true">
                          ';
                    	foreach($attributes_value as $val_data){ 
                          if(!empty($existing_attr_values) && in_array($val_data['id'],$existing_attr_values)){ 
                            $html_data = $html_data.'<option value="'.$val_data['id'].'" selected>'.$val_data['attr_value_name'].'</option>';
                         }else{
                            $html_data = $html_data.'<option value="'.$val_data['id'].'">'.$val_data['attr_value_name'].'</option>';
                       
                            } }
                            $html_data = $html_data.'</select>';
                        }
                        
                	$html_data = $html_data.'</div>';
                    $html_data = $html_data.'</div>';  
			}
		}
		echo $html_data;
	}

	public function save_data(){
		$personality=$this->input->post('personality');
		$category=$this->input->post('category');
		$attributes=$this->input->post('attributes');

		 $this->product_personalisation_model->save_data($personality,$category,$attributes);

	}

}
