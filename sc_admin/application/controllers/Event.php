<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends MY_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
       parent::__construct();
        $this->load->model('category_model');    
		$this->load->model('lookcreator_model');
		$this->load->model('Eventcreator_model');
		$this->load->model('Collection_model');
		$this->load->model('Pa_model');
   } 
   
	public function index($offset = null){ 
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{ 
			if($this->has_rights(50) == 1){
				
				$data_post = array();
			
			    $data['parent_category_list'] = $this->lookcreator_model->parent_catgory_list(); 
				$data['category_list'] = $this->lookcreator_model->catgory_list();
				// $data['body_shape'] = $this->product_model->load_questAns(1);
				// $data['age'] = $this->product_model->load_questAns(8);
				// $data['style'] = $this->product_model->load_questAns(2);
				// $data['size'] = $this->product_model->load_questAns(9);
				// $data['budget'] = $this->product_model->load_questAns(7);
				// $data['colors_list'] = $this->lookcreator_model->get_tags_by_tag_type(1); 
				$data['brand_list'] = $this->lookcreator_model->get_brands_list(); 

				$data['street_data'] = $this->Eventcreator_model->get_event_pic('','','','');
				$this->load->view('common/header',$data);
				$this->load->view('event/create_event');
				$this->load->view('common/footer');
			}
		}
	}
	
	public function get_product(){
		
			$brand = $this->input->post('brand');
			$cat = $this->input->post('cat');
			$attributes_chosen = $this->input->post('attributes_chosen');
			$category_specific_attr = $this->input->post('category_specific_attr');
			$attributes_chosen_ = ''; $category_specific_attr_ = '';

			if(!empty($attributes_chosen)){ $i = 0;
				foreach($attributes_chosen as $val) {
					if($i!=0) { $attributes_chosen_ = $attributes_chosen_.','; }
					$attributes_chosen_ = $attributes_chosen_.$val;
					$i++;
				}
			}
			$lookoffset = $this->input->post('lookoffset');
			$data_post['product_list'] = $this->lookcreator_model->get_product_list_v2($brand,$cat,$attributes_chosen_,$category_specific_attr,$lookoffset); 
			$subcat_combo_html = '';
			if($lookoffset == 0){
				$subcat_combo_html = $subcat_combo_html.'<input type="hidden" id="lookoffset" name="lookoffset" value="1">';
			}
			if(!empty($data_post['product_list'])){ 
				foreach($data_post['product_list'] as $val){ 
					$stock_count =$this->lookcreator_model->product_stock($val['product_id']);
					if($stock_count>0){
					$subcat_combo_html = $subcat_combo_html.'<li><img data-img-link="'.$val['image'].'" class="lazy" id="'.$val['product_id'].'" src="'.$val['image'].'" title="Url : '.$val['url'].', Used_count:'.$val['product_used_count'].', Product Id : '.$val['product_id'].',stock count: '.$stock_count.', Price : '.$val['price'].'"   alt="'.$val['name'].'" width="80px" height="100px">
						 <a class="product-link" href="'.$val['url'].'" target="_blank"><span class="fa fa-link"></span></a>
					</li>';
					}
					
				}
				$subcat_combo_html = $subcat_combo_html.'<li class="btn-load-more" style="border: medium none; cursor: default; padding-top: 30px;"><div class="btn btn-sm btn-primary load_more" id="load_more"> LOAD MORE <i class="fa fa-arrow-down"></i></div></li>';
			}else{
					$subcat_combo_html = $subcat_combo_html.'<li class="cat">No record found </li>';
			}
			
			echo $subcat_combo_html; 
		}
		
		
	function save_event(){
		
		$rand_string = substr(str_shuffle(MD5(microtime())), 0, 10);
		$event_img_id = $this->input->post('event_img_id');
		$object_data['event_name'] = $this->input->post('event_name');
		$object_meta_data['event_short_desc'] = $this->input->post('event_short_desc');
		$object_meta_data['event_long_desc'] = $this->input->post('event_long_desc');
		$object_meta_data['event_venue'] = $this->input->post('event_venue');
		$object_meta_data['event_start_date'] = $this->input->post('event_start_date');
		$object_meta_data['event_end_date'] = $this->input->post('event_end_date');
		$object_meta_data['event_lat'] = $this->input->post('event_lat');
		$object_meta_data['event_long'] = $this->input->post('event_long');
		$object_meta_data['event_link'] = $this->input->post('event_link');
		$object_data['event_slug'] = $this->input->post('event_slug').$rand_string;
		/*pa attr*/
		$object_meta_data['pa_body_shape'] = '';
		$object_meta_data['pa_age'] = '';
		$object_meta_data['pa_budget'] = '';
		$object_meta_data['pa_pref1'] = '';
		$object_meta_data['pa_pref2'] = '';
		$object_meta_data['pa_pref3'] = '';
		$object_meta_data['body_shape'] = '';
		$object_meta_data['pref1'] = '';
		$object_meta_data['gender'] = '';
		/*pa attr*/
		$event_products = $this->input->post('event_products');
		// echo "<pre>";print_r($_POST);exit;
		$prod_look = array();
		if(!empty($event_products)){
				foreach($event_products as $val){
						$prod_look[]  = str_replace("recommended_","",$val);
				}
		}
		// echo "<pre>";print_r($prod_look);
		$event_prod_array = array_unique($prod_look);
		$object_meta_data['event_products'] = serialize($event_prod_array);
		
		if($event_img_id >0){ $object_data['event_img'] = $this->Eventcreator_model->get_event_image($event_img_id); }else { $object_data['event_img'] = '';}
		$object_id = $this->Eventcreator_model->save_object_data($object_data,$object_meta_data,$type='event');
		if($object_id >0){
			$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Event name = '.$object_data['event_name'].' </br>Event ID = '.$object_id.' </br>Event Added successfully!</div>');
			return true;
		}else {
			redirect('event/event_mgmt');
		}
		
	}
	
	public function event_mgmt(){
		
		if($this->has_rights(52) == 1 || 1 == 1){

			$search_by = 0;
			$table_search = 0;
			$search_by = $this->input->post('search_by')!='' ? $this->input->post('search_by') : $this->uri->segment(4);
			$table_search = $this->input->post('table_search')!='' ? $this->input->post('table_search') : $this->uri->segment(5);
			if($search_by!='0' && $table_search!='0' && $search_by!='' && $table_search!=''){ 
				$paginationUrl = 'event/event_mgmt/index/'.$search_by.'/'.$table_search;
				$uri_segment = 6;
			}else{
				$paginationUrl = 'event/event_mgmt/index/0/0';
				$uri_segment = 6;
			}

			$config['per_page'] = 20;
			$offset = $this->uri->segment(6)!='' ? $this->uri->segment(6) : 0;
			$config['total_rows'] = count($this->Eventcreator_model->get_all_values($search_by,$table_search,'','','event'));

			$total_rows = $config['total_rows'];
			$get_data = $this->Eventcreator_model->get_all_values($search_by,$table_search,$offset,$config['per_page'],'event');
			$count = $config['total_rows'];

			$data_post['event_list'] =  $get_data ;
			$data_post['search_by'] = $search_by;
			$data_post['table_search'] = $table_search;

			$config['base_url'] = base_url().$paginationUrl;        
			$config['total_rows'] = $count;
			$config['per_page'] = 20;
			$config['uri_segment'] = $uri_segment;
			$config['use_page_numbers'] = TRUE;
			$config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
			$config['full_tag_close'] = '</ul></div>';

			$config['first_link'] = '&laquo; First';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';

			$config['last_link'] = 'Last &raquo;';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';

			$config['next_link'] = 'Next &rarr;';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';

			$config['prev_link'] = '&larr; Previous';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';

			$config['cur_tag_open'] = '<li><a href="" style="background-color:#7CCDEF;">';
			$config['cur_tag_close'] = '</a></li>';

			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';


			$this->pagination->initialize($config); 
			$this->data['total_rows'] = $total_rows;

			$this->load->view('common/header');
			$this->load->view('event/manage_event',$data_post);
			$this->load->view('common/footer');

		}else{
			$this->load->view('common/header');
			$this->load->view('not_permission');
			$this->load->view('common/footer');
		}
		
	}
	
	function view_event(){
		$data = array();
		$object_id = $this->uri->segment(3);
		$data['event'] = $this->Eventcreator_model->get_event($object_id,$type='event');
		$this->load->view('common/header2');
		$this->load->view('event/view_event',$data);
		$this->load->view('common/footer2');
	}
	
	public function edit_event(){
		$event_img = '';
	 	if($this->has_rights(53) == 1){
			
			$object_id = $this->uri->segment(3);
			$data['event_data'] = $this->Eventcreator_model->get_event($object_id);
			if($this->input->post()){
				$config['upload_path'] = 'assets/event_img/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size']	= '2000';
				$config['max_width']  = '1500';
				$config['max_height']  = '1500';
				$config['file_name'] = time();
				$ok = 1;
				
				if(@$_FILES['event_img']['name']){
					$this->upload->initialize($config);
					if(!($this->upload->do_upload('event_img'))){
						$data['error'] = array($this->upload->display_errors()); 
						$this->load->view('common/header',$data);
						$this->load->view('event/edit_event');
						$this->load->view('common/footer');
						$ok = 0;
					}else{
						$event_img = $this->upload->data('file_name'); 
					}
				}
				
				$object_data['object_img'] = $event_img;
				$object_data['object_name'] = $this->input->post('event_name');
				
				$object_meta_data['event_short_desc'] = $this->input->post('event_short_desc');		
				$object_meta_data['event_long_desc'] = $this->input->post('event_long_desc');
				$object_meta_data['event_venue'] = $this->input->post('event_venue');
				$object_meta_data['event_start_date'] = $this->input->post('event_start_date');
				$object_meta_data['event_end_date'] = $this->input->post('event_end_date');
				$object_meta_data['event_lat'] = $this->input->post('event_lat');
				$object_meta_data['event_long'] = $this->input->post('event_long');
				$object_meta_data['event_link'] = $this->input->post('event_link');
				// echo "<pre>";print_r($_POST);exit;
				$data = $this->Eventcreator_model->update_object($object_data,$object_meta_data,$object_id);
				$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Event name = '.$object_data['object_name'].' </br>Event ID = '.$object_id.' </br>Event Edited successfully!</div>');
				redirect('event/event_mgmt');
			}
			
			$this->load->view('common/header',$data);
			$this->load->view('event/edit_event');
			$this->load->view('common/footer');
	
		}else{
			$this->load->view('common/header');
			$this->load->view('not_permission');
			$this->load->view('common/footer');
		}
	}
	
	public function delete_object(){
		if($this->has_rights(55) == 1){
		   $id = $this->input->post('id');   
		   if($this->Eventcreator_model->delete_object($id)){
				return true;
			}
			    
		}
	}
	
	public function broadcast_event(){
		$data = array();
		$object_id = $this->uri->segment(3);
		$data['event'] = $this->Eventcreator_model->get_event($object_id);
		if($this->input->post()){
			// echo "<pre>";print_r($_POST);exit;
			$object_data = array();
			/*pa attr*/
			if(!empty($this->input->post('pa_body_shape'))){
				$object_meta_data['pa_body_shape'] = serialize($this->input->post('pa_body_shape'));
				$object_meta_data['body_shape'] = $this->input->post('pa_body_shape');
			}
			if(!empty($this->input->post('pa_age')))$object_meta_data['pa_age'] = serialize($this->input->post('pa_age'));
			if(!empty($this->input->post('pa_budget')))$object_meta_data['pa_budget'] = serialize($this->input->post('pa_budget'));
			if(!empty($this->input->post('pa_pref1'))){
				$object_meta_data['pa_pref1'] = serialize($this->input->post('pa_pref1'));
				$object_meta_data['pref1'] = $this->input->post('pa_pref1');
			}
			if(!empty($this->input->post('pa_pref2')))$object_meta_data['pa_pref2'] = serialize($this->input->post('pa_pref2'));
			if(!empty($this->input->post('pa_pref3')))$object_meta_data['pa_pref3'] = serialize($this->input->post('pa_pref3'));
			if(!empty($this->input->post('gender')))$object_meta_data['gender'] = $this->input->post('gender');
			/*pa attr*/
			$data = $this->Eventcreator_model->update_object($object_data,$object_meta_data,$object_id);
			$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Event ID = '.$object_id.' </br>Pa Edited successfully!</div>');
			redirect('event/event_mgmt');
		}
		$this->load->view('common/header2');
		$this->load->view('event/broadcast_event',$data);
		$this->load->view('common/footer2');
	}
	
}