<?php
ini_set('max_execution_time', 0);
ini_set('memory_limit','1024M');
// require ('class-imagify.php');
require APPPATH.'libraries/class-imagify.php';
class Imagify_new extends MY_Controller {
	
	function __construct(){
        parent::__construct();
        $this->load->model('Imagify_model'); 
       
    }

    public function index(){
    	echo 'Imagify_new';exit;
    }    

	public function imagify_resize($image_name=null,$img_path=null){
		$imagify = new Imagify\Optimizer('f10158186956098dbc33c2800d10a4ca41165cea');
		echo "image name = ".$image_name.'</br>';
		// "width"=> 1025,"height"=> 1400
		$param = array(
			"level"=> 'normal',
			"resize"=> array("width"=> 1025,"height"=> 1400),
			);
		$data = (array)$imagify->optimize($image_name, $param);
		echo "<pre>";print_r($data);exit;
		if(!empty($data)){
			if($data['code'] == '422'){
				$url = $img_path;
				/* Extract the filename */
				$filename = substr($url, strrpos($url, '/') + 1);
				/* Save file wherever you want */
				file_put_contents('./assets/product_images2/zoom/'.$filename, file_get_contents($url));
			}else{
				$url = $data['image'];
				/* Extract the filename */
				$filename = substr($url, strrpos($url, '/') + 1);
				/* Save file wherever you want */
				unlink($image_name); //delete old file
				file_put_contents('./assets/product_images2/zoom/'.$filename, file_get_contents($url));
			}
			
		}
		// "width"=> 1025,"height"=> 1400
		
		
		// "width"=> 364,"height"=> 497
		$param = array(
			"level"=> 'normal',
			"resize"=> array("width"=> 364,"height"=> 497),
			);
		$data = (array)$imagify->optimize($image_name, $param);
		// echo "<pre>";print_r($data);
		if(!empty($data)){
			$url = $data['image'];
			/* Extract the filename */
			$filename = substr($url, strrpos($url, '/') + 1);
			/* Save file wherever you want */
			file_put_contents('./assets/product_images2/normal/'.$filename, file_get_contents($url));
		}
		// "width"=> 364,"height"=> 497
		
		
		// "width"=> 206,"height"=> 281
		$param = array(
			"level"=> 'normal',
			"resize"=> array("width"=> 206,"height"=> 281),
			);
		$data = (array)$imagify->optimize($image_name, $param);
		// echo "<pre>";print_r($data);
		if(!empty($data)){
			$url = $data['image'];
			/* Extract the filename */
			$filename = substr($url, strrpos($url, '/') + 1);
			/* Save file wherever you want */
			file_put_contents('./assets/product_images2/small/'.$filename, file_get_contents($url));
		}
		// "width"=> 206,"height"=> 281
		
		
		// "width"=> 154,"height"=> 210
		$param = array(
			"level"=> 'normal',
			"resize"=> array("width"=> 154,"height"=> 210),
			);
		$data = (array)$imagify->optimize($image_name, $param);
		// echo "<pre>";print_r($data);
		if(!empty($data)){
			$url = $data['image'];
			/* Extract the filename */
			$filename = substr($url, strrpos($url, '/') + 1);
			/* Save file wherever you want */
			file_put_contents('./assets/product_images2/thumbnail/'.$filename, file_get_contents($url));
		}
		// "width"=> 154,"height"=> 210
		
		
		// "width"=> 58,"height"=> 79
		$param = array(
			"level"=> 'normal',
			"resize"=> array("width"=> 58,"height"=> 79),
			);
		$data = (array)$imagify->optimize($image_name, $param);
		// echo "<pre>";print_r($data);
		if(!empty($data)){
			$url = $data['image'];
			/* Extract the filename */
			$filename = substr($url, strrpos($url, '/') + 1);
			/* Save file wherever you want */
			file_put_contents('./assets/product_images2/tiny/'.$filename, file_get_contents($url));
		}
		// "width"=> 58,"height"=> 79
		
		
		return true;
	}
	
	public function create_img_thumbnails(){
		$dir = './assets/product_images2/zoom';
		$i = 0;
		//here list of files of your directory with extension (jpg, jpeg, png) case insenstive
		if (is_dir($dir)) {
			$files = glob($dir . '/*.{jpg,JPG,jpeg,JPEG,png,PNG}', GLOB_BRACE);
			$currentDate = date("F d Y"); // get today's date
			// $currentDate = date("F d Y", time() - 60 * 60 * 24); //get yesterdays
			
			if (!empty($files) && is_array($files)) {
				foreach ($files as $f) {
					if (date("F d Y", filemtime($f)) == $currentDate) {
						
						$img_dir = base_url().ltrim($f,"./");
						$file_name = basename($img_dir);
						$get_img_url = $this->imagify_resize($f,$f);
						$i++;
					}
				}
			}
		}else{
			echo "dir not found";
		}
		echo '</br>'.$i.'</br>';
		if($i==0){ echo "no image found"; }
	}
	
	public function save_resize_img($width,$folder,$image_name){
		$imagify = new Imagify\Optimizer('465021353ebcec99059d97cd8adf11f4aa232bc7');
		$param = array(
			"level"=> 'normal',
			"resize"=> array("width"=> $width),
			);
		$data = (array)$imagify->optimize($image_name, $param);
		echo "<pre>";print_r($data);
		if(!empty($data)){
			if($data['code'] == '422'){
				
				$param = array("level"=> 'normal');
				$data = (array)$imagify->optimize($image_name, $param);
				if($data['code'] == '200'){
					$url = $data['image'];
				}else{
					$url = $image_name;
				}
				/* Extract the filename */
				$filename = basename($url);
				/* Save file wherever you want */
				file_put_contents('./assets/product_images2/'.$folder.'/'.$filename, file_get_contents($url));
			}else{
				$url = $data['image'];
				/* Extract the filename */
				$filename = substr($url, strrpos($url, '/') + 1);
				/* Save file wherever you want */
				// unlink($image_name); //delete old file
				file_put_contents('./assets/product_images2/'.$folder.'/'.$filename, file_get_contents($url));
			}
			
		}
		return true;
	}
	
	function get_product_images(){
		$i = 0;
		$brand_ids = '47370,40282,47530,47355';
		$product_data = $this->Imagify_model->get_product_data($brand_ids);
		echo "<pre>";print_r($product_data);exit;
		if(!empty($product_data)){
			
			foreach($product_data as $val){
				$start = microtime(true);
				if($val['img_type'] == 'product_img'){
					$image_name = './assets/products/'.$val['image'];
				}else{
					$image_name = './assets/products_extra/'.$val['image'];
				}
				$get_img_url = $this->save_resize_img(1025,'zoom',$image_name);
				$get_img_url = $this->save_resize_img(364,'normal',$image_name);
				$get_img_url = $this->save_resize_img(206,'small',$image_name);
				$get_img_url = $this->save_resize_img(154,'thumbnail',$image_name);
				$get_img_url = $this->save_resize_img(58,'tiny',$image_name);
				$i++;
				$end = microtime(true);echo '</br>'.($end - $start).'seconds'.'</br>';
			}
			
		}else{
			echo "no products found!";
		}
		echo "Done";
	}
	
	function test(){
		$image_name = 'https://www.stylecracker.com/sc_admin/assets/products/14588830569348.png';
		$img_path = './assets/products/';
		$ext = explode(".",$image_name);
		if(end($ext) == 'png'){
			$img_array = explode(".",$image_name);
			$image = imagecreatefrompng($image_name);
			$img = imagejpeg($image, $img_path.'/14588830569348.jpg', 100);
			echo $image_name."<br/>done";
		}
	}
	
	function get_brand_imagenames(){
		$brand_ids = '47370';
		$brand_id = $this->uri->segment(3);
		$product_data = $this->Imagify_model->get_product_data2($brand_id);
		// echo "<pre>";print_r($product_data);
		foreach($product_data as $val){
			echo $val['image'].'<br/>';
		}		
	}

	public function getimage_file(){
		$dir = './assets/product_images/zoom';
		$i = 0;
		//here list of files of your directory with extension (jpg, jpeg, png) case insenstive
		if (is_dir($dir)) {
			$files = glob($dir . '/*.{jpg,JPG,jpeg,JPEG,png,PNG}', GLOB_BRACE);
			//$currentDate = date("F d Y"); // get today's date
			// $currentDate = date("F d Y", time() - 60 * 60 * 24); //get yesterdays
			
			if (!empty($files) && is_array($files)) {
				echo $count = sizeof($files);exit;
				
				/*foreach ($files as $f) {
					
						echo $f;
						echo '<br/>';
						
						$i++;
					
				}*/
			}
		}else{
			echo "dir not found";
		}
		echo '</br>'.$i.'</br>';
		if($i==0){ echo "no image found"; }
	}
}	