<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Websiteusers extends MY_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->model('websiteusers_model'); 
		$this->load->model('users_model');
		$this->load->model('Scbox_model');
		$this->load->model('Pa_model');
	} 
   
	public function index($offset = null)
	{ 
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{ 
			$data = array();
			
			if($this->has_rights(15) == 1){
			  $search_by = 0;
				$table_search = 0;
				  $search_by = $this->input->post('search_by')!='' ? $this->input->post('search_by') : $this->uri->segment(3);
                $table_search = $this->input->post('table_search')!='' ? $this->input->post('table_search') : $this->uri->segment(4);
                
                if($search_by!='0' && $table_search!='0' && $search_by!='' && $table_search!=''){ 
                	$paginationUrl = 'websiteusers/index/'.$search_by.'/'.$table_search;
                	$uri_segment = 5;
            	}else{
            		$paginationUrl = 'websiteusers/index/0/0';
            		$uri_segment = 5;
            	}

				
				//$config['base_url'] = base_url().'websiteusers/index/';
				
				$config['per_page'] = 20;
				$offset = $this->uri->segment(5)!='' ? $this->uri->segment(5) : 0;
				$config['total_rows'] = count($this->websiteusers_model->get_allusers($search_by,$table_search,'',''));
				$total_rows = $config['total_rows'];
				$get_users = $this->websiteusers_model->get_allusers($search_by,$table_search,$offset,$config['per_page']);
				$data_post['users_data'] = $get_users;
				$data_post['search_by'] = $search_by;
                $data_post['table_search'] = $table_search;
				 $count = $config['total_rows'];


				$config['base_url'] = base_url().$paginationUrl;		
				$config['total_rows'] = $count;
				$config['per_page'] = 20;
				$config['uri_segment'] = $uri_segment;
				$config['use_page_numbers'] = TRUE;
				$config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
				$config['full_tag_close'] = '</ul></div>';

				$config['first_link'] = '&laquo; First';
				$config['first_tag_open'] = '<li>';
				$config['first_tag_close'] = '</li>';

				$config['last_link'] = 'Last &raquo;';
				$config['last_tag_open'] = '<li>';
				$config['last_tag_close'] = '</li>';

				$config['next_link'] = 'Next &rarr;';
				$config['next_tag_open'] = '<li>';
				$config['next_tag_close'] = '</li>';

				$config['prev_link'] = '&larr; Previous';
				$config['prev_tag_open'] = '<li>';
				$config['prev_tag_close'] = '</li>';

				$config['cur_tag_open'] = '<li><a href="" style="background-color:#7CCDEF;">';
				$config['cur_tag_close'] = '</a></li>';

				$config['num_tag_open'] = '<li>';
				$config['num_tag_close'] = '</li>';

				
				$this->pagination->initialize($config);	
				$data_post['total_rows'] = $total_rows;


				$this->load->view('common/header',$data_post);
				$this->load->view('websiteusers');
				$this->load->view('common/footer');
			}else{
				$this->load->view('common/header',$data_post);
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			}
		}
	}
	
		function user_view(){
			 $data = array();
		
			$data['roles'] = $this->users_model->get_roles(); 
			$user_id =  $this->uri->segment(3);
			$data['user_data'] = $this->users_model->get_user_data($user_id);
			//echo '<pre>';print_r($data['user_data']);
			$data['sc_box_mobile_number'] = $this->users_model->sc_box_mobile_number($user_id);
			$data['pa_data'] = $this->users_model->get_user_pa($user_id);
			//echo '<pre>';print_r($data['pa_data']);
			$data['wordrobe'] = $this->users_model->get_wordrobe_image($user_id); 
			$data['extra_info'] = $this->users_model->extra_info($user_id); 
			$data['visit_count_user'] = $this->users_model->visit_count($user_id); 
			$data['last_login'] =  $this->users_model->last_login($user_id); 
			$data['total_look_click_count'] =  $this->users_model->total_look_click_count($user_id); 
			$data['total_product_click_count'] =  $this->users_model->total_product_click_count($user_id); 
			$data['fav_look_count'] =  $this->users_model->fav_look_count($user_id); 
			$data['pa_data_extra'] = $this->users_model->get_user_pa_extras($user_id); 

			if($data['user_data'][0]['gender']==1)
			{
				$brand_list = unserialize(PA_BRAND_LIST_WOMEN);
			}else
			{
				$brand_list = unserialize(PA_BRAND_LIST_MEN);
			}
			
			//echo '<pre>';print_r($brand_list);
			$brands = '';
			if(isset($data['pa_data']['0']['brand_ids'])){ 
				$brand_ids = explode(',',$data['pa_data']['0']['brand_ids']);				
				foreach($brand_ids as $value){
					// print_r($brand_ids);exit;					
					$brands = $brands.'<br/> '.$brand_list[$value]->brand_name;					
				}
			}
			$data['brand_list'] = $brands;
			// echo "<pre>";print_r($data['pa_data']);exit;
			// echo "<pre>";print_r(unserialize(PA_BRAND_LIST));exit; 
			if($this->has_rights(15) == 1){
				
				$this->load->view('common/header',$data);
				$this->load->view('websiteusers_view');
				$this->load->view('common/footer');
				
			}else{
				
				$this->load->view('common/header',$data);
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			 }
			
			}

			function user_look($type,$user_id){
			 $data = array();
		
			 $data['roles'] = $this->users_model->get_roles(); 
			 $type =  $this->uri->segment(3);	
			 $user_id =  $this->uri->segment(4);	
			 

			 if($type == 'look')
			 {
			 	 $data['user_click'] =  $this->users_model->user_look_click_id($user_id);
			 	 $data['total_rows'] = count($this->users_model->user_look_click_id($user_id));
			 	 $data['type'] = 'Look';
			 }
			
			if($type == 'product')
			{
			 	$data['user_click'] =  $this->users_model->user_product_click_id($user_id);
			 	$data['total_rows'] = count($this->users_model->user_product_click_id($user_id));
			 	$data['type'] = 'Product';
			}

			if($type == 'fav')
			{
				$data['user_click'] =  $this->users_model->fav_look_detail($user_id);
				$data['total_rows'] = count($this->users_model->fav_look_detail($user_id));
			 	$data['type'] = 'Favourite';
		
			}
			 
			/*if($this->has_rights(15) == 1){*/
				
				$this->load->view('common/header',$data);
				$this->load->view('user_click');
				$this->load->view('common/footer');
				
			/*}else{
				
				$this->load->view('common/header',$data);
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			 }*/
			
			}

	public function scbox($offset = null)
	{ 
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{ 
			$data = array();
			
			if($this->has_rights(15) == 1){
			  $search_by = 0;
				$table_search = 0;
				$date_from = $this->input->post('date_from');
				$date_to = $this->input->post('date_to');
				
				$search_by = $this->input->post('search_by')!='' ? $this->input->post('search_by') : $this->uri->segment(4);
				if($date_from != '' || $date_to != ''){
					$date = $date_from.','.$date_to;
					$table_search = base64_encode($date); 
				}else{
					$table_search = $this->input->post('table_search')!='' ? $this->input->post('table_search') : $this->uri->segment(5);
				}
                if($search_by!='0' && $table_search!='0' && $search_by!='' && $table_search!=''){ 
                	$paginationUrl = 'websiteusers/scbox/index/'.$search_by.'/'.$table_search;
                	$uri_segment = 6;
            	}else{
            		$paginationUrl = 'websiteusers/scbox/index/0/0';
            		$uri_segment = 6;
            	}

				
				//$config['base_url'] = base_url().'websiteusers/index/';
				
				$config['per_page'] = 20;
				$offset = $this->uri->segment(6)!='' ? $this->uri->segment(6) : 0;
				$config['total_rows'] = count($this->websiteusers_model->get_scboxusers($search_by,$table_search,'',''));
				$total_rows = $config['total_rows'];
				$get_users = $this->websiteusers_model->get_scboxusers($search_by,$table_search,$offset,$config['per_page']);
				$data_post['users_data'] = $get_users;
				$data_post['search_by'] = $search_by;
                $data_post['table_search'] = $table_search;
				$count = $config['total_rows'];


				$config['base_url'] = base_url().$paginationUrl;		
				$config['total_rows'] = $count;
				$config['per_page'] = 20;
				$config['uri_segment'] = $uri_segment;
				$config['use_page_numbers'] = TRUE;
				$config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
				$config['full_tag_close'] = '</ul></div>';

				$config['first_link'] = '&laquo; First';
				$config['first_tag_open'] = '<li>';
				$config['first_tag_close'] = '</li>';

				$config['last_link'] = 'Last &raquo;';
				$config['last_tag_open'] = '<li>';
				$config['last_tag_close'] = '</li>';

				$config['next_link'] = 'Next &rarr;';
				$config['next_tag_open'] = '<li>';
				$config['next_tag_close'] = '</li>';

				$config['prev_link'] = '&larr; Previous';
				$config['prev_tag_open'] = '<li>';
				$config['prev_tag_close'] = '</li>';

				$config['cur_tag_open'] = '<li><a href="" style="background-color:#7CCDEF;">';
				$config['cur_tag_close'] = '</a></li>';

				$config['num_tag_open'] = '<li>';
				$config['num_tag_close'] = '</li>';

				
				$this->pagination->initialize($config);	
				$data_post['total_rows'] = $total_rows;


				$this->load->view('common/header',$data_post);
				$this->load->view('scbox_users');
				$this->load->view('common/footer');
			}else{
				$this->load->view('common/header',$data_post);
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			}
		}
	}

	function scboxuser_view(){
		 $data = array();
	
		$data['roles'] = $this->users_model->get_roles(); 
		$user_id =  $this->uri->segment(3);
		$data['user_data'] = $this->users_model->get_user_data($user_id);
		//$data['sc_box_mobile_number'] = $this->users_model->sc_box_mobile_number($user_id);
		$data['pa_data'] = $this->users_model->get_user_pa($user_id); 
		$data['wordrobe'] = $this->users_model->get_wordrobe_image($user_id); 
		$data['extra_info'] = $this->users_model->extra_info($user_id); 
		$data['visit_count_user'] = $this->users_model->visit_count($user_id); 
		$data['last_login'] =  $this->users_model->last_login($user_id); 
		$data['total_look_click_count'] =  $this->users_model->total_look_click_count($user_id); 
		$data['total_product_click_count'] =  $this->users_model->total_product_click_count($user_id); 
		$data['fav_look_count'] =  $this->users_model->fav_look_count($user_id);  
		$data['pa_data_extra'] = $this->users_model->get_user_pa_extras($user_id);

		$data['scboxuser'] = $this->websiteusers_model->get_scbox_userdata($user_id);
		$brand_list = unserialize(PA_BRAND_LIST);
		$brands = '';
		if(isset($data['pa_data']['0']['brand_ids'])){ 
			$brand_ids = explode(',',$data['pa_data']['0']['brand_ids']);
			foreach($brand_list as $value){
				// print_r($brand_ids);exit;
				if(in_array($value->brand_id,$brand_ids)){
					$brands = $brands.' '.$value->brand_name;
				}
			}
		}
		$data['brand_list'] = $brands;
		$data['scboxuser']['user_sizetop_name'] = @$this->websiteusers_model->get_size_data($data['scboxuser']['user_sizetop'])[0]['answer'];
		$data['scboxuser']['user_sizebottom_name'] = @$this->websiteusers_model->get_size_data($data['scboxuser']['user_sizebottom'])[0]['answer'];
		$data['scboxuser']['user_sizefoot_name'] = @$this->websiteusers_model->get_size_data($data['scboxuser']['user_sizefoot'])[0]['answer'];
		$data['scboxuser']['user_sizeband_name'] = @$this->websiteusers_model->get_size_data($data['scboxuser']['user_sizeband'])[0]['answer'];
		$data['scboxuser']['user_sizecup_name'] = @$this->websiteusers_model->get_size_data($data['scboxuser']['user_sizecup'])[0]['answer'];

		$data['scboxuser']['user_colors_name'] = '';
		$data['scboxuser']['user_prints_name'] = '';
		$data['scboxuser']['user_state_name'] = @$this->websiteusers_model->get_state($data['scboxuser']['user_state']);

		$scboxskintone = unserialize(SCBOX_SKINTONE_LIST);
		$scboxcolors = unserialize(SCBOX_COLOR_LIST);
		$scboxprints = unserialize(SCBOX_PRINT_LIST);
		 //echo '<pre>';print_r($scboxskintone);exit;
		 $usercolors_arr = explode(',',@$data['scboxuser']['user_colors']);
		 $userprints_arr = explode(',',@$data['scboxuser']['user_prints']);

		 foreach($scboxskintone as $val)
		 {
			 // print_r($val);exit;
		 	if($val->id==@$data['scboxuser']['user_skintone'])
		 	{
		 		$data['scboxuser']['user_skintone_name'] = $val->name;
		 	}
		 }

		 foreach($scboxcolors as $val)
		 {
		 	if(in_array($val->id,$usercolors_arr))
		 	{
		 		$data['scboxuser']['user_colors_name'] = $data['scboxuser']['user_colors_name'].','.$val->name;
		 	}
		 }

		 foreach($scboxprints as $val)
		 {
		 	if(in_array($val->id,$userprints_arr))
		 	{
		 		$data['scboxuser']['user_prints_name'] = $data['scboxuser']['user_prints_name'].','.$val->name;
		 	}
		 }		
		// echo "<pre>";print_r($data['pa_data']);exit;
		// echo "<pre>";print_r(unserialize(PA_BRAND_LIST));exit; 
		if($this->has_rights(15) == 1){
			
			$this->load->view('common/header',$data);
			$this->load->view('scbox_users_view');
			$this->load->view('common/footer');
			
		}else{
			
			$this->load->view('common/header',$data);
			$this->load->view('not_permission');
			$this->load->view('common/footer');
		 }
			
	}
	
	public function download_scbox_user_data($search_by=NULL,$table_search=NULL,$is_download=NULL){
		$table_search = base64_decode($table_search);
		$get_users = $this->websiteusers_model->get_scboxusers($search_by,$table_search);
		//echo $this->db->last_query();
		/* echo "<pre>";
		print_r($get_users);
		echo "</pre>"; */
		//$fieldName = array('User ID','First name','Last name','Email','Gender','Contact no','Birth Date','Order count','Age Range','Remarks','Registration date','Last visit time');
		$fieldName = array('User ID','First name','Last name','Email','Gender','Contact no','Birth Date','Order count','Registration date','Last visit time');
		$result_data = array();$i =0;
		foreach($get_users as $val) {
			$result_data[$i][] = $val->id;
			$result_data[$i][] = $val->first_name;
			$result_data[$i][] = $val->last_name;
			$result_data[$i][] = $val->email;
			if($val->gender == '1'){
				$gender = 'women';
			}else{
				$gender = 'men';
			}
			$result_data[$i][] = $gender;
			$result_data[$i][] = $val->contact_no;
			$result_data[$i][] = $val->birth_date;
			$result_data[$i][] = $val->order_count;
			//$result_data[$i][] = $val->user_age;
			//$result_data[$i][] = $val->remarks;
			$created_datetime = new DateTime($val->created_datetime);
			$result_data[$i][] = $created_datetime->format('d-m-Y H:i:s');
			$modified_datetime = new DateTime($val->modified_datetime);
			$result_data[$i][] = $modified_datetime->format('d-m-Y H:i:s');
			$i++;
		}
		
		$this->writeDataintoCSV($fieldName,$result_data,'sc_box_user_data');
	}
	
	public function get_box_remark_popup(){
		$user_id = $this->input->post('user_id');
		$objectid = $this->input->post('objectid');
		$data = $this->websiteusers_model->get_scbox_userdata($user_id);
		// echo "<pre>";print_r($data);exit;
		$remark_html = '';
		$remark_html = $remark_html.'<label for="stylist_remark">Stylist Remarks</label><br/><textarea tabindex="1" rows="10" cols="70" name="stylist_remark_'.$user_id.'" id="stylist_remark_'.$user_id.'" >'.@$data['stylist_remark'].'</textarea>
						<br/><button class="btn btn-sm btn-primary" onclick="save_stylist_remark(\''.$user_id.'\',\''.$objectid.'\');"><i class="fa fa-save"></i> Save</button>';
		echo $remark_html;
	}
	
	public function save_stylist_remark(){
		$user_id = $this->input->post('user_id');
		$objectid = $this->input->post('objectid');
		$remark = $this->input->post('remark');
		$question = 'stylist_remark';
		$data = $this->Scbox_model->save_scboxuser_answer($remark,$user_id,$objectid,$question);
		// echo $this->db->last_query();
	}
	
	function download_stylist_extracted_data($search_by=NULL,$table_search=NULL,$is_download=NULL){
		$table_search = base64_decode($table_search);
		// echo $search_by.'  '.$table_search;exit;
		$get_users = $this->websiteusers_model->get_scboxusers($search_by,$table_search,'','','',1);
		$SCBOX_SKINTONE_LIST = unserialize(SCBOX_SKINTONE_LIST);
		$SCBOX_COLOR_LIST = unserialize(SCBOX_COLOR_LIST);
		$SCBOX_PRINT_LIST = unserialize(SCBOX_PRINT_LIST);
		
		if(!empty($get_users)){
			// men data
			$men_body_shape = $this->Pa_model->load_questAns(24);
			$men_style = $this->Pa_model->load_questAns(25);
			$SCBOX_MENTOP_LIST = $this->Pa_model->get_size_data('mentopsize');
			$SCBOX_MENBOTTOM_LIST = $this->Pa_model->get_size_data('menbottomsize');
			$SCBOX_MENFOOT_LIST = $this->Pa_model->get_size_data('menfootsize');
			
			//women data 
			$women_body_shape = $this->Pa_model->load_questAns(1);
			$women_style = $this->Pa_model->load_questAns(2);
			$SCBOX_WOMENTOP_LIST = $this->Pa_model->get_size_data('womentopsize');
			$SCBOX_WOMENBOTTOM_LIST = $this->Pa_model->get_size_data('womenbottomsize');
			$SCBOX_WOMENFOOT_LIST = $this->Pa_model->get_size_data('womenfootsize');
			$SCBOX_BANDSIZE_LIST = $this->Pa_model->get_size_data('womenbandsize');
			$SCBOX_CUPSIZE_LIST = $this->Pa_model->get_size_data('womencupsize');
			// echo "<pre>";print_r($get_users);
			$fieldName = array('User ID','User Name','Gender','SC BOX Amount','Body Shape','Personality','Size (Shirt/T-shirt Size, WAIST SIZE (INCH), Footwear, Lingerie, Cup size)','Color','Prints','Remarks (Any other specifications)','Notes');
			$result_data = array();$i =0;
			foreach($get_users as $val) {
				$style_pref = '';$size_data = '';$color_prints = '';$user_print = '';$user_color = '';
				$user_colors_array = explode(',',@$val['user_colors']['object_meta_value']);
				$user_prints_array = explode(',',@$val['user_prints']['object_meta_value']);
				$gender = $val['user_gender']['object_meta_value'];
				$result_data[$i][] = @$val['user_id']['object_meta_value'];
				$result_data[$i][] = @$val['user_name']['object_meta_value'];
				$result_data[$i][] = @$val['user_gender']['object_meta_value'];
				$result_data[$i][] = @$val['scbox_price']['object_meta_value'];
				if($gender == '1'){
					
					foreach($women_body_shape as $key=>$val2){
						if(@$val['user_body_shape']['object_meta_value'] == $key){ 
							$result_data[$i][] = @$val2;
						}
					}
					
					foreach($women_style as $key=>$val2){
						if(@$val['user_style_p1']['object_meta_value'] == $key || @$val['user_style_p2']['object_meta_value'] == $key || @$val['user_style_p3']['object_meta_value'] == $key){ 
							$style_pref = $style_pref.'  '.@$val2;	
						}	
					}
					$result_data[$i][] = $style_pref;
					
					foreach($SCBOX_WOMENTOP_LIST as $val2){
						if(@$val['user_sizetop']['object_meta_value'] == $val2['id']){
							$size_data = $size_data .' Top = '.@$val2['answer'].' , ';
						}	
					}
					
					foreach($SCBOX_WOMENBOTTOM_LIST as $val2){
						if(@$val['user_sizebottom']['object_meta_value'] == $val2['id']){
							$size_data = $size_data .' BOTTOM = '.@$val2['answer'].' , ';
						}	
					}
					
					foreach($SCBOX_WOMENFOOT_LIST as $val2){
						if(@$val['user_sizefoot']['object_meta_value'] == $val2['id']){
							$size_data = $size_data .' FOOT = '.@$val2['answer'].' , ';
						}	
					}
					
					foreach($SCBOX_BANDSIZE_LIST as $val2){
						if(@$val['user_sizeband']['object_meta_value'] == $val2['id']){
							$size_data = $size_data .' BANDSIZE = '.@$val2['answer'].' , ';
						}	
					}
					
					foreach($SCBOX_CUPSIZE_LIST as $val2){
						if(@$val['user_sizecup']['object_meta_value'] == $val2['id']){
							$size_data = $size_data .' CUPSIZE = '.@$val2['answer'];
						}	
					}
					$result_data[$i][] = $size_data;
					
				}else{
					
					foreach($men_body_shape as $key=>$val2){
						if(@$val['user_body_shape']['object_meta_value'] == $key){ 
							$result_data[$i][] = @$val2;
						}
					}
					
					foreach($men_style as $key=>$val2){
						if(@$val['user_style_p1']['object_meta_value'] == $key || @$val['user_style_p2']['object_meta_value'] == $key || @$val['user_style_p3']['object_meta_value'] == $key){ 
							$style_pref = $style_pref.'  '.@$val2;	
						}
					}
					$result_data[$i][] = $style_pref;
					
					foreach($SCBOX_MENTOP_LIST as $val2){
						if(@$val['user_sizetop']['object_meta_value'] == $val2['id']){
							$size_data = $size_data .' Top = '.@$val2['answer'].' , ';
						}	
					}
					foreach($SCBOX_MENBOTTOM_LIST as $val2){
						if(@$val['user_sizebottom']['object_meta_value'] == $val2['id']){
							$size_data = $size_data .' BOTTOM = '.@$val2['answer'].' , ';
						}	
					}
					foreach($SCBOX_MENFOOT_LIST as $val2){
						if(@$val['user_sizefoot']['object_meta_value'] == $val2['id']){
							$size_data = $size_data .' FOOT = '.@$val2['answer'];
						}	
					}
					$result_data[$i][] = $size_data;
					
				}
				
				foreach($SCBOX_COLOR_LIST as $val2){
					if(in_array($val2->id,$user_colors_array)){
						$user_color = $user_color.$val2->name.' , ';
					}	
				}
				$result_data[$i][] = $user_color;
				
				foreach($SCBOX_PRINT_LIST as $val2){
					if(in_array($val2->id,$user_prints_array)){
						$user_print = $user_print.$val2->name.' , ';
					}	
				}
				$result_data[$i][] = $user_print;
				
				if(@$val['user_manual_data']['object_meta_value'] != ''){
					$user_manual_data = unserialize($val['user_manual_data']['object_meta_value']);
					$result_data[$i][] = $user_manual_data['other_specifications'];
				}
				
				$result_data[$i][] = '';
				$i++;
			}
			// echo "<pre>";print_r($result_data);exit;
			$this->writeDataintoCSV($fieldName,$result_data,'sc_box_user_data');
		}
	}
			
	
	
	function padata(){
		 $data = array();
		 $jsonStuffWearing = array();$paSelectionAfter = array();$jsonStuffWearing_data= array();
			$paSelectionAfter_data= array();
		$data['roles'] 		= $this->users_model->get_roles(); 
		$oid 				= $this->input->post('oid');
		$user_id 			= $this->input->post('user_id');
		$data['user_data'] 	= $this->users_model->get_user_data($user_id);	
		//$data['sc_box_mobile_number'] = $this->users_model->sc_box_mobile_number($user_id);
		//$data['pa_data'] 	= $this->users_model->get_user_pa($user_id); 
		$data['pa_data'] 	= ''; 
		$data['wordrobe'] 	= $this->users_model->get_wordrobe_image($user_id); 
		$data['extra_info'] = $this->users_model->extra_info($user_id); 
	
		$data['visit_count_user'] 	= $this->users_model->visit_count($user_id); 
		$data['last_login'] 		=  $this->users_model->last_login($user_id); 
		
		$data['total_look_click_count'] 	=  '';
		$data['total_product_click_count'] 	=  $this->users_model->total_product_click_count($user_id);  
		$data['fav_look_count'] 			=  '';  
		$data['pa_data_extra'] 				= $this->users_model->get_user_pa_extras($user_id);

		$data['scboxuser'] = $this->websiteusers_model->get_scbox_userdata($user_id);

		$scbox_gift_object = $this->Scbox_model->get_object('scbox_gift')[0]['object_id'];
      	$gift_data = $this->Scbox_model->get_gift_data($scbox_gift_object,$oid);
      	if(!empty($gift_data))
      	{
      		$data['scboxuser']['gift'] = 'yes';
			$data['scboxuser']['gifted_name'] = $gift_data['name'];
			$data['scboxuser']['gifted_email'] = $gift_data['email'];
			$data['scboxuser']['gifted_dob'] = $gift_data['dob'];
			$data['scboxuser']['gifted_gender'] = ($gift_data['gender']==1) ? 'Female' : 'Men';
			$data['scboxuser']['gifted_mobile'] = $gift_data['mobileno'];
			$data['scboxuser']['gifted_pincode'] = $gift_data['pincode'];
			$data['scboxuser']['gifted_city'] = $gift_data['city'];
			$data['scboxuser']['gifted_address'] = $gift_data['address'];
			$data['scboxuser']['gifted_profession'] = $gift_data['profession'];
			$data['scboxuser']['gifted_state'] = (@$this->users_model->get_state_byid($gift_data['state'])[0]['state_name']!='') ? $this->users_model->get_state_byid($gift_data['state'])[0]['state_name'] : $gift_data['state'] ;			
			$data['scboxuser']['stylist_call_receiver'] = $gift_data['stylist_call_receiver'];
		}else
		{
			$data['scboxuser']['gift'] = 'no';
		}
		
		$brand_list = unserialize(PA_BRAND_LIST);
		$brands = '';
		if(isset($data['pa_data']['0']['brand_ids'])){ 
			$brand_ids = explode(',',$data['pa_data']['0']['brand_ids']);
			foreach($brand_list as $value){
				// print_r($brand_ids);exit;
				if(in_array($value->brand_id,$brand_ids)){
					$brands = $brands.' '.$value->brand_name;
				}
			}
		}
		$data['brand_list'] = $brands;
		$data['scboxuser']['user_sizetop_name'] 	= @$this->websiteusers_model->get_size_data($data['scboxuser']['user_sizetop'])[0]['answer'];
		$data['scboxuser']['user_sizebottom_name'] 	= @$this->websiteusers_model->get_size_data($data['scboxuser']['user_sizebottom'])[0]['answer'];
		$data['scboxuser']['user_sizefoot_name'] 	= @$this->websiteusers_model->get_size_data($data['scboxuser']['user_sizefoot'])[0]['answer'];
		$data['scboxuser']['user_sizeband_name'] 	= @$this->websiteusers_model->get_size_data($data['scboxuser']['user_sizeband'])[0]['answer'];
		$data['scboxuser']['user_sizecup_name'] 	= @$this->websiteusers_model->get_size_data($data['scboxuser']['user_sizecup'])[0]['answer'];

		$data['scboxuser']['user_colors_name'] 	= '';
		$data['scboxuser']['user_prints_name'] 	= '';
		$data['scboxuser']['user_state_name'] 	= @$this->websiteusers_model->get_state($data['scboxuser']['user_state']);

		$scboxskintone 	= unserialize(SCBOX_SKINTONE_LIST);
		$scboxcolors 	= unserialize(SCBOX_COLOR_LIST);
		$scboxprints 	= unserialize(SCBOX_PRINT_LIST);
		
		 //echo '<pre>';print_r($scboxskintone);exit;
		 $usercolors_arr = explode(',',@$data['scboxuser']['user_colors']);
		 $userprints_arr = explode(',',@$data['scboxuser']['user_prints']);

		 foreach($scboxskintone as $val)
		 {
			 // print_r($val);exit;
		 	if($val->id==@$data['scboxuser']['user_skintone'])
		 	{
		 		$data['scboxuser']['user_skintone_name'] = $val->name;
		 	}
		 }

		 foreach($scboxcolors as $val)
		 {
		 	if(in_array($val->id,$usercolors_arr))
		 	{
		 		$data['scboxuser']['user_colors_name'] = $data['scboxuser']['user_colors_name'].','.$val->name;
		 	}
		 }

		 foreach($scboxprints as $val)
		 {
		 	if(in_array($val->id,$userprints_arr))
		 	{
		 		$data['scboxuser']['user_prints_name'] = $data['scboxuser']['user_prints_name'].','.$val->name;
		 	}
		 }	



		/*$jsonStuffWearing			= $this->users_model->paSelectionBefore($user_id);
		$data['jsonStuffWearing'] 	= json_decode($jsonStuffWearing['object_meta_value'], true);
		
		$paSelectionAfter			= $this->users_model->paSelectionAfter($user_id);
		$data['paSelectionAfter'] 	= json_decode($paSelectionAfter['object_meta_value'], true);*/
		//echo 'user id = '.$user_id;
			$slugAfter 		= $this->websiteusers_model->paSelectionAfterSlug();
			//echo $this->db->last_query().'---------';
			$slugBefore 	= $this->websiteusers_model->paSelectionBeforeSlug();
			//echo $this->db->last_query().'---------';
			$getAfterJson 	= $this->websiteusers_model->getAfterJson($oid, $slugBefore['object_id']); 
			//echo $this->db->last_query().'---------';
			$getBeforeJson 	= $this->websiteusers_model->getBeforeJson($oid, $slugAfter['object_id']);
			//echo $this->db->last_query();
			//$data['jsonStuffWearing'] = json_decode($getAfterJson['object_meta_value'], true);
			$jsonStuffWearing 		= json_decode($getAfterJson['object_meta_value'], true);

			$categories = array('objApparel','objFootwear','objAccessories','objJewellery','objBeauty','objBag');
			$i=0;$vals = [];
			if(!empty($jsonStuffWearing))
			{
				foreach($jsonStuffWearing as $key => $value)				
				{	
					$vals = array_merge($vals, array_keys($value));					
				}
			}

			if(!empty($jsonStuffWearing))
			{
				foreach($jsonStuffWearing as $key => $value)
				{					
					switch (@$vals[$i]) {
					    case 'objApparel':					       
					     	$jsonStuffWearing_data[0]=$value;
					        break;
					    case 'objFootwear':					       
					    	$jsonStuffWearing_data[1]=$value;
					        break;
					   case 'objAccessories':					        
					   		$jsonStuffWearing_data[2]=$value;
					        break;
					   case 'objJewellery':					       
					   		$jsonStuffWearing_data[3]=$value;
					        break;
					   case 'objBeauty':					        
					   		$jsonStuffWearing_data[4]=$value;
					        break;
					   case 'objBag':					        
					   		$jsonStuffWearing_data[5]=$value;
					        break;
					}
					$i++;
				}
			}
			
			$data['jsonStuffWearing'] = $jsonStuffWearing_data;			
			$paSelectionAfter= json_decode($getBeforeJson['object_meta_value'], true); 

			$j=0;
			if(!empty($paSelectionAfter))
			{
				foreach($paSelectionAfter as $key => $value)
				{	
				//echo '<pre>';print_r($value);	
					switch (@$vals[$j]) {
					    case 'objApparel':					        
					    	$paSelectionAfter_data[0]=$value;
					        break;
					    case 'objFootwear':					        
					    	$paSelectionAfter_data[1]=$value;
					        break;
					   case 'objAccessories':					       
					   		$paSelectionAfter_data[2]=$value;
					        break;
					   case 'objJewellery':					        
					   		$paSelectionAfter_data[3]=$value;
					        break;
					   case 'objBeauty':					        
					   		$paSelectionAfter_data[4]=$value;
					        break;
					   case 'objBag':					        
					   		$paSelectionAfter_data[5]=$value;
					        break;
					}
					$j++;
				}
			}
			$data['paSelectionAfter'] = $paSelectionAfter_data;
			$data['scboxuser']['user_id']	= $user_id;
			
			$categoryObject = $this->websiteusers_model->paObjectIdBySlug('category_selection');			
			$data['categorySelection'] =  $this->websiteusers_model->getObjectmetaByKeyObject($oid, $categoryObject['object_id'])['object_meta_value'];
			//echo '<pre>';print_r($data['categorySelection']);
			//  echo "<pre>";
			// print_r($data['jsonStuffWearing']);
			// print_r($data['paSelectionAfter']);
			// echo "</pre>"; 
			
		/*  echo "<pre>";print_r($data['pa_data']);exit;
		 echo "<pre>";print_r(unserialize(PA_BRAND_LIST));exit;  */
		/* if($this->has_rights(15) == 1){ */
			
			//$this->load->view('common/header',$data);
			echo $this->load->view('scbox_padata', $data, true);
			//$this->load->view('common/footer');
			
		/* }else{
			
			$this->load->view('common/header',$data);
			$this->load->view('not_permission');
			$this->load->view('common/footer');
		 } */
			
	}
	
	public function wesite_users_pa_data()
	{
		$oid 						= $this->input->post('oid');
		$user_id 					= $this->input->post('user_id');
		$slugAfter 					= $this->websiteusers_model->paSelectionAfterSlug();
		$slugBefore 				= $this->websiteusers_model->paSelectionBeforeSlug();
		$getAfterJson 				= $this->websiteusers_model->getAfterJson($oid, $slugBefore['object_id']); 
		$getBeforeJson 				= $this->websiteusers_model->getBeforeJson($oid, $slugAfter['object_id']);
		$data['jsonStuffWearing'] 	= json_decode($getAfterJson['object_meta_value'], true);
		$data['paSelectionAfter'] 	= json_decode($getBeforeJson['object_meta_value'], true); 
		$data['user_id']			= $user_id;
		/*echo "<pre>";
		print_r($data['jsonStuffWearing']);
		print_r($data['paSelectionAfter']);
		echo "</pre>"; */
	}
	

	
}
