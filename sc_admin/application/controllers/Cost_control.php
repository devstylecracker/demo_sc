<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cost_control extends MY_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
    {
        // Call the Model constructor
        
        parent::__construct();
		$this->load->model('cost_model');
		$this->load->model('Scbox_model');
		$this->load->library('excel');
        
    }
    
	public function index()
	{
		
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{
			$data = array();			
			for($i=1;$i<=4;$i++)
			{
				$form['form_'.$i] = $this->cost_model->get_scx_option_meta($i);
			}
			if(is_array($form['form_1']) && count($form['form_1'])>0)
			{
				foreach($form['form_1'] as $key => $value)
				{
					$form_2999[$value['option_key']] = $value['option_value'];
					
				} 
				$data['packageCostCod_2999'] 	= ($form_2999['max_box_cost']+$form_2999['shipping_cost']+$form_2999['acquisition_cost']+$form_2999['package_cost']+$form_2999['transaction_cost_cod']);
				$percentageValue 				= (('2999' * $form_2999['transaction_cost_online']) / 100);
				$data['packageCostOnline_2999'] = (($form_2999['max_box_cost']+$form_2999['shipping_cost']+$form_2999['acquisition_cost']+$form_2999['package_cost']) + $percentageValue);
				
				$data['calculationOnline_2999']	=	(2999 - $data['packageCostOnline_2999']);
				$data['calculationCod_2999']	= 	(2999 - $data['packageCostCod_2999']);
			}
			if(is_array($form['form_2']) && count($form['form_2'])>0)
			{
				foreach($form['form_2'] as $key => $value)
				{
					$form_4999[$value['option_key']] = $value['option_value'];
				} 
				$data['packageCostCod_4999'] 	= ($form_4999['max_box_cost']+$form_4999['shipping_cost']+$form_4999['acquisition_cost']+$form_4999['package_cost']+$form_4999['transaction_cost_cod']);
				$percentageValue 				= (('4999' * $form_4999['transaction_cost_online']) / 100);
				$data['packageCostOnline_4999'] = (($form_4999['max_box_cost']+$form_4999['shipping_cost']+$form_4999['acquisition_cost']+$form_4999['package_cost']) + $percentageValue);
				
				$data['calculationOnline_4999']	=	(4999 - $data['packageCostOnline_4999']);
				$data['calculationCod_4999']	= 	(4999 - $data['packageCostCod_4999']);
			}
			if(is_array($form['form_3']) && count($form['form_3'])>0)
			{
				foreach($form['form_3'] as $key => $value)
				{
					$form_6999[$value['option_key']] = $value['option_value'];
				}
				$data['packageCostCod_6999'] 	= ($form_6999['max_box_cost']+$form_6999['shipping_cost']+$form_6999['acquisition_cost']+$form_6999['package_cost']+$form_6999['transaction_cost_cod']);
				$percentageValue 				= (('6999' * $form_6999['transaction_cost_online']) / 100);
				$data['packageCostOnline_6999'] = (($form_6999['max_box_cost']+$form_6999['shipping_cost']+$form_6999['acquisition_cost']+$form_6999['package_cost']) + $percentageValue);
				
				$data['calculationOnline_6999']	=	(6999 - $data['packageCostOnline_6999']);
				$data['calculationCod_6999']	= 	(6999 - $data['packageCostCod_6999']);
			}	
			if(is_array($form['form_4']) && count($form['form_4'])>0)
			{			
				foreach($form['form_4'] as $key => $value)
				{
					$form_custom[$value['option_key']] = $value['option_value'];
				} 
				$data['packageCostCod_custom'] 		= ($form_custom['max_box_cost']+$form_custom['shipping_cost']+$form_custom['acquisition_cost']+$form_custom['package_cost']+$form_custom['transaction_cost_cod']);
				$percentageValue 					= (('8000' * $form_custom['transaction_cost_online']) / 100);
				$data['packageCostOnline_custom']  	= (($form_custom['max_box_cost']+$form_custom['shipping_cost']+$form_custom['acquisition_cost']+$form_custom['package_cost']) + $percentageValue);
				
				$data['calculationOnline_custom']	=	(8000 - $data['packageCostOnline_custom']);
				$data['calculationCod_custom']		= 	(8000 - $data['packageCostCod_custom']);
			}
			
			$data['form_2999']		= @$form_2999;
			$data['form_4999']		= @$form_4999;
			$data['form_6999']		= @$form_6999;
			$data['form_custom']	= @$form_custom;
			
			$this->load->view('common/header2');
			$this->load->view('cost/cost_control', $data);
			$this->load->view('common/footer2');
		}
	}

	public function sales_report()
	{ 
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}
		else
		{ 
			$search_by 		= 0;
			$table_search 	= 0;
			$search_by2 	= 0;
			
			 $search_by 	= $this->input->post('search_by')!='' ? $this->input->post('search_by') : $this->uri->segment(3);
		     $table_search 	= $this->input->post('table_search')!='' ? $this->input->post('table_search') : $this->uri->segment(4);
			 $search_by2 = $this->input->post('search_by2')!='' ? $this->input->post('search_by2') : $this->uri->segment(5);
			
			if(($search_by!='0' && $table_search!='0' && $search_by!='' && $table_search!='') || ($search_by2!='0' && $search_by2!='') ){
				$paginationUrl = 'cost_control/sales_report/'.$search_by.'/'.$table_search.'/'.$search_by2;
				$uri_segment = 6;
			}else{
				$paginationUrl = 'cost_control/sales_report/0/0/0';
				$uri_segment = 6;
			}
			$object_id  = $this->Scbox_model->get_object('box_order_products')[0]['object_id'];
				$config['per_page'] = 20;
		        $offset = $this->uri->segment(6)!='' ? $this->uri->segment(6) : 0;


		        $config['total_rows'] = count($this->cost_model->get_sales_report($search_by,$table_search,'','',$search_by2,$object_id));

		        $get_data_new = array();
		      
		        //$order_data = $this->cost_model->get_sales_report();
		       
		        $total_rows = $config['total_rows'];
		        $get_data = $this->cost_model->get_sales_report($search_by,$table_search,$offset,$config['per_page'],$search_by2,$object_id);
		        //echo $this->db->last_query();
		        $count = $config['total_rows'];
		        $data['orders'] 			= $get_data;
		        $data['search_by'] 			= $search_by;
		        $data['table_search'] 		= $table_search;
				$data['search_by2'] 		= $search_by2;
		        $data['sr_offset'] 			= $offset;       
				$config['base_url'] 		= base_url().$paginationUrl;
				$config['total_rows'] 		= $count;
				$config['uri_segment'] 		= $uri_segment;
				$config['use_page_numbers'] = TRUE;
				$config['full_tag_open'] 	= '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
				$config['full_tag_close'] 	= '</ul></div>';
				$config['first_link'] 		= '&laquo; First';
				$config['first_tag_open'] 	= '<li>';
				$config['first_tag_close'] 	= '</li>';
				$config['last_link'] 		= 'Last &raquo;';
				$config['last_tag_open'] 	= '<li>';
				$config['last_tag_close'] 	= '</li>';
				$config['next_link'] 		= 'Next &rarr;';
				$config['next_tag_open'] 	= '<li>';
				$config['next_tag_close'] 	= '</li>';
				$config['prev_link'] 		= '&larr; Previous';
				$config['prev_tag_open'] 	= '<li>';
				$config['prev_tag_close'] 	= '</li>';
				$config['cur_tag_open'] 	= '<li class="active" ><a href="" >';
				$config['cur_tag_close'] 	= '</a></li>';
				$config['num_tag_open'] 	= '<li>';
				$config['num_tag_close'] 	= '</li>';

			$this->pagination->initialize($config);
			$data['total_rows'] 			= $total_rows;
			$data['box_order_products'] 	= $object_id; 

			$data['arrSalaesReport']		= $get_data;
			$data['hello']		= $get_data;
			//echo '<pre>';print_r($data);exit;
			$this->load->view('common/header2');
			$this->load->view('cost/sales_report',$data);			 
			$this->load->view('common/footer2');
		}
	}
	
	
		 public function exportExcel($param,$search_by=NULL,$table_search=NULL,$is_download=NULL,$search_by2=NULL)
	{
		
		$is_download 			= $this->uri->segment(6);
		if($search_by == 2 )
		{
		  $table_search_encode = base64_decode($table_search);
		}
		else{
		$table_search_encode 	= $table_search;
		}
		$object_id 				= $this->Scbox_model->get_object('box_order_products')[0]['object_id'];
		$get_data 				= $this->cost_model->get_sales_report($search_by,$table_search_encode,'','',$search_by2,$object_id);
		$scxOption				= $this->cost_model->get_scx_options();
		$filename   = 'sales_report_export_'.date('YmdHis');
		$header 	= array('Date','Order Number','Name','Gender','Apparel(MRP)','Bag(MRP)','Shoes(MRP)','Jewellery(MRP)','Accessories(MRP)','Make Up/Grooming(MRP)','Discount(%)(MRP)','Tax(%)(MRP)','Order Total(MRP)','Apparel(CTC)','Bag(CTC)','Shoes(CTC)','Jewellery(CTC)','Accessories(CTC)','Make Up/Grooming(CTC)','Marketing Cost of Acqusition','Packaging','Shipping','Payment Gateway Transaction Fees','Apparel(FE)','Bag(FE)','Shoes(FE)','Jewellery(FE)','Accessories(FE)','Make Up/Grooming(FE)','Packaging(FE)','Shipping(FE)','Apparel(SE)','Bag(SE)','Shoes(SE)','Jewellery(SE)','Accessories(SE)','Make Up/Grooming(SE)','Packaging(SE)','Shipping(SE)','Reverse Pick up','Reverse Packaging','Refund Value','Total Cost','Profit');
		
		 // output headers so that the file is downloaded rather than displayed
		 header('Content-Type: text/csv; charset=utf-8');
		 header('Content-Disposition: attachment; filename='.$filename.'.csv');
		 
		 // create a file pointer connected to the output stream
		 $lead_export_file = fopen('php://output', 'w');
		 
		 // output the column headings
		 
		 fputcsv($lead_export_file, $header);
		 $acqusition_cost = '';
		 if(is_array($get_data) && count($get_data)>0)
		 {
		   foreach($get_data as $listData)
		   {
				$productPrice 		= $this->cost_model->get_product_meta($object_id,$listData['order_display_no']);
				$discount_percent	= $this->cost_model->get_coupon_setting($listData['coupon_code']);
				if(!empty($productPrice))
				{
					$price 				= array_column($productPrice, 'price');
					$compare_price 		= array_column($productPrice, 'compare_price');
					$totalPrice			= array_sum($price);
					$totalComparePrice	= array_sum($compare_price);
					//$totalProfit		= ($totalPrice - $totalComparePrice);
					$totalProfit		= ($listData['product_price'] - $totalComparePrice);
					$catId 				= array_column($productPrice, 'id');
					$catgoryId			= implode(',',$catId);
					$getCategory		= $this->cost_model->get_categories_by_id($catgoryId);
					$profit				= ($totalProfit)?$totalProfit:'0.00';
					if(!empty($getCategory))
					{
					$apparel_mrp 		= ($getCategory['category_name'] == 'Apparel')?$getCategory['price']:'0';
					$bags_mrp 			= ($getCategory['category_name'] == 'Bags')?$getCategory['price']:'0';
					$footwear_mrp 		= ($getCategory['category_name'] == 'Footwear')?$getCategory['price']:'0';
					$jewellery_mrp 		= ($getCategory['category_name'] == 'Jewellery')?$getCategory['price']:'0';
					$accessories_mrp	= ($getCategory['category_name'] == 'Accessories')?$getCategory['price']:'0';
					$grooming_mrp 		= ($getCategory['category_name'] == 'Grooming')?$getCategory['price']:'0';
					
					$apparel_ctc 		= ($getCategory['category_name'] == 'Apparel')?$getCategory['compare_price']:'0';
					$bags_ctc 			= ($getCategory['category_name'] == 'Bags')?$getCategory['compare_price']:'0';
					$footwear_ctc 		= ($getCategory['category_name'] == 'Footwear')?$getCategory['compare_price']:'0';
					$jewellery_ctc 		= ($getCategory['category_name'] == 'Jewellery')?$getCategory['compare_price']:'0';
					$accessories_ctc	= ($getCategory['category_name'] == 'Accessories')?$getCategory['compare_price']:'0';
					$grooming_ctc 		= ($getCategory['category_name'] == 'Grooming')?$getCategory['compare_price']:'0';
					}					
				}
				
				if($listData['product_price'] == '2999')
				{
					$acqusition_cost 	= ($scxOption[1]['acquisition_cost'])?$scxOption[1]['acquisition_cost']:'no';
					$package_cost 		= ($scxOption[1]['package_cost'])?$scxOption[1]['package_cost']:'no';
					$shipping_cost 		= ($scxOption[1]['shipping_cost'])?$scxOption[1]['shipping_cost']:'no';
					$transaction_cost_online 		= ($scxOption[1]['transaction_cost_online'])?$scxOption[1]['transaction_cost_online']:'no';
				}
				if($listData['product_price'] == '4999')
				{
					$acqusition_cost 	= ($scxOption[2]['acquisition_cost'])?$scxOption[2]['acquisition_cost']:'no';
					$package_cost 		= ($scxOption[2]['package_cost'])?$scxOption[2]['package_cost']:'no';
					$shipping_cost 		= ($scxOption[2]['shipping_cost'])?$scxOption[2]['shipping_cost']:'no';
					$transaction_cost_online 		= ($scxOption[2]['transaction_cost_online'])?$scxOption[2]['transaction_cost_online']:'no';
				}
				if($listData['product_price'] == '6999')
				{
					$acqusition_cost 	= ($scxOption[3]['acquisition_cost'])?$scxOption[3]['acquisition_cost']:'no';
					$package_cost 		= ($scxOption[3]['package_cost'])?$scxOption[3]['package_cost']:'no';
					$shipping_cost 		= ($scxOption[3]['shipping_cost'])?$scxOption[3]['shipping_cost']:'no';
					$transaction_cost_online 		= ($scxOption[3]['transaction_cost_online'])?$scxOption[3]['transaction_cost_online']:'no';
				}
				if($listData['product_price'] != '2999' && $listData['product_price'] != '4999' && $listData['product_price'] != '6999' )
				{
					$acqusition_cost 	= ($scxOption[4]['acquisition_cost'])?$scxOption[4]['acquisition_cost']:'no';
					$package_cost 		= ($scxOption[4]['package_cost'])?$scxOption[4]['package_cost']:'no';
					$shipping_cost 		= ($scxOption[4]['shipping_cost'])?$scxOption[4]['shipping_cost']:'no';
					$transaction_cost_online 		= ($scxOption[4]['transaction_cost_online'])?$scxOption[4]['transaction_cost_online']:'no';
				}
				if(isset($scxOption[5]['scbox_tax']))
				{
				$tax = ($scxOption[5]['scbox_tax'])?$scxOption[5]['scbox_tax']:'no';
				}
				else{
					$tax = 'no';
		        }
				   $resultData = array(
						$listData['created_datetime'],
						$listData['order_display_no'], 
						$listData['first_name'].' '.$listData['last_name'],
						($listData['gender'] == 1)?'Female':'Male',
						$apparel_mrp,	 						
						$bags_mrp,	 						
						$footwear_mrp,	 						
						$jewellery_mrp,	 						
						$accessories_mrp,	 						
						$grooming_mrp,	
						($discount_percent['coupon_amount'])?$discount_percent['coupon_amount']:'0',//Discount(%)(MRP)
						$tax,//Tax(%)(MRP)
						$listData['order_total'],//Order Total(MRP)
						$apparel_ctc,	 						
						$bags_ctc,	 						
						$footwear_ctc,	 						
						$jewellery_ctc,	 						
						$accessories_ctc,	 						
						$grooming_ctc,
						$acqusition_cost,//Marketing Cost of Acqusition
						$package_cost,//Packaging
						$shipping_cost,//Shipping
						$transaction_cost_online,//Payment Gateway Transaction Fees
						'0',//Apparel(FE)
						'0',//Bag(FE)
						'0',//Shoes(FE)
						'0',//Jewellery(FE)
						'0',//Accessories(FE)
						'0',//Make Up/Grooming(FE)
						'0',//Packaging(FE)
						'0',//Shipping(FE)
						'0',//Apparel(SE)
						'0',//Bag(SE)
						'0',//Shoes(SE)
						'0',//Jewellery(SE)
						'0',//Accessories(SE)
						'0',//Make Up/Grooming(SE)
						'0',//Packaging(SE)
						'0',//Shipping(SE)
						'0',//Reverse Pick up
						'0',//Reverse Packaging
						'0',//Refund Value
						$totalComparePrice,//Total Cost
						$profit,//Profit
					);
				//print_r($resultData);
		   fputcsv($lead_export_file, $resultData);
		   }
		 }
		 exit;

	}
	
  
	
}
