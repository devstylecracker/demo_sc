<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends MY_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
       parent::__construct();
       $this->load->model('users_model');    
       $this->load->model('brandusers_model');  
       $this->load->model('store_model');    
      
   } 
   
	public function index($offset = null)
	{ 
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{ 
			
		
			$this->load->view('common/header');
			$this->load->view('brand/brandusers_view');
			$this->load->view('common/footer');
		}
	}
	
	function is_online(){
			if (!empty($this->input->post('is_online'))){return TRUE;}
				else{
					$error = 'Please select Store Type';
					$this->form_validation->set_message('is_online', $error);
					return FALSE;
				}
		}
		function target_market(){
			if ($this->input->post('target_market')){return TRUE;}
				else{
					$error = 'Please select Target Market';
					$this->form_validation->set_message('target_market[]', $error);
					return FALSE;
				}
		}
		function is_male(){ 
			if (!empty($this->input->post('is_male'))){ return TRUE; }
				else{
					$error = 'Please select Demographic';
					$this->form_validation->set_message('is_male', $error);
					return FALSE;
				}
		}
	
	
	public function store_add(){
	
		$data['target_market'] = $this->brandusers_model->target_market(); 
		$data['all_brands'] = $this->store_model->all_brands(); 
		$data['brand_id'] = $this->uri->segment(3);
		if($this->has_rights(23) == 1){ 
			if($this->input->post()){
				$this->form_validation->set_rules('brand_store_name', 'Store Name', 'required|prep_for_form');
				$this->form_validation->set_rules('address', 'Address', 'required|prep_for_form');
				$this->form_validation->set_rules('country', 'Country', 'required|prep_for_form');
				$this->form_validation->set_rules('state', 'State', 'required|prep_for_form');
				$this->form_validation->set_rules('city', 'City', 'required|prep_for_form');
				$this->form_validation->set_rules('contact_person', 'Contact Person', 'required');
				$this->form_validation->set_rules('mobile_no', 'Mobile Number', 'required|numeric|exact_length[10]');
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
				//$this->form_validation->set_rules('is_online', 'Online', 'callback_is_online|prep_for_form');
				$this->form_validation->set_rules('age_group_from', 'Age group from', 'required|is_natural|prep_for_form|max_length[2]');
				$this->form_validation->set_rules('age_group_to', 'Age group to', 'required|is_natural|prep_for_form|max_length[2]');
				$this->form_validation->set_rules('price_range_from', 'Price range from', 'required|is_natural|prep_for_form');
				$this->form_validation->set_rules('price_range_to', 'Price range to', 'required|is_natural|prep_for_form');
				$this->form_validation->set_rules('pincode', 'Pincode', 'required|is_natural|exact_length[6]');
				$this->form_validation->set_rules('product_in_stock_count', 'Product in Stock', 'required|is_natural');
				
				$this->form_validation->set_rules('rating', 'Rating', 'is_natural|min_length[0]|max_length[5]');
				
					$config['upload_path'] = 'assets/store_logo/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					//$config['max_size']	= '1000';
					//$config['max_width']  = '500';
					//$config['max_height']  = '500';
					$config['file_name'] = time().mt_rand(0,1000);
						
				
				if ($this->form_validation->run() == FALSE)
					{
						$this->load->view('common/header');
						$this->load->view('brand/add_store',$data);
						$this->load->view('common/footer');
					}else{
					
					$ok =1;
					$logo= '';	
						if($_FILES['logo']['name']){
							$this->upload->initialize($config);
						if(!($this->upload->do_upload('logo'))){
								$data['error'] = array($this->upload->display_errors()); 
								$this->load->view('common/header',$data);
								$this->load->view('brand/add_store');
								$this->load->view('common/footer');
								$ok =0;
							 }else{
								 $logo= $this->upload->data('file_name'); 
								 }
						}	
						if($ok == 1){
						
						$brand_store_name= $this->input->post('brand_store_name');
						$url= $this->input->post('url');
						$address= $this->input->post('address');
						$contact= $this->input->post('contact');
						$established_at= $this->input->post('established_at');
						$rating= $this->input->post('rating');
						$product_in_stock_count= $this->input->post('product_in_stock_count');
						$price_range_from= $this->input->post('price_range_from');
						$price_range_to= $this->input->post('price_range_to');
						$country= $this->input->post('country');
						$state= $this->input->post('state');
						$city= $this->input->post('city');
						$contact_person= $this->input->post('contact_person');
						$mobile_no= $this->input->post('mobile_no');
						$email= $this->input->post('email');
						$pincode= $this->input->post('pincode');
						$age_group_from= $this->input->post('age_group_from');
						$age_group_to= $this->input->post('age_group_to');
						$is_online= $this->input->post('is_online');
						$is_offline= $this->input->post('is_offline');
						$is_male= $this->input->post('is_male');
						$is_female= $this->input->post('is_female');
						$is_children= $this->input->post('is_children');
						$brand_id= $this->input->post('brand_id');
						$target_market= $this->input->post('target_market');
						$brand= $this->input->post('brand');
					
					$data['store_info'] = array(
							'brand_store_name'=>$brand_store_name,
							'url'=>$url,
							'address'=>$address,
							'country'=>$country,
							'pincode'=>$pincode,
							'contact'=>$contact,
							'established_at'=>$established_at,
							'product_in_stock_count'=>$product_in_stock_count,
							'price_range_from'=>$price_range_from,
							'price_range_to'=>$price_range_to,
							'age_group_from'=>$age_group_from,
							'age_group_to'=>$age_group_to,
							'country'=>$country,
							'state'=>$state,
							'city'=>$city,
							'contact_person'=>$contact_person,
							'mobile_no'=>$mobile_no,
							'email_id'=> $email,
							'rating'=>$rating,
							'is_online'=>$is_online,
							'is_offline'=>$is_offline,
							'is_male'=>$is_male,
							'is_female'=>$is_female,
							'is_children'=>$is_children,
							'brand_id'=>$brand_id,
							'pincode' => $pincode,
							'created_by'=>$this->session->userdata('user_id'),
							'modified_by'=>$this->session->userdata('user_id'),
							'created_datetime'=>date('Y-m-d H:i:s'),
							'logo' => $logo
							
						);
						
						$data['target_market'] = array('target_market' => $target_market);
						$data['brand'] = array('target_market' => $brand);
							$this->store_model->add_new_store($data); 
							redirect('/brandusers/manage_store/'.$brand_id);
					}
				}
			}else{
			
				$this->load->view('common/header',$data);
				$this->load->view('brand/add_store');
				$this->load->view('common/footer');
			}
		}else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
				
		}
	
		}
		
		
		function store_edit(){
			if($this->has_rights(37) == 1){ 
				$data['all_brands'] = $this->store_model->all_brands(); 
				$brand_id = $this->uri->segment(3);
				$store_id = $this->uri->segment(4);
				$data['brand_id'] = $brand_id;
				$data['store_data'] = $this->store_model->get_store_info($store_id); 
				$data['target_market'] = $this->brandusers_model->target_market(); 
				$data['brand_target_market'] = $this->store_model->store_target_market_assoc($store_id); 
				$data['all_storewise_brands'] = $this->store_model->all_storewise_brands($store_id); 
				if($this->input->post()){
					$this->form_validation->set_rules('brand_store_name', 'Store Name', 'required|prep_for_form');
					$this->form_validation->set_rules('address', 'Address', 'required|prep_for_form');
					$this->form_validation->set_rules('country', 'Country', 'required|prep_for_form');
					$this->form_validation->set_rules('state', 'State', 'required|prep_for_form');
					$this->form_validation->set_rules('city', 'City', 'required|prep_for_form');
					$this->form_validation->set_rules('contact_person', 'Contact Person', 'required');
					$this->form_validation->set_rules('mobile_no', 'Mobile Number', 'required|numeric|exact_length[10]');
					$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
					//$this->form_validation->set_rules('is_online', 'Online', 'callback_is_online|prep_for_form');
					$this->form_validation->set_rules('age_group_from', 'Age group from', 'required|is_natural|prep_for_form|max_length[2]');
					$this->form_validation->set_rules('age_group_to', 'Age group to', 'required|is_natural|prep_for_form|max_length[2]');
					$this->form_validation->set_rules('price_range_from', 'Price range from', 'required|is_natural|prep_for_form');
					$this->form_validation->set_rules('price_range_to', 'Price range to', 'required|is_natural|prep_for_form');
					$this->form_validation->set_rules('pincode', 'Pincode', 'required|is_natural|exact_length[6]');
					$this->form_validation->set_rules('product_in_stock_count', 'Product in Stock', 'required|is_natural');
					$this->form_validation->set_rules('rating', 'Rating', 'is_natural|min_length[0]|max_length[5]');
						if ($this->form_validation->run() == FALSE){
							$this->load->view('common/header',$data);
							$this->load->view('brand/edit_store');
							$this->load->view('common/footer');
						}else{
							
							$config['upload_path'] = 'assets/store_logo/';
							$config['allowed_types'] = 'gif|jpg|png|jpeg';
							//$config['max_size']	= '1000';
							//$config['max_width']  = '500';
							//$config['max_height']  = '500'; 
							$config['file_name'] = $data['store_data'][0]['logo'] == '' ? time() : $data['store_data'][0]['logo'];
							
							$ok =1;$logo= '';	
							if($_FILES['logo']['name']){
								$this->upload->initialize($config);
							if(!($this->upload->do_upload('logo'))){
									$data['error'] = array($this->upload->display_errors()); 
									$this->load->view('common/header',$data);
									$this->load->view('brand/edit_store');
									$this->load->view('common/footer');
									$ok =0;
								
								 }else{
									 $logo= $this->upload->data('file_name'); 
									 }
							}
							if($ok == 1){
							$brand_store_name= $this->input->post('brand_store_name');
							$url= $this->input->post('url');
							$address= $this->input->post('address');
							$contact= $this->input->post('contact');
							$established_at= $this->input->post('established_at');
							$product_in_stock_count= $this->input->post('product_in_stock_count');
							$price_range_from= $this->input->post('price_range_from');
							$price_range_to= $this->input->post('price_range_to');
							$country= $this->input->post('country');
							$state= $this->input->post('state');
							$city= $this->input->post('city');
							$contact_person= $this->input->post('contact_person');
							$mobile_no= $this->input->post('mobile_no');
							$email= $this->input->post('email');
							$rating= $this->input->post('rating');
							$pincode= $this->input->post('pincode');
							$age_group_from= $this->input->post('age_group_from');
							$age_group_to= $this->input->post('age_group_to');
							$is_online= $this->input->post('is_online');
							$is_offline= $this->input->post('is_offline');
							$is_male= $this->input->post('is_male');
							$is_female= $this->input->post('is_female');
							$is_children= $this->input->post('is_children');
							$brand_id= $this->input->post('brand_id');
							$target_market= $this->input->post('target_market');
							$brand_id= $this->input->post('brand_id');
							$store_id= $this->input->post('store_id');
							$brand= $this->input->post('brand');
							
							$data['target_market'] = array('target_market' => $target_market);
							$data['brand'] = array('target_market' => $brand);
							
							$data['store_edit_info'] = array(
							'brand_store_name'=>$brand_store_name,
							'url'=>$url,
							'address'=>$address,
							'country'=>$country,
							'pincode'=>$pincode,
							'contact'=>$contact,
							'established_at'=>$established_at,
							'product_in_stock_count'=>$product_in_stock_count,
							'price_range_from'=>$price_range_from,
							'price_range_to'=>$price_range_to,
							'age_group_from'=>$age_group_from,
							'age_group_to'=>$age_group_to,
							'country'=>$country,
							'state'=>$state,
							'rating'=>$rating, 
							'city'=>$city,
							'contact_person'=>$contact_person,
							'mobile_no'=>$mobile_no,
							'email_id'=> $email,
							'is_online'=>$is_online,
							'is_offline'=>$is_offline,
							'is_male'=>$is_male,
							'is_female'=>$is_female,
							'is_children'=>$is_children,
							'pincode' => $pincode,
							'modified_datetime'=>date('Y-m-d H:i:s'),
							'modified_by'=>$this->session->userdata('user_id'),
							'logo'=>$logo
							
							);
							
							if($this->store_model->edit_new_store($data,$store_id)){
								redirect('/brandusers/manage_store/'.$brand_id);
							}
						}
							 
						}
						
						
					
				}else{
			
					$this->load->view('common/header',$data);
					$this->load->view('brand/edit_store');
					$this->load->view('common/footer');
				}
			}else{
					$this->load->view('common/header');
					$this->load->view('not_permission');
					$this->load->view('common/footer');
			}
		}
		
		function store_view(){
				if($this->has_rights(22) == 1){ 
					$brand_id = $this->uri->segment(3);
					$store_id = $this->uri->segment(4);
					$data['store_info'] = $this->store_model->get_store_info($store_id);
					$data['target_market'] = $this->store_model->store_target_market_assoc($store_id);
					
					$this->load->view('common/header',$data);
					$this->load->view('brand/view_store');
					$this->load->view('common/footer');
				}else{
					$this->load->view('common/header');
					$this->load->view('not_permission');
					$this->load->view('common/footer');
					
				}
			
			}
			
	  function store_delete(){
			$data = array();
		   
			if($this->has_rights(38) == 1){
				$store_id = $this->input->post('id');
				$this->store_model->delete_store($store_id,$this->session->userdata('user_id'));
			}
			
		}
	
	
}
