<?php
ini_set('memory_limit', '-1');
/*
*
  This controller is basically for the dashboard report  - User Related All the Dashboard functions 
  are return in this controller
*/

defined('BASEPATH') OR exit('No direct script access allowed');

class Dailyreports extends MY_Controller {
  
  function __construct(){
        parent::__construct(); 
        $this->load->model('Dailyreports_model');
    }    

/*current_report*/
  public function current_report(){   
    $date_from = '';  $date_to = '';

       if($this->input->post()){
          $date_from = $this->input->post('date_from');          
          $date_to = $this->input->post('date_to');          
       } 
    $data['d_report'] = $this->Dailyreports_model->daily_report($date_from,$date_to); 
    $this->load->view('common/header');
    $this->load->view('reports/current_report',$data);
    $this->load->view('common/footer');
  }
}

?>