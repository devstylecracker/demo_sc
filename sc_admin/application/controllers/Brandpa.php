<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brandpa extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Brandpa_model');
        $this->load->model('Categories_model');

    }

    function index(){
    	
    	if($this->has_rights(1) == 1 && $this->session->userdata('user_id')){
            $data = array();

            $data['cat_data'] = $this->Categories_model->get_cat_data();
            $data['brands_data'] = $this->Brandpa_model->get_all_paid_brands();    	
            $data['bucket'] = $this->Categories_model->get_all_buckets();

            $age_id = '8,17';
            $data['age'] = $this->Categories_model->get_question_answers($age_id);

            $budget_id = '7,18';
            $data['budget'] = $this->Categories_model->get_question_answers($budget_id);

            $this->load->view('common/header');
            $this->load->view('brandpa',$data);
            $this->load->view('common/footer');

        }else{

            $this->load->view('common/header');
            $this->load->view('not_permission');
            $this->load->view('common/footer');
        }
        
    }

    function save_brands_data(){
        $brand_info = $this->input->post('brand_info');
        if($brand_info){
            $this->Brandpa_model->save_brands_data($_POST,$brand_info);
        }
    }

    public function get_brands_data(){
        $id = $this->input->post('id');
        $data = array();
        $data['category_data'] = $this->Brandpa_model->get_brands_category($id,'category');
        $data['bucket_data'] = $this->Categories_model->get_pa_data($id,'brand','bucket');
        $data['age_data'] = $this->Categories_model->get_pa_data($id,'brand','age');
        $data['budget_data'] = $this->Categories_model->get_pa_data($id,'brand','budget');
        echo json_encode($data);
    }

    
}
?>