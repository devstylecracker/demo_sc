<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Size extends MY_Controller {

	function __construct(){
       parent::__construct();
       $this->load->model('Size_model');
   	}

   	function index(){ 
   		$data = array();
   		$data['sizes'] = $this->Size_model->get_product_sizes();

   		$this->load->view('common/header');
        $this->load->view('size',$data);
        $this->load->view('common/footer');
   	}

   	function update_sort_order(){ 
   		$k = array();
   		$k = explode(",",$_POST['data']);
		$this->Size_model->update_sort_order($k);   		
   	}

}