<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CouponSettings extends MY_Controller {
	
	function __construct(){
       parent::__construct();
       $this->load->library('excel');
       $this->load->model('Couponsettings_model');    
       $this->load->model('brandusers_model');  
       $this->load->model('store_model');    
      
   } 
   
	public function index($offset = null)
	{ 
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');

		}else{ 		
		
            $search_by = $this->input->post('search_by');
            $table_search = $this->input->post('table_search');
            $date_from = $this->input->post('date_from');
            $date_to = $this->input->post('date_to');
            $paginationUrl = 'CouponSettings/index/';
            $uri_segment = 3;
           	$config['per_page'] = 20;
            //$config['total_rows'] = count($this->couponSettings_model->get_coupon($search_by,$table_search,'','',''));         
            $config['total_rows'] = count($this->Couponsettings_model->get_coupon($search_by,$table_search,'','','',$date_from,$date_to));         
            $total_rows = $config['total_rows'];
            $get_coupons = $this->Couponsettings_model->get_coupon($search_by,$table_search,$offset,$config['per_page'],'',$date_from,$date_to);             
            /* echo'<pre>';print_r($get_coupons);*/
            $count = $config['total_rows'];		
            $data['coupons'] = $get_coupons;
            $data['discount_type'] = array('1'=>'Cart discount','2'=>'Cart % discount','3'=>'Product discount','4'=>'Product % discount');		
            $config['base_url'] = base_url().$paginationUrl;		
			$config['total_rows'] = $count;
			
			$config['uri_segment'] = $uri_segment;
			$config['use_page_numbers'] = TRUE;
			$config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
			$config['full_tag_close'] = '</ul></div>';

			$config['first_link'] = '&laquo; First';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';

			$config['last_link'] = 'Last &raquo;';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';

			$config['next_link'] = 'Next &rarr;';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';

			$config['prev_link'] = '&larr; Previous';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';

			$config['cur_tag_open'] = '<li><a href="" style="background-color:#7CCDEF;">';
			$config['cur_tag_close'] = '</a></li>';

			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';

			$this->pagination->initialize($config);	
			$this->data['total_rows'] = $total_rows;
		/*	$this->customPagination($paginationUrl,$count,$uri_segment,$get_coupons,$total_rows);*/
			$this->load->view('common/header');
			$this->load->view('couponsetting_view',$data);
			$this->load->view('common/footer');
		}
	}
	
	
	
	public function coupon_addedit($coupon_id=NULL)
	{	
		$brand= $this->input->post('brand');
		$data['paid_stores'] = $this->Couponsettings_model->get_paid_stores($brand); 
		$data['discount_type'] = array('1'=>'Cart discount','2'=>'Cart % discount','3'=>'Product discount','4'=>'Product % discount');
		$data['coupon_type'] = array('0'=>'All','1'=>'SC BOX Coupon','2'=>'Special Coupon','3'=>'Products Coupon');			
		$data['coupons'] = $this->Couponsettings_model->get_coupon();		
		
		if($this->has_rights(64) == 1)
		{ 
			/*CouponCode Add Section */	
			if((!empty($this->input->post())) && $coupon_id==NULL)
			{				
				$this->form_validation->set_rules('coupon_code', 'Coupon Code', 'required');
				$this->form_validation->set_rules('coupon_desc', 'Coupon Description', 'required');
				$this->form_validation->set_rules('date_from', 'Date From', 'required');
				$this->form_validation->set_rules('date_to', 'Date To', 'required');
				$this->form_validation->set_rules('min_spend', 'Minimum Spend', 'required|numeric');
				$this->form_validation->set_rules('max_spend', 'Maximum Spend', 'required|numeric');
				$this->form_validation->set_rules('coupon_amount', 'Coupon Amount', 'required|numeric');
				$this->form_validation->set_rules('usage_limit_per_coupon', 'Usage Limit Per Coupon', 'required|numeric');
				//$this->form_validation->set_rules('usage_limit_to_items', 'Usage Limit To Items', 'required|numeric');
				$this->form_validation->set_rules('usage_limit_to_user', 'Usage Limit To User', 'required|numeric');		
				$this->form_validation->set_rules('brand', 'Brand', 'required');
				//$this->form_validation->set_rules('products', 'Products', 'required');				
				//$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
						
				
				if($this->form_validation->run() != TRUE)
				{					
					$this->load->view('common/header',$data);
					$this->load->view('addEdit_Coupon');
					$this->load->view('common/footer');

				}else{					
						
						$coupon_code= trim($this->input->post('coupon_code'));
						$coupon_desc= trim($this->input->post('coupon_desc'));
						$date_from= $this->input->post('date_from');
						$date_to= $this->input->post('date_to');
						$min_spend= trim($this->input->post('min_spend'));
						$max_spend= trim($this->input->post('max_spend'));
						$coupon_amount= trim($this->input->post('coupon_amount'));
						$discount_type = $this->input->post('discount_type');										
						$individual_use_only= $this->input->post('individual_use_only')==0 ? 0 :1;
						$exclude_sale_items= $this->input->post('exclude_sale_items')==1 ? 1 : 0;
						$stylecracker_discount= $this->input->post('stylecracker_discount')==1 ? 1 : 0;
						$showEmailPopUp= $this->input->post('showEmailPopUp')==1 ? 1 : 0;
						$bin_no = $this->input->post('bin_no');
						$show_on_web = $this->input->post('show_on_web')==0 ? 0 : 1;
						$coupon_type = $this->input->post('coupon_type');

						if(!empty($this->input->post('coupon_email_applied')))
						{
							$coupon_email_applied = $this->input->post('coupon_email_applied');
						}else
						{
							$coupon_email_applied = 0;
						}
						
						if($this->input->post('brand')!='')
						{
							$brand= $this->input->post('brand');
						}else
						{
							$brand = 0;
						}
						
						if($this->input->post('products')!='')
						{
							$products= $this->input->post('products');
							$products_str =  implode(",",$products);
										
						}else
						{
							$products_str = 0;
						}
								
						$email= $this->input->post('email');
						$allow_free_ship = $this->input->post('allow_free_ship')==1 ? 1 : 0;
						$exclude_products = '';
						$prd_category = '';
						$exclude_prd_category = '';	
						$usage_limit_per_coupon	 = trim($this->input->post('usage_limit_per_coupon'));
						$usage_limit_to_items = trim($this->input->post('usage_limit_to_items'));
						$usage_limit_to_user = trim($this->input->post('usage_limit_to_user'));	
								
				
						$data['coupon_info'] = array(
							'coupon_code'=>$coupon_code,
							'coupon_desc'=>$coupon_desc,
							'discount_type'=>$discount_type,
							'coupon_amount'=>$coupon_amount,
							'allow_free_ship'=>$allow_free_ship,
							'coupon_start_datetime'=>$date_from,						
							'coupon_end_datetime'=>$date_to,
							'min_spend'=>$min_spend,
							'max_spend'=>$max_spend,
							'individual_use_only'=>$individual_use_only,
							'exclude_sale_items'=>$exclude_sale_items,
							'brand_id' => $brand, 							
							'products'=>$products_str,							
							'exclude_products'=>$exclude_products,
							'prd_category'=>$prd_category,
							'exclude_prd_category'=>$exclude_prd_category,
							'email_restriction'=>$email,
							'usage_limit_per_coupon'=> $usage_limit_per_coupon,
							'usage_limit_to_items'=>$usage_limit_to_items,
							'usage_limit_to_user'=>$usage_limit_to_user,
							'stylecrackerDiscount'=>$stylecracker_discount,
							'coupon_email_applied' => $coupon_email_applied,
							'showEmailPopUp'=>$showEmailPopUp,
							'status'=>1,
							'created_datetime'=>date('Y-m-d H:i:s'),						
							'created_by'=>$this->session->userdata('user_id'),
							'modified_by' => $this->session->userdata('user_id'),
							'show_on_web' => $show_on_web,
							'bin_no' => $bin_no,
							'is_special_code'=>$coupon_type	
						);					
						
						$result_add = $this->Couponsettings_model->saveUpdate_coupon($data); 

						if($result_add!='')
						{
							$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Coupon Code '.$coupon_code.' added successfully!</div>');
                          	redirect('/CouponSettings/');
							
						}else
						{
							$this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Error!</strong> </h5> Coupon Code '.$coupon_code.' already exist!</div>');
							redirect('/CouponSettings/');
							
						}				
					}					
					/*CouponCode Add Section End */	
			}else{
					/*CouponCode Edit Section*/				
				$coupon_id = $this->uri->segment(3);
				if($coupon_id!='')
				{
				  $data['coupon_data'] = $this->Couponsettings_model->get_coupon('','','','',$coupon_id);
				 if(!empty($data['coupon_data']))
				 {
				 	  $product_info = $data['coupon_data'][0]->products;				 
					  $product_db =  explode(',',$product_info);
					  $product_info_str ='';
					  foreach($product_db as $val)
					  {
					  	$product_info_str = $product_info_str.'"'.$val.'",';
					  }

					  $data['products'] =  $product_info_str;	
				 }				  

				  if(!empty($this->input->post()))
					{

						$this->form_validation->set_rules('coupon_code', 'Coupon Code', 'required');
						$this->form_validation->set_rules('coupon_desc', 'Coupon Description', 'required');
						$this->form_validation->set_rules('date_from', 'Date From', 'required');
						$this->form_validation->set_rules('date_to', 'Date To', 'required');
						$this->form_validation->set_rules('min_spend', 'Minimum Spend', 'required|numeric');
						$this->form_validation->set_rules('max_spend', 'Maximum Spend', 'required|numeric');
						$this->form_validation->set_rules('coupon_amount', 'Coupon Amount', 'required|numeric');
						$this->form_validation->set_rules('usage_limit_per_coupon', 'Usage Limit Per Coupon', 'required|numeric');
						//$this->form_validation->set_rules('usage_limit_to_items', 'Usage Limit To Items', 'required|numeric');
						$this->form_validation->set_rules('usage_limit_to_user', 'Usage Limit To User', 'required|numeric');		
						$this->form_validation->set_rules('brand', 'Brand', 'required');
						//$this->form_validation->set_rules('products', 'Products', 'required');				
						//$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
								
						
						if($this->form_validation->run() != TRUE)
						{					
							$this->load->view('common/header',$data);
							$this->load->view('addEdit_Coupon');
							$this->load->view('common/footer');

						}else{					
								
								$coupon_code= trim($this->input->post('coupon_code'));
								$coupon_desc= trim($this->input->post('coupon_desc'));
								$date_from= $this->input->post('date_from');
								$date_to= $this->input->post('date_to');
								$min_spend= trim($this->input->post('min_spend'));
								$max_spend= trim($this->input->post('max_spend'));
								$coupon_amount= trim($this->input->post('coupon_amount'));
								$discount_type = $this->input->post('discount_type');
								$individual_use_only= $this->input->post('individual_use_only')==0 ? 0 :1;
								$exclude_sale_items= $this->input->post('exclude_sale_items')==1 ? 1 : 0;
								$brand= $this->input->post('brand');
								$products= $this->input->post('products');					
								$email= $this->input->post('email');
								$allow_free_ship = $this->input->post('allow_free_ship')==1 ? 1 : 0;
								$exclude_products = '';
								$prd_category = '';
								$exclude_prd_category = '';	
								$usage_limit_per_coupon	 = trim($this->input->post('usage_limit_per_coupon'));
								$usage_limit_to_items = trim($this->input->post('usage_limit_to_items'));
								$usage_limit_to_user = trim($this->input->post('usage_limit_to_user'));	
								$stylecracker_discount= $this->input->post('stylecracker_discount')==1 ? 1 : 0;
								$showEmailPopUp= $this->input->post('showEmailPopUp')==1 ? 1 : 0;
								$bin_no = $this->input->post('bin_no');
								$show_on_web = $this->input->post('show_on_web')==0 ? 0 :1;
								$coupon_type = $this->input->post('coupon_type');
								
								if(!empty($this->input->post('coupon_email_applied')))
								{
									$coupon_email_applied = $this->input->post('coupon_email_applied');
								}else
								{
									$coupon_email_applied = 0;
								}
								
								if($this->input->post('brand')!='')
								{
									$brand= $this->input->post('brand');
								}else
								{
									$brand = 0;
								}
								
								if( ($discount_type == 3) || ($discount_type == 4) )
								{
									if($this->input->post('products')!='')
									{
										$products_str =  implode(",",$products);
									}else
									{
										$products_str = 0;
									}		
								}else
								{
									$products_str = 0;
								}	
						
								$data['coupon_info'] = array(									
									'coupon_desc'=>$coupon_desc,
									'discount_type'=>$discount_type,
									'coupon_amount'=>$coupon_amount,
									'allow_free_ship'=>$allow_free_ship,
									'coupon_start_datetime'=>$date_from,						
									'coupon_end_datetime'=>$date_to,
									'min_spend'=>$min_spend,
									'max_spend'=>$max_spend,
									'individual_use_only'=>$individual_use_only,
									'exclude_sale_items'=>$exclude_sale_items,
									'brand_id' => $brand, 							
									'products'=>$products_str,							
									'exclude_products'=>$exclude_products,
									'prd_category'=>$prd_category,
									'exclude_prd_category'=>$exclude_prd_category,
									'email_restriction'=>$email,
									'usage_limit_per_coupon'=> $usage_limit_per_coupon,
									'usage_limit_to_items'=>$usage_limit_to_items,
									'usage_limit_to_user'=>$usage_limit_to_user,
									'stylecrackerDiscount'=>$stylecracker_discount,
									'coupon_email_applied' => $coupon_email_applied,
									'showEmailPopUp'=>$showEmailPopUp,
									'status'=>1,									
									'modified_by' => $this->session->userdata('user_id'),
									'show_on_web' => $show_on_web,
									'bin_no' => $bin_no,
									'is_special_code'=>$coupon_type	
								);	
								
								$result_edit = $this->Couponsettings_model->saveUpdate_coupon($data,$coupon_id); 
								if($result_edit!='')
								{
									$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Coupon Code '.$coupon_code.' Updated successfully!</div>');
		                          	redirect('/CouponSettings/');																		
								}else
								{
									$this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Error!</strong> </h5> Coupon Code '.$coupon_code.' already exist!</div>');
									redirect('/CouponSettings/');									
								}			
								
							}
						}
				}	
				/*CouponCode Edit Section End*/									
				$this->load->view('common/header',$data);
				$this->load->view('addEdit_Coupon');
				$this->load->view('common/footer');
			}			

		}else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');				
		}
	
	}
		

	public function get_store_products($brand =NULL){		
		$product_info = array();
		$type = $this->input->post('type');
		
		if(!empty($this->input->post('brand_id')))
		{
			$brand_id = $this->input->post('brand_id');
		}else
		{
			$brand_id = $brand;
		}
		$sel_id =$this->input->post('sel_id');
		$product_info = $this->Couponsettings_model->get_store_products($brand_id);	
		$product = json_encode($product_info);
		echo $product;
	}


	function delete_coupon()
    {
        $catID = $this->input->post('couponID');
        $deleted = $this->Couponsettings_model->delete_coupon($catID);

        if($deleted)
        {
            $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Record has been deleted successfully</div>');
            redirect('/CouponSettings');
        }
        else
        {
            $this->session->set_flashdata('feedback', '<div class="alert alert-danger alert-dismissable"><h5><i class="icon fa fa-ban"></i><strong>Error!</strong></h5>The server is not responding, try again later.</div>');
            redirect('/CouponSettings');
        }
    }


    function download_coupon($exel,$search_by=NULL,$date_from=NULL,$date_to=NULL,$table_search=NULL){

		//echo $search_by.'-------'.$date_from.'-------'.$date_to.'-------'.$table_search;exit;
		/*$search_by = $this->uri->segment(6);

		$search_by = $this->input->post('search_by');
		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');
		$table_search = $this->input->post('table_search');*/
		//$result = $this->Couponsettings_model->download_coupon_sheet($search_by,$table_search,$date_from,$date_to);
		/*echo $search_by;
		if($search_by == 2){
			echo base64_decode($table_search);		
		}else{
			echo $table_search;
		}*/
		$result = $this->Couponsettings_model->download_coupon_sheet($search_by,$date_from,$date_to,$table_search);

		//$discount_type = array('1'=>'Cart discount','2'=>'Cart % discount','3'=>'Product discount','4'=>'Product % discount');			


		/*echo '<pre>';
		print_r($result);
		exit;*/
		$fieldName = array('ID','COUPON CODE','DESCRIPTION','BRAND','DISCOUNT TYPE','START DATETIME','END DATETIME','MINIMUM SPEND','COUPON AMOUNT','STYLECRACKER DISCOUNT','COUPON USED COUNT','STATUS');
		//echo "<pre>";print_r($result);exit;
		$i =0;
		$is_download = 1;
		$result_data = array();
		
		foreach($result as $val) {   		
			$result_data[$i][] = $val['id'];
			$result_data[$i][] = $val['coupon_code'];				
			$result_data[$i][] = $val['coupon_desc'];									
			$result_data[$i][] = $val['brand_id'];							
			if($val['discount_type']==1){ $discount_type = 'Cart discount';}elseif($val['discount_type']==2){ $discount_type = "Cart % discount";}elseif($val['discount_type']==1){ $discount_type = "Product discount";}else{ $discount_type = "Product % discount";}
			$result_data[$i][] = $discount_type;	
			$result_data[$i][] = $val['coupon_start_datetime'];	
			$result_data[$i][] = $val['coupon_end_datetime'];	
			$result_data[$i][] = $val['min_spend'];	
			$result_data[$i][] = $val['coupon_amount'];	
			if($val['stylecrackerDiscount']==1){ $stylecrackerDiscount = 'Yes';}else{ $stylecrackerDiscount = "No";}
			$result_data[$i][] = $stylecrackerDiscount;	
			$result_data[$i][] = $val['coupon_used_count'];				
			if($val['status']==1){ $status = 'Yes';}else{ $status = "No";}
			$result_data[$i][] =  $status;				
			$i++;
		}		
		if($is_download == 1){       	
		  //========================Excel Download==============================
			//$excel_result = $this->writeDataintoCSV($fieldName,$get_data,'view_order_payment');
		   $this->writeDataintoCSV($fieldName,$result_data,'coupon_sheet');
		   //========================End Excel Download==========================
		}
	}
	

	function upload_couponSettings(){
		$this->load->view('common/header');
		$this->load->view('upload_couponSettings');
		$this->load->view('common/footer');
	}	



	function upload_way_queesheet(){
		if($this->input->post('Save') == 'Save'){		
			if($_FILES['product_upload']['tmp_name'] != ""){
				$inputFileName = $_FILES['product_upload']['tmp_name'];					
			}else{
				redirect('upload_couponSettings','refresh');
			}
			$handle = fopen($inputFileName, "r");
			$fieldorder='0';
			try{
				//read file from path
				$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);				
			}catch(Exception $e) {
				die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
			}
			//$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
			$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);			
			$arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet							
			$excelfields =array('coupon_code','usage_limit_per_coupon','usage_limit_to_user','coupon_description','email_Id_applicable','start_date','end_date','minimum_spend','maximum_spend','discount_type','coupon_amount','individual_use_only','exclude_sale_items','stylecracker_discount','show_email_popup');			
			if(trim($allDataInSheet[1]["A"]) != $excelfields[0]){ $fieldorder = 1; echo '0';  }
			else if(trim($allDataInSheet[1]["B"]) != $excelfields[1]){ $fieldorder = 1; echo '1'; }
			else if(trim($allDataInSheet[1]["C"]) != $excelfields[2]){ $fieldorder = 1; echo '2'; }
			else if(trim($allDataInSheet[1]["D"]) != $excelfields[3]){ $fieldorder = 1; echo '3'; }
			else if(trim($allDataInSheet[1]["E"]) != $excelfields[4]){ $fieldorder = 1; echo '4'; }
			else if(trim($allDataInSheet[1]["F"]) != $excelfields[5]){ $fieldorder = 1; echo '5'; }			
			else if(trim($allDataInSheet[1]["G"]) != $excelfields[6]){ $fieldorder = 1; echo '6'; }			
			else if(trim($allDataInSheet[1]["H"]) != $excelfields[7]){ $fieldorder = 1; echo '7'; }
			else if(trim($allDataInSheet[1]["I"]) != $excelfields[8]){ $fieldorder = 1; echo '8'; }
			else if(trim($allDataInSheet[1]["J"]) != $excelfields[9]){ $fieldorder = 1; echo '9'; }
			else if(trim($allDataInSheet[1]["K"]) != $excelfields[10]){ $fieldorder = 1; echo '10'; }
			else if(trim($allDataInSheet[1]["L"]) != $excelfields[11]){ $fieldorder = 1; echo '11'; }
			else if(trim($allDataInSheet[1]["M"]) != $excelfields[12]){ $fieldorder = 1; echo '12'; }
			else if(trim($allDataInSheet[1]["N"]) != $excelfields[13]){ $fieldorder = 1; echo '13'; }
			else if(trim($allDataInSheet[1]["O"]) != $excelfields[14]){ $fieldorder = 1; echo '14'; }
			$errorcount = 0;			
			$successcount = 0;
			$resultmsg = 0;
			$excelmsg = array();
			$show_error = array();
			if($fieldorder == 1){
				$excelmsg = "Please Insert in the order of the fields uploaded.";
			}else{								
				for($i=2; $i<=$arrayCount; $i++){
					$imagename = "";
					$errorFlag = 0;
					$resultmsg = 0;
					$price = "";
					$coupon_code = trim($allDataInSheet[$i]["A"]);
					$usage_limit_per_coupon = trim($allDataInSheet[$i]["B"]);
					$usage_limit_to_user=trim($allDataInSheet[$i]["C"]);
					$coupon_description=strtoupper($allDataInSheet[$i]["D"]);
					$email_Id_applicable = trim($allDataInSheet[$i]["E"]);
					$start_date = trim($allDataInSheet[$i]["F"]);
					$end_date = trim(@$allDataInSheet[$i]["G"]);
					$minimum_spend = trim(@$allDataInSheet[$i]["H"]);
					$maximum_spend = trim(@$allDataInSheet[$i]["I"]);
					$discount_type = trim(@$allDataInSheet[$i]["J"]);
					$coupon_amount = trim($allDataInSheet[$i]["K"]);
					$individual_use_only = trim(@$allDataInSheet[$i]["L"]);
					$exclude_sale_items = trim(@$allDataInSheet[$i]["M"]);
					$stylecracker_discount = trim(@$allDataInSheet[$i]["N"]);
					$show_email_popup = trim(@$allDataInSheet[$i]["O"]);
					$error_meassage = 0;
					if($coupon_code == "" || $usage_limit_per_coupon == "" || $usage_limit_to_user == "" || $start_date == "" || $end_date == "" || $minimum_spend == "" || $maximum_spend == ""|| $coupon_amount == ""){
						$error_meassage = $i;
						$data['show_error'][] = $error_meassage;
						//print_r($data['show_error']);
						$blank_entry = $data['show_error'][0];
						/*$msg3 = '<div class="alert alert-info alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			            <h5 class="text-red"><i class="fa fa-times-circle sign"></i><strong>Error! Blank Entry '.$blank_entry.' </strong></h5></div>';
			            	$this->session->set_flashdata('feedback',$msg3);*/
						//exit;
					}else{	
							if($error_meassage == 0){				
								$data_post[$i] = array(
									'coupon_code'=>$coupon_code,
									'usage_limit_per_coupon'=>$usage_limit_per_coupon,
									'usage_limit_to_user'=>$usage_limit_to_user,
									'coupon_description'=>$coupon_description,
									'email_Id_applicable'=>$email_Id_applicable,
									'start_date'=>$start_date,
									'end_date'=>$end_date,
									'minimum_spend'=>$minimum_spend,
									'maximum_spend'=>$maximum_spend,
									'discount_type'=>$discount_type,
									'coupon_amount'=>$coupon_amount,
									'individual_use_only'=>$individual_use_only,
									'exclude_sale_items'=>$exclude_sale_items,
									'stylecracker_discount'=>$stylecracker_discount,
									'show_email_popup'=>$show_email_popup,
								);								
							}
						}
					}					
					$data['meassage'] = $this->Couponsettings_model->Coupon_uploadqueesheet($data_post);																
					$coupon_id = "";
					foreach ($data['meassage']['duplicate_id'] as $key) {
						$coupon_id .= $key.' ';
					}
					$error_message = "";               					    
					$successfully_message = "";         
					$blank_entry = "";         

						if(!empty($data['meassage']['successfully']) || !empty($data['meassage']['duplicate']) || !empty($blank_entry)){                           							
			                  $successfully_message = $successfully_message .$data['meassage']['successfully'];
			                  $error_message = $error_message .$data['meassage']['duplicate'];                                
			            }

			        /*echo '<pre>';
					print_r($data);            
					exit;*/    
			            /*$msg2 = '<div class="alert alert-info alert-dismissable" style="background:#d9edf7;  border: 1px solid #cce0ea; "><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			            <p class="text-red">Error! Duplicate Entry '.$error_message.' 
			            <br/>Duplicate coupon id\'s :-  '.@$coupon_id.'</p>
			            <p class="text-red">Line number Error '.@$blank_entry.' </p>
			            <p class="text-green">Successfully Entry '.@$successfully_message.'</p> </div>';
			            	$this->session->set_flashdata('feedback',$msg2);*/

/*
			            	 $msg2 = '<p class="text-red">Error! Duplicate Entry '.$error_message.' <br/>Duplicate coupon id\'s :-  '.@$coupon_id.'</p><p class="text-red">Line number Error '.@$blank_entry.' </p><p class="text-green">Successfully Entry '.@$successfully_message.'</p> ';
			            	

			            	$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>'. $msg2.'</div>');*/
					}			
				
						$this->load->view('common/header');
						$this->load->view('upload_couponSettings',$data);
						$this->load->view('common/footer');									
			    }
		/*$this->load->view('common/header');
		$this->load->view('upload_couponSettings');
		$this->load->view('common/footer');*/
	}
	
}
