<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Collectionsimageupload extends MY_Controller {    

    function __construct(){
        parent::__construct();
        $this->load->model('Imageupload_model');  
		$this->load->model('Collection_img_model');    	
    }  
 /*---------Category submodule functionalities----------*/

    public function index($offset = null)
    {   
      
        if(!$this->session->userdata('user_id')){

            $this->load->view('login');                    

        }else{ 
			
			/*All Data Show*/			
			$data_post = array();                       
           if($this->has_rights(49) == 1){	
			    
			    $data['street_pic'] =  $this->Collection_img_model->get_collection_pic();  
			    
			    $paginationUrl = 'Imageupload/index/';
                $uri_segment = 3;
                $config['per_page'] = 20;
                $config['total_rows'] = count($this->Collection_img_model->get_collection_pic('','','',''));                
                $total_rows = $config['total_rows'];                                                    
                $data['street_pic'] = $this->Collection_img_model->get_collection_pic('','',$offset,$config['per_page']);

			    $this->customPagination($paginationUrl,$config['total_rows'],$uri_segment,$data['street_pic'],$total_rows);
			    $this->load->view('common/header');
				$this->load->view('data_upload/collection_img',$data);
				$this->load->view('common/footer');
			}else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			}        
		}
	}
	
	public function upload_images(){
		if(!$this->session->userdata('user_id')){
			
            $this->load->view('login');                    

        }else{ 
			 if($this->has_rights(49) == 1){	  
				
			
				$files = $_FILES;
				$count = count($_FILES['images']['name']);
				$output = [];
				for($i=0; $i<$count; $i++)
				{ 
					$_FILES['images']['name']= $files['images']['name'][$i];
					$_FILES['images']['type']= $files['images']['type'][$i];
					$_FILES['images']['tmp_name']= $files['images']['tmp_name'][$i];
					$_FILES['images']['error']= $files['images']['error'][$i];
					$_FILES['images']['size']= $files['images']['size'][$i];    
					$ext = pathinfo($files['images']['name'][$i], PATHINFO_EXTENSION);
					$img_name = time().mt_rand(0,10000).'.'.$ext;
					$pathAndName = "assets/event_img/".$img_name;
					// Run the move_uploaded_file() function here
					$moveResult = move_uploaded_file($files['images']['tmp_name'][$i], $pathAndName);
					$this->Collection_img_model->collection_pic($img_name,$this->session->userdata('user_id'));  
					if($ext!='gif'){
					$this->compress_image("assets/event_img/".$img_name, "assets/event_img/".$img_name, '70');
					}
                 }
                  echo json_encode($output);
                          
			 }else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			 }   
		}
	}
	
	  private function set_upload_options()
        {   

            $config = array();
            $config['upload_path'] = base_url().'assets/event_img/';
            $config['allowed_types'] = 'gif|jpg|png|jepg';
            $config['overwrite']     = FALSE;
            return $config;
        }
	
	 public function delete_collection_pic(){
			$del_id = $this->input->post('id');
			$this->Collection_img_model->delete_collection_pic($del_id,$this->session->userdata('user_id'));  
		 }
	    
/*----------End of Common functions------------------------------------*/	
}
