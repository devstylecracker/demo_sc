<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BrandCategory extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('BrandCategory_model');
        $this->load->model('Category_model');
        $this->load->library('curl');

    }

 /*---------Category submodule functionalities----------*/

    public function index($offset = null)
    {

        if(!$this->session->userdata('user_id')){

            $this->load->view('login');

        }else{

            $data_post = array();

            //$data['menu_permissions'] = $this->data['users_menu'];

            if($this->has_rights(66) == 1){

                $search_by = $this->input->post('search_by');
                $table_search = $this->input->post('table_search');
                $paginationUrl = 'brandcategory/index/';
                $uri_segment = 3;
                $config['per_page'] = 20;
                $config['total_rows'] = count($this->BrandCategory_model->get_all_categories($search_by,$table_search,'',''));
                $total_rows = $config['total_rows'];
                $get_users = $this->BrandCategory_model->get_all_categories($search_by,$table_search,$offset,$config['per_page']);
                $count = $config['total_rows'];
                $data_post['categories'] = $get_users;

                $this->customPagination($paginationUrl,$count,$uri_segment,$get_users,$total_rows);
                $this->load->view('common/header',$data_post);
                $this->load->view('brandcategory_view');
                $this->load->view('common/footer');

            }else{

                $this->load->view('common/header');
                $this->load->view('not_permission');
                $this->load->view('common/footer');
            }
         }


    }

    public function brandcategory_addedit($id=NULL)
    {        
        $data_post = array();
        $loggedInUser=$this->session->userdata('user_id');
        /*echo '<pre>';print_r($this->input->post());exit;*/
         if($id==NULL)
        {
            if($this->has_rights(66) == 1)
            {
                $data_post['brandCat'] = $this->BrandCategory_model->get_brand_parent_category();
                if($this->input->post())
                {                   
                    $ok=0;
                    $this->form_validation->set_rules('categoryName', 'Category Name', 'required');
                    $this->form_validation->set_rules('categorySlug', 'Category Description', 'required');

                    $config['upload_path'] = 'assets/brandCategory_images/';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['max_size'] = '2048'; // 2048 KB = 2MB = 2048000 B
                    $config['max_width']  = '1400';
                    //$config['max_height']  = '500';
                    $config['file_name'] = time().mt_rand(0,10000);
                    $this->upload->initialize($config);                   

                    if($this->form_validation->run() == FALSE ||  (!($this->upload->do_upload('category_image'))))
                    {
                        $data_post['image_error'] = array($this->upload->display_errors());
                        $this->load->view('common/header');
                        $this->load->view('brandcategory',$data_post);
                        $this->load->view('common/footer');

                    }else{

                            $categoryName = $this->input->post('categoryName');
                            $categorySlug = $this->input->post('categorySlug');
                            $categoryParent = $this->input->post('categoryParent');
                            $categoryType = $this->input->post('category_type');
                            $active = $this->input->post('active');

                            $category_logo= $this->upload->data('file_name');
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = './assets/brandCategory_images/'.$category_logo;
                            $config['new_image'] = './assets/brandCategory_images/thumb/';
                            $config['create_thumb'] = TRUE;
                            $config['thumb_marker'] = '';
                            $config['maintain_ratio'] = TRUE;
                            $config['width'] = 160;
                            //$config['height'] = 200;

                            $this->image_lib->initialize($config);
                            if (!$this->image_lib->resize())
                            {
                                $this->image_lib->display_errors();
                                $ok =0;

                            }else{

                                $ok =1;
                            }

                            $catCount = $this->BrandCategory_model->checkMultilpeCategroy($this->input->post('categoryName'),$categoryParent);
                            //echo $catCount->catCount;exit;
                            if($catCount->catCount > 0)
                            {                               
                                $this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>  Brand Category already exits! </div>');
                                $this->load->view('common/header');
                                $this->load->view('brandcategory',$data_post);
                                $this->load->view('common/footer');
                            }
                            else
                            {
                                $data_post = array(
                                    'category_name'=>$categoryName,
                                    'category_slug' => strtolower($categorySlug),
                                    'category_type' => 'brand_cat',
                                    'category_image' => $category_logo,
                                    'status'=> $active,
                                    'parent' => $categoryParent,
                                    'category_type' => $categoryType,
                                    'created_by'=> $loggedInUser,
                                    'created_datetime'=> date("Y-m-d H:i:s"),
                                    'modified_by'=> $loggedInUser,
                                    'modified_datetime'=> date("Y-m-d H:i:s")
                                    );

                                $created = $this->BrandCategory_model->new_category($data_post);
                                if($created){

                                        $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Brand Category added successfully!</div>');
                                        redirect('/BrandCategory');
                                }
                                else
                                {
                                     $this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5> The server is not responding, try again later.</div>');
                                        redirect('/BrandCategory');
                                }
                            }
                        }
                    }else
                    {
                        $this->load->view('common/header');
                        $this->load->view('brandcategory',$data_post);
                        $this->load->view('common/footer');
                    }
                }else
                {
                    $this->load->view('common/header');
                    $this->load->view('not_permission');
                    $this->load->view('common/footer');
                }

            }else
            {
               
                if($this->has_rights(66) == 1)
                {
                    $data_post['category_details'] = $this->BrandCategory_model->get_category_by_ID($id);                    
                    $data_post['brandCat'] = $this->BrandCategory_model->get_brand_parent_category();
                    if($this->input->post())
                    {

                        $this->form_validation->set_rules('categoryName', 'Category Name', 'required');
                        $this->form_validation->set_rules('categorySlug', 'Category Slug', 'required');  


                        if(isset($_FILES['category_image']['name']) && $_FILES['category_image']['name']!=''){

                        $category_logo= $this->upload->data('file_name');
                        $config['upload_path'] = 'assets/brandCategory_images/';
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config['max_size'] = '2048';
                        $config['max_width']  = '1400';
                        $config['overwrite'] = TRUE; //overwrite user avatar
                        //$config['max_height']  = '500';
                        $config['file_name'] = $data_post['category_details']->category_image;
                        $this->upload->initialize($config);

                        if($this->form_validation->run() == FALSE ||  (!($this->upload->do_upload('category_image'))))
                        {
                            $data_post['image_error'] = array($this->upload->display_errors());
                            $this->load->view('common/header');
                            $this->load->view('brandcategory',$data_post);
                            $this->load->view('common/footer');

                            }else{

                                    $config['image_library'] = 'gd2';
                                    $config['source_image'] = './assets/brandCategory_images/'.$data_post['category_details']->category_image;
                                    $config['new_image'] = './assets/brandCategory_images/thumb/';
                                    $config['create_thumb'] = TRUE;
                                    $config['thumb_marker'] = '';
                                    $config['maintain_ratio'] = TRUE;
                                    $config['overwrite'] = TRUE; //overwrite user avatar
                                    $config['width'] = 200;
                                    //$config['height'] = 200;

                                    $this->image_lib->initialize($config);

                                    if (!$this->image_lib->resize())
                                    {
                                        $this->image_lib->display_errors();
                                        $ok =0;

                                    }else{

                                        $ok =1;
                                    }
                                }
                        }                     

                        if ($this->form_validation->run() == FALSE)
                        {
                            $this->load->view('common/header');
                            $this->load->view('BrandCategory');
                            $this->load->view('common/footer');

                        }else{
                                $categoryName = $this->input->post('categoryName');
                                $categorySlug = $this->input->post('categorySlug');
                                $categoryParent = $this->input->post('categoryParent');
                                $active = $this->input->post('active');

                                $catCount = $this->BrandCategory_model->checkMultilpeCategroyEdit($id,$categoryName,$categoryParent);
                                if($catCount->catCount > 0)
                                {
                                    $this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-warning"></i><strong>Error!</strong></h5> Category already exits!</div>');
                                        $this->load->view('common/header');
                                        $this->load->view('brandcategory',$data_post);
                                        $this->load->view('common/footer');
                                }
                                else
                                {
                                    $data_post = array(
                                            'category_name'=>$categoryName,
                                            'category_slug' => strtolower($categorySlug),
                                            'category_type' => 'brand_cat',
                                            'status'=> $active,
                                            'parent' => $categoryParent,
                                            'created_by'=> $loggedInUser,
                                            'created_datetime'=> date("Y-m-d H:i:s"),
                                            'modified_by'=> $loggedInUser,
                                            'modified_datetime'=> date("Y-m-d H:i:s")
                                            );

                                    $update = $this->BrandCategory_model->update_category($id,$data_post);

                                    if($update)
                                    {
                                        $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Changes has been Updated successfully</div>');
                                        redirect('/BrandCategory');
                                    }
                                    else
                                    {
                                        $this->session->set_flashdata('feedback', '<div class="alert alert-warning alert-dismissable "><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-warning"></i><strong>Error!</strong></h5> The server is not responding, try again later.</div>');
                                        redirect('/BrandCategory');
                                    }
                                }
                            }
                        }else
                        {
                            $this->load->view('common/header');
                            $this->load->view('brandcategory',$data_post);
                            $this->load->view('common/footer');
                        }
                }else
                {
                    $this->load->view('common/header');
                    $this->load->view('not_permission');
                    $this->load->view('common/footer');
                }

             }
    }

    function delete_brandcategory()
    {
        $catID = $this->input->post('catID');
        $deleted = $this->BrandCategory_model->delete_category($catID);

        if($deleted)
        {
            $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Record has been deleted successfully</div>');
            redirect('/BrandCategory');
        }
        else
        {
            $this->session->set_flashdata('feedback', '<div class="alert alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-ban"></i><strong>Error!</strong></h5>The server is not responding, try again later.</div>');
            redirect('/BrandCategory');
        }
    }

/*---------End of Category submodule functionalities-------*/   


   function brandcategory_relation()
    {
        $data_post = array();       
        $data_post['paid_stores'] = $this->BrandCategory_model->get_paid_stores(); 
        $data_post['brand_cat'] = $this->BrandCategory_model->get_brand_categories(); 

        $brand_id = $this->input->post('brand');
        $brand_cat = $this->input->post('brandCat');
        
        if($brand_id!='')
        {
            $result = $this->BrandCategory_model->saveBrandCat($brand_id,$brand_cat);
            if($result)
            {
                $result = $this->curl->simple_post(LIVE_SITE.'Sccache/clear_brand_listing_page', false, array(CURLOPT_USERAGENT => true));
                $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Record has been Added successfully</div>');  
            }else
            {
                 $this->session->set_flashdata('feedback', '<div class="alert alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-ban"></i><strong>Error!</strong></h5>The server is not responding, try again later.</div>');            
            }
        }

        $this->load->view('common/header');
        $this->load->view('brandcategory_relation',$data_post);
        $this->load->view('common/footer');
    }

    function get_brand_categories($brand =NULL){     
       
        $brand_id = $this->input->post('brand_id');
        if($brand_id){
            echo $category_info = $this->BrandCategory_model->get_brand_categories($brand_id); 
        }
    }

}
