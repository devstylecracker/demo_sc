<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Most_sharing extends MY_Controller {
	
	function __construct(){
       parent::__construct();
       $this->load->model('websiteusers_model'); 
       $this->load->model('users_model');    
	   $this->load->model('reports_model'); 
      
   } 
  
        public function index($offset = null){
			    //Least_returning_view.php
				$search_by = $this->input->post('search_by');
				$table_search = $this->input->post('table_search');
				
				$config['base_url'] = base_url().'websiteusers/index/';
				//$config['total_rows'] = count($this->reports_model->most_sharing_all($date_from,$date_to);
				$config['per_page'] = 20;
				$config['uri_segment'] = 3;
				$config['use_page_numbers'] = TRUE;
				$config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
				$config['full_tag_close'] = '</ul></div>';

				$config['first_link'] = '&laquo; First';
				$config['first_tag_open'] = '<li>';
				$config['first_tag_close'] = '</li>';

				$config['last_link'] = 'Last &raquo;';
				$config['last_tag_open'] = '<li>';
				$config['last_tag_close'] = '</li>';

				$config['next_link'] = 'Next &rarr;';
				$config['next_tag_open'] = '<li>';
				$config['next_tag_close'] = '</li>';

				$config['prev_link'] = '&larr; Previous';
				$config['prev_tag_open'] = '<li>';
				$config['prev_tag_close'] = '</li>';

				$config['cur_tag_open'] = '<li><a href="">';
				$config['cur_tag_close'] = '</a></li>';

				$config['num_tag_open'] = '<li>';
				$config['num_tag_close'] = '</li>';

				
				$this->pagination->initialize($config);
				if($this->session->userdata('user_id')){
					$data1 = array();
					$date_from = '';  $date_to = '';

					   if($this->input->post()){
						  $date_from = $this->input->post('date_from');
						  $date_to = $this->input->post('date_to');
					   }
				 	$data1['most_sharing_all'] = $this->reports_model->most_sharing_all($search_by,$table_search,$offset,$config['per_page']);
				   //$data1['most_least_runing'] = $this->reports_model->most_least_runing($date_from,$date_to);
					$data1['total_rows'] = count($data1['most_sharing_all']);
					$this->load->view('common/header');
					$this->load->view('reports/Most_sharing_view',$data1);
					$this->load->view('common/footer');
				}
		}
		public function user_view(){
			 $data = array();
		
			 $data['roles'] = $this->users_model->get_roles(); 
			 $user_id =  $this->uri->segment(3);
			 $data['user_data'] = $this->users_model->get_user_data($user_id);
			 $data['pa_data'] = $this->users_model->get_user_pa($user_id); 
			 $data['wordrobe'] = $this->users_model->get_wordrobe_image($user_id); 
			if($this->has_rights(15) == 1){
				
				$this->load->view('common/header',$data);
				$this->load->view('View_all_users_view');
				$this->load->view('common/footer');
				
			}else{
				
				$this->load->view('common/header',$data);
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			 }
			
			}
}
