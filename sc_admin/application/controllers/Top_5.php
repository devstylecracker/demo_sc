<?php
/*
*
  This controller is basically for the dashboard report  - User Related All the Dashboard functions 
  are return in this controller
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class Top_5 extends MY_Controller {
	
	function __construct(){
        parent::__construct(); 
        $this->load->model('top_5_model'); 
       
    }  

	public function top_5_product_cpc()
	{
		if($this->session->userdata('user_id')){ 
      $data = array();  
      
      $from_date = $this->uri->segment(3);
      $to_date = $this->uri->segment(4); 
      $product_id = $this->uri->segment(5); 

      $data['product_cpc_user_info'] = $this->top_5_model->top_5_product_cpc($from_date,$to_date,$product_id);
      $this->load->view('common/header');
      $this->load->view('reports/top_5_product_cpc',$data);
      $this->load->view('common/footer');
      
		}else{ 
      $this->load->view('common/header',$data);
      $this->load->view('not_permission');
      $this->load->view('common/footer');
		}
	}

  public function top_5_looks_click(){
    if($this->session->userdata('user_id')){ 
      $data = array();  
      
      $from_date = $this->uri->segment(3);
      $to_date = $this->uri->segment(4); 
      $product_id = $this->uri->segment(5); 

      $data['product_cpc_user_info'] = $this->top_5_model->top_5_looks_click($from_date,$to_date,$product_id);
      $this->load->view('common/header');
      $this->load->view('reports/top_5_looks_click',$data);
      $this->load->view('common/footer');
      
    }else{ 
      $this->load->view('common/header',$data);
      $this->load->view('not_permission');
      $this->load->view('common/footer');
    }
  }

  public function top_5_product_cpa(){
      if($this->session->userdata('user_id')){ 
      $data = array();  
      
      $from_date = $this->uri->segment(3);
      $to_date = $this->uri->segment(4); 
      $product_id = $this->uri->segment(5); 

      $data['product_cpc_user_info'] = $this->top_5_model->top_5_product_cpa($from_date,$to_date,$product_id);
      $this->load->view('common/header');
      $this->load->view('reports/top_5_product_cpa',$data);
      $this->load->view('common/footer');
      
      }else{ 
        $this->load->view('common/header',$data);
        $this->load->view('not_permission');
        $this->load->view('common/footer');
      }
  }

  public function top_5_brands_cpc(){
    if($this->session->userdata('user_id')){ 
      $data = array();  
      
      $from_date = $this->uri->segment(3);
      $to_date = $this->uri->segment(4); 
      $product_id = $this->uri->segment(5); 

      $data['product_cpc_user_info'] = $this->top_5_model->top_5_brands_cpc($from_date,$to_date,$product_id);
      $this->load->view('common/header');
      $this->load->view('reports/top_5_brands_cpc',$data);
      $this->load->view('common/footer');
      
      }else{ 
        $this->load->view('common/header',$data);
        $this->load->view('not_permission');
        $this->load->view('common/footer');
      }
  }

}

?>