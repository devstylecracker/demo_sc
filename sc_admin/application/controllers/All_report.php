<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class All_report extends MY_Controller {
	
	function __construct(){
        parent::__construct();
        $this->load->model('all_report_model'); 
       
    }  

	public function index()
	{

      $date_from = $this->input->post('date_from');
      $date_to = $this->input->post('date_to');
      $data['date_from'] = $date_from;
      $data['date_to'] = $date_to;
      if($date_from!=''){ $date_from = date('Y-m-d H:i:s', strtotime($date_from)); }
      if($date_to!=''){ $date_to = date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 second',str_replace('-', '/', strtotime($date_to)))); }
      $this->load->view('common/header');
      $this->load->view('all_report',$data);
      $this->load->view('common/footer');
	}
	
	public function download_exel(){
	  $date_from = $this->uri->segment(4);
      $date_to = $this->uri->segment(5);
	  $is_download = 1;
      $data['date_from'] = $date_from;
      $data['date_to'] = $date_to; 
      if($date_from!='' && $date_from != 1){ $date_from = date('Y-m-d H:i:s', strtotime($date_from)); }
      if($date_to!='' && $date_to != 1){ $date_to = date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 second',str_replace('-', '/', strtotime($date_to)))); }
	  if($date_from == 1) $date_from = '';
	  if($date_to == 1) $date_to = '';
	  $result = $this->all_report_model->abendant_cart_list($date_from,$date_to);
	   $fieldName = array('User Id','Email Id','Product Name','User Name','mobile_no','Created Datetime','Product Price','Brand Name');//Brand Payout
	   $result_data = array();
	   $i =0;
	   foreach($result as $val) {   
            $result_data[$i][] = $val->user_id;
			$result_data[$i][] = $val->email_id;
            $result_data[$i][] = $val->name;
			$result_data[$i][] = $val->user_name;
            $result_data[$i][] = $val->mobile_no;
            $result_data[$i][] = $val->created_datetime;
			$result_data[$i][] = $val->price;
            $result_data[$i][] = $val->company_name;
			$i++;
	    }
		// echo $this->db->last_query();exit;
	   // print_r($result_data);exit;
		if($is_download == 1)
		{       	
		  //========================Excel Download==============================
			//$excel_result = $this->writeDataintoCSV($fieldName,$get_data,'view_order_payment');
		   $this->writeDataintoCSV($fieldName,$result_data,'abendant_cart_list');
		   //========================End Excel Download==========================

		   
		}
	}
	public function failed_order_exel(){
		$date_from = $this->uri->segment(4);
		$date_to = $this->uri->segment(5);
		//echo $date_from."commingdate_from";echo $date_to."comming";exit;
		$is_download = 1;
		$data['date_from'] = $date_from;
		$data['date_to'] = $date_to; 
		if($date_from!='' && $date_from != 1){ $date_from = date('Y-m-d H:i:s', strtotime($date_from)); }
		if($date_to!='' && $date_to != 1){ $date_to = date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 second',str_replace('-', '/', strtotime($date_to)))); }
		if($date_from == 1) $date_from = '';
		if($date_to == 1) $date_to = '';
		$result = $this->all_report_model->failed_order_list($date_from,$date_to);
		$fieldName = array('Cart Id','First Name','Last Name','Email Id','Product Name','Product Price','Product Qty','Product Size','Brand Name','Address','Mobile No','Paymod','Created Datetime');//Brand Payout
		$result_data = array();
		$i =0;
		//echo "<pre>";print_r($result);exit;
		foreach($result as $val) {   
			
			$result_data[$i][] = $val->cart_id;
			$result_data[$i][] = $val->first_name;
			$result_data[$i][] = $val->last_name;
			$result_data[$i][] = $val->email_id;
			$result_data[$i][] = $val->product_name;
			$result_data[$i][] = $val->prduct_price;
			$result_data[$i][] = $val->product_qty;
			$result_data[$i][] = $val->product_size;
			$result_data[$i][] = $val->brand_name;
			$result_data[$i][] = $val->address;
			$result_data[$i][] = $val->mobile_no;
			if($val->paymod == 1) $paymod = 'cod'; else $paymod = 'online';
			$result_data[$i][] = $paymod;
			$result_data[$i][] = $val->created_datetime;
			$i++;
		}
		// echo $this->db->last_query();exit;
		// print_r($result_data);exit;
		if($is_download == 1)
		{       	
		  //========================Excel Download==============================
			//$excel_result = $this->writeDataintoCSV($fieldName,$get_data,'view_order_payment');
		   $this->writeDataintoCSV($fieldName,$result_data,'failed_order_list');
		   //========================End Excel Download==========================

		   
		}
	}

	function google_ads(){
		
		$is_download = 1;
		$fieldName = array('id','title','description','link','condition','price','availability','image');
		$result = $this->all_report_model->google_ads();
		$result_data = array();
		$i =0;
		
		foreach($result as $val) {   
			
			$result_data[$i][] = $val['id'];
			$result_data[$i][] = strtoupper($val['company_name']).' '.$val['name'];
			$result_data[$i][] = strip_tags($val['description']);
			$result_data[$i][] = LIVE_SITE.'product/details/'.$val['slug'].'-'.$val['id'];
			$result_data[$i][] = 'New';
			$result_data[$i][] = 'INR'.' '.number_format($val['price']);
			$result_data[$i][] = 'In stock';
			$result_data[$i][] = LIVE_SITE.'sc_admin/assets/products/'.$val['image'];
			
			$i++;
		}
		
		if($is_download == 1)
		{       	
		  //========================Excel Download==============================
			//$excel_result = $this->writeDataintoCSV($fieldName,$get_data,'view_order_payment');
		   $this->writeDataintoCSV($fieldName,$result_data,'Google_Merchant_Center_Feed_Shopping_ad');
		   //========================End Excel Download==========================

		   
		}
	}

	function facebook_ads(){
		
		$is_download = 1;
		$fieldName = array('id','title','ios_url','ios_app_store_id','ios_app_name','android_url','android_package','android_app_name','windows_phone_url','windows_phone_app_id','windows_phone_app_name','description','google_product_category','product_type','link','image_link','condition','availability','price','sale_price','sale_price_effective_date','gtin','brand','mpn','item_group_id','gender','age_group','color','size','shipping','custom_label_0');
		$result = $this->all_report_model->facebook_ads();
		$result_data = array();
		$i =0;
		
		foreach($result as $val) {   
			
			$result_data[$i][] = $val['id'];
			$result_data[$i][] = strtoupper($val['company_name']).' '.$val['name'];
			$result_data[$i][] = '';
			$result_data[$i][] = '';
			$result_data[$i][] = '';
			$result_data[$i][] = LIVE_SITE.'product/'.$val['id'];
			$result_data[$i][] = 'com.stylecracker.android';
			$result_data[$i][] = 'StyleCracker';
			$result_data[$i][] = '';
			$result_data[$i][] = '';
			$result_data[$i][] = '';
			$result_data[$i][] = strip_tags($val['description']);
			$cat_infos = $this->get_categories($val['id']);
			$cat_infos_last = explode('>', $cat_infos);
			$result_data[$i][] = $cat_infos;
			$result_data[$i][] = end($cat_infos_last);
			$result_data[$i][] = LIVE_SITE.'product/details/'.$val['slug'].'-'.$val['id'];
			$result_data[$i][] = LIVE_SITE.'sc_admin/assets/products/'.$val['image'];
			$result_data[$i][] = 'New';
			$result_data[$i][] = 'in stock';
			if($val['compare_price'] == 0){
				$result_data[$i][] = number_format($val['price']).' INR';
			}else{
				$result_data[$i][] = number_format($val['compare_price']).' INR';
			}
			$result_data[$i][] = number_format($val['price']).' INR';
			if($val['compare_price'] == 0){
				$result_data[$i][] = '';
			}else{
				$result_data[$i][] = $val['discount_start_date'];
			}
			$result_data[$i][] = '';
			$result_data[$i][] = $val['company_name'];
			$result_data[$i][] = '';
			$result_data[$i][] = '';
			$result_data[$i][] = $this->get_gender($val['id']);
			$result_data[$i][] = '';
			$result_data[$i][] = '';
			$result_data[$i][] = '';
			$result_data[$i][] = '';
			$result_data[$i][] = '';
			$result_data[$i][] = '';
			
			$i++;
		}
		
		if($is_download == 1)
		{       	
		  //========================Excel Download==============================
			//$excel_result = $this->writeDataintoCSV($fieldName,$get_data,'view_order_payment');
		   $this->writeDataintoCSV($fieldName,$result_data,'Facebook_Shopping_ad');
		   //========================End Excel Download==========================

		   
		}
	}

	function get_gender($product_id){
		return $result = $this->all_report_model->get_gender($product_id);
	}

	function get_categories($product_id){
		return $result = $this->all_report_model->get_categories($product_id);
	}

	public function failed_onlineorder_excel(){
		$date_from = $this->uri->segment(4);
		$date_to = $this->uri->segment(5);
		//echo $date_from."commingdate_from";echo $date_to."comming";exit;
		$is_download = 1;
		$data['date_from'] = $date_from;
		$data['date_to'] = $date_to; 
		if($date_from!='' && $date_from != 1){ $date_from = date('Y-m-d H:i:s', strtotime($date_from)); }
		if($date_to!='' && $date_to != 1){ $date_to = date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 second',str_replace('-', '/', strtotime($date_to)))); }
		if($date_from == 1) $date_from = '';
		if($date_to == 1) $date_to = '';
		$result = $this->all_report_model->failed_response_payu_order($date_from,$date_to);
		$fieldName = array('User Id','Order Unique Number','First Name','Last Name','Email Id','Order Total','Mobile No','Paymod','Created Datetime');//Brand Payout
		$result_data = array();
		$i =0;
		//echo "<pre>";print_r($result);exit;
		foreach($result as $val) {   
			
			$result_data[$i][] = $val->user_id;
			$result_data[$i][] = $val->order_unique_no;
			$result_data[$i][] = $val->first_name;
			$result_data[$i][] = $val->last_name;
			$result_data[$i][] = $val->email_id;
			//$result_data[$i][] = @$val->product_name;
			$result_data[$i][] = $val->order_total;	
			//$result_data[$i][] = @$val->address;
			$result_data[$i][] = $val->mobile_no;
			if($val->pay_mode == 1) $paymod = 'cod'; else $paymod = 'online';
			$result_data[$i][] = $paymod;
			$result_data[$i][] = $val->created_datetime;
			$i++;
		}
		 //echo $this->db->last_query();exit;
		//print_r($result_data);exit;
		if($is_download == 1)
		{       	
		  //========================Excel Download==============================
			//$excel_result = $this->writeDataintoCSV($fieldName,$get_data,'view_order_payment');
		   $this->writeDataintoCSV($fieldName,$result_data,'failed_onlineorder_list');
		   //========================End Excel Download==========================

		   
		}
	}
}