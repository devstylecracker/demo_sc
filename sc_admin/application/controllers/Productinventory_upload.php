<?php
header('Content-type: text/html; charset=utf-8');
defined('BASEPATH') OR exit('No direct script access allowed');

class Productinventory_upload extends MY_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
       parent::__construct();
        $this->load->library('excel');
       $this->load->model('Productinventory_upload_model'); 
       //$this->load->model('users_model'); 
    }   

	public function index($offset = null)
	{
	 	if(!$this->session->userdata('user_id')){
			$this->load->view('login');
			}else{ 
				$data = array();			
				if($this->has_rights(61) == 1){				
					$this->load->view('common/header');
					$this->load->view('productinventory_upload');
					$this->load->view('common/footer');
				}else{
					$this->load->view('common/header');
					$this->load->view('not_permission');
					$this->load->view('common/footer');
				}
			}
	}	
		

	public function product_upload()
	{

		if($this->has_rights(61) == 1){

				if($this->input->post('Save') == 'Save')
				{		
					
				 	if($_FILES['product_upload']['tmp_name'] != "")
				 	{
				 		// This is the file path to be uploaded.

						$inputFileName = $_FILES['product_upload']['tmp_name'];	
                            
				 	}else
				 	{					 			 		

				 		redirect('/product_upload');

				 	}

					/*if($_FILES['product_upload']['size'] > 5000000)

					{

						$excelmsg = "Error Excel File size is greater";

					}*/

					$handle = fopen($inputFileName, "r");
					
					$fieldorder='';
                     
					try{
							//read file from path

						$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);


					}catch(Exception $e) {
						
						die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());

					}
					
					$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

					$arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet	

					/*$excelfields =array('product_id','product_name','product_size','quantity','sku_code','price','description');*/
					$excelfields =array('product_id','product_name','product_size','quantity','sku_code','price','is_active');
					// echo "<pre>";print_r($allDataInSheet);exit;
					if(trim($allDataInSheet[1]["A"]) != $excelfields[0]){ $fieldorder = 1; }
					else if(trim($allDataInSheet[1]["B"]) != $excelfields[1]){ $fieldorder = 1; }
					else if(trim($allDataInSheet[1]["C"]) != $excelfields[2]){ $fieldorder = 1; }
					else if(trim($allDataInSheet[1]["D"]) != $excelfields[3]){ $fieldorder = 1; }
					else if(trim($allDataInSheet[1]["E"]) != $excelfields[4]){ $fieldorder = 1; }
					//else if(trim($allDataInSheet[1]["F"]) != $excelfields[5]){ $fieldorder = 1; }
					//else if(trim($allDataInSheet[1]["G"]) != $excelfields[6]){ $fieldorder = 1; }
					else if(trim($allDataInSheet[1]["G"]) != $excelfields[6]){ $fieldorder = 1; }
					

				$errorcount = 0;

				$successcount = 0;

				$resultmsg = 0;

				$excelmsg = array();



					if($fieldorder == 1)
					{
						$excelmsg = "Please Insert in the order of the fields uploaded.";
					}else
					{

						for($i=2;$i<=$arrayCount;$i++)
						{

							$imagename = "";

							$errorFlag = 0;

							$resultmsg = 0;
							$price = "";$is_active = 0;

							$product_id = trim($allDataInSheet[$i]["A"]);
							$Product_Name = trim($allDataInSheet[$i]["B"]);
							$Product_Size=trim($allDataInSheet[$i]["C"]);
							$Product_Size=strtoupper($Product_Size);
							$Quantity = trim($allDataInSheet[$i]["D"]);
							$sku_code = trim($allDataInSheet[$i]["E"]);
							$price = trim(@$allDataInSheet[$i]["F"]);
							$is_active = trim(@$allDataInSheet[$i]["G"]);
							if($is_active < 0){ $is_active = '0'; }
							$description = '';
						//	echo $Quantity;

						$data_post[$i] = array(
							'product_id'=>$product_id,
							'product_name'=>$Product_Name,
							'product_size'=>$Product_Size,	
							'quantity'=>$Quantity,
							'sku_code' => $sku_code,
							'price' => $price,
							'description'=>htmlspecialchars($description),
							'is_active' => $is_active
						);						

					} 
				
				$data = $this->Productinventory_upload_model->product_upload($data_post);
				
                $this->session->set_flashdata('feedback',$data);
             	redirect('/productinventory_upload');
				}
   			}	                
			}
		}
  
	public function download_sheet(){

			$this->load->helper('download');
			$data = file_get_contents(base_url()."assets/cue_sheet/upload_product_inventory_sample.xlsx"); // Read the file's contents			
			$name = 'upload_product_inventory_sample.xlsx';
			$result = force_download($name, $data);
			if($result)
			{
			// $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Sample CueSheet downloaded successfully!</div>');
			}else
			{				
				$msg = '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>something wrong in Cue Sheet</div>';
				$this->session->set_flashdata('feedback',$msg);
			}
	}

			
  }
