<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delivery_partner_mapping extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Delivery_partner_mapping_model','dpm_model');
    }
 /*---------Delivery_partner_mapping submodule functionalities----------*/

    public function index()
    {
    	$search_by = $this->input->post('search_by');
    	$search_value = $this->input->post('table_search');
		$dpartners = $this->dpm_model->getDelPartners();		
		$data_post['dpartners'] = $dpartners;
		$brands = $this->dpm_model->get_paid_stores($search_by,$search_value);
		$data_post['brands'] = $brands;
		$sel_dpids = $this->dpm_model->get_alldpmIds();		
		$mapped_ids = array();
		foreach($sel_dpids as $value)
		{
			$ids_array = explode(',',$value['dp_id']);
			$mapped_ids[$value['brand_id']] = $ids_array; 
		}
		$data_post['mapped_ids'] = $mapped_ids;
		
		$brand_id = $this->input->post("dpsave");			
		$dp_array = $this->input->post("dp");
		
		if($brand_id!='' && !empty($dp_array))
		{	
			if(@$dp_array[$brand_id])
			{ 
				$dpids = $dp_array[$brand_id];
				$dpids_str = implode(',',$dpids);	
			}else
			{
				$dpids_str = 0;		
			}			
			
			$result_id = $this->dpm_model->dp_save($dpids_str,$brand_id);
			//echo $result_id;
			if($result_id)
			{
				$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Success</div>');
				redirect('/Delivery_partner_mapping');
			}else
			{
				 /*$this->session->set_flashdata('feedback', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5> </div>');
				 redirect('/Delivery_partner_mapping','refresh');*/
			}
		}

		$this->load->view('common/header');
		$this->load->view('delivery_process/delivery_partner_mapping',$data_post);
		$this->load->view('common/footer');

	}

}
