<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clearcache extends MY_Controller {
	
	function __construct(){
        parent::__construct();
        $this->load->library('curl');
        
    }  

	public function index()
	{ 
		$msg = '';
		$this->session->set_flashdata('feedback','');
		if($this->has_rights(67) == 1){
			if(!empty($_POST)){
				if($this->curl->simple_post(LIVE_SITE.'Sccache/clear_home_page_filter_type_all', false, array(CURLOPT_USERAGENT => true))){ }else{ $msg = $msg. "<br>Clear Home Page Filter - all".'<br>'; }
				if($this->curl->simple_post(LIVE_SITE.'Sccache/clear_home_page_filter_type_men', false, array(CURLOPT_USERAGENT => true))){ }else{ $msg = $msg. "Clear Home Page Filter - men".'<br>'; }
				if($this->curl->simple_post(LIVE_SITE.'Sccache/clear_home_page_filter_type_women', false, array(CURLOPT_USERAGENT => true))){ }else{ $msg = $msg. "Clear Home Page Filter - women".'<br>'; }
				if($this->curl->simple_post(LIVE_SITE.'Sccache/clear_category_list', false, array(CURLOPT_USERAGENT => true))){ }else{ $msg = $msg. "Clear Category Cache".'<br>'; }
				if($this->curl->simple_post(LIVE_SITE.'Sccache/clear_brand_list', false, array(CURLOPT_USERAGENT => true))){ }else{ $msg = $msg. "Clear Brand List Cache".'<br>'; }
				if($this->curl->simple_post(LIVE_SITE.'Sccache/clear_brand_listing_page', false, array(CURLOPT_USERAGENT => true))){ }else{ $msg = $msg. "Clear Brand Page".'<br>'; }
				if($this->curl->simple_post(LIVE_SITE.'Sccache/clear_sc_about_us', false, array(CURLOPT_USERAGENT => true))){ }else{ $msg = $msg. "Clear About Us cache".'<br>'; }
				if($this->curl->simple_post(LIVE_SITE.'Sccache/clear_get_megamenu_mens', false, array(CURLOPT_USERAGENT => true))){ }else{ $msg = $msg. "Clear Megamenu -Men cache".'<br>'; }
				if($this->curl->simple_post(LIVE_SITE.'Sccache/clear_get_megamenu_men_html', false, array(CURLOPT_USERAGENT => true))){ }else{ $msg = $msg. "Clear Megamenu -Men HTML cache".'<br>'; }
				if($this->curl->simple_post(LIVE_SITE.'Sccache/clear_get_megamenu_womens', false, array(CURLOPT_USERAGENT => true))){ }else{ $msg = $msg. "Clear Megamenu -Women cache".'<br>'; }
				if($this->curl->simple_post(LIVE_SITE.'Sccache/clear_get_megamenu_women_html', false, array(CURLOPT_USERAGENT => true))){ }else{ $msg = $msg. "Clear Megamenu Women HTML cache".'<br>'; }
				if($this->curl->simple_post(LIVE_SITE.'Sccache/clear_look_women', false, array(CURLOPT_USERAGENT => true))){ }else{ $msg = $msg. "Clear Look cache - Women".'<br>'; }
				if($this->curl->simple_post(LIVE_SITE.'Sccache/clear_look_men', false, array(CURLOPT_USERAGENT => true))){ }else{ $msg = $msg. "Clear Look cache - Men".'<br>'; }

				$this->session->set_flashdata('feedback', '<div class="alert alert-success"><button aria-hidden="true" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Cache Clear Successfully'.$msg.'</div>');
			}

			$this->load->view('common/header');
			$this->load->view('clearcache');
			$this->load->view('common/footer');

		}else{

			$this->load->view('common/header');
            $this->load->view('not_permission');
            $this->load->view('common/footer');
		}
	}
}
?>