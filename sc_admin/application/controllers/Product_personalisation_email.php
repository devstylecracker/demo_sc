<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_personalisation_email extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
        parent::__construct();
        $this->load->model('product_personalisation_model');
        $this->load->model('productmanagement_model');
    }

    public function index()
	{
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{
			if($this->has_rights(70) == 1)
			{	
				$data=array();				
				$this->load->view('common/header');
				$this->load->view('product_personalisation/product_personalisation_email',$data);
				$this->load->view('common/footer');
			}else{
				$this->load->view('common/header');
        		$this->load->view('not_permission');
        		$this->load->view('common/footer');
			}
		}
	}

	function show_gender_specific_categories(){
		$data=array();
		$gender = $this->input->post('gender');
		if($gender == 1){ $option_name = 'women_upselling_cat'; }
        if($gender == 3){ $option_name = 'men_upselling_cat'; }
		$data['save_cat']=$this->product_personalisation_model->get_options($option_name);
		$data['category']=$this->product_personalisation_model->show_gender_specific_categories($gender);
		echo json_encode($data);
	}

	function save_data(){
		$gender = $this->input->post('gender');
		$category = $this->input->post('category');
		$data['category']=$this->product_personalisation_model->save_data_logic($gender,$category);
	}

	function show_results(){
		$order_id = $this->input->post('order_id');
		echo $this->product_personalisation_model->show_results($order_id);
	}

	function show_comp_results()
	{
		$order_id = $this->input->post('order_id');
		$order_prdid = $this->input->post('orderprdid');
		$this->product_personalisation_model->show_complementary_results($order_id,$order_prdid);
		$this->sendMail($order_prdid);
	}

	/*function recommendation_sendmail()
	{
		//$apikey = 'MY_API_KEY';
		$apikey = '534c4d6771f8ed3e03122be7c37b0aab-us14';

		$dataCenter = substr($apikey,strpos($apikey,'-')+1);	
		 
		$to_emails = array('sudha@stylecracker.com');
		$to_names = array('You', 'Test');
		 
		$message = array(
		    'html'=>'Yo, this is the <b>html</b> portion',
		    'text'=>'Yo, this is the *text* portion',
		    'subject'=>'This is the subject',
		    'from_name'=>'Me!',
		    'from_email'=>$this->config->item('from_email'),
		    'to_email'=>$to_emails,
		    'to_name'=>$to_names
		);
		 
		$tags = array('WelcomeEmail');
		 
		$params = array(
		    'apikey'=>$apikey,
		    'message'=>$message,
		    'track_opens'=>true,
		    'track_clicks'=>false,
		    'tags'=>$tags
		);
		 
		//$url = "http://us1.sts.mailchimp.com/1.0/SendEmail";
		$url = 'https://' .$dataCenter. '.api.mailchimp.com/3.0/SendEmail';
		 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url.'?'.http_build_query($params));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		 
		$result = curl_exec($ch);
		echo $result;
		curl_close ($ch);
		 
		$data = json_decode($result);
		echo "Status = ".$data->status."\n";		 
	}*/

	function sendMail($order_prdid)
	{

		$data['prd_recom']= $this->product_personalisation_model->show_complementary_results($order_prdid,$order_prdid);
		
		$config['protocol'] = 'smtp';
	  //      $config['mailpath'] = '/usr/sbin/sendmail';
	        $config['charset'] = 'iso-8859-1';
	        $config['mailtype'] = 'html';
	        $config['wordwrap'] = TRUE;
	        $config['smtp_host'] = $this->config->item('smtp_host');
	        $config['smtp_user'] = $this->config->item('smtp_user');
	        $config['smtp_pass'] = $this->config->item('smtp_pass');
	        $config['smtp_port'] = $this->config->item('smtp_port');

	        $this->email->initialize($config);

	        $this->email->from($this->config->item('from_email'), 'StyleCracker');
	        //$this->email->to($data['prd_recom']['user']['email']);
	        $this->email->to('sudha@stylecracker.com');
	        //$this->email->cc(array('arun@stylecracker.com','sudhags147@gmail.com'));
	        $this->email->subject('Love our product? Tell us more!');
	       	
	       	$this->load->view('emailer/specific-product-followup',$data);
	        /*$message = $this->load->view('emailer/specific-product-followup',$data,true);
            $this->email->message($message);

	        $this->email->send();*/
	}

	function sendEmailCron()
	{
		$result = $this->product_personalisation_model->get_deliver_order();
		
		//$this->product_personalisation_model->show_complementary_results($order_prdid,$order_prdid);
		foreach($result as $val)
		{
			$data['prd_recom']= $this->product_personalisation_model->show_complementary_results($val['id'],$val['id']);
		echo $data['prd_recom']['user']['email'];
			//$this->sendMail($val['id']);
		}
		
	}
}
?>