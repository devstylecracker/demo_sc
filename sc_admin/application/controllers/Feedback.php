<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends MY_Controller {    

    function __construct(){
        parent::__construct();
        $this->load->model('Feedback_model');       

    }  
 /*---------Feedback submodule functionalities----------*/

    public function index($offset = null)
    {   
      
        if(!$this->session->userdata('user_id')){

            $this->load->view('login');                    

        }else{ 
					

		/*Add and edit feedback function here*/

		$data_post = array();        
        $loggedInUser=$this->session->userdata('user_id');

				
				if($this->input->post()){
					
					$this->form_validation->set_rules('feedback', 'Feedback', 'required');
										
					if ($this->form_validation->run() == FALSE)
                    {
										
						$this->load->view('common/header');
                        $this->load->view('feedback_add',$data_post);
                        $this->load->view('common/footer');	

					}else
					{
						$feedback = $this->input->post('feedback');
						$username = $this->input->post('username');						
												
						$data_post = array(
						   'feedback'=>$feedback,
						   'username'=>$username,     
						   'created_by'=> $loggedInUser,
						   'created_datetime'=> date("Y-m-d H:i:s"),                                
						   'modified_by'=> $loggedInUser,
						   'modified_datetime'=> date("Y-m-d H:i:s")                                
						);

						$created = $this->Feedback_model->new_feedback($data_post); 

					    if($created){  
					    	
						 	$this->session->set_flashdata('feedback', '<div class="callout callout-success"><h5><i class="icon fa fa-check"></i><strong>Thank you !</strong> </h5> For Your feeback. </div>');						  		
						  		
						  		redirect('Feedback'); 

						}else{    
								
						  	$this->session->set_flashdata('feedback', '<i class="fa fa-times-circle sign"></i><strong>Error!</strong> The server is not responding, try again later.');
						 
						 		redirect ('Feedback'); 

						} 
                                                  				
					}

				}

				$this->load->view('common/header');
				$this->load->view('feedback_add',$data_post);
				$this->load->view('common/footer');	
			
		}	


	}
	
	 /* Feedback listing */	
	public function feedback_list($offset = null)
    {
	   		
			$data_post = array();                       
           if($this->has_rights(57) == 1){	 

			    $search_by = $this->input->post('search_by');
                $table_search = $this->input->post('table_search');                 
			    $paginationUrl = 'Feedback/feedback_list/';
                $uri_segment = 3;
                $config['per_page'] = 20;
                $config['total_rows'] = count($this->Feedback_model->get_all_feedback($search_by,$table_search,'',''));                
                $total_rows = $config['total_rows'];                                                    
                $get_feedback = $this->Feedback_model->get_all_feedback($search_by,$table_search,$offset,$config['per_page']);
                //echo '<pre>'; print_r($get_users); echo '</pre>';
                $count = $config['total_rows']; 
                $data_post['feedback'] = $get_feedback;
                
                $this->customPagination($paginationUrl,$count,$uri_segment,$get_feedback,$total_rows);    
			    $this->load->view('common/header');
				$this->load->view('feedback_view',$data_post);
				$this->load->view('common/footer');
			    

			}else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			}        
		}

	}
