<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand_categories_manage extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Brand_categories_model');
        $this->load->model('Lookcreator_model');
        /*$this->load->model('Brandpa_model');
        $this->load->model('Categories_model');*/

    }

    function index(){    	
    	if($this->has_rights(1) == 1 && $this->session->userdata('user_id')){            
            $data = array();
            $data['category'] = $this->Brand_categories_model->get_all_paid_brands();               
            $data['category_listings'] = $this->Lookcreator_model->catgory_list();    

            

            $this->load->view('common/header');
            $this->load->view('brand_categories_manage',$data);
            $this->load->view('common/footer');
            /*$data = array();

            $data['cat_data'] = $this->Categories_model->get_cat_data();
            $data['brands_data'] = $this->Brandpa_model->get_all_paid_brands();    	
            $data['bucket'] = $this->Categories_model->get_all_buckets();

            $age_id = '8,15';
            $data['age'] = $this->Categories_model->get_question_answers($age_id);

            $budget_id = '7,15';
            $data['budget'] = $this->Categories_model->get_question_answers($budget_id);

            $this->load->view('common/header');
            $this->load->view('brandpa',$data);
            $this->load->view('common/footer');*/

        }else{

            $this->load->view('common/header');
            $this->load->view('not_permission');
            $this->load->view('common/footer');
        }
        
    }

    function save_brands_data(){
        @$category_name = $_REQUEST['category_name'];
        @$sc_category_name = $_REQUEST['sc_category_name'];
        //print_r($_REQUEST);exit;
        if($sc_category_name!="" && $category_name != ""){
            $this->Brand_categories_model->update_category_mapping($sc_category_name, $category_name);
            header("Location: ".base_url()."/Brand_categories_manage?success=1");
            //echo $this->db->last_query();
        }else{
            header("Location: ".base_url()."/Brand_categories_manage?success=0");
        }
        
    }

    /*public function get_brands_data(){
        $id = $this->input->post('id');
        $data = array();
        $data['category_data'] = $this->Brandpa_model->get_brands_category($id,'category');
        $data['bucket_data'] = $this->Categories_model->get_pa_data($id,'brand','bucket');
        $data['age_data'] = $this->Categories_model->get_pa_data($id,'brand','age');
        $data['budget_data'] = $this->Categories_model->get_pa_data($id,'brand','budget');
        echo json_encode($data);
    }*/

    
}
?>