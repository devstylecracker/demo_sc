<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attrsettings extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Attrsettings_model');
    }

    function index($id){
    	if($this->has_rights(1) == 1 && $this->session->userdata('user_id')){
            if($id == ''){ redirect('attributes'); }
    		$data = array();
    		 $is_true = 0;
    		if($this->input->post()){
                $this->form_validation->set_rules('attr_value_name','Attributes Name','required|trim|min_length[2]|max_length[50]');
                
                if($this->form_validation->run() == FALSE){   
                     $is_true = 1;
                }else{ 
                   
                    $is_true = 0;

                }
                if($is_true == 0){
    			     $this->Attrsettings_model->save_attribues($_POST);
                     
                     redirect('Attrsettings/index/'.$_POST['attribute_id']);
                     $_POST = array();
                }else{
                    $data['error'] = "Error : Check data again";
                }
    		} 
            $data['cat_data'] = $this->Attrsettings_model->get_attr_value($this->uri->segment(3));
            $data['parent_attr_name'] = $this->Attrsettings_model->get_attr_info($this->uri->segment(3));
           
			$this->load->view('common/header');
            $this->load->view('attrsettings',$data);
            $this->load->view('common/footer');

        }else{

            $this->load->view('common/header');
            $this->load->view('not_permission');
            $this->load->view('common/footer');
        }
        
    }

    function attrsettings_edit($aid=null,$id=null){
        if($this->has_rights(1) == 1 && $this->session->userdata('user_id')){
            $data = array(); $is_true = 0;
            
            if($this->input->post()){
                if($is_true == 0){
                     $this->Attrsettings_model->edit_attribute($_POST,$id);
                     redirect('Attrsettings/index/'.$aid);
                     $_POST = array();
                }else{
                    $data['error'] = "Error : please check data";
                }

            }

            $data['cat_data'] = $this->Attrsettings_model->get_attr_value($this->uri->segment(3));
            $data['cat_data_data'] = $this->Attrsettings_model->get_attr_value_data($this->uri->segment(4));

            $this->load->view('common/header');
            $this->load->view('attrsettings',$data);
            $this->load->view('common/footer');
         }else{

            $this->load->view('common/header');
            $this->load->view('not_permission');
            $this->load->view('common/footer');
        }
    }

    function check_product_count(){
        if (!$this->input->is_ajax_request()) {
           exit('No direct script access allowed');
        }else{
           $id = $this->input->post('id');
           echo $this->Attrsettings_model->get_attributes($id)[0]['count'];
        }
    }

    function delete_category(){
        if (!$this->input->is_ajax_request()) {
           exit('No direct script access allowed');
        }else{
           $id = $this->input->post('id');
           echo $this->Attrsettings_model->delete_category($id);
        }
    }

    function search_category($aid=null,$id=null){
        if (!$this->input->is_ajax_request()) {
           exit('No direct script access allowed');
        }else{
            $data = array();
            $search_category = @$this->input->post('search_text');
            $data['cat_data'] = $this->Attrsettings_model->get_filtered_category($search_category,$aid);
            
            $this->load->view('attrsettings_list',$data);
        }
    }
    function update_display(){
        if (!$this->input->is_ajax_request()) {
           exit('No direct script access allowed');
        }else{
            $display_val = $this->input->post('display_val');
            $attr_val = $this->input->post('attr_val');
            $data['cat_data'] = $this->Attrsettings_model->update_display($attr_val,$display_val);
        }
    }
}
?>