<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Megamenu extends MY_Controller {
	
	function __construct(){
       parent::__construct();
       $this->load->model('Megamenu_model'); 
       $this->load->model('Homepage_settings_model'); 
       $this->load->model('Categories_model');
       $this->load->model('Lookcreator_model'); 
       $this->load->library('curl'); 
       $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));   
	} 
   
	public function index()
	{ 
		$data['mm_women_html'] = $this->Megamenu_model->get_megamenu_women_html();  
		$data['mm_image'] = SC_SITE_URL.'assets/images/menu/thumb/megamenu_img_women.jpeg';
		
		if(!empty($_FILES))
		{
			$uploaddir = '../assets/images/menu/';		
			$file_type = @$_FILES['file']['type'];
			$image_type = explode('/',$file_type);
			$image_name = 'megamenu_img_women.'.@$image_type[1];
			//$uploadfile = $uploaddir . basename(@$_FILES['file']['name']);
			$uploadfile = $uploaddir . $image_name;
			//$data['mm_image'] = '../assets/images/menu/'.@$image_name; 			

			$image_info = getimagesize(@$_FILES["file"]["tmp_name"]);
			/*$image_width = 310;
			$image_height = 180;*/	
			$image_width = 356;
			$image_height = 140;	

			if (($image_info[0]>$image_width) && ($image_info[1]>$image_height)){ 
		        $message = 'Upload Image of proper Dimension 356 X 140.'; 
		        echo '<script type="text/javascript">alert("'.$message.'");</script>';

		    }else if (($file_type != "image/jpeg") && ($file_type != "image/jpg") )
		    {
		        $message = 'Invalid file type. Only JPEG types are accepted.'; 
		        echo '<script type="text/javascript">alert("'.$message.'");</script>';         
		    }    
		    else 
		    {
		    	if (move_uploaded_file(@$_FILES['file']['tmp_name'], $uploadfile)) {

						$config['image_library'] = 'gd2';
						$config['source_image'] = '../assets/images/menu/'.$image_name;
						$config['new_image'] = '../assets/images/menu/thumb/';
						$config['create_thumb'] = TRUE;
						$config['thumb_marker'] = '';
						$config['maintain_ratio'] = TRUE;
						$config['width'] = 356;
						$config['height'] = 140;

						$this->image_lib->initialize($config);

						if (!$this->image_lib->resize())
						{	$this->image_lib->display_errors();
							$ok =0;
						}else{
							$ok =1;}  

						     $message = 'Image Uploaded Successfully'; 
		        echo '<script type="text/javascript">alert("'.$message.'");</script>';
						}else{
						    $message = 'Error Uploaded Image'; 
		        echo '<script type="text/javascript">alert("'.$message.'");</script>';
						}	
    		}			
		}	

		$this->load->view('common/header');
		$this->load->view('webui/megamenu_women_backend',$data);
		$this->load->view('common/footer');
	}
	

	public function men()
	{ 
		$data['mm_men_html'] = $this->Megamenu_model->get_megamenu_men_html();  
		$data['mm_image'] = SC_SITE_URL.'assets/images/menu/thumb/megamenu_img_men.jpeg';			
		
		if(!empty($_FILES))
		{			
			$uploaddir = '../assets/images/menu/';		
			$file_type = @$_FILES['file']['type'];
			$image_type = explode('/',$file_type);
			/*$image_name = 'megamenu_img_men'.$type.'.'.@$image_type[1];*/
			$image_name = 'megamenu_img_men.'.@$image_type[1];
			//$uploadfile = $uploaddir . basename(@$_FILES['file']['name']);
			$uploadfile = $uploaddir . $image_name;
			//$data['mm_image'] = '../assets/images/menu/'.@$image_name; 	

			$image_info = getimagesize(@$_FILES["file"]["tmp_name"]);
			$image_width = 356;
			$image_height = 140;		

			if (($image_info[0]>$image_width) && ($image_info[1]>$image_height)){  							
		        $message = 'Upload Image of proper Dimension 356 X 140.'; 
		        echo '<script type="text/javascript">alert("'.$message.'");</script>';
		        
		    }else if (($file_type != "image/jpeg") && ($file_type != "image/jpg") )
		    {
		        $message = 'Invalid file type. Only JPEG types are accepted.'; 
		        echo '<script type="text/javascript">alert("'.$message.'");</script>';         
		    }    
		    else 
		    {
		    	if (move_uploaded_file(@$_FILES['file']['tmp_name'], $uploadfile)) {
		    		$config['image_library'] = 'gd2';
					$config['source_image'] = '../assets/images/menu/'.$image_name;
					$config['new_image'] = '../assets/images/menu/thumb/';
					$config['create_thumb'] = TRUE;
					$config['thumb_marker'] = '';
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 356;
					$config['height'] = 140;

					$this->image_lib->initialize($config);

					if (!$this->image_lib->resize())
					{	$this->image_lib->display_errors();
						$ok =0;
					}else{
						$ok =1;}  

				$message = 'Image Uploaded Successfully'; 
		        echo '<script type="text/javascript">alert("'.$message.'");</script>';
						}else{
						    $message = 'Error Uploaded Image'; 
		        echo '<script type="text/javascript">alert("'.$message.'");</script>';
						}	
    		}			
		}
		
		$this->load->view('common/header');
		$this->load->view('webui/megamenu_men_backend',$data);
		$this->load->view('common/footer');
	}

	public function get_variation_type(){
			if(WOMEN_CATEGORY_ID!='')
			{
				$women_cat_id = WOMEN_CATEGORY_ID;
			}else
			{
				$women_cat_id = '';
			}
			$id= $this->input->post('id');
			if($id==1)
			{ 
				if ( $this->cache->get('filter_type') !== $id ) {
				  $list = $this->Megamenu_model->get_all_variationtype($id,$women_cat_id);
		          $filter_type1 = $id;
		       	  $this->cache->save('variationType', $list, 500000);
		          $this->cache->save('filter_type', $filter_type1, 500000);
				}
				if($this->cache->get('variationType')){
			        $list = $this->cache->get('variationType');
			      }
			}
			else 
			{ 
				if ( $this->cache->get('filter_type') !== $id ) {
				  $list = $this->Megamenu_model->get_variationvalue($id,$women_cat_id);
		          $filter_type1 = $id;
		       	  $this->cache->save('variationValue', $list, 500000);
		          $this->cache->save('filter_type', $filter_type1, 500000);
				}
				if($this->cache->get('variationValue')){
			        $list = $this->cache->get('variationValue');
			      }
			}
			// $list = $this->Megamenu_model->get_variationvalue($id,$women_cat_id);
			$producthtml = '';
			$producthtml = $producthtml.'<label for="" class="control-label">Select </label>';
			$producthtml = $producthtml.'<select class="form-control chosen-select variations">
              <option>
              </option>';
			foreach($list as $row){
				$producthtml = $producthtml.'<option value="'.$row['id'].'" slug="'.$row['slug'].'" label="'.$row['name'].'"> '.$row['name'].'-'.$row['id'].'
              </option>';
			}
			$producthtml = $producthtml.'</select>';
			
			echo $producthtml;
	}
	
	public function get_variation_type_men(){
			if(MEN_CATEGORY_ID!='')
			{
				$men_cat_id = MEN_CATEGORY_ID;
			}else
			{
				$men_cat_id = '';
			}
			$id= $this->input->post('id');
			if($id==1)
			{ 
				if ( $this->cache->get('filter_type') !== $id ) {
				  $list = $this->Megamenu_model->get_all_variationtype($id,$men_cat_id);
		          $filter_type1 = $id;
		       	  $this->cache->save('variationType', $list, 500000);
		          $this->cache->save('filter_type', $filter_type1, 500000);
				}
				if($this->cache->get('variationType')){
			        $list = $this->cache->get('variationType');
			      }
			}
			else 
			{ 
				if ( $this->cache->get('filter_type') !== $id ) {
				  $list = $this->Megamenu_model->get_variationvalue($id,$men_cat_id);
		          $filter_type1 = $id;
		       	  $this->cache->save('variationValue', $list, 500000);
		          $this->cache->save('filter_type', $filter_type1, 500000);
				}
				if($this->cache->get('variationValue')){
			        $list = $this->cache->get('variationValue');
			      }
			}
			// $list = $this->Megamenu_model->get_variationvalue($id,$men_cat_id);
			$producthtml = '';
			$producthtml = $producthtml.'<label for="" class="control-label">Select </label>';
			$producthtml = $producthtml.'<select class="form-control chosen-select variations">
              <option>
              </option>';
			foreach($list as $row){
				$producthtml = $producthtml.'<option value="'.$row['id'].'" slug="'.$row['slug'].'" label="'.$row['name'].'"> '.$row['name'].'-'.$row['id'].'
              </option>';
			}
			$producthtml = $producthtml.'</select>';
			
			echo $producthtml;
	}
	
	
	
	public function get_categories_list_mens(){
		$this->get_categories_list(3);
	}
	public function get_categories_list_womens(){
		$this->get_categories_list(1);
	}
	public function get_categories_list($gender = 0){
			if(MEN_CATEGORY_ID!='')
			{
				$men_cat_id = MEN_CATEGORY_ID;
			}else
			{
				$men_cat_id = '';
			}
			$id= $this->input->post('id');
				
//echo "<pre>";
//			$result = $this->Categories_model->get_all();
			if($gender == 1)
				$result = $this->Lookcreator_model->get_women_categories();
			else if($gender == 3)
				$result = $this->Lookcreator_model->get_men_categories();
			else
				$result = $this->Lookcreator_model->catgory_list();
			//print_r($this->db->last_query());
			$producthtml = '<label for="" class="control-label">Categories </label><select class="form-control chosen-select variations">
              <option>
              </option>';
			foreach($result as $row){
				$producthtml .= $row;
			}
			$producthtml = $producthtml.'</select>';
			
			print_r($producthtml);
			exit;

			$result1 = $this->Categories_model->get_all_parent();
			foreach ($result1 as $value) {
				$category_list[] = $value['id'];
			}
			//print_r($category_list );
			/*exit;*/
			foreach($category_list as $roww=>$value){
				foreach($result as $row){
					if($value == $row['category_parent']){

						$data_all[$row['category_parent']][$row['id']]['id'] = $row['id'];
						$data_all[$row['category_parent']][$row['id']]['category_name'] = $row['category_name'];
						$data_all[$row['category_parent']][$row['id']]['category_slug'] = $row['category_slug'];
						$data_all[$row['category_parent']][$row['id']]['category_parent'] = $row['category_parent'];
					}
				}
			}
			//PRINT_R($data_all);//EXIT;
			$i = 0;
			foreach($data_all[-1] as $row1){
				echo $row1['category_name']."</br>";
				if(isset($data_all[$row1['id']])){
					foreach($data_all[$row1['id']] as $row2){
						echo "&nbsp".$row2['category_name']."</br>";
						if(isset($data_all[$row2['id']])){
							foreach($data_all[$row2['id']] as $row3){
								echo "&nbsp&nbsp".$row3['category_name']."</br>";
							}
						}
					}
				}
			}
			
		//	print_r($data_all);
			exit;
	





			// $list = $this->Megamenu_model->get_variationvalue($id,$men_cat_id);
			$producthtml = '';
			$producthtml = $producthtml.'<label for="" class="control-label">Select </label>';
			$producthtml = $producthtml.'<select class="form-control chosen-select variations">
              <option>
              </option>';
			foreach($list as $row){
				$producthtml = $producthtml.'<option value="'.$row['id'].'" slug="'.$row['slug'].'" label="'.$row['name'].'"> '.$row['name'].'-'.$row['id'].'
              </option>';
			}
			$producthtml = $producthtml.'</select>';
			



			echo $producthtml;
	}
	
	public function save_megamenu_data()
	{
		$megamenu_array = json_decode($this->input->post('megamenu_array'),1);
		$megamenuWomen_html = trim($this->input->post('megamenu_html'));
		$type = $this->input->post('menutype');			
		if($type=='men')
		{
			$option_name_arr = "mega_menu_men";
			$option_name_html = "mega_menu_men_html";
		}else
		{
			$option_name_arr = "mega_menu_women";
			$option_name_html = "mega_menu_women_html";
		}
		$result1 = $this->Megamenu_model->save_MMcategory($megamenu_array,$option_name_arr);
		$result2 = $this->Megamenu_model->saveMegamenuHtml($megamenuWomen_html,$option_name_html);
		if($type=='men')
		{
			$result = $this->curl->simple_post(LIVE_SITE.'Sccache/clear_get_megamenu_mens', false, array(CURLOPT_USERAGENT => true));
			$result = $this->curl->simple_post(LIVE_SITE.'Sccache/clear_get_megamenu_men_html', false, array(CURLOPT_USERAGENT => true));
		}else
		{
			$result = $this->curl->simple_post(LIVE_SITE.'Sccache/clear_get_megamenu_womens', false, array(CURLOPT_USERAGENT => true));
			$result = $this->curl->simple_post(LIVE_SITE.'Sccache/clear_get_megamenu_women_html', false, array(CURLOPT_USERAGENT => true));
		}
		return 'success';
	}
	
	public function get_category_slug_url($category_slug)
	{
		echo $category_slug_url = $this->Megamenu_model->get_category_chailds_parents($category_slug);
	}
}
