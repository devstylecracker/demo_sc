<?php
header('Content-type: text/html; charset=utf-8');
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand_setting extends MY_Controller {

	function __construct(){
       parent::__construct();
       $this->load->model('Brand_setting_model');
		$this->load->library('upload');	   
		$this->load->helper(array('form', 'url'));
		// $this->load->library('bitly');
       }

	public function index()
	{	
		if($this->input->post()){
			// echo "<pre>";print_r($_FILES);exit;
			$this->form_validation->set_rules('date_from','Start Date','required');
			$this->form_validation->set_rules('date_to','End Date','required');
			$banner = '';
			$submit = $this->input->post('submit');
			$row_id = $this->input->post('row_id');
			$line1 = $this->input->post('line1'.$submit);
			// echo $line1;exit;
			$line2 = $this->input->post('line2'.$submit);
			$button_text = $this->input->post('button_text'.$submit);
			$url = $this->input->post('url'.$submit);
			$status = $this->input->post('status'.$submit);
			$fromdate = $this->input->post('date_from'.$submit);
			$todate = $this->input->post('date_to'.$submit);
			$order = $this->input->post('order'.$submit);
			$banner_image = $this->input->post('file'.$submit);
			if($status == 'active'){ $new_status = 0; } else $new_status = 1;
			// $is_available = $this->Brand_setting_model->check_availability($fromdate,$todate,$order);
			// if($is_available > 0){
				// $error = array('We have another banner on that day please change Date or position of banner');
				// $data['date_error'] = $error; 
				// $this->load->view('common/header');
				// $this->load->view('webui/brand_event_based_banners',$data);
				// $this->load->view('common/footer');
			// }
				$msg = "";
				$config['upload_path'] = 'assets/brand_banners/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size']	= '130';
				$config['max_width']  = '1500';
				$config['max_height']  = '1500';
				$config['file_name'] = time();
				$ok =1;
				if($_FILES['file'.$submit]['name']){
					$this->upload->initialize($config);
					if(!($this->upload->do_upload('file'.$submit))){
							$data['error'] = array($this->upload->display_errors()); 
							$this->load->view('common/header',$data);
							$this->load->view('webui/brand_banners');
							$this->load->view('common/footer');
							$ok =0;
						}else{
						 $banner= $this->upload->data('file_name'); 
						 $test2 = getimagesize('assets/brand_banners/' . $banner);
							$width = $test2[0];
							$height = $test2[1];
							if ($width != 1366 && $height != 371){
								$error = array('Image Height And Width Should Be 1366 X 371');
								$data['error'] = $error; 
								$this->load->view('common/header',$data);
								$this->load->view('webui/brand_banners');
								$this->load->view('common/footer');
								$ok =0; 
							}
						}
					}
					$is_available = 0;
					if($ok == 1 && $is_available == 0){
					$result = $this->Brand_setting_model->update_default_banner($submit,$line1,$line2,$button_text,$url,$new_status,$fromdate,$todate,$banner,$order);
					// echo $this->db->last_query();exit;
					redirect('/webui/Brand_setting');
					}
			
				
				
		}// code for updating image and extra things
		else {
			$data['data'] = $this->Brand_setting_model->get_all();
			$this->load->view('common/header');
			$this->load->view('webui/brand_banners',$data);
			$this->load->view('common/footer');
		}
	}
	
	public function get_event_based_sliders(){	
		if($this->input->post()){
			// echo "<pre>";print_r($_POST);exit;
			$this->form_validation->set_rules('date_from','Start Date','required');
			$this->form_validation->set_rules('date_to','End Date','required');
			$banner = '';
			$submit = $this->input->post('submit');
			$row_id = $this->input->post('row_id');
			$line1 = $this->input->post('line1'.$submit);
			// echo $line1;exit;
			$line2 = $this->input->post('line2'.$submit);
			$button_text = $this->input->post('button_text'.$submit);
			$url = $this->input->post('url'.$submit);
			$status = $this->input->post('status'.$submit);
			$fromdate = $this->input->post('date_from'.$submit);
			$todate = $this->input->post('date_to'.$submit);
			$order = $this->input->post('order'.$submit);
			$banner_image = $this->input->post('file'.$submit);
			if($status == 'active'){ $new_status = 0; } else $new_status = 1;
			// $is_available = $this->Brand_setting_model->check_availability($fromdate,$todate,$order);
			// if($is_available > 0){
				// $error = array('We have another banner on that day please change Date or position of banner');
				// $data['date_error'] = $error; 
				// $this->load->view('common/header');
				// $this->load->view('webui/brand_event_based_banners',$data);
				// $this->load->view('common/footer');
			// }
				$msg = "";
				$config['upload_path'] = 'assets/brand_banners/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size']	= '130';
				$config['max_width']  = '1500';
				$config['max_height']  = '1500';
				$config['file_name'] = time();
				$ok =1;
				if(!empty($_FILES)){
				if(@$_FILES['file'.$submit]['name']!='' && @$_FILES['file'.$submit]['error']!=4){
					$this->upload->initialize($config);
					if(!($this->upload->do_upload('file'.$submit))){
							$data['error'] = array($this->upload->display_errors()); 
							$this->load->view('common/header',$data);
							$this->load->view('webui/brand_event_based_banners');
							$this->load->view('common/footer');
							$ok =0;
						}else{
						 $banner= $this->upload->data('file_name'); 
						 $test2 = getimagesize('assets/brand_banners/' . $banner);
							$width = $test2[0];
							$height = $test2[1];
							if ($width != 1366 && $height != 371){
								$error = array('Image Height And Width Should Be 1366 X 371');
								$data['error'] = $error; 
								$this->load->view('common/header',$data);
								$this->load->view('webui/brand_event_based_banners');
								$this->load->view('common/footer');
								$ok =0; 
							}
						}
					}
				}
					$is_available = 0;
						// echo "banner".$banner;echo "new_status".$new_status;exit;
					if($ok == 1 && $is_available == 0){
					$result = $this->Brand_setting_model->update_banner_data($submit,$line1,$line2,$button_text,$url,$new_status,$fromdate,$todate,$banner,$order);
					// echo $this->db->last_query();exit;
					redirect('/webui/Brand_setting/get_event_based_sliders');
					}
			
				
				
		}// code for updating image and extra things
		else {
			$data['data'] = $this->Brand_setting_model->get_event_based_sliders();
			$data['active_banners'] = $this->Brand_setting_model->get_active_banners();
			$data['upcomming_banners'] = $this->Brand_setting_model->get_upcomming_banners();
			// echo "<pre>";print_r($data['upcomming_banners']);exit;
			$this->load->view('common/header');
			$this->load->view('webui/brand_event_based_banners',$data);
			$this->load->view('common/footer');
		}//end of else 
	
	}
	
	public function add(){
		if(!$this->session->userdata('user_id'))
		{
			$this->load->view('login');

		}else{
			
			if($this->input->post()){
			$this->form_validation->set_rules('date_from','Start Date','required');
			$this->form_validation->set_rules('date_to','End Date','required');
			$banner = '';
			if ($this->form_validation->run() == FALSE){
						$this->load->view('common/header');
						$this->load->view('webui/add_banners');
						$this->load->view('common/footer');
						
			}else{
			// echo "comming";echo $this->form_validation->run();exit;
			$line1 = $this->input->post('line1');
			$line2 = $this->input->post('line2');
			$button_text = $this->input->post('button_text');
			$url = $this->input->post('url');
			$status = $this->input->post('status');
			$fromdate = $this->input->post('date_from');
			$todate = $this->input->post('date_to');
			$order = $this->input->post('order');
			$banner_image = $this->input->post('file');
			$is_available = $this->Brand_setting_model->check_availability($fromdate,$todate,$order);
			if($is_available > 0){
				$error = array('We have another banner on that day please change Date or position of banner');
				$data['date_error'] = $error; 
				$this->load->view('common/header');
				$this->load->view('webui/add_banners',$data);
				$this->load->view('common/footer');
			}
				$msg = "";
				$config['upload_path'] = 'assets/brand_banners/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size']	= '130';
				$config['max_width']  = '1500';
				$config['max_height']  = '1500';
				$config['file_name'] = time();
				$ok =1;
				if($_FILES['file']['name']){
					$this->upload->initialize($config);
					if(!($this->upload->do_upload('file'))){
							$data['error'] = array($this->upload->display_errors()); 
							$this->load->view('common/header',$data);
							$this->load->view('webui/add_brand_banner');
							$this->load->view('common/footer');
							$ok =0;
						}else{
						 $banner= $this->upload->data('file_name'); 
						 $test2 = getimagesize('assets/brand_banners/' . $banner);
							$width = $test2[0];
							$height = $test2[1];
							if ($width != 1366 && $height != 371){
								$error = array('Image Height And Width Should Be 1366 X 371');
								$data['error'] = $error; 
								$this->load->view('common/header',$data);
								$this->load->view('webui/add_brand_banner');
								$this->load->view('common/footer');
								$ok =0; 
							}
						}
					}
					if($ok == 1 && $is_available == 0){
					$result = $this->Brand_setting_model->save_banner($line1,$line2,$button_text,$url,$status,$fromdate,$todate,$banner,$order);
					redirect('/webui/Brand_setting/get_event_based_sliders');
					}
			
				
			}	
		}else {
				$this->load->view('common/header');
				$this->load->view('webui/add_brand_banner');
				$this->load->view('common/footer');
		}	
	}
	}
	
	public function remove_banner(){
		$id = $this->input->post('id');
		$is_deleted = $this->Brand_setting_model->remove_banner($id);
		
		return $is_deleted;
	}
	
	public function update_banner(){
		// print_r($_POST);exit;
		$banner = '';
		$id = $this->input->post('id');
		$line1 = $this->input->post('line1');
		$line2 = $this->input->post('line2');
		$button_text = $this->input->post('button_text');
		$url = $this->input->post('url');
		$status = $this->input->post('status');
		$fromdate = $this->input->post('date_from');
		$todate = $this->input->post('date_to');
		$order = $this->input->post('order');
		$file = $this->input->post('file');
		$msg = "";
		$config['upload_path'] = 'assets/brand_banners/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '130';
		$config['max_width']  = '1500';
		$config['max_height']  = '1500';
		$config['file_name'] = time();
		$ok =1;
		// print_r($_FILES['file']);exit;
		if($_FILES['file']['name']){
		
			$this->upload->initialize($config);
			if(!($this->upload->do_upload('file'))){
					$data['error'] = array($this->upload->display_errors()); 
					$this->load->view('common/header',$data);
					$this->load->view('webui/add_brand_banner');
					$this->load->view('common/footer');
					$ok =0;
				}else{
				 $banner= $this->upload->data('file_name'); 
				 $test2 = getimagesize('assets/brand_banners/' . $banner);
					$width = $test2[0];
					$height = $test2[1];
					if ($width != 1366 && $height != 371){
						$error = array('Image Height And Width Should Be 1366 X 371');
						$data['error'] = $error; 
						$this->load->view('common/header',$data);
						$this->load->view('webui/add_brand_banner');
						$this->load->view('common/footer');
						$ok =0; 
					}
				}
			}
		if($status == 'active'){ $new_status = 1; } else $new_status = 0;
		// echo $id."---cap1--".$line1."----cap 2-".$line2."-button tet----".$button_text."--url---".$url."---ststus--".$new_status."--fromdate---".$fromdate."---todate--".$todate."-----".$order;exit;
		//$result = $this->Brand_setting_model->save_banner($line1,$line2,$button_text,$url,$status,$fromdate,$todate,$banner_image);
		$result = $this->Brand_setting_model->update_banner_data($id,$line1,$line2,$button_text,$url,$new_status,$fromdate,$todate,$order);
		}
		
	
}