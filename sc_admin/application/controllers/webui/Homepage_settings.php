<?php
header('Content-type: text/html; charset=utf-8');
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage_settings extends MY_Controller {

	function __construct(){
       parent::__construct();
       		$this->load->model('Homepage_settings_model');
       		$this->load->library('curl');
       }

	public function top_looks()
	{	
		$data = array();
		$data['top_looks_all'] = $this->Homepage_settings_model->get_options('top_looks_all');
		$data['top_looks_men'] = $this->Homepage_settings_model->get_options('top_looks_men');
		$data['top_looks_women'] = $this->Homepage_settings_model->get_options('top_looks_women');
		$this->load->view('common/header');
		$this->load->view('webui/homepage_look_settings',$data);
		$this->load->view('common/footer');
	}
	
	public function save_top_look(){
		$type = $this->input->post('type');
		$look_id_one = $this->input->post('look_id_one');
		$look_id_two = $this->input->post('look_id_two');
		$look_id_three = $this->input->post('look_id_three');

		$look_array = array($look_id_one,$look_id_two,$look_id_three);
		$result = $this->curl->simple_post(LIVE_SITE.'Sccache/clear_home_page_filter_type_'.$type, false, array(CURLOPT_USERAGENT => true));
		echo $this->Homepage_settings_model->look_exist($look_array,$type);
	}

	public function get_look_img(){
		$lookId = $this->input->post('lookId');
		$look_data = $this->Homepage_settings_model->get_look_img($lookId);
		if(!empty($look_data)){
			if($look_data['0']['look_type']== 2 || $look_data['0']['look_type']== 4) {
				echo '<div class="product-img-wrp"><div class="col-md-4"><div class="form-group"><img src="'.$this->config->item('street_style_look').''.$look_data['0']['look_image'].'" style="width:50%"></div></div></div>';
			}else if($look_data['0']['look_type']== 3){
				echo '<div class="product-img-wrp"><div class="col-md-4"><div class="form-group"><img src="'.base_url().'assets/products/thumb/'.$look_data['0']['product_img'].'" style="width:50%"></div></div></div>';
			}else{
				echo '<div class="product-img-wrp"><div class="col-md-4"><div class="form-group"><img src="'.$this->config->item('stylist_look').''.$look_data['0']['image'].'" style="width:50%"></div></div></div>';
			}
		}
	}

	/*public function top_category(){
		$data=array(); $all_cat= array();
		$data['top_cat_all'] = $this->Homepage_settings_model->get_options('top_cat_all');
		$data['top_cat_men'] = $this->Homepage_settings_model->get_options('top_cat_men');
		$data['top_cat_women'] = $this->Homepage_settings_model->get_options('top_cat_women');

		$all_cat = $this->Homepage_settings_model->get_options('sc_view_all_cat');
		
		$data['category'] = $this->Homepage_settings_model->get_category(1,$all_cat['cat']);
		$data['subcategory'] = $this->Homepage_settings_model->get_category(2,$all_cat['sub_cat']);
		$data['variationtypes'] = $this->Homepage_settings_model->get_category(3,$all_cat['var_type']);
		$data['variationvalues'] = $this->Homepage_settings_model->get_category(4,$all_cat['var_values']);

		$this->load->view('common/header');
		$this->load->view('webui/homepage_category_settings',$data);
		$this->load->view('common/footer');
	}*/

	public function top_category(){

		$data=array(); $all_cat= array();

		$data['top_cat_all'] = $this->Homepage_settings_model->get_options('top_cat_all');

		$data['top_cat_men'] = $this->Homepage_settings_model->get_options('top_cat_men');

		$data['top_cat_women'] = $this->Homepage_settings_model->get_options('top_cat_women');



		$all_cat = $this->Homepage_settings_model->get_options('sc_view_all_cat');

		

		$data['category'] = $this->Homepage_settings_model->get_category(1,$all_cat['cat']);

		$data['subcategory'] = $this->Homepage_settings_model->get_category(2,$all_cat['sub_cat']);

		$data['variationtypes'] = $this->Homepage_settings_model->get_category(1,$all_cat['cat']);

		$data['variationvalues'] = $this->Homepage_settings_model->get_category(4,$all_cat['var_values']);



		$this->load->view('common/header');

		$this->load->view('webui/homepage_category_settings',$data);

		$this->load->view('common/footer');

	}

	public function get_category(){
		$cat_id = $this->input->post('cat_id');
		$preselected = $this->input->post('preselected');
		
		echo $cat_data = $this->Homepage_settings_model->get_category($cat_id,$preselected);

	}

		public function save_top_cat(){

		$type = $this->input->post('type');

		$cat_id_one = $this->input->post('cat_id_one');

		$cat_id_two = $this->input->post('cat_id_two');

		$cat_id_three = $this->input->post('cat_id_three');

		$cat_one_select = $this->input->post('cat_one_select');

		$cat_two_select = $this->input->post('cat_two_select');

		$cat_three_select = $this->input->post('cat_three_select');

		$error = 0;
		$error_msg = '';

		/*if($cat_one_select == 1){ 
			if(!file_exists('../assets/images/products_category/category/'.$cat_id_one.'/'.$cat_id_one.'.jpg'))
				$error = 1;
               $error_msg =  json_encode('Category image is missing id :- '.$cat_id_one); 
		}else if($cat_one_select == 2){
			if(!file_exists('../assets/images/products_category/sub_category/'.$cat_id_one.'/'.$cat_id_one.'.jpg'))
				$error = 1;
               $error_msg =  json_encode('Sub Category image is missing id :- '.$cat_id_one);  
		}else if($cat_one_select == 3){
			if(!file_exists('../assets/images/products_category/variation_types/'.$cat_id_one.'/'.$cat_id_one.'.jpg'))
				$error = 1;
               $error_msg =  json_encode('Variation type image is missing id :- '.$cat_id_one); 
		}else if($cat_one_select == 4){
			if(!file_exists('../assets/images/products_category/variation_values/'.$cat_id_one.'/'.$cat_id_one.'.jpg'))
				$error = 1;
               $error_msg =  json_encode('Variation value image is missing id :- '.$cat_id_one); 
		}
		if($error == 0 ){
		if($cat_two_select == 1){ 
			if(!file_exists('../assets/images/products_category/category/'.$cat_id_two.'/'.$cat_id_two.'.jpg'))
				$error = 1;
               $error_msg =  json_encode('Category image is missing id :- '.$cat_id_two); 
		}else if($cat_two_select == 2){
			if(!file_exists('../assets/images/products_category/sub_category/'.$cat_id_two.'/'.$cat_id_two.'.jpg'))
				$error = 1;
               $error_msg =  json_encode('Sub Category image is missing id :- '.$cat_id_two);  
		}else if($cat_two_select == 3){
			if(!file_exists('../assets/images/products_category/variation_types/'.$cat_id_two.'/'.$cat_id_two.'.jpg'))
				$error = 1;
               $error_msg =  json_encode('Variation type image is missing id :- '.$cat_id_two); 
		}else if($cat_two_select == 4){
			if(!file_exists('../assets/images/products_category/variation_values/'.$cat_id_two.'/'.$cat_id_two.'.jpg'))
				$error = 1;
               $error_msg =  json_encode('Variation value image is missing id :- '.$cat_id_two); 
		} }
		if($error == 0 ){
		if($cat_three_select == 1){ 
			if(!file_exists('../assets/images/products_category/category/'.$cat_id_three.'/'.$cat_id_three.'.jpg'))
				$error = 1;
               $error_msg =  json_encode('Category image is missing id :- '.$cat_id_three); 
		}else if($cat_three_select == 2){
			if(!file_exists('../assets/images/products_category/sub_category/'.$cat_id_three.'/'.$cat_id_three.'.jpg'))
				$error = 1;
               $error_msg =  json_encode('Sub Category image is missing id :- '.$cat_id_three);  
		}else if($cat_three_select == 3){
			if(!file_exists('../assets/images/products_category/variation_types/'.$cat_id_three.'/'.$cat_id_three.'.jpg'))
				$error = 1;
               $error_msg =  json_encode('Variation type image is missing id :- '.$cat_id_three); 
		}else if($cat_three_select == 4){
			if(!file_exists('../assets/images/products_category/variation_values/'.$cat_id_three.'/'.$cat_id_three.'.jpg'))
				$error = 1;
               $error_msg =  json_encode('Variation value image is missing id :- '.$cat_id_three); 
		}
		}*/
		if($error == 0){

		$result = $this->curl->simple_post(LIVE_SITE.'Sccache/clear_home_page_filter_type_'.$type, false, array(CURLOPT_USERAGENT => true));
		//$cat_array = array($cat_one_select.'|'.$cat_id_one,$cat_two_select.'|'.$cat_id_two,$cat_three_select.'|'.$cat_id_three);

		$cat_array = array(array('type'=>$cat_one_select,'cat'=>$cat_id_one),array('type'=>$cat_two_select,'cat'=>$cat_id_two),array('type'=>$cat_three_select,'cat'=>$cat_id_three));

		$error_msg =  $this->Homepage_settings_model->cat_save($cat_array,$type);
		}
		echo $error_msg;
	}


	/*public function save_top_cat(){
		$type = $this->input->post('type');
		$cat_id_one = $this->input->post('cat_id_one');
		$cat_id_two = $this->input->post('cat_id_two');
		$cat_id_three = $this->input->post('cat_id_three');
		$cat_one_select = $this->input->post('cat_one_select');
		$cat_two_select = $this->input->post('cat_two_select');
		$cat_three_select = $this->input->post('cat_three_select');

		$result = $this->curl->simple_post(LIVE_SITE.'Sccache/clear_home_page_filter_type_'.$type, false, array(CURLOPT_USERAGENT => true));
		//$cat_array = array($cat_one_select.'|'.$cat_id_one,$cat_two_select.'|'.$cat_id_two,$cat_three_select.'|'.$cat_id_three);
		$cat_array = array(array('type'=>$cat_one_select,'cat'=>$cat_id_one),array('type'=>$cat_two_select,'cat'=>$cat_id_two),array('type'=>$cat_three_select,'cat'=>$cat_id_three));
		echo $this->Homepage_settings_model->cat_save($cat_array,$type);
	}*/

	public function save_view_cat(){
		$cat_id = $this->input->post('cat_id');
		$sub_cat_id = $this->input->post('sub_cat_id');
		$variation_type = $this->input->post('variation_type');
		$variation_values = $this->input->post('variation_values');

		$all_cat = array();
		$all_cat['cat'] =  $cat_id;
		$all_cat['sub_cat'] =  $sub_cat_id;
		$all_cat['var_type'] =  $variation_type;
		$all_cat['var_values'] =  $variation_values;
		$result = $this->curl->simple_post(LIVE_SITE.'Sccache/clear_category_list', false, array(CURLOPT_USERAGENT => true));
		$this->Homepage_settings_model->save_view_cat($all_cat);
	}

	public function top_brands(){
		$all_brand = $this->Homepage_settings_model->get_options('trending_brand_all');
		$data['all_brand_one_id'] = $this->Homepage_settings_model->get_paid_brands($all_brand[0]);
		$data['all_brand_two_id'] = $this->Homepage_settings_model->get_paid_brands($all_brand[1]);
		$data['all_brand_three_id'] = $this->Homepage_settings_model->get_paid_brands($all_brand[2]);

		$all_brand = $this->Homepage_settings_model->get_options('trending_brand_men');
		$data['men_brand_one_id'] = $this->Homepage_settings_model->get_paid_brands($all_brand[0]);
		$data['men_brand_two_id'] = $this->Homepage_settings_model->get_paid_brands($all_brand[1]);
		$data['men_brand_three_id'] = $this->Homepage_settings_model->get_paid_brands($all_brand[2]);

		$all_brand = $this->Homepage_settings_model->get_options('trending_brand_women');
		$data['women_brand_one_id'] = $this->Homepage_settings_model->get_paid_brands($all_brand[0]);
		$data['women_brand_two_id'] = $this->Homepage_settings_model->get_paid_brands($all_brand[1]);
		$data['women_brand_three_id'] = $this->Homepage_settings_model->get_paid_brands($all_brand[2]);

		$all_brand = $this->Homepage_settings_model->get_options('sc_top_brands');
		$data['paid_brand_list'] = $this->Homepage_settings_model->get_paid_brands($all_brand);

		

		$this->load->view('common/header');
		$this->load->view('webui/homepage_brands_settings',$data);
		$this->load->view('common/footer');
	}

	public function save_trending_brands(){
		$type = $this->input->post('type');
		$brand_id_one = $this->input->post('brand_id_one');
		$brand_id_two = $this->input->post('brand_id_two');
		$brand_id_three = $this->input->post('brand_id_three');

		$brand_array = array($brand_id_one,$brand_id_two,$brand_id_three);
		$result = $this->curl->simple_post(LIVE_SITE.'Sccache/clear_home_page_filter_type_'.$type, false, array(CURLOPT_USERAGENT => true));
		echo $this->Homepage_settings_model->save_trending_brands($brand_array,$type);
	}

	public function save_top_brands(){
		$type = $this->input->post('type');
		$top_brand_list = $this->input->post('top_brand_list');
		
		echo $this->Homepage_settings_model->save_top_brand($top_brand_list);
	}

	public function megamenu_category()
	{
		$data = array();
		$result ='';
		
		$category = $this->input->post('category');
		$mmcategory = $this->input->post('mmcategory');
				
		/*Womens Wear-1,Men-12*/
		$data['category'] = array(WOMEN_CATEGORY_ID=>'Women',MEN_CATEGORY_ID=>'Men');
		$option = '';
		if((!empty($data['category'])) && $category!='')
		{
			foreach($data['category'] as $key=>$val)
			{	
				if($category == WOMEN_CATEGORY_ID)
				{
					$option = "mega_menu_women";
				}else if($category == MEN_CATEGORY_ID)
				{
					$option = "mega_menu_men";
				}		
								
			}
		}		
		
		if(!empty($this->input->post('mmcategory')))
		{		
			$result = $this->Homepage_settings_model->save_MMcategory($mmcategory,$option);
		}

		if($result)
		{
			$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> MegaMenu category Updated successfully!</div>');         	
			
		}	

		$this->load->view('common/header');
		$this->load->view('webui/megamenu_setting',$data);
		$this->load->view('common/footer');
	}

	public function category_display()
	{
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}else{			
			$data = array();
			$html_cat = '';
			$category = $this->input->post('category');
			
			$data['category'] = array(WOMEN_CATEGORY_ID=>'Women',MEN_CATEGORY_ID=>'Men');
			$option = '';
			$subcategorysel =array();
			$dbmenu = array();
			$variationSel =array();
			$subcategorysel =array();
			if((!empty($data['category'])) && $category!='')
			{
				foreach($data['category'] as $key=>$val)
				{			
					if($category == WOMEN_CATEGORY_ID)
					{
						$option = "mega_menu_women";
					}else if($category == MEN_CATEGORY_ID)
					{
						$option = "mega_menu_men";
					}				
				}
			}			
			
			$variation = $this->Homepage_settings_model->get_categoryLevel($category);
			if($option!='')
			{
				$dbmenu = $this->Homepage_settings_model->get_options($option);
				if(!empty($dbmenu))
				{
					foreach($dbmenu as $key => $val)
					{
						$subcategorysel[] = $key;
						if(!empty($val))
						{
							foreach($val as $k => $v)
							{
								$variationSel[] = $k;
								if(isset($v))
								{
									foreach($v as $k1 => $v1)
									{
										$varValSel[] = $v1;
									}
								}							
							}
						}						
					}
				}				
			}
			
			
			$variationSelected = array();
			if($variation!='')
			{
				$new_subcat = '';
				$old_subcat = '';
				$new_vartype = '';
				$old_vartype = '';

				foreach($variation as $val)
				{					
					$new_vartype = $val['vartype_id'];					

					if($new_vartype != $old_vartype)
					{	
						if(in_array($val['vartype_id'], $variationSel)){		
						$html_cat = $html_cat.'</div><div class="form-group" ><label style="color:blue;">&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="checkbox" name="mmcategory['.$category.']['.$val['vartype_id'].']" id="vartype" value="'.$val['vartype_id'].'" checked>'.$val['variation_type'].'</label><br>';	
						}else
						{
							$html_cat = $html_cat.'</div><div class="form-group" ><label style="color:blue;">&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="checkbox" name="mmcategory['.$category.']['.$val['vartype_id'].']" id="vartype" value="'.$val['vartype_id'].'">'.$val['variation_type'].'</label><br>';
						}
					}

					if(!empty($varValSel)){
					if(in_array($val['id'], $varValSel)){

						$html_cat = $html_cat.'<label class="radio-inline"><input type="checkbox" name="mmcategory['.$category.']['.$val['vartype_id'].']['.$val['id'].']" value="'.$val['id'].'" checked>'.$val['variation_value'].'</label>';
							}else{
						$html_cat = $html_cat.'<label class="radio-inline"><input type="checkbox" name="mmcategory['.$category.']['.$val['vartype_id'].']['.$val['id'].']" value="'.$val['id'].'">'.$val['variation_value'].'</label>';
							}
						}else{
							$html_cat = $html_cat.'<label class="radio-inline"><input type="checkbox" name="mmcategory['.$category.']['.$val['vartype_id'].']['.$val['id'].']" value="'.$val['id'].'">'.$val['variation_value'].'</label>';
					}
					$old_subcat = $new_subcat;
					$old_vartype = $new_vartype;
				}
			}
			$html_cat = $html_cat.'<div class="box-footer">
            <button class="btn btn-primary" type="submit">Submit</button>
          </div>'; 
		}
		echo $html_cat;
		
	}
	
}
?>