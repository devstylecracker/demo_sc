<?php
header('Content-type: text/html; charset=utf-8');
defined('BASEPATH') OR exit('No direct script access allowed');

class Filtermanagement extends MY_Controller {

	function __construct(){
       parent::__construct();
       		$this->load->model('Filtermanagement_model');
       }

	public function index()
	{
		$data = array();
		$this->load->view('common/header');
		$this->load->view('webui/filtermanagement',$data);
		$this->load->view('common/footer');
	}

	public function get_data(){
		$select_type = $this->input->post('select_type');
		$table_name = '';
		if($select_type==1){ $table_name = 'product_variation_type'; }
		else if($select_type==2){ $table_name = 'product_variation_value'; }
		if($select_type!=0 && $table_name!=''){
			$options_html = '';
			$res = $this->Filtermanagement_model->get_data($table_name);
			if(!empty($res)){
				foreach($res as $val){
					if($val['show_in_filter'] == 1){
						$options_html = $options_html .'<option value="'.$val['id'].'" selected>'.$val['name'].'-'.$val['id'].'</option>';
					}else{
						$options_html = $options_html .'<option value="'.$val['id'].'">'.$val['name'].'-'.$val['id'].'</option>';
					}
				}
			}
			echo $options_html;
		}else{
			echo '';
		}
	}

	public function save_type(){
		$select_type = $this->input->post('select_type');
		$show_in_filters = $this->input->post('show_in_filters');
		$table_name = '';
		if($select_type==1){ $table_name = 'product_variation_type'; }
		else if($select_type==2){ $table_name = 'product_variation_value'; }
		if($select_type!=0 && $table_name!=''){
			$res = $this->Filtermanagement_model->save_data($table_name,$show_in_filters);
		}
	}
}
?>