<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Popups extends MY_Controller {
	
	function __construct(){
              parent::__construct();
              $this->load->model('Popups_model');
	} 
   
	public function index()
	{ 
              $data = array();
              $is_true = 0;
              if($this->input->post()){
                $this->form_validation->set_rules('popup_name','Popup Name','required|trim|min_length[2]|max_length[50]');
                $this->form_validation->set_rules('start_datetime','Start Datetime','required|trim');
                $this->form_validation->set_rules('end_datetime','End Datetime','required|trim');
                
                
                if($this->form_validation->run() == FALSE){   
                    
                }else{ 
                    if(@$_FILES['desktop_img']['name']!=''){
                       $ext = pathinfo($_FILES['desktop_img']['name'], PATHINFO_EXTENSION);
                        $image_info = getimagesize($_FILES["desktop_img"]["tmp_name"]);
                        $image_width = $image_info[0]; 
                        $image_height = $image_info[1];
                        //if($ext!='jpg' || $image_width!=530 || $image_height!=530){
                        if($ext!='jpg' || $image_width!=1366 || $image_height!=140){
                            $is_true = 1;
                            $data['desks_error'] = "Check desktop image format or dimension";
                       }
                    }

                    if(@$_FILES['mobile_img']['name']!=''){
                       $ext = pathinfo($_FILES['mobile_img']['name'], PATHINFO_EXTENSION);
                        $image_info = getimagesize($_FILES["mobile_img"]["tmp_name"]);
                        $image_width = $image_info[0]; 
                        $image_height = $image_info[1];
                        //if($ext!='jpg' || $image_width!=530 || $image_height!=530){
                        if($ext!='jpg' || $image_width!=360 || $image_height!=70){
                            $is_true = 1;
                            $data['mobile_error'] = "Check mobile image format or dimension";
                       }
                    }
                }

                if($is_true == 0){
                     $this->Popups_model->save_popup($_POST);
                     $_POST = array();
                }

              }

              $data['get_popup'] = $this->Popups_model->get_popup();
              $this->load->view('common/header');
              $this->load->view('webui/popups',$data);
              $this->load->view('common/footer');
       }

       function popups_edit($id=null){
           if($this->has_rights(1) == 1 && $this->session->userdata('user_id')){
            $data = array(); $is_true = 0;
            
            if($this->input->post()){

                if(@$_FILES['desktop_img']['name']!=''){
                       $ext = pathinfo($_FILES['desktop_img']['name'], PATHINFO_EXTENSION);
                        $image_info = getimagesize($_FILES["desktop_img"]["tmp_name"]);
                        $image_width = $image_info[0]; 
                        $image_height = $image_info[1];
                        //if($ext!='jpg' || $image_width!=530 || $image_height!=530){
                        if($ext!='jpg'){
                            $is_true = 1;
                            $data['desks_error'] = "Check desktop image format or dimension";
                       }
                }

                if(@$_FILES['mobile_img']['name']!=''){
                       $ext = pathinfo($_FILES['mobile_img']['name'], PATHINFO_EXTENSION);
                        $image_info = getimagesize($_FILES["mobile_img"]["tmp_name"]);
                        $image_width = $image_info[0]; 
                        $image_height = $image_info[1];
                        //if($ext!='jpg' || $image_width!=530 || $image_height!=530){
                        if($ext!='jpg'){
                            $is_true = 1;
                            $data['mobile_error'] = "Check mobile image format or dimension";
                       }
                }

                if($is_true == 0){
                     $this->Popups_model->edit_popup($_POST,$id);
                     $_POST = array();
                }

            }

            $data['cat_data_data'] = $this->Popups_model->get_popup_data($id);
            $data['get_popup'] = $this->Popups_model->get_popup();

            $this->load->view('common/header');
            $this->load->view('webui/popups',$data);
            $this->load->view('common/footer');
         }else{

            $this->load->view('common/header');
            $this->load->view('not_permission');
            $this->load->view('common/footer');
        }
       }

       function delete_popups(){
          if (!$this->input->is_ajax_request()) {
              exit('No direct script access allowed');
          }else{
              $id = $this->input->post('id');
              echo $this->Popups_model->delete_popups($id);
          }
       }
}
?>