<?php
header('Content-type: text/html; charset=utf-8');
defined('BASEPATH') OR exit('No direct script access allowed');

class Banners extends MY_Controller {

	function __construct(){
       parent::__construct();
       $this->load->model('Sc_home_slider_model');
		$this->load->library('upload');	   
		$this->load->helper(array('form', 'url'));
		// $this->load->library('bitly');
       }

	public function index()
	{	
		$data['data'] = $this->Sc_home_slider_model->get_all();
		 // echo "<pre>";print_r($data);exit;
		$this->load->view('common/header');
		$this->load->view('webui/banners',$data);
		$this->load->view('common/footer');
	}
	
	public function get_event_based_sliders(){	
	
		$data['data'] = $this->Sc_home_slider_model->get_event_based_sliders();
		$data['active_banners'] = $this->Sc_home_slider_model->get_active_banners();
		$data['upcomming_banners'] = $this->Sc_home_slider_model->get_upcomming_banners();
		// echo "<pre>";print_r($data['upcomming_banners']);exit;
		$this->load->view('common/header');
		$this->load->view('webui/event_based_banners',$data);
		$this->load->view('common/footer');
	}
	
	public function add(){
		if(!$this->session->userdata('user_id'))
		{
			$this->load->view('login');

		}else{
			
			if($this->input->post()){
			// $this->form_validation->set_rules('line1','Sub Category','required');
			// $this->form_validation->set_rules('line2','Price Range','required');
			// $this->form_validation->set_rules('url','Banner Url','required');
			$this->form_validation->set_rules('date_from','Start Date','required');
			$this->form_validation->set_rules('date_to','End Date','required');
			$banner = '';$mobile_banner= '';
			if ($this->form_validation->run() == FALSE){
				$this->load->view('common/header');
				$this->load->view('webui/add_banners');
				$this->load->view('common/footer');
						
			}else{
			// echo "comming";echo $this->form_validation->run();exit;
				$line1 = $this->input->post('line1');
				$line2 = $this->input->post('line2');
				$button_text = $this->input->post('button_text');
				$url = $this->input->post('url');
				$status = $this->input->post('status');
				$fromdate = $this->input->post('date_from');
				$todate = $this->input->post('date_to');
				$order = $this->input->post('order');
				$banner_image = $this->input->post('file');
				$mobile_banner_image = $this->input->post('mobile_img');
				$is_available = $this->Sc_home_slider_model->check_availability($fromdate,$todate,$order);
				
				if($is_available > 0){
					echo $this->db->last_query();
					$error = array('We have another banner on that day please change Date or position of banner');
					$data['date_error'] = $error; 
					$this->load->view('common/header');
					$this->load->view('webui/add_banners',$data);
					$this->load->view('common/footer');
				}
				$msg = "";
				$config['upload_path'] = 'assets/banners/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size']	= '130';
				$config['max_width']  = '1500';
				$config['max_height']  = '1500';
				$config['file_name'] = time();
				$ok = 0;
				if($_FILES['file']['name']){
					$this->upload->initialize($config);
					if(!($this->upload->do_upload('file'))){
						$data['error'] = array($this->upload->display_errors()); 
						$this->load->view('common/header',$data);
						$this->load->view('webui/add_banners');
						$this->load->view('common/footer');
						$ok =0;
					}else{
						$banner= $this->upload->data('file_name'); 
						$test2 = getimagesize('assets/banners/' . $banner);
						$width = $test2[0];
						$height = $test2[1];
						if ($width != 1366 && $height != 370){
							$error = array('Web banner Image Height And Width Should Be 1366 X 371');
							$data['error'] = $error; 
							$this->load->view('common/header',$data);
							$this->load->view('webui/add_banners');
							$this->load->view('common/footer');
							$ok =0; 
						}else{
							$ok =1; 
						}
					}

				}
				
				$config2['upload_path'] = 'assets/banners/mobile_banner/';
				$config2['allowed_types'] = 'gif|jpg|png|jpeg';
				$config2['max_size']	= '130';
				$config2['max_width']  = '1500';
				$config2['max_height']  = '1500';
				$config2['file_name'] = $banner;
				
				if($_FILES['mobile_img']['name']){
					$this->upload->initialize($config2);
					if(!($this->upload->do_upload('mobile_img'))){
						$data['error'] = array($this->upload->display_errors()); 
						$this->load->view('common/header',$data);
						$this->load->view('webui/add_banners');
						$this->load->view('common/footer');
						$ok =0;
					}else{
						$mobile_banner= $this->upload->data('file_name'); 
						$test2 = getimagesize('assets/banners/mobile_banner/' . $mobile_banner);
						$width = $test2[0];
						$height = $test2[1];
						if ($width != 375 && $height != 200){
							$error = array('Mobile banner Image Height And Width Should Be 375 X 200');
							$data['error'] = $error; 
							$this->load->view('common/header',$data);
							$this->load->view('webui/add_banners');
							$this->load->view('common/footer');
							$ok =0; 
						}else{
							$ok =1; 
						}
					}

				}
				
				
				if($ok == 1 && $is_available == 0 && $banner != '' && $mobile_banner != '' ){
					// echo $mobile_banner."vsdv".$banner;exit;
					$result = $this->Sc_home_slider_model->save_banner($line1,$line2,$button_text,$url,$status,$fromdate,$todate,$banner,$order);
					redirect('/webui/Banners/get_event_based_sliders');
				}else{
					if($banner =='' || $mobile_banner ==''){
						$error = array('Please upload both the images.');
						$data['error'] = $error; 
						$this->load->view('common/header',$data);
						$this->load->view('webui/add_banners');
						$this->load->view('common/footer');
					}
				}
		
				
			}	
		}else {
				$this->load->view('common/header');
				$this->load->view('webui/add_banners');
				$this->load->view('common/footer');
		}	
	}
	}
	
	public function remove_banner(){
		$id = $this->input->post('id');
		$is_deleted = $this->Sc_home_slider_model->remove_banner($id);
		return $is_deleted;
	}
	
	public function update_banner(){
		// print_r($_POST);exit;
		$banner = '';
		$id = $this->input->post('id');
		$line1 = $this->input->post('line1');
		$line2 = $this->input->post('line2');
		$button_text = $this->input->post('button_text');
		$url = $this->input->post('url');
		$status = $this->input->post('status');
		$fromdate = $this->input->post('date_from');
		$todate = $this->input->post('date_to');
		$order = $this->input->post('order');
		if($status == 'active'){ $new_status = 1; } else $new_status = 0;
		// echo $id."---cap1--".$line1."----cap 2-".$line2."-button tet----".$button_text."--url---".$url."---ststus--".$new_status."--fromdate---".$fromdate."---todate--".$todate."-----".$order;exit;
		//$result = $this->Sc_home_slider_model->save_banner($line1,$line2,$button_text,$url,$status,$fromdate,$todate,$banner_image);
		$result = $this->Sc_home_slider_model->update_banner_data($id,$line1,$line2,$button_text,$url,$new_status,$fromdate,$todate,$order);
		}
		
	
}
