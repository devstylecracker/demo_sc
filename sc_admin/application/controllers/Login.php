<?php
//error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
    {
        // Call the Model constructor
        
        parent::__construct();
        $this->load->library('bcrypt');
        $this->load->model('login_model');
        
        
    }
    
	public function index()
	{
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{ 
			$this->load->view('common/header');
			$this->load->view('home');
			$this->load->view('common/footer');
		}
	}
	
	public function check_login(){ 
			
			$this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
		
			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('login');
			}
			else
			{
				 $email = $this->input->post('email');
				 $password = $this->input->post('password');
				 $remember_me = $this->input->post('remember_me');

				 if($remember_me == 1){

				 	setcookie('7572ce8a49ff8982ba4d17f9e56b6d1f',$this->encryptOrDecrypt($email,'encrypt'), time() + (86400 * 30), "/");
				 	setcookie('a6c16340e402e946c64ce53bb41ac24a',$this->encryptOrDecrypt($password,'encrypt'), time() + (86400 * 30), "/");
				 	
				 }

				 $res = $this->login_model->login_user($email,$password); 

				 if(trim($res) == '1'){ 
				 		// setcookie('7572ce8a49ff8982ba4d17f9e56b6d1f',$this->encryptOrDecrypt($email,'encrypt'), time() + (86400 * 30), "/");
				 		// setcookie('a6c16340e402e946c64ce53bb41ac24a',$this->encryptOrDecrypt($password,'encrypt'), time() + (86400 * 30), "/");
						redirect('/home');
				 		// header('Location: https://www.stylecracker.com/sc_admin/home');
  					// 	die();
				 } else{
						$data['error'] = "Invalid Username and Password";
						$this->load->view('login',$data); 
				 }
				 
			}

		
	}
	
	public function logout(){
		session_destroy();
		delete_cookie("7572ce8a49ff8982ba4d17f9e56b6d1f");
		delete_cookie("a6c16340e402e946c64ce53bb41ac24a");
		redirect('/home');
		
		}
}
