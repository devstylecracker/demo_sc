<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_cron extends MY_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('Product_cron_model');    

	} 

	public function get_product_data()
	{ 
		$product_added = $this->Product_cron_model->product_import_cron($type='add');
		$product_updated = $this->Product_cron_model->product_import_cron($type='update');
		
		$prd_added_message = " Product Added Data </br></br> ";$total_added = 0;
		if(!empty($product_added)){
			$prd_added_message = $prd_added_message.'<table border="1"><tr> <th>Brand Name</th> <th>Brand ID</th> <th>Product Added</th> </tr>';
			foreach($product_added as $val){
				$prd_added_message = $prd_added_message."<tr><td>".$val['company_name']."</td><td>".$val['brand_id']."</td><td>".$val['product_added']."</td></tr>";
				$total_added = $total_added + $val['product_added'];
			}
			$prd_added_message = $prd_added_message."<tr><td></td><td>Total Count</td><td>".$total_added."</td></tr></table>";
		}else {
			$prd_added_message = $prd_added_message.' No product Added.</br> ';
		}
		
		$prd_updated_message = "</br></br> Product Updated Data</br></br>";$total_updated = 0;
		if(!empty($product_updated)){
			$prd_updated_message = $prd_updated_message.'<table border="1" ><tr> <th>Brand Name</th> <th>Brand ID</th> <th>Product Updated</th> </tr>';
			foreach($product_updated as $val){
				$prd_updated_message = $prd_updated_message."<tr><td>".$val['company_name']."</td><td>".$val['brand_id']."</td><td>".$val['product_updated']."</td></tr>";
				$total_updated = $total_updated + $val['product_updated'];
			}
			$prd_updated_message = $prd_updated_message."<tr><td></td><td>Total Count</td><td>".$total_updated."</td></tr></table>";
		}else {
			$prd_updated_message = $prd_updated_message.' No product Updated.</br> ';
		}
		$message = $prd_added_message.$prd_updated_message;
		echo $message;
		echo $this->send_email($message,'Product');
	}
	
	function send_email($message,$type){
		
		$config['protocol'] = 'smtp';
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;
		$config['smtp_host'] = $this->config->item('smtp_host');
		$config['smtp_user'] = $this->config->item('smtp_user');
		$config['smtp_pass'] = $this->config->item('smtp_pass');
		$config['smtp_port'] = $this->config->item('smtp_port');
		$prdate = date('Y-m-d' ,strtotime(date('Y-m-d 00:00:00') . ' -1 day'));
		$email = 'arun@stylecracker.com'; 	
		$email_id2 = 'sudha@stylecracker.com';
		$this->email->initialize($config);

		$this->email->from($this->config->item('from_email'), 'Stylecracker');
		$this->email->to($email);
		$this->email->bcc($email_id2);
		$this->email->subject(''.$type.' Info of '.$prdate.' ');
		$this->email->message("Hi Arun,</br></br>".$message." ");

		if($this->email->send())
		{
			return "Email Sent";
		}else
		{
			return "Email Error";
		}
	}
	
	public function get_user_data()
	{ 
		$user_added = $this->Product_cron_model->user_data();
		
		$user_added_message = " User Added Data </br></br> ";$total_added = 0;
		
		if(!empty($user_added)){
			
			$user_added_message = $user_added_message.'<table border="1"><tr> <th>User ID</th> <th>Bucket ID</th> <th>Email</th> <th>Registered From</th> </tr>';
			foreach($user_added as $val){
				$user_added_message = $user_added_message."<tr><td>".$val['id']."</td><td>".$val['bucket']."</td><td>".$val['email']."</td><td>".$val['registered_from']."</td></tr>";
				$total_added++;
			}
			$user_added_message = $user_added_message."<tr><td></td><td>Total Count</td><td>".$total_added."</td></tr></table>";
			
		}else {
			
			$user_added_message = $user_added_message.' No product Added.</br> ';
			
		}
		echo $user_added_message;
		echo $this->send_email($user_added_message,'User');
	}
	
	public function get_cart_data(){ 
	
		$data = $this->Product_cron_model->get_cart_data();
		$message = " Cart Data </br></br> ";$total_added = 0;
		
		if(!empty($data)){
			
			$message = $message.'<table border="1"><tr> <th>Cart ID</th><th>User ID</th> <th>Product name</th> <th>Amount</th> <th>Brand name</th> <th>Platform</th></tr>';
			foreach($data as $val){
				$message = $message."<tr><td>".$val['id']."</td><td>".$val['user_id']."</td><td>".$val['name']."</td><td>".$val['price']."</td><td>".$val['company_name']."</td><td>".$val['platform_info']."</td></tr>";
				$total_added++;
			}
			$message = $message."<tr><td></td><td>Total Count</td><td>".$total_added."</td></tr></table>";
			
		}else {
			
			$message = $message.' No product Added.</br> ';
			
		}
		echo $message;
		echo $this->send_email($message,'Cart');
		
	}
	
	public function get_order_data(){ 
	
		$data = $this->Product_cron_model->get_order_data();
		$message = " Order Data </br></br> ";$total_added = 0;
		
		if(!empty($data)){
			
			$message = $message.'<table border="1"><tr> <th>Order ID</th><th>User ID</th> <th>order_display_no</th> <th>platform_info</th> <th>order_total </th></tr>';
			foreach($data as $val){
				$message = $message."<tr><td>".$val['id']."</td><td>".$val['user_id']."</td><td>".$val['order_display_no']."</td><td>".$val['platform_info']."</td><td>".$val['order_total']."</td></tr>";
				$total_added = $total_added + $val['order_total'];
			}
			$message = $message."<tr><td></td><td>Total Count</td><td></td><td></td><td>".$total_added."</td></tr></table>";
			
		}else {
			
			$message = $message.' No product Added.</br> ';
			
		}
		echo $message;
		echo $this->send_email($message,'Order');
		
	}


	function look_delete_cron()
	{
		$data = $this->Product_cron_model->getdeletelook();
		echo $this->send_email($data,'Look Delete');
	}

	public function remove_collection(){
		$data = $this->Product_cron_model->remove_collection();
	}
	
	public function order_next_box_trigger(){
		$days = ' -21 day';
		$data = $this->Product_cron_model->get_box_order_data($days);
		// echo "<pre>";print_r($data);exit;
		foreach($data as $val){
			$subject = 'We miss you!';
			$message = 'Dear '.$val['first_name'].',<br/><br/>I hope you’re doing well. It’s been awhile that we had been in touch and I just wanted to say hi and see how you and your new clothes and accessories were doing! As you know we’re always adding fun and exciting brands and trends to the StyleCracker Box to make the experience even more perfect for you.<br/><br/>If you enjoyed your first experience, this is a great time to place your next order for the StyleCracker Box, because who doesn’t love a box of goodies arriving at their doorstep?<br/><br/>You can order via our website http://thescbox.com/ or reach out to me directly at styling@stylecracker.com so we can figure out exactly what your wardrobe needs.<br/><br/>Thanks for shopping with us and have a great day!';
			echo $send_email = $this->send_email_user($subject,$message,$val['email_id']);exit;
		}
	}
	
	public function feedback_email_trigger(){
		$days = ' -21 day';
		$data = $this->Product_cron_model->get_box_order_data($days);
		// echo "<pre>";print_r($data);exit;
		foreach($data as $val){
			$val['email_id'] = 'rajesh@stylecracker.com';
			$subject = 'We hope you loved your SCBox!';
			$message = 'Dear '.$val['first_name'].',<br/><br/>I\'d like to thank you for shopping with StyleCracker and experiencing the StyleCracker Box. Being one of our newest offerings, we would love to get your feedback and hear about what you loved and maybe didn’t love. All you need to do is click on the link below and answer a few quick and easy questions (we promise it won’t take long!).<br/><br/>If you want to get in touch directly you can always reach me at styling@stylecracker.com<br/><br/> you for shopping with us and have a great day!';
			echo $send_email = $this->send_email_user($subject,$message,$val['email_id']);
		}
	}
	
	function send_email_user($subject,$message,$email){
		
		$config['protocol'] = 'smtp';
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;
		$config['smtp_host'] = $this->config->item('smtp_host');
		$config['smtp_user'] = $this->config->item('smtp_user');
		$config['smtp_pass'] = $this->config->item('smtp_pass');
		$config['smtp_port'] = $this->config->item('smtp_port');
		$this->email->initialize($config);

		$this->email->from($this->config->item('from_email'), 'Stylecracker');
		$this->email->to($email);
		$this->email->subject($subject);
		$this->email->message($message);

		if($this->email->send())
		{
			return "Email Sent";
		}else
		{
			return "Email Error";
		}
	}
}