<?php
ini_set('max_execution_time', 0);
ini_set('memory_limit','512M');
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Manage extends MY_Controller {

    function __construct()
    {
    	 parent::__construct();   
        $this->load->library('excel');
        $this->load->model('product_upload_model');
        $this->load->model('productnew_model');
        $this->load->model('attribute_model');
       //$this->load->model('product_model');
        $this->load->model('category_model');
        $this->load->model('categories_model');
        $this->load->model('store_model');  
        $this->load->model('brandusers_model');
        $this->load->model('tag_model');    
        $this->load->model('SCProductinventory_model');
        $this->load->model('Manage_que_sheet_model');
	}

	function index(){
		if(!$this->session->userdata('user_id'))
		{
			$this->load->view('login');
		}else
		{ 
			 if($this->has_rights(39) == 1)
			{	
				$data_post = array();

				if(isset($_FILES['product_upload'])){
					$data_post['excelmsg'] = $this->validate_cue_sheet();
				}

				$cat = $this->categories_model->get_all_categories();

				$data_post['category'] = $cat;

				$women_cat = $this->category_model->get_women_categories();				
				$data_post['women_category'] = $women_cat;

				$men_cat = $this->category_model->get_men_categories();				
				$data_post['men_category'] = $men_cat;

				$unisex_cat = $this->category_model->get_unisex_categories();					
				$data_post['unisex_category'] = $unisex_cat;

				$data_post['brand_data'] = $this->store_model->all_brands();		

				//$data_post['brand_store_data'] = $this->store_model->all_brand_store();				

				$data_post['tag_data'] = $this->tag_model->get_all_tag('','','','');

			 	$data_post['all_stores'] = $this->productnew_model->all_store();
			 	//echo '<pre>';print_r($data_post['all_stores']);exit;
			 	$data_post['attribute'] = $this->attribute_model->get_parent_attributes();

			 	$data_post['attribute_child'] = $this->attribute_model->get_chaild_attributes();

				$this->load->view('common/header');
				$this->load->view('product/que_sheet_upload',$data_post);
				$this->load->view('common/footer');

			}else
			{
				$this->load->view('common/header');
                $this->load->view('not_permission');
                $this->load->view('common/footer');
			}			

		}
	}

	function getdata($csvFile){
		ini_set('auto_detect_line_endings',TRUE);
	    $file_handle = fopen($csvFile, 'r');
	    while (!feof($file_handle) ) {
	    	//print_r(fgetcsv($file_handle, 0, ',', '"', '"'));
	    	$line_of_text[] = fgetcsv($file_handle, 0, ',', '"', '"');
	    }
	    fclose($file_handle);
	    return $line_of_text;
	}

	function validate_cue_sheet(){
		$csv_array = $this->getdata((($_FILES["product_upload"]["tmp_name"])));		
		$err = array();
		$i = 1;
		$ids1 = array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14);
		$ids = array(1,2,3,4,5,7,11,12,14);//Compulsory field ids
		//$ids = array(1,2,3,4,5,7,12,14);//Compulsory field ids
		foreach ($csv_array as $row) {
			# code...
			if($row != $csv_array[0] && !empty($row)){
				$err[] = $this->error_log_send($row, $ids, $csv_array[0],$i);
				$err = array_filter($err);
				if(count($err) > 14){
					continue;
				}
				$i++;
			}else if($row == $csv_array[0]){
				$err[] = $this->file_header_validate($row, $ids1, $csv_array[0],1);
				$err = array_filter($err);
				if(count($err) > 0){
					break;
				}
				$i++;
			}

		}
		
		$err = array_filter($err);
		
		if(count($err)>0){
			$msg = '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> Error in CSV please rectify error and try again.</div>';
		 	$this->session->set_flashdata('feedback',$msg);
			return $err;

		}else{
			$sms = $this->upload_file();
			//exit;
			$msg = '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> Uploaded successfully. </br>'.$sms.'</div>';
		 	$this->session->set_flashdata('feedback',$msg);
		 	
		 	return "";//$sms[0]=$sms;
		}
	}

	function file_header_validate($data, $ids, $csv_array,$i){
		$excelmsg = "";
		foreach($ids as $value){
			if(!isset($data[$value]) || $data[$value]==''){// && in_array($key, $ids)
				@$excelmsg .= "File is invalid.";
				break;
			}
		}
		if(isset($excelmsg) && $excelmsg != ""){
			$excelmsg = " <b> Error</b>: ".$excelmsg;
			return $excelmsg;
		}
	}

	function error_log_send($data, $ids, $csv_array,$i){
		$excelmsg="";$attributestr="";$attributes = array();$attributeValues = array();
		foreach($data as $key=>$row){
			if($row=='' && in_array($key, $ids)){
				$excelmsg .= $csv_array[$key]." is empty, ";
			}

			if($key == 14){
				$row = explode('|',$row );
				if(is_array($row)) {
		    	 	$img_inc = 0;  if(!isset($s))$s = 0;
		        	
		        	foreach($row as $valsqs){
		        			$valsqsval = explode(':', $valsqs);
		        			if(count($valsqsval) != 2){		        				
		        				$excelmsg .= "Invalid Attributes, ";
		        				break;
		        			}
		        			$attributes[] = $valsqsval[0];
		        			//$attributes[] = "'".$valsqsval[0]."'";
		        			//$attributeValues = explode(',',$valsqsval[1]);
		        			//$attributeValues[] = "'".$valsqsval[1]."'";
		        		}
		        		$attr_count = count($attributes);
		        		$attributestr = implode(",",$attributes);
		        		//echo $attributestr;exit;
		        	}
			}
//echo '<pre>';print_r($data);exit;
			if($key == 11 && $data[0]==0){
				$row = explode(',',$row );
				if(is_array($row)) {
					foreach($row as $valsqs){
						if($valsqs){
				    	 	$ext = pathinfo($valsqs, PATHINFO_EXTENSION);
				    	 	if($ext != "png" && $ext != "jpeg" && $ext != "jpg" && $ext != "PNG" && $ext != "JPEG" && $ext != "JPG" ){
				    	 		$excelmsg .= "$valsqs Invalid image URL (Extension must be .jpg or .jpeg or .png), ";
				        		//break;
				    	 	}
				    	 }
			    	 }
		        }
			}
		}
		
		if(isset($excelmsg) && $excelmsg != "")
			$excelmsg = " <b> Row ".$i."</b>: ".$excelmsg;
		return $excelmsg;
	}



	function upload_file(){
		$target_dir = "assets/que_sheet/pending/";
		$filename = date("YmdhisA").basename($_FILES["product_upload"]["name"]);
		$target_file = $target_dir . $filename;
		$uploadOk = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		// Check if image file is a actual image or fake image
		if (move_uploaded_file($_FILES['product_upload']['tmp_name'], $target_file)) {
		    //echo "File is valid, and was successfully uploaded.\n";
		    $fp = file($target_file, FILE_SKIP_EMPTY_LINES);
		    $number_of_product_list = (count($fp)-1);
		    $status = "Pending";
		    $uploaded_by = $this->session->userdata('user_id');
		    $brands = "";
		    $progress= 0;
		    if($this->Manage_que_sheet_model->add_que_sheet($filename,$number_of_product_list,$status,$uploaded_by,$brands,$progress)){
		    	//echo "Updated";
		    	return $this->upload_products($filename);
		    }else{
		    	//echo "Failed";
		    }
		} else {
		    echo "Possible file upload attack!\n";
		}
	}

	function upload_products($file_path){

		$product_data1 = $this->getdata("assets/que_sheet/pending/".$file_path);
		$product_data1 = array_filter($product_data1);
		//echo "<pre>";
		//print_r($product_data);
		$errorcount = 0;
		$successcount = 0;
		$resultmsg = 0;
		$cnt_two_hundred_series=0;
		$cnt_two_hundred_series_update=0;
		$excelmsg = array();
		$allowed_ext =  array('gif','png' ,'jpg','jpeg','GIF','PNG' ,'JPG','JPEG');
		/* Add product details*/
		$i = -1;
		$add = 0;
		$update = 0;
		$add_query_cnt = 0;
		$update_query_cnt = 0;
		$product_color = '';


		
/*echo "<pre>";
		print_r($product_data1);
		exit;*/
		unset($product_data1[0]);
		$product_counts = count($product_data1);

		foreach ($product_data1 as $product_data) {
			$i++;
			$brand_product_id = trim($product_data[0]);
			$parent_sku = trim($product_data[0]);
			$product_sku = trim($product_data[1]);
			$product_sub_cat_id = trim($product_data[2]);
			$store_id = trim($product_data[3]);
			$brand_name = trim($product_data[4]);
			$product_name = trim($product_data[5]);			
			$description = trim($product_data[6]);
			//$discount_percent = $product_data[6];
			$price= $price1 = trim($product_data[7]);
			$product_discount = $product_discount1 = (int)trim($product_data[8]);
			if((int)$product_discount1>0){
				$price = $product_discount1;
				$product_discount = $price1;
			}
			$discount_start_date = date("Y-m-d H:i:s",strtotime(trim($product_data[9])));
			$discount_end_date = date("Y-m-d H:i:s",strtotime(trim($product_data[10])));
						
			$product_imges = trim($product_data[11]);
			
			$product_stock1 = trim($product_data[12]);
			// $product_stock1 = array();
			// if($product_data[18]!=''){ $product_stock1 = explode(',',$product_data[18] ); }

			$product_tags = explode(",", trim($product_data[13]));
			$attributes_list = explode("|", trim($product_data[14]));
			$attributes_count = 0;
			$attribute_ids = array();
			if(trim($product_data[14]) != "")
				$attributes_count = count($attributes_list);	       

	       if(!isset($product_color) || $product_color == "")
	        	$product_color = "default";
	        $t=0;
	        for($ia=0;$ia<$attributes_count;$ia++){
	        	$attributesvalue = array();
	        	$attributes_data = explode(":", $attributes_list[$ia]);
	        	$attr_slug = strtolower(trim($attributes_data[0]));
	        	$attr_id = $this->product_upload_model->get_attribute_id_by_slug($attr_slug);

	        	//$attributes[] = strtolower($attributes_data[1]);
	        	$attributesvalue = explode(',',strtolower(trim($attributes_data[1])));
	        	//echo '<pre>';print_r($attributesvalue);
	
	        	if(is_array($attributesvalue) && !empty($attributesvalue) && !empty($attr_id))
	        	{	        		        	
	        		foreach ($attributesvalue as $value) {	        		
	        			$attributes[$t]['name'] = strtolower(trim($value));
	        			$attributes[$t]['id'] = $attr_id;
	        			$attribute_ids[] = $attr_id;
	        			$t++;
	        		}
	        	}        	
	        	
	        }
	        //echo '<pre>';print_r($attributes);
	       // echo '<pre>';print_r($attribute_ids);
			
			
			$product_id = $this->SCProductinventory_model->get_product_id_by_sku($product_sku);

			if($product_id == 0){
				$add++;
				$add_query_cnt++;
				$subquery = "(select id from product_desc where sku_number = '$product_sku' limit 1)";
				
				if($this->session->userdata('role_id') == 6){
					$store_id = $this->session->userdata('user_id');
				}else{
					$store_id = $this->SCProductinventory_model->get_store_id_name($store_id);
				}
				
				$brand_info_list = explode(",",$brand_name);
				if(is_array($brand_info_list)){
		            foreach($brand_info_list as $brands_){
		            	$store_subquery = "(select id from brand_store where brand_store_name = '".strtoupper($brands_)."')";
		            	if(!isset($brand_add_query))
		            	{
		            		$brand_add_query = "INSERT INTO product_store (product_id,store_id,status) VALUES ";	
		            	}
						@$brand_add_query .= "( $subquery , $store_subquery, 0), ";
					}
				}

				$data['product_desc']['pro_add'][$i]['product_color'] = $product_color;
				
				$data['product_desc']['pro_add'][$i]['parent_id'] = $parent_sku;
				$product_subcategory_ids = "";
				$product_sub_cat_slug = explode(",", $product_sub_cat_id);
				foreach ($product_sub_cat_slug as $slug) {
					@$product_subcategory_ids .= $this->get_product_categories_list($slug);
				}

				//echo $product_subcategory_ids;exit;
				$data['product_desc']['pro_add'][$i]['product_sub_cat_id'] = $product_subcategory_ids;				
		        $data['product_desc']['pro_add'][$i]['brand_id']= $brand_id = $store_id;
		        $data['product_desc']['pro_add'][$i]['name'] = $product_name;
		        //$data['product_desc']['pro_add'][$i]['url'] = $product_url;
		        $data['product_desc']['pro_add'][$i]['sku_number'] = $product_sku;
		        $data['product_desc']['pro_add'][$i]['stock_count'] = $product_stock1;
		        $data['product_desc']['pro_add'][$i]['description'] = $description;		        
		        
	        	$data['product_desc']['pro_add'][$i]['price'] = $price;
	        	$data['product_desc']['pro_add'][$i]['compare_price'] = $product_discount;
		       
		        $data['product_desc']['pro_add'][$i]['created_by'] = $store_id;
		        $data['product_desc']['pro_add'][$i]['created_datetime'] = date('Y-m-d H:i:s');
		       
		        $data['product_desc']['pro_add'][$i]['discount_start_date'] = $discount_start_date;
		        $data['product_desc']['pro_add'][$i]['discount_end_date'] = $discount_end_date;		      
		        $data['product_desc']['pro_add'][$i]['status'] = 1;
		        $data['product_desc']['pro_add'][$i]['is_delete'] = 0;				
				$data['product_desc']['pro_add'][$i]['approve_reject'] = 'B';
				
		        $data['product_desc']['pro_add'][$i]['slug'] = str_replace(' ','-',strtolower($product_name)).'-'.time().mt_rand();
				
				

		        if(!empty($product_tags)) {
	            	$product_tags = (array)$product_tags;
	            	$product_tags = array_filter($product_tags);
	            	
	            	foreach($product_tags as $tags){
            			$tags = addslashes($tags);
            			$subquery =  "(select id from product_desc where sku_number = '$product_sku' limit 1)";
						$create_date = date('Y-m-d H:i:s');
						$tag_id = "add_tags('".strtolower(trim($tags))."', '".date('Y-m-d H:i:s')."', '".str_replace(' ','-',strtolower($tags))."-".time().mt_rand()."')";
						
						
						if($product_id == 0){
            				if(!isset($sql_product_tag_update)){
                        		$sql_product_tag_update = "INSERT INTO product_tag (created_by, created_datetime, product_id, status, tag_id) VALUES ";
                        	}
        					$sql_product_tag_update .= "('$brand_id','$create_date',$subquery,1,$tag_id), ";
        				}
					}
				}

		        //$data['product_desc'][$i]['product_tags']= $tags_data ;
		        //echo '<pre>';print_r($attribute_ids);
				//echo '<pre>';print_r($attributes);exit;
				
				if(isset($attributes) && $attributes)
				{	
					foreach($attributes as $key => $value)
					{
						if(!isset($attribute_query) || $attribute_query == ""){
								$attribute_query = "INSERT INTO attribute_object_relationship
									(object_id,
									attribute_id,
									object_type,
									modified_datetime,
									modified_by)
									VALUES";
							}
							$attribute_query .= "($subquery,
								(select id from attribute_values_relationship where LOWER(attr_value_slug) = LOWER('".$value['name']."') and attribute_id = ".$value['id']." limit 1),
								'product', 
								now(),
								'$brand_id'), ";
							
					}
				}
				
						
				//$product_imges1 = ''; $data['product_extra_images'] =array();
				//if($product_imges!='' && $product_id==0 && $parent_sku==0) { 
				if($parent_sku==0) { 
					$product_imges1 = explode(',',$product_imges); 

					$img_inc = 0; 
	            	foreach($product_imges1 as $pimg){					                		
	            		$imagename = basename($pimg);  
	                                                 
	                    $ext = pathinfo($imagename, PATHINFO_EXTENSION);
	                    
	                    if(!in_array($ext,$allowed_ext)){ $ok = 0;  }else{
							    $newimagename = time().mt_rand(0,10000).'.'.$ext; 
	                            if($img_inc == 0 &&  $product_id==0){ 
	                            	$data['product_desc']['pro_add'][$i]['image'] =  $newimagename; 
	                            	$this->saveProductImage(trim($pimg),trim($newimagename));

	                            }else{
	            					//$data['product_extra_images'][] =  $newimagename; 
	            					if(!isset($sql_extra_img)){
                                		$sql_extra_img = "INSERT INTO extra_images (created_datetime, product_id, product_images) VALUES ";
                                	}
                					$image_info =  $newimagename;
                					$sql_extra_img .= "('".date('Y-m-d H:i:s')."', (select id from product_desc where sku_number = '$product_sku' limit 1), '$image_info'), ";

	            					$this->saveProductExtraImages(trim($pimg),$newimagename);
	            				}
	                           	//$data['product_desc']['image'] =$newimagename;			                               
	                    }

	            		$img_inc++;
	            	}
	        	}

	        	//-------------Add Categories to products
				if(!isset($sql_product_category_add)){
            		$sql_product_category_add = "INSERT INTO category_object_relationship
										(object_id,category_id,object_type)VALUES";
            	}

            	$product_subcategory_ids = explode(",",$product_subcategory_ids);            	
            	$product_subcategory_ids = array_unique($product_subcategory_ids);            	
            	foreach ($product_subcategory_ids as $value) {
            		if($value){
            			$sql_product_category_add .= "((select id from product_desc where sku_number = '$product_sku' limit 1),'$value','product'), ";
            		}
            	}
            	//-------------Add Categories to products


            	$cnt_two_hundred_series++;
				/*if($cnt_two_hundred_series==600){
					$cnt_two_hundred_series=0;
					@$data['product_desc']['sql_product_category_add'][] = $sql_product_category_add;
					$data['product_desc']['sql_extra_img'][] = $sql_extra_img;					
					$data['product_desc']['sql_product_tag_update'][] = $sql_product_tag_update;					
					$data['product_desc']['attribute_query'][] = $attribute_query;
					$data['product_desc']['brand_add_query'][] = $brand_add_query;
					 if(isset($data['product_desc']))
        			$this->product_upload_model->add_product($data['product_desc']);

					unset($sql_product_category_add);
					unset($sql_extra_img);					
					unset($sql_product_tag_update);					
					unset($attribute_query);
					unset($brand_add_query);
				}*/
				//-------------Add Categories to products

				
	        }else{//-------------Update Starts
	        	$update++;
	        	$update_query_cnt++;
				$data['product_udesc']['pro_update'][$i]['id'] = $product_id;
				$data['product_udesc']['pro_update'][$i]['approve_reject'] = 'B';				
	        	@$data['product_udesc']['pro_update'][$i]['price'] = $price;
	        	@$data['product_udesc']['pro_update'][$i]['compare_price'] = $product_discount;	       
				
				$data['product_udesc']['pro_update'][$i]['discount_start_date'] = $discount_start_date;
				$data['product_udesc']['pro_update'][$i]['discount_end_date'] = $discount_end_date;
				
				//echo $store_id;//
				if($this->session->userdata('role_id') == 6){
					$store_id = $this->session->userdata('user_id');
				}else{
					$store_id = $this->SCProductinventory_model->get_store_id_name($store_id);
				}

				$data['product_udesc']['pro_update'][$i]['brand_id'] = $store_id;
				$data['product_udesc']['pro_update'][$i]['stock_count'] = $product_stock1;

		        //$data['product_udesc'][$i]['product_tags']= $tags_data ;		       
		        
	    	}
			//echo '<pre>';print_r($data['product_desc']);exit;
	    	if($update_query_cnt >= 600){
	    		$this->product_upload_model->add_product($data['product_desc']);
	    		$update_query_cnt=0;
	    	}
	    	if($add_query_cnt >= 600){
	    		$this->product_upload_model->update_product($data['product_udesc']);
	    		$add_query_cnt=0;
	    	}
	    	
		}
		/*$this->db->trans_start();
        $this->db->insert_batch('product_desc', $data['product_desc']['pro_add']); 
        $this->db->trans_complete();
*/		
       /* echo "<pre>";
        print_R($data['product_desc']);
        exit;*/

        if(isset($sql_product_category_add))
        	@$data['product_desc']['sql_product_category_add'][] = $sql_product_category_add;
        if(isset($sql_extra_img))
        	@$data['product_desc']['sql_extra_img'][] = $sql_extra_img;
        if(isset($sql_product_tag_update))
        	@$data['product_desc']['sql_product_tag_update'][] = $sql_product_tag_update;
        if(isset($attribute_query))
        	@$data['product_desc']['attribute_query'][] = $attribute_query;
        if(isset($brand_add_query))
        	@$data['product_desc']['brand_add_query'][] = $brand_add_query;

        /*foreach ($data as $key => $value) {
        	# code...
        	foreach ($value as $key1 => $value1) {
        		# code...
				print_r($key1);echo "</br>";
        	}
        }*/
        /*echo "<pre>";
        print_r($data);
        exit;*/
        if(isset($data['product_desc']))
        	$this->product_upload_model->add_product($data['product_desc']);

		if(isset($data['product_udesc']))
        	$this->product_upload_model->update_product($data['product_udesc']);

        $this->Manage_que_sheet_model->update_que_sheet($product_in_draft=0,$product_in_error=0,$product_in_published=$add,$status="Complete",$progress=$add+$update,$file_path);
        copy("assets/que_sheet/pending/".$file_path, "assets/que_sheet/completed/".$file_path);
        unlink("assets/que_sheet/pending/".$file_path);
        unset($sql_product_category_add);
		unset($sql_extra_img);					
		unset($sql_product_tag_update);					
		unset($attribute_query);
		unset($brand_add_query);
		unset($data['product_desc']);
		unset($data['product_udesc']);

        return "Product Add: ".$add." Product Updated: ".$update;


	}

	public function saveProductImage($pimg,$newimagename){ 
	// if($pimg!='')
	//     {
	//         $ok =0;
	//     }else{
	        
	        file_put_contents('./assets/products/'.$newimagename, file_get_contents($pimg));

	        file_put_contents('./assets/product_images/zoom/'.$newimagename, file_get_contents($pimg));
	        
	        $config['image_library'] = 'gd2';
	        $config['source_image'] = './assets/products/'.$newimagename;
	        $config['new_image'] = './assets/products/thumb/';
	        $config['create_thumb'] = TRUE;
	        $config['thumb_marker'] = '';
	        $config['maintain_ratio'] = TRUE;
	        $config['width'] = 200;
	        $config['height'] = 200;

	        $this->image_lib->initialize($config);

	            if (!$this->image_lib->resize()){
	                    $this->image_lib->display_errors();
	                    $ok =0;
					}else{
						$ok =1;
	                    }
	    // }

	    	$size = getimagesize('./assets/products/'.$newimagename);

	       /* 160 thumb creation*/
	        $config['image_library'] = 'gd2';
	        $config['source_image'] = './assets/products/'.$newimagename;
	        $config['new_image'] = './assets/products/thumb_160/';
	        $config['create_thumb'] = TRUE;
	        $config['thumb_marker'] = '';
	        $config['maintain_ratio'] = TRUE;
	        $config['width'] = 160;
	        //$config['height'] = 200;

	        $this->image_lib->initialize($config);

	            if (!$this->image_lib->resize()){
	                    $this->image_lib->display_errors();
	                    $ok =0;
					}else{
						$ok =1;
	        }

	        /*thumb for 100x124  */
			if(!empty($size))
			{
				if($size[1]<=124)
				{
					$config['image_library'] = 'gd2';
					$config['source_image'] = './assets/products/'.$newimagename;
					$config['new_image'] = './assets/products/thumb_124/';
					$config['create_thumb'] = TRUE;
					$config['thumb_marker'] = '';
					$config['maintain_ratio'] = TRUE;
					//$config['width'] = 310;
					$config['height'] = $size[1];

					//$this->load->library('image_lib', $config);
					$this->image_lib->initialize($config);

					 if(!$this->image_lib->resize()){
		                        $this->image_lib->display_errors();
		                        $ok =0;
							}else{
								$ok =1;
		            }
				}else
				{
					$config['image_library'] = 'gd2';
					$config['source_image'] = './assets/products/'.$newimagename;
					$config['new_image'] = './assets/products/thumb_124/';
					$config['create_thumb'] = TRUE;
					$config['thumb_marker'] = '';
					$config['maintain_ratio'] = TRUE;
					//$config['width'] = 310;
					$config['height'] = 124;

					//$this->load->library('image_lib', $config);
					$this->image_lib->initialize($config);

					 if (!$this->image_lib->resize()){
		                        $this->image_lib->display_errors();
		                        $ok =0;
							}else{
								$ok =1;
		            }
		         }
		    }
	     
    }

    public function saveProductExtraImages($pimg,$newimagename){
    	 /*if($pimg!='')
		        {
		            $ok =0;
		        }else{*/
		            
        file_put_contents('./assets/products_extra/'.$newimagename, file_get_contents($pimg));
        file_put_contents('./assets/product_images/zoom/'.$newimagename, file_get_contents($pimg));
        $size = getimagesize('./assets/products_extra/'.$newimagename);

        $config['image_library'] = 'gd2';
        $config['source_image'] = './assets/products_extra/'.$newimagename;
        $config['new_image'] = './assets/products_extra/thumb/';
        $config['create_thumb'] = TRUE;
        $config['thumb_marker'] = '';
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 200;
        $config['height'] = 200;

        $this->image_lib->initialize($config);

            if (!$this->image_lib->resize()){
                    $this->image_lib->display_errors();
                    $ok =0;
				}else{
					$ok =1;
                    }

        /* 160 thumb creation*/

        $config['image_library'] = 'gd2';
        $config['source_image'] = './assets/products_extra/'.$newimagename;
        $config['new_image'] = './assets/products_extra/thumb_160/';
        $config['create_thumb'] = TRUE;
        $config['thumb_marker'] = '';
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 160;
        //$config['height'] = 200;

        $this->image_lib->initialize($config);

            if (!$this->image_lib->resize()){
                    $this->image_lib->display_errors();
                    $ok =0;
				}else{
					$ok =1;
        }

        /*thumb for 100x124  */
		if(!empty($size))
		{
			if($size[1]<=124)
			{
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/products_extra/'.$newimagename;
				$config['new_image'] =  './assets/products_extra/thumb_124/';
				$config['create_thumb'] = TRUE;
				$config['thumb_marker'] = '';
				$config['maintain_ratio'] = TRUE;
				//$config['width'] = 310;
				$config['height'] = $size[1];

				//$this->load->library('image_lib', $config);
				$this->image_lib->initialize($config);

				 if (!$this->image_lib->resize()){
	                        $this->image_lib->display_errors();
	                        $ok =0;
						}else{
							$ok =1;
	            }
			}else
			{
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/products_extra/'.$newimagename;
				$config['new_image'] =  './assets/products_extra/thumb_124/';
				$config['create_thumb'] = TRUE;
				$config['thumb_marker'] = '';
				$config['maintain_ratio'] = TRUE;
				//$config['width'] = 310;
				$config['height'] = 124;

				//$this->load->library('image_lib', $config);
				$this->image_lib->initialize($config);

				 if (!$this->image_lib->resize()){
	                        $this->image_lib->display_errors();
	                        $ok =0;
						}else{
							$ok =1;
	            }
	         }
	    }     

    }

    public function download_cuesheet(){


			$this->load->helper('download');

			$data = file_get_contents(base_url()."assets/cue_sheet/demo_v3.csv"); // Read the file's contents			

			$name = 'sample_product_cue_sheet.csv';

			// $data = file_get_contents("./assets/cue_sheet/Cue_sheet_documentation.xlsx"); // Read the file's contents			

			// $name = 'sample_product_cue_sheet.xlsx';

			$result = force_download($name, $data);



			if($result)
			{

				// $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Sample CueSheet downloaded successfully!</div>');

			}else
			{				

				$msg = '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5> Downloading Sample Cue Sheet</div>';

				$this->session->set_userdata('feedback',$msg);

			}

		}
	
	function get_product_categories_list($sc_category_id){
        $sql = "SELECT GROUP_CONCAT(T2.id) parent FROM (SELECT
                                @r AS _id,
                                (SELECT @r := category_parent FROM category WHERE id = _id) AS category_parent,
                                @l := @l + 1 AS lvl
                            FROM
                                (SELECT @r := (select id from category where category_slug = '$sc_category_id')) vars,
                                category m
                            WHERE @r <> 0) T1
                        JOIN category T2
                        ON T1._id = T2.id
                        ORDER BY T1.lvl DESC limit 1";
            /*) parent from category ct
                        where id = ".$row['id']." limit 1;*/
            $result = $this->db->query($sql);
            $result = $result->result_array();
            $parents = explode(',', $result[0]['parent']);
            return $result[0]['parent'];
    }  

    function append_quotes($item, $key)
	{
	    echo "'".$item."'";
	}  



}