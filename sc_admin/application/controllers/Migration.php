<?php
ini_set('memory_limit', '-1');
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration extends MY_Controller {

	function __construct(){
       parent::__construct();
       $this->load->model('Migration_model');
   	}

   	function index(){ echo 'sas'; exit; }

   	function category_migration11(){
   		$this->Migration_model->category_migration();
   		exit;
   	}

   	function product_migration11(){
   		$this->Migration_model->product_migration();
   		exit;
   	}

    function delete_products11(){
      $this->Migration_model->delete_products();
      exit; 
    }

    function cat_level_411(){ 
      $this->Migration_model->cat_level_4();
      exit;  
    }
    function attribute_category_mapping11(){
      $this->Migration_model->attribute_category_mapping();
      exit;
    }
    function attributes_product_mapping11(){
      $this->Migration_model->attributes_product_mapping();
      exit;
    }
    function attributes_product_extra_mapping11(){
      $this->Migration_model->attributes_product_extra_mapping();
      exit; 
    }
      function category_count_update11(){
      $this->Migration_model->category_count_update();
      exit;  
    }
    function attribute_count_update11(){
      $this->Migration_model->attribute_count_update();
      exit;  
    }
    function lifestyle_product_mapping11(){
      $this->Migration_model->lifestyle_product_mapping();
      exit;   
    }
    function update_print11(){
      $this->Migration_model->update_print();
      exit; 
    }
    function go_live_categorisation(){
      $this->Migration_model->go_live_categorisation();
      exit; 
    }
    function attribute_sheet_mapping(){
      $this->Migration_model->attribute_sheet_mapping();
      exit;     
    }
    function perosnality_attribute_mapping(){
      $this->Migration_model->perosnality_attribute_mapping();
      exit;    
    }
    function add_order_display_no(){
      $this->Migration_model->add_order_display_no();
      exit;     
    }
}