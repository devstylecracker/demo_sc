<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delivery_process extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('delivery_process_model','dp_model');
        $this->load->model('Delivery_partner_mapping_model','dpm_model');
        $this->load->model('Delivery_process_model','dprocess_model');
        $this->load->model('Manage_orders_model');

    }
 /*---------Feedback submodule functionalities----------*/

    public function index()
    {
        $search_by = $this->input->post('search_by')!='' ? $this->input->post('search_by') : $this->uri->segment(3);
        $table_search = $this->input->post('table_search')!='' ? $this->input->post('table_search') : $this->uri->segment(4);
    	$orders = $this->dp_model->getDpProduct($search_by,$table_search);       
        $data_post['orders'] = $orders;
        $count_rows = count($orders);
        $data_post['count_rows'] = $count_rows;
        $dpartners = $this->dpm_model->getDelPartners();        
        $data_post['dpartners'] = $dpartners;
        $search_by = 0;
        $table_search = 0;

         //=======================
                $search_by = 0;
        $table_search = 0;
        //echo "<pre>"; print_r($this->uri); 
                $search_by = $this->input->post('search_by')!='' ? $this->input->post('search_by') : $this->uri->segment(3);
                $table_search = $this->input->post('table_search')!='' ? $this->input->post('table_search') : $this->uri->segment(4);
                
                if($search_by!='0' && $table_search!='0' && $search_by!='' && $table_search!=''){ 
                  $paginationUrl = 'delivery_process/index/'.$search_by.'/'.$table_search;
                  $uri_segment = 5;
              }else{
                $paginationUrl = 'delivery_process/index/0/0';
                $uri_segment = 5;
              }

                $config['per_page'] = 5;
                $offset = $this->uri->segment(5)!='' ? $this->uri->segment(5) : 0;
                $config['total_rows'] = count($this->dp_model->getDpProduct($search_by,$table_search,'',''));
                $total_rows = $config['total_rows'];
                $orders = $this->dp_model->getDpProduct($search_by,$table_search,$offset,$config['per_page']);
                $data_post['orders'] = $orders;
                $count = $config['total_rows'];
                
                $data_post['search_by'] = $search_by;
                $data_post['table_search'] = $table_search;
               // $this->customPagination($paginationUrl,$count,$uri_segment,$get_data,$total_rows);


              $config['base_url'] = base_url().$paginationUrl;    
              $config['total_rows'] = $count;
              $config['per_page'] = 5;
              $config['uri_segment'] = $uri_segment;
              $config['use_page_numbers'] = TRUE;
              $config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
              $config['full_tag_close'] = '</ul></div>';

              $config['first_link'] = '&laquo; First';
              $config['first_tag_open'] = '<li>';
              $config['first_tag_close'] = '</li>';

              $config['last_link'] = 'Last &raquo;';
              $config['last_tag_open'] = '<li>';
              $config['last_tag_close'] = '</li>';

              $config['next_link'] = 'Next &rarr;';
              $config['next_tag_open'] = '<li>';
              $config['next_tag_close'] = '</li>';

              $config['prev_link'] = '&larr; Previous';
              $config['prev_tag_open'] = '<li>';
              $config['prev_tag_close'] = '</li>';

              $config['cur_tag_open'] = '<li><a href="" style="background-color:#7CCDEF;">';
              $config['cur_tag_close'] = '</a></li>';

              $config['num_tag_open'] = '<li>';
              $config['num_tag_close'] = '</li>';

              
              $this->pagination->initialize($config); 
              $this->data['total_rows'] = $total_rows;
        //=======================

        $orderStatusList = array('1'=>'<span class="text-blue">Processing</span>', '2'=>'<span class="text-green">Confirmed</span>', '3'=>'<span class="text-green">Dispatched</span>', '4'=>'<span class="text-green">Delivered</span>','5'=>'<span class="text-red">Cancelled</span>','6'=>'<span class="text-orange">Return</span>','7'=>'<span class="text-red">Fake</span>');

        if($search_by!='0' && $table_search!='0' && $search_by!='' && $table_search!=''){
            $paginationUrl = 'Delivery_process/index/'.$search_by.'/'.$table_search;
            $uri_segment = 5;
        }else{
            $paginationUrl = 'Delivery_process/index/0/0';
            $uri_segment = 5;
        }
        
        

        $data_post['orderStatusList'] = $orderStatusList;         

        $data_post['dp_orders'] = array();
        $result = $this->dprocess_model->getDelPartnerOrder();
        
        $i=0;
        foreach($result as $val)
        {
            $i++;
            $data_post['dp_orders'][$val['order_prd_id']] = array( 'dp_id'=>$val['dp_id'],
                                                                    'pickup_address'=>$val['pickup_address'],
                                                                    'shipping_address'=>$val['shipping_address'],
                                                                    'awp_no'=>$val['awp_no'],
                                                                    'dpCharges'=>$val['dpCharges'],
                                                                    'dp_status'=>$val['dp_status']
                                                                    );            
        }        

        $this->load->view('common/header');
		$this->load->view('delivery_process/delivery_process',$data_post);
		$this->load->view('common/footer');

	}

    public function DpOrderSave()
    {
        if(!$this->input->is_ajax_request())
        {
            exit('No direct script access allowed');
        }else
        {
            $result = 0;
            $orderUnqId = $this->input->post('orderUnqId');
            $orderPrdId = $this->input->post('orderPrdId');
            $orderId = $this->input->post('orderId');
            $dpartner = $this->input->post('dpartner');
            $pickupAddr = $this->input->post('pickupAddr');
            $shipAddr = $this->input->post('shipAddr');
            $awp = $this->input->post('awp');
            $dpCharges = $this->input->post('dpCharges');
            $dpstatus = $this->input->post('dpstatus');         
            $dp_order_info = $this->input->post('dp_order_info');
            $doid = $this->input->post('doid');
            $productId = $this->input->post('productId');
            $brandId = $this->input->post('brandId');
            $emailSent = $this->input->post('emailSent');           

            $dpUserId = $this->dprocess_model->getDpUserId($dpartner);

            if($this->session->userdata('role_id') == '7' && $awp=='' )
            {               
                echo '4';exit;                
            }

            if($doid!='')
            {
                if($this->session->userdata('role_id') == '7' && $awp=='' )
                { 
                    $data_post = array();
                }else
                {
                    $data_post = array( 
                    'order_prd_id'=>$orderPrdId,               
                    'awp_no'=>$awp,
                    'dpCharges'=>$dpCharges,
                    'dp_status'=>$dpstatus,
                    'created_by' =>$this->session->userdata('user_id'),
                    'created_datetime' => date('Y-m-d H:i:s')             
                    );   
                }
                
            }else
            {
                $data_post = array(
                    'order_unq_no'=>$orderUnqId,
                    'order_prd_id'=>$orderPrdId,
                    'order_no'=>$orderId,
                    'dp_id'=>$dpartner,
                    'product_id'=>$productId,
                    'brand_id'=>$brandId,
                    'pickup_address'=>$pickupAddr,
                    'shipping_address'=>$shipAddr,
                    'awp_no'=>$awp,
                    'dpCharges'=>$dpCharges,
                    'dp_status'=>$dpstatus,
                    'created_by' =>$this->session->userdata('user_id'),
                    'created_datetime' => date('Y-m-d H:i:s')             
                    );
            }

            if(!empty($data_post))
            {
               $result = $this->dprocess_model->dp_order_save($data_post,$doid); 
            }
            
            $data = $this->Manage_orders_model->get_order_unique_no($orderId);
            if($emailSent==1 &&  $result==2)
            {                 
                 if($dpstatus == '2'){//Dispatched=2 in order_delivery_process
                $this->order_Dispatched_Message($orderUnqId,$data[0]['user_id'],$data[0]['first_name'],$data[0]['last_name'],$orderId,$productId,3,$brandId,$dpUserId);
                //In order_product_info Dispatched=3

                }else if($dpstatus == '3'){//Delivered=3 in order_delivery_process
                    
                $this->order_Delivered_Message($orderUnqId,$data[0]['user_id'],$data[0]['first_name'],$data[0]['last_name'],$orderId,$productId,4,$brandId,$dpUserId);
                //In order_product_info Delivered=4              
                }else if($dpstatus == '5'){//Confirmed=5 in order_delivery_process
   
               $this->order_Confirmed_Message($orderUnqId,$data[0]['user_id'],$data[0]['first_name'],$data[0]['last_name'],$orderId,$productId,5,$brandId,$dpUserId);
                  //In order_product_info Confirmed=2   
                }

            }

            if($result)
            {
               /*if($result == 1)
               {
                    $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Record Save Successfully </div>');
                    redirect('/Delivery_process','refresh');
               }else if($result == 2)
               {
                    $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Email Sent and Record Save Successfully </div>');
                    redirect('/Delivery_process','refresh');
               }*/
                
                echo $result;
            }else
            {
                echo 3;
            }
            
        }
    }

    function order_Delivered_Message($order_id,$user_id,$first_name,$last_last,$product_order_id,$product_id,$order_status,$brand_id,$dpUserId)
    {

     /*function order_Delivered_Message(){
      $order_id = '31335_1433';
      $user_id = '31335';
      $first_name = 'Rohith'; 
      $last_last = 'Chevuru';
      $product_order_id = '4388';
      $product_id = '99992';
      $order_status = '4';*/

      if($this->getEmailAddress($user_id)!=''){
        $config['protocol'] = 'smtp';
        $totalProductPrice=0;

      //      $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;
        $config['smtp_host'] = $this->config->item('smtp_host');
        $config['smtp_user'] = $this->config->item('smtp_user');
        $config['smtp_pass'] = $this->config->item('smtp_pass');
        $config['smtp_port'] = $this->config->item('smtp_port');

            $this->email->initialize($config);

            $data['orderUniqueNo'] = $order_id;
            $data['Username'] = $first_name.' '.$last_last;
            /*$data['order_product_info'] = $this->cartnew_model->su_get_order_info($order_id);
            $data['order_info'] = $this->cartnew_model->su_get_order_price_info_res($order_id);*/
            $data['order_product_info'] =  $this->Manage_orders_model->su_get_order_info($order_id,$product_order_id,$product_id,$order_status);

            /*echo '<pre>';print_r($data['order_product_info']);*/
            $data['order_info'] = $this->Manage_orders_model->su_get_order_price_info_res($order_id,$product_order_id);
         /*   echo '<pre>';print_r($data['order_info']);exit;  */

            $data['shipping_address'] = $data['order_info'][0]['shipping_address'];
            $data['city_name'] = $data['order_info'][0]['city_name'];
            $data['state_name'] = $data['order_info'][0]['state_name'];
            $data['pincode'] = $data['order_info'][0]['pincode'];
            $data['country_name'] = $data['order_info'][0]['country_name'];
            $data['paymentmode'] = $data['order_info'][0]['pay_mode'];
            $data['company_name'] = trim($this->getBrands_info($brand_id));  
            $data['set_product_id'] = $product_id;
            $data['order_product_id'] = $product_order_id;
            $brand_email = trim($this->getEmailAddress($brand_id));
            $dp_email = trim($this->getEmailAddress($dpUserId)); 

            if($data['order_info'][0]['state_id'] == '36') { $data['state_name'] =  $data['order_info'][0]['it_state']; }

            //$data['res'] =  $this->Manage_orders_model->su_get_order_info($order_id);
            //$this->load->view('emails/order_successful_user_copy',$data);

            $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Delivered');
            if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){
               $this->email->to($this->config->item('sc_test_emaild'));
               $this->email->cc($this->config->item('sc_testcc_emaild'));
            }else{
              $this->email->to($this->getEmailAddress($user_id));
              $this->email->cc(array('order@stylecracker.com',$brand_email,$dp_email));
            }
           
            $this->email->subject('StyleCracker: Order Delivered');
            //$this->load->view('emails/order_delivered_user_email',$data);
            $message = $this->load->view('emails/order_delivered_user_email',$data,true);
            $this->email->message($message);
            $this->email->send();

        }
    }

    function order_Dispatched_Message($order_id,$user_id,$first_name,$last_last,$product_order_id,$product_id,$order_status,$brand_id,$dpUserId){
 /* function order_Dispatched_Message(){
      $order_id = '24224_551_552_553';
      $user_id = '24224';
      $first_name = 'Chatur ';
      $last_last = 'Ramalingum';
      $product_order_id = '580';
      $product_id = '11577';
      $order_status = '3';*/

      if($this->getEmailAddress($user_id)!=''){

          $config['protocol'] = 'smtp';
          $totalProductPrice=0;
        //$config['mailpath'] = '/usr/sbin/sendmail';
          $config['charset'] = 'iso-8859-1';
          $config['mailtype'] = 'html';
          $config['wordwrap'] = TRUE;
          $config['smtp_host'] = $this->config->item('smtp_host');
          $config['smtp_user'] = $this->config->item('smtp_user');
          $config['smtp_pass'] = $this->config->item('smtp_pass');
          $config['smtp_port'] = $this->config->item('smtp_port');

            $this->email->initialize($config);

            $data['orderUniqueNo'] = $order_id;
            $data['Username'] = $first_name.' '.$last_last;
            /*$data['order_product_info'] = $this->cartnew_model->su_get_order_info($order_id);
            $data['order_info'] = $this->cartnew_model->su_get_order_price_info_res($order_id);*/
            $data['order_product_info'] =  $this->Manage_orders_model->su_get_order_info($order_id,$product_order_id,$product_id,$order_status);

            /*echo '<pre>';print_r($data['order_product_info']);*/
            $data['order_info'] = $this->Manage_orders_model->su_get_order_price_info_res($order_id,$product_order_id);
          /*  echo '<pre>';print_r($data['order_info']);exit;  */

            $data['shipping_address'] = $data['order_info'][0]['shipping_address'];
            $data['city_name'] = $data['order_info'][0]['city_name'];
            $data['state_name'] = $data['order_info'][0]['state_name'];
            $data['pincode'] = $data['order_info'][0]['pincode'];
            $data['country_name'] = $data['order_info'][0]['country_name'];
            $data['paymentmode'] = $data['order_info'][0]['pay_mode'];
            $data['company_name'] = trim($this->getBrands_info($brand_id));  
            $data['set_product_id'] = $product_id;
            $data['order_product_id'] = $product_order_id;
            $brand_email = trim($this->getEmailAddress($brand_id));  
            $dp_email = trim($this->getEmailAddress($dpUserId));  

            if($data['order_info'][0]['state_id'] == '36') { $data['state_name'] =  $data['order_info'][0]['it_state']; }

            //$data['res'] =  $this->Manage_orders_model->su_get_order_info($order_id);
            //$this->load->view('emails/order_successful_user_copy',$data);

            $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Dispatched');
            if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){              
                $this->email->to($this->config->item('sc_test_emaild')); 
                 $this->email->cc($this->config->item('sc_testcc_emaild'));      
            }else{
              $this->email->to($this->getEmailAddress($user_id));            
              $this->email->cc(array('order@stylecracker.com',$brand_email,$dp_email));   
            }
                      
            $this->email->subject('StyleCracker: Order Dispatched');
            //$this->load->view('emails/order_dispatched_user_email',$data);
            $message = $this->load->view('emails/order_dispatched_user_email',$data,true);
            $this->email->message($message);
            $this->email->send();

        }
    }

    function order_Confirmed_Message($order_id,$user_id,$first_name,$last_last,$product_order_id,$product_id,$order_status,$brand_id,$dpUserId){
 /* function order_Confirmed_Message(){
      $order_id = '24224_551_552_553';
      $user_id = '24224';
      $first_name = 'Chatur ';
      $last_last = 'Ramalingum';
      $product_order_id = '580';
      $product_id = '11577';
      $order_status = '2';*/

      if($this->getEmailAddress($user_id)!=''){

          $config['protocol'] = 'smtp';
          $totalProductPrice=0;
        //$config['mailpath'] = '/usr/sbin/sendmail';
          $config['charset'] = 'iso-8859-1';
          $config['mailtype'] = 'html';
          $config['wordwrap'] = TRUE;
          $config['smtp_host'] = $this->config->item('smtp_host');
          $config['smtp_user'] = $this->config->item('smtp_user');
          $config['smtp_pass'] = $this->config->item('smtp_pass');
          $config['smtp_port'] = $this->config->item('smtp_port');

            $this->email->initialize($config);

            $data['orderUniqueNo'] = $order_id;
            $data['Username'] = $first_name.' '.$last_last;
            /*$data['order_product_info'] = $this->cartnew_model->su_get_order_info($order_id);
            $data['order_info'] = $this->cartnew_model->su_get_order_price_info_res($order_id);*/
            $data['order_product_info'] =  $this->Manage_orders_model->su_get_order_info($order_id,$product_order_id,$product_id,$order_status);

            /*echo '<pre>';print_r($data['order_product_info']);*/
            $data['order_info'] = $this->Manage_orders_model->su_get_order_price_info_res($order_id,$product_order_id);
          /*  echo '<pre>';print_r($data['order_info']);exit;  */

            $data['shipping_address'] = $data['order_info'][0]['shipping_address'];
            $data['city_name'] = $data['order_info'][0]['city_name'];
            $data['state_name'] = $data['order_info'][0]['state_name'];
            $data['pincode'] = $data['order_info'][0]['pincode'];
            $data['country_name'] = $data['order_info'][0]['country_name'];
            $data['paymentmode'] = $data['order_info'][0]['pay_mode'];
            $data['company_name'] = trim($this->getBrands_info($brand_id));  
            $data['set_product_id'] = $product_id;
            $data['order_product_id'] = $product_order_id;
            $data['order_display_no'] = $data['order_info'][0]['order_display_no'];
            $brand_email = trim($this->getEmailAddress($brand_id));
            $dp_email = trim($this->getEmailAddress($dpUserId)); 

            if($data['order_info'][0]['state_id'] == '36') { $data['state_name'] =  $data['order_info'][0]['it_state']; }

            //$data['res'] =  $this->Manage_orders_model->su_get_order_info($order_id);
            //$this->load->view('emails/order_successful_user_copy',$data);

            $this->email->from($this->config->item('from_email'), 'StyleCracker: Order Confirmed');
            if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){              
                $this->email->to($this->config->item('sc_test_emaild')); 
                $this->email->cc($this->config->item('sc_testcc_emaild'));      
            }else{
                $this->email->to($this->getEmailAddress($user_id));
                $this->email->cc(array('order@stylecracker.com',$brand_email,$dp_email)); 
            }
            
            $this->email->subject('StyleCracker: Order Confirmed');
            //$this->load->view('emails/order_confirmed_user_email',$data);
            $message = $this->load->view('emails/order_confirmed_user_email',$data,true);
            $this->email->message($message);
            $this->email->send();

        }
    }


    function getEmailAddress($user_id){
      $res = $this->Manage_orders_model->get_user_info($user_id);
      if(!empty($res)){
        return $res[0]['email'];
      }else{ return ''; }
    }

    function getBrands_info($user_id){
      $res = $this->Manage_orders_model->getBrands_info($user_id);
      if(!empty($res)){
        return $res[0]['company_name'];
      }else{ return ''; }
    }

    public function exportExcel($param,$search_by=NULL,$table_search=NULL,$is_download=NULL)
  { $is_download = $this->uri->segment(6);

    if($search_by == 6 )
    {
      $table_search_encode = base64_decode($table_search);
    }else if($search_by == 8)
    {
      $table_search_encode = base64_decode($table_search);
    }else
    {
      $table_search_encode = $table_search;
    }
    //$is_download = $this->input->post('param');
        $fieldName = array('Order ID','Product Name','Store','Delivery Partner','Pickup Address','Shipping Address','Amount to be Collected','Payment Type','Order Status','AWB No','DPCharges','DP Status');
    
    $get_data = $this->dp_model->getDPOrdersExcel($search_by,$table_search_encode);
    /*echo '<pre>';print_r($get_data);exit;*/
    $result_data = array();
    $i =0;

    foreach($get_data as $val) {        
       
            $result_data[$i][] = $val['order_id'];
            $result_data[$i][] = $val['product_name'];
            $result_data[$i][] = $val['company_name'];
            $result_data[$i][] = $val['dp_name'];
            $result_data[$i][] = $val['pickup_address'];
            $result_data[$i][] = $val['shipping_address'];
			$result_data[$i][] = $val['product_price'];
			$result_data[$i][] = $val['pay_mode'];
            $order_status = "";
            if($val['order_status'] == 1)
            {
                $order_status = 'Processing';
            }else if($val['order_status'] == 2)
            {
                $order_status = 'Confirmed';
            }else if($val['order_status'] == 3)
            {
                $order_status = 'Dispatched';
            }else if($val['order_status'] == 4)
            {
                $order_status = 'Delivered';
            }else if($val['order_status'] == 5)
            {
                $order_status = 'Cancelled';
            }else if($val['order_status'] == 6)
            {
                $order_status = 'Return';
            }else if($val['order_status'] == 7)
            {
                $order_status = 'Fake';
            }

            $result_data[$i][] = $order_status;    

            $result_data[$i][] = $val['awp_no'];
            $result_data[$i][] = $val['dpCharges'];

            $dop_status = "";
            if($val['dp_status'] == 1)
            {
                $dop_status = 'Pending';
            }else if($val['dp_status'] == 2)
            {
                $dop_status = 'Dispatched';
            }else if($val['dp_status'] == 3)
            {
                $dop_status = 'Delivered';
            }else if($val['dp_status'] == 4)
            {
                $dop_status = 'Return';
            }

            $result_data[$i][] = $dop_status;  
           
            $i++;
        }
        //echo "<pre>";print_r($result_data);exit;
       // echo $param.' '.$is_download;
        if($param == 'downloadExcel' && $is_download == 1)
        {
        //========================Excel Download==============================
          //$excel_result = $this->writeDataintoCSV($fieldName,$get_data,'view_order_payment');
         $this->writeDataintoCSV($fieldName,$result_data,'delivery_process_order');
         //========================End Excel Download==========================


        }

  }
  
public function get_awb_no($order_unique_number ='')
	{
		$order_unique_number  	= $this->input->post('order_unique_number');
		$boxWeight  			= $this->input->post('boxWeight');
		$deliveryType  			= $this->input->post('deliveryType');
		$result 				= $this->Manage_orders_model->getOrderProductInfo($order_unique_number);
		$paymod_data 			= $result[0]['pay_mode'];
		$paymod 				= ($paymod_data == 1)?'C':'P';
		$CollectableAmount 		= ($paymod_data == 1)? round($result[0]['OrderGrandTotal']):' ';
		$userid = explode('_',$order_unique_number);
		$fields = '
			<NewDataSet xmlns="http://schemas.datacontract.org/2004/07/WebX.Entity">
			  <Customer>
				<CUSTCD>CC000100132</CUSTCD>
			  </Customer>
			  <DocketList>
			 <DocketList>
				  <AgentID></AgentID>
				  <AwbNo></AwbNo>
				  <Breath>1</Breath>
				  <CollectableAmount>'.$CollectableAmount.'</CollectableAmount>
				  <CustomerName>'.$result[0]['first_name'].' '.$result[0]['last_name'].'</CustomerName>
				  <Height>1</Height>
				  <IsPudo>N</IsPudo>
				  <ItemName>'.$result[0]['product_name'].'</ItemName>
				  <Length>1</Length>
				  <Mode>'.$paymod.'</Mode>
				  <NoOfPieces>'.$result[0]['product_qty'].'</NoOfPieces>
				  <OrderConformation>Y</OrderConformation>
				  <OrderNo>'.$result[0]['order_id'].'</OrderNo>
				  <ProductCode>'.$result[0]['order_prd_id'].'</ProductCode>
				  <PudoId></PudoId>
				  <RateCalculation>N</RateCalculation>
				  <ShippingAdd1>'.$result[0]['shipping_address'].'</ShippingAdd1>
				  <ShippingAdd2> </ShippingAdd2>
				  <ShippingCity>'.$result[0]['city_name'].'</ShippingCity>
				  <ShippingEmailId>'.$result[0]['email_id'].'</ShippingEmailId>
				  <ShippingMobileNo>'.$result[0]['mobile_no'].'</ShippingMobileNo>
				  <ShippingState>'.$result[0]['state_name'].'</ShippingState>
				  <ShippingTelephoneNo>'.$result[0]['mobile_no'].'</ShippingTelephoneNo>
				  <ShippingZip>'.$result[0]['pincode'].'</ShippingZip>
				  <TotalAmount>'.$CollectableAmount.'</TotalAmount>
				  <TypeOfDelivery>Home Delivery</TypeOfDelivery>
				  <TypeOfService>'.$deliveryType.'</TypeOfService>
				  <UOM>Per KG</UOM>
				  <VendorAddress1>Impression House,</VendorAddress1>
				  <VendorAddress2>First Floor, 42A, G. D. Ambekar Marg, Wadala,</VendorAddress2>
				  <VendorName>StyleCracker</VendorName>
				  <VendorPincode>400031</VendorPincode>
				  <VendorTeleNo>8048130591</VendorTeleNo>
				  <Weight>'.$boxWeight.'</Weight>
				</DocketList>
			  </DocketList>
			</NewDataSet> ';
			
		$headers = array
		(
			//'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/xml'
		);

		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'http://dotzot-test.azurewebsites.net/RestService/PushOrderDataService.svc/PushOrderData_PUDO_New' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, $fields );
		$returnData = curl_exec($ch );
		curl_close( $ch );

		$awbData = json_decode($returnData, true);
		$login_user_id = $this->session->userdata('user_id');
		$deliveryProcess 						= array();
		$deliveryProcess['order_unq_no']		= $order_unique_number;
		$deliveryProcess['order_prd_id']		= $result[0]['order_prd_id'];	
		$deliveryProcess['order_no']			= $result[0]['order_id'];
		$deliveryProcess['product_id']			= $result[0]['product_id'];
		$deliveryProcess['brand_id']			= $result[0]['brand_id'];
		$deliveryProcess['dp_id']				= '';
		$deliveryProcess['pickup_address']		= 'Impression House, First Floor, 42A, G. D. Ambekar Marg, Wadala, Mumbai - 400031, Maharashtra, India';
		$deliveryProcess['shipping_address']	= $result[0]['shipping_address'].', '.$result[0]['city_name'].'-'.$result[0]['pincode'].', '.$result[0]['state_name'];
		$deliveryProcess['awp_no']				= $awbData[0]['DockNo'];
		$deliveryProcess['dpCharges']			= $awbData[0]['CODCHARGE'];
		$deliveryProcess['dp_status']			= '';
		$deliveryProcess['created_by']			= $login_user_id;
		$deliveryProcess['modified_by']			= $login_user_id;
		$deliveryProcess['created_datetime']	= date('Y-m-d H:i:s');
		$dpId = $this->Manage_orders_model->addDeliveryProcess($deliveryProcess);
		
		$getAwbNosData = $this->Manage_orders_model->getAwbNosData($dpId, $order_unique_number);
		echo  json_encode($getAwbNosData);
		

	}
	
  
  public function add_tracking_data($awbno='')
	{
		$awbno = $this->input->post('awbno')?$this->input->post('awbno'):'I31000108777';
		
		$fields = '<RequestDocket xmlns="http://schemas.datacontract.org/2004/07/WebX.Entity">
					  <DocketNo>'.$awbno.'</DocketNo>
					</RequestDocket>';
		
		$headers = array
		(
			//'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/xml'
		);
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'http://dotzot-test.azurewebsites.net/RestService/DocketTrackingService.svc/GetDocketTrackingDetails');
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, $fields );
		$returnData = curl_exec($ch );
		curl_close( $ch );

		$tracking_data  						= array();
		$tracking_data['tracking_data'] 		= $returnData;
		$tracking_data['modified_datetime'] 	= date('Y-m-d H:i:s');
		$this->Manage_orders_model->update_trackdata_by_awbno($tracking_data, $awbno);
		$trackData = $this->dprocess_model->getTrackData($awbno); //dprocess_model  = delivery_process_model
		$trackResponse 			=  json_decode(json_encode($trackData['tracking_data']), true);
		$data['json_decode'] 	=  json_decode($trackResponse, true);
		//echo json_decode(json_encode($trackResponse, true));
		
		echo $this->load->view('delivery_process/delivery-tracking-data', $data, true);
		
		 
	}
  

}
