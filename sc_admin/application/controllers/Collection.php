<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
class Collection extends MY_Controller {
	
	function __construct(){
       parent::__construct();
        $this->load->model('users_model');    
        $this->load->model('product_model');  
        $this->load->model('Collection_img_model');  
		$this->load->model('Collection_model');
		$this->load->model('lookcreator_model'); 
		$this->load->model('tag_model');
		$this->load->model('Users_model');
		$this->load->model('Stylist_model');	
			
      
   }
   
	public function index($offset = null){
	
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{ 
			if($this->has_rights(50) == 1){
				
			    $data['parent_category_list'] = $this->Collection_model->parent_catgory_list(); 
				$data['brand_list'] = $this->Collection_model->get_brands_list(); 
				$data['street_data'] = $this->Collection_img_model->get_collection_pic('','','','');
				
				$this->load->view('common/header',$data);
				$this->load->view('collection/add_collection');
				$this->load->view('common/footer2');
			}
		}
		
	}
	
	public function get_attributes(){
		$id = $this->input->post('cat_id');
		$global_attributes = $this->lookcreator_model->all_attributes_depends_on_cat($id);
		$option='';
		$option = $option."<option value=''>Select</option>";
		if(!empty($global_attributes)){
			foreach ($global_attributes as $key => $value) {
			
			$option =$option."<option disabled>".$value['attribute_name']."</option>";
			$attributes_value = $this->lookcreator_model->attributes_values($value['id']); 
				foreach ($attributes_value as $ke => $va) { 
					$option =$option."<option value=".$va['id'].">".$va['attr_value_name']."</option>";
			 }						
			}
		}
		echo $option;
	}
	
	public function get_parent_cat(){
		$parent_category_list = $this->Collection_model->parent_catgory_list();
		$option = '';
		$option = $option."<option value=''>Select</option>";
		if(!empty($parent_category_list)){
			foreach ($parent_category_list as $key => $value) {
				$option =$option."<option value=".$value['id'].">".$value['category_name']."</option>";					
			}
		}
		echo $option;
	}
	
	public function get_brands_list(){
		$brand_list = $this->Collection_model->get_brands_list();
		$option = '';
		$option = $option."<option value=''>Select</option>";
		if(!empty($brand_list)){
			foreach ($brand_list as $key => $value) {
				$option =$option."<option value=".$value['id'].">".$value['company_name'] ."-". $value['cpa_percentage']." % </option>";
				// $option =$option."<option value=".$value['id'].">".$value['category_name']."</option>";					
			}
		}
		echo $option;
	}
	
	public function get_child_cat(){
		$id = $this->input->post('gender');
		$global_attributes = $this->lookcreator_model->get_all_child($id);
		$option = '';
		$option = $option."<option value=''>Select</option>";
		if(!empty($global_attributes)){
			foreach ($global_attributes as $key => $value) {
				$option =$option."<option value=".$value['id'].">".$value['category_name']."</option>";					
			}
		}
		echo $option;
	}
	
	public function get_product(){

		$brand = $this->input->post('brand');
		$cat = $this->input->post('cat');
		$attributes_chosen = $this->input->post('attributes_chosen');
		$category_specific_attr = $this->input->post('category_specific_attr');
		$attributes_chosen_ = ''; $category_specific_attr_ = '';

		if(!empty($attributes_chosen)){ $i = 0;
			foreach($attributes_chosen as $val) {
				if($i!=0) { $attributes_chosen_ = $attributes_chosen_.','; }
				$attributes_chosen_ = $attributes_chosen_.$val;
				$i++;
			}
		}
		
		$lookoffset = $this->input->post('lookoffset');
		
		$data_post['product_list'] = $this->lookcreator_model->get_product_list_v2($brand,$cat,$attributes_chosen_,$category_specific_attr,$lookoffset); 
		$subcat_combo_html = '';
		if($lookoffset == 0){
			$subcat_combo_html = $subcat_combo_html.'<input type="hidden" id="lookoffset" name="lookoffset" value="1">';
		}
		//echo "<pre>";print_r($data_post['product_list']);exit;
		if(!empty($data_post['product_list'])){ 
			foreach($data_post['product_list'] as $val){ 
				$stock_count =$this->lookcreator_model->product_stock($val['product_id']);
				if($stock_count>0){
				$subcat_combo_html = $subcat_combo_html.'<li><img data-img-link="'.LIVE_SITE_URL.'assets/products/'.$val['image'].'" class="lazy" id="'.$val['product_id'].'" src="'.LIVE_SITE_URL.'assets/products/thumb_160/'.$val['image'].'" title="Url : '.$val['url'].', Used_count:'.$val['product_used_count'].', Product Id : '.$val['product_id'].',stock count: '.$stock_count.', Price : '.$val['price'].'"   alt="'.$val['name'].'" width="80px" height="100px" onerror="this.onerror=null; this.src='.DEFAULT_IMAGE_PATH.';">
					 <a class="product-link" href="'.$val['url'].'" target="_blank"><span class="fa fa-link"></span></a>
				</li>';
				}
				
			}
			//$subcat_combo_html = $subcat_combo_html.'<li class="btn-load-more" style="border: medium none; cursor: default; padding-top: 30px;"><div class="btn btn-sm btn-primary load_more" id="load_more"> LOAD MORE <i class="fa fa-arrow-down"></i></div></li>';
		}else{
				$subcat_combo_html = $subcat_combo_html.'<li class="cat">No record found </li>';
			}
		echo $subcat_combo_html; 
		
	}
	
	public function save_collection(){
		// echo "<pre>";print_r($_POST);exit;
		$object_data['collection_img'] = $this->input->post('collection_img');
		$object_data['collection_name'] = $this->input->post('collection_name');
		$object_meta_data['collection_short_desc'] = $this->input->post('short_desc');
		$object_meta_data['collection_long_desc'] = $this->input->post('long_desc');
		$object_meta_data['collection_tags'] = $this->input->post('collection_tags');
		$collection_products = $this->input->post('collection_products');
		$object_data['collection_slug'] = str_replace(' ','-',strtolower($object_data['collection_name'])).'-'.time().mt_rand(0,10000);
		if(!empty($collection_products)){
				foreach($collection_products as $val){
						$prod_look[]  = str_replace("recommended_","",$val);
				}
		}
		
		$object_meta_data['collection_products'] = serialize($prod_look);
		
		if($this->Collection_model->save_collection($object_data,$object_meta_data)){
				return true;
		}
		
	}
	
	public function collection_mgmt(){
		
		if($this->has_rights(52) == 1 || 1 == 1){

			$search_by = 0;
			$table_search = 0;
			$search_by = $this->input->post('search_by')!='' ? $this->input->post('search_by') : $this->uri->segment(4);
			$table_search = $this->input->post('table_search')!='' ? $this->input->post('table_search') : $this->uri->segment(5);
			if($search_by!='0' && $table_search!='0' && $search_by!='' && $table_search!=''){ 
				$paginationUrl = 'collection/collection_mgmt/index/'.$search_by.'/'.$table_search;
				$uri_segment = 6;
			}else{
				$paginationUrl = 'collection/collection_mgmt/index/0/0';
				$uri_segment = 6;
			}

			$config['per_page'] = 20;
			$offset = $this->uri->segment(6)!='' ? $this->uri->segment(6) : 0;
			$config['total_rows'] = count($this->Collection_model->get_all_collection($search_by,$table_search,'',''));

			$total_rows = $config['total_rows'];
			$get_data = $this->Collection_model->get_all_collection($search_by,$table_search,$offset,$config['per_page']);
			$count = $config['total_rows'];

			$data_post['collection_list'] =  $get_data ;
			$data_post['search_by'] = $search_by;
			$data_post['table_search'] = $table_search;

			$config['base_url'] = base_url().$paginationUrl;        
			$config['total_rows'] = $count;
			$config['per_page'] = 20;
			$config['uri_segment'] = $uri_segment;
			$config['use_page_numbers'] = TRUE;
			$config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
			$config['full_tag_close'] = '</ul></div>';

			$config['first_link'] = '&laquo; First';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';

			$config['last_link'] = 'Last &raquo;';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';

			$config['next_link'] = 'Next &rarr;';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';

			$config['prev_link'] = '&larr; Previous';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';

			$config['cur_tag_open'] = '<li><a href="" style="background-color:#7CCDEF;">';
			$config['cur_tag_close'] = '</a></li>';

			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';


			$this->pagination->initialize($config); 
			$this->data['total_rows'] = $total_rows;

			$this->load->view('common/header');
			$this->load->view('collection/manage_collection',$data_post);
			$this->load->view('common/footer');

		}else{
			$this->load->view('common/header');
			$this->load->view('not_permission');
			$this->load->view('common/footer');
		}
		
	}
	
	public function delete_collection(){
		if($this->has_rights(55) == 1){
			   $id = $this->input->post('id');   
			   if($this->Collection_model->delete_collection($id)){
					return true;
				}
			    
		}
	}
	
	public function get_product_new(){

		$brand = $this->input->post('brand');
		$cat = $this->input->post('cat');
		$popup_tags = $this->input->post('popup_tags');
		$offset = $this->input->post('offset');
		// if(!empty($popup_tags)){ $popup_tags = implode(',',$popup_tags); }else { $popup_tags = '';}
		if($popup_tags != ''){ $popup_tags = $this->Collection_model->get_tags(trim($popup_tags)); }
		$product_list='';
		$product_data = $this->Collection_model->get_product_list($brand,$cat,$attributes_chosen_='',$category_specific_attr='',$offset,$popup_tags);
		// echo "<pre>";print_r($product_data);exit;
		if(!empty($product_data)){ 
			foreach($product_data as $val){ 
				
				$product_list = $product_list.'<li class="item">
						<div id ="product_img'.$val['product_id'].'" class="product-img" onclick="show_product('.$val['product_id'].');"  data-toggle="tooltip" data-placement="top" data-html="true">
								<img data-img-link="'.$val['image'].'" class="lazy" id="'.$val['product_id'].'" src="'.$val['image'].'" width="80px" height="100px" onerror="this.onerror=null; this.src='.DEFAULT_IMAGE_PATH.';">
								  <button id ="product_check'.$val['product_id'].'" onclick="add_in_list('.$val['product_id'].')" type="button" class="btn btn-box-tool"><i class="fa fa-check"></i>
									</button>
									<button class="btn btn-box-tool" style="left:0 !important;" onclick="get_product_extraimage('.$val['product_id'].')" ><i class="fa fa-search-plus fa-lg align" "=""></i></button>
							  </div>
								<div class="desc">
							   <div><i class="fa fa-inr"></i> '.$val['price'].' </div>
								<div>Seller: '.$val['brand_name'].'</div></div>
							</li>';
			}
			
			if($product_list != ''){
				//$product_list = $product_list.'<li id="load_more_'.$offset.'" class="item show-more"><div class="btn btn-info" onclick=more_product('.$cat.') >Load more </div></li>';	
			}else{
				$product_list = $product_list.'<li class="item">No record found </li>';
			}
		}else{
			
			$product_list = $product_list.'<li class="item">No record found</li>';
		}
		
		echo $product_list;
		
	}
	
	public function get_single_product(){
		$product_id = $this->input->post('product_id');
		$cart_id = 	$this->input->post('cart_id');
		$type = 	$this->input->post('type');
		$product_data = $this->Collection_model->get_single_product($product_id);
		$product_sku = $this->Collection_model->get_product_sku($product_id);
		$product_child_skus = $this->Collection_model->product_child_sku($product_id,$product_sku);
		$order_attribute_data = array();$order_attribute_ids = array();
		$sku_html = '';$attr_html = ''; $check_match_attr = '';
		
		//$productInventory = $this->Collection_model->product_inventory($product_id);
		$productAttributes = $this->Collection_model->get_product_attributes($product_id);

		if($cart_id!='')
		{
			$order_attribute_data = $this->Collection_model->get_order_attributes($cart_id);
			$order_attribute_ids = array_column($order_attribute_data, 'attribute_id');	
		}
		
		//echo '<pre>';print_r($order_attribute_ids);exit;
		//$sku_html = 'SKU - Size - Stock';
		

		//$sku_html = $sku_html.'<table class="table table-hover sku-table">';
		$sku_html = $sku_html.'<table class="table table-bordered">';
		$sku_html = $sku_html.'<tr>';
		$sku_html = $sku_html.'<th></th>';
		$sku_html = $sku_html.'<th>Product Sku\'s </th>';		
		$sku_html = $sku_html.'</tr>';
		$sku_html = $sku_html.'<tbody>';
		if(is_array($product_child_skus) && count($product_child_skus)>0)
		{
			//echo '<pre>';print_r($product_child_skus);exit;
			foreach($product_child_skus as $pskus)
			{				
				$sku_html = $sku_html.'<tr>';
				if($type=='manual')
				{
					$sku_html = $sku_html.'<td><button id ="product_check'.$pskus['id'].'" onclick="add_in_list('.$pskus['id'].')" type="button" class="btn btn-box-tool" style="color:#a52a2a;"><i class="fa fa-plus-circle fa-lg"></i> ADD</button></td>';
				}else
				{
					$sku_html = $sku_html.'<td></td>';
				}
				
				$sku_html = $sku_html.'<td>'. $pskus["sku_number"] .'</td>';								
				$sku_html = $sku_html.'</tr>';				
			}
		}else 
		{
			$sku_html = $sku_html."<tr>";
			$sku_html  = $sku_html.'<td colspan="2">Empty</td>';
			$sku_html = $sku_html."</tr>";
		}
		$sku_html = $sku_html.'</tbody>';
		$sku_html = $sku_html.'</table>';

		//$attr_html = $attr_html.'<table class="table table-hover ">';
		$attr_html = $attr_html.'<table class="table table-bordered">';
		$attr_html = $attr_html.'<tr>';
		$attr_html = $attr_html.'<th></th>';
		$attr_html = $attr_html.'<th>Attributes </th>';
		$attr_html = $attr_html.'<th>Attributes Value</th>';		
		$attr_html = $attr_html.'</tr>';
		$attr_html = $attr_html.'<tbody>';
		
		if(is_array($productAttributes) && count($productAttributes)>0)
		{
			foreach($productAttributes as $pattr)
			{			
				if(in_array($pattr['attribute_id'], @$order_attribute_ids))
				{
					$check_match_attr = 'matchparam';
				}else
				{
					$check_match_attr = '';
				}
				$attr_html = $attr_html.'<tr class="'.$check_match_attr.'">';
				$attr_html = $attr_html.'<td></td>';
				$attr_html = $attr_html.'<td>'. $pattr["attribute_parent_name"] .'</td>';	
				$attr_html = $attr_html.'<td>'. $pattr["attr_value_name"] .'</td>';							
				$attr_html = $attr_html.'</tr>';				
			}
		}else 
		{
			$attr_html = $attr_html."<tr>";
			$attr_html  = $attr_html.'<td colspan="2">Empty</td>';
			$attr_html = $attr_html."</tr>";
		}
		$attr_html = $attr_html.'</tbody>';
		$attr_html = $attr_html.'</table>';

		$product_html = '';
		if(!empty($product_data)){ 
			foreach($product_data as $val){ 
				$product_html = $product_html.'<div class="product-details-inner"><div class="products-list-wrp">
							  <ul class="products-list product-list-in-box product-list-inline">
								<li class="item">
								  <div class="product-img">
									<img src="'.$val['image'].'" alt="" onerror="this.onerror=null; this.src='.DEFAULT_IMAGE_PATH.';">
								  </div>
								</li>
							  </ul>
							</div>
							<p style="padding-left:9px;">
							  <strong>Product id:</strong> <a style="color:#a52a2a;font-weight:bolder;" href="'.base_url().'product_management/product_edit/'.$product_id.'" target="_blank" alt="Product Edit" >'.$product_id.' <i class="fa fa-edit fa-lg" style="padding-left:5px;color:brown;"></i></a>
							</p>
							<p>
							  <strong>Product SKU:</strong> '.$val['sku_number'].'
							</p>
							<p>
							  <strong>Product Stock:</strong> '.$val['stock_count'].'
							</p>
							<p>
							  <strong>Name:</strong> '.$val['name'].'
							</p>
							<p>
								'.$sku_html.'
							</p>
							<p>
								'.$attr_html.'
							</p>							
							<p style="padding-left:9px;">
							  <strong>Desc:</strong> '.$val['description'].'
							</p>							
							</div>';
							/*<p>
							<strong>URL:</strong> <a target="_blank" href="https://www.stylecracker.com/product/details/'.$val['slug'].'-'.$val['id'].'"> https://www.stylecracker.com/product/details/'.$val['slug'].'-'.$val['id'].'</a>
							</p>*/
			}
		}else
		{
			$product_html = '<div style="height:420px;font-size:18px;font-weight:600;color:Red;"><p style="padding-top:200px;">This product is currently out of stock</p></div>';
		}
		echo $product_html;
	}
	
	function add_in_list(){
		$product_id = $this->input->post('product_id');
		$sku_id = $this->input->post('sku_id');
		$cat_id = $this->input->post('cat_id');
		$product_data = $this->Collection_model->get_single_product($product_id);
		//echo '<pre>';print_r($product_data);exit;
		$product_list ='';
		/*$product_list = $product_list.'<li class="item" id="list_'.$product_id.'" sku_id="'.$product_data[0]['sku_number'].'" selected_cat_id="'.$cat_id.'">
								  <div class="product-img">
									<img data-img-link="'.$product_data[0]['image'].'" class="lazy" id="'.$product_id.'" src="'.$product_data[0]['image'].'" width="80px" height="100px" onerror="this.onerror=null; this.src='.DEFAULT_IMAGE_PATH.';">
								  </div>
								  <a class="btn-prod-remove" href="javascript:void(0);" onclick="delete_product_from_list('.$product_id.')"><i class="fa fa-times-circle-o"></i></a>
								</li>';*/
		if(!empty($product_data))
		{
			$stock_count = $this->Stylist_model->get_inventory($product_id);			
			if($stock_count>0)
			{
			$product_list = $product_list.'<li class="item new_item" id="list_'.$product_id.'" sku_id="'.$product_data[0]['sku_number'].'" selected_cat_id="'.$product_data[0]['product_category_id'].'" selected_cat_name="'.$product_data[0]['product_category_name'].'" style="border:1px dashed #ccc;margin-right:5px;">
				<div class="product-img" style="width: 70px; height: 60px">
			     <img data-img-link="'.$product_data[0]['image'].'" class="lazy" id="'.$product_id.'" src="'.$product_data[0]['image'].'" width="80px" height="100px" onerror="this.onerror=null; this.src='.DEFAULT_IMAGE_PATH.';">
			    </div>
	            <a class="btn-prod-remove" href="javascript:void(0);" onclick="delete_product_from_list('.$product_id.')"><i class="fa fa-times-circle-o"></i></a>	
	            <div  style="font-size: 9px;font-weight: bold ;padding-left: 2px;">CTC: <span class="fa fa-inr"></span><span class="price">'.$product_data[0]['price'].'</span></div>
	            <div  style="font-size: 9px;font-weight: bold;padding-left: 2px;">MRP: <span class="fa fa-inr"></span>'.$product_data[0]['compare_price'].'</div>                               
	  			</li>';
	  		} 
  		}      
		echo $product_list;
	}
	
	function get_product_list(){
		$collection_products = $this->input->post('collection_products');
		
		if(!empty($collection_products)){
				foreach($collection_products as $val){
						$product_ids[]  = str_replace("list_","",$val);
				}
		}
		$product_id = implode(",",$product_ids);
		$product_data = $this->Collection_model->get_products($product_id);
		$product_list ='';
		foreach($product_data as $val){
			
			$product_list = $product_list.'<li class="item" id="final_list_'.$val['id'].'">
								  <div class="product-img">
									<img data-img-link="'.LIVE_SITE_URL.'assets/products/'.$val['image'].'" class="lazy" id="'.$val['id'].'" src="'.LIVE_SITE_URL.'assets/products/thumb_160/'.$val['image'].'" width="80px" height="100px" onerror="this.onerror=null; this.src='.DEFAULT_IMAGE_PATH.';">
								  </div>
								  <a class="btn-prod-remove" href="javascript:void(0);" onclick="remove_product('.$val['id'].');change_list('.$val['id'].');"><i class="fa fa-times-circle-o"></i></a>
								</li>';
			
		}
		$product_list = $product_list.'<input type="hidden" class="form-control" name="product_ids" id="product_ids" value="'.$product_id.'">';
		echo $product_list;
	}
	
	function get_all_tag(){
		$tag_data = $this->tag_model->get_all_tag('','','','');
		$tag_list = '';
		if(!empty($tag_data)){
			foreach($tag_data as $val){
				$tag_list = $tag_list.'<option value="'.$val->id.'">'.$val->name.'</option>';
			}
		}
		echo $tag_list;
	}
	
	function get_user_images(){
		$user_id = '10668';
		$user_fb_data = unserialize($this->Users_model->get_user_data($user_id)[0]['fb_data']);
		// echo "<pre>";print_r($user_fb_data);exit;
		$this->load->view('common/header');
		$this->load->view('test',$user_fb_data);
		$this->load->view('common/footer');
	}
	
	function show_images(){
		$img_data = $this->input->post('data');
		// echo "<pre>";print_r($img_data);
		$img_html = '';
		foreach($img_data['data'] as $val){
			// echo "<pre>";print_r($val['images'][1]['source']);
			$img_html = $img_html.'<img src="'.$val['images'][1]['source'].'" onerror="this.onerror=null; this.src='.DEFAULT_IMAGE_PATH.';"> ';
		}
		echo $img_html;
	}

	function get_productImages()
	{
		echo "get_productImages";exit;
	}
	
}
