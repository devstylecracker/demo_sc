<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brandusers extends MY_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
       parent::__construct();
       $this->load->model('brandusers_model'); 
       $this->load->model('users_model'); 
       $this->load->model('store_model');   
       $this->load->model('productnew_model');    
       $this->load->library('curl');
   } 
   
   public function index($offset = null)
	{  
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{ 
			$data = array();
			$search_by = 0;
			$table_search = 0;
			$filterby = 0;
			if($this->has_rights(18) == 1){
				// $search_by = $this->input->post('search_by');
				// $table_search = $this->input->post('table_search');
				// $filterby = $this->input->post('filter_by');
			  $search_by = $this->input->post('search_by')!='' ? $this->input->post('search_by') : $this->uri->segment(3);
    		  $table_search = $this->input->post('table_search')!='' ? $this->input->post('table_search') : $this->uri->segment(4);
	          $filterby = $this->input->post('filter_by')!='' ? $this->input->post('filter_by') : $this->uri->segment(5);

				
				if($table_search=='')
				{
					$table_search = 0;
				}
			
				if($search_by!='' && $table_search!='' )
				{

					if($search_by==8)//For DateRange
					{
						$date_from = $this->input->post('date_from');
						$date_to = $this->input->post('date_to');
						$table_search = $date_from.','.$date_to;
						$config['base_url'] = base_url().'brandusers/index/'.$search_by.'/'.$table_search.'/'.$filterby;

					}else
					{
						$config['base_url'] = base_url().'brandusers/index/'.$search_by.'/'.$table_search.'/'.$filterby;
					}
					
					$uri_segment = 6;
				}else if($filterby!='')
				{
					$config['base_url'] = base_url().'brandusers/index/'.$search_by.'/'.$table_search.'/'.$filterby;
					$uri_segment = 6;
				}else
				{
					$config['base_url'] = base_url().'brandusers/index/'.$search_by.'/'.$table_search.'/'.$filterby;
					$uri_segment = 6;
				}
				
				$config['total_rows'] = count($this->brandusers_model->get_allusers($search_by,$table_search,'','',$filterby));
				$config['per_page'] = 20;
				$offset = $this->uri->segment(6)!='' ? $this->uri->segment(6) : 0;
				$config['uri_segment'] = $uri_segment;
				$config['use_page_numbers'] = TRUE;
				$config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
				$config['full_tag_close'] = '</ul></div>';

				$config['first_link'] = '&laquo; First';
				$config['first_tag_open'] = '<li>';
				$config['first_tag_close'] = '</li>';

				$config['last_link'] = 'Last &raquo;';
				$config['last_tag_open'] = '<li>';
				$config['last_tag_close'] = '</li>';

				$config['next_link'] = 'Next &rarr;';
				$config['next_tag_open'] = '<li>';
				$config['next_tag_close'] = '</li>';

				$config['prev_link'] = '&larr; Previous';
				$config['prev_tag_open'] = '<li>';
				$config['prev_tag_close'] = '</li>';

				$config['cur_tag_open'] = '<li class="active" ><a href="">';
				$config['cur_tag_close'] = '</a></li>';

				$config['num_tag_open'] = '<li>';
				$config['num_tag_close'] = '</li>';

				
				$this->pagination->initialize($config); 
				$get_users = $this->brandusers_model->get_allusers($search_by,$table_search,$offset,$config['per_page'],$filterby);
				$data['users_data'] = $get_users;
				$data['total_rows'] = $config['total_rows'];
				$data['search_by'] = $search_by;
				$data['table_search'] = $table_search;
				$data['filterby'] = $filterby;

				$this->load->view('common/header',$data);
				$this->load->view('brand/brandusers');
				$this->load->view('common/footer');
			}else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			}
		}
	}
	
	/*public function index($offset = null)
	{  
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{ 
			$data = array();
			
	
			if($this->has_rights(18) == 1){
				$search_by = $this->input->post('search_by');
				$table_search = $this->input->post('table_search');
				
				$config['base_url'] = base_url().'brandusers/index/';
				$config['total_rows'] = count($this->brandusers_model->get_allusers($search_by,$table_search,'',''));
				$config['per_page'] = 20;
				$config['uri_segment'] = 3;
				$config['use_page_numbers'] = TRUE;
				$config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
				$config['full_tag_close'] = '</ul></div>';

				$config['first_link'] = '&laquo; First';
				$config['first_tag_open'] = '<li>';
				$config['first_tag_close'] = '</li>';

				$config['last_link'] = 'Last &raquo;';
				$config['last_tag_open'] = '<li>';
				$config['last_tag_close'] = '</li>';

				$config['next_link'] = 'Next &rarr;';
				$config['next_tag_open'] = '<li>';
				$config['next_tag_close'] = '</li>';

				$config['prev_link'] = '&larr; Previous';
				$config['prev_tag_open'] = '<li>';
				$config['prev_tag_close'] = '</li>';

				$config['cur_tag_open'] = '<li><a href="">';
				$config['cur_tag_close'] = '</a></li>';

				$config['num_tag_open'] = '<li>';
				$config['num_tag_close'] = '</li>';

				
				$this->pagination->initialize($config); 
				$get_users = $this->brandusers_model->get_allusers($search_by,$table_search,$offset,$config['per_page']);
				$data['users_data'] = $get_users;
				$data['total_rows'] = $config['total_rows'];
				$this->load->view('common/header',$data);
				$this->load->view('brand/brandusers');
				$this->load->view('common/footer');
			}else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			}
		}
	}*/
	
		function user_view(){
			 $data = array();
			
			 $data['roles'] = $this->users_model->get_roles(); 
			 $user_id =  $this->uri->segment(3);
			 $data['user_data'] = $this->users_model->get_user_data($user_id); 
			if($this->has_rights(18) == 1){
				
				$this->load->view('common/header',$data);
				$this->load->view('brand/brandusers_view');
				$this->load->view('common/footer');
				
			}else{
				
				$this->load->view('common/header',$data);
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			 }
			
			}
			
			
		function is_online(){

			if ($this->input->post('is_online') =="" && $this->input->post('is_offline')=="")
				{
					$error = 'Please select Store Type';
					$this->form_validation->set_message('is_online', $error);
					return FALSE;
				}
				else{
					
					return TRUE;
				}
		}

		function target_market(){
			if ($this->input->post('target_market')){return TRUE;}
				else{
					$error = 'Please select Target Market';
					$this->form_validation->set_message('target_market', $error);
					return FALSE;
				}
		}

		function is_male(){
			$male = $this->input->post('is_male');
			$female = $this->input->post('is_female');
			$children = $this->input->post('is_children');
			if($male == ""  && $female == "" && $children == "")
			{
				$error = 'Please select Demographic';
				$this->form_validation->set_message('is_male', $error);
				return FALSE;
			}else{
					return TRUE;
				 }
		}

		function year_establish(){
			if ($this->input->post('year_establish') <= date('Y')){return TRUE;}
				else{
					$error = 'Year Of Establish should not be greater than current year';
					$this->form_validation->set_message('year_establish', $error);
					return FALSE;
				}
		}

		function age_group_from(){
			$male = $this->input->post('is_male');
			$female = $this->input->post('is_female');
			$agegroupfrom = $this->input->post('age_group_from');
			if ($male =="" && $female == "" && $this->input->post('is_children') == 1 && $agegroupfrom >= 12 )
				{
					//$age_group_from >= 12
					$error = 'Children Age should be less than 12';
					$this->form_validation->set_message('age_group_from', $error);
					return FALSE;
				}
				else{
					
					return TRUE;
				}
		}


		function age_group_to(){
			$male = $this->input->post('is_male');
			$female = $this->input->post('is_female');
			$agegroupto = $this->input->post('age_group_to');
			if ($male =="" && $female == "" && $this->input->post('is_children') == 1 && $agegroupto > 12 )
				{
					$error = 'Children Age should be less than or equal to 12';
					$this->form_validation->set_message('age_group_to', $error);
					return FALSE;
				}
				else{
					
					return TRUE;
				}
		}

		function phone_two(){
			if ($this->input->post('phone_one') == $this->input->post('phone_two'))
				{
					$error = 'Phone 1 and Phone 2 cannot be same';
					$this->form_validation->set_message('phone_two', $error);
					return FALSE;
				}
				else{
					
					return TRUE;
				}
		}

		function contact_person(){
			if($this->input->post('contact_person')!="" && $this->input->post('contact_person_two')!="")
			{
				if($this->input->post('contact_person') == $this->input->post('contact_person_two'))
				{
					$error = 'Contact Person 1 and Contact Person 2 cannot be same';
					$this->form_validation->set_message('contact_person', $error);
					return FALSE;
				}
				else{
					
					return TRUE;
				}
			}
			
		}
		
		function mobile_two(){
			if ($this->input->post('mobile_one') == $this->input->post('mobile_two'))
				{
					$error = 'Mobile 1 and Mobile 2 cannot be same';
					$this->form_validation->set_message('mobile_two', $error);
					return FALSE;
				}
				else{
					
					return TRUE;
				}
		}

		function alt_emailid(){
			if ($this->input->post('emailid') == $this->input->post('alt_emailid'))
				{
					$error = 'Email-Id and Alt Email-Id cannot be same';
					$this->form_validation->set_message('alt_emailid', $error);
					return FALSE;
				}
				else{
					
					return TRUE;
				}
		}
		

		function brand_business_edit(){			
			$data = array();
			$company_logo = '';
			$cover_image = '';
			$email = '';
			$data['roles'] = $this->users_model->get_roles(); 
			$data['category'] = $this->category_model->get_all_categories('','','','');
			$brand_id =  $this->uri->segment(3);
			$data['user_data'] = $this->brandusers_model->get_user_data($brand_id); 
			$data['brand_target_market'] = $this->brandusers_model->target_market_by_brand($brand_id); 
			$data['brand_cat'] = $this->brandusers_model->get_brand_cat();			 
			if(!empty($data['brand_target_market'])){ $data['t_market']=array(); 
			foreach($data['brand_target_market'] as $val){ $data['t_market'][]= $val['target_market_id']; } }
			$data['target_market'] = $this->brandusers_model->target_market(); 
			if($this->has_rights(19) == 1){ 
				if($this->input->post()){
					$this->form_validation->set_rules('company_name', 'Company Name', 'required|prep_for_form');
					// $this->form_validation->set_rules('short_description', 'Short Description', 'required|prep_for_form');
					$this->form_validation->set_rules('is_online', '', 'callback_is_online|prep_for_form');
					$this->form_validation->set_rules('target_market', '', 'callback_target_market|prep_for_form');
					$this->form_validation->set_rules('is_male', '', 'callback_is_male|prep_for_form');
					$this->form_validation->set_rules('age_group_from', 'Age group from', 'required|is_natural|prep_for_form|max_length[2]|callback_age_group_from');
					$this->form_validation->set_rules('age_group_to', 'Age group to', 'required|is_natural|prep_for_form|max_length[2]|callback_age_group_to');
					$this->form_validation->set_rules('price_range_from', 'Price range from', 'required|is_natural|prep_for_form');
					$this->form_validation->set_rules('price_range_to', 'Price range to', 'required|is_natural|prep_for_form');
					$this->form_validation->set_rules('website_url', 'Website URL', 'prep_url|prep_for_form');
					$this->form_validation->set_rules('shop_url', 'Shop URL', 'prep_url|prep_for_form');					
					$this->form_validation->set_rules('year_establish', 'Year of Establishment', 'callback_year_establish');
					
					
					$config['upload_path'] = 'assets/brand_logo/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']	= '1000';
					$config['max_width']  = '500';
					$config['max_height']  = '500';
					$config['file_name'] = time();
					
					//$config2['upload_path'] = 'assets/images/brands/';
					$config2['upload_path'] = 'D:/xampp/htdocs/scproject/assets/images/brands/';
					$config2['allowed_types'] = 'gif|jpg|png|jpeg';
					$config2['max_size']	= '100';
					$config2['max_width']  = '760';
					$config2['max_height']  = '250';
					$config2['file_name'] = time();					
						
						
					if ($this->form_validation->run() == FALSE)
					{
						
						$this->load->view('common/header',$data);
						$this->load->view('brand/business_profile');
						$this->load->view('common/footer');
						
					}else{ $ok =1;
					$email = $this->brandusers_model->get_user_email($data['user_data'][0]->user_id);
						if(@$_FILES['company_logo']['name']){
							$this->upload->initialize($config);
						if(!($this->upload->do_upload('company_logo'))){
								$data['error'] = array($this->upload->display_errors()); 
								$this->load->view('common/header',$data);
								$this->load->view('brand/business_profile');
								$this->load->view('common/footer');
								$ok =0;
							 }else{
								 $company_logo= $this->upload->data('file_name'); 
								 $test2 = getimagesize('assets/brand_logo/' . $company_logo);
									$width = $test2[0];
									$height = $test2[1];
									if ($width != 380 && $height != 250){
										$error = array('Image Height And Width Should Be 380 X 250');
										$data['error'] = $error; 
										// print_r($data['error2']);exit;
										$this->load->view('common/header',$data);
										$this->load->view('brand/business_profile');
										$this->load->view('common/footer');
										$ok =0; 
									}
								 }
						}	else{
							$company_logo= $data['user_data'][0]->logo;
						}
						// cover image upload code start
						if(@$_FILES['cover_image']['name']){
							$this->upload->initialize($config2);
						if(!($this->upload->do_upload('cover_image'))){
								$data['error2'] = array($this->upload->display_errors());
								$this->load->view('common/header',$data);
								$this->load->view('brand/business_profile');
								$this->load->view('common/footer');
								$ok =0;
							 }else{
								 // print_r($_FILES);exit;
								 
								$cover_image= $this->upload->data('file_name'); 
								$test = getimagesize('D:/xampp/htdocs/scproject/assets/images/brands/' . $cover_image);
								// print_r($test);exit;
								$width = $test[0];
								$height = $test[1];
								if ($width != 760 && $height != 250){
									$error2 = array('Image Height And Width Should Be 760 X 250');
									$data['error2'] = $error2; 
									// print_r($data['error2']);exit;
									$this->load->view('common/header',$data);
									$this->load->view('brand/business_profile');
									$this->load->view('common/footer');
									$ok =0; 
								}
								}
						}	else{
							$cover_image= $data['user_data'][0]->cover_image;
						}
						// cover image upload code End
						
						// contract image upload code Start
						$config3['upload_path'] = 'assets/images/brands/brand_contract';
						$config3['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
						$config3['max_size']	= '1000';
						$config3['max_width']  = '7600';
						$config3['max_height']  = '2503';
						$config3['file_name'] = time();	
					
						if(@$_FILES['contract_file']['name']){
							$this->upload->initialize($config3);
						if(!($this->upload->do_upload('contract_file'))){
								$data['error2'] = array($this->upload->display_errors());
								$this->load->view('common/header',$data);
								$this->load->view('brand/business_profile');
								$this->load->view('common/footer');
								$ok =0;
							 }else{
								 
									$contract_name= $this->upload->data('file_name'); 
									$this->send_email($email);
								}
						}else{
							$contract_name = $data['user_data'][0]->contract_name;
						}
						
						// contract image upload code End
						
						
						if($ok == 1){
						$company_name= $this->input->post('company_name');
						$short_description= $this->input->post('short_description');
						$long_description= $this->input->post('long_description');
						$year_establish= $this->input->post('year_establish');
						$registration_no= $this->input->post('registration_no');
						$website_url= $this->input->post('website_url');
						$shop_url= $this->input->post('shop_url');
						$designer_name= $this->input->post('designer_name');
						$age_group_from= $this->input->post('age_group_from');
						$age_group_to= $this->input->post('age_group_to');
						$price_range_from= $this->input->post('price_range_from');
						$price_range_to= $this->input->post('price_range_to');
						$age_group_to= $this->input->post('age_group_to');
						$is_online= $this->input->post('is_online');
						$is_offline= $this->input->post('is_offline');
						$is_male= $this->input->post('is_male');
						$is_female= $this->input->post('is_female');
						$is_children= $this->input->post('is_children');
						$target_market= $this->input->post('target_market');

						$category = $this->input->post('category');
						$sub_category = $this->input->post('sub_category');
						$var_type = $this->input->post('var_type');
						$is_verify = $this->input->post('is_verify');
						
						if($is_verify == '1' && @$data['user_data'][0]->is_verify != '1'){
							$this->send_verifaction_email($email,$is_verify,$data['user_data'][0]->company_name);
						}else if($is_verify == '0' && @$data['user_data'][0]->is_verify != '0'){
							$this->send_verifaction_email($email,$is_verify,$data['user_data'][0]->company_name);
						}
						
						if($company_logo == ''){
							$company_logo= '';
						}
						if($cover_image == ''){
							$cover_image= '';
						}
						
						//added by rajesh 6jun2016
						if($short_description!=''){
						$data['brand_info'] = array(
							'company_name'=>$company_name,
							'short_description'=>$short_description,
							'long_description'=>$long_description,
							'year_establish'=>$year_establish,
							'registration_no'=>$registration_no,
							'url'=>$website_url,
							'shop_url'=>$shop_url,
							'designer_name'=>$designer_name,
							'age_group_from'=>$age_group_from,
							'age_group_to'=>$age_group_to,
							'price_range_from'=>$price_range_from,
							'price_range_to'=>$price_range_to,
							'is_online'=>$is_online,
							'is_offline'=>$is_offline,
							'is_male'=>$is_male,
							'is_female'=>$is_female,
							'is_children'=>$is_children,
							'created_by'=>$this->session->userdata('user_id'),
							'modified_by'=>$this->session->userdata('user_id'),
							'created_datetime'=>date('Y-m-d H:i:s'),
							'user_id' => $brand_id,
							'logo' =>$company_logo,
							'cover_image' =>$cover_image,
							'contract_name' =>$contract_name,
							'is_verify' =>$is_verify,
							
						);
						}else{
							$data['brand_info'] = array(
							'company_name'=>$company_name,
							'long_description'=>$long_description,
							'year_establish'=>$year_establish,
							'registration_no'=>$registration_no,
							'url'=>$website_url,
							'shop_url'=>$shop_url,
							'designer_name'=>$designer_name,
							'age_group_from'=>$age_group_from,
							'age_group_to'=>$age_group_to,
							'price_range_from'=>$price_range_from,
							'price_range_to'=>$price_range_to,
							'is_online'=>$is_online,
							'is_offline'=>$is_offline,
							'is_male'=>$is_male,
							'is_female'=>$is_female,
							'is_children'=>$is_children,
							'created_by'=>$this->session->userdata('user_id'),
							'modified_by'=>$this->session->userdata('user_id'),
							'created_datetime'=>date('Y-m-d H:i:s'),
							'user_id' => $brand_id,
							'logo' =>$company_logo,
							'cover_image' =>$cover_image,
							'contract_name' =>$contract_name,
							'is_verify' =>$is_verify,
							
							);
						}

						$data['target_market'] = $target_market; 

						$data['brand_cat_info'] = array(
							'brand_id'=> $brand_id,							
							'prod_cat_id'=>$category,
							'prod_sub_cat_id'=>$sub_category,
							'var_type_id'=>$var_type,
							'is_active'=>'1'
						);

						$brand_id = $this->brandusers_model->update_brand_data($data); 

						$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Brand Business profile updated successfully</div>');
						redirect('/brandusers');
					}
						}
				}else{
				
				$this->load->view('common/header',$data);
				$this->load->view('brand/business_profile');
				$this->load->view('common/footer');
			}
			}else{
				
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			 }
		}
		
		function send_email($email_id){

			$config['protocol'] = 'smtp';
			$config['mailtype'] = 'html';
			$config['wordwrap'] = TRUE;
			$config['smtp_host'] = $this->config->item('smtp_host');
			$config['smtp_user'] = $this->config->item('smtp_user');
			$config['smtp_pass'] = $this->config->item('smtp_pass');
			$config['smtp_port'] = $this->config->item('smtp_port');
		
			$email_id3 = 'alliances@stylecracker.com';
			$this->email->initialize($config);

			$this->email->from($this->config->item('from_email'), 'Stylecracker');
			if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){		        
		        $this->email->to($this->config->item('sc_test_emaild'));
				$this->email->cc($this->config->item('sc_testcc_emaild'));
		    }else{
				$this->email->to($email_id);
				$this->email->cc($email_id3);				
			}
			$this->email->subject('Welcome to Stylecracker');
			$this->email->message("Your contract uploaded please download from www.scnest.in/sc_admin . <br><br>Warm Regards,<br>StyleCracker");
			$this->email->send();
		}
  
		function send_verifaction_email($email_id,$is_verify,$company_name){
			$config['protocol'] = 'smtp';
			$config['mailtype'] = 'html';
			$config['wordwrap'] = TRUE;
			$config['smtp_host'] = $this->config->item('smtp_host');
			$config['smtp_user'] = $this->config->item('smtp_user');
			$config['smtp_pass'] = $this->config->item('smtp_pass');
			$config['smtp_port'] = $this->config->item('smtp_port');
			
			$email_id3 = 'alliances@stylecracker.com';
			$this->email->initialize($config);

			$this->email->from($this->config->item('from_email'), 'Stylecracker');
			if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){		        
		        $this->email->to($this->config->item('sc_test_emaild'));
				$this->email->cc($this->config->item('sc_testcc_emaild'));
		    }else{
				$this->email->to($email_id);
				$this->email->cc($email_id3);				
			}
			$this->email->subject('Welcome to Stylecracker');
			if($is_verify == '1'){
				$this->email->message("Dear ".$company_name." <br><br>Welcome to StyleCracker!<br><br>After having a look at your registration details, we are please to inform you that we would love to have ".$company_name." onboard with us on StyleCracker!<br><br>Please find below the link from where you can download the contract copy:www.stylecracker.com/sc_admin .<br><br>Once you send us an email confirmation to the contract, we shall courier you the signed hardcopy of the same and simultaneously begin the product integration process.<br><br>Please feel free to contact us with any queries.<br><br>Thank you!<br><br>Warm Regards,<br>Team StyleCracker<br><br>Contact : 022-61738506 ");
			}else {
				$this->email->message("Dear ".$company_name." <br><br>Thank you for your interest in collaborating with StyleCracker!<br><br>We regret to inform you that we won't be able to take this collaboration ahead at this moment. We shall keep you on our list for any associations in the future.<br><br>Thank you!<br><br>Warm Regards,<br>Team StyleCracker ");
			}
			$this->email->send();
		}		
			
	function brand_contact_edit(){
		$data = array();
		$company_logo = '';

		$data['roles'] = $this->users_model->get_roles(); 
		$brand_id =  $this->uri->segment(3);
		$data = $this->brandusers_model->contact_info($_POST,$brand_id); 
		// $data = $this->Brand_details_model->contact_info();
		if($this->has_rights(20) == 1){ 
			if($this->input->post()){
				$this->form_validation->set_rules('company_address','Company Name','required|trim');
				$this->form_validation->set_rules('city','City','required|trim');
				$this->form_validation->set_rules('state','State','required|trim');
				$this->form_validation->set_rules('pincode','Pincode','required|trim');
				$this->form_validation->set_rules('country','country','required|trim');
				$this->form_validation->set_rules('contact_person','Contact Person','required|trim');
				$this->form_validation->set_rules('phone_one','Phone Number','required|trim|min_length[10]|max_length[13]');
				$this->form_validation->set_rules('emailid','Email Id','required|trim|valid_email');
				$this->form_validation->set_rules('contact_person_two','Phone Number','required|trim');
				$this->form_validation->set_rules('phone_two','Phone Number','required|trim|min_length[10]|max_length[13]');
				$this->form_validation->set_rules('alt_emailid','Email Id','required|trim|valid_email');
				$this->form_validation->set_rules('contact_person_three','Phone Number','required|trim');
				$this->form_validation->set_rules('phone_three','Phone Number','required|trim|min_length[10]|max_length[13]');
				$this->form_validation->set_rules('alt_emailid_three','Email Id','required|trim|valid_email');
					
					if ($this->form_validation->run() == FALSE){
					
						$this->load->view('common/header',$data);
						$this->load->view('brand/contact_profile');
						$this->load->view('common/footer');
					
					}else{
						
						$data = $this->brandusers_model->contact_info($_POST,$brand_id);		
						
						$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Brand Contact profile updated successfully</div>');
						redirect('/Brandusers');
					}
					
			}else{
				$data = $this->brandusers_model->contact_info($_POST,$brand_id);
				$this->load->view('common/header',$data);
				$this->load->view('brand/contact_profile');
				$this->load->view('common/footer');
			}
		}else{
			$this->load->view('common/header');
			$this->load->view('not_permission');
			$this->load->view('common/footer');
		}
	}
			
	function brand_user_delete(){
			$data = array();
			
			if($this->has_rights(21) == 1){
				$user_id = $this->input->post('id');
				$this->users_model->delete_user($user_id,$this->session->userdata('user_id'));
			}
			
		}
		
	function brand_view(){
	
		$brand_id =  $this->uri->segment(3);
			if($this->has_rights(18) == 1){
			 $data['brand_profile_data'] = $this->brandusers_model->get_user_data($brand_id); 
			 $data['contact_profile_data'] = $this->brandusers_model->get_user_contact_data($brand_id); 
				$this->load->view('common/header',$data);
				$this->load->view('brand/brand_view');
				$this->load->view('common/footer');
		 }
	}
	
	function manage_store(){
		$brand_id =  $this->uri->segment(3);
		$offset =  $this->uri->segment(4);
			if($this->has_rights(22) == 1){
				
				$search_by = $this->input->post('search_by');
				$table_search = $this->input->post('table_search');
				
				$config['base_url'] = base_url().'brandusers/manage_store/'.$brand_id;
				$config['total_rows'] = count($this->store_model->get_store_data($search_by,$table_search,'','',$brand_id));
				$config['per_page'] = 20;
				$config['uri_segment'] = 4;
				$config['use_page_numbers'] = TRUE;
				$config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
				$config['full_tag_close'] = '</ul></div>';

				$config['first_link'] = '&laquo; First';
				$config['first_tag_open'] = '<li>';
				$config['first_tag_close'] = '</li>';

				$config['last_link'] = 'Last &raquo;';
				$config['last_tag_open'] = '<li>';
				$config['last_tag_close'] = '</li>';

				$config['next_link'] = 'Next &rarr;';
				$config['next_tag_open'] = '<li>';
				$config['next_tag_close'] = '</li>';

				$config['prev_link'] = '&larr; Previous';
				$config['prev_tag_open'] = '<li>';
				$config['prev_tag_close'] = '</li>';

				$config['cur_tag_open'] = '<li><a href="">';
				$config['cur_tag_close'] = '</a></li>';

				$config['num_tag_open'] = '<li>';
				$config['num_tag_close'] = '</li>';

				
				$this->pagination->initialize($config); 
			
				 $data['get_all_store'] = $this->store_model->get_store_data($search_by,$table_search,$offset,$config['per_page'],$brand_id); 
				 $data['total_rows'] = count($data['get_all_store']);
				 $data['brand_id'] = $brand_id;
				$this->load->view('common/header',$data);
				$this->load->view('brand/brand_store_view');
				$this->load->view('common/footer');
		 }else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
		}	
	}

/*--Function to approve Reject brand --*/
	function approveReject_brand()
	{
		$apr_flag = $this->input->post('apr_flag');
		$brandID = $this->input->post('brandID');		
		$reason = $this->input->post('reason')." - ".date('Y-m-d H:i:s');
		
		$result = $this->brandusers_model->approveReject_brand($apr_flag,$brandID,$reason);

		if($result)
		{
			if($apr_flag == 'approve')
			{
				echo "Approved";
			}else
			{
				echo "Rejected";
			}
		
		}
	}

	public function get_sub_cat(){

		$cat_info = array();		
		$type = $this->input->post('type');
		$cat_id = $this->input->post('cat_id');
		$sel_id =$this->input->post('sel_id');
		$brand_cat = $this->brandusers_model->get_brand_cat();		
		$cat_info = $this->productnew_model->get_sub_cat($cat_id);
		$html_cat = '';
		if(!empty($cat_info)){
			$html_cat = $html_cat.'<option value="0">Select Subcategory</option>';
			foreach($cat_info as $val){
				if(($val['id'] == $sel_id) || ($val['id'] == @$brand_cat['prod_sub_cat_id'])){
					$html_cat = $html_cat.'<option value="'.$val['id'].'" selected>'.$val['name'].'</option>';
					}else{
					$html_cat = $html_cat.'<option value="'.$val['id'].'">'.$val['name'].'</option>';
					}

				}
			}
		echo $html_cat;

	}


	public function get_var_value_type(){
			$cat_info = array();			
			$type = $this->input->post('type');
			$cat_id = $this->input->post('sub_cat_id');
			$sel_id =$this->input->post('sel_id');
			$brand_cat = $this->brandusers_model->get_brand_cat();			
			$cat_info = $this->brandusers_model->get_var_type($cat_id);
			
			$html_cat = ' <label for="varType">Variation Type </label>
			<select tabindex="10" class="form-control"  name="var_type" id="var_type">
			<option value="0">Select Variation-Type</option>';
			if(!empty($cat_info)){
				$new_name = '';
				$old_name = '';
				$product_variation_values = $this->productnew_model->get_product_variations($sel_id);
				foreach($cat_info as $val){									
					if(($val['id'] == $sel_id) || ($val['id'] == @$brand_cat['var_type_id'])){
					$html_cat = $html_cat.'<option value="'.$val['id'].'" selected>'.$val['name'].'</option>';
					}else{
					$html_cat = $html_cat.'<option value="'.$val['id'].'">'.$val['name'].'</option>';
					}
					 				
					}
				}
			echo $html_cat;

		}

	/* Added by pratik for brand settings*/

	public function Store_Settings(){

 $data = array();
 $user_id =$this->uri->segment(3);
 $data['user_data'] = $this->brandusers_model->user_menu($user_id);
 $company_logo = '';
     /* echo "<pre>";
		print_r($data);
		exit; */
if($this->has_rights(60) == 1){ 
				if($this->input->post()){
					$ok =0;
 	$this->form_validation->set_rules('cpa', 'cpa', 'trim|numeric');

 	$this->form_validation->set_rules('payment_gateway_charges', 'payment_gateway_charges', 'trim|numeric');
 	$this->form_validation->set_rules('service_tax', 'service_tax', 'trim|numeric');
 	$this->form_validation->set_rules('integration_charges', 'integration_charges', 'trim|numeric');

	$this->form_validation->set_rules('cpc', 'cpc', 'trim|numeric');
	$this->form_validation->set_rules('sto_radd', 'Store register address', 'trim');
	$this->form_validation->set_rules('sto_ra', 'Store address returning', 'trim');
	$this->form_validation->set_rules('sc', 'Shipping Charges', 'trim|numeric');
	$this->form_validation->set_rules('max', 'max', 'trim|numeric');
	$this->form_validation->set_rules('min', 'min', 'trim|numeric');
	$this->form_validation->set_rules('cod', 'COD Charges', 'trim|numeric');
	$this->form_validation->set_rules('maxcod', 'max', 'trim|numeric');
	$this->form_validation->set_rules('mincod', 'min', 'trim|numeric');
	

	$this->form_validation->set_rules('shipping_location', 'Text area for shipping', 'trim');
	$this->form_validation->set_rules('cod_location', 'Text area for COD', 'trim');
    $this->form_validation->set_rules('str_id', 'Store register Id', 'trim|numeric');
    $this->form_validation->set_rules('cea', 'Store email address', 'valid_email');
    $this->form_validation->set_rules('vat', 'VAT/TIN number', 'trim');
    $this->form_validation->set_rules('tax', 'Tax', 'trim|numeric');
    $this->form_validation->set_rules('cst', 'CST number', 'trim');
    $this->form_validation->set_rules('str_phn_no', 'Store Phone number', 'numeric|min_length[8]|max_length[12]');
    $this->form_validation->set_rules('str_email', 'Store email address', 'valid_email'); 
    $this->form_validation->set_rules('fileToUpload', 'sign image','trim|xss_clean');
     $this->form_validation->set_rules('brand_code', 'brand code','trim|max_length[5]');
      $this->form_validation->set_rules('min_del', 'Minimum delivery days','trim|numeric');
       $this->form_validation->set_rules('max_del', 'Maximum delivery days','trim|numeric');
       $this->form_validation->set_rules('return_policy', 'Return Policy', 'trim|required');
       //$this->form_validation->set_message('max_length[150]', '%s:Maximum 150 characters');

            $config['upload_path'] = 'assets/brand_signature/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']	= '2048'; // 2048 KB = 2MB = 2048000 B
					$config['max_width']  = '1400';
					//$config['max_height']  = '500';
					$config['file_name'] = time().mt_rand(0,10000);

					$this->upload->initialize($config);

					if(!($this->upload->do_upload('fileToUpload'))){
							$data['error'] = array($this->upload->display_errors());
						
							$ok =0;
					}else{

						/*echo "<pre>";
						print_r($this->upload);exit;*/
						$company_logo= $this->upload->data('file_name');


							$this->image_lib->initialize($config);

							if (!$this->image_lib->resize())
							{
								$this->image_lib->display_errors();
								$ok =0;

							}else{

								$ok =1;
							}


						}

   
	
}
if ($this->form_validation->run() == FALSE){
						
						$this->load->view('common/header');
						$this->load->view('Store_Settings',$data);
						$this->load->view('common/footer');
						
						}else
						{
                               $cpc=$this->input->post('cpc');
                               $commission=$this->input->post('commission');
                               $payment_gateway_charges=$this->input->post('payment_gateway_charges');
                               $service_tax=$this->input->post('service_tax');
                               $integration_charges=$this->input->post('integration_charges');

                               $cpa=$this->input->post('cpa');

 								$ispaid=$this->input->post('ispaid');
 								
 								$showin=$this->input->post('show_in');
								$date_from=$this->input->post('date_from');
								$contract_end_date=$this->input->post('contract_end_date');
								$Store_register_address=$this->input->post('sto_radd');
								$Store_address_returning=$this->input->post('sto_ra');
								$is_shipping=$this->input->post('cell') ? $this->input->post('cell') : 0;
								if($is_shipping == 0){
									$Shipping_Charges=0;
									$min_sc=0;
								    $max_sc=0; 
								}else{
									$Shipping_Charges=$this->input->post('sc');
									$min_sc=$this->input->post('min');
									$max_sc=$this->input->post('max');
								}

								$is_cod=$this->input->post('cod') ? $this->input->post('cod'): 0;
                                 if($is_cod==0){
									$cod_charge='';
									$min_cod='';                               	
									$max_cod='';                                 
								}else{
                                    $cod_charge=$this->input->post('cod_charge'); 
								    $min_cod=$this->input->post('mincod'); 
								    $max_cod=$this->input->post('maxcod'); 
								}


								
								$sign_image=$this->input->post('fileToUpload');

								$shipping_location=$this->input->post('shipping_location');
								$cod_location=$this->input->post('cod_location');

								$Store_Registrations_ID=$this->input->post('str_id');
								$contact_email_address=$this->input->post('cea');
								$Store_Email=$this->input->post('str_email');
								$ret_pol = preg_replace('/[^A-Za-z0-9@.\-]/', ' ', trim($this->input->post('return_policy')));
								$return_policy=$ret_pol;
		
							
								
								
								$vat_tin_no=$this->input->post('vat');
								$tax=$this->input->post('tax');
								$cst_no=$this->input->post('cst');
								
								$store_contact_no=$this->input->post('str_phn_no');


							$brand_code=$this->input->post('brand_code');
							$min_del=$this->input->post('min_del');
							$max_del=$this->input->post('max_del');

							$cod_available = $this->input->post('cod_available');

							if($cod_available == 0){
								$is_cod = 0;
								$cod_charge='';
								$min_cod='';                               	
								$max_cod='';    
							}


		//$var = '`show_in brand_list`';

							$data['Store_Settings'] = array(
											`is_paid` =>$ispaid, 
					`payment_gateway_charges` =>$payment_gateway_charges,
					`service_tax` =>$service_tax,
					`integration_charges` =>$integration_charges,

                    `show_in brand_list`=>$showin,
                    `onboarded_date`=>$date_from, 
					`contract_end_date`=>$contract_end_date, 
                    `cpc_percentage`=>$cpc,
                    `cpa_percentage` =>$cpa,
                    `is_shipping` =>$is_shipping, 
                    `shipping_charges` =>$Shipping_Charges, 
                    `shipping_min_values` =>$min_sc, 
                    `shipping_max_values` =>$max_sc, 
                    `is_cod` =>$is_cod, 
                    `cod_min_value` =>$min_cod, 
                    `code_max_value` =>$max_cod, 
                    `shipping_exc_location` =>$shipping_location,  
                    `cod_exc_location` =>$cod_location, 
                    `registered_address` =>$Store_register_address,
                    `registration_number` =>$Store_Registrations_ID, 
                    `contact_email_address` =>$contact_email_address, 
                    `vat_tin_number` =>$vat_tin_no, 
                    `store_tax` =>$tax, 
                    `cst_number` =>$cst_no,
                    `query_address` =>$Store_address_returning, 
                    `query_contact_no` =>$store_contact_no, 
                    `query_email_id` =>$Store_Email, 
                    `sign_img` =>$sign_image, 
                    `cod_charges` =>$cod_charge, 
                     `brand_code`=>$brand_code,
                     `min_del_days`=>$min_del,
                     `max_del_days`=>$max_del,
                     `cod_available`=>$cod_available,
                      `return_policy`=>$return_policy,
					  `commission_cat`=>$commission

		   											 );
$data['comapanylogo']=$company_logo;
   /*echo '<pre>';
	echo "coming";
	print_r($data['Store_Settings']);
	exit;*/
  
	if($this->brandusers_model->user_menu_update($ispaid,$showin,$date_from,$cpc,$cpa,$is_shipping,$Shipping_Charges,$min_sc,$max_sc,$is_cod,$min_cod,$max_cod,$shipping_location,$cod_location,$Store_register_address,$Store_Registrations_ID,$contact_email_address,$vat_tin_no,$tax,$cst_no,$Store_address_returning,$store_contact_no,$Store_Email,$sign_image,$cod_charge,$brand_code,$min_del,$max_del,$company_logo,$user_id,$cod_available,$return_policy,$payment_gateway_charges,$service_tax,$integration_charges,$commission,$contract_end_date)){

							if($data['user_data'][0]['is_paid'] != $ispaid){
 									$result = $this->curl->simple_post(LIVE_SITE.'Sccache/clear_brand_list', false, array(CURLOPT_USERAGENT => true));
 								}

 							if($data['user_data'][0]['show_in brand_list'] != $showin){
 									$result = $this->curl->simple_post(LIVE_SITE.'Sccache/clear_brand_listing_page', false, array(CURLOPT_USERAGENT => true));
 								}

								$this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Store setting Edited Successfully</div>');
								 redirect('/Brandusers');	
							}else{
								echo '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>Something goes wrong Please try again</div>';

							}










	
	  $this->load->view('common/header');
  $this->load->view('Store_Settings',$data);
  $this->load->view('common/footer');	
             }
            /* redirect('/Brandusers','refresh');*/
  //$this->load->view('common/header');
  //$this->load->view('Store_Settings',$data);
  //$this->load->view('common/footer');
  

}
}
	/*function new_store_settings(){
		$data = array();
		$user_id =$this->uri->segment(3);
		$counter = $this->input->post('counter');
		$j=$counter;
		$data['user_data'] = $this->brandusers_model->user_menu($user_id);
		$data['child_brand'] = $this->brandusers_model->get_child_brand($user_id);
		if($this->input->post()){
			
			$this->form_validation->set_rules('cpa', 'Cpa', 'trim|numeric');
			$this->form_validation->set_rules('payment_gateway_charges', 'Payment Gateway Charges', 'trim|numeric');
			$this->form_validation->set_rules('service_tax', 'Service Tax', 'trim|numeric');
			$this->form_validation->set_rules('integration_charges', 'Integration Charges', 'trim|numeric');
			$this->form_validation->set_rules('brand_code', 'Brand Code','trim|max_length[5]');

			if ($this->form_validation->run() == FALSE){
			
				$this->load->view('common/header');
				$this->load->view('new_store_settings',$data);
				$this->load->view('common/footer');
			
			}else{
				$ispaid = $this->input->post('ispaid');
				$date_from = $this->input->post('date_from');
				$brand_code = $this->input->post('brand_code');
				$cpa = $this->input->post('cpa');
				$showin = $this->input->post('show_in');
				$commission_old = $this->input->post('commission');
				$payment_gateway_charges = $this->input->post('payment_gateway_charges');
				$service_tax = $this->input->post('service_tax');
				$integration_charges = $this->input->post('integration_charges');
				
				if($counter >= 1 && count($data['child_brand']) > 0){
					$cpa_percentage = array();
					for($i=1;$i<=$counter;$i++){
						if($this->input->post('cpa_'.$i) != ''){
							$cpa_percentage[$i] = $this->input->post('cpa_'.$i);
							$commission[$i] = $this->input->post('commission_'.$i);
							$brand_Store_id[$i] = $this->input->post('brand_Store_id_'.$i);
							$show_on_web[$i] = $this->input->post('show_on_web_'.$i);
							$data = $this->brandusers_model->save_brand_cpa_percentage($cpa_percentage[$i],$brand_Store_id[$i],$commission[$i],$show_on_web[$i]);
							$j--;
						}
					}
				}
				$data = $this->brandusers_model->update_brand_data_new($ispaid,$date_from,$brand_code,$cpa,$showin,$commission_old,$payment_gateway_charges,$service_tax,$integration_charges,$user_id);
				redirect('/brandusers');
			}		
		}else {
				$this->load->view('common/header');
				$this->load->view('new_store_settings',$data);
				$this->load->view('common/footer');	

		}
	}*/
	function new_store_settings(){
		$data = array();
		$user_id =$this->uri->segment(3);		
		$counter = $this->input->post('counter');
		$j=$counter;
		$data['user_data'] = $this->brandusers_model->user_menu($user_id);	
		/*echo '<pre>';
		print_r($data['user_data']);
		exit;	*/
		$data['child_brand'] = $this->brandusers_model->get_child_brand($user_id);
		if($this->input->post()){
			
			$this->form_validation->set_rules('cpa', 'Cpa', 'trim|numeric');
			$this->form_validation->set_rules('payment_gateway_charges', 'Payment Gateway Charges', 'trim|numeric');
			$this->form_validation->set_rules('service_tax', 'Service Tax', 'trim|numeric');
			$this->form_validation->set_rules('integration_charges', 'Integration Charges', 'trim|numeric');
			$this->form_validation->set_rules('brand_code', 'Brand Code','trim|max_length[5]');

			if ($this->form_validation->run() == FALSE){
			
				$this->load->view('common/header');
				$this->load->view('new_store_settings',$data);
				$this->load->view('common/footer');
			
			}else{								
				$data['user_data'] = $this->brandusers_model->user_menu($user_id);			
				//echo '<pre>'; print_r($data['user_data']);
				$brand_id = $data['user_data'][0]['user_id'];
				//exit;
				//echo '<pre>';print_r($this->input->post());				
				$ispaid = $this->input->post('ispaid');
				$date_from = $this->input->post('date_from');
				$brand_code = $this->input->post('brand_code');
				$cpa = $this->input->post('cpa');
				$showin = $this->input->post('show_in');
				$commission_old = $this->input->post('commission');
				$payment_gateway_charges = $this->input->post('payment_gateway_charges');
				$service_tax = $this->input->post('service_tax');
				$integration_charges = $this->input->post('integration_charges');
				$company_logo = $_FILES['company_logo']['name'];
				$contract_file = $_FILES['contract_file']['name'];				
				$is_verify = $this->input->post('is_verify');						
				$ok =1;
				//$config['upload_path'] = 'assets/images/brands/';
				$config['upload_path'] = 'assets/brand_logo/';
				//D:\wamp\www\scprojectbit\scproject\sc_admin\
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size']	= '2000';
				$config['max_width']  = '1000';
				$config['max_height']  = '1000';
				//$config['file_name'] = time();		
				$config['file_name'] = $brand_id;	
				$this->upload->initialize($config);	
				if($_FILES['company_logo']['name']){
					if(!($this->upload->do_upload('company_logo'))){	
						$data['error'] = array($this->upload->display_errors()); 																							
						/*$this->load->view('common/header',$data);
						$this->load->view('new_store_settings');
						$this->load->view('common/footer');*/
						$ok =0;
					}
				}
				if($_FILES['company_logo']['name']){					
					$this->upload->initialize($config);
					$ok =1;
					if(!($this->upload->do_upload('company_logo'))){							
						$data['error'] = array($this->upload->display_errors()); 						
						/*$this->load->view('common/header',$data);
						$this->load->view('brandusers');
						$this->load->view('common/footer');*/
						$ok =0;
					}else{
						$company_logo= $this->upload->data('file_name'); 																		
						$test2 = getimagesize('assets/brand_logo/' . $company_logo);												
						$width = $test2[0];
						$height = $test2[1];
							if ($width != 530 && $height != 530){
								$error = array('Image Height And Width Should Be 530 X 530');
								$data['error1'] = $error; 								
								echo $data['error1'];																
								/*$this->load->view('common/header',$data);
								$this->load->view('new_store_settings');
								$this->load->view('common/footer');*/
								$ok =0; 
							}
					 }
				}					
						$config3['upload_path'] = 'assets/images/brands/brand_contract';
						$config3['allowed_types'] = 'pdf';
						$config3['max_size']	= '5000';
						$config3['max_width']  = '7600';
						$config3['max_height']  = '2503';
						$config3['file_name'] = time();	
						
						if($_FILES['contract_file']['name']){							
							$this->upload->initialize($config3);							
						if(!($this->upload->do_upload('contract_file'))){
								$data['error2'] = array($this->upload->display_errors());								
								$this->load->view('common/header',$data);
								$this->load->view('new_store_settings');
								$this->load->view('common/footer');
								$ok =0;
							 }else{								 
									$contract_name= $this->upload->data('file_name'); 									
									//$this->send_email($email);
								}
						}else{
							@$contract_name = $data['user_data'][0]->contract_name;
						}
				
				if($counter >= 1 && count($data['child_brand']) > 0){
					$cpa_percentage = array();
					for($i=1;$i<=$counter;$i++){
						if($this->input->post('cpa_'.$i) != ''){
							$cpa_percentage[$i] = $this->input->post('cpa_'.$i);
							$commission[$i] = $this->input->post('commission_'.$i);
							$brand_Store_id[$i] = $this->input->post('brand_Store_id_'.$i);
							$show_on_web[$i] = $this->input->post('show_on_web_'.$i);
							$data = $this->brandusers_model->save_brand_cpa_percentage($cpa_percentage[$i],$brand_Store_id[$i],$commission[$i],$show_on_web[$i]);
							//$this->brandusers_model->brand_logo_contract($company_logo,$user_id);
							$j--;
						}
					}
				}				
				if($ok==1){					
					$data1 = $this->brandusers_model->update_brand_data_new($ispaid,$date_from,$brand_code,$cpa,$showin,$commission_old,$payment_gateway_charges,$service_tax,$integration_charges,$user_id);
					//if($company_logo != ''){
						$this->brandusers_model->brand_logo_contract($company_logo,$contract_name,$user_id,$is_verify);
					//}
					redirect('/brandusers');
				}else{
					/*$this->load->view('common/header');
					$this->load->view('new_store_settings',$data);
					$this->load->view('common/footer');*/
				}
				

			}		
		}else {
				$this->load->view('common/header');
				$this->load->view('new_store_settings',$data);
				$this->load->view('common/footer');	

		}
	}
}
