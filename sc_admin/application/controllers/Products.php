<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Products extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('api_details_model');  
       
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        //$this->methods['products_details_get']['limit'] = 100; // 100 requests per hour per user/key
        //$this->methods['products_details_post']['limit'] = 100; // 100 requests per hour per user/key
    }

    public function products_details_get($id_param = NULL)
    {
       
    }

    public function products_details_post()
    {
        $api_status = ''; $api_message = ''; $product_error = array(); $product_consumer_type = 0; $body_part_info =0; $h = 0;$ok = 1; 
        $api_key = $this->post('api_key');
        $api_secret_key = $this->post('secret_key');
        $total_products_count = $this->post('total_count');
        $product_info = $this->post('product_info');
        $decrypt_api_key = $this->encryptOrDecrypt($api_key,null);
        $decrypt_secrete_key = $this->encryptOrDecrypt($api_secret_key,null);
        $allowed_ext =  array('gif','png' ,'jpg','jpeg');  
        
        if(trim($api_key) == ''){ $api_message = $api_message.'API key is missing'; $api_status='false';  $ok = 0; }
        else if(trim($api_secret_key == '') && $ok ==1 ){ $api_message = $api_message.'Secret Key is missing'; $api_status='false';  $ok = 0; }
        else if($ok ==1 && $this->api_details_model->authorized_api_user($decrypt_api_key,$decrypt_secrete_key) == 1){ $api_message = $api_message.'Not the Authorized API Credentials'; $api_status='false'; $ok = 0; }
        else if($ok ==1 && !is_numeric($total_products_count)){ $api_message = $api_message.'Invalid Product Count'; $api_status='false';  $ok = 0; }
        else if($ok ==1 && empty($product_info)){ $api_message = $api_message.'Products are missing'; $api_status='false';  $ok = 0; }
        else if($ok ==1 && $total_products_count != count($product_info)){ $api_message = $api_message.'Actual Products and product count is not matching'; $api_status='false';  $ok = 0; }
        else{ 
                for($i=0;$i<$total_products_count;$i++){
                    $data= array(); $product_consumer_type = 0; $body_part_info =0; $tags_id = array(); $stores_id = array(); $variation_values_array = array();
                    $product_name = isset($product_info[$i]['product_name']) ? trim($product_info[$i]['product_name']) :''; 
                    $description = isset($product_info[$i]['description']) ? trim($product_info[$i]['description']) : ''; 
                    $category = isset($product_info[$i]['category']) ? trim($product_info[$i]['category']) : ''; 
                    $sub_category = isset($product_info[$i]['sub_category']) ? trim($product_info[$i]['sub_category']) : ''; 
                    $image = isset($product_info[$i]['image']) ? trim($product_info[$i]['image']) : ''; 
                    $price = isset($product_info[$i]['price']) ? trim($product_info[$i]['price']) : ''; 
                    $price_range = isset($product_info[$i]['price_range']) ? trim($product_info[$i]['price_range']) : ''; 
                    $consumer_type = isset($product_info[$i]['consumer_type']) ? trim($product_info[$i]['consumer_type']) : ''; 
                    $rating = isset($product_info[$i]['rating']) ? trim($product_info[$i]['rating']) : ''; 
                    $body_part = isset($product_info[$i]['body_part']) ? trim($product_info[$i]['body_part']) : ''; 
                    $age_group = isset($product_info[$i]['age_group']) ? trim($product_info[$i]['age_group']) : ''; 
                    $brand_name = isset($product_info[$i]['brand_name']) ? trim($product_info[$i]['brand_name']) : ''; 
                    $tags = isset($product_info[$i]['tags']) ? $product_info[$i]['tags'] : ''; 
                    $target_market = isset($product_info[$i]['target_market']) ? trim($product_info[$i]['target_market']) : ''; 
                    $body_type = isset($product_info[$i]['body_type']) ? trim($product_info[$i]['body_type']) : ''; 
                    $body_shape = isset($product_info[$i]['body_shape']) ? trim($product_info[$i]['body_shape']) : ''; 
                    $personality = isset($product_info[$i]['personality']) ? trim($product_info[$i]['personality']) : ''; 
                    $product_availability = isset($product_info[$i]['product_availability']) ? $product_info[$i]['product_availability'] : ''; 
                    $product_url = isset($product_info[$i]['product_url']) ? trim($product_info[$i]['product_url']) : ''; 
                    $variation_values = isset($product_info[$i]['variation_values']) ? $product_info[$i]['variation_values'] : ''; 

                    if($product_name && $category && $sub_category && $price_range && $price && $image){
                        if($this->api_details_model->unique_product_name($product_name)){
                            $ok = 0;
                        } else{ $ok = 1; }
                        $cat_id = $this->api_details_model->get_cat_id($category);
                        if($cat_id>0 && $ok == 1){ $ok = 1;
                            $sub_cat = $this->api_details_model->get_subcat_id($cat_id,$sub_category);
                            if($sub_cat>0){ $ok = 1; }else{ $ok =0; } 
                        }
                            else{ $ok = 0; }
                        if($ok == 1){
                            $price_range_id = $this->api_details_model->get_price_range($price_range,7);  
                            if($price_range_id){ $ok = 1;} else { $ok = 0; }
                            }else{ $ok = 0; }
                        if(is_numeric($price) && $ok == 1){$ok = 1; } else{ $ok = 0; }

                        /*if($ok == 1 && !empty($product_availability)){
                            $stores_id = $this->api_details_model->get_store_id($product_availability); 
                            if(!empty($stores_id)){   $ok = 1;  }else{ $ok = 0; }
                        } else{ $ok = 0; }*/
                        
                      
                        if($ok == 1){
                            $old_imagename = basename($image);  
                                                     
                            $ext = pathinfo($old_imagename, PATHINFO_EXTENSION);                                    
                            
                            if(!in_array($ext,$allowed_ext)){ $ok = 0;  }else{
                                $newimagename = time().mt_rand(0,10000).'.'.$ext; 
                                //$resultimg = copy($image,'./assets/products/'.$newimagename);  
                                //if(!copy($image,'./assets/products/'.$newimagename))
                                if(!file_get_contents($image))
                                                {
                                                   $ok =0; /*-- Create Uploded image thumbnil --*/
                                                    //$company_logo= $this->upload->data('file_name')
                                                }else
                                                {
                                                	file_put_contents('./assets/products/'.$newimagename, file_get_contents($image));
                                                   $config['image_library'] = 'gd2';
                                                    $config['source_image'] = './assets/products/'.$newimagename;
                                                    $config['new_image'] = './assets/products/thumb_160/';
                                                    $config['create_thumb'] = TRUE;
                                                    $config['thumb_marker'] = '';
                                                    $config['maintain_ratio'] = TRUE;
                                                    $config['width'] = 160;
                                                    //$config['height'] = 200;

                                                    $this->image_lib->initialize($config);

                                                    if (!$this->image_lib->resize())
                                                    {
                                                        $this->image_lib->display_errors();
                                                        $ok =0;

                                                    }else{

                                                         $ok =1;
                                                        }


                                                        $config['image_library'] = 'gd2';
                                                    $config['source_image'] = './assets/products/'.$newimagename;
                                                    $config['new_image'] = './assets/products/thumb_310/';
                                                    $config['create_thumb'] = TRUE;
                                                    $config['thumb_marker'] = '';
                                                    $config['maintain_ratio'] = TRUE;
                                                    $config['width'] = 310;
                                                    //$config['height'] = 200;

                                                    $this->image_lib->initialize($config);

                                                    if (!$this->image_lib->resize())
                                                    {
                                                        $this->image_lib->display_errors();
                                                        $ok =0;

                                                    }else{

                                                         $ok =1;
                                                        }

                                                             $config['image_library'] = 'gd2';
                                                    $config['source_image'] = './assets/products/'.$newimagename;
                                                    $config['new_image'] = './assets/products/thumb/';
                                                    $config['create_thumb'] = TRUE;
                                                    $config['thumb_marker'] = '';
                                                    $config['maintain_ratio'] = TRUE;
                                                    $config['width'] = 200;
                                                    $config['height'] = 200;

                                                    $this->image_lib->initialize($config);

                                                    if (!$this->image_lib->resize())
                                                    {
                                                        $this->image_lib->display_errors();
                                                        $ok =0;

                                                    }else{

                                                         $ok =1;
                                                        }
                                                }
                                        }
                                    }
                                   

                        if($ok == 1){
                       $brand_name_id = $this->api_details_model->get_brand_id($brand_name,$decrypt_api_key,$decrypt_secrete_key); 
                       $target_market_id = $this->api_details_model->get_target_market_id($target_market);
                       $body_shape_id = $this->api_details_model->get_price_range($body_shape,1) ? $this->api_details_model->get_price_range($body_shape,1): 0;
                        $style_id = $this->api_details_model->get_price_range($personality,2) ? $this->api_details_model->get_price_range($personality,2): 0; 
                        $body_type_id = $this->api_details_model->get_price_range($body_type,10) ? $this->api_details_model->get_price_range($body_type,10): 0;
                        $age = $this->api_details_model->get_price_range($age_group,8) ? $this->api_details_model->get_price_range($age_group,8) : 0;

                        if(strcasecmp($consumer_type,"Maternity &amp; Baby") == 0){
                            $product_consumer_type = "Maternity &amp; Baby";
                        }else if(strcasecmp($consumer_type,"Female") == 0){
                            $product_consumer_type = "Female";
                        }else if(strcasecmp($consumer_type,"Male") == 0) {
                            $product_consumer_type = "Male";
                        }else if(strcasecmp($consumer_type,"Kidswear") == 0){
                            $product_consumer_type = "Kidswear";
                        }else if(strcasecmp($consumer_type,"Plus Male") == 0){ 
                            $product_consumer_type = "Plus Male";
                        }else if(strcasecmp($consumer_type,"Plus Female") == 0){
                            $product_consumer_type = "Plus Female";
                        }


                        if(strcasecmp($body_part,"Head gear / Hair") == 0){ $body_part_info = 'Head gear / Hair'; }
                        if(strcasecmp($body_part,"Ears") == 0){ $body_part_info = 'Ears'; }
                        if(strcasecmp($body_part,"Eyes") == 0){ $body_part_info = 'Eyes'; }
                        if(strcasecmp($body_part,"Nose") == 0){ $body_part_info = 'Nose'; }
                        if(strcasecmp($body_part,"Neck") == 0){ $body_part_info = 'Neck'; }
                        if(strcasecmp($body_part,"Arms") == 0){ $body_part_info = 'Arms'; }
                        if(strcasecmp($body_part,"Wrist") == 0){ $body_part_info = 'Wrist'; }
                        if(strcasecmp($body_part,"Fingers") == 0){ $body_part_info = 'Fingers'; }
                        if(strcasecmp($body_part,"Torso") == 0){ $body_part_info = 'Torso'; }
                        if(strcasecmp($body_part,"Feet") == 0){ $body_part_info = 'Feet'; }
                        if(strcasecmp($body_part,"Shoes") == 0){ $body_part_info = 'Shoes'; }
                        if(strcasecmp($body_part,"Accessories") == 0){ $body_part_info = 'Accessories'; }
                        
                      
                       $stores_id = $this->api_details_model->get_store_id($product_availability); 
                        
                       $data['product_info'] = array(
                                    'name'=> addslashes((strip_tags($product_name))),
                                    'description' => addslashes((strip_tags($description))),
                                    'brand_id' =>$brand_name_id,
                                    'url' =>$product_url,
                                    'product_cat_id' =>$cat_id,
                                    'product_sub_cat_id' =>$sub_cat,
                                    'price_range' =>addslashes((strip_tags($price_range_id))),
                                    'price' =>$price, 
                                    'target_market' =>$target_market_id,
                                    'consumer_type' =>addslashes((strip_tags($product_consumer_type))),
                                    'rating' =>is_numeric($rating) ? $rating:0,
                                    'body_part' =>$body_part_info,
                                    'body_shape' =>$body_shape_id,
                                    'personality' =>$style_id,
                                    'body_type'=>$body_type_id,
                                    'age_group' =>$age,
                                    'status' =>0,
                                    'is_promotional' =>0,
                                    'created_by' => $this->api_details_model->get_brand_info($decrypt_api_key,$decrypt_secrete_key),
                                    'created_datetime'=>date('Y-m-d H:i:s'),
                                    'image'=>$newimagename,
                                    'slug'=>str_replace(' ','-',strtolower($product_name))
                                );
                            $tags_id = $this->api_details_model->get_tags_id($tags);
                            $data['product_tags_array'] =  $tags_id;
                            $data['stores'] =  $stores_id;  
                            $variation_values_array = $this->api_details_model->get_variations_id($variation_values); 
                            $data['variation_val_array'] =  $variation_values_array;
                            $data['product_avail_array'] =  $stores_id;

                            if($this->api_details_model->add_products($data,$this->api_details_model->get_brand_info($decrypt_api_key,$decrypt_secrete_key))){
                                $ok =1; $h++;
                            }else{
                                $ok = 0;
                            }

                      }
                        if($ok == 0){ $product_error[] = $i; }
                    }else{

                       $product_error[] = $i;
                    }

                    unset($product_name,$description,$category,$sub_category,$image,$price,$price_range,$consumer_type,$rating,$body_part,$age_group,$brand_name,$tags,$target_market,$body_type,$body_shape,$personality,$product_availability,$product_url,$variation_values);
                }

        }
        if(!empty($product_error)){ 
            $k = 0;
            $api_message = count($product_error).' Rows are not inserted [';
            foreach($product_error as $val){
                if($k!=0){  $api_message = $api_message. ',' ; }
                $api_message = $api_message. ($val+1);
                $k++;
            }
            $api_message = $api_message. ']';
            $api_status = TRUE;
        }else{
           $api_message = $api_message.'Uploaded Products :-'.$h; 
           $api_status = TRUE; 
        }
        $this->set_response(['status' => $api_status,'message' => $api_message], REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function products_details_delete($id_param = NULL)
    {
        
    }

    public function product_cpa_track_post(){
        $api_message = ''; $api_status='false';  $ok = 0;
        $api_key = $this->post('api_key');
        $api_secret_key = $this->post('secret_key');  
        $product_info = $this->post('product_info');

        $product_id = isset($product_info[0]['product_id']) ? $product_info[0]['product_id'] : 0;
        $sc_price = isset($product_info[0]['price']) ? $product_info[0]['price'] : 0;

        // echo $this->encryptOrDecrypt('47','encrypt');
        if(trim($api_key) == ''){ $api_message = $api_message.'API key is missing'; $api_status='false';  $ok = 0; }else{ $ok = 1; $decrypt_api_key = $this->encryptOrDecrypt($api_key,null); }
        
        if(trim($api_secret_key == '') && $ok ==1 ){ $api_message = $api_message.'Secret Key is missing'; $api_status='false';  $ok = 0; }else{
            $ok = 1; $decrypt_secrete_key = $this->encryptOrDecrypt($api_secret_key,null);
        }

        if($ok ==1 && $this->api_details_model->authorized_api_user($decrypt_api_key,$decrypt_secrete_key) == 1){ $api_message = $api_message.'Not the Authorized API Credentials'; $api_status='false'; $ok = 0; }else{ $ok = 1; }
        
        if($product_id == '0' ){
            $api_message.'Product Id is missing'; $ok = 0; 
        }else{
            $ok = 1;
        }
        if($ok == 1){
            $product_ecryid = trim($this->encryptOrDecrypt($product_id,null));

            if($this->api_details_model->product_cpa_track($product_ecryid,$sc_price)){

            $api_status="true";
            $api_message="Done";
            }else{
            $api_status="false";
            $api_message="Something goes wrong";
            }

        }

        $this->set_response(['status' => $api_status,'message' => $api_message], REST_Controller::HTTP_CREATED);
    }

    public function get_excel_get(){
        $file = './assets/need-user-ids.xlsx';
        //load the excel library
        $this->load->library('excel');
        //read file from path
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        //get only the Cell Collection
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        //extract to a PHP readable array format
        $data_value="";
        $i=0;
        $j=0;
        foreach ($cell_collection as $cell) {
            if($i >= 5){
                $i=0;
                //echo ",<br/>"; 
                $j++;
            }

            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            
            $data_value[$j][] = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

            $i++;

            //header will/should be in row 1 only. of course this can be modified to suit your need.
            if ($row == 1) {
                //$header[$row][$column] = $data_value;
            } else {
                //$arr_data[$row][$column] = $data_value;
            }
        }
        $i=0;
        foreach($data_value as $row){
                $qr = $this->db->query("SELECT user_login.email,user_login.id id,user_info.contact_no phone  FROM user_info,user_login where user_login.id = user_info.user_id and user_login.email= '".$row[0]."' order by user_login.id desc limit 1;");
                $res = $qr->result_array();
                //print_r($res);exit;
                @$data_value[$i][1] = $res[0]['id'];
                @$data_value[$i][2] = $res[0]['phone'];
                $i++;
        }
        
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="users.csv";');
        
        $f = fopen('php://output', 'w');

        foreach ($data_value as $line) {
            fputcsv($f, $line, ";");
        }
        //$objPHPExcel->save($file);
    }

}