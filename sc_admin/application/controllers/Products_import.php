<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products_import extends MY_Controller {    

    function __construct(){
        parent::__construct();
        $this->load->model('Products_import_model');       

    }  
 /*---------Feedback submodule functionalities----------*/

    public function index()
    {   
      
       $read_file_content =  file_get_contents(base_url().'assets/brands_files/stylefiesta2.json');

       $json_to_array = json_decode($read_file_content);  //echo "<pre>"; print_r($json_to_array); exit;
       $allowed_ext =  array('gif','png' ,'jpg','jpeg','GIF','PNG' ,'JPG','JPEG');  $i = 0; $k = 0;
       if(!empty($json_to_array)){
       		foreach($json_to_array as $value){

       			$product_name  =strip_tags(trim($value->product_name));
       			//$image         =strip_tags(trim($value->image));
       			$price         =strip_tags(trim($value->price));
       			$product_url   =trim($value->product_url);
       			$description   =strip_tags(trim($value->description));
       			$product_stock =strip_tags(trim($value->product_stock)); 
       			//$size          =strip_tags(trim($value->size));
       			$quantity      =strip_tags(trim($value->quantity));
       			$extra_images  =strip_tags(trim($value->extra_images));
       			$product_sku   =strip_tags(trim($value->product_sku));

       			$size          =strip_tags(trim($value->size)); 
            $quantity      =strip_tags(trim($value->quantity));
       			$category      = '1';
       			$subcategory   = '1';
       			
       			//$brand_id = 20681; // Phive Revers 
       			//$brand_id = 20933; //TNG
       			//$brand_id =20734; //Travelle
       			//$brand_id = 22102; // themuslin
       			//$brand_id = 22103;
       			/*$image = explode('?', $image);
       			$image = $image[0];*/
       			$get_product_id = $this->Products_import_model->get_product_id($product_url);

       			/* Convert price from text to float*/
       			/*if($brand_id == 22103){
       				$price =  preg_replace('/[^A-Za-z0-9_%\[\]\.\(\)%&-]/s', '', $price);
       				$new_price = explode('Rs.', $price);
       				$price = $new_price[1];
       			} */

       			if($get_product_id == 0){ // Insert into the product_desc

       				/* $old_imagename = basename($image);  
                     $ext = pathinfo($old_imagename, PATHINFO_EXTENSION); 
                     $newimagename = time().mt_rand(0,10000).'.'.$ext; 

                     if(in_array($ext,$allowed_ext)){
                     file_put_contents('./assets/products/'.$newimagename, file_get_contents($image));
                     $config['image_library'] = 'gd2';
                     $config['source_image'] = './assets/products/'.$newimagename;
                     $config['new_image'] = './assets/products/thumb/';
                     $config['create_thumb'] = TRUE;
                     $config['thumb_marker'] = '';
                     $config['maintain_ratio'] = TRUE;
                     $config['width'] = 200;
                     $config['height'] = 200;
					 $this->image_lib->initialize($config);
					 $this->image_lib->resize();

					 $data = array();

					 $data['product_cat_id'] = $category;
					 $data['product_sub_cat_id'] = $subcategory;
					 $data['brand_id'] = $brand_id;
					 $data['name'] = $product_name;
					 $data['url'] = $product_url;
					 $data['description'] = $description;
					 $data['price'] = $price;
					 $data['image'] = $newimagename;
					 $data['slug'] = str_replace(' ','_',$product_name).time().mt_rand(0,10000);
					 $data['created_by'] = $brand_id;
					 $data['created_datetime'] = date('Y-m-d H:i:s');
					
					  $this->Products_import_model->add_product($data);

					  $i++;
					}*/

       			}else{ // Update the product
       				
       				/*if($price > 0){
       					$this->Products_import_model->update_price($product_url,$price);

                if($quantity > 0){
                  //$this->Products_import_model->update_quantity($product_url,$quantity);
                }
       					
       				}

       				if($description !=''){
       					$this->Products_import_model->update_description($product_url,$description);
       					
       				}*/

       				if($product_sku !=''){
       					$this->Products_import_model->update_product_sku($product_url,$product_sku);
       				}	

              if($quantity!=''){
                $this->Products_import_model->update_quantity($product_url,$quantity,$size);
              }

       				$k++;


       			}

       			
       		}
       }
       echo "Total Products :- " .$i;
       echo "<br>";
       echo "Update :- " .$k;
       echo "<br>";
       echo '--'.$product_url.'---';
	}
	


}
