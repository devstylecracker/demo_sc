<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Product extends MY_Controller {

	function __construct()
    {
        // Call the Model constructor
        
        parent::__construct();
        $this->load->model('product_model');
        $this->load->model('category_model');
        $this->load->model('brandusers_model');
        $this->load->model('store_model');
        $this->load->model('brand_model');
        $this->load->model('tag_model');        

    }

	public function index($offset = null)
	{
		if(!$this->session->userdata('user_id'))
		{
			$this->load->view('login');

		}else
		{ 
			 $data_post = array();
			 
			 if($this->has_rights(39) == 1)
			{
				$search_by = $this->input->post('search_by');
                $table_search = $this->input->post('table_search');
                $paginationUrl = 'product/index/';
                $uri_segment = 3;
                $config['per_page'] = 20;
                $config['total_rows'] = count($this->product_model->get_all_product_details($search_by,$table_search,'',''));
                $total_rows = $config['total_rows'];    
                $get_data = $this->product_model->get_all_product_details($search_by,$table_search,$offset,$config['per_page']);
                $count = $config['total_rows']; 
                $data_post['product'] = $get_data;                
                $this->customPagination($paginationUrl,$count,$uri_segment,$get_data,$total_rows);
				
				$this->load->view('common/header');
				$this->load->view('product_view',$data_post);
				$this->load->view('common/footer');

			}else
			{
				$this->load->view('common/header');
                $this->load->view('not_permission');
                $this->load->view('common/footer');

			}
			
		}
	}

	public function alpha_dash_space()
	{
		/*if ($this->input->post('product_name')){return TRUE;}
		else{
		$error = 'Please select Store Type';
		$this->form_validation->set_message('The field may only contain alpha-numeric characters, underscores, and dashes/spaces.', $error);
		return FALSE;
		}*/
		$str = $this->input->post('product_name');
		//$result = ( ! preg_match("/^([-a-z_ ])+$/i", $str)) ? FALSE : TRUE;		
		$error = 'The field may only contain alpha-numeric characters, underscores, and dashes/spaces.';
		$this->form_validation->set_message('product_name', $error);
	    //return ( ! preg_match("/^([-a-z_ ])+$/i", $str)) ? FALSE : TRUE;

	}
	
	public function product_addedit($id=NULL)
	{
			$data_post = array();	
			
			$this->load->view('common/header');		
			//print_r($this->input->post());
			//category select box section			
			$cat = $this->category_model->get_all_categories();
			$data_post['category'] = $cat;			
			//category ends

			//select the body part 
			 
			$body_part = array(
						'' => 'Select Body Part',
						'Head gear / Hair' => 'Head gear / Hair',
						'Ears' => 'Ears',
						'Eyes' => 'Eyes',
						'Nose' => 'Nose',
						'Neck' => 'Neck',
						'Arms' => 'Arms',
						'Wrist' => 'Wrist',
						'Fingers' => 'Fingers',
						'Torso' => 'Torso',
						'Feet' => 'Feet',
						'Shoes' => 'Shoes',
						'Accessories' => 'Accessories'
						);

			$data_post['body_part'] = $body_part;
			//body part ends

			//select the body shape from answer and questions table
			$body_shape = $this->product_model->load_questAns(1);
			$data_post['body_shape'] = $body_shape;
			//body shape ends

			//select the body shape from answer and questions table
			//load body type
			$body_type = $this->product_model->load_questAns(10);
			$data_post['body_type'] = $body_type;
			//body type ends

			//load personality			
			$personality = $this->product_model->load_questAns(2);
			$data_post['personality'] = $personality;
			//personality end

			//load age			
			$age = $this->product_model->load_questAns(8);
			$data_post['age'] = $age;
			//age end

			//load budget			
			$budget = $this->product_model->load_questAns(7);
			$data_post['budget'] = $budget;
			//budget end   

             //load consumer type
			$consumer_type = array(
						'' => 'Select Consumer Type',
						'Maternity &amp; Baby' => 'Maternity &amp; Baby',
						'Female' => 'Female',
						'Male' => 'Male',
						'Kidswear' => 'Kidswear',
						'Plus Male' => 'Plus Male',
						'Plus Female' => 'Plus Female'						
						);			
			$data_post['consumer_type'] = $consumer_type;	
			// consumer type end      


			//load target market
			$target_market = $this->brandusers_model->target_market();			
			$data_post['target_market'] = $target_market;	
			// target market end
			
			//load stores
			$stores = $this->product_model->get_store_info();
			$data_post['stores'] = $stores;					
			// stores end

			//load brand names
			//$brand = $this->brand_model->get_all_tag();
			$brand = $this->store_model->all_brands();
			$data_post['brand'] = $brand;		
			//brand names end

			//load tag
			$tag = $this->tag_model->get_all_tag();
			$data_post['tag'] = $tag;	
			//tag end

			if($id == NULL)
			{
					if($this->has_rights(40) == 1)
					{

						if($this->input->post())
						{ 
							
							$this->form_validation->set_rules('product_name', 'Product Name', 'required|trim|min_length[2]|max_length[75]|callback_alpha_dash_space|is_unique[product_desc.name]');
							$this->form_validation->set_rules('category','Category','required|callback_check_category');
							$this->form_validation->set_rules('product_avail','Product Availability','callback_check_price_range');
							$this->form_validation->set_rules('budget','Price Range','required');
							$this->form_validation->set_rules('product_desc','Product Description','prep_for_form');
							$this->form_validation->set_rules('sub_category', 'Sub Category', 'required|callback_check_sub_category');
							$this->form_validation->set_rules('product_price','Price','required|is_natural');
							$this->form_validation->set_rules('product_url', 'Product URL', 'prep_url|prep_for_form');
							$this->form_validation->set_rules('active','Active','required');
							$this->form_validation->set_rules('promotional','Promotional','required');
							
							$this->form_validation->set_message('check_category', 'Please select the category');					
							$this->form_validation->set_message('check_sub_category', 'Please select the sub category');			
							$this->form_validation->set_message('product_price', 'Price is required');
							$this->form_validation->set_message('budget', 'Please select Price Range');	

							$config['upload_path'] = './assets/products/';
							$config['allowed_types'] = 'gif|jpg|png|jpeg';
							$ext = pathinfo($_FILES['images']['name'], PATHINFO_EXTENSION);
							$img_name = time().mt_rand(0,10000).'.'.$ext;
							//echo $img_name ;exit;					
							//$config['file_name'] = time().rand(0,10000);

							$ok=0;

							$config['file_name'] = $img_name;
							if ($this->form_validation->run() == FALSE)
							{
								//if validation error happens
				                           			
							}
							else
							{ 
								/*if (!empty($_POST['email'])){
								    if ($this->verifyInput($_POST['email'], 6)){
								        $fill['email'] = $_POST['email'];//$fill is the array of values that passed validation
								    } else $tally++;//$tally keeps a running count of the total number of erors
								}*/
								if (empty($_FILES['images']['name']))
								{

								    $data_post['product_image_error'] = 'No files found for upload.'; 
								    // or you can throw an exception 
								    //return; // terminate 
								    //exit;
								}
								 else{

								 		$ok = 1;

								 		if($_FILES['images']['name'])
								 		{
								 			$this->upload->initialize($config); 
											if(!($this->upload->do_upload('images')))
											{
												 $data_post['error'] = $this->upload->display_errors(); 
												
												 //redirect('/Product/product_addedit','refresh');
												//$this->load->view('common/header',$data_post);
												$this->load->view('product',$data_post);
												//$this->load->view('common/footer');
												$ok =0;

										    }else
										    {
												$product_img= $this->upload->data('file_name');
												if($product_img!="")
												{
													$config['image_library'] = 'gd2';											
													$config['source_image'] = './assets/products/'.$product_img;
													$config['new_image'] = './assets/products/thumb/';
													$config['create_thumb'] = TRUE;
													$config['thumb_marker'] = '';
													$config['maintain_ratio'] = TRUE;
													$config['width'] = 200;
													$config['height'] = 200;											

													$this->image_lib->initialize($config); 

													if (!$this->image_lib->resize())
													{
													     $this->image_lib->display_errors();

													}else
													{

													}
													
												}
												
											}
										}	
										if($ok == 1)
										{						

											// get the files posted											
											$img_name = $config['file_name'];
											$product_formFields = $this->input->post();
											$res = $this->product_model->insert_product($product_formFields,$img_name); 
											
											if($res == 1){ 

												//$this->load->view('product',$data_post);
												$this->session->set_flashdata('feedback', 'Product Uploaded successfully');
												redirect('/Product');
											}else
											{
												$this->session->set_flashdata('feedback', 'Error Uploaded Product');
												redirect('/Product');
											}
										   //else{
											// 	$data['error'] = "Invalid Username and Password";
											// 	$this->load->view('login',$data); 
										 // }

										}
										
									}
								}
						}
					$this->load->view('product',$data_post);
					$this->load->view('common/footer');

				}else{
							
						$this->load->view('common/header');
						$this->load->view('not_permission');
						$this->load->view('common/footer');
					}
			}else
			{
				
				if($this->has_rights(41) == 1 || $this->has_rights(43) == 1 )
				{

					$data_post['productData'] = $this->product_model->get_all_product_details_id($id);
					$data_post['prdtag'] = $this->product_model->get_tag_byprdid($id);
					$data_post['prdstore'] = $this->product_model->get_store_byprdid($id);
					$data_post['prdvar'] =  $this->product_model->get_productvar_byprdid($id);
					
					if($this->input->post())
					{ //echo "<pre>";print_r($data_post['productData']);exit;
							
							$this->form_validation->set_rules('product_name', 'Product Name', 'required|trim|min_length[2]|max_length[75]|callback_alpha_dash_space|is_unique[product_desc.name]');
							$this->form_validation->set_rules('category','Category','required|callback_check_category');
							$this->form_validation->set_rules('product_avail','Product Availability','callback_check_price_range');
							$this->form_validation->set_rules('budget','Price Range','required');
							$this->form_validation->set_rules('product_desc','Product Description','prep_for_form');
							$this->form_validation->set_rules('sub_category', 'Sub Category', 'required|callback_check_sub_category');
							$this->form_validation->set_rules('product_price','Price','required|is_natural');
							$this->form_validation->set_rules('product_url', 'Product URL', 'prep_url|prep_for_form');
							$this->form_validation->set_rules('active','Active','required');
							$this->form_validation->set_rules('promotional','Promotional','required');
							
							$this->form_validation->set_message('check_category', 'Please select the category');					
							$this->form_validation->set_message('check_sub_category', 'Please select the sub category');			
							$this->form_validation->set_message('product_price', 'Price is required');
							$this->form_validation->set_message('budget', 'Please select Price Range');	

							$config['upload_path'] = './assets/products/';
							$config['allowed_types'] = 'gif|jpg|png|jpeg';
							//$ext = pathinfo($_FILES['images']['name'], PATHINFO_EXTENSION);
							//$img_name = time().mt_rand(0,10000).'.'.$ext;
							//$img_name = time().mt_rand(0,10000).'.'.$ext;
							  $img_name = $data_post['productData'][0]->image;
							  		
							$config['file_name'] = $img_name;
							if ($this->form_validation->run() == FALSE)
							{
								//if validation error happens
				                         			
							}
							else
							{ 							
								if ($_FILES['images']['name'] == '')
								{	  	

								    //$data_post['product_image_error'] = 'No files found for upload.'; 
								    // or you can throw an exception 
								    //return; // terminate 
								    //exit;
								    		$img_name = $config['file_name'];
											$product_formFields = $this->input->post();
											$res = $this->product_model->update_product($product_formFields,$img_name,$id); 

											if($res == 1){ 
												
												$this->session->set_flashdata('feedback', 'Product Updated successfully');
												redirect('/Product');
											}
								}
								 else{

								 		$ok = 1;

								 		if($_FILES['images']['name'] )
								 		{
								 			$this->upload->initialize($config); 
											if(!($this->upload->do_upload('images')))
											{
												 $data_post['error'] = $this->upload->display_errors(); 
												 //redirect('/Product','refresh');
												//$this->load->view('common/header',$data_post);
												//$this->load->view('product',$data_post);
												//$this->load->view('common/footer');
												$ok =0;

										    }else
										    {
												$product_img= $this->upload->data('file_name');
												if($product_img!="")
												{
													$config['image_library'] = 'gd2';											
													$config['source_image'] = './assets/products/'.$product_img;
													$config['new_image'] = './assets/products/thumb/';
													$config['create_thumb'] = TRUE;
													$config['thumb_marker'] = '';
													$config['maintain_ratio'] = TRUE;
													$config['width'] = 200;
													$config['height'] = 200;											

													$this->image_lib->initialize($config); 

													if (!$this->image_lib->resize())
													{
													     $this->image_lib->display_errors();

													}else
													{

													}
													
												}
												
											}
										}

										if($ok == 1)
										{	
											// get the files posted											
											$img_name = $config['file_name'];
											$product_formFields = $this->input->post();
											$res = $this->product_model->update_product($product_formFields,$img_name,$id); 

											if($res == 1){ 
												
												$this->session->set_flashdata('feedback', 'Product Updated successfully');
												redirect('/Product');
											}
										   

										}
										
									}
								}
						}
						
					if($this->uri->segment(4) =='verify')
					{
						$data_post['verify']=true;
						$this->load->view('product_details',$data_post);

					}else
					{
						$this->load->view('product',$data_post);
					}				
					$this->load->view('common/footer');


				}else
				{

					$this->load->view('common/header');
					$this->load->view('not_permission');
					$this->load->view('common/footer');

				}


			}
	}


	/*public function product_verify($id=NULL)
	{
		$data_post['productData'] = $this->product_model->get_all_product_details_id($id);
		$data_post['prdtag'] = $this->product_model->get_tag_byprdid($id);
		$data_post['prdstore'] = $this->product_model->get_store_byprdid($id);
		$data_post['prdvar'] =  $this->product_model->get_productvar_byprdid($id);

		$this->load->view('common/header');
		$this->load->view('product_verify',$data_post);
		$this->load->view('common/footer');

	}*/


	 function delete_product()
    {   

        $prodID = $this->input->post('prodID');  

        $deleted = $this->product_model->delete_product($prodID);
        //echo $deleted;exit;
        if($deleted)
        {
            $this->session->set_flashdata('feedback', 'Record has been deleted successfully');
            //redirect('/product','refresh');
        }
        else
        {           
            $this->session->set_flashdata('feedback', 'The server is not responding, try again later');  
            //redirect('/product','refresh');    
        }
    }


function approveReject_product()
   {        
       $prodID = $this->input->post('prodID'); 
       $apr_status = $this->input->post('apr_status');
       $reason = $this->input->post('reason');


       if($apr_status == 'approved')
       {
       	$prdSave = product_addedit($prodID);
       	if($prdSave)
       	{
       	$result = $this->product_model->approve_product($prodID);  
       	$this->session->set_flashdata('feedback', '<div class="callout callout-success"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Product has been Approved successfully</div>');
           redirect('/product');      	
       	}
   	
   	}else if($apr_status == 'reject')
   	{
   	$result = $this->product_model->reject_product($prodID,$reason);
   	if($result){ 
echo 1; 
$this->session->set_flashdata('feedback', '<div class="callout callout-success"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Product has been Rejected successfully</div>');
           redirect('/product');




}
   	}else{
          
           $this->session->set_flashdata('feedback', '<div class="callout callout-danger"><h5><i class="icon fa fa-ban"></i><strong>Error!</strong></h5>Product has been Pending</div>');  
           redirect('/product'); 
           //return false;   
       }

      
       //echo $result;

      
   }
	

	
	public function check_category($post_string)
	{// to check whether the category select box is selected
	  return $post_string == '0' ? FALSE : TRUE;
	}

	public function check_sub_category($post_string)
	{// to check whether the category select box is selected
	  return $post_string == '0' ? FALSE : TRUE;
	}

	public function check_price_range($post_string)
	{
		// to check whether the price range  selected
	  return $post_string == 'Select Some Options' ? FALSE : TRUE;
	}

	public function get_sub_category(){
	//// to get the subcategories as per category select		
        $cat_id=$this->input->post('catid');
        $subcat = $this->category_model->get_sub_categories($cat_id);       
        echo json_encode($subcat);
	}

	public function get_variation_type(){
	//// to get the subcategories as per category select
		$subcategory_id = $this->input->post('subcategory');		
        $variationtype = array();       
        $variationtype = $this->category_model->get_variationtype($subcategory_id);
        echo json_encode($variationtype);
	}

	public function get_variation_value(){

	//// to get the subcategories as per category select
		$variation_type_id=$this->input->post('variation_type');		
        $variationvalue = array();        
        $variationvalue = $this->category_model->get_variationvalue($variation_type_id);        
        echo json_encode($variationvalue);
	}

	

}
?>
