<?php
/*
*
  This controller is basically for the dashboard report  - User Related All the Dashboard functions 
  are return in this controller
*/

defined('BASEPATH') OR exit('No direct script access allowed');

class Brand_report_details extends MY_Controller {
	
	function __construct(){
        parent::__construct(); 
        $this->load->model('reports_model'); 
        $this->load->model('brand_details_info_model');
        
    }  

	public function index()
	{
    $data = array();
    $data['brand_id'] = $this->uri->segment(3);
    $data['brand_name'] = $this->brand_details_info_model->brand_name($data['brand_id'] );

    $date_from = '';  $date_to = '';

    if($this->input->post()){
           $date_from = $this->input->post('date_from');
           $date_to = $this->input->post('date_to');
    }

    $data['product_uploaded'] = $this->brand_details_info_model->product_uploaded_info($data['brand_id'],$date_from,$date_to);
    $data['product_used'] = $this->brand_details_info_model->product_used_info($data['brand_id'],$date_from,$date_to);
    $data['product_click_info'] = $this->brand_details_info_model->product_click_info($data['brand_id'],$date_from,$date_to);
    $data['product_cpa_info'] = $this->brand_details_info_model->product_cpa_info($data['brand_id'],$date_from,$date_to);
    
    $this->load->view('common/header');
    $this->load->view('reports/brand_details_info',$data);
    $this->load->view('common/footer');
  
  }

}
?>