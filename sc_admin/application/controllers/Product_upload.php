<?php
//ini_set('memory_limit', '-1');
ini_set('memory_limit', '2048M');     // OK - 2048MB
set_time_limit (500);

defined('BASEPATH') OR exit('No direct script access allowed');
class Product_upload extends MY_Controller {
	function __construct(){
        parent::__construct();   
        $this->load->library('excel');
        $this->load->model('product_upload_model');
        $this->load->model('productnew_model');
       // $this->load->model('product_model');
        $this->load->model('category_model');
        $this->load->model('store_model');  
        $this->load->model('brandusers_model');
        $this->load->model('tag_model');    
        $this->load->model('SCProductinventory_model');
    }



	public function index($offset = null)
	{

		if(!$this->session->userdata('user_id'))
		{

			$this->load->view('login');
		}else
		{ 
			 if($this->has_rights(39) == 1)
			{			

				$data_post = array();

				$cat = $this->category_model->get_all_categories();

				$data_post['category'] = $cat;

				$subcat = $this->product_upload_model->getall_subcategories();

				$data_post['subcategory'] = $subcat;

				$budget = $this->productnew_model->load_questAns(7);				

				$data_post['pricerange'] = $budget;

				$age = $this->productnew_model->load_questAns(8);

				$data_post['agegroup'] = $age;

				$data_post['brand_data'] = $this->store_model->all_brands();

				$data_post['style'] = $this->productnew_model->load_questAns(2);			

				$data_post['body_shape'] = $this->productnew_model->load_questAns(1);

				$data_post['body_type'] = $this->productnew_model->load_questAns(10);

				$data_post['tag_data'] = $this->tag_model->get_all_tag('','','','');

				$data_post['target_market'] = $this->brandusers_model->target_market();

			 	$data_post['all_stores'] = $this->productnew_model->all_store();

			 	$data_post['variation'] = $this->product_upload_model->getall_variation();	

				$this->load->view('common/header');

				$this->load->view('product/product_upload1',$data_post);

				//$this->load->view('product/product_upload');

				$this->load->view('common/footer');

			}else
			{

				$this->load->view('common/header');

                $this->load->view('not_permission');

                $this->load->view('common/footer');

			}			

		}

	}

	public function product_upload()
	{
		if(!$this->session->userdata('user_id'))
		{

			$this->load->view('login');

		}else{
				unset($data_post);

				unset($errorarray);	

			 $data_post = array();
			 $imagemsg = "";
			 $excelmsg = "";
			 $msg = "";
			 $ok = 0;
			 $fieldorder = 0;
			 $errorarray = array();			

			/*$cat = $this->category_model->get_all_categories();
			//echo "<pre>";print_r($cat);exit;

			$data_post['category'] = $cat;

			$subcat = $this->product_upload_model->getall_subcategories();			

			$data_post['subcategory'] = $subcat;*/

			$budget = $this->productnew_model->load_questAns(7);

			$data_post['pricerange'] = $budget;

			$age = $this->productnew_model->load_questAns(8);

			$data_post['agegroup'] = $age;

			$data_post['brand_data'] = $this->store_model->all_brands();

			$data_post['tag_data'] = $this->tag_model->get_all_tag('','','','');			

			$data_post['target_market'] = $this->brandusers_model->target_market();

			 $data_post['all_stores'] = $this->productnew_model->all_store();

			 $data_post['body_shape'] = $this->productnew_model->load_questAns(1);			

			 $data_post['style'] = $this->productnew_model->load_questAns(2);			

			 $data_post['body_type'] = $this->productnew_model->load_questAns(10);

			/* $data_post['variation'] = $this->product_upload_model->getall_variation();	*/



			 if($this->has_rights(39) == 1){

				if($this->input->post('Save') == 'Save')
				{		

				 	if($_FILES['product_upload']['tmp_name'] != "")
				 	{

				 		// This is the file path to be uploaded.

						$inputFileName = $_FILES['product_upload']['tmp_name'];	

				 	}else
				 	{					 			 		

				 		redirect('/product_upload');

				 	}

					/*if($_FILES['product_upload']['size'] > 5000000)

					{

						$excelmsg = "Error Excel File size is greater";

					}*/

					$handle = fopen($inputFileName, "r");
					

					try{
							//read file from path

							$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);



					}catch(Exception $e) {

						die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());

					}

					$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

					$arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet					



					/*$excelfields =array('product_name','description','category','sub_category','image','price','price_range','consumer_type','rating','body_part','age_group','store','tags','target_market','body_type','body_shape','personality','brand','product_url','variation_values');*/

					$excelfields =array('product_color','parent_category','child_category','store','product_name','product_url','description','price','product_discount','discount_start_date','discount_end_date','consumer_type','ratings','body_part','age_group','target_market','body_type','body_shape','personality','attributes_value','brand','image','product_inventory','tags');


/*--Condition for checking the order of Field name is same as given in sample cue sheet --*/

if(trim($allDataInSheet[1]["A"]) != $excelfields[0])
{ $fieldorder = 1; }else{ 

	 if(trim($allDataInSheet[1]["B"]) != $excelfields[1])

 	 { $fieldorder = 1;}else{	

	 		if(trim($allDataInSheet[1]["C"]) != $excelfields[2])

			{ $fieldorder = 1;	}else{	

				if(trim($allDataInSheet[1]["D"]) != $excelfields[3])

				{	$fieldorder = 1; }else{	

					if(trim($allDataInSheet[1]["E"]) != $excelfields[4])

					{ $fieldorder = 1;	}else{	

						if(trim($allDataInSheet[1]["F"]) != $excelfields[5])

						{	$fieldorder = 1; }else{	

							if(trim($allDataInSheet[1]["G"]) != $excelfields[6])

							{	$fieldorder = 1; }else{	

								if(trim($allDataInSheet[1]["H"]) != $excelfields[7])

								{	$fieldorder = 1; }else{

									if(trim($allDataInSheet[1]["I"])!= $excelfields[8])

									{	$fieldorder = 1; }else{	

										if(trim($allDataInSheet[1]["J"]) != $excelfields[9])

										{	$fieldorder = 1; }else{	

											if(trim($allDataInSheet[1]["K"])!= $excelfields[10])

											{ $fieldorder = 1;	}else{	

												if(trim($allDataInSheet[1]["L"]) != $excelfields[11])

												{ $fieldorder = 1;	}else{

													if(trim($allDataInSheet[1]["M"]) != $excelfields[12])

													{ $fieldorder = 1;	}else{

														if(trim($allDataInSheet[1]["N"]) != $excelfields[13])

														{ $fieldorder = 1;	}else{

															if(trim($allDataInSheet[1]["O"]) != $excelfields[14])

														{ $fieldorder = 1;	}else{

															if(trim($allDataInSheet[1]["P"]) != $excelfields[15])

														{ $fieldorder = 1;	}else{

															if(trim($allDataInSheet[1]["Q"]) != $excelfields[16])

														{ $fieldorder = 1;	}else{

															if(trim($allDataInSheet[1]["R"]) != $excelfields[17])

														{ $fieldorder = 1;	}else{

															if(trim($allDataInSheet[1]["S"]) != $excelfields[18])

														{ $fieldorder = 1;	}else{

															if(trim($allDataInSheet[1]["T"]) != $excelfields[19])

														{ $fieldorder = 1;	}else{												
																if(trim($allDataInSheet[1]["U"]) != $excelfields[20])

														{ $fieldorder = 1;	}else{
																if(trim($allDataInSheet[1]["V"]) != $excelfields[21])

														{ $fieldorder = 1;	}else{

																if(trim($allDataInSheet[1]["W"]) != $excelfields[22])

														{ $fieldorder = 1;	}else{

															if(trim($allDataInSheet[1]["X"]) != $excelfields[23])

														{ $fieldorder = 1;	}else{

														}
																				}
																			

																					}

																				}

																			}



																		}



																	}

									 							}													

								  							}

						 								}

				  									}

		 										 }

											 }

										}

									}

								}

							}

						}

					}

				}

			}

		}

	}

}


				$errorcount = 0;
				$successcount = 0;
				$resultmsg = 0;
				$excelmsg = array();
				$allowed_ext =  array('gif','png' ,'jpg','jpeg','GIF','PNG' ,'JPG','JPEG');

					if($fieldorder == 1)
					{
						//$imagemsg = '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>Please Insert in the order of the fields uploaded.</div>';

						$excelmsg = "Please Insert in the order of the fields uploaded.";
					}else
					{
						for($i=2;$i<=$arrayCount;$i++)
						{
							$imagename = "";
							$product_imges = "";
							$errorFlag = 0;
							$resultmsg = 0;
							$sel_cat_id = "";
							$sel_subcat_id = "";
							$var_id="";
							$vartype_id="";
						
							$product_color = trim($allDataInSheet[$i]["A"]);
							$category = trim($allDataInSheet[$i]["B"]);/*parent category level-1*/
							$sub_category = trim($allDataInSheet[$i]["C"]);/*child categories*/
							$store = trim($allDataInSheet[$i]["D"]);
							$product_name = trim($allDataInSheet[$i]["E"]);
							$product_url = trim($allDataInSheet[$i]["F"]);
							$description = trim($allDataInSheet[$i]["G"]);
							$price = trim($allDataInSheet[$i]["H"]);
							$product_discount = trim($allDataInSheet[$i]["I"]);
							$discount_start_date = trim($allDataInSheet[$i]["J"]);
							$discount_end_date = trim($allDataInSheet[$i]["K"]);
							$consumer_type = trim($allDataInSheet[$i]["L"]);
							$ratings = trim($allDataInSheet[$i]["M"]);
							$body_part = trim($allDataInSheet[$i]["N"]);
							$age_group = trim($allDataInSheet[$i]["O"]);
							$target_market = trim($allDataInSheet[$i]["P"]);
							$body_type = trim($allDataInSheet[$i]["Q"]);
							$body_shape = trim($allDataInSheet[$i]["R"]);
							$personality = trim($allDataInSheet[$i]["S"]);
							$attribute_value = trim($allDataInSheet[$i]["T"]);/*attribute values*/
							$brand = trim($allDataInSheet[$i]["U"]);
							$product_imges = trim($allDataInSheet[$i]["V"]);
							$product_stock = trim($allDataInSheet[$i]["W"]); 
							$product_stock = $product_stock; $product_stock1 = array();
							if($product_stock!=''){ $product_stock1 = explode(',',$product_stock ); }
							
							$tags = trim($allDataInSheet[$i]["X"]);
							
							if($this->session->userdata('role_id') == 6){
									$store_id = $this->session->userdata('user_id');
							}else{
									$store_id = $this->SCProductinventory_model->get_store_id($store);
							}

							//$brand_code = $this->SCProductinventory_model->getbrand_code($store_id);
							if($product_name==''){
								$excelmsg[] = "<b> Row ".$i."</b> : Product name is empty ";
								$errorcount++;
								$errorFlag = 1;
							}else if($product_url==''){
								$excelmsg[] = "<b> Row ".$i."</b> : Product URL is empty ";
								$errorcount++;
								$errorFlag = 1;
							}else if($category==''){
								$excelmsg[] = "<b> Row ".$i."</b> : Product Parent Category is empty ";
								$errorcount++;
								$errorFlag = 1;
							}else if($sub_category==''){
								$excelmsg[] = "<b> Row ".$i."</b> : Product Child Category is empty ";
								$errorcount++;
								$errorFlag = 1;
							}else if($price<=0 || $price==''){
								$excelmsg[] = "<b> Row ".$i."</b> : Product Price is empty ";
								$errorcount++;
								$errorFlag = 1;
							}
							else if($errorFlag==0){
								$product_color = strip_tags($product_color);
								$product_id = $this->SCProductinventory_model->get_product_id($product_color,$product_url);
								$consumer_type_value = '';

								if($age_group != "")
								{	
									//$age_group_id = '40';

									//load age		
									$age = $this->productnew_model->load_questAns(8);		
									//age end

									foreach($age as $value)
									{
										if($value['answer'] == $age_group)
										{
											$age_group_id = $value['id'];
										}
									}	
								}

								if($body_shape != "")
								{	
									//$body_shape_id = '3';
									//load body_shape	
									$bodyshape = $this->productnew_model->load_questAns(1);	
									//body_shape end	

									foreach($bodyshape as $value)
									{
										if($value['answer'] == $body_shape)
										{
											$body_shape_id = $value['id'];
										}
									}		
								}

								if($body_type != "")
								{								
									//$body_type_id = '1097';
									//load body_type
									$bodytype = $this->productnew_model->load_questAns(10);	
									//body_type end	

									foreach($bodytype as $value)
									{
										if($value['answer'] == $body_type)
										{
											$body_type_id = $value['id'];
										}
									}
								}

								if($personality != "")
								{	
									//$personality_id = '10';		

									//load style			

									$style = $this->productnew_model->load_questAns(2);							

									//style end	

									foreach($style as $value)
									{
										if($value['answer'] == $personality)
										{
											$personality_id = $value['id'];
										}
									}							

								}

								

								
									/* Add product details*/
									if($consumer_type!=''){
										if($consumer_type == 'Maternity & Baby'){ $consumer_type_value=addslashes("Maternity & Baby"); }
										elseif($consumer_type == 'Female'){ $consumer_type_value="Female"; }
										elseif($consumer_type == 'Male'){ $consumer_type_value="Male"; }
										elseif($consumer_type == 'Kidswear'){ $consumer_type_value="Kidswear"; }
										elseif($consumer_type == 'Plus Male'){ $consumer_type_value="Plus Male"; }
										elseif($consumer_type == 'Plus Female'){ $consumer_type_value="Plus Female"; }
									}

									if($target_market!=''){
										if($target_market == 'Export Surplus'){ $target_market=1; }
										elseif($target_market == 'High Street'){ $target_market=2; }
										elseif($target_market == 'Premium'){ $target_market=3; }
										elseif($target_market == 'Luxury'){ $target_market=4; }
										elseif($target_market == 'Street'){ $target_market=5; }
									}

									$data['product_desc']['product_color'] = $product_color;
					                //$data['product_cat_info']['product_cat_id'] = $category;

					                $cat_id = $this->product_upload_model->get_parentCatId($category);
					                $data['product_cat_info']['product_cat_id'] = $cat_id;
					                $childCat_id = array();
					               
					                $childCat_arr = explode(',',$sub_category);
					                foreach ($childCat_arr as $key => $value) {
					                	$childCat_id[] = $this->product_upload_model->get_childCatId($cat_id,$value);
					                }
					                $childCat_id[] = $cat_id;

					                $data['product_cat_info']['product_sub_cat_id'] = $childCat_id;
					                //$data['product_cat_info']['product_sub_cat_id'] = $sub_category;
					                $data['product_desc']['brand_id'] = $store_id;
					                $data['product_desc']['name'] = $product_name;
					                $data['product_desc']['url'] = $product_url;
					                $data['product_desc']['description'] = $description;
					                
					                $data['product_desc']['price'] = $price;
					                $data['product_desc']['price_range'] = $this->SCProductinventory_model->get_pricerangeID($price);
					                $data['product_desc']['created_by'] = $store_id;
					                $data['product_desc']['created_datetime'] = date('Y-m-d H:i:s');
					                $data['product_desc']['product_discount'] = $product_discount;
					                $data['product_desc']['discount_start_date'] = date("Y-m-d H:i:s",strtotime($discount_start_date));
					                $data['product_desc']['discount_end_date'] = date("Y-m-d H:i:s",strtotime($discount_end_date));
					                $data['product_desc']['consumer_type'] = $consumer_type;
					                $data['product_desc']['rating'] = $ratings;
					                $data['product_desc']['target_market'] = $target_market;
					                $data['product_desc']['status'] = 1;
					                $data['product_desc']['is_promotional'] = 0;
					                $data['product_desc']['is_delete'] = 0;
									$data['product_desc']['body_part'] = $body_part;
									$data['product_desc']['body_shape'] = $body_shape_id;
									$data['product_desc']['body_type'] = $body_type_id;
									$data['product_desc']['personality'] = $personality_id;
									$data['product_desc']['age_group'] = $age_group_id;
									$data['product_desc']['approve_reject'] = 'D';

									$data['product_udesc']['price'] = $price;
	                				$data['product_udesc']['price_range'] = $this->SCProductinventory_model->get_pricerangeID($price);
	                				$data['product_udesc']['product_discount'] = $product_discount;
	                				
	                				$data['product_udesc']['discount_start_date'] = date("Y-m-d H:i:s",strtotime($discount_start_date));
	                				$data['product_udesc']['discount_end_date'] = date("Y-m-d H:i:s",strtotime($discount_end_date));
					                $data['product_desc']['slug'] = str_replace(' ','-',strtolower($product_name)).'-'.time().mt_rand();

					                $tags_data = explode(",",$tags);
					                $data['product_tags']= [];
					                if(!empty($tags_data)){
						                foreach($tags_data as $tag_info){
	                						$data['product_tags'][] = $tag_info; 
	                					}
                					}

                					$brand_info = explode(",",$brand);
	                					if(!empty($brand_info)){
						                foreach($brand_info as $brands_){
	                						$data['product_brands'][] = $brands_; 
	                					}
                					}
                					
                					/*if($variation_value !="")
									{	
										$variation = $this->product_upload_model->getall_cat_rel_variation($category,$sub_category);
										//variation value end	
										$var_array = explode(',',$variation_value);								

										foreach($variation as $value)
										{	
											if(in_array($value['name'],$var_array))
											{
												$var_id[] = $value['id'];
												$vartype_id[] = $value['product_variation_type_id'];
											}
										}

									}*/
									$attribute_array = array();
									if($attribute_value !="")
									{	
										$attribute = explode(',',$attribute_value);								

										foreach($attribute as $value)
										{	
											$attribute_array[] = $this->product_upload_model->get_attributeValueId($value);
										}

										$data['attribute']['attr_mapped'] = $attribute_array;
									}									
 
									$product_imges1 = ''; $data['product_extra_images'] =array();
									if($product_imges!='' && $product_id==0) { $product_imges1 = explode(',',$product_imges); $img_inc = 0; 
					                	foreach($product_imges1 as $pimg){					                		
					                		$imagename = basename($pimg);  
					                                                     
					                        $ext = pathinfo($imagename, PATHINFO_EXTENSION);
											
											if(!in_array($ext,$allowed_ext)){ $ok = 0;  }else{
					                                $newimagename = time().mt_rand(0,10000).'.'.$ext; 
					                                if($img_inc == 0 &&  $product_id==0){ 
					                                	$data['product_desc']['image'] =  $newimagename; 
					                                	$this->saveProductImage(trim($pimg),$newimagename);

					                                }else{
					                					$data['product_extra_images'][] =  $newimagename; 
					                					$this->saveProductExtraImages(trim($pimg),$newimagename);
					                				}
					                               	//$data['product_desc']['image'] =$newimagename;			                               
					                        }

					                		$img_inc++;
					                	}
				                	}

				                	 $data['product_stock'] = array();
				                	 if(!empty($product_stock1)) {
				                	 	$img_inc = 0;  $s = 0;
					                	
					                	foreach($product_stock1 as $valsqs){
					                			$valsqsval = explode('|', $valsqs);
					                			//$data['product_stock']['size'][] = $size; 
					                			$data['product_stock']['product_sku'][$s] = $valsqsval[0]; 
					                			$data['product_stock']['size'][$s] = $valsqsval[1]; 
					                			$data['product_stock']['stock'][$s] = $valsqsval[2]; 
					                			$s++;
					                	}
					                }
					               	$product_stock1 = array();
					                /*$data['variations']['var'] = $var_id;
					                $data['variations']['vartype'] = $vartype_id;*/					               
					                $data['variations']['cat_mapped'] = $childCat_id;
					                $data['attribute']['attr_mapped'] = $attribute_array;


					            if($product_id == 0){   
                					$this->SCProductinventory_model->add_product($data);
                					$successcount++;
								}else{
									/* Insert product details*/
									$data['product_id'] = $product_id;
									$this->SCProductinventory_model->update_product($data);
                					$successcount++;
								}

							}else{
								$excelmsg[] = "<b> Row ".$i."</b> : Brand code is empty ";
								$errorcount++;
								$errorFlag = 1;
							}

							// if($excelmsg != "")

							// {

									$data_post['excelmsg'] = $excelmsg;
									$errorarray[$i]['error'] = $excelmsg;

							// }	

							}

						}

				

					 	$data_post['errorCount'] = $errorcount;

					 	$data_post['successcount'] = $successcount;

					 	

					 	//$errorFlag == 0

					 	if($errorcount ==0 )

					 	{

					 		$msg = '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>Record has been added successfully: '.$successcount.' <br/> Error inserting records : '.$errorcount.' </div>';

					 		 //redirect('/product_new','refresh');

					 	}else

					 	{

					 		$msg = '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> Record has been added successfully: '.$successcount.' <br/> Error inserting records : '.$errorcount.'</div>';

					 		 

					 	}

				

					    $this->session->set_flashdata('feedback',$msg);

						$this->load->view('common/header');

						//$this->load->view('product/product_upload',$data_post);

						$this->load->view('product/product_upload1',$data_post);

						$this->load->view('common/footer');

				}			 



			 }else{

				 

				$this->load->view('common/header');

                $this->load->view('not_permission');

                $this->load->view('common/footer');

			}

		}

		}





		/*-- Code to download CueSheet --*/



		public function download_cuesheet(){


			$this->load->helper('download');

			$data = file_get_contents(base_url()."assets/cue_sheet/sample_product_cue_sheet.xlsx"); // Read the file's contents			

			$name = 'sample_product_cue_sheet.xlsx';

			// $data = file_get_contents("./assets/cue_sheet/Cue_sheet_documentation.xlsx"); // Read the file's contents			

			// $name = 'sample_product_cue_sheet.xlsx';

			$result = force_download($name, $data);



			if($result)
			{

				// $this->session->set_flashdata('feedback', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Sample CueSheet downloaded successfully!</div>');

			}else
			{				

				$msg = '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5> Downloading Sample Cue Sheet</div>';

				$this->session->set_userdata('feedback',$msg);

			}

		}

		public function saveProductImage($pimg,$newimagename){ 
    	 /*if($pimg!='')
		        {
		            $ok =0;
		        }else{*/
		            
		            file_put_contents('./assets/products/'.$newimagename, file_get_contents($pimg));
		            $config['image_library'] = 'gd2';
		            $config['source_image'] = './assets/products/'.$newimagename;
		            $config['new_image'] = './assets/products/thumb/';
		            $config['create_thumb'] = TRUE;
		            $config['thumb_marker'] = '';
		            $config['maintain_ratio'] = TRUE;
		            $config['width'] = 200;
		            $config['height'] = 200;

		            $this->image_lib->initialize($config);

		                if (!$this->image_lib->resize()){
		                        $this->image_lib->display_errors();
		                        $ok =0;
							}else{
								$ok =1;
		                        }
		        /*}*/

		        	$size = getimagesize('./assets/products/'.$newimagename);

		           /* 310 thumb creation*/
		            $config['image_library'] = 'gd2';
		            $config['source_image'] = './assets/products/'.$newimagename;
		            $config['new_image'] = './assets/products/thumb_160/';
		            $config['create_thumb'] = TRUE;
		            $config['thumb_marker'] = '';
		            $config['maintain_ratio'] = TRUE;
		            $config['width'] = 160;
		            //$config['height'] = 200;

		            $this->image_lib->initialize($config);

		                if (!$this->image_lib->resize()){
		                        $this->image_lib->display_errors();
		                        $ok =0;
							}else{
								$ok =1;
		            }

		               /* 310 thumb creation*/
		            $config['image_library'] = 'gd2';
		            $config['source_image'] = './assets/products/'.$newimagename;
		            $config['new_image'] = './assets/products/thumb_310/';
		            $config['create_thumb'] = TRUE;
		            $config['thumb_marker'] = '';
		            $config['maintain_ratio'] = TRUE;
		            $config['width'] = 310;
		            //$config['height'] = 200;

		            $this->image_lib->initialize($config);

		                if (!$this->image_lib->resize()){
		                        $this->image_lib->display_errors();
		                        $ok =0;
							}else{
								$ok =1;
		            }

		        /*thumb for 274x340 */
		        if(!empty($size))
				{
					if($size[1]<=340)
					{
						$config['image_library'] = 'gd2';
						$config['source_image'] = './assets/products/'.$newimagename;
						$config['new_image'] = './assets/products/thumb_340/';
						$config['create_thumb'] = TRUE;
						$config['thumb_marker'] = '';
						 $config['maintain_ratio'] = TRUE;
						//$config['width'] = 310;
						$config['height'] = $size[1];

						//$this->load->library('image_lib', $config);
						$this->image_lib->initialize($config);

						 if (!$this->image_lib->resize()){
			                        $this->image_lib->display_errors();
			                        $ok =0;
								}else{
									$ok =1;
			            }

					}else
					{

						$config['image_library'] = 'gd2';
						$config['source_image'] = './assets/products/'.$newimagename;
						$config['new_image'] = './assets/products/thumb_340/';
						$config['create_thumb'] = TRUE;
						$config['thumb_marker'] = '';
						 $config['maintain_ratio'] = TRUE;
						//$config['width'] = 310;
						$config['height'] = 340;

						//$this->load->library('image_lib', $config);
						$this->image_lib->initialize($config);

						 if (!$this->image_lib->resize()){
			                        $this->image_lib->display_errors();
			                        $ok =0;
								}else{
									$ok =1;
			            }
			         }
			    }

					/*thumb for 440x546  */
					if(!empty($size))
					{
						if($size[1]<=546)
						{
							$config['image_library'] = 'gd2';
							$config['source_image'] = './assets/products/'.$newimagename;
							$config['new_image'] = './assets/products/thumb_546/';
							$config['create_thumb'] = TRUE;
							$config['thumb_marker'] = '';
							$config['maintain_ratio'] = TRUE;
							//$config['width'] = 310;
							$config['height'] = $size[1];

							//$this->load->library('image_lib', $config);
							$this->image_lib->initialize($config);

							 if (!$this->image_lib->resize()){
				                        $this->image_lib->display_errors();
				                        $ok =0;
									}else{
										$ok =1;
				            }

						}else
						{
							$config['image_library'] = 'gd2';
							$config['source_image'] = './assets/products/'.$newimagename;
							$config['new_image'] = './assets/products/thumb_546/';
							$config['create_thumb'] = TRUE;
							$config['thumb_marker'] = '';
							$config['maintain_ratio'] = TRUE;
							//$config['width'] = 310;
							$config['height'] = 546;

							//$this->load->library('image_lib', $config);
							$this->image_lib->initialize($config);

							 if (!$this->image_lib->resize()){
				                        $this->image_lib->display_errors();
				                        $ok =0;
									}else{
										$ok =1;
				            }
			        	}
			        }

					/*thumb for 100x124  */
					if(!empty($size))
					{
						if($size[1]<=124)
						{
							$config['image_library'] = 'gd2';
							$config['source_image'] = './assets/products/'.$newimagename;
							$config['new_image'] = './assets/products/thumb_124/';
							$config['create_thumb'] = TRUE;
							$config['thumb_marker'] = '';
							$config['maintain_ratio'] = TRUE;
							//$config['width'] = 310;
							$config['height'] = $size[1];

							//$this->load->library('image_lib', $config);
							$this->image_lib->initialize($config);

							 if(!$this->image_lib->resize()){
				                        $this->image_lib->display_errors();
				                        $ok =0;
									}else{
										$ok =1;
				            }
						}else
						{
							$config['image_library'] = 'gd2';
							$config['source_image'] = './assets/products/'.$newimagename;
							$config['new_image'] = './assets/products/thumb_124/';
							$config['create_thumb'] = TRUE;
							$config['thumb_marker'] = '';
							$config['maintain_ratio'] = TRUE;
							//$config['width'] = 310;
							$config['height'] = 124;

							//$this->load->library('image_lib', $config);
							$this->image_lib->initialize($config);

							 if (!$this->image_lib->resize()){
				                        $this->image_lib->display_errors();
				                        $ok =0;
									}else{
										$ok =1;
				            }
				         }
				    }
    }

    public function saveProductExtraImages($pimg,$newimagename){
    	 /*if($pimg!='')
		        {
		            $ok =0;
		        }else{*/
		            
		            file_put_contents('./assets/products_extra/'.$newimagename, file_get_contents($pimg));

		            $size = getimagesize('./assets/products_extra/'.$newimagename);

		            $config['image_library'] = 'gd2';
		            $config['source_image'] = './assets/products_extra/'.$newimagename;
		            $config['new_image'] = './assets/products_extra/thumb/';
		            $config['create_thumb'] = TRUE;
		            $config['thumb_marker'] = '';
		            $config['maintain_ratio'] = TRUE;
		            $config['width'] = 200;
		            $config['height'] = 200;

		            $this->image_lib->initialize($config);

		                if (!$this->image_lib->resize()){
		                        $this->image_lib->display_errors();
		                        $ok =0;
							}else{
								$ok =1;
		                        }

		            /* 160 thumb creation*/

		            $config['image_library'] = 'gd2';
		            $config['source_image'] = './assets/products_extra/'.$newimagename;
		            $config['new_image'] = './assets/products_extra/thumb_160/';
		            $config['create_thumb'] = TRUE;
		            $config['thumb_marker'] = '';
		            $config['maintain_ratio'] = TRUE;
		            $config['width'] = 160;
		            //$config['height'] = 200;

		            $this->image_lib->initialize($config);

		                if (!$this->image_lib->resize()){
		                        $this->image_lib->display_errors();
		                        $ok =0;
							}else{
								$ok =1;
		            }

		            /* 310 thumb creation*/
		            $config['image_library'] = 'gd2';
		            $config['source_image'] = './assets/products_extra/'.$newimagename;
		            $config['new_image'] = './assets/products_extra/thumb_310/';
		            $config['create_thumb'] = TRUE;
		            $config['thumb_marker'] = '';
		            $config['maintain_ratio'] = TRUE;
		            $config['width'] = 310;
		            //$config['height'] = 200;

		            $this->image_lib->initialize($config);

		                if (!$this->image_lib->resize()){
		                        $this->image_lib->display_errors();
		                        $ok =0;
							}else{
								$ok =1;
		            }
		       
		       	  /*thumb for 274x340 */
		       	  	if(!empty($size))
					{
						if($size[1]<=340)
						{
							$config['image_library'] = 'gd2';
							$config['source_image'] = './assets/products_extra/'.$newimagename;
							$config['new_image'] =  './assets/products_extra/thumb_340/';
							$config['create_thumb'] = TRUE;
							$config['thumb_marker'] = '';
							$config['maintain_ratio'] = TRUE;
							//$config['width'] = 310;
							$config['height'] =  $size[1];

							//$this->load->library('image_lib', $config);
							$this->image_lib->initialize($config);

							 if (!$this->image_lib->resize()){
				                        $this->image_lib->display_errors();
				                        $ok =0;
									}else{
										$ok =1;
				            }

						}else
						{

							$config['image_library'] = 'gd2';
							$config['source_image'] = './assets/products_extra/'.$newimagename;
							$config['new_image'] =  './assets/products_extra/thumb_340/';
							$config['create_thumb'] = TRUE;
							$config['thumb_marker'] = '';
							$config['maintain_ratio'] = TRUE;
							//$config['width'] = 310;
							$config['height'] = 340;

							//$this->load->library('image_lib', $config);
							$this->image_lib->initialize($config);

							 if (!$this->image_lib->resize()){
				                        $this->image_lib->display_errors();
				                        $ok =0;
									}else{
										$ok =1;
				            }
				         }
				    }

					/*thumb for 440x546  */
					if(!empty($size))
					{
						if($size[1]<=546)
						{
							$config['image_library'] = 'gd2';
							$config['source_image'] = './assets/products_extra/'.$newimagename;
							$config['new_image'] =  './assets/products_extra/thumb_546/';
							$config['create_thumb'] = TRUE;
							$config['thumb_marker'] = '';
							$config['maintain_ratio'] = TRUE;
							//$config['width'] = 310;
							$config['height'] =  $size[1];

							//$this->load->library('image_lib', $config);
							$this->image_lib->initialize($config);

							 if (!$this->image_lib->resize()){
				                        $this->image_lib->display_errors();
				                        $ok =0;
									}else{
										$ok =1;
				            }
						}else
						{
							$config['image_library'] = 'gd2';
							$config['source_image'] = './assets/products_extra/'.$newimagename;
							$config['new_image'] =  './assets/products_extra/thumb_546/';
							$config['create_thumb'] = TRUE;
							$config['thumb_marker'] = '';
							$config['maintain_ratio'] = TRUE;
							//$config['width'] = 310;
							$config['height'] = 546;

							//$this->load->library('image_lib', $config);
							$this->image_lib->initialize($config);

							 if (!$this->image_lib->resize()){
				                        $this->image_lib->display_errors();
				                        $ok =0;
									}else{
										$ok =1;
				            }
				         }
				    }

					/*thumb for 100x124  */
					if(!empty($size))
					{
						if($size[1]<=124)
						{
							$config['image_library'] = 'gd2';
							$config['source_image'] = './assets/products_extra/'.$newimagename;
							$config['new_image'] =  './assets/products_extra/thumb_124/';
							$config['create_thumb'] = TRUE;
							$config['thumb_marker'] = '';
							$config['maintain_ratio'] = TRUE;
							//$config['width'] = 310;
							$config['height'] = $size[1];

							//$this->load->library('image_lib', $config);
							$this->image_lib->initialize($config);

							 if (!$this->image_lib->resize()){
				                        $this->image_lib->display_errors();
				                        $ok =0;
									}else{
										$ok =1;
				            }
						}else
						{
							$config['image_library'] = 'gd2';
							$config['source_image'] = './assets/products_extra/'.$newimagename;
							$config['new_image'] =  './assets/products_extra/thumb_124/';
							$config['create_thumb'] = TRUE;
							$config['thumb_marker'] = '';
							$config['maintain_ratio'] = TRUE;
							//$config['width'] = 310;
							$config['height'] = 124;

							//$this->load->library('image_lib', $config);
							$this->image_lib->initialize($config);

							 if (!$this->image_lib->resize()){
				                        $this->image_lib->display_errors();
				                        $ok =0;
									}else{
										$ok =1;
				            }
				         }
				    }
    }

}
?>