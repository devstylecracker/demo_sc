<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
class Pa extends MY_Controller {
	
	function __construct(){
       parent::__construct();
		$this->load->model('Collection_model');
		$this->load->model('lookcreator_model'); 
		$this->load->model('tag_model');	
		$this->load->model('Pa_model');		
      
	}
   
	public function get_question_ans(){
		
		$data = array();$pa_code ='';
		$gender = $this->input->post('gender');
		/* female body shape */
		$female_body_shape = $this->Pa_model->load_questAns(1);
		$female_age = $this->Pa_model->load_questAns(8);
		$female_style = $this->Pa_model->load_questAns(2);
		$female_budget = $this->Pa_model->load_questAns(7);
		/* female body shape */
		
		/* male body shape */
		$mens_body_shape = $this->Pa_model->load_questAns(24);
		$mens_age = $this->Pa_model->load_questAns(17);
		$mens_style = $this->Pa_model->load_questAns(25);
		$mens_budget = $this->Pa_model->load_questAns(18);
		/* male body shape */
		$f_body_shape = '';
		if(!empty($female_body_shape)) {
			foreach($female_body_shape as $key=>$val){
				$f_body_shape = $f_body_shape.'<option value="'.$key.'">'.$val.'</option>';
			}
		}
		$f_age = '';
		if(!empty($female_age)) {
			foreach($female_age as $key=>$val){
				$f_age = $f_age.'<option value="'.$key.'">'.$val.'</option>';
			}
		}
		$f_style = '';
		if(!empty($female_style)) {
			foreach($female_style as $key=>$val){
				$f_style = $f_style.'<option value="'.$key.'">'.$val.'</option>';
			}
		}
		$f_budget = '';
		if(!empty($female_budget)) {
			foreach($female_budget as $key=>$val){
				$f_budget = $f_budget.'<option value="'.$key.'">'.$val.'</option>';
			}
		}
		
		$m_body_shape = '';
		if(!empty($mens_body_shape)) {
			foreach($mens_body_shape as $key=>$val){
				$m_body_shape = $m_body_shape.'<option value="'.$key.'">'.$val.'</option>';
			}
		}
		$m_age = '';
		if(!empty($mens_age)) {
			foreach($mens_age as $key=>$val){
				$m_age = $m_age.'<option value="'.$key.'">'.$val.'</option>';
			}
		}
		$m_style = '';
		if(!empty($mens_style)) {
			foreach($mens_style as $key=>$val){
				$m_style = $m_style.'<option value="'.$key.'">'.$val.'</option>';
			}
		}
		$m_budget = '';
		if(!empty($mens_budget)) {
			foreach($mens_budget as $key=>$val){
				$m_budget = $m_budget.'<option value="'.$key.'">'.$val.'</option>';
			}
		}
		
		$pa_code = $pa_code.'<div class="box-body female hide" id="female_pa">
								<div class="row">
								<div class="col-md-2">
								 <div class="form-group">
									  <label>Body Shape<span class="text-red">*</span></label>
										<select name="pa_body_shape_f" id="pa_body_shape_f"  class="form-control input-sm" multiple="true">'.$f_body_shape.'</select></div></div>
										<div class="col-md-2">
								 <div class="form-group">
									  <label>Age range<span class="text-red">*</span></label>
										<select name="pa_age_f" id="pa_age_f"  class="form-control input-sm" multiple="true">'.$f_age.'</select></div></div>
										<div class="col-md-2">
								 <div class="form-group">
									  <label>Budget<span class="text-red">*</span></label>
										<select name="pa_budget_f" id="pa_budget_f"  class="form-control input-sm" multiple="true">'.$f_budget.'</select></div></div>
										<div class="col-md-2">
								 <div class="form-group">
									  <label>Style Preference 1<span class="text-red">*</span></label>
										<select name="pa_pref1_f" id="pa_pref1_f"  class="form-control input-sm" multiple="true">'.$f_style.'</select></div></div>
										<div class="col-md-2">
								 <div class="form-group">
									  <label>Style Preference 2<span class="text-red">*</span></label>
										<select name="pa_pref2_f" id="pa_pref2_f"  class="form-control input-sm" multiple="true">'.$f_style.'</select></div></div>
										<div class="col-md-2">
								 <div class="form-group">
									  <label>Style Preference 3<span class="text-red">*</span></label>
										<select name="pa_pref3_f" id="pa_pref3_f"  class="form-control input-sm" multiple="true">'.$f_style.'</select></div></div>
								</div></div>';
										
		$pa_code = $pa_code.'<div class="box-body male hide" id="male_pa">
								<div class="row">
								<div class="col-md-2">
								 <div class="form-group">
									  <label>Body Shape<span class="text-red">*</span></label>
										<select name="pa_body_shape_m" id="pa_body_shape_m"  class="form-control input-sm" multiple="true">'.$m_body_shape.'</select></div></div>
										<div class="row">
								<div class="col-md-2">
								 <div class="form-group">
									  <label>Age range<span class="text-red">*</span></label>
										<select name="pa_age_m" id="pa_age_m"  class="form-control input-sm" multiple="true">'.$m_age.'</select></div></div>
										<div class="row">
								<div class="col-md-2">
								 <div class="form-group">
									  <label>Budget<span class="text-red">*</span></label>
										<select name="pa_budget_m" id="pa_budget_m"  class="form-control input-sm" multiple="true">'.$m_budget.'</select></div></div>
										<div class="row">
								<div class="col-md-2">
								 <div class="form-group">
									  <label>Style Preference 1<span class="text-red">*</span></label>
										<select name="pa_pref1_m" id="pa_pref1_m"  class="form-control input-sm" multiple="true">'.$m_style.'</select></div></div>
										<div class="row">
								<div class="col-md-2">
								 <div class="form-group">
									  <label>Style Preference 2<span class="text-red">*</span></label>
										<select name="pa_pref2_m" id="pa_pref2_m"  class="form-control input-sm" multiple="true">'.$m_style.'</select></div></div>
										<div class="row">
								<div class="col-md-2">
								 <div class="form-group">
									  <label>Style Preference 3<span class="text-red">*</span></label>
										<select name="pa_pref3_m" id="pa_pref3_m"  class="form-control input-sm" multiple="true">'.$m_style.'</select></div></div>
										
								</div></div>';
		echo $pa_code;
	}
	
	function get_answer_html(){
		$pa_body_shape = $this->input->post('pa_body_shape');
		$pa_age = $this->input->post('pa_age');
		$pa_budget = $this->input->post('pa_budget');
		$pa_pref1 = $this->input->post('pa_pref1');
		$pa_pref2 = $this->input->post('pa_pref2');
		$pa_pref3 = $this->input->post('pa_pref3');
		$gender = $this->input->post('gender');
		if($gender != 3){ 
			$body_shape = implode(",",$pa_body_shape);
			$pa_age = implode(",",$pa_age);
			$pa_budget = implode(",",$pa_budget);
			$pa_pref1 = implode(",",$pa_pref1);
			$pa_pref2 = implode(",",$pa_pref2);
			$pa_pref3 = implode(",",$pa_pref3);
			$body_shape_ans = $this->Pa_model->get_answer($body_shape,'Body Shape');
			$pa_age_ans = $this->Pa_model->get_answer($pa_age,'Age Range');
			$pa_budget_ans = $this->Pa_model->get_answer($pa_budget,'Budget');
			$pa_pref1_ans = $this->Pa_model->get_answer($pa_pref1,'Style Preference 1');
			$pa_pref2_ans = $this->Pa_model->get_answer($pa_pref2,'Style Preference 2');
			$pa_pref3_ans = $this->Pa_model->get_answer($pa_pref3,'Style Preference 3');
			echo $body_shape_ans.'</br></br>'.$pa_age_ans.'</br></br>'.$pa_budget_ans.'</br></br>'.$pa_pref1_ans.'</br></br>'.$pa_pref2_ans.'</br></br>'.$pa_pref3_ans.'</br>';
		}else {
			echo "All PA";
		}
	}
	
}