<?php
ini_set('memory_limit', '-1');
defined('BASEPATH') OR exit('No direct script access allowed');

class Mailchimp extends MY_Controller {

	function __construct(){
       parent::__construct();
       $this->load->model('Mailchimp_model');
       $this->load->library('email');
   	}

   	function index(){ echo 'mailChimp'; exit; }
   	
   	function newUserImport($userId)
   	{	
   		if($userId!="")
   		{
   			$logindata = $this->Mailchimp_model->getLoginUserData($userId);
   			/*echo '<pre>';print_r($logindata);exit;*/
   			$last_login = $this->Mailchimp_model->get_user_login($userId);

   			if($this->get_nonscdomain($logindata[0]['email']))
        	{
		   			$bucketname = '';
		   			if($logindata[0]['first']!='')
		   			{
		   				$bucketname = @strip_tags($logindata[0]['first']);
		   			}
		   			if($logindata[0]['second']!='')
		   			{
		   				$bucketname = $bucketname.'-'.@strip_tags($logindata[0]['second']);
		   			}
		   			 if($logindata[0]['third']!='')
		   			{
		   				$bucketname = $bucketname.'-'.@strip_tags($logindata[0]['third']);
		   			}
		   			if($logindata[0]['forth']!='')
		   			{
		   				$bucketname = $bucketname.'-'.@strip_tags($logindata[0]['forth']);
		   			}
		   			if($logindata[0]['pa_complete']!='' && $logindata[0]['pa_complete']!=0 )
		   			{
		   				$pa_complete = 1;
		   			}else
		   			{
		   				$pa_complete = 0;
		   			}
		   			/*
						'BUCKETNAME'=>@strip_tags($logindata[0]['first']).' - '.@strip_tags($logindata[0]['second']).' - '.@strip_tags($logindata[0]['third']).' - '.@strip_tags($logindata[0]['forth']),
		   			*/
			   		$data = array(
			   						'EMAIL'=>$logindata[0]['email'], 
			   						'FNAME'=>$logindata[0]['first_name'],
			   						'LNAME'=>$logindata[0]['last_name'],
			   						'GENDER'=>$logindata[0]['gender'],
			   						'AGE'=>@$logindata[0]['age'],
			   						'BUCKET'=>$bucketname,
			   						'BUCKETID'=>$logindata[0]['bucket'],
			   						'SIZE'=>$logindata[0]['size'],
			   						'USERID'=>$logindata[0]['userid'],
			   						'PLATFORM'=>$logindata[0]['platform'],
			   						'BUDGET'=>@$logindata[0]['budget'],
			   						'CONTACTNO'=>@$logindata[0]['contact_no'],
			   						'BIRTHDATE'=>@$logindata[0]['birth_date'],
			   						'LASTLOGIN'=>$last_login,
			   						'PACOMPLETE'=>$pa_complete
			   					 );
			   		/*echo '<pre>';print_r($data);exit;*/
			   		$this->syncMailchimp($data);
			}	
   		}   		
   	}

   	function syncMailchimp($data) 
   	{   		
	    //ac--scmailchimp01
		$apiKey = '534c4d6771f8ed3e03122be7c37b0aab-us14';
	    $listId = '7316bed5ee';//live=7316bed5ee Test-a84152528a

	    /*$memberId = md5(strtolower($data['email']));*/
	    $memberId = md5(strtolower($data['EMAIL']));
	    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
	    $url = 'http://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/members/' . $memberId;   

	   $json = json_encode([
	        'email_address' => $data['EMAIL'],
	        'status'        => 'subscribed', // "subscribed","unsubscribed","cleaned","pending"
	        'double_optin'  => 'FALSE',
	        'merge_fields'  => [
	            		'EMAIL'=>$data['EMAIL'], 
   						'FNAME'=>$data['FNAME']==null ? ' ' : $data['FNAME'],
   						'LNAME'=>$data['LNAME']==null ? ' ' : $data['LNAME'],
   						'GENDER'=>$data['GENDER']==null ? ' ' : $data['GENDER'],
   						'AGE'=>$data['AGE']==null ? ' ' : $data['AGE'],
   						'BUCKET'=>$data['BUCKET']==null ? ' ' : $data['BUCKET'],
   						'BUCKETID'=>$data['BUCKETID']==null ? ' ' : $data['BUCKETID'],
   						'SIZE'=>$data['SIZE']==null ? ' ' : $data['SIZE'],
   						'USERID'=>$data['USERID']==null ? ' ' : $data['USERID'],
   						'PLATFORM'=>$data['PLATFORM']==null ? ' ' : $data['PLATFORM'],
   						'BUDGET'=>$data['BUDGET']==null ? ' ' : $data['BUDGET'],
   						'CONTACTNO'=>$data['CONTACTNO']==null ? ' ' : $data['CONTACTNO'],
	   					'BIRTHDATE'=>$data['BIRTHDATE']==null ? ' ' : $data['BIRTHDATE'],
	   					'LASTLOGIN'=>$data['LASTLOGIN']==null ? ' ' : $data['LASTLOGIN'],
	   					'PACOMPLETE'=>$data['PACOMPLETE']
	        ]
	    ]);    
	  

	    $ch = curl_init($url);

	    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
	    //curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);                                                                                                                 

	    $result = curl_exec($ch);
	    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    if(curl_exec($ch) === false)
	    {
	        echo 'Curl error: ' . curl_error($ch);
	    }
	    else
	    {
	        echo 'Operation completed without any errors';
	    }

	    curl_close($ch);

	    return $httpCode;
	}

/* Ecommerce Cart */
	function ecommerceConnectStore()
	{
		
	     //ac--scmailchimp01
		$apiKey = '534c4d6771f8ed3e03122be7c37b0aab-us14';
	    $listId = '7316bed5ee';//live=7316bed5ee Test-a84152528a

	    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);	  
	    $url = 'http://' . $dataCenter . '.api.mailchimp.com/3.0/ecommerce/stores/';

	    $json = '{
				  "id" : "store_002",
				  "list_id" : '.$listId.',
				  "name" : "StyleCracker",
				  "domain" : "www.stylecracker.com",
				  "email_address" : "sudha@stylecracker.com",
				  "currency_code" : "INR"
				}';	  

	    $ch = curl_init($url);

	    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	    //curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);                                                                                                                 

	    $result = curl_exec($ch);
	    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    if(curl_exec($ch) === false)
	    {
	        echo 'Curl error: ' . curl_error($ch);
	    }
	    else
	    {
	        echo 'Operation completed without any errors';
	    }

	    curl_close($ch);
	    return $httpCode;
	}		
	
	function product_import()
	{
		$apiKey = '534c4d6771f8ed3e03122be7c37b0aab-us14';
	    $listId = '7316bed5ee';//live=7316bed5ee Test-a84152528a

	    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
	    $url = 'http://' .$dataCenter. '.api.mailchimp.com/3.0/ecommerce/stores/store_002/products';

		//$cart_result = $this->Mailchimp_model->product_import($mcstart_datetime);

		$ch = curl_init($url);

	    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	    //curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    //curl_setopt($ch, CURLOPT_POSTFIELDS, $json); 

	    $result = curl_exec($ch);
	   	$res = json_decode($result,true);
	   	
	   	echo "<pre>"; print_r($res['products']);
	   	if(!empty($res['products'])){
	   		$product_count = count($res['products']);
	   		for($i=0;$i<$product_count;$i++){
	   			$product_id = $res['products'][$i]['id'];	   			
	   			
	   			//$this->delete_product($product_id);
	   			
			   $this->update_product($product_id);		   

	   		}
	   	}else{
	   		//$this->add_product(0);
	   	}

	   	$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    if(curl_exec($ch) === false)
	    {
	        echo 'Curl error: ' . curl_error($ch);
	    }
	    else
	    {
	        echo 'Operation completed without any errors';
	    }

	    curl_close($ch);
	}

	function delete_product($product_id){
		$apiKey = '534c4d6771f8ed3e03122be7c37b0aab-us14';
	    $listId = '7316bed5ee';//live=7316bed5ee Test-a84152528a

	    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
		$url = 'http://' .$dataCenter. '.api.mailchimp.com/3.0/ecommerce/stores/store_002/products/'.$product_id;
		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
			    //curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			    //curl_setopt($ch, CURLOPT_POSTFIELDS, $json); 

		curl_exec($ch);
	}

	function add_product($all_is_true,$o_sizeid=Null,$o_sizetext=Null){
		$product_data_all = array();
		$apiKey = '534c4d6771f8ed3e03122be7c37b0aab-us14';
		$listId = '7316bed5ee';//live=7316bed5ee Test-a84152528a

		$dataCenter = substr($apiKey,strpos($apiKey,'-')+1);	  
		
		$url1 = 'ecommerce/stores/store_002/products';
		if($all_is_true!=''){
			$result_data = $this->Mailchimp_model->product_import($all_is_true);
			//$res = $this->get_mccart($all_is_true,'products');
			//echo '<pre>fff';print_r($res);exit;
			if(!empty($result_data)){ $product_data = array(); $i=0; $cat_tree = '';
				foreach($result_data as $val){

					$cat = $this->Mailchimp_model->get_products_categories($val['id']);
					/*$att = $this->Mailchimp_model->get_products_attributes($val['id']);
					$cat_att = array_merge($cat,$att);*/
					if(!empty($cat))
					{
						$cat_tree = implode('-',$cat);
					}

					$product_data['id'] = $val['id'];
					$product_data['title'] = $val['name'];
					$product_data['url'] = LIVE_SITE.'product/details/'.$val['slug'].'-'.$val['id'];
					$product_data['description'] = '';
					$product_data['type'] = $cat_tree;
					$product_data['vendor'] = $val['company_name'];
					$product_data['image_url'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_124/'.$val['image'];
					$product_data['published_at_foreign'] = $val['created_datetime'];
					
					$res = $this->Mailchimp_model->product_variants_import($val['id']);
					$k = 0;
					if(!empty($res)){ 
						foreach($res as $value){
							//$product_data['variants'][$k]['id'] = $val['id'].'_'.$value['size_id'];
							$product_data['variants'][$k]['id'] = $value['size_id'];
							$product_data['variants'][$k]['title'] = $value['size_text'];
							$product_data['variants'][$k]['url'] = LIVE_SITE.'product/details/'.$val['slug'].'-'.$val['id'];
							$product_data['variants'][$k]['sku'] = $value['product_sku'];
							$product_data['variants'][$k]['price'] = $val['price'];
							$product_data['variants'][$k]['inventory_quantity'] = (int)$value['stock_count'];
							$product_data['variants'][$k]['image_url'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_124/'.$val['image'];
							//$product_data['variants'][$k]['visibility'] = true;
							$k++;
						}
					}else{
						//$product_data['variants'] = array();
						$product_data['variants'][$k]['id'] = $o_sizeid;
						$product_data['variants'][$k]['title'] = $o_sizetext;
						$product_data['variants'][$k]['url'] = LIVE_SITE.'product/details/'.$val['slug'].'-'.$val['id'];
						$product_data['variants'][$k]['sku'] = '';
						$product_data['variants'][$k]['price'] = $val['price'];
						$product_data['variants'][$k]['inventory_quantity'] = 0;
						$product_data['variants'][$k]['image_url'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_124/'.$val['image'];
					}
					$product_data_all['operations'][$i]['path']= $url1;
					$product_data_all['operations'][$i]['method']= 'POST';
					$product_data_all['operations'][$i]['body'] = json_encode($product_data);
					$i++;
				}
			}
			//echo '<pre>';echo json_encode($product_data);exit;
			echo '<pre>';print_r($product_data);
			//echo json_encode($product_data_all); 
			$url = 'http://' .$dataCenter. '.api.mailchimp.com/3.0/batches/';
			$ch = curl_init($url);

			curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
			curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($product_data_all)); 

			curl_exec($ch);
			
			if(curl_exec($ch) === false)
		    {
		        echo 'Curl error: ' . curl_error($ch);
		    }
		    else
		    {
		        echo 'Operation completed without any errors';
		    }
		}
	}

	function get_mccart($typeid=Null,$type=Null,$userid=Null)
	{	
		/*  $type - customers , products , orders , carts 
			$typeid - 0 or any type id
		*/	
		$apiKey = '534c4d6771f8ed3e03122be7c37b0aab-us14';
	    $listId = '7316bed5ee';//live=7316bed5ee Test-a84152528a
	    $result_array = array();

	    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
	    if($type!='')
	    {
		    if($typeid!='')
		    {
		    	if($type=='lines')
		    	{
		    		// here type=carts, typeid(userid)
		    		$url = 'http://' . $dataCenter . '.api.mailchimp.com/3.0/ecommerce/stores/store_002/'.$type.'/'.$typeid.'/lines';
		    	}else
		    	{
		    		$url = 'http://' . $dataCenter . '.api.mailchimp.com/3.0/ecommerce/stores/store_002/'.$type.'/'.$typeid;
		    	}
		    }else
		    {
		    	$url = 'http://' . $dataCenter . '.api.mailchimp.com/3.0/ecommerce/stores/store_002/'.$type;
		    }
		}
	    
	    $ch = curl_init($url);	    

		curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
			    //curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			    //curl_setopt($ch, CURLOPT_POSTFIELDS, $json); 

		$result = curl_exec($ch);

		if(!empty($result))
		{
			$result_array = json_decode($result,true);			
		}
		
		echo '<pre>';print_r(json_decode($result,true));		
		echo '<br/>';
		echo 'Cart Line--';
		echo $cartLineCount = (@$result_array['lines']!='' ? count(@$result_array['lines']) : 0);
		if(!empty($result_array))
		{
			return $result_array;
		}else
		{
			echo false;			
		}
		return $result_array;

	}

	function mailchimp_update_cart($cartid=Null,$userid=Null)
	{		
		$mc_cartid = '';
		if($cartid!='')
		{
			$cart_result = $this->get_mccart($userid,'carts',$userid);
			echo '<pre>Cart-';print_r($cart_result);
			$mc_cartid = @$cart_result['id'];

			if(!empty($mc_cartid))
			{
				//update cart
				//$this->update_mccart($cart_result);
				
				$this->delete_mccart($mc_cartid,$userid);
				$this->addto_mccart($cartid,$userid);
			}else
			{	
				//Add to Cart
				$this->addto_mccart($cartid,$userid);
			}
		} 
	}

	function addto_mccart($cartid,$userid)
	{	
		$productid = '';
		$cart_result = $this->Mailchimp_model->get_Uniquecart($cartid);
		//echo '<pre>';print_r($cart_result);
		if(!empty($cart_result))
		{
			$productid = $cart_result[0]['product_id'];
			if(!empty($productid))
			{
				//echo 'Deleted';$this->delete_product($productid);exit;
				$this->add_product($productid);
				$res = $this->get_mccart($productid,'products');
				echo 'Added Product--<pre>';print_r($res);
				$this->add_cart($cartid,$userid);
				$res = $this->get_mccart($cartid,'carts');
				echo 'Added Cart--<pre>';print_r($res);
			}
		}
	}

	function add_cart($cartid,$userid)
	{
		$cartUnqresult = $this->Mailchimp_model->get_Uniquecart($cartid);
		//echo '<pre> cartUnqresult----<br/>';print_r($cartUnqresult);exit;
		//$userid = $cartUnqresult[0]['user_id'];
		$cart_result = $this->Mailchimp_model->get_cart($cartid,$userid);		
		//echo '<pre>';print_r($cartUnqresult);exit;
		foreach($cartUnqresult as $val)
		{	$cart_array= array();
			//$cart_array['id']='cart_'.$val['user_id'];
			//$cart_array['id'] = $cartid;
			$cart_array['id'] = $val['user_id'];
			//$cart_array['customer']['id'] = 'customer_'.$val['user_id'];
			$cart_array['customer']['id'] = $val['user_id'];
			$cart_array['customer']['email_address'] = $val['email'];
			$cart_array['customer']['opt_in_status'] = true;
			$cart_array['customer']['company'] = "";
			$cart_array['customer']['first_name'] = $val['first_name']!='' ? $val['first_name'] : " ";
			$cart_array['customer']['last_name'] = $val['last_name']!='' ? $val['last_name'] : " ";
			$cart_array['customer']['orders_count'] = (int)$val['product_qty'];
			$cart_array['customer']['total_spent'] = (int)($val['product_qty']*$val['product_price']);
			$cart_array['customer']['address']['address1'] = "";
			$cart_array['customer']['address']['address2'] = "";
			$cart_array['customer']['address']['city'] = "";
			$cart_array['customer']['address']['province'] = "";
			$cart_array['customer']['address']['province_code'] = "";
			$cart_array['customer']['address']['postal_code'] = "";
			$cart_array['customer']['address']['country'] = "";
			$cart_array['customer']['address']['country_code'] = "";
			$cart_array['campaign_id'] = "4621a28e71";
			$cart_array['checkout_url'] = "https://www.stylecracker.com/cartnew/checkout";
			$cart_array['currency_code'] = "INR";
			$cart_array['order_total'] = "0";
			$cart_array['tax_total'] = "0";	
			$i = 0;
			foreach($cart_result as $value)
			{ 
				/*if($val['user_id']==$value['user_id'])
				{*/
					$this->add_product($value['product_id']);
					$cart_array['lines'][$i]['id'] = $value['id'];
					$cart_array['lines'][$i]['product_id'] = $value['product_id'];
					$cart_array['lines'][$i]['product_title'] = $value['product_name'];
					$cart_array['lines'][$i]['product_variant_id'] = $value['product_size'];
					$cart_array['lines'][$i]['product_variant_title'] = $value['size_text'];
					$cart_array['lines'][$i]['quantity'] = (int)$value['product_qty'];
					$cart_array['lines'][$i]['price'] = $value['product_price'];				
					$i++;
					/*echo $value['product_id']."_".$value['product_size'];echo '<br/>';
					   $cartress = $this->get_mccart($value['product_id'],'products');	
	    			echo '<pre>';print_r($cartress);exit;*/
	    			
				//}
			}
			if(!empty($cart_array)){
				$json = json_encode($cart_array); 
			}
		}		
		/*echo '<pre>';print_r($cart_array);
		echo 'AddedToCart--<br/>';echo $json;exit;		*/
	   // echo '<pre>';print_r($json);

	     //ac--scmailchimp01
		$apiKey = '534c4d6771f8ed3e03122be7c37b0aab-us14';
	    $listId = '7316bed5ee';//live=7316bed5ee Test-a84152528a

	    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);	  
	    $url = 'http://' . $dataCenter . '.api.mailchimp.com/3.0/ecommerce/stores/store_002/carts';

	    $ch = curl_init($url);

	    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	    //curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);  

	    $result = curl_exec($ch);
	    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    if(curl_exec($ch) === false)
	    {
	        echo 'Curl error: ' . curl_error($ch);
	    }
	    else
	    {
	        echo 'Operation completed without any errors';
	        $result = $this->Mailchimp_model->getset_scoption('mailchimp_start_datetime','','Update_data');
	    }
	    curl_close($ch);
	    //return $httpCode;
	}	

	function delete_mccart($cartid=Null,$userid=Null){
		echo $cartid;echo $userid;
		$cart_result = $this->get_mccart($cartid,'carts');
		echo '<pre>Cart To Remove - '.$cartid;
		echo 'Total Items--'.($cartid==''? $cart_result['total_items'] : count($cart_result) );
		print_r($cart_result);
		$apiKey = '534c4d6771f8ed3e03122be7c37b0aab-us14';
	    $listId = '7316bed5ee';//live=7316bed5ee Test-a84152528a

	    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
	    if($cartid!=0)
	    {
	    	$url = 'http://' .$dataCenter. '.api.mailchimp.com/3.0/ecommerce/stores/store_002/carts/'.$cartid;	
	    }else
	    {
	    	$url = 'http://' .$dataCenter. '.api.mailchimp.com/3.0/ecommerce/stores/store_002/carts';
	    }

	    $ch = curl_init($url);
		curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
			    //curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			    //curl_setopt($ch, CURLOPT_POSTFIELDS, $json); 

		curl_exec($ch);		
		
	}

	function update_mccart($cartid,$userid=Null)
	{
		$cartUnqresult = $this->Mailchimp_model->get_Uniquecart($cartid);
		echo '<pre>UpdateCart-';print_r($cartUnqresult);
		$cart_array = array();
		if(!empty($cartUnqresult))
		{
			$productid = $cartUnqresult[0]['product_id'];
			if(!empty($productid))
			{
				//echo 'Deleted';$this->delete_product($productid);exit;
				$this->add_product($productid);
				$res = $this->get_mccart($productid,'products');
				echo 'Added Product--<pre>';print_r($res);
				/*$this->add_cart_line($cartid);
				$res = $this->get_mccart($cartid,'carts');
				echo 'Added Cart--<pre>';print_r($res);*/
				$cart_array['id'] = $cartUnqresult[0]['id'];
				$cart_array['product_id'] = $cartUnqresult[0]['product_id'];
				$cart_array['product_title'] = $cartUnqresult[0]['product_name'];
				$cart_array['product_variant_id'] = $cartUnqresult[0]['product_size'];
				$cart_array['product_variant_title'] = $cartUnqresult[0]['size_text'];
				$cart_array['quantity'] = (int)$cartUnqresult[0]['product_qty'];
				$cart_array['price'] = $cartUnqresult[0]['product_price'];	
			}
			echo $json = json_encode($cart_array);
			echo '<br/>';

			$apiKey = '534c4d6771f8ed3e03122be7c37b0aab-us14';
		    $listId = '7316bed5ee';//live=7316bed5ee Test-a84152528a

		    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);	  
		    $url = 'http://' . $dataCenter . '.api.mailchimp.com/3.0/ecommerce/stores/store_002/carts/'.$cartUnqresult[0]['id'].'/lines';

		    $ch = curl_init($url);

		    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		    //curl_setopt($ch, CURLOPT_POST, true);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);  

		    $result = curl_exec($ch);
		    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		    if(curl_exec($ch) === false)
		    {
		        echo 'Curl error: ' . curl_error($ch);
		    }
		    else
		    {
		        echo 'Operation completed without any errors';
		        $result = $this->Mailchimp_model->getset_scoption('mailchimp_start_datetime','','Update_data');
		    }
		    curl_close($ch);
		    //return $httpCode;
		    $res = $this->get_mccart('10668','carts');
				echo 'Cart Line Product--<pre>';print_r($res);	    

		}
	}

	function deleteCartLine($cartid=Null,$userid=Null,$lineid=Null)
	{
		echo $cartid;echo $userid;
		$cart_result = $this->get_mccart($userid,'carts');
		echo '<pre>CartLines To Remove - '.$cartid.'--'.$lineid;
		echo 'Total Items--'.($cartid==''? $cart_result['total_items'] : count($cart_result) );
		print_r($cart_result);

		$apiKey = '534c4d6771f8ed3e03122be7c37b0aab-us14';
	    $listId = '7316bed5ee';//live=7316bed5ee Test-a84152528a

	    $cartLineCount = (@$cart_result['lines']!='' ? count(@$cart_result['lines']) : 0);

	    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
	    if($cartid!='' && $lineid!='')
	    {
	    	if($cartLineCount>1)
	    	{
	    		$url = 'http://' .$dataCenter. '.api.mailchimp.com/3.0/ecommerce/stores/store_002/carts/'.$cartid.'/lines/'.$lineid;
	    	}else
	    	{
	    		$url = 'http://' .$dataCenter. '.api.mailchimp.com/3.0/ecommerce/stores/store_002/carts/'.$cartid;
	    	}

	    }else
	    {
	    	$url = 'http://' .$dataCenter. '.api.mailchimp.com/3.0/ecommerce/stores/store_002/carts';
	    }

	   /* $url = 'http://' .$dataCenter. '.api.mailchimp.com/3.0/ecommerce/stores/store_002/carts/'.$cartid;*/	    
	

	    $ch = curl_init($url);
		curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
			    //curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			    //curl_setopt($ch, CURLOPT_POSTFIELDS, $json); 

		curl_exec($ch);		
		/*$this->addto_mccart($cartid,$userid);*/
	}

	public function manualUserImport()
	{
		$startUserId = 41312;		
		$lastUserId = 42141;

		/*for($i=$startUserId;$i<$lastUserId;$i++)
		{
			echo $i;			
			$this->newUserImport($i);
			echo 'Done.<br/>';
		}*/
	}

	public function get_nonscdomain($email)
    {    	
        $allowed = array('stylecracker.com');

        $explodedEmail = explode('@', $email);
        $domain = array_pop($explodedEmail);
        $domain1 = strtolower($domain);
        if (!in_array($domain1, $allowed))
        {
            return true;
        }else
        {
            return false;
        }       
    }

    public function delete_orders($orderid=NULL,$orderline=NULL)
    {
    	echo $orderid;
		$order_result = $this->get_mccart($orderid,'orders');
		echo '<pre>Order To Remove - '.$orderid;
		echo 'Total Items--'.($orderid==''? $order_result['total_items'] : count($order_result) );
		print_r($order_result);
		$apiKey = '534c4d6771f8ed3e03122be7c37b0aab-us14';
	    $listId = '7316bed5ee';//live=7316bed5ee Test-a84152528a

	    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
	    if($orderid!='')
	    {
	    	if($orderline!='')
	    	{
	    		$url = 'http://' .$dataCenter. '.api.mailchimp.com/3.0/ecommerce/stores/store_002/orders/'.$orderid.'/lines/'.$orderline;
	    	}else
	    	{
	    		$url = 'http://' .$dataCenter. '.api.mailchimp.com/3.0/ecommerce/stores/store_002/orders/'.$orderid;
	    	}
	    		
	    }else
	    {
	    	$url = 'http://' .$dataCenter. '.api.mailchimp.com/3.0/ecommerce/stores/store_002/orders';
	    }

	    echo $url;
	    echo '<br/>';
	    $ch = curl_init($url);
		curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
			    //curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			    //curl_setopt($ch, CURLOPT_POSTFIELDS, $json); 

		curl_exec($ch);		
    }

    public function send_orderinfo($orderid)
    {
    	$order_details = $this->Mailchimp_model->get_orderDetails($orderid);
    	$order_line = $this->Mailchimp_model->get_orderLine($orderid);
    	echo "get_orderDetails-model===<pre>";print_r($order_details);
    	echo 'order_line==<pre>';print_r($order_line); 
    	$data_post = array();
    	$data_post['orderStatusList'] = array('1'=>'Processing', '2'=>'Confirmed', '3'=>'Dispatched', '4'=>'Delivered','5'=>'Cancelled','6'=>'Return','7'=>'Fake');
    	$order_status = "";

    	foreach($order_details as $val)
		{	$order_array= array();

			$order_array['id'] = $val['order_display_no'];
			$order_array['customer']['id'] = $val['user_id'];
			$order_array['customer']['opt_in_status'] = true;
			$order_array['customer']['email_address'] = $val['email_id'];
			$order_array['campaign_id'] = "4621a28e71";
			$order_array['financial_status'] = "";
			$order_array['checkout_url'] = "https://www.stylecracker.com/cartnew/checkout";
			$order_array['currency_code'] = "INR";
			$order_array['order_total'] = (int)$val['order_total'];
			$order_array['tax_total'] = (int)$val['tax_total'];
			$order_array['shipping_total'] = $val['shipping_total'];
			$order_array['processed_at_foreign'] = $val['created_datetime'];
			//$order_array['cancelled_at_foreign'] = "";
			$order_array['updated_at_foreign'] = $val['modified_datetime']!=''? $val['modified_datetime'] : date('Y-m-d H:i:s');
			$order_array['shipping_address']['name'] = $val['first_name'].' '.$val['last_name'];
			$order_array['shipping_address']['address1'] = $val['shipping_address'];
			$order_array['shipping_address']['address2'] = "";
			$order_array['shipping_address']['city'] = $val['city_name'];
			$order_array['shipping_address']['province'] = $val['statename'];
			$order_array['shipping_address']['province_code'] = $val['statename'];
			$order_array['shipping_address']['postal_code'] = $val['pincode'];
			$order_array['shipping_address']['country'] = $val['country_name']!="" ? $val['country_name'] : "INDIA";
			$order_array['shipping_address']['country_code'] = $val['country_name']!="" ? $val['country_name'] : "IN";
			$order_array['shipping_address']['phone'] = $val['mobile_no'];
			$order_array['billing_address']['name'] = $val['first_name'].' '.$val['last_name'];
			$order_array['billing_address']['address1'] = $val['shipping_address'];
			$order_array['billing_address']['address2'] = "";
			$order_array['billing_address']['city'] = $val['city_name'];
			$order_array['billing_address']['province'] = $val['statename'];
			$order_array['billing_address']['province_code'] = $val['statename'];
			$order_array['billing_address']['postal_code'] = $val['pincode'];
			$order_array['billing_address']['country'] = $val['country_name']!="" ? $val['country_name'] : "INDIA";
			$order_array['billing_address']['country_code'] = $val['country_name']!="" ? $val['country_name'] : "IN";
			$order_array['billing_address']['phone'] = $val['mobile_no'];
			//$order_array['lines'] = $order_line;
			$i=0;
				foreach($order_line as $value)
	    		{	 
	    			$this->add_product($value['product_id'],$value['product_variant_id'],$value['product_variant_title']);
	    			 $prd_result = $this->get_mccart($value['product_id'],'products');
	    			if(@$prd_result['status']=='404')
	    			{
	    				$this->add_product($value['product_id'],$value['product_variant_id'],$value['product_variant_title']);
	    			}

	    			$order_array['lines'][$i]['id']=$value['id'];
	    			$order_array['lines'][$i]['product_id']=$value['product_id'];
	    			$order_array['lines'][$i]['product_title']=$value['product_title'];
	    			$order_array['lines'][$i]['product_variant_id']=$value['product_variant_id'];
	    			$order_array['lines'][$i]['product_variant_title']=$value['product_variant_title'];
	    			$order_array['lines'][$i]['quantity']=(int)$value['quantity'];
	    			$order_array['lines'][$i]['price']=$value['price'];

		            if($value['order_status'] == 1)
		            {
		                $order_status = $order_status.'P-';
		                //$order_status = 'false';
		            }else if($value['order_status'] == 2)
		            {
		                $order_status = $order_status.'C-';
		               // $order_status = 'true';
		            }else if($value['order_status'] == 3)
		            {
		                $order_status = $order_status.'Di-';
		                // $order_status = 'true';
		            }else if($value['order_status'] == 4)
		            {
		                $order_status = $order_status.'De-';
		                 //$order_status = 'true';
		            }else if($value['order_status'] == 5)
		            {
		                $order_status = $order_status.'C-';
		                //$order_status = 'false';
		            }else if($value['order_status'] == 6)
		            {
		                $order_status = $order_status.'R-';
		            }else if($value['order_status'] == 7)
		            {
		                $order_status = $order_status.'F-';
		            }
	    			$i++;
	    		}
	    	$order_array['financial_status'] = $order_status;	    
		}

		echo '<pre>';print_r($order_array);
		if(!empty($order_array))
		{
			$json = json_encode($order_array); 
		}
		echo $json;

		   //ac--scmailchimp01
		$apiKey = '534c4d6771f8ed3e03122be7c37b0aab-us14';
	    $listId = '7316bed5ee';//live=7316bed5ee Test-a84152528a

	    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);	  
	    $url = 'http://' . $dataCenter . '.api.mailchimp.com/3.0/ecommerce/stores/store_002/orders';

	    $ch = curl_init($url);

	    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	    //curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);  

	    $result = curl_exec($ch);
	    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    if(curl_exec($ch) === false)
	    {
	        echo 'Curl error: ' . curl_error($ch);
	    }
	    else
	    {
	        echo 'Operation completed without any errors';
	        //$result = $this->Mailchimp_model->getset_scoption('mailchimp_start_datetime','','Update_data');
	    }
	    curl_close($ch);

	    $odr_result = $this->get_mccart($orderid,'orders');
	   /* if($odr_result['status']=='404')
	    {
	    	$this->send_orderinfo($orderid);exit;
	    }*/
    }

    function manualOrderImport()
    {
    	$order_array = $this->Mailchimp_model->get_ordernos();
    	//echo '<pre>'; print_r($order_array);
    	$orderCount = count($order_array);
    	for($i=0;$i<count($order_array);$i++)
    	{
    		/*echo $orderNo = $order_array[$i]['order_unique_no'];
    		echo '<br/>';*/
    		echo $orderDisNo = $order_array[$i]['order_display_no'];	
    		echo '<br/>';
    		/*$this->delete_orders($orderDisNo);*/
    		/*$this->send_orderinfo($orderNo);*/
    		//$this->get_mccart($orderDisNo,'orders');    		
    		
    	}
    }

    function manualOrderImportNew()
    {
		    	/*$order_array = array('18116_19',
		'24214_18',
		'24126_17',
		'24114_14_15_16',
		'12_13',
		'10760_12',
		'22981_11',
		'24111_10',
		'8243_8_9',
		'21655_7',
		'4267_4_5_6',
		'21655_3',
		'10214_2',
		'21655_1');*/
		$order_array = array('SC1476505295','SC1476504372','SC1476468151','SC1476466906','SC1476464927','SC1476462359');


    	//echo '<pre>'; print_r($order_array);
    	$orderCount = count($order_array);
    	echo 'Not Done ';
    	foreach($order_array as $val)
    	{
    		//echo $orderNo = $val;
    		$orderDisNo = $val;	
    		//echo '<br/>';
    		/*$this->delete_orders($orderDisNo);*/
    		//$this->send_orderinfo($orderNo);
    		$output = $this->get_mccart($orderDisNo,'orders');    	
    		//echo '<pre>';print_r($output);
    		
    		if(isset($output['status']))
    		{
    			
    			echo $orderDisNo;
    			echo '<br/>';
    		}else
    		{
    			echo '<br/>';
    		}
    		
    	}
    } 

    function OrderImportCron()
    { 
    	$orders = $this->Mailchimp_model->get_tdyOrders(); 
    	echo '<pre>';print_r($orders);
    	foreach($orders as $val)
    	{
    		$orderDisNo = $val['order_display_no'];
    		$orderNo = $val['order_unique_no']; 
    		echo '<br/>';
    		if($this->Mailchimp_model->check_fakeOrders($orderNo)) 
    		{  		
	    		$this->send_orderinfo($orderNo);
	    		
	    		$order_result = $this->get_mccart($orderDisNo,'orders');
	    		if(@$order_result['status']=='404')
	    		{
	    			echo 'NotDone---'.$orderDisNo.'  '.$orderNo;
	    		}	    		

	    	}else
	    	{

	    	}
	    		
    	}   	
    }

    function product_cron($type,$typeid=null)
    {
       	$product_data_all = array();
		$apiKey = '534c4d6771f8ed3e03122be7c37b0aab-us14';
		$listId = '7316bed5ee';

		$dataCenter = substr($apiKey,strpos($apiKey,'-')+1);	  
		
		$url1 = 'ecommerce/stores/store_002/products';				
		$result_data = $this->Mailchimp_model->product_import_cron($type,$typeid);				

			//echo '<pre>';print_r($result_data);
			// echo 'Count--'.count($result_data);
			// exit;
			//$res = $this->get_mccart($all_is_true,'products');
			//echo '<pre>fff';print_r($res);exit;
			$totalcount = count($result_data);
			echo 'Total-Count--'.count($result_data);

			if($type == 'update')
			{		
				foreach($result_data as $value){				
					$this->delete_product($value['id']);
				}
			}
			if(!empty($result_data)){ $product_data = array(); $i=0; $cat = array(); $cat_tree = '';$message='';
				foreach($result_data as $val){
					$message = $message.'   '.$val['id'];
					echo $val['id'].'<br/>';						

					$cat = $this->Mailchimp_model->get_products_categories($val['id']);
					/*$att = $this->Mailchimp_model->get_products_attributes($val['id']);
					$cat_att = array_merge($cat,$att);*/
					if(!empty($cat))
					{
						$cat_tree = implode('-',$cat);
					}
					
					//echo '<pre>';print_r($cat_att);exit;

					$product_data['id'] = $val['id'];
					$product_data['title'] = $val['name'];
					$product_data['url'] = LIVE_SITE.'product/details/'.$val['slug'].'-'.$val['id'];
					$product_data['description'] = '';
					$product_data['type'] = $cat_tree;
					$product_data['vendor'] = $val['company_name'];
					$product_data['image_url'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_124/'.$val['image'];
					$product_data['published_at_foreign'] = $val['created_datetime'];

					unset($res);
					$res = array();
					$res = $this->Mailchimp_model->product_variants_import($val['id']);
					if(!empty($res)){ $k = 0;
						foreach($res as $value){
							//$product_data['variants'][$k]['id'] = $val['id'].'_'.$value['size_id'];
							$product_data['variants'][$k]['id'] = $value['size_id'];
							$product_data['variants'][$k]['title'] = $value['size_text'];
							$product_data['variants'][$k]['url'] = LIVE_SITE.'product/details/'.$val['slug'].'-'.$val['id'];
							$product_data['variants'][$k]['sku'] = $value['product_sku'];
							$product_data['variants'][$k]['price'] = $val['price'];
							$product_data['variants'][$k]['inventory_quantity'] = (int)$value['stock_count'];
							$product_data['variants'][$k]['image_url'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_124/'.$val['image'];
							//$product_data['variants'][$k]['visibility'] = true;
							$k++;
						}
						
					}else{
						//$product_data['variants'] = array();
						/*$product_data['variants'][$k]['id'] = $o_sizeid;
						$product_data['variants'][$k]['title'] = $o_sizetext;
						$product_data['variants'][$k]['url'] = LIVE_SITE.'product/details/'.$val['slug'].'-'.$val['id'];
						$product_data['variants'][$k]['sku'] = '';
						$product_data['variants'][$k]['price'] = $val['price'];
						$product_data['variants'][$k]['inventory_quantity'] = 0;
						$product_data['variants'][$k]['image_url'] = 'https://www.stylecracker.com/sc_admin/assets/products/thumb_124/'.$val['image'];*/
					}
					$product_data_all['operations'][$i]['path']= $url1;
					$product_data_all['operations'][$i]['method']= 'POST';
					$product_data_all['operations'][$i]['body'] = json_encode($product_data);
					$i++;					
				}
			}
			/*echo '<pre>';print_r($product_data);exit;
			echo '<pre>';echo json_encode($product_data);exit;*/
			//echo json_encode($product_data_all); 
			$url = 'http://' .$dataCenter. '.api.mailchimp.com/3.0/batches/';
			$ch = curl_init($url);

			curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
			curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($product_data_all)); 

			curl_exec($ch);
			
			if(curl_exec($ch) === false)
		    {
		        echo 'Curl error: ' . curl_error($ch);
		    }
		    else
		    {
		        echo 'Operation completed without any errors';
		          	$config['protocol'] = 'smtp';
			  //      $config['mailpath'] = '/usr/sbin/sendmail';
			        $config['charset'] = 'iso-8859-1';
			        $config['mailtype'] = 'html';
			        $config['wordwrap'] = TRUE;
			        $config['smtp_host'] = $this->config->item('smtp_host');
			        $config['smtp_user'] = $this->config->item('smtp_user');
			        $config['smtp_pass'] = $this->config->item('smtp_pass');
			        $config['smtp_port'] = $this->config->item('smtp_port');

			        $this->email->initialize($config);

			        $this->email->from($this->config->item('from_email'), 'Stylecracker-MailChimpCron-'.$type);
			        $this->email->to('sudha@stylecracker.com');
			        $this->email->cc(array('arun@stylecracker.com','sudhags147@gmail.com'));
			        $this->email->subject('MailchimpCron-'.$type);
			        $new_msg = $message.'<br/>'.'Total Count--'.$totalcount;
			        $this->email->message($new_msg);

			        $this->email->send();
		    }		
    }

     function mccart_cron()
    {
    	$cartdata = $this->Mailchimp_model->get_cart_user();
    	
    	$totalcount = 0;
    	$message = '';
    	$totalcount = count($cartdata);
    	foreach($cartdata as $val)
    	{
    		$message = $message.' '.$val['userid'];
    		$this->delete_mccart($val['userid']);    		
    	}
    	foreach($cartdata as $val)
    	{
    		$this->add_cart($val['cartid'],$val['userid']);
    	}
    		$config['protocol'] = 'smtp';
			  //      $config['mailpath'] = '/usr/sbin/sendmail';
	        $config['charset'] = 'iso-8859-1';
	        $config['mailtype'] = 'html';
	        $config['wordwrap'] = TRUE;
	        $config['smtp_host'] = $this->config->item('smtp_host');
	        $config['smtp_user'] = $this->config->item('smtp_user');
	        $config['smtp_pass'] = $this->config->item('smtp_pass');
	        $config['smtp_port'] = $this->config->item('smtp_port');

	        $this->email->initialize($config);

	        $this->email->from($this->config->item('from_email'), 'Stylecracker-MailChimpCron-'.$type);
	        $this->email->to('sudha@stylecracker.com');
	        $this->email->cc(array('arun@stylecracker.com','sudhags147@gmail.com'));
	        $this->email->subject('MailchimpCron- CartSync');
	        $new_msg = $message.'<br/>'.'Total Count--'.$totalcount;
	        $this->email->message($new_msg);
	        $this->email->send();
    }



}

 ?>